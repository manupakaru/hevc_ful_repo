`timescale 1ns / 10ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:38:22 12/16/2013 
// Design Name: 
// Module Name:    video_tx_top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module video_tx_top_dp(
		reset,
		vid_clk,
        vid_run_clk_75 ,
        vid_run_clk_150 ,
        config_done ,
		uhd_en_in,
		uhd_en_reg,
		fifo_data_in,
		fifo_empty,
		fifo_prog_empty,
		fifo_rd_en,
		vid_hsync ,
		vid_vsync ,
		vid_enable ,
		vid_rst,
		vid_clk_en ,
		vid_oddeven,
		vid_pixel0 ,
		vid_pixel1 ,
		vid_pixel2 ,
		vid_pixel3  
    );
	
	
	`include "video_parameters.v"

	
//-------------------------------------------------------------------------------
// I/O signals
//-------------------------------------------------------------------------------
    
    input                               vid_run_clk_75;
    input                               vid_run_clk_150;
	input						reset;
	input						config_done;
	input						uhd_en_in;

	output	                         vid_clk;	
	input	[63:0]				fifo_data_in;
	input						fifo_empty;
	input						fifo_prog_empty;
	output						fifo_rd_en;
	
	output	reg					vid_hsync  ;
	output  reg                 vid_vsync  ;
	output  reg                 vid_enable ;
	output	reg					vid_rst    ;
	output	reg					vid_clk_en ;
	output	reg					uhd_en_reg ;
	output						vid_oddeven;
	
	output	[47:0]				vid_pixel0 ;
	output	[47:0]				vid_pixel1 ;
	output	[47:0]				vid_pixel2 ;
	output	[47:0]				vid_pixel3 ;
	
//-------------------------------------------------------------------------------
// parameter definitions
//-------------------------------------------------------------------------------
    parameter					FREQ_IN		=	150;
	parameter					FREQ_OUT	=	75;
	parameter					VIDEO_OUT	=	"1080P30";

//-------------------------------------------------------------------------------
// localparam definitions
//-------------------------------------------------------------------------------
    localparam					STATE_INIT						=	0 ;
	
	localparam                  STATE_VIDEO_TX                  = 	1 ;
	localparam                  STATE_VIDEO_TX_1                = 	2 ;
	localparam                  STATE_VIDEO_TX_2                = 	3 ;
	localparam                  STATE_VIDEO_TX_3                = 	4 ;
	localparam                  STATE_VIDEO_TX_4                = 	5 ;
	
	
//-------------------------------------------------------------------------------
// Internal wires and registers
//-------------------------------------------------------------------------------
    
	
	(* MARK_DEBUG *) reg		[23:0]				pclk_count;
	(* MARK_DEBUG *) reg		[12:0]				hsync_count;
	(* MARK_DEBUG *) reg		[12:0]				DE_count;
	reg 	[47:0]				D_out;
	
	reg							fifo_rd_en;
	reg		[19:0]				disp_count;
	reg							fifo_pixel_count;
	
	reg		[23:0]				FRAME_PCLK_COUNT	  ;
	reg		[12:0]				HSYNC_PCLK_COUNT	  ;
	reg		[23:0]				DE_START_PCLK_COUNT	  ;
	reg		[23:0]				DE_STOP_PCLK_COUNT	  ;
	reg		[12:0]				HSYNC_HIGH_PCLK_COUNT ;
	reg		[23:0]				VSYNC_HIGH_PCLK_COUNT ;
	reg		[12:0]				DE_HIGH_PCLK_COUNT	  ;
	reg		[12:0]				DE_LOW_PCLK_COUNT	  ;
	
	wire	[7:0]				Y_0;
	wire	[7:0]				Y_1;
	wire	[7:0]				Y_2;
	wire	[7:0]				Y_3;
	wire	[7:0]				CB_0;
	wire	[7:0]				CR_0;
	wire	[7:0]				CB_2;
	wire	[7:0]				CR_2;
	wire	[7:0]				R_0;
	wire	[7:0]				G_0;
	wire	[7:0]				B_0;
	wire	[7:0]				R_1;
	wire	[7:0]				G_1;
	wire	[7:0]				B_1;
	wire	[7:0]				R_2;
	wire	[7:0]				G_2;
	wire	[7:0]				B_2;
	wire	[7:0]				R_3;
	wire	[7:0]				G_3;
	wire	[7:0]				B_3;
	
	(* MARK_DEBUG *) integer						state;

    //assign state_out = state;
//-------------------------------------------------------------------------------
// Implementation
//-------------------------------------------------------------------------------
	
	
	assign	vid_pixel0  =  {D_out[23:16], 8'd0, D_out[15:8] , 8'd0, D_out[7:0]  , 8'd0} ;
	assign	vid_pixel1  =  {D_out[47:40], 8'd0, D_out[39:32], 8'd0, D_out[31:24], 8'd0} ;
	assign	vid_pixel2  =  48'd0 ;
	assign	vid_pixel3  =  48'd0 ;
	
	
	//-------------------------------Generate sync and DE signals--------------------------------- //
	                                                                                               //
	always@(posedge vid_clk or posedge reset) begin                                                //
		if(reset) begin                                                                            //
			pclk_count 	<= 24'd0;	 // 1                                                          //
			hsync_count <= 13'd0;                                                                  //
			DE_count 	<= 13'd0;                                                                  //
			//pclk_en <= 1'b1;	//----------------for now                                          //
		end                                                                                        //
		else if(vid_clk_en) begin                                                                  //
			//-----Increment pclk counter-----------                                               //
			if(pclk_count == FRAME_PCLK_COUNT) begin                                               //
				pclk_count <= 24'd1;                                                               //
			end                                                                                    //
			else begin                                                                             //
				pclk_count <= pclk_count + 1'b1 ;                                                  //
			end                                                                                    //
			//--------------------------------------                                               //
			                                                                                       //
			//-----Generate Hsync-------------------                                               //
			if(hsync_count == HSYNC_PCLK_COUNT) begin                                              //
				hsync_count <= 13'd1;                                                              //
			end                                                                                    //
			else begin                                                                             //
				hsync_count <= hsync_count + 1'b1 ;                                                //
			end                                                                                    //
			                                                                                       //
			//-----Generate DE---------------------                                                //
			if(pclk_count >= DE_START_PCLK_COUNT && pclk_count < DE_STOP_PCLK_COUNT) begin         //
				if(DE_count == HSYNC_PCLK_COUNT) begin                                             //
					DE_count <= 13'd1;                                                             //
				end                                                                                //
				else begin                                                                         //
					DE_count <= DE_count + 1'b1;                                                   //
				end                                                                                //
			end                                                                                    //
			else begin                                                                             //
				DE_count <= 13'd0; // 1                                                            //
			end                                                                                    //
			                                                                                       //
		end                                                                                        //
	end                                                                                            //
	                                                                                               //
	                                                                                               //
	always@(*) begin                                                                               //
		//-----Generate Hsync-------------------                                                   //
		if(hsync_count < HSYNC_HIGH_PCLK_COUNT || hsync_count == HSYNC_PCLK_COUNT) begin           //
			vid_hsync = 1'b1;                                                                      //
		end                                                                                        //
		else begin                                                                                 //
			vid_hsync = 1'b0;                                                                      //
		end                                                                                        //
		//-----Generate Vsync------------------                                                    //
		if(pclk_count < VSYNC_HIGH_PCLK_COUNT || pclk_count == FRAME_PCLK_COUNT) begin             //
			vid_vsync = 1'b1;                                                                      //
		end                                                                                        //
		else begin                                                                                 //
			vid_vsync = 1'b0;                                                                      //
		end                                                                                        //
		//-----Generate DE---------------------                                                    //
		if(DE_count >= DE_HIGH_PCLK_COUNT &&  DE_count < DE_LOW_PCLK_COUNT) begin                  //
			vid_enable = 1'b1;                                                                     //
		end                                                                                        //
		else begin                                                                                 //
			vid_enable = 1'b0;                                                                     //
		end                                                                                        //
	end                                                                                            //
	                                                                                               //
	//-------------------------------------------------------------------------------------------- //
	
	
	
	
	//------------------------------Main SM to transmit video----------------------------------------------
	
	always@(posedge vid_clk or posedge reset) begin
		if(reset) begin
			vid_rst <= 1'b1;
			vid_clk_en <= 1'b0;
			disp_count <= 20'd0;
			state	<= STATE_INIT;
		end
		else begin
			case(state)
				
				STATE_INIT : begin		                 
					if(config_done) begin
						state	<= STATE_VIDEO_TX;  
					end                                  
				end
				
				//-----------Transmit video data--------------------------------------------------// //
				
				STATE_VIDEO_TX : begin
					if(fifo_prog_empty == 1'b0) begin
						vid_clk_en <= 1'b1;				// wait till data is available
						vid_rst <= 1'b0;
						fifo_pixel_count <= 1'b0;
						D_out <= {R_1,G_1,B_1,R_0,G_0,B_0};
						state <= STATE_VIDEO_TX_1;
					end	
				end
				
				STATE_VIDEO_TX_1 : begin
					if(fifo_empty) begin
						state <= STATE_VIDEO_TX_1;
					end
					else begin
						case(fifo_pixel_count)
							1'b0 : begin
								if(vid_enable) begin
									D_out <= {R_3,G_3,B_3,R_2,G_2,B_2};
									fifo_pixel_count <= fifo_pixel_count + 1'b1 ;
									//fifo_rd_en  =  1'b1;
								end
							end
							1'b1 : begin
								if(vid_enable) begin
									D_out <= {R_1,G_1,B_1,R_0,G_0,B_0};
									fifo_pixel_count <= fifo_pixel_count + 1'b1 ;
								end
							end
						endcase
						state <= STATE_VIDEO_TX_1; 			// coincide with rising edge of pclk 
					end
				end
				STATE_VIDEO_TX_3 : begin
					if(pclk_count == FRAME_PCLK_COUNT) begin		// continue till end of the frame
						state <= STATE_VIDEO_TX;
					end
				end
				
				
			endcase
		end
	end
	
	
	always@(*) begin
		fifo_rd_en = 1'b0;
		if((state == STATE_VIDEO_TX_1) && (fifo_empty==0) && (fifo_pixel_count==0) && (vid_enable==1)) begin
			fifo_rd_en = 1'b1;
		end
	end
	
	
	//----------------------- HD to 4K Switching using VIO -----------------------------
		
// BUFGMUX_CTRL: 2-to-1 Global Clock MUX Buffer
// 7 Series
// Xilinx HDL Libraries Guide, version 14.1

BUFGMUX_CTRL BUFGMUX_CTRL_inst (
    .O(vid_clk),         // 1-bit output: Clock output
    .I0(vid_run_clk_75),         // 1-bit input: Clock input (S=0)
    .I1(vid_run_clk_150),         // 1-bit input: Clock input (S=1)
    .S(uhd_en_reg)         // 1-bit input: Clock select
);

// End of BUFGMUX_CTRL_inst instantiation

	
	always@(posedge vid_clk or posedge reset) begin
		if(reset) begin
			uhd_en_reg <= 1'b0;
		end
		else begin
			if(pclk_count == 1) begin
				uhd_en_reg <= uhd_en_in;
			end
		end
	end
	
	
	always@(*) begin
		if(uhd_en_reg) begin
			FRAME_PCLK_COUNT		=	UHD_FRAME_PCLK_COUNT	   ;
			HSYNC_PCLK_COUNT		=	UHD_HSYNC_PCLK_COUNT	   ;
			DE_START_PCLK_COUNT	    =	UHD_DE_START_PCLK_COUNT	   ;
			DE_STOP_PCLK_COUNT	    =	UHD_DE_STOP_PCLK_COUNT	   ;
			HSYNC_HIGH_PCLK_COUNT   =	UHD_HSYNC_HIGH_PCLK_COUNT  ;
			VSYNC_HIGH_PCLK_COUNT   =	UHD_VSYNC_HIGH_PCLK_COUNT  ;
			DE_HIGH_PCLK_COUNT	    =	UHD_DE_HIGH_PCLK_COUNT	   ;
			DE_LOW_PCLK_COUNT	    =	UHD_DE_LOW_PCLK_COUNT	   ;
		end
		else begin
			FRAME_PCLK_COUNT		=	HD_FRAME_PCLK_COUNT	      ;
			HSYNC_PCLK_COUNT		=	HD_HSYNC_PCLK_COUNT	      ;
			DE_START_PCLK_COUNT	    =	HD_DE_START_PCLK_COUNT	  ;
			DE_STOP_PCLK_COUNT	    =	HD_DE_STOP_PCLK_COUNT	  ;
			HSYNC_HIGH_PCLK_COUNT   =	HD_HSYNC_HIGH_PCLK_COUNT  ;
			VSYNC_HIGH_PCLK_COUNT   =	HD_VSYNC_HIGH_PCLK_COUNT  ;
			DE_HIGH_PCLK_COUNT	    =	HD_DE_HIGH_PCLK_COUNT	  ;
			DE_LOW_PCLK_COUNT	    =	HD_DE_LOW_PCLK_COUNT	  ;
		end
	end
	
	
	
	//----------------------- YCbCr to RGB Conversion -----------------------------
	
	rgb_pixel_converter pixel_0(
		.Y_in (Y_0),
		.CB_in(CB_0),
		.CR_in(CR_0),
		.R_out(R_0),
		.G_out(G_0),
		.B_out(B_0)
    );
	
	rgb_pixel_converter pixel_1(
		.Y_in (Y_1),
		.CB_in(CB_0),
		.CR_in(CR_0),
		.R_out(R_1),
		.G_out(G_1),
		.B_out(B_1)
    );
	
	rgb_pixel_converter pixel_2(
		.Y_in (Y_2),
		.CB_in(CB_2),
		.CR_in(CR_2),
		.R_out(R_2),
		.G_out(G_2),
		.B_out(B_2)
    );
	
	rgb_pixel_converter pixel_3(
		.Y_in (Y_3),
		.CB_in(CB_2),
		.CR_in(CR_2),
		.R_out(R_3),
		.G_out(G_3),
		.B_out(B_3)
    );
	
	
	assign		Y_0  	=  fifo_data_in[7:0] ;
	assign		Y_1  	=  fifo_data_in[23:16] ;
	assign		Y_2  	=  fifo_data_in[39:32] ;
	assign		Y_3  	=  fifo_data_in[55:48] ;
	assign		CB_0  	=  fifo_data_in[15:8] ;
	assign		CR_0  	=  fifo_data_in[31:24] ;
	assign		CB_2  	=  fifo_data_in[47:40] ;
	assign		CR_2  	=  fifo_data_in[63:56] ;
	
	

endmodule
