module vid_crc_16 (
  input wire        vid_clk,
  input wire        vid_reset,
  input wire        vsync,
  input wire        hsync,
  input wire        enable,
  input wire [47:0] pix0,
  input wire [47:0] pix1,
  input wire [47:0] pix2,
  input wire [47:0] pix3,
  input wire [2:0]  bpc,
  input wire [1:0]  user_pix_mode,

  output reg        frame_crc_vld,
  output reg [15:0] rcrc0_out,
  output reg [15:0] gcrc0_out,
  output reg [15:0] bcrc0_out,
  output reg [15:0] rcrc1_out,
  output reg [15:0] gcrc1_out,
  output reg [15:0] bcrc1_out,
  output reg [15:0] rcrc2_out,
  output reg [15:0] gcrc2_out,
  output reg [15:0] bcrc2_out,
  output reg [15:0] rcrc3_out,
  output reg [15:0] gcrc3_out,
  output reg [15:0] bcrc3_out
);

reg  [15:0] rcrc0;
reg  [15:0] gcrc0;
reg  [15:0] bcrc0;
reg  [15:0] rcrc1;
reg  [15:0] gcrc1;
reg  [15:0] bcrc1;
reg  [15:0] rcrc2;
reg  [15:0] gcrc2;
reg  [15:0] bcrc2;
reg  [15:0] rcrc3;
reg  [15:0] gcrc3;
reg  [15:0] bcrc3;
reg  [15:0] r0, g0, b0;
reg  [15:0] r1, g1, b1;
reg  [15:0] r2, g2, b2;
reg  [15:0] r3, g3, b3;
wire [15:0] crcr0, crcg0, crcb0;
wire [15:0] crcr1, crcg1, crcb1;
wire [15:0] crcr2, crcg2, crcb2;
wire [15:0] crcr3, crcg3, crcb3;

(* KEEP = "TRUE" *) (* MARK_DEBUG="true" *) reg        vsync_d;
(* KEEP = "TRUE" *) (* MARK_DEBUG="true" *) reg        hsync_d;
(* KEEP = "TRUE" *) (* MARK_DEBUG="true" *) reg        enable_d;
(* KEEP = "TRUE" *) reg [47:0] pix0_d;
(* KEEP = "TRUE" *) reg [47:0] pix1_d;
(* KEEP = "TRUE" *) reg [47:0] pix2_d;
(* KEEP = "TRUE" *) reg [47:0] pix3_d;
(* KEEP = "TRUE" *) (* MARK_DEBUG="true" *) reg [2:0]  bpc_d;
(* KEEP = "TRUE" *) reg [1:0]  user_pix_mode_d;
reg        vsync_2d;
reg        vid_vsync_mask;

always @ ( posedge vid_clk )
begin
   if ( vid_reset )
   begin
      frame_crc_vld   <= 1'b 0;
      rcrc0_out       <= 16'b 0;
      gcrc0_out       <= 16'b 0;
      bcrc0_out       <= 16'b 0;
      rcrc1_out       <= 16'b 0;
      gcrc1_out       <= 16'b 0;
      bcrc1_out       <= 16'b 0;
      rcrc2_out       <= 16'b 0;
      gcrc2_out       <= 16'b 0;
      bcrc2_out       <= 16'b 0;
      rcrc3_out       <= 16'b 0;
      gcrc3_out       <= 16'b 0;
      bcrc3_out       <= 16'b 0;

      rcrc0           <= 16'b 0;
      gcrc0           <= 16'b 0;
      bcrc0           <= 16'b 0;
      rcrc1           <= 16'b 0;
      gcrc1           <= 16'b 0;
      bcrc1           <= 16'b 0;
      rcrc2           <= 16'b 0;
      gcrc2           <= 16'b 0;
      bcrc2           <= 16'b 0;
      rcrc3           <= 16'b 0;
      gcrc3           <= 16'b 0;
      bcrc3           <= 16'b 0;
      vsync_d         <= 1'b 0;
      hsync_d         <= 1'b 0;
      enable_d        <= 1'b 0;
      pix0_d          <= 48'b 0;
      pix1_d          <= 48'b 0;
      pix2_d          <= 48'b 0;
      pix3_d          <= 48'b 0;
      bpc_d           <= 3'b 0;
      user_pix_mode_d <= 2'b 0;
      vsync_2d        <= 1'b 0;
      vid_vsync_mask  <= 1'b 0;
   end
   else
   begin
      vsync_d         <= vsync;
      hsync_d         <= hsync;
      enable_d        <= enable;
      pix0_d          <= pix0;
      pix1_d          <= pix1;
      pix2_d          <= pix2;
      pix3_d          <= pix3;
      bpc_d           <= bpc;
      user_pix_mode_d <= user_pix_mode;
      vsync_2d        <= vsync_d;

      if ( ( vsync_2d ^ vsync_d ) & ~vid_vsync_mask )
         vid_vsync_mask   <= 1'b 1;
      else if ( ( vsync_2d ^ vsync_d ) & vid_vsync_mask )
         vid_vsync_mask   <= 1'b 0;

      if ( ( vsync_2d ^ vsync_d ) & ~vid_vsync_mask )
      begin
        rcrc0_out      <= rcrc0;
        gcrc0_out      <= gcrc0;
        bcrc0_out      <= bcrc0;
        rcrc1_out      <= rcrc1;
        gcrc1_out      <= gcrc1;
        bcrc1_out      <= bcrc1;
        rcrc2_out      <= rcrc2;
        gcrc2_out      <= gcrc2;
        bcrc2_out      <= bcrc2;
        rcrc3_out      <= rcrc3;
        gcrc3_out      <= gcrc3;
        bcrc3_out      <= bcrc3;
      end
      frame_crc_vld   <= vid_vsync_mask;
      

      if ( vsync_2d ^ vsync_d )
      begin
        rcrc0          <= 16'b 0;
        gcrc0          <= 16'b 0;
        bcrc0          <= 16'b 0;
        rcrc1          <= 16'b 0;
        gcrc1          <= 16'b 0;
        bcrc1          <= 16'b 0;
        rcrc2          <= 16'b 0;
        gcrc2          <= 16'b 0;
        bcrc2          <= 16'b 0;
        rcrc3          <= 16'b 0;
        gcrc3          <= 16'b 0;
        bcrc3          <= 16'b 0;
      end
      else if ( enable_d & ~hsync_d & ~vsync_d )
      begin
        rcrc0          <= crcr0;
        gcrc0          <= crcg0;
        bcrc0          <= crcb0;
        rcrc1          <= crcr1;
        gcrc1          <= crcg1;
        bcrc1          <= crcb1;
        rcrc2          <= crcr2;
        gcrc2          <= crcg2;
        bcrc2          <= crcb2;
        rcrc3          <= crcr3;
        gcrc3          <= crcg3;
        bcrc3          <= crcb3;
      end
   end
end


always @ ( bpc_d or pix0_d or pix1_d or pix2_d or pix3_d )
begin
   case ( bpc_d )
      3'b 000: r0 = { pix0_d[47:42], 10'b 0 };
      3'b 001: r0 = { pix0_d[47:40],  8'b 0 };
      3'b 010: r0 = { pix0_d[47:38],  6'b 0 };
      3'b 011: r0 = { pix0_d[47:36],  4'b 0 };
      default: r0 =   pix0_d[47:32];
   endcase
   case ( bpc_d )
      3'b 000: g0 = { pix0_d[31:26], 10'b 0 };
      3'b 001: g0 = { pix0_d[31:24],  8'b 0 };
      3'b 010: g0 = { pix0_d[31:22],  6'b 0 };
      3'b 011: g0 = { pix0_d[31:20],  4'b 0 };
      default: g0 =   pix0_d[31:16];
   endcase
   case ( bpc_d )
      3'b 000: b0 = { pix0_d[15:10], 10'b 0 };
      3'b 001: b0 = { pix0_d[15:8] ,  8'b 0 };
      3'b 010: b0 = { pix0_d[15:6] ,  6'b 0 };
      3'b 011: b0 = { pix0_d[15:4] ,  4'b 0 };
      default: b0 =   pix0_d[15:0];
   endcase
   case ( bpc_d )
      3'b 000: r1 = { pix1_d[47:42], 10'b 0 };
      3'b 001: r1 = { pix1_d[47:40],  8'b 0 };
      3'b 010: r1 = { pix1_d[47:38],  6'b 0 };
      3'b 011: r1 = { pix1_d[47:36],  4'b 0 };
      default: r1 =   pix1_d[47:32];
   endcase
   case ( bpc_d )
      3'b 000: g1 = { pix1_d[31:26], 10'b 0 };
      3'b 001: g1 = { pix1_d[31:24],  8'b 0 };
      3'b 010: g1 = { pix1_d[31:22],  6'b 0 };
      3'b 011: g1 = { pix1_d[31:20],  4'b 0 };
      default: g1 =   pix1_d[31:16];
   endcase
   case ( bpc_d )
      3'b 000: b1 = { pix1_d[15:10], 10'b 0 };
      3'b 001: b1 = { pix1_d[15:8] ,  8'b 0 };
      3'b 010: b1 = { pix1_d[15:6] ,  6'b 0 };
      3'b 011: b1 = { pix1_d[15:4] ,  4'b 0 };
      default: b1 =   pix1_d[15:0];
   endcase
   case ( bpc_d )
      3'b 000: r2 = { pix2_d[47:42], 10'b 0 };
      3'b 001: r2 = { pix2_d[47:40],  8'b 0 };
      3'b 010: r2 = { pix2_d[47:38],  6'b 0 };
      3'b 011: r2 = { pix2_d[47:36],  4'b 0 };
      default: r2 =   pix2_d[47:32];
   endcase
   case ( bpc_d )
      3'b 000: g2 = { pix2_d[31:26], 10'b 0 };
      3'b 001: g2 = { pix2_d[31:24],  8'b 0 };
      3'b 010: g2 = { pix2_d[31:22],  6'b 0 };
      3'b 011: g2 = { pix2_d[31:20],  4'b 0 };
      default: g2 =   pix2_d[31:16];
   endcase
   case ( bpc_d )
      3'b 000: b2 = { pix2_d[15:10], 10'b 0 };
      3'b 001: b2 = { pix2_d[15:8] ,  8'b 0 };
      3'b 010: b2 = { pix2_d[15:6] ,  6'b 0 };
      3'b 011: b2 = { pix2_d[15:4] ,  4'b 0 };
      default: b2 =   pix2_d[15:0];
   endcase
   case ( bpc_d )
      3'b 000: r3 = { pix3_d[47:42], 10'b 0 };
      3'b 001: r3 = { pix3_d[47:40],  8'b 0 };
      3'b 010: r3 = { pix3_d[47:38],  6'b 0 };
      3'b 011: r3 = { pix3_d[47:36],  4'b 0 };
      default: r3 =   pix3_d[47:32];
   endcase
   case ( bpc_d )
      3'b 000: g3 = { pix3_d[31:26], 10'b 0 };
      3'b 001: g3 = { pix3_d[31:24],  8'b 0 };
      3'b 010: g3 = { pix3_d[31:22],  6'b 0 };
      3'b 011: g3 = { pix3_d[31:20],  4'b 0 };
      default: g3 =   pix3_d[31:16];
   endcase
   case ( bpc_d )
      3'b 000: b3 = { pix3_d[15:10], 10'b 0 };
      3'b 001: b3 = { pix3_d[15:8] ,  8'b 0 };
      3'b 010: b3 = { pix3_d[15:6] ,  6'b 0 };
      3'b 011: b3 = { pix3_d[15:4] ,  4'b 0 };
      default: b3 =   pix3_d[15:0];
   endcase
end

crc_16_comp crc_16_comp_r0 (.data_in (r0),.init_in (rcrc0),.crc_out (crcr0) );
crc_16_comp crc_16_comp_g0 (.data_in (g0),.init_in (gcrc0),.crc_out (crcg0) );
crc_16_comp crc_16_comp_b0 (.data_in (b0),.init_in (bcrc0),.crc_out (crcb0) );

crc_16_comp crc_16_comp_r1 (.data_in (r1),.init_in (rcrc1),.crc_out (crcr1) );
crc_16_comp crc_16_comp_g1 (.data_in (g1),.init_in (gcrc1),.crc_out (crcg1) );
crc_16_comp crc_16_comp_b1 (.data_in (b1),.init_in (bcrc1),.crc_out (crcb1) );

crc_16_comp crc_16_comp_r2 (.data_in (r2),.init_in (rcrc2),.crc_out (crcr2) );
crc_16_comp crc_16_comp_g2 (.data_in (g2),.init_in (gcrc2),.crc_out (crcg2) );
crc_16_comp crc_16_comp_b2 (.data_in (b2),.init_in (bcrc2),.crc_out (crcb2) );

crc_16_comp crc_16_comp_r3 (.data_in (r3),.init_in (rcrc3),.crc_out (crcr3) );
crc_16_comp crc_16_comp_g3 (.data_in (g3),.init_in (gcrc3),.crc_out (crcg3) );
crc_16_comp crc_16_comp_b3 (.data_in (b3),.init_in (bcrc3),.crc_out (crcb3) );

endmodule
