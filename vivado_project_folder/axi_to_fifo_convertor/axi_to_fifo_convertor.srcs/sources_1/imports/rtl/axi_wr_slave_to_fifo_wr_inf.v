`timescale 1ns / 1ps
module axi_wr_slave_to_fifo_wr_inf(
	reset,
	axi_write_clk,
	axi_fifo_state_out,
	config_data_out,
	y_residual_data_out,
	cb_residual_data_out,
	cr_residual_data_out,
  
	config_wr_en,
	y_residual_wr_en,
	cb_residual_wr_en,
	cr_residual_wr_en,
  
	config_full,
	y_residual_full,
	cr_residual_full, 
	cb_residual_full,
	
	S00_AXI_ACLK,
	S00_AXI_AWID,
	S00_AXI_AWADDR,
	S00_AXI_AWLEN,
	S00_AXI_AWSIZE,
	S00_AXI_AWBURST,
	S00_AXI_AWLOCK,
	S00_AXI_AWCACHE,
	S00_AXI_AWPROT,
	S00_AXI_AWQOS,
	S00_AXI_AWVALID,
	S00_AXI_AWREADY,
	S00_AXI_WDATA,
	S00_AXI_WSTRB,
	S00_AXI_WLAST,
	S00_AXI_WVALID,
	S00_AXI_WREADY,
	S00_AXI_BID,
	S00_AXI_BRESP,
	S00_AXI_BVALID,
	S00_AXI_BREADY,
	S00_AXI_ARID,
	S00_AXI_ARADDR,
	S00_AXI_ARLEN,
	S00_AXI_ARSIZE,
	S00_AXI_ARBURST,
	S00_AXI_ARLOCK,
	S00_AXI_ARCACHE,
	S00_AXI_ARPROT,
	S00_AXI_ARQOS,
	S00_AXI_ARVALID,
	S00_AXI_ARREADY,
	S00_AXI_RID,
	S00_AXI_RDATA,
	S00_AXI_RRESP,
	S00_AXI_RLAST,
	S00_AXI_RVALID,
	S00_AXI_RREADY
);

	input reset;
  input 			S00_AXI_ACLK;
  input [12-1 : 0] 	S00_AXI_AWID;
  input [16-1 : 0] 	S00_AXI_AWADDR;
  input [7 : 0] 	S00_AXI_AWLEN;
  input [2 : 0] 	S00_AXI_AWSIZE;
  input [1 : 0] 	S00_AXI_AWBURST;
  input 			S00_AXI_AWLOCK;
  input [3 : 0] 	S00_AXI_AWCACHE;
  input [2 : 0] 	S00_AXI_AWPROT;
  input [3 : 0] 	S00_AXI_AWQOS;
  input 			S00_AXI_AWVALID;
  output reg			S00_AXI_AWREADY;
  input [32-1 : 0] 	S00_AXI_WDATA;
  input [32/8 : 0] 	S00_AXI_WSTRB;
  input 			S00_AXI_WLAST;
  input 			S00_AXI_WVALID;
  output reg		 	S00_AXI_WREADY;
  output [12-1 : 0] 	S00_AXI_BID;
  output [1 : 0] 	S00_AXI_BRESP;
  output reg			S00_AXI_BVALID;
  input 			S00_AXI_BREADY;
  input [12-1 : 0] 	S00_AXI_ARID;
  input [16-1 : 0] 	S00_AXI_ARADDR;
  input [7 : 0] 	S00_AXI_ARLEN;
  input [2 : 0] 	S00_AXI_ARSIZE;
  input [1 : 0] 	S00_AXI_ARBURST;
  input 			S00_AXI_ARLOCK;
  input [3 : 0] 	S00_AXI_ARCACHE;
  input [2 : 0] 	S00_AXI_ARPROT;
  input [3 : 0] 	S00_AXI_ARQOS;
  input 			S00_AXI_ARVALID;
  output 			S00_AXI_ARREADY;
  output [12-1 : 0] 	S00_AXI_RID;
  output [32-1 : 0] S00_AXI_RDATA;
  output [1 : 0] 	S00_AXI_RRESP;
  output 			S00_AXI_RLAST;
  output 			S00_AXI_RVALID;
  input 			S00_AXI_RREADY;
  
  output reg [32-1:0]   config_data_out;
  output  [144-1:0]  y_residual_data_out;
  output  [144-1:0]  cb_residual_data_out;
  output  [144-1:0]  cr_residual_data_out;
  
  output reg config_wr_en;
  output reg y_residual_wr_en;
  output reg cb_residual_wr_en;
  output reg cr_residual_wr_en;
  
  output [4:0] axi_fifo_state_out;
  
  input config_full;
  input y_residual_full;
  input cr_residual_full;  
  input cb_residual_full;
 
  output axi_write_clk;

            localparam TYPE_INITAL= 0;
			localparam TYPE_ADDRESS_READY = 7;
			localparam CONFIG_DATA_READY = 1;
			localparam TYPE_FULL_WAIT = 2;
			localparam Y_DATA_READY = 3;
			localparam CB_DATA_READY = 4;
			localparam CR_DATA_READY = 5;
			localparam JUST_ADDRESS_READY = 6;
	
	
  integer byte_counter;
  integer state;
  assign S00_AXI_ARREADY = 0;
  assign S00_AXI_RVALID = 0;
  assign S00_AXI_RLAST = 0;
  assign S00_AXI_RID = 0;
  assign S00_AXI_BID = 0;
  assign S00_AXI_BRESP = 0;
  assign axi_write_clk = S00_AXI_ACLK;
  reg [16-1:0] S00_AXI_AWADDR_reg;
  
	reg [7:0] yy_residual_fifo_read[23:0];
	reg [7:0] cb_residual_fifo_read[23:0];
	reg [7:0] cr_residual_fifo_read[23:0];
	
	  	assign cb_residual_data_out = {
				cb_residual_fifo_read[21][0],
				cb_residual_fifo_read[20],
				cb_residual_fifo_read[19][2:0],
				cb_residual_fifo_read[18],
				cb_residual_fifo_read[17],
				cb_residual_fifo_read[16],
				cb_residual_fifo_read[15][2:0],
				cb_residual_fifo_read[14],
				cb_residual_fifo_read[13],
				cb_residual_fifo_read[12],
				cb_residual_fifo_read[11][2:0],
				cb_residual_fifo_read[10],
				cb_residual_fifo_read[ 9],
				cb_residual_fifo_read[ 8],
				cb_residual_fifo_read[ 7][2:0],
				cb_residual_fifo_read[ 6],
				cb_residual_fifo_read[ 5],
				cb_residual_fifo_read[ 4],
				cb_residual_fifo_read[ 3][2:0],
				cb_residual_fifo_read[ 2],
				cb_residual_fifo_read[ 1],
				cb_residual_fifo_read[ 0]
	};
     	assign cr_residual_data_out = {
				cr_residual_fifo_read[21][0],
				cr_residual_fifo_read[20],
				cr_residual_fifo_read[19][2:0],
				cr_residual_fifo_read[18],
				cr_residual_fifo_read[17],
				cr_residual_fifo_read[16],
				cr_residual_fifo_read[15][2:0],
				cr_residual_fifo_read[14],
				cr_residual_fifo_read[13],
				cr_residual_fifo_read[12],
				cr_residual_fifo_read[11][2:0],
				cr_residual_fifo_read[10],
				cr_residual_fifo_read[ 9],
				cr_residual_fifo_read[ 8],
				cr_residual_fifo_read[ 7][2:0],
				cr_residual_fifo_read[ 6],
				cr_residual_fifo_read[ 5],
				cr_residual_fifo_read[ 4],
				cr_residual_fifo_read[ 3][2:0],
				cr_residual_fifo_read[ 2],
				cr_residual_fifo_read[ 1],
				cr_residual_fifo_read[ 0]
	};
     	assign y_residual_data_out = {
				yy_residual_fifo_read[21][0],
				yy_residual_fifo_read[20],
				yy_residual_fifo_read[19][2:0],
				yy_residual_fifo_read[18],
				yy_residual_fifo_read[17],
				yy_residual_fifo_read[16],
				yy_residual_fifo_read[15][2:0],
				yy_residual_fifo_read[14],
				yy_residual_fifo_read[13],
				yy_residual_fifo_read[12],
				yy_residual_fifo_read[11][2:0],
				yy_residual_fifo_read[10],
				yy_residual_fifo_read[ 9],
				yy_residual_fifo_read[ 8],
				yy_residual_fifo_read[ 7][2:0],
				yy_residual_fifo_read[ 6],
				yy_residual_fifo_read[ 5],
				yy_residual_fifo_read[ 4],
				yy_residual_fifo_read[ 3][2:0],
				yy_residual_fifo_read[ 2],
				yy_residual_fifo_read[ 1],
				yy_residual_fifo_read[ 0]
	};
	
  assign axi_fifo_state_out = state;
  always@(posedge S00_AXI_ACLK) begin
    S00_AXI_BVALID <= S00_AXI_WVALID;
  end
  always@(posedge S00_AXI_ACLK or posedge reset) begin
	if(reset) begin
		state <= TYPE_INITAL;
		byte_counter <= 0;
		config_wr_en <= 0;
		y_residual_wr_en <= 0;
		cb_residual_wr_en <= 0;
		cr_residual_wr_en <= 0;
		
	end
	else begin
			config_wr_en <= 0;
			y_residual_wr_en <= 0;
			cb_residual_wr_en <= 0;
			cr_residual_wr_en <= 0;
		case(state)
		    TYPE_INITAL: begin
		      state <= TYPE_ADDRESS_READY;
		    end
			TYPE_ADDRESS_READY: begin
				byte_counter <= 0;
				if(S00_AXI_AWVALID) begin
					S00_AXI_AWADDR_reg <= S00_AXI_AWADDR;
					if(S00_AXI_AWADDR == 16) begin
						state <= CONFIG_DATA_READY;
					end
					else if(S00_AXI_AWADDR == 32)begin
						state <= Y_DATA_READY;
					end
					else if(S00_AXI_AWADDR == 48)begin
						state <= CB_DATA_READY;
					end
					else if(S00_AXI_AWADDR == 64)begin
						state <= CR_DATA_READY;
					end
				end			
			
			end
			CONFIG_DATA_READY: begin
				if(S00_AXI_WVALID) begin
					config_wr_en <= 1;
					config_data_out <= S00_AXI_WDATA;
					if(!config_full) begin
						state <= TYPE_ADDRESS_READY;
					end
					else begin
						state <= TYPE_FULL_WAIT;
					end
				end
			end
			TYPE_FULL_WAIT: begin
				if((!config_full) & (!y_residual_full) & (!cr_residual_full) & (!cb_residual_full)) begin
					state <= TYPE_ADDRESS_READY;
				end
			end
			Y_DATA_READY: begin
				if(S00_AXI_WVALID) begin
					byte_counter <= byte_counter + 4'd4;
					{yy_residual_fifo_read[byte_counter+3],yy_residual_fifo_read[byte_counter+2],yy_residual_fifo_read[byte_counter+1],yy_residual_fifo_read[byte_counter] }<= S00_AXI_WDATA;
					if(byte_counter == 20) begin
						y_residual_wr_en <= 1;
						if(!y_residual_full) begin
							state <= TYPE_ADDRESS_READY;
						end
						else begin
							state <= TYPE_FULL_WAIT;
						end
					end
					else begin
						state <= JUST_ADDRESS_READY;
					end
				end	
			end
			CB_DATA_READY: begin
				if(S00_AXI_WVALID) begin
					byte_counter <= byte_counter + 4'd4;
					{cb_residual_fifo_read[byte_counter+3],cb_residual_fifo_read[byte_counter+2],cb_residual_fifo_read[byte_counter+1],cb_residual_fifo_read[byte_counter] }<= S00_AXI_WDATA;
					if(byte_counter == 20) begin
						cb_residual_wr_en <= 1;
						if(!cb_residual_full) begin
							state <= TYPE_ADDRESS_READY;
						end
						else begin
							state <= TYPE_FULL_WAIT;
						end
					end
					else begin
						state <= JUST_ADDRESS_READY;
					end
				end	
			end
			CR_DATA_READY: begin
				if(S00_AXI_WVALID) begin
					byte_counter <= byte_counter + 4'd4;
					{cr_residual_fifo_read[byte_counter+3],cr_residual_fifo_read[byte_counter+2],cr_residual_fifo_read[byte_counter+1],cr_residual_fifo_read[byte_counter] }<= S00_AXI_WDATA;
					if(byte_counter == 20) begin
						cr_residual_wr_en <= 1;
						if(!cr_residual_full) begin
							state <= TYPE_ADDRESS_READY;
						end
						else begin
							state <= TYPE_FULL_WAIT;
						end
					end
					else begin
						state <= JUST_ADDRESS_READY;
					end
				end	
			end
			JUST_ADDRESS_READY: begin
				if(S00_AXI_AWVALID) begin
					if(S00_AXI_AWADDR_reg == 32)begin
						state <= Y_DATA_READY;
					end
					else if(S00_AXI_AWADDR_reg == 48)begin
						state <= CB_DATA_READY;
					end
					else if(S00_AXI_AWADDR_reg == 64)begin
						state <= CR_DATA_READY;
					end
					else begin
					   state <= TYPE_ADDRESS_READY;
					end
				end
			end
		endcase

	end
  end
  
  always@(*) begin
  
        S00_AXI_AWREADY = 0;
        S00_AXI_WREADY = 0;
		case(state)

			TYPE_ADDRESS_READY: begin
				S00_AXI_AWREADY = 1;		
			end
			CONFIG_DATA_READY: begin
				S00_AXI_WREADY = 1;
			end
			TYPE_FULL_WAIT: begin
				S00_AXI_AWREADY = 0;
				S00_AXI_WREADY = 0;
			end
			Y_DATA_READY: begin
				S00_AXI_WREADY = 1;
			end
			CB_DATA_READY: begin
				S00_AXI_WREADY = 1;	
			end
			CR_DATA_READY: begin
				S00_AXI_WREADY = 1;
			end
			JUST_ADDRESS_READY: begin
				S00_AXI_AWREADY = 1;
			end
		endcase
  end
  
endmodule