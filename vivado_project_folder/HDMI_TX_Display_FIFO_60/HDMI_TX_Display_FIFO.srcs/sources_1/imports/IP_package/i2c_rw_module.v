`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:11:47 12/17/2013 
// Design Name: 
// Module Name:    i2c_rw_module 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module i2c_rw_module(
		clk,
		reset,
		slave_addr_in,
		base_addr_in,
		wr_data_in,
		wr_en,
		valid_in,
		ready_out,
		rd_data_out,
		done_out,
		scl_pad_i, 
		scl_pad_o,	 
		scl_padoen_o,
		sda_pad_i,	 
		sda_pad_o,	 
		sda_padoen_o,
		state
    );
	
	
//-------------------------------------------------------------------------------
// I/O signals
//-------------------------------------------------------------------------------
    
	input						clk;
	input						reset;
	
	input	[6:0]				slave_addr_in;
	input	[7:0]				base_addr_in;
	input	[7:0]				wr_data_in;
	input						wr_en;
	input						valid_in;
	output						ready_out;
	output	[7:0]				rd_data_out;
	output						done_out;
	
	input                       scl_pad_i;	 
	output                      scl_pad_o;	 
	output                      scl_padoen_o;
	input                       sda_pad_i;	 
	output                      sda_pad_o;	 
	output                      sda_padoen_o;
	
	output	reg	[7:0]			state;
	
//-------------------------------------------------------------------------------
// parameter definitions
//-------------------------------------------------------------------------------
    
	parameter					FREQ_IN		=	298;

//-------------------------------------------------------------------------------
// localparam definitions
//-------------------------------------------------------------------------------
	localparam 				STATE_INIT				  =     0  ;
	localparam 				STATE_INIT_1              =     1  ;
	localparam 				STATE_INIT_2              =     2  ;
	localparam 				STATE_INIT_3              =     3  ;
	localparam 				STATE_INIT_4              =     4  ;
	localparam 				STATE_INIT_5              =     5  ;
	localparam 				STATE_IDLE                =     6  ;
	
	localparam 				STATE_READ                =     10  ;
	localparam 				STATE_HPD_SENSE_1_1       =     11  ;
	localparam 				STATE_HPD_SENSE_1_2       =     12  ;
	localparam 				STATE_HPD_SENSE_1_3       =     13  ;
	localparam 				STATE_HPD_SENSE_1_WAIT    =     14  ;
	localparam 				STATE_HPD_SENSE_1_WAIT_1  =     15  ;
	localparam 				STATE_HPD_SENSE_1_WAIT_2  =     16  ;
	localparam 				STATE_HPD_SENSE_2         =     17  ;
	localparam 				STATE_HPD_SENSE_2_1       =     18  ;
	localparam 				STATE_HPD_SENSE_2_2       =     19  ;
	localparam 				STATE_HPD_SENSE_2_3       =     20  ;
	localparam 				STATE_HPD_SENSE_2_WAIT    =     21  ;
	localparam 				STATE_HPD_SENSE_2_WAIT_1  =     22  ;
	localparam 				STATE_HPD_SENSE_2_WAIT_2  =     23  ;
	localparam 				STATE_HPD_SENSE_3         =     24  ;
	localparam 				STATE_HPD_SENSE_3_1       =     25 ;
	localparam 				STATE_HPD_SENSE_3_2       =     26  ;
	localparam 				STATE_HPD_SENSE_3_3       =     27  ;
	localparam 				STATE_HPD_SENSE_3_WAIT    =     28  ;
	localparam 				STATE_HPD_SENSE_3_WAIT_1  =     29  ;
	localparam 				STATE_HPD_SENSE_3_WAIT_2  =     30  ;
	localparam 				STATE_HPD_SENSE_4         =     31  ;
	localparam 				STATE_HPD_SENSE_4_1       =     32  ;
	localparam 				STATE_HPD_SENSE_4_WAIT    =     33  ;
	localparam 				STATE_HPD_SENSE_4_WAIT_1  =     34  ;
	localparam 				STATE_HPD_SENSE_4_WAIT_2  =     35  ;
	localparam 				STATE_HPD_SENSE_5         =     36  ;
	localparam 				STATE_HPD_SENSE_5_1       =     37  ;
	localparam 				STATE_HPD_SENSE_5_2       =     38  ;
	
	localparam 				STATE_WRITE               =     40  ;
	localparam 				STATE_POWER_UP_1_1        =     41  ;
	localparam 				STATE_POWER_UP_1_2        =     42  ;
	localparam 				STATE_POWER_UP_1_3        =     43  ;
	localparam 				STATE_POWER_UP_1_WAIT     =     44  ;
	localparam 				STATE_POWER_UP_1_WAIT_1   =     45  ;
	localparam 				STATE_POWER_UP_1_WAIT_2   =     46  ;
	localparam 				STATE_POWER_UP_2          =     47  ;
	localparam 				STATE_POWER_UP_2_1        =     48  ;
	localparam 				STATE_POWER_UP_2_2        =     49  ;
	localparam 				STATE_POWER_UP_2_3        =     50  ;
	localparam 				STATE_POWER_UP_2_WAIT     =     51  ;
	localparam 				STATE_POWER_UP_2_WAIT_1   =     52  ;
	localparam 				STATE_POWER_UP_2_WAIT_2   =     53  ;
	localparam 				STATE_POWER_UP_3          =     54  ;
	localparam 				STATE_POWER_UP_3_1        =     55  ;
	localparam 				STATE_POWER_UP_3_2        =     56  ;
	localparam 				STATE_POWER_UP_3_3        =     57  ;
	localparam 				STATE_POWER_UP_3_WAIT     =     58  ;
	localparam 				STATE_POWER_UP_3_WAIT_1   =     59  ;
	localparam 				STATE_POWER_UP_3_WAIT_2   =     60  ;
	localparam 				STATE_POWER_UP_4          =     61  ;
	localparam 				STATE_POWER_UP_4_1        =     62  ;
	
	// I2C clock pre scaler
	localparam 	[15:0]		IIC_PS     				=     FREQ_IN - 1'b1 ;
	localparam 	[7:0]		IIC_PS_LOW     			=     IIC_PS[7:0]    ;
	localparam 	[7:0]		IIC_PS_HIGH     		=     IIC_PS[15:8]   ;
	
//-------------------------------------------------------------------------------
// Internal wires and registers
//-------------------------------------------------------------------------------
    
	//-----I2C module signals---------------
	reg		[2:0]				wb_adr_i;	 
	reg     [7:0]               wb_dat_i;	 
	wire    [7:0]               wb_dat_o;	 
	reg                         wb_we_i ;	 
	reg                         wb_stb_i;	 
	reg                         wb_cyc_i;	 
	wire                        wb_ack_o;	

	//-------Interface signals---------------
	reg     [6:0]               slave_addr;
	reg     [7:0]               base_addr;
	reg     [7:0]               wr_data;
	reg		[7:0]				rd_data_out;
	reg							ready_out;
	reg							done_out;
	reg							rd_wrn;
	
	reg		[7:0]				recd_wb_data;
	
	//integer						state;

//-------------------------------------------------------------------------------
// Implementation
//-------------------------------------------------------------------------------
	
	//-----Generate I2C sub module-----------------
	i2c_master_top #(1'b1) i2c_master(
		.wb_clk_i	  (clk),	
		.wb_rst_i	  (1'b0),
		.arst_i	 	  (reset),
		.wb_adr_i	  (wb_adr_i),
		.wb_dat_i	  (wb_dat_i),
		.wb_dat_o	  (wb_dat_o),
		.wb_we_i 	  (wb_we_i),
		.wb_stb_i	  (wb_stb_i),
		.wb_cyc_i	  (wb_cyc_i),
		.wb_ack_o	  (wb_ack_o),
		.wb_inta_o	  (),
		.scl_pad_i	  (scl_pad_i),
		.scl_pad_o	  (scl_pad_o),
		.scl_padoen_o (scl_padoen_o),
		.sda_pad_i	  (sda_pad_i),
		.sda_pad_o	  (sda_pad_o),	
		.sda_padoen_o (sda_padoen_o)
	);
	
	always@(posedge clk or posedge reset) begin
		if(reset) begin
			done_out <= 1'b0;
			ready_out <= 1'b0;
			wb_adr_i <= 3'b000;  
			wb_dat_i <= 8'd0;
			wb_we_i  <= 1'b0;  
			wb_stb_i <= 1'b0;  
			wb_cyc_i <= 1'b0;  
			
			
			
			
			state	<= STATE_INIT;
		end
		else begin
			case(state)
				//--------Initialize I2C core---------------------------------------------//
				STATE_INIT : begin		// set prescaler                                  //
					wb_adr_i <= 3'b0;                                                     //
					wb_dat_i <= IIC_PS_LOW;		// 200kHz                                 //
					wb_we_i  <= 1'b1;                                                     //
					wb_stb_i <= 1'b1;                                                     //
					wb_cyc_i <= 1'b1;                                                     //
					state	<= STATE_INIT_1;                                              //
				end                                                                       //
				STATE_INIT_1 : begin	// wait for wb ack                                //
					if(wb_ack_o) begin                                                    //
						wb_stb_i <= 1'b0;                                                 //
						wb_cyc_i <= 1'b0;                                                 //
						state	<= STATE_INIT_2;                                          //
					end                                                                   //
				end                                                                       //
				STATE_INIT_2 : begin                                                      //
					wb_adr_i <= 3'b1;                                                     //
					wb_dat_i <= IIC_PS_HIGH;		// 200kHz                             //
					wb_we_i  <= 1'b1;                                                     //
					wb_stb_i <= 1'b1;                                                     //
					wb_cyc_i <= 1'b1;                                                     //
					state	<= STATE_INIT_3;                                              //
				end                                                                       //
				STATE_INIT_3 : begin                                                      //
					if(wb_ack_o) begin                                                    //
						wb_stb_i <= 1'b0;                                                 //
						wb_cyc_i <= 1'b0;                                                 //
						state	<= STATE_INIT_4;                                          //
					end                                                                   //
				end                                                                       //
				STATE_INIT_4 : begin	// enable core by control register                //
					wb_adr_i <= 3'b10;                                                    //
					wb_dat_i <= 8'b10000000;                                              //
					wb_we_i  <= 1'b1;                                                     //
					wb_stb_i <= 1'b1;                                                     //
					wb_cyc_i <= 1'b1;                                                     //
					state	<= STATE_INIT_5;                                              //
				end                                                                       //
				STATE_INIT_5 : begin                                                      //
					if(wb_ack_o) begin                                                    //
						wb_stb_i <= 1'b0;                                                 //
						wb_cyc_i <= 1'b0;                                                 //
						ready_out <= 1'b1;												  //
						state	<= STATE_IDLE;                                       	  //
					end                                                                   //
				end                                                                       //
				//------------------------------------------------------------------------//
				
				
				//----------Wait state - wait for valid data-----------------------
				STATE_IDLE : begin
					if(valid_in) begin
						slave_addr <= slave_addr_in;
						base_addr  <= base_addr_in;
						ready_out <= 1'b0;
						if(wr_en) begin
							wr_data <= wr_data_in;
							rd_wrn <= 1'b0;
							state <= STATE_WRITE;
						end
						else begin
							rd_wrn <= 1'b1;
							state <= STATE_READ;
						end
					end
				end
				//-----------------------------------------------------------------
				
				
				//---------------------------Data read operation-----------------------------------//
				STATE_READ : begin		                                                   		   //
					wb_adr_i <= 3'b11;			// tx reg                                          //
					wb_dat_i <= {slave_addr,1'b0};		// I2C slave address, write                //
					wb_we_i  <= 1'b1;                                                              //
					wb_stb_i <= 1'b1;                                                              //
					wb_cyc_i <= 1'b1;                                                              //
					state	<= STATE_HPD_SENSE_1_1;                                                //
				end                                                                                //
				STATE_HPD_SENSE_1_1 : begin	// wishbone ack                                        //
					if(wb_ack_o) begin                                                             //
						wb_stb_i <= 1'b0;                                                          //
						wb_cyc_i <= 1'b0;                                                          //
						state	<= STATE_HPD_SENSE_1_2;                                            //
					end                                                                            //
				end                                                                                //
				STATE_HPD_SENSE_1_2 : begin	                                                       //
					wb_adr_i <= 3'b100;			// command reg                                     //
					wb_dat_i <= 8'b10010000;	// start bit and write bit                         //
					wb_we_i  <= 1'b1;                                                              //
					wb_stb_i <= 1'b1;                                                              //
					wb_cyc_i <= 1'b1;                                                              //
					state	<= STATE_HPD_SENSE_1_3;                                                //
				end                                                                                //
				STATE_HPD_SENSE_1_3 : begin	// wishbone ack                                        //
					if(wb_ack_o) begin                                                             //
						wb_stb_i <= 1'b0;                                                          //
						wb_cyc_i <= 1'b0;                                                          //
						state	<= STATE_HPD_SENSE_1_WAIT;                                         //
					end                                                                            //
				end                                                                                //
				                                                                                   //
				//--------Wait for I2C transfer to complete------------------//                    //
				STATE_HPD_SENSE_1_WAIT : begin	// read TIP flag             //                    //
					wb_adr_i <= 3'b100;			// status reg                //                    //
					wb_we_i  <= 1'b0;			// read                      //                    //
					wb_stb_i <= 1'b1;                                        //                    //
					wb_cyc_i <= 1'b1;                                        //                    //
					state	<= STATE_HPD_SENSE_1_WAIT_1;                     //                    //
				end                                                          //                    //
				STATE_HPD_SENSE_1_WAIT_1 : begin	// wishbone ack          //                    //
					if(wb_ack_o) begin                                       //                    //
						wb_stb_i <= 1'b0;                                    //                    //
						wb_cyc_i <= 1'b0;                                    //                    //
						recd_wb_data <= wb_dat_o;                            //                    //
						state	<= STATE_HPD_SENSE_1_WAIT_2;                 //                    //
					end                                                      //                    //
				end                                                          //                    //
				STATE_HPD_SENSE_1_WAIT_2 : begin                             //                    //
					if(recd_wb_data[1]==1'b0) begin		// transfer done     //                    //
						state <= STATE_HPD_SENSE_2;                          //                    //
					end                                                      //                    //
					else begin                                               //                    //
						state <= STATE_HPD_SENSE_1_WAIT;                     //                    //
					end                                                      //                    //
				end                                                          //                    //
				//-----------------------------------------------------------//                    //
				                                                                                   //
				STATE_HPD_SENSE_2 : begin	                                                       //
					wb_adr_i <= 3'b11;			// tx reg                                          //
					wb_dat_i <= base_addr;		// base register address                           //
					wb_we_i  <= 1'b1;                                                              //
					wb_stb_i <= 1'b1;                                                              //
					wb_cyc_i <= 1'b1;                                                              //
					state	<= STATE_HPD_SENSE_2_1;                                                //
				end                                                                                //
				STATE_HPD_SENSE_2_1 : begin                                                        //
					if(wb_ack_o) begin                                                             //
						wb_stb_i <= 1'b0;                                                          //
						wb_cyc_i <= 1'b0;                                                          //
						state	<= STATE_HPD_SENSE_2_2;                                            //
					end                                                                            //
				end                                                                                //
				STATE_HPD_SENSE_2_2 : begin	                                                       //
					wb_adr_i <= 3'b100;			// command reg                                     //
					wb_dat_i <= 8'b00010000;	// write bit                                       //
					wb_we_i  <= 1'b1;                                                              //
					wb_stb_i <= 1'b1;                                                              //
					wb_cyc_i <= 1'b1;                                                              //
					state	<= STATE_HPD_SENSE_2_3;                                                //
				end                                                                                //
				STATE_HPD_SENSE_2_3 : begin                                                        //
					if(wb_ack_o) begin                                                             //
						wb_stb_i <= 1'b0;                                                          //
						wb_cyc_i <= 1'b0;                                                          //
						state	<= STATE_HPD_SENSE_2_WAIT;                                         //
					end                                                                            //
				end                                                                                //
				//--------Wait for I2C transfer to complete------------------//                    //
				STATE_HPD_SENSE_2_WAIT : begin	// read TIP flag             //                    //
					wb_adr_i <= 3'b100;			// status reg                //                    //
					wb_we_i  <= 1'b0;			// read                      //                    //
					wb_stb_i <= 1'b1;                                        //                    //
					wb_cyc_i <= 1'b1;                                        //                    //
					state	<= STATE_HPD_SENSE_2_WAIT_1;                     //                    //
				end                                                          //                    //
				STATE_HPD_SENSE_2_WAIT_1 : begin	// wishbone ack          //                    //
					if(wb_ack_o) begin                                       //                    //
						wb_stb_i <= 1'b0;                                    //                    //
						wb_cyc_i <= 1'b0;                                    //                    //
						recd_wb_data <= wb_dat_o;                            //                    //
						state	<= STATE_HPD_SENSE_2_WAIT_2;                 //                    //
					end                                                      //                    //
				end                                                          //                    //
				STATE_HPD_SENSE_2_WAIT_2 : begin                             //                    //
					if(recd_wb_data[1]==1'b0) begin		// transfer done     //                    //
						state <= STATE_HPD_SENSE_3;                          //                    //
					end                                                      //                    //
					else begin                                               //                    //
						state <= STATE_HPD_SENSE_2_WAIT;                     //                    //
					end                                                      //                    //
				end                                                          //                    //
				//-----------------------------------------------------------//                    //
				                                                                                   //
				STATE_HPD_SENSE_3 : begin		                                                   //
					wb_adr_i <= 3'b11;			// tx reg                                          //
					wb_dat_i <= {slave_addr,1'b1};		// I2C slave address, read                 //
					wb_we_i  <= 1'b1;                                                              //
					wb_stb_i <= 1'b1;                                                              //
					wb_cyc_i <= 1'b1;                                                              //
					state	<= STATE_HPD_SENSE_3_1;                                                //
				end                                                                                //
				STATE_HPD_SENSE_3_1 : begin	// wishbone ack                                        //
					if(wb_ack_o) begin                                                             //
						wb_stb_i <= 1'b0;                                                          //
						wb_cyc_i <= 1'b0;                                                          //
						state	<= STATE_HPD_SENSE_3_2;                                            //
					end                                                                            //
				end                                                                                //
				STATE_HPD_SENSE_3_2 : begin	                                                       //
					wb_adr_i <= 3'b100;			// command reg                                     //
					wb_dat_i <= 8'b10010000;	// start bit and write bit                         //
					wb_we_i  <= 1'b1;                                                              //
					wb_stb_i <= 1'b1;                                                              //
					wb_cyc_i <= 1'b1;                                                              //
					state	<= STATE_HPD_SENSE_3_3;                                                //
				end                                                                                //
				STATE_HPD_SENSE_3_3 : begin	// wishbone ack                                        //
					if(wb_ack_o) begin                                                             //
						wb_stb_i <= 1'b0;                                                          //
						wb_cyc_i <= 1'b0;                                                          //
						state	<= STATE_HPD_SENSE_3_WAIT;                                         //
					end                                                                            //
				end                                                                                //
				//--------Wait for I2C transfer to complete------------------//                    //
				STATE_HPD_SENSE_3_WAIT : begin	// read TIP flag             //                    //
					wb_adr_i <= 3'b100;			// status reg                //                    //
					wb_we_i  <= 1'b0;			// read                      //                    //
					wb_stb_i <= 1'b1;                                        //                    //
					wb_cyc_i <= 1'b1;                                        //                    //
					state	<= STATE_HPD_SENSE_3_WAIT_1;                     //                    //
				end                                                          //                    //
				STATE_HPD_SENSE_3_WAIT_1 : begin	// wishbone ack          //                    //
					if(wb_ack_o) begin                                       //                    //
						wb_stb_i <= 1'b0;                                    //                    //
						wb_cyc_i <= 1'b0;                                    //                    //
						recd_wb_data <= wb_dat_o;                            //                    //
						state	<= STATE_HPD_SENSE_3_WAIT_2;                 //                    //
					end                                                      //                    //
				end                                                          //                    //
				STATE_HPD_SENSE_3_WAIT_2 : begin                             //                    //
					if(recd_wb_data[1]==1'b0) begin		// transfer done     //                    //
						state <= STATE_HPD_SENSE_4;                          //                    //
					end                                                      //                    //
					else begin                                               //                    //
						state <= STATE_HPD_SENSE_3_WAIT;                     //                    //
					end                                                      //                    //
				end                                                          //                    //
				//-----------------------------------------------------------//                    //
				                                                                                   //
				STATE_HPD_SENSE_4 : begin                                                          //
					wb_adr_i <= 3'b100;			// command reg                                     //
					wb_dat_i <= 8'b01101000;	// read bit,stop bit,nack bit           		   //
					wb_we_i  <= 1'b1;                                                              //
					wb_stb_i <= 1'b1;                                                              //
					wb_cyc_i <= 1'b1;                                                              //
					state	<= STATE_HPD_SENSE_4_1;                                                //
				end                                                                                //
				STATE_HPD_SENSE_4_1 : begin	// wishbone ack                                        //
					if(wb_ack_o) begin                                                             //
						wb_stb_i <= 1'b0;                                                          //
						wb_cyc_i <= 1'b0;                                                          //
						state	<= STATE_HPD_SENSE_4_WAIT;                                         //
					end                                                                            //
				end                                                                                //
				//--------Wait for I2C transfer to complete------------------//                    //
				STATE_HPD_SENSE_4_WAIT : begin	// read TIP flag             //                    //
					wb_adr_i <= 3'b100;			// status reg                //                    //
					wb_we_i  <= 1'b0;			// read                      //                    //
					wb_stb_i <= 1'b1;                                        //                    //
					wb_cyc_i <= 1'b1;                                        //                    //
					state	<= STATE_HPD_SENSE_4_WAIT_1;                     //                    //
				end                                                          //                    //
				STATE_HPD_SENSE_4_WAIT_1 : begin	// wishbone ack          //                    //
					if(wb_ack_o) begin                                       //                    //
						wb_stb_i <= 1'b0;                                    //                    //
						wb_cyc_i <= 1'b0;                                    //                    //
						recd_wb_data <= wb_dat_o;                            //                    //
						state	<= STATE_HPD_SENSE_4_WAIT_2;                 //                    //
					end                                                      //                    //
				end                                                          //                    //
				STATE_HPD_SENSE_4_WAIT_2 : begin                             //                    //
					if(recd_wb_data[1]==1'b0) begin		// transfer done     //                    //
						state <= STATE_HPD_SENSE_5;                          //                    //
					end                                                      //                    //
					else begin                                               //                    //
						state <= STATE_HPD_SENSE_4_WAIT;                     //                    //
					end                                                      //                    //
				end                                                          //                    //
				//-----------------------------------------------------------//                    //
				                                                                                   //
				STATE_HPD_SENSE_5 : begin		                                                   //
					wb_adr_i <= 3'b11;			// rx reg                                          //
					wb_we_i  <= 1'b0;			// read                                            //
					wb_stb_i <= 1'b1;                                                              //
					wb_cyc_i <= 1'b1;                                                              //
					state	<= STATE_HPD_SENSE_5_1;                                                //
				end                                                                                //
				STATE_HPD_SENSE_5_1 : begin	// wishbone ack                                        //
					if(wb_ack_o) begin                                                             //
						wb_stb_i <= 1'b0;                                                          //
						wb_cyc_i <= 1'b0;                                                          //
						rd_data_out <= wb_dat_o;                                                   //
						done_out <= 1'b1;														   //
						state <= STATE_HPD_SENSE_5_2;                                              //
					end                                                                            //
				end                                                                                //
				STATE_HPD_SENSE_5_2 : begin                                                        //
					done_out <= 1'b0;															   //
					ready_out <= 1'b1;															   //
					state <= STATE_IDLE;													       //
				end                                                                                //
				                                                                                   //
				//---------------------------------------------------------------------------------//
				
				
				//-------------------------Data write operation-----------------------------------// //
				STATE_WRITE : begin		                                                 		  // //
					wb_adr_i <= 3'b11;			// tx reg                                         // //
					wb_dat_i <= {slave_addr,1'b0};		// I2C slave address, write               // //
					wb_we_i  <= 1'b1;                                                             // //
					wb_stb_i <= 1'b1;                                                             // //
					wb_cyc_i <= 1'b1;                                                             // //
					state	<= STATE_POWER_UP_1_1;                                                // //
				end                                                                               // //
				STATE_POWER_UP_1_1 : begin	// wishbone ack                                       // //
					if(wb_ack_o) begin                                                            // //
						wb_stb_i <= 1'b0;                                                         // //
						wb_cyc_i <= 1'b0;                                                         // //
						state	<= STATE_POWER_UP_1_2;                                            // //
					end                                                                           // //
				end                                                                               // //
				STATE_POWER_UP_1_2 : begin	                                                      // //
					wb_adr_i <= 3'b100;			// command reg                                    // //
					wb_dat_i <= 8'b10010000;	// start bit and write bit                        // //
					wb_we_i  <= 1'b1;                                                             // //
					wb_stb_i <= 1'b1;                                                             // //
					wb_cyc_i <= 1'b1;                                                             // //
					state	<= STATE_POWER_UP_1_3;                                                // //
				end                                                                               // //
				STATE_POWER_UP_1_3 : begin	// wishbone ack                                       // //
					if(wb_ack_o) begin                                                            // //
						wb_stb_i <= 1'b0;                                                         // //
						wb_cyc_i <= 1'b0;                                                         // //
						state	<= STATE_POWER_UP_1_WAIT;                                         // //
					end                                                                           // //
				end                                                                               // //
				                                                                                  // //
				//--------Wait for I2C transfer to complete------------------//                   // //
				STATE_POWER_UP_1_WAIT : begin	// read TIP flag             //                   // //
					wb_adr_i <= 3'b100;			// status reg                //                   // //
					wb_we_i  <= 1'b0;			// read                      //                   // //
					wb_stb_i <= 1'b1;                                        //                   // //
					wb_cyc_i <= 1'b1;                                        //                   // //
					state	<= STATE_POWER_UP_1_WAIT_1;                      //                   // //
				end                                                          //                   // //
				STATE_POWER_UP_1_WAIT_1 : begin		// wishbone ack          //                   // //
					if(wb_ack_o) begin                                       //                   // //
						wb_stb_i <= 1'b0;                                    //                   // //
						wb_cyc_i <= 1'b0;                                    //                   // //
						recd_wb_data <= wb_dat_o;                            //                   // //
						state	<= STATE_POWER_UP_1_WAIT_2;                  //                   // //
					end                                                      //                   // //
				end                                                          //                   // //
				STATE_POWER_UP_1_WAIT_2 : begin                              //                   // //
					if(recd_wb_data[1]==1'b0) begin		// transfer done     //                   // //
						if(slave_addr == 7'b1110100) begin					 //                   // //
							state <= STATE_POWER_UP_3;                       //                   // //
						end                                                  //                   // //
						else begin                                           //                   // //
							state <= STATE_POWER_UP_2;                       //                   // //
						end													 //                   // //
					end                                                      //                   // //
					else begin                                               //                   // //
						state <= STATE_POWER_UP_1_WAIT;                      //                   // //
					end                                                      //                   // //
				end                                                          //                   // //
				//-----------------------------------------------------------//                   // //
				                                                                                  // //
				STATE_POWER_UP_2 : begin	                                                      // //
					wb_adr_i <= 3'b11;			// tx reg                                         // //
					wb_dat_i <= base_addr;		// base register address                          // //
					wb_we_i  <= 1'b1;                                                             // //
					wb_stb_i <= 1'b1;                                                             // //
					wb_cyc_i <= 1'b1;                                                             // //
					state	<= STATE_POWER_UP_2_1;                                                // //
				end                                                                               // //
				STATE_POWER_UP_2_1 : begin                                                        // //
					if(wb_ack_o) begin                                                            // //
						wb_stb_i <= 1'b0;                                                         // //
						wb_cyc_i <= 1'b0;                                                         // //
						state	<= STATE_POWER_UP_2_2;                                            // //
					end                                                                           // //
				end                                                                               // //
				STATE_POWER_UP_2_2 : begin	                                                      // //
					wb_adr_i <= 3'b100;			// command reg                                    // //
					wb_dat_i <= 8'b00010000;	// write bit                                      // //
					wb_we_i  <= 1'b1;                                                             // //
					wb_stb_i <= 1'b1;                                                             // //
					wb_cyc_i <= 1'b1;                                                             // //
					state	<= STATE_POWER_UP_2_3;                                                // //
				end                                                                               // //
				STATE_POWER_UP_2_3 : begin                                                        // //
					if(wb_ack_o) begin                                                            // //
						wb_stb_i <= 1'b0;                                                         // //
						wb_cyc_i <= 1'b0;                                                         // //
						state	<= STATE_POWER_UP_2_WAIT;                                         // //
					end                                                                           // //
				end                                                                               // //
				//--------Wait for I2C transfer to complete------------------//                   // //
				STATE_POWER_UP_2_WAIT : begin	// read TIP flag             //                   // //
					wb_adr_i <= 3'b100;			// status reg                //                   // //
					wb_we_i  <= 1'b0;			// read                      //                   // //
					wb_stb_i <= 1'b1;                                        //                   // //
					wb_cyc_i <= 1'b1;                                        //                   // //
					state	<= STATE_POWER_UP_2_WAIT_1;                      //                   // //
				end                                                          //                   // //
				STATE_POWER_UP_2_WAIT_1 : begin	// wishbone ack          	 //                   // //
					if(wb_ack_o) begin                                       //                   // //
						wb_stb_i <= 1'b0;                                    //                   // //
						wb_cyc_i <= 1'b0;                                    //                   // //
						recd_wb_data <= wb_dat_o;                            //                   // //
						state	<= STATE_POWER_UP_2_WAIT_2;                  //                   // //
					end                                                      //                   // //
				end                                                          //                   // //
				STATE_POWER_UP_2_WAIT_2 : begin                              //                   // //
					if(recd_wb_data[1]==1'b0) begin		// transfer done     //                   // //
						state <= STATE_POWER_UP_3;                           //                   // //
					end                                                      //                   // //
					else begin                                               //                   // //
						state <= STATE_POWER_UP_2_WAIT;                      //                   // //
					end                                                      //                   // //
				end                                                          //                   // //
				//-----------------------------------------------------------//                   // //
																								  // //
				STATE_POWER_UP_3 : begin	                                                      // //
					wb_adr_i <= 3'b11;			// tx reg                                         // //
					wb_dat_i <= wr_data;		// data to be written                             // //
					wb_we_i  <= 1'b1;                                                             // //
					wb_stb_i <= 1'b1;                                                             // //
					wb_cyc_i <= 1'b1;                                                             // //
					state	<= STATE_POWER_UP_3_1;                                                // //
				end                                                                               // //
				STATE_POWER_UP_3_1 : begin                                                        // //
					if(wb_ack_o) begin                                                            // //
						wb_stb_i <= 1'b0;                                                         // //
						wb_cyc_i <= 1'b0;                                                         // //
						state	<= STATE_POWER_UP_3_2;                                            // //
					end                                                                           // //
				end                                                                               // //
				STATE_POWER_UP_3_2 : begin	                                                      // //
					wb_adr_i <= 3'b100;			// command reg                                    // //
					wb_dat_i <= 8'b01010000;	// write bit,stop bit                             // //
					wb_we_i  <= 1'b1;                                                             // //
					wb_stb_i <= 1'b1;                                                             // //
					wb_cyc_i <= 1'b1;                                                             // //
					state	<= STATE_POWER_UP_3_3;                                                // //
				end                                                                               // //
				STATE_POWER_UP_3_3 : begin                                                        // //
					if(wb_ack_o) begin                                                            // //
						wb_stb_i <= 1'b0;                                                         // //
						wb_cyc_i <= 1'b0;                                                         // //
						state	<= STATE_POWER_UP_3_WAIT;                                         // //
					end                                                                           // //
				end                                                                               // //
				//--------Wait for I2C transfer to complete------------------//                   // //
				STATE_POWER_UP_3_WAIT : begin	// read TIP flag             //                   // //
					wb_adr_i <= 3'b100;			// status reg                //                   // //
					wb_we_i  <= 1'b0;			// read                      //                   // //
					wb_stb_i <= 1'b1;                                        //                   // //
					wb_cyc_i <= 1'b1;                                        //                   // //
					state	<= STATE_POWER_UP_3_WAIT_1;                      //                   // //
				end                                                          //                   // //
				STATE_POWER_UP_3_WAIT_1 : begin	// wishbone ack          	 //                   // //
					if(wb_ack_o) begin                                       //                   // //
						wb_stb_i <= 1'b0;                                    //                   // //
						wb_cyc_i <= 1'b0;                                    //                   // //
						recd_wb_data <= wb_dat_o;                            //                   // //
						state	<= STATE_POWER_UP_3_WAIT_2;                  //                   // //
					end                                                      //                   // //
				end                                                          //                   // //
				STATE_POWER_UP_3_WAIT_2 : begin                              //                   // //
					if(recd_wb_data[1]==1'b0) begin		// transfer done     //                   // //
						state <= STATE_POWER_UP_4;                           //                   // //
					end                                                      //                   // //
					else begin                                               //                   // //
						state <= STATE_POWER_UP_3_WAIT;                      //                   // //
					end                                                      //                   // //
				end                                                          //                   // //
				//-----------------------------------------------------------//                   // //
																								  // //
				STATE_POWER_UP_4 : begin														  // //
					done_out <= 1'b1;															  // //
					state <= STATE_POWER_UP_4_1;                                                  // //
				end                                                                               // //
				STATE_POWER_UP_4_1 : begin                                                        // //
					done_out <= 1'b0;															  // //
					ready_out <= 1'b1;															  // //
					state <= STATE_IDLE;													      // //
				end																				  // //
				//--------------------------------------------------------------------------------// //
				
				
			endcase
		end
	end	
	

endmodule
