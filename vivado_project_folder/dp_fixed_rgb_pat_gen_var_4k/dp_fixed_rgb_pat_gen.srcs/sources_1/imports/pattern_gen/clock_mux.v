`timescale 1ns / 10ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:38:22 12/16/2013 
// Design Name: 
// Module Name:    video_tx_top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module clock_mux(
		clk_in_0,
		clk_in_1,
		clk_sel,
		clk_out
    );
	

	
//-------------------------------------------------------------------------------
// I/O signals
//-------------------------------------------------------------------------------
    
	input						clk_in_0;
	input						clk_in_1;
	input						clk_sel;
	
	output						clk_out;
	
	
	
	// BUFGMUX_CTRL: 2-to-1 Global Clock MUX Buffer
	// 7 Series
	// Xilinx HDL Libraries Guide, version 14.1
	
	BUFGMUX_CTRL BUFGMUX_CTRL_inst (
		.O(clk_out), 		// 1-bit output: Clock output
		.I0(clk_in_0), 		// 1-bit input: Clock input (S=0)
		.I1(clk_in_1), 		// 1-bit input: Clock input (S=1)
		.S(clk_sel) 		// 1-bit input: Clock select
	);
	
	// End of BUFGMUX_CTRL_inst instantiation
	
	
	
	
endmodule
	