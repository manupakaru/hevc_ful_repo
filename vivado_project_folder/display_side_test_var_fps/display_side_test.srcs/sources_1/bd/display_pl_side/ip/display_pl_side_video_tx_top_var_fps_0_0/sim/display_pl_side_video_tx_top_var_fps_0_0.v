// (c) Copyright 1995-2014 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:user:video_tx_top_var_fps:1.3
// IP Revision: 6

`timescale 1ns/1ps

(* DowngradeIPIdentifiedWarnings = "yes" *)
module display_pl_side_video_tx_top_var_fps_0_0 (
  state_out,
  clk,
  reset,
  fifo_data_in,
  fifo_empty,
  fifo_prog_empty,
  fifo_rd_en,
  hsync,
  vsync,
  DE,
  pclk,
  Data_out,
  scl_pad,
  fps_in,
  sda_pad
);

output wire [31 : 0] state_out;
input wire clk;
input wire reset;
input wire [63 : 0] fifo_data_in;
input wire fifo_empty;
input wire fifo_prog_empty;
output wire fifo_rd_en;
output wire hsync;
output wire vsync;
output wire DE;
output wire pclk;
output wire [23 : 0] Data_out;
inout wire scl_pad;
input wire [31 : 0] fps_in;
inout wire sda_pad;

  video_tx_top #(
    .FREQ_IN(150),
    .FREQ_OUT(75),
    .VIDEO_OUT("1080P30")
  ) inst (
    .state_out(state_out),
    .clk(clk),
    .reset(reset),
    .fifo_data_in(fifo_data_in),
    .fifo_empty(fifo_empty),
    .fifo_prog_empty(fifo_prog_empty),
    .fifo_rd_en(fifo_rd_en),
    .hsync(hsync),
    .vsync(vsync),
    .DE(DE),
    .pclk(pclk),
    .Data_out(Data_out),
    .scl_pad(scl_pad),
    .fps_in(fps_in),
    .sda_pad(sda_pad)
  );
endmodule
