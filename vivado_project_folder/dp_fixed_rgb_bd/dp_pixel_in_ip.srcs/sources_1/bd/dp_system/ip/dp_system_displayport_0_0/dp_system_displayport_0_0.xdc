# file: dp_system_displayport_0_0.xdc (IP Level XDC)

#-----------------------------------------------------------------
## Clock Constraints
#-----------------------------------------------------------------
# Assuming 135 MHz GT Reference Clock

#create_clock  -period 7.407   [get_ports lnk_clk]
create_clock -name "TX_GTCLK"  -period 7.407 [get_pins -hierarchical -filter {name=~*gt0*TXOUTCLK}]
create_clock -name "RX_GTCLK"  -period 7.407 [get_pins -hierarchical -filter {name=~*gt0*RXOUTCLK}]
#create_clock  -period 7.407   [get_pins -hierarchical *ref_clk_out_bufg*/O]

