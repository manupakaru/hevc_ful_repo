/******************************************************************************
-- (c) Copyright 1995 - 2013 Xilinx, Inc. All rights reserved.
--
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and 
-- international copyright and other intellectual property
-- laws.
--
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
*******************************************************************************/

`timescale 1ns / 1ps

module dp_src_pm_v1_0
    (
    // output [13:0]DDR3_addr,
    // output [2:0]DDR3_ba,
    // output DDR3_cas_n,
    // output [0:0]DDR3_ck_n,
    // output [0:0]DDR3_ck_p,
    // output [0:0]DDR3_cke,
    // output [0:0]DDR3_cs_n,
    // output [7:0]DDR3_dm,
    // inout [63:0]DDR3_dq,
    // inout [7:0]DDR3_dqs_n,
    // inout [7:0]DDR3_dqs_p,
    // output [0:0]DDR3_odt,
    // output DDR3_ras_n,
    // output DDR3_reset_n,
    // output DDR3_we_n,
    inout [14:0]DDR_addr,
    inout [2:0]DDR_ba,
    inout DDR_cas_n,
    inout DDR_ck_n,
    inout DDR_ck_p,
    inout DDR_cke,
    inout DDR_cs_n,
    inout [3:0]DDR_dm,
    inout [31:0]DDR_dq,
    inout [3:0]DDR_dqs_n,
    inout [3:0]DDR_dqs_p,
    inout DDR_odt,
    inout DDR_ras_n,
    inout DDR_reset_n,
    inout DDR_we_n,
    //input [0:0]Dp_Int,
    inout FIXED_IO_ddr_vrn,
    inout FIXED_IO_ddr_vrp,
    inout [53:0]FIXED_IO_mio,
    inout FIXED_IO_ps_clk,
    inout FIXED_IO_ps_porb,
    inout FIXED_IO_ps_srstb,
      input  wire        clk_200_p,
      input  wire        clk_200_n,

      input  wire        lnk_clk_p,
      input  wire        lnk_clk_n,
      

      output wire [4-1:0]  lnk_tx_lane_p,
      output wire [4-1:0]  lnk_tx_lane_n,

      inout  wire        aux_tx_io_p,
      inout wire        aux_tx_io_n,
      input  wire        hpd,
      inout  wire        kc705_iic_Scl,
      inout  wire        kc705_iic_Sda,
      output	clk_int_135_out_n, //135output non-cleaned, going to SI5324
      output	clk_int_135_out_p,
      output wire [1:0]  gpo

    );


  wire clk_int_135;
  wire clk_int_135_out;









//IPI Design
dp_system_wrapper dp_policy_maker_inst
  (
       .dp_mainlink_lnk_clk_p            (lnk_clk_p),
       .dp_mainlink_lnk_clk_n            (lnk_clk_n),
       .dp_mainlink_lnk_tx_lane_p        (lnk_tx_lane_p),
       .dp_mainlink_lnk_tx_lane_n        (lnk_tx_lane_n),

       .aux_tx_io_p (aux_tx_io_p),
       .aux_tx_io_n (aux_tx_io_n),

       .tx_hpd               (hpd),

   .DDR_addr(DDR_addr),
   .DDR_ba(DDR_ba),
   .DDR_cas_n(DDR_cas_n),
   .DDR_ck_n(DDR_ck_n),
   .DDR_ck_p(DDR_ck_p),
   .DDR_cke(DDR_cke),
   .DDR_cs_n(DDR_cs_n),
   .DDR_dm(DDR_dm),
   .DDR_dq(DDR_dq),
   .DDR_dqs_n(DDR_dqs_n),
   .DDR_dqs_p(DDR_dqs_p),
   .DDR_odt(DDR_odt),
   .DDR_ras_n(DDR_ras_n),
   .DDR_reset_n(DDR_reset_n),
   .DDR_we_n(DDR_we_n),
   .FIXED_IO_ddr_vrn(FIXED_IO_ddr_vrn),
   .FIXED_IO_ddr_vrp(FIXED_IO_ddr_vrp),
   .FIXED_IO_mio(FIXED_IO_mio),
   .FIXED_IO_ps_clk(FIXED_IO_ps_clk),
   .FIXED_IO_ps_porb(FIXED_IO_ps_porb),
   .FIXED_IO_ps_srstb(FIXED_IO_ps_srstb),
    .iic2inctc_irpt_sda_io                    (kc705_iic_Sda),
    .iic2inctc_irpt_scl_io                    (kc705_iic_Scl),

    .SYS_CLK_clk_p                     (clk_200_p),
    .SYS_CLK_clk_n                     (clk_200_n),
    
    
    .gpo                           (gpo),
    
    .clk_out2                     (clk_int_135)

    
    );
        
//generation of 135MHz through ODDR to si5324 input
ODDR clk_out_oddr
   (.Q  (clk_int_135_out),
    .C  (clk_int_135),
    .CE (1'b1),
    .D1 (1'b1),
    .D2 (1'b0),
    .R  (1'b0),
    .S  (1'b0));

////135output non-cleaned, going to SI5324 - Clock Output Buffer
OBUFDS
 #(.IOSTANDARD ("LVDS_25"))
clk135out_int_inst
  (.O          (clk_int_135_out_p),
   .OB         (clk_int_135_out_n),
   .I          (clk_int_135_out));

   
endmodule
