`timescale 1ps/1ps

module vid_clkgen (
  input  wire       reset_i,      //reset 
  input  wire       refclk_i,     //reference clock for synthesis
  input  wire       progclk_i,    //progamming interface clock
  input  wire       prog_i,       //one cycle long programming kick off
  input  wire [7:0] M_i,          //M
  input  wire [15:0] D_i,          //D
  output wire       vid_clk_o,    //vid_clk output
  output wire       locked_o      //vid_clk is locked
 );

////
//// Delay the kick off command for 16 cycles so the SPI controller
//// has enough time to reset
////
  wire go;
  SRL16E # (
    .INIT(16'h0)
  ) SRL16E_0 (
    .Q(go),
    .A0(1'b1),
    .A1(1'b1),
    .A2(1'b1),
    .A3(1'b1),
    .CE(1'b1),
    .CLK(progclk_i),
    .D(prog_i)
  );

wire [6:0]  DADDR;
wire        DCLK;
wire        DEN;
wire [15:0] DI;
wire [15:0] DO;
wire        DRDY;
wire        DWE;

  wire vid_clk;
  wire srdy_out;
  wire rst_mmcm;

  video_pixel_clk_gen video_pixel_clk_gen_inst
   (// Clock in ports
    .CLK_IN1(refclk_i),      // IN
    // Clock out ports
    .CLK_OUT1(vid_clk),     // OUT
    // Dynamic reconfiguration ports
    .DADDR(DADDR),// IN [6:0]
    .DCLK(DCLK), // IN
    .DEN(DEN),// IN
    .DIN(DI), // IN [15:0]
    .DOUT(DO), // OUT [15:0]
    .DRDY(DRDY), // IN
    .DWE(DWE),// OUT
    // Status and control signals
    .RESET(rst_mmcm),// IN
    .LOCKED(locked_o));      // OUT

  assign vid_clk_o = vid_clk;


  mmcm_drp mmcm_drp_inst
  (
    .SADDR (0),
    .SEN (go),
    .SCLK (progclk_i),
    .RST (reset_i),
    .SRDY (srdy_out),
    .video_drp_in ({8'h00,M_i,D_i}),

    .DO (DO),
    .DRDY (DRDY),
    .LOCKED (locked_o),
    .DWE (DWE),
    .DEN (DEN),
    .DADDR (DADDR),
    .DI (DI),
    .DCLK(DCLK),
    .RST_MMCM(rst_mmcm)

  );

endmodule
