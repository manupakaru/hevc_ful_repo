//Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2014.2 (win64) Build 932637 Wed Jun 11 13:33:10 MDT 2014
//Date        : Fri Dec 05 05:58:34 2014
//Host        : GCLAP4 running 64-bit major release  (build 9200)
//Command     : generate_target display_pl_side.bd
//Design      : display_pl_side
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "display_pl_side,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLanguage=VERILOG,numBlks=14,numReposBlks=11,numNonXlnxBlks=0,numHierBlks=3,maxHierDepth=1,da_axi4_cnt=4,da_ps7_cnt=1}" *) 
module display_pl_side
   (DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    DE,
    Data_out,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    hsync,
    pclk,
    scl_pad,
    sda_pad,
    vsync);
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  output DE;
  output [23:0]Data_out;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  output hsync;
  output pclk;
  inout scl_pad;
  inout sda_pad;
  output vsync;

  wire GND_1;
  wire Net;
  wire Net1;
  wire VCC_1;
(* MARK_DEBUG *)   wire [31:0]axi_displaybuffer_handle_0_axi_ARADDR;
(* MARK_DEBUG *)   wire [1:0]axi_displaybuffer_handle_0_axi_ARBURST;
(* MARK_DEBUG *)   wire [3:0]axi_displaybuffer_handle_0_axi_ARID;
(* MARK_DEBUG *)   wire [7:0]axi_displaybuffer_handle_0_axi_ARLEN;
(* MARK_DEBUG *)   wire axi_displaybuffer_handle_0_axi_ARREADY;
(* MARK_DEBUG *)   wire [2:0]axi_displaybuffer_handle_0_axi_ARSIZE;
(* MARK_DEBUG *)   wire axi_displaybuffer_handle_0_axi_ARVALID;
(* MARK_DEBUG *)   wire [31:0]axi_displaybuffer_handle_0_axi_AWADDR;
(* MARK_DEBUG *)   wire [1:0]axi_displaybuffer_handle_0_axi_AWBURST;
(* MARK_DEBUG *)   wire [3:0]axi_displaybuffer_handle_0_axi_AWID;
(* MARK_DEBUG *)   wire [7:0]axi_displaybuffer_handle_0_axi_AWLEN;
(* MARK_DEBUG *)   wire axi_displaybuffer_handle_0_axi_AWREADY;
(* MARK_DEBUG *)   wire [2:0]axi_displaybuffer_handle_0_axi_AWSIZE;
(* MARK_DEBUG *)   wire axi_displaybuffer_handle_0_axi_AWVALID;
(* MARK_DEBUG *)   wire axi_displaybuffer_handle_0_axi_BREADY;
(* MARK_DEBUG *)   wire [1:0]axi_displaybuffer_handle_0_axi_BRESP;
(* MARK_DEBUG *)   wire axi_displaybuffer_handle_0_axi_BVALID;
(* MARK_DEBUG *)   wire [511:0]axi_displaybuffer_handle_0_axi_RDATA;
(* MARK_DEBUG *)   wire axi_displaybuffer_handle_0_axi_RLAST;
(* MARK_DEBUG *)   wire axi_displaybuffer_handle_0_axi_RREADY;
(* MARK_DEBUG *)   wire [1:0]axi_displaybuffer_handle_0_axi_RRESP;
(* MARK_DEBUG *)   wire axi_displaybuffer_handle_0_axi_RVALID;
(* MARK_DEBUG *)   wire [511:0]axi_displaybuffer_handle_0_axi_WDATA;
(* MARK_DEBUG *)   wire axi_displaybuffer_handle_0_axi_WLAST;
(* MARK_DEBUG *)   wire axi_displaybuffer_handle_0_axi_WREADY;
(* MARK_DEBUG *)   wire [63:0]axi_displaybuffer_handle_0_axi_WSTRB;
(* MARK_DEBUG *)   wire axi_displaybuffer_handle_0_axi_WVALID;
(* MARK_DEBUG *)   wire axi_displaybuffer_handle_0_displaybuffer_fifo_read_en_out;
(* MARK_DEBUG *)   wire [63:0]axi_displaybuffer_handle_0_hdmi_fifo_data_out;
(* MARK_DEBUG *)   wire axi_displaybuffer_handle_0_hdmi_fifo_w_en;
(* MARK_DEBUG *)   wire [31:0]axi_dwidth_converter_0_M_AXI_ARADDR;
(* MARK_DEBUG *)   wire [1:0]axi_dwidth_converter_0_M_AXI_ARBURST;
(* MARK_DEBUG *)   wire [3:0]axi_dwidth_converter_0_M_AXI_ARCACHE;
(* MARK_DEBUG *)   wire [3:0]axi_dwidth_converter_0_M_AXI_ARLEN;
(* MARK_DEBUG *)   wire [1:0]axi_dwidth_converter_0_M_AXI_ARLOCK;
(* MARK_DEBUG *)   wire [2:0]axi_dwidth_converter_0_M_AXI_ARPROT;
(* MARK_DEBUG *)   wire [3:0]axi_dwidth_converter_0_M_AXI_ARQOS;
(* MARK_DEBUG *)   wire axi_dwidth_converter_0_M_AXI_ARREADY;
(* MARK_DEBUG *)   wire [2:0]axi_dwidth_converter_0_M_AXI_ARSIZE;
(* MARK_DEBUG *)   wire axi_dwidth_converter_0_M_AXI_ARVALID;
(* MARK_DEBUG *)   wire [31:0]axi_dwidth_converter_0_M_AXI_AWADDR;
(* MARK_DEBUG *)   wire [1:0]axi_dwidth_converter_0_M_AXI_AWBURST;
(* MARK_DEBUG *)   wire [3:0]axi_dwidth_converter_0_M_AXI_AWCACHE;
(* MARK_DEBUG *)   wire [3:0]axi_dwidth_converter_0_M_AXI_AWLEN;
(* MARK_DEBUG *)   wire [1:0]axi_dwidth_converter_0_M_AXI_AWLOCK;
(* MARK_DEBUG *)   wire [2:0]axi_dwidth_converter_0_M_AXI_AWPROT;
(* MARK_DEBUG *)   wire [3:0]axi_dwidth_converter_0_M_AXI_AWQOS;
(* MARK_DEBUG *)   wire axi_dwidth_converter_0_M_AXI_AWREADY;
(* MARK_DEBUG *)   wire [2:0]axi_dwidth_converter_0_M_AXI_AWSIZE;
(* MARK_DEBUG *)   wire axi_dwidth_converter_0_M_AXI_AWVALID;
(* MARK_DEBUG *)   wire axi_dwidth_converter_0_M_AXI_BREADY;
(* MARK_DEBUG *)   wire [1:0]axi_dwidth_converter_0_M_AXI_BRESP;
(* MARK_DEBUG *)   wire axi_dwidth_converter_0_M_AXI_BVALID;
(* MARK_DEBUG *)   wire [63:0]axi_dwidth_converter_0_M_AXI_RDATA;
(* MARK_DEBUG *)   wire axi_dwidth_converter_0_M_AXI_RLAST;
(* MARK_DEBUG *)   wire axi_dwidth_converter_0_M_AXI_RREADY;
(* MARK_DEBUG *)   wire [1:0]axi_dwidth_converter_0_M_AXI_RRESP;
(* MARK_DEBUG *)   wire axi_dwidth_converter_0_M_AXI_RVALID;
(* MARK_DEBUG *)   wire [63:0]axi_dwidth_converter_0_M_AXI_WDATA;
(* MARK_DEBUG *)   wire axi_dwidth_converter_0_M_AXI_WLAST;
(* MARK_DEBUG *)   wire axi_dwidth_converter_0_M_AXI_WREADY;
(* MARK_DEBUG *)   wire [7:0]axi_dwidth_converter_0_M_AXI_WSTRB;
(* MARK_DEBUG *)   wire axi_dwidth_converter_0_M_AXI_WVALID;
(* MARK_DEBUG *)   wire [31:0]axi_protocol_converter_0_M_AXI_ARADDR;
(* MARK_DEBUG *)   wire [1:0]axi_protocol_converter_0_M_AXI_ARBURST;
(* MARK_DEBUG *)   wire [3:0]axi_protocol_converter_0_M_AXI_ARCACHE;
(* MARK_DEBUG *)   wire [0:0]axi_protocol_converter_0_M_AXI_ARID;
(* MARK_DEBUG *)   wire [3:0]axi_protocol_converter_0_M_AXI_ARLEN;
(* MARK_DEBUG *)   wire [1:0]axi_protocol_converter_0_M_AXI_ARLOCK;
(* MARK_DEBUG *)   wire [2:0]axi_protocol_converter_0_M_AXI_ARPROT;
(* MARK_DEBUG *)   wire [3:0]axi_protocol_converter_0_M_AXI_ARQOS;
(* MARK_DEBUG *)   wire axi_protocol_converter_0_M_AXI_ARREADY;
(* MARK_DEBUG *)   wire [2:0]axi_protocol_converter_0_M_AXI_ARSIZE;
(* MARK_DEBUG *)   wire axi_protocol_converter_0_M_AXI_ARVALID;
(* MARK_DEBUG *)   wire [31:0]axi_protocol_converter_0_M_AXI_AWADDR;
(* MARK_DEBUG *)   wire [1:0]axi_protocol_converter_0_M_AXI_AWBURST;
(* MARK_DEBUG *)   wire [3:0]axi_protocol_converter_0_M_AXI_AWCACHE;
(* MARK_DEBUG *)   wire [0:0]axi_protocol_converter_0_M_AXI_AWID;
(* MARK_DEBUG *)   wire [3:0]axi_protocol_converter_0_M_AXI_AWLEN;
(* MARK_DEBUG *)   wire [1:0]axi_protocol_converter_0_M_AXI_AWLOCK;
(* MARK_DEBUG *)   wire [2:0]axi_protocol_converter_0_M_AXI_AWPROT;
(* MARK_DEBUG *)   wire [3:0]axi_protocol_converter_0_M_AXI_AWQOS;
(* MARK_DEBUG *)   wire axi_protocol_converter_0_M_AXI_AWREADY;
(* MARK_DEBUG *)   wire [2:0]axi_protocol_converter_0_M_AXI_AWSIZE;
(* MARK_DEBUG *)   wire axi_protocol_converter_0_M_AXI_AWVALID;
(* MARK_DEBUG *)   wire [0:0]axi_protocol_converter_0_M_AXI_BID;
(* MARK_DEBUG *)   wire axi_protocol_converter_0_M_AXI_BREADY;
(* MARK_DEBUG *)   wire [1:0]axi_protocol_converter_0_M_AXI_BRESP;
(* MARK_DEBUG *)   wire axi_protocol_converter_0_M_AXI_BVALID;
(* MARK_DEBUG *)   wire [511:0]axi_protocol_converter_0_M_AXI_RDATA;
(* MARK_DEBUG *)   wire [0:0]axi_protocol_converter_0_M_AXI_RID;
(* MARK_DEBUG *)   wire axi_protocol_converter_0_M_AXI_RLAST;
(* MARK_DEBUG *)   wire axi_protocol_converter_0_M_AXI_RREADY;
(* MARK_DEBUG *)   wire [1:0]axi_protocol_converter_0_M_AXI_RRESP;
(* MARK_DEBUG *)   wire axi_protocol_converter_0_M_AXI_RVALID;
(* MARK_DEBUG *)   wire [511:0]axi_protocol_converter_0_M_AXI_WDATA;
(* MARK_DEBUG *)   wire axi_protocol_converter_0_M_AXI_WLAST;
(* MARK_DEBUG *)   wire axi_protocol_converter_0_M_AXI_WREADY;
(* MARK_DEBUG *)   wire [63:0]axi_protocol_converter_0_M_AXI_WSTRB;
(* MARK_DEBUG *)   wire axi_protocol_converter_0_M_AXI_WVALID;
(* MARK_DEBUG *)   wire [783:0]dummy_display_fifo_driver_0_fifo_data_out;
(* MARK_DEBUG *)   wire dummy_display_fifo_driver_0_fifo_wr_en_out;
(* MARK_DEBUG *)   wire [11:0]dummy_display_fifo_driver_0_pic_height_out;
(* MARK_DEBUG *)   wire [11:0]dummy_display_fifo_driver_0_pic_width_out;
(* MARK_DEBUG *)   wire [3:0]dummy_display_fifo_driver_0_poc_out;
(* MARK_DEBUG *)   wire [15:0]dummy_ps_logic_M00_AXI_ARADDR;
(* MARK_DEBUG *)   wire [1:0]dummy_ps_logic_M00_AXI_ARBURST;
(* MARK_DEBUG *)   wire [3:0]dummy_ps_logic_M00_AXI_ARCACHE;
(* MARK_DEBUG *)   wire [11:0]dummy_ps_logic_M00_AXI_ARID;
(* MARK_DEBUG *)   wire [7:0]dummy_ps_logic_M00_AXI_ARLEN;
(* MARK_DEBUG *)   wire dummy_ps_logic_M00_AXI_ARLOCK;
(* MARK_DEBUG *)   wire [2:0]dummy_ps_logic_M00_AXI_ARPROT;
(* MARK_DEBUG *)   wire [3:0]dummy_ps_logic_M00_AXI_ARQOS;
(* MARK_DEBUG *)   wire dummy_ps_logic_M00_AXI_ARREADY;
(* MARK_DEBUG *)   wire [2:0]dummy_ps_logic_M00_AXI_ARSIZE;
(* MARK_DEBUG *)   wire dummy_ps_logic_M00_AXI_ARVALID;
(* MARK_DEBUG *)   wire [15:0]dummy_ps_logic_M00_AXI_AWADDR;
(* MARK_DEBUG *)   wire [1:0]dummy_ps_logic_M00_AXI_AWBURST;
(* MARK_DEBUG *)   wire [3:0]dummy_ps_logic_M00_AXI_AWCACHE;
(* MARK_DEBUG *)   wire [11:0]dummy_ps_logic_M00_AXI_AWID;
(* MARK_DEBUG *)   wire [7:0]dummy_ps_logic_M00_AXI_AWLEN;
(* MARK_DEBUG *)   wire dummy_ps_logic_M00_AXI_AWLOCK;
(* MARK_DEBUG *)   wire [2:0]dummy_ps_logic_M00_AXI_AWPROT;
(* MARK_DEBUG *)   wire [3:0]dummy_ps_logic_M00_AXI_AWQOS;
(* MARK_DEBUG *)   wire dummy_ps_logic_M00_AXI_AWREADY;
(* MARK_DEBUG *)   wire [2:0]dummy_ps_logic_M00_AXI_AWSIZE;
(* MARK_DEBUG *)   wire dummy_ps_logic_M00_AXI_AWVALID;
(* MARK_DEBUG *)   wire [11:0]dummy_ps_logic_M00_AXI_BID;
(* MARK_DEBUG *)   wire dummy_ps_logic_M00_AXI_BREADY;
(* MARK_DEBUG *)   wire [1:0]dummy_ps_logic_M00_AXI_BRESP;
(* MARK_DEBUG *)   wire dummy_ps_logic_M00_AXI_BVALID;
(* MARK_DEBUG *)   wire [31:0]dummy_ps_logic_M00_AXI_RDATA;
(* MARK_DEBUG *)   wire [11:0]dummy_ps_logic_M00_AXI_RID;
(* MARK_DEBUG *)   wire dummy_ps_logic_M00_AXI_RLAST;
(* MARK_DEBUG *)   wire dummy_ps_logic_M00_AXI_RREADY;
(* MARK_DEBUG *)   wire [1:0]dummy_ps_logic_M00_AXI_RRESP;
(* MARK_DEBUG *)   wire dummy_ps_logic_M00_AXI_RVALID;
(* MARK_DEBUG *)   wire [31:0]dummy_ps_logic_M00_AXI_WDATA;
(* MARK_DEBUG *)   wire dummy_ps_logic_M00_AXI_WLAST;
(* MARK_DEBUG *)   wire dummy_ps_logic_M00_AXI_WREADY;
(* MARK_DEBUG *)   wire [3:0]dummy_ps_logic_M00_AXI_WSTRB;
(* MARK_DEBUG *)   wire dummy_ps_logic_M00_AXI_WVALID;
(* MARK_DEBUG *)   wire fifo_generator_0_almost_full;
(* MARK_DEBUG *)   wire [783:0]fifo_generator_0_dout;
(* MARK_DEBUG *)   wire fifo_generator_0_empty;
(* MARK_DEBUG *)   wire [63:0]fifo_generator_1_dout;
(* MARK_DEBUG *)   wire fifo_generator_1_empty;
(* MARK_DEBUG *)   wire fifo_generator_1_prog_empty;
(* MARK_DEBUG *)   wire fifo_generator_1_prog_full;
  wire [0:0]proc_sys_reset_0_interconnect_aresetn;
  wire [0:0]proc_sys_reset_0_peripheral_aresetn;
(* MARK_DEBUG *)   wire [0:0]proc_sys_reset_0_peripheral_reset;
  wire [14:0]processing_system7_0_DDR_ADDR;
  wire [2:0]processing_system7_0_DDR_BA;
  wire processing_system7_0_DDR_CAS_N;
  wire processing_system7_0_DDR_CKE;
  wire processing_system7_0_DDR_CK_N;
  wire processing_system7_0_DDR_CK_P;
  wire processing_system7_0_DDR_CS_N;
  wire [3:0]processing_system7_0_DDR_DM;
  wire [31:0]processing_system7_0_DDR_DQ;
  wire [3:0]processing_system7_0_DDR_DQS_N;
  wire [3:0]processing_system7_0_DDR_DQS_P;
  wire processing_system7_0_DDR_ODT;
  wire processing_system7_0_DDR_RAS_N;
  wire processing_system7_0_DDR_RESET_N;
  wire processing_system7_0_DDR_WE_N;
  wire processing_system7_0_FCLK_CLK0;
  wire processing_system7_0_FCLK_CLK1;
  wire processing_system7_0_FCLK_CLK2;
(* MARK_DEBUG *)   wire processing_system7_0_FCLK_RESET0_N;
  wire processing_system7_0_FIXED_IO_DDR_VRN;
  wire processing_system7_0_FIXED_IO_DDR_VRP;
  wire [53:0]processing_system7_0_FIXED_IO_MIO;
  wire processing_system7_0_FIXED_IO_PS_CLK;
  wire processing_system7_0_FIXED_IO_PS_PORB;
  wire processing_system7_0_FIXED_IO_PS_SRSTB;
  wire [31:0]processing_system7_0_M_AXI_GP0_ARADDR;
  wire [1:0]processing_system7_0_M_AXI_GP0_ARBURST;
  wire [3:0]processing_system7_0_M_AXI_GP0_ARCACHE;
  wire [11:0]processing_system7_0_M_AXI_GP0_ARID;
  wire [3:0]processing_system7_0_M_AXI_GP0_ARLEN;
  wire [1:0]processing_system7_0_M_AXI_GP0_ARLOCK;
  wire [2:0]processing_system7_0_M_AXI_GP0_ARPROT;
  wire [3:0]processing_system7_0_M_AXI_GP0_ARQOS;
  wire processing_system7_0_M_AXI_GP0_ARREADY;
  wire [2:0]processing_system7_0_M_AXI_GP0_ARSIZE;
  wire processing_system7_0_M_AXI_GP0_ARVALID;
  wire [31:0]processing_system7_0_M_AXI_GP0_AWADDR;
  wire [1:0]processing_system7_0_M_AXI_GP0_AWBURST;
  wire [3:0]processing_system7_0_M_AXI_GP0_AWCACHE;
  wire [11:0]processing_system7_0_M_AXI_GP0_AWID;
  wire [3:0]processing_system7_0_M_AXI_GP0_AWLEN;
  wire [1:0]processing_system7_0_M_AXI_GP0_AWLOCK;
  wire [2:0]processing_system7_0_M_AXI_GP0_AWPROT;
  wire [3:0]processing_system7_0_M_AXI_GP0_AWQOS;
  wire processing_system7_0_M_AXI_GP0_AWREADY;
  wire [2:0]processing_system7_0_M_AXI_GP0_AWSIZE;
  wire processing_system7_0_M_AXI_GP0_AWVALID;
  wire [11:0]processing_system7_0_M_AXI_GP0_BID;
  wire processing_system7_0_M_AXI_GP0_BREADY;
  wire [1:0]processing_system7_0_M_AXI_GP0_BRESP;
  wire processing_system7_0_M_AXI_GP0_BVALID;
  wire [31:0]processing_system7_0_M_AXI_GP0_RDATA;
  wire [11:0]processing_system7_0_M_AXI_GP0_RID;
  wire processing_system7_0_M_AXI_GP0_RLAST;
  wire processing_system7_0_M_AXI_GP0_RREADY;
  wire [1:0]processing_system7_0_M_AXI_GP0_RRESP;
  wire processing_system7_0_M_AXI_GP0_RVALID;
  wire [31:0]processing_system7_0_M_AXI_GP0_WDATA;
  wire [11:0]processing_system7_0_M_AXI_GP0_WID;
  wire processing_system7_0_M_AXI_GP0_WLAST;
  wire processing_system7_0_M_AXI_GP0_WREADY;
  wire [3:0]processing_system7_0_M_AXI_GP0_WSTRB;
  wire processing_system7_0_M_AXI_GP0_WVALID;
(* MARK_DEBUG *)   wire [10:0]rd_data_count;
(* MARK_DEBUG *)   wire [31:0]state_out;
(* MARK_DEBUG *)   wire video_tx_top_0_DE;
  wire [23:0]video_tx_top_0_Data_out;
(* MARK_DEBUG *)   wire video_tx_top_0_fifo_rd_en;
(* MARK_DEBUG *)   wire video_tx_top_0_hsync;
(* MARK_DEBUG *)   wire video_tx_top_0_pclk;
(* MARK_DEBUG *)   wire video_tx_top_0_vsync;
(* MARK_DEBUG *)   wire [10:0]wr_data_count;

  assign DE = video_tx_top_0_DE;
  assign Data_out[23:0] = video_tx_top_0_Data_out;
  assign hsync = video_tx_top_0_hsync;
  assign pclk = video_tx_top_0_pclk;
  assign vsync = video_tx_top_0_vsync;
GND GND
       (.G(GND_1));
VCC VCC
       (.P(VCC_1));
display_pl_side_axi_displaybuffer_handle_0_0 axi_displaybuffer_handle_0
       (.axi_araddr(axi_displaybuffer_handle_0_axi_ARADDR),
        .axi_arburst(axi_displaybuffer_handle_0_axi_ARBURST),
        .axi_arid(axi_displaybuffer_handle_0_axi_ARID),
        .axi_arlen(axi_displaybuffer_handle_0_axi_ARLEN),
        .axi_arready(axi_displaybuffer_handle_0_axi_ARREADY),
        .axi_arsize(axi_displaybuffer_handle_0_axi_ARSIZE),
        .axi_arvalid(axi_displaybuffer_handle_0_axi_ARVALID),
        .axi_awaddr(axi_displaybuffer_handle_0_axi_AWADDR),
        .axi_awburst(axi_displaybuffer_handle_0_axi_AWBURST),
        .axi_awid(axi_displaybuffer_handle_0_axi_AWID),
        .axi_awlen(axi_displaybuffer_handle_0_axi_AWLEN),
        .axi_awready(axi_displaybuffer_handle_0_axi_AWREADY),
        .axi_awsize(axi_displaybuffer_handle_0_axi_AWSIZE),
        .axi_awvalid(axi_displaybuffer_handle_0_axi_AWVALID),
        .axi_bready(axi_displaybuffer_handle_0_axi_BREADY),
        .axi_bresp(axi_displaybuffer_handle_0_axi_BRESP),
        .axi_bvalid(axi_displaybuffer_handle_0_axi_BVALID),
        .axi_clk(VCC_1),
        .axi_rdata(axi_displaybuffer_handle_0_axi_RDATA),
        .axi_rlast(axi_displaybuffer_handle_0_axi_RLAST),
        .axi_rready(axi_displaybuffer_handle_0_axi_RREADY),
        .axi_rresp(axi_displaybuffer_handle_0_axi_RRESP),
        .axi_rvalid(axi_displaybuffer_handle_0_axi_RVALID),
        .axi_wdata(axi_displaybuffer_handle_0_axi_WDATA),
        .axi_wlast(axi_displaybuffer_handle_0_axi_WLAST),
        .axi_wready(axi_displaybuffer_handle_0_axi_WREADY),
        .axi_wstrb(axi_displaybuffer_handle_0_axi_WSTRB),
        .axi_wvalid(axi_displaybuffer_handle_0_axi_WVALID),
        .clk(processing_system7_0_FCLK_CLK2),
        .config_valid_in(VCC_1),
        .displaybuffer_fifo_almost_empty_in(fifo_generator_0_empty),
        .displaybuffer_fifo_data_in(fifo_generator_0_dout),
        .displaybuffer_fifo_empty_in(fifo_generator_0_empty),
        .displaybuffer_fifo_read_en_out(axi_displaybuffer_handle_0_displaybuffer_fifo_read_en_out),
        .hdmi_fifo_almost_full(fifo_generator_1_prog_full),
        .hdmi_fifo_data_out(axi_displaybuffer_handle_0_hdmi_fifo_data_out),
        .hdmi_fifo_w_en(axi_displaybuffer_handle_0_hdmi_fifo_w_en),
        .pic_height_in(dummy_display_fifo_driver_0_pic_height_out[10:0]),
        .pic_width_in(dummy_display_fifo_driver_0_pic_width_out[10:0]),
        .poc_4bits_in(dummy_display_fifo_driver_0_poc_out),
        .reset(proc_sys_reset_0_peripheral_reset));
display_pl_side_axi_dwidth_converter_0_0 axi_dwidth_converter_0
       (.m_axi_araddr(axi_dwidth_converter_0_M_AXI_ARADDR),
        .m_axi_arburst(axi_dwidth_converter_0_M_AXI_ARBURST),
        .m_axi_arcache(axi_dwidth_converter_0_M_AXI_ARCACHE),
        .m_axi_arlen(axi_dwidth_converter_0_M_AXI_ARLEN),
        .m_axi_arlock(axi_dwidth_converter_0_M_AXI_ARLOCK),
        .m_axi_arprot(axi_dwidth_converter_0_M_AXI_ARPROT),
        .m_axi_arqos(axi_dwidth_converter_0_M_AXI_ARQOS),
        .m_axi_arready(axi_dwidth_converter_0_M_AXI_ARREADY),
        .m_axi_arsize(axi_dwidth_converter_0_M_AXI_ARSIZE),
        .m_axi_arvalid(axi_dwidth_converter_0_M_AXI_ARVALID),
        .m_axi_awaddr(axi_dwidth_converter_0_M_AXI_AWADDR),
        .m_axi_awburst(axi_dwidth_converter_0_M_AXI_AWBURST),
        .m_axi_awcache(axi_dwidth_converter_0_M_AXI_AWCACHE),
        .m_axi_awlen(axi_dwidth_converter_0_M_AXI_AWLEN),
        .m_axi_awlock(axi_dwidth_converter_0_M_AXI_AWLOCK),
        .m_axi_awprot(axi_dwidth_converter_0_M_AXI_AWPROT),
        .m_axi_awqos(axi_dwidth_converter_0_M_AXI_AWQOS),
        .m_axi_awready(axi_dwidth_converter_0_M_AXI_AWREADY),
        .m_axi_awsize(axi_dwidth_converter_0_M_AXI_AWSIZE),
        .m_axi_awvalid(axi_dwidth_converter_0_M_AXI_AWVALID),
        .m_axi_bready(axi_dwidth_converter_0_M_AXI_BREADY),
        .m_axi_bresp(axi_dwidth_converter_0_M_AXI_BRESP),
        .m_axi_bvalid(axi_dwidth_converter_0_M_AXI_BVALID),
        .m_axi_rdata(axi_dwidth_converter_0_M_AXI_RDATA),
        .m_axi_rlast(axi_dwidth_converter_0_M_AXI_RLAST),
        .m_axi_rready(axi_dwidth_converter_0_M_AXI_RREADY),
        .m_axi_rresp(axi_dwidth_converter_0_M_AXI_RRESP),
        .m_axi_rvalid(axi_dwidth_converter_0_M_AXI_RVALID),
        .m_axi_wdata(axi_dwidth_converter_0_M_AXI_WDATA),
        .m_axi_wlast(axi_dwidth_converter_0_M_AXI_WLAST),
        .m_axi_wready(axi_dwidth_converter_0_M_AXI_WREADY),
        .m_axi_wstrb(axi_dwidth_converter_0_M_AXI_WSTRB),
        .m_axi_wvalid(axi_dwidth_converter_0_M_AXI_WVALID),
        .s_axi_aclk(processing_system7_0_FCLK_CLK2),
        .s_axi_araddr(axi_protocol_converter_0_M_AXI_ARADDR),
        .s_axi_arburst(axi_protocol_converter_0_M_AXI_ARBURST),
        .s_axi_arcache(axi_protocol_converter_0_M_AXI_ARCACHE),
        .s_axi_aresetn(proc_sys_reset_0_peripheral_aresetn),
        .s_axi_arid(axi_protocol_converter_0_M_AXI_ARID),
        .s_axi_arlen(axi_protocol_converter_0_M_AXI_ARLEN),
        .s_axi_arlock(axi_protocol_converter_0_M_AXI_ARLOCK),
        .s_axi_arprot(axi_protocol_converter_0_M_AXI_ARPROT),
        .s_axi_arqos(axi_protocol_converter_0_M_AXI_ARQOS),
        .s_axi_arready(axi_protocol_converter_0_M_AXI_ARREADY),
        .s_axi_arsize(axi_protocol_converter_0_M_AXI_ARSIZE),
        .s_axi_arvalid(axi_protocol_converter_0_M_AXI_ARVALID),
        .s_axi_awaddr(axi_protocol_converter_0_M_AXI_AWADDR),
        .s_axi_awburst(axi_protocol_converter_0_M_AXI_AWBURST),
        .s_axi_awcache(axi_protocol_converter_0_M_AXI_AWCACHE),
        .s_axi_awid(axi_protocol_converter_0_M_AXI_AWID),
        .s_axi_awlen(axi_protocol_converter_0_M_AXI_AWLEN),
        .s_axi_awlock(axi_protocol_converter_0_M_AXI_AWLOCK),
        .s_axi_awprot(axi_protocol_converter_0_M_AXI_AWPROT),
        .s_axi_awqos(axi_protocol_converter_0_M_AXI_AWQOS),
        .s_axi_awready(axi_protocol_converter_0_M_AXI_AWREADY),
        .s_axi_awsize(axi_protocol_converter_0_M_AXI_AWSIZE),
        .s_axi_awvalid(axi_protocol_converter_0_M_AXI_AWVALID),
        .s_axi_bid(axi_protocol_converter_0_M_AXI_BID),
        .s_axi_bready(axi_protocol_converter_0_M_AXI_BREADY),
        .s_axi_bresp(axi_protocol_converter_0_M_AXI_BRESP),
        .s_axi_bvalid(axi_protocol_converter_0_M_AXI_BVALID),
        .s_axi_rdata(axi_protocol_converter_0_M_AXI_RDATA),
        .s_axi_rid(axi_protocol_converter_0_M_AXI_RID),
        .s_axi_rlast(axi_protocol_converter_0_M_AXI_RLAST),
        .s_axi_rready(axi_protocol_converter_0_M_AXI_RREADY),
        .s_axi_rresp(axi_protocol_converter_0_M_AXI_RRESP),
        .s_axi_rvalid(axi_protocol_converter_0_M_AXI_RVALID),
        .s_axi_wdata(axi_protocol_converter_0_M_AXI_WDATA),
        .s_axi_wlast(axi_protocol_converter_0_M_AXI_WLAST),
        .s_axi_wready(axi_protocol_converter_0_M_AXI_WREADY),
        .s_axi_wstrb(axi_protocol_converter_0_M_AXI_WSTRB),
        .s_axi_wvalid(axi_protocol_converter_0_M_AXI_WVALID));
display_pl_side_axi_protocol_converter_0_0 axi_protocol_converter_0
       (.aclk(processing_system7_0_FCLK_CLK2),
        .aresetn(proc_sys_reset_0_peripheral_aresetn),
        .m_axi_araddr(axi_protocol_converter_0_M_AXI_ARADDR),
        .m_axi_arburst(axi_protocol_converter_0_M_AXI_ARBURST),
        .m_axi_arcache(axi_protocol_converter_0_M_AXI_ARCACHE),
        .m_axi_arid(axi_protocol_converter_0_M_AXI_ARID),
        .m_axi_arlen(axi_protocol_converter_0_M_AXI_ARLEN),
        .m_axi_arlock(axi_protocol_converter_0_M_AXI_ARLOCK),
        .m_axi_arprot(axi_protocol_converter_0_M_AXI_ARPROT),
        .m_axi_arqos(axi_protocol_converter_0_M_AXI_ARQOS),
        .m_axi_arready(axi_protocol_converter_0_M_AXI_ARREADY),
        .m_axi_arsize(axi_protocol_converter_0_M_AXI_ARSIZE),
        .m_axi_arvalid(axi_protocol_converter_0_M_AXI_ARVALID),
        .m_axi_awaddr(axi_protocol_converter_0_M_AXI_AWADDR),
        .m_axi_awburst(axi_protocol_converter_0_M_AXI_AWBURST),
        .m_axi_awcache(axi_protocol_converter_0_M_AXI_AWCACHE),
        .m_axi_awid(axi_protocol_converter_0_M_AXI_AWID),
        .m_axi_awlen(axi_protocol_converter_0_M_AXI_AWLEN),
        .m_axi_awlock(axi_protocol_converter_0_M_AXI_AWLOCK),
        .m_axi_awprot(axi_protocol_converter_0_M_AXI_AWPROT),
        .m_axi_awqos(axi_protocol_converter_0_M_AXI_AWQOS),
        .m_axi_awready(axi_protocol_converter_0_M_AXI_AWREADY),
        .m_axi_awsize(axi_protocol_converter_0_M_AXI_AWSIZE),
        .m_axi_awvalid(axi_protocol_converter_0_M_AXI_AWVALID),
        .m_axi_bid(axi_protocol_converter_0_M_AXI_BID),
        .m_axi_bready(axi_protocol_converter_0_M_AXI_BREADY),
        .m_axi_bresp(axi_protocol_converter_0_M_AXI_BRESP),
        .m_axi_bvalid(axi_protocol_converter_0_M_AXI_BVALID),
        .m_axi_rdata(axi_protocol_converter_0_M_AXI_RDATA),
        .m_axi_rid(axi_protocol_converter_0_M_AXI_RID),
        .m_axi_rlast(axi_protocol_converter_0_M_AXI_RLAST),
        .m_axi_rready(axi_protocol_converter_0_M_AXI_RREADY),
        .m_axi_rresp(axi_protocol_converter_0_M_AXI_RRESP),
        .m_axi_rvalid(axi_protocol_converter_0_M_AXI_RVALID),
        .m_axi_wdata(axi_protocol_converter_0_M_AXI_WDATA),
        .m_axi_wlast(axi_protocol_converter_0_M_AXI_WLAST),
        .m_axi_wready(axi_protocol_converter_0_M_AXI_WREADY),
        .m_axi_wstrb(axi_protocol_converter_0_M_AXI_WSTRB),
        .m_axi_wvalid(axi_protocol_converter_0_M_AXI_WVALID),
        .s_axi_araddr(axi_displaybuffer_handle_0_axi_ARADDR),
        .s_axi_arburst(axi_displaybuffer_handle_0_axi_ARBURST),
        .s_axi_arcache({GND_1,GND_1,GND_1,GND_1}),
        .s_axi_arid(axi_displaybuffer_handle_0_axi_ARID[0]),
        .s_axi_arlen(axi_displaybuffer_handle_0_axi_ARLEN),
        .s_axi_arlock(GND_1),
        .s_axi_arprot({GND_1,GND_1,GND_1}),
        .s_axi_arqos({GND_1,GND_1,GND_1,GND_1}),
        .s_axi_arready(axi_displaybuffer_handle_0_axi_ARREADY),
        .s_axi_arregion({GND_1,GND_1,GND_1,GND_1}),
        .s_axi_arsize(axi_displaybuffer_handle_0_axi_ARSIZE),
        .s_axi_arvalid(axi_displaybuffer_handle_0_axi_ARVALID),
        .s_axi_awaddr(axi_displaybuffer_handle_0_axi_AWADDR),
        .s_axi_awburst(axi_displaybuffer_handle_0_axi_AWBURST),
        .s_axi_awcache({GND_1,GND_1,GND_1,GND_1}),
        .s_axi_awid(axi_displaybuffer_handle_0_axi_AWID[0]),
        .s_axi_awlen(axi_displaybuffer_handle_0_axi_AWLEN),
        .s_axi_awlock(GND_1),
        .s_axi_awprot({GND_1,GND_1,GND_1}),
        .s_axi_awqos({GND_1,GND_1,GND_1,GND_1}),
        .s_axi_awready(axi_displaybuffer_handle_0_axi_AWREADY),
        .s_axi_awregion({GND_1,GND_1,GND_1,GND_1}),
        .s_axi_awsize(axi_displaybuffer_handle_0_axi_AWSIZE),
        .s_axi_awvalid(axi_displaybuffer_handle_0_axi_AWVALID),
        .s_axi_bready(axi_displaybuffer_handle_0_axi_BREADY),
        .s_axi_bresp(axi_displaybuffer_handle_0_axi_BRESP),
        .s_axi_bvalid(axi_displaybuffer_handle_0_axi_BVALID),
        .s_axi_rdata(axi_displaybuffer_handle_0_axi_RDATA),
        .s_axi_rlast(axi_displaybuffer_handle_0_axi_RLAST),
        .s_axi_rready(axi_displaybuffer_handle_0_axi_RREADY),
        .s_axi_rresp(axi_displaybuffer_handle_0_axi_RRESP),
        .s_axi_rvalid(axi_displaybuffer_handle_0_axi_RVALID),
        .s_axi_wdata(axi_displaybuffer_handle_0_axi_WDATA),
        .s_axi_wlast(axi_displaybuffer_handle_0_axi_WLAST),
        .s_axi_wready(axi_displaybuffer_handle_0_axi_WREADY),
        .s_axi_wstrb(axi_displaybuffer_handle_0_axi_WSTRB),
        .s_axi_wvalid(axi_displaybuffer_handle_0_axi_WVALID));
display_pl_side_clk_wiz_0_0 clk_wiz_0
       (.clk_in1(processing_system7_0_FCLK_CLK1),
        .clk_out1(processing_system7_0_FCLK_CLK0));
display_pl_side_dummy_display_fifo_driver_0_0 dummy_display_fifo_driver_0
       (.S00_AXI_ARADDR(dummy_ps_logic_M00_AXI_ARADDR),
        .S00_AXI_ARBURST(dummy_ps_logic_M00_AXI_ARBURST),
        .S00_AXI_ARCACHE(dummy_ps_logic_M00_AXI_ARCACHE),
        .S00_AXI_ARID(dummy_ps_logic_M00_AXI_ARID),
        .S00_AXI_ARLEN(dummy_ps_logic_M00_AXI_ARLEN),
        .S00_AXI_ARLOCK(dummy_ps_logic_M00_AXI_ARLOCK),
        .S00_AXI_ARPROT(dummy_ps_logic_M00_AXI_ARPROT),
        .S00_AXI_ARQOS(dummy_ps_logic_M00_AXI_ARQOS),
        .S00_AXI_ARREADY(dummy_ps_logic_M00_AXI_ARREADY),
        .S00_AXI_ARSIZE(dummy_ps_logic_M00_AXI_ARSIZE),
        .S00_AXI_ARVALID(dummy_ps_logic_M00_AXI_ARVALID),
        .S00_AXI_AWADDR(dummy_ps_logic_M00_AXI_AWADDR),
        .S00_AXI_AWBURST(dummy_ps_logic_M00_AXI_AWBURST),
        .S00_AXI_AWCACHE(dummy_ps_logic_M00_AXI_AWCACHE),
        .S00_AXI_AWID(dummy_ps_logic_M00_AXI_AWID),
        .S00_AXI_AWLEN(dummy_ps_logic_M00_AXI_AWLEN),
        .S00_AXI_AWLOCK(dummy_ps_logic_M00_AXI_AWLOCK),
        .S00_AXI_AWPROT(dummy_ps_logic_M00_AXI_AWPROT),
        .S00_AXI_AWQOS(dummy_ps_logic_M00_AXI_AWQOS),
        .S00_AXI_AWREADY(dummy_ps_logic_M00_AXI_AWREADY),
        .S00_AXI_AWSIZE(dummy_ps_logic_M00_AXI_AWSIZE),
        .S00_AXI_AWVALID(dummy_ps_logic_M00_AXI_AWVALID),
        .S00_AXI_BID(dummy_ps_logic_M00_AXI_BID),
        .S00_AXI_BREADY(dummy_ps_logic_M00_AXI_BREADY),
        .S00_AXI_BRESP(dummy_ps_logic_M00_AXI_BRESP),
        .S00_AXI_BVALID(dummy_ps_logic_M00_AXI_BVALID),
        .S00_AXI_RDATA(dummy_ps_logic_M00_AXI_RDATA),
        .S00_AXI_RID(dummy_ps_logic_M00_AXI_RID),
        .S00_AXI_RLAST(dummy_ps_logic_M00_AXI_RLAST),
        .S00_AXI_RREADY(dummy_ps_logic_M00_AXI_RREADY),
        .S00_AXI_RRESP(dummy_ps_logic_M00_AXI_RRESP),
        .S00_AXI_RVALID(dummy_ps_logic_M00_AXI_RVALID),
        .S00_AXI_WDATA(dummy_ps_logic_M00_AXI_WDATA),
        .S00_AXI_WLAST(dummy_ps_logic_M00_AXI_WLAST),
        .S00_AXI_WREADY(dummy_ps_logic_M00_AXI_WREADY),
        .S00_AXI_WSTRB(dummy_ps_logic_M00_AXI_WSTRB),
        .S00_AXI_WVALID(dummy_ps_logic_M00_AXI_WVALID),
        .clk(processing_system7_0_FCLK_CLK2),
        .fifo_data_out(dummy_display_fifo_driver_0_fifo_data_out),
        .fifo_prog_full_in(fifo_generator_0_almost_full),
        .fifo_wr_en_out(dummy_display_fifo_driver_0_fifo_wr_en_out),
        .pic_height_out(dummy_display_fifo_driver_0_pic_height_out),
        .pic_width_out(dummy_display_fifo_driver_0_pic_width_out),
        .poc_out(dummy_display_fifo_driver_0_poc_out),
        .reset(proc_sys_reset_0_peripheral_reset));
dummy_ps_logic_imp_CO8RTL dummy_ps_logic
       (.ARESETN(proc_sys_reset_0_interconnect_aresetn),
        .M00_AXI_araddr(dummy_ps_logic_M00_AXI_ARADDR),
        .M00_AXI_arburst(dummy_ps_logic_M00_AXI_ARBURST),
        .M00_AXI_arcache(dummy_ps_logic_M00_AXI_ARCACHE),
        .M00_AXI_arid(dummy_ps_logic_M00_AXI_ARID),
        .M00_AXI_arlen(dummy_ps_logic_M00_AXI_ARLEN),
        .M00_AXI_arlock(dummy_ps_logic_M00_AXI_ARLOCK),
        .M00_AXI_arprot(dummy_ps_logic_M00_AXI_ARPROT),
        .M00_AXI_arqos(dummy_ps_logic_M00_AXI_ARQOS),
        .M00_AXI_arready(dummy_ps_logic_M00_AXI_ARREADY),
        .M00_AXI_arsize(dummy_ps_logic_M00_AXI_ARSIZE),
        .M00_AXI_arvalid(dummy_ps_logic_M00_AXI_ARVALID),
        .M00_AXI_awaddr(dummy_ps_logic_M00_AXI_AWADDR),
        .M00_AXI_awburst(dummy_ps_logic_M00_AXI_AWBURST),
        .M00_AXI_awcache(dummy_ps_logic_M00_AXI_AWCACHE),
        .M00_AXI_awid(dummy_ps_logic_M00_AXI_AWID),
        .M00_AXI_awlen(dummy_ps_logic_M00_AXI_AWLEN),
        .M00_AXI_awlock(dummy_ps_logic_M00_AXI_AWLOCK),
        .M00_AXI_awprot(dummy_ps_logic_M00_AXI_AWPROT),
        .M00_AXI_awqos(dummy_ps_logic_M00_AXI_AWQOS),
        .M00_AXI_awready(dummy_ps_logic_M00_AXI_AWREADY),
        .M00_AXI_awsize(dummy_ps_logic_M00_AXI_AWSIZE),
        .M00_AXI_awvalid(dummy_ps_logic_M00_AXI_AWVALID),
        .M00_AXI_bid(dummy_ps_logic_M00_AXI_BID),
        .M00_AXI_bready(dummy_ps_logic_M00_AXI_BREADY),
        .M00_AXI_bresp(dummy_ps_logic_M00_AXI_BRESP),
        .M00_AXI_bvalid(dummy_ps_logic_M00_AXI_BVALID),
        .M00_AXI_rdata(dummy_ps_logic_M00_AXI_RDATA),
        .M00_AXI_rid(dummy_ps_logic_M00_AXI_RID),
        .M00_AXI_rlast(dummy_ps_logic_M00_AXI_RLAST),
        .M00_AXI_rready(dummy_ps_logic_M00_AXI_RREADY),
        .M00_AXI_rresp(dummy_ps_logic_M00_AXI_RRESP),
        .M00_AXI_rvalid(dummy_ps_logic_M00_AXI_RVALID),
        .M00_AXI_wdata(dummy_ps_logic_M00_AXI_WDATA),
        .M00_AXI_wlast(dummy_ps_logic_M00_AXI_WLAST),
        .M00_AXI_wready(dummy_ps_logic_M00_AXI_WREADY),
        .M00_AXI_wstrb(dummy_ps_logic_M00_AXI_WSTRB),
        .M00_AXI_wvalid(dummy_ps_logic_M00_AXI_WVALID),
        .S00_ACLK(processing_system7_0_FCLK_CLK2),
        .S00_ARESETN(proc_sys_reset_0_peripheral_aresetn),
        .S00_AXI_araddr(processing_system7_0_M_AXI_GP0_ARADDR),
        .S00_AXI_arburst(processing_system7_0_M_AXI_GP0_ARBURST),
        .S00_AXI_arcache(processing_system7_0_M_AXI_GP0_ARCACHE),
        .S00_AXI_arid(processing_system7_0_M_AXI_GP0_ARID),
        .S00_AXI_arlen(processing_system7_0_M_AXI_GP0_ARLEN),
        .S00_AXI_arlock(processing_system7_0_M_AXI_GP0_ARLOCK),
        .S00_AXI_arprot(processing_system7_0_M_AXI_GP0_ARPROT),
        .S00_AXI_arqos(processing_system7_0_M_AXI_GP0_ARQOS),
        .S00_AXI_arready(processing_system7_0_M_AXI_GP0_ARREADY),
        .S00_AXI_arsize(processing_system7_0_M_AXI_GP0_ARSIZE),
        .S00_AXI_arvalid(processing_system7_0_M_AXI_GP0_ARVALID),
        .S00_AXI_awaddr(processing_system7_0_M_AXI_GP0_AWADDR),
        .S00_AXI_awburst(processing_system7_0_M_AXI_GP0_AWBURST),
        .S00_AXI_awcache(processing_system7_0_M_AXI_GP0_AWCACHE),
        .S00_AXI_awid(processing_system7_0_M_AXI_GP0_AWID),
        .S00_AXI_awlen(processing_system7_0_M_AXI_GP0_AWLEN),
        .S00_AXI_awlock(processing_system7_0_M_AXI_GP0_AWLOCK),
        .S00_AXI_awprot(processing_system7_0_M_AXI_GP0_AWPROT),
        .S00_AXI_awqos(processing_system7_0_M_AXI_GP0_AWQOS),
        .S00_AXI_awready(processing_system7_0_M_AXI_GP0_AWREADY),
        .S00_AXI_awsize(processing_system7_0_M_AXI_GP0_AWSIZE),
        .S00_AXI_awvalid(processing_system7_0_M_AXI_GP0_AWVALID),
        .S00_AXI_bid(processing_system7_0_M_AXI_GP0_BID),
        .S00_AXI_bready(processing_system7_0_M_AXI_GP0_BREADY),
        .S00_AXI_bresp(processing_system7_0_M_AXI_GP0_BRESP),
        .S00_AXI_bvalid(processing_system7_0_M_AXI_GP0_BVALID),
        .S00_AXI_rdata(processing_system7_0_M_AXI_GP0_RDATA),
        .S00_AXI_rid(processing_system7_0_M_AXI_GP0_RID),
        .S00_AXI_rlast(processing_system7_0_M_AXI_GP0_RLAST),
        .S00_AXI_rready(processing_system7_0_M_AXI_GP0_RREADY),
        .S00_AXI_rresp(processing_system7_0_M_AXI_GP0_RRESP),
        .S00_AXI_rvalid(processing_system7_0_M_AXI_GP0_RVALID),
        .S00_AXI_wdata(processing_system7_0_M_AXI_GP0_WDATA),
        .S00_AXI_wid(processing_system7_0_M_AXI_GP0_WID),
        .S00_AXI_wlast(processing_system7_0_M_AXI_GP0_WLAST),
        .S00_AXI_wready(processing_system7_0_M_AXI_GP0_WREADY),
        .S00_AXI_wstrb(processing_system7_0_M_AXI_GP0_WSTRB),
        .S00_AXI_wvalid(processing_system7_0_M_AXI_GP0_WVALID));
display_pl_side_fifo_generator_0_0 fifo_generator_0
       (.almost_full(fifo_generator_0_almost_full),
        .clk(processing_system7_0_FCLK_CLK2),
        .din(dummy_display_fifo_driver_0_fifo_data_out),
        .dout(fifo_generator_0_dout),
        .empty(fifo_generator_0_empty),
        .rd_en(axi_displaybuffer_handle_0_displaybuffer_fifo_read_en_out),
        .rst(proc_sys_reset_0_peripheral_reset),
        .wr_en(dummy_display_fifo_driver_0_fifo_wr_en_out));
display_pl_side_fifo_generator_1_0 fifo_generator_1
       (.din(axi_displaybuffer_handle_0_hdmi_fifo_data_out),
        .dout(fifo_generator_1_dout),
        .empty(fifo_generator_1_empty),
        .prog_empty(fifo_generator_1_prog_empty),
        .prog_full(fifo_generator_1_prog_full),
        .rd_clk(processing_system7_0_FCLK_CLK0),
        .rd_data_count(rd_data_count),
        .rd_en(video_tx_top_0_fifo_rd_en),
        .rst(proc_sys_reset_0_peripheral_reset),
        .wr_clk(processing_system7_0_FCLK_CLK2),
        .wr_data_count(wr_data_count),
        .wr_en(axi_displaybuffer_handle_0_hdmi_fifo_w_en));
display_pl_side_proc_sys_reset_0_1 proc_sys_reset_0
       (.aux_reset_in(VCC_1),
        .dcm_locked(VCC_1),
        .ext_reset_in(processing_system7_0_FCLK_RESET0_N),
        .interconnect_aresetn(proc_sys_reset_0_interconnect_aresetn),
        .mb_debug_sys_rst(GND_1),
        .peripheral_aresetn(proc_sys_reset_0_peripheral_aresetn),
        .peripheral_reset(proc_sys_reset_0_peripheral_reset),
        .slowest_sync_clk(processing_system7_0_FCLK_CLK2));
display_pl_side_processing_system7_0_0 processing_system7_0
       (.DDR_Addr(DDR_addr[14:0]),
        .DDR_BankAddr(DDR_ba[2:0]),
        .DDR_CAS_n(DDR_cas_n),
        .DDR_CKE(DDR_cke),
        .DDR_CS_n(DDR_cs_n),
        .DDR_Clk(DDR_ck_p),
        .DDR_Clk_n(DDR_ck_n),
        .DDR_DM(DDR_dm[3:0]),
        .DDR_DQ(DDR_dq[31:0]),
        .DDR_DQS(DDR_dqs_p[3:0]),
        .DDR_DQS_n(DDR_dqs_n[3:0]),
        .DDR_DRSTB(DDR_reset_n),
        .DDR_ODT(DDR_odt),
        .DDR_RAS_n(DDR_ras_n),
        .DDR_VRN(FIXED_IO_ddr_vrn),
        .DDR_VRP(FIXED_IO_ddr_vrp),
        .DDR_WEB(DDR_we_n),
        .FCLK_CLK0(processing_system7_0_FCLK_CLK1),
        .FCLK_CLK1(processing_system7_0_FCLK_CLK2),
        .FCLK_RESET0_N(processing_system7_0_FCLK_RESET0_N),
        .MIO(FIXED_IO_mio[53:0]),
        .M_AXI_GP0_ACLK(processing_system7_0_FCLK_CLK2),
        .M_AXI_GP0_ARADDR(processing_system7_0_M_AXI_GP0_ARADDR),
        .M_AXI_GP0_ARBURST(processing_system7_0_M_AXI_GP0_ARBURST),
        .M_AXI_GP0_ARCACHE(processing_system7_0_M_AXI_GP0_ARCACHE),
        .M_AXI_GP0_ARID(processing_system7_0_M_AXI_GP0_ARID),
        .M_AXI_GP0_ARLEN(processing_system7_0_M_AXI_GP0_ARLEN),
        .M_AXI_GP0_ARLOCK(processing_system7_0_M_AXI_GP0_ARLOCK),
        .M_AXI_GP0_ARPROT(processing_system7_0_M_AXI_GP0_ARPROT),
        .M_AXI_GP0_ARQOS(processing_system7_0_M_AXI_GP0_ARQOS),
        .M_AXI_GP0_ARREADY(processing_system7_0_M_AXI_GP0_ARREADY),
        .M_AXI_GP0_ARSIZE(processing_system7_0_M_AXI_GP0_ARSIZE),
        .M_AXI_GP0_ARVALID(processing_system7_0_M_AXI_GP0_ARVALID),
        .M_AXI_GP0_AWADDR(processing_system7_0_M_AXI_GP0_AWADDR),
        .M_AXI_GP0_AWBURST(processing_system7_0_M_AXI_GP0_AWBURST),
        .M_AXI_GP0_AWCACHE(processing_system7_0_M_AXI_GP0_AWCACHE),
        .M_AXI_GP0_AWID(processing_system7_0_M_AXI_GP0_AWID),
        .M_AXI_GP0_AWLEN(processing_system7_0_M_AXI_GP0_AWLEN),
        .M_AXI_GP0_AWLOCK(processing_system7_0_M_AXI_GP0_AWLOCK),
        .M_AXI_GP0_AWPROT(processing_system7_0_M_AXI_GP0_AWPROT),
        .M_AXI_GP0_AWQOS(processing_system7_0_M_AXI_GP0_AWQOS),
        .M_AXI_GP0_AWREADY(processing_system7_0_M_AXI_GP0_AWREADY),
        .M_AXI_GP0_AWSIZE(processing_system7_0_M_AXI_GP0_AWSIZE),
        .M_AXI_GP0_AWVALID(processing_system7_0_M_AXI_GP0_AWVALID),
        .M_AXI_GP0_BID(processing_system7_0_M_AXI_GP0_BID),
        .M_AXI_GP0_BREADY(processing_system7_0_M_AXI_GP0_BREADY),
        .M_AXI_GP0_BRESP(processing_system7_0_M_AXI_GP0_BRESP),
        .M_AXI_GP0_BVALID(processing_system7_0_M_AXI_GP0_BVALID),
        .M_AXI_GP0_RDATA(processing_system7_0_M_AXI_GP0_RDATA),
        .M_AXI_GP0_RID(processing_system7_0_M_AXI_GP0_RID),
        .M_AXI_GP0_RLAST(processing_system7_0_M_AXI_GP0_RLAST),
        .M_AXI_GP0_RREADY(processing_system7_0_M_AXI_GP0_RREADY),
        .M_AXI_GP0_RRESP(processing_system7_0_M_AXI_GP0_RRESP),
        .M_AXI_GP0_RVALID(processing_system7_0_M_AXI_GP0_RVALID),
        .M_AXI_GP0_WDATA(processing_system7_0_M_AXI_GP0_WDATA),
        .M_AXI_GP0_WID(processing_system7_0_M_AXI_GP0_WID),
        .M_AXI_GP0_WLAST(processing_system7_0_M_AXI_GP0_WLAST),
        .M_AXI_GP0_WREADY(processing_system7_0_M_AXI_GP0_WREADY),
        .M_AXI_GP0_WSTRB(processing_system7_0_M_AXI_GP0_WSTRB),
        .M_AXI_GP0_WVALID(processing_system7_0_M_AXI_GP0_WVALID),
        .PS_CLK(FIXED_IO_ps_clk),
        .PS_PORB(FIXED_IO_ps_porb),
        .PS_SRSTB(FIXED_IO_ps_srstb),
        .S_AXI_HP0_ACLK(processing_system7_0_FCLK_CLK2),
        .S_AXI_HP0_ARADDR(axi_dwidth_converter_0_M_AXI_ARADDR),
        .S_AXI_HP0_ARBURST(axi_dwidth_converter_0_M_AXI_ARBURST),
        .S_AXI_HP0_ARCACHE(axi_dwidth_converter_0_M_AXI_ARCACHE),
        .S_AXI_HP0_ARID({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_HP0_ARLEN(axi_dwidth_converter_0_M_AXI_ARLEN),
        .S_AXI_HP0_ARLOCK(axi_dwidth_converter_0_M_AXI_ARLOCK),
        .S_AXI_HP0_ARPROT(axi_dwidth_converter_0_M_AXI_ARPROT),
        .S_AXI_HP0_ARQOS(axi_dwidth_converter_0_M_AXI_ARQOS),
        .S_AXI_HP0_ARREADY(axi_dwidth_converter_0_M_AXI_ARREADY),
        .S_AXI_HP0_ARSIZE(axi_dwidth_converter_0_M_AXI_ARSIZE),
        .S_AXI_HP0_ARVALID(axi_dwidth_converter_0_M_AXI_ARVALID),
        .S_AXI_HP0_AWADDR(axi_dwidth_converter_0_M_AXI_AWADDR),
        .S_AXI_HP0_AWBURST(axi_dwidth_converter_0_M_AXI_AWBURST),
        .S_AXI_HP0_AWCACHE(axi_dwidth_converter_0_M_AXI_AWCACHE),
        .S_AXI_HP0_AWID({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_HP0_AWLEN(axi_dwidth_converter_0_M_AXI_AWLEN),
        .S_AXI_HP0_AWLOCK(axi_dwidth_converter_0_M_AXI_AWLOCK),
        .S_AXI_HP0_AWPROT(axi_dwidth_converter_0_M_AXI_AWPROT),
        .S_AXI_HP0_AWQOS(axi_dwidth_converter_0_M_AXI_AWQOS),
        .S_AXI_HP0_AWREADY(axi_dwidth_converter_0_M_AXI_AWREADY),
        .S_AXI_HP0_AWSIZE(axi_dwidth_converter_0_M_AXI_AWSIZE),
        .S_AXI_HP0_AWVALID(axi_dwidth_converter_0_M_AXI_AWVALID),
        .S_AXI_HP0_BREADY(axi_dwidth_converter_0_M_AXI_BREADY),
        .S_AXI_HP0_BRESP(axi_dwidth_converter_0_M_AXI_BRESP),
        .S_AXI_HP0_BVALID(axi_dwidth_converter_0_M_AXI_BVALID),
        .S_AXI_HP0_RDATA(axi_dwidth_converter_0_M_AXI_RDATA),
        .S_AXI_HP0_RDISSUECAP1_EN(GND_1),
        .S_AXI_HP0_RLAST(axi_dwidth_converter_0_M_AXI_RLAST),
        .S_AXI_HP0_RREADY(axi_dwidth_converter_0_M_AXI_RREADY),
        .S_AXI_HP0_RRESP(axi_dwidth_converter_0_M_AXI_RRESP),
        .S_AXI_HP0_RVALID(axi_dwidth_converter_0_M_AXI_RVALID),
        .S_AXI_HP0_WDATA(axi_dwidth_converter_0_M_AXI_WDATA),
        .S_AXI_HP0_WID({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_HP0_WLAST(axi_dwidth_converter_0_M_AXI_WLAST),
        .S_AXI_HP0_WREADY(axi_dwidth_converter_0_M_AXI_WREADY),
        .S_AXI_HP0_WRISSUECAP1_EN(GND_1),
        .S_AXI_HP0_WSTRB(axi_dwidth_converter_0_M_AXI_WSTRB),
        .S_AXI_HP0_WVALID(axi_dwidth_converter_0_M_AXI_WVALID),
        .USB0_VBUS_PWRFAULT(GND_1));
display_pl_side_video_tx_top_0_0 video_tx_top_0
       (.DE(video_tx_top_0_DE),
        .Data_out(video_tx_top_0_Data_out),
        .clk(processing_system7_0_FCLK_CLK0),
        .fifo_data_in(fifo_generator_1_dout),
        .fifo_empty(fifo_generator_1_empty),
        .fifo_prog_empty(fifo_generator_1_prog_empty),
        .fifo_rd_en(video_tx_top_0_fifo_rd_en),
        .hsync(video_tx_top_0_hsync),
        .pclk(video_tx_top_0_pclk),
        .reset(proc_sys_reset_0_peripheral_reset),
        .scl_pad(scl_pad),
        .sda_pad(sda_pad),
        .state_out(state_out),
        .vsync(video_tx_top_0_vsync));
endmodule

module display_pl_side_axi_mem_intercon_1_0
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arburst,
    M00_AXI_arcache,
    M00_AXI_arid,
    M00_AXI_arlen,
    M00_AXI_arlock,
    M00_AXI_arprot,
    M00_AXI_arqos,
    M00_AXI_arready,
    M00_AXI_arsize,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awburst,
    M00_AXI_awcache,
    M00_AXI_awid,
    M00_AXI_awlen,
    M00_AXI_awlock,
    M00_AXI_awprot,
    M00_AXI_awqos,
    M00_AXI_awready,
    M00_AXI_awsize,
    M00_AXI_awvalid,
    M00_AXI_bid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rid,
    M00_AXI_rlast,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wlast,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arburst,
    S00_AXI_arcache,
    S00_AXI_arid,
    S00_AXI_arlen,
    S00_AXI_arlock,
    S00_AXI_arprot,
    S00_AXI_arqos,
    S00_AXI_arready,
    S00_AXI_arsize,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awburst,
    S00_AXI_awcache,
    S00_AXI_awid,
    S00_AXI_awlen,
    S00_AXI_awlock,
    S00_AXI_awprot,
    S00_AXI_awqos,
    S00_AXI_awready,
    S00_AXI_awsize,
    S00_AXI_awvalid,
    S00_AXI_bid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rid,
    S00_AXI_rlast,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wid,
    S00_AXI_wlast,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid);
  input ACLK;
  input [0:0]ARESETN;
  input M00_ACLK;
  input [0:0]M00_ARESETN;
  output [15:0]M00_AXI_araddr;
  output [1:0]M00_AXI_arburst;
  output [3:0]M00_AXI_arcache;
  output [11:0]M00_AXI_arid;
  output [7:0]M00_AXI_arlen;
  output M00_AXI_arlock;
  output [2:0]M00_AXI_arprot;
  output [3:0]M00_AXI_arqos;
  input M00_AXI_arready;
  output [2:0]M00_AXI_arsize;
  output M00_AXI_arvalid;
  output [15:0]M00_AXI_awaddr;
  output [1:0]M00_AXI_awburst;
  output [3:0]M00_AXI_awcache;
  output [11:0]M00_AXI_awid;
  output [7:0]M00_AXI_awlen;
  output M00_AXI_awlock;
  output [2:0]M00_AXI_awprot;
  output [3:0]M00_AXI_awqos;
  input M00_AXI_awready;
  output [2:0]M00_AXI_awsize;
  output M00_AXI_awvalid;
  input [11:0]M00_AXI_bid;
  output M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  input [11:0]M00_AXI_rid;
  input M00_AXI_rlast;
  output M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  output M00_AXI_wlast;
  input M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output M00_AXI_wvalid;
  input S00_ACLK;
  input [0:0]S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [1:0]S00_AXI_arburst;
  input [3:0]S00_AXI_arcache;
  input [11:0]S00_AXI_arid;
  input [3:0]S00_AXI_arlen;
  input [1:0]S00_AXI_arlock;
  input [2:0]S00_AXI_arprot;
  input [3:0]S00_AXI_arqos;
  output S00_AXI_arready;
  input [2:0]S00_AXI_arsize;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [1:0]S00_AXI_awburst;
  input [3:0]S00_AXI_awcache;
  input [11:0]S00_AXI_awid;
  input [3:0]S00_AXI_awlen;
  input [1:0]S00_AXI_awlock;
  input [2:0]S00_AXI_awprot;
  input [3:0]S00_AXI_awqos;
  output S00_AXI_awready;
  input [2:0]S00_AXI_awsize;
  input S00_AXI_awvalid;
  output [11:0]S00_AXI_bid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  output [11:0]S00_AXI_rid;
  output S00_AXI_rlast;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  input [11:0]S00_AXI_wid;
  input S00_AXI_wlast;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;

  wire S00_ACLK_1;
  wire [0:0]S00_ARESETN_1;
  wire axi_mem_intercon_1_ACLK_net;
  wire [0:0]axi_mem_intercon_1_ARESETN_net;
  wire [31:0]axi_mem_intercon_1_to_s00_couplers_ARADDR;
  wire [1:0]axi_mem_intercon_1_to_s00_couplers_ARBURST;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_ARCACHE;
  wire [11:0]axi_mem_intercon_1_to_s00_couplers_ARID;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_ARLEN;
  wire [1:0]axi_mem_intercon_1_to_s00_couplers_ARLOCK;
  wire [2:0]axi_mem_intercon_1_to_s00_couplers_ARPROT;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_ARQOS;
  wire axi_mem_intercon_1_to_s00_couplers_ARREADY;
  wire [2:0]axi_mem_intercon_1_to_s00_couplers_ARSIZE;
  wire axi_mem_intercon_1_to_s00_couplers_ARVALID;
  wire [31:0]axi_mem_intercon_1_to_s00_couplers_AWADDR;
  wire [1:0]axi_mem_intercon_1_to_s00_couplers_AWBURST;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_AWCACHE;
  wire [11:0]axi_mem_intercon_1_to_s00_couplers_AWID;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_AWLEN;
  wire [1:0]axi_mem_intercon_1_to_s00_couplers_AWLOCK;
  wire [2:0]axi_mem_intercon_1_to_s00_couplers_AWPROT;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_AWQOS;
  wire axi_mem_intercon_1_to_s00_couplers_AWREADY;
  wire [2:0]axi_mem_intercon_1_to_s00_couplers_AWSIZE;
  wire axi_mem_intercon_1_to_s00_couplers_AWVALID;
  wire [11:0]axi_mem_intercon_1_to_s00_couplers_BID;
  wire axi_mem_intercon_1_to_s00_couplers_BREADY;
  wire [1:0]axi_mem_intercon_1_to_s00_couplers_BRESP;
  wire axi_mem_intercon_1_to_s00_couplers_BVALID;
  wire [31:0]axi_mem_intercon_1_to_s00_couplers_RDATA;
  wire [11:0]axi_mem_intercon_1_to_s00_couplers_RID;
  wire axi_mem_intercon_1_to_s00_couplers_RLAST;
  wire axi_mem_intercon_1_to_s00_couplers_RREADY;
  wire [1:0]axi_mem_intercon_1_to_s00_couplers_RRESP;
  wire axi_mem_intercon_1_to_s00_couplers_RVALID;
  wire [31:0]axi_mem_intercon_1_to_s00_couplers_WDATA;
  wire [11:0]axi_mem_intercon_1_to_s00_couplers_WID;
  wire axi_mem_intercon_1_to_s00_couplers_WLAST;
  wire axi_mem_intercon_1_to_s00_couplers_WREADY;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_WSTRB;
  wire axi_mem_intercon_1_to_s00_couplers_WVALID;
  wire [15:0]s00_couplers_to_axi_mem_intercon_1_ARADDR;
  wire [1:0]s00_couplers_to_axi_mem_intercon_1_ARBURST;
  wire [3:0]s00_couplers_to_axi_mem_intercon_1_ARCACHE;
  wire [11:0]s00_couplers_to_axi_mem_intercon_1_ARID;
  wire [7:0]s00_couplers_to_axi_mem_intercon_1_ARLEN;
  wire s00_couplers_to_axi_mem_intercon_1_ARLOCK;
  wire [2:0]s00_couplers_to_axi_mem_intercon_1_ARPROT;
  wire [3:0]s00_couplers_to_axi_mem_intercon_1_ARQOS;
  wire s00_couplers_to_axi_mem_intercon_1_ARREADY;
  wire [2:0]s00_couplers_to_axi_mem_intercon_1_ARSIZE;
  wire s00_couplers_to_axi_mem_intercon_1_ARVALID;
  wire [15:0]s00_couplers_to_axi_mem_intercon_1_AWADDR;
  wire [1:0]s00_couplers_to_axi_mem_intercon_1_AWBURST;
  wire [3:0]s00_couplers_to_axi_mem_intercon_1_AWCACHE;
  wire [11:0]s00_couplers_to_axi_mem_intercon_1_AWID;
  wire [7:0]s00_couplers_to_axi_mem_intercon_1_AWLEN;
  wire s00_couplers_to_axi_mem_intercon_1_AWLOCK;
  wire [2:0]s00_couplers_to_axi_mem_intercon_1_AWPROT;
  wire [3:0]s00_couplers_to_axi_mem_intercon_1_AWQOS;
  wire s00_couplers_to_axi_mem_intercon_1_AWREADY;
  wire [2:0]s00_couplers_to_axi_mem_intercon_1_AWSIZE;
  wire s00_couplers_to_axi_mem_intercon_1_AWVALID;
  wire [11:0]s00_couplers_to_axi_mem_intercon_1_BID;
  wire s00_couplers_to_axi_mem_intercon_1_BREADY;
  wire [1:0]s00_couplers_to_axi_mem_intercon_1_BRESP;
  wire s00_couplers_to_axi_mem_intercon_1_BVALID;
  wire [31:0]s00_couplers_to_axi_mem_intercon_1_RDATA;
  wire [11:0]s00_couplers_to_axi_mem_intercon_1_RID;
  wire s00_couplers_to_axi_mem_intercon_1_RLAST;
  wire s00_couplers_to_axi_mem_intercon_1_RREADY;
  wire [1:0]s00_couplers_to_axi_mem_intercon_1_RRESP;
  wire s00_couplers_to_axi_mem_intercon_1_RVALID;
  wire [31:0]s00_couplers_to_axi_mem_intercon_1_WDATA;
  wire s00_couplers_to_axi_mem_intercon_1_WLAST;
  wire s00_couplers_to_axi_mem_intercon_1_WREADY;
  wire [3:0]s00_couplers_to_axi_mem_intercon_1_WSTRB;
  wire s00_couplers_to_axi_mem_intercon_1_WVALID;

  assign M00_AXI_araddr[15:0] = s00_couplers_to_axi_mem_intercon_1_ARADDR;
  assign M00_AXI_arburst[1:0] = s00_couplers_to_axi_mem_intercon_1_ARBURST;
  assign M00_AXI_arcache[3:0] = s00_couplers_to_axi_mem_intercon_1_ARCACHE;
  assign M00_AXI_arid[11:0] = s00_couplers_to_axi_mem_intercon_1_ARID;
  assign M00_AXI_arlen[7:0] = s00_couplers_to_axi_mem_intercon_1_ARLEN;
  assign M00_AXI_arlock = s00_couplers_to_axi_mem_intercon_1_ARLOCK;
  assign M00_AXI_arprot[2:0] = s00_couplers_to_axi_mem_intercon_1_ARPROT;
  assign M00_AXI_arqos[3:0] = s00_couplers_to_axi_mem_intercon_1_ARQOS;
  assign M00_AXI_arsize[2:0] = s00_couplers_to_axi_mem_intercon_1_ARSIZE;
  assign M00_AXI_arvalid = s00_couplers_to_axi_mem_intercon_1_ARVALID;
  assign M00_AXI_awaddr[15:0] = s00_couplers_to_axi_mem_intercon_1_AWADDR;
  assign M00_AXI_awburst[1:0] = s00_couplers_to_axi_mem_intercon_1_AWBURST;
  assign M00_AXI_awcache[3:0] = s00_couplers_to_axi_mem_intercon_1_AWCACHE;
  assign M00_AXI_awid[11:0] = s00_couplers_to_axi_mem_intercon_1_AWID;
  assign M00_AXI_awlen[7:0] = s00_couplers_to_axi_mem_intercon_1_AWLEN;
  assign M00_AXI_awlock = s00_couplers_to_axi_mem_intercon_1_AWLOCK;
  assign M00_AXI_awprot[2:0] = s00_couplers_to_axi_mem_intercon_1_AWPROT;
  assign M00_AXI_awqos[3:0] = s00_couplers_to_axi_mem_intercon_1_AWQOS;
  assign M00_AXI_awsize[2:0] = s00_couplers_to_axi_mem_intercon_1_AWSIZE;
  assign M00_AXI_awvalid = s00_couplers_to_axi_mem_intercon_1_AWVALID;
  assign M00_AXI_bready = s00_couplers_to_axi_mem_intercon_1_BREADY;
  assign M00_AXI_rready = s00_couplers_to_axi_mem_intercon_1_RREADY;
  assign M00_AXI_wdata[31:0] = s00_couplers_to_axi_mem_intercon_1_WDATA;
  assign M00_AXI_wlast = s00_couplers_to_axi_mem_intercon_1_WLAST;
  assign M00_AXI_wstrb[3:0] = s00_couplers_to_axi_mem_intercon_1_WSTRB;
  assign M00_AXI_wvalid = s00_couplers_to_axi_mem_intercon_1_WVALID;
  assign S00_ACLK_1 = S00_ACLK;
  assign S00_ARESETN_1 = S00_ARESETN[0];
  assign S00_AXI_arready = axi_mem_intercon_1_to_s00_couplers_ARREADY;
  assign S00_AXI_awready = axi_mem_intercon_1_to_s00_couplers_AWREADY;
  assign S00_AXI_bid[11:0] = axi_mem_intercon_1_to_s00_couplers_BID;
  assign S00_AXI_bresp[1:0] = axi_mem_intercon_1_to_s00_couplers_BRESP;
  assign S00_AXI_bvalid = axi_mem_intercon_1_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = axi_mem_intercon_1_to_s00_couplers_RDATA;
  assign S00_AXI_rid[11:0] = axi_mem_intercon_1_to_s00_couplers_RID;
  assign S00_AXI_rlast = axi_mem_intercon_1_to_s00_couplers_RLAST;
  assign S00_AXI_rresp[1:0] = axi_mem_intercon_1_to_s00_couplers_RRESP;
  assign S00_AXI_rvalid = axi_mem_intercon_1_to_s00_couplers_RVALID;
  assign S00_AXI_wready = axi_mem_intercon_1_to_s00_couplers_WREADY;
  assign axi_mem_intercon_1_ACLK_net = M00_ACLK;
  assign axi_mem_intercon_1_ARESETN_net = M00_ARESETN[0];
  assign axi_mem_intercon_1_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARBURST = S00_AXI_arburst[1:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARCACHE = S00_AXI_arcache[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARID = S00_AXI_arid[11:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARLEN = S00_AXI_arlen[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARLOCK = S00_AXI_arlock[1:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARQOS = S00_AXI_arqos[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARSIZE = S00_AXI_arsize[2:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARVALID = S00_AXI_arvalid;
  assign axi_mem_intercon_1_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWBURST = S00_AXI_awburst[1:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWCACHE = S00_AXI_awcache[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWID = S00_AXI_awid[11:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWLEN = S00_AXI_awlen[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWLOCK = S00_AXI_awlock[1:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWQOS = S00_AXI_awqos[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWSIZE = S00_AXI_awsize[2:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWVALID = S00_AXI_awvalid;
  assign axi_mem_intercon_1_to_s00_couplers_BREADY = S00_AXI_bready;
  assign axi_mem_intercon_1_to_s00_couplers_RREADY = S00_AXI_rready;
  assign axi_mem_intercon_1_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign axi_mem_intercon_1_to_s00_couplers_WID = S00_AXI_wid[11:0];
  assign axi_mem_intercon_1_to_s00_couplers_WLAST = S00_AXI_wlast;
  assign axi_mem_intercon_1_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_WVALID = S00_AXI_wvalid;
  assign s00_couplers_to_axi_mem_intercon_1_ARREADY = M00_AXI_arready;
  assign s00_couplers_to_axi_mem_intercon_1_AWREADY = M00_AXI_awready;
  assign s00_couplers_to_axi_mem_intercon_1_BID = M00_AXI_bid[11:0];
  assign s00_couplers_to_axi_mem_intercon_1_BRESP = M00_AXI_bresp[1:0];
  assign s00_couplers_to_axi_mem_intercon_1_BVALID = M00_AXI_bvalid;
  assign s00_couplers_to_axi_mem_intercon_1_RDATA = M00_AXI_rdata[31:0];
  assign s00_couplers_to_axi_mem_intercon_1_RID = M00_AXI_rid[11:0];
  assign s00_couplers_to_axi_mem_intercon_1_RLAST = M00_AXI_rlast;
  assign s00_couplers_to_axi_mem_intercon_1_RRESP = M00_AXI_rresp[1:0];
  assign s00_couplers_to_axi_mem_intercon_1_RVALID = M00_AXI_rvalid;
  assign s00_couplers_to_axi_mem_intercon_1_WREADY = M00_AXI_wready;
s00_couplers_imp_1GWH4PS s00_couplers
       (.M_ACLK(axi_mem_intercon_1_ACLK_net),
        .M_ARESETN(axi_mem_intercon_1_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_axi_mem_intercon_1_ARADDR),
        .M_AXI_arburst(s00_couplers_to_axi_mem_intercon_1_ARBURST),
        .M_AXI_arcache(s00_couplers_to_axi_mem_intercon_1_ARCACHE),
        .M_AXI_arid(s00_couplers_to_axi_mem_intercon_1_ARID),
        .M_AXI_arlen(s00_couplers_to_axi_mem_intercon_1_ARLEN),
        .M_AXI_arlock(s00_couplers_to_axi_mem_intercon_1_ARLOCK),
        .M_AXI_arprot(s00_couplers_to_axi_mem_intercon_1_ARPROT),
        .M_AXI_arqos(s00_couplers_to_axi_mem_intercon_1_ARQOS),
        .M_AXI_arready(s00_couplers_to_axi_mem_intercon_1_ARREADY),
        .M_AXI_arsize(s00_couplers_to_axi_mem_intercon_1_ARSIZE),
        .M_AXI_arvalid(s00_couplers_to_axi_mem_intercon_1_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_axi_mem_intercon_1_AWADDR),
        .M_AXI_awburst(s00_couplers_to_axi_mem_intercon_1_AWBURST),
        .M_AXI_awcache(s00_couplers_to_axi_mem_intercon_1_AWCACHE),
        .M_AXI_awid(s00_couplers_to_axi_mem_intercon_1_AWID),
        .M_AXI_awlen(s00_couplers_to_axi_mem_intercon_1_AWLEN),
        .M_AXI_awlock(s00_couplers_to_axi_mem_intercon_1_AWLOCK),
        .M_AXI_awprot(s00_couplers_to_axi_mem_intercon_1_AWPROT),
        .M_AXI_awqos(s00_couplers_to_axi_mem_intercon_1_AWQOS),
        .M_AXI_awready(s00_couplers_to_axi_mem_intercon_1_AWREADY),
        .M_AXI_awsize(s00_couplers_to_axi_mem_intercon_1_AWSIZE),
        .M_AXI_awvalid(s00_couplers_to_axi_mem_intercon_1_AWVALID),
        .M_AXI_bid(s00_couplers_to_axi_mem_intercon_1_BID),
        .M_AXI_bready(s00_couplers_to_axi_mem_intercon_1_BREADY),
        .M_AXI_bresp(s00_couplers_to_axi_mem_intercon_1_BRESP),
        .M_AXI_bvalid(s00_couplers_to_axi_mem_intercon_1_BVALID),
        .M_AXI_rdata(s00_couplers_to_axi_mem_intercon_1_RDATA),
        .M_AXI_rid(s00_couplers_to_axi_mem_intercon_1_RID),
        .M_AXI_rlast(s00_couplers_to_axi_mem_intercon_1_RLAST),
        .M_AXI_rready(s00_couplers_to_axi_mem_intercon_1_RREADY),
        .M_AXI_rresp(s00_couplers_to_axi_mem_intercon_1_RRESP),
        .M_AXI_rvalid(s00_couplers_to_axi_mem_intercon_1_RVALID),
        .M_AXI_wdata(s00_couplers_to_axi_mem_intercon_1_WDATA),
        .M_AXI_wlast(s00_couplers_to_axi_mem_intercon_1_WLAST),
        .M_AXI_wready(s00_couplers_to_axi_mem_intercon_1_WREADY),
        .M_AXI_wstrb(s00_couplers_to_axi_mem_intercon_1_WSTRB),
        .M_AXI_wvalid(s00_couplers_to_axi_mem_intercon_1_WVALID),
        .S_ACLK(S00_ACLK_1),
        .S_ARESETN(S00_ARESETN_1),
        .S_AXI_araddr(axi_mem_intercon_1_to_s00_couplers_ARADDR),
        .S_AXI_arburst(axi_mem_intercon_1_to_s00_couplers_ARBURST),
        .S_AXI_arcache(axi_mem_intercon_1_to_s00_couplers_ARCACHE),
        .S_AXI_arid(axi_mem_intercon_1_to_s00_couplers_ARID),
        .S_AXI_arlen(axi_mem_intercon_1_to_s00_couplers_ARLEN),
        .S_AXI_arlock(axi_mem_intercon_1_to_s00_couplers_ARLOCK),
        .S_AXI_arprot(axi_mem_intercon_1_to_s00_couplers_ARPROT),
        .S_AXI_arqos(axi_mem_intercon_1_to_s00_couplers_ARQOS),
        .S_AXI_arready(axi_mem_intercon_1_to_s00_couplers_ARREADY),
        .S_AXI_arsize(axi_mem_intercon_1_to_s00_couplers_ARSIZE),
        .S_AXI_arvalid(axi_mem_intercon_1_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(axi_mem_intercon_1_to_s00_couplers_AWADDR),
        .S_AXI_awburst(axi_mem_intercon_1_to_s00_couplers_AWBURST),
        .S_AXI_awcache(axi_mem_intercon_1_to_s00_couplers_AWCACHE),
        .S_AXI_awid(axi_mem_intercon_1_to_s00_couplers_AWID),
        .S_AXI_awlen(axi_mem_intercon_1_to_s00_couplers_AWLEN),
        .S_AXI_awlock(axi_mem_intercon_1_to_s00_couplers_AWLOCK),
        .S_AXI_awprot(axi_mem_intercon_1_to_s00_couplers_AWPROT),
        .S_AXI_awqos(axi_mem_intercon_1_to_s00_couplers_AWQOS),
        .S_AXI_awready(axi_mem_intercon_1_to_s00_couplers_AWREADY),
        .S_AXI_awsize(axi_mem_intercon_1_to_s00_couplers_AWSIZE),
        .S_AXI_awvalid(axi_mem_intercon_1_to_s00_couplers_AWVALID),
        .S_AXI_bid(axi_mem_intercon_1_to_s00_couplers_BID),
        .S_AXI_bready(axi_mem_intercon_1_to_s00_couplers_BREADY),
        .S_AXI_bresp(axi_mem_intercon_1_to_s00_couplers_BRESP),
        .S_AXI_bvalid(axi_mem_intercon_1_to_s00_couplers_BVALID),
        .S_AXI_rdata(axi_mem_intercon_1_to_s00_couplers_RDATA),
        .S_AXI_rid(axi_mem_intercon_1_to_s00_couplers_RID),
        .S_AXI_rlast(axi_mem_intercon_1_to_s00_couplers_RLAST),
        .S_AXI_rready(axi_mem_intercon_1_to_s00_couplers_RREADY),
        .S_AXI_rresp(axi_mem_intercon_1_to_s00_couplers_RRESP),
        .S_AXI_rvalid(axi_mem_intercon_1_to_s00_couplers_RVALID),
        .S_AXI_wdata(axi_mem_intercon_1_to_s00_couplers_WDATA),
        .S_AXI_wid(axi_mem_intercon_1_to_s00_couplers_WID),
        .S_AXI_wlast(axi_mem_intercon_1_to_s00_couplers_WLAST),
        .S_AXI_wready(axi_mem_intercon_1_to_s00_couplers_WREADY),
        .S_AXI_wstrb(axi_mem_intercon_1_to_s00_couplers_WSTRB),
        .S_AXI_wvalid(axi_mem_intercon_1_to_s00_couplers_WVALID));
endmodule

module dummy_ps_logic_imp_CO8RTL
   (ARESETN,
    M00_AXI_araddr,
    M00_AXI_arburst,
    M00_AXI_arcache,
    M00_AXI_arid,
    M00_AXI_arlen,
    M00_AXI_arlock,
    M00_AXI_arprot,
    M00_AXI_arqos,
    M00_AXI_arready,
    M00_AXI_arsize,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awburst,
    M00_AXI_awcache,
    M00_AXI_awid,
    M00_AXI_awlen,
    M00_AXI_awlock,
    M00_AXI_awprot,
    M00_AXI_awqos,
    M00_AXI_awready,
    M00_AXI_awsize,
    M00_AXI_awvalid,
    M00_AXI_bid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rid,
    M00_AXI_rlast,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wlast,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arburst,
    S00_AXI_arcache,
    S00_AXI_arid,
    S00_AXI_arlen,
    S00_AXI_arlock,
    S00_AXI_arprot,
    S00_AXI_arqos,
    S00_AXI_arready,
    S00_AXI_arsize,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awburst,
    S00_AXI_awcache,
    S00_AXI_awid,
    S00_AXI_awlen,
    S00_AXI_awlock,
    S00_AXI_awprot,
    S00_AXI_awqos,
    S00_AXI_awready,
    S00_AXI_awsize,
    S00_AXI_awvalid,
    S00_AXI_bid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rid,
    S00_AXI_rlast,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wid,
    S00_AXI_wlast,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid);
  input [0:0]ARESETN;
  output [15:0]M00_AXI_araddr;
  output [1:0]M00_AXI_arburst;
  output [3:0]M00_AXI_arcache;
  output [11:0]M00_AXI_arid;
  output [7:0]M00_AXI_arlen;
  output M00_AXI_arlock;
  output [2:0]M00_AXI_arprot;
  output [3:0]M00_AXI_arqos;
  input M00_AXI_arready;
  output [2:0]M00_AXI_arsize;
  output M00_AXI_arvalid;
  output [15:0]M00_AXI_awaddr;
  output [1:0]M00_AXI_awburst;
  output [3:0]M00_AXI_awcache;
  output [11:0]M00_AXI_awid;
  output [7:0]M00_AXI_awlen;
  output M00_AXI_awlock;
  output [2:0]M00_AXI_awprot;
  output [3:0]M00_AXI_awqos;
  input M00_AXI_awready;
  output [2:0]M00_AXI_awsize;
  output M00_AXI_awvalid;
  input [11:0]M00_AXI_bid;
  output M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  input [11:0]M00_AXI_rid;
  input M00_AXI_rlast;
  output M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  output M00_AXI_wlast;
  input M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output M00_AXI_wvalid;
  input S00_ACLK;
  input [0:0]S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [1:0]S00_AXI_arburst;
  input [3:0]S00_AXI_arcache;
  input [11:0]S00_AXI_arid;
  input [3:0]S00_AXI_arlen;
  input [1:0]S00_AXI_arlock;
  input [2:0]S00_AXI_arprot;
  input [3:0]S00_AXI_arqos;
  output S00_AXI_arready;
  input [2:0]S00_AXI_arsize;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [1:0]S00_AXI_awburst;
  input [3:0]S00_AXI_awcache;
  input [11:0]S00_AXI_awid;
  input [3:0]S00_AXI_awlen;
  input [1:0]S00_AXI_awlock;
  input [2:0]S00_AXI_awprot;
  input [3:0]S00_AXI_awqos;
  output S00_AXI_awready;
  input [2:0]S00_AXI_awsize;
  input S00_AXI_awvalid;
  output [11:0]S00_AXI_bid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  output [11:0]S00_AXI_rid;
  output S00_AXI_rlast;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  input [11:0]S00_AXI_wid;
  input S00_AXI_wlast;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;

  wire [0:0]ARESETN_1;
  wire [15:0]Conn1_ARADDR;
  wire [1:0]Conn1_ARBURST;
  wire [3:0]Conn1_ARCACHE;
  wire [11:0]Conn1_ARID;
  wire [7:0]Conn1_ARLEN;
  wire Conn1_ARLOCK;
  wire [2:0]Conn1_ARPROT;
  wire [3:0]Conn1_ARQOS;
  wire Conn1_ARREADY;
  wire [2:0]Conn1_ARSIZE;
  wire Conn1_ARVALID;
  wire [15:0]Conn1_AWADDR;
  wire [1:0]Conn1_AWBURST;
  wire [3:0]Conn1_AWCACHE;
  wire [11:0]Conn1_AWID;
  wire [7:0]Conn1_AWLEN;
  wire Conn1_AWLOCK;
  wire [2:0]Conn1_AWPROT;
  wire [3:0]Conn1_AWQOS;
  wire Conn1_AWREADY;
  wire [2:0]Conn1_AWSIZE;
  wire Conn1_AWVALID;
  wire [11:0]Conn1_BID;
  wire Conn1_BREADY;
  wire [1:0]Conn1_BRESP;
  wire Conn1_BVALID;
  wire [31:0]Conn1_RDATA;
  wire [11:0]Conn1_RID;
  wire Conn1_RLAST;
  wire Conn1_RREADY;
  wire [1:0]Conn1_RRESP;
  wire Conn1_RVALID;
  wire [31:0]Conn1_WDATA;
  wire Conn1_WLAST;
  wire Conn1_WREADY;
  wire [3:0]Conn1_WSTRB;
  wire Conn1_WVALID;
  wire [0:0]proc_sys_reset_0_peripheral_aresetn;
  wire processing_system7_0_FCLK_CLK0;
  wire [31:0]processing_system7_0_M_AXI_GP0_ARADDR;
  wire [1:0]processing_system7_0_M_AXI_GP0_ARBURST;
  wire [3:0]processing_system7_0_M_AXI_GP0_ARCACHE;
  wire [11:0]processing_system7_0_M_AXI_GP0_ARID;
  wire [3:0]processing_system7_0_M_AXI_GP0_ARLEN;
  wire [1:0]processing_system7_0_M_AXI_GP0_ARLOCK;
  wire [2:0]processing_system7_0_M_AXI_GP0_ARPROT;
  wire [3:0]processing_system7_0_M_AXI_GP0_ARQOS;
  wire processing_system7_0_M_AXI_GP0_ARREADY;
  wire [2:0]processing_system7_0_M_AXI_GP0_ARSIZE;
  wire processing_system7_0_M_AXI_GP0_ARVALID;
  wire [31:0]processing_system7_0_M_AXI_GP0_AWADDR;
  wire [1:0]processing_system7_0_M_AXI_GP0_AWBURST;
  wire [3:0]processing_system7_0_M_AXI_GP0_AWCACHE;
  wire [11:0]processing_system7_0_M_AXI_GP0_AWID;
  wire [3:0]processing_system7_0_M_AXI_GP0_AWLEN;
  wire [1:0]processing_system7_0_M_AXI_GP0_AWLOCK;
  wire [2:0]processing_system7_0_M_AXI_GP0_AWPROT;
  wire [3:0]processing_system7_0_M_AXI_GP0_AWQOS;
  wire processing_system7_0_M_AXI_GP0_AWREADY;
  wire [2:0]processing_system7_0_M_AXI_GP0_AWSIZE;
  wire processing_system7_0_M_AXI_GP0_AWVALID;
  wire [11:0]processing_system7_0_M_AXI_GP0_BID;
  wire processing_system7_0_M_AXI_GP0_BREADY;
  wire [1:0]processing_system7_0_M_AXI_GP0_BRESP;
  wire processing_system7_0_M_AXI_GP0_BVALID;
  wire [31:0]processing_system7_0_M_AXI_GP0_RDATA;
  wire [11:0]processing_system7_0_M_AXI_GP0_RID;
  wire processing_system7_0_M_AXI_GP0_RLAST;
  wire processing_system7_0_M_AXI_GP0_RREADY;
  wire [1:0]processing_system7_0_M_AXI_GP0_RRESP;
  wire processing_system7_0_M_AXI_GP0_RVALID;
  wire [31:0]processing_system7_0_M_AXI_GP0_WDATA;
  wire [11:0]processing_system7_0_M_AXI_GP0_WID;
  wire processing_system7_0_M_AXI_GP0_WLAST;
  wire processing_system7_0_M_AXI_GP0_WREADY;
  wire [3:0]processing_system7_0_M_AXI_GP0_WSTRB;
  wire processing_system7_0_M_AXI_GP0_WVALID;

  assign ARESETN_1 = ARESETN[0];
  assign Conn1_ARREADY = M00_AXI_arready;
  assign Conn1_AWREADY = M00_AXI_awready;
  assign Conn1_BID = M00_AXI_bid[11:0];
  assign Conn1_BRESP = M00_AXI_bresp[1:0];
  assign Conn1_BVALID = M00_AXI_bvalid;
  assign Conn1_RDATA = M00_AXI_rdata[31:0];
  assign Conn1_RID = M00_AXI_rid[11:0];
  assign Conn1_RLAST = M00_AXI_rlast;
  assign Conn1_RRESP = M00_AXI_rresp[1:0];
  assign Conn1_RVALID = M00_AXI_rvalid;
  assign Conn1_WREADY = M00_AXI_wready;
  assign M00_AXI_araddr[15:0] = Conn1_ARADDR;
  assign M00_AXI_arburst[1:0] = Conn1_ARBURST;
  assign M00_AXI_arcache[3:0] = Conn1_ARCACHE;
  assign M00_AXI_arid[11:0] = Conn1_ARID;
  assign M00_AXI_arlen[7:0] = Conn1_ARLEN;
  assign M00_AXI_arlock = Conn1_ARLOCK;
  assign M00_AXI_arprot[2:0] = Conn1_ARPROT;
  assign M00_AXI_arqos[3:0] = Conn1_ARQOS;
  assign M00_AXI_arsize[2:0] = Conn1_ARSIZE;
  assign M00_AXI_arvalid = Conn1_ARVALID;
  assign M00_AXI_awaddr[15:0] = Conn1_AWADDR;
  assign M00_AXI_awburst[1:0] = Conn1_AWBURST;
  assign M00_AXI_awcache[3:0] = Conn1_AWCACHE;
  assign M00_AXI_awid[11:0] = Conn1_AWID;
  assign M00_AXI_awlen[7:0] = Conn1_AWLEN;
  assign M00_AXI_awlock = Conn1_AWLOCK;
  assign M00_AXI_awprot[2:0] = Conn1_AWPROT;
  assign M00_AXI_awqos[3:0] = Conn1_AWQOS;
  assign M00_AXI_awsize[2:0] = Conn1_AWSIZE;
  assign M00_AXI_awvalid = Conn1_AWVALID;
  assign M00_AXI_bready = Conn1_BREADY;
  assign M00_AXI_rready = Conn1_RREADY;
  assign M00_AXI_wdata[31:0] = Conn1_WDATA;
  assign M00_AXI_wlast = Conn1_WLAST;
  assign M00_AXI_wstrb[3:0] = Conn1_WSTRB;
  assign M00_AXI_wvalid = Conn1_WVALID;
  assign S00_AXI_arready = processing_system7_0_M_AXI_GP0_ARREADY;
  assign S00_AXI_awready = processing_system7_0_M_AXI_GP0_AWREADY;
  assign S00_AXI_bid[11:0] = processing_system7_0_M_AXI_GP0_BID;
  assign S00_AXI_bresp[1:0] = processing_system7_0_M_AXI_GP0_BRESP;
  assign S00_AXI_bvalid = processing_system7_0_M_AXI_GP0_BVALID;
  assign S00_AXI_rdata[31:0] = processing_system7_0_M_AXI_GP0_RDATA;
  assign S00_AXI_rid[11:0] = processing_system7_0_M_AXI_GP0_RID;
  assign S00_AXI_rlast = processing_system7_0_M_AXI_GP0_RLAST;
  assign S00_AXI_rresp[1:0] = processing_system7_0_M_AXI_GP0_RRESP;
  assign S00_AXI_rvalid = processing_system7_0_M_AXI_GP0_RVALID;
  assign S00_AXI_wready = processing_system7_0_M_AXI_GP0_WREADY;
  assign proc_sys_reset_0_peripheral_aresetn = S00_ARESETN[0];
  assign processing_system7_0_FCLK_CLK0 = S00_ACLK;
  assign processing_system7_0_M_AXI_GP0_ARADDR = S00_AXI_araddr[31:0];
  assign processing_system7_0_M_AXI_GP0_ARBURST = S00_AXI_arburst[1:0];
  assign processing_system7_0_M_AXI_GP0_ARCACHE = S00_AXI_arcache[3:0];
  assign processing_system7_0_M_AXI_GP0_ARID = S00_AXI_arid[11:0];
  assign processing_system7_0_M_AXI_GP0_ARLEN = S00_AXI_arlen[3:0];
  assign processing_system7_0_M_AXI_GP0_ARLOCK = S00_AXI_arlock[1:0];
  assign processing_system7_0_M_AXI_GP0_ARPROT = S00_AXI_arprot[2:0];
  assign processing_system7_0_M_AXI_GP0_ARQOS = S00_AXI_arqos[3:0];
  assign processing_system7_0_M_AXI_GP0_ARSIZE = S00_AXI_arsize[2:0];
  assign processing_system7_0_M_AXI_GP0_ARVALID = S00_AXI_arvalid;
  assign processing_system7_0_M_AXI_GP0_AWADDR = S00_AXI_awaddr[31:0];
  assign processing_system7_0_M_AXI_GP0_AWBURST = S00_AXI_awburst[1:0];
  assign processing_system7_0_M_AXI_GP0_AWCACHE = S00_AXI_awcache[3:0];
  assign processing_system7_0_M_AXI_GP0_AWID = S00_AXI_awid[11:0];
  assign processing_system7_0_M_AXI_GP0_AWLEN = S00_AXI_awlen[3:0];
  assign processing_system7_0_M_AXI_GP0_AWLOCK = S00_AXI_awlock[1:0];
  assign processing_system7_0_M_AXI_GP0_AWPROT = S00_AXI_awprot[2:0];
  assign processing_system7_0_M_AXI_GP0_AWQOS = S00_AXI_awqos[3:0];
  assign processing_system7_0_M_AXI_GP0_AWSIZE = S00_AXI_awsize[2:0];
  assign processing_system7_0_M_AXI_GP0_AWVALID = S00_AXI_awvalid;
  assign processing_system7_0_M_AXI_GP0_BREADY = S00_AXI_bready;
  assign processing_system7_0_M_AXI_GP0_RREADY = S00_AXI_rready;
  assign processing_system7_0_M_AXI_GP0_WDATA = S00_AXI_wdata[31:0];
  assign processing_system7_0_M_AXI_GP0_WID = S00_AXI_wid[11:0];
  assign processing_system7_0_M_AXI_GP0_WLAST = S00_AXI_wlast;
  assign processing_system7_0_M_AXI_GP0_WSTRB = S00_AXI_wstrb[3:0];
  assign processing_system7_0_M_AXI_GP0_WVALID = S00_AXI_wvalid;
display_pl_side_axi_mem_intercon_1_0 axi_mem_intercon_1
       (.ACLK(processing_system7_0_FCLK_CLK0),
        .ARESETN(ARESETN_1),
        .M00_ACLK(processing_system7_0_FCLK_CLK0),
        .M00_ARESETN(proc_sys_reset_0_peripheral_aresetn),
        .M00_AXI_araddr(Conn1_ARADDR),
        .M00_AXI_arburst(Conn1_ARBURST),
        .M00_AXI_arcache(Conn1_ARCACHE),
        .M00_AXI_arid(Conn1_ARID),
        .M00_AXI_arlen(Conn1_ARLEN),
        .M00_AXI_arlock(Conn1_ARLOCK),
        .M00_AXI_arprot(Conn1_ARPROT),
        .M00_AXI_arqos(Conn1_ARQOS),
        .M00_AXI_arready(Conn1_ARREADY),
        .M00_AXI_arsize(Conn1_ARSIZE),
        .M00_AXI_arvalid(Conn1_ARVALID),
        .M00_AXI_awaddr(Conn1_AWADDR),
        .M00_AXI_awburst(Conn1_AWBURST),
        .M00_AXI_awcache(Conn1_AWCACHE),
        .M00_AXI_awid(Conn1_AWID),
        .M00_AXI_awlen(Conn1_AWLEN),
        .M00_AXI_awlock(Conn1_AWLOCK),
        .M00_AXI_awprot(Conn1_AWPROT),
        .M00_AXI_awqos(Conn1_AWQOS),
        .M00_AXI_awready(Conn1_AWREADY),
        .M00_AXI_awsize(Conn1_AWSIZE),
        .M00_AXI_awvalid(Conn1_AWVALID),
        .M00_AXI_bid(Conn1_BID),
        .M00_AXI_bready(Conn1_BREADY),
        .M00_AXI_bresp(Conn1_BRESP),
        .M00_AXI_bvalid(Conn1_BVALID),
        .M00_AXI_rdata(Conn1_RDATA),
        .M00_AXI_rid(Conn1_RID),
        .M00_AXI_rlast(Conn1_RLAST),
        .M00_AXI_rready(Conn1_RREADY),
        .M00_AXI_rresp(Conn1_RRESP),
        .M00_AXI_rvalid(Conn1_RVALID),
        .M00_AXI_wdata(Conn1_WDATA),
        .M00_AXI_wlast(Conn1_WLAST),
        .M00_AXI_wready(Conn1_WREADY),
        .M00_AXI_wstrb(Conn1_WSTRB),
        .M00_AXI_wvalid(Conn1_WVALID),
        .S00_ACLK(processing_system7_0_FCLK_CLK0),
        .S00_ARESETN(proc_sys_reset_0_peripheral_aresetn),
        .S00_AXI_araddr(processing_system7_0_M_AXI_GP0_ARADDR),
        .S00_AXI_arburst(processing_system7_0_M_AXI_GP0_ARBURST),
        .S00_AXI_arcache(processing_system7_0_M_AXI_GP0_ARCACHE),
        .S00_AXI_arid(processing_system7_0_M_AXI_GP0_ARID),
        .S00_AXI_arlen(processing_system7_0_M_AXI_GP0_ARLEN),
        .S00_AXI_arlock(processing_system7_0_M_AXI_GP0_ARLOCK),
        .S00_AXI_arprot(processing_system7_0_M_AXI_GP0_ARPROT),
        .S00_AXI_arqos(processing_system7_0_M_AXI_GP0_ARQOS),
        .S00_AXI_arready(processing_system7_0_M_AXI_GP0_ARREADY),
        .S00_AXI_arsize(processing_system7_0_M_AXI_GP0_ARSIZE),
        .S00_AXI_arvalid(processing_system7_0_M_AXI_GP0_ARVALID),
        .S00_AXI_awaddr(processing_system7_0_M_AXI_GP0_AWADDR),
        .S00_AXI_awburst(processing_system7_0_M_AXI_GP0_AWBURST),
        .S00_AXI_awcache(processing_system7_0_M_AXI_GP0_AWCACHE),
        .S00_AXI_awid(processing_system7_0_M_AXI_GP0_AWID),
        .S00_AXI_awlen(processing_system7_0_M_AXI_GP0_AWLEN),
        .S00_AXI_awlock(processing_system7_0_M_AXI_GP0_AWLOCK),
        .S00_AXI_awprot(processing_system7_0_M_AXI_GP0_AWPROT),
        .S00_AXI_awqos(processing_system7_0_M_AXI_GP0_AWQOS),
        .S00_AXI_awready(processing_system7_0_M_AXI_GP0_AWREADY),
        .S00_AXI_awsize(processing_system7_0_M_AXI_GP0_AWSIZE),
        .S00_AXI_awvalid(processing_system7_0_M_AXI_GP0_AWVALID),
        .S00_AXI_bid(processing_system7_0_M_AXI_GP0_BID),
        .S00_AXI_bready(processing_system7_0_M_AXI_GP0_BREADY),
        .S00_AXI_bresp(processing_system7_0_M_AXI_GP0_BRESP),
        .S00_AXI_bvalid(processing_system7_0_M_AXI_GP0_BVALID),
        .S00_AXI_rdata(processing_system7_0_M_AXI_GP0_RDATA),
        .S00_AXI_rid(processing_system7_0_M_AXI_GP0_RID),
        .S00_AXI_rlast(processing_system7_0_M_AXI_GP0_RLAST),
        .S00_AXI_rready(processing_system7_0_M_AXI_GP0_RREADY),
        .S00_AXI_rresp(processing_system7_0_M_AXI_GP0_RRESP),
        .S00_AXI_rvalid(processing_system7_0_M_AXI_GP0_RVALID),
        .S00_AXI_wdata(processing_system7_0_M_AXI_GP0_WDATA),
        .S00_AXI_wid(processing_system7_0_M_AXI_GP0_WID),
        .S00_AXI_wlast(processing_system7_0_M_AXI_GP0_WLAST),
        .S00_AXI_wready(processing_system7_0_M_AXI_GP0_WREADY),
        .S00_AXI_wstrb(processing_system7_0_M_AXI_GP0_WSTRB),
        .S00_AXI_wvalid(processing_system7_0_M_AXI_GP0_WVALID));
endmodule

module s00_couplers_imp_1GWH4PS
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arid,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awid,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rid,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wid,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [15:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [11:0]M_AXI_arid;
  output [7:0]M_AXI_arlen;
  output M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  output [15:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [11:0]M_AXI_awid;
  output [7:0]M_AXI_awlen;
  output M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  input [11:0]M_AXI_bid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  input [11:0]M_AXI_rid;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [11:0]S_AXI_arid;
  input [3:0]S_AXI_arlen;
  input [1:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [11:0]S_AXI_awid;
  input [3:0]S_AXI_awlen;
  input [1:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [11:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [11:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input [11:0]S_AXI_wid;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_pc_to_s00_couplers_ARADDR;
  wire [1:0]auto_pc_to_s00_couplers_ARBURST;
  wire [3:0]auto_pc_to_s00_couplers_ARCACHE;
  wire [11:0]auto_pc_to_s00_couplers_ARID;
  wire [7:0]auto_pc_to_s00_couplers_ARLEN;
  wire [0:0]auto_pc_to_s00_couplers_ARLOCK;
  wire [2:0]auto_pc_to_s00_couplers_ARPROT;
  wire [3:0]auto_pc_to_s00_couplers_ARQOS;
  wire auto_pc_to_s00_couplers_ARREADY;
  wire [2:0]auto_pc_to_s00_couplers_ARSIZE;
  wire auto_pc_to_s00_couplers_ARVALID;
  wire [31:0]auto_pc_to_s00_couplers_AWADDR;
  wire [1:0]auto_pc_to_s00_couplers_AWBURST;
  wire [3:0]auto_pc_to_s00_couplers_AWCACHE;
  wire [11:0]auto_pc_to_s00_couplers_AWID;
  wire [7:0]auto_pc_to_s00_couplers_AWLEN;
  wire [0:0]auto_pc_to_s00_couplers_AWLOCK;
  wire [2:0]auto_pc_to_s00_couplers_AWPROT;
  wire [3:0]auto_pc_to_s00_couplers_AWQOS;
  wire auto_pc_to_s00_couplers_AWREADY;
  wire [2:0]auto_pc_to_s00_couplers_AWSIZE;
  wire auto_pc_to_s00_couplers_AWVALID;
  wire [11:0]auto_pc_to_s00_couplers_BID;
  wire auto_pc_to_s00_couplers_BREADY;
  wire [1:0]auto_pc_to_s00_couplers_BRESP;
  wire auto_pc_to_s00_couplers_BVALID;
  wire [31:0]auto_pc_to_s00_couplers_RDATA;
  wire [11:0]auto_pc_to_s00_couplers_RID;
  wire auto_pc_to_s00_couplers_RLAST;
  wire auto_pc_to_s00_couplers_RREADY;
  wire [1:0]auto_pc_to_s00_couplers_RRESP;
  wire auto_pc_to_s00_couplers_RVALID;
  wire [31:0]auto_pc_to_s00_couplers_WDATA;
  wire auto_pc_to_s00_couplers_WLAST;
  wire auto_pc_to_s00_couplers_WREADY;
  wire [3:0]auto_pc_to_s00_couplers_WSTRB;
  wire auto_pc_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_auto_pc_ARADDR;
  wire [1:0]s00_couplers_to_auto_pc_ARBURST;
  wire [3:0]s00_couplers_to_auto_pc_ARCACHE;
  wire [11:0]s00_couplers_to_auto_pc_ARID;
  wire [3:0]s00_couplers_to_auto_pc_ARLEN;
  wire [1:0]s00_couplers_to_auto_pc_ARLOCK;
  wire [2:0]s00_couplers_to_auto_pc_ARPROT;
  wire [3:0]s00_couplers_to_auto_pc_ARQOS;
  wire s00_couplers_to_auto_pc_ARREADY;
  wire [2:0]s00_couplers_to_auto_pc_ARSIZE;
  wire s00_couplers_to_auto_pc_ARVALID;
  wire [31:0]s00_couplers_to_auto_pc_AWADDR;
  wire [1:0]s00_couplers_to_auto_pc_AWBURST;
  wire [3:0]s00_couplers_to_auto_pc_AWCACHE;
  wire [11:0]s00_couplers_to_auto_pc_AWID;
  wire [3:0]s00_couplers_to_auto_pc_AWLEN;
  wire [1:0]s00_couplers_to_auto_pc_AWLOCK;
  wire [2:0]s00_couplers_to_auto_pc_AWPROT;
  wire [3:0]s00_couplers_to_auto_pc_AWQOS;
  wire s00_couplers_to_auto_pc_AWREADY;
  wire [2:0]s00_couplers_to_auto_pc_AWSIZE;
  wire s00_couplers_to_auto_pc_AWVALID;
  wire [11:0]s00_couplers_to_auto_pc_BID;
  wire s00_couplers_to_auto_pc_BREADY;
  wire [1:0]s00_couplers_to_auto_pc_BRESP;
  wire s00_couplers_to_auto_pc_BVALID;
  wire [31:0]s00_couplers_to_auto_pc_RDATA;
  wire [11:0]s00_couplers_to_auto_pc_RID;
  wire s00_couplers_to_auto_pc_RLAST;
  wire s00_couplers_to_auto_pc_RREADY;
  wire [1:0]s00_couplers_to_auto_pc_RRESP;
  wire s00_couplers_to_auto_pc_RVALID;
  wire [31:0]s00_couplers_to_auto_pc_WDATA;
  wire [11:0]s00_couplers_to_auto_pc_WID;
  wire s00_couplers_to_auto_pc_WLAST;
  wire s00_couplers_to_auto_pc_WREADY;
  wire [3:0]s00_couplers_to_auto_pc_WSTRB;
  wire s00_couplers_to_auto_pc_WVALID;

  assign M_AXI_araddr[15:0] = auto_pc_to_s00_couplers_ARADDR[15:0];
  assign M_AXI_arburst[1:0] = auto_pc_to_s00_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_pc_to_s00_couplers_ARCACHE;
  assign M_AXI_arid[11:0] = auto_pc_to_s00_couplers_ARID;
  assign M_AXI_arlen[7:0] = auto_pc_to_s00_couplers_ARLEN;
  assign M_AXI_arlock = auto_pc_to_s00_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_pc_to_s00_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = auto_pc_to_s00_couplers_ARQOS;
  assign M_AXI_arsize[2:0] = auto_pc_to_s00_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_pc_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[15:0] = auto_pc_to_s00_couplers_AWADDR[15:0];
  assign M_AXI_awburst[1:0] = auto_pc_to_s00_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_pc_to_s00_couplers_AWCACHE;
  assign M_AXI_awid[11:0] = auto_pc_to_s00_couplers_AWID;
  assign M_AXI_awlen[7:0] = auto_pc_to_s00_couplers_AWLEN;
  assign M_AXI_awlock = auto_pc_to_s00_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_pc_to_s00_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = auto_pc_to_s00_couplers_AWQOS;
  assign M_AXI_awsize[2:0] = auto_pc_to_s00_couplers_AWSIZE;
  assign M_AXI_awvalid = auto_pc_to_s00_couplers_AWVALID;
  assign M_AXI_bready = auto_pc_to_s00_couplers_BREADY;
  assign M_AXI_rready = auto_pc_to_s00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_pc_to_s00_couplers_WDATA;
  assign M_AXI_wlast = auto_pc_to_s00_couplers_WLAST;
  assign M_AXI_wstrb[3:0] = auto_pc_to_s00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_pc_to_s00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_arready = s00_couplers_to_auto_pc_ARREADY;
  assign S_AXI_awready = s00_couplers_to_auto_pc_AWREADY;
  assign S_AXI_bid[11:0] = s00_couplers_to_auto_pc_BID;
  assign S_AXI_bresp[1:0] = s00_couplers_to_auto_pc_BRESP;
  assign S_AXI_bvalid = s00_couplers_to_auto_pc_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_auto_pc_RDATA;
  assign S_AXI_rid[11:0] = s00_couplers_to_auto_pc_RID;
  assign S_AXI_rlast = s00_couplers_to_auto_pc_RLAST;
  assign S_AXI_rresp[1:0] = s00_couplers_to_auto_pc_RRESP;
  assign S_AXI_rvalid = s00_couplers_to_auto_pc_RVALID;
  assign S_AXI_wready = s00_couplers_to_auto_pc_WREADY;
  assign auto_pc_to_s00_couplers_ARREADY = M_AXI_arready;
  assign auto_pc_to_s00_couplers_AWREADY = M_AXI_awready;
  assign auto_pc_to_s00_couplers_BID = M_AXI_bid[11:0];
  assign auto_pc_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_pc_to_s00_couplers_BVALID = M_AXI_bvalid;
  assign auto_pc_to_s00_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_pc_to_s00_couplers_RID = M_AXI_rid[11:0];
  assign auto_pc_to_s00_couplers_RLAST = M_AXI_rlast;
  assign auto_pc_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_pc_to_s00_couplers_RVALID = M_AXI_rvalid;
  assign auto_pc_to_s00_couplers_WREADY = M_AXI_wready;
  assign s00_couplers_to_auto_pc_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_auto_pc_ARBURST = S_AXI_arburst[1:0];
  assign s00_couplers_to_auto_pc_ARCACHE = S_AXI_arcache[3:0];
  assign s00_couplers_to_auto_pc_ARID = S_AXI_arid[11:0];
  assign s00_couplers_to_auto_pc_ARLEN = S_AXI_arlen[3:0];
  assign s00_couplers_to_auto_pc_ARLOCK = S_AXI_arlock[1:0];
  assign s00_couplers_to_auto_pc_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_auto_pc_ARQOS = S_AXI_arqos[3:0];
  assign s00_couplers_to_auto_pc_ARSIZE = S_AXI_arsize[2:0];
  assign s00_couplers_to_auto_pc_ARVALID = S_AXI_arvalid;
  assign s00_couplers_to_auto_pc_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_auto_pc_AWBURST = S_AXI_awburst[1:0];
  assign s00_couplers_to_auto_pc_AWCACHE = S_AXI_awcache[3:0];
  assign s00_couplers_to_auto_pc_AWID = S_AXI_awid[11:0];
  assign s00_couplers_to_auto_pc_AWLEN = S_AXI_awlen[3:0];
  assign s00_couplers_to_auto_pc_AWLOCK = S_AXI_awlock[1:0];
  assign s00_couplers_to_auto_pc_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_auto_pc_AWQOS = S_AXI_awqos[3:0];
  assign s00_couplers_to_auto_pc_AWSIZE = S_AXI_awsize[2:0];
  assign s00_couplers_to_auto_pc_AWVALID = S_AXI_awvalid;
  assign s00_couplers_to_auto_pc_BREADY = S_AXI_bready;
  assign s00_couplers_to_auto_pc_RREADY = S_AXI_rready;
  assign s00_couplers_to_auto_pc_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_auto_pc_WID = S_AXI_wid[11:0];
  assign s00_couplers_to_auto_pc_WLAST = S_AXI_wlast;
  assign s00_couplers_to_auto_pc_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_auto_pc_WVALID = S_AXI_wvalid;
display_pl_side_auto_pc_0 auto_pc
       (.aclk(S_ACLK_1),
        .aresetn(S_ARESETN_1),
        .m_axi_araddr(auto_pc_to_s00_couplers_ARADDR),
        .m_axi_arburst(auto_pc_to_s00_couplers_ARBURST),
        .m_axi_arcache(auto_pc_to_s00_couplers_ARCACHE),
        .m_axi_arid(auto_pc_to_s00_couplers_ARID),
        .m_axi_arlen(auto_pc_to_s00_couplers_ARLEN),
        .m_axi_arlock(auto_pc_to_s00_couplers_ARLOCK),
        .m_axi_arprot(auto_pc_to_s00_couplers_ARPROT),
        .m_axi_arqos(auto_pc_to_s00_couplers_ARQOS),
        .m_axi_arready(auto_pc_to_s00_couplers_ARREADY),
        .m_axi_arsize(auto_pc_to_s00_couplers_ARSIZE),
        .m_axi_arvalid(auto_pc_to_s00_couplers_ARVALID),
        .m_axi_awaddr(auto_pc_to_s00_couplers_AWADDR),
        .m_axi_awburst(auto_pc_to_s00_couplers_AWBURST),
        .m_axi_awcache(auto_pc_to_s00_couplers_AWCACHE),
        .m_axi_awid(auto_pc_to_s00_couplers_AWID),
        .m_axi_awlen(auto_pc_to_s00_couplers_AWLEN),
        .m_axi_awlock(auto_pc_to_s00_couplers_AWLOCK),
        .m_axi_awprot(auto_pc_to_s00_couplers_AWPROT),
        .m_axi_awqos(auto_pc_to_s00_couplers_AWQOS),
        .m_axi_awready(auto_pc_to_s00_couplers_AWREADY),
        .m_axi_awsize(auto_pc_to_s00_couplers_AWSIZE),
        .m_axi_awvalid(auto_pc_to_s00_couplers_AWVALID),
        .m_axi_bid(auto_pc_to_s00_couplers_BID),
        .m_axi_bready(auto_pc_to_s00_couplers_BREADY),
        .m_axi_bresp(auto_pc_to_s00_couplers_BRESP),
        .m_axi_bvalid(auto_pc_to_s00_couplers_BVALID),
        .m_axi_rdata(auto_pc_to_s00_couplers_RDATA),
        .m_axi_rid(auto_pc_to_s00_couplers_RID),
        .m_axi_rlast(auto_pc_to_s00_couplers_RLAST),
        .m_axi_rready(auto_pc_to_s00_couplers_RREADY),
        .m_axi_rresp(auto_pc_to_s00_couplers_RRESP),
        .m_axi_rvalid(auto_pc_to_s00_couplers_RVALID),
        .m_axi_wdata(auto_pc_to_s00_couplers_WDATA),
        .m_axi_wlast(auto_pc_to_s00_couplers_WLAST),
        .m_axi_wready(auto_pc_to_s00_couplers_WREADY),
        .m_axi_wstrb(auto_pc_to_s00_couplers_WSTRB),
        .m_axi_wvalid(auto_pc_to_s00_couplers_WVALID),
        .s_axi_araddr(s00_couplers_to_auto_pc_ARADDR),
        .s_axi_arburst(s00_couplers_to_auto_pc_ARBURST),
        .s_axi_arcache(s00_couplers_to_auto_pc_ARCACHE),
        .s_axi_arid(s00_couplers_to_auto_pc_ARID),
        .s_axi_arlen(s00_couplers_to_auto_pc_ARLEN),
        .s_axi_arlock(s00_couplers_to_auto_pc_ARLOCK),
        .s_axi_arprot(s00_couplers_to_auto_pc_ARPROT),
        .s_axi_arqos(s00_couplers_to_auto_pc_ARQOS),
        .s_axi_arready(s00_couplers_to_auto_pc_ARREADY),
        .s_axi_arsize(s00_couplers_to_auto_pc_ARSIZE),
        .s_axi_arvalid(s00_couplers_to_auto_pc_ARVALID),
        .s_axi_awaddr(s00_couplers_to_auto_pc_AWADDR),
        .s_axi_awburst(s00_couplers_to_auto_pc_AWBURST),
        .s_axi_awcache(s00_couplers_to_auto_pc_AWCACHE),
        .s_axi_awid(s00_couplers_to_auto_pc_AWID),
        .s_axi_awlen(s00_couplers_to_auto_pc_AWLEN),
        .s_axi_awlock(s00_couplers_to_auto_pc_AWLOCK),
        .s_axi_awprot(s00_couplers_to_auto_pc_AWPROT),
        .s_axi_awqos(s00_couplers_to_auto_pc_AWQOS),
        .s_axi_awready(s00_couplers_to_auto_pc_AWREADY),
        .s_axi_awsize(s00_couplers_to_auto_pc_AWSIZE),
        .s_axi_awvalid(s00_couplers_to_auto_pc_AWVALID),
        .s_axi_bid(s00_couplers_to_auto_pc_BID),
        .s_axi_bready(s00_couplers_to_auto_pc_BREADY),
        .s_axi_bresp(s00_couplers_to_auto_pc_BRESP),
        .s_axi_bvalid(s00_couplers_to_auto_pc_BVALID),
        .s_axi_rdata(s00_couplers_to_auto_pc_RDATA),
        .s_axi_rid(s00_couplers_to_auto_pc_RID),
        .s_axi_rlast(s00_couplers_to_auto_pc_RLAST),
        .s_axi_rready(s00_couplers_to_auto_pc_RREADY),
        .s_axi_rresp(s00_couplers_to_auto_pc_RRESP),
        .s_axi_rvalid(s00_couplers_to_auto_pc_RVALID),
        .s_axi_wdata(s00_couplers_to_auto_pc_WDATA),
        .s_axi_wid(s00_couplers_to_auto_pc_WID),
        .s_axi_wlast(s00_couplers_to_auto_pc_WLAST),
        .s_axi_wready(s00_couplers_to_auto_pc_WREADY),
        .s_axi_wstrb(s00_couplers_to_auto_pc_WSTRB),
        .s_axi_wvalid(s00_couplers_to_auto_pc_WVALID));
endmodule
