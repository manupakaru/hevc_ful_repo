`timescale 1ns / 10ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:38:22 12/16/2013 
// Design Name: 
// Module Name:    video_tx_top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module video_tx_top(
		clk,
		reset,
		hsync,
		vsync,
		DE,
		pclk,
		Data_out,
		scl_pad,
		sda_pad
    );

	
//-------------------------------------------------------------------------------
// I/O signals
//-------------------------------------------------------------------------------
    
	input						clk;
	input						reset;
	
	output						hsync;
	output                      vsync;
	output                      DE;
	output						pclk;
	output	[23:0]				Data_out;
	
	inout					    scl_pad;
	inout					    sda_pad;
	
	assign  scl_pad = (~scl_padoen_o) ? scl_pad_o : 1'bz ;
	assign  sda_pad = (~sda_padoen_o) ? sda_pad_o : 1'bz ;
	
	assign  scl_pad_i = scl_pad;
	assign  sda_pad_i = sda_pad;
	
//-------------------------------------------------------------------------------
// parameter definitions
//-------------------------------------------------------------------------------
    parameter					FREQ_IN		=	150;
	parameter					FREQ_OUT	=	75;

//-------------------------------------------------------------------------------
// localparam definitions
//-------------------------------------------------------------------------------
    localparam					STATE_INIT						=	0 ;
	localparam					STATE_I2C_INIT_WAIT				=	50 ;
	localparam					STATE_HPD_SENSE_1				=	1 ;
	localparam					STATE_HPD_SENSE_2				=	51 ;
	localparam					STATE_POWER_UP					= 	2 ; 
	localparam                  STATE_POWER_UP_WAIT             = 	3 ;
	localparam                  STATE_FIXED_REG_SET_1           = 	4 ;
	localparam                  STATE_FIXED_REG_SET_1_WAIT      = 	5 ;
	localparam                  STATE_FIXED_REG_SET_2           = 	6 ;
	localparam                  STATE_FIXED_REG_SET_2_WAIT      = 	7 ;
	localparam                  STATE_FIXED_REG_SET_3           = 	8 ;
	localparam                  STATE_FIXED_REG_SET_3_WAIT      = 	9 ;
	localparam                  STATE_FIXED_REG_SET_4           = 	10;
	localparam                  STATE_FIXED_REG_SET_4_WAIT      = 	11;
	localparam                  STATE_FIXED_REG_SET_5           = 	12;
	localparam                  STATE_FIXED_REG_SET_5_WAIT      = 	13;
	localparam                  STATE_FIXED_REG_SET_6           = 	14;
	localparam                  STATE_FIXED_REG_SET_6_WAIT      = 	15;
	localparam                  STATE_FIXED_REG_SET_7           = 	16;
	localparam                  STATE_FIXED_REG_SET_7_WAIT      = 	17;
	localparam                  STATE_FIXED_REG_SET_8           = 	18;
	localparam                  STATE_FIXED_REG_SET_8_WAIT      = 	19;
	localparam                  STATE_INPUT_MODE_SET_1          = 	20;
	localparam                  STATE_INPUT_MODE_SET_1_WAIT     = 	21;
	localparam                  STATE_INPUT_MODE_SET_2          = 	22;
	localparam                  STATE_INPUT_MODE_SET_2_WAIT     = 	23;
	localparam                  STATE_INPUT_MODE_SET_3          = 	24;
	localparam                  STATE_INPUT_MODE_SET_3_WAIT     = 	25;
	localparam                  STATE_INPUT_MODE_SET_4          = 	26;
	localparam                  STATE_INPUT_MODE_SET_4_WAIT     = 	27;
	localparam                  STATE_OUTPUT_MODE_SET_1         = 	28;
	localparam                  STATE_OUTPUT_MODE_SET_1_WAIT    = 	29;
	localparam                  STATE_OUTPUT_MODE_SET_2         = 	30;
	localparam                  STATE_OUTPUT_MODE_SET_2_WAIT    = 	31;
	localparam                  STATE_OUTPUT_MODE_SET_3         = 	32;
	localparam                  STATE_OUTPUT_MODE_SET_3_WAIT    = 	33;
	localparam                  STATE_OUTPUT_MODE_SET_4         = 	34;
	localparam                  STATE_OUTPUT_MODE_SET_4_WAIT    = 	35;
	localparam                  STATE_OUTPUT_MODE_SET_5         = 	36;
	localparam                  STATE_OUTPUT_MODE_SET_5_WAIT    = 	37;
	localparam                  STATE_VIDEO_TX                  = 	38;
	localparam                  STATE_VIDEO_TX_1                = 	39;
	localparam                  STATE_VIDEO_TX_2                = 	40;
	localparam                  STATE_VIDEO_TX_3                = 	41;
	localparam                  STATE_VIDEO_TX_4                = 	42;
	localparam                  STATE_VIDEO_TX_5                = 	43;
	localparam                  STATE_VIDEO_TX_6                = 	44;
	localparam                  STATE_VIDEO_TX_7                = 	45;
	localparam                  STATE_VIDEO_TX_8                = 	46;
	localparam                  STATE_VIDEO_TX_9                = 	47;
	localparam                  STATE_VIDEO_TX_10               = 	48;
	localparam                  STATE_VIDEO_TX_11               = 	49;
	
//-------------------------------------------------------------------------------
// Internal wires and registers
//-------------------------------------------------------------------------------
    
	reg							pclk_reg;
	reg							pclk_en;
	
	reg							hsync;
	reg							vsync;
	reg							DE;
	
	reg		[21:0]				pclk_count;
	reg		[11:0]				hsync_count;
	reg		[11:0]				DE_count;
	reg 	[35:0]				D_out;
	
	//-----I2C module signals---------------
	reg 	[6:0]				i2c_slave_addr;
	reg 	[7:0]				i2c_base_addr;
	reg 	[7:0]				i2c_wr_data;
	reg 						i2c_wr_en;
	reg 						i2c_valid;
	wire						i2c_ready;
	wire	[7:0]				i2c_rd_data;
	wire						i2c_done;	 
	wire                        scl_pad_i;	 
	wire                        scl_pad_o;	 
	wire                        scl_padoen_o;
	wire                        sda_pad_i;	 
	wire                        sda_pad_o;	 
	wire                        sda_padoen_o;
	
	reg		[7:0]				Y_data;
	reg		[7:0]				C_data;
	reg		[19:0]				disp_count;
	integer						state;

//-------------------------------------------------------------------------------
// Implementation
//-------------------------------------------------------------------------------
	
	//------Generate pixel clock-------------------
	always@(posedge clk or posedge reset) begin
		if(reset) begin
			pclk_reg	<= 1'b0;
		end
		else begin
			if(pclk_en) begin
				pclk_reg <= ~pclk_reg ;			// divide by 2 for 75 MHz pclk
			end
		end
	end
	
	BUFG BUFG_pclk ( 
	   .O(pclk), // 1-bit output: Clock output 
	   .I(pclk_reg) // 1-bit input: Clock input 
    );

	
	//-------Generate sync and DE signals----------
	always@(posedge pclk or posedge reset) begin
		if(reset) begin
			pclk_count <= 22'd0;	 // 1
			hsync_count <= 12'd0;
			DE_count <= 12'd0;
			//pclk_en <= 1'b1;	//----------------for now
		end
		else begin
			//-----Increment pclk counter-----------
			if(pclk_count == 22'd2475000) begin
				pclk_count <= 22'd1;
			end
			else begin
				pclk_count <= pclk_count + 1'b1 ;
			end
			//--------------------------------------
			
			//-----Generate Hsync-------------------
			if(hsync_count == 12'd2200) begin
				hsync_count <= 12'd1;
			end
			else begin
				hsync_count <= hsync_count + 1'b1 ;
			end
			
			//-----Generate DE---------------------
			if(pclk_count >= 22'd90200 && pclk_count < 22'd2466200) begin
				if(DE_count == 12'd2200) begin
					DE_count <= 12'd1;
				end
				else begin
					DE_count <= DE_count + 1'b1;
				end
			end
			else begin
				DE_count <= 12'd0; // 1
			end
			
		end
	end
	
	
	always@(posedge clk or posedge reset) begin
		if(reset) begin
			hsync <= 1'b1;
			vsync <= 1'b1;
			DE	  <= 1'b0;
		end
		else begin
			//-----Generate Hsync-------------------
			if(hsync_count < 12'd44 || hsync_count == 12'd2200) begin
				hsync <= 1'b1;
			end
			else begin
				hsync <= 1'b0;
			end
			//-----Generate Vsync------------------
			if(pclk_count < 22'd11000 || pclk_count == 22'd2475000) begin
				vsync <= 1'b1;
			end
			else begin
				vsync <= 1'b0;
			end
			//-----Generate DE---------------------
			if(DE_count >= 12'd192 &&  DE_count < 12'd2112) begin
				DE <= 1'b1;
			end
			else begin
				DE <= 1'b0;
			end
		end
	end
	
	//-----Generate I2C RW sub module-----------------
	i2c_rw_module  i2c_read_write(
		.clk			(clk),	
		.reset          (reset),
		.slave_addr_in  (i2c_slave_addr),
		.base_addr_in   (i2c_base_addr),
		.wr_data_in     (i2c_wr_data),
		.wr_en          (i2c_wr_en),
		.valid_in       (i2c_valid),
		.ready_out		(i2c_ready),
		.rd_data_out    (i2c_rd_data),
		.done_out       (i2c_done),
		.scl_pad_i		(scl_pad_i),
		.scl_pad_o      (scl_pad_o),
		.scl_padoen_o   (scl_padoen_o),
		.sda_pad_i      (sda_pad_i),
		.sda_pad_o      (sda_pad_o),	
		.sda_padoen_o   (sda_padoen_o),
		.state			()
    );
	
	
	assign	Data_out  =  {D_out[35:28],D_out[27:20],8'b00000000} ;
	
	
	//-----Configure the ADV chip upon power up------------------------------------------------------
	
	always@(posedge clk or posedge reset) begin
		if(reset) begin
			pclk_en <= 1'b0;
			disp_count <= 20'd0;
			state	<= STATE_INIT;
		end
		else begin
			case(state)
				
				//----------Configure I2C switch-----------------------------------------------	  //
				STATE_INIT : begin		                                                  	  	  // //
					if(i2c_ready) begin                                                           // //
						i2c_slave_addr 	<= 	7'b1110100;                                           // //
						i2c_base_addr	<= 	8'h00;                                                // //
						i2c_wr_data		<=	8'b00000010;                                          // //
						i2c_wr_en		<=	1'b1;                                                 // //
						i2c_valid		<=	1'b1;                                                 // //
						state	<= STATE_I2C_INIT_WAIT;                                           // //  
					end                                                                           // //
				end                                                                               // //
				STATE_I2C_INIT_WAIT : begin                                                       // //
					i2c_valid  <= 1'b0;                                                           // //
					if(i2c_done) begin                                                            // //
						state <= STATE_HPD_SENSE_1;                                           	  // //                          
					end                                                                           // //
				end																				  // //
				//--------------------------------------------------------------------------------// //
				
				
				//----------Monitor HPD status before power up-------------------------------------//
				                                                                                   //
				STATE_HPD_SENSE_1 : begin														   //
					if(i2c_ready) begin                                                            //
						i2c_slave_addr 	<= 	7'b0111001;                                            //
						i2c_base_addr	<= 	8'h42;                                                 //
						i2c_wr_data		<=	8'h0;                                                  //
						i2c_wr_en		<=	1'b0;                                                  //
						i2c_valid		<=	1'b1;                                                  //
						state	<= STATE_HPD_SENSE_2;                                              //
					end                                                                            //
				end                                                                                //
				STATE_HPD_SENSE_2 : begin                                                          //
					i2c_valid  <= 1'b0;                                                            //
					if(i2c_done) begin                                                             //
						if(i2c_rd_data[6]==1'b1) begin                                             //
							state <= STATE_POWER_UP;                                               //
						end                                                                        //
						else begin                                                                 //
							state <= STATE_HPD_SENSE_1;                                            //
						end                                                                        //
					end                                                                            //
				end                                                                                //
				//---------------------------------------------------------------------------------//
				
				
				
				//----------Power up--------------------------------------------------------	  //
				STATE_POWER_UP : begin		                                                  	  // //
					if(i2c_ready) begin                                                           // //
						i2c_slave_addr 	<= 	7'b0111001;                                           // //
						i2c_base_addr	<= 	8'h41;                                                // //
						i2c_wr_data		<=	8'b00010000;                                          // //
						i2c_wr_en		<=	1'b1;                                                 // //
						i2c_valid		<=	1'b1;                                                 // //
						state	<= STATE_POWER_UP_WAIT;                                           // //  
					end                                                                           // //
				end                                                                               // //
				STATE_POWER_UP_WAIT : begin                                                       // //
					i2c_valid  <= 1'b0;                                                           // //
					if(i2c_done) begin                                                            // //
						state <= STATE_FIXED_REG_SET_1;                                           // //                          
					end                                                                           // //
				end																				  // //
				//--------------------------------------------------------------------------------// //
				
				
				
				//----------Set fixed registers---------------------------------------------------//
																								  //
				STATE_FIXED_REG_SET_1 : begin                                                     //
					if(i2c_ready) begin                                                           //
						i2c_slave_addr 	<= 	7'b0111001;                                           //
						i2c_base_addr	<= 	8'h98;                                                //
						i2c_wr_data		<=	8'b00000011;                                          //
						i2c_wr_en		<=	1'b1;                                                 //
						i2c_valid		<=	1'b1;                                                 //
						state	<= STATE_FIXED_REG_SET_1_WAIT;                                    //
					end																			  //
				end                                                                               //
				STATE_FIXED_REG_SET_1_WAIT : begin                                                //
					i2c_valid  <= 1'b0;                                                           //
					if(i2c_done) begin                                                            //
						state <= STATE_FIXED_REG_SET_2;                                           //
					end																			  //
				end                                                                               //
																								  //
				STATE_FIXED_REG_SET_2 : begin                                                     //
					if(i2c_ready) begin                                                           //
						i2c_slave_addr 	<= 	7'b0111001;                                           //
						i2c_base_addr	<= 	8'h9A;                                                //
						i2c_wr_data		<=	8'b11100000;                                          //
						i2c_wr_en		<=	1'b1;                                                 //
						i2c_valid		<=	1'b1;                                                 //
						state	<= STATE_FIXED_REG_SET_2_WAIT;                                    //
					end																			  //
				end                                                                               //
				STATE_FIXED_REG_SET_2_WAIT : begin                                                //
					i2c_valid  <= 1'b0;                                                           //
					if(i2c_done) begin                                                            //
						state <= STATE_FIXED_REG_SET_3;                                           //
					end																			  //
				end                                                                               //
																								  //
				STATE_FIXED_REG_SET_3 : begin                                                     //
					if(i2c_ready) begin                                                           //
						i2c_slave_addr 	<= 	7'b0111001;                                           //
						i2c_base_addr	<= 	8'h9C;                                                //
						i2c_wr_data		<=	8'b00110000;                                          //
						i2c_wr_en		<=	1'b1;                                                 //
						i2c_valid		<=	1'b1;                                                 //
						state	<= STATE_FIXED_REG_SET_3_WAIT;                                    //
					end																			  //
				end                                                                               //
				STATE_FIXED_REG_SET_3_WAIT : begin                                                //
					i2c_valid  <= 1'b0;                                                           //
					if(i2c_done) begin                                                            //
						state <= STATE_FIXED_REG_SET_4;                                           //
					end																			  //
				end                                                                               //
																								  //
				STATE_FIXED_REG_SET_4 : begin                                                     //
					if(i2c_ready) begin                                                           //
						i2c_slave_addr 	<= 	7'b0111001;                                           //
						i2c_base_addr	<= 	8'h9D;                                                //
						i2c_wr_data		<=	8'b01100001;                                          //
						i2c_wr_en		<=	1'b1;                                                 //
						i2c_valid		<=	1'b1;                                                 //
						state	<= STATE_FIXED_REG_SET_4_WAIT;                                    //
					end																			  //
				end                                                                               //
				STATE_FIXED_REG_SET_4_WAIT : begin                                                //
					i2c_valid  <= 1'b0;                                                           //
					if(i2c_done) begin                                                            //
						state <= STATE_FIXED_REG_SET_5;                                           //
					end																			  //
				end                                                                               //
																								  //
				STATE_FIXED_REG_SET_5 : begin                                                     //
					if(i2c_ready) begin                                                           //
						i2c_slave_addr 	<= 	7'b0111001;                                           //
						i2c_base_addr	<= 	8'hA2;                                                //
						i2c_wr_data		<=	8'b10100100;                                          //
						i2c_wr_en		<=	1'b1;                                                 //
						i2c_valid		<=	1'b1;                                                 //
						state	<= STATE_FIXED_REG_SET_5_WAIT;                                    //
					end																			  //
				end                                                                               //
				STATE_FIXED_REG_SET_5_WAIT : begin                                                //
					i2c_valid  <= 1'b0;                                                           //
					if(i2c_done) begin                                                            //
						state <= STATE_FIXED_REG_SET_6;                                           //
					end																			  //
				end                                                                               //
																								  //
				STATE_FIXED_REG_SET_6 : begin                                                     //
					if(i2c_ready) begin                                                           //
						i2c_slave_addr 	<= 	7'b0111001;                                           //
						i2c_base_addr	<= 	8'hA3;                                                //
						i2c_wr_data		<=	8'b10100100;                                          //
						i2c_wr_en		<=	1'b1;                                                 //
						i2c_valid		<=	1'b1;                                                 //
						state	<= STATE_FIXED_REG_SET_6_WAIT;                                    //
					end																			  //
				end                                                                               //
				STATE_FIXED_REG_SET_6_WAIT : begin                                                //
					i2c_valid  <= 1'b0;                                                           //
					if(i2c_done) begin                                                            //
						state <= STATE_FIXED_REG_SET_7;                                           //
					end																			  //
				end                                                                               //
																								  //
				STATE_FIXED_REG_SET_7 : begin                                                     //
					if(i2c_ready) begin                                                           //
						i2c_slave_addr 	<= 	7'b0111001;                                           //
						i2c_base_addr	<= 	8'hE0;                                                //
						i2c_wr_data		<=	8'b11010000;                                          //
						i2c_wr_en		<=	1'b1;                                                 //
						i2c_valid		<=	1'b1;                                                 //
						state	<= STATE_FIXED_REG_SET_7_WAIT;                                    //
					end																			  //
				end                                                                               //
				STATE_FIXED_REG_SET_7_WAIT : begin                                                //
					i2c_valid  <= 1'b0;                                                           //
					if(i2c_done) begin                                                            //
						state <= STATE_FIXED_REG_SET_8;                                           //
					end																			  //
				end                                                                               //
																								  //
				STATE_FIXED_REG_SET_8 : begin                                                     //
					if(i2c_ready) begin                                                           //
						i2c_slave_addr 	<= 	7'b0111001;                                           //
						i2c_base_addr	<= 	8'hF9;                                                //
						i2c_wr_data		<=	8'b00000000;                                          //
						i2c_wr_en		<=	1'b1;                                                 //
						i2c_valid		<=	1'b1;                                                 //
						state	<= STATE_FIXED_REG_SET_8_WAIT;                                    //
					end																			  //
				end                                                                               //
				STATE_FIXED_REG_SET_8_WAIT : begin                                                //
					i2c_valid  <= 1'b0;                                                           //
					if(i2c_done) begin                                                            //
						state <= STATE_INPUT_MODE_SET_1;                                          //
					end																			  //
				end                                                                               //
				//--------------------------------------------------------------------------------//
				
				
				//----------Set input mode registers----------------------------------------------// //
																								  // //
				// Input ID = 1 (4:2:2 separate syncs)										  	  // //
				STATE_INPUT_MODE_SET_1 : begin                                                    // //
					if(i2c_ready) begin                                                           // //
						i2c_slave_addr 	<= 	7'b0111001;                                           // //
						i2c_base_addr	<= 	8'h15;                                                // //
						i2c_wr_data		<=	8'b00000001;                                          // //
						i2c_wr_en		<=	1'b1;                                                 // //
						i2c_valid		<=	1'b1;                                                 // //
						state	<= STATE_INPUT_MODE_SET_1_WAIT;                                   // //
					end																			  // //
				end                                                                               // //
				STATE_INPUT_MODE_SET_1_WAIT : begin                                               // //
					i2c_valid  <= 1'b0;                                                           // //
					if(i2c_done) begin                                                            // //
						state <= STATE_INPUT_MODE_SET_2;                                          // //
					end																			  // //
				end                                                                               // //
																								  // //
				// output format(4:2:2), input style and colour depth 							  // //
				STATE_INPUT_MODE_SET_2 : begin                                                    // //
					if(i2c_ready) begin                                                           // //
						i2c_slave_addr 	<= 	7'b0111001;                                           // //
						i2c_base_addr	<= 	8'h16;                                                // //
						i2c_wr_data		<=	8'b10111101;                                          // //
						i2c_wr_en		<=	1'b1;                                                 // //
						i2c_valid		<=	1'b1;                                                 // //
						state	<= STATE_INPUT_MODE_SET_2_WAIT;                                   // //
					end																			  // //
				end                                                                               // //
				STATE_INPUT_MODE_SET_2_WAIT : begin                                               // //
					i2c_valid  <= 1'b0;                                                           // //
					if(i2c_done) begin                                                            // //
						state <= STATE_INPUT_MODE_SET_3;                                          // //
					end																			  // //
				end                                                                               // //
																								  // //
				// Data input justification (even justified, table 17) 							  // //
				STATE_INPUT_MODE_SET_3 : begin                                                    // //
					if(i2c_ready) begin                                                           // //
						i2c_slave_addr 	<= 	7'b0111001;                                           // //
						i2c_base_addr	<= 	8'h48;                                                // //
						i2c_wr_data		<=	8'b00000000;                                          // //
						i2c_wr_en		<=	1'b1;                                                 // //
						i2c_valid		<=	1'b1;                                                 // //
						state	<= STATE_INPUT_MODE_SET_3_WAIT;                                   // //
					end																			  // //
				end                                                                               // //
				STATE_INPUT_MODE_SET_3_WAIT : begin                                               // //
					i2c_valid  <= 1'b0;                                                           // //
					if(i2c_done) begin                                                            // //
						state <= STATE_INPUT_MODE_SET_4;                                          // //
					end																			  // //
				end                                                                               // //
																								  // //
				// Input aspect ratio (16:9) 							  						  // //
				STATE_INPUT_MODE_SET_4 : begin                                                    // //
					if(i2c_ready) begin                                                           // //
						i2c_slave_addr 	<= 	7'b0111001;                                           // //
						i2c_base_addr	<= 	8'h17;                                                // //
						i2c_wr_data		<=	8'b00000010;                                          // //
						i2c_wr_en		<=	1'b1;                                                 // //
						i2c_valid		<=	1'b1;                                                 // //
						state	<= STATE_INPUT_MODE_SET_4_WAIT;                                   // //
					end																			  // //
				end                                                                               // //
				STATE_INPUT_MODE_SET_4_WAIT : begin                                               // //
					i2c_valid  <= 1'b0;                                                           // //
					if(i2c_done) begin                                                            // //
						state <= STATE_OUTPUT_MODE_SET_1;                                         // //
					end																			  // //
				end                                                                               // //
																								  // //
				//--------------------------------------------------------------------------------// //
				
				
				//----------Set output mode registers---------------------------------------------//
																								  //
				// Set HDMI mode output  							  						      //
				STATE_OUTPUT_MODE_SET_1 : begin                                                   //
					if(i2c_ready) begin                                                           //
						i2c_slave_addr 	<= 	7'b0111001;                                           //
						i2c_base_addr	<= 	8'hAF;                                                //
						i2c_wr_data		<=	8'b00010110;                                          //
						i2c_wr_en		<=	1'b1;                                                 //
						i2c_valid		<=	1'b1;                                                 //
						state	<= STATE_OUTPUT_MODE_SET_1_WAIT;                                  //
					end																			  //
				end                                                                               //
				STATE_OUTPUT_MODE_SET_1_WAIT : begin                                              //
					i2c_valid  <= 1'b0;                                                           //
					if(i2c_done) begin                                                            //
						state <= STATE_OUTPUT_MODE_SET_4;                                         //
					end																			  //
				end                                                                               //
																								  //
				// Activate AVI packet update  							  	  					  //
				STATE_OUTPUT_MODE_SET_4 : begin                                                   //
					if(i2c_ready) begin                                                           //
						i2c_slave_addr 	<= 	7'b0111001;                                           //
						i2c_base_addr	<= 	8'h4A;                                                //
						i2c_wr_data		<=	8'b11000000;                                          //
						i2c_wr_en		<=	1'b1;                                                 //
						i2c_valid		<=	1'b1;                                                 //
						state	<= STATE_OUTPUT_MODE_SET_4_WAIT;                                  //
					end																			  //
				end                                                                               //
				STATE_OUTPUT_MODE_SET_4_WAIT : begin                                              //
					i2c_valid  <= 1'b0;                                                           //
					if(i2c_done) begin                                                            //
						state <= STATE_OUTPUT_MODE_SET_2;                                         //
					end																			  //
				end                                                                               //
																								  //																				  
				// Set AVI info frame output format (YCbCr 4:2:2)  							  	  //
				STATE_OUTPUT_MODE_SET_2 : begin                                                   //
					if(i2c_ready) begin                                                           //
						i2c_slave_addr 	<= 	7'b0111001;                                           //
						i2c_base_addr	<= 	8'h55;                                                //
						i2c_wr_data		<=	8'b00100000;                                          //
						i2c_wr_en		<=	1'b1;                                                 //
						i2c_valid		<=	1'b1;                                                 //
						state	<= STATE_OUTPUT_MODE_SET_2_WAIT;                                  //
					end																			  //
				end                                                                               //
				STATE_OUTPUT_MODE_SET_2_WAIT : begin                                              //
					i2c_valid  <= 1'b0;                                                           //
					if(i2c_done) begin                                                            //
						state <= STATE_OUTPUT_MODE_SET_3;                                         //
					end																			  //
				end                                                                               //
																								  //
				// Set AVI info frame output aspect ratio (16:9)  							  	  //
				STATE_OUTPUT_MODE_SET_3 : begin                                                   //
					if(i2c_ready) begin                                                           //
						i2c_slave_addr 	<= 	7'b0111001;                                           //
						i2c_base_addr	<= 	8'h56;                                                //
						i2c_wr_data		<=	8'b00100000;                                          //
						i2c_wr_en		<=	1'b1;                                                 //
						i2c_valid		<=	1'b1;                                                 //
						state	<= STATE_OUTPUT_MODE_SET_3_WAIT;                                  //
					end																			  //
				end                                                                               //
				STATE_OUTPUT_MODE_SET_3_WAIT : begin                                              //
					i2c_valid  <= 1'b0;                                                           //
					if(i2c_done) begin                                                            //
						state <= STATE_OUTPUT_MODE_SET_5;                                         //
					end																			  //
				end                                                                               //
																								  //
				// Deactivate AVI packet update  							  	  				  //
				STATE_OUTPUT_MODE_SET_5 : begin                                                   //
					if(i2c_ready) begin                                                           //
						i2c_slave_addr 	<= 	7'b0111001;                                           //
						i2c_base_addr	<= 	8'h4A;                                                //
						i2c_wr_data		<=	8'b10000000;                                          //
						i2c_wr_en		<=	1'b1;                                                 //
						i2c_valid		<=	1'b1;                                                 //
						state	<= STATE_OUTPUT_MODE_SET_5_WAIT;                                  //
					end																			  //
				end                                                                               //
				STATE_OUTPUT_MODE_SET_5_WAIT : begin                                              //
					i2c_valid  <= 1'b0;                                                           //
					if(i2c_done) begin                                                            //
						state <= STATE_VIDEO_TX;                                         		  //
						Y_data	<= 8'd76;
						C_data	<= 8'd230;  
						pclk_en <= 1'b1;
					end																			  //
				end                                                                               //
				//--------------------------------------------------------------------------------//
				
				
				
				//-----------Transmit video data--------------------------------------------------// //
				
				STATE_VIDEO_TX : begin
					D_out <= {Y_data,C_data,20'b0};		
					state <= STATE_VIDEO_TX_1; 			// coincide with falling edge of pclk 
				end
				STATE_VIDEO_TX_1 : begin
					if(DE) begin
						Y_data	<= 8'd76;				
						C_data	<= 8'd84;               
						state <= STATE_VIDEO_TX_2;		// coincide with rising edge of pclk
					end
				end
				STATE_VIDEO_TX_2 : begin              
					D_out <= {Y_data,C_data,20'b0};		
					state <= STATE_VIDEO_TX_3; 			// coincide with falling edge of pclk 
				end
				STATE_VIDEO_TX_3 : begin
					if(DE) begin
						if(disp_count == 20'd230398 ) begin		// 230398 for 120 rows
							disp_count <= 20'd0;
							Y_data	<= 8'd153;					
							C_data	<= 8'd46; 
							state <= STATE_VIDEO_TX_4;
						end
						else begin
							disp_count <= disp_count + 2'd2;
							Y_data	<= 8'd76;					
							C_data	<= 8'd230; 
							state <= STATE_VIDEO_TX;		// coincide with rising edge of pclk
						end	
					end
				end
				
				
				STATE_VIDEO_TX_4 : begin
					D_out <= {Y_data,C_data,20'b0};		
					state <= STATE_VIDEO_TX_5; 			// coincide with falling edge of pclk 
				end
				STATE_VIDEO_TX_5 : begin
					if(DE) begin
						Y_data	<= 8'd153;				
						C_data	<= 8'd25;               
						state <= STATE_VIDEO_TX_6;		// coincide with rising edge of pclk
					end
				end
				STATE_VIDEO_TX_6 : begin              
					D_out <= {Y_data,C_data,20'b0};		
					state <= STATE_VIDEO_TX_7; 			// coincide with falling edge of pclk 
				end
				STATE_VIDEO_TX_7 : begin
					if(DE) begin
						if(disp_count == 20'd230398 ) begin
							disp_count <= 20'd0;
							Y_data	<= 8'd83;					
							C_data	<= 8'd86; 
							state <= STATE_VIDEO_TX_8;
						end
						else begin
							disp_count <= disp_count + 2'd2;
							Y_data	<= 8'd153;					
							C_data	<= 8'd46; 
							state <= STATE_VIDEO_TX_4;		// coincide with rising edge of pclk
						end	
					end
				end
				
				
				STATE_VIDEO_TX_8 : begin
					D_out <= {Y_data,C_data,20'b0};		
					state <= STATE_VIDEO_TX_9; 			// coincide with falling edge of pclk 
				end
				STATE_VIDEO_TX_9 : begin
					if(DE) begin
						Y_data	<= 8'd83;				
						C_data	<= 8'd250;               
						state <= STATE_VIDEO_TX_10;		// coincide with rising edge of pclk
					end
				end
				STATE_VIDEO_TX_10 : begin              
					D_out <= {Y_data,C_data,20'b0};		
					state <= STATE_VIDEO_TX_11; 			// coincide with falling edge of pclk 
				end
				STATE_VIDEO_TX_11 : begin
					if(DE) begin
						if(disp_count == 20'd230398 ) begin
							disp_count <= 20'd0;
							Y_data	<= 8'd76;					
							C_data	<= 8'd230; 
							state <= STATE_VIDEO_TX;
						end
						else begin
							disp_count <= disp_count + 2'd2;
							Y_data	<= 8'd83;					
							C_data	<= 8'd86; 
							state <= STATE_VIDEO_TX_8;		// coincide with rising edge of pclk
						end	
					end
				end
				
				
				
				
			endcase
		end
	end
	
	

endmodule
