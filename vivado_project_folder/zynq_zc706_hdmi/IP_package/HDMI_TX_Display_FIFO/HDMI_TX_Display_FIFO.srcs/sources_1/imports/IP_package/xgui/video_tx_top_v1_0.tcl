# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
	set Page0 [ipgui::add_page $IPINST -name "Page 0" -layout vertical]
	set Component_Name [ipgui::add_param $IPINST -parent $Page0 -name Component_Name]
	set VIDEO_OUT [ipgui::add_param $IPINST -parent $Page0 -name VIDEO_OUT]
	set FREQ_OUT [ipgui::add_param $IPINST -parent $Page0 -name FREQ_OUT]
	set FREQ_IN [ipgui::add_param $IPINST -parent $Page0 -name FREQ_IN]
}

proc update_PARAM_VALUE.VIDEO_OUT { PARAM_VALUE.VIDEO_OUT } {
	# Procedure called to update VIDEO_OUT when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.VIDEO_OUT { PARAM_VALUE.VIDEO_OUT } {
	# Procedure called to validate VIDEO_OUT
	return true
}

proc update_PARAM_VALUE.FREQ_OUT { PARAM_VALUE.FREQ_OUT } {
	# Procedure called to update FREQ_OUT when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.FREQ_OUT { PARAM_VALUE.FREQ_OUT } {
	# Procedure called to validate FREQ_OUT
	return true
}

proc update_PARAM_VALUE.FREQ_IN { PARAM_VALUE.FREQ_IN } {
	# Procedure called to update FREQ_IN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.FREQ_IN { PARAM_VALUE.FREQ_IN } {
	# Procedure called to validate FREQ_IN
	return true
}


proc update_MODELPARAM_VALUE.FREQ_IN { MODELPARAM_VALUE.FREQ_IN PARAM_VALUE.FREQ_IN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.FREQ_IN}] ${MODELPARAM_VALUE.FREQ_IN}
}

proc update_MODELPARAM_VALUE.FREQ_OUT { MODELPARAM_VALUE.FREQ_OUT PARAM_VALUE.FREQ_OUT } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.FREQ_OUT}] ${MODELPARAM_VALUE.FREQ_OUT}
}

proc update_MODELPARAM_VALUE.VIDEO_OUT { MODELPARAM_VALUE.VIDEO_OUT PARAM_VALUE.VIDEO_OUT } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.VIDEO_OUT}] ${MODELPARAM_VALUE.VIDEO_OUT}
}

