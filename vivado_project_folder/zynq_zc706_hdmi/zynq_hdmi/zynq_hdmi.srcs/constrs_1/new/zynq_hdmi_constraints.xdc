

set_property PACKAGE_PIN U24 [get_ports {Data_out[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[0]}]
set_property PACKAGE_PIN T22 [get_ports {Data_out[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[1]}]
set_property PACKAGE_PIN R23 [get_ports {Data_out[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[2]}]
set_property PACKAGE_PIN AA25 [get_ports {Data_out[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[3]}]
set_property PACKAGE_PIN AE28 [get_ports {Data_out[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[4]}]
set_property PACKAGE_PIN T23 [get_ports {Data_out[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[5]}]
set_property PACKAGE_PIN AB25 [get_ports {Data_out[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[6]}]
set_property PACKAGE_PIN T27 [get_ports {Data_out[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[7]}]
set_property PACKAGE_PIN AD26 [get_ports {Data_out[8]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[8]}]
set_property PACKAGE_PIN AB26 [get_ports {Data_out[9]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[9]}]
set_property PACKAGE_PIN AA28 [get_ports {Data_out[10]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[10]}]
set_property PACKAGE_PIN AC26 [get_ports {Data_out[11]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[11]}]
set_property PACKAGE_PIN AE30 [get_ports {Data_out[12]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[12]}]
set_property PACKAGE_PIN Y25 [get_ports {Data_out[13]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[13]}]
set_property PACKAGE_PIN AA29 [get_ports {Data_out[14]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[14]}]
set_property PACKAGE_PIN AD30 [get_ports {Data_out[15]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[15]}]
set_property PACKAGE_PIN Y28 [get_ports {Data_out[16]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[16]}]
set_property PACKAGE_PIN AF28 [get_ports {Data_out[17]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[17]}]
set_property PACKAGE_PIN V22 [get_ports {Data_out[18]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[18]}]
set_property PACKAGE_PIN AA27 [get_ports {Data_out[19]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[19]}]
set_property PACKAGE_PIN U22 [get_ports {Data_out[20]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[20]}]
set_property PACKAGE_PIN N28 [get_ports {Data_out[21]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[21]}]
set_property PACKAGE_PIN V21 [get_ports {Data_out[22]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[22]}]
set_property PACKAGE_PIN AC22 [get_ports {Data_out[23]}]
set_property IOSTANDARD LVCMOS25 [get_ports {Data_out[23]}]

set_property PACKAGE_PIN P28 [get_ports pclk]
set_property IOSTANDARD LVCMOS25 [get_ports pclk]

set_property PACKAGE_PIN R22 [get_ports hsync]
set_property IOSTANDARD LVCMOS25 [get_ports hsync]

set_property PACKAGE_PIN U21 [get_ports vsync]
set_property IOSTANDARD LVCMOS25 [get_ports vsync]

set_property PACKAGE_PIN V24 [get_ports DE]
set_property IOSTANDARD LVCMOS25 [get_ports DE]

set_property PACKAGE_PIN AJ14 [get_ports scl_pad]
set_property IOSTANDARD LVCMOS25 [get_ports scl_pad]
set_property PACKAGE_PIN AJ18 [get_ports sda_pad]
set_property IOSTANDARD LVCMOS25 [get_ports sda_pad]

#set_property PACKAGE_PIN A8 [get_ports aux_reset_in]
#set_property IOSTANDARD LVCMOS15 [get_ports aux_reset_in]







connect_debug_port u_ila_0/probe3 [get_nets [list hdmi_design_i/video_tx_top_0_pclk]]

