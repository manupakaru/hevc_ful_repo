`timescale 1ns / 1ps
module dummy_display_fifo_driver(
    clk,
    reset,
    
    S00_AXI_AWID,
    S00_AXI_AWADDR,
    S00_AXI_AWLEN,
    S00_AXI_AWSIZE,
    S00_AXI_AWBURST,
    S00_AXI_AWLOCK,
    S00_AXI_AWCACHE,
    S00_AXI_AWPROT,
    S00_AXI_AWQOS,
    S00_AXI_AWVALID,
    S00_AXI_AWREADY,
    S00_AXI_WDATA,
    S00_AXI_WSTRB,
    S00_AXI_WLAST,
    S00_AXI_WVALID,
    S00_AXI_WREADY,
    S00_AXI_BID,
    S00_AXI_BRESP,
    S00_AXI_BVALID,
    S00_AXI_BREADY,
    S00_AXI_ARID,
    S00_AXI_ARADDR,
    S00_AXI_ARLEN,
    S00_AXI_ARSIZE,
    S00_AXI_ARBURST,
    S00_AXI_ARLOCK,
    S00_AXI_ARCACHE,
    S00_AXI_ARPROT,
    S00_AXI_ARQOS,
    S00_AXI_ARVALID,
    S00_AXI_ARREADY,
    S00_AXI_RID,
    S00_AXI_RDATA,
    S00_AXI_RRESP,
    S00_AXI_RLAST,
    S00_AXI_RVALID,
    S00_AXI_RREADY,
    
	//fifo interface
	fifo_prog_full_in		,
	fifo_wr_en_out	    ,
	fifo_data_out		,
	
	poc_out,

    pic_width_out,
    pic_height_out


    
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
`include "pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------

	
	parameter YY_WIDTH = PIXEL_WIDTH*DBF_OUT_Y_BLOCK_SIZE*DBF_OUT_Y_BLOCK_SIZE;
	parameter CH_WIDTH = PIXEL_WIDTH*DBF_OUT_CH_BLOCK_SIZE*DBF_OUT_CH_BLOCK_SIZE;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    // localparam  STATE_AXI_WRITE_ADDRESS_SEND = 1;
    // localparam  STATE_AXI_WRITE_ADDRESS_SEND_WAIT = 2;
    // localparam  STATE_AXI_WRITE_WAIT = 3;
    // localparam  STATE_AXI_WRITE_RESP = 4;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------

//input             S00_AXI_ACLK;
input [12-1 : 0]     S00_AXI_AWID;
input [16-1 : 0]     S00_AXI_AWADDR;
input [7 : 0]     S00_AXI_AWLEN;
input [2 : 0]     S00_AXI_AWSIZE;
input [1 : 0]     S00_AXI_AWBURST;
input             S00_AXI_AWLOCK;
input [3 : 0]     S00_AXI_AWCACHE;
input [2 : 0]     S00_AXI_AWPROT;
input [3 : 0]     S00_AXI_AWQOS;
input             S00_AXI_AWVALID;
output             S00_AXI_AWREADY; 
input [32-1 : 0]     S00_AXI_WDATA;
input [32/8 : 0]     S00_AXI_WSTRB;
input             S00_AXI_WLAST;
input             S00_AXI_WVALID;
output              S00_AXI_WREADY; 
output [12-1 : 0]     S00_AXI_BID;  
output [1 : 0]     S00_AXI_BRESP;   
output             S00_AXI_BVALID;  
input             S00_AXI_BREADY;
input [12-1 : 0]     S00_AXI_ARID;
input [16-1 : 0]     S00_AXI_ARADDR;
input [7 : 0]     S00_AXI_ARLEN;
input [2 : 0]     S00_AXI_ARSIZE;
input [1 : 0]     S00_AXI_ARBURST;
input             S00_AXI_ARLOCK;
input [3 : 0]     S00_AXI_ARCACHE;
input [2 : 0]     S00_AXI_ARPROT;
input [3 : 0]     S00_AXI_ARQOS;
input             S00_AXI_ARVALID;
output             S00_AXI_ARREADY;
output [12-1 : 0]     S00_AXI_RID; 
output [32-1 : 0] S00_AXI_RDATA;
output [1 : 0]     S00_AXI_RRESP;
output             S00_AXI_RLAST;
output             S00_AXI_RVALID; 
input             S00_AXI_RREADY;

    input clk;
    input reset;

    //luma sao fifo interface
	input 											fifo_prog_full_in	;
	output reg 										fifo_wr_en_out		;
	output  		[SAO_OUT_FIFO_WIDTH-1:0]		fifo_data_out		;
	
	output reg [3:0]								poc_out				;

	
    output     [PIC_WIDTH_WIDTH-1:0]    pic_width_out;
    output     [PIC_WIDTH_WIDTH-1:0]    pic_height_out;


//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

    
	reg [X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1: 0]				Xc_out_8x8;
	reg [X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1: 0]				Yc_out_8x8;	
	
	reg [PIXEL_WIDTH*DBF_OUT_Y_BLOCK_SIZE*DBF_OUT_Y_BLOCK_SIZE -1:0] 	yy_pixels_8x8;
	reg [PIXEL_WIDTH*DBF_OUT_CH_BLOCK_SIZE*DBF_OUT_CH_BLOCK_SIZE -1:0] cb_pixels_8x8;
	reg [PIXEL_WIDTH*DBF_OUT_CH_BLOCK_SIZE*DBF_OUT_CH_BLOCK_SIZE -1:0] cr_pixels_8x8;
	
	// reg [AXI_ADDR_WDTH -1:0]			current_pic_dpb_base_addr_d;
	// reg [AXI_ADDR_WDTH -1:0]			current_pic_dpb_base_addr_use;
  
    wire        [CTB_SIZE_WIDTH - LOG2_MIN_DU_SIZE - 1:0]      		y_8x8_in_ctu;
    wire        [CTB_SIZE_WIDTH - LOG2_MIN_DU_SIZE - 1:0]         	x_8x8_in_ctu;
    wire        [X11_ADDR_WDTH - CTB_SIZE_WIDTH - 1:0]          	x_ctu;
    wire        [X11_ADDR_WDTH - CTB_SIZE_WIDTH - 1:0]         		y_ctu;
    
    reg 	start =0;
	
	assign pic_width_out = 1920;
	assign pic_height_out = 1080;
	integer wr_en_counter;
	integer wait_counter;
	
   assign S00_AXI_RVALID = 0;
    assign S00_AXI_RID = 0;
    assign S00_AXI_BVALID = 1;
    assign S00_AXI_BRESP = 0;
    assign S00_AXI_BID = S00_AXI_AWID;
    assign S00_AXI_WREADY = 1;
    assign S00_AXI_AWREADY = 1;	
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
	wire [X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1:0] pic_width_by_8 = pic_width_out[X11_ADDR_WDTH -1:LOG2_MIN_DU_SIZE];

					
	assign y_8x8_in_ctu = Yc_out_8x8[CTB_SIZE_WIDTH - LOG2_MIN_DU_SIZE - 1:0];
	assign x_8x8_in_ctu = Xc_out_8x8[CTB_SIZE_WIDTH - LOG2_MIN_DU_SIZE - 1:0];
	
	assign x_ctu = Xc_out_8x8[X11_ADDR_WDTH - LOG2_MIN_DU_SIZE - 1:CTB_SIZE_WIDTH - LOG2_MIN_DU_SIZE ];
	assign y_ctu = Yc_out_8x8[X11_ADDR_WDTH - LOG2_MIN_DU_SIZE - 1:CTB_SIZE_WIDTH - LOG2_MIN_DU_SIZE ];
		
	assign fifo_data_out = {Xc_out_8x8,Yc_out_8x8,yy_pixels_8x8,cb_pixels_8x8,cr_pixels_8x8};

always@(posedge clk or posedge reset) begin
	if(reset) begin
		start <= 0;
		
	end
	else begin
        if(S00_AXI_AWVALID) begin
            start <=1;
        end			
	end	
end


	always@(posedge clk ) begin
		if(start ==0 ) begin
			Xc_out_8x8 <= 0;
			Yc_out_8x8 <= 0;
			poc_out <= 0;
			yy_pixels_8x8 <= {YY_WIDTH{1'b0}};
			cb_pixels_8x8 <= {CH_WIDTH{1'b0}};
			cr_pixels_8x8 <= {CH_WIDTH{1'b0}};
			fifo_wr_en_out <= 0;
			wr_en_counter <= 0;
			wait_counter <= 0;
		end
		else begin
			if(wait_counter < 100) begin
				wait_counter <= wait_counter + 1;
				if(wait_counter == 0) begin
					fifo_wr_en_out <= 1;
				end
				else begin
					fifo_wr_en_out <= 0;
				end
					
			end
			else begin
				if(fifo_prog_full_in ==0) begin
					fifo_wr_en_out <= 1;
					wr_en_counter <= wr_en_counter + 1;
					if( (Xc_out_8x8[X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1:0] == pic_width_by_8 -1) && (Yc_out_8x8[X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1:0] == pic_height_out[X11_ADDR_WDTH -1:LOG2_MIN_DU_SIZE] -1))begin
						Xc_out_8x8 <= 0;
						Yc_out_8x8 <= 0;
						poc_out <= poc_out + 1;
						yy_pixels_8x8 <= {{{YY_WIDTH/2-4}{1'b0}},poc_out,{{YY_WIDTH/2-4}{1'b0}},poc_out};
						cb_pixels_8x8 <= {{{CH_WIDTH/2-4}{1'b0}},poc_out,{{CH_WIDTH/2-4}{1'b0}},poc_out};
						cr_pixels_8x8 <= {{{CH_WIDTH/2-4}{1'b0}},poc_out,{{CH_WIDTH/2-4}{1'b0}},poc_out};
					end
					else begin
						if((Xc_out_8x8[X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1:0] == pic_width_by_8 -1) && (Yc_out_8x8[2:0] == 7)) begin
							Xc_out_8x8 <= 0;
							Yc_out_8x8[X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1:3] <= Yc_out_8x8[X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1:3] + 1;
							Yc_out_8x8[2:0] <= 0;
							yy_pixels_8x8[7:0] <= yy_pixels_8x8[7:0] + 100;
							yy_pixels_8x8[YY_WIDTH-1:8] <= yy_pixels_8x8[YY_WIDTH-8 -1:0];
							cb_pixels_8x8[7:0] <= cb_pixels_8x8[7:0] + 50;
							cb_pixels_8x8[CH_WIDTH-1:8] <= cb_pixels_8x8[CH_WIDTH-8 -1:0];
							cr_pixels_8x8[7:0] <= cr_pixels_8x8[7:0] - 50;
							cr_pixels_8x8[CH_WIDTH-1:8] <= cr_pixels_8x8[CH_WIDTH-8 -1:0];
						end
						else begin
							if(( (Yc_out_8x8[2:0] == 7) || (Yc_out_8x8[X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1:0] == pic_height_out[X11_ADDR_WDTH -1:LOG2_MIN_DU_SIZE] -1) ) && (Xc_out_8x8[2:0] == 7)) begin
								Yc_out_8x8[2:0] <= 0;
								Xc_out_8x8[2:0] <= 0;
								Xc_out_8x8[X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1:3] <= Xc_out_8x8[X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1:3] + 1;
								yy_pixels_8x8[7:0] <= yy_pixels_8x8[7:0] + 2;
								yy_pixels_8x8[YY_WIDTH-1:8] <= yy_pixels_8x8[YY_WIDTH-8 -1:0];
								cb_pixels_8x8[7:0] <= cb_pixels_8x8[7:0] + 1;
								cb_pixels_8x8[CH_WIDTH-1:8] <= cb_pixels_8x8[CH_WIDTH-8 -1:0];
								cr_pixels_8x8[7:0] <= cr_pixels_8x8[7:0] - 1;
								cr_pixels_8x8[CH_WIDTH-1:8] <= cr_pixels_8x8[CH_WIDTH-8 -1:0];
							end
							else begin
								if((Xc_out_8x8[2:0] == 7)) begin
									Xc_out_8x8[2:0] <= 0;
									Yc_out_8x8[2:0] <= Yc_out_8x8[2:0] + 1;
								end
								else begin
									Xc_out_8x8[2:0] <= Xc_out_8x8[2:0] + 1;
								end
							end
						end
					end
				end
				else begin
					fifo_wr_en_out <= 0;
				end
			
			end
		end
	end
	
	 // always@(*) begin
		// if(reset) begin
			// fifo_wr_en_out = 0;
		// end
		// else begin
			// fifo_wr_en_out = 0;
			// if(wait_counter < 200) begin
			// end
			// else begin
				// if(!fifo_prog_full_in) begin
					// fifo_wr_en_out = 1;
				// end
				// else begin
					// fifo_wr_en_out = 0;
				// end
			// end
			
		// end
	 // end

    
    

endmodule