//Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2014.2 (win64) Build 932637 Wed Jun 11 13:33:10 MDT 2014
//Date        : Fri Feb 27 11:08:25 2015
//Host        : GCLAP4 running 64-bit major release  (build 9200)
//Command     : generate_target dp_system.bd
//Design      : dp_system
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module axi4_lite_peripherals_imp_8FS6LK
   (APB_M_paddr,
    APB_M_penable,
    APB_M_prdata,
    APB_M_pready,
    APB_M_psel,
    APB_M_pslverr,
    APB_M_pwdata,
    APB_M_pwrite,
    AXI4_LITE_araddr,
    AXI4_LITE_arready,
    AXI4_LITE_arvalid,
    AXI4_LITE_awaddr,
    AXI4_LITE_awready,
    AXI4_LITE_awvalid,
    AXI4_LITE_bready,
    AXI4_LITE_bresp,
    AXI4_LITE_bvalid,
    AXI4_LITE_rdata,
    AXI4_LITE_rready,
    AXI4_LITE_rresp,
    AXI4_LITE_rvalid,
    AXI4_LITE_wdata,
    AXI4_LITE_wready,
    AXI4_LITE_wvalid,
    DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    FCLK_RESET0_N,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    IIC_scl_i,
    IIC_scl_o,
    IIC_scl_t,
    IIC_sda_i,
    IIC_sda_o,
    IIC_sda_t,
    In1,
    M_AXI_GP0_araddr,
    M_AXI_GP0_arburst,
    M_AXI_GP0_arcache,
    M_AXI_GP0_arid,
    M_AXI_GP0_arlen,
    M_AXI_GP0_arlock,
    M_AXI_GP0_arprot,
    M_AXI_GP0_arqos,
    M_AXI_GP0_arready,
    M_AXI_GP0_arsize,
    M_AXI_GP0_arvalid,
    M_AXI_GP0_awaddr,
    M_AXI_GP0_awburst,
    M_AXI_GP0_awcache,
    M_AXI_GP0_awid,
    M_AXI_GP0_awlen,
    M_AXI_GP0_awlock,
    M_AXI_GP0_awprot,
    M_AXI_GP0_awqos,
    M_AXI_GP0_awready,
    M_AXI_GP0_awsize,
    M_AXI_GP0_awvalid,
    M_AXI_GP0_bid,
    M_AXI_GP0_bready,
    M_AXI_GP0_bresp,
    M_AXI_GP0_bvalid,
    M_AXI_GP0_rdata,
    M_AXI_GP0_rid,
    M_AXI_GP0_rlast,
    M_AXI_GP0_rready,
    M_AXI_GP0_rresp,
    M_AXI_GP0_rvalid,
    M_AXI_GP0_wdata,
    M_AXI_GP0_wid,
    M_AXI_GP0_wlast,
    M_AXI_GP0_wready,
    M_AXI_GP0_wstrb,
    M_AXI_GP0_wvalid,
    S_AXI3_araddr,
    S_AXI3_arready,
    S_AXI3_arvalid,
    S_AXI3_awaddr,
    S_AXI3_awready,
    S_AXI3_awvalid,
    S_AXI3_bready,
    S_AXI3_bresp,
    S_AXI3_bvalid,
    S_AXI3_rdata,
    S_AXI3_rready,
    S_AXI3_rresp,
    S_AXI3_rvalid,
    S_AXI3_wdata,
    S_AXI3_wready,
    S_AXI3_wstrb,
    S_AXI3_wvalid,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid,
    bus_struct_reset,
    dcm_locked,
    gpo,
    interconnect_aresetn,
    peripheral_aresetn,
    s_axi2_araddr,
    s_axi2_arready,
    s_axi2_arvalid,
    s_axi2_awaddr,
    s_axi2_awready,
    s_axi2_awvalid,
    s_axi2_bready,
    s_axi2_bresp,
    s_axi2_bvalid,
    s_axi2_rdata,
    s_axi2_rready,
    s_axi2_rresp,
    s_axi2_rvalid,
    s_axi2_wdata,
    s_axi2_wready,
    s_axi2_wstrb,
    s_axi2_wvalid,
    s_axi_aclk);
  output [11:0]APB_M_paddr;
  output APB_M_penable;
  input [31:0]APB_M_prdata;
  input APB_M_pready;
  output APB_M_psel;
  input APB_M_pslverr;
  output [31:0]APB_M_pwdata;
  output APB_M_pwrite;
  input [31:0]AXI4_LITE_araddr;
  output [0:0]AXI4_LITE_arready;
  input [0:0]AXI4_LITE_arvalid;
  input [31:0]AXI4_LITE_awaddr;
  output [0:0]AXI4_LITE_awready;
  input [0:0]AXI4_LITE_awvalid;
  input [0:0]AXI4_LITE_bready;
  output [1:0]AXI4_LITE_bresp;
  output [0:0]AXI4_LITE_bvalid;
  output [31:0]AXI4_LITE_rdata;
  input [0:0]AXI4_LITE_rready;
  output [1:0]AXI4_LITE_rresp;
  output [0:0]AXI4_LITE_rvalid;
  input [31:0]AXI4_LITE_wdata;
  output [0:0]AXI4_LITE_wready;
  input [0:0]AXI4_LITE_wvalid;
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  output FCLK_RESET0_N;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  input IIC_scl_i;
  output IIC_scl_o;
  output IIC_scl_t;
  input IIC_sda_i;
  output IIC_sda_o;
  output IIC_sda_t;
  input [0:0]In1;
  output [31:0]M_AXI_GP0_araddr;
  output [1:0]M_AXI_GP0_arburst;
  output [3:0]M_AXI_GP0_arcache;
  output [11:0]M_AXI_GP0_arid;
  output [3:0]M_AXI_GP0_arlen;
  output [1:0]M_AXI_GP0_arlock;
  output [2:0]M_AXI_GP0_arprot;
  output [3:0]M_AXI_GP0_arqos;
  input M_AXI_GP0_arready;
  output [2:0]M_AXI_GP0_arsize;
  output M_AXI_GP0_arvalid;
  output [31:0]M_AXI_GP0_awaddr;
  output [1:0]M_AXI_GP0_awburst;
  output [3:0]M_AXI_GP0_awcache;
  output [11:0]M_AXI_GP0_awid;
  output [3:0]M_AXI_GP0_awlen;
  output [1:0]M_AXI_GP0_awlock;
  output [2:0]M_AXI_GP0_awprot;
  output [3:0]M_AXI_GP0_awqos;
  input M_AXI_GP0_awready;
  output [2:0]M_AXI_GP0_awsize;
  output M_AXI_GP0_awvalid;
  input [11:0]M_AXI_GP0_bid;
  output M_AXI_GP0_bready;
  input [1:0]M_AXI_GP0_bresp;
  input M_AXI_GP0_bvalid;
  input [31:0]M_AXI_GP0_rdata;
  input [11:0]M_AXI_GP0_rid;
  input M_AXI_GP0_rlast;
  output M_AXI_GP0_rready;
  input [1:0]M_AXI_GP0_rresp;
  input M_AXI_GP0_rvalid;
  output [31:0]M_AXI_GP0_wdata;
  output [11:0]M_AXI_GP0_wid;
  output M_AXI_GP0_wlast;
  input M_AXI_GP0_wready;
  output [3:0]M_AXI_GP0_wstrb;
  output M_AXI_GP0_wvalid;
  input [4:0]S_AXI3_araddr;
  output [0:0]S_AXI3_arready;
  input [0:0]S_AXI3_arvalid;
  input [4:0]S_AXI3_awaddr;
  output [0:0]S_AXI3_awready;
  input [0:0]S_AXI3_awvalid;
  input [0:0]S_AXI3_bready;
  output [1:0]S_AXI3_bresp;
  output [0:0]S_AXI3_bvalid;
  output [31:0]S_AXI3_rdata;
  input [0:0]S_AXI3_rready;
  output [1:0]S_AXI3_rresp;
  output [0:0]S_AXI3_rvalid;
  input [31:0]S_AXI3_wdata;
  output [0:0]S_AXI3_wready;
  input [3:0]S_AXI3_wstrb;
  input [0:0]S_AXI3_wvalid;
  input [8:0]S_AXI_araddr;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [8:0]S_AXI_awaddr;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;
  output [0:0]bus_struct_reset;
  input dcm_locked;
  output [1:0]gpo;
  output [0:0]interconnect_aresetn;
  output [0:0]peripheral_aresetn;
  input [8:0]s_axi2_araddr;
  output [0:0]s_axi2_arready;
  input [0:0]s_axi2_arvalid;
  input [8:0]s_axi2_awaddr;
  output [0:0]s_axi2_awready;
  input [0:0]s_axi2_awvalid;
  input [0:0]s_axi2_bready;
  output [1:0]s_axi2_bresp;
  output [0:0]s_axi2_bvalid;
  output [31:0]s_axi2_rdata;
  input [0:0]s_axi2_rready;
  output [1:0]s_axi2_rresp;
  output [0:0]s_axi2_rvalid;
  input [31:0]s_axi2_wdata;
  output [0:0]s_axi2_wready;
  input [3:0]s_axi2_wstrb;
  input [0:0]s_axi2_wvalid;
  input s_axi_aclk;

  wire [14:0]Conn1_ADDR;
  wire [2:0]Conn1_BA;
  wire Conn1_CAS_N;
  wire Conn1_CKE;
  wire Conn1_CK_N;
  wire Conn1_CK_P;
  wire Conn1_CS_N;
  wire [3:0]Conn1_DM;
  wire [31:0]Conn1_DQ;
  wire [3:0]Conn1_DQS_N;
  wire [3:0]Conn1_DQS_P;
  wire Conn1_ODT;
  wire Conn1_RAS_N;
  wire Conn1_RESET_N;
  wire Conn1_WE_N;
  wire Conn2_DDR_VRN;
  wire Conn2_DDR_VRP;
  wire [53:0]Conn2_MIO;
  wire Conn2_PS_CLK;
  wire Conn2_PS_PORB;
  wire Conn2_PS_SRSTB;
  wire [31:0]Conn3_ARADDR;
  wire [1:0]Conn3_ARBURST;
  wire [3:0]Conn3_ARCACHE;
  wire [11:0]Conn3_ARID;
  wire [3:0]Conn3_ARLEN;
  wire [1:0]Conn3_ARLOCK;
  wire [2:0]Conn3_ARPROT;
  wire [3:0]Conn3_ARQOS;
  wire Conn3_ARREADY;
  wire [2:0]Conn3_ARSIZE;
  wire Conn3_ARVALID;
  wire [31:0]Conn3_AWADDR;
  wire [1:0]Conn3_AWBURST;
  wire [3:0]Conn3_AWCACHE;
  wire [11:0]Conn3_AWID;
  wire [3:0]Conn3_AWLEN;
  wire [1:0]Conn3_AWLOCK;
  wire [2:0]Conn3_AWPROT;
  wire [3:0]Conn3_AWQOS;
  wire Conn3_AWREADY;
  wire [2:0]Conn3_AWSIZE;
  wire Conn3_AWVALID;
  wire [11:0]Conn3_BID;
  wire Conn3_BREADY;
  wire [1:0]Conn3_BRESP;
  wire Conn3_BVALID;
  wire [31:0]Conn3_RDATA;
  wire [11:0]Conn3_RID;
  wire Conn3_RLAST;
  wire Conn3_RREADY;
  wire [1:0]Conn3_RRESP;
  wire Conn3_RVALID;
  wire [31:0]Conn3_WDATA;
  wire [11:0]Conn3_WID;
  wire Conn3_WLAST;
  wire Conn3_WREADY;
  wire [3:0]Conn3_WSTRB;
  wire Conn3_WVALID;
  wire GND_1;
  wire [0:0]In1_1;
  wire VCC_1;
  wire [31:0]axi_apb_bridge_1_APB_M_PADDR;
  wire axi_apb_bridge_1_APB_M_PENABLE;
  wire [31:0]axi_apb_bridge_1_APB_M_PRDATA;
  wire axi_apb_bridge_1_APB_M_PREADY;
  wire [0:0]axi_apb_bridge_1_APB_M_PSEL;
  wire axi_apb_bridge_1_APB_M_PSLVERR;
  wire [31:0]axi_apb_bridge_1_APB_M_PWDATA;
  wire axi_apb_bridge_1_APB_M_PWRITE;
  wire axi_iic_1_IIC_SCL_I;
  wire axi_iic_1_IIC_SCL_O;
  wire axi_iic_1_IIC_SCL_T;
  wire axi_iic_1_IIC_SDA_I;
  wire axi_iic_1_IIC_SDA_O;
  wire axi_iic_1_IIC_SDA_T;
  wire [1:0]axi_iic_1_gpo;
  wire axi_iic_1_iic2intc_irpt;
  wire [8:0]axi_interconnect_1_M00_AXI_ARADDR;
  wire axi_interconnect_1_M00_AXI_ARREADY;
  wire [0:0]axi_interconnect_1_M00_AXI_ARVALID;
  wire [8:0]axi_interconnect_1_M00_AXI_AWADDR;
  wire axi_interconnect_1_M00_AXI_AWREADY;
  wire [0:0]axi_interconnect_1_M00_AXI_AWVALID;
  wire [0:0]axi_interconnect_1_M00_AXI_BREADY;
  wire [1:0]axi_interconnect_1_M00_AXI_BRESP;
  wire axi_interconnect_1_M00_AXI_BVALID;
  wire [31:0]axi_interconnect_1_M00_AXI_RDATA;
  wire [0:0]axi_interconnect_1_M00_AXI_RREADY;
  wire [1:0]axi_interconnect_1_M00_AXI_RRESP;
  wire axi_interconnect_1_M00_AXI_RVALID;
  wire [31:0]axi_interconnect_1_M00_AXI_WDATA;
  wire axi_interconnect_1_M00_AXI_WREADY;
  wire [3:0]axi_interconnect_1_M00_AXI_WSTRB;
  wire [0:0]axi_interconnect_1_M00_AXI_WVALID;
  wire [31:0]axi_interconnect_1_M02_AXI_ARADDR;
  wire axi_interconnect_1_M02_AXI_ARREADY;
  wire [0:0]axi_interconnect_1_M02_AXI_ARVALID;
  wire [31:0]axi_interconnect_1_M02_AXI_AWADDR;
  wire axi_interconnect_1_M02_AXI_AWREADY;
  wire [0:0]axi_interconnect_1_M02_AXI_AWVALID;
  wire [0:0]axi_interconnect_1_M02_AXI_BREADY;
  wire [1:0]axi_interconnect_1_M02_AXI_BRESP;
  wire axi_interconnect_1_M02_AXI_BVALID;
  wire [31:0]axi_interconnect_1_M02_AXI_RDATA;
  wire [0:0]axi_interconnect_1_M02_AXI_RREADY;
  wire [1:0]axi_interconnect_1_M02_AXI_RRESP;
  wire axi_interconnect_1_M02_AXI_RVALID;
  wire [31:0]axi_interconnect_1_M02_AXI_WDATA;
  wire axi_interconnect_1_M02_AXI_WREADY;
  wire [0:0]axi_interconnect_1_M02_AXI_WVALID;
  wire [4:0]axi_interconnect_1_M03_AXI_ARADDR;
  wire axi_interconnect_1_M03_AXI_ARREADY;
  wire [0:0]axi_interconnect_1_M03_AXI_ARVALID;
  wire [4:0]axi_interconnect_1_M03_AXI_AWADDR;
  wire axi_interconnect_1_M03_AXI_AWREADY;
  wire [0:0]axi_interconnect_1_M03_AXI_AWVALID;
  wire [0:0]axi_interconnect_1_M03_AXI_BREADY;
  wire [1:0]axi_interconnect_1_M03_AXI_BRESP;
  wire axi_interconnect_1_M03_AXI_BVALID;
  wire [31:0]axi_interconnect_1_M03_AXI_RDATA;
  wire [0:0]axi_interconnect_1_M03_AXI_RREADY;
  wire [1:0]axi_interconnect_1_M03_AXI_RRESP;
  wire axi_interconnect_1_M03_AXI_RVALID;
  wire [31:0]axi_interconnect_1_M03_AXI_WDATA;
  wire axi_interconnect_1_M03_AXI_WREADY;
  wire [3:0]axi_interconnect_1_M03_AXI_WSTRB;
  wire [0:0]axi_interconnect_1_M03_AXI_WVALID;
  wire [8:0]axi_interconnect_1_M06_AXI_ARADDR;
  wire axi_interconnect_1_M06_AXI_ARREADY;
  wire [0:0]axi_interconnect_1_M06_AXI_ARVALID;
  wire [8:0]axi_interconnect_1_M06_AXI_AWADDR;
  wire axi_interconnect_1_M06_AXI_AWREADY;
  wire [0:0]axi_interconnect_1_M06_AXI_AWVALID;
  wire [0:0]axi_interconnect_1_M06_AXI_BREADY;
  wire [1:0]axi_interconnect_1_M06_AXI_BRESP;
  wire axi_interconnect_1_M06_AXI_BVALID;
  wire [31:0]axi_interconnect_1_M06_AXI_RDATA;
  wire [0:0]axi_interconnect_1_M06_AXI_RREADY;
  wire [1:0]axi_interconnect_1_M06_AXI_RRESP;
  wire axi_interconnect_1_M06_AXI_RVALID;
  wire [31:0]axi_interconnect_1_M06_AXI_WDATA;
  wire axi_interconnect_1_M06_AXI_WREADY;
  wire [3:0]axi_interconnect_1_M06_AXI_WSTRB;
  wire [0:0]axi_interconnect_1_M06_AXI_WVALID;
  wire axi_timer_1_interrupt;
  wire clk_wiz_1_clk_out3;
  wire clk_wiz_1_locked;
  wire [0:0]proc_sys_reset_1_bus_struct_reset;
  wire [0:0]proc_sys_reset_1_interconnect_aresetn;
  wire [0:0]proc_sys_reset_1_peripheral_aresetn;
  wire processing_system7_0_FCLK_RESET0_N;
(* MARK_DEBUG *)   wire [4:0]xlconcat_1_dout;
  wire [0:0]xlconstant_0_dout;

  assign APB_M_paddr[11:0] = axi_apb_bridge_1_APB_M_PADDR[11:0];
  assign APB_M_penable = axi_apb_bridge_1_APB_M_PENABLE;
  assign APB_M_psel = axi_apb_bridge_1_APB_M_PSEL;
  assign APB_M_pwdata[31:0] = axi_apb_bridge_1_APB_M_PWDATA;
  assign APB_M_pwrite = axi_apb_bridge_1_APB_M_PWRITE;
  assign AXI4_LITE_arready[0] = axi_interconnect_1_M02_AXI_ARREADY;
  assign AXI4_LITE_awready[0] = axi_interconnect_1_M02_AXI_AWREADY;
  assign AXI4_LITE_bresp[1:0] = axi_interconnect_1_M02_AXI_BRESP;
  assign AXI4_LITE_bvalid[0] = axi_interconnect_1_M02_AXI_BVALID;
  assign AXI4_LITE_rdata[31:0] = axi_interconnect_1_M02_AXI_RDATA;
  assign AXI4_LITE_rresp[1:0] = axi_interconnect_1_M02_AXI_RRESP;
  assign AXI4_LITE_rvalid[0] = axi_interconnect_1_M02_AXI_RVALID;
  assign AXI4_LITE_wready[0] = axi_interconnect_1_M02_AXI_WREADY;
  assign Conn3_ARREADY = M_AXI_GP0_arready;
  assign Conn3_AWREADY = M_AXI_GP0_awready;
  assign Conn3_BID = M_AXI_GP0_bid[11:0];
  assign Conn3_BRESP = M_AXI_GP0_bresp[1:0];
  assign Conn3_BVALID = M_AXI_GP0_bvalid;
  assign Conn3_RDATA = M_AXI_GP0_rdata[31:0];
  assign Conn3_RID = M_AXI_GP0_rid[11:0];
  assign Conn3_RLAST = M_AXI_GP0_rlast;
  assign Conn3_RRESP = M_AXI_GP0_rresp[1:0];
  assign Conn3_RVALID = M_AXI_GP0_rvalid;
  assign Conn3_WREADY = M_AXI_GP0_wready;
  assign FCLK_RESET0_N = processing_system7_0_FCLK_RESET0_N;
  assign IIC_scl_o = axi_iic_1_IIC_SCL_O;
  assign IIC_scl_t = axi_iic_1_IIC_SCL_T;
  assign IIC_sda_o = axi_iic_1_IIC_SDA_O;
  assign IIC_sda_t = axi_iic_1_IIC_SDA_T;
  assign In1_1 = In1[0];
  assign M_AXI_GP0_araddr[31:0] = Conn3_ARADDR;
  assign M_AXI_GP0_arburst[1:0] = Conn3_ARBURST;
  assign M_AXI_GP0_arcache[3:0] = Conn3_ARCACHE;
  assign M_AXI_GP0_arid[11:0] = Conn3_ARID;
  assign M_AXI_GP0_arlen[3:0] = Conn3_ARLEN;
  assign M_AXI_GP0_arlock[1:0] = Conn3_ARLOCK;
  assign M_AXI_GP0_arprot[2:0] = Conn3_ARPROT;
  assign M_AXI_GP0_arqos[3:0] = Conn3_ARQOS;
  assign M_AXI_GP0_arsize[2:0] = Conn3_ARSIZE;
  assign M_AXI_GP0_arvalid = Conn3_ARVALID;
  assign M_AXI_GP0_awaddr[31:0] = Conn3_AWADDR;
  assign M_AXI_GP0_awburst[1:0] = Conn3_AWBURST;
  assign M_AXI_GP0_awcache[3:0] = Conn3_AWCACHE;
  assign M_AXI_GP0_awid[11:0] = Conn3_AWID;
  assign M_AXI_GP0_awlen[3:0] = Conn3_AWLEN;
  assign M_AXI_GP0_awlock[1:0] = Conn3_AWLOCK;
  assign M_AXI_GP0_awprot[2:0] = Conn3_AWPROT;
  assign M_AXI_GP0_awqos[3:0] = Conn3_AWQOS;
  assign M_AXI_GP0_awsize[2:0] = Conn3_AWSIZE;
  assign M_AXI_GP0_awvalid = Conn3_AWVALID;
  assign M_AXI_GP0_bready = Conn3_BREADY;
  assign M_AXI_GP0_rready = Conn3_RREADY;
  assign M_AXI_GP0_wdata[31:0] = Conn3_WDATA;
  assign M_AXI_GP0_wid[11:0] = Conn3_WID;
  assign M_AXI_GP0_wlast = Conn3_WLAST;
  assign M_AXI_GP0_wstrb[3:0] = Conn3_WSTRB;
  assign M_AXI_GP0_wvalid = Conn3_WVALID;
  assign S_AXI3_arready[0] = axi_interconnect_1_M03_AXI_ARREADY;
  assign S_AXI3_awready[0] = axi_interconnect_1_M03_AXI_AWREADY;
  assign S_AXI3_bresp[1:0] = axi_interconnect_1_M03_AXI_BRESP;
  assign S_AXI3_bvalid[0] = axi_interconnect_1_M03_AXI_BVALID;
  assign S_AXI3_rdata[31:0] = axi_interconnect_1_M03_AXI_RDATA;
  assign S_AXI3_rresp[1:0] = axi_interconnect_1_M03_AXI_RRESP;
  assign S_AXI3_rvalid[0] = axi_interconnect_1_M03_AXI_RVALID;
  assign S_AXI3_wready[0] = axi_interconnect_1_M03_AXI_WREADY;
  assign S_AXI_arready[0] = axi_interconnect_1_M06_AXI_ARREADY;
  assign S_AXI_awready[0] = axi_interconnect_1_M06_AXI_AWREADY;
  assign S_AXI_bresp[1:0] = axi_interconnect_1_M06_AXI_BRESP;
  assign S_AXI_bvalid[0] = axi_interconnect_1_M06_AXI_BVALID;
  assign S_AXI_rdata[31:0] = axi_interconnect_1_M06_AXI_RDATA;
  assign S_AXI_rresp[1:0] = axi_interconnect_1_M06_AXI_RRESP;
  assign S_AXI_rvalid[0] = axi_interconnect_1_M06_AXI_RVALID;
  assign S_AXI_wready[0] = axi_interconnect_1_M06_AXI_WREADY;
  assign axi_apb_bridge_1_APB_M_PRDATA = APB_M_prdata[31:0];
  assign axi_apb_bridge_1_APB_M_PREADY = APB_M_pready;
  assign axi_apb_bridge_1_APB_M_PSLVERR = APB_M_pslverr;
  assign axi_iic_1_IIC_SCL_I = IIC_scl_i;
  assign axi_iic_1_IIC_SDA_I = IIC_sda_i;
  assign axi_interconnect_1_M00_AXI_ARADDR = s_axi2_araddr[8:0];
  assign axi_interconnect_1_M00_AXI_ARVALID = s_axi2_arvalid[0];
  assign axi_interconnect_1_M00_AXI_AWADDR = s_axi2_awaddr[8:0];
  assign axi_interconnect_1_M00_AXI_AWVALID = s_axi2_awvalid[0];
  assign axi_interconnect_1_M00_AXI_BREADY = s_axi2_bready[0];
  assign axi_interconnect_1_M00_AXI_RREADY = s_axi2_rready[0];
  assign axi_interconnect_1_M00_AXI_WDATA = s_axi2_wdata[31:0];
  assign axi_interconnect_1_M00_AXI_WSTRB = s_axi2_wstrb[3:0];
  assign axi_interconnect_1_M00_AXI_WVALID = s_axi2_wvalid[0];
  assign axi_interconnect_1_M02_AXI_ARADDR = AXI4_LITE_araddr[31:0];
  assign axi_interconnect_1_M02_AXI_ARVALID = AXI4_LITE_arvalid[0];
  assign axi_interconnect_1_M02_AXI_AWADDR = AXI4_LITE_awaddr[31:0];
  assign axi_interconnect_1_M02_AXI_AWVALID = AXI4_LITE_awvalid[0];
  assign axi_interconnect_1_M02_AXI_BREADY = AXI4_LITE_bready[0];
  assign axi_interconnect_1_M02_AXI_RREADY = AXI4_LITE_rready[0];
  assign axi_interconnect_1_M02_AXI_WDATA = AXI4_LITE_wdata[31:0];
  assign axi_interconnect_1_M02_AXI_WVALID = AXI4_LITE_wvalid[0];
  assign axi_interconnect_1_M03_AXI_ARADDR = S_AXI3_araddr[4:0];
  assign axi_interconnect_1_M03_AXI_ARVALID = S_AXI3_arvalid[0];
  assign axi_interconnect_1_M03_AXI_AWADDR = S_AXI3_awaddr[4:0];
  assign axi_interconnect_1_M03_AXI_AWVALID = S_AXI3_awvalid[0];
  assign axi_interconnect_1_M03_AXI_BREADY = S_AXI3_bready[0];
  assign axi_interconnect_1_M03_AXI_RREADY = S_AXI3_rready[0];
  assign axi_interconnect_1_M03_AXI_WDATA = S_AXI3_wdata[31:0];
  assign axi_interconnect_1_M03_AXI_WSTRB = S_AXI3_wstrb[3:0];
  assign axi_interconnect_1_M03_AXI_WVALID = S_AXI3_wvalid[0];
  assign axi_interconnect_1_M06_AXI_ARADDR = S_AXI_araddr[8:0];
  assign axi_interconnect_1_M06_AXI_ARVALID = S_AXI_arvalid[0];
  assign axi_interconnect_1_M06_AXI_AWADDR = S_AXI_awaddr[8:0];
  assign axi_interconnect_1_M06_AXI_AWVALID = S_AXI_awvalid[0];
  assign axi_interconnect_1_M06_AXI_BREADY = S_AXI_bready[0];
  assign axi_interconnect_1_M06_AXI_RREADY = S_AXI_rready[0];
  assign axi_interconnect_1_M06_AXI_WDATA = S_AXI_wdata[31:0];
  assign axi_interconnect_1_M06_AXI_WSTRB = S_AXI_wstrb[3:0];
  assign axi_interconnect_1_M06_AXI_WVALID = S_AXI_wvalid[0];
  assign bus_struct_reset[0] = proc_sys_reset_1_bus_struct_reset;
  assign clk_wiz_1_clk_out3 = s_axi_aclk;
  assign clk_wiz_1_locked = dcm_locked;
  assign gpo[1:0] = axi_iic_1_gpo;
  assign interconnect_aresetn[0] = proc_sys_reset_1_interconnect_aresetn;
  assign peripheral_aresetn[0] = proc_sys_reset_1_peripheral_aresetn;
  assign s_axi2_arready[0] = axi_interconnect_1_M00_AXI_ARREADY;
  assign s_axi2_awready[0] = axi_interconnect_1_M00_AXI_AWREADY;
  assign s_axi2_bresp[1:0] = axi_interconnect_1_M00_AXI_BRESP;
  assign s_axi2_bvalid[0] = axi_interconnect_1_M00_AXI_BVALID;
  assign s_axi2_rdata[31:0] = axi_interconnect_1_M00_AXI_RDATA;
  assign s_axi2_rresp[1:0] = axi_interconnect_1_M00_AXI_RRESP;
  assign s_axi2_rvalid[0] = axi_interconnect_1_M00_AXI_RVALID;
  assign s_axi2_wready[0] = axi_interconnect_1_M00_AXI_WREADY;
GND GND
       (.G(GND_1));
VCC VCC
       (.P(VCC_1));
dp_system_axi_apb_bridge_0_0 axi_apb_bridge_1
       (.m_apb_paddr(axi_apb_bridge_1_APB_M_PADDR),
        .m_apb_penable(axi_apb_bridge_1_APB_M_PENABLE),
        .m_apb_prdata(axi_apb_bridge_1_APB_M_PRDATA),
        .m_apb_pready(axi_apb_bridge_1_APB_M_PREADY),
        .m_apb_psel(axi_apb_bridge_1_APB_M_PSEL),
        .m_apb_pslverr(axi_apb_bridge_1_APB_M_PSLVERR),
        .m_apb_pwdata(axi_apb_bridge_1_APB_M_PWDATA),
        .m_apb_pwrite(axi_apb_bridge_1_APB_M_PWRITE),
        .s_axi_aclk(clk_wiz_1_clk_out3),
        .s_axi_araddr(axi_interconnect_1_M02_AXI_ARADDR),
        .s_axi_aresetn(proc_sys_reset_1_peripheral_aresetn),
        .s_axi_arready(axi_interconnect_1_M02_AXI_ARREADY),
        .s_axi_arvalid(axi_interconnect_1_M02_AXI_ARVALID),
        .s_axi_awaddr(axi_interconnect_1_M02_AXI_AWADDR),
        .s_axi_awready(axi_interconnect_1_M02_AXI_AWREADY),
        .s_axi_awvalid(axi_interconnect_1_M02_AXI_AWVALID),
        .s_axi_bready(axi_interconnect_1_M02_AXI_BREADY),
        .s_axi_bresp(axi_interconnect_1_M02_AXI_BRESP),
        .s_axi_bvalid(axi_interconnect_1_M02_AXI_BVALID),
        .s_axi_rdata(axi_interconnect_1_M02_AXI_RDATA),
        .s_axi_rready(axi_interconnect_1_M02_AXI_RREADY),
        .s_axi_rresp(axi_interconnect_1_M02_AXI_RRESP),
        .s_axi_rvalid(axi_interconnect_1_M02_AXI_RVALID),
        .s_axi_wdata(axi_interconnect_1_M02_AXI_WDATA),
        .s_axi_wready(axi_interconnect_1_M02_AXI_WREADY),
        .s_axi_wvalid(axi_interconnect_1_M02_AXI_WVALID));
dp_system_axi_iic_0_0 axi_iic_1
       (.gpo(axi_iic_1_gpo),
        .iic2intc_irpt(axi_iic_1_iic2intc_irpt),
        .s_axi_aclk(clk_wiz_1_clk_out3),
        .s_axi_araddr(axi_interconnect_1_M06_AXI_ARADDR),
        .s_axi_aresetn(proc_sys_reset_1_peripheral_aresetn),
        .s_axi_arready(axi_interconnect_1_M06_AXI_ARREADY),
        .s_axi_arvalid(axi_interconnect_1_M06_AXI_ARVALID),
        .s_axi_awaddr(axi_interconnect_1_M06_AXI_AWADDR),
        .s_axi_awready(axi_interconnect_1_M06_AXI_AWREADY),
        .s_axi_awvalid(axi_interconnect_1_M06_AXI_AWVALID),
        .s_axi_bready(axi_interconnect_1_M06_AXI_BREADY),
        .s_axi_bresp(axi_interconnect_1_M06_AXI_BRESP),
        .s_axi_bvalid(axi_interconnect_1_M06_AXI_BVALID),
        .s_axi_rdata(axi_interconnect_1_M06_AXI_RDATA),
        .s_axi_rready(axi_interconnect_1_M06_AXI_RREADY),
        .s_axi_rresp(axi_interconnect_1_M06_AXI_RRESP),
        .s_axi_rvalid(axi_interconnect_1_M06_AXI_RVALID),
        .s_axi_wdata(axi_interconnect_1_M06_AXI_WDATA),
        .s_axi_wready(axi_interconnect_1_M06_AXI_WREADY),
        .s_axi_wstrb(axi_interconnect_1_M06_AXI_WSTRB),
        .s_axi_wvalid(axi_interconnect_1_M06_AXI_WVALID),
        .scl_i(axi_iic_1_IIC_SCL_I),
        .scl_o(axi_iic_1_IIC_SCL_O),
        .scl_t(axi_iic_1_IIC_SCL_T),
        .sda_i(axi_iic_1_IIC_SDA_I),
        .sda_o(axi_iic_1_IIC_SDA_O),
        .sda_t(axi_iic_1_IIC_SDA_T));
dp_system_axi_intc_0_0 axi_intc_1
       (.intr(xlconcat_1_dout),
        .s_axi_aclk(clk_wiz_1_clk_out3),
        .s_axi_araddr(axi_interconnect_1_M00_AXI_ARADDR),
        .s_axi_aresetn(proc_sys_reset_1_peripheral_aresetn),
        .s_axi_arready(axi_interconnect_1_M00_AXI_ARREADY),
        .s_axi_arvalid(axi_interconnect_1_M00_AXI_ARVALID),
        .s_axi_awaddr(axi_interconnect_1_M00_AXI_AWADDR),
        .s_axi_awready(axi_interconnect_1_M00_AXI_AWREADY),
        .s_axi_awvalid(axi_interconnect_1_M00_AXI_AWVALID),
        .s_axi_bready(axi_interconnect_1_M00_AXI_BREADY),
        .s_axi_bresp(axi_interconnect_1_M00_AXI_BRESP),
        .s_axi_bvalid(axi_interconnect_1_M00_AXI_BVALID),
        .s_axi_rdata(axi_interconnect_1_M00_AXI_RDATA),
        .s_axi_rready(axi_interconnect_1_M00_AXI_RREADY),
        .s_axi_rresp(axi_interconnect_1_M00_AXI_RRESP),
        .s_axi_rvalid(axi_interconnect_1_M00_AXI_RVALID),
        .s_axi_wdata(axi_interconnect_1_M00_AXI_WDATA),
        .s_axi_wready(axi_interconnect_1_M00_AXI_WREADY),
        .s_axi_wstrb(axi_interconnect_1_M00_AXI_WSTRB),
        .s_axi_wvalid(axi_interconnect_1_M00_AXI_WVALID));
dp_system_axi_timer_0_0 axi_timer_1
       (.capturetrig0(GND_1),
        .capturetrig1(GND_1),
        .freeze(GND_1),
        .interrupt(axi_timer_1_interrupt),
        .s_axi_aclk(clk_wiz_1_clk_out3),
        .s_axi_araddr(axi_interconnect_1_M03_AXI_ARADDR),
        .s_axi_aresetn(proc_sys_reset_1_peripheral_aresetn),
        .s_axi_arready(axi_interconnect_1_M03_AXI_ARREADY),
        .s_axi_arvalid(axi_interconnect_1_M03_AXI_ARVALID),
        .s_axi_awaddr(axi_interconnect_1_M03_AXI_AWADDR),
        .s_axi_awready(axi_interconnect_1_M03_AXI_AWREADY),
        .s_axi_awvalid(axi_interconnect_1_M03_AXI_AWVALID),
        .s_axi_bready(axi_interconnect_1_M03_AXI_BREADY),
        .s_axi_bresp(axi_interconnect_1_M03_AXI_BRESP),
        .s_axi_bvalid(axi_interconnect_1_M03_AXI_BVALID),
        .s_axi_rdata(axi_interconnect_1_M03_AXI_RDATA),
        .s_axi_rready(axi_interconnect_1_M03_AXI_RREADY),
        .s_axi_rresp(axi_interconnect_1_M03_AXI_RRESP),
        .s_axi_rvalid(axi_interconnect_1_M03_AXI_RVALID),
        .s_axi_wdata(axi_interconnect_1_M03_AXI_WDATA),
        .s_axi_wready(axi_interconnect_1_M03_AXI_WREADY),
        .s_axi_wstrb(axi_interconnect_1_M03_AXI_WSTRB),
        .s_axi_wvalid(axi_interconnect_1_M03_AXI_WVALID));
dp_system_proc_sys_reset_0_0 proc_sys_reset_1
       (.aux_reset_in(VCC_1),
        .bus_struct_reset(proc_sys_reset_1_bus_struct_reset),
        .dcm_locked(clk_wiz_1_locked),
        .ext_reset_in(processing_system7_0_FCLK_RESET0_N),
        .interconnect_aresetn(proc_sys_reset_1_interconnect_aresetn),
        .mb_debug_sys_rst(GND_1),
        .peripheral_aresetn(proc_sys_reset_1_peripheral_aresetn),
        .slowest_sync_clk(clk_wiz_1_clk_out3));
dp_system_processing_system7_0_0 processing_system7_0
       (.Core0_nFIQ(GND_1),
        .DDR_Addr(DDR_addr[14:0]),
        .DDR_BankAddr(DDR_ba[2:0]),
        .DDR_CAS_n(DDR_cas_n),
        .DDR_CKE(DDR_cke),
        .DDR_CS_n(DDR_cs_n),
        .DDR_Clk(DDR_ck_p),
        .DDR_Clk_n(DDR_ck_n),
        .DDR_DM(DDR_dm[3:0]),
        .DDR_DQ(DDR_dq[31:0]),
        .DDR_DQS(DDR_dqs_p[3:0]),
        .DDR_DQS_n(DDR_dqs_n[3:0]),
        .DDR_DRSTB(DDR_reset_n),
        .DDR_ODT(DDR_odt),
        .DDR_RAS_n(DDR_ras_n),
        .DDR_VRN(FIXED_IO_ddr_vrn),
        .DDR_VRP(FIXED_IO_ddr_vrp),
        .DDR_WEB(DDR_we_n),
        .FCLK_RESET0_N(processing_system7_0_FCLK_RESET0_N),
        .MIO(FIXED_IO_mio[53:0]),
        .M_AXI_GP0_ACLK(clk_wiz_1_clk_out3),
        .M_AXI_GP0_ARADDR(Conn3_ARADDR),
        .M_AXI_GP0_ARBURST(Conn3_ARBURST),
        .M_AXI_GP0_ARCACHE(Conn3_ARCACHE),
        .M_AXI_GP0_ARID(Conn3_ARID),
        .M_AXI_GP0_ARLEN(Conn3_ARLEN),
        .M_AXI_GP0_ARLOCK(Conn3_ARLOCK),
        .M_AXI_GP0_ARPROT(Conn3_ARPROT),
        .M_AXI_GP0_ARQOS(Conn3_ARQOS),
        .M_AXI_GP0_ARREADY(Conn3_ARREADY),
        .M_AXI_GP0_ARSIZE(Conn3_ARSIZE),
        .M_AXI_GP0_ARVALID(Conn3_ARVALID),
        .M_AXI_GP0_AWADDR(Conn3_AWADDR),
        .M_AXI_GP0_AWBURST(Conn3_AWBURST),
        .M_AXI_GP0_AWCACHE(Conn3_AWCACHE),
        .M_AXI_GP0_AWID(Conn3_AWID),
        .M_AXI_GP0_AWLEN(Conn3_AWLEN),
        .M_AXI_GP0_AWLOCK(Conn3_AWLOCK),
        .M_AXI_GP0_AWPROT(Conn3_AWPROT),
        .M_AXI_GP0_AWQOS(Conn3_AWQOS),
        .M_AXI_GP0_AWREADY(Conn3_AWREADY),
        .M_AXI_GP0_AWSIZE(Conn3_AWSIZE),
        .M_AXI_GP0_AWVALID(Conn3_AWVALID),
        .M_AXI_GP0_BID(Conn3_BID),
        .M_AXI_GP0_BREADY(Conn3_BREADY),
        .M_AXI_GP0_BRESP(Conn3_BRESP),
        .M_AXI_GP0_BVALID(Conn3_BVALID),
        .M_AXI_GP0_RDATA(Conn3_RDATA),
        .M_AXI_GP0_RID(Conn3_RID),
        .M_AXI_GP0_RLAST(Conn3_RLAST),
        .M_AXI_GP0_RREADY(Conn3_RREADY),
        .M_AXI_GP0_RRESP(Conn3_RRESP),
        .M_AXI_GP0_RVALID(Conn3_RVALID),
        .M_AXI_GP0_WDATA(Conn3_WDATA),
        .M_AXI_GP0_WID(Conn3_WID),
        .M_AXI_GP0_WLAST(Conn3_WLAST),
        .M_AXI_GP0_WREADY(Conn3_WREADY),
        .M_AXI_GP0_WSTRB(Conn3_WSTRB),
        .M_AXI_GP0_WVALID(Conn3_WVALID),
        .PS_CLK(FIXED_IO_ps_clk),
        .PS_PORB(FIXED_IO_ps_porb),
        .PS_SRSTB(FIXED_IO_ps_srstb));
dp_system_xlconcat_0_0 xlconcat_1
       (.In0(xlconstant_0_dout),
        .In1(In1_1),
        .In2(axi_timer_1_interrupt),
        .In3(xlconstant_0_dout),
        .In4(axi_iic_1_iic2intc_irpt),
        .dout(xlconcat_1_dout));
dp_system_xlconstant_0_0 xlconstant_0
       (.dout(xlconstant_0_dout));
endmodule

(* CORE_GENERATION_INFO = "dp_system,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLanguage=VERILOG,numBlks=32,numReposBlks=24,numNonXlnxBlks=0,numHierBlks=8,maxHierDepth=1,da_axi4_cnt=5,da_bram_cntlr_cnt=4,da_ps7_cnt=2}" *) 
module dp_system
   (DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    SYS_CLK_clk_n,
    SYS_CLK_clk_p,
    aux_tx_io_n,
    aux_tx_io_p,
    clk_out2,
    dp_mainlink_lnk_clk_n,
    dp_mainlink_lnk_clk_p,
    dp_mainlink_lnk_tx_lane_n,
    dp_mainlink_lnk_tx_lane_p,
    gpo,
    iic2inctc_irpt_scl_i,
    iic2inctc_irpt_scl_o,
    iic2inctc_irpt_scl_t,
    iic2inctc_irpt_sda_i,
    iic2inctc_irpt_sda_o,
    iic2inctc_irpt_sda_t,
    tx_hpd);
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  input SYS_CLK_clk_n;
  input SYS_CLK_clk_p;
  inout aux_tx_io_n;
  inout aux_tx_io_p;
  output clk_out2;
  input dp_mainlink_lnk_clk_n;
  input dp_mainlink_lnk_clk_p;
  output [3:0]dp_mainlink_lnk_tx_lane_n;
  output [3:0]dp_mainlink_lnk_tx_lane_p;
  output [1:0]gpo;
  input iic2inctc_irpt_scl_i;
  output iic2inctc_irpt_scl_o;
  output iic2inctc_irpt_scl_t;
  input iic2inctc_irpt_sda_i;
  output iic2inctc_irpt_sda_o;
  output iic2inctc_irpt_sda_t;
  input tx_hpd;

  wire [31:0]AXI4_LITE_1_ARADDR;
  wire [0:0]AXI4_LITE_1_ARREADY;
  wire [0:0]AXI4_LITE_1_ARVALID;
  wire [31:0]AXI4_LITE_1_AWADDR;
  wire [0:0]AXI4_LITE_1_AWREADY;
  wire [0:0]AXI4_LITE_1_AWVALID;
  wire [0:0]AXI4_LITE_1_BREADY;
  wire [1:0]AXI4_LITE_1_BRESP;
  wire [0:0]AXI4_LITE_1_BVALID;
  wire [31:0]AXI4_LITE_1_RDATA;
  wire [0:0]AXI4_LITE_1_RREADY;
  wire [1:0]AXI4_LITE_1_RRESP;
  wire [0:0]AXI4_LITE_1_RVALID;
  wire [31:0]AXI4_LITE_1_WDATA;
  wire [0:0]AXI4_LITE_1_WREADY;
  wire [0:0]AXI4_LITE_1_WVALID;
  wire CLK_IN1_D_1_CLK_N;
  wire CLK_IN1_D_1_CLK_P;
  wire Net;
  wire Net1;
  wire [31:0]S00_AXI_1_ARADDR;
  wire [1:0]S00_AXI_1_ARBURST;
  wire [3:0]S00_AXI_1_ARCACHE;
  wire [11:0]S00_AXI_1_ARID;
  wire [3:0]S00_AXI_1_ARLEN;
  wire [1:0]S00_AXI_1_ARLOCK;
  wire [2:0]S00_AXI_1_ARPROT;
  wire [3:0]S00_AXI_1_ARQOS;
  wire S00_AXI_1_ARREADY;
  wire [2:0]S00_AXI_1_ARSIZE;
  wire S00_AXI_1_ARVALID;
  wire [31:0]S00_AXI_1_AWADDR;
  wire [1:0]S00_AXI_1_AWBURST;
  wire [3:0]S00_AXI_1_AWCACHE;
  wire [11:0]S00_AXI_1_AWID;
  wire [3:0]S00_AXI_1_AWLEN;
  wire [1:0]S00_AXI_1_AWLOCK;
  wire [2:0]S00_AXI_1_AWPROT;
  wire [3:0]S00_AXI_1_AWQOS;
  wire S00_AXI_1_AWREADY;
  wire [2:0]S00_AXI_1_AWSIZE;
  wire S00_AXI_1_AWVALID;
  wire [11:0]S00_AXI_1_BID;
  wire S00_AXI_1_BREADY;
  wire [1:0]S00_AXI_1_BRESP;
  wire S00_AXI_1_BVALID;
  wire [31:0]S00_AXI_1_RDATA;
  wire [11:0]S00_AXI_1_RID;
  wire S00_AXI_1_RLAST;
  wire S00_AXI_1_RREADY;
  wire [1:0]S00_AXI_1_RRESP;
  wire S00_AXI_1_RVALID;
  wire [31:0]S00_AXI_1_WDATA;
  wire [11:0]S00_AXI_1_WID;
  wire S00_AXI_1_WLAST;
  wire S00_AXI_1_WREADY;
  wire [3:0]S00_AXI_1_WSTRB;
  wire S00_AXI_1_WVALID;
  wire [4:0]S_AXI3_1_ARADDR;
  wire [0:0]S_AXI3_1_ARREADY;
  wire [0:0]S_AXI3_1_ARVALID;
  wire [4:0]S_AXI3_1_AWADDR;
  wire [0:0]S_AXI3_1_AWREADY;
  wire [0:0]S_AXI3_1_AWVALID;
  wire [0:0]S_AXI3_1_BREADY;
  wire [1:0]S_AXI3_1_BRESP;
  wire [0:0]S_AXI3_1_BVALID;
  wire [31:0]S_AXI3_1_RDATA;
  wire [0:0]S_AXI3_1_RREADY;
  wire [1:0]S_AXI3_1_RRESP;
  wire [0:0]S_AXI3_1_RVALID;
  wire [31:0]S_AXI3_1_WDATA;
  wire [0:0]S_AXI3_1_WREADY;
  wire [3:0]S_AXI3_1_WSTRB;
  wire [0:0]S_AXI3_1_WVALID;
  wire [8:0]S_AXI_1_ARADDR;
  wire [0:0]S_AXI_1_ARREADY;
  wire [0:0]S_AXI_1_ARVALID;
  wire [8:0]S_AXI_1_AWADDR;
  wire [0:0]S_AXI_1_AWREADY;
  wire [0:0]S_AXI_1_AWVALID;
  wire [0:0]S_AXI_1_BREADY;
  wire [1:0]S_AXI_1_BRESP;
  wire [0:0]S_AXI_1_BVALID;
  wire [31:0]S_AXI_1_RDATA;
  wire [0:0]S_AXI_1_RREADY;
  wire [1:0]S_AXI_1_RRESP;
  wire [0:0]S_AXI_1_RVALID;
  wire [31:0]S_AXI_1_WDATA;
  wire [0:0]S_AXI_1_WREADY;
  wire [3:0]S_AXI_1_WSTRB;
  wire [0:0]S_AXI_1_WVALID;
  wire [11:0]axi4_lite_peripherals_APB_M_PADDR;
  wire axi4_lite_peripherals_APB_M_PENABLE;
  wire [31:0]axi4_lite_peripherals_APB_M_PRDATA;
  wire axi4_lite_peripherals_APB_M_PREADY;
  wire axi4_lite_peripherals_APB_M_PSEL;
  wire axi4_lite_peripherals_APB_M_PSLVERR;
  wire [31:0]axi4_lite_peripherals_APB_M_PWDATA;
  wire axi4_lite_peripherals_APB_M_PWRITE;
  wire [14:0]axi4_lite_peripherals_DDR_ADDR;
  wire [2:0]axi4_lite_peripherals_DDR_BA;
  wire axi4_lite_peripherals_DDR_CAS_N;
  wire axi4_lite_peripherals_DDR_CKE;
  wire axi4_lite_peripherals_DDR_CK_N;
  wire axi4_lite_peripherals_DDR_CK_P;
  wire axi4_lite_peripherals_DDR_CS_N;
  wire [3:0]axi4_lite_peripherals_DDR_DM;
  wire [31:0]axi4_lite_peripherals_DDR_DQ;
  wire [3:0]axi4_lite_peripherals_DDR_DQS_N;
  wire [3:0]axi4_lite_peripherals_DDR_DQS_P;
  wire axi4_lite_peripherals_DDR_ODT;
  wire axi4_lite_peripherals_DDR_RAS_N;
  wire axi4_lite_peripherals_DDR_RESET_N;
  wire axi4_lite_peripherals_DDR_WE_N;
(* MARK_DEBUG *)   wire axi4_lite_peripherals_FCLK_RESET0_N;
  wire axi4_lite_peripherals_FIXED_IO_DDR_VRN;
  wire axi4_lite_peripherals_FIXED_IO_DDR_VRP;
  wire [53:0]axi4_lite_peripherals_FIXED_IO_MIO;
  wire axi4_lite_peripherals_FIXED_IO_PS_CLK;
  wire axi4_lite_peripherals_FIXED_IO_PS_PORB;
  wire axi4_lite_peripherals_FIXED_IO_PS_SRSTB;
  wire axi_iic_1_IIC_SCL_I;
  wire axi_iic_1_IIC_SCL_O;
  wire axi_iic_1_IIC_SCL_T;
  wire axi_iic_1_IIC_SDA_I;
  wire axi_iic_1_IIC_SDA_O;
  wire axi_iic_1_IIC_SDA_T;
  wire [1:0]axi_iic_1_gpo;
  wire [8:0]axi_interconnect_1_M00_AXI_ARADDR;
  wire [0:0]axi_interconnect_1_M00_AXI_ARREADY;
  wire [0:0]axi_interconnect_1_M00_AXI_ARVALID;
  wire [8:0]axi_interconnect_1_M00_AXI_AWADDR;
  wire [0:0]axi_interconnect_1_M00_AXI_AWREADY;
  wire [0:0]axi_interconnect_1_M00_AXI_AWVALID;
  wire [0:0]axi_interconnect_1_M00_AXI_BREADY;
  wire [1:0]axi_interconnect_1_M00_AXI_BRESP;
  wire [0:0]axi_interconnect_1_M00_AXI_BVALID;
  wire [31:0]axi_interconnect_1_M00_AXI_RDATA;
  wire [0:0]axi_interconnect_1_M00_AXI_RREADY;
  wire [1:0]axi_interconnect_1_M00_AXI_RRESP;
  wire [0:0]axi_interconnect_1_M00_AXI_RVALID;
  wire [31:0]axi_interconnect_1_M00_AXI_WDATA;
  wire [0:0]axi_interconnect_1_M00_AXI_WREADY;
  wire [3:0]axi_interconnect_1_M00_AXI_WSTRB;
  wire [0:0]axi_interconnect_1_M00_AXI_WVALID;
(* MARK_DEBUG *)   wire [31:0]axi_interconnect_1_M03_AXI_ARADDR;
(* MARK_DEBUG *)   wire [2:0]axi_interconnect_1_M03_AXI_ARPROT;
(* MARK_DEBUG *)   wire axi_interconnect_1_M03_AXI_ARREADY;
(* MARK_DEBUG *)   wire [0:0]axi_interconnect_1_M03_AXI_ARVALID;
(* MARK_DEBUG *)   wire [31:0]axi_interconnect_1_M03_AXI_AWADDR;
(* MARK_DEBUG *)   wire [2:0]axi_interconnect_1_M03_AXI_AWPROT;
(* MARK_DEBUG *)   wire axi_interconnect_1_M03_AXI_AWREADY;
(* MARK_DEBUG *)   wire [0:0]axi_interconnect_1_M03_AXI_AWVALID;
(* MARK_DEBUG *)   wire [0:0]axi_interconnect_1_M03_AXI_BREADY;
(* MARK_DEBUG *)   wire [1:0]axi_interconnect_1_M03_AXI_BRESP;
(* MARK_DEBUG *)   wire axi_interconnect_1_M03_AXI_BVALID;
(* MARK_DEBUG *)   wire [31:0]axi_interconnect_1_M03_AXI_RDATA;
(* MARK_DEBUG *)   wire [0:0]axi_interconnect_1_M03_AXI_RREADY;
(* MARK_DEBUG *)   wire [1:0]axi_interconnect_1_M03_AXI_RRESP;
(* MARK_DEBUG *)   wire axi_interconnect_1_M03_AXI_RVALID;
(* MARK_DEBUG *)   wire [31:0]axi_interconnect_1_M03_AXI_WDATA;
(* MARK_DEBUG *)   wire axi_interconnect_1_M03_AXI_WREADY;
(* MARK_DEBUG *)   wire [3:0]axi_interconnect_1_M03_AXI_WSTRB;
(* MARK_DEBUG *)   wire [0:0]axi_interconnect_1_M03_AXI_WVALID;
  wire [0:0]c_shift_ram_0_Q;
  wire clk_wiz_0_clk_out1;
  wire clk_wiz_1_clk_out1;
  wire clk_wiz_1_clk_out2;
  wire clk_wiz_1_clk_out3;
  wire clk_wiz_1_locked;
  wire clk_wiz_2_clk_out1;
  wire clk_wiz_2_clk_out2;
  wire clk_wiz_2_locked;
  wire displayport_0_axi_int;
  wire displayport_0_dp_mainlink_LNK_CLK_N;
  wire displayport_0_dp_mainlink_LNK_CLK_P;
  wire [3:0]displayport_0_dp_mainlink_LNK_TX_LANE_N;
  wire [3:0]displayport_0_dp_mainlink_LNK_TX_LANE_P;
  wire displayport_0_lnk_clk;
  wire [0:0]dp_tx_vid_reset_shift_reg_Q;
  wire [0:0]proc_sys_reset_1_interconnect_aresetn;
  wire [0:0]proc_sys_reset_1_peripheral_aresetn;
  wire tx_hpd_1;
  wire [0:0]util_vector_logic_0_Res;
  wire [0:0]util_vector_logic_1_Res;
  wire video_pat_gen_0_sw_vid_reset;
  wire [15:0]video_pat_gen_0_vid_clk_D;
  wire [7:0]video_pat_gen_0_vid_clk_M;
  wire video_pat_gen_0_vid_clk_prog;
  wire video_transmitter_1_dp_vid_inf_TX_VID_ENABLE;
  wire video_transmitter_1_dp_vid_inf_TX_VID_HSYNC;
  wire video_transmitter_1_dp_vid_inf_TX_VID_ODDEVEN;
  wire [47:0]video_transmitter_1_dp_vid_inf_TX_VID_PIXEL0;
  wire [47:0]video_transmitter_1_dp_vid_inf_TX_VID_PIXEL1;
  wire video_transmitter_1_dp_vid_inf_TX_VID_VSYNC;
  wire video_transmitter_1_vid_clk;
  wire [0:0]vio_0_probe_out0;
  wire [0:0]vio_0_probe_out1;
  wire [0:0]xlconstant_0_dout;
  wire [0:0]xlconstant_1_dout;

  assign CLK_IN1_D_1_CLK_N = SYS_CLK_clk_n;
  assign CLK_IN1_D_1_CLK_P = SYS_CLK_clk_p;
  assign axi_iic_1_IIC_SCL_I = iic2inctc_irpt_scl_i;
  assign axi_iic_1_IIC_SDA_I = iic2inctc_irpt_sda_i;
  assign clk_out2 = clk_wiz_1_clk_out2;
  assign displayport_0_dp_mainlink_LNK_CLK_N = dp_mainlink_lnk_clk_n;
  assign displayport_0_dp_mainlink_LNK_CLK_P = dp_mainlink_lnk_clk_p;
  assign dp_mainlink_lnk_tx_lane_n[3:0] = displayport_0_dp_mainlink_LNK_TX_LANE_N;
  assign dp_mainlink_lnk_tx_lane_p[3:0] = displayport_0_dp_mainlink_LNK_TX_LANE_P;
  assign gpo[1:0] = axi_iic_1_gpo;
  assign iic2inctc_irpt_scl_o = axi_iic_1_IIC_SCL_O;
  assign iic2inctc_irpt_scl_t = axi_iic_1_IIC_SCL_T;
  assign iic2inctc_irpt_sda_o = axi_iic_1_IIC_SDA_O;
  assign iic2inctc_irpt_sda_t = axi_iic_1_IIC_SDA_T;
  assign tx_hpd_1 = tx_hpd;
axi4_lite_peripherals_imp_8FS6LK axi4_lite_peripherals
       (.APB_M_paddr(axi4_lite_peripherals_APB_M_PADDR),
        .APB_M_penable(axi4_lite_peripherals_APB_M_PENABLE),
        .APB_M_prdata(axi4_lite_peripherals_APB_M_PRDATA),
        .APB_M_pready(axi4_lite_peripherals_APB_M_PREADY),
        .APB_M_psel(axi4_lite_peripherals_APB_M_PSEL),
        .APB_M_pslverr(axi4_lite_peripherals_APB_M_PSLVERR),
        .APB_M_pwdata(axi4_lite_peripherals_APB_M_PWDATA),
        .APB_M_pwrite(axi4_lite_peripherals_APB_M_PWRITE),
        .AXI4_LITE_araddr(AXI4_LITE_1_ARADDR),
        .AXI4_LITE_arready(AXI4_LITE_1_ARREADY),
        .AXI4_LITE_arvalid(AXI4_LITE_1_ARVALID),
        .AXI4_LITE_awaddr(AXI4_LITE_1_AWADDR),
        .AXI4_LITE_awready(AXI4_LITE_1_AWREADY),
        .AXI4_LITE_awvalid(AXI4_LITE_1_AWVALID),
        .AXI4_LITE_bready(AXI4_LITE_1_BREADY),
        .AXI4_LITE_bresp(AXI4_LITE_1_BRESP),
        .AXI4_LITE_bvalid(AXI4_LITE_1_BVALID),
        .AXI4_LITE_rdata(AXI4_LITE_1_RDATA),
        .AXI4_LITE_rready(AXI4_LITE_1_RREADY),
        .AXI4_LITE_rresp(AXI4_LITE_1_RRESP),
        .AXI4_LITE_rvalid(AXI4_LITE_1_RVALID),
        .AXI4_LITE_wdata(AXI4_LITE_1_WDATA),
        .AXI4_LITE_wready(AXI4_LITE_1_WREADY),
        .AXI4_LITE_wvalid(AXI4_LITE_1_WVALID),
        .DDR_addr(DDR_addr[14:0]),
        .DDR_ba(DDR_ba[2:0]),
        .DDR_cas_n(DDR_cas_n),
        .DDR_ck_n(DDR_ck_n),
        .DDR_ck_p(DDR_ck_p),
        .DDR_cke(DDR_cke),
        .DDR_cs_n(DDR_cs_n),
        .DDR_dm(DDR_dm[3:0]),
        .DDR_dq(DDR_dq[31:0]),
        .DDR_dqs_n(DDR_dqs_n[3:0]),
        .DDR_dqs_p(DDR_dqs_p[3:0]),
        .DDR_odt(DDR_odt),
        .DDR_ras_n(DDR_ras_n),
        .DDR_reset_n(DDR_reset_n),
        .DDR_we_n(DDR_we_n),
        .FCLK_RESET0_N(axi4_lite_peripherals_FCLK_RESET0_N),
        .FIXED_IO_ddr_vrn(FIXED_IO_ddr_vrn),
        .FIXED_IO_ddr_vrp(FIXED_IO_ddr_vrp),
        .FIXED_IO_mio(FIXED_IO_mio[53:0]),
        .FIXED_IO_ps_clk(FIXED_IO_ps_clk),
        .FIXED_IO_ps_porb(FIXED_IO_ps_porb),
        .FIXED_IO_ps_srstb(FIXED_IO_ps_srstb),
        .IIC_scl_i(axi_iic_1_IIC_SCL_I),
        .IIC_scl_o(axi_iic_1_IIC_SCL_O),
        .IIC_scl_t(axi_iic_1_IIC_SCL_T),
        .IIC_sda_i(axi_iic_1_IIC_SDA_I),
        .IIC_sda_o(axi_iic_1_IIC_SDA_O),
        .IIC_sda_t(axi_iic_1_IIC_SDA_T),
        .In1(displayport_0_axi_int),
        .M_AXI_GP0_araddr(S00_AXI_1_ARADDR),
        .M_AXI_GP0_arburst(S00_AXI_1_ARBURST),
        .M_AXI_GP0_arcache(S00_AXI_1_ARCACHE),
        .M_AXI_GP0_arid(S00_AXI_1_ARID),
        .M_AXI_GP0_arlen(S00_AXI_1_ARLEN),
        .M_AXI_GP0_arlock(S00_AXI_1_ARLOCK),
        .M_AXI_GP0_arprot(S00_AXI_1_ARPROT),
        .M_AXI_GP0_arqos(S00_AXI_1_ARQOS),
        .M_AXI_GP0_arready(S00_AXI_1_ARREADY),
        .M_AXI_GP0_arsize(S00_AXI_1_ARSIZE),
        .M_AXI_GP0_arvalid(S00_AXI_1_ARVALID),
        .M_AXI_GP0_awaddr(S00_AXI_1_AWADDR),
        .M_AXI_GP0_awburst(S00_AXI_1_AWBURST),
        .M_AXI_GP0_awcache(S00_AXI_1_AWCACHE),
        .M_AXI_GP0_awid(S00_AXI_1_AWID),
        .M_AXI_GP0_awlen(S00_AXI_1_AWLEN),
        .M_AXI_GP0_awlock(S00_AXI_1_AWLOCK),
        .M_AXI_GP0_awprot(S00_AXI_1_AWPROT),
        .M_AXI_GP0_awqos(S00_AXI_1_AWQOS),
        .M_AXI_GP0_awready(S00_AXI_1_AWREADY),
        .M_AXI_GP0_awsize(S00_AXI_1_AWSIZE),
        .M_AXI_GP0_awvalid(S00_AXI_1_AWVALID),
        .M_AXI_GP0_bid(S00_AXI_1_BID),
        .M_AXI_GP0_bready(S00_AXI_1_BREADY),
        .M_AXI_GP0_bresp(S00_AXI_1_BRESP),
        .M_AXI_GP0_bvalid(S00_AXI_1_BVALID),
        .M_AXI_GP0_rdata(S00_AXI_1_RDATA),
        .M_AXI_GP0_rid(S00_AXI_1_RID),
        .M_AXI_GP0_rlast(S00_AXI_1_RLAST),
        .M_AXI_GP0_rready(S00_AXI_1_RREADY),
        .M_AXI_GP0_rresp(S00_AXI_1_RRESP),
        .M_AXI_GP0_rvalid(S00_AXI_1_RVALID),
        .M_AXI_GP0_wdata(S00_AXI_1_WDATA),
        .M_AXI_GP0_wid(S00_AXI_1_WID),
        .M_AXI_GP0_wlast(S00_AXI_1_WLAST),
        .M_AXI_GP0_wready(S00_AXI_1_WREADY),
        .M_AXI_GP0_wstrb(S00_AXI_1_WSTRB),
        .M_AXI_GP0_wvalid(S00_AXI_1_WVALID),
        .S_AXI3_araddr(S_AXI3_1_ARADDR),
        .S_AXI3_arready(S_AXI3_1_ARREADY),
        .S_AXI3_arvalid(S_AXI3_1_ARVALID),
        .S_AXI3_awaddr(S_AXI3_1_AWADDR),
        .S_AXI3_awready(S_AXI3_1_AWREADY),
        .S_AXI3_awvalid(S_AXI3_1_AWVALID),
        .S_AXI3_bready(S_AXI3_1_BREADY),
        .S_AXI3_bresp(S_AXI3_1_BRESP),
        .S_AXI3_bvalid(S_AXI3_1_BVALID),
        .S_AXI3_rdata(S_AXI3_1_RDATA),
        .S_AXI3_rready(S_AXI3_1_RREADY),
        .S_AXI3_rresp(S_AXI3_1_RRESP),
        .S_AXI3_rvalid(S_AXI3_1_RVALID),
        .S_AXI3_wdata(S_AXI3_1_WDATA),
        .S_AXI3_wready(S_AXI3_1_WREADY),
        .S_AXI3_wstrb(S_AXI3_1_WSTRB),
        .S_AXI3_wvalid(S_AXI3_1_WVALID),
        .S_AXI_araddr(S_AXI_1_ARADDR),
        .S_AXI_arready(S_AXI_1_ARREADY),
        .S_AXI_arvalid(S_AXI_1_ARVALID),
        .S_AXI_awaddr(S_AXI_1_AWADDR),
        .S_AXI_awready(S_AXI_1_AWREADY),
        .S_AXI_awvalid(S_AXI_1_AWVALID),
        .S_AXI_bready(S_AXI_1_BREADY),
        .S_AXI_bresp(S_AXI_1_BRESP),
        .S_AXI_bvalid(S_AXI_1_BVALID),
        .S_AXI_rdata(S_AXI_1_RDATA),
        .S_AXI_rready(S_AXI_1_RREADY),
        .S_AXI_rresp(S_AXI_1_RRESP),
        .S_AXI_rvalid(S_AXI_1_RVALID),
        .S_AXI_wdata(S_AXI_1_WDATA),
        .S_AXI_wready(S_AXI_1_WREADY),
        .S_AXI_wstrb(S_AXI_1_WSTRB),
        .S_AXI_wvalid(S_AXI_1_WVALID),
        .dcm_locked(clk_wiz_1_locked),
        .gpo(axi_iic_1_gpo),
        .interconnect_aresetn(proc_sys_reset_1_interconnect_aresetn),
        .peripheral_aresetn(proc_sys_reset_1_peripheral_aresetn),
        .s_axi2_araddr(axi_interconnect_1_M00_AXI_ARADDR),
        .s_axi2_arready(axi_interconnect_1_M00_AXI_ARREADY),
        .s_axi2_arvalid(axi_interconnect_1_M00_AXI_ARVALID),
        .s_axi2_awaddr(axi_interconnect_1_M00_AXI_AWADDR),
        .s_axi2_awready(axi_interconnect_1_M00_AXI_AWREADY),
        .s_axi2_awvalid(axi_interconnect_1_M00_AXI_AWVALID),
        .s_axi2_bready(axi_interconnect_1_M00_AXI_BREADY),
        .s_axi2_bresp(axi_interconnect_1_M00_AXI_BRESP),
        .s_axi2_bvalid(axi_interconnect_1_M00_AXI_BVALID),
        .s_axi2_rdata(axi_interconnect_1_M00_AXI_RDATA),
        .s_axi2_rready(axi_interconnect_1_M00_AXI_RREADY),
        .s_axi2_rresp(axi_interconnect_1_M00_AXI_RRESP),
        .s_axi2_rvalid(axi_interconnect_1_M00_AXI_RVALID),
        .s_axi2_wdata(axi_interconnect_1_M00_AXI_WDATA),
        .s_axi2_wready(axi_interconnect_1_M00_AXI_WREADY),
        .s_axi2_wstrb(axi_interconnect_1_M00_AXI_WSTRB),
        .s_axi2_wvalid(axi_interconnect_1_M00_AXI_WVALID),
        .s_axi_aclk(clk_wiz_1_clk_out3));
dp_system_axi_interconnect_1_0 axi_interconnect_1
       (.ACLK(clk_wiz_1_clk_out3),
        .ARESETN(proc_sys_reset_1_interconnect_aresetn),
        .M00_ACLK(clk_wiz_1_clk_out3),
        .M00_ARESETN(proc_sys_reset_1_peripheral_aresetn),
        .M00_AXI_araddr(axi_interconnect_1_M00_AXI_ARADDR),
        .M00_AXI_arready(axi_interconnect_1_M00_AXI_ARREADY),
        .M00_AXI_arvalid(axi_interconnect_1_M00_AXI_ARVALID),
        .M00_AXI_awaddr(axi_interconnect_1_M00_AXI_AWADDR),
        .M00_AXI_awready(axi_interconnect_1_M00_AXI_AWREADY),
        .M00_AXI_awvalid(axi_interconnect_1_M00_AXI_AWVALID),
        .M00_AXI_bready(axi_interconnect_1_M00_AXI_BREADY),
        .M00_AXI_bresp(axi_interconnect_1_M00_AXI_BRESP),
        .M00_AXI_bvalid(axi_interconnect_1_M00_AXI_BVALID),
        .M00_AXI_rdata(axi_interconnect_1_M00_AXI_RDATA),
        .M00_AXI_rready(axi_interconnect_1_M00_AXI_RREADY),
        .M00_AXI_rresp(axi_interconnect_1_M00_AXI_RRESP),
        .M00_AXI_rvalid(axi_interconnect_1_M00_AXI_RVALID),
        .M00_AXI_wdata(axi_interconnect_1_M00_AXI_WDATA),
        .M00_AXI_wready(axi_interconnect_1_M00_AXI_WREADY),
        .M00_AXI_wstrb(axi_interconnect_1_M00_AXI_WSTRB),
        .M00_AXI_wvalid(axi_interconnect_1_M00_AXI_WVALID),
        .M01_ACLK(clk_wiz_1_clk_out3),
        .M01_ARESETN(proc_sys_reset_1_peripheral_aresetn),
        .M01_AXI_araddr(AXI4_LITE_1_ARADDR),
        .M01_AXI_arready(AXI4_LITE_1_ARREADY),
        .M01_AXI_arvalid(AXI4_LITE_1_ARVALID),
        .M01_AXI_awaddr(AXI4_LITE_1_AWADDR),
        .M01_AXI_awready(AXI4_LITE_1_AWREADY),
        .M01_AXI_awvalid(AXI4_LITE_1_AWVALID),
        .M01_AXI_bready(AXI4_LITE_1_BREADY),
        .M01_AXI_bresp(AXI4_LITE_1_BRESP),
        .M01_AXI_bvalid(AXI4_LITE_1_BVALID),
        .M01_AXI_rdata(AXI4_LITE_1_RDATA),
        .M01_AXI_rready(AXI4_LITE_1_RREADY),
        .M01_AXI_rresp(AXI4_LITE_1_RRESP),
        .M01_AXI_rvalid(AXI4_LITE_1_RVALID),
        .M01_AXI_wdata(AXI4_LITE_1_WDATA),
        .M01_AXI_wready(AXI4_LITE_1_WREADY),
        .M01_AXI_wvalid(AXI4_LITE_1_WVALID),
        .M02_ACLK(clk_wiz_1_clk_out3),
        .M02_ARESETN(proc_sys_reset_1_peripheral_aresetn),
        .M02_AXI_araddr(S_AXI3_1_ARADDR),
        .M02_AXI_arready(S_AXI3_1_ARREADY),
        .M02_AXI_arvalid(S_AXI3_1_ARVALID),
        .M02_AXI_awaddr(S_AXI3_1_AWADDR),
        .M02_AXI_awready(S_AXI3_1_AWREADY),
        .M02_AXI_awvalid(S_AXI3_1_AWVALID),
        .M02_AXI_bready(S_AXI3_1_BREADY),
        .M02_AXI_bresp(S_AXI3_1_BRESP),
        .M02_AXI_bvalid(S_AXI3_1_BVALID),
        .M02_AXI_rdata(S_AXI3_1_RDATA),
        .M02_AXI_rready(S_AXI3_1_RREADY),
        .M02_AXI_rresp(S_AXI3_1_RRESP),
        .M02_AXI_rvalid(S_AXI3_1_RVALID),
        .M02_AXI_wdata(S_AXI3_1_WDATA),
        .M02_AXI_wready(S_AXI3_1_WREADY),
        .M02_AXI_wstrb(S_AXI3_1_WSTRB),
        .M02_AXI_wvalid(S_AXI3_1_WVALID),
        .M03_ACLK(clk_wiz_1_clk_out3),
        .M03_ARESETN(proc_sys_reset_1_peripheral_aresetn),
        .M03_AXI_araddr(axi_interconnect_1_M03_AXI_ARADDR),
        .M03_AXI_arprot(axi_interconnect_1_M03_AXI_ARPROT),
        .M03_AXI_arready(axi_interconnect_1_M03_AXI_ARREADY),
        .M03_AXI_arvalid(axi_interconnect_1_M03_AXI_ARVALID),
        .M03_AXI_awaddr(axi_interconnect_1_M03_AXI_AWADDR),
        .M03_AXI_awprot(axi_interconnect_1_M03_AXI_AWPROT),
        .M03_AXI_awready(axi_interconnect_1_M03_AXI_AWREADY),
        .M03_AXI_awvalid(axi_interconnect_1_M03_AXI_AWVALID),
        .M03_AXI_bready(axi_interconnect_1_M03_AXI_BREADY),
        .M03_AXI_bresp(axi_interconnect_1_M03_AXI_BRESP),
        .M03_AXI_bvalid(axi_interconnect_1_M03_AXI_BVALID),
        .M03_AXI_rdata(axi_interconnect_1_M03_AXI_RDATA),
        .M03_AXI_rready(axi_interconnect_1_M03_AXI_RREADY),
        .M03_AXI_rresp(axi_interconnect_1_M03_AXI_RRESP),
        .M03_AXI_rvalid(axi_interconnect_1_M03_AXI_RVALID),
        .M03_AXI_wdata(axi_interconnect_1_M03_AXI_WDATA),
        .M03_AXI_wready(axi_interconnect_1_M03_AXI_WREADY),
        .M03_AXI_wstrb(axi_interconnect_1_M03_AXI_WSTRB),
        .M03_AXI_wvalid(axi_interconnect_1_M03_AXI_WVALID),
        .M04_ACLK(clk_wiz_1_clk_out3),
        .M04_ARESETN(proc_sys_reset_1_peripheral_aresetn),
        .M04_AXI_araddr(S_AXI_1_ARADDR),
        .M04_AXI_arready(S_AXI_1_ARREADY),
        .M04_AXI_arvalid(S_AXI_1_ARVALID),
        .M04_AXI_awaddr(S_AXI_1_AWADDR),
        .M04_AXI_awready(S_AXI_1_AWREADY),
        .M04_AXI_awvalid(S_AXI_1_AWVALID),
        .M04_AXI_bready(S_AXI_1_BREADY),
        .M04_AXI_bresp(S_AXI_1_BRESP),
        .M04_AXI_bvalid(S_AXI_1_BVALID),
        .M04_AXI_rdata(S_AXI_1_RDATA),
        .M04_AXI_rready(S_AXI_1_RREADY),
        .M04_AXI_rresp(S_AXI_1_RRESP),
        .M04_AXI_rvalid(S_AXI_1_RVALID),
        .M04_AXI_wdata(S_AXI_1_WDATA),
        .M04_AXI_wready(S_AXI_1_WREADY),
        .M04_AXI_wstrb(S_AXI_1_WSTRB),
        .M04_AXI_wvalid(S_AXI_1_WVALID),
        .S00_ACLK(clk_wiz_1_clk_out3),
        .S00_ARESETN(proc_sys_reset_1_peripheral_aresetn),
        .S00_AXI_araddr(S00_AXI_1_ARADDR),
        .S00_AXI_arburst(S00_AXI_1_ARBURST),
        .S00_AXI_arcache(S00_AXI_1_ARCACHE),
        .S00_AXI_arid(S00_AXI_1_ARID),
        .S00_AXI_arlen(S00_AXI_1_ARLEN),
        .S00_AXI_arlock(S00_AXI_1_ARLOCK),
        .S00_AXI_arprot(S00_AXI_1_ARPROT),
        .S00_AXI_arqos(S00_AXI_1_ARQOS),
        .S00_AXI_arready(S00_AXI_1_ARREADY),
        .S00_AXI_arsize(S00_AXI_1_ARSIZE),
        .S00_AXI_arvalid(S00_AXI_1_ARVALID),
        .S00_AXI_awaddr(S00_AXI_1_AWADDR),
        .S00_AXI_awburst(S00_AXI_1_AWBURST),
        .S00_AXI_awcache(S00_AXI_1_AWCACHE),
        .S00_AXI_awid(S00_AXI_1_AWID),
        .S00_AXI_awlen(S00_AXI_1_AWLEN),
        .S00_AXI_awlock(S00_AXI_1_AWLOCK),
        .S00_AXI_awprot(S00_AXI_1_AWPROT),
        .S00_AXI_awqos(S00_AXI_1_AWQOS),
        .S00_AXI_awready(S00_AXI_1_AWREADY),
        .S00_AXI_awsize(S00_AXI_1_AWSIZE),
        .S00_AXI_awvalid(S00_AXI_1_AWVALID),
        .S00_AXI_bid(S00_AXI_1_BID),
        .S00_AXI_bready(S00_AXI_1_BREADY),
        .S00_AXI_bresp(S00_AXI_1_BRESP),
        .S00_AXI_bvalid(S00_AXI_1_BVALID),
        .S00_AXI_rdata(S00_AXI_1_RDATA),
        .S00_AXI_rid(S00_AXI_1_RID),
        .S00_AXI_rlast(S00_AXI_1_RLAST),
        .S00_AXI_rready(S00_AXI_1_RREADY),
        .S00_AXI_rresp(S00_AXI_1_RRESP),
        .S00_AXI_rvalid(S00_AXI_1_RVALID),
        .S00_AXI_wdata(S00_AXI_1_WDATA),
        .S00_AXI_wid(S00_AXI_1_WID),
        .S00_AXI_wlast(S00_AXI_1_WLAST),
        .S00_AXI_wready(S00_AXI_1_WREADY),
        .S00_AXI_wstrb(S00_AXI_1_WSTRB),
        .S00_AXI_wvalid(S00_AXI_1_WVALID));
dp_system_c_shift_ram_0_0 axi_lite_reset_shift_reg
       (.CLK(clk_wiz_1_clk_out3),
        .D(util_vector_logic_0_Res),
        .Q(c_shift_ram_0_Q));
dp_system_clk_wiz_0_1 clk_wiz_0
       (.clk_in1_n(CLK_IN1_D_1_CLK_N),
        .clk_in1_p(CLK_IN1_D_1_CLK_P),
        .clk_out1(clk_wiz_0_clk_out1),
        .reset(util_vector_logic_0_Res));
dp_system_clk_wiz_0_0 clk_wiz_1
       (.clk_in1(clk_wiz_0_clk_out1),
        .clk_out1(clk_wiz_1_clk_out1),
        .clk_out2(clk_wiz_1_clk_out2),
        .clk_out3(clk_wiz_1_clk_out3),
        .locked(clk_wiz_1_locked),
        .reset(util_vector_logic_0_Res));
dp_system_clk_wiz_2_0 clk_wiz_2
       (.clk_in1(clk_wiz_1_clk_out1),
        .clk_out1(clk_wiz_2_clk_out1),
        .clk_out2(clk_wiz_2_clk_out2),
        .locked(clk_wiz_2_locked),
        .reset(c_shift_ram_0_Q));
dp_system_displayport_0_0 displayport_0
       (.aux_tx_io_n(aux_tx_io_n),
        .aux_tx_io_p(aux_tx_io_p),
        .axi_int(displayport_0_axi_int),
        .lnk_clk(displayport_0_lnk_clk),
        .lnk_clk_n(displayport_0_dp_mainlink_LNK_CLK_N),
        .lnk_clk_p(displayport_0_dp_mainlink_LNK_CLK_P),
        .lnk_tx_lane_n(displayport_0_dp_mainlink_LNK_TX_LANE_N),
        .lnk_tx_lane_p(displayport_0_dp_mainlink_LNK_TX_LANE_P),
        .s_axi_aclk(clk_wiz_1_clk_out3),
        .s_axi_araddr(axi_interconnect_1_M03_AXI_ARADDR),
        .s_axi_aresetn(proc_sys_reset_1_peripheral_aresetn),
        .s_axi_arprot(axi_interconnect_1_M03_AXI_ARPROT),
        .s_axi_arready(axi_interconnect_1_M03_AXI_ARREADY),
        .s_axi_arvalid(axi_interconnect_1_M03_AXI_ARVALID),
        .s_axi_awaddr(axi_interconnect_1_M03_AXI_AWADDR),
        .s_axi_awprot(axi_interconnect_1_M03_AXI_AWPROT),
        .s_axi_awready(axi_interconnect_1_M03_AXI_AWREADY),
        .s_axi_awvalid(axi_interconnect_1_M03_AXI_AWVALID),
        .s_axi_bready(axi_interconnect_1_M03_AXI_BREADY),
        .s_axi_bresp(axi_interconnect_1_M03_AXI_BRESP),
        .s_axi_bvalid(axi_interconnect_1_M03_AXI_BVALID),
        .s_axi_rdata(axi_interconnect_1_M03_AXI_RDATA),
        .s_axi_rready(axi_interconnect_1_M03_AXI_RREADY),
        .s_axi_rresp(axi_interconnect_1_M03_AXI_RRESP),
        .s_axi_rvalid(axi_interconnect_1_M03_AXI_RVALID),
        .s_axi_wdata(axi_interconnect_1_M03_AXI_WDATA),
        .s_axi_wready(axi_interconnect_1_M03_AXI_WREADY),
        .s_axi_wstrb(axi_interconnect_1_M03_AXI_WSTRB),
        .s_axi_wvalid(axi_interconnect_1_M03_AXI_WVALID),
        .tx_hpd(tx_hpd_1),
        .tx_vid_clk(video_transmitter_1_vid_clk),
        .tx_vid_enable(video_transmitter_1_dp_vid_inf_TX_VID_ENABLE),
        .tx_vid_hsync(video_transmitter_1_dp_vid_inf_TX_VID_HSYNC),
        .tx_vid_oddeven(video_transmitter_1_dp_vid_inf_TX_VID_ODDEVEN),
        .tx_vid_pixel0(video_transmitter_1_dp_vid_inf_TX_VID_PIXEL0),
        .tx_vid_pixel1(video_transmitter_1_dp_vid_inf_TX_VID_PIXEL1),
        .tx_vid_rst(dp_tx_vid_reset_shift_reg_Q),
        .tx_vid_vsync(video_transmitter_1_dp_vid_inf_TX_VID_VSYNC));
dp_system_c_shift_ram_1_0 dp_tx_vid_reset_shift_reg
       (.CLK(clk_wiz_1_clk_out3),
        .D(util_vector_logic_0_Res),
        .Q(dp_tx_vid_reset_shift_reg_Q));
dp_system_util_vector_logic_0_0 util_vector_logic_0
       (.Op1(axi4_lite_peripherals_FCLK_RESET0_N),
        .Res(util_vector_logic_0_Res));
dp_system_util_vector_logic_1_0 util_vector_logic_1
       (.Op1(clk_wiz_2_locked),
        .Res(util_vector_logic_1_Res));
dp_system_vid_clkgen_0_0 vid_clkgen_0
       (.D_i(video_pat_gen_0_vid_clk_D),
        .M_i(video_pat_gen_0_vid_clk_M),
        .prog_i(video_pat_gen_0_vid_clk_prog),
        .progclk_i(clk_wiz_1_clk_out1),
        .refclk_i(displayport_0_lnk_clk),
        .reset_i(video_pat_gen_0_sw_vid_reset));
dp_system_video_pat_gen_0_0 video_pat_gen_0
       (.apb_addr(axi4_lite_peripherals_APB_M_PADDR),
        .apb_clk(clk_wiz_1_clk_out3),
        .apb_enable(axi4_lite_peripherals_APB_M_PENABLE),
        .apb_pready(axi4_lite_peripherals_APB_M_PREADY),
        .apb_pslverr(axi4_lite_peripherals_APB_M_PSLVERR),
        .apb_rdata(axi4_lite_peripherals_APB_M_PRDATA),
        .apb_reset(c_shift_ram_0_Q),
        .apb_select(axi4_lite_peripherals_APB_M_PSEL),
        .apb_wdata(axi4_lite_peripherals_APB_M_PWDATA),
        .apb_write(axi4_lite_peripherals_APB_M_PWRITE),
        .run_pattern(xlconstant_1_dout),
        .sw_vid_reset(video_pat_gen_0_sw_vid_reset),
        .vid_clk(video_transmitter_1_vid_clk),
        .vid_clk_D(video_pat_gen_0_vid_clk_D),
        .vid_clk_M(video_pat_gen_0_vid_clk_M),
        .vid_clk_prog(video_pat_gen_0_vid_clk_prog),
        .vid_reset(util_vector_logic_1_Res));
dp_system_video_transmitter_1_0 video_transmitter_1
       (.config_done(xlconstant_0_dout),
        .reset(util_vector_logic_1_Res),
        .start(vio_0_probe_out0),
        .uhd_en_in(vio_0_probe_out1),
        .vid_clk(video_transmitter_1_vid_clk),
        .vid_enable(video_transmitter_1_dp_vid_inf_TX_VID_ENABLE),
        .vid_hsync(video_transmitter_1_dp_vid_inf_TX_VID_HSYNC),
        .vid_oddeven(video_transmitter_1_dp_vid_inf_TX_VID_ODDEVEN),
        .vid_pixel0(video_transmitter_1_dp_vid_inf_TX_VID_PIXEL0),
        .vid_pixel1(video_transmitter_1_dp_vid_inf_TX_VID_PIXEL1),
        .vid_run_clk_150(clk_wiz_2_clk_out2),
        .vid_run_clk_75(clk_wiz_2_clk_out1),
        .vid_vsync(video_transmitter_1_dp_vid_inf_TX_VID_VSYNC));
dp_system_vio_0_0 vio_0
       (.clk(clk_wiz_2_clk_out1),
        .probe_out0(vio_0_probe_out0),
        .probe_out1(vio_0_probe_out1));
dp_system_xlconstant_1_0 xlconstant_0
       (.dout(xlconstant_1_dout));
dp_system_xlconstant_0_1 xlconstant_1
       (.dout(xlconstant_0_dout));
endmodule

module dp_system_axi_interconnect_1_0
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arready,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awready,
    M00_AXI_awvalid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    M01_ACLK,
    M01_ARESETN,
    M01_AXI_araddr,
    M01_AXI_arready,
    M01_AXI_arvalid,
    M01_AXI_awaddr,
    M01_AXI_awready,
    M01_AXI_awvalid,
    M01_AXI_bready,
    M01_AXI_bresp,
    M01_AXI_bvalid,
    M01_AXI_rdata,
    M01_AXI_rready,
    M01_AXI_rresp,
    M01_AXI_rvalid,
    M01_AXI_wdata,
    M01_AXI_wready,
    M01_AXI_wvalid,
    M02_ACLK,
    M02_ARESETN,
    M02_AXI_araddr,
    M02_AXI_arready,
    M02_AXI_arvalid,
    M02_AXI_awaddr,
    M02_AXI_awready,
    M02_AXI_awvalid,
    M02_AXI_bready,
    M02_AXI_bresp,
    M02_AXI_bvalid,
    M02_AXI_rdata,
    M02_AXI_rready,
    M02_AXI_rresp,
    M02_AXI_rvalid,
    M02_AXI_wdata,
    M02_AXI_wready,
    M02_AXI_wstrb,
    M02_AXI_wvalid,
    M03_ACLK,
    M03_ARESETN,
    M03_AXI_araddr,
    M03_AXI_arprot,
    M03_AXI_arready,
    M03_AXI_arvalid,
    M03_AXI_awaddr,
    M03_AXI_awprot,
    M03_AXI_awready,
    M03_AXI_awvalid,
    M03_AXI_bready,
    M03_AXI_bresp,
    M03_AXI_bvalid,
    M03_AXI_rdata,
    M03_AXI_rready,
    M03_AXI_rresp,
    M03_AXI_rvalid,
    M03_AXI_wdata,
    M03_AXI_wready,
    M03_AXI_wstrb,
    M03_AXI_wvalid,
    M04_ACLK,
    M04_ARESETN,
    M04_AXI_araddr,
    M04_AXI_arready,
    M04_AXI_arvalid,
    M04_AXI_awaddr,
    M04_AXI_awready,
    M04_AXI_awvalid,
    M04_AXI_bready,
    M04_AXI_bresp,
    M04_AXI_bvalid,
    M04_AXI_rdata,
    M04_AXI_rready,
    M04_AXI_rresp,
    M04_AXI_rvalid,
    M04_AXI_wdata,
    M04_AXI_wready,
    M04_AXI_wstrb,
    M04_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arburst,
    S00_AXI_arcache,
    S00_AXI_arid,
    S00_AXI_arlen,
    S00_AXI_arlock,
    S00_AXI_arprot,
    S00_AXI_arqos,
    S00_AXI_arready,
    S00_AXI_arsize,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awburst,
    S00_AXI_awcache,
    S00_AXI_awid,
    S00_AXI_awlen,
    S00_AXI_awlock,
    S00_AXI_awprot,
    S00_AXI_awqos,
    S00_AXI_awready,
    S00_AXI_awsize,
    S00_AXI_awvalid,
    S00_AXI_bid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rid,
    S00_AXI_rlast,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wid,
    S00_AXI_wlast,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid);
  input ACLK;
  input [0:0]ARESETN;
  input M00_ACLK;
  input [0:0]M00_ARESETN;
  output [8:0]M00_AXI_araddr;
  input [0:0]M00_AXI_arready;
  output [0:0]M00_AXI_arvalid;
  output [8:0]M00_AXI_awaddr;
  input [0:0]M00_AXI_awready;
  output [0:0]M00_AXI_awvalid;
  output [0:0]M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input [0:0]M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  output [0:0]M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input [0:0]M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  input [0:0]M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output [0:0]M00_AXI_wvalid;
  input M01_ACLK;
  input [0:0]M01_ARESETN;
  output [31:0]M01_AXI_araddr;
  input [0:0]M01_AXI_arready;
  output [0:0]M01_AXI_arvalid;
  output [31:0]M01_AXI_awaddr;
  input [0:0]M01_AXI_awready;
  output [0:0]M01_AXI_awvalid;
  output [0:0]M01_AXI_bready;
  input [1:0]M01_AXI_bresp;
  input [0:0]M01_AXI_bvalid;
  input [31:0]M01_AXI_rdata;
  output [0:0]M01_AXI_rready;
  input [1:0]M01_AXI_rresp;
  input [0:0]M01_AXI_rvalid;
  output [31:0]M01_AXI_wdata;
  input [0:0]M01_AXI_wready;
  output [0:0]M01_AXI_wvalid;
  input M02_ACLK;
  input [0:0]M02_ARESETN;
  output [4:0]M02_AXI_araddr;
  input [0:0]M02_AXI_arready;
  output [0:0]M02_AXI_arvalid;
  output [4:0]M02_AXI_awaddr;
  input [0:0]M02_AXI_awready;
  output [0:0]M02_AXI_awvalid;
  output [0:0]M02_AXI_bready;
  input [1:0]M02_AXI_bresp;
  input [0:0]M02_AXI_bvalid;
  input [31:0]M02_AXI_rdata;
  output [0:0]M02_AXI_rready;
  input [1:0]M02_AXI_rresp;
  input [0:0]M02_AXI_rvalid;
  output [31:0]M02_AXI_wdata;
  input [0:0]M02_AXI_wready;
  output [3:0]M02_AXI_wstrb;
  output [0:0]M02_AXI_wvalid;
  input M03_ACLK;
  input [0:0]M03_ARESETN;
  output [31:0]M03_AXI_araddr;
  output [2:0]M03_AXI_arprot;
  input [0:0]M03_AXI_arready;
  output [0:0]M03_AXI_arvalid;
  output [31:0]M03_AXI_awaddr;
  output [2:0]M03_AXI_awprot;
  input [0:0]M03_AXI_awready;
  output [0:0]M03_AXI_awvalid;
  output [0:0]M03_AXI_bready;
  input [1:0]M03_AXI_bresp;
  input [0:0]M03_AXI_bvalid;
  input [31:0]M03_AXI_rdata;
  output [0:0]M03_AXI_rready;
  input [1:0]M03_AXI_rresp;
  input [0:0]M03_AXI_rvalid;
  output [31:0]M03_AXI_wdata;
  input [0:0]M03_AXI_wready;
  output [3:0]M03_AXI_wstrb;
  output [0:0]M03_AXI_wvalid;
  input M04_ACLK;
  input [0:0]M04_ARESETN;
  output [8:0]M04_AXI_araddr;
  input [0:0]M04_AXI_arready;
  output [0:0]M04_AXI_arvalid;
  output [8:0]M04_AXI_awaddr;
  input [0:0]M04_AXI_awready;
  output [0:0]M04_AXI_awvalid;
  output [0:0]M04_AXI_bready;
  input [1:0]M04_AXI_bresp;
  input [0:0]M04_AXI_bvalid;
  input [31:0]M04_AXI_rdata;
  output [0:0]M04_AXI_rready;
  input [1:0]M04_AXI_rresp;
  input [0:0]M04_AXI_rvalid;
  output [31:0]M04_AXI_wdata;
  input [0:0]M04_AXI_wready;
  output [3:0]M04_AXI_wstrb;
  output [0:0]M04_AXI_wvalid;
  input S00_ACLK;
  input [0:0]S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [1:0]S00_AXI_arburst;
  input [3:0]S00_AXI_arcache;
  input [11:0]S00_AXI_arid;
  input [3:0]S00_AXI_arlen;
  input [1:0]S00_AXI_arlock;
  input [2:0]S00_AXI_arprot;
  input [3:0]S00_AXI_arqos;
  output S00_AXI_arready;
  input [2:0]S00_AXI_arsize;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [1:0]S00_AXI_awburst;
  input [3:0]S00_AXI_awcache;
  input [11:0]S00_AXI_awid;
  input [3:0]S00_AXI_awlen;
  input [1:0]S00_AXI_awlock;
  input [2:0]S00_AXI_awprot;
  input [3:0]S00_AXI_awqos;
  output S00_AXI_awready;
  input [2:0]S00_AXI_awsize;
  input S00_AXI_awvalid;
  output [11:0]S00_AXI_bid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  output [11:0]S00_AXI_rid;
  output S00_AXI_rlast;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  input [11:0]S00_AXI_wid;
  input S00_AXI_wlast;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;

  wire M00_ACLK_1;
  wire [0:0]M00_ARESETN_1;
  wire M01_ACLK_1;
  wire [0:0]M01_ARESETN_1;
  wire M02_ACLK_1;
  wire [0:0]M02_ARESETN_1;
  wire M03_ACLK_1;
  wire [0:0]M03_ARESETN_1;
  wire M04_ACLK_1;
  wire [0:0]M04_ARESETN_1;
  wire S00_ACLK_1;
  wire [0:0]S00_ARESETN_1;
  wire axi_interconnect_1_ACLK_net;
  wire [0:0]axi_interconnect_1_ARESETN_net;
  wire [31:0]axi_interconnect_1_to_s00_couplers_ARADDR;
  wire [1:0]axi_interconnect_1_to_s00_couplers_ARBURST;
  wire [3:0]axi_interconnect_1_to_s00_couplers_ARCACHE;
  wire [11:0]axi_interconnect_1_to_s00_couplers_ARID;
  wire [3:0]axi_interconnect_1_to_s00_couplers_ARLEN;
  wire [1:0]axi_interconnect_1_to_s00_couplers_ARLOCK;
  wire [2:0]axi_interconnect_1_to_s00_couplers_ARPROT;
  wire [3:0]axi_interconnect_1_to_s00_couplers_ARQOS;
  wire axi_interconnect_1_to_s00_couplers_ARREADY;
  wire [2:0]axi_interconnect_1_to_s00_couplers_ARSIZE;
  wire axi_interconnect_1_to_s00_couplers_ARVALID;
  wire [31:0]axi_interconnect_1_to_s00_couplers_AWADDR;
  wire [1:0]axi_interconnect_1_to_s00_couplers_AWBURST;
  wire [3:0]axi_interconnect_1_to_s00_couplers_AWCACHE;
  wire [11:0]axi_interconnect_1_to_s00_couplers_AWID;
  wire [3:0]axi_interconnect_1_to_s00_couplers_AWLEN;
  wire [1:0]axi_interconnect_1_to_s00_couplers_AWLOCK;
  wire [2:0]axi_interconnect_1_to_s00_couplers_AWPROT;
  wire [3:0]axi_interconnect_1_to_s00_couplers_AWQOS;
  wire axi_interconnect_1_to_s00_couplers_AWREADY;
  wire [2:0]axi_interconnect_1_to_s00_couplers_AWSIZE;
  wire axi_interconnect_1_to_s00_couplers_AWVALID;
  wire [11:0]axi_interconnect_1_to_s00_couplers_BID;
  wire axi_interconnect_1_to_s00_couplers_BREADY;
  wire [1:0]axi_interconnect_1_to_s00_couplers_BRESP;
  wire axi_interconnect_1_to_s00_couplers_BVALID;
  wire [31:0]axi_interconnect_1_to_s00_couplers_RDATA;
  wire [11:0]axi_interconnect_1_to_s00_couplers_RID;
  wire axi_interconnect_1_to_s00_couplers_RLAST;
  wire axi_interconnect_1_to_s00_couplers_RREADY;
  wire [1:0]axi_interconnect_1_to_s00_couplers_RRESP;
  wire axi_interconnect_1_to_s00_couplers_RVALID;
  wire [31:0]axi_interconnect_1_to_s00_couplers_WDATA;
  wire [11:0]axi_interconnect_1_to_s00_couplers_WID;
  wire axi_interconnect_1_to_s00_couplers_WLAST;
  wire axi_interconnect_1_to_s00_couplers_WREADY;
  wire [3:0]axi_interconnect_1_to_s00_couplers_WSTRB;
  wire axi_interconnect_1_to_s00_couplers_WVALID;
  wire [8:0]m00_couplers_to_axi_interconnect_1_ARADDR;
  wire [0:0]m00_couplers_to_axi_interconnect_1_ARREADY;
  wire [0:0]m00_couplers_to_axi_interconnect_1_ARVALID;
  wire [8:0]m00_couplers_to_axi_interconnect_1_AWADDR;
  wire [0:0]m00_couplers_to_axi_interconnect_1_AWREADY;
  wire [0:0]m00_couplers_to_axi_interconnect_1_AWVALID;
  wire [0:0]m00_couplers_to_axi_interconnect_1_BREADY;
  wire [1:0]m00_couplers_to_axi_interconnect_1_BRESP;
  wire [0:0]m00_couplers_to_axi_interconnect_1_BVALID;
  wire [31:0]m00_couplers_to_axi_interconnect_1_RDATA;
  wire [0:0]m00_couplers_to_axi_interconnect_1_RREADY;
  wire [1:0]m00_couplers_to_axi_interconnect_1_RRESP;
  wire [0:0]m00_couplers_to_axi_interconnect_1_RVALID;
  wire [31:0]m00_couplers_to_axi_interconnect_1_WDATA;
  wire [0:0]m00_couplers_to_axi_interconnect_1_WREADY;
  wire [3:0]m00_couplers_to_axi_interconnect_1_WSTRB;
  wire [0:0]m00_couplers_to_axi_interconnect_1_WVALID;
  wire [31:0]m01_couplers_to_axi_interconnect_1_ARADDR;
  wire [0:0]m01_couplers_to_axi_interconnect_1_ARREADY;
  wire [0:0]m01_couplers_to_axi_interconnect_1_ARVALID;
  wire [31:0]m01_couplers_to_axi_interconnect_1_AWADDR;
  wire [0:0]m01_couplers_to_axi_interconnect_1_AWREADY;
  wire [0:0]m01_couplers_to_axi_interconnect_1_AWVALID;
  wire [0:0]m01_couplers_to_axi_interconnect_1_BREADY;
  wire [1:0]m01_couplers_to_axi_interconnect_1_BRESP;
  wire [0:0]m01_couplers_to_axi_interconnect_1_BVALID;
  wire [31:0]m01_couplers_to_axi_interconnect_1_RDATA;
  wire [0:0]m01_couplers_to_axi_interconnect_1_RREADY;
  wire [1:0]m01_couplers_to_axi_interconnect_1_RRESP;
  wire [0:0]m01_couplers_to_axi_interconnect_1_RVALID;
  wire [31:0]m01_couplers_to_axi_interconnect_1_WDATA;
  wire [0:0]m01_couplers_to_axi_interconnect_1_WREADY;
  wire [0:0]m01_couplers_to_axi_interconnect_1_WVALID;
  wire [4:0]m02_couplers_to_axi_interconnect_1_ARADDR;
  wire [0:0]m02_couplers_to_axi_interconnect_1_ARREADY;
  wire [0:0]m02_couplers_to_axi_interconnect_1_ARVALID;
  wire [4:0]m02_couplers_to_axi_interconnect_1_AWADDR;
  wire [0:0]m02_couplers_to_axi_interconnect_1_AWREADY;
  wire [0:0]m02_couplers_to_axi_interconnect_1_AWVALID;
  wire [0:0]m02_couplers_to_axi_interconnect_1_BREADY;
  wire [1:0]m02_couplers_to_axi_interconnect_1_BRESP;
  wire [0:0]m02_couplers_to_axi_interconnect_1_BVALID;
  wire [31:0]m02_couplers_to_axi_interconnect_1_RDATA;
  wire [0:0]m02_couplers_to_axi_interconnect_1_RREADY;
  wire [1:0]m02_couplers_to_axi_interconnect_1_RRESP;
  wire [0:0]m02_couplers_to_axi_interconnect_1_RVALID;
  wire [31:0]m02_couplers_to_axi_interconnect_1_WDATA;
  wire [0:0]m02_couplers_to_axi_interconnect_1_WREADY;
  wire [3:0]m02_couplers_to_axi_interconnect_1_WSTRB;
  wire [0:0]m02_couplers_to_axi_interconnect_1_WVALID;
  wire [31:0]m03_couplers_to_axi_interconnect_1_ARADDR;
  wire [2:0]m03_couplers_to_axi_interconnect_1_ARPROT;
  wire [0:0]m03_couplers_to_axi_interconnect_1_ARREADY;
  wire [0:0]m03_couplers_to_axi_interconnect_1_ARVALID;
  wire [31:0]m03_couplers_to_axi_interconnect_1_AWADDR;
  wire [2:0]m03_couplers_to_axi_interconnect_1_AWPROT;
  wire [0:0]m03_couplers_to_axi_interconnect_1_AWREADY;
  wire [0:0]m03_couplers_to_axi_interconnect_1_AWVALID;
  wire [0:0]m03_couplers_to_axi_interconnect_1_BREADY;
  wire [1:0]m03_couplers_to_axi_interconnect_1_BRESP;
  wire [0:0]m03_couplers_to_axi_interconnect_1_BVALID;
  wire [31:0]m03_couplers_to_axi_interconnect_1_RDATA;
  wire [0:0]m03_couplers_to_axi_interconnect_1_RREADY;
  wire [1:0]m03_couplers_to_axi_interconnect_1_RRESP;
  wire [0:0]m03_couplers_to_axi_interconnect_1_RVALID;
  wire [31:0]m03_couplers_to_axi_interconnect_1_WDATA;
  wire [0:0]m03_couplers_to_axi_interconnect_1_WREADY;
  wire [3:0]m03_couplers_to_axi_interconnect_1_WSTRB;
  wire [0:0]m03_couplers_to_axi_interconnect_1_WVALID;
  wire [8:0]m04_couplers_to_axi_interconnect_1_ARADDR;
  wire [0:0]m04_couplers_to_axi_interconnect_1_ARREADY;
  wire [0:0]m04_couplers_to_axi_interconnect_1_ARVALID;
  wire [8:0]m04_couplers_to_axi_interconnect_1_AWADDR;
  wire [0:0]m04_couplers_to_axi_interconnect_1_AWREADY;
  wire [0:0]m04_couplers_to_axi_interconnect_1_AWVALID;
  wire [0:0]m04_couplers_to_axi_interconnect_1_BREADY;
  wire [1:0]m04_couplers_to_axi_interconnect_1_BRESP;
  wire [0:0]m04_couplers_to_axi_interconnect_1_BVALID;
  wire [31:0]m04_couplers_to_axi_interconnect_1_RDATA;
  wire [0:0]m04_couplers_to_axi_interconnect_1_RREADY;
  wire [1:0]m04_couplers_to_axi_interconnect_1_RRESP;
  wire [0:0]m04_couplers_to_axi_interconnect_1_RVALID;
  wire [31:0]m04_couplers_to_axi_interconnect_1_WDATA;
  wire [0:0]m04_couplers_to_axi_interconnect_1_WREADY;
  wire [3:0]m04_couplers_to_axi_interconnect_1_WSTRB;
  wire [0:0]m04_couplers_to_axi_interconnect_1_WVALID;
  wire [31:0]s00_couplers_to_xbar_ARADDR;
  wire [2:0]s00_couplers_to_xbar_ARPROT;
  wire [0:0]s00_couplers_to_xbar_ARREADY;
  wire s00_couplers_to_xbar_ARVALID;
  wire [31:0]s00_couplers_to_xbar_AWADDR;
  wire [2:0]s00_couplers_to_xbar_AWPROT;
  wire [0:0]s00_couplers_to_xbar_AWREADY;
  wire s00_couplers_to_xbar_AWVALID;
  wire s00_couplers_to_xbar_BREADY;
  wire [1:0]s00_couplers_to_xbar_BRESP;
  wire [0:0]s00_couplers_to_xbar_BVALID;
  wire [31:0]s00_couplers_to_xbar_RDATA;
  wire s00_couplers_to_xbar_RREADY;
  wire [1:0]s00_couplers_to_xbar_RRESP;
  wire [0:0]s00_couplers_to_xbar_RVALID;
  wire [31:0]s00_couplers_to_xbar_WDATA;
  wire [0:0]s00_couplers_to_xbar_WREADY;
  wire [3:0]s00_couplers_to_xbar_WSTRB;
  wire s00_couplers_to_xbar_WVALID;
  wire [31:0]xbar_to_m00_couplers_ARADDR;
  wire [0:0]xbar_to_m00_couplers_ARREADY;
  wire [0:0]xbar_to_m00_couplers_ARVALID;
  wire [31:0]xbar_to_m00_couplers_AWADDR;
  wire [0:0]xbar_to_m00_couplers_AWREADY;
  wire [0:0]xbar_to_m00_couplers_AWVALID;
  wire [0:0]xbar_to_m00_couplers_BREADY;
  wire [1:0]xbar_to_m00_couplers_BRESP;
  wire [0:0]xbar_to_m00_couplers_BVALID;
  wire [31:0]xbar_to_m00_couplers_RDATA;
  wire [0:0]xbar_to_m00_couplers_RREADY;
  wire [1:0]xbar_to_m00_couplers_RRESP;
  wire [0:0]xbar_to_m00_couplers_RVALID;
  wire [31:0]xbar_to_m00_couplers_WDATA;
  wire [0:0]xbar_to_m00_couplers_WREADY;
  wire [3:0]xbar_to_m00_couplers_WSTRB;
  wire [0:0]xbar_to_m00_couplers_WVALID;
  wire [63:32]xbar_to_m01_couplers_ARADDR;
  wire [0:0]xbar_to_m01_couplers_ARREADY;
  wire [1:1]xbar_to_m01_couplers_ARVALID;
  wire [63:32]xbar_to_m01_couplers_AWADDR;
  wire [0:0]xbar_to_m01_couplers_AWREADY;
  wire [1:1]xbar_to_m01_couplers_AWVALID;
  wire [1:1]xbar_to_m01_couplers_BREADY;
  wire [1:0]xbar_to_m01_couplers_BRESP;
  wire [0:0]xbar_to_m01_couplers_BVALID;
  wire [31:0]xbar_to_m01_couplers_RDATA;
  wire [1:1]xbar_to_m01_couplers_RREADY;
  wire [1:0]xbar_to_m01_couplers_RRESP;
  wire [0:0]xbar_to_m01_couplers_RVALID;
  wire [63:32]xbar_to_m01_couplers_WDATA;
  wire [0:0]xbar_to_m01_couplers_WREADY;
  wire [1:1]xbar_to_m01_couplers_WVALID;
  wire [95:64]xbar_to_m02_couplers_ARADDR;
  wire [0:0]xbar_to_m02_couplers_ARREADY;
  wire [2:2]xbar_to_m02_couplers_ARVALID;
  wire [95:64]xbar_to_m02_couplers_AWADDR;
  wire [0:0]xbar_to_m02_couplers_AWREADY;
  wire [2:2]xbar_to_m02_couplers_AWVALID;
  wire [2:2]xbar_to_m02_couplers_BREADY;
  wire [1:0]xbar_to_m02_couplers_BRESP;
  wire [0:0]xbar_to_m02_couplers_BVALID;
  wire [31:0]xbar_to_m02_couplers_RDATA;
  wire [2:2]xbar_to_m02_couplers_RREADY;
  wire [1:0]xbar_to_m02_couplers_RRESP;
  wire [0:0]xbar_to_m02_couplers_RVALID;
  wire [95:64]xbar_to_m02_couplers_WDATA;
  wire [0:0]xbar_to_m02_couplers_WREADY;
  wire [11:8]xbar_to_m02_couplers_WSTRB;
  wire [2:2]xbar_to_m02_couplers_WVALID;
  wire [127:96]xbar_to_m03_couplers_ARADDR;
  wire [11:9]xbar_to_m03_couplers_ARPROT;
  wire [0:0]xbar_to_m03_couplers_ARREADY;
  wire [3:3]xbar_to_m03_couplers_ARVALID;
  wire [127:96]xbar_to_m03_couplers_AWADDR;
  wire [11:9]xbar_to_m03_couplers_AWPROT;
  wire [0:0]xbar_to_m03_couplers_AWREADY;
  wire [3:3]xbar_to_m03_couplers_AWVALID;
  wire [3:3]xbar_to_m03_couplers_BREADY;
  wire [1:0]xbar_to_m03_couplers_BRESP;
  wire [0:0]xbar_to_m03_couplers_BVALID;
  wire [31:0]xbar_to_m03_couplers_RDATA;
  wire [3:3]xbar_to_m03_couplers_RREADY;
  wire [1:0]xbar_to_m03_couplers_RRESP;
  wire [0:0]xbar_to_m03_couplers_RVALID;
  wire [127:96]xbar_to_m03_couplers_WDATA;
  wire [0:0]xbar_to_m03_couplers_WREADY;
  wire [15:12]xbar_to_m03_couplers_WSTRB;
  wire [3:3]xbar_to_m03_couplers_WVALID;
  wire [159:128]xbar_to_m04_couplers_ARADDR;
  wire [0:0]xbar_to_m04_couplers_ARREADY;
  wire [4:4]xbar_to_m04_couplers_ARVALID;
  wire [159:128]xbar_to_m04_couplers_AWADDR;
  wire [0:0]xbar_to_m04_couplers_AWREADY;
  wire [4:4]xbar_to_m04_couplers_AWVALID;
  wire [4:4]xbar_to_m04_couplers_BREADY;
  wire [1:0]xbar_to_m04_couplers_BRESP;
  wire [0:0]xbar_to_m04_couplers_BVALID;
  wire [31:0]xbar_to_m04_couplers_RDATA;
  wire [4:4]xbar_to_m04_couplers_RREADY;
  wire [1:0]xbar_to_m04_couplers_RRESP;
  wire [0:0]xbar_to_m04_couplers_RVALID;
  wire [159:128]xbar_to_m04_couplers_WDATA;
  wire [0:0]xbar_to_m04_couplers_WREADY;
  wire [19:16]xbar_to_m04_couplers_WSTRB;
  wire [4:4]xbar_to_m04_couplers_WVALID;
  wire [14:0]NLW_xbar_m_axi_arprot_UNCONNECTED;
  wire [14:0]NLW_xbar_m_axi_awprot_UNCONNECTED;
  wire [19:0]NLW_xbar_m_axi_wstrb_UNCONNECTED;

  assign M00_ACLK_1 = M00_ACLK;
  assign M00_ARESETN_1 = M00_ARESETN[0];
  assign M00_AXI_araddr[8:0] = m00_couplers_to_axi_interconnect_1_ARADDR;
  assign M00_AXI_arvalid[0] = m00_couplers_to_axi_interconnect_1_ARVALID;
  assign M00_AXI_awaddr[8:0] = m00_couplers_to_axi_interconnect_1_AWADDR;
  assign M00_AXI_awvalid[0] = m00_couplers_to_axi_interconnect_1_AWVALID;
  assign M00_AXI_bready[0] = m00_couplers_to_axi_interconnect_1_BREADY;
  assign M00_AXI_rready[0] = m00_couplers_to_axi_interconnect_1_RREADY;
  assign M00_AXI_wdata[31:0] = m00_couplers_to_axi_interconnect_1_WDATA;
  assign M00_AXI_wstrb[3:0] = m00_couplers_to_axi_interconnect_1_WSTRB;
  assign M00_AXI_wvalid[0] = m00_couplers_to_axi_interconnect_1_WVALID;
  assign M01_ACLK_1 = M01_ACLK;
  assign M01_ARESETN_1 = M01_ARESETN[0];
  assign M01_AXI_araddr[31:0] = m01_couplers_to_axi_interconnect_1_ARADDR;
  assign M01_AXI_arvalid[0] = m01_couplers_to_axi_interconnect_1_ARVALID;
  assign M01_AXI_awaddr[31:0] = m01_couplers_to_axi_interconnect_1_AWADDR;
  assign M01_AXI_awvalid[0] = m01_couplers_to_axi_interconnect_1_AWVALID;
  assign M01_AXI_bready[0] = m01_couplers_to_axi_interconnect_1_BREADY;
  assign M01_AXI_rready[0] = m01_couplers_to_axi_interconnect_1_RREADY;
  assign M01_AXI_wdata[31:0] = m01_couplers_to_axi_interconnect_1_WDATA;
  assign M01_AXI_wvalid[0] = m01_couplers_to_axi_interconnect_1_WVALID;
  assign M02_ACLK_1 = M02_ACLK;
  assign M02_ARESETN_1 = M02_ARESETN[0];
  assign M02_AXI_araddr[4:0] = m02_couplers_to_axi_interconnect_1_ARADDR;
  assign M02_AXI_arvalid[0] = m02_couplers_to_axi_interconnect_1_ARVALID;
  assign M02_AXI_awaddr[4:0] = m02_couplers_to_axi_interconnect_1_AWADDR;
  assign M02_AXI_awvalid[0] = m02_couplers_to_axi_interconnect_1_AWVALID;
  assign M02_AXI_bready[0] = m02_couplers_to_axi_interconnect_1_BREADY;
  assign M02_AXI_rready[0] = m02_couplers_to_axi_interconnect_1_RREADY;
  assign M02_AXI_wdata[31:0] = m02_couplers_to_axi_interconnect_1_WDATA;
  assign M02_AXI_wstrb[3:0] = m02_couplers_to_axi_interconnect_1_WSTRB;
  assign M02_AXI_wvalid[0] = m02_couplers_to_axi_interconnect_1_WVALID;
  assign M03_ACLK_1 = M03_ACLK;
  assign M03_ARESETN_1 = M03_ARESETN[0];
  assign M03_AXI_araddr[31:0] = m03_couplers_to_axi_interconnect_1_ARADDR;
  assign M03_AXI_arprot[2:0] = m03_couplers_to_axi_interconnect_1_ARPROT;
  assign M03_AXI_arvalid[0] = m03_couplers_to_axi_interconnect_1_ARVALID;
  assign M03_AXI_awaddr[31:0] = m03_couplers_to_axi_interconnect_1_AWADDR;
  assign M03_AXI_awprot[2:0] = m03_couplers_to_axi_interconnect_1_AWPROT;
  assign M03_AXI_awvalid[0] = m03_couplers_to_axi_interconnect_1_AWVALID;
  assign M03_AXI_bready[0] = m03_couplers_to_axi_interconnect_1_BREADY;
  assign M03_AXI_rready[0] = m03_couplers_to_axi_interconnect_1_RREADY;
  assign M03_AXI_wdata[31:0] = m03_couplers_to_axi_interconnect_1_WDATA;
  assign M03_AXI_wstrb[3:0] = m03_couplers_to_axi_interconnect_1_WSTRB;
  assign M03_AXI_wvalid[0] = m03_couplers_to_axi_interconnect_1_WVALID;
  assign M04_ACLK_1 = M04_ACLK;
  assign M04_ARESETN_1 = M04_ARESETN[0];
  assign M04_AXI_araddr[8:0] = m04_couplers_to_axi_interconnect_1_ARADDR;
  assign M04_AXI_arvalid[0] = m04_couplers_to_axi_interconnect_1_ARVALID;
  assign M04_AXI_awaddr[8:0] = m04_couplers_to_axi_interconnect_1_AWADDR;
  assign M04_AXI_awvalid[0] = m04_couplers_to_axi_interconnect_1_AWVALID;
  assign M04_AXI_bready[0] = m04_couplers_to_axi_interconnect_1_BREADY;
  assign M04_AXI_rready[0] = m04_couplers_to_axi_interconnect_1_RREADY;
  assign M04_AXI_wdata[31:0] = m04_couplers_to_axi_interconnect_1_WDATA;
  assign M04_AXI_wstrb[3:0] = m04_couplers_to_axi_interconnect_1_WSTRB;
  assign M04_AXI_wvalid[0] = m04_couplers_to_axi_interconnect_1_WVALID;
  assign S00_ACLK_1 = S00_ACLK;
  assign S00_ARESETN_1 = S00_ARESETN[0];
  assign S00_AXI_arready = axi_interconnect_1_to_s00_couplers_ARREADY;
  assign S00_AXI_awready = axi_interconnect_1_to_s00_couplers_AWREADY;
  assign S00_AXI_bid[11:0] = axi_interconnect_1_to_s00_couplers_BID;
  assign S00_AXI_bresp[1:0] = axi_interconnect_1_to_s00_couplers_BRESP;
  assign S00_AXI_bvalid = axi_interconnect_1_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = axi_interconnect_1_to_s00_couplers_RDATA;
  assign S00_AXI_rid[11:0] = axi_interconnect_1_to_s00_couplers_RID;
  assign S00_AXI_rlast = axi_interconnect_1_to_s00_couplers_RLAST;
  assign S00_AXI_rresp[1:0] = axi_interconnect_1_to_s00_couplers_RRESP;
  assign S00_AXI_rvalid = axi_interconnect_1_to_s00_couplers_RVALID;
  assign S00_AXI_wready = axi_interconnect_1_to_s00_couplers_WREADY;
  assign axi_interconnect_1_ACLK_net = ACLK;
  assign axi_interconnect_1_ARESETN_net = ARESETN[0];
  assign axi_interconnect_1_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign axi_interconnect_1_to_s00_couplers_ARBURST = S00_AXI_arburst[1:0];
  assign axi_interconnect_1_to_s00_couplers_ARCACHE = S00_AXI_arcache[3:0];
  assign axi_interconnect_1_to_s00_couplers_ARID = S00_AXI_arid[11:0];
  assign axi_interconnect_1_to_s00_couplers_ARLEN = S00_AXI_arlen[3:0];
  assign axi_interconnect_1_to_s00_couplers_ARLOCK = S00_AXI_arlock[1:0];
  assign axi_interconnect_1_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign axi_interconnect_1_to_s00_couplers_ARQOS = S00_AXI_arqos[3:0];
  assign axi_interconnect_1_to_s00_couplers_ARSIZE = S00_AXI_arsize[2:0];
  assign axi_interconnect_1_to_s00_couplers_ARVALID = S00_AXI_arvalid;
  assign axi_interconnect_1_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign axi_interconnect_1_to_s00_couplers_AWBURST = S00_AXI_awburst[1:0];
  assign axi_interconnect_1_to_s00_couplers_AWCACHE = S00_AXI_awcache[3:0];
  assign axi_interconnect_1_to_s00_couplers_AWID = S00_AXI_awid[11:0];
  assign axi_interconnect_1_to_s00_couplers_AWLEN = S00_AXI_awlen[3:0];
  assign axi_interconnect_1_to_s00_couplers_AWLOCK = S00_AXI_awlock[1:0];
  assign axi_interconnect_1_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign axi_interconnect_1_to_s00_couplers_AWQOS = S00_AXI_awqos[3:0];
  assign axi_interconnect_1_to_s00_couplers_AWSIZE = S00_AXI_awsize[2:0];
  assign axi_interconnect_1_to_s00_couplers_AWVALID = S00_AXI_awvalid;
  assign axi_interconnect_1_to_s00_couplers_BREADY = S00_AXI_bready;
  assign axi_interconnect_1_to_s00_couplers_RREADY = S00_AXI_rready;
  assign axi_interconnect_1_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign axi_interconnect_1_to_s00_couplers_WID = S00_AXI_wid[11:0];
  assign axi_interconnect_1_to_s00_couplers_WLAST = S00_AXI_wlast;
  assign axi_interconnect_1_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign axi_interconnect_1_to_s00_couplers_WVALID = S00_AXI_wvalid;
  assign m00_couplers_to_axi_interconnect_1_ARREADY = M00_AXI_arready[0];
  assign m00_couplers_to_axi_interconnect_1_AWREADY = M00_AXI_awready[0];
  assign m00_couplers_to_axi_interconnect_1_BRESP = M00_AXI_bresp[1:0];
  assign m00_couplers_to_axi_interconnect_1_BVALID = M00_AXI_bvalid[0];
  assign m00_couplers_to_axi_interconnect_1_RDATA = M00_AXI_rdata[31:0];
  assign m00_couplers_to_axi_interconnect_1_RRESP = M00_AXI_rresp[1:0];
  assign m00_couplers_to_axi_interconnect_1_RVALID = M00_AXI_rvalid[0];
  assign m00_couplers_to_axi_interconnect_1_WREADY = M00_AXI_wready[0];
  assign m01_couplers_to_axi_interconnect_1_ARREADY = M01_AXI_arready[0];
  assign m01_couplers_to_axi_interconnect_1_AWREADY = M01_AXI_awready[0];
  assign m01_couplers_to_axi_interconnect_1_BRESP = M01_AXI_bresp[1:0];
  assign m01_couplers_to_axi_interconnect_1_BVALID = M01_AXI_bvalid[0];
  assign m01_couplers_to_axi_interconnect_1_RDATA = M01_AXI_rdata[31:0];
  assign m01_couplers_to_axi_interconnect_1_RRESP = M01_AXI_rresp[1:0];
  assign m01_couplers_to_axi_interconnect_1_RVALID = M01_AXI_rvalid[0];
  assign m01_couplers_to_axi_interconnect_1_WREADY = M01_AXI_wready[0];
  assign m02_couplers_to_axi_interconnect_1_ARREADY = M02_AXI_arready[0];
  assign m02_couplers_to_axi_interconnect_1_AWREADY = M02_AXI_awready[0];
  assign m02_couplers_to_axi_interconnect_1_BRESP = M02_AXI_bresp[1:0];
  assign m02_couplers_to_axi_interconnect_1_BVALID = M02_AXI_bvalid[0];
  assign m02_couplers_to_axi_interconnect_1_RDATA = M02_AXI_rdata[31:0];
  assign m02_couplers_to_axi_interconnect_1_RRESP = M02_AXI_rresp[1:0];
  assign m02_couplers_to_axi_interconnect_1_RVALID = M02_AXI_rvalid[0];
  assign m02_couplers_to_axi_interconnect_1_WREADY = M02_AXI_wready[0];
  assign m03_couplers_to_axi_interconnect_1_ARREADY = M03_AXI_arready[0];
  assign m03_couplers_to_axi_interconnect_1_AWREADY = M03_AXI_awready[0];
  assign m03_couplers_to_axi_interconnect_1_BRESP = M03_AXI_bresp[1:0];
  assign m03_couplers_to_axi_interconnect_1_BVALID = M03_AXI_bvalid[0];
  assign m03_couplers_to_axi_interconnect_1_RDATA = M03_AXI_rdata[31:0];
  assign m03_couplers_to_axi_interconnect_1_RRESP = M03_AXI_rresp[1:0];
  assign m03_couplers_to_axi_interconnect_1_RVALID = M03_AXI_rvalid[0];
  assign m03_couplers_to_axi_interconnect_1_WREADY = M03_AXI_wready[0];
  assign m04_couplers_to_axi_interconnect_1_ARREADY = M04_AXI_arready[0];
  assign m04_couplers_to_axi_interconnect_1_AWREADY = M04_AXI_awready[0];
  assign m04_couplers_to_axi_interconnect_1_BRESP = M04_AXI_bresp[1:0];
  assign m04_couplers_to_axi_interconnect_1_BVALID = M04_AXI_bvalid[0];
  assign m04_couplers_to_axi_interconnect_1_RDATA = M04_AXI_rdata[31:0];
  assign m04_couplers_to_axi_interconnect_1_RRESP = M04_AXI_rresp[1:0];
  assign m04_couplers_to_axi_interconnect_1_RVALID = M04_AXI_rvalid[0];
  assign m04_couplers_to_axi_interconnect_1_WREADY = M04_AXI_wready[0];
m00_couplers_imp_1HQUX1D m00_couplers
       (.M_ACLK(M00_ACLK_1),
        .M_ARESETN(M00_ARESETN_1),
        .M_AXI_araddr(m00_couplers_to_axi_interconnect_1_ARADDR),
        .M_AXI_arready(m00_couplers_to_axi_interconnect_1_ARREADY),
        .M_AXI_arvalid(m00_couplers_to_axi_interconnect_1_ARVALID),
        .M_AXI_awaddr(m00_couplers_to_axi_interconnect_1_AWADDR),
        .M_AXI_awready(m00_couplers_to_axi_interconnect_1_AWREADY),
        .M_AXI_awvalid(m00_couplers_to_axi_interconnect_1_AWVALID),
        .M_AXI_bready(m00_couplers_to_axi_interconnect_1_BREADY),
        .M_AXI_bresp(m00_couplers_to_axi_interconnect_1_BRESP),
        .M_AXI_bvalid(m00_couplers_to_axi_interconnect_1_BVALID),
        .M_AXI_rdata(m00_couplers_to_axi_interconnect_1_RDATA),
        .M_AXI_rready(m00_couplers_to_axi_interconnect_1_RREADY),
        .M_AXI_rresp(m00_couplers_to_axi_interconnect_1_RRESP),
        .M_AXI_rvalid(m00_couplers_to_axi_interconnect_1_RVALID),
        .M_AXI_wdata(m00_couplers_to_axi_interconnect_1_WDATA),
        .M_AXI_wready(m00_couplers_to_axi_interconnect_1_WREADY),
        .M_AXI_wstrb(m00_couplers_to_axi_interconnect_1_WSTRB),
        .M_AXI_wvalid(m00_couplers_to_axi_interconnect_1_WVALID),
        .S_ACLK(axi_interconnect_1_ACLK_net),
        .S_ARESETN(axi_interconnect_1_ARESETN_net),
        .S_AXI_araddr(xbar_to_m00_couplers_ARADDR[8:0]),
        .S_AXI_arready(xbar_to_m00_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m00_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m00_couplers_AWADDR[8:0]),
        .S_AXI_awready(xbar_to_m00_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m00_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m00_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m00_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m00_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m00_couplers_RDATA),
        .S_AXI_rready(xbar_to_m00_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m00_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m00_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m00_couplers_WDATA),
        .S_AXI_wready(xbar_to_m00_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m00_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m00_couplers_WVALID));
m01_couplers_imp_CW3EGV m01_couplers
       (.M_ACLK(M01_ACLK_1),
        .M_ARESETN(M01_ARESETN_1),
        .M_AXI_araddr(m01_couplers_to_axi_interconnect_1_ARADDR),
        .M_AXI_arready(m01_couplers_to_axi_interconnect_1_ARREADY),
        .M_AXI_arvalid(m01_couplers_to_axi_interconnect_1_ARVALID),
        .M_AXI_awaddr(m01_couplers_to_axi_interconnect_1_AWADDR),
        .M_AXI_awready(m01_couplers_to_axi_interconnect_1_AWREADY),
        .M_AXI_awvalid(m01_couplers_to_axi_interconnect_1_AWVALID),
        .M_AXI_bready(m01_couplers_to_axi_interconnect_1_BREADY),
        .M_AXI_bresp(m01_couplers_to_axi_interconnect_1_BRESP),
        .M_AXI_bvalid(m01_couplers_to_axi_interconnect_1_BVALID),
        .M_AXI_rdata(m01_couplers_to_axi_interconnect_1_RDATA),
        .M_AXI_rready(m01_couplers_to_axi_interconnect_1_RREADY),
        .M_AXI_rresp(m01_couplers_to_axi_interconnect_1_RRESP),
        .M_AXI_rvalid(m01_couplers_to_axi_interconnect_1_RVALID),
        .M_AXI_wdata(m01_couplers_to_axi_interconnect_1_WDATA),
        .M_AXI_wready(m01_couplers_to_axi_interconnect_1_WREADY),
        .M_AXI_wvalid(m01_couplers_to_axi_interconnect_1_WVALID),
        .S_ACLK(axi_interconnect_1_ACLK_net),
        .S_ARESETN(axi_interconnect_1_ARESETN_net),
        .S_AXI_araddr(xbar_to_m01_couplers_ARADDR),
        .S_AXI_arready(xbar_to_m01_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m01_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m01_couplers_AWADDR),
        .S_AXI_awready(xbar_to_m01_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m01_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m01_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m01_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m01_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m01_couplers_RDATA),
        .S_AXI_rready(xbar_to_m01_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m01_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m01_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m01_couplers_WDATA),
        .S_AXI_wready(xbar_to_m01_couplers_WREADY),
        .S_AXI_wvalid(xbar_to_m01_couplers_WVALID));
m02_couplers_imp_1IQC0SC m02_couplers
       (.M_ACLK(M02_ACLK_1),
        .M_ARESETN(M02_ARESETN_1),
        .M_AXI_araddr(m02_couplers_to_axi_interconnect_1_ARADDR),
        .M_AXI_arready(m02_couplers_to_axi_interconnect_1_ARREADY),
        .M_AXI_arvalid(m02_couplers_to_axi_interconnect_1_ARVALID),
        .M_AXI_awaddr(m02_couplers_to_axi_interconnect_1_AWADDR),
        .M_AXI_awready(m02_couplers_to_axi_interconnect_1_AWREADY),
        .M_AXI_awvalid(m02_couplers_to_axi_interconnect_1_AWVALID),
        .M_AXI_bready(m02_couplers_to_axi_interconnect_1_BREADY),
        .M_AXI_bresp(m02_couplers_to_axi_interconnect_1_BRESP),
        .M_AXI_bvalid(m02_couplers_to_axi_interconnect_1_BVALID),
        .M_AXI_rdata(m02_couplers_to_axi_interconnect_1_RDATA),
        .M_AXI_rready(m02_couplers_to_axi_interconnect_1_RREADY),
        .M_AXI_rresp(m02_couplers_to_axi_interconnect_1_RRESP),
        .M_AXI_rvalid(m02_couplers_to_axi_interconnect_1_RVALID),
        .M_AXI_wdata(m02_couplers_to_axi_interconnect_1_WDATA),
        .M_AXI_wready(m02_couplers_to_axi_interconnect_1_WREADY),
        .M_AXI_wstrb(m02_couplers_to_axi_interconnect_1_WSTRB),
        .M_AXI_wvalid(m02_couplers_to_axi_interconnect_1_WVALID),
        .S_ACLK(axi_interconnect_1_ACLK_net),
        .S_ARESETN(axi_interconnect_1_ARESETN_net),
        .S_AXI_araddr(xbar_to_m02_couplers_ARADDR[68:64]),
        .S_AXI_arready(xbar_to_m02_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m02_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m02_couplers_AWADDR[68:64]),
        .S_AXI_awready(xbar_to_m02_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m02_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m02_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m02_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m02_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m02_couplers_RDATA),
        .S_AXI_rready(xbar_to_m02_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m02_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m02_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m02_couplers_WDATA),
        .S_AXI_wready(xbar_to_m02_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m02_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m02_couplers_WVALID));
m03_couplers_imp_BSO91E m03_couplers
       (.M_ACLK(M03_ACLK_1),
        .M_ARESETN(M03_ARESETN_1),
        .M_AXI_araddr(m03_couplers_to_axi_interconnect_1_ARADDR),
        .M_AXI_arprot(m03_couplers_to_axi_interconnect_1_ARPROT),
        .M_AXI_arready(m03_couplers_to_axi_interconnect_1_ARREADY),
        .M_AXI_arvalid(m03_couplers_to_axi_interconnect_1_ARVALID),
        .M_AXI_awaddr(m03_couplers_to_axi_interconnect_1_AWADDR),
        .M_AXI_awprot(m03_couplers_to_axi_interconnect_1_AWPROT),
        .M_AXI_awready(m03_couplers_to_axi_interconnect_1_AWREADY),
        .M_AXI_awvalid(m03_couplers_to_axi_interconnect_1_AWVALID),
        .M_AXI_bready(m03_couplers_to_axi_interconnect_1_BREADY),
        .M_AXI_bresp(m03_couplers_to_axi_interconnect_1_BRESP),
        .M_AXI_bvalid(m03_couplers_to_axi_interconnect_1_BVALID),
        .M_AXI_rdata(m03_couplers_to_axi_interconnect_1_RDATA),
        .M_AXI_rready(m03_couplers_to_axi_interconnect_1_RREADY),
        .M_AXI_rresp(m03_couplers_to_axi_interconnect_1_RRESP),
        .M_AXI_rvalid(m03_couplers_to_axi_interconnect_1_RVALID),
        .M_AXI_wdata(m03_couplers_to_axi_interconnect_1_WDATA),
        .M_AXI_wready(m03_couplers_to_axi_interconnect_1_WREADY),
        .M_AXI_wstrb(m03_couplers_to_axi_interconnect_1_WSTRB),
        .M_AXI_wvalid(m03_couplers_to_axi_interconnect_1_WVALID),
        .S_ACLK(axi_interconnect_1_ACLK_net),
        .S_ARESETN(axi_interconnect_1_ARESETN_net),
        .S_AXI_araddr(xbar_to_m03_couplers_ARADDR),
        .S_AXI_arprot(xbar_to_m03_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m03_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m03_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m03_couplers_AWADDR),
        .S_AXI_awprot(xbar_to_m03_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m03_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m03_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m03_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m03_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m03_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m03_couplers_RDATA),
        .S_AXI_rready(xbar_to_m03_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m03_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m03_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m03_couplers_WDATA),
        .S_AXI_wready(xbar_to_m03_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m03_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m03_couplers_WVALID));
m04_couplers_imp_1JLHK1N m04_couplers
       (.M_ACLK(M04_ACLK_1),
        .M_ARESETN(M04_ARESETN_1),
        .M_AXI_araddr(m04_couplers_to_axi_interconnect_1_ARADDR),
        .M_AXI_arready(m04_couplers_to_axi_interconnect_1_ARREADY),
        .M_AXI_arvalid(m04_couplers_to_axi_interconnect_1_ARVALID),
        .M_AXI_awaddr(m04_couplers_to_axi_interconnect_1_AWADDR),
        .M_AXI_awready(m04_couplers_to_axi_interconnect_1_AWREADY),
        .M_AXI_awvalid(m04_couplers_to_axi_interconnect_1_AWVALID),
        .M_AXI_bready(m04_couplers_to_axi_interconnect_1_BREADY),
        .M_AXI_bresp(m04_couplers_to_axi_interconnect_1_BRESP),
        .M_AXI_bvalid(m04_couplers_to_axi_interconnect_1_BVALID),
        .M_AXI_rdata(m04_couplers_to_axi_interconnect_1_RDATA),
        .M_AXI_rready(m04_couplers_to_axi_interconnect_1_RREADY),
        .M_AXI_rresp(m04_couplers_to_axi_interconnect_1_RRESP),
        .M_AXI_rvalid(m04_couplers_to_axi_interconnect_1_RVALID),
        .M_AXI_wdata(m04_couplers_to_axi_interconnect_1_WDATA),
        .M_AXI_wready(m04_couplers_to_axi_interconnect_1_WREADY),
        .M_AXI_wstrb(m04_couplers_to_axi_interconnect_1_WSTRB),
        .M_AXI_wvalid(m04_couplers_to_axi_interconnect_1_WVALID),
        .S_ACLK(axi_interconnect_1_ACLK_net),
        .S_ARESETN(axi_interconnect_1_ARESETN_net),
        .S_AXI_araddr(xbar_to_m04_couplers_ARADDR[136:128]),
        .S_AXI_arready(xbar_to_m04_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m04_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m04_couplers_AWADDR[136:128]),
        .S_AXI_awready(xbar_to_m04_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m04_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m04_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m04_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m04_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m04_couplers_RDATA),
        .S_AXI_rready(xbar_to_m04_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m04_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m04_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m04_couplers_WDATA),
        .S_AXI_wready(xbar_to_m04_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m04_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m04_couplers_WVALID));
s00_couplers_imp_A5QIOZ s00_couplers
       (.M_ACLK(axi_interconnect_1_ACLK_net),
        .M_ARESETN(axi_interconnect_1_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_xbar_ARADDR),
        .M_AXI_arprot(s00_couplers_to_xbar_ARPROT),
        .M_AXI_arready(s00_couplers_to_xbar_ARREADY),
        .M_AXI_arvalid(s00_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_xbar_AWADDR),
        .M_AXI_awprot(s00_couplers_to_xbar_AWPROT),
        .M_AXI_awready(s00_couplers_to_xbar_AWREADY),
        .M_AXI_awvalid(s00_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s00_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s00_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s00_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s00_couplers_to_xbar_RDATA),
        .M_AXI_rready(s00_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s00_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s00_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s00_couplers_to_xbar_WDATA),
        .M_AXI_wready(s00_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s00_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s00_couplers_to_xbar_WVALID),
        .S_ACLK(S00_ACLK_1),
        .S_ARESETN(S00_ARESETN_1),
        .S_AXI_araddr(axi_interconnect_1_to_s00_couplers_ARADDR),
        .S_AXI_arburst(axi_interconnect_1_to_s00_couplers_ARBURST),
        .S_AXI_arcache(axi_interconnect_1_to_s00_couplers_ARCACHE),
        .S_AXI_arid(axi_interconnect_1_to_s00_couplers_ARID),
        .S_AXI_arlen(axi_interconnect_1_to_s00_couplers_ARLEN),
        .S_AXI_arlock(axi_interconnect_1_to_s00_couplers_ARLOCK),
        .S_AXI_arprot(axi_interconnect_1_to_s00_couplers_ARPROT),
        .S_AXI_arqos(axi_interconnect_1_to_s00_couplers_ARQOS),
        .S_AXI_arready(axi_interconnect_1_to_s00_couplers_ARREADY),
        .S_AXI_arsize(axi_interconnect_1_to_s00_couplers_ARSIZE),
        .S_AXI_arvalid(axi_interconnect_1_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(axi_interconnect_1_to_s00_couplers_AWADDR),
        .S_AXI_awburst(axi_interconnect_1_to_s00_couplers_AWBURST),
        .S_AXI_awcache(axi_interconnect_1_to_s00_couplers_AWCACHE),
        .S_AXI_awid(axi_interconnect_1_to_s00_couplers_AWID),
        .S_AXI_awlen(axi_interconnect_1_to_s00_couplers_AWLEN),
        .S_AXI_awlock(axi_interconnect_1_to_s00_couplers_AWLOCK),
        .S_AXI_awprot(axi_interconnect_1_to_s00_couplers_AWPROT),
        .S_AXI_awqos(axi_interconnect_1_to_s00_couplers_AWQOS),
        .S_AXI_awready(axi_interconnect_1_to_s00_couplers_AWREADY),
        .S_AXI_awsize(axi_interconnect_1_to_s00_couplers_AWSIZE),
        .S_AXI_awvalid(axi_interconnect_1_to_s00_couplers_AWVALID),
        .S_AXI_bid(axi_interconnect_1_to_s00_couplers_BID),
        .S_AXI_bready(axi_interconnect_1_to_s00_couplers_BREADY),
        .S_AXI_bresp(axi_interconnect_1_to_s00_couplers_BRESP),
        .S_AXI_bvalid(axi_interconnect_1_to_s00_couplers_BVALID),
        .S_AXI_rdata(axi_interconnect_1_to_s00_couplers_RDATA),
        .S_AXI_rid(axi_interconnect_1_to_s00_couplers_RID),
        .S_AXI_rlast(axi_interconnect_1_to_s00_couplers_RLAST),
        .S_AXI_rready(axi_interconnect_1_to_s00_couplers_RREADY),
        .S_AXI_rresp(axi_interconnect_1_to_s00_couplers_RRESP),
        .S_AXI_rvalid(axi_interconnect_1_to_s00_couplers_RVALID),
        .S_AXI_wdata(axi_interconnect_1_to_s00_couplers_WDATA),
        .S_AXI_wid(axi_interconnect_1_to_s00_couplers_WID),
        .S_AXI_wlast(axi_interconnect_1_to_s00_couplers_WLAST),
        .S_AXI_wready(axi_interconnect_1_to_s00_couplers_WREADY),
        .S_AXI_wstrb(axi_interconnect_1_to_s00_couplers_WSTRB),
        .S_AXI_wvalid(axi_interconnect_1_to_s00_couplers_WVALID));
dp_system_xbar_2 xbar
       (.aclk(axi_interconnect_1_ACLK_net),
        .aresetn(axi_interconnect_1_ARESETN_net),
        .m_axi_araddr({xbar_to_m04_couplers_ARADDR,xbar_to_m03_couplers_ARADDR,xbar_to_m02_couplers_ARADDR,xbar_to_m01_couplers_ARADDR,xbar_to_m00_couplers_ARADDR}),
        .m_axi_arprot({xbar_to_m03_couplers_ARPROT,NLW_xbar_m_axi_arprot_UNCONNECTED[8:0]}),
        .m_axi_arready({xbar_to_m04_couplers_ARREADY,xbar_to_m03_couplers_ARREADY,xbar_to_m02_couplers_ARREADY,xbar_to_m01_couplers_ARREADY,xbar_to_m00_couplers_ARREADY}),
        .m_axi_arvalid({xbar_to_m04_couplers_ARVALID,xbar_to_m03_couplers_ARVALID,xbar_to_m02_couplers_ARVALID,xbar_to_m01_couplers_ARVALID,xbar_to_m00_couplers_ARVALID}),
        .m_axi_awaddr({xbar_to_m04_couplers_AWADDR,xbar_to_m03_couplers_AWADDR,xbar_to_m02_couplers_AWADDR,xbar_to_m01_couplers_AWADDR,xbar_to_m00_couplers_AWADDR}),
        .m_axi_awprot({xbar_to_m03_couplers_AWPROT,NLW_xbar_m_axi_awprot_UNCONNECTED[8:0]}),
        .m_axi_awready({xbar_to_m04_couplers_AWREADY,xbar_to_m03_couplers_AWREADY,xbar_to_m02_couplers_AWREADY,xbar_to_m01_couplers_AWREADY,xbar_to_m00_couplers_AWREADY}),
        .m_axi_awvalid({xbar_to_m04_couplers_AWVALID,xbar_to_m03_couplers_AWVALID,xbar_to_m02_couplers_AWVALID,xbar_to_m01_couplers_AWVALID,xbar_to_m00_couplers_AWVALID}),
        .m_axi_bready({xbar_to_m04_couplers_BREADY,xbar_to_m03_couplers_BREADY,xbar_to_m02_couplers_BREADY,xbar_to_m01_couplers_BREADY,xbar_to_m00_couplers_BREADY}),
        .m_axi_bresp({xbar_to_m04_couplers_BRESP,xbar_to_m03_couplers_BRESP,xbar_to_m02_couplers_BRESP,xbar_to_m01_couplers_BRESP,xbar_to_m00_couplers_BRESP}),
        .m_axi_bvalid({xbar_to_m04_couplers_BVALID,xbar_to_m03_couplers_BVALID,xbar_to_m02_couplers_BVALID,xbar_to_m01_couplers_BVALID,xbar_to_m00_couplers_BVALID}),
        .m_axi_rdata({xbar_to_m04_couplers_RDATA,xbar_to_m03_couplers_RDATA,xbar_to_m02_couplers_RDATA,xbar_to_m01_couplers_RDATA,xbar_to_m00_couplers_RDATA}),
        .m_axi_rready({xbar_to_m04_couplers_RREADY,xbar_to_m03_couplers_RREADY,xbar_to_m02_couplers_RREADY,xbar_to_m01_couplers_RREADY,xbar_to_m00_couplers_RREADY}),
        .m_axi_rresp({xbar_to_m04_couplers_RRESP,xbar_to_m03_couplers_RRESP,xbar_to_m02_couplers_RRESP,xbar_to_m01_couplers_RRESP,xbar_to_m00_couplers_RRESP}),
        .m_axi_rvalid({xbar_to_m04_couplers_RVALID,xbar_to_m03_couplers_RVALID,xbar_to_m02_couplers_RVALID,xbar_to_m01_couplers_RVALID,xbar_to_m00_couplers_RVALID}),
        .m_axi_wdata({xbar_to_m04_couplers_WDATA,xbar_to_m03_couplers_WDATA,xbar_to_m02_couplers_WDATA,xbar_to_m01_couplers_WDATA,xbar_to_m00_couplers_WDATA}),
        .m_axi_wready({xbar_to_m04_couplers_WREADY,xbar_to_m03_couplers_WREADY,xbar_to_m02_couplers_WREADY,xbar_to_m01_couplers_WREADY,xbar_to_m00_couplers_WREADY}),
        .m_axi_wstrb({xbar_to_m04_couplers_WSTRB,xbar_to_m03_couplers_WSTRB,xbar_to_m02_couplers_WSTRB,NLW_xbar_m_axi_wstrb_UNCONNECTED[7:4],xbar_to_m00_couplers_WSTRB}),
        .m_axi_wvalid({xbar_to_m04_couplers_WVALID,xbar_to_m03_couplers_WVALID,xbar_to_m02_couplers_WVALID,xbar_to_m01_couplers_WVALID,xbar_to_m00_couplers_WVALID}),
        .s_axi_araddr(s00_couplers_to_xbar_ARADDR),
        .s_axi_arprot(s00_couplers_to_xbar_ARPROT),
        .s_axi_arready(s00_couplers_to_xbar_ARREADY),
        .s_axi_arvalid(s00_couplers_to_xbar_ARVALID),
        .s_axi_awaddr(s00_couplers_to_xbar_AWADDR),
        .s_axi_awprot(s00_couplers_to_xbar_AWPROT),
        .s_axi_awready(s00_couplers_to_xbar_AWREADY),
        .s_axi_awvalid(s00_couplers_to_xbar_AWVALID),
        .s_axi_bready(s00_couplers_to_xbar_BREADY),
        .s_axi_bresp(s00_couplers_to_xbar_BRESP),
        .s_axi_bvalid(s00_couplers_to_xbar_BVALID),
        .s_axi_rdata(s00_couplers_to_xbar_RDATA),
        .s_axi_rready(s00_couplers_to_xbar_RREADY),
        .s_axi_rresp(s00_couplers_to_xbar_RRESP),
        .s_axi_rvalid(s00_couplers_to_xbar_RVALID),
        .s_axi_wdata(s00_couplers_to_xbar_WDATA),
        .s_axi_wready(s00_couplers_to_xbar_WREADY),
        .s_axi_wstrb(s00_couplers_to_xbar_WSTRB),
        .s_axi_wvalid(s00_couplers_to_xbar_WVALID));
endmodule

module m00_couplers_imp_1HQUX1D
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [8:0]M_AXI_araddr;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [8:0]M_AXI_awaddr;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [8:0]S_AXI_araddr;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [8:0]S_AXI_awaddr;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [8:0]m00_couplers_to_m00_couplers_ARADDR;
  wire [0:0]m00_couplers_to_m00_couplers_ARREADY;
  wire [0:0]m00_couplers_to_m00_couplers_ARVALID;
  wire [8:0]m00_couplers_to_m00_couplers_AWADDR;
  wire [0:0]m00_couplers_to_m00_couplers_AWREADY;
  wire [0:0]m00_couplers_to_m00_couplers_AWVALID;
  wire [0:0]m00_couplers_to_m00_couplers_BREADY;
  wire [1:0]m00_couplers_to_m00_couplers_BRESP;
  wire [0:0]m00_couplers_to_m00_couplers_BVALID;
  wire [31:0]m00_couplers_to_m00_couplers_RDATA;
  wire [0:0]m00_couplers_to_m00_couplers_RREADY;
  wire [1:0]m00_couplers_to_m00_couplers_RRESP;
  wire [0:0]m00_couplers_to_m00_couplers_RVALID;
  wire [31:0]m00_couplers_to_m00_couplers_WDATA;
  wire [0:0]m00_couplers_to_m00_couplers_WREADY;
  wire [3:0]m00_couplers_to_m00_couplers_WSTRB;
  wire [0:0]m00_couplers_to_m00_couplers_WVALID;

  assign M_AXI_araddr[8:0] = m00_couplers_to_m00_couplers_ARADDR;
  assign M_AXI_arvalid[0] = m00_couplers_to_m00_couplers_ARVALID;
  assign M_AXI_awaddr[8:0] = m00_couplers_to_m00_couplers_AWADDR;
  assign M_AXI_awvalid[0] = m00_couplers_to_m00_couplers_AWVALID;
  assign M_AXI_bready[0] = m00_couplers_to_m00_couplers_BREADY;
  assign M_AXI_rready[0] = m00_couplers_to_m00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m00_couplers_to_m00_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m00_couplers_to_m00_couplers_WSTRB;
  assign M_AXI_wvalid[0] = m00_couplers_to_m00_couplers_WVALID;
  assign S_AXI_arready[0] = m00_couplers_to_m00_couplers_ARREADY;
  assign S_AXI_awready[0] = m00_couplers_to_m00_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m00_couplers_to_m00_couplers_BRESP;
  assign S_AXI_bvalid[0] = m00_couplers_to_m00_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m00_couplers_to_m00_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m00_couplers_to_m00_couplers_RRESP;
  assign S_AXI_rvalid[0] = m00_couplers_to_m00_couplers_RVALID;
  assign S_AXI_wready[0] = m00_couplers_to_m00_couplers_WREADY;
  assign m00_couplers_to_m00_couplers_ARADDR = S_AXI_araddr[8:0];
  assign m00_couplers_to_m00_couplers_ARREADY = M_AXI_arready[0];
  assign m00_couplers_to_m00_couplers_ARVALID = S_AXI_arvalid[0];
  assign m00_couplers_to_m00_couplers_AWADDR = S_AXI_awaddr[8:0];
  assign m00_couplers_to_m00_couplers_AWREADY = M_AXI_awready[0];
  assign m00_couplers_to_m00_couplers_AWVALID = S_AXI_awvalid[0];
  assign m00_couplers_to_m00_couplers_BREADY = S_AXI_bready[0];
  assign m00_couplers_to_m00_couplers_BRESP = M_AXI_bresp[1:0];
  assign m00_couplers_to_m00_couplers_BVALID = M_AXI_bvalid[0];
  assign m00_couplers_to_m00_couplers_RDATA = M_AXI_rdata[31:0];
  assign m00_couplers_to_m00_couplers_RREADY = S_AXI_rready[0];
  assign m00_couplers_to_m00_couplers_RRESP = M_AXI_rresp[1:0];
  assign m00_couplers_to_m00_couplers_RVALID = M_AXI_rvalid[0];
  assign m00_couplers_to_m00_couplers_WDATA = S_AXI_wdata[31:0];
  assign m00_couplers_to_m00_couplers_WREADY = M_AXI_wready[0];
  assign m00_couplers_to_m00_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m00_couplers_to_m00_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module m01_couplers_imp_CW3EGV
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [0:0]S_AXI_wvalid;

  wire [31:0]m01_couplers_to_m01_couplers_ARADDR;
  wire [0:0]m01_couplers_to_m01_couplers_ARREADY;
  wire [0:0]m01_couplers_to_m01_couplers_ARVALID;
  wire [31:0]m01_couplers_to_m01_couplers_AWADDR;
  wire [0:0]m01_couplers_to_m01_couplers_AWREADY;
  wire [0:0]m01_couplers_to_m01_couplers_AWVALID;
  wire [0:0]m01_couplers_to_m01_couplers_BREADY;
  wire [1:0]m01_couplers_to_m01_couplers_BRESP;
  wire [0:0]m01_couplers_to_m01_couplers_BVALID;
  wire [31:0]m01_couplers_to_m01_couplers_RDATA;
  wire [0:0]m01_couplers_to_m01_couplers_RREADY;
  wire [1:0]m01_couplers_to_m01_couplers_RRESP;
  wire [0:0]m01_couplers_to_m01_couplers_RVALID;
  wire [31:0]m01_couplers_to_m01_couplers_WDATA;
  wire [0:0]m01_couplers_to_m01_couplers_WREADY;
  wire [0:0]m01_couplers_to_m01_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m01_couplers_to_m01_couplers_ARADDR;
  assign M_AXI_arvalid[0] = m01_couplers_to_m01_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m01_couplers_to_m01_couplers_AWADDR;
  assign M_AXI_awvalid[0] = m01_couplers_to_m01_couplers_AWVALID;
  assign M_AXI_bready[0] = m01_couplers_to_m01_couplers_BREADY;
  assign M_AXI_rready[0] = m01_couplers_to_m01_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m01_couplers_to_m01_couplers_WDATA;
  assign M_AXI_wvalid[0] = m01_couplers_to_m01_couplers_WVALID;
  assign S_AXI_arready[0] = m01_couplers_to_m01_couplers_ARREADY;
  assign S_AXI_awready[0] = m01_couplers_to_m01_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m01_couplers_to_m01_couplers_BRESP;
  assign S_AXI_bvalid[0] = m01_couplers_to_m01_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m01_couplers_to_m01_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m01_couplers_to_m01_couplers_RRESP;
  assign S_AXI_rvalid[0] = m01_couplers_to_m01_couplers_RVALID;
  assign S_AXI_wready[0] = m01_couplers_to_m01_couplers_WREADY;
  assign m01_couplers_to_m01_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m01_couplers_to_m01_couplers_ARREADY = M_AXI_arready[0];
  assign m01_couplers_to_m01_couplers_ARVALID = S_AXI_arvalid[0];
  assign m01_couplers_to_m01_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m01_couplers_to_m01_couplers_AWREADY = M_AXI_awready[0];
  assign m01_couplers_to_m01_couplers_AWVALID = S_AXI_awvalid[0];
  assign m01_couplers_to_m01_couplers_BREADY = S_AXI_bready[0];
  assign m01_couplers_to_m01_couplers_BRESP = M_AXI_bresp[1:0];
  assign m01_couplers_to_m01_couplers_BVALID = M_AXI_bvalid[0];
  assign m01_couplers_to_m01_couplers_RDATA = M_AXI_rdata[31:0];
  assign m01_couplers_to_m01_couplers_RREADY = S_AXI_rready[0];
  assign m01_couplers_to_m01_couplers_RRESP = M_AXI_rresp[1:0];
  assign m01_couplers_to_m01_couplers_RVALID = M_AXI_rvalid[0];
  assign m01_couplers_to_m01_couplers_WDATA = S_AXI_wdata[31:0];
  assign m01_couplers_to_m01_couplers_WREADY = M_AXI_wready[0];
  assign m01_couplers_to_m01_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module m02_couplers_imp_1IQC0SC
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [4:0]M_AXI_araddr;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [4:0]M_AXI_awaddr;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [4:0]S_AXI_araddr;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [4:0]S_AXI_awaddr;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [4:0]m02_couplers_to_m02_couplers_ARADDR;
  wire [0:0]m02_couplers_to_m02_couplers_ARREADY;
  wire [0:0]m02_couplers_to_m02_couplers_ARVALID;
  wire [4:0]m02_couplers_to_m02_couplers_AWADDR;
  wire [0:0]m02_couplers_to_m02_couplers_AWREADY;
  wire [0:0]m02_couplers_to_m02_couplers_AWVALID;
  wire [0:0]m02_couplers_to_m02_couplers_BREADY;
  wire [1:0]m02_couplers_to_m02_couplers_BRESP;
  wire [0:0]m02_couplers_to_m02_couplers_BVALID;
  wire [31:0]m02_couplers_to_m02_couplers_RDATA;
  wire [0:0]m02_couplers_to_m02_couplers_RREADY;
  wire [1:0]m02_couplers_to_m02_couplers_RRESP;
  wire [0:0]m02_couplers_to_m02_couplers_RVALID;
  wire [31:0]m02_couplers_to_m02_couplers_WDATA;
  wire [0:0]m02_couplers_to_m02_couplers_WREADY;
  wire [3:0]m02_couplers_to_m02_couplers_WSTRB;
  wire [0:0]m02_couplers_to_m02_couplers_WVALID;

  assign M_AXI_araddr[4:0] = m02_couplers_to_m02_couplers_ARADDR;
  assign M_AXI_arvalid[0] = m02_couplers_to_m02_couplers_ARVALID;
  assign M_AXI_awaddr[4:0] = m02_couplers_to_m02_couplers_AWADDR;
  assign M_AXI_awvalid[0] = m02_couplers_to_m02_couplers_AWVALID;
  assign M_AXI_bready[0] = m02_couplers_to_m02_couplers_BREADY;
  assign M_AXI_rready[0] = m02_couplers_to_m02_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m02_couplers_to_m02_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m02_couplers_to_m02_couplers_WSTRB;
  assign M_AXI_wvalid[0] = m02_couplers_to_m02_couplers_WVALID;
  assign S_AXI_arready[0] = m02_couplers_to_m02_couplers_ARREADY;
  assign S_AXI_awready[0] = m02_couplers_to_m02_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m02_couplers_to_m02_couplers_BRESP;
  assign S_AXI_bvalid[0] = m02_couplers_to_m02_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m02_couplers_to_m02_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m02_couplers_to_m02_couplers_RRESP;
  assign S_AXI_rvalid[0] = m02_couplers_to_m02_couplers_RVALID;
  assign S_AXI_wready[0] = m02_couplers_to_m02_couplers_WREADY;
  assign m02_couplers_to_m02_couplers_ARADDR = S_AXI_araddr[4:0];
  assign m02_couplers_to_m02_couplers_ARREADY = M_AXI_arready[0];
  assign m02_couplers_to_m02_couplers_ARVALID = S_AXI_arvalid[0];
  assign m02_couplers_to_m02_couplers_AWADDR = S_AXI_awaddr[4:0];
  assign m02_couplers_to_m02_couplers_AWREADY = M_AXI_awready[0];
  assign m02_couplers_to_m02_couplers_AWVALID = S_AXI_awvalid[0];
  assign m02_couplers_to_m02_couplers_BREADY = S_AXI_bready[0];
  assign m02_couplers_to_m02_couplers_BRESP = M_AXI_bresp[1:0];
  assign m02_couplers_to_m02_couplers_BVALID = M_AXI_bvalid[0];
  assign m02_couplers_to_m02_couplers_RDATA = M_AXI_rdata[31:0];
  assign m02_couplers_to_m02_couplers_RREADY = S_AXI_rready[0];
  assign m02_couplers_to_m02_couplers_RRESP = M_AXI_rresp[1:0];
  assign m02_couplers_to_m02_couplers_RVALID = M_AXI_rvalid[0];
  assign m02_couplers_to_m02_couplers_WDATA = S_AXI_wdata[31:0];
  assign m02_couplers_to_m02_couplers_WREADY = M_AXI_wready[0];
  assign m02_couplers_to_m02_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m02_couplers_to_m02_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module m03_couplers_imp_BSO91E
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [31:0]m03_couplers_to_m03_couplers_ARADDR;
  wire [2:0]m03_couplers_to_m03_couplers_ARPROT;
  wire [0:0]m03_couplers_to_m03_couplers_ARREADY;
  wire [0:0]m03_couplers_to_m03_couplers_ARVALID;
  wire [31:0]m03_couplers_to_m03_couplers_AWADDR;
  wire [2:0]m03_couplers_to_m03_couplers_AWPROT;
  wire [0:0]m03_couplers_to_m03_couplers_AWREADY;
  wire [0:0]m03_couplers_to_m03_couplers_AWVALID;
  wire [0:0]m03_couplers_to_m03_couplers_BREADY;
  wire [1:0]m03_couplers_to_m03_couplers_BRESP;
  wire [0:0]m03_couplers_to_m03_couplers_BVALID;
  wire [31:0]m03_couplers_to_m03_couplers_RDATA;
  wire [0:0]m03_couplers_to_m03_couplers_RREADY;
  wire [1:0]m03_couplers_to_m03_couplers_RRESP;
  wire [0:0]m03_couplers_to_m03_couplers_RVALID;
  wire [31:0]m03_couplers_to_m03_couplers_WDATA;
  wire [0:0]m03_couplers_to_m03_couplers_WREADY;
  wire [3:0]m03_couplers_to_m03_couplers_WSTRB;
  wire [0:0]m03_couplers_to_m03_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m03_couplers_to_m03_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = m03_couplers_to_m03_couplers_ARPROT;
  assign M_AXI_arvalid[0] = m03_couplers_to_m03_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m03_couplers_to_m03_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = m03_couplers_to_m03_couplers_AWPROT;
  assign M_AXI_awvalid[0] = m03_couplers_to_m03_couplers_AWVALID;
  assign M_AXI_bready[0] = m03_couplers_to_m03_couplers_BREADY;
  assign M_AXI_rready[0] = m03_couplers_to_m03_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m03_couplers_to_m03_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m03_couplers_to_m03_couplers_WSTRB;
  assign M_AXI_wvalid[0] = m03_couplers_to_m03_couplers_WVALID;
  assign S_AXI_arready[0] = m03_couplers_to_m03_couplers_ARREADY;
  assign S_AXI_awready[0] = m03_couplers_to_m03_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m03_couplers_to_m03_couplers_BRESP;
  assign S_AXI_bvalid[0] = m03_couplers_to_m03_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m03_couplers_to_m03_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m03_couplers_to_m03_couplers_RRESP;
  assign S_AXI_rvalid[0] = m03_couplers_to_m03_couplers_RVALID;
  assign S_AXI_wready[0] = m03_couplers_to_m03_couplers_WREADY;
  assign m03_couplers_to_m03_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m03_couplers_to_m03_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m03_couplers_to_m03_couplers_ARREADY = M_AXI_arready[0];
  assign m03_couplers_to_m03_couplers_ARVALID = S_AXI_arvalid[0];
  assign m03_couplers_to_m03_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m03_couplers_to_m03_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m03_couplers_to_m03_couplers_AWREADY = M_AXI_awready[0];
  assign m03_couplers_to_m03_couplers_AWVALID = S_AXI_awvalid[0];
  assign m03_couplers_to_m03_couplers_BREADY = S_AXI_bready[0];
  assign m03_couplers_to_m03_couplers_BRESP = M_AXI_bresp[1:0];
  assign m03_couplers_to_m03_couplers_BVALID = M_AXI_bvalid[0];
  assign m03_couplers_to_m03_couplers_RDATA = M_AXI_rdata[31:0];
  assign m03_couplers_to_m03_couplers_RREADY = S_AXI_rready[0];
  assign m03_couplers_to_m03_couplers_RRESP = M_AXI_rresp[1:0];
  assign m03_couplers_to_m03_couplers_RVALID = M_AXI_rvalid[0];
  assign m03_couplers_to_m03_couplers_WDATA = S_AXI_wdata[31:0];
  assign m03_couplers_to_m03_couplers_WREADY = M_AXI_wready[0];
  assign m03_couplers_to_m03_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m03_couplers_to_m03_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module m04_couplers_imp_1JLHK1N
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [8:0]M_AXI_araddr;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [8:0]M_AXI_awaddr;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [8:0]S_AXI_araddr;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [8:0]S_AXI_awaddr;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [8:0]m04_couplers_to_m04_couplers_ARADDR;
  wire [0:0]m04_couplers_to_m04_couplers_ARREADY;
  wire [0:0]m04_couplers_to_m04_couplers_ARVALID;
  wire [8:0]m04_couplers_to_m04_couplers_AWADDR;
  wire [0:0]m04_couplers_to_m04_couplers_AWREADY;
  wire [0:0]m04_couplers_to_m04_couplers_AWVALID;
  wire [0:0]m04_couplers_to_m04_couplers_BREADY;
  wire [1:0]m04_couplers_to_m04_couplers_BRESP;
  wire [0:0]m04_couplers_to_m04_couplers_BVALID;
  wire [31:0]m04_couplers_to_m04_couplers_RDATA;
  wire [0:0]m04_couplers_to_m04_couplers_RREADY;
  wire [1:0]m04_couplers_to_m04_couplers_RRESP;
  wire [0:0]m04_couplers_to_m04_couplers_RVALID;
  wire [31:0]m04_couplers_to_m04_couplers_WDATA;
  wire [0:0]m04_couplers_to_m04_couplers_WREADY;
  wire [3:0]m04_couplers_to_m04_couplers_WSTRB;
  wire [0:0]m04_couplers_to_m04_couplers_WVALID;

  assign M_AXI_araddr[8:0] = m04_couplers_to_m04_couplers_ARADDR;
  assign M_AXI_arvalid[0] = m04_couplers_to_m04_couplers_ARVALID;
  assign M_AXI_awaddr[8:0] = m04_couplers_to_m04_couplers_AWADDR;
  assign M_AXI_awvalid[0] = m04_couplers_to_m04_couplers_AWVALID;
  assign M_AXI_bready[0] = m04_couplers_to_m04_couplers_BREADY;
  assign M_AXI_rready[0] = m04_couplers_to_m04_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m04_couplers_to_m04_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m04_couplers_to_m04_couplers_WSTRB;
  assign M_AXI_wvalid[0] = m04_couplers_to_m04_couplers_WVALID;
  assign S_AXI_arready[0] = m04_couplers_to_m04_couplers_ARREADY;
  assign S_AXI_awready[0] = m04_couplers_to_m04_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m04_couplers_to_m04_couplers_BRESP;
  assign S_AXI_bvalid[0] = m04_couplers_to_m04_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m04_couplers_to_m04_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m04_couplers_to_m04_couplers_RRESP;
  assign S_AXI_rvalid[0] = m04_couplers_to_m04_couplers_RVALID;
  assign S_AXI_wready[0] = m04_couplers_to_m04_couplers_WREADY;
  assign m04_couplers_to_m04_couplers_ARADDR = S_AXI_araddr[8:0];
  assign m04_couplers_to_m04_couplers_ARREADY = M_AXI_arready[0];
  assign m04_couplers_to_m04_couplers_ARVALID = S_AXI_arvalid[0];
  assign m04_couplers_to_m04_couplers_AWADDR = S_AXI_awaddr[8:0];
  assign m04_couplers_to_m04_couplers_AWREADY = M_AXI_awready[0];
  assign m04_couplers_to_m04_couplers_AWVALID = S_AXI_awvalid[0];
  assign m04_couplers_to_m04_couplers_BREADY = S_AXI_bready[0];
  assign m04_couplers_to_m04_couplers_BRESP = M_AXI_bresp[1:0];
  assign m04_couplers_to_m04_couplers_BVALID = M_AXI_bvalid[0];
  assign m04_couplers_to_m04_couplers_RDATA = M_AXI_rdata[31:0];
  assign m04_couplers_to_m04_couplers_RREADY = S_AXI_rready[0];
  assign m04_couplers_to_m04_couplers_RRESP = M_AXI_rresp[1:0];
  assign m04_couplers_to_m04_couplers_RVALID = M_AXI_rvalid[0];
  assign m04_couplers_to_m04_couplers_WDATA = S_AXI_wdata[31:0];
  assign m04_couplers_to_m04_couplers_WREADY = M_AXI_wready[0];
  assign m04_couplers_to_m04_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m04_couplers_to_m04_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module s00_couplers_imp_A5QIOZ
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wid,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [11:0]S_AXI_arid;
  input [3:0]S_AXI_arlen;
  input [1:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [11:0]S_AXI_awid;
  input [3:0]S_AXI_awlen;
  input [1:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [11:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [11:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input [11:0]S_AXI_wid;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_pc_to_s00_couplers_ARADDR;
  wire [2:0]auto_pc_to_s00_couplers_ARPROT;
  wire auto_pc_to_s00_couplers_ARREADY;
  wire auto_pc_to_s00_couplers_ARVALID;
  wire [31:0]auto_pc_to_s00_couplers_AWADDR;
  wire [2:0]auto_pc_to_s00_couplers_AWPROT;
  wire auto_pc_to_s00_couplers_AWREADY;
  wire auto_pc_to_s00_couplers_AWVALID;
  wire auto_pc_to_s00_couplers_BREADY;
  wire [1:0]auto_pc_to_s00_couplers_BRESP;
  wire auto_pc_to_s00_couplers_BVALID;
  wire [31:0]auto_pc_to_s00_couplers_RDATA;
  wire auto_pc_to_s00_couplers_RREADY;
  wire [1:0]auto_pc_to_s00_couplers_RRESP;
  wire auto_pc_to_s00_couplers_RVALID;
  wire [31:0]auto_pc_to_s00_couplers_WDATA;
  wire auto_pc_to_s00_couplers_WREADY;
  wire [3:0]auto_pc_to_s00_couplers_WSTRB;
  wire auto_pc_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_auto_pc_ARADDR;
  wire [1:0]s00_couplers_to_auto_pc_ARBURST;
  wire [3:0]s00_couplers_to_auto_pc_ARCACHE;
  wire [11:0]s00_couplers_to_auto_pc_ARID;
  wire [3:0]s00_couplers_to_auto_pc_ARLEN;
  wire [1:0]s00_couplers_to_auto_pc_ARLOCK;
  wire [2:0]s00_couplers_to_auto_pc_ARPROT;
  wire [3:0]s00_couplers_to_auto_pc_ARQOS;
  wire s00_couplers_to_auto_pc_ARREADY;
  wire [2:0]s00_couplers_to_auto_pc_ARSIZE;
  wire s00_couplers_to_auto_pc_ARVALID;
  wire [31:0]s00_couplers_to_auto_pc_AWADDR;
  wire [1:0]s00_couplers_to_auto_pc_AWBURST;
  wire [3:0]s00_couplers_to_auto_pc_AWCACHE;
  wire [11:0]s00_couplers_to_auto_pc_AWID;
  wire [3:0]s00_couplers_to_auto_pc_AWLEN;
  wire [1:0]s00_couplers_to_auto_pc_AWLOCK;
  wire [2:0]s00_couplers_to_auto_pc_AWPROT;
  wire [3:0]s00_couplers_to_auto_pc_AWQOS;
  wire s00_couplers_to_auto_pc_AWREADY;
  wire [2:0]s00_couplers_to_auto_pc_AWSIZE;
  wire s00_couplers_to_auto_pc_AWVALID;
  wire [11:0]s00_couplers_to_auto_pc_BID;
  wire s00_couplers_to_auto_pc_BREADY;
  wire [1:0]s00_couplers_to_auto_pc_BRESP;
  wire s00_couplers_to_auto_pc_BVALID;
  wire [31:0]s00_couplers_to_auto_pc_RDATA;
  wire [11:0]s00_couplers_to_auto_pc_RID;
  wire s00_couplers_to_auto_pc_RLAST;
  wire s00_couplers_to_auto_pc_RREADY;
  wire [1:0]s00_couplers_to_auto_pc_RRESP;
  wire s00_couplers_to_auto_pc_RVALID;
  wire [31:0]s00_couplers_to_auto_pc_WDATA;
  wire [11:0]s00_couplers_to_auto_pc_WID;
  wire s00_couplers_to_auto_pc_WLAST;
  wire s00_couplers_to_auto_pc_WREADY;
  wire [3:0]s00_couplers_to_auto_pc_WSTRB;
  wire s00_couplers_to_auto_pc_WVALID;

  assign M_AXI_araddr[31:0] = auto_pc_to_s00_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = auto_pc_to_s00_couplers_ARPROT;
  assign M_AXI_arvalid = auto_pc_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_pc_to_s00_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = auto_pc_to_s00_couplers_AWPROT;
  assign M_AXI_awvalid = auto_pc_to_s00_couplers_AWVALID;
  assign M_AXI_bready = auto_pc_to_s00_couplers_BREADY;
  assign M_AXI_rready = auto_pc_to_s00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_pc_to_s00_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = auto_pc_to_s00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_pc_to_s00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_arready = s00_couplers_to_auto_pc_ARREADY;
  assign S_AXI_awready = s00_couplers_to_auto_pc_AWREADY;
  assign S_AXI_bid[11:0] = s00_couplers_to_auto_pc_BID;
  assign S_AXI_bresp[1:0] = s00_couplers_to_auto_pc_BRESP;
  assign S_AXI_bvalid = s00_couplers_to_auto_pc_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_auto_pc_RDATA;
  assign S_AXI_rid[11:0] = s00_couplers_to_auto_pc_RID;
  assign S_AXI_rlast = s00_couplers_to_auto_pc_RLAST;
  assign S_AXI_rresp[1:0] = s00_couplers_to_auto_pc_RRESP;
  assign S_AXI_rvalid = s00_couplers_to_auto_pc_RVALID;
  assign S_AXI_wready = s00_couplers_to_auto_pc_WREADY;
  assign auto_pc_to_s00_couplers_ARREADY = M_AXI_arready;
  assign auto_pc_to_s00_couplers_AWREADY = M_AXI_awready;
  assign auto_pc_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_pc_to_s00_couplers_BVALID = M_AXI_bvalid;
  assign auto_pc_to_s00_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_pc_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_pc_to_s00_couplers_RVALID = M_AXI_rvalid;
  assign auto_pc_to_s00_couplers_WREADY = M_AXI_wready;
  assign s00_couplers_to_auto_pc_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_auto_pc_ARBURST = S_AXI_arburst[1:0];
  assign s00_couplers_to_auto_pc_ARCACHE = S_AXI_arcache[3:0];
  assign s00_couplers_to_auto_pc_ARID = S_AXI_arid[11:0];
  assign s00_couplers_to_auto_pc_ARLEN = S_AXI_arlen[3:0];
  assign s00_couplers_to_auto_pc_ARLOCK = S_AXI_arlock[1:0];
  assign s00_couplers_to_auto_pc_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_auto_pc_ARQOS = S_AXI_arqos[3:0];
  assign s00_couplers_to_auto_pc_ARSIZE = S_AXI_arsize[2:0];
  assign s00_couplers_to_auto_pc_ARVALID = S_AXI_arvalid;
  assign s00_couplers_to_auto_pc_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_auto_pc_AWBURST = S_AXI_awburst[1:0];
  assign s00_couplers_to_auto_pc_AWCACHE = S_AXI_awcache[3:0];
  assign s00_couplers_to_auto_pc_AWID = S_AXI_awid[11:0];
  assign s00_couplers_to_auto_pc_AWLEN = S_AXI_awlen[3:0];
  assign s00_couplers_to_auto_pc_AWLOCK = S_AXI_awlock[1:0];
  assign s00_couplers_to_auto_pc_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_auto_pc_AWQOS = S_AXI_awqos[3:0];
  assign s00_couplers_to_auto_pc_AWSIZE = S_AXI_awsize[2:0];
  assign s00_couplers_to_auto_pc_AWVALID = S_AXI_awvalid;
  assign s00_couplers_to_auto_pc_BREADY = S_AXI_bready;
  assign s00_couplers_to_auto_pc_RREADY = S_AXI_rready;
  assign s00_couplers_to_auto_pc_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_auto_pc_WID = S_AXI_wid[11:0];
  assign s00_couplers_to_auto_pc_WLAST = S_AXI_wlast;
  assign s00_couplers_to_auto_pc_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_auto_pc_WVALID = S_AXI_wvalid;
dp_system_auto_pc_0 auto_pc
       (.aclk(S_ACLK_1),
        .aresetn(S_ARESETN_1),
        .m_axi_araddr(auto_pc_to_s00_couplers_ARADDR),
        .m_axi_arprot(auto_pc_to_s00_couplers_ARPROT),
        .m_axi_arready(auto_pc_to_s00_couplers_ARREADY),
        .m_axi_arvalid(auto_pc_to_s00_couplers_ARVALID),
        .m_axi_awaddr(auto_pc_to_s00_couplers_AWADDR),
        .m_axi_awprot(auto_pc_to_s00_couplers_AWPROT),
        .m_axi_awready(auto_pc_to_s00_couplers_AWREADY),
        .m_axi_awvalid(auto_pc_to_s00_couplers_AWVALID),
        .m_axi_bready(auto_pc_to_s00_couplers_BREADY),
        .m_axi_bresp(auto_pc_to_s00_couplers_BRESP),
        .m_axi_bvalid(auto_pc_to_s00_couplers_BVALID),
        .m_axi_rdata(auto_pc_to_s00_couplers_RDATA),
        .m_axi_rready(auto_pc_to_s00_couplers_RREADY),
        .m_axi_rresp(auto_pc_to_s00_couplers_RRESP),
        .m_axi_rvalid(auto_pc_to_s00_couplers_RVALID),
        .m_axi_wdata(auto_pc_to_s00_couplers_WDATA),
        .m_axi_wready(auto_pc_to_s00_couplers_WREADY),
        .m_axi_wstrb(auto_pc_to_s00_couplers_WSTRB),
        .m_axi_wvalid(auto_pc_to_s00_couplers_WVALID),
        .s_axi_araddr(s00_couplers_to_auto_pc_ARADDR),
        .s_axi_arburst(s00_couplers_to_auto_pc_ARBURST),
        .s_axi_arcache(s00_couplers_to_auto_pc_ARCACHE),
        .s_axi_arid(s00_couplers_to_auto_pc_ARID),
        .s_axi_arlen(s00_couplers_to_auto_pc_ARLEN),
        .s_axi_arlock(s00_couplers_to_auto_pc_ARLOCK),
        .s_axi_arprot(s00_couplers_to_auto_pc_ARPROT),
        .s_axi_arqos(s00_couplers_to_auto_pc_ARQOS),
        .s_axi_arready(s00_couplers_to_auto_pc_ARREADY),
        .s_axi_arsize(s00_couplers_to_auto_pc_ARSIZE),
        .s_axi_arvalid(s00_couplers_to_auto_pc_ARVALID),
        .s_axi_awaddr(s00_couplers_to_auto_pc_AWADDR),
        .s_axi_awburst(s00_couplers_to_auto_pc_AWBURST),
        .s_axi_awcache(s00_couplers_to_auto_pc_AWCACHE),
        .s_axi_awid(s00_couplers_to_auto_pc_AWID),
        .s_axi_awlen(s00_couplers_to_auto_pc_AWLEN),
        .s_axi_awlock(s00_couplers_to_auto_pc_AWLOCK),
        .s_axi_awprot(s00_couplers_to_auto_pc_AWPROT),
        .s_axi_awqos(s00_couplers_to_auto_pc_AWQOS),
        .s_axi_awready(s00_couplers_to_auto_pc_AWREADY),
        .s_axi_awsize(s00_couplers_to_auto_pc_AWSIZE),
        .s_axi_awvalid(s00_couplers_to_auto_pc_AWVALID),
        .s_axi_bid(s00_couplers_to_auto_pc_BID),
        .s_axi_bready(s00_couplers_to_auto_pc_BREADY),
        .s_axi_bresp(s00_couplers_to_auto_pc_BRESP),
        .s_axi_bvalid(s00_couplers_to_auto_pc_BVALID),
        .s_axi_rdata(s00_couplers_to_auto_pc_RDATA),
        .s_axi_rid(s00_couplers_to_auto_pc_RID),
        .s_axi_rlast(s00_couplers_to_auto_pc_RLAST),
        .s_axi_rready(s00_couplers_to_auto_pc_RREADY),
        .s_axi_rresp(s00_couplers_to_auto_pc_RRESP),
        .s_axi_rvalid(s00_couplers_to_auto_pc_RVALID),
        .s_axi_wdata(s00_couplers_to_auto_pc_WDATA),
        .s_axi_wid(s00_couplers_to_auto_pc_WID),
        .s_axi_wlast(s00_couplers_to_auto_pc_WLAST),
        .s_axi_wready(s00_couplers_to_auto_pc_WREADY),
        .s_axi_wstrb(s00_couplers_to_auto_pc_WSTRB),
        .s_axi_wvalid(s00_couplers_to_auto_pc_WVALID));
endmodule
