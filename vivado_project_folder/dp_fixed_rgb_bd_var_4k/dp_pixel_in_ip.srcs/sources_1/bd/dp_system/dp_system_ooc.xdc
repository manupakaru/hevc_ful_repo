################################################################################

# This XDC is used only for OOC mode of synthesis, implementation
# This constraints file contains default clock frequencies to be used during
# out-of-context flows such as OOC Synthesis and Hierarchical Designs.
# This constraints file is not used in normal top-down synthesis (default flow
# of Vivado)
################################################################################
#create_clock -name clock_name -period 10 [get_ports clock_name]
################################################################################
create_clock -name SYS_CLK_clk_n -period 10 [get_ports SYS_CLK_clk_n]
create_clock -name SYS_CLK_clk_p -period 10 [get_ports SYS_CLK_clk_p]
create_clock -name dp_mainlink_lnk_clk_n -period 10 [get_ports dp_mainlink_lnk_clk_n]
create_clock -name dp_mainlink_lnk_clk_p -period 10 [get_ports dp_mainlink_lnk_clk_p]
create_clock -name axi4_lite_peripherals_processing_system7_0_FCLK_CLK0 -period 20 [get_pins axi4_lite_peripherals/processing_system7_0/FCLK_CLK0]

################################################################################