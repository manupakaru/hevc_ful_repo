// (c) Copyright 2009 - 2013 Xilinx, Inc. All rights reserved. 
//
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
//
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
//
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
//
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.


`ifndef __DPORT_TX_DEFS__
`define __DPORT_TX_DEFS__

// ----------------------------------------------
// Transmitter Configuration Registers
//   Emulation of high level data structure
// ----------------------------------------------
//`define kDPORT_TX_CFG_REG_COUNT    52 
`define tDPORT_TX_CFG_REGS         [476:0]

`define kCFG_TX_MST_TS_FRAC                  476:467
`define kCFG_TX_MST_TS_INT                   466:459  
`define kCFG_TX_AUX_SOFT_RESET               458  
`define kCFG_TX_AUX_SIGNAL_WIDTH_FILTER      457:450  
`define kCFG_TX_VIDEO_SOFT_RESET_STREAM4     449
`define kCFG_TX_VIDEO_SOFT_RESET_STREAM3     448
`define kCFG_TX_VIDEO_SOFT_RESET_STREAM2     447
`define kCFG_TX_VC_PAYLOAD_UPDATED_IN_SINK   446
`define kCFG_TX_VC_PAYLOAD_WR                445
`define kCFG_TX_VC_PAYLOAD_DATA              444:437
`define kCFG_TX_VC_PAYLOAD_ADDR              436:431
`define kCFG_TX_MST_ENABLE                   430       // 43*32 +: 1
`define kCFG_TX_INIT_WAIT                    429:423   // 42*32 +: 7
`define kCFG_TX_FRAC_BYTES_PER_TU            422:413   // 41*32 +: 10
`define kCFG_TX_MIN_BYTES_PER_TU             412:406   // 40*32 +: 7
`define kCFG_TX_VIDEO_SOFT_RESET                 405   // 39*32 +: 1
`define kCFG_TX_HDCP_KM_UPDATE                   404   // 38*32 +: 1
`define kCFG_TX_HDCP_ENABLE                      403   // 37*32 +: 1
`define kCFG_TX_HDCP_AN_UPPER                402:371   // 36*32 +: 32
`define kCFG_TX_HDCP_AN_LOWER                370:339   // 35*32 +: 32
`define kCFG_TX_HDCP_KM_UPPER                338:315   // 34*32 +: 24
`define kCFG_TX_HDCP_KM_LOWER                314:283   // 33*32 +: 32
`define kCFG_TX_FORCE_SR                         282   // 32*32 +: 1
`define kCFG_TX_INTERLACED                       281   // 31*32 +: 1
`define kCFG_TX_WORDS_PER_LINE               280:263   // 30*32 +: 18
`define kCFG_TX_TRANSFER_UNIT_SIZE           262:257   // 29*32 +: 6
`define kCFG_TX_USER_PIXEL_WIDTH             256:254   // 28*32 +: 3
`define kCFG_TX_N_VID                        253:230   // 27*32 +: 24
`define kCFG_TX_M_VID                        229:206   // 26*32 +: 24
`define kCFG_TX_MSA_MISC1                    205:198   // 25*32 +: 8
`define kCFG_TX_MSA_MISC0                    197:190   // 24*32 +: 8
`define kCFG_TX_POLARITY                     189:188   // 23*32 +: 2
`define kCFG_TX_VRES                         187:172   // 22*32 +: 16
`define kCFG_TX_HRES                         171:156   // 21*32 +: 16
`define kCFG_TX_VSTART                       155:140   // 20*32 +: 16
`define kCFG_TX_HSTART                       139:124   // 19*32 +: 16
`define kCFG_TX_VSWIDTH                      123:109   // 18*32 +: 15
`define kCFG_TX_HSWIDTH                      108:94    // 17*32 +: 15
`define kCFG_TX_VTOTAL                        93:78    // 16*32 +: 16
`define kCFG_TX_HTOTAL                        77:62    // 15*32 +: 16
`define kCFG_TX_SECONDARY_STREAM_ENABLE          61    // 14*32 +: 1
`define kCFG_TX_MAIN_STREAM_ENABLE               60    // 13*32 +: 1
`define kCFG_TX_DISABLE_SCRAMBLING               59    // 12*32 +: 1
`define kCFG_TX_INTERRUPT_MASK                58:53    // 11*32 +: 6
`define kCFG_TRANSMITTER_ENABLE                  52    // 10*32 +: 1
`define kCFG_AUX_LENGTH                       51:48    // 9 *32 +: 4
`define kCFG_AUX_ADDRESS                      47:28    // 8 *32 +: 20
`define kCFG_TX_AUX_CLOCK_DIVIDER             27:20    // 7 *32 +: 8
`define kCFG_DOWNSPREAD_CTRL                     19    // 6 *32 +: 1
`define kCFG_RECOVERED_CLOCK_OUT_EN              18    // 5 *32 +: 1
`define kCFG_LINK_QUAL_PATTERN_SET            17:16    // 4 *32 +: 2
`define kCFG_TX_TRAINING_PATTERN_SET          15:14    // 3 *32 +: 2
`define kCFG_ENHANCED_FRAME_EN                   13    // 2 *32 +: 1
`define kCFG_TX_LANE_COUNT_SET                12:8     // 1 *32 +: 5
`define kCFG_TX_LINK_BW_SET                    7:0     // 0 *32 +: 8

`define kDPORT_TX_CFG_REGS_RESET  {    \
/* kCFG_TX_AUX_SOFT_RESET                */   1'h 0,   \
/* kCFG_TX_AUX_SIGNAL_WIDTH_FILTER       */   8'h 0,   \
/* kCFG_TX_VC_PAYLOAD_UPDATED_IN_SINK                */   1'b0,   \
/* kCFG_TX_VC_PAYLOAD_WR                */   1'b0,   \
/* kCFG_TX_VC_PAYLOAD_DATA                */   8'd0,   \
/* kCFG_TX_VC_PAYLOAD_ADDR                */   6'd0,   \
/* kCFG_TX_MST_ENABLE                */   1'b0,   \
/* kCFG_TX_INIT_WAIT                */   7'h 20,   \
/* kCFG_TX_FRAC_BYTES_PER_TU        */  10'h 00,   \
/* kCFG_TX_MIN_BYTES_PER_TU         */   7'h 00,   \
/* kCFG_TX_VIDEO_SOFT_RESET         */   1'h 0 ,   \
/* kCFG_TX_HDCP_KM_UPDATE           */   1'h 0 ,   \
/* kCFG_TX_HDCP_ENABLE              */   1'h 0 ,   \
/* kCFG_TX_HDCP_AN_UPPER            */  32'h 00,   \
/* kCFG_TX_HDCP_AN_LOWER            */  32'h 00,   \
/* kCFG_TX_HDCP_KM_UPPER            */  24'h 00,   \
/* kCFG_TX_HDCP_KM_LOWER            */  32'h 00,   \
/* kCFG_TX_FORCE_SR                 */   1'h 0 ,   \
/* kCFG_TX_INTERLACED               */   1'h 0 ,   \
/* kCFG_TX_WORDS_PER_LINE           */  18'h 00,   \
/* kCFG_TX_TRANSFER_UNIT_SIZE       */   6'h 40,   \
/* kCFG_TX_USER_PIXEL_WIDTH         */   3'h 0 ,   \
/* kCFG_TX_N_VID                    */  24'h 00,   \
/* kCFG_TX_M_VID                    */  24'h 00,   \
/* kCFG_TX_MSA_MISC1                */   8'h 00,   \
/* kCFG_TX_MSA_MISC0                */   8'h 00,   \
/* kCFG_TX_POLARITY                 */   2'h 0 ,   \
/* kCFG_TX_VRES                     */  16'h 00,   \
/* kCFG_TX_HRES                     */  16'h 00,   \
/* kCFG_TX_VSTART                   */  16'h 00,   \
/* kCFG_TX_HSTART                   */  16'h 00,   \
/* kCFG_TX_VSWIDTH                  */  15'h 00,   \
/* kCFG_TX_HSWIDTH                  */  15'h 00,   \
/* kCFG_TX_VTOTAL                   */  16'h 00,   \
/* kCFG_TX_HTOTAL                   */  16'h 00,   \
/* kCFG_TX_SECONDARY_STREAM_ENABLE  */   1'h 0 ,   \
/* kCFG_TX_MAIN_STREAM_ENABLE       */   1'h 0 ,   \
/* kCFG_TX_DISABLE_SCRAMBLING       */   1'h 0 ,   \
/* kCFG_TX_INTERRUPT_MASK           */   6'h 3F,   \
/* kCFG_TRANSMITTER_ENABLE          */   1'h 0 ,   \
/* kCFG_AUX_LENGTH                  */   4'h 0 ,   \
/* kCFG_AUX_ADDRESS                 */  20'h 00,   \
/* kCFG_TX_AUX_CLOCK_DIVIDER        */   8'h 00,   \
/* kCFG_DOWNSPREAD_CTRL             */   1'h 0 ,   \
/* kCFG_RECOVERED_CLOCK_OUT_EN      */   1'h 0 ,   \
/* kCFG_LINK_QUAL_PATTERN_SET       */   2'h 0 ,   \
/* kCFG_TX_TRAINING_PATTERN_SET     */   2'h 0 ,   \
/* kCFG_ENHANCED_FRAME_EN           */   1'h 0 ,   \
/* kCFG_TX_LANE_COUNT_SET           */   5'h 00,   \
/* kCFG_TX_LINK_BW_SET              */   8'h 00    \
}

`define kDPORT_TX_SEC_CFG_REG_COUNT    9
`define tDPORT_TX_SEC_CFG_REGS      [125:0]

`define kCFG_TX_AUDIO_EXT_PKT_DATA   125:94 // 8*32 +: 32
`define kCFG_TX_AUDIO_EXT_PKT_WRITE      93 // 7*32 +: 1
`define kCFG_TX_AUDIO_PACKET_ID       92:85 // 6*32 +: 8
`define kCFG_TX_AUDIO_NAUD            84:61 // 5*32 +: 24
`define kCFG_TX_AUDIO_MAUD            60:37 // 4*32 +: 24
`define kCFG_TX_AUDIO_INFO_DATA       36:5  // 3*32 +: 32
`define kCFG_TX_AUDIO_INFO_WRITE         4  // 2*32 +: 1
`define kCFG_TX_AUDIO_CHANNEL_COUNT    3:1  // 1*32 +: 3
`define kCFG_TX_AUDIO_ENABLE             0  // 0*32 +: 1

`define kDPORT_TX_SEC_CFG_REGS_RESET  {    \
/* kCFG_TX_AUDIO_EXT_PKT_DATA  */  32'h0,  \
/* kCFG_TX_AUDIO_EXT_PKT_WRITE */   1'h0,  \
/* kCFG_TX_AUDIO_PACKET_ID     */   8'h0,  \
/* kCFG_TX_AUDIO_PACKET_ID     */  24'h0,  \
/* kCFG_TX_AUDIO_SAMPLE_DATA   */  24'h0,  \
/* kCFG_TX_AUDIO_SAMPLE_WRITE  */  32'h0,  \
/* kCFG_TX_AUDIO_DMA_MODE      */   1'h0,  \
/* kCFG_TX_AUDIO_CHANNEL_COUNT */   3'h0,  \
/* kCFG_TX_AUDIO_ENABLE        */   1'h0   \
}

// ----------------------------------------------
//  HDCP status registers
// ----------------------------------------------
`define kDPORT_TX_HDCP_STATUS_COUNT    4
`define tDPORT_TX_HDCP_STATUS         [80:0]

`define kDPORT_HDCP_R0_READY          80 //3*32 +: 1
`define kDPORT_HDCP_R0             79:64 //2*32 +: 16
`define kDPORT_HDCP_AUTO_AN_UPPER  63:32 //1*32 +: 32
`define kDPORT_HDCP_AUTO_AN_LOWER  31:0  //0*32 +: 32

// ----------------------------------------------
// Aux Channel Command Types
// ----------------------------------------------
`define kDPORT_AUX_WRITE            4'b1000
`define kDPORT_AUX_READ             4'b1001
`define kDPORT_IIC_WRITE            4'b0000
`define kDPORT_IIC_WRITE_MOT        4'b0100
`define kDPORT_IIC_READ             4'b0001
`define kDPORT_IIC_READ_MOT         4'b0101
`define kDPORT_IIC_WRITE_STATUS     4'b0010
`define kDPORT_IIC_WRITE_STATUS_MOT 4'b0110

// ----------------------------------------------
// AUX Channel Reply Status
// ----------------------------------------------
`define kDPORT_TX_AUX_REPLY_COUNT     5
`define tDPORT_TX_AUX_REPLY_STATUS    [33:0]

`define kAUX_REPLY_ERROR                  33 //4*16 +: 1
`define kAUX_REPLY_DATA_COUNT          32:28 //3*16 +: 5
`define kAUX_REPLY_TRANSACTION_STATUS  27:12 //2*16 +: 16
`define kAUX_REPLY_COUNT               11:4  //1*16 +: 8
`define kAUX_REPLY_CODE                 3:0  //0*16 +: 4

`define kCFG_AUX_REPLY_STATUS_RESET   {   \
/* kAUX_REPLY_ERROR               (1) */  1'h 0, \
/* kAUX_REPLY_DATA_COUNT          (5) */  5'h 0, \
/* kAUX_REPLY_TRANSACTION_STATUS (16) */ 16'h 0, \
/* kAUX_REPLY_COUNT               (8) */  8'h 0, \
/* kAUX_REPLY_CODE                (4) */  4'h 0  \
}

// ----------------------------------------------
// Interrupt status register
// ----------------------------------------------
`define tDPORT_TX_INTERRUPT_STATUS    [31:0]

`define kINTERRUPT_HPD_IRQ                0
`define kINTERRUPT_HPD_DETECTED           1
`define kINTERRUPT_REPLY_RECEIVED         2
`define kINTERRUPT_REPLY_TIMEOUT          3
`define kINTERRUPT_HPD_PULSE_DET          4
`define kINTERRUPT_EXT_PKT_TXD            5

`define kINTERRUPT_HPD_STATE              8
`define kINTERRUPT_REQUEST_STATE          9
`define kINTERRUPT_REPLY_STATE            10
`define kINTERRUPT_REPLY_TIMEOUT_STATE    11

`define kINTERRUPT_EXT_PKT_TXD_STREAM2    12
`define kINTERRUPT_EXT_PKT_TXD_STREAM3    13
`define kINTERRUPT_EXT_PKT_TXD_STREAM4    14

`define kINTERRUPT_HPD_DURATION           31:16

// ----------------------------------------------
// PHY config and status
// ----------------------------------------------
`define tDPORT_TX_PHY_STATUS  [49:0]
`define kCFG_TX_PHY_STATUS_DRP_LOCKED    49
`define kCFG_TX_PHY_STATUS_DRP_READY     48
`define kCFG_TX_PHY_STATUS_DRP_RDATA  47:32
`define kCFG_TX_PHY_STATUS            31:0

`define tDPORT_TX_PHY_CONFIG  [117:0]
`define kCFG_TX_PHY_DRP_ACCESS               117
`define kCFG_TX_PHY_DRP_WDATA            116:101
`define kCFG_TX_PHY_DRP_ADDR             100:93
`define kCFG_TX_PHY_DRP_WRITE                92
`define kCFG_TX_PHY_LOOPBACK              91:89
`define kCFG_TX_PHY_PRBSFORCEERR          88
`define kCFG_TX_PHY_TXPOLARITY            87
`define kCFG_TX_PHY_PCS_RST               86
`define kCFG_TX_PHY_BUF_RST               85
`define kCFG_TX_PHY_PMA_RST               84
`define kCFG_TX_PHY_POSTCURSOR_LANE_3     83:79
`define kCFG_TX_PHY_POSTCURSOR_LANE_2     78:74
`define kCFG_TX_PHY_POSTCURSOR_LANE_1     73:69
`define kCFG_TX_PHY_POSTCURSOR_LANE_0     68:64
`define kCFG_TX_PHY_PRECURSOR_LANE_3      63:59
`define kCFG_TX_PHY_PRECURSOR_LANE_2      58:54
`define kCFG_TX_PHY_PRECURSOR_LANE_1      53:49
`define kCFG_TX_PHY_PRECURSOR_LANE_0      48:44
`define kCFG_TX_PHY_POWER_DOWN            43:40
`define kCFG_TX_PHY_RESET_2                39
`define kCFG_TX_PHY_PLL_FB_UPDATE          38
`define kCFG_TX_PHY_PLL_FB_SETTING        37:30
`define kCFG_TX_PHY_TRANSMIT_PRBS7         29
`define kCFG_TX_PHY_VOLTAGE_DIFF_LANE_3   28:25
`define kCFG_TX_PHY_VOLTAGE_DIFF_LANE_2   24:21
`define kCFG_TX_PHY_VOLTAGE_DIFF_LANE_1   20:17
`define kCFG_TX_PHY_VOLTAGE_DIFF_LANE_0   16:13
`define kCFG_TX_PHY_PREEMPHASIS_LANE_3    12:10
`define kCFG_TX_PHY_PREEMPHASIS_LANE_2     9:7
`define kCFG_TX_PHY_PREEMPHASIS_LANE_1     6:4
`define kCFG_TX_PHY_PREEMPHASIS_LANE_0     3:1
`define kCFG_TX_PHY_RESET                   0

`define kCFG_TX_PHY_CONFIG_RESET  {    \
/* kCFG_TX_PHY_LOOPBACK */          3'b000, \
/* kCFG_TX_PHY_PRBSFORCEERR */      1'b0, \
/* kCFG_TX_PHY_TXPOLARITY */        1'b0, \
/* kCFG_TX_PHY_PCS_RST */           1'b0, \
/* kCFG_TX_PHY_BUF_RST */           1'b0, \
/* kCFG_TX_PHY_PMA_RST */           1'b0, \
/* kCFG_TX_PHY_POSTCURSOR_LANE_3   */    5'b0,           \
/* kCFG_TX_PHY_POSTCURSOR_LANE_2   */    5'b0,           \
/* kCFG_TX_PHY_POSTCURSOR_LANE_1   */    5'b0,           \
/* kCFG_TX_PHY_POSTCURSOR_LANE_0   */    5'b0,           \
/* kCFG_TX_PHY_PRECURSOR_LANE_3    */    5'b0,           \
/* kCFG_TX_PHY_PRECURSOR_LANE_2    */    5'b0,           \
/* kCFG_TX_PHY_PRECURSOR_LANE_1    */    5'b0,           \
/* kCFG_TX_PHY_PRECURSOR_LANE_0    */    5'b0,           \
/* kCFG_TX_PHY_POWER_DOWN          */    4'b0000,        \
/* kCFG_TX_PHY_RESET_2             */    1'b1,           \
/* kCFG_TX_PHY_PLL_FB_UPDATE       */    1'b0,           \
/* kCFG_TX_PHY_PLL_FB_SETTING      */    8'h00,          \
/* kCFG_TX_PHY_TRANSMIT_PRBS7      */    1'b0,           \
/* kCFG_TX_PHY_VOLTAGE_DIFF_LANE_3 */    4'b000,         \
/* kCFG_TX_PHY_VOLTAGE_DIFF_LANE_2 */    4'b000,         \
/* kCFG_TX_PHY_VOLTAGE_DIFF_LANE_1 */    4'b000,         \
/* kCFG_TX_PHY_VOLTAGE_DIFF_LANE_0 */    4'b000,         \
/* kCFG_TX_PHY_PREEMPHASIS_LANE_3  */    3'b000,         \
/* kCFG_TX_PHY_PREEMPHASIS_LANE_2  */    3'b000,         \
/* kCFG_TX_PHY_PREEMPHASIS_LANE_1  */    3'b000,         \
/* kCFG_TX_PHY_PREEMPHASIS_LANE_0  */    3'b000,         \
/* kCFG_TX_PHY_RESET               */    1'b1            \
}

// ----------------------------------------------
// synchronizer stage            
// ----------------------------------------------
`define SYNC_STAGE_LINK 4
`define SYNC_STAGE_AXI  2
`define SYNC_STAGE_AUD  2
`define SYNC_STAGE_VID  3

// ----------------------------------------------
// control character definitions
// ----------------------------------------------
`define kTX_DUMMY_CHAR      8'h00
`define kTX_CONTROL_CHAR_BS 8'hbc        // K28.5
`define kTX_CONTROL_CHAR_BE 8'hfb        // K27.7
`define kTX_CONTROL_CHAR_SS 8'h5c        // K28.2
`define kTX_CONTROL_CHAR_SE 8'hfd        // K29.7
`define kTX_CONTROL_CHAR_FS 8'hfe        // K30.7
`define kTX_CONTROL_CHAR_FE 8'hf7        // K23.7
`define kTX_CONTROL_CHAR_SR 8'h1c        // K28.0
`define kTX_CONTROL_CHAR_CP 8'h3c        // K28.1
`define kTX_CONTROL_CHAR_BF 8'h7c        // K28.3
`define kTX_CONTROL_CHAR_MST_SR 8'hBC        // K28.5

//      K-code         Single-stream Multi-stream
`define TX_K28_0 8'h1C // SR            GP1 index 2
`define TX_K28_1 8'h3C // CPBS/CP       GP2 CPSR
`define TX_K28_2 8'h5C // SS            GP1 index 3
`define TX_K28_3 8'h7C // CPSR/BF       GP1 index 4
`define TX_K28_4 8'h9C // rsvd          GP2 rsvd
`define TX_K28_5 8'hBC // BS            GP2 SR
`define TX_K28_6 8'hDC // rsvd          GP1 index 5
`define TX_K28_7 8'hFC // rsvd          GP2 rsvd
`define TX_K23_7 8'hF7 // FE            GP1 index 0
`define TX_K27_7 8'hFB // BE            GP1 index 1
`define TX_K29_7 8'hFD // SE            GP1 index 6
`define TX_K30_7 8'hFE // FS            GP1 index 7

`define TX_SST_SR   `TX_K28_0
`define TX_SST_CPBS `TX_K28_1
`define TX_SST_SS   `TX_K28_2
`define TX_SST_CPSR `TX_K28_3
`define TX_SST_BF   `TX_K28_3
`define TX_SST_BS   `TX_K28_5
`define TX_SST_FE   `TX_K23_7
`define TX_SST_BE   `TX_K27_7
`define TX_SST_SE   `TX_K29_7
`define TX_SST_FS   `TX_K30_7

`define TX_MST_CPSR `TX_K28_1
`define TX_MST_SR   `TX_K28_5

`define kTX_CONTROL_MTPH_SR    `TX_K28_5
`define kTX_CONTROL_MTPH_RSVD1 `TX_K28_1
`define kTX_CONTROL_MTPH_RSVD2 `TX_K28_7
`define kTX_CONTROL_MTPH_RSVD3 `TX_K28_4

`define kTX_VCPAYLOD_AV_PORT0    6'b000001
`define kTX_VCPAYLOD_AV_PORT1    6'b000010
`define kTX_VCPAYLOD_UNALLOCATED 6'b000000

`define kTX_SPECIAL_CONTROL_CONSTANT 8'hFF
`define TX_C0_KCHAR `TX_K23_7
`define TX_C1_KCHAR `TX_K27_7
`define TX_C2_KCHAR `TX_K28_0
`define TX_C3_KCHAR `TX_K28_2
`define TX_C4_KCHAR `TX_K28_3
`define TX_C5_KCHAR `TX_K28_6
`define TX_C6_KCHAR `TX_K29_7
`define TX_C7_KCHAR `TX_K30_7

`endif
