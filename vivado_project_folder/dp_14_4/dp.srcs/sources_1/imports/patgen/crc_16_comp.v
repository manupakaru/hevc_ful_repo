module crc_16_comp (
input  wire [15:0] data_in,
input  wire [15:0] init_in,
output reg  [15:0] crc_out);

  reg [15:0] d;
  reg [15:0] c;

  always @ ( data_in or init_in )
  begin
    d = data_in;
    c = init_in;

    crc_out[0]  = d[15]^d[13]^d[12]^d[11]^d[10]^d[9]^d[8]^d[7]^d[6]^d[5]^d[4]^d[3]^d[2]^d[1]^d[0]^
                  c[0]^c[1]^c[2]^c[3]^c[4]^c[5]^c[6]^c[7]^c[8]^c[9]^c[10]^c[11]^c[12]^c[13]^c[15];
    crc_out[1]  = d[14]^d[13]^d[12]^d[11]^d[10]^d[9]^d[8]^d[7]^d[6]^d[5]^d[4]^d[3]^d[2]^d[1]^
                  c[1]^c[2]^c[3]^c[4]^c[5]^c[6]^c[7]^c[8]^c[9]^c[10]^c[11]^c[12]^c[13]^c[14];
    crc_out[2]  = d[14]^d[1]^d[0]^c[0]^c[1]^c[14];
    crc_out[3]  = d[15]^d[2]^d[1]^c[1]^c[2]^c[15];
    crc_out[4]  = d[3]^d[2]^c[2]^c[3];
    crc_out[5]  = d[4]^d[3]^c[3]^c[4];
    crc_out[6]  = d[5]^d[4]^c[4]^c[5];
    crc_out[7]  = d[6]^d[5]^c[5]^c[6];
    crc_out[8]  = d[7]^d[6]^c[6]^c[7];
    crc_out[9]  = d[8]^d[7]^c[7]^c[8];
    crc_out[10] = d[9]^d[8]^c[8]^c[9];
    crc_out[11] = d[10]^d[9]^c[9]^c[10];
    crc_out[12] = d[11]^d[10]^c[10]^c[11];
    crc_out[13] = d[12]^d[11]^c[11]^c[12];
    crc_out[14] = d[13]^d[12]^c[12]^c[13];
    crc_out[15] = d[15]^d[14]^d[12]^d[11]^d[10]^d[9]^d[8]^d[7]^d[6]^d[5]^d[4]^d[3]^d[2]^d[1]^d[0]^
                  c[0]^c[1]^c[2]^c[3]^c[4]^c[5]^c[6]^c[7]^c[8]^c[9]^c[10]^c[11]^c[12]^c[14]^c[15];
  end

endmodule


