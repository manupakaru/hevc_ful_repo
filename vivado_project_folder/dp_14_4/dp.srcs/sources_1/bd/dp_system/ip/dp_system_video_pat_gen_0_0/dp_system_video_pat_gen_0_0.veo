// (c) Copyright 1995-2015 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.

// IP VLNV: xilinx.com:ip:video_pat_gen:1.1
// IP Revision: 7

// The following must be inserted into your Verilog file for this
// core to be instantiated. Change the instance name and port connections
// (in parentheses) to your own signal names.

//----------- Begin Cut here for INSTANTIATION Template ---// INST_TAG
dp_system_video_pat_gen_0_0 your_instance_name (
  .apb_clk(apb_clk),            // input wire apb_clk
  .apb_reset(apb_reset),        // input wire apb_reset
  .apb_select(apb_select),      // input wire apb_select
  .apb_enable(apb_enable),      // input wire apb_enable
  .apb_write(apb_write),        // input wire apb_write
  .apb_addr(apb_addr),          // input wire [11 : 0] apb_addr
  .apb_wdata(apb_wdata),        // input wire [31 : 0] apb_wdata
  .apb_rdata(apb_rdata),        // output wire [31 : 0] apb_rdata
  .apb_ready(apb_ready),        // output wire apb_ready
  .vid_clk_sel(vid_clk_sel),    // output wire vid_clk_sel
  .vid_clk_M(vid_clk_M),        // output wire [7 : 0] vid_clk_M
  .vid_clk_D(vid_clk_D),        // output wire [15 : 0] vid_clk_D
  .vid_clk_prog(vid_clk_prog),  // output wire vid_clk_prog
  .sw_vid_reset(sw_vid_reset),  // output wire sw_vid_reset
  .run_pattern(run_pattern),    // input wire run_pattern
  .vid_clk(vid_clk),            // input wire vid_clk
  .vid_reset(vid_reset),        // input wire vid_reset
  .vid_enable(vid_enable),      // output wire vid_enable
  .vid_vsync(vid_vsync),        // output wire vid_vsync
  .vid_hsync(vid_hsync),        // output wire vid_hsync
  .vid_oddeven(vid_oddeven),    // output wire vid_oddeven
  .vid_pixel0(vid_pixel0),      // output wire [47 : 0] vid_pixel0
  .vid_pixel1(vid_pixel1),      // output wire [47 : 0] vid_pixel1
  .vid_pixel2(vid_pixel2),      // output wire [47 : 0] vid_pixel2
  .vid_pixel3(vid_pixel3),      // output wire [47 : 0] vid_pixel3
  .patt_done(patt_done)        // output wire patt_done
);
// INST_TAG_END ------ End INSTANTIATION Template ---------

// You must compile the wrapper file dp_system_video_pat_gen_0_0.v when simulating
// the core, dp_system_video_pat_gen_0_0. When compiling the wrapper file, be sure to
// reference the Verilog simulation library.

