2014.4:
 * Version 5.0 (Rev. 1)
 * Added Automotive and new device support in 7 series
 * Encrypted source files are concatenated together to reduce the number of files and to reduce simulator compile time

2014.3:
 * Version 5.0
 * Updated security attributes
 * Added 32-bit GT Data width support in TX and RX to improve timing
 * Added MST support in RX
 * Added support for Q grade 7 Series devices
 * Added HDCP Interface support

2014.2:
 * Version 4.2 (Rev. 2)
 * Fixed sync_cell instantiation in rx_interrupt module

2014.1:
 * Version 4.2 (Rev. 1)
 * Repackaged to improve internal automation, no functional changes
 * Netlists created by write_verilog and write_vhdl are IEEE P1735 encrypted, with keys for supported simulators so that netlist simulation can still be done
 * Enable third party synthesis tools to read encrypted netlists (but not source HDL)
 * Added support for XC7Z015 devices
 * XDC File updates for cross-clock paths

2013.4:
 * Version 4.2
 * Added optional transceiver control and status ports
 * Added Software access to GT DRP
 * XDC File updates for Audio

2013.3:
 * Version 4.1
 * Reduced warnings in Synthesis and Simulation
 * Added GUI option to include or exclude shareable logic resources in the core, for more information on this change please refer to the Migrating section in the Product Guide
 * Added optional transceiver control and status ports
 * Added support for Zynq devices
 * Updated clock synchronizers to improve Mean Time Between Failures (MTBF) for metastability
 * Added GUI option to select Bi-directional / Uni-directional IOs for AUX
 * Upgraded the fifo_generator_v10_0 to fifo_generator_v11_0
 * MST Transmit Interop Related Updates
 * Added optional ports (MST related), existing IP instances are unchanged
 * Added output pixel mode and hres/vres ports for sink controller
 * AUX Filter and IIC 1-sec timeout updates
 * XDC File Updates for cross-clock paths
 * Enhanced support for IP Integrator

2013.2:
 * Version 4.0 (Rev. 1)
 * Repackaged to enable internal version management, no functional changes.
 * Constraints processing order changed

2013.1:
 * Version 4.0
 * Upgraded the fifo_generator_v9_1 to fifo_generator_v10_0
 * Upgraded the blk_mem_gen_v7_2 to blk_mem_gen_v8_0
 * Upgraded the dst_mem_gen_v7_2 to dst_mem_gen_v8_0
 * Added audio support SST mode
 * Added Multi stream support
 * New GTH wrapper is added

2012.4:
 * Version 3.2
 * Artix 7 support is added
 * Upgraded the fifo_generator_v8_2 to fifo_generator_v9_1

(c) Copyright 2011 - 2014 Xilinx, Inc. All rights reserved.

This file contains confidential and proprietary information
of Xilinx, Inc. and is protected under U.S. and
international copyright and other intellectual property
laws.

DISCLAIMER
This disclaimer is not a license and does not grant any
rights to the materials distributed herewith. Except as
otherwise provided in a valid license issued to you by
Xilinx, and to the maximum extent permitted by applicable
law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
(2) Xilinx shall not be liable (whether in contract or tort,
including negligence, or under any other theory of
liability) for any loss or damage of any kind or nature
related to, arising under or in connection with these
materials, including for any direct, or any indirect,
special, incidental, or consequential loss or damage
(including loss of data, profits, goodwill, or any type of
loss or damage suffered as a result of any action brought
by a third party) even if such damage or loss was
reasonably foreseeable or Xilinx had been advised of the
possibility of the same.

CRITICAL APPLICATIONS
Xilinx products are not designed or intended to be fail-
safe, or for use in any application requiring fail-safe
performance, such as life-support or safety devices or
systems, Class III medical devices, nuclear facilities,
applications related to the deployment of airbags, or any
other applications that could lead to death, personal
injury, or severe property or environmental damage
(individually and collectively, "Critical
Applications"). Customer assumes the sole risk and
liability of any use of Xilinx products in Critical
Applications, subject only to applicable laws and
regulations governing limitations on product liability.

THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
PART OF THIS FILE AT ALL TIMES.
