//////////////////////////////////////////////////////////////////////////
//
// Programmable Video Pattern Generator
// 
//
// Author: Bob Feng
//
//////////////////////////////////////////////////////////////////////////
`include "def.v"

module video_pat_gen (
  input  wire         apb_clk,                        // APB bus clock
  input  wire         apb_reset,                      // APB domain reset
  input  wire         apb_select,                     // select
  input  wire         apb_enable,                     // enable
  input  wire         apb_write,                      // write enable
  input  wire [11:0]  apb_addr,                       // address
  input  wire [31:0]  apb_wdata,                      // write data
  output wire [31:0]  apb_rdata,                      // read data
  output wire         apb_ready,                      // read data
  output wire         vid_clk_sel,
  output wire [7:0]   vid_clk_M,
  output wire [15:0]  vid_clk_D,
  output reg          vid_clk_prog,
  output wire         sw_vid_reset,

  input   wire        run_pattern,
  input   wire        vid_clk,
  input   wire        vid_reset,
  output  wire        vid_enable,
  output  wire        vid_vsync,
  output  wire        vid_hsync,
  output  wire        vid_oddeven,
  output  wire [47:0] vid_pixel0,
  output  wire [47:0] vid_pixel1,
  output  wire [47:0] vid_pixel2,
  output  wire [47:0] vid_pixel3,
  output  reg         patt_done
);

  wire  [`DISP_DTC_REGS_SIZE-1:0]  disp_dtc_regs;
  wire  [2:0]    hdcolorbar_cfg;
  wire [11:0] bgnd_hcount, bgnd_vcount;
(* MARK_DEBUG="true" *)   wire [7:0] dp_misc0, dp_misc1;
(* MARK_DEBUG="true" *)   wire [2:0] test_pattern;
  wire [2:0] test_pattern_mux;
  wire en_sw_pattern;
(* MARK_DEBUG="true" *)   wire dual_pixel_mode;
(* MARK_DEBUG="true" *)   wire quad_pixel_mode;
(* MARK_DEBUG="true" *)   wire [2:0] bpc;
(* MARK_DEBUG="true" *)   wire vid_enable_adj;
(* MARK_DEBUG="true" *) wire        frame_crc_vld;
wire [15:0] rcrc0_out;
wire [15:0] gcrc0_out;
wire [15:0] bcrc0_out;
wire [15:0] rcrc1_out;
wire [15:0] gcrc1_out;
wire [15:0] bcrc1_out;
wire [15:0] rcrc2_out;
wire [15:0] gcrc2_out;
wire [15:0] bcrc2_out;
wire [15:0] rcrc3_out;
wire [15:0] gcrc3_out;
wire [15:0] bcrc3_out;
wire VGA_HSYNC_INT, VGA_VSYNC_INT;
wire bgnd_hblnk, bgnd_vblnk;
wire [47:0] cbar_vid_pixel0, cbar_vid_pixel1;
wire [47:0] cbar_vid_pixel2, cbar_vid_pixel3;
wire [47:0] test_pixel0, test_pixel1;
wire [47:0] test_pixel2, test_pixel3;
wire sw_enable;

reg [2:0] test_pattern_ext;
reg [`DISP_DTC_REGS_SIZE-1:0] disp_dtc_regs_sync, disp_dtc_regs_q;
reg active;
reg vsync, hsync;
reg VGA_HSYNC, VGA_VSYNC;
reg de;
reg [7:0] de_taps;
reg [2:0] vid_fmt_cnt;
reg [7:0] vid_chg_cnt;
reg run_pattern_q;
reg sw_enable_q;
  
 assign apb_ready = 1'b1;
 
  always @ (posedge vid_clk) begin
    disp_dtc_regs_q <= disp_dtc_regs;
    disp_dtc_regs_sync <= disp_dtc_regs_q;
  end

  assign sw_enable = disp_dtc_regs_sync[`ENABLE];

  regs apb_regs_inst (
    // apb interface
    .run_pattern                 ( 1'b0       ),
    .apb_clk                     ( apb_clk           ),
    .apb_reset                   ( apb_reset         ),
    .apb_select                  ( apb_select        ),
    .apb_enable                  ( apb_enable        ),
    .apb_write                   ( apb_write         ),
    .apb_addr                    ( apb_addr          ),
    .apb_wdata                   ( apb_wdata         ),
    .apb_rdata                   ( apb_rdata         ),
    // register interface
    .disp_dtc_regs               ( disp_dtc_regs     ),
    .hdcolorbar_cfg              ( hdcolorbar_cfg    ),
    .vid_clk_sel                 ( vid_clk_sel       ),
    .vid_clk_M                   ( vid_clk_M         ),
    .vid_clk_D                   ( vid_clk_D         ),
    .sw_vid_reset                ( sw_vid_reset      ),

    .vsync_counter               (bgnd_vcount),
    .hsync_counter               (bgnd_hcount),
    .dp_misc0                    (dp_misc0),
    .dp_misc1                    (dp_misc1),
    .test_pattern                (test_pattern),
    .en_sw_pattern               (en_sw_pattern),
    .dual_pixel_mode             (dual_pixel_mode),
    .quad_pixel_mode             (quad_pixel_mode),
    .frame_crc_vld               (frame_crc_vld ),
    .rcrc0                       (rcrc0_out),
    .gcrc0                       (gcrc0_out),
    .bcrc0                       (bcrc0_out),
    .rcrc1                       (rcrc1_out),
    .gcrc1                       (gcrc1_out),
    .bcrc1                       (bcrc1_out),
    .rcrc2                       (rcrc2_out),
    .gcrc2                       (gcrc2_out),
    .bcrc2                       (bcrc2_out),
    .rcrc3                       (rcrc3_out),
    .gcrc3                       (gcrc3_out),
    .bcrc3                       (bcrc3_out),    
    .data_en_counter             (12'b0));

  timing vtc_inst (
    .tc_hsblnk(disp_dtc_regs_sync[`TC_HSBLNK]),   //input
    .tc_hssync(disp_dtc_regs_sync[`TC_HSSYNC]),   //input
    .tc_hesync(disp_dtc_regs_sync[`TC_HESYNC]),   //input
    .tc_heblnk(disp_dtc_regs_sync[`TC_HEBLNK]),   //input
    .hcount(bgnd_hcount),                         //output
    .hsync(VGA_HSYNC_INT),                        //output
    .hblnk(bgnd_hblnk),                           //output
    .tc_vsblnk(disp_dtc_regs_sync[`TC_VSBLNK]),   //input
    .tc_vssync(disp_dtc_regs_sync[`TC_VSSYNC]),   //input
    .tc_vesync(disp_dtc_regs_sync[`TC_VESYNC]),   //input
    .tc_veblnk(disp_dtc_regs_sync[`TC_VEBLNK]),   //input
    .vcount(bgnd_vcount),                         //output
    .vsync(VGA_VSYNC_INT),                        //output
    .vblnk(bgnd_vblnk),                           //output
    .restart(vid_reset),
    .clk(vid_clk));

  always @ (posedge vid_clk) begin
    hsync <= VGA_HSYNC_INT; //^ disp_dtc_regs_sync[`HSYNC_POLARITY];
    vsync <= VGA_VSYNC_INT; //^ disp_dtc_regs_sync[`VSYNC_POLARITY];
    VGA_HSYNC <= hsync;
    VGA_VSYNC <= vsync;
    active <= !bgnd_hblnk && !bgnd_vblnk;
    de <= active;
  end

  always@(posedge vid_clk) begin
    de_taps <= {de_taps[2:0], de};    
  end    

  always @(posedge vid_clk) begin
    if(vid_reset) begin
      vid_chg_cnt <= 'h0;
    //end else if(bgnd_vcount[6:0] == 7'h00) begin
    end else if(VGA_VSYNC_INT & ~VGA_VSYNC) begin
      vid_chg_cnt <= vid_chg_cnt + 1;
    end      

    if(vid_reset) begin
      vid_fmt_cnt <= 3'b000;      
    end else if(&vid_chg_cnt) begin      
      vid_fmt_cnt <= vid_fmt_cnt + 3'b001;
    end      
  end    

  //assign vid_enable = de;
  assign vid_enable = vid_enable_adj;
  assign vid_hsync = VGA_HSYNC;
  assign vid_vsync = VGA_VSYNC;
  assign vid_oddeven = 1'b 0;

  hdcolorbar hdcolorbar_inst (
    .i_clk_74M (vid_clk),             //video pixel clock
    .i_rst     (vid_reset),
    .baronly   (hdcolorbar_cfg[0]),   //output only 75% color bar
    .i_format  (hdcolorbar_cfg[2:1]),
    //.baronly   (vid_fmt_cnt[0]),   //output only 75% color bar
    //.i_format  (vid_fmt_cnt[2:1]),
    .i_vcnt    (bgnd_vcount),         //vertical counter from video timing generator
    .i_hcnt    (bgnd_hcount),         //horizontal counter from video timing generator
    .o_r_0       (cbar_vid_pixel0[47:40]),
    .o_g_0       (cbar_vid_pixel0[31:24]),
    .o_b_0       (cbar_vid_pixel0[15:8]),
    .o_r_1       (cbar_vid_pixel1[47:40]),
    .o_g_1       (cbar_vid_pixel1[31:24]),
    .o_b_1       (cbar_vid_pixel1[15:8]),
    .o_r_2       (cbar_vid_pixel2[47:40]),
    .o_g_2       (cbar_vid_pixel2[31:24]),
    .o_b_2       (cbar_vid_pixel2[15:8]),
    .o_r_3       (cbar_vid_pixel3[47:40]),
    .o_g_3       (cbar_vid_pixel3[31:24]),
    .o_b_3       (cbar_vid_pixel3[15:8])
  );

  assign test_pattern_mux = (en_sw_pattern)?test_pattern:test_pattern_ext;

  dp_test_pattern dp_test_pattern_inst(
	  .clk (vid_clk),
	  .rst (vid_reset),
	  .vid_enable (de),
	  .vid_enable_adj (vid_enable_adj),
	  .misc0 (dp_misc0),
	  .misc1 (dp_misc1),
	  .vcount (bgnd_vcount),
	  .hcount (bgnd_hcount),
	  .hsync (hsync),
	  .vsync (vsync),
	  .dual_pixel_mode (dual_pixel_mode),
	  .quad_pixel_mode (quad_pixel_mode),
	  .bpc_out (bpc),
	  .pattern (test_pattern_mux),
	  .pixel0 (test_pixel0),
	  .pixel1 (test_pixel1),
	  .pixel2 (test_pixel2),
	  .pixel3 (test_pixel3)
	  );

  assign cbar_vid_pixel0[7:0]   = 8'b 0;
  assign cbar_vid_pixel0[23:16] = 8'b 0;
  assign cbar_vid_pixel0[39:32] = 8'b 0;

  assign cbar_vid_pixel1[7:0]   = 8'b 0;
  assign cbar_vid_pixel1[23:16] = 8'b 0;
  assign cbar_vid_pixel1[39:32] = 8'b 0;

  assign cbar_vid_pixel2[7:0]   = 8'b 0;
  assign cbar_vid_pixel2[23:16] = 8'b 0;
  assign cbar_vid_pixel2[39:32] = 8'b 0;

  assign cbar_vid_pixel3[7:0]   = 8'b 0;
  assign cbar_vid_pixel3[23:16] = 8'b 0;
  assign cbar_vid_pixel3[39:32] = 8'b 0;


  always@(posedge apb_clk) begin
    run_pattern_q <= run_pattern;    
  end

  always@(posedge apb_clk) begin
    if(apb_reset || vid_reset) begin     
      test_pattern_ext <= 3'b001;      
    end else if(run_pattern_q != run_pattern) begin
      test_pattern_ext <= test_pattern_ext + 1;      
    end      
  end    

  assign vid_pixel0 = (test_pattern_mux==3'b000)?cbar_vid_pixel0:test_pixel0;
  assign vid_pixel1 = (test_pattern_mux==3'b000)?cbar_vid_pixel1:test_pixel1;
  assign vid_pixel2 = (test_pattern_mux==3'b000)?cbar_vid_pixel2:test_pixel2;
  assign vid_pixel3 = (test_pattern_mux==3'b000)?cbar_vid_pixel3:test_pixel3;

  always @ ( posedge vid_clk ) begin
    patt_done   <= vid_vsync;
  end


  always @ (posedge apb_clk) begin
    sw_enable_q <= sw_enable;
    vid_clk_prog <= sw_enable & !sw_enable_q;
  end


vid_crc_16 vid_crc_16_inst(
        .vid_clk (vid_clk),
        .vid_reset (vid_reset),
        .vsync     (vid_vsync),
        .hsync     (vid_hsync),
        .enable    (vid_enable),
        .pix0 (vid_pixel0),
        .pix1 (vid_pixel1),
        .pix2 (vid_pixel2),
        .pix3 (vid_pixel3),
        .bpc  (bpc),
        .user_pix_mode (2'b 0),
        .frame_crc_vld (frame_crc_vld),
        .rcrc0_out     (rcrc0_out    ),
        .gcrc0_out     (gcrc0_out    ),
        .bcrc0_out     (bcrc0_out    ),
        .rcrc1_out     (rcrc1_out    ),
        .gcrc1_out     (gcrc1_out    ),
        .bcrc1_out     (bcrc1_out    ),
        .rcrc2_out     (rcrc2_out    ),
        .gcrc2_out     (gcrc2_out    ),
        .bcrc2_out     (bcrc2_out    ),
        .rcrc3_out     (rcrc3_out    ),
        .gcrc3_out     (gcrc3_out    ),
        .bcrc3_out     (bcrc3_out    ));

endmodule
