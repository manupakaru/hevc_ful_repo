// (c) Copyright 2009 - 2013 Xilinx, Inc. All rights reserved. 
//
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
//
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
//
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
//
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.


`ifndef __DPORT_RX_DEFS__
`define __DPORT_RX_DEFS__

//`define SYSTEM_VERILOG

// ----------------------------------------------
//  AUX command field definitions
// ----------------------------------------------
`define kAUX_COMM_AUX_READ              4'b1001
`define kAUX_COMM_AUX_WRITE             4'b1000
`define kAUX_COMM_I2C_READ              4'b0001
`define kAUX_COMM_I2C_READ_MOT          4'b0101
`define kAUX_COMM_I2C_WRITE             4'b0000
`define kAUX_COMM_I2C_WRITE_MOT         4'b0100
`define kAUX_COMM_I2C_WRITE_STATUS      4'b0010
`define kAUX_COMM_I2C_WRITE_STATUS_MOT  4'b0110

// ----------------------------------------------
//  Commands from the request receiver
//  to the I2C controller
// ----------------------------------------------
`define kAUX_I2C_COMMAND_IDLE          3'h0
`define kAUX_I2C_COMMAND_WRITE         3'h1
`define kAUX_I2C_COMMAND_READ          3'h2
`define kAUX_I2C_COMMAND_READ_ADDRESS  3'h3
`define kAUX_I2C_COMMAND_WRITE_ADDRESS 3'h4
`define kAUX_I2C_COMMAND_LENGTH        3'h5
`define kAUX_I2C_COMMAND_WRITE_STATUS  3'h6

// ----------------------------------------------
//  Commands from the request receiver
//  to the reply transmitter
// ----------------------------------------------
`define kAUX_REPLY_COMMAND_IDLE                     3'b000
`define kAUX_REPLY_COMMAND_WRITE                    3'b001
`define kAUX_REPLY_COMMAND_READ                     3'b010

`define kAUX_I2C_REPLY_COMMAND_IDLE                 3'b000
`define kAUX_I2C_REPLY_COMMAND_DEFER                3'b001
`define kAUX_I2C_REPLY_COMMAND_NACK                 3'b010
`define kAUX_I2C_REPLY_COMMAND_READ_ACK             3'b011
`define kAUX_I2C_REPLY_COMMAND_WRITE_PARTIAL_ACK    3'b100
`define kAUX_I2C_REPLY_COMMAND_WRITE_ACK            3'b101

// ----------------------------------------------
// Receiver Configuration Registers
// ----------------------------------------------
`define kDPORT_RX_CFG_REG_COUNT    58
`define tDPORT_RX_CFG_REGS         [723:0]

`define kCFG_RX_CLEAR_VID_TIMING_OVERFLOW 723         // 15*32 +: 1
`define kCFG_RX_CLEAR_VID_UNPACKOVERFLOW  722         // 15*32 +: 1
`define kCFG_RX_INTERRUPT_MASK_2        721:712
`define kCFG_BRANCH_IEEE_OUI_2          711:704
`define kCFG_BRANCH_IEEE_OUI_1          703:672
`define kCFG_BRANCH_IEEE_OUI_0          671:640
`define kCFG_DOWNSTREAMPORT_COUNT       639:636
`define kCFG_DOWN_STRM_PORT3_B3         635:628
`define kCFG_DOWN_STRM_PORT3_B2         627:620
`define kCFG_DOWN_STRM_PORT3_B1         619:612
`define kCFG_DOWN_STRM_PORT3_B0         611:604
`define kCFG_DOWN_STRM_PORT2_B3         603:596
`define kCFG_DOWN_STRM_PORT2_B2         595:588
`define kCFG_DOWN_STRM_PORT2_B1         587:580
`define kCFG_DOWN_STRM_PORT2_B0         579:572
`define kCFG_DOWN_STRM_PORT1_B3         571:564
`define kCFG_DOWN_STRM_PORT1_B2         563:556
`define kCFG_DOWN_STRM_PORT1_B1         555:548
`define kCFG_DOWN_STRM_PORT1_B0         547:540
`define kCFG_DOWN_STRM_PORT0_B3         539:532
`define kCFG_DOWN_STRM_PORT0_B2         531:524
`define kCFG_DOWN_STRM_PORT0_B1         523:516
`define kCFG_DOWN_STRM_PORT0_B0         515:508
`define kCFG_DOWNSTREAMPORT_PRESENT     507:500
`define kCFG_RX_OVERRIDE_LINKSTATUS_UPDATED 499
`define kCFG_RX_OVERRIDE_TP23               498
`define kCFG_RX_OVERRIDE_TP1                497
`define kCFG_RX_OVERRIDE_TP23_SCORE     496:484
`define kCFG_RX_OVERRIDE_TP1_SCORE      483:469
`define kCFG_RX_HDCP_WR_DPCD_DBG_COUNT  468:463
`define kCFG_RX_HDCP_CP_IRQ                 462
`define kCFG_RX_CP_ENABLE                   461  
`define kCFG_PREEMP_TABLE               460:451
`define kCFG_FIXED_PREEMP               450:449
`define kCFG_PREEMP_SWEEP_TYPE          448:447
`define kCFG_FIXED_VSWING               446:445
`define kCFG_VSWING_SWEEP_CNT           444:442
`define kCFG_TRAINING_VS_SWEEP          441:440
`define kCFG_SINK_COUNT_SET             439:433    
`define kCFG_RX_AUX_SOFT_RESET              432  
`define kCFG_RX_AUX_SIGNAL_WIDTH_FILTER 431:424   
`define kTRAINING_AUX_RD_INTERVAL       423:416
`define kCFG_RX_AUDIO_CONTROL_STREAM4       415
`define kCFG_RX_AUDIO_CONTROL_STREAM3       414
`define kCFG_RX_AUDIO_CONTROL_STREAM2       413
`define kCFG_FI2C_DIVIDER               412:405
`define kCFG_GUID_SET_VALUE                 404
`define kCFG_GUID_W3                    403:372
`define kCFG_GUID_W2                    371:340
`define kCFG_GUID_W1                    339:308
`define kCFG_GUID_W0                    307:276
`define kCFG_RX_INTERRUPT_MASK_1        275:258  // 46*32 +: 18
`define kCFG_RX_HOST_INTERRUPT_LENGTH   257:242  // 45*32 +: 16
`define kCFG_DOWN_REP_MSG_RDY               241  // 44*32 +: 1
`define kCFG_MST_CAP                        240  // 43*32 +: 1
`define kCFG_RX_SEC_EXT_PKT_RD              239  // 42*32 +: 1
`define kCFG_LINK_QUAL_LANE3_SET        238:236  // 41*32 +: 3
`define kCFG_LINK_QUAL_LANE2_SET        235:233  // 40*32 +: 3
`define kCFG_LINK_QUAL_LANE1_SET        232:230  // 39*32 +: 3
`define kCFG_LINK_QUAL_LANE0_SET        229:227  // 38*32 +: 3
`define kCFG_DOWNSPREAD_CTRL_SET            226  // 37*32 +: 1
`define kCFG_RX_SEC_INFO_PKT_RD             225  // 36*32 +: 1
`define kCFG_RX_AUDIO_CONTROL               224  // 35*32 +: 1
`define kCFG_TRAINING_PATTERN_SET       223:216  // 34*32 +: 8
`define kCFG_TRAINING_LANE0_SET         215:208  // 33*32 +: 8
`define kCFG_TRAINING_LANE1_SET         207:200  // 32*32 +: 8
`define kCFG_TRAINING_LANE2_SET         199:192  // 31*32 +: 8
`define kCFG_TRAINING_LANE3_SET         191:184  // 30*32 +: 8
`define kCFG_LANE_COUNT_SET             183:176  // 29*32 +: 8
`define kCFG_LINK_BW_SET                175:168  // 28*32 +: 8
`define kCFG_RX_VIDEO_SOFT_RESET            167  // 27*32 +: 1
`define kCFG_RX_DEFER_LONG_I2C          166:165  // 26*32 +: 2
`define kCFG_RX_HDCP_KM_UPDATE          164:164  // 25*32 +: 1
`define kCFG_RX_HDCP_KM_UPPER           163:140  // 24*32 +:24
`define kCFG_RX_HDCP_KM_LOWER           139:108  // 23*32 +:32
`define kCFG_RX_HDCP_BKSV_UPPER         107:100  // 22*32 +: 8
`define kCFG_RX_HDCP_BKSV_LOWER          99:68   // 21*32 +:32
`define kCFG_RX_MIN_PRE_EMPHASIS         67:66   // 20*32 +: 2
`define kCFG_RX_MIN_VOLTAGE_SWING        65:64   // 19*32 +: 2
`define kCFG_RX_DMA_HSYNC                63:48   // 18*32 +:16
`define kCFG_RX_FORCE_DUAL_PIXEL            47   // 17*32 +: 1
`define kCFG_RX_DMA_MODE_ENABLE             46   // 16*32 +: 1
`define kCFG_RX_CLEAR_VID_OVERFLOW          45   // 15*32 +: 1
`define kCFG_RX_USE_FILTERED_MSA            44   // 14*32 +: 1
`define kCFG_RX_INTERRUPT_MASK           43:29   // 13*32 +:10
`define kCFG_RX_CLEAR_INTERRUPTS            28   // 12*32 +: 1
`define kCFG_RX_USER_PIXEL_WIDTH         27:25   // 11*32 +: 3
`define kCFG_DTG_ENABLE                     24   // 10*32 +: 1
`define kCFG_RX_HOST_INTERRUPT              23   //  9*32 +: 1
`define kCFG_LINK_ENABLE                    22   //  8*32 +: 1
`define kCFG_CLEAR_REQUEST_ERROR_COUNT      21   //  7*32 +: 1
`define kCFG_RX_AUX_CLOCK_DIVIDER        20:13   //  6*32 +: 8
`define kCFG_AUDIO_UNSUPPORTED              12   //  5*32 +: 1
`define kCFG_VIDEO_UNSUPPORTED              11   //  4*32 +: 1
`define kCFG_REMOTE_COMMAND_NEW             10   //  3*32 +: 1
`define kCFG_REMOTE_COMMAND               9:2    //  2*32 +: 8
`define kCFG_LOCAL_EDID_AUDIO               1    //  1*32 +: 1
`define kCFG_LOCAL_EDID_VIDEO               0    //  0*32 +: 1

`define kCFG_RX_REGS_RESET{\
/* kCFG_RX_INTERRUPT_MASK_2*/10'h3FF,\
/*kCFG_BRANCH_IEEE_OUI_2*/8'd0,\
/*kCFG_BRANCH_IEEE_OUI_1*/32'd0,\
/*kCFG_BRANCH_IEEE_OUI_0*/32'd0,\
/* kCFG_DOWNSTREAMPORT_COUNT*/4'd0,\
/* kCFG_DOWN_STRM_PORT3_B3         */  8'd0,    \
/* kCFG_DOWN_STRM_PORT3_B2         */  8'd0,    \
/* kCFG_DOWN_STRM_PORT3_B1         */  8'd0,    \
/* kCFG_DOWN_STRM_PORT3_B0         */  8'd0,    \
/* kCFG_DOWN_STRM_PORT2_B3         */  8'd0,    \
/* kCFG_DOWN_STRM_PORT2_B2         */  8'd0,    \
/* kCFG_DOWN_STRM_PORT2_B1         */  8'd0,    \
/* kCFG_DOWN_STRM_PORT2_B0         */  8'd0,    \
/* kCFG_DOWN_STRM_PORT1_B3         */  8'd0,    \
/* kCFG_DOWN_STRM_PORT1_B2         */  8'd0,    \
/* kCFG_DOWN_STRM_PORT1_B1         */  8'd0,    \
/* kCFG_DOWN_STRM_PORT1_B0         */  8'd0,    \
/* kCFG_DOWN_STRM_PORT0_B3         */  8'd0,    \
/* kCFG_DOWN_STRM_PORT0_B2         */  8'd0,    \
/* kCFG_DOWN_STRM_PORT0_B1         */  8'd0,    \
/* kCFG_DOWN_STRM_PORT0_B0         */  8'd0,    \
/* kCFG_DOWNSTREAMPORT_PRESENT     */  8'd0,    \
/* kCFG_RX_OVERRIDE_LINKSTATUS_UPDATED */ 1'b0,   \
/* kCFG_RX_OVERRIDE_TP23           */  1'b0,      \
/* kCFG_RX_OVERRIDE_TP1            */  1'b0,      \
/* kCFG_RX_OVERRIDE_TP23_SCORE     */  13'd0,     \
/* kCFG_RX_OVERRIDE_TP1_SCORE      */  15'd0,     \
/* kCFG_RX_HDCP_WR_DPCD_DBG_COUNT  */  6'd0,      \
/* kCFG_RX_HDCP_CP_IRQ             */  1'b0,      \
/* kCFG_RX_CP_ENABLE               */  1'h0,      \
/* kCFG_PREEMP_TABLE               */  10'b0,     \
/* kCFG_FIXED_PREEMP               */  2'b0,      \
/* kCFG_PREEMP_SWEEP_TYPE          */  2'b0,      \
/* kCFG_FIXED_VSWING               */  2'b0,      \
/* kCFG_VSWING_SWEEP_CNT           */  3'b0,      \
/* kCFG_TRAINING_VS_SWEEP          */  2'b0,      \
/* kCFG_SINK_COUNT_SET             */  7'h01,     \
/* kCFG_RX_AUX_SOFT_RESET          */  1'h0,      \
/* kCFG_RX_AUX_SIGNAL_WIDTH_FILTER */  8'h0,      \
/* kTRAINING_AUX_RD_INTERVAL      */   8'd0,          \
/* kCFG_RX_AUDIO_CONTROL_STREAM4  */   1'b0,          \
/* kCFG_RX_AUDIO_CONTROL_STREAM3  */   1'b0,          \
/* kCFG_RX_AUDIO_CONTROL_STREAM2  */   1'b0,          \
/* kCFG_FI2C_DIVIDER              */   8'd0,          \
/* kCFG_GUID_SET_VALUE            */   1'd0,          \
/* kCFG_GUID_W3                   */  32'd0,          \
/* kCFG_GUID_W2                   */  32'd0,          \
/* kCFG_GUID_W1                   */  32'd0,          \
/* kCFG_GUID_W0                   */  32'd0,          \
/* kCFG_RX_INTERRUPT_MASK_1       */  18'h3FFFF,      \
/* kCFG_RX_HOST_INTERRUPT_LENGTH  */  16'd0,          \
/* kCFG_DOWN_REP_MSG_RDY          */  1'h0,           \
/* kCFG_MST_CAP                   */  1'h0,           \
/* kCFG_RX_SEC_EXT_PKT_RD         */  1'h0,           \
/* kCFG_LINK_QUAL_LANE3_SET       */  3'h0,           \
/* kCFG_LINK_QUAL_LANE2_SET       */  3'h0,           \
/* kCFG_LINK_QUAL_LANE1_SET       */  3'h0,           \
/* kCFG_LINK_QUAL_LANE0_SET       */  3'h0,           \
/* kCFG_DOWNSPREAD_CTRL_SET       */  1'h0,           \
/* kCFG_RX_SEC_INFO_PKT_RD        */  1'h0,           \
/* kCFG_RX_AUDIO_CONTROL          */  1'h0,           \
/* kCFG_TRAINING_PATTERN_SET      */  8'h0,           \
/* kCFG_TRAINING_LANE0_SET        */  8'h0,           \
/* kCFG_TRAINING_LANE1_SET        */  8'h0,           \
/* kCFG_TRAINING_LANE2_SET        */  8'h0,           \
/* kCFG_TRAINING_LANE3_SET        */  8'h0,           \
/* kCFG_LANE_COUNT_SET            */  8'h0,           \
/* kCFG_LINK_BW_SET               */  8'h0,           \
/* kCFG_RX_VIDEO_SOFT_RESET       */  1'h0,           \
/* kCFG_RX_DEFER_LONG_I2C         */  2'h0,           \
/* kCFG_RX_HDCP_KM_UPDATE         */  1'h0,           \
/* kCFG_RX_HDCP_KM_UPPER          */ 24'h0,           \
/* kCFG_RX_HDCP_KM_LOWER          */ 32'h0,           \
/* kCFG_RX_HDCP_BKSV_UPPER        */  8'h0,           \
/* kCFG_RX_HDCP_BKSV_LOWER        */ 32'h0,           \
/* kCFG_RX_MIN_PRE_EMPHASIS       */  2'h0,           \
/* kCFG_RX_MIN_VOLTAGE_SWING      */  2'h0,           \
/* kCFG_RX_DMA_HSYNC              */ 16'h0f0f,         \
/* kCFG_RX_FORCE_DUAL_PIXEL       */  1'h0,           \
/* kCFG_RX_DMA_MODE_ENABLE        */  1'h0,           \
/* kCFG_RX_CLEAR_VID_OVERFLOW     */  1'h0,           \
/* kCFG_RX_USE_FILTERED_MSA       */  1'h0,           \
/* kCFG_RX_INTERRUPT_MASK         */ 15'h7fff,          \
/* kCFG_RX_CLEAR_INTERRUPTS       */  1'h0,           \
/* kCFG_RX_USER_PIXEL_WIDTH       */  3'h0,           \
/* kCFG_DTG_ENABLE                */  1'h0,           \
/* kCFG_RX_HOST_INTERRUPT         */  1'h0,           \
/* kCFG_LINK_ENABLE               */  1'h0,           \
/* kCFG_CLEAR_REQUEST_ERROR_COUNT */  1'h0,           \
/* kCFG_RX_AUX_CLOCK_DIVIDER      */  8'h0,           \
/* kCFG_AUDIO_UNSUPPORTED         */  1'h0,           \
/* kCFG_VIDEO_UNSUPPORTED         */  1'h0,           \
/* kCFG_REMOTE_COMMAND_NEW        */  1'h0,           \
/* kCFG_REMOTE_COMMAND            */  8'h0,           \
/* kCFG_LOCAL_EDID_AUDIO          */  1'h0,           \
/* kCFG_LOCAL_EDID_VIDEO          */  1'h0            \
}

// ----------------------------------------------
// synchronizer stage            
// ----------------------------------------------
`define RX_SYNC_STAGE_LINK 4
`define RX_SYNC_STAGE_AXI  2
`define RX_SYNC_STAGE_AUD  2
`define RX_SYNC_STAGE_VID  3

// ----------------------------------------------
// Lane status registers
// ----------------------------------------------
`define kDPORT_RX_LANE_REG_COUNT   10
`define tDPORT_RX_LANE_STATUS      [25:0]
`define  kLANE_STATUS_TRAINING_DONE                 25  // 9*8+: 1
`define  kLANE_STATUS_TRAINING_LOST                 24  // 9*8+: 1
`define  kLANE_STATUS_SYMBOL_ERROR_COUNT_LOWER   23:16  // 8*8+: 8
`define  kLANE_STATUS_SYMBOL_ERROR_COUNT_UPPER   15:9   // 7*8+: 7
`define  kLANE_STATUS_SYMBOL_ERROR_COUNT_UPDATE     8   // 6*8+: 1
`define  kLANE_STATUS_ADJUST_EMPHASIS            7 :6   // 5*8+: 2
`define  kLANE_STATUS_ADJUST_VOLTAGE             5 :4   // 4*8+: 2
`define  kLANE_STATUS_SYMBOL_LOCKED                 3   // 3*8+: 1
`define  kLANE_STATUS_CHANNEL_EQ_DONE               2   // 2*8+: 1
`define  kLANE_STATUS_CR_DONE                       1   // 1*8+: 1
`define  kLANE_STATUS_UPDATED                       0   // 0*8+: 1

`define kDPORT_RX_LANE_STATUS_RESET {                    \
/* kLANE_STATUS_TRAINING_DONE             */ 1'h 0,    \
/* kLANE_STATUS_TRAINING_LOST             */ 1'h 0,    \
/* kLANE_STATUS_SYMBOL_ERROR_COUNT_LOWER  */ 8'h 0,    \
/* kLANE_STATUS_SYMBOL_ERROR_COUNT_UPPER  */ 7'h 0,    \
/* kLANE_STATUS_SYMBOL_ERROR_COUNT_UPDATE */ 1'h 0,    \
/* kLANE_STATUS_ADJUST_EMPHASIS           */ 2'h 0,    \
/* kLANE_STATUS_ADJUST_VOLTAGE            */ 2'h 0,    \
/* kLANE_STATUS_SYMBOL_LOCKED             */ 1'h 0,    \
/* kLANE_STATUS_CHANNEL_EQ_DONE           */ 1'h 0,    \
/* kLANE_STATUS_CR_DONE                   */ 1'h 0,    \
/* kLANE_STATUS_UPDATED                   */ 1'h 0     \
}

// ----------------------------------------------
//  User data status
// ----------------------------------------------
`define kDPORT_RX_USER_STATUS_COUNT   2
`define tDPORT_RX_USER_STATUS      [3:0]
`define kRX_VIDTIMING_FIFO_OVERFLOW             3  
`define kRX_VIDUNPACK_FIFO_OVERFLOW             2  
`define kRX_USER_STATUS_VSYNC                   1
`define kRX_USER_STATUS_FIFO_OVERFLOW           0

// ----------------------------------------------
//  Video timing parameters
// ----------------------------------------------
`define kDPORT_RX_VIDMODE_REG_COUNT 18
`define kDPORT_RX_VIDMODE_TIMING_REG_COUNT 12

`define tDPORT_RX_VIDMODE_REGS      [204:0]

`define  kCFG_RX_FSFE_UPDATE        204:203  // 17*16+:  1
`define  kCFG_RX_VIDMODE_UPDATE         202  // 17*16+:  1
`define  kCFG_RX_VIDMODE_NVID_MSW   201:194  // 16*16+:  8
`define  kCFG_RX_VIDMODE_NVID_LSW   193:178  // 15*16+: 16
`define  kCFG_RX_VIDMODE_MVID_MSW   177:170  // 14*16+:  8
`define  kCFG_RX_VIDMODE_MVID_LSW   169:154  // 13*16+: 16
`define  kCFG_RX_VIDMODE_VBID       153:144  // 12*16+: 10
`define  kCFG_RX_VIDMODE_MISC1      143:136  // 11*16+:  8
`define  kCFG_RX_VIDMODE_MISC0      135:128  // 10*16+:  8
`define  kCFG_RX_VIDMODE_HRES       127:112  //  9*16+: 16
`define  kCFG_RX_VIDMODE_HSPOL          111  //  8*16+:  1
`define  kCFG_RX_VIDMODE_HSWIDTH    110:96   //  7*16+: 15
`define  kCFG_RX_VIDMODE_HSTART      95:80   //  6*16+: 16
`define  kCFG_RX_VIDMODE_HTOTAL      79:64   //  5*16+: 16
`define  kCFG_RX_VIDMODE_VRES        63:48   //  4*16+: 16
`define  kCFG_RX_VIDMODE_VSPOL          47   //  3*16+:  1
`define  kCFG_RX_VIDMODE_VSWIDTH     46:32   //  2*16+: 15
`define  kCFG_RX_VIDMODE_VSTART      31:16   //  1*16+: 16
`define  kCFG_RX_VIDMODE_VTOTAL      15:0    //  0*16+: 16

`define kDPORT_RX_VIDMODE_REGS_RESET    { \
/* kCFG_RX_VIDMODE_UPDATE  */  1'h0,  \
/* kCFG_RX_VIDMODE_NVID_MSW*/  8'h0,  \
/* kCFG_RX_VIDMODE_NVID_LSW*/ 16'h0,  \
/* kCFG_RX_VIDMODE_MVID_MSW*/  8'h0,  \
/* kCFG_RX_VIDMODE_MVID_LSW*/ 16'h0,  \
/* kCFG_RX_VIDMODE_VBID    */ 10'h19, \
/* kCFG_RX_VIDMODE_MISC1   */  8'h0,  \
/* kCFG_RX_VIDMODE_MISC0   */  8'h0,  \
/* kCFG_RX_VIDMODE_HRES    */ 16'h0,  \
/* kCFG_RX_VIDMODE_HSPOL   */  1'h0,  \
/* kCFG_RX_VIDMODE_HSWIDTH */ 15'h0,  \
/* kCFG_RX_VIDMODE_HSTART  */ 16'h0,  \
/* kCFG_RX_VIDMODE_HTOTAL  */ 16'h0,  \
/* kCFG_RX_VIDMODE_VRES    */ 16'h0,  \
/* kCFG_RX_VIDMODE_VSPOL   */  1'h0,  \
/* kCFG_RX_VIDMODE_VSWIDTH */ 15'h0,  \
/* kCFG_RX_VIDMODE_VSTART  */ 16'h0,  \
/* kCFG_RX_VIDMODE_VTOTAL  */ 16'h0 }

// ----------------------------------------------
//  Link training status registers
// ----------------------------------------------
`define kDPORT_RX_STATUS_REG_COUNT 7
`define tDPORT_RX_TRAINING_STATUS [21:0]

`define kCFG_RX_TRAINING_SE_COUNT_UPDATE      21  // 6*8+:1
`define kCFG_RX_TRAINING_SE_COUNT_LSB      20:13  // 5*8+:8
`define kCFG_RX_TRAINING_SE_COUNT_MSB      12:6   // 4*8+:7
`define kCFG_RX_TRAINING_ADJUST_EMPHASIS    5:4   // 3*8+:2
`define kCFG_RX_TRAINING_ADJUST_VOLTAGE     3:2   // 2*8+:2
`define kCFG_RX_TRAINING_CHANNEL_EQ_DONE      1   // 1*8+:1
`define kCFG_RX_TRAINING_CR_DONE              0   // 0*8+:1

// ----------------------------------------------
// Audio registers
// ----------------------------------------------
`define kDPORT_RX_AUDIO_REG_COUNT 12
`define tDPORT_RX_AUDIO_REGS [57:0]

`define  kCFG_RX_AUDIO_EXT_PKT_RXD       57 // 11*32+:  1
`define  kCFG_RX_AUDIO_RSD_STATUS    56: 49 // 10*32+:  8
`define  kCFG_RX_AUDIO_INFO_RXD          48 //  9*32+:  1
`define  kCFG_RX_AUDIO_MAUD          47:24  //  1*32+: 24
`define  kCFG_RX_AUDIO_NAUD          23:0   //  0*32+: 24

`define kCFG_RX_AUDIO_REGS_RESET    { \
/* kCFG_RX_AUDIO_EXT_PKT_RXD  */  1'h 0,  \
/* kCFG_RX_AUDIO_RSD_STATUS   */  8'h 0,  \
/* kCFG_RX_AUDIO_INFO_RXD     */  1'h 0,  \
/* kCFG_RX_AUDIO_MAUD         */ 24'h 0,  \
/* kCFG_RX_AUDIO_NAUD         */ 24'h 0   \
}

// ----------------------------------------------
// PHY config and status
// ----------------------------------------------
`define tDPORT_RX_PHY_STATUS  [57:0]
`define kCFG_RX_PHY_LANE0_ELEC_IDLE       12
`define kCFG_RX_PHY_LANE1_ELEC_IDLE       13
`define kCFG_RX_PHY_LANE2_ELEC_IDLE       14
`define kCFG_RX_PHY_LANE3_ELEC_IDLE       15
`define kCFG_RX_PHY_LANE0_ALIGNED         16
`define kCFG_RX_PHY_LANE1_ALIGNED         17
`define kCFG_RX_PHY_LANE2_ALIGNED         18
`define kCFG_RX_PHY_LANE3_ALIGNED         19
`define kCFG_RX_PHY_LANE0_SYMBOL_LOCKED   20
`define kCFG_RX_PHY_LANE1_SYMBOL_LOCKED   21
`define kCFG_RX_PHY_LANE2_SYMBOL_LOCKED   22
`define kCFG_RX_PHY_LANE3_SYMBOL_LOCKED   23
`define kCFG_RX_PHY_STATUS_DRP_LOCKED     40
`define kCFG_RX_PHY_STATUS_DRP_READY      41
`define kCFG_RX_PHY_STATUS_DRP_RDATA   57:42

`define tDPORT_RX_PHY_CONFIG  [78:0]
`define kCFG_RX_PHY_RXPOLARITY_LANE3       78 
`define kCFG_RX_PHY_RXPOLARITY_LANE2       77 
`define kCFG_RX_PHY_RXPOLARITY_LANE1       76 
`define kCFG_RX_PHY_RXPOLARITY_LANE0       75 
`define kCFG_RX_PHY_RXPOLARITY_LANE_CTRL   74
`define kCFG_RX_PHY_ENABLE_TP1_RESET       73
`define kCFG_RX_PHY_ENABLE_ITR_RESET       72
`define kCFG_RX_PHY_ENABLE_BWCH_RESET      71
`define kCFG_RX_PHY_DRP_ACCESS             70
`define kCFG_RX_PHY_DRP_WDATA          69: 54
`define kCFG_RX_PHY_DRP_ADDR           53: 46
`define kCFG_RX_PHY_DRP_WRITE              45
`define kCFG_RX_PHY_CDRHOLD                44
`define kCFG_RX_PHY_RXLPMHFOVERDEN         43
`define kCFG_RX_PHY_RXLPMLFHOLD            42
`define kCFG_RX_PHY_RXLPMHFHOLD            41
`define kCFG_RX_PHY_PRBSCNTRESET           40
`define kCFG_RX_PHY_EYESCANTRIGGER         39
`define kCFG_RX_PHY_EYESCANRESET           38   
`define kCFG_RX_PHY_LOOPBACK               37:35
`define kCFG_RX_PHY_RXPOLARITY             34
`define kCFG_RX_PHY_DFE_LPM_RST            33
`define kCFG_RX_PHY_PCS_RST                32
`define kCFG_RX_PHY_BUF_RST                31
`define kCFG_RX_PHY_PMA_RST                30
`define kCFG_RX_PHY_USE_DFE                29
`define kCFG_RX_PHY_TDLOCK_VALUE           28:9
`define kCFG_RX_PHY_LANE_COUNT_RESET       8
`define kCFG_RX_PHY_POWER_DOWN             7:4
`define kCFG_RX_PHY_ENABLE_ELEC_IDLE       3
`define kCFG_RX_PHY_RESET_2                2
`define kCFG_RX_PHY_CLOCK_SELECT           1
`define kCFG_RX_PHY_RESET                  0

`define kCFG_RX_PHY_CONFIG_RESET  {       \
/* kCFG_RX_PHY_RXPOLARITY_LANE3    */     1'b0, \
/* kCFG_RX_PHY_RXPOLARITY_LANE2    */     1'b0, \
/* kCFG_RX_PHY_RXPOLARITY_LANE1    */     1'b0, \
/* kCFG_RX_PHY_RXPOLARITY_LANE0    */     1'b0, \
/* kCFG_RX_PHY_RXPOLARITY_LANE_CTRL    */     1'b0, \
/* kCFG_RX_PHY_ENABLE_TP1_RESET*/   1'b0, \
/* kCFG_RX_PHY_ENABLE_ITR_RESET*/   1'b0, \
/* kCFG_RX_PHY_ENABLE_BWCH_RESET*/  1'b0, \
/* kCFG_RX_PHY_CDRHOLD*/            1'b0, \
/* kCFG_RX_PHY_RXLPMHFOVERDEN*/     1'b0, \
/* kCFG_RX_PHY_RXLPMLFHOLD*/        1'b0, \
/* kCFG_RX_PHY_RXLPMHFHOLD*/        1'b0, \
/* kCFG_RX_PHY_PRBSCNTRESET*/       1'b0, \
/* kCFG_RX_PHY_EYESCANTRIGGER*/     1'b0, \
/* kCFG_RX_PHY_EYESCANRESET  */     1'b0, \
/* kCFG_RX_PHY_LOOPBACK      */     3'b000, \
/* kCFG_RX_PHY_RXPOLARITY    */     1'b0, \
/* kCFG_RX_PHY_DFE_LPM_RST   */     1'b0, \
/* kCFG_RX_PHY_PCS_RST */           1'b0, \
/* kCFG_RX_PHY_BUF_RST */           1'b0, \
/* kCFG_RX_PHY_PMA_RST */           1'b0, \
/* kCFG_RX_PHY_USE_DFE */           1'b1, \
/* kCFG_RX_PHY_TDLOCK_VALUE */      20'h11364, \
/* kCFG_RX_PHY_LANE_COUNT_RESET */  1'b0, \
/* kCFG_RX_PHY_POWER_DOWN */        4'b0, \
/* kCFG_RX_PHY_ENABLE_ELEC_IDLE */  1'b0, \
/* kCFG_RX_PHY_RESET_2 */           1'b1, \
/* kCFG_RX_PHY_CLOCK_SELECT */      1'b0, \
/* kCFG_RX_PHY_RESET */             1'b1  \
}

// ----------------------------------------------
// HDCP status registers
// ----------------------------------------------
`define kDPORT_RX_HDCP_STATUS_COUNT  5
`define tDPORT_RX_HDCP_STATUS        [18:0]

`define kDPORT_RX_HDCP_CP_IRQ                     18  //4*8 +: 1
`define kDPORT_RX_HDCP_R0_PRIME_BYTE1          17:10  //3*8 +: 8
`define kDPORT_RX_HDCP_R0_PRIME_BYTE0           9:2   //2*8 +: 8
`define kDPORT_RX_HDCP_R0_PRIME_AVAILABLE         1   //1*8 +: 1
`define kDPORT_RX_HDCP_LINK_INTEGRITY_FAILURE     0   //0*8 +: 1

`define kDPORT_RX_HDCP_STATUS_RESET   {19{1'b0}}

// ----------------------------------------------
// HDCP registers
// ----------------------------------------------
`define tDPORT_RX_HDCP_CFG           [11:0]

`define kDPORT_RX_HDCP_WR_DPCD_DBG_COUNT  11:6  
`define kDPORT_RX_HDCP_RD_DPCD_BINFO         5  //5*8 +: 1
`define kDPORT_RX_HDCP_RD_DPCD_R0            4  //4*8 +: 1
`define kDPORT_RX_HDCP_WR_DPCD_DBG           3  //3*8 +: 1
`define kDPORT_RX_HDCP_WR_DPCD_AINFO         2  //2*8 +: 1
`define kDPORT_RX_HDCP_WR_DPCD_AN            1  //1*8 +: 1
`define kDPORT_RX_HDCP_WR_DPCD_AKSV          0  //0*8 +: 1

`define kDPORT_RX_HDCP_CFG_RESET   {12{1'b0}}

// ----------------------------------------------
// Request controller status
// ----------------------------------------------
`define tDPORT_RX_REQUEST_STATUS    [50:0]
`define kDPORT_RX_REQUEST_CLOCK_WIDTH    50:41 // 6*32 +: 10
`define kDPORT_RX_REQUEST_LENGTH         40:37 // 5*32 +: 4
`define kDPORT_RX_REQUEST_ADDRESS        36:17 // 4*32 +: 20
`define kDPORT_RX_REQUEST_COMMAND        16:13 // 3*32 +: 4
`define kDPORT_RX_REQUEST_IN_PROGRESS       12 // 2*32 +: 1
`define kDPORT_RX_REQUEST_COUNT          11:4  // 1*32 +: 8
`define kDPORT_RX_REQUEST_ERROR_COUNT     3:0  // 0*32 +: 4

// ----------------------------------------------
// Interrupt status
// ----------------------------------------------
`define tDPORT_RX_INTERRUPT_STATUS [24:0]
`define kDPORT_RX_HDCP_RD_DPCD_BINFO_INTR     24  
`define kDPORT_RX_HDCP_RD_DPCD_R0_INTR   23  
`define kDPORT_RX_HDCP_WR_DPCD_AINFO_INTR     22  
`define kDPORT_RX_HDCP_WR_DPCD_AN_INTR        21  
`define kDPORT_RX_HDCP_WR_DPCD_AKSV_INTR      20  
`define kDPORT_RX_HDCP_WR_DPCD_DBG_INTR       19  
`define kDPORT_RX_TP3_START                   18
`define kDPORT_RX_TP2_START                   17
`define kDPORT_RX_TP1_START                   16 
`define kDPORT_RX_BW_CHANGE                   15 
`define kDPORT_RX_TRAINING_DONE_INTERRUPT     14
`define kDPORT_RX_DOWN_REQUEST_BUFFER_READY   13
`define kDPORT_RX_DOWN_REPLY_BUFFER_READ      12
`define kDPORT_RX_VCPAYLOAD_DEALLOCATED       11
`define kDPORT_RX_VCPAYLOAD_ALLOCATED         10
`define kDPORT_RX_SEC_EXT_PKT_RXD             9
`define kDPORT_RX_SEC_INFO_PKT_RXD            8
`define kDPORT_RX_CP_INTERRUPT                7
`define kDPORT_RX_VIDEO_INTERRUPT             6
`define kDPORT_RX_AKSV_INTERRUPT              5
`define kDPORT_RX_LOSS_OF_TRAINING_INTERRUPT  4
`define kDPORT_RX_VBLANK_INTERRUPT            3
`define kDPORT_RX_NO_VIDEO_INTERRUPT          2
`define kDPORT_RX_POWER_INTERRUPT             1
`define kDPORT_RX_MODE_INTERRUPT              0



// ----------------------------------------------
// control character definitions
// ----------------------------------------------
`define kRX_DUMMY_CHAR          8'h00
`define kRX_CONTROL_CHAR_BS     8'hbc        // K28.5
`define kRX_CONTROL_CHAR_BE     8'hfb        // K27.7
`define kRX_CONTROL_CHAR_SS     8'h5c        // K28.2
`define kRX_CONTROL_CHAR_SE     8'hfd        // K29.7
`define kRX_CONTROL_CHAR_FS     8'hfe        // K30.7
`define kRX_CONTROL_CHAR_FE     8'hf7        // K23.7
`define kRX_CONTROL_CHAR_SR     8'h1c        // K28.0
`define kRX_CONTROL_CHAR_CP     8'h3c        // K28.1
`define kRX_CONTROL_CHAR_BF     8'h7c        // K28.3
`define kRX_CONTROL_CHAR_MST_SR 8'hBC        // K28.5

//      K-code         Single-stream Multi-stream
`define K28_0 8'h1C // SR            GP1 index 2
`define K28_1 8'h3C // CPBS/CP       GP2 CPSR
`define K28_2 8'h5C // SS            GP1 index 3
`define K28_3 8'h7C // CPSR/BF       GP1 index 4
`define K28_4 8'h9C // rsvd          GP2 rsvd
`define K28_5 8'hBC // BS            GP2 SR
`define K28_6 8'hDC // rsvd          GP1 index 5
`define K28_7 8'hFC // rsvd          GP2 rsvd
`define K23_7 8'hF7 // FE            GP1 index 0
`define K27_7 8'hFB // BE            GP1 index 1
`define K29_7 8'hFD // SE            GP1 index 6
`define K30_7 8'hFE // FS            GP1 index 7

`define SST_SR   `K28_0
`define SST_CPBS `K28_1
`define SST_SS   `K28_2
`define SST_CPSR `K28_3
`define SST_BF   `K28_3
`define SST_BS   `K28_5
`define SST_FE   `K23_7
`define SST_BE   `K27_7
`define SST_SE   `K29_7
`define SST_FS   `K30_7

`define MST_CPSR `K28_1
`define MST_SR   `K28_5

`define kRX_CONTROL_MTPH_SR    `K28_5
`define kRX_CONTROL_MTPH_RSVD1 `K28_1
`define kRX_CONTROL_MTPH_RSVD2 `K28_7
`define kRX_CONTROL_MTPH_RSVD3 `K28_4

`define kVCPAYLOD_AV_PORT0    6'b000001
`define kVCPAYLOD_AV_PORT1    6'b000010
`define kVCPAYLOD_UNALLOCATED 6'b000000

`define kSPECIAL_CONTROL_CONSTANT 8'hFF
`define C0_KCHAR `K23_7
`define C1_KCHAR `K27_7
`define C2_KCHAR `K28_0
`define C3_KCHAR `K28_2
`define C4_KCHAR `K28_3
`define C5_KCHAR `K28_6
`define C6_KCHAR `K29_7
`define C7_KCHAR `K30_7

`endif
