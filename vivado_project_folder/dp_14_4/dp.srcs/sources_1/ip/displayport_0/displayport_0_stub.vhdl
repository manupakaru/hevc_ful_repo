-- Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2014.4 (win64) Build 1071353 Tue Nov 18 18:24:04 MST 2014
-- Date        : Mon Mar 23 17:48:59 2015
-- Host        : GCLAP4 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               F:/GalaxyCores/HEVC/repo/vivado_project_folder/dp_14_4/dp.srcs/sources_1/ip/displayport_0/displayport_0_stub.vhdl
-- Design      : displayport_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z045ffg900-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity displayport_0 is
  Port ( 
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    axi_int : out STD_LOGIC;
    tx_vid_clk : in STD_LOGIC;
    tx_vid_rst : in STD_LOGIC;
    tx_vid_vsync : in STD_LOGIC;
    tx_vid_hsync : in STD_LOGIC;
    tx_vid_oddeven : in STD_LOGIC;
    tx_vid_enable : in STD_LOGIC;
    tx_vid_pixel0 : in STD_LOGIC_VECTOR ( 47 downto 0 );
    tx_vid_pixel1 : in STD_LOGIC_VECTOR ( 47 downto 0 );
    lnk_tx_lane_p : out STD_LOGIC_VECTOR ( 3 downto 0 );
    lnk_tx_lane_n : out STD_LOGIC_VECTOR ( 3 downto 0 );
    aux_tx_io_p : inout STD_LOGIC;
    aux_tx_io_n : inout STD_LOGIC;
    tx_hpd : in STD_LOGIC;
    lnk_clk : out STD_LOGIC;
    common_qpll_clk_out : out STD_LOGIC;
    common_qpll_ref_clk_out : out STD_LOGIC;
    common_qpll_lock_out : out STD_LOGIC;
    lnk_clk_ibufds_out : out STD_LOGIC;
    lnk_clk_p : in STD_LOGIC;
    lnk_clk_n : in STD_LOGIC
  );

end displayport_0;

architecture stub of displayport_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "s_axi_aclk,s_axi_aresetn,s_axi_awaddr[31:0],s_axi_awprot[2:0],s_axi_awvalid,s_axi_awready,s_axi_wdata[31:0],s_axi_wstrb[3:0],s_axi_wvalid,s_axi_wready,s_axi_bresp[1:0],s_axi_bvalid,s_axi_bready,s_axi_araddr[31:0],s_axi_arprot[2:0],s_axi_arvalid,s_axi_arready,s_axi_rdata[31:0],s_axi_rresp[1:0],s_axi_rvalid,s_axi_rready,axi_int,tx_vid_clk,tx_vid_rst,tx_vid_vsync,tx_vid_hsync,tx_vid_oddeven,tx_vid_enable,tx_vid_pixel0[47:0],tx_vid_pixel1[47:0],lnk_tx_lane_p[3:0],lnk_tx_lane_n[3:0],aux_tx_io_p,aux_tx_io_n,tx_hpd,lnk_clk,common_qpll_clk_out,common_qpll_ref_clk_out,common_qpll_lock_out,lnk_clk_ibufds_out,lnk_clk_p,lnk_clk_n";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "displayport_0_dport_wrapper,Vivado 2014.4";
begin
end;
