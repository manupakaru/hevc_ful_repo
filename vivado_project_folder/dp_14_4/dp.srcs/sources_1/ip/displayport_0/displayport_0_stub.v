// Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2014.4 (win64) Build 1071353 Tue Nov 18 18:24:04 MST 2014
// Date        : Mon Mar 23 17:48:59 2015
// Host        : GCLAP4 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               F:/GalaxyCores/HEVC/repo/vivado_project_folder/dp_14_4/dp.srcs/sources_1/ip/displayport_0/displayport_0_stub.v
// Design      : displayport_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z045ffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "displayport_0_dport_wrapper,Vivado 2014.4" *)
module displayport_0(s_axi_aclk, s_axi_aresetn, s_axi_awaddr, s_axi_awprot, s_axi_awvalid, s_axi_awready, s_axi_wdata, s_axi_wstrb, s_axi_wvalid, s_axi_wready, s_axi_bresp, s_axi_bvalid, s_axi_bready, s_axi_araddr, s_axi_arprot, s_axi_arvalid, s_axi_arready, s_axi_rdata, s_axi_rresp, s_axi_rvalid, s_axi_rready, axi_int, tx_vid_clk, tx_vid_rst, tx_vid_vsync, tx_vid_hsync, tx_vid_oddeven, tx_vid_enable, tx_vid_pixel0, tx_vid_pixel1, lnk_tx_lane_p, lnk_tx_lane_n, aux_tx_io_p, aux_tx_io_n, tx_hpd, lnk_clk, common_qpll_clk_out, common_qpll_ref_clk_out, common_qpll_lock_out, lnk_clk_ibufds_out, lnk_clk_p, lnk_clk_n)
/* synthesis syn_black_box black_box_pad_pin="s_axi_aclk,s_axi_aresetn,s_axi_awaddr[31:0],s_axi_awprot[2:0],s_axi_awvalid,s_axi_awready,s_axi_wdata[31:0],s_axi_wstrb[3:0],s_axi_wvalid,s_axi_wready,s_axi_bresp[1:0],s_axi_bvalid,s_axi_bready,s_axi_araddr[31:0],s_axi_arprot[2:0],s_axi_arvalid,s_axi_arready,s_axi_rdata[31:0],s_axi_rresp[1:0],s_axi_rvalid,s_axi_rready,axi_int,tx_vid_clk,tx_vid_rst,tx_vid_vsync,tx_vid_hsync,tx_vid_oddeven,tx_vid_enable,tx_vid_pixel0[47:0],tx_vid_pixel1[47:0],lnk_tx_lane_p[3:0],lnk_tx_lane_n[3:0],aux_tx_io_p,aux_tx_io_n,tx_hpd,lnk_clk,common_qpll_clk_out,common_qpll_ref_clk_out,common_qpll_lock_out,lnk_clk_ibufds_out,lnk_clk_p,lnk_clk_n" */;
  input s_axi_aclk;
  input s_axi_aresetn;
  input [31:0]s_axi_awaddr;
  input [2:0]s_axi_awprot;
  input s_axi_awvalid;
  output s_axi_awready;
  input [31:0]s_axi_wdata;
  input [3:0]s_axi_wstrb;
  input s_axi_wvalid;
  output s_axi_wready;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  input s_axi_bready;
  input [31:0]s_axi_araddr;
  input [2:0]s_axi_arprot;
  input s_axi_arvalid;
  output s_axi_arready;
  output [31:0]s_axi_rdata;
  output [1:0]s_axi_rresp;
  output s_axi_rvalid;
  input s_axi_rready;
  output axi_int;
  input tx_vid_clk;
  input tx_vid_rst;
  input tx_vid_vsync;
  input tx_vid_hsync;
  input tx_vid_oddeven;
  input tx_vid_enable;
  input [47:0]tx_vid_pixel0;
  input [47:0]tx_vid_pixel1;
  output [3:0]lnk_tx_lane_p;
  output [3:0]lnk_tx_lane_n;
  inout aux_tx_io_p;
  inout aux_tx_io_n;
  input tx_hpd;
  output lnk_clk;
  output common_qpll_clk_out;
  output common_qpll_ref_clk_out;
  output common_qpll_lock_out;
  output lnk_clk_ibufds_out;
  input lnk_clk_p;
  input lnk_clk_n;
endmodule
