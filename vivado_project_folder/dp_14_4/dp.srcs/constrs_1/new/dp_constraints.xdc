# (c) Copyright 2013 Xilinx, Inc. All rights reserved.
#
# This file contains confidential and proprietary information
# of Xilinx, Inc. and is protected under U.S. and
# international copyright and other intellectual property
# laws.
#
# DISCLAIMER
# This disclaimer is not a license and does not grant any
# rights to the materials distributed herewith. Except as
# otherwise provided in a valid license issued to you by
# Xilinx, and to the maximum extent permitted by applicable
# law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
# WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
# AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
# BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
# INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
# (2) Xilinx shall not be liable (whether in contract or tort,
# including negligence, or under any other theory of
# liability) for any loss or damage of any kind or nature
# related to, arising under or in connection with these
# materials, including for any direct, or any indirect,
# special, incidental, or consequential loss or damage
# (including loss of data, profits, goodwill, or any type of
# loss or damage suffered as a result of any action brought
# by a third party) even if such damage or loss was
# reasonably foreseeable or Xilinx had been advised of the
# possibility of the same.
#
# CRITICAL APPLICATIONS
# Xilinx products are not designed or intended to be fail-
# safe, or for use in any application requiring fail-safe
# performance, such as life-support or safety devices or
# systems, Class III medical devices, nuclear facilities,
# applications related to the deployment of airbags, or any
# other applications that could lead to death, personal
# injury, or severe property or environmental damage
# (individually and collectively, "Critical
# Applications"). Customer assumes the sole risk and
# liability of any use of Xilinx products in Critical
# Applications, subject only to applicable laws and
# regulations governing limitations on product liability.
#
# THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
# PART OF THIS FILE AT ALL TIMES.
#--------------------------------------------------------------------------
#
# KC705 XDC constraints for IPI Example Design
#

#-----------------------------------------------------------------
## Clock Constraints
#-----------------------------------------------------------------
set_property IOSTANDARD LVDS [get_ports clk_200_n]

set_property PACKAGE_PIN G9 [get_ports clk_200_n]
set_property IOSTANDARD LVDS [get_ports clk_200_p]

create_clock -period 5.000 -name CLK_IN1_Pin [get_ports clk_200_p]

# MGT refclk for DP FMC
set_property PACKAGE_PIN AC8 [get_ports lnk_clk_p]
create_clock -period 7.407 -name lnk_clk_p [get_ports lnk_clk_p]


#set_property IOSTANDARD LVDS_25 [get_ports ext_aud_clk_n]
#set_property PACKAGE_PIN AG14 [get_ports ext_aud_clk_n]
#set_property IOSTANDARD LVDS_25 [get_ports ext_aud_clk_p]
#create_clock -period 40.000 -name ext_aud_clk_p [get_ports ext_aud_clk_p]

#-----------------------------------------------------------------
## Reset Constraints bank 33
#-----------------------------------------------------------------
#set_property PACKAGE_PIN A8 [get_ports reset]
#set_property IOSTANDARD LVCMOS15 [get_ports reset]

#-----------------------------------------------------------------
## UART Constraints
#-----------------------------------------------------------------
#UART TX RX connected to FMC HPC header LA06P - TX
#UART TX RX connected to FMC HPC header LA06N - RX
#set_property PACKAGE_PIN AH22 [get_ports RS232_Uart_1_RX]
#set_property IOSTANDARD LVCMOS25 [get_ports RS232_Uart_1_RX]

#set_property PACKAGE_PIN AG22 [get_ports RS232_Uart_1_TX]
#set_property IOSTANDARD LVCMOS25 [get_ports RS232_Uart_1_TX]

#-----------------------------------------------------------------
## KC705 IIC Constraints
#-----------------------------------------------------------------
set_property PACKAGE_PIN AJ14 [get_ports kc705_iic_Scl]
set_property IOSTANDARD LVCMOS25 [get_ports kc705_iic_Scl]
set_property SLEW SLOW [get_ports kc705_iic_Scl]
set_property DRIVE 8 [get_ports kc705_iic_Scl]
set_property PACKAGE_PIN AJ18 [get_ports kc705_iic_Sda]
set_property IOSTANDARD LVCMOS25 [get_ports kc705_iic_Sda]
set_property SLEW SLOW [get_ports kc705_iic_Sda]
set_property DRIVE 8 [get_ports kc705_iic_Sda]


#-----------------------------------------------------------------
## DisplayPort Constraints
#-----------------------------------------------------------------
create_clock -period 20.000 -name dp_axi_aclk [get_nets *clk_int_50]
create_clock -period 7.407 -name dp_tx_vid_clk [get_nets dp_tx_vid_clk]
create_generated_clock -name lnk_clk -source [get_ports lnk_clk_p] -divide_by 4 -multiply_by 5 [get_pins -of_objects [get_cells -hier *ref_clk_out_bufg] -filter {direction == out}]
create_clock -period 3.703 -name dp_lnk_clk [get_nets lnk_clk]
set_false_path -from [get_clocks *axi_aclk] -to [get_clocks *vid_clk]
set_false_path -from [get_clocks *vid_clk] -to [get_clocks *axi_aclk]
set_false_path -from [get_clocks *axi_aclk] -to [get_clocks *lnk_clk]
set_false_path -from [get_clocks *lnk_clk] -to [get_clocks *axi_aclk]
set_false_path -from [get_clocks *vid_clk] -to [get_clocks *lnk_clk]
set_false_path -from [get_clocks *lnk_clk] -to [get_clocks *vid_clk]

#FMC_HPC_DP0_C2M_P,N
# set_property LOC GTXE2_CHANNEL_X0Y12 [get_cells displayport_v4_0_tx_inst/inst/dport_tx_phy_inst/gt_wrapper_inst/gt0_gt_7_series_wrapper_4_i/gtxe2_i]
set_property PACKAGE_PIN AK10 [get_ports {lnk_tx_lane_p[0]}]
#FMC_HPC_DP1_C2M_P,N
# set_property LOC GTXE2_CHANNEL_X0Y13 [get_cells displayport_v4_0_tx_inst/inst/dport_tx_phy_inst/gt_wrapper_inst/gt1_gt_7_series_wrapper_4_i/gtxe2_i]
set_property PACKAGE_PIN AK6 [get_ports {lnk_tx_lane_p[1]}]
#FMC_HPC_DP2_C2M_P,N
# set_property LOC GTXE2_CHANNEL_X0Y14 [get_cells displayport_v4_0_tx_inst/inst/dport_tx_phy_inst/gt_wrapper_inst/gt2_gt_7_series_wrapper_4_i/gtxe2_i]
set_property PACKAGE_PIN AJ4 [get_ports {lnk_tx_lane_p[2]}]
#FMC_HPC_DP3_C2M_P,N
# set_property LOC GTXE2_CHANNEL_X0Y15 [get_cells displayport_v4_0_tx_inst/inst/dport_tx_phy_inst/gt_wrapper_inst/gt3_gt_7_series_wrapper_4_i/gtxe2_i]
set_property PACKAGE_PIN AK2 [get_ports {lnk_tx_lane_p[3]}]

# DP HPD
set_property PACKAGE_PIN AJ20 [get_ports hpd]
set_property IOSTANDARD LVCMOS25 [get_ports hpd]
set_property PULLDOWN true [get_ports hpd]

# AUX channel pins
set_property IOSTANDARD BLVDS_25 [get_ports aux_tx_io_n]
set_property IOSTANDARD BLVDS_25 [get_ports aux_tx_io_p]
set_property PACKAGE_PIN AJ23 [get_ports aux_tx_io_p]

#-------------------clocks of 135MHz (generated by clk_wiz) going to Si5324------------
set_property IOSTANDARD LVDS_25 [get_ports clk_int_135_out_p]
#set_property LOC OLOGIC_X0Y96 [get_cells clk_out_oddr]
set_property PACKAGE_PIN AE20 [get_ports clk_int_135_out_n]
set_property IOSTANDARD LVDS_25 [get_ports clk_int_135_out_n]

#set_property PACKAGE_PIN F20 [get_ports {gpo[0]}]
#PS side pin
set_property PACKAGE_PIN W21 [get_ports {gpo[0]}]

set_property IOSTANDARD LVCMOS25 [get_ports {gpo[0]}]

set_property PACKAGE_PIN W23 [get_ports {gpo[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {gpo[1]}]