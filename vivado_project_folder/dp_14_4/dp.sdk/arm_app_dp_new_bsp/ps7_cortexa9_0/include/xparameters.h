/* Definition for CPU ID */
#define XPAR_CPU_ID 0

/* Definitions for peripheral PS7_CORTEXA9_0 */
#define XPAR_PS7_CORTEXA9_0_CPU_CLK_FREQ_HZ 666666687


/******************************************************************/

/* Canonical definitions for peripheral PS7_CORTEXA9_0 */
#define XPAR_CPU_CORTEXA9_0_CPU_CLK_FREQ_HZ 666666687


/******************************************************************/


/* Definitions for interface APB_M */
#define XPAR_APB_M_BASEADDR 0x42000000
#define XPAR_APB_M_HIGHADDR 0x4200FFFF


/* Definitions for interface M05_AXI */
#define XPAR_M05_AXI_BASEADDR 0x44A00000
#define XPAR_M05_AXI_HIGHADDR 0x44A0FFFF

#include "xparameters_ps.h"

#define STDIN_BASEADDRESS 0xE0001000
#define STDOUT_BASEADDRESS 0xE0001000

/******************************************************************/

/* Definitions for driver DEVCFG */
#define XPAR_XDCFG_NUM_INSTANCES 1

/* Definitions for peripheral PS7_DEV_CFG_0 */
#define XPAR_PS7_DEV_CFG_0_DEVICE_ID 0
#define XPAR_PS7_DEV_CFG_0_BASEADDR 0xF8007000
#define XPAR_PS7_DEV_CFG_0_HIGHADDR 0xF80070FF


/******************************************************************/

/* Canonical definitions for peripheral PS7_DEV_CFG_0 */
#define XPAR_XDCFG_0_DEVICE_ID XPAR_PS7_DEV_CFG_0_DEVICE_ID
#define XPAR_XDCFG_0_BASEADDR 0xF8007000
#define XPAR_XDCFG_0_HIGHADDR 0xF80070FF


/******************************************************************/

/* Definitions for driver DMAPS */
#define XPAR_XDMAPS_NUM_INSTANCES 2

/* Definitions for peripheral PS7_DMA_NS */
#define XPAR_PS7_DMA_NS_DEVICE_ID 0
#define XPAR_PS7_DMA_NS_BASEADDR 0xF8004000
#define XPAR_PS7_DMA_NS_HIGHADDR 0xF8004FFF


/* Definitions for peripheral PS7_DMA_S */
#define XPAR_PS7_DMA_S_DEVICE_ID 1
#define XPAR_PS7_DMA_S_BASEADDR 0xF8003000
#define XPAR_PS7_DMA_S_HIGHADDR 0xF8003FFF


/******************************************************************/

/* Canonical definitions for peripheral PS7_DMA_NS */
#define XPAR_XDMAPS_0_DEVICE_ID XPAR_PS7_DMA_NS_DEVICE_ID
#define XPAR_XDMAPS_0_BASEADDR 0xF8004000
#define XPAR_XDMAPS_0_HIGHADDR 0xF8004FFF

/* Canonical definitions for peripheral PS7_DMA_S */
#define XPAR_XDMAPS_1_DEVICE_ID XPAR_PS7_DMA_S_DEVICE_ID
#define XPAR_XDMAPS_1_BASEADDR 0xF8003000
#define XPAR_XDMAPS_1_HIGHADDR 0xF8003FFF


/******************************************************************/

/* Definitions for driver EMACPS */
#define XPAR_XEMACPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_ETHERNET_0 */
#define XPAR_PS7_ETHERNET_0_DEVICE_ID 0
#define XPAR_PS7_ETHERNET_0_BASEADDR 0xE000B000
#define XPAR_PS7_ETHERNET_0_HIGHADDR 0xE000BFFF
#define XPAR_PS7_ETHERNET_0_ENET_CLK_FREQ_HZ 25000000
#define XPAR_PS7_ETHERNET_0_ENET_SLCR_1000MBPS_DIV0 8
#define XPAR_PS7_ETHERNET_0_ENET_SLCR_1000MBPS_DIV1 1
#define XPAR_PS7_ETHERNET_0_ENET_SLCR_100MBPS_DIV0 8
#define XPAR_PS7_ETHERNET_0_ENET_SLCR_100MBPS_DIV1 5
#define XPAR_PS7_ETHERNET_0_ENET_SLCR_10MBPS_DIV0 8
#define XPAR_PS7_ETHERNET_0_ENET_SLCR_10MBPS_DIV1 50


/******************************************************************/

/* Canonical definitions for peripheral PS7_ETHERNET_0 */
#define XPAR_XEMACPS_0_DEVICE_ID XPAR_PS7_ETHERNET_0_DEVICE_ID
#define XPAR_XEMACPS_0_BASEADDR 0xE000B000
#define XPAR_XEMACPS_0_HIGHADDR 0xE000BFFF
#define XPAR_XEMACPS_0_ENET_CLK_FREQ_HZ 25000000
#define XPAR_XEMACPS_0_ENET_SLCR_1000Mbps_DIV0 8
#define XPAR_XEMACPS_0_ENET_SLCR_1000Mbps_DIV1 1
#define XPAR_XEMACPS_0_ENET_SLCR_100Mbps_DIV0 8
#define XPAR_XEMACPS_0_ENET_SLCR_100Mbps_DIV1 5
#define XPAR_XEMACPS_0_ENET_SLCR_10Mbps_DIV0 8
#define XPAR_XEMACPS_0_ENET_SLCR_10Mbps_DIV1 50


/******************************************************************/


/* Definitions for peripheral PS7_AFI_0 */
#define XPAR_PS7_AFI_0_S_AXI_BASEADDR 0xF8008000
#define XPAR_PS7_AFI_0_S_AXI_HIGHADDR 0xF8008FFF


/* Definitions for peripheral PS7_AFI_1 */
#define XPAR_PS7_AFI_1_S_AXI_BASEADDR 0xF8009000
#define XPAR_PS7_AFI_1_S_AXI_HIGHADDR 0xF8009FFF


/* Definitions for peripheral PS7_AFI_2 */
#define XPAR_PS7_AFI_2_S_AXI_BASEADDR 0xF800A000
#define XPAR_PS7_AFI_2_S_AXI_HIGHADDR 0xF800AFFF


/* Definitions for peripheral PS7_AFI_3 */
#define XPAR_PS7_AFI_3_S_AXI_BASEADDR 0xF800B000
#define XPAR_PS7_AFI_3_S_AXI_HIGHADDR 0xF800BFFF


/* Definitions for peripheral PS7_CORESIGHT_COMP_0 */
#define XPAR_PS7_CORESIGHT_COMP_0_S_AXI_BASEADDR 0xF8800000
#define XPAR_PS7_CORESIGHT_COMP_0_S_AXI_HIGHADDR 0xF88FFFFF


/* Definitions for peripheral PS7_DDR_0 */
#define XPAR_PS7_DDR_0_S_AXI_BASEADDR 0x00100000
#define XPAR_PS7_DDR_0_S_AXI_HIGHADDR 0x3FFFFFFF


/* Definitions for peripheral PS7_DDRC_0 */
#define XPAR_PS7_DDRC_0_S_AXI_BASEADDR 0xF8006000
#define XPAR_PS7_DDRC_0_S_AXI_HIGHADDR 0xF8006FFF


/* Definitions for peripheral PS7_GLOBALTIMER_0 */
#define XPAR_PS7_GLOBALTIMER_0_S_AXI_BASEADDR 0xF8F00200
#define XPAR_PS7_GLOBALTIMER_0_S_AXI_HIGHADDR 0xF8F002FF


/* Definitions for peripheral PS7_GPV_0 */
#define XPAR_PS7_GPV_0_S_AXI_BASEADDR 0xF8900000
#define XPAR_PS7_GPV_0_S_AXI_HIGHADDR 0xF89FFFFF


/* Definitions for peripheral PS7_INTC_DIST_0 */
#define XPAR_PS7_INTC_DIST_0_S_AXI_BASEADDR 0xF8F01000
#define XPAR_PS7_INTC_DIST_0_S_AXI_HIGHADDR 0xF8F01FFF


/* Definitions for peripheral PS7_IOP_BUS_CONFIG_0 */
#define XPAR_PS7_IOP_BUS_CONFIG_0_S_AXI_BASEADDR 0xE0200000
#define XPAR_PS7_IOP_BUS_CONFIG_0_S_AXI_HIGHADDR 0xE0200FFF


/* Definitions for peripheral PS7_L2CACHEC_0 */
#define XPAR_PS7_L2CACHEC_0_S_AXI_BASEADDR 0xF8F02000
#define XPAR_PS7_L2CACHEC_0_S_AXI_HIGHADDR 0xF8F02FFF


/* Definitions for peripheral PS7_OCMC_0 */
#define XPAR_PS7_OCMC_0_S_AXI_BASEADDR 0xF800C000
#define XPAR_PS7_OCMC_0_S_AXI_HIGHADDR 0xF800CFFF


/* Definitions for peripheral PS7_PL310_0 */
#define XPAR_PS7_PL310_0_S_AXI_BASEADDR 0xF8F02000
#define XPAR_PS7_PL310_0_S_AXI_HIGHADDR 0xF8F02FFF


/* Definitions for peripheral PS7_PMU_0 */
#define XPAR_PS7_PMU_0_S_AXI_BASEADDR 0xF8891000
#define XPAR_PS7_PMU_0_S_AXI_HIGHADDR 0xF8891FFF
#define XPAR_PS7_PMU_0_PMU1_S_AXI_BASEADDR 0xF8893000
#define XPAR_PS7_PMU_0_PMU1_S_AXI_HIGHADDR 0xF8893FFF


/* Definitions for peripheral PS7_QSPI_LINEAR_0 */
#define XPAR_PS7_QSPI_LINEAR_0_S_AXI_BASEADDR 0xFC000000
#define XPAR_PS7_QSPI_LINEAR_0_S_AXI_HIGHADDR 0xFCFFFFFF


/* Definitions for peripheral PS7_RAM_0 */
#define XPAR_PS7_RAM_0_S_AXI_BASEADDR 0x00000000
#define XPAR_PS7_RAM_0_S_AXI_HIGHADDR 0x0003FFFF


/* Definitions for peripheral PS7_RAM_1 */
#define XPAR_PS7_RAM_1_S_AXI_BASEADDR 0xFFFC0000
#define XPAR_PS7_RAM_1_S_AXI_HIGHADDR 0xFFFFFFFF


/* Definitions for peripheral PS7_SCUC_0 */
#define XPAR_PS7_SCUC_0_S_AXI_BASEADDR 0xF8F00000
#define XPAR_PS7_SCUC_0_S_AXI_HIGHADDR 0xF8F000FC


/* Definitions for peripheral PS7_SLCR_0 */
#define XPAR_PS7_SLCR_0_S_AXI_BASEADDR 0xF8000000
#define XPAR_PS7_SLCR_0_S_AXI_HIGHADDR 0xF8000FFF


/******************************************************************/

/* Definitions for driver GPIOPS */
#define XPAR_XGPIOPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_GPIO_0 */
#define XPAR_PS7_GPIO_0_DEVICE_ID 0
#define XPAR_PS7_GPIO_0_BASEADDR 0xE000A000
#define XPAR_PS7_GPIO_0_HIGHADDR 0xE000AFFF


/******************************************************************/

/* Canonical definitions for peripheral PS7_GPIO_0 */
#define XPAR_XGPIOPS_0_DEVICE_ID XPAR_PS7_GPIO_0_DEVICE_ID
#define XPAR_XGPIOPS_0_BASEADDR 0xE000A000
#define XPAR_XGPIOPS_0_HIGHADDR 0xE000AFFF


/******************************************************************/

/* Definitions for driver IIC */
#define XPAR_XIIC_NUM_INSTANCES 1

/* Definitions for peripheral AXI4_LITE_PERIPHERALS_AXI_IIC_1 */
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_IIC_1_DEVICE_ID 0
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_IIC_1_BASEADDR 0x40800000
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_IIC_1_HIGHADDR 0x4080FFFF
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_IIC_1_TEN_BIT_ADR 0
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_IIC_1_GPO_WIDTH 2


/******************************************************************/

/* Canonical definitions for peripheral AXI4_LITE_PERIPHERALS_AXI_IIC_1 */
#define XPAR_IIC_0_DEVICE_ID XPAR_AXI4_LITE_PERIPHERALS_AXI_IIC_1_DEVICE_ID
#define XPAR_IIC_0_BASEADDR 0x40800000
#define XPAR_IIC_0_HIGHADDR 0x4080FFFF
#define XPAR_IIC_0_TEN_BIT_ADR 0
#define XPAR_IIC_0_GPO_WIDTH 2


/******************************************************************/

#define XPAR_INTC_MAX_NUM_INTR_INPUTS 5
#define XPAR_XINTC_HAS_IPR 1
#define XPAR_XINTC_HAS_SIE 1
#define XPAR_XINTC_HAS_CIE 1
#define XPAR_XINTC_HAS_IVR 1
/* Definitions for driver INTC */
#define XPAR_XINTC_NUM_INSTANCES 1

/* Definitions for peripheral AXI4_LITE_PERIPHERALS_AXI_INTC_1 */
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_INTC_1_DEVICE_ID 0
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_INTC_1_BASEADDR 0x41200000
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_INTC_1_HIGHADDR 0x4120FFFF
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_INTC_1_KIND_OF_INTR 0xFFFFFFEA
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_INTC_1_HAS_FAST 0
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_INTC_1_IVAR_RESET_VALUE 0x00000010
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_INTC_1_NUM_INTR_INPUTS 5

#define XPAR_MICROBLAZE_CORE_CLOCK_FREQ_HZ 50000000
/******************************************************************/

#define XPAR_INTC_SINGLE_BASEADDR 0x41200000
#define XPAR_INTC_SINGLE_HIGHADDR 0x4120FFFF
#define XPAR_INTC_SINGLE_DEVICE_ID XPAR_AXI4_LITE_PERIPHERALS_AXI_INTC_1_DEVICE_ID
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_INTC_1_TYPE 0
//#define XPAR_AXI4_LITE_PERIPHERALS_XLCONSTANT_0_DOUT_MASK 0X000001
//#define XPAR_AXI4_LITE_PERIPHERALS_AXI_INTC_1_AXI4_LITE_PERIPHERALS_XLCONSTANT_0_DOUT_INTR 0
#define XPAR_SYSTEM_DP_INT_0_MASK 0X000002
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_INTC_1_SYSTEM_DP_INT_0_INTR 1
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_TIMER_1_INTERRUPT_MASK 0X000004
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_INTC_1_AXI4_LITE_PERIPHERALS_AXI_TIMER_1_INTERRUPT_INTR 2
#define XPAR_AXI4_LITE_PERIPHERALS_XLCONSTANT_0_DOUT_MASK 0X000008
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_INTC_1_AXI4_LITE_PERIPHERALS_XLCONSTANT_0_DOUT_INTR 3
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_IIC_1_IIC2INTC_IRPT_MASK 0X000010
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_INTC_1_AXI4_LITE_PERIPHERALS_AXI_IIC_1_IIC2INTC_IRPT_INTR 4

/******************************************************************/

/* Canonical definitions for peripheral AXI4_LITE_PERIPHERALS_AXI_INTC_1 */
#define XPAR_INTC_0_DEVICE_ID XPAR_AXI4_LITE_PERIPHERALS_AXI_INTC_1_DEVICE_ID
#define XPAR_INTC_0_BASEADDR 0x41200000
#define XPAR_INTC_0_HIGHADDR 0x4120FFFF
#define XPAR_INTC_0_KIND_OF_INTR 0xFFFFFFEA
#define XPAR_INTC_0_HAS_FAST 0
#define XPAR_INTC_0_IVAR_RESET_VALUE 0x00000010
#define XPAR_INTC_0_NUM_INTR_INPUTS 5
#define XPAR_INTC_0_INTC_TYPE 0

#define XPAR_INTC_0_TMRCTR_0_VEC_ID XPAR_AXI4_LITE_PERIPHERALS_AXI_INTC_1_AXI4_LITE_PERIPHERALS_AXI_TIMER_1_INTERRUPT_INTR
#define XPAR_INTC_0_IIC_0_VEC_ID XPAR_AXI4_LITE_PERIPHERALS_AXI_INTC_1_AXI4_LITE_PERIPHERALS_AXI_IIC_1_IIC2INTC_IRPT_INTR

/******************************************************************/

/* Definitions for driver QSPIPS */
#define XPAR_XQSPIPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_QSPI_0 */
#define XPAR_PS7_QSPI_0_DEVICE_ID 0
#define XPAR_PS7_QSPI_0_BASEADDR 0xE000D000
#define XPAR_PS7_QSPI_0_HIGHADDR 0xE000DFFF
#define XPAR_PS7_QSPI_0_QSPI_CLK_FREQ_HZ 200000000
#define XPAR_PS7_QSPI_0_QSPI_MODE 2


/******************************************************************/

/* Canonical definitions for peripheral PS7_QSPI_0 */
#define XPAR_XQSPIPS_0_DEVICE_ID XPAR_PS7_QSPI_0_DEVICE_ID
#define XPAR_XQSPIPS_0_BASEADDR 0xE000D000
#define XPAR_XQSPIPS_0_HIGHADDR 0xE000DFFF
#define XPAR_XQSPIPS_0_QSPI_CLK_FREQ_HZ 200000000
#define XPAR_XQSPIPS_0_QSPI_MODE 2


/******************************************************************/


/***Definitions for Core_nIRQ/nFIQ interrupts ****/
/* Definitions for driver SCUGIC */
#define XPAR_XSCUGIC_NUM_INSTANCES 1

/* Definitions for peripheral PS7_SCUGIC_0 */
#define XPAR_PS7_SCUGIC_0_DEVICE_ID 0
#define XPAR_PS7_SCUGIC_0_BASEADDR 0xF8F00100
#define XPAR_PS7_SCUGIC_0_HIGHADDR 0xF8F001FF
#define XPAR_PS7_SCUGIC_0_DIST_BASEADDR 0xF8F01000


/******************************************************************/

/* Canonical definitions for peripheral PS7_SCUGIC_0 */
#define XPAR_SCUGIC_0_DEVICE_ID 0
#define XPAR_SCUGIC_0_CPU_BASEADDR 0xF8F00100
#define XPAR_SCUGIC_0_CPU_HIGHADDR 0xF8F001FF
#define XPAR_SCUGIC_0_DIST_BASEADDR 0xF8F01000


/******************************************************************/

/* Definitions for driver SCUTIMER */
#define XPAR_XSCUTIMER_NUM_INSTANCES 1

/* Definitions for peripheral PS7_SCUTIMER_0 */
#define XPAR_PS7_SCUTIMER_0_DEVICE_ID 0
#define XPAR_PS7_SCUTIMER_0_BASEADDR 0xF8F00600
#define XPAR_PS7_SCUTIMER_0_HIGHADDR 0xF8F0061F


/******************************************************************/

/* Canonical definitions for peripheral PS7_SCUTIMER_0 */
#define XPAR_XSCUTIMER_0_DEVICE_ID XPAR_PS7_SCUTIMER_0_DEVICE_ID
#define XPAR_XSCUTIMER_0_BASEADDR 0xF8F00600
#define XPAR_XSCUTIMER_0_HIGHADDR 0xF8F0061F


/******************************************************************/

/* Definitions for driver SCUWDT */
#define XPAR_XSCUWDT_NUM_INSTANCES 1

/* Definitions for peripheral PS7_SCUWDT_0 */
#define XPAR_PS7_SCUWDT_0_DEVICE_ID 0
#define XPAR_PS7_SCUWDT_0_BASEADDR 0xF8F00620
#define XPAR_PS7_SCUWDT_0_HIGHADDR 0xF8F006FF


/******************************************************************/

/* Canonical definitions for peripheral PS7_SCUWDT_0 */
#define XPAR_SCUWDT_0_DEVICE_ID XPAR_PS7_SCUWDT_0_DEVICE_ID
#define XPAR_SCUWDT_0_BASEADDR 0xF8F00620
#define XPAR_SCUWDT_0_HIGHADDR 0xF8F006FF


/******************************************************************/

/* Definitions for driver SDPS */
#define XPAR_XSDPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_SD_0 */
#define XPAR_PS7_SD_0_DEVICE_ID 0
#define XPAR_PS7_SD_0_BASEADDR 0xE0100000
#define XPAR_PS7_SD_0_HIGHADDR 0xE0100FFF
#define XPAR_PS7_SD_0_SDIO_CLK_FREQ_HZ 50000000


/******************************************************************/

/* Canonical definitions for peripheral PS7_SD_0 */
#define XPAR_XSDPS_0_DEVICE_ID XPAR_PS7_SD_0_DEVICE_ID
#define XPAR_XSDPS_0_BASEADDR 0xE0100000
#define XPAR_XSDPS_0_HIGHADDR 0xE0100FFF
#define XPAR_XSDPS_0_SDIO_CLK_FREQ_HZ 50000000


/******************************************************************/

/* Definitions for driver TMRCTR */
#define XPAR_XTMRCTR_NUM_INSTANCES 1

/* Definitions for peripheral AXI4_LITE_PERIPHERALS_AXI_TIMER_1 */
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_TIMER_1_DEVICE_ID 0
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_TIMER_1_BASEADDR 0x41C00000
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_TIMER_1_HIGHADDR 0x41C0FFFF
#define XPAR_AXI4_LITE_PERIPHERALS_AXI_TIMER_1_CLOCK_FREQ_HZ 50000000


/******************************************************************/

/* Canonical definitions for peripheral AXI4_LITE_PERIPHERALS_AXI_TIMER_1 */
#define XPAR_TMRCTR_0_DEVICE_ID 0
#define XPAR_TMRCTR_0_BASEADDR 0x41C00000
#define XPAR_TMRCTR_0_HIGHADDR 0x41C0FFFF
#define XPAR_TMRCTR_0_CLOCK_FREQ_HZ XPAR_AXI4_LITE_PERIPHERALS_AXI_TIMER_1_CLOCK_FREQ_HZ

/******************************************************************/

/* Definitions for driver TTCPS */
#define XPAR_XTTCPS_NUM_INSTANCES 3

/* Definitions for peripheral PS7_TTC_0 */
#define XPAR_PS7_TTC_0_DEVICE_ID 0
#define XPAR_PS7_TTC_0_BASEADDR 0XF8001000
#define XPAR_PS7_TTC_0_TTC_CLK_FREQ_HZ 111111115
#define XPAR_PS7_TTC_0_TTC_CLK_CLKSRC 0
#define XPAR_PS7_TTC_1_DEVICE_ID 1
#define XPAR_PS7_TTC_1_BASEADDR 0XF8001004
#define XPAR_PS7_TTC_1_TTC_CLK_FREQ_HZ 111111115
#define XPAR_PS7_TTC_1_TTC_CLK_CLKSRC 0
#define XPAR_PS7_TTC_2_DEVICE_ID 2
#define XPAR_PS7_TTC_2_BASEADDR 0XF8001008
#define XPAR_PS7_TTC_2_TTC_CLK_FREQ_HZ 111111115
#define XPAR_PS7_TTC_2_TTC_CLK_CLKSRC 0


/******************************************************************/

/* Canonical definitions for peripheral PS7_TTC_0 */
#define XPAR_XTTCPS_0_DEVICE_ID XPAR_PS7_TTC_0_DEVICE_ID
#define XPAR_XTTCPS_0_BASEADDR 0xF8001000
#define XPAR_XTTCPS_0_TTC_CLK_FREQ_HZ 111111115
#define XPAR_XTTCPS_0_TTC_CLK_CLKSRC 0

#define XPAR_XTTCPS_1_DEVICE_ID XPAR_PS7_TTC_1_DEVICE_ID
#define XPAR_XTTCPS_1_BASEADDR 0xF8001004
#define XPAR_XTTCPS_1_TTC_CLK_FREQ_HZ 111111115
#define XPAR_XTTCPS_1_TTC_CLK_CLKSRC 0

#define XPAR_XTTCPS_2_DEVICE_ID XPAR_PS7_TTC_2_DEVICE_ID
#define XPAR_XTTCPS_2_BASEADDR 0xF8001008
#define XPAR_XTTCPS_2_TTC_CLK_FREQ_HZ 111111115
#define XPAR_XTTCPS_2_TTC_CLK_CLKSRC 0


/******************************************************************/

/* Definitions for driver UARTPS */
#define XPAR_XUARTPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_UART_1 */
#define XPAR_PS7_UART_1_DEVICE_ID 0
#define XPAR_PS7_UART_1_BASEADDR 0xE0001000
#define XPAR_PS7_UART_1_HIGHADDR 0xE0001FFF
#define XPAR_PS7_UART_1_UART_CLK_FREQ_HZ 50000000
#define XPAR_PS7_UART_1_HAS_MODEM 0


/******************************************************************/

/* Canonical definitions for peripheral PS7_UART_1 */
#define XPAR_XUARTPS_0_DEVICE_ID XPAR_PS7_UART_1_DEVICE_ID
#define XPAR_XUARTPS_0_BASEADDR 0xE0001000
#define XPAR_XUARTPS_0_HIGHADDR 0xE0001FFF
#define XPAR_XUARTPS_0_UART_CLK_FREQ_HZ 50000000
#define XPAR_XUARTPS_0_HAS_MODEM 0


/******************************************************************/

/* Definitions for driver XADCPS */
#define XPAR_XADCPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_XADC_0 */
#define XPAR_PS7_XADC_0_DEVICE_ID 0
#define XPAR_PS7_XADC_0_BASEADDR 0xF8007100
#define XPAR_PS7_XADC_0_HIGHADDR 0xF8007120


/******************************************************************/

/* Canonical definitions for peripheral PS7_XADC_0 */
#define XPAR_XADCPS_0_DEVICE_ID XPAR_PS7_XADC_0_DEVICE_ID
#define XPAR_XADCPS_0_BASEADDR 0xF8007100
#define XPAR_XADCPS_0_HIGHADDR 0xF8007120


/******************************************************************/

/* Definitions for driver DPTX */
#define XPAR_XDPTX_NUM_INSTANCES 1


/* Definitions for peripheral DISPLAYPORT_0 */
#define XPAR_DISPLAYPORT_0_DEVICE_ID 0
#define XPAR_DISPLAYPORT_0_BASEADDR 0x44A00000
#define XPAR_DISPLAYPORT_0_LANE_COUNT 4
#define XPAR_DISPLAYPORT_0_LINK_RATE 20
#define XPAR_DISPLAYPORT_0_MAX_BITS_PER_COLOR 16
#define XPAR_DISPLAYPORT_0_QUAD_PIXEL_ENABLE 1
#define XPAR_DISPLAYPORT_0_DUAL_PIXEL_ENABLE 1
#define XPAR_DISPLAYPORT_0_YCRCB_ENABLE 1
#define XPAR_DISPLAYPORT_0_YONLY_ENABLE 0
#define XPAR_DISPLAYPORT_0_GT_DATAWIDTH 2
#define XPAR_DISPLAYPORT_0_SECONDARY_SUPPORT 0
#define XPAR_DISPLAYPORT_0_AUDIO_CHANNELS 2
#define XPAR_DISPLAYPORT_0_MST_ENABLE 0
#define XPAR_DISPLAYPORT_0_NUMBER_OF_MST_STREAMS 2
#define XPAR_DISPLAYPORT_0_PROTOCOL_SELECTION 1
#define XPAR_DISPLAYPORT_0_S_AXI_ACLK 50000000


/******************************************************************/

/* Canonical definitions for peripheral DISPLAYPORT_0 */
#define XPAR_XDPTX_0_NUM_INSTANCES 0
#define XPAR_XDPTX_0_DEVICE_ID XPAR_DISPLAYPORT_0_DEVICE_ID
#define XPAR_XDPTX_0_BASEADDR 0x44A10000
#define XPAR_XDPTX_0_LANE_COUNT 4
#define XPAR_XDPTX_0_LINK_RATE 20
#define XPAR_XDPTX_0_MAX_BITS_PER_COLOR 16
#define XPAR_XDPTX_0_QUAD_PIXEL_ENABLE 1
#define XPAR_XDPTX_0_DUAL_PIXEL_ENABLE 1
#define XPAR_XDPTX_0_YCRCB_ENABLE 1
#define XPAR_XDPTX_0_YONLY_ENABLE 0
#define XPAR_XDPTX_0_GT_DATAWIDTH 2
#define XPAR_XDPTX_0_SECONDARY_SUPPORT 0
#define XPAR_XDPTX_0_AUDIO_CHANNELS 2
#define XPAR_XDPTX_0_MST_ENABLE 0
#define XPAR_XDPTX_0_NUMBER_OF_MST_STREAMS 2
#define XPAR_XDPTX_0_PROTOCOL_SELECTION 1
#define XPAR_XDPTX_0_S_AXI_ACLK 50000000


/******************************************************************/
