/*******************************************************************************
 *
 * Copyright (C) 2014 Xilinx, Inc.  All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Use of the Software is limited solely to applications:
 * (a) running on a Xilinx device, or
 * (b) that interact with a Xilinx device through a bus or interconnect.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * XILINX CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Except as contained in this notice, the name of the Xilinx shall not be used
 * in advertising or otherwise to promote the sale, use or other dealings in
 * this Software without prior written authorization from Xilinx.
 *
*******************************************************************************/
/******************************************************************************/
/**
 *
 * @file sst_example.h
 *
 * Contains a design example using the XDptx driver in single-stream transport and multi-stream transport
 * (MST) mode based on the menu selection.
 *
 * @note	For this example to display output, the user will need to
 *		implement initialization of the system (Dptx_PlatformInit) and,
 *		after training is complete, implement configuration of the video
 *		stream source in order to provide the DisplayPort core with
 *		input. See XAPP1178 for reference.
 * @note	The functions Dptx_PlatformInit and Dptx_StreamSrc* are declared
 *		extern in xdptx_example_common.h and are left up to the user to
 *		implement.
 * @note	Some setups may require introduction of delays when sending
 *		sideband messages.
 *
 * <pre>
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -----------------------------------------------
 * 1.0   als  08/07/14 Initial creation.
 * 2.0   als  09/23/14 Improved programming sequence for payload allocation.
 * </pre>
 *
*******************************************************************************/

/******************************* Include Files ********************************/

#include "xdptx_example_common.h"
#include "xtmrctr.h"
#include "vidgen.h"
#include "xintc.h"
#include "xdptx.h"
/*************************** Constant Definitions ****************************/
#define NUM_STREAMS 1



/****************************** Type Definitions ******************************/
/* For a MicroBlaze system
 * following drivers and associated types will be used. */
#ifdef XPAR_INTC_0_DEVICE_ID
#define INTC		XIntc
//#define INTC_HANDLER	XIntc_InterruptHandler
#endif /* XPAR_INTC_0_DEVICE_ID */

/**************************** Function Prototypes *****************************/
u32 Dptx_SstExample(XDptx *InstancePtr, u16 DeviceId,user_config_struct *user_config);
u32 Dptx_SstExampleRun(XDptx *InstancePtr, user_config_struct *user_config);

/*************************** Variable Declarations ****************************/
XTmrCtr TimerCounterInst; /* The timer counter instance. */
INTC IntcInstance; /* The interrupt controller instance. */
u8 Index;

/**************************** Function Definitions ****************************/

/******************************************************************************/
/**
 * The main entry point for the single-stream transport (SST) example using the
 * XDptx driver. This function will call the video sending data over main link
 *
 * @param	InstancePtr is a pointer to the XDptx instance.
 * @param	DeviceId is the unique device ID of the DisplayPort TX core
 *		instance.
 *
 * @return
 *		- XST_FAILURE if the system setup failed.
 *		- XST_SUCCESS should never return since this function, if setup
 *		  was successful, is blocking.
 *
 * @note	If system setup was successful, this function is blocking.
 *
*******************************************************************************/
u32 Dptx_SstExample(XDptx *InstancePtr, u16 DeviceId,user_config_struct *user_config )
{
	u32 Status;


	/* Do platform initialization here. This is hardware system specific -
	 * it is up to the user to implement this function. */
	//Dptx_PlatformInit();
	/******************/

	XDptx_SetUserTimerHandler(InstancePtr, &Dptx_CustomWaitUs, &TimerCounterInst);

	Status = Dptx_SetupExample(InstancePtr, DeviceId);
	if (Status != XST_SUCCESS)
	{
		return Status;
	}

	xil_printf("Reading EDID to check resolution support that is going to be displayed\r\n");
	XDptx_DbgPrintEdid(&DptxInstance);
	select_resolution_from_edid();
	Status = Dptx_SstExampleRun(InstancePtr, user_config);



	return Status;
}





/******************************************************************************/
/**
 * This function trains the link and allocates stream payloads for single stream (SST)
 *
 * @param	InstancePtr is a pointer to the XDptx instance.
 * @param	DeviceId is the unique device ID of the DisplayPort TX core
 *		instance.
 *
 * @return
 *		- XST_SUCCESS if SST transfer was successful.
 *		- XST_FAILURE otherwise.
 *
 * @note	None.
 *
*******************************************************************************/

u32 Dptx_SstExampleRun(XDptx *InstancePtr, user_config_struct *user_config)
{
	u32 Status;
	u8 Bpc = user_config->user_bpc;
	XVid_VideoMode VideoMode = user_config->VideoMode_local;
#ifdef __MICROBLAZE__
	microblaze_disable_interrupts();
#endif
	xil_printf("*******************Dptx_SstExampleRun******************\r\n");

	//Status = Dptx_CheckValidity(InstancePtr);
	Status = XST_SUCCESS;
	if (Status != XST_SUCCESS)
	{
		xil_printf("!!! Link over-subscribed !!! for selected resolution, bpc, lane count and link rate values\r\n");
		xil_printf("! Hence try setting Link rate and Lane count to the max supported values.\n\r");
		xil_printf("! Still no Video on Screen tells user to bring down resolution or bpc values\n\r");
		Status = XDptx_CfgMainLinkMax(InstancePtr);
		if (Status != XST_SUCCESS)
		{
			xil_printf("%s Function Failed in %s file at %d line \r\n",__func__,__FILE__,__LINE__);
			xil_printf("Rx Device is not connected.\n\r");
#ifdef __MICROBLAZE__
			microblaze_enable_interrupts();
#endif
			return XST_FAILURE;
		}
		Status = XDptx_EstablishLink(InstancePtr);
		if (Status != XST_SUCCESS)
		{

			xil_printf("%s Function Failed in %s file at %d line \r\n",__func__,__FILE__,__LINE__);
			xil_printf("!!! Training failed !!!\n\r");
#ifdef __MICROBLAZE__
			microblaze_enable_interrupts();
#endif
			return XST_FAILURE;

		}
		XDptx_CfgMsaUseStandardVideoMode(InstancePtr,XDPTX_STREAM_ID1, VideoMode);
	}
	/* Reset MST mode in both the RX and TX. */
	XDptx_MstDisable(InstancePtr);
	XDptx_EnableTrainAdaptive(InstancePtr, TRAIN_ADAPTIVE);
	XDptx_SetHasRedriverInPath(InstancePtr, TRAIN_HAS_REDRIVER);

	/* A DisplayPort connection must exist at this point. See the interrupt
	 * and polling examples for waiting for connection events. */
	Status = Dptx_StartLink(InstancePtr);

	if (Status != XST_SUCCESS)
	{
		xil_printf("%s Function Failed in %s file at %d line \r\n",__func__,__FILE__,__LINE__);
		xil_printf("! Link Training failed.\n\r");
#ifdef __MICROBLAZE__
		microblaze_enable_interrupts();
#endif
		return Status;
	}
	else
	{
		xil_printf("Dptx_SstExampleRun:Dptx_StartLink:  !!! Link Training Passed.\n\r");
	}

	/* Disable main stream to force sending of IDLE patterns. */
	XDptx_DisableMainLink(InstancePtr);
    XDptx_ClearMsaValues(InstancePtr, XDPTX_STREAM_ID1);

	/* Set the main stream attributes (MSA) for each enabled stream (each
		 * stream has an identical configuration). Then, set the configuration
		 * for that stream in the corresponding DisplayPort TX registers. */
	XDptx_CfgMsaSetBpc(InstancePtr, XDPTX_STREAM_ID1, Bpc);
	XDptx_CfgMsaEnSynchClkMode(InstancePtr,	XDPTX_STREAM_ID1, 1);
	XDptx_CfgMsaUseStandardVideoMode(InstancePtr,XDPTX_STREAM_ID1, VideoMode);

	/* Reset the transmitter. */
	XDptx_WriteReg(InstancePtr->Config.BaseAddr, XDPTX_SOFT_RESET, XDPTX_SOFT_RESET_VIDEO_STREAM_ALL_MASK);
	XDptx_WriteReg(InstancePtr->Config.BaseAddr, XDPTX_SOFT_RESET, 0x0);

	/* Configure video stream source or generator here. This function needs
 	 to be implemented in order for video to be displayed and is hardware
	 system specific. It is up to the user to implement this function. */
	/* Reset and disable the pattern generator. */
	XDptx_SetVideoMode(InstancePtr,XDPTX_STREAM_ID1);
	Dptx_StreamSrcSetup(InstancePtr);
	Dptx_StreamSrcConfigure(InstancePtr);
	Dptx_StreamSrcSync(InstancePtr);

	/* Enable the main link. */
	XDptx_EnableMainLink(InstancePtr);
	xil_printf("Enabled main link for SST Mode \n\r");
#ifdef __MICROBLAZE__
	microblaze_enable_interrupts();
#endif
	return XST_SUCCESS;
}



/******************************************************************************/
/**
 * This function sets up the interrupt system such that interrupts caused by
 * Hot-Plug-Detect (HPD) events and pulses are handled. This function is
 * application-specific for systems that have an interrupt controller connected
 * to the processor. The user should modify this function to fit the
 * application.
 *
 * @param	InstancePtr is a pointer to the XDptx instance.
 * @param	IntcPtr is a pointer to the interrupt instance.
 * @param	IntrId is the unique device ID of the interrupt controller.
 * @param	DpIntrId is the interrupt ID of the DisplayPort TX connection to
 *		the interrupt controller.
 * @param	HpdEventHandler is a pointer to the handler called when an HPD
 *		event occurs.
 * @param	HpdPulseHandler is a pointer to the handler called when an HPD
 *		pulse occurs.
 *
 * @return
 *		- XST_SUCCESS if the interrupt system was successfully set up.
 *		- XST_FAILURE otherwise.
 *
 * @note	An interrupt controller must be present in the system, connected
 *		to the processor and the DisplayPort TX core.
 *
*******************************************************************************/
/*
u32 Dptx_SetupInterruptHandler(XDptx *InstancePtr, INTC *IntcPtr,
		u16 IntrId, u16 DpIntrId, XDptx_HpdEventHandler HpdEventHandler,
		XDptx_HpdPulseHandler HpdPulseHandler)
{
	u32 Status;

	 Set the HPD interrupt handlers.
	XDptx_SetHpdEventHandler(InstancePtr, HpdEventHandler, InstancePtr);
	XDptx_SetHpdPulseHandler(InstancePtr, HpdPulseHandler, InstancePtr);

	Status = XIntc_Initialize(IntcPtr, IntrId);
	if (Status != XST_SUCCESS) {
		xil_printf("%s Function Failed in %s file at %d line \r\n",__func__,__FILE__,__LINE__);
		return XST_FAILURE;
	}

	 Connect the device driver handler that will be called when an
	 * interrupt for the device occurs, the handler defined above performs
	 * the specific interrupt processing for the device.
	Status = XIntc_Connect(IntcPtr, DpIntrId,
		(XInterruptHandler)XDptx_HpdInterruptHandler, InstancePtr);

	if (Status != XST_SUCCESS) {
		xil_printf("%s Function Failed in %s file at %d line \r\n",__func__,__FILE__,__LINE__);
		return XST_FAILURE;
	}

	 Start the interrupt controller.
	Status = XIntc_Start(IntcPtr, XIN_REAL_MODE);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	XIntc_Enable(IntcPtr, DpIntrId);

	 Initialize the exception table.
	Xil_ExceptionInit();

	 Register the interrupt controller handler with the exception table.
	//Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,(Xil_ExceptionHandler)INTC_HANDLER, IntcPtr);

	 Enable exceptions.
	Xil_ExceptionEnable();

	return XST_SUCCESS;
}

*/


