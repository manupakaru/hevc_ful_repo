/*******************************************************************************
 *
 * Copyright (C) 2014 Xilinx, Inc.  All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Use of the Software is limited solely to applications:
 * (a) running on a Xilinx device, or
 * (b) that interact with a Xilinx device through a bus or interconnect.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * XILINX CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Except as contained in this notice, the name of the Xilinx shall not be used
 * in advertising or otherwise to promote the sale, use or other dealings in
 * this Software without prior written authorization from Xilinx.
 *
*******************************************************************************/
/******************************************************************************/
/**
 *
 * @file xdptx_example_common.c
 *
 * Contains a design example using the XDptx driver. It performs a self test on
 * the DisplayPort TX core by training the main link at the maximum common
 * capabilities between the TX and RX and checking the lane status.
 *
 * @note	The DisplayPort TX core does not work alone - video/audio
 *		sources need to be set up in the system correctly, as well as
 *		setting up the output path (for example, configuring the
 *		hardware system with the DisplayPort TX core output to an FMC
 *		card with DisplayPort output capabilities. Some platform
 *		initialization will need to happen prior to calling XDptx driver
 *		functions. See XAPP1178 as a reference.
 *
 * <pre>
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -----------------------------------------------
 * 1.0   als  06/17/14 Initial creation.
 * </pre>
 *
*******************************************************************************/

/******************************* Include Files ********************************/

#include "xdptx_example_common.h"
#include "xstatus.h"
#include "xtmrctr.h"
#define DP_INTERRUPT_ID		XPAR_AXI_INTC_1_DISPLAYPORT_0_AXI_INT_INTR

/**************************** Function Prototypes *****************************/
void Dptx_Mst_hpd_call(XDptx *InstancePtr, user_config_struct *user_config);
user_config_struct user_config;
user_config_struct user_config_default;
XDptx DptxInstance;
volatile u32 mst_hpd_event=0;
/**************************** Function Definitions ****************************/

/******************************************************************************/
/**
 * This function will setup and initialize the DisplayPort TX core. The core's
 * configuration parameters will be retrieved based on the configuration
 * to the DisplayPort TX core instance with the specified device ID.
 *
 * @param	InstancePtr is a pointer to the XDptx instance.
 * @param	DeviceId is the unique device ID of the DisplayPort TX core
 *		instance.
 *
 * @return
 *		- XST_SUCCESS if the device configuration was found and obtained
 *		  and if the main link was successfully established.
 *		- XST_FAILURE otherwise.
 *
 * @note	None.
 *
*******************************************************************************/
u32 Dptx_SetupExample(XDptx *InstancePtr, u16 DeviceId)
{
	XDptx_Config *ConfigPtr;
	u32 Status;

	/* Obtain the device configuration for the DisplayPort TX core. */
	ConfigPtr = XDptx_LookupConfig(DeviceId);
	if (!ConfigPtr) {
		return XST_FAILURE;
	}
	/* Copy the device configuration into the InstancePtr's Config
	 * structure. */
	XDptx_CfgInitialize(InstancePtr, ConfigPtr, ConfigPtr->BaseAddr);

	/* Initialize the DisplayPort TX core. */
	Status = XDptx_InitializeTx(InstancePtr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	return XST_SUCCESS;
}

/******************************************************************************/
/**
 * This function will configure and establish a link with the receiver device.
 *
 * @param	InstancePtr is a pointer to the XDptx instance.
 *
 * @return
 *		- XST_SUCCESS the if main link was successfully established.
 *		- XST_FAILURE otherwise.
 *
 * @note	None.
 *
*******************************************************************************/
u32 Dptx_StartLink(XDptx *InstancePtr)
{
	u32 Status;
	u8 Data[8];
	u32 VsLevelTx;
	u32 PeLevelTx;
	static u32 SetmaxLinkLaneCount=0;
	u32 IntrMask;

	IntrMask = XDptx_ReadReg(InstancePtr->Config.BaseAddr, XDPTX_INTERRUPT_MASK);

	/* Disable HPD pulse interrupts during link training. */
	XDptx_WriteReg(InstancePtr->Config.BaseAddr,
		XDPTX_INTERRUPT_MASK, IntrMask |
		XDPTX_INTERRUPT_MASK_HPD_PULSE_DETECTED_MASK);

	/* xil_printf("!!! Before  XDptx_GetRxCapabilities LinkRate:0x%02lx LineCount:%d !!!\n\r",
													InstancePtr->LinkConfig.LinkRate,
													InstancePtr->LinkConfig.LaneCount); */

	/* Obtain the capabilities of the RX device by reading the monitor's
	 * DPCD. */
	{
		Status = XDptx_GetRxCapabilities(InstancePtr);
		if (Status != XST_SUCCESS)
		{
			xil_printf("%s Function Failed in %s file at %d line \r\n",__func__,__FILE__,__LINE__);
			XDptx_WriteReg(InstancePtr->Config.BaseAddr, XDPTX_INTERRUPT_MASK, IntrMask);
			return XST_FAILURE;
		}
		XDptx_SetDownspread(InstancePtr, 1);
		XDptx_SetEnhancedFrameMode(InstancePtr, 1);

		//Configure link with max values of link rate and lane count for the first time
		//from next onwards configure it with the user set values
		if(SetmaxLinkLaneCount==0)
		{
			SetmaxLinkLaneCount = 1;
			/* Configure the main link based on the maximum common capabilities of
			 * the DisplayPort TX core and the receiver device. */
			Status = XDptx_CfgMainLinkMax(InstancePtr);
			if (Status != XST_SUCCESS)
			{
				xil_printf("%s Function Failed in %s file at %d line \r\n",__func__,__FILE__,__LINE__);
				xil_printf("Rx Device is not connected.\n\r");
				XDptx_WriteReg(InstancePtr->Config.BaseAddr, XDPTX_INTERRUPT_MASK, IntrMask);
				return XST_FAILURE;
			}
		}
		else
		{
			 XDptx_SetLinkRate(InstancePtr, InstancePtr->LinkConfig.LinkRate);
			 XDptx_SetLaneCount(InstancePtr, InstancePtr->LinkConfig.LaneCount);
		}

		Status = XDptx_EstablishLink(InstancePtr);
		if (Status != XST_SUCCESS)
		{

			xil_printf("%s Function Failed in %s file at %d line \r\n",__func__,__FILE__,__LINE__);
			xil_printf("!!! Training failed !!!\n\r");
			XDptx_WriteReg(InstancePtr->Config.BaseAddr, XDPTX_INTERRUPT_MASK, IntrMask);
			return XST_FAILURE;
		}
		else
		{
			/* read symbol error count for lanes */
			int i=0;
			for(i=0;i<8;i=i+1){
				XDptx_AuxRead(InstancePtr, XDPTX_DPCD_SYMBOL_ERROR_COUNT_LANE_0+i, 1, &Data[i]);
			}
			//XDptx_AuxRead(InstancePtr, XDPTX_DPCD_SYMBOL_ERROR_COUNT_LANE_0, 8, &Data);
			xil_printf("Dptx_StartLink: !!! Training passed at LinkRate:0x%02lx LineCount:%d !!!\n\r",
												InstancePtr->LinkConfig.LinkRate,
												InstancePtr->LinkConfig.LaneCount);
			VsLevelTx = XDptx_ReadReg(InstancePtr->Config.BaseAddr,
								XDPTX_PHY_VOLTAGE_DIFF_LANE_0);
			PeLevelTx = XDptx_ReadReg(InstancePtr->Config.BaseAddr,
								XDPTX_PHY_POSTCURSOR_LANE_0);

			xil_printf("VS:%d (TX:%d) PE:%d (TX:%d)\n\r",
						InstancePtr->LinkConfig.VsLevel, VsLevelTx,
						InstancePtr->LinkConfig.PeLevel, PeLevelTx);
		}
	}
	/* Enable HPD pulse interrupts after link training. */
	XDptx_WriteReg(InstancePtr->Config.BaseAddr, XDPTX_INTERRUPT_MASK, IntrMask);
	return XST_SUCCESS;
}

/******************************************************************************/
/**
 * This function will check the validity of the selected link and lane rates for required resolutions
 *
 * @param	InstancePtr is a pointer to the XDptx instance.
 *
 * @return
 *		- XST_SUCCESS the if main link was successfully established.
 *		- XST_FAILURE otherwise.
 *
 * @note	None.
 *
*******************************************************************************/
u32 Dptx_CheckValidity(XDptx *InstancePtr)
{
	/* Check that the stream allocation will succeed based on capabilities.
	 * Don't go through training and allocation sequence if the pre-calculations
	 * indicate that it will fail. */

	u32 LinkBw = (InstancePtr->LinkConfig.LaneCount * InstancePtr->LinkConfig.LinkRate * 27);
	u8 BitsPerPixel = 3 * (user_config.user_bpc);

	if (InstancePtr->LinkConfig.MaxLinkRate < InstancePtr->LinkConfig.LinkRate) {
		xil_printf("!!! Requested link rate exceeds maximum capabilities.\n\r");
		xil_printf("!!!\tMaximum link rate = ");
		switch (InstancePtr->LinkConfig.MaxLinkRate) {
		case XDPTX_LINK_BW_SET_540GBPS:
			xil_printf("5.40 Gbps.\n\r");
			break;
		case XDPTX_LINK_BW_SET_270GBPS:
			xil_printf("2.70 Gbps.\n\r");
			break;
		case XDPTX_LINK_BW_SET_162GBPS:
			xil_printf("1.62 Gbps.\n\r");
			break;
		}
		return XST_BUFFER_TOO_SMALL;
	}
	else if (InstancePtr->LinkConfig.MaxLaneCount < InstancePtr->LinkConfig.LaneCount) {
		xil_printf("!!! Requested lane count exceeds maximum capabilities.\n\r");
		xil_printf("!!!\tMaximum lane count = %d.\n\r", InstancePtr->LinkConfig.MaxLaneCount);
		return XST_BUFFER_TOO_SMALL;
	}

	u32 MstCapable = XDptx_MstCapable(InstancePtr);
	/* This check is done so that this function the check can be called from anywhere
	 * and it will precaculate the required total timeslots based on the number of
	 * sinks and MSA values.
	 * This works because if the example will always run in MST mode if the monitor
	 * is capable of it, otherwise in SST mode. */

	if (MstCapable != XST_SUCCESS) {
		xil_printf("Checking link Bandwidth Validity for SST.......\r\n");

		u32 TransferUnitSize = 64;
		u32 VideoBw = (XVid_VideoTimingModes[user_config.VideoMode_local].PixelClkKhz * (BitsPerPixel)) / 8;
		u32 AvgBytesPerTU = (VideoBw * TransferUnitSize) / LinkBw;

		xil_printf("!!!Link bandwidth = %d Kbps\r\n", (LinkBw * 1000));
		xil_printf("!!!Video bandwidth = %d Kbps\r\n", VideoBw);

		if (AvgBytesPerTU > (TransferUnitSize * 1000)) {
			xil_printf("!!! SST link is over-subscribed.\n\r");
			xil_printf("!!!Link bandwidth = %d Kbps\n\r", (LinkBw * 1000));
			xil_printf("!!!Video bandwidth = %d Kbps\n\r", VideoBw);

			return XST_BUFFER_TOO_SMALL;
		}
	}
	else {
		xil_printf("Checking link Bandwidth Validity for MST........\r\n");

		if (NewConnection == 1) {
			/* Need to run topology discovery first. Second check will catch over-subscription. */
			return XST_SUCCESS;
		}

		u8 StreamIndex;
		u32 TimeSlots;
		u32 TotalTimeSlots = 0;
		double PeakPixelBw;
		double Average_StreamSymbolTimeSlotsPerMTP;
		double Target_Average_StreamSymbolTimeSlotsPerMTP;
		double MaximumTarget_Average_StreamSymbolTimeSlotsPerMTP;
		u32 TsInt;
		u32 TsFrac;
		u16 Pbn;

		PeakPixelBw = ((double)XVid_VideoTimingModes[user_config.VideoMode_local].PixelClkKhz / 1000) *
							((double)BitsPerPixel / 8);

		Pbn = 1.006 * PeakPixelBw * ((double)64 / 54);

		if ((double)(1.006 * PeakPixelBw * ((double)64 / 54)) > ((double)Pbn)) {
			Pbn++;
		}

		Average_StreamSymbolTimeSlotsPerMTP = (64.0 * PeakPixelBw / LinkBw);
		MaximumTarget_Average_StreamSymbolTimeSlotsPerMTP = (54.0 * ((double)Pbn / LinkBw));

		Target_Average_StreamSymbolTimeSlotsPerMTP = (u32)Average_StreamSymbolTimeSlotsPerMTP;
		Target_Average_StreamSymbolTimeSlotsPerMTP += ((1.0 / 8.0) * (u32)(8.0 *
				(MaximumTarget_Average_StreamSymbolTimeSlotsPerMTP -
				Target_Average_StreamSymbolTimeSlotsPerMTP)));

		TsInt = Target_Average_StreamSymbolTimeSlotsPerMTP;
		TsFrac = (((double)Target_Average_StreamSymbolTimeSlotsPerMTP * 1000) - (TsInt * 1000));

		TimeSlots = TsInt;
		if (TsFrac != 0) {
			TimeSlots++;
		}
		if ((InstancePtr->Config.PayloadDataWidth == 4) && (TimeSlots % 4) != 0) {
			TimeSlots += (4 - (TimeSlots % 4));
		}
		else if ((TimeSlots % 2) != 0) {
			TimeSlots++;
		}

		/* Add up all the timeslots. */
		for (StreamIndex = 0; StreamIndex < 4; StreamIndex++) {
			if (XDptx_MstStreamIsEnabled(InstancePtr, StreamIndex + 1)) {
				TotalTimeSlots += TimeSlots;
			}
		}

		if (TotalTimeSlots > 63) {

			xil_printf("\n!!!!!!!!!!!!!!!!!!!!!! WARNING: MST link Over-subscribed.!!!!!!!!!!!!!!\r\n");
			xil_printf("Total time slots required: %d for %d streams.\n\r", TotalTimeSlots, TotalTimeSlots / TimeSlots);
			xil_printf("But only 63 time slots are available.\n\r");
			return XST_FAILURE;
		}
	}

	return XST_SUCCESS;
}

/******************************************************************************/
/**
 * This function is called when a Hot-Plug-Detect (HPD) event is received by the
 * DisplayPort TX core. The XDPTX_INTERRUPT_STATUS_HPD_EVENT_MASK bit of the
 * core's XDPTX_INTERRUPT_STATUS register indicates that an HPD event has
 * occurred.
 *
 * @param	InstancePtr is a pointer to the XDptx instance.
 *
 * @return	None.
 *
 * @note	Use the XDptx_SetHpdEventHandler driver function to set this
 *		function as the handler for HPD pulses.
 *
*******************************************************************************/
void Dptx_HpdEventHandler(void *InstancePtr)
{
	XDptx *DptxInstance = (XDptx *)InstancePtr;
	u32 IntrMask;

	IntrMask = XDptx_ReadReg(((XDptx *)InstancePtr)->Config.BaseAddr, XDPTX_INTERRUPT_MASK);

	XDptx_WriteReg(DptxInstance->Config.BaseAddr,
			XDPTX_INTERRUPT_MASK, IntrMask |
			XDPTX_INTERRUPT_MASK_HPD_PULSE_DETECTED_MASK |
			XDPTX_INTERRUPT_MASK_HPD_EVENT_MASK);

	xil_printf("+===> HPD connection event detected.\r\n");
	if (XDptx_IsConnected(DptxInstance))
	{
		if(DptxInstance->MstEnable==0)
		{
			u32 Status = XDptx_CheckLinkStatus(DptxInstance, DptxInstance->LinkConfig.LaneCount);
			if (Status != XST_SUCCESS)
			{
				xil_printf("! Need to re-train.\n\r");
				Status = Dptx_StartLink(DptxInstance);
				if (Status != XST_SUCCESS)
				{
					xil_printf("! Link re-training failed.\n\r");
				}

			}
		}
		else
		{
			mst_hpd_event = 1;
		}
		xil_printf("+===> HPD Connection event ISR is executed.\r\n");
	}
	else
	{
		xil_printf("+===> HPD disconnection event detected.\r\n");
	}

	XDptx_WriteReg(((XDptx *)InstancePtr)->Config.BaseAddr, XDPTX_INTERRUPT_MASK, IntrMask);
}

/******************************************************************************/
/**
 * This function is called when a Hot-Plug-Detect (HPD) pulse is received by the
 * DisplayPort TX core. The XDPTX_INTERRUPT_STATUS_HPD_PULSE_DETECTED_MASK bit
 * of the core's XDPTX_INTERRUPT_STATUS register indicates that an HPD event has
 * occurred.
 *
 * @param	InstancePtr is a pointer to the XDptx instance.
 *
 * @return	None.
 *
 * @note	Use the XDptx_SetHpdPulseHandler driver function to set this
 *		function as the handler for HPD pulses.
 *
*******************************************************************************/
void Dptx_HpdPulseHandler(void *InstancePtr)
{
	u32 Status = XDptx_CheckLinkStatus((XDptx *)InstancePtr, ((XDptx *)InstancePtr)->LinkConfig.LaneCount);
	XDptx *DptxInstance = (XDptx *)InstancePtr;
	if(DptxInstance->MstEnable==1)
	{
		mst_hpd_event = 1;
	}
	if (Status != XST_SUCCESS) {
		xil_printf("===> HPD pulse detected. ! Lost training, need to re-train.\n\r");
		Status = Dptx_StartLink((XDptx *)InstancePtr);
		if (Status != XST_SUCCESS) {
			xil_printf("! Link re-training failed.\n\r");
		}
	}
}


void Dptx_CustomWaitUs(void *InstancePtr, u32 MicroSeconds)
{

	XDptx *XDptx_InstancePtr = (XDptx *)InstancePtr;
	u32 TimerVal;
	u32 NumTicks = (MicroSeconds * (XDptx_InstancePtr->Config.SAxiClkHz / 1000000));

	XTmrCtr_Reset(XDptx_InstancePtr->UserTimerPtr, 0);
	XTmrCtr_Start(XDptx_InstancePtr->UserTimerPtr, 0);

	/* Wait specified number of useconds. */
	do {
		TimerVal = XTmrCtr_GetValue(XDptx_InstancePtr->UserTimerPtr, 0);
	}
	while (TimerVal < NumTicks);
}
