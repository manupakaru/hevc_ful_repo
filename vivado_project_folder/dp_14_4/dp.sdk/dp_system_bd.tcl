
################################################################
# This is a generated script based on design: dp_system
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2014.4
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   puts "ERROR: This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source dp_system_script.tcl

# If you do not already have a project created,
# you can create a project using the following command:
#    create_project project_1 myproj -part xc7z045ffg900-2
#    set_property BOARD_PART xilinx.com:zc706:part0:1.0 [current_project]


# CHANGE DESIGN NAME HERE
set design_name dp_system

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# CHECKING IF PROJECT EXISTS
if { [get_projects -quiet] eq "" } {
   puts "ERROR: Please open or create a project!"
   return 1
}


# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "ERROR: Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      puts "INFO: Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   puts "INFO: Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "ERROR: Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "ERROR: Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   puts "INFO: Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   puts "INFO: Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

puts "INFO: Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   puts $errMsg
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: pattern_gen_sub_system
proc create_hier_cell_pattern_gen_sub_system { parentCell nameHier } {

  if { $parentCell eq "" || $nameHier eq "" } {
     puts "ERROR: create_hier_cell_pattern_gen_sub_system() - Empty argument(s)!"
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 AXI4_LITE

  # Create pins
  create_bd_pin -dir I -from 0 -to 0 Op1
  create_bd_pin -dir O -from 0 -to 0 Res
  create_bd_pin -dir I -from 0 -to 0 apb_reset
  create_bd_pin -dir I -type clk s_axi_aclk
  create_bd_pin -dir I -from 0 -to 0 -type rst s_axi_aresetn
  create_bd_pin -dir O -type rst sw_vid_reset
  create_bd_pin -dir I -type clk vid_clk
  create_bd_pin -dir O -from 15 -to 0 vid_clk_D
  create_bd_pin -dir O -from 7 -to 0 vid_clk_M
  create_bd_pin -dir O vid_clk_prog
  create_bd_pin -dir O vid_enable
  create_bd_pin -dir O vid_hsync
  create_bd_pin -dir O vid_oddeven
  create_bd_pin -dir O -from 47 -to 0 vid_pixel0
  create_bd_pin -dir O -from 47 -to 0 vid_pixel1
  create_bd_pin -dir O -from 47 -to 0 vid_pixel2
  create_bd_pin -dir O -from 47 -to 0 vid_pixel3
  create_bd_pin -dir O vid_vsync

  # Create instance: axi_apb_bridge_1, and set properties
  set axi_apb_bridge_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_apb_bridge:3.0 axi_apb_bridge_1 ]
  set_property -dict [ list CONFIG.C_APB_NUM_SLAVES {1} CONFIG.C_M_APB_PROTOCOL {apb3}  ] $axi_apb_bridge_1

  # Create instance: util_vector_logic_1, and set properties
  set util_vector_logic_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_1 ]
  set_property -dict [ list CONFIG.C_OPERATION {not} CONFIG.C_SIZE {1}  ] $util_vector_logic_1

  # Create instance: video_pat_gen_0, and set properties
  set video_pat_gen_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:video_pat_gen:1.1 video_pat_gen_0 ]

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list CONFIG.CONST_VAL {0}  ] $xlconstant_0

  # Create instance: xlconstant_1, and set properties
  set xlconstant_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_1 ]
  set_property -dict [ list CONFIG.CONST_VAL {0}  ] $xlconstant_1

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins AXI4_LITE] [get_bd_intf_pins axi_apb_bridge_1/AXI4_LITE]
  connect_bd_intf_net -intf_net axi_apb_bridge_1_APB_M [get_bd_intf_pins axi_apb_bridge_1/APB_M] [get_bd_intf_pins video_pat_gen_0/vid_pat_apb_inf]

  # Create port connections
  connect_bd_net -net Op1_1 [get_bd_pins Op1] [get_bd_pins util_vector_logic_1/Op1]
  connect_bd_net -net apb_reset_1 [get_bd_pins apb_reset] [get_bd_pins video_pat_gen_0/apb_reset]
  connect_bd_net -net s_axi_aclk_1 [get_bd_pins s_axi_aclk] [get_bd_pins axi_apb_bridge_1/s_axi_aclk] [get_bd_pins video_pat_gen_0/apb_clk]
  connect_bd_net -net s_axi_aresetn_1 [get_bd_pins s_axi_aresetn] [get_bd_pins axi_apb_bridge_1/s_axi_aresetn]
  connect_bd_net -net util_vector_logic_1_Res [get_bd_pins Res] [get_bd_pins util_vector_logic_1/Res] [get_bd_pins video_pat_gen_0/vid_reset]
  connect_bd_net -net vid_clk_1 [get_bd_pins vid_clk] [get_bd_pins video_pat_gen_0/vid_clk]
  connect_bd_net -net video_pat_gen_0_sw_vid_reset [get_bd_pins sw_vid_reset] [get_bd_pins video_pat_gen_0/sw_vid_reset]
  connect_bd_net -net video_pat_gen_0_vid_clk_D [get_bd_pins vid_clk_D] [get_bd_pins video_pat_gen_0/vid_clk_D]
  connect_bd_net -net video_pat_gen_0_vid_clk_M [get_bd_pins vid_clk_M] [get_bd_pins video_pat_gen_0/vid_clk_M]
  connect_bd_net -net video_pat_gen_0_vid_clk_prog [get_bd_pins vid_clk_prog] [get_bd_pins video_pat_gen_0/vid_clk_prog]
  connect_bd_net -net video_pat_gen_0_vid_enable [get_bd_pins vid_enable] [get_bd_pins video_pat_gen_0/vid_enable]
  connect_bd_net -net video_pat_gen_0_vid_hsync [get_bd_pins vid_hsync] [get_bd_pins video_pat_gen_0/vid_hsync]
  connect_bd_net -net video_pat_gen_0_vid_oddeven [get_bd_pins vid_oddeven] [get_bd_pins video_pat_gen_0/vid_oddeven]
  connect_bd_net -net video_pat_gen_0_vid_pixel0 [get_bd_pins vid_pixel0] [get_bd_pins video_pat_gen_0/vid_pixel0]
  connect_bd_net -net video_pat_gen_0_vid_pixel1 [get_bd_pins vid_pixel1] [get_bd_pins video_pat_gen_0/vid_pixel1]
  connect_bd_net -net video_pat_gen_0_vid_pixel2 [get_bd_pins vid_pixel2] [get_bd_pins video_pat_gen_0/vid_pixel2]
  connect_bd_net -net video_pat_gen_0_vid_pixel3 [get_bd_pins vid_pixel3] [get_bd_pins video_pat_gen_0/vid_pixel3]
  connect_bd_net -net video_pat_gen_0_vid_vsync [get_bd_pins vid_vsync] [get_bd_pins video_pat_gen_0/vid_vsync]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins video_pat_gen_0/run_pattern] [get_bd_pins xlconstant_0/dout]
  connect_bd_net -net xlconstant_1_dout [get_bd_pins axi_apb_bridge_1/m_apb_pslverr] [get_bd_pins xlconstant_1/dout]
  
  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: axi4_lite_peripherals
proc create_hier_cell_axi4_lite_peripherals { parentCell nameHier } {

  if { $parentCell eq "" || $nameHier eq "" } {
     puts "ERROR: create_hier_cell_axi4_lite_peripherals() - Empty argument(s)!"
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:ddrx_rtl:1.0 DDR
  create_bd_intf_pin -mode Master -vlnv xilinx.com:display_processing_system7:fixedio_rtl:1.0 FIXED_IO
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:iic_rtl:1.0 IIC
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:mbdebug_rtl:3.0 MBDEBUG_0
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M_AXI_GP0
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S_AXI
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S_AXI3
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:mbinterrupt_rtl:1.0 interrupt
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 s_axi2

  # Create pins
  create_bd_pin -dir O -type rst FCLK_RESET0_N
  create_bd_pin -dir I -from 0 -to 0 In1
  create_bd_pin -dir O -from 0 -to 0 -type rst bus_struct_reset
  create_bd_pin -dir I dcm_locked
  create_bd_pin -dir O -from 1 -to 0 gpo
  create_bd_pin -dir O -from 0 -to 0 -type rst interconnect_aresetn
  create_bd_pin -dir O -from 0 -to 0 -type rst peripheral_aresetn
  create_bd_pin -dir O -from 0 -to 0 peripheral_reset
  create_bd_pin -dir I -type clk s_axi_aclk

  # Create instance: axi_iic_1, and set properties
  set axi_iic_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_iic:2.0 axi_iic_1 ]
  set_property -dict [ list CONFIG.C_GPO_WIDTH {2}  ] $axi_iic_1

  # Create instance: axi_intc_1, and set properties
  set axi_intc_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_intc:4.1 axi_intc_1 ]
  set_property -dict [ list CONFIG.C_HAS_FAST {0}  ] $axi_intc_1

  # Create instance: axi_timer_1, and set properties
  set axi_timer_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_timer:2.0 axi_timer_1 ]

  # Create instance: proc_sys_reset_1, and set properties
  set proc_sys_reset_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 proc_sys_reset_1 ]

  # Create instance: processing_system7_0, and set properties
  set processing_system7_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0 ]
  set_property -dict [ list CONFIG.PCW_CORE0_FIQ_INTR {1} CONFIG.PCW_I2C0_PERIPHERAL_ENABLE {0} CONFIG.PCW_P2F_UART1_INTR {0} CONFIG.PCW_USB0_PERIPHERAL_ENABLE {0} CONFIG.PCW_USE_FABRIC_INTERRUPT {1} CONFIG.PCW_USE_M_AXI_GP0 {1} CONFIG.PCW_USE_S_AXI_GP0 {0} CONFIG.preset {ZC706*}  ] $processing_system7_0

  # Create instance: xlconcat_1, and set properties
  set xlconcat_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_1 ]
  set_property -dict [ list CONFIG.NUM_PORTS {5}  ] $xlconcat_1

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list CONFIG.CONST_VAL {0}  ] $xlconstant_0

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins DDR] [get_bd_intf_pins processing_system7_0/DDR]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins FIXED_IO] [get_bd_intf_pins processing_system7_0/FIXED_IO]
  connect_bd_intf_net -intf_net Conn3 [get_bd_intf_pins M_AXI_GP0] [get_bd_intf_pins processing_system7_0/M_AXI_GP0]
  connect_bd_intf_net -intf_net axi_iic_1_IIC [get_bd_intf_pins IIC] [get_bd_intf_pins axi_iic_1/IIC]
  connect_bd_intf_net -intf_net axi_interconnect_1_M00_AXI [get_bd_intf_pins s_axi2] [get_bd_intf_pins axi_intc_1/s_axi]
  connect_bd_intf_net -intf_net axi_interconnect_1_M03_AXI [get_bd_intf_pins S_AXI3] [get_bd_intf_pins axi_timer_1/S_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_1_M06_AXI [get_bd_intf_pins S_AXI] [get_bd_intf_pins axi_iic_1/S_AXI]

  # Create port connections
  connect_bd_net -net In1_1 [get_bd_pins In1] [get_bd_pins xlconcat_1/In1]
  connect_bd_net -net axi_iic_1_gpo [get_bd_pins gpo] [get_bd_pins axi_iic_1/gpo]
  connect_bd_net -net axi_iic_1_iic2intc_irpt [get_bd_pins axi_iic_1/iic2intc_irpt] [get_bd_pins xlconcat_1/In4]
  connect_bd_net -net axi_intc_1_irq [get_bd_pins axi_intc_1/irq] [get_bd_pins processing_system7_0/Core0_nFIQ]
  connect_bd_net -net axi_timer_1_interrupt [get_bd_pins axi_timer_1/interrupt] [get_bd_pins xlconcat_1/In2]
  connect_bd_net -net clk_wiz_1_clk_out3 [get_bd_pins s_axi_aclk] [get_bd_pins axi_iic_1/s_axi_aclk] [get_bd_pins axi_intc_1/s_axi_aclk] [get_bd_pins axi_timer_1/s_axi_aclk] [get_bd_pins proc_sys_reset_1/slowest_sync_clk] [get_bd_pins processing_system7_0/M_AXI_GP0_ACLK]
  connect_bd_net -net clk_wiz_1_locked [get_bd_pins dcm_locked] [get_bd_pins proc_sys_reset_1/dcm_locked]
  connect_bd_net -net proc_sys_reset_1_bus_struct_reset [get_bd_pins bus_struct_reset] [get_bd_pins proc_sys_reset_1/bus_struct_reset]
  connect_bd_net -net proc_sys_reset_1_interconnect_aresetn [get_bd_pins interconnect_aresetn] [get_bd_pins proc_sys_reset_1/interconnect_aresetn]
  connect_bd_net -net proc_sys_reset_1_peripheral_aresetn [get_bd_pins peripheral_aresetn] [get_bd_pins axi_iic_1/s_axi_aresetn] [get_bd_pins axi_intc_1/s_axi_aresetn] [get_bd_pins axi_timer_1/s_axi_aresetn] [get_bd_pins proc_sys_reset_1/peripheral_aresetn]
  connect_bd_net -net proc_sys_reset_1_peripheral_reset [get_bd_pins peripheral_reset] [get_bd_pins proc_sys_reset_1/peripheral_reset]
  connect_bd_net -net processing_system7_0_FCLK_RESET0_N [get_bd_pins FCLK_RESET0_N] [get_bd_pins proc_sys_reset_1/ext_reset_in] [get_bd_pins processing_system7_0/FCLK_RESET0_N]
  connect_bd_net -net xlconcat_1_dout [get_bd_pins axi_intc_1/intr] [get_bd_pins xlconcat_1/dout]
  set_property -dict [ list HDL_ATTRIBUTE.MARK_DEBUG {true}  ] [get_bd_nets xlconcat_1_dout]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins xlconcat_1/In0] [get_bd_pins xlconcat_1/In3] [get_bd_pins xlconstant_0/dout]
  
  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set DDR [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:ddrx_rtl:1.0 DDR ]
  set FIXED_IO [ create_bd_intf_port -mode Master -vlnv xilinx.com:display_processing_system7:fixedio_rtl:1.0 FIXED_IO ]
  set SYS_CLK [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 SYS_CLK ]
  set_property -dict [ list CONFIG.FREQ_HZ {200000000}  ] $SYS_CLK
  set dp_mainlink [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:dp_main_lnk_rtl:1.0 dp_mainlink ]
  set iic2inctc_irpt [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:iic_rtl:1.0 iic2inctc_irpt ]

  # Create ports
  set aux_tx_io_n [ create_bd_port -dir IO aux_tx_io_n ]
  set aux_tx_io_p [ create_bd_port -dir IO aux_tx_io_p ]
  set clk_out2 [ create_bd_port -dir O -type clk clk_out2 ]
  set gpo [ create_bd_port -dir O -from 1 -to 0 gpo ]
  set tx_hpd [ create_bd_port -dir I tx_hpd ]

  # Create instance: axi4_lite_peripherals
  create_hier_cell_axi4_lite_peripherals [current_bd_instance .] axi4_lite_peripherals

  # Create instance: axi_interconnect_1, and set properties
  set axi_interconnect_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_1 ]
  set_property -dict [ list CONFIG.NUM_MI {5}  ] $axi_interconnect_1

  # Create instance: clk_wiz_1, and set properties
  set clk_wiz_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:5.1 clk_wiz_1 ]
  set_property -dict [ list CONFIG.CLKIN1_JITTER_PS {50.0} CONFIG.CLKOUT1_JITTER {87.550} CONFIG.CLKOUT1_PHASE_ERROR {77.113} CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {200.000} CONFIG.CLKOUT2_JITTER {94.412} CONFIG.CLKOUT2_PHASE_ERROR {77.113} CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {135.000} CONFIG.CLKOUT2_USED {true} CONFIG.CLKOUT3_JITTER {114.355} CONFIG.CLKOUT3_PHASE_ERROR {77.113} CONFIG.CLKOUT3_REQUESTED_OUT_FREQ {50.000} CONFIG.CLKOUT3_USED {true} CONFIG.MMCM_CLKFBOUT_MULT_F {6.750} CONFIG.MMCM_CLKIN1_PERIOD {5.0} CONFIG.PRIM_IN_FREQ {200.000} CONFIG.PRIM_SOURCE {Differential_clock_capable_pin}  ] $clk_wiz_1

  # Create instance: displayport_0, and set properties
  set displayport_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:displayport:5.0 displayport_0 ]
  set_property -dict [ list CONFIG.Link_Rate {2.7} CONFIG.Protocol_Selection {DP_1_1_A} CONFIG.Quad_Pixel_Enable {true} CONFIG.aux_io_type {0}  ] $displayport_0

  # Create instance: pattern_gen_sub_system
  create_hier_cell_pattern_gen_sub_system [current_bd_instance .] pattern_gen_sub_system

  # Create instance: util_vector_logic_0, and set properties
  set util_vector_logic_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_0 ]
  set_property -dict [ list CONFIG.C_OPERATION {not} CONFIG.C_SIZE {1}  ] $util_vector_logic_0

  # Create instance: vid_clkgen_0, and set properties
  set vid_clkgen_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:vid_clkgen:1.0 vid_clkgen_0 ]

  # Create interface connections
  connect_bd_intf_net -intf_net S00_AXI_1 [get_bd_intf_pins axi4_lite_peripherals/M_AXI_GP0] [get_bd_intf_pins axi_interconnect_1/S00_AXI]
  connect_bd_intf_net -intf_net SYS_CLK_1 [get_bd_intf_ports SYS_CLK] [get_bd_intf_pins clk_wiz_1/CLK_IN1_D]
  connect_bd_intf_net -intf_net S_AXI3_1 [get_bd_intf_pins axi4_lite_peripherals/S_AXI3] [get_bd_intf_pins axi_interconnect_1/M02_AXI]
  connect_bd_intf_net -intf_net S_AXI_1 [get_bd_intf_pins axi4_lite_peripherals/S_AXI] [get_bd_intf_pins axi_interconnect_1/M04_AXI]
  connect_bd_intf_net -intf_net axi4_lite_peripherals_DDR [get_bd_intf_ports DDR] [get_bd_intf_pins axi4_lite_peripherals/DDR]
  connect_bd_intf_net -intf_net axi4_lite_peripherals_FIXED_IO [get_bd_intf_ports FIXED_IO] [get_bd_intf_pins axi4_lite_peripherals/FIXED_IO]
  connect_bd_intf_net -intf_net axi_iic_1_IIC [get_bd_intf_ports iic2inctc_irpt] [get_bd_intf_pins axi4_lite_peripherals/IIC]
  connect_bd_intf_net -intf_net axi_interconnect_1_M00_AXI [get_bd_intf_pins axi4_lite_peripherals/s_axi2] [get_bd_intf_pins axi_interconnect_1/M00_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_1_M01_AXI [get_bd_intf_pins axi_interconnect_1/M01_AXI] [get_bd_intf_pins pattern_gen_sub_system/AXI4_LITE]
  connect_bd_intf_net -intf_net axi_interconnect_1_M03_AXI [get_bd_intf_pins axi_interconnect_1/M03_AXI] [get_bd_intf_pins displayport_0/dp_s_axilite]
  set_property -dict [ list HDL_ATTRIBUTE.MARK_DEBUG {true}  ] [get_bd_intf_nets axi_interconnect_1_M03_AXI]
  connect_bd_intf_net -intf_net displayport_0_dp_mainlink [get_bd_intf_ports dp_mainlink] [get_bd_intf_pins displayport_0/dp_mainlink]

  # Create port connections
  connect_bd_net -net Net [get_bd_ports aux_tx_io_p] [get_bd_pins displayport_0/aux_tx_io_p]
  connect_bd_net -net Net1 [get_bd_ports aux_tx_io_n] [get_bd_pins displayport_0/aux_tx_io_n]
  connect_bd_net -net axi4_lite_peripherals_FCLK_RESET0_N [get_bd_pins axi4_lite_peripherals/FCLK_RESET0_N] [get_bd_pins util_vector_logic_0/Op1]
  set_property -dict [ list HDL_ATTRIBUTE.MARK_DEBUG {true}  ] [get_bd_nets axi4_lite_peripherals_FCLK_RESET0_N]
  connect_bd_net -net axi4_lite_peripherals_peripheral_reset [get_bd_pins axi4_lite_peripherals/peripheral_reset] [get_bd_pins pattern_gen_sub_system/apb_reset]
  connect_bd_net -net axi_iic_1_gpo [get_bd_ports gpo] [get_bd_pins axi4_lite_peripherals/gpo]
  connect_bd_net -net clk_wiz_1_clk_out1 [get_bd_pins clk_wiz_1/clk_out1] [get_bd_pins vid_clkgen_0/progclk_i]
  connect_bd_net -net clk_wiz_1_clk_out2 [get_bd_ports clk_out2] [get_bd_pins clk_wiz_1/clk_out2]
  connect_bd_net -net clk_wiz_1_clk_out3 [get_bd_pins axi4_lite_peripherals/s_axi_aclk] [get_bd_pins axi_interconnect_1/ACLK] [get_bd_pins axi_interconnect_1/M00_ACLK] [get_bd_pins axi_interconnect_1/M01_ACLK] [get_bd_pins axi_interconnect_1/M02_ACLK] [get_bd_pins axi_interconnect_1/M03_ACLK] [get_bd_pins axi_interconnect_1/M04_ACLK] [get_bd_pins axi_interconnect_1/S00_ACLK] [get_bd_pins clk_wiz_1/clk_out3] [get_bd_pins displayport_0/s_axi_aclk] [get_bd_pins pattern_gen_sub_system/s_axi_aclk]
  connect_bd_net -net clk_wiz_1_locked [get_bd_pins axi4_lite_peripherals/dcm_locked] [get_bd_pins clk_wiz_1/locked]
  connect_bd_net -net displayport_0_axi_int [get_bd_pins axi4_lite_peripherals/In1] [get_bd_pins displayport_0/axi_int]
  connect_bd_net -net displayport_0_lnk_clk [get_bd_pins displayport_0/lnk_clk] [get_bd_pins vid_clkgen_0/refclk_i]
  connect_bd_net -net pattern_gen_sub_system_Res [get_bd_pins displayport_0/tx_vid_rst] [get_bd_pins pattern_gen_sub_system/Res]
  connect_bd_net -net pattern_gen_sub_system_sw_vid_reset [get_bd_pins pattern_gen_sub_system/sw_vid_reset] [get_bd_pins vid_clkgen_0/reset_i]
  connect_bd_net -net pattern_gen_sub_system_vid_clk_D [get_bd_pins pattern_gen_sub_system/vid_clk_D] [get_bd_pins vid_clkgen_0/D_i]
  connect_bd_net -net pattern_gen_sub_system_vid_clk_M [get_bd_pins pattern_gen_sub_system/vid_clk_M] [get_bd_pins vid_clkgen_0/M_i]
  connect_bd_net -net pattern_gen_sub_system_vid_clk_prog [get_bd_pins pattern_gen_sub_system/vid_clk_prog] [get_bd_pins vid_clkgen_0/prog_i]
  connect_bd_net -net pattern_gen_sub_system_vid_enable [get_bd_pins displayport_0/tx_vid_enable] [get_bd_pins pattern_gen_sub_system/vid_enable]
  connect_bd_net -net pattern_gen_sub_system_vid_hsync [get_bd_pins displayport_0/tx_vid_hsync] [get_bd_pins pattern_gen_sub_system/vid_hsync]
  connect_bd_net -net pattern_gen_sub_system_vid_oddeven [get_bd_pins displayport_0/tx_vid_oddeven] [get_bd_pins pattern_gen_sub_system/vid_oddeven]
  connect_bd_net -net pattern_gen_sub_system_vid_pixel0 [get_bd_pins displayport_0/tx_vid_pixel0] [get_bd_pins pattern_gen_sub_system/vid_pixel0]
  connect_bd_net -net pattern_gen_sub_system_vid_pixel1 [get_bd_pins displayport_0/tx_vid_pixel1] [get_bd_pins pattern_gen_sub_system/vid_pixel1]
  connect_bd_net -net pattern_gen_sub_system_vid_pixel2 [get_bd_pins displayport_0/tx_vid_pixel2] [get_bd_pins pattern_gen_sub_system/vid_pixel2]
  connect_bd_net -net pattern_gen_sub_system_vid_pixel3 [get_bd_pins displayport_0/tx_vid_pixel3] [get_bd_pins pattern_gen_sub_system/vid_pixel3]
  connect_bd_net -net pattern_gen_sub_system_vid_vsync [get_bd_pins displayport_0/tx_vid_vsync] [get_bd_pins pattern_gen_sub_system/vid_vsync]
  connect_bd_net -net proc_sys_reset_1_interconnect_aresetn [get_bd_pins axi4_lite_peripherals/interconnect_aresetn] [get_bd_pins axi_interconnect_1/ARESETN]
  connect_bd_net -net proc_sys_reset_1_peripheral_aresetn [get_bd_pins axi4_lite_peripherals/peripheral_aresetn] [get_bd_pins axi_interconnect_1/M00_ARESETN] [get_bd_pins axi_interconnect_1/M01_ARESETN] [get_bd_pins axi_interconnect_1/M02_ARESETN] [get_bd_pins axi_interconnect_1/M03_ARESETN] [get_bd_pins axi_interconnect_1/M04_ARESETN] [get_bd_pins axi_interconnect_1/S00_ARESETN] [get_bd_pins displayport_0/s_axi_aresetn] [get_bd_pins pattern_gen_sub_system/s_axi_aresetn]
  connect_bd_net -net tx_hpd_1 [get_bd_ports tx_hpd] [get_bd_pins displayport_0/tx_hpd]
  connect_bd_net -net util_vector_logic_0_Res [get_bd_pins clk_wiz_1/reset] [get_bd_pins util_vector_logic_0/Res]
  connect_bd_net -net vid_clkgen_0_locked_o [get_bd_pins pattern_gen_sub_system/Op1] [get_bd_pins vid_clkgen_0/locked_o]
  connect_bd_net -net vid_clkgen_0_vid_clk_o [get_bd_pins displayport_0/tx_vid_clk] [get_bd_pins pattern_gen_sub_system/vid_clk] [get_bd_pins vid_clkgen_0/vid_clk_o]

  # Create address segments
  create_bd_addr_seg -range 0x10000 -offset 0x40800000 [get_bd_addr_spaces axi4_lite_peripherals/processing_system7_0/Data] [get_bd_addr_segs axi4_lite_peripherals/axi_iic_1/S_AXI/Reg] SEG_axi_iic_1_Reg
  create_bd_addr_seg -range 0x10000 -offset 0x41200000 [get_bd_addr_spaces axi4_lite_peripherals/processing_system7_0/Data] [get_bd_addr_segs axi4_lite_peripherals/axi_intc_1/s_axi/Reg] SEG_axi_intc_1_Reg
  create_bd_addr_seg -range 0x10000 -offset 0x41C00000 [get_bd_addr_spaces axi4_lite_peripherals/processing_system7_0/Data] [get_bd_addr_segs axi4_lite_peripherals/axi_timer_1/S_AXI/Reg] SEG_axi_timer_1_Reg
  create_bd_addr_seg -range 0x10000 -offset 0x44A00000 [get_bd_addr_spaces axi4_lite_peripherals/processing_system7_0/Data] [get_bd_addr_segs displayport_0/dp_s_axilite/Reg] SEG_displayport_0_Reg
  create_bd_addr_seg -range 0x10000 -offset 0x42000000 [get_bd_addr_spaces axi4_lite_peripherals/processing_system7_0/Data] [get_bd_addr_segs pattern_gen_sub_system/video_pat_gen_0/vid_pat_apb_inf/addr_block] SEG_video_pat_gen_0_addr_block
  

  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


