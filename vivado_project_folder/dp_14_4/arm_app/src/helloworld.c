/*
 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xuartps_hw.h"

//void print(char *str);

#include "xparameters.h"
int main()
{
    init_platform();
    print("Hello World\n\r");
/*
    print("Hello World\n\r");
    u32 readmode = XUartPs_ReadReg(XPAR_PS7_UART_1_BASEADDR,XUARTPS_MR_OFFSET);
    xil_printf("read mode %d\n\r",readmode);
    readmode = 1;
    //XUartPs_WriteReg(XPAR_PS7_UART_1_BASEADDR,XUARTPS_MR_OFFSET,readmode);

    readmode = XUartPs_ReadReg(XPAR_PS7_UART_1_BASEADDR,XUARTPS_IER_OFFSET);
    xil_printf("XUARTPS_IER_OFFSET %d\n\r",readmode);
    readmode = 1;
    XUartPs_WriteReg(XPAR_PS7_UART_1_BASEADDR,XUARTPS_IER_OFFSET,readmode);

    readmode = XUartPs_ReadReg(XPAR_PS7_UART_1_BASEADDR,XUARTPS_IER_OFFSET);
    xil_printf("XUARTPS_IER_OFFSET %d\n\r",readmode);

    readmode = XUartPs_ReadReg(XPAR_PS7_UART_1_BASEADDR,XUARTPS_IDR_OFFSET);
    xil_printf("XUARTPS_IDR_OFFSET %d\n\r",readmode);

    readmode = XUartPs_ReadReg(XPAR_PS7_UART_1_BASEADDR,XUARTPS_IMR_OFFSET);
    xil_printf("XUARTPS_IMR_OFFSET %d\n\r",readmode);

   	   readmode = XUartPs_ReadReg(XPAR_PS7_UART_1_BASEADDR,XUARTPS_ISR_OFFSET);
   	   xil_printf("XUARTPS_ISR_OFFSET %d\n\r",readmode);
*/
    return 0;
}
