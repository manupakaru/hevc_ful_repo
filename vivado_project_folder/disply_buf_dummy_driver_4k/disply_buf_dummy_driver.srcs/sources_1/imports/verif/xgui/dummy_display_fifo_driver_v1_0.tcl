# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
	set Page0 [ipgui::add_page $IPINST -name "Page 0" -layout vertical]
	set Component_Name [ipgui::add_param $IPINST -parent $Page0 -name Component_Name]
	set DPB_STATUS_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name DPB_STATUS_WIDTH]
	set POC_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name POC_WIDTH]
	set NUM_NEG_POS_POC_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name NUM_NEG_POS_POC_WIDTH]
	set LOG2_MIN_DU_SIZE [ipgui::add_param $IPINST -parent $Page0 -name LOG2_MIN_DU_SIZE]
	set X11_ADDR_WDTH [ipgui::add_param $IPINST -parent $Page0 -name X11_ADDR_WDTH]
	set REF_POC_LIST5_DATA_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name REF_POC_LIST5_DATA_WIDTH]
	set DPB_DATA_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name DPB_DATA_WIDTH]
	set DPB_FRAME_OFFSET_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name DPB_FRAME_OFFSET_WIDTH]
	set RPS_ENTRY_DATA_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name RPS_ENTRY_DATA_WIDTH]
	set RPS_HEADER_DATA_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name RPS_HEADER_DATA_WIDTH]
	set RPS_HEADER_ADDR_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name RPS_HEADER_ADDR_WIDTH]
	set CH_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name CH_WIDTH]
	set YY_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name YY_WIDTH]
	set MV_C_INT_WIDTH_LOW [ipgui::add_param $IPINST -parent $Page0 -name MV_C_INT_WIDTH_LOW]
	set MV_C_FRAC_WIDTH_HIGH [ipgui::add_param $IPINST -parent $Page0 -name MV_C_FRAC_WIDTH_HIGH]
	set MV_L_FRAC_WIDTH_HIGH [ipgui::add_param $IPINST -parent $Page0 -name MV_L_FRAC_WIDTH_HIGH]
	set MV_L_INT_WIDTH_LOW [ipgui::add_param $IPINST -parent $Page0 -name MV_L_INT_WIDTH_LOW]
	set MAX_MV_PER_CU [ipgui::add_param $IPINST -parent $Page0 -name MAX_MV_PER_CU]
	set NUM_IN_TO_8_FILTER [ipgui::add_param $IPINST -parent $Page0 -name NUM_IN_TO_8_FILTER]
	set FILTER_PIXEL_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name FILTER_PIXEL_WIDTH]
	set STEP_8x8_IN_PUS [ipgui::add_param $IPINST -parent $Page0 -name STEP_8x8_IN_PUS]
	set SAO_OUT_FIFO_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name SAO_OUT_FIFO_WIDTH]
	set NUM_BI_PRED_CANDS_TYPES [ipgui::add_param $IPINST -parent $Page0 -name NUM_BI_PRED_CANDS_TYPES]
	set DIST_SCALE_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name DIST_SCALE_WIDTH]
	set NUM_BI_PRED_CANDS [ipgui::add_param $IPINST -parent $Page0 -name NUM_BI_PRED_CANDS]
	set TX_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name TX_WIDTH]
	set MERGE_CAND_TYPE_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name MERGE_CAND_TYPE_WIDTH]
	set MAX_NUM_MERGE_CAND_CONST [ipgui::add_param $IPINST -parent $Page0 -name MAX_NUM_MERGE_CAND_CONST]
	set AMVP_NUM_CAND_CONST [ipgui::add_param $IPINST -parent $Page0 -name AMVP_NUM_CAND_CONST]
	set REF_PIC_LIST5_DPB_STATE_HIGH [ipgui::add_param $IPINST -parent $Page0 -name REF_PIC_LIST5_DPB_STATE_HIGH]
	set REF_PIC_LIST5_POC_RANGE_HIGH [ipgui::add_param $IPINST -parent $Page0 -name REF_PIC_LIST5_POC_RANGE_HIGH]
	set REF_PIC_LIST5_POC_RANGE_LOW [ipgui::add_param $IPINST -parent $Page0 -name REF_PIC_LIST5_POC_RANGE_LOW]
	set REF_PIC_LIST5_DPB_STATE_LOW [ipgui::add_param $IPINST -parent $Page0 -name REF_PIC_LIST5_DPB_STATE_LOW]
	set DPB_POC_RANGE_LOW [ipgui::add_param $IPINST -parent $Page0 -name DPB_POC_RANGE_LOW]
	set DPB_POC_RANGE_HIGH [ipgui::add_param $IPINST -parent $Page0 -name DPB_POC_RANGE_HIGH]
	set MV_COL_AXI_DATA_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name MV_COL_AXI_DATA_WIDTH]
	set MV_FIELD_DATA_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name MV_FIELD_DATA_WIDTH]
	set REF_PIC_LIST_DATA_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name REF_PIC_LIST_DATA_WIDTH]
	set REF_PIC_LIST_ADDR_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name REF_PIC_LIST_ADDR_WIDTH]
	set INTER_TOP_CONFIG_BUS_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name INTER_TOP_CONFIG_BUS_WIDTH]
	set REF_POC_LIST5_ADDR_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name REF_POC_LIST5_ADDR_WIDTH]
	set INTER_TOP_CONFIG_BUS_MODE_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name INTER_TOP_CONFIG_BUS_MODE_WIDTH]
	set DPB_ADDR_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name DPB_ADDR_WIDTH]
	set DPB_REF_PIC_ADDR_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name DPB_REF_PIC_ADDR_WIDTH]
	set REF_PIC_LIST_POC_DATA_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name REF_PIC_LIST_POC_DATA_WIDTH]
	set DPB_FILLED_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name DPB_FILLED_WIDTH]
	set AVAILABLE_CONFIG_BUS_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name AVAILABLE_CONFIG_BUS_WIDTH]
	set LOG2_MIN_TU_SIZE [ipgui::add_param $IPINST -parent $Page0 -name LOG2_MIN_TU_SIZE]
	set LOG2_MIN_PU_SIZE [ipgui::add_param $IPINST -parent $Page0 -name LOG2_MIN_PU_SIZE]
	set Y11_ADDR_WDTH [ipgui::add_param $IPINST -parent $Page0 -name Y11_ADDR_WDTH]
	set Y_ADDR_WDTH [ipgui::add_param $IPINST -parent $Page0 -name Y_ADDR_WDTH]
	set X_ADDR_WDTH [ipgui::add_param $IPINST -parent $Page0 -name X_ADDR_WDTH]
	set RPS_ENTRY_DELTA_POC_RANGE_LOW [ipgui::add_param $IPINST -parent $Page0 -name RPS_ENTRY_DELTA_POC_RANGE_LOW]
	set RPS_HEADER_NUM_DELTA_POC_RANGE_LOW [ipgui::add_param $IPINST -parent $Page0 -name RPS_HEADER_NUM_DELTA_POC_RANGE_LOW]
	set RPS_ENTRY_USED_FLAG_RANGE_HIGH [ipgui::add_param $IPINST -parent $Page0 -name RPS_ENTRY_USED_FLAG_RANGE_HIGH]
	set RPS_ENTRY_DELTA_POC_RANGE_HIGH [ipgui::add_param $IPINST -parent $Page0 -name RPS_ENTRY_DELTA_POC_RANGE_HIGH]
	set RPS_ENTRY_DELTA_POC_MSB [ipgui::add_param $IPINST -parent $Page0 -name RPS_ENTRY_DELTA_POC_MSB]
	set BS_FIFO_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name BS_FIFO_WIDTH]
	set MAX_PIC_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name MAX_PIC_WIDTH]
	set MAX_PIC_HEIGHT [ipgui::add_param $IPINST -parent $Page0 -name MAX_PIC_HEIGHT]
	set CTB_SIZE_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name CTB_SIZE_WIDTH]
	set CB_SIZE_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name CB_SIZE_WIDTH]
	set TB_SIZE_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name TB_SIZE_WIDTH]
	set LOG2_CTB_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name LOG2_CTB_WIDTH]
	set PIC_DIM_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name PIC_DIM_WIDTH]
	set LOG2_MIN_COLLOCATE_SIZE [ipgui::add_param $IPINST -parent $Page0 -name LOG2_MIN_COLLOCATE_SIZE]
	set DBF_SAMPLE_XY_ADDR [ipgui::add_param $IPINST -parent $Page0 -name DBF_SAMPLE_XY_ADDR]
	set RPS_ENTRY_ADDR_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name RPS_ENTRY_ADDR_WIDTH]
	set RPS_HEADER_NUM_POS_POC_RANGE_HIGH [ipgui::add_param $IPINST -parent $Page0 -name RPS_HEADER_NUM_POS_POC_RANGE_HIGH]
	set RPS_HEADER_NUM_POS_POC_RANGE_LOW [ipgui::add_param $IPINST -parent $Page0 -name RPS_HEADER_NUM_POS_POC_RANGE_LOW]
	set RPS_HEADER_NUM_NEG_POC_RANGE_HIGH [ipgui::add_param $IPINST -parent $Page0 -name RPS_HEADER_NUM_NEG_POC_RANGE_HIGH]
	set RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH [ipgui::add_param $IPINST -parent $Page0 -name RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH]
	set RPS_HEADER_NUM_NEG_POC_RANGE_LOW [ipgui::add_param $IPINST -parent $Page0 -name RPS_HEADER_NUM_NEG_POC_RANGE_LOW]
	set RPS_ENTRY_USED_FLAG_RANGE_LOW [ipgui::add_param $IPINST -parent $Page0 -name RPS_ENTRY_USED_FLAG_RANGE_LOW]
}

proc update_PARAM_VALUE.DPB_STATUS_WIDTH { PARAM_VALUE.DPB_STATUS_WIDTH } {
	# Procedure called to update DPB_STATUS_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPB_STATUS_WIDTH { PARAM_VALUE.DPB_STATUS_WIDTH } {
	# Procedure called to validate DPB_STATUS_WIDTH
	return true
}

proc update_PARAM_VALUE.POC_WIDTH { PARAM_VALUE.POC_WIDTH } {
	# Procedure called to update POC_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.POC_WIDTH { PARAM_VALUE.POC_WIDTH } {
	# Procedure called to validate POC_WIDTH
	return true
}

proc update_PARAM_VALUE.NUM_NEG_POS_POC_WIDTH { PARAM_VALUE.NUM_NEG_POS_POC_WIDTH } {
	# Procedure called to update NUM_NEG_POS_POC_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NUM_NEG_POS_POC_WIDTH { PARAM_VALUE.NUM_NEG_POS_POC_WIDTH } {
	# Procedure called to validate NUM_NEG_POS_POC_WIDTH
	return true
}

proc update_PARAM_VALUE.LOG2_MIN_DU_SIZE { PARAM_VALUE.LOG2_MIN_DU_SIZE } {
	# Procedure called to update LOG2_MIN_DU_SIZE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.LOG2_MIN_DU_SIZE { PARAM_VALUE.LOG2_MIN_DU_SIZE } {
	# Procedure called to validate LOG2_MIN_DU_SIZE
	return true
}

proc update_PARAM_VALUE.X11_ADDR_WDTH { PARAM_VALUE.X11_ADDR_WDTH } {
	# Procedure called to update X11_ADDR_WDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.X11_ADDR_WDTH { PARAM_VALUE.X11_ADDR_WDTH } {
	# Procedure called to validate X11_ADDR_WDTH
	return true
}

proc update_PARAM_VALUE.REF_POC_LIST5_DATA_WIDTH { PARAM_VALUE.REF_POC_LIST5_DATA_WIDTH } {
	# Procedure called to update REF_POC_LIST5_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.REF_POC_LIST5_DATA_WIDTH { PARAM_VALUE.REF_POC_LIST5_DATA_WIDTH } {
	# Procedure called to validate REF_POC_LIST5_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.DPB_DATA_WIDTH { PARAM_VALUE.DPB_DATA_WIDTH } {
	# Procedure called to update DPB_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPB_DATA_WIDTH { PARAM_VALUE.DPB_DATA_WIDTH } {
	# Procedure called to validate DPB_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.DPB_FRAME_OFFSET_WIDTH { PARAM_VALUE.DPB_FRAME_OFFSET_WIDTH } {
	# Procedure called to update DPB_FRAME_OFFSET_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPB_FRAME_OFFSET_WIDTH { PARAM_VALUE.DPB_FRAME_OFFSET_WIDTH } {
	# Procedure called to validate DPB_FRAME_OFFSET_WIDTH
	return true
}

proc update_PARAM_VALUE.RPS_ENTRY_DATA_WIDTH { PARAM_VALUE.RPS_ENTRY_DATA_WIDTH } {
	# Procedure called to update RPS_ENTRY_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RPS_ENTRY_DATA_WIDTH { PARAM_VALUE.RPS_ENTRY_DATA_WIDTH } {
	# Procedure called to validate RPS_ENTRY_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.RPS_HEADER_DATA_WIDTH { PARAM_VALUE.RPS_HEADER_DATA_WIDTH } {
	# Procedure called to update RPS_HEADER_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RPS_HEADER_DATA_WIDTH { PARAM_VALUE.RPS_HEADER_DATA_WIDTH } {
	# Procedure called to validate RPS_HEADER_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.RPS_HEADER_ADDR_WIDTH { PARAM_VALUE.RPS_HEADER_ADDR_WIDTH } {
	# Procedure called to update RPS_HEADER_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RPS_HEADER_ADDR_WIDTH { PARAM_VALUE.RPS_HEADER_ADDR_WIDTH } {
	# Procedure called to validate RPS_HEADER_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.CH_WIDTH { PARAM_VALUE.CH_WIDTH } {
	# Procedure called to update CH_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CH_WIDTH { PARAM_VALUE.CH_WIDTH } {
	# Procedure called to validate CH_WIDTH
	return true
}

proc update_PARAM_VALUE.YY_WIDTH { PARAM_VALUE.YY_WIDTH } {
	# Procedure called to update YY_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.YY_WIDTH { PARAM_VALUE.YY_WIDTH } {
	# Procedure called to validate YY_WIDTH
	return true
}

proc update_PARAM_VALUE.MV_C_INT_WIDTH_LOW { PARAM_VALUE.MV_C_INT_WIDTH_LOW } {
	# Procedure called to update MV_C_INT_WIDTH_LOW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MV_C_INT_WIDTH_LOW { PARAM_VALUE.MV_C_INT_WIDTH_LOW } {
	# Procedure called to validate MV_C_INT_WIDTH_LOW
	return true
}

proc update_PARAM_VALUE.MV_C_FRAC_WIDTH_HIGH { PARAM_VALUE.MV_C_FRAC_WIDTH_HIGH } {
	# Procedure called to update MV_C_FRAC_WIDTH_HIGH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MV_C_FRAC_WIDTH_HIGH { PARAM_VALUE.MV_C_FRAC_WIDTH_HIGH } {
	# Procedure called to validate MV_C_FRAC_WIDTH_HIGH
	return true
}

proc update_PARAM_VALUE.MV_L_FRAC_WIDTH_HIGH { PARAM_VALUE.MV_L_FRAC_WIDTH_HIGH } {
	# Procedure called to update MV_L_FRAC_WIDTH_HIGH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MV_L_FRAC_WIDTH_HIGH { PARAM_VALUE.MV_L_FRAC_WIDTH_HIGH } {
	# Procedure called to validate MV_L_FRAC_WIDTH_HIGH
	return true
}

proc update_PARAM_VALUE.MV_L_INT_WIDTH_LOW { PARAM_VALUE.MV_L_INT_WIDTH_LOW } {
	# Procedure called to update MV_L_INT_WIDTH_LOW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MV_L_INT_WIDTH_LOW { PARAM_VALUE.MV_L_INT_WIDTH_LOW } {
	# Procedure called to validate MV_L_INT_WIDTH_LOW
	return true
}

proc update_PARAM_VALUE.MAX_MV_PER_CU { PARAM_VALUE.MAX_MV_PER_CU } {
	# Procedure called to update MAX_MV_PER_CU when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MAX_MV_PER_CU { PARAM_VALUE.MAX_MV_PER_CU } {
	# Procedure called to validate MAX_MV_PER_CU
	return true
}

proc update_PARAM_VALUE.NUM_IN_TO_8_FILTER { PARAM_VALUE.NUM_IN_TO_8_FILTER } {
	# Procedure called to update NUM_IN_TO_8_FILTER when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NUM_IN_TO_8_FILTER { PARAM_VALUE.NUM_IN_TO_8_FILTER } {
	# Procedure called to validate NUM_IN_TO_8_FILTER
	return true
}

proc update_PARAM_VALUE.FILTER_PIXEL_WIDTH { PARAM_VALUE.FILTER_PIXEL_WIDTH } {
	# Procedure called to update FILTER_PIXEL_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.FILTER_PIXEL_WIDTH { PARAM_VALUE.FILTER_PIXEL_WIDTH } {
	# Procedure called to validate FILTER_PIXEL_WIDTH
	return true
}

proc update_PARAM_VALUE.STEP_8x8_IN_PUS { PARAM_VALUE.STEP_8x8_IN_PUS } {
	# Procedure called to update STEP_8x8_IN_PUS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STEP_8x8_IN_PUS { PARAM_VALUE.STEP_8x8_IN_PUS } {
	# Procedure called to validate STEP_8x8_IN_PUS
	return true
}

proc update_PARAM_VALUE.SAO_OUT_FIFO_WIDTH { PARAM_VALUE.SAO_OUT_FIFO_WIDTH } {
	# Procedure called to update SAO_OUT_FIFO_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SAO_OUT_FIFO_WIDTH { PARAM_VALUE.SAO_OUT_FIFO_WIDTH } {
	# Procedure called to validate SAO_OUT_FIFO_WIDTH
	return true
}

proc update_PARAM_VALUE.NUM_BI_PRED_CANDS_TYPES { PARAM_VALUE.NUM_BI_PRED_CANDS_TYPES } {
	# Procedure called to update NUM_BI_PRED_CANDS_TYPES when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NUM_BI_PRED_CANDS_TYPES { PARAM_VALUE.NUM_BI_PRED_CANDS_TYPES } {
	# Procedure called to validate NUM_BI_PRED_CANDS_TYPES
	return true
}

proc update_PARAM_VALUE.DIST_SCALE_WIDTH { PARAM_VALUE.DIST_SCALE_WIDTH } {
	# Procedure called to update DIST_SCALE_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DIST_SCALE_WIDTH { PARAM_VALUE.DIST_SCALE_WIDTH } {
	# Procedure called to validate DIST_SCALE_WIDTH
	return true
}

proc update_PARAM_VALUE.NUM_BI_PRED_CANDS { PARAM_VALUE.NUM_BI_PRED_CANDS } {
	# Procedure called to update NUM_BI_PRED_CANDS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NUM_BI_PRED_CANDS { PARAM_VALUE.NUM_BI_PRED_CANDS } {
	# Procedure called to validate NUM_BI_PRED_CANDS
	return true
}

proc update_PARAM_VALUE.TX_WIDTH { PARAM_VALUE.TX_WIDTH } {
	# Procedure called to update TX_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.TX_WIDTH { PARAM_VALUE.TX_WIDTH } {
	# Procedure called to validate TX_WIDTH
	return true
}

proc update_PARAM_VALUE.MERGE_CAND_TYPE_WIDTH { PARAM_VALUE.MERGE_CAND_TYPE_WIDTH } {
	# Procedure called to update MERGE_CAND_TYPE_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MERGE_CAND_TYPE_WIDTH { PARAM_VALUE.MERGE_CAND_TYPE_WIDTH } {
	# Procedure called to validate MERGE_CAND_TYPE_WIDTH
	return true
}

proc update_PARAM_VALUE.MAX_NUM_MERGE_CAND_CONST { PARAM_VALUE.MAX_NUM_MERGE_CAND_CONST } {
	# Procedure called to update MAX_NUM_MERGE_CAND_CONST when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MAX_NUM_MERGE_CAND_CONST { PARAM_VALUE.MAX_NUM_MERGE_CAND_CONST } {
	# Procedure called to validate MAX_NUM_MERGE_CAND_CONST
	return true
}

proc update_PARAM_VALUE.AMVP_NUM_CAND_CONST { PARAM_VALUE.AMVP_NUM_CAND_CONST } {
	# Procedure called to update AMVP_NUM_CAND_CONST when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.AMVP_NUM_CAND_CONST { PARAM_VALUE.AMVP_NUM_CAND_CONST } {
	# Procedure called to validate AMVP_NUM_CAND_CONST
	return true
}

proc update_PARAM_VALUE.REF_PIC_LIST5_DPB_STATE_HIGH { PARAM_VALUE.REF_PIC_LIST5_DPB_STATE_HIGH } {
	# Procedure called to update REF_PIC_LIST5_DPB_STATE_HIGH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.REF_PIC_LIST5_DPB_STATE_HIGH { PARAM_VALUE.REF_PIC_LIST5_DPB_STATE_HIGH } {
	# Procedure called to validate REF_PIC_LIST5_DPB_STATE_HIGH
	return true
}

proc update_PARAM_VALUE.REF_PIC_LIST5_POC_RANGE_HIGH { PARAM_VALUE.REF_PIC_LIST5_POC_RANGE_HIGH } {
	# Procedure called to update REF_PIC_LIST5_POC_RANGE_HIGH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.REF_PIC_LIST5_POC_RANGE_HIGH { PARAM_VALUE.REF_PIC_LIST5_POC_RANGE_HIGH } {
	# Procedure called to validate REF_PIC_LIST5_POC_RANGE_HIGH
	return true
}

proc update_PARAM_VALUE.REF_PIC_LIST5_POC_RANGE_LOW { PARAM_VALUE.REF_PIC_LIST5_POC_RANGE_LOW } {
	# Procedure called to update REF_PIC_LIST5_POC_RANGE_LOW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.REF_PIC_LIST5_POC_RANGE_LOW { PARAM_VALUE.REF_PIC_LIST5_POC_RANGE_LOW } {
	# Procedure called to validate REF_PIC_LIST5_POC_RANGE_LOW
	return true
}

proc update_PARAM_VALUE.REF_PIC_LIST5_DPB_STATE_LOW { PARAM_VALUE.REF_PIC_LIST5_DPB_STATE_LOW } {
	# Procedure called to update REF_PIC_LIST5_DPB_STATE_LOW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.REF_PIC_LIST5_DPB_STATE_LOW { PARAM_VALUE.REF_PIC_LIST5_DPB_STATE_LOW } {
	# Procedure called to validate REF_PIC_LIST5_DPB_STATE_LOW
	return true
}

proc update_PARAM_VALUE.DPB_POC_RANGE_LOW { PARAM_VALUE.DPB_POC_RANGE_LOW } {
	# Procedure called to update DPB_POC_RANGE_LOW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPB_POC_RANGE_LOW { PARAM_VALUE.DPB_POC_RANGE_LOW } {
	# Procedure called to validate DPB_POC_RANGE_LOW
	return true
}

proc update_PARAM_VALUE.DPB_POC_RANGE_HIGH { PARAM_VALUE.DPB_POC_RANGE_HIGH } {
	# Procedure called to update DPB_POC_RANGE_HIGH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPB_POC_RANGE_HIGH { PARAM_VALUE.DPB_POC_RANGE_HIGH } {
	# Procedure called to validate DPB_POC_RANGE_HIGH
	return true
}

proc update_PARAM_VALUE.MV_COL_AXI_DATA_WIDTH { PARAM_VALUE.MV_COL_AXI_DATA_WIDTH } {
	# Procedure called to update MV_COL_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MV_COL_AXI_DATA_WIDTH { PARAM_VALUE.MV_COL_AXI_DATA_WIDTH } {
	# Procedure called to validate MV_COL_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.MV_FIELD_DATA_WIDTH { PARAM_VALUE.MV_FIELD_DATA_WIDTH } {
	# Procedure called to update MV_FIELD_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MV_FIELD_DATA_WIDTH { PARAM_VALUE.MV_FIELD_DATA_WIDTH } {
	# Procedure called to validate MV_FIELD_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.REF_PIC_LIST_DATA_WIDTH { PARAM_VALUE.REF_PIC_LIST_DATA_WIDTH } {
	# Procedure called to update REF_PIC_LIST_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.REF_PIC_LIST_DATA_WIDTH { PARAM_VALUE.REF_PIC_LIST_DATA_WIDTH } {
	# Procedure called to validate REF_PIC_LIST_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.REF_PIC_LIST_ADDR_WIDTH { PARAM_VALUE.REF_PIC_LIST_ADDR_WIDTH } {
	# Procedure called to update REF_PIC_LIST_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.REF_PIC_LIST_ADDR_WIDTH { PARAM_VALUE.REF_PIC_LIST_ADDR_WIDTH } {
	# Procedure called to validate REF_PIC_LIST_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.INTER_TOP_CONFIG_BUS_WIDTH { PARAM_VALUE.INTER_TOP_CONFIG_BUS_WIDTH } {
	# Procedure called to update INTER_TOP_CONFIG_BUS_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.INTER_TOP_CONFIG_BUS_WIDTH { PARAM_VALUE.INTER_TOP_CONFIG_BUS_WIDTH } {
	# Procedure called to validate INTER_TOP_CONFIG_BUS_WIDTH
	return true
}

proc update_PARAM_VALUE.REF_POC_LIST5_ADDR_WIDTH { PARAM_VALUE.REF_POC_LIST5_ADDR_WIDTH } {
	# Procedure called to update REF_POC_LIST5_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.REF_POC_LIST5_ADDR_WIDTH { PARAM_VALUE.REF_POC_LIST5_ADDR_WIDTH } {
	# Procedure called to validate REF_POC_LIST5_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.INTER_TOP_CONFIG_BUS_MODE_WIDTH { PARAM_VALUE.INTER_TOP_CONFIG_BUS_MODE_WIDTH } {
	# Procedure called to update INTER_TOP_CONFIG_BUS_MODE_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.INTER_TOP_CONFIG_BUS_MODE_WIDTH { PARAM_VALUE.INTER_TOP_CONFIG_BUS_MODE_WIDTH } {
	# Procedure called to validate INTER_TOP_CONFIG_BUS_MODE_WIDTH
	return true
}

proc update_PARAM_VALUE.DPB_ADDR_WIDTH { PARAM_VALUE.DPB_ADDR_WIDTH } {
	# Procedure called to update DPB_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPB_ADDR_WIDTH { PARAM_VALUE.DPB_ADDR_WIDTH } {
	# Procedure called to validate DPB_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.DPB_REF_PIC_ADDR_WIDTH { PARAM_VALUE.DPB_REF_PIC_ADDR_WIDTH } {
	# Procedure called to update DPB_REF_PIC_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPB_REF_PIC_ADDR_WIDTH { PARAM_VALUE.DPB_REF_PIC_ADDR_WIDTH } {
	# Procedure called to validate DPB_REF_PIC_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.REF_PIC_LIST_POC_DATA_WIDTH { PARAM_VALUE.REF_PIC_LIST_POC_DATA_WIDTH } {
	# Procedure called to update REF_PIC_LIST_POC_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.REF_PIC_LIST_POC_DATA_WIDTH { PARAM_VALUE.REF_PIC_LIST_POC_DATA_WIDTH } {
	# Procedure called to validate REF_PIC_LIST_POC_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.DPB_FILLED_WIDTH { PARAM_VALUE.DPB_FILLED_WIDTH } {
	# Procedure called to update DPB_FILLED_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPB_FILLED_WIDTH { PARAM_VALUE.DPB_FILLED_WIDTH } {
	# Procedure called to validate DPB_FILLED_WIDTH
	return true
}

proc update_PARAM_VALUE.AVAILABLE_CONFIG_BUS_WIDTH { PARAM_VALUE.AVAILABLE_CONFIG_BUS_WIDTH } {
	# Procedure called to update AVAILABLE_CONFIG_BUS_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.AVAILABLE_CONFIG_BUS_WIDTH { PARAM_VALUE.AVAILABLE_CONFIG_BUS_WIDTH } {
	# Procedure called to validate AVAILABLE_CONFIG_BUS_WIDTH
	return true
}

proc update_PARAM_VALUE.LOG2_MIN_TU_SIZE { PARAM_VALUE.LOG2_MIN_TU_SIZE } {
	# Procedure called to update LOG2_MIN_TU_SIZE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.LOG2_MIN_TU_SIZE { PARAM_VALUE.LOG2_MIN_TU_SIZE } {
	# Procedure called to validate LOG2_MIN_TU_SIZE
	return true
}

proc update_PARAM_VALUE.LOG2_MIN_PU_SIZE { PARAM_VALUE.LOG2_MIN_PU_SIZE } {
	# Procedure called to update LOG2_MIN_PU_SIZE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.LOG2_MIN_PU_SIZE { PARAM_VALUE.LOG2_MIN_PU_SIZE } {
	# Procedure called to validate LOG2_MIN_PU_SIZE
	return true
}

proc update_PARAM_VALUE.Y11_ADDR_WDTH { PARAM_VALUE.Y11_ADDR_WDTH } {
	# Procedure called to update Y11_ADDR_WDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.Y11_ADDR_WDTH { PARAM_VALUE.Y11_ADDR_WDTH } {
	# Procedure called to validate Y11_ADDR_WDTH
	return true
}

proc update_PARAM_VALUE.Y_ADDR_WDTH { PARAM_VALUE.Y_ADDR_WDTH } {
	# Procedure called to update Y_ADDR_WDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.Y_ADDR_WDTH { PARAM_VALUE.Y_ADDR_WDTH } {
	# Procedure called to validate Y_ADDR_WDTH
	return true
}

proc update_PARAM_VALUE.X_ADDR_WDTH { PARAM_VALUE.X_ADDR_WDTH } {
	# Procedure called to update X_ADDR_WDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.X_ADDR_WDTH { PARAM_VALUE.X_ADDR_WDTH } {
	# Procedure called to validate X_ADDR_WDTH
	return true
}

proc update_PARAM_VALUE.RPS_ENTRY_DELTA_POC_RANGE_LOW { PARAM_VALUE.RPS_ENTRY_DELTA_POC_RANGE_LOW } {
	# Procedure called to update RPS_ENTRY_DELTA_POC_RANGE_LOW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RPS_ENTRY_DELTA_POC_RANGE_LOW { PARAM_VALUE.RPS_ENTRY_DELTA_POC_RANGE_LOW } {
	# Procedure called to validate RPS_ENTRY_DELTA_POC_RANGE_LOW
	return true
}

proc update_PARAM_VALUE.RPS_HEADER_NUM_DELTA_POC_RANGE_LOW { PARAM_VALUE.RPS_HEADER_NUM_DELTA_POC_RANGE_LOW } {
	# Procedure called to update RPS_HEADER_NUM_DELTA_POC_RANGE_LOW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RPS_HEADER_NUM_DELTA_POC_RANGE_LOW { PARAM_VALUE.RPS_HEADER_NUM_DELTA_POC_RANGE_LOW } {
	# Procedure called to validate RPS_HEADER_NUM_DELTA_POC_RANGE_LOW
	return true
}

proc update_PARAM_VALUE.RPS_ENTRY_USED_FLAG_RANGE_HIGH { PARAM_VALUE.RPS_ENTRY_USED_FLAG_RANGE_HIGH } {
	# Procedure called to update RPS_ENTRY_USED_FLAG_RANGE_HIGH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RPS_ENTRY_USED_FLAG_RANGE_HIGH { PARAM_VALUE.RPS_ENTRY_USED_FLAG_RANGE_HIGH } {
	# Procedure called to validate RPS_ENTRY_USED_FLAG_RANGE_HIGH
	return true
}

proc update_PARAM_VALUE.RPS_ENTRY_DELTA_POC_RANGE_HIGH { PARAM_VALUE.RPS_ENTRY_DELTA_POC_RANGE_HIGH } {
	# Procedure called to update RPS_ENTRY_DELTA_POC_RANGE_HIGH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RPS_ENTRY_DELTA_POC_RANGE_HIGH { PARAM_VALUE.RPS_ENTRY_DELTA_POC_RANGE_HIGH } {
	# Procedure called to validate RPS_ENTRY_DELTA_POC_RANGE_HIGH
	return true
}

proc update_PARAM_VALUE.RPS_ENTRY_DELTA_POC_MSB { PARAM_VALUE.RPS_ENTRY_DELTA_POC_MSB } {
	# Procedure called to update RPS_ENTRY_DELTA_POC_MSB when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RPS_ENTRY_DELTA_POC_MSB { PARAM_VALUE.RPS_ENTRY_DELTA_POC_MSB } {
	# Procedure called to validate RPS_ENTRY_DELTA_POC_MSB
	return true
}

proc update_PARAM_VALUE.BS_FIFO_WIDTH { PARAM_VALUE.BS_FIFO_WIDTH } {
	# Procedure called to update BS_FIFO_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.BS_FIFO_WIDTH { PARAM_VALUE.BS_FIFO_WIDTH } {
	# Procedure called to validate BS_FIFO_WIDTH
	return true
}

proc update_PARAM_VALUE.MAX_PIC_WIDTH { PARAM_VALUE.MAX_PIC_WIDTH } {
	# Procedure called to update MAX_PIC_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MAX_PIC_WIDTH { PARAM_VALUE.MAX_PIC_WIDTH } {
	# Procedure called to validate MAX_PIC_WIDTH
	return true
}

proc update_PARAM_VALUE.MAX_PIC_HEIGHT { PARAM_VALUE.MAX_PIC_HEIGHT } {
	# Procedure called to update MAX_PIC_HEIGHT when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MAX_PIC_HEIGHT { PARAM_VALUE.MAX_PIC_HEIGHT } {
	# Procedure called to validate MAX_PIC_HEIGHT
	return true
}

proc update_PARAM_VALUE.CTB_SIZE_WIDTH { PARAM_VALUE.CTB_SIZE_WIDTH } {
	# Procedure called to update CTB_SIZE_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CTB_SIZE_WIDTH { PARAM_VALUE.CTB_SIZE_WIDTH } {
	# Procedure called to validate CTB_SIZE_WIDTH
	return true
}

proc update_PARAM_VALUE.CB_SIZE_WIDTH { PARAM_VALUE.CB_SIZE_WIDTH } {
	# Procedure called to update CB_SIZE_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CB_SIZE_WIDTH { PARAM_VALUE.CB_SIZE_WIDTH } {
	# Procedure called to validate CB_SIZE_WIDTH
	return true
}

proc update_PARAM_VALUE.TB_SIZE_WIDTH { PARAM_VALUE.TB_SIZE_WIDTH } {
	# Procedure called to update TB_SIZE_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.TB_SIZE_WIDTH { PARAM_VALUE.TB_SIZE_WIDTH } {
	# Procedure called to validate TB_SIZE_WIDTH
	return true
}

proc update_PARAM_VALUE.LOG2_CTB_WIDTH { PARAM_VALUE.LOG2_CTB_WIDTH } {
	# Procedure called to update LOG2_CTB_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.LOG2_CTB_WIDTH { PARAM_VALUE.LOG2_CTB_WIDTH } {
	# Procedure called to validate LOG2_CTB_WIDTH
	return true
}

proc update_PARAM_VALUE.PIC_DIM_WIDTH { PARAM_VALUE.PIC_DIM_WIDTH } {
	# Procedure called to update PIC_DIM_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.PIC_DIM_WIDTH { PARAM_VALUE.PIC_DIM_WIDTH } {
	# Procedure called to validate PIC_DIM_WIDTH
	return true
}

proc update_PARAM_VALUE.LOG2_MIN_COLLOCATE_SIZE { PARAM_VALUE.LOG2_MIN_COLLOCATE_SIZE } {
	# Procedure called to update LOG2_MIN_COLLOCATE_SIZE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.LOG2_MIN_COLLOCATE_SIZE { PARAM_VALUE.LOG2_MIN_COLLOCATE_SIZE } {
	# Procedure called to validate LOG2_MIN_COLLOCATE_SIZE
	return true
}

proc update_PARAM_VALUE.DBF_SAMPLE_XY_ADDR { PARAM_VALUE.DBF_SAMPLE_XY_ADDR } {
	# Procedure called to update DBF_SAMPLE_XY_ADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DBF_SAMPLE_XY_ADDR { PARAM_VALUE.DBF_SAMPLE_XY_ADDR } {
	# Procedure called to validate DBF_SAMPLE_XY_ADDR
	return true
}

proc update_PARAM_VALUE.RPS_ENTRY_ADDR_WIDTH { PARAM_VALUE.RPS_ENTRY_ADDR_WIDTH } {
	# Procedure called to update RPS_ENTRY_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RPS_ENTRY_ADDR_WIDTH { PARAM_VALUE.RPS_ENTRY_ADDR_WIDTH } {
	# Procedure called to validate RPS_ENTRY_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.RPS_HEADER_NUM_POS_POC_RANGE_HIGH { PARAM_VALUE.RPS_HEADER_NUM_POS_POC_RANGE_HIGH } {
	# Procedure called to update RPS_HEADER_NUM_POS_POC_RANGE_HIGH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RPS_HEADER_NUM_POS_POC_RANGE_HIGH { PARAM_VALUE.RPS_HEADER_NUM_POS_POC_RANGE_HIGH } {
	# Procedure called to validate RPS_HEADER_NUM_POS_POC_RANGE_HIGH
	return true
}

proc update_PARAM_VALUE.RPS_HEADER_NUM_POS_POC_RANGE_LOW { PARAM_VALUE.RPS_HEADER_NUM_POS_POC_RANGE_LOW } {
	# Procedure called to update RPS_HEADER_NUM_POS_POC_RANGE_LOW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RPS_HEADER_NUM_POS_POC_RANGE_LOW { PARAM_VALUE.RPS_HEADER_NUM_POS_POC_RANGE_LOW } {
	# Procedure called to validate RPS_HEADER_NUM_POS_POC_RANGE_LOW
	return true
}

proc update_PARAM_VALUE.RPS_HEADER_NUM_NEG_POC_RANGE_HIGH { PARAM_VALUE.RPS_HEADER_NUM_NEG_POC_RANGE_HIGH } {
	# Procedure called to update RPS_HEADER_NUM_NEG_POC_RANGE_HIGH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RPS_HEADER_NUM_NEG_POC_RANGE_HIGH { PARAM_VALUE.RPS_HEADER_NUM_NEG_POC_RANGE_HIGH } {
	# Procedure called to validate RPS_HEADER_NUM_NEG_POC_RANGE_HIGH
	return true
}

proc update_PARAM_VALUE.RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH { PARAM_VALUE.RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH } {
	# Procedure called to update RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH { PARAM_VALUE.RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH } {
	# Procedure called to validate RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH
	return true
}

proc update_PARAM_VALUE.RPS_HEADER_NUM_NEG_POC_RANGE_LOW { PARAM_VALUE.RPS_HEADER_NUM_NEG_POC_RANGE_LOW } {
	# Procedure called to update RPS_HEADER_NUM_NEG_POC_RANGE_LOW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RPS_HEADER_NUM_NEG_POC_RANGE_LOW { PARAM_VALUE.RPS_HEADER_NUM_NEG_POC_RANGE_LOW } {
	# Procedure called to validate RPS_HEADER_NUM_NEG_POC_RANGE_LOW
	return true
}

proc update_PARAM_VALUE.RPS_ENTRY_USED_FLAG_RANGE_LOW { PARAM_VALUE.RPS_ENTRY_USED_FLAG_RANGE_LOW } {
	# Procedure called to update RPS_ENTRY_USED_FLAG_RANGE_LOW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RPS_ENTRY_USED_FLAG_RANGE_LOW { PARAM_VALUE.RPS_ENTRY_USED_FLAG_RANGE_LOW } {
	# Procedure called to validate RPS_ENTRY_USED_FLAG_RANGE_LOW
	return true
}


proc update_MODELPARAM_VALUE.MAX_PIC_WIDTH { MODELPARAM_VALUE.MAX_PIC_WIDTH PARAM_VALUE.MAX_PIC_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MAX_PIC_WIDTH}] ${MODELPARAM_VALUE.MAX_PIC_WIDTH}
}

proc update_MODELPARAM_VALUE.MAX_PIC_HEIGHT { MODELPARAM_VALUE.MAX_PIC_HEIGHT PARAM_VALUE.MAX_PIC_HEIGHT } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MAX_PIC_HEIGHT}] ${MODELPARAM_VALUE.MAX_PIC_HEIGHT}
}

proc update_MODELPARAM_VALUE.CTB_SIZE_WIDTH { MODELPARAM_VALUE.CTB_SIZE_WIDTH PARAM_VALUE.CTB_SIZE_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CTB_SIZE_WIDTH}] ${MODELPARAM_VALUE.CTB_SIZE_WIDTH}
}

proc update_MODELPARAM_VALUE.CB_SIZE_WIDTH { MODELPARAM_VALUE.CB_SIZE_WIDTH PARAM_VALUE.CB_SIZE_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CB_SIZE_WIDTH}] ${MODELPARAM_VALUE.CB_SIZE_WIDTH}
}

proc update_MODELPARAM_VALUE.TB_SIZE_WIDTH { MODELPARAM_VALUE.TB_SIZE_WIDTH PARAM_VALUE.TB_SIZE_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.TB_SIZE_WIDTH}] ${MODELPARAM_VALUE.TB_SIZE_WIDTH}
}

proc update_MODELPARAM_VALUE.LOG2_CTB_WIDTH { MODELPARAM_VALUE.LOG2_CTB_WIDTH PARAM_VALUE.LOG2_CTB_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.LOG2_CTB_WIDTH}] ${MODELPARAM_VALUE.LOG2_CTB_WIDTH}
}

proc update_MODELPARAM_VALUE.PIC_DIM_WIDTH { MODELPARAM_VALUE.PIC_DIM_WIDTH PARAM_VALUE.PIC_DIM_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.PIC_DIM_WIDTH}] ${MODELPARAM_VALUE.PIC_DIM_WIDTH}
}

proc update_MODELPARAM_VALUE.LOG2_MIN_COLLOCATE_SIZE { MODELPARAM_VALUE.LOG2_MIN_COLLOCATE_SIZE PARAM_VALUE.LOG2_MIN_COLLOCATE_SIZE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.LOG2_MIN_COLLOCATE_SIZE}] ${MODELPARAM_VALUE.LOG2_MIN_COLLOCATE_SIZE}
}

proc update_MODELPARAM_VALUE.DBF_SAMPLE_XY_ADDR { MODELPARAM_VALUE.DBF_SAMPLE_XY_ADDR PARAM_VALUE.DBF_SAMPLE_XY_ADDR } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DBF_SAMPLE_XY_ADDR}] ${MODELPARAM_VALUE.DBF_SAMPLE_XY_ADDR}
}

proc update_MODELPARAM_VALUE.RPS_HEADER_ADDR_WIDTH { MODELPARAM_VALUE.RPS_HEADER_ADDR_WIDTH PARAM_VALUE.RPS_HEADER_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RPS_HEADER_ADDR_WIDTH}] ${MODELPARAM_VALUE.RPS_HEADER_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.NUM_NEG_POS_POC_WIDTH { MODELPARAM_VALUE.NUM_NEG_POS_POC_WIDTH PARAM_VALUE.NUM_NEG_POS_POC_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NUM_NEG_POS_POC_WIDTH}] ${MODELPARAM_VALUE.NUM_NEG_POS_POC_WIDTH}
}

proc update_MODELPARAM_VALUE.RPS_HEADER_DATA_WIDTH { MODELPARAM_VALUE.RPS_HEADER_DATA_WIDTH PARAM_VALUE.RPS_HEADER_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RPS_HEADER_DATA_WIDTH}] ${MODELPARAM_VALUE.RPS_HEADER_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.RPS_ENTRY_DATA_WIDTH { MODELPARAM_VALUE.RPS_ENTRY_DATA_WIDTH PARAM_VALUE.RPS_ENTRY_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RPS_ENTRY_DATA_WIDTH}] ${MODELPARAM_VALUE.RPS_ENTRY_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.RPS_ENTRY_ADDR_WIDTH { MODELPARAM_VALUE.RPS_ENTRY_ADDR_WIDTH PARAM_VALUE.RPS_ENTRY_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RPS_ENTRY_ADDR_WIDTH}] ${MODELPARAM_VALUE.RPS_ENTRY_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.RPS_HEADER_NUM_POS_POC_RANGE_HIGH { MODELPARAM_VALUE.RPS_HEADER_NUM_POS_POC_RANGE_HIGH PARAM_VALUE.RPS_HEADER_NUM_POS_POC_RANGE_HIGH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RPS_HEADER_NUM_POS_POC_RANGE_HIGH}] ${MODELPARAM_VALUE.RPS_HEADER_NUM_POS_POC_RANGE_HIGH}
}

proc update_MODELPARAM_VALUE.RPS_HEADER_NUM_POS_POC_RANGE_LOW { MODELPARAM_VALUE.RPS_HEADER_NUM_POS_POC_RANGE_LOW PARAM_VALUE.RPS_HEADER_NUM_POS_POC_RANGE_LOW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RPS_HEADER_NUM_POS_POC_RANGE_LOW}] ${MODELPARAM_VALUE.RPS_HEADER_NUM_POS_POC_RANGE_LOW}
}

proc update_MODELPARAM_VALUE.RPS_HEADER_NUM_NEG_POC_RANGE_HIGH { MODELPARAM_VALUE.RPS_HEADER_NUM_NEG_POC_RANGE_HIGH PARAM_VALUE.RPS_HEADER_NUM_NEG_POC_RANGE_HIGH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RPS_HEADER_NUM_NEG_POC_RANGE_HIGH}] ${MODELPARAM_VALUE.RPS_HEADER_NUM_NEG_POC_RANGE_HIGH}
}

proc update_MODELPARAM_VALUE.RPS_HEADER_NUM_NEG_POC_RANGE_LOW { MODELPARAM_VALUE.RPS_HEADER_NUM_NEG_POC_RANGE_LOW PARAM_VALUE.RPS_HEADER_NUM_NEG_POC_RANGE_LOW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RPS_HEADER_NUM_NEG_POC_RANGE_LOW}] ${MODELPARAM_VALUE.RPS_HEADER_NUM_NEG_POC_RANGE_LOW}
}

proc update_MODELPARAM_VALUE.RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH { MODELPARAM_VALUE.RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH PARAM_VALUE.RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH}] ${MODELPARAM_VALUE.RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH}
}

proc update_MODELPARAM_VALUE.RPS_HEADER_NUM_DELTA_POC_RANGE_LOW { MODELPARAM_VALUE.RPS_HEADER_NUM_DELTA_POC_RANGE_LOW PARAM_VALUE.RPS_HEADER_NUM_DELTA_POC_RANGE_LOW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RPS_HEADER_NUM_DELTA_POC_RANGE_LOW}] ${MODELPARAM_VALUE.RPS_HEADER_NUM_DELTA_POC_RANGE_LOW}
}

proc update_MODELPARAM_VALUE.RPS_ENTRY_USED_FLAG_RANGE_HIGH { MODELPARAM_VALUE.RPS_ENTRY_USED_FLAG_RANGE_HIGH PARAM_VALUE.RPS_ENTRY_USED_FLAG_RANGE_HIGH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RPS_ENTRY_USED_FLAG_RANGE_HIGH}] ${MODELPARAM_VALUE.RPS_ENTRY_USED_FLAG_RANGE_HIGH}
}

proc update_MODELPARAM_VALUE.RPS_ENTRY_USED_FLAG_RANGE_LOW { MODELPARAM_VALUE.RPS_ENTRY_USED_FLAG_RANGE_LOW PARAM_VALUE.RPS_ENTRY_USED_FLAG_RANGE_LOW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RPS_ENTRY_USED_FLAG_RANGE_LOW}] ${MODELPARAM_VALUE.RPS_ENTRY_USED_FLAG_RANGE_LOW}
}

proc update_MODELPARAM_VALUE.RPS_ENTRY_DELTA_POC_RANGE_HIGH { MODELPARAM_VALUE.RPS_ENTRY_DELTA_POC_RANGE_HIGH PARAM_VALUE.RPS_ENTRY_DELTA_POC_RANGE_HIGH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RPS_ENTRY_DELTA_POC_RANGE_HIGH}] ${MODELPARAM_VALUE.RPS_ENTRY_DELTA_POC_RANGE_HIGH}
}

proc update_MODELPARAM_VALUE.RPS_ENTRY_DELTA_POC_RANGE_LOW { MODELPARAM_VALUE.RPS_ENTRY_DELTA_POC_RANGE_LOW PARAM_VALUE.RPS_ENTRY_DELTA_POC_RANGE_LOW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RPS_ENTRY_DELTA_POC_RANGE_LOW}] ${MODELPARAM_VALUE.RPS_ENTRY_DELTA_POC_RANGE_LOW}
}

proc update_MODELPARAM_VALUE.RPS_ENTRY_DELTA_POC_MSB { MODELPARAM_VALUE.RPS_ENTRY_DELTA_POC_MSB PARAM_VALUE.RPS_ENTRY_DELTA_POC_MSB } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RPS_ENTRY_DELTA_POC_MSB}] ${MODELPARAM_VALUE.RPS_ENTRY_DELTA_POC_MSB}
}

proc update_MODELPARAM_VALUE.BS_FIFO_WIDTH { MODELPARAM_VALUE.BS_FIFO_WIDTH PARAM_VALUE.BS_FIFO_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.BS_FIFO_WIDTH}] ${MODELPARAM_VALUE.BS_FIFO_WIDTH}
}

proc update_MODELPARAM_VALUE.X_ADDR_WDTH { MODELPARAM_VALUE.X_ADDR_WDTH PARAM_VALUE.X_ADDR_WDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.X_ADDR_WDTH}] ${MODELPARAM_VALUE.X_ADDR_WDTH}
}

proc update_MODELPARAM_VALUE.X11_ADDR_WDTH { MODELPARAM_VALUE.X11_ADDR_WDTH PARAM_VALUE.X11_ADDR_WDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.X11_ADDR_WDTH}] ${MODELPARAM_VALUE.X11_ADDR_WDTH}
}

proc update_MODELPARAM_VALUE.Y_ADDR_WDTH { MODELPARAM_VALUE.Y_ADDR_WDTH PARAM_VALUE.Y_ADDR_WDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.Y_ADDR_WDTH}] ${MODELPARAM_VALUE.Y_ADDR_WDTH}
}

proc update_MODELPARAM_VALUE.Y11_ADDR_WDTH { MODELPARAM_VALUE.Y11_ADDR_WDTH PARAM_VALUE.Y11_ADDR_WDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.Y11_ADDR_WDTH}] ${MODELPARAM_VALUE.Y11_ADDR_WDTH}
}

proc update_MODELPARAM_VALUE.LOG2_MIN_PU_SIZE { MODELPARAM_VALUE.LOG2_MIN_PU_SIZE PARAM_VALUE.LOG2_MIN_PU_SIZE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.LOG2_MIN_PU_SIZE}] ${MODELPARAM_VALUE.LOG2_MIN_PU_SIZE}
}

proc update_MODELPARAM_VALUE.LOG2_MIN_TU_SIZE { MODELPARAM_VALUE.LOG2_MIN_TU_SIZE PARAM_VALUE.LOG2_MIN_TU_SIZE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.LOG2_MIN_TU_SIZE}] ${MODELPARAM_VALUE.LOG2_MIN_TU_SIZE}
}

proc update_MODELPARAM_VALUE.LOG2_MIN_DU_SIZE { MODELPARAM_VALUE.LOG2_MIN_DU_SIZE PARAM_VALUE.LOG2_MIN_DU_SIZE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.LOG2_MIN_DU_SIZE}] ${MODELPARAM_VALUE.LOG2_MIN_DU_SIZE}
}

proc update_MODELPARAM_VALUE.AVAILABLE_CONFIG_BUS_WIDTH { MODELPARAM_VALUE.AVAILABLE_CONFIG_BUS_WIDTH PARAM_VALUE.AVAILABLE_CONFIG_BUS_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.AVAILABLE_CONFIG_BUS_WIDTH}] ${MODELPARAM_VALUE.AVAILABLE_CONFIG_BUS_WIDTH}
}

proc update_MODELPARAM_VALUE.POC_WIDTH { MODELPARAM_VALUE.POC_WIDTH PARAM_VALUE.POC_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.POC_WIDTH}] ${MODELPARAM_VALUE.POC_WIDTH}
}

proc update_MODELPARAM_VALUE.DPB_FRAME_OFFSET_WIDTH { MODELPARAM_VALUE.DPB_FRAME_OFFSET_WIDTH PARAM_VALUE.DPB_FRAME_OFFSET_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPB_FRAME_OFFSET_WIDTH}] ${MODELPARAM_VALUE.DPB_FRAME_OFFSET_WIDTH}
}

proc update_MODELPARAM_VALUE.DPB_STATUS_WIDTH { MODELPARAM_VALUE.DPB_STATUS_WIDTH PARAM_VALUE.DPB_STATUS_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPB_STATUS_WIDTH}] ${MODELPARAM_VALUE.DPB_STATUS_WIDTH}
}

proc update_MODELPARAM_VALUE.DPB_FILLED_WIDTH { MODELPARAM_VALUE.DPB_FILLED_WIDTH PARAM_VALUE.DPB_FILLED_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPB_FILLED_WIDTH}] ${MODELPARAM_VALUE.DPB_FILLED_WIDTH}
}

proc update_MODELPARAM_VALUE.DPB_DATA_WIDTH { MODELPARAM_VALUE.DPB_DATA_WIDTH PARAM_VALUE.DPB_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPB_DATA_WIDTH}] ${MODELPARAM_VALUE.DPB_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.REF_PIC_LIST_POC_DATA_WIDTH { MODELPARAM_VALUE.REF_PIC_LIST_POC_DATA_WIDTH PARAM_VALUE.REF_PIC_LIST_POC_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.REF_PIC_LIST_POC_DATA_WIDTH}] ${MODELPARAM_VALUE.REF_PIC_LIST_POC_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.DPB_REF_PIC_ADDR_WIDTH { MODELPARAM_VALUE.DPB_REF_PIC_ADDR_WIDTH PARAM_VALUE.DPB_REF_PIC_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPB_REF_PIC_ADDR_WIDTH}] ${MODELPARAM_VALUE.DPB_REF_PIC_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.DPB_ADDR_WIDTH { MODELPARAM_VALUE.DPB_ADDR_WIDTH PARAM_VALUE.DPB_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPB_ADDR_WIDTH}] ${MODELPARAM_VALUE.DPB_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.REF_PIC_LIST_ADDR_WIDTH { MODELPARAM_VALUE.REF_PIC_LIST_ADDR_WIDTH PARAM_VALUE.REF_PIC_LIST_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.REF_PIC_LIST_ADDR_WIDTH}] ${MODELPARAM_VALUE.REF_PIC_LIST_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.REF_PIC_LIST_DATA_WIDTH { MODELPARAM_VALUE.REF_PIC_LIST_DATA_WIDTH PARAM_VALUE.REF_PIC_LIST_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.REF_PIC_LIST_DATA_WIDTH}] ${MODELPARAM_VALUE.REF_PIC_LIST_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.REF_POC_LIST5_ADDR_WIDTH { MODELPARAM_VALUE.REF_POC_LIST5_ADDR_WIDTH PARAM_VALUE.REF_POC_LIST5_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.REF_POC_LIST5_ADDR_WIDTH}] ${MODELPARAM_VALUE.REF_POC_LIST5_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.REF_POC_LIST5_DATA_WIDTH { MODELPARAM_VALUE.REF_POC_LIST5_DATA_WIDTH PARAM_VALUE.REF_POC_LIST5_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.REF_POC_LIST5_DATA_WIDTH}] ${MODELPARAM_VALUE.REF_POC_LIST5_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.INTER_TOP_CONFIG_BUS_MODE_WIDTH { MODELPARAM_VALUE.INTER_TOP_CONFIG_BUS_MODE_WIDTH PARAM_VALUE.INTER_TOP_CONFIG_BUS_MODE_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.INTER_TOP_CONFIG_BUS_MODE_WIDTH}] ${MODELPARAM_VALUE.INTER_TOP_CONFIG_BUS_MODE_WIDTH}
}

proc update_MODELPARAM_VALUE.INTER_TOP_CONFIG_BUS_WIDTH { MODELPARAM_VALUE.INTER_TOP_CONFIG_BUS_WIDTH PARAM_VALUE.INTER_TOP_CONFIG_BUS_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.INTER_TOP_CONFIG_BUS_WIDTH}] ${MODELPARAM_VALUE.INTER_TOP_CONFIG_BUS_WIDTH}
}

proc update_MODELPARAM_VALUE.MV_FIELD_DATA_WIDTH { MODELPARAM_VALUE.MV_FIELD_DATA_WIDTH PARAM_VALUE.MV_FIELD_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MV_FIELD_DATA_WIDTH}] ${MODELPARAM_VALUE.MV_FIELD_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.MV_COL_AXI_DATA_WIDTH { MODELPARAM_VALUE.MV_COL_AXI_DATA_WIDTH PARAM_VALUE.MV_COL_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MV_COL_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.MV_COL_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.DPB_POC_RANGE_HIGH { MODELPARAM_VALUE.DPB_POC_RANGE_HIGH PARAM_VALUE.DPB_POC_RANGE_HIGH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPB_POC_RANGE_HIGH}] ${MODELPARAM_VALUE.DPB_POC_RANGE_HIGH}
}

proc update_MODELPARAM_VALUE.DPB_POC_RANGE_LOW { MODELPARAM_VALUE.DPB_POC_RANGE_LOW PARAM_VALUE.DPB_POC_RANGE_LOW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPB_POC_RANGE_LOW}] ${MODELPARAM_VALUE.DPB_POC_RANGE_LOW}
}

proc update_MODELPARAM_VALUE.REF_PIC_LIST5_POC_RANGE_HIGH { MODELPARAM_VALUE.REF_PIC_LIST5_POC_RANGE_HIGH PARAM_VALUE.REF_PIC_LIST5_POC_RANGE_HIGH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.REF_PIC_LIST5_POC_RANGE_HIGH}] ${MODELPARAM_VALUE.REF_PIC_LIST5_POC_RANGE_HIGH}
}

proc update_MODELPARAM_VALUE.REF_PIC_LIST5_POC_RANGE_LOW { MODELPARAM_VALUE.REF_PIC_LIST5_POC_RANGE_LOW PARAM_VALUE.REF_PIC_LIST5_POC_RANGE_LOW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.REF_PIC_LIST5_POC_RANGE_LOW}] ${MODELPARAM_VALUE.REF_PIC_LIST5_POC_RANGE_LOW}
}

proc update_MODELPARAM_VALUE.REF_PIC_LIST5_DPB_STATE_HIGH { MODELPARAM_VALUE.REF_PIC_LIST5_DPB_STATE_HIGH PARAM_VALUE.REF_PIC_LIST5_DPB_STATE_HIGH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.REF_PIC_LIST5_DPB_STATE_HIGH}] ${MODELPARAM_VALUE.REF_PIC_LIST5_DPB_STATE_HIGH}
}

proc update_MODELPARAM_VALUE.REF_PIC_LIST5_DPB_STATE_LOW { MODELPARAM_VALUE.REF_PIC_LIST5_DPB_STATE_LOW PARAM_VALUE.REF_PIC_LIST5_DPB_STATE_LOW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.REF_PIC_LIST5_DPB_STATE_LOW}] ${MODELPARAM_VALUE.REF_PIC_LIST5_DPB_STATE_LOW}
}

proc update_MODELPARAM_VALUE.MERGE_CAND_TYPE_WIDTH { MODELPARAM_VALUE.MERGE_CAND_TYPE_WIDTH PARAM_VALUE.MERGE_CAND_TYPE_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MERGE_CAND_TYPE_WIDTH}] ${MODELPARAM_VALUE.MERGE_CAND_TYPE_WIDTH}
}

proc update_MODELPARAM_VALUE.MAX_NUM_MERGE_CAND_CONST { MODELPARAM_VALUE.MAX_NUM_MERGE_CAND_CONST PARAM_VALUE.MAX_NUM_MERGE_CAND_CONST } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MAX_NUM_MERGE_CAND_CONST}] ${MODELPARAM_VALUE.MAX_NUM_MERGE_CAND_CONST}
}

proc update_MODELPARAM_VALUE.AMVP_NUM_CAND_CONST { MODELPARAM_VALUE.AMVP_NUM_CAND_CONST PARAM_VALUE.AMVP_NUM_CAND_CONST } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.AMVP_NUM_CAND_CONST}] ${MODELPARAM_VALUE.AMVP_NUM_CAND_CONST}
}

proc update_MODELPARAM_VALUE.NUM_BI_PRED_CANDS { MODELPARAM_VALUE.NUM_BI_PRED_CANDS PARAM_VALUE.NUM_BI_PRED_CANDS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NUM_BI_PRED_CANDS}] ${MODELPARAM_VALUE.NUM_BI_PRED_CANDS}
}

proc update_MODELPARAM_VALUE.TX_WIDTH { MODELPARAM_VALUE.TX_WIDTH PARAM_VALUE.TX_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.TX_WIDTH}] ${MODELPARAM_VALUE.TX_WIDTH}
}

proc update_MODELPARAM_VALUE.DIST_SCALE_WIDTH { MODELPARAM_VALUE.DIST_SCALE_WIDTH PARAM_VALUE.DIST_SCALE_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DIST_SCALE_WIDTH}] ${MODELPARAM_VALUE.DIST_SCALE_WIDTH}
}

proc update_MODELPARAM_VALUE.NUM_BI_PRED_CANDS_TYPES { MODELPARAM_VALUE.NUM_BI_PRED_CANDS_TYPES PARAM_VALUE.NUM_BI_PRED_CANDS_TYPES } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NUM_BI_PRED_CANDS_TYPES}] ${MODELPARAM_VALUE.NUM_BI_PRED_CANDS_TYPES}
}

proc update_MODELPARAM_VALUE.SAO_OUT_FIFO_WIDTH { MODELPARAM_VALUE.SAO_OUT_FIFO_WIDTH PARAM_VALUE.SAO_OUT_FIFO_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SAO_OUT_FIFO_WIDTH}] ${MODELPARAM_VALUE.SAO_OUT_FIFO_WIDTH}
}

proc update_MODELPARAM_VALUE.FILTER_PIXEL_WIDTH { MODELPARAM_VALUE.FILTER_PIXEL_WIDTH PARAM_VALUE.FILTER_PIXEL_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.FILTER_PIXEL_WIDTH}] ${MODELPARAM_VALUE.FILTER_PIXEL_WIDTH}
}

proc update_MODELPARAM_VALUE.NUM_IN_TO_8_FILTER { MODELPARAM_VALUE.NUM_IN_TO_8_FILTER PARAM_VALUE.NUM_IN_TO_8_FILTER } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NUM_IN_TO_8_FILTER}] ${MODELPARAM_VALUE.NUM_IN_TO_8_FILTER}
}

proc update_MODELPARAM_VALUE.MAX_MV_PER_CU { MODELPARAM_VALUE.MAX_MV_PER_CU PARAM_VALUE.MAX_MV_PER_CU } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MAX_MV_PER_CU}] ${MODELPARAM_VALUE.MAX_MV_PER_CU}
}

proc update_MODELPARAM_VALUE.STEP_8x8_IN_PUS { MODELPARAM_VALUE.STEP_8x8_IN_PUS PARAM_VALUE.STEP_8x8_IN_PUS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STEP_8x8_IN_PUS}] ${MODELPARAM_VALUE.STEP_8x8_IN_PUS}
}

proc update_MODELPARAM_VALUE.MV_L_FRAC_WIDTH_HIGH { MODELPARAM_VALUE.MV_L_FRAC_WIDTH_HIGH PARAM_VALUE.MV_L_FRAC_WIDTH_HIGH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MV_L_FRAC_WIDTH_HIGH}] ${MODELPARAM_VALUE.MV_L_FRAC_WIDTH_HIGH}
}

proc update_MODELPARAM_VALUE.MV_L_INT_WIDTH_LOW { MODELPARAM_VALUE.MV_L_INT_WIDTH_LOW PARAM_VALUE.MV_L_INT_WIDTH_LOW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MV_L_INT_WIDTH_LOW}] ${MODELPARAM_VALUE.MV_L_INT_WIDTH_LOW}
}

proc update_MODELPARAM_VALUE.MV_C_FRAC_WIDTH_HIGH { MODELPARAM_VALUE.MV_C_FRAC_WIDTH_HIGH PARAM_VALUE.MV_C_FRAC_WIDTH_HIGH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MV_C_FRAC_WIDTH_HIGH}] ${MODELPARAM_VALUE.MV_C_FRAC_WIDTH_HIGH}
}

proc update_MODELPARAM_VALUE.MV_C_INT_WIDTH_LOW { MODELPARAM_VALUE.MV_C_INT_WIDTH_LOW PARAM_VALUE.MV_C_INT_WIDTH_LOW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MV_C_INT_WIDTH_LOW}] ${MODELPARAM_VALUE.MV_C_INT_WIDTH_LOW}
}

proc update_MODELPARAM_VALUE.YY_WIDTH { MODELPARAM_VALUE.YY_WIDTH PARAM_VALUE.YY_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.YY_WIDTH}] ${MODELPARAM_VALUE.YY_WIDTH}
}

proc update_MODELPARAM_VALUE.CH_WIDTH { MODELPARAM_VALUE.CH_WIDTH PARAM_VALUE.CH_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CH_WIDTH}] ${MODELPARAM_VALUE.CH_WIDTH}
}

