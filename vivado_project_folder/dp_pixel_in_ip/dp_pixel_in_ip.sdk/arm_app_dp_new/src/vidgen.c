/*******************************************************************************
 *
 * Copyright (C) 2014 Xilinx, Inc.  All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Use of the Software is limited solely to applications:
 * (a) running on a Xilinx device, or
 * (b) that interact with a Xilinx device through a bus or interconnect.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * XILINX CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Except as contained in this notice, the name of the Xilinx shall not be used
 * in advertising or otherwise to promote the sale, use or other dealings in
 * this Software without prior written authorization from Xilinx.
 *
*******************************************************************************/
/*
 * vidgen.c
 *
 *  Created on: Jun 18, 2014
 *      Author: Andrei-Liviu Simion
 */

#include "vidgen.h"
#include "xparameters.h"
#include "xstatus.h"
#include "xdptx.h"

static void Dptx_VidgenSetConfig(XDptx *InstancePtr, XDptx_VidgenConfig *VidgenConfig, u8 Stream);
static void Dptx_VidgenWriteConfig(XDptx *InstancePtr, XDptx_VidgenConfig *VidgenConfig, u8 Stream);
static void Dptx_WaitTxVsyncs(XDptx *InstancePtr, u32 LoopCount, u8 Stream);
static void Dptx_VidgenComputeMVid(XDptx *InstancePtr, XDptx_VidgenConfig *VidgenConfig);

u32 StreamOffset[4] = {0, XILINX_DISPLAYPORT_VID2_BASE_ADDRESS_OFFSET, XILINX_DISPLAYPORT_VID3_BASE_ADDRESS_OFFSET, XILINX_DISPLAYPORT_VID4_BASE_ADDRESS_OFFSET};
u8 StreamPattern[4] = {0x11, 0x13, 0x15, 0x16};

u32 Dptx_StreamSrcSetup(XDptx *InstancePtr)
{
	Dptx_DoWriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS, 0x0, 0x2);
	Dptx_DoWriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS, 0x0, 0x0);

	return XST_SUCCESS;
}

u32 Dptx_StreamSrcConfigure(XDptx *InstancePtr)
{
	XDptx_VidgenConfig VidgenConfig;

	Dptx_DoFunction(Dptx_VidgenSetConfig, InstancePtr, &VidgenConfig);
	Dptx_DoFunction(Dptx_VidgenWriteConfig, InstancePtr, &VidgenConfig);
	Dptx_DoFunction(Dptx_VidgenSetTestPattern, InstancePtr);

	Dptx_DoWriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS, 0x0, 0x1);

	return XST_SUCCESS;
}

u32 Dptx_StreamSrcSync(XDptx *InstancePtr)
{
	Dptx_DoFunction(Dptx_WaitTxVsyncs, InstancePtr, 20);
	XDptx_WaitUs(InstancePtr, 200000);

	return XST_SUCCESS;
}

void Dptx_VidgenSetTestPattern(XDptx *InstancePtr, u8 Stream)
{
	if (XDptx_ReadReg(InstancePtr->Config.BaseAddr, XDPTX_USER_PIXEL_WIDTH) == 0x4) {
		XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x308, (0x200 | StreamPattern[Stream - 1]));
	}
	else if (XDptx_ReadReg(InstancePtr->Config.BaseAddr, XDPTX_USER_PIXEL_WIDTH) == 0x2) {
		XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x308, (0x100 | StreamPattern[Stream - 1]));
	}
	else {
		XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x308, StreamPattern[Stream - 1]);
	}
}

static void Dptx_VidgenWriteConfig(XDptx *InstancePtr, XDptx_VidgenConfig *VidgenConfig, u8 Stream)
{
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x04, VidgenConfig->VSyncPolarity);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x08, VidgenConfig->HSyncPolarity);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x0C, VidgenConfig->DePolarity);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x10, VidgenConfig->VSyncPulseWidth);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x14, VidgenConfig->VBackPorch);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x18, VidgenConfig->VFrontPorch);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x1C, VidgenConfig->VResolution);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x20, VidgenConfig->HSyncPulseWidth);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x24, VidgenConfig->HBackPorch);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x28, VidgenConfig->HFrontPorch);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x2C, VidgenConfig->HResolution);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x34, VidgenConfig->FrameLock0);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x3C, VidgenConfig->FrameLock1);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x40, VidgenConfig->HdColorBarMode);

	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x44, VidgenConfig->TcHsBlnk);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x48, VidgenConfig->TcHsSync);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x4C, VidgenConfig->TcHeSync);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x50, VidgenConfig->TcHeBlnk);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x54, VidgenConfig->TcVsBlnk);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x58, VidgenConfig->TcVsSync);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x5C, VidgenConfig->TcVeSync);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x60, VidgenConfig->TcVeBlnk);

	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x100, VidgenConfig->VidClkSel);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x104, VidgenConfig->MVid);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x108, VidgenConfig->VidClkD);

	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x300, InstancePtr->MsaConfig[0].Misc0);
	XDptx_WriteReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x304, InstancePtr->MsaConfig[0].Misc1);
}

static void Dptx_VidgenComputeMVid(XDptx *InstancePtr, XDptx_VidgenConfig *VidgenConfig)
{
	XDptx_LinkConfig *LinkConfig = &InstancePtr->LinkConfig;

	u32 RefFreq;
	u32 VidFreq = VidgenConfig->MVid;
	u32 m, d, Div, Freq, Diff, Fvco;
	u32 Minerr = 10000;
	u32 MVal = 0;
	u32 DVal = 0;
	u32 DivVal = 0;

	RefFreq = (LinkConfig->LinkRate == XDPTX_LINK_BW_SET_540GBPS) ? 270000 :
				(LinkConfig->LinkRate == XDPTX_LINK_BW_SET_270GBPS) ? 135000 :
											81000;

	if (InstancePtr->Config.PayloadDataWidth == 4) {
		RefFreq /= 2;
	}

	for (m = 20; m <= 64; m++) {
		for (d = 1; d <= 80; d++) {
			Fvco = RefFreq * m / d;

			if ( Fvco >= 600000 && Fvco <= 900000 ) {
				for (Div = 1; Div <= 128; Div++ ) {
					Freq = Fvco/Div;

					if (Freq >= VidFreq) {
						Diff = Freq - VidFreq;
					}
					else {
						Diff = VidFreq - Freq;
					}

					if (Diff == 0) {
						MVal = m;
						DVal = d;
						DivVal = Div;
						m = 257;
						d = 257;
						Div = 257;
						Minerr = 0;
					}
					else if (Diff < Minerr) {
						Minerr = Diff;
						MVal = m;
						DVal = d;
						DivVal = Div;

						if (Minerr < 100) {
							m = 257;
							d = 257;
							Div = 257;
						}
					}
				}
			}
		}
	}
	VidgenConfig->MVid = MVal;
	VidgenConfig->VidClkD = (DivVal & 0xff) | ((DVal & 0xff) << 8);
}

static void Dptx_VidgenSetConfig(XDptx *InstancePtr, XDptx_VidgenConfig *VidgenConfig, u8 Stream)
{
	XDptx_MainStreamAttributes *MsaConfig = &InstancePtr->MsaConfig[Stream - 1];
	XDptx_LinkConfig *LinkConfig = &InstancePtr->LinkConfig;
	u32 UserPixelWidth;

	/* Configure the MSA values from the Display Monitor Timing (DMT) table. */
	/* Will provide a way to optionally acquire these values from the EDID of the sink. */
	/* For now, use a hard-coded video mode. */

	VidgenConfig->MVid = MsaConfig->Vtm.PixelClkKhz;

	VidgenConfig->VidClkSel = (LinkConfig->LinkRate == XDPTX_LINK_BW_SET_270GBPS);

	UserPixelWidth = MsaConfig->UserPixelWidth;
	VidgenConfig->MVid /= UserPixelWidth;

	VidgenConfig->Misc0 = MsaConfig->Misc0;
	VidgenConfig->Misc1 = MsaConfig->Misc1;

	VidgenConfig->DePolarity = 0x0;
	VidgenConfig->FrameLock0 = 0;
	VidgenConfig->FrameLock1 = 0;
	VidgenConfig->HdColorBarMode = 0;

	VidgenConfig->VSyncPolarity = MsaConfig->Vtm.Timing.VSyncPolarity;
	VidgenConfig->HSyncPolarity = MsaConfig->Vtm.Timing.HSyncPolarity;
	VidgenConfig->VSyncPulseWidth = MsaConfig->Vtm.Timing.F0PVSyncWidth;
	VidgenConfig->VBackPorch = MsaConfig->Vtm.Timing.F0PVBackPorch;
	VidgenConfig->VFrontPorch = MsaConfig->Vtm.Timing.F0PVFrontPorch;
	VidgenConfig->VResolution = MsaConfig->Vtm.Timing.VActive;

	/* Re-compute horizontal values based on user pixel width. */
	VidgenConfig->HResolution = MsaConfig->Vtm.Timing.HActive / UserPixelWidth;
	VidgenConfig->HBackPorch = MsaConfig->Vtm.Timing.HBackPorch / UserPixelWidth;
	VidgenConfig->HFrontPorch = MsaConfig->Vtm.Timing.HFrontPorch / UserPixelWidth;
	VidgenConfig->HSyncPulseWidth = MsaConfig->Vtm.Timing.HSyncWidth / UserPixelWidth;

	Dptx_VidgenComputeMVid(InstancePtr, VidgenConfig);

	/* Configure the pattern generator. */
	VidgenConfig->TcHsBlnk = VidgenConfig->HResolution - 1;
	VidgenConfig->TcHsSync = VidgenConfig->HResolution + VidgenConfig->HFrontPorch - 1 ;
	VidgenConfig->TcHeSync = VidgenConfig->HResolution + VidgenConfig->HFrontPorch +
																	VidgenConfig->HSyncPulseWidth - 1;
	VidgenConfig->TcHeBlnk = VidgenConfig->HResolution + VidgenConfig->HFrontPorch +
								VidgenConfig->HSyncPulseWidth + VidgenConfig->HBackPorch - 1;

	VidgenConfig->TcVsBlnk = VidgenConfig->VResolution - 1;
	VidgenConfig->TcVsSync = VidgenConfig->VResolution + VidgenConfig->VFrontPorch - 1;
	VidgenConfig->TcVeSync = VidgenConfig->VResolution + VidgenConfig->VFrontPorch +
																VidgenConfig->VSyncPulseWidth - 1;
	VidgenConfig->TcVeBlnk = VidgenConfig->VResolution + VidgenConfig->VFrontPorch +
								VidgenConfig->VSyncPulseWidth + VidgenConfig->VBackPorch - 1;
}

/* Wait specified number of vsyncs. */
static void Dptx_WaitTxVsyncs(XDptx *InstancePtr, u32 LoopCount, u8 Stream)
{
	u32 VBlank = XDptx_ReadReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x60);
	u32 VCount = XDptx_ReadReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x200);
	u32 Start = XDptx_ReadReg(InstancePtr->Config.BaseAddr, XDPTX_INTERRUPT_STATUS + 0x14);
	u32 End;
	u32 Diff;

	while (LoopCount > 0) {
		End = XDptx_ReadReg(InstancePtr->Config.BaseAddr, XDPTX_INTERRUPT_STATUS + 0x14);

		if (Start > End) {
			Diff = Start - End;
		}
		else {
			Diff = End - Start;
		}

		if (VCount >= VBlank) {
			LoopCount--;
			Diff = 0;
		}
		else if (Diff > 100000000) {
			LoopCount = 0;
		}

		VCount = XDptx_ReadReg(XILINX_DISPLAYPORT_VID_BASE_ADDRESS + StreamOffset[Stream - 1], 0x200);
	}
}
