/*******************************************************************************
 *
 * Copyright (C) 2014 Xilinx, Inc.  All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Use of the Software is limited solely to applications:
 * (a) running on a Xilinx device, or
 * (b) that interact with a Xilinx device through a bus or interconnect.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * XILINX CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Except as contained in this notice, the name of the Xilinx shall not be used
 * in advertising or otherwise to promote the sale, use or other dealings in
 * this Software without prior written authorization from Xilinx.
 *
*******************************************************************************/
/*
 * vidgen.h
 *
 *  Created on: Jun 18, 2014
 *      Author: Andrei-Liviu Simion
 */

#ifndef VIDGEN_H_
#define VIDGEN_H_

#include "xdptx.h"

#ifdef __MICROBLAZE__
#define XILINX_DISPLAYPORT_VID_BASE_ADDRESS     0x44A00000
#define XILINX_DISPLAYPORT_VID2_BASE_ADDRESS    0x44A20000
#define XILINX_DISPLAYPORT_VID3_BASE_ADDRESS    0x44A30000
#define XILINX_DISPLAYPORT_VID4_BASE_ADDRESS    0x44A40000

#define XILINX_DISPLAYPORT_VID2_BASE_ADDRESS_OFFSET	0x20000
#define XILINX_DISPLAYPORT_VID3_BASE_ADDRESS_OFFSET	0x30000
#define XILINX_DISPLAYPORT_VID4_BASE_ADDRESS_OFFSET	0x40000
#else
#define XILINX_DISPLAYPORT_VID_BASE_ADDRESS     0x43C10000
#define XILINX_DISPLAYPORT_VID2_BASE_ADDRESS    0x43C20000
#define XILINX_DISPLAYPORT_VID3_BASE_ADDRESS    0x43C30000
#define XILINX_DISPLAYPORT_VID4_BASE_ADDRESS    0x43C40000

#define XILINX_DISPLAYPORT_VID2_BASE_ADDRESS_OFFSET	0x10000
#define XILINX_DISPLAYPORT_VID3_BASE_ADDRESS_OFFSET	0x20000
#define XILINX_DISPLAYPORT_VID4_BASE_ADDRESS_OFFSET	0x30000
#endif

#define Dptx_DoFunction(f, ...) \
	{ \
		if (InstancePtr->MstEnable == 0) { \
			f(__VA_ARGS__, XDPTX_STREAM_ID1); \
		} \
		else { \
			u8 StreamIndex; \
			for (StreamIndex = 0; StreamIndex < 4; StreamIndex++) { \
				if (XDptx_MstStreamIsEnabled(InstancePtr, XDPTX_STREAM_ID1 + StreamIndex)) { \
					f(__VA_ARGS__, XDPTX_STREAM_ID1 + StreamIndex); \
				} \
			} \
		} \
	}

#define Dptx_DoWriteReg(b, r, v) \
	{ \
		if (InstancePtr->MstEnable == 0) { \
			XDptx_WriteReg(b, r, v); \
		} \
		else { \
			u8 StreamIndex; \
			for (StreamIndex = 0; StreamIndex < 4; StreamIndex++) { \
				if (XDptx_MstStreamIsEnabled(InstancePtr, XDPTX_STREAM_ID1 + StreamIndex)) { \
					XDptx_WriteReg(b, r + StreamOffset[StreamIndex], v); \
				} \
			} \
		} \
	}

typedef struct {
	u32 VSyncPolarity;
	u32 HSyncPolarity;
	u32 DePolarity;
	u32 VSyncPulseWidth;
	u32 VBackPorch;
	u32 VFrontPorch;
	u32 VResolution;
	u32 HSyncPulseWidth;
	u32 HBackPorch;
	u32 HFrontPorch;
	u32 HResolution;
	u32 FrameLock0;
	u32 FrameLock1;
	u32 HdColorBarMode;

	u32 TcHsBlnk;
	u32 TcHsSync;
	u32 TcHeSync;
	u32 TcHeBlnk;
	u32 TcVsBlnk;
	u32 TcVsSync;
	u32 TcVeSync;
	u32 TcVeBlnk;

	u32 VidClkSel;
	u32 MVid;
	u32 VidClkD;

	u32 Misc0;
	u32 Misc1;
} XDptx_VidgenConfig;

u32 Dptx_StreamSrcSetup(XDptx *InstancePtr);
u32 Dptx_StreamSrcConfigure(XDptx *InstancePtr);
u32 Dptx_StreamSrcSync(XDptx *InstancePtr);
void Dptx_VidgenSetTestPattern(XDptx *InstancePtr, u8 Stream);

#endif /* VIDGEN_H_ */
