/*******************************************************************************
 *
 * Copyright (C) 2014 Xilinx, Inc.  All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Use of the Software is limited solely to applications:
 * (a) running on a Xilinx device, or
 * (b) that interact with a Xilinx device through a bus or interconnect. 
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * XILINX CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Except as contained in this notice, the name of the Xilinx shall not be used
 * in advertising or otherwise to promote the sale, use or other dealings in
 * this Software without prior written authorization from Xilinx.
 *
*******************************************************************************/
/******************************************************************************/
/**
 *
 * @file xdptx_dbg.c
 *
 * This file contains functions used for printing debug information.
 *
 * <pre>
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -----------------------------------------------
 * 1.00a als  05/17/14 First release
 * </pre>
 *
*******************************************************************************/

/***************************** Include Files **********************************/

#include "xdptx.h"
#include "xdptx_hw.h"
#include "xedid_print_example.h"
//#include "xuartlite_l.h"
#include "xparameters.h"

/************************** Constant Definitions ******************************/

/**************************** Type Definitions ********************************/

/***************** Macros (Inline Functions) Definitions **********************/

/************************** Variable Definitions ******************************/

/************************** Function Prototypes *******************************/

/******************************************************************************/
/**
 * This function prints the capabilities of the DisplayPort TX core.
 * 
 * @param       InstancePtr is a pointer to the XDptx instance.
*******************************************************************************/
void XDptx_DbgPrintTxCaps(XDptx *InstancePtr)
{
        XDptx_Config *TxConfig = &InstancePtr->Config;

        xil_printf("TX capabilities:\n\r"
                        "\tMax link rate: %s\n\r"
                        "\tMax lane count: %d\n\r"
                        "\tBus speed: %dHz\n\r",
                (TxConfig->MaxLinkRate == XDPTX_DPCD_LINK_BW_SET_162GBPS) ? "1.62Gbps" :
                 (TxConfig->MaxLinkRate == XDPTX_DPCD_LINK_BW_SET_270GBPS) ? "2.70Gbps" :
                 (TxConfig->MaxLinkRate == XDPTX_DPCD_LINK_BW_SET_540GBPS) ? "5.40Gbps" :
                 "Unknown link rate",
                TxConfig->MaxLaneCount,
                TxConfig->SAxiClkHz
        );
}

/******************************************************************************/
/**
 * This function prints the capabilities of the DisplayPort sink.
 * 
 * @param       InstancePtr is a pointer to the XDptx instance.
*******************************************************************************/
void XDptx_DbgPrintSinkCaps(XDptx *InstancePtr)
{
        u8 *Dpcd = InstancePtr->RxConfig.DpcdRxCapsField;

        u8 DpcdRevMajor = (Dpcd[XDPTX_DPCD_REV] &
                XDPTX_DPCD_REV_MJR_MASK) >> XDPTX_DPCD_REV_MJR_SHIFT;
        u8 DpcdRevMinor = Dpcd[XDPTX_DPCD_REV] &
                XDPTX_DPCD_REV_MNR_MASK;
        u8 MaxLinkRate = Dpcd[XDPTX_DPCD_MAX_LINK_RATE];
        u8 MaxLaneCount = Dpcd[XDPTX_DPCD_MAX_LANE_COUNT] &
                XDPTX_DPCD_MAX_LANE_COUNT_MASK;
        u8 Tps3Support = Dpcd[XDPTX_DPCD_MAX_LANE_COUNT] &
        		XDPTX_DPCD_TPS3_SUPPORT_MASK;
        u8 EnhancedFrameSupport = Dpcd[XDPTX_DPCD_MAX_LANE_COUNT] &
        		XDPTX_DPCD_ENHANCED_FRAME_SUPPORT_MASK;
        u8 MaxDownspreadSupport = Dpcd[XDPTX_DPCD_MAX_DOWNSPREAD] &
                XDPTX_DPCD_MAX_DOWNSPREAD_MASK;
        u8 NoAuxHandshakeRequired = Dpcd[XDPTX_DPCD_MAX_DOWNSPREAD] &
                XDPTX_DPCD_NO_AUX_HANDSHAKE_LINK_TRAIN_MASK;
        u8 NumRxPorts = (Dpcd[XDPTX_DPCD_NORP_PWR_V_CAP] &
                (0x01)) + 1;
        u8 PwrDpCap5V = Dpcd[XDPTX_DPCD_NORP_PWR_V_CAP] &
                (0x01);
        u8 PwrDpCap12V = Dpcd[XDPTX_DPCD_NORP_PWR_V_CAP] &
                (0x20);
        u8 PwrDpCap18V = Dpcd[XDPTX_DPCD_NORP_PWR_V_CAP] &
                (0x40);
        u8 DownstreamPortPresent = Dpcd[XDPTX_DPCD_DOWNSP_PRESENT] &
                (0x80);
        u8 DownPortType = (Dpcd[XDPTX_DPCD_DOWNSP_PRESENT] &
                XDPTX_DPCD_DOWNSP_TYPE_MASK) >>
                XDPTX_DPCD_DOWNSP_TYPE_SHIFT;
        u8 FormatConversionBlockPresent = Dpcd[XDPTX_DPCD_DOWNSP_PRESENT] &
                XDPTX_DPCD_DOWNSP_FORMAT_CONV_MASK;
        u8 DetailedCapInfoAvailable = Dpcd[XDPTX_DPCD_DOWNSP_PRESENT] &
                XDPTX_DPCD_DOWNSP_DCAP_INFO_AVAIL_MASK;
        u8 MainLinkAnsi8b10bCodingSupport = Dpcd[XDPTX_DPCD_ML_CH_CODING_CAP] &
                XDPTX_DPCD_ML_CH_CODING_MASK;
        u8 DownstreamPortCount = Dpcd[XDPTX_DPCD_DOWNSP_COUNT_MSA_OUI] &
                XDPTX_DPCD_DOWNSP_COUNT_MASK;
        u8 MsaTimingParIgnored = Dpcd[XDPTX_DPCD_DOWNSP_COUNT_MSA_OUI] &
                XDPTX_DPCD_MSA_TIMING_PAR_IGNORED_MASK;
        u8 OuiSupported = Dpcd[XDPTX_DPCD_DOWNSP_COUNT_MSA_OUI] &
        		XDPTX_DPCD_OUI_SUPPORT_MASK;
        u8 RxPort0LocalEdidPresent = Dpcd[XDPTX_DPCD_RX_PORT0_CAP_0] &
                XDPTX_DPCD_RX_PORTX_CAP_0_LOCAL_EDID_PRESENT_MASK;
        u8 RxPort0AssocPrecedePort = Dpcd[XDPTX_DPCD_RX_PORT0_CAP_0] &
                XDPTX_DPCD_RX_PORTX_CAP_0_ASSOC_TO_PRECEDING_PORT_MASK;
        u8 RxPort0BufSizeBpl = (Dpcd[XDPTX_DPCD_RX_PORT0_CAP_1] + 1) * 32;
        u8 RxPort1LocalEdidPresent = Dpcd[XDPTX_DPCD_RX_PORT1_CAP_0] &
                XDPTX_DPCD_RX_PORTX_CAP_0_LOCAL_EDID_PRESENT_MASK;
        u8 RxPort1AssocPrecedePort = Dpcd[XDPTX_DPCD_RX_PORT1_CAP_0] &
                XDPTX_DPCD_RX_PORTX_CAP_0_ASSOC_TO_PRECEDING_PORT_MASK;
        u8 RxPort1BufSizeBpl = (Dpcd[XDPTX_DPCD_RX_PORT1_CAP_1] + 1) * 32;
        u8 IicSpeed = Dpcd[XDPTX_DPCD_I2C_SPEED_CTL_CAP];
        u8 EdpAltScramblerResetCap = Dpcd[XDPTX_DPCD_EDP_CFG_CAP] &
                (0x1);
        u8 EdpFramingChangeCap = Dpcd[XDPTX_DPCD_EDP_CFG_CAP] &
                (0x2);
        u8 TraingAuxReadInt = Dpcd[XDPTX_DPCD_TRAIN_AUX_RD_INTERVAL];
        u8 AdapterForceLoadSenseCap = Dpcd[XDPTX_DPCD_ADAPTER_CAP] &
                (0x01);
        u8 AdapterAltI2CPatternCap = Dpcd[XDPTX_DPCD_ADAPTER_CAP] &
                (0x02);
        u8 FauxCap = Dpcd[XDPTX_DPCD_FAUX_CAP] &
                XDPTX_DPCD_FAUX_CAP_MASK;
        u8 MstmCap = Dpcd[XDPTX_DPCD_MSTM_CAP] &
                XDPTX_DPCD_MST_CAP_MASK;
        u8 NumAudioEps = Dpcd[XDPTX_DPCD_NUM_AUDIO_EPS];

        xil_printf("RX capabilities:\n\r"
                        "\tDPCD rev major (0x00000): %d\n\r"
                        "\tDPCD rev minor (0x00000): %d\n\r"
                        "\tMax link rate (0x00001): %s\n\r"
                        "\tMax lane count (0x00002): %d\n\r"
                        "\tTPS3 supported (0x00002): %s\n\r"
                        "\tEnhanced frame support? (0x00002) %s\n\r"
                        "\tMax downspread support? (0x00003) %s\n\r"
                        "\tNo AUX handshake required? (0x00003)%s\n\r"
                        "\t# of receiver ports (0x00004): %d\n\r"
                        "\tDP power 5v cap? (0x00004) %s\n\r"
                        "\tDP power 12v cap? (0x00004) %s\n\r"
                        "\tDP power 18v cap? (0x00004) %s\n\r"
                        "\tDownstream ports present? (0x00005) %s\n\r"
                        "\tDownstreamport0 type (0x00005): %s\n\r"
                        "\tFormat conversion block present (0x00005): %s\n\r"
                        "\tDetailed cap info available? (0x00005) %s\n\r"
                        "\tMain link ANSI 8b/10b channel coding support? (0x00006) %s\n\r"
                        "\tDownstream port count (0x00007): %d\n\r"
                        "\tMSA timing paramaters ignored? (0x00007) %s\n\r"
                        "\tOUI supported? (0x00007) %s\n\r"
                        "\tReceive port0 local edid present (0x00008) %s\n\r"
                        "\tReceive port0 associated to preceding port? (0x00008) %s\n\r"
                        "\tReceive port0 buffer size (0x00009): %d bytes per lane\n\r"
                        "\tReceive port1 local edid present (0x0000A) %s\n\r"
                        "\tReceive port1 associated to preceding port? (0x0000A) %s\n\r"
                        "\tReceive port1 buffer size (0x0000B): %d bytes per lane\n\r"
                        "\tI2C speed (0x0000C): %s\n\r"
                        "\tEDP alt scrambler reset cap? (0x0000D) %s\n\r"
                        "\tEDP framing change cap? (0x0000D) %s\n\r"
                        "\tTraining AUX read interval (0x0000E): %s\n\r"
                        "\tAdapter force load sense cap? (0x0000F) %s\n\r"
                        "\tAdapter alt i2c pattern cap? (0x0000F) %s\n\r"
                        "\tFaux cap?  (0x00020) %s\n\r"
                        "\tMSTM cap? (0x00021) %s\n\r"
                        "\t# of audio eps (0x00022) : %d\n\r",
                DpcdRevMajor,
                DpcdRevMinor,
                (MaxLinkRate == XDPTX_DPCD_LINK_BW_SET_162GBPS) ? "1.62Gbps" :
                 (MaxLinkRate == XDPTX_DPCD_LINK_BW_SET_270GBPS) ? "2.70Gbps" :
                 (MaxLinkRate == XDPTX_DPCD_LINK_BW_SET_540GBPS) ? "5.40Gbps" :
                 "Unknown link rate",
                MaxLaneCount,
                Tps3Support ? "Y" : "N",
                EnhancedFrameSupport ? "Y" : "N",
                MaxDownspreadSupport ? "Y" : "N",
                NoAuxHandshakeRequired ? "Y" : "N",
                NumRxPorts,
                PwrDpCap5V ? "Y" : "N",
                PwrDpCap12V ? "Y" : "N",
                PwrDpCap18V ? "Y" : "N",
                DownstreamPortPresent ? "Y" : "N",
                (DownPortType == XDPTX_DPCD_DOWNSP_TYPE_DP) ?
                        "DisplayPort" :
                 (DownPortType == XDPTX_DPCD_DOWNSP_TYPE_AVGA_ADVII) ?
                        "Analog VGA or analog video over DVI-I" :
                 (DownPortType == XDPTX_DPCD_DOWNSP_TYPE_DVI_HDMI_DPPP) ?
                        "DVI, HDMI, or DP++" :
                 (DownPortType == XDPTX_DPCD_DOWNSP_TYPE_OTHERS) ?
                        "Others" :
                 "Unknown downstream port type",
                FormatConversionBlockPresent ? "Y" : "N",
                DetailedCapInfoAvailable ? "Y" : "N",
                MainLinkAnsi8b10bCodingSupport ? "Y" : "N",
                DownstreamPortCount,
                MsaTimingParIgnored ? "Y" : "N",
                OuiSupported ? "Y" : "N",
                RxPort0LocalEdidPresent ? "Y" : "N",
                RxPort0AssocPrecedePort ? "Y" : "N",
                RxPort0BufSizeBpl,
                RxPort1LocalEdidPresent ? "Y" : "N",
                RxPort1AssocPrecedePort ? "Y" : "N",
                RxPort1BufSizeBpl,
                (IicSpeed == XDPTX_DPCD_I2C_SPEED_CTL_NONE) ? "No control" :
                 (IicSpeed == XDPTX_DPCD_I2C_SPEED_CTL_1KBIPS) ? "1Kbps" :
                 (IicSpeed == XDPTX_DPCD_I2C_SPEED_CTL_5KBIPS) ? "5Kbps" :
                 (IicSpeed == XDPTX_DPCD_I2C_SPEED_CTL_10KBIPS) ? "10Kbps" :
                 (IicSpeed == XDPTX_DPCD_I2C_SPEED_CTL_100KBIPS) ? "100Kbps" :
                 (IicSpeed == XDPTX_DPCD_I2C_SPEED_CTL_400KBIPS) ? "400Kbps" :
                 (IicSpeed == XDPTX_DPCD_I2C_SPEED_CTL_1MBIPS) ? "1Mbps" :
                 "Unknown I2C speed",
                EdpAltScramblerResetCap ? "Y" : "N",
                EdpFramingChangeCap ? "Y" : "N",
                (TraingAuxReadInt
                        == XDPTX_DPCD_TRAIN_AUX_RD_INT_100_400US) ?
                        "100us for CR, 400us for CE" :
                 (TraingAuxReadInt
                        == XDPTX_DPCD_TRAIN_AUX_RD_INT_4MS) ?
                        "4us" :
                 (TraingAuxReadInt
                        == XDPTX_DPCD_TRAIN_AUX_RD_INT_8MS) ?
                        "8us" :
                 (TraingAuxReadInt
                        == XDPTX_DPCD_TRAIN_AUX_RD_INT_12MS) ?
                        "12us" :
                 (TraingAuxReadInt
                        == XDPTX_DPCD_TRAIN_AUX_RD_INT_16MS) ?
                        "16us" :
                 "Unknown AUX read interval",
                AdapterForceLoadSenseCap ? "Y" : "N",
                AdapterAltI2CPatternCap ? "Y" : "N",
                FauxCap ? "Y" : "N",
                MstmCap ? "Y" : "N",
                NumAudioEps
        );
}

/******************************************************************************/
/**
 * This function prints the current link configuration.
 * 
 * @param       InstancePtr is a pointer to the XDptx instance.
*******************************************************************************/
void XDptx_DbgPrintLinkConfig(XDptx *InstancePtr)
{
        XDptx_LinkConfig *LinkConfig = &InstancePtr->LinkConfig;

        xil_printf("Main link configuration:\n\r"
                        "\tLink rate: %s\n\r"
                        "\tLane count: %d\n\r",
                (LinkConfig->LinkRate == XDPTX_DPCD_LINK_BW_SET_162GBPS) ? "1.62Gbps" :
                 (LinkConfig->LinkRate == XDPTX_DPCD_LINK_BW_SET_270GBPS) ? "2.70Gbps" :
                 (LinkConfig->LinkRate == XDPTX_DPCD_LINK_BW_SET_540GBPS) ? "5.40Gbps" :
                 "Unknown link rate",
                LinkConfig->LaneCount
        );
}

/******************************************************************************/
/**
 * This function prints the current main stream attributes from the DisplayPort
 * TX core.
 * 
 * @param       InstancePtr is a pointer to the XDptx instance.
*******************************************************************************/
void XDptx_DbgPrintMSAValuesTx(XDptx *InstancePtr)
{
        XDptx_Config *TxConfig = &InstancePtr->Config;

        xil_printf("TX MSA registers:\n\r"
                       "\tClocks, H Total    	 	  (0x180) : %d\n\r"
                        "\tClocks, V Total      	  (0x184) : %d\n\r"
                        "\tPolarity (V / H)     	  (0x188) : %d\n\r"
                        "\tHSync Width          	  (0x180) : %d\n\r"
                        "\tVSync Width          	  (0x180) : %d\n\r"
                        "\tHorz Resolution      	  (0x180) : %d\n\r"
                        "\tVert Resolution      	  (0x180) : %d\n\r"
                        "\tHorz Start           	  (0x180) : %d\n\r"
                        "\tVert Start           	  (0x180) : %d\n\r"
                        "\tMisc0                	  (0x180) : 0x%08X\n\r"
                        "\tMisc1                	  (0x180) : 0x%08X\n\r"
                        "\tUser Pixel Width     	  (0x180) : %d\n\r"
                        "\tM Vid                	  (0x180) : %d\n\r"
                        "\tN Vid                	  (0x180) : %d\n\r"
                        "\tTransfer Unit Size   	  (0x180) : %d\n\r"
                        "\tUser Data Count      	  (0x180) : %d\n\r"
                        "\tMinimum bytes per TU 	  (0x180) : %d\n\r"
                        "\tFractional bytes per TU	  (0x180) : %d\n\r"
                        "\tInit wait              	  (0x180) : %d\n\r",
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_MAIN_STREAM_HTOTAL),
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_MAIN_STREAM_VTOTAL),
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_MAIN_STREAM_POLARITY),
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_MAIN_STREAM_HSWIDTH),
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_MAIN_STREAM_VSWIDTH),
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_MAIN_STREAM_HRES),
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_MAIN_STREAM_VRES),
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_MAIN_STREAM_HSTART),
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_MAIN_STREAM_VSTART),
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_MAIN_STREAM_MISC0),
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_MAIN_STREAM_MISC1),
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_USER_PIXEL_WIDTH),
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_M_VID),
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_N_VID),
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_TU_SIZE),
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_USER_DATA_COUNT_PER_LANE),
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_MIN_BYTES_PER_TU),
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_FRAC_BYTES_PER_TU),
                XDptx_ReadReg(TxConfig->BaseAddr, XDPTX_INIT_WAIT)
        );
}

/******************************************************************************/
/**
 * This function prints the current main stream attributes stored in the MSA
 * configuration structure.
 * 
 * @param       InstancePtr is a pointer to the XDptx instance.
*******************************************************************************/
void XDptx_DbgPrintMsaValues(XDptx *InstancePtr)
{
        XDptx_MainStreamAttributes *MsaConfig = &InstancePtr->MsaConfig[0];

        xil_printf("MSA values:\n\r"
                        "\tHClkTotal = %d\n\r"
                        "\tVClkTotal = %d\n\r"
                        "\tHStart = %d\n\r"
                        "\tVStart = %d\n\r"
                        "\tMisc0 = %d\n\r"
                        "\tMisc1 = %d\n\r"
        				"\tNVid = %d\n\r"
                		"\tUserPixelWidth = %d\n\r"
                		"\tDataPerLane = %d\n\r"
        				"\tAvgBytesPerTU = %d\n\r"
        				"\tTransferUnitSize = %d\n\r"
						"\tInitWait = %d\n\r"
						"\tBitsPerColor = %d\n\r"
						"\tComponentFormat = %d\n\r"
						"\tDynamicRange = %d\n\r"
						"\tYCbCrColorimetry = %d\n\r"
						"\tSynchronousClockMode = %d\n\r"
						"\tOverrideUserPixelWidth = %d\n\r",
                MsaConfig->HClkTotal,
                MsaConfig->VClkTotal,
                MsaConfig->HStart,
                MsaConfig->VStart,
                MsaConfig->Misc0,
                MsaConfig->Misc1,
                MsaConfig->NVid,
                MsaConfig->UserPixelWidth,
                MsaConfig->DataPerLane,
                MsaConfig->AvgBytesPerTU,
                MsaConfig->TransferUnitSize,
                MsaConfig->InitWait,
                MsaConfig->BitsPerColor,
                MsaConfig->ComponentFormat,
                MsaConfig->DynamicRange,
                MsaConfig->YCbCrColorimetry,
                MsaConfig->SynchronousClockMode,
                MsaConfig->OverrideUserPixelWidth

        );
}

/******************************************************************************/
/**
 * This function prints the contents of the EDID.
 * 
 * @param       InstancePtr is a pointer to the XDptx instance.
*******************************************************************************/
void XDptx_DbgPrintEdid(XDptx *InstancePtr)
{
	u8 i = 0;
	u8 Edid[128];
	static u32 first_time=0;
	for(i=0; i<128;i=i+1){
		Edid[i] = 0xFF;
	}
	if (InstancePtr->Config.MstSupport == 0) {
	/* SST mode sink is accessed directly. */
	XDptx_GetEdid(InstancePtr, Edid);
	}
	else {
	/* MST mode has remote sinks. */
	u8 SinkNum;

	xil_printf("%d sinks found, display EDID for which sink #? (Choices 0-%d):\n\r",
	InstancePtr->Topology.SinkTotal, InstancePtr->Topology.SinkTotal - 1);
	if(first_time==0)
	{
		SinkNum = 0;
		first_time = 1;
	}
	else
	{
		//SinkNum = XUartLite_RecvByte(STDIN_BASEADDRESS) - '0';
		SinkNum = XUartPs_RecvByte(STDIN_BASEADDRESS) - '0';
	}

	if ((SinkNum < 0) || (SinkNum > (InstancePtr->Topology.SinkTotal - 1))) {
	xil_printf("%d is an invalid choice. Returning to main menu.\n\r", SinkNum);
	return;
	}

	XDptx_TopologyNode *SelSink = InstancePtr->Topology.SinkList[SinkNum];

	xil_printf("> Sink #%d: LCT = %d ; RAD = ", SinkNum, SelSink->LinkCountTotal);

	for (i = 0; i < SelSink->LinkCountTotal; i++) {
	xil_printf("%d ", SelSink->RelativeAddress[i]);
	}
	xil_printf("\n\r");

	/* Fetch the EDID base block from the remote sink in the topology. */
	XDptx_GetRemoteEdid(InstancePtr, SelSink->LinkCountTotal, SelSink->RelativeAddress, Edid);
	}
	xil_printf("Edid contents:");
	for (i = 0; i < 128; i++) {
		if ((i % 16) == 0) {
			xil_printf("\n\r\t");
		}

		xil_printf("%02lX ", Edid[i]);
	}

	xil_printf("\n\r");

	Edid_PrintDecodeBase(Edid);
}
