//Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2014.2 (win64) Build 932637 Wed Jun 11 13:33:10 MDT 2014
//Date        : Sun Mar 22 07:43:06 2015
//Host        : GCLAP4 running 64-bit major release  (build 9200)
//Command     : generate_target dp_system_wrapper.bd
//Design      : dp_system_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module dp_system_wrapper
   (DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    SYS_CLK_clk_n,
    SYS_CLK_clk_p,
    aux_tx_io_n,
    aux_tx_io_p,
    clk_out2,
    dp_mainlink_lnk_clk,
    dp_mainlink_lnk_clk_n,
    dp_mainlink_lnk_clk_p,
    dp_mainlink_lnk_tx_lane_n,
    dp_mainlink_lnk_tx_lane_p,
    gpo,
    iic2inctc_irpt_scl_io,
    iic2inctc_irpt_sda_io,
    tx_hpd);
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  input SYS_CLK_clk_n;
  input SYS_CLK_clk_p;
  inout aux_tx_io_n;
  inout aux_tx_io_p;
  output clk_out2;
  output dp_mainlink_lnk_clk;
  input dp_mainlink_lnk_clk_n;
  input dp_mainlink_lnk_clk_p;
  output [3:0]dp_mainlink_lnk_tx_lane_n;
  output [3:0]dp_mainlink_lnk_tx_lane_p;
  output [1:0]gpo;
  inout iic2inctc_irpt_scl_io;
  inout iic2inctc_irpt_sda_io;
  input tx_hpd;

  wire [14:0]DDR_addr;
  wire [2:0]DDR_ba;
  wire DDR_cas_n;
  wire DDR_ck_n;
  wire DDR_ck_p;
  wire DDR_cke;
  wire DDR_cs_n;
  wire [3:0]DDR_dm;
  wire [31:0]DDR_dq;
  wire [3:0]DDR_dqs_n;
  wire [3:0]DDR_dqs_p;
  wire DDR_odt;
  wire DDR_ras_n;
  wire DDR_reset_n;
  wire DDR_we_n;
  wire FIXED_IO_ddr_vrn;
  wire FIXED_IO_ddr_vrp;
  wire [53:0]FIXED_IO_mio;
  wire FIXED_IO_ps_clk;
  wire FIXED_IO_ps_porb;
  wire FIXED_IO_ps_srstb;
  wire SYS_CLK_clk_n;
  wire SYS_CLK_clk_p;
  wire aux_tx_io_n;
  wire aux_tx_io_p;
  wire clk_out2;
  wire dp_mainlink_lnk_clk;
  wire dp_mainlink_lnk_clk_n;
  wire dp_mainlink_lnk_clk_p;
  wire [3:0]dp_mainlink_lnk_tx_lane_n;
  wire [3:0]dp_mainlink_lnk_tx_lane_p;
  wire [1:0]gpo;
  wire iic2inctc_irpt_scl_i;
  wire iic2inctc_irpt_scl_io;
  wire iic2inctc_irpt_scl_o;
  wire iic2inctc_irpt_scl_t;
  wire iic2inctc_irpt_sda_i;
  wire iic2inctc_irpt_sda_io;
  wire iic2inctc_irpt_sda_o;
  wire iic2inctc_irpt_sda_t;
  wire tx_hpd;

dp_system dp_system_i
       (.DDR_addr(DDR_addr),
        .DDR_ba(DDR_ba),
        .DDR_cas_n(DDR_cas_n),
        .DDR_ck_n(DDR_ck_n),
        .DDR_ck_p(DDR_ck_p),
        .DDR_cke(DDR_cke),
        .DDR_cs_n(DDR_cs_n),
        .DDR_dm(DDR_dm),
        .DDR_dq(DDR_dq),
        .DDR_dqs_n(DDR_dqs_n),
        .DDR_dqs_p(DDR_dqs_p),
        .DDR_odt(DDR_odt),
        .DDR_ras_n(DDR_ras_n),
        .DDR_reset_n(DDR_reset_n),
        .DDR_we_n(DDR_we_n),
        .FIXED_IO_ddr_vrn(FIXED_IO_ddr_vrn),
        .FIXED_IO_ddr_vrp(FIXED_IO_ddr_vrp),
        .FIXED_IO_mio(FIXED_IO_mio),
        .FIXED_IO_ps_clk(FIXED_IO_ps_clk),
        .FIXED_IO_ps_porb(FIXED_IO_ps_porb),
        .FIXED_IO_ps_srstb(FIXED_IO_ps_srstb),
        .SYS_CLK_clk_n(SYS_CLK_clk_n),
        .SYS_CLK_clk_p(SYS_CLK_clk_p),
        .aux_tx_io_n(aux_tx_io_n),
        .aux_tx_io_p(aux_tx_io_p),
        .clk_out2(clk_out2),
        .dp_mainlink_lnk_clk(dp_mainlink_lnk_clk),
        .dp_mainlink_lnk_clk_n(dp_mainlink_lnk_clk_n),
        .dp_mainlink_lnk_clk_p(dp_mainlink_lnk_clk_p),
        .dp_mainlink_lnk_tx_lane_n(dp_mainlink_lnk_tx_lane_n),
        .dp_mainlink_lnk_tx_lane_p(dp_mainlink_lnk_tx_lane_p),
        .gpo(gpo),
        .iic2inctc_irpt_scl_i(iic2inctc_irpt_scl_i),
        .iic2inctc_irpt_scl_o(iic2inctc_irpt_scl_o),
        .iic2inctc_irpt_scl_t(iic2inctc_irpt_scl_t),
        .iic2inctc_irpt_sda_i(iic2inctc_irpt_sda_i),
        .iic2inctc_irpt_sda_o(iic2inctc_irpt_sda_o),
        .iic2inctc_irpt_sda_t(iic2inctc_irpt_sda_t),
        .tx_hpd(tx_hpd));
IOBUF iic2inctc_irpt_scl_iobuf
       (.I(iic2inctc_irpt_scl_o),
        .IO(iic2inctc_irpt_scl_io),
        .O(iic2inctc_irpt_scl_i),
        .T(iic2inctc_irpt_scl_t));
IOBUF iic2inctc_irpt_sda_iobuf
       (.I(iic2inctc_irpt_sda_o),
        .IO(iic2inctc_irpt_sda_io),
        .O(iic2inctc_irpt_sda_i),
        .T(iic2inctc_irpt_sda_t));
endmodule
