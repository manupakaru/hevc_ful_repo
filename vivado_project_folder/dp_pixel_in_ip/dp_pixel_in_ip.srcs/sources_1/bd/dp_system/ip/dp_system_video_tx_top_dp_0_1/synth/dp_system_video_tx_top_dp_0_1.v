// (c) Copyright 1995-2015 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:user:video_tx_top_dp:1.2
// IP Revision: 11

(* X_CORE_INFO = "video_tx_top_dp,Vivado 2014.2" *)
(* CHECK_LICENSE_TYPE = "dp_system_video_tx_top_dp_0_1,video_tx_top_dp,{}" *)
(* CORE_GENERATION_INFO = "dp_system_video_tx_top_dp_0_1,video_tx_top_dp,{x_ipProduct=Vivado 2014.2,x_ipVendor=xilinx.com,x_ipLibrary=user,x_ipName=video_tx_top_dp,x_ipVersion=1.2,x_ipCoreRevision=11,x_ipLanguage=VERILOG,FREQ_IN=150,FREQ_OUT=75,VIDEO_OUT=1080P30}" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module dp_system_video_tx_top_dp_0_1 (
  clk,
  reset,
  vid_clk,
  config_done_in,
  fifo_data_in,
  fifo_empty,
  fifo_prog_empty,
  fifo_rd_en,
  vid_hsync,
  vid_vsync,
  vid_enable,
  vid_rst,
  vid_clk_en,
  vid_oddeven,
  vid_pixel0,
  vid_pixel1,
  vid_pixel2,
  vid_pixel3
);

input wire clk;
input wire reset;
(* X_INTERFACE_INFO = "xilinx.com:interface:dp_vid:1.0 dp_video_inf TX_VID_CLK" *)
input wire vid_clk;
input wire config_done_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 dp_fifo_read RD_DATA" *)
input wire [63 : 0] fifo_data_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 dp_fifo_read EMPTY" *)
input wire fifo_empty;
input wire fifo_prog_empty;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 dp_fifo_read RD_EN" *)
output wire fifo_rd_en;
(* X_INTERFACE_INFO = "xilinx.com:interface:dp_vid:1.0 dp_video_inf TX_VID_HSYNC" *)
output wire vid_hsync;
(* X_INTERFACE_INFO = "xilinx.com:interface:dp_vid:1.0 dp_video_inf TX_VID_VSYNC" *)
output wire vid_vsync;
(* X_INTERFACE_INFO = "xilinx.com:interface:dp_vid:1.0 dp_video_inf TX_VID_ENABLE" *)
output wire vid_enable;
output wire vid_rst;
output wire vid_clk_en;
(* X_INTERFACE_INFO = "xilinx.com:interface:dp_vid:1.0 dp_video_inf TX_VID_ODDEVEN" *)
output wire vid_oddeven;
(* X_INTERFACE_INFO = "xilinx.com:interface:dp_vid:1.0 dp_video_inf TX_VID_PIXEL0" *)
output wire [47 : 0] vid_pixel0;
(* X_INTERFACE_INFO = "xilinx.com:interface:dp_vid:1.0 dp_video_inf TX_VID_PIXEL1" *)
output wire [47 : 0] vid_pixel1;
(* X_INTERFACE_INFO = "xilinx.com:interface:dp_vid:1.0 dp_video_inf TX_VID_PIXEL2" *)
output wire [47 : 0] vid_pixel2;
(* X_INTERFACE_INFO = "xilinx.com:interface:dp_vid:1.0 dp_video_inf TX_VID_PIXEL3" *)
output wire [47 : 0] vid_pixel3;

  video_tx_top_dp #(
    .FREQ_IN(150),
    .FREQ_OUT(75),
    .VIDEO_OUT("1080P30")
  ) inst (
    .clk(clk),
    .reset(reset),
    .vid_clk(vid_clk),
    .config_done_in(config_done_in),
    .fifo_data_in(fifo_data_in),
    .fifo_empty(fifo_empty),
    .fifo_prog_empty(fifo_prog_empty),
    .fifo_rd_en(fifo_rd_en),
    .vid_hsync(vid_hsync),
    .vid_vsync(vid_vsync),
    .vid_enable(vid_enable),
    .vid_rst(vid_rst),
    .vid_clk_en(vid_clk_en),
    .vid_oddeven(vid_oddeven),
    .vid_pixel0(vid_pixel0),
    .vid_pixel1(vid_pixel1),
    .vid_pixel2(vid_pixel2),
    .vid_pixel3(vid_pixel3)
  );
endmodule
