// (c) Copyright 1995-2014 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.

// IP VLNV: xilinx.com:user:axi_displaybuffer_handle:1.14
// IP Revision: 53

// The following must be inserted into your Verilog file for this
// core to be instantiated. Change the instance name and port connections
// (in parentheses) to your own signal names.

//----------- Begin Cut here for INSTANTIATION Template ---// INST_TAG
dp_system_axi_displaybuffer_handle_0_0 your_instance_name (
  .clk(clk),                                                                // input wire clk
  .reset(reset),                                                            // input wire reset
  .displaybuffer_fifo_read_en_out(displaybuffer_fifo_read_en_out),          // output wire displaybuffer_fifo_read_en_out
  .displaybuffer_fifo_empty_in(displaybuffer_fifo_empty_in),                // input wire displaybuffer_fifo_empty_in
  .displaybuffer_fifo_almost_empty_in(displaybuffer_fifo_almost_empty_in),  // input wire displaybuffer_fifo_almost_empty_in
  .displaybuffer_fifo_data_in(displaybuffer_fifo_data_in),                  // input wire [783 : 0] displaybuffer_fifo_data_in
  .log2_ctu_size_in(log2_ctu_size_in),                                      // input wire [2 : 0] log2_ctu_size_in
  .poc_4bits_in(poc_4bits_in),                                              // input wire [3 : 0] poc_4bits_in
  .pic_width_in(pic_width_in),                                              // input wire [10 : 0] pic_width_in
  .pic_height_in(pic_height_in),                                            // input wire [10 : 0] pic_height_in
  .config_valid_in(config_valid_in),                                        // input wire config_valid_in
  .axi_clk(axi_clk),                                                        // input wire axi_clk
  .axi_awaddr(axi_awaddr),                                                  // output wire [31 : 0] axi_awaddr
  .axi_awvalid(axi_awvalid),                                                // output wire axi_awvalid
  .axi_awready(axi_awready),                                                // input wire axi_awready
  .axi_awlen(axi_awlen),                                                    // output wire [7 : 0] axi_awlen
  .axi_awid(axi_awid),                                                      // output wire [3 : 0] axi_awid
  .axi_awburst(axi_awburst),                                                // output wire [1 : 0] axi_awburst
  .axi_awsize(axi_awsize),                                                  // output wire [2 : 0] axi_awsize
  .axi_wid(axi_wid),                                                        // output wire [0 : 0] axi_wid
  .axi_wdata(axi_wdata),                                                    // output wire [511 : 0] axi_wdata
  .axi_wlast(axi_wlast),                                                    // output wire axi_wlast
  .axi_wvalid(axi_wvalid),                                                  // output wire axi_wvalid
  .axi_wready(axi_wready),                                                  // input wire axi_wready
  .axi_wstrb(axi_wstrb),                                                    // output wire [63 : 0] axi_wstrb
  .axi_bresp(axi_bresp),                                                    // input wire [1 : 0] axi_bresp
  .axi_bvalid(axi_bvalid),                                                  // input wire axi_bvalid
  .axi_bready(axi_bready),                                                  // output wire axi_bready
  .axi_araddr(axi_araddr),                                                  // output wire [31 : 0] axi_araddr
  .axi_arvalid(axi_arvalid),                                                // output wire axi_arvalid
  .axi_arready(axi_arready),                                                // input wire axi_arready
  .axi_arlen(axi_arlen),                                                    // output wire [7 : 0] axi_arlen
  .axi_arburst(axi_arburst),                                                // output wire [1 : 0] axi_arburst
  .axi_arsize(axi_arsize),                                                  // output wire [2 : 0] axi_arsize
  .axi_arid(axi_arid),                                                      // output wire [3 : 0] axi_arid
  .axi_rdata(axi_rdata),                                                    // input wire [511 : 0] axi_rdata
  .axi_rresp(axi_rresp),                                                    // input wire [1 : 0] axi_rresp
  .axi_rlast(axi_rlast),                                                    // input wire axi_rlast
  .axi_rready(axi_rready),                                                  // output wire axi_rready
  .axi_rvalid(axi_rvalid),                                                  // input wire axi_rvalid
  .hdmi_fifo_almost_full(hdmi_fifo_almost_full),                            // input wire hdmi_fifo_almost_full
  .hdmi_fifo_data_out(hdmi_fifo_data_out),                                  // output wire [63 : 0] hdmi_fifo_data_out
  .hdmi_fifo_w_en(hdmi_fifo_w_en)                                          // output wire hdmi_fifo_w_en
);
// INST_TAG_END ------ End INSTANTIATION Template ---------

// You must compile the wrapper file dp_system_axi_displaybuffer_handle_0_0.v when simulating
// the core, dp_system_axi_displaybuffer_handle_0_0. When compiling the wrapper file, be sure to
// reference the Verilog simulation library.

