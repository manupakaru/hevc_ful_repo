// (c) Copyright 1995-2014 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:user:dummy_display_fifo_driver:2.1
// IP Revision: 16

(* X_CORE_INFO = "dummy_display_fifo_driver,Vivado 2014.2" *)
(* CHECK_LICENSE_TYPE = "dp_system_dummy_display_fifo_driver_0_0,dummy_display_fifo_driver,{}" *)
(* CORE_GENERATION_INFO = "dp_system_dummy_display_fifo_driver_0_0,dummy_display_fifo_driver,{x_ipProduct=Vivado 2014.2,x_ipVendor=xilinx.com,x_ipLibrary=user,x_ipName=dummy_display_fifo_driver,x_ipVersion=2.1,x_ipCoreRevision=16,x_ipLanguage=VERILOG,MAX_PIC_WIDTH=2048,MAX_PIC_HEIGHT=2048,CTB_SIZE_WIDTH=6,CB_SIZE_WIDTH=7,TB_SIZE_WIDTH=6,LOG2_CTB_WIDTH=3,PIC_DIM_WIDTH=11,LOG2_MIN_COLLOCATE_SIZE=4,DBF_SAMPLE_XY_ADDR=3,RPS_HEADER_ADDR_WIDTH=6,NUM_NEG_POS_POC_WIDTH=4,RPS_HEADER_DATA_WIDTH=12,RPS_ENTRY_DATA_WIDTH=17,RPS_ENTRY_ADDR_WIDTH=10,RPS_HEADER_NUM_POS_POC_RANGE_HIGH=11,RPS_HEADER_NUM_POS_POC_RANGE_LOW=8,RPS_HEADER_NUM_NEG_POC_RANGE_HIGH=7,RPS_HEADER_NUM_NEG_POC_RANGE_LOW=4,RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH=3,RPS_HEADER_NUM_DELTA_POC_RANGE_LOW=0,RPS_ENTRY_USED_FLAG_RANGE_HIGH=16,RPS_ENTRY_USED_FLAG_RANGE_LOW=16,RPS_ENTRY_DELTA_POC_RANGE_HIGH=15,RPS_ENTRY_DELTA_POC_RANGE_LOW=0,RPS_ENTRY_DELTA_POC_MSB=15,BS_FIFO_WIDTH=8,X_ADDR_WDTH=12,X11_ADDR_WDTH=11,Y_ADDR_WDTH=12,Y11_ADDR_WDTH=11,LOG2_MIN_PU_SIZE=2,LOG2_MIN_TU_SIZE=2,LOG2_MIN_DU_SIZE=3,AVAILABLE_CONFIG_BUS_WIDTH=2,POC_WIDTH=32,DPB_FRAME_OFFSET_WIDTH=4,DPB_STATUS_WIDTH=3,DPB_FILLED_WIDTH=1,DPB_DATA_WIDTH=32,REF_PIC_LIST_POC_DATA_WIDTH=32,DPB_REF_PIC_ADDR_WIDTH=28,DPB_ADDR_WIDTH=4,REF_PIC_LIST_ADDR_WIDTH=4,REF_PIC_LIST_DATA_WIDTH=36,REF_POC_LIST5_ADDR_WIDTH=4,REF_POC_LIST5_DATA_WIDTH=19,INTER_TOP_CONFIG_BUS_MODE_WIDTH=5,INTER_TOP_CONFIG_BUS_WIDTH=32,MV_FIELD_DATA_WIDTH=74,MV_COL_AXI_DATA_WIDTH=512,DPB_POC_RANGE_HIGH=31,DPB_POC_RANGE_LOW=0,REF_PIC_LIST5_POC_RANGE_HIGH=15,REF_PIC_LIST5_POC_RANGE_LOW=0,REF_PIC_LIST5_DPB_STATE_HIGH=18,REF_PIC_LIST5_DPB_STATE_LOW=16,MERGE_CAND_TYPE_WIDTH=3,MAX_NUM_MERGE_CAND_CONST=5,AMVP_NUM_CAND_CONST=2,NUM_BI_PRED_CANDS=12,TX_WIDTH=16,DIST_SCALE_WIDTH=13,NUM_BI_PRED_CANDS_TYPES=4,SAO_OUT_FIFO_WIDTH=784,FILTER_PIXEL_WIDTH=16,NUM_IN_TO_8_FILTER=8,MAX_MV_PER_CU=4,STEP_8x8_IN_PUS=2,MV_L_FRAC_WIDTH_HIGH=2,MV_L_INT_WIDTH_LOW=2,MV_C_FRAC_WIDTH_HIGH=3,MV_C_INT_WIDTH_LOW=3,YY_WIDTH=512,CH_WIDTH=128}" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module dp_system_dummy_display_fifo_driver_0_0 (
  clk,
  reset,
  S00_AXI_AWID,
  S00_AXI_AWADDR,
  S00_AXI_AWLEN,
  S00_AXI_AWSIZE,
  S00_AXI_AWBURST,
  S00_AXI_AWLOCK,
  S00_AXI_AWCACHE,
  S00_AXI_AWPROT,
  S00_AXI_AWQOS,
  S00_AXI_AWVALID,
  S00_AXI_AWREADY,
  S00_AXI_WDATA,
  S00_AXI_WSTRB,
  S00_AXI_WLAST,
  S00_AXI_WVALID,
  S00_AXI_WREADY,
  S00_AXI_BID,
  S00_AXI_BRESP,
  S00_AXI_BVALID,
  S00_AXI_BREADY,
  S00_AXI_ARID,
  S00_AXI_ARADDR,
  S00_AXI_ARLEN,
  S00_AXI_ARSIZE,
  S00_AXI_ARBURST,
  S00_AXI_ARLOCK,
  S00_AXI_ARCACHE,
  S00_AXI_ARPROT,
  S00_AXI_ARQOS,
  S00_AXI_ARVALID,
  S00_AXI_ARREADY,
  S00_AXI_RID,
  S00_AXI_RDATA,
  S00_AXI_RRESP,
  S00_AXI_RLAST,
  S00_AXI_RVALID,
  S00_AXI_RREADY,
  fifo_prog_full_in,
  fifo_wr_en_out,
  fifo_data_out,
  poc_out,
  pic_width_out,
  pic_height_out
);

(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 signal_clock CLK" *)
input wire clk;
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 signal_reset RST" *)
input wire reset;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWID" *)
input wire [11 : 0] S00_AXI_AWID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *)
input wire [15 : 0] S00_AXI_AWADDR;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWLEN" *)
input wire [7 : 0] S00_AXI_AWLEN;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWSIZE" *)
input wire [2 : 0] S00_AXI_AWSIZE;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWBURST" *)
input wire [1 : 0] S00_AXI_AWBURST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWLOCK" *)
input wire S00_AXI_AWLOCK;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWCACHE" *)
input wire [3 : 0] S00_AXI_AWCACHE;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *)
input wire [2 : 0] S00_AXI_AWPROT;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWQOS" *)
input wire [3 : 0] S00_AXI_AWQOS;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *)
input wire S00_AXI_AWVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *)
output wire S00_AXI_AWREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *)
input wire [31 : 0] S00_AXI_WDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *)
input wire [4 : 0] S00_AXI_WSTRB;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WLAST" *)
input wire S00_AXI_WLAST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *)
input wire S00_AXI_WVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *)
output wire S00_AXI_WREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BID" *)
output wire [11 : 0] S00_AXI_BID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *)
output wire [1 : 0] S00_AXI_BRESP;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *)
output wire S00_AXI_BVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *)
input wire S00_AXI_BREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARID" *)
input wire [11 : 0] S00_AXI_ARID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *)
input wire [15 : 0] S00_AXI_ARADDR;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARLEN" *)
input wire [7 : 0] S00_AXI_ARLEN;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARSIZE" *)
input wire [2 : 0] S00_AXI_ARSIZE;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARBURST" *)
input wire [1 : 0] S00_AXI_ARBURST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARLOCK" *)
input wire S00_AXI_ARLOCK;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARCACHE" *)
input wire [3 : 0] S00_AXI_ARCACHE;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *)
input wire [2 : 0] S00_AXI_ARPROT;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARQOS" *)
input wire [3 : 0] S00_AXI_ARQOS;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *)
input wire S00_AXI_ARVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *)
output wire S00_AXI_ARREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RID" *)
output wire [11 : 0] S00_AXI_RID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *)
output wire [31 : 0] S00_AXI_RDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *)
output wire [1 : 0] S00_AXI_RRESP;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RLAST" *)
output wire S00_AXI_RLAST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *)
output wire S00_AXI_RVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *)
input wire S00_AXI_RREADY;
input wire fifo_prog_full_in;
output wire fifo_wr_en_out;
output wire [783 : 0] fifo_data_out;
output wire [3 : 0] poc_out;
output wire [11 : 0] pic_width_out;
output wire [11 : 0] pic_height_out;

  dummy_display_fifo_driver #(
    .MAX_PIC_WIDTH(2048),
    .MAX_PIC_HEIGHT(2048),
    .CTB_SIZE_WIDTH(6),
    .CB_SIZE_WIDTH(7),
    .TB_SIZE_WIDTH(6),
    .LOG2_CTB_WIDTH(3),
    .PIC_DIM_WIDTH(11),
    .LOG2_MIN_COLLOCATE_SIZE(4),
    .DBF_SAMPLE_XY_ADDR(3),
    .RPS_HEADER_ADDR_WIDTH(6),
    .NUM_NEG_POS_POC_WIDTH(4),
    .RPS_HEADER_DATA_WIDTH(12),
    .RPS_ENTRY_DATA_WIDTH(17),
    .RPS_ENTRY_ADDR_WIDTH(10),
    .RPS_HEADER_NUM_POS_POC_RANGE_HIGH(11),
    .RPS_HEADER_NUM_POS_POC_RANGE_LOW(8),
    .RPS_HEADER_NUM_NEG_POC_RANGE_HIGH(7),
    .RPS_HEADER_NUM_NEG_POC_RANGE_LOW(4),
    .RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH(3),
    .RPS_HEADER_NUM_DELTA_POC_RANGE_LOW(0),
    .RPS_ENTRY_USED_FLAG_RANGE_HIGH(16),
    .RPS_ENTRY_USED_FLAG_RANGE_LOW(16),
    .RPS_ENTRY_DELTA_POC_RANGE_HIGH(15),
    .RPS_ENTRY_DELTA_POC_RANGE_LOW(0),
    .RPS_ENTRY_DELTA_POC_MSB(15),
    .BS_FIFO_WIDTH(8),
    .X_ADDR_WDTH(12),
    .X11_ADDR_WDTH(11),
    .Y_ADDR_WDTH(12),
    .Y11_ADDR_WDTH(11),
    .LOG2_MIN_PU_SIZE(2),
    .LOG2_MIN_TU_SIZE(2),
    .LOG2_MIN_DU_SIZE(3),
    .AVAILABLE_CONFIG_BUS_WIDTH(2),
    .POC_WIDTH(32),
    .DPB_FRAME_OFFSET_WIDTH(4),
    .DPB_STATUS_WIDTH(3),
    .DPB_FILLED_WIDTH(1),
    .DPB_DATA_WIDTH(32),
    .REF_PIC_LIST_POC_DATA_WIDTH(32),
    .DPB_REF_PIC_ADDR_WIDTH(28),
    .DPB_ADDR_WIDTH(4),
    .REF_PIC_LIST_ADDR_WIDTH(4),
    .REF_PIC_LIST_DATA_WIDTH(36),
    .REF_POC_LIST5_ADDR_WIDTH(4),
    .REF_POC_LIST5_DATA_WIDTH(19),
    .INTER_TOP_CONFIG_BUS_MODE_WIDTH(5),
    .INTER_TOP_CONFIG_BUS_WIDTH(32),
    .MV_FIELD_DATA_WIDTH(74),
    .MV_COL_AXI_DATA_WIDTH(512),
    .DPB_POC_RANGE_HIGH(31),
    .DPB_POC_RANGE_LOW(0),
    .REF_PIC_LIST5_POC_RANGE_HIGH(15),
    .REF_PIC_LIST5_POC_RANGE_LOW(0),
    .REF_PIC_LIST5_DPB_STATE_HIGH(18),
    .REF_PIC_LIST5_DPB_STATE_LOW(16),
    .MERGE_CAND_TYPE_WIDTH(3),
    .MAX_NUM_MERGE_CAND_CONST(5),
    .AMVP_NUM_CAND_CONST(2),
    .NUM_BI_PRED_CANDS(12),
    .TX_WIDTH(16),
    .DIST_SCALE_WIDTH(13),
    .NUM_BI_PRED_CANDS_TYPES(4),
    .SAO_OUT_FIFO_WIDTH(784),
    .FILTER_PIXEL_WIDTH(16),
    .NUM_IN_TO_8_FILTER(8),
    .MAX_MV_PER_CU(4),
    .STEP_8x8_IN_PUS(2),
    .MV_L_FRAC_WIDTH_HIGH(2),
    .MV_L_INT_WIDTH_LOW(2),
    .MV_C_FRAC_WIDTH_HIGH(3),
    .MV_C_INT_WIDTH_LOW(3),
    .YY_WIDTH(512),
    .CH_WIDTH(128)
  ) inst (
    .clk(clk),
    .reset(reset),
    .S00_AXI_AWID(S00_AXI_AWID),
    .S00_AXI_AWADDR(S00_AXI_AWADDR),
    .S00_AXI_AWLEN(S00_AXI_AWLEN),
    .S00_AXI_AWSIZE(S00_AXI_AWSIZE),
    .S00_AXI_AWBURST(S00_AXI_AWBURST),
    .S00_AXI_AWLOCK(S00_AXI_AWLOCK),
    .S00_AXI_AWCACHE(S00_AXI_AWCACHE),
    .S00_AXI_AWPROT(S00_AXI_AWPROT),
    .S00_AXI_AWQOS(S00_AXI_AWQOS),
    .S00_AXI_AWVALID(S00_AXI_AWVALID),
    .S00_AXI_AWREADY(S00_AXI_AWREADY),
    .S00_AXI_WDATA(S00_AXI_WDATA),
    .S00_AXI_WSTRB(S00_AXI_WSTRB),
    .S00_AXI_WLAST(S00_AXI_WLAST),
    .S00_AXI_WVALID(S00_AXI_WVALID),
    .S00_AXI_WREADY(S00_AXI_WREADY),
    .S00_AXI_BID(S00_AXI_BID),
    .S00_AXI_BRESP(S00_AXI_BRESP),
    .S00_AXI_BVALID(S00_AXI_BVALID),
    .S00_AXI_BREADY(S00_AXI_BREADY),
    .S00_AXI_ARID(S00_AXI_ARID),
    .S00_AXI_ARADDR(S00_AXI_ARADDR),
    .S00_AXI_ARLEN(S00_AXI_ARLEN),
    .S00_AXI_ARSIZE(S00_AXI_ARSIZE),
    .S00_AXI_ARBURST(S00_AXI_ARBURST),
    .S00_AXI_ARLOCK(S00_AXI_ARLOCK),
    .S00_AXI_ARCACHE(S00_AXI_ARCACHE),
    .S00_AXI_ARPROT(S00_AXI_ARPROT),
    .S00_AXI_ARQOS(S00_AXI_ARQOS),
    .S00_AXI_ARVALID(S00_AXI_ARVALID),
    .S00_AXI_ARREADY(S00_AXI_ARREADY),
    .S00_AXI_RID(S00_AXI_RID),
    .S00_AXI_RDATA(S00_AXI_RDATA),
    .S00_AXI_RRESP(S00_AXI_RRESP),
    .S00_AXI_RLAST(S00_AXI_RLAST),
    .S00_AXI_RVALID(S00_AXI_RVALID),
    .S00_AXI_RREADY(S00_AXI_RREADY),
    .fifo_prog_full_in(fifo_prog_full_in),
    .fifo_wr_en_out(fifo_wr_en_out),
    .fifo_data_out(fifo_data_out),
    .poc_out(poc_out),
    .pic_width_out(pic_width_out),
    .pic_height_out(pic_height_out)
  );
endmodule
