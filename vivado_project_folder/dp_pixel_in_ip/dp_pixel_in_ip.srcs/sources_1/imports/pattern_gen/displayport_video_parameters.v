
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:23:12 08/07/2014 
// Design Name: 
// Module Name:    displayport_video_parameters 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


//======================================================================
//[DISPLAY PORT] Link Rate=270, No Of Lanes=4
//======================================================================

//----------------------------------------------------------------	2160p30		2160p30(RB)	2160p24		1200p60		1080p30		1080p24		1080p60		1080p60(RB)	2160p30(HDMI)	1080p60(HDMI)	
//														clock(MHz)	338.75		262.75		266.75		154 		79.75		63  		173         138.5       297		        148.5		
`define 	HSW              		24'd44  			//			408 		32  		400 		32			192			192 		200         32          88  		    44  		
`define 	HBP              		24'd148 			//			648 		80  		616 		80			248			240 		328         80          296 		    148 		
`define 	HFP              		24'd88  			//			240 		48  		216 		48			56			48  		128         48          176 		    88  		
`define 	HRES             		24'd1920			//			3840		3840		3840		1920		1920		1920		1920        1920        3840		    1920		
`define 	VSW              		24'd5  				//			5   		5   		5   		6			5 			5   		5           5           10  		    5  		
`define 	VBP              		24'd36  			//			32  		23  		24  		26			14			10  		32          23          72  		    36  		
`define 	VFP              		24'd4   			//			3   		3   		3   		3			3			3   		3           3           8   		    4   		
`define 	VRES             		24'd1080			//			2160		2160		2160		1200		1080		1080		1080        1080        2160		    1080		
//----------------------------------------------------------------------------------------------------------------

`define 	CD               		24'h0_0010  
`define 	DW               		24'h0_0004  			// video interface width - 1,2,4 pixels
`define 	VPOL             		1'b1 
`define 	HPOL             		1'b1
`define 	VPOL_HPOL        		24'h0_0003 
`define 	HTOTAL           		`HFP + `HBP + `HSW + `HRES	
`define 	VTOTAL           		`VFP + `VBP + `VSW + `VRES	
`define 	VTOTAL_MINUS_VFP 		`VBP + `VSW + `VRES + 1						
`define 	WORDS_PER_LINE   		`HRES - 24'h0_0004 					//(HRES * BPP / 16) - 4 

`ifndef LANE_COUNT
	`define LANE_COUNT    	20'h4			// 1,2,4 lanes
`endif
`ifndef LINK_RATE
	`define LINK_RATE		20'h0000A 		// 1.6G - 06, 2.7G - 0A, 5.4G - 14
`endif

//Delay Element
`ifndef TCQ
	`define TCQ  	100 
`endif


// Parameters for Displayport video tx top controller

localparam		VIDEO_CLK_FREQ		= 	150 ;
localparam		DP_FIFO_WIDTH		= 	64 ;

localparam		FRAME_PCLK_COUNT			= 	((`HTOTAL) * (`VTOTAL)) / 2				;
localparam		HSYNC_PCLK_COUNT			= 	(`HTOTAL) / 2                       	;
localparam		DE_START_PCLK_COUNT			= 	((`VSW +`VBP) * (`HTOTAL)) / 2        	;
localparam		DE_STOP_PCLK_COUNT			= 	((`VSW +`VBP +`VRES) * (`HTOTAL))/2 	;
localparam		HSYNC_HIGH_PCLK_COUNT		= 	(`HSW) / 2                            	;
localparam		VSYNC_HIGH_PCLK_COUNT		= 	((`VSW) * (`HTOTAL)) / 2              	;
localparam		DE_HIGH_PCLK_COUNT			= 	(`HSW + `HBP ) / 2                    	;
localparam		DE_LOW_PCLK_COUNT			= 	(`HSW + `HBP + (`HRES) ) / 2          	;













