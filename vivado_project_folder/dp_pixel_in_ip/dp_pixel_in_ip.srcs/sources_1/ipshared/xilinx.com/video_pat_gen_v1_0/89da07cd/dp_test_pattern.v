//------------------------------------------------------------------------------ 
// Copyright (c) 2013 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: Vamsi Krishna, IPS Division, Xilinx, Inc.
//  \   \        
//  /   /        
// /___/   /\    Date Created: Oct 11, 2010
// \   \  /  \ 
//  \___\/\___\ 
// 
//------------------------------------------------------------------------------ 
/*
This video generator is used to create different test patterns as listed in 
DP compliance specification.


*/

`timescale 1 ps / 1 ps


module dp_test_pattern
#(
  parameter pTCQ = 100
)

(
  input wire clk,
  input wire rst,
  input wire [7:0]  misc0, // DP config register
  input wire [7:0]  misc1, // DP confing register
  input wire [11:0] vcount,
  input wire [11:0] hcount,
  input wire        hsync,
  input wire        vsync,
  input wire        vid_enable,
  output reg        vid_enable_adj,
  input wire        dual_pixel_mode,  
  input wire [2:0]  pattern,
  output reg  [47:0] pixel0,
  output reg  [47:0] pixel1
  );

// DP MISC0 Fields
parameter VESA_RANGE = 1'b0;
parameter CEA_RANGE  = 1'b1;

parameter YCBCR_ITU_R_BT601 = 1'b0;
parameter YCBCR_ITU_R_BT709 = 1'b1;

parameter BPC_6  = 3'b000;
parameter BPC_8  = 3'b001;
parameter BPC_10 = 3'b010;
parameter BPC_12 = 3'b011;
parameter BPC_16 = 3'b100;

parameter RGB_FORMAT      = 2'b00;
parameter YCBCR_FORMAT    = 2'b01;

// Color Square
parameter COLOR_WHITE  = 3'b000;
parameter COLOR_YELLOW = 3'b001;
parameter COLOR_CYAN   = 3'b010;
parameter COLOR_GREEN  = 3'b011;
parameter COLOR_MAGENTA= 3'b100;
parameter COLOR_RED    = 3'b101;
parameter COLOR_BLUE   = 3'b110;
parameter COLOR_BLACK  = 3'b111;

parameter STATE_RED    = 2'b00;
parameter STATE_GREEN  = 2'b01;
parameter STATE_BLUE   = 2'b10;
parameter STATE_WHITE  = 2'b11;
parameter TOGGLE_BLACK = 1'b0;
parameter TOGGLE_WHITE = 1'b1;

parameter COLOR_RAMP_PATTERN        = 3'b001;
parameter BW_VERTICAL_LINES_PATTERN = 3'b010;
parameter COLOR_SQUARE_PATTERN      = 3'b011;
parameter FLAT_RED_PATTERN          = 3'b100;
parameter FLAT_GREEN_PATTERN        = 3'b101;
parameter FLAT_BLUE_PATTERN         = 3'b110;
parameter FLAT_YELLOW_PATTERN       = 3'b111;

//Color Coefficient ROM
(* rom_extract = "yes" *)
reg [47:0] data_out;

reg [2:0]  color_square;

wire [1:0] comp_format       = (misc0[2:1]!=0)?YCBCR_FORMAT:RGB_FORMAT;
wire       dyn_range         = misc0[3];
wire       ycbcr_coloromitry = misc0[4];
wire [2:0] bpc               = misc0[7:5];

wire [9:0] color_def_addr = {color_square, bpc, ycbcr_coloromitry, dyn_range, comp_format};

reg [47:0] pixel0_reg, pixel1_reg;
reg [47:0] pixel0_reg_q, pixel1_reg_q;

reg [2:0] pattern_state;
wire [47:0] max_color_value = data_out;
reg [15:0] step_size=1;
reg lines64_q, lines32_q;
reg hlines64_q;
reg hlines64_qq;
reg hsync_q;
reg vsync_q;
reg [7:0] vid_enable_adj_taps;

// Video enable - adjusted to new pixel data
always@(posedge clk) begin
    vid_enable_adj_taps <= #pTCQ {vid_enable_adj_taps[6:0],vid_enable};     
end 

always@(posedge clk) begin
  if(rst) begin
    vid_enable_adj <= 0;
  end else begin
    case(pattern)  
      COLOR_RAMP_PATTERN:         vid_enable_adj <= vid_enable;   
      BW_VERTICAL_LINES_PATTERN:  vid_enable_adj <= vid_enable_adj_taps[3];   
      COLOR_SQUARE_PATTERN:       vid_enable_adj <= vid_enable_adj_taps[4];   
      default:                    vid_enable_adj <= vid_enable;   
    endcase    
  end
end  

// Controls to help detecting edges...
always@(posedge clk) begin
  if(rst) begin
    hsync_q <= #pTCQ 0;
    vsync_q <= #pTCQ 0;
  end else begin
    hsync_q <= #pTCQ hsync;
    vsync_q <= #pTCQ vsync;
  end  
end 

reg vid_started;
always@(posedge clk) begin
  if(rst || vsync) begin
    vid_started <= #pTCQ 0;
  end else if(vid_enable) begin
    vid_started <= #pTCQ 1;
  end	  
end

wire [1:0]   pixel_mode = (dual_pixel_mode)?2'b01:2'b00;

// Control signals to detect depth of 32 & 64 lines
wire [11:0] vcount_1 = vcount + 1;
wire [11:0] hcount_2 = hcount - 2;

wire lines64_ahead_1p  = ((vcount_1%64) ? 1'b0: 1'b1 ) & vid_started;
wire lines32_ahead_1p  = ((vcount_1%32) ? 1'b0: 1'b1 ) & vid_started;
wire lines64_1p        = ( (vcount%64)?1'b0:1'b1 )     & vid_enable;
wire lines32_1p        = ( (vcount%32)?1'b0:1'b1 )     & vid_enable;
wire hlines64_1p       = ( (hcount_2%64)?1'b0:1'b1 )   & vid_enable; 

//wire lines64_ahead_2p  = ((vcount_1%32) ? 1'b0: 1'b1 ) & vid_started;
//wire lines32_ahead_2p  = ((vcount_1%16) ? 1'b0: 1'b1 ) & vid_started;
//wire lines64_2p        = ( (vcount%32)?1'b0:1'b1 )     & vid_enable;
//wire lines32_2p        = ( (vcount%16)?1'b0:1'b1 )     & vid_enable;
wire hlines64_2p       = ( (hcount_2%32)?1'b0:1'b1 )   & vid_enable; 

wire lines64_ahead  = lines64_ahead_1p;  
wire lines32_ahead  = lines32_ahead_1p;  
wire lines64        = lines64_1p; 
wire lines32        = lines32_1p; 
wire hlines64       = (dual_pixel_mode)?hlines64_2p:hlines64_1p; 
                                                          

always@(posedge clk) begin
  if(rst) begin
    lines64_q  <= #pTCQ 0;
    lines32_q  <= #pTCQ 0;
    hlines64_q <= #pTCQ 0;
    hlines64_qq <= #pTCQ 0;
  end else begin
    lines64_q  <= #pTCQ lines64;
    lines32_q  <= #pTCQ lines32;
    hlines64_q <= #pTCQ hlines64;
    hlines64_qq<= #pTCQ hlines64_q;
  end
end

// Logic to generate MIN value for different BPC & Range
reg [3:0] cea_shift = 0;

always@(posedge clk) begin
  case(misc0[7:5])
    BPC_8:  cea_shift <= #pTCQ 2;
    BPC_10: cea_shift <= #pTCQ 4;   
    BPC_12: cea_shift <= #pTCQ 6;   
    BPC_16: cea_shift <= #pTCQ 10;   
  endcase
end

// Logic to handle reset/init values for all patterns & ranges
wire [15:0] rgb_reset_value = 16'h0;
wire [15:0] pixel_reset_value_r = (misc0[3]==VESA_RANGE)?rgb_reset_value: (16'h0004<<cea_shift);
wire [15:0] pixel_reset_value_g = (misc0[3]==VESA_RANGE)?rgb_reset_value: (16'h0004<<cea_shift);
wire [15:0] pixel_reset_value_b = (misc0[3]==VESA_RANGE)?rgb_reset_value: (16'h0004<<cea_shift);

// Test pattern - Color Ramp
// Ramps pattern is R -> G -> B -> W
reg [1:0]   ramp_index = STATE_RED;
wire [1:0]  ramp_index_next = ramp_index + 1; 
reg         bw_toggle  = TOGGLE_BLACK;
reg [15:0]  ramp_mid_value; 
reg [2:0]   color_square_cnt = 0;
reg         color_square_seq = 0;
reg         lines32_seq = 0;
reg         lines32_seq_q;


// Get ramp mid values - used in coarse & fine ramps...
always@(*) begin
  case(misc0[7:5])
    BPC_10:  ramp_mid_value = (lines32_seq)?16'h0000:16'h0180;      
    BPC_12:  ramp_mid_value = (lines32_seq)?16'h0000:16'h0780;      
    BPC_16:  ramp_mid_value = (lines32_seq)?16'h0000:16'h7F80;      
    default: ramp_mid_value = (lines32_seq)?16'h0000:16'h0000;
  endcase    
end  

// COLOR_SQUARE_PATTERN sequence counter
always@(posedge clk) begin
  if(rst || vsync || (hsync & ~hsync_q)) begin
    color_square_cnt <= #pTCQ 7;
  end else if(hlines64 & ~hlines64_q) begin
    color_square_cnt <= #pTCQ color_square_cnt + 1;
  end	  
end

always@(*) begin
  if(misc0[7:5]!=BPC_6 || misc0[7:5]!=BPC_8) begin
     // Adjuts step size based on ramp pattern defined in spec
     if(lines32_seq) begin
       case(misc0[7:5])
         BPC_10:  step_size =  4;		       
         BPC_12:  step_size =  16;	       
         BPC_16:  step_size =  256;		       
         default: step_size =  1;		 
       endcase		      
     end else begin  
       step_size = 1;		 
     end		      
  end else begin
    step_size = 1;
  end    
end

always@(posedge clk) begin
  if(rst || vsync) 
    lines32_seq <= #pTCQ 0;	    
  else if( (lines32_ahead & (hsync & ~hsync_q))  || (lines64_ahead & (hsync & ~hsync_q)))
    lines32_seq <= #pTCQ ~lines32_seq;	    

  lines32_seq_q <= #pTCQ lines32_seq;
end  


always@(posedge clk) begin
  if(rst || vsync) begin
    if(pattern==COLOR_SQUARE_PATTERN)    
      color_square     <= #pTCQ COLOR_WHITE;
    else if(pattern==BW_VERTICAL_LINES_PATTERN)    
      color_square     <= #pTCQ COLOR_BLACK;
    else    
      color_square     <= #pTCQ COLOR_RED;
    ramp_index       <= #pTCQ STATE_RED;//STATE_WHITE; 
    bw_toggle        <= #pTCQ TOGGLE_BLACK;
    color_square_seq <= 1'b0;
    pixel0_reg       <= #pTCQ {pixel_reset_value_r,pixel_reset_value_g,pixel_reset_value_b};      
    pixel1_reg       <= #pTCQ {pixel_reset_value_r,pixel_reset_value_g,pixel_reset_value_b};      

  end else begin
    if(pattern==COLOR_RAMP_PATTERN) begin

          // For every 64 vertical lines, ramp changes
          //if(lines64 & ~lines64_q) begin
          if(lines64_ahead & (hsync & ~hsync_q)) begin
            ramp_index <= #pTCQ ramp_index + 1;		  

            case(ramp_index_next)	
              STATE_RED: begin	  
                pixel0_reg        <= #pTCQ {ramp_mid_value, pixel_reset_value_g, pixel_reset_value_b};
                pixel1_reg        <= #pTCQ {ramp_mid_value+step_size, pixel_reset_value_g, pixel_reset_value_b};
              end	    
              STATE_GREEN: begin	  
                pixel0_reg        <= #pTCQ {pixel_reset_value_r, ramp_mid_value, pixel_reset_value_b};
                pixel1_reg        <= #pTCQ {pixel_reset_value_r, ramp_mid_value+step_size, pixel_reset_value_b};
              end	    
              STATE_BLUE: begin	  
                pixel0_reg        <= #pTCQ {pixel_reset_value_r, pixel_reset_value_g, ramp_mid_value};
                pixel1_reg        <= #pTCQ {pixel_reset_value_r, pixel_reset_value_g, ramp_mid_value+step_size};
              end	    
              STATE_WHITE: begin	  
                pixel0_reg        <= #pTCQ {ramp_mid_value, ramp_mid_value, ramp_mid_value};
                pixel1_reg[47:32]        <= #pTCQ ramp_mid_value+step_size;
                pixel1_reg[31:16]        <= #pTCQ ramp_mid_value+step_size;
                pixel1_reg[15:0]        <= #pTCQ ramp_mid_value+step_size;
              end	    
            endcase	

	  // Generate ramp based on color config programmed above  
          end else begin

	    // Gets color value from ROM
            case(ramp_index)
              STATE_RED:   color_square <= #pTCQ COLOR_RED;		    
              STATE_GREEN: color_square <= #pTCQ COLOR_GREEN;		    
              STATE_BLUE:  color_square <= #pTCQ COLOR_BLUE;		    
              STATE_WHITE: color_square <= #pTCQ COLOR_WHITE;		    
	    endcase

             //if(lines32 & ~lines32_q)  lines32_seq <= #pTCQ ~lines32_seq;	    

	     // RED Pixel Ramp 
              if( pixel0_reg[47:32] == max_color_value[47:32] ||(~hsync & hsync_q) ||(lines32_seq & ~lines32_seq_q) ||
                 ( ((pixel0_reg[47:32] == 16'h0280 && lines32_seq==0) || pixel0_reg[47:32] == 16'h0400 ||
	            (pixel1_reg[47:32] == 16'h0280 && lines32_seq==0) || pixel1_reg[47:32] == 16'h0400) && misc0[7:5]==BPC_10 && pixel_mode==0) ||
                 ( ((pixel0_reg[47:32] == 16'h027F) || pixel0_reg[47:32] == 16'h03FC ||
	            (pixel1_reg[47:32] == 16'h027F) || pixel1_reg[47:32] == 16'h03FC) && misc0[7:5]==BPC_10 && pixel_mode==1) ||
                 ( ((pixel0_reg[47:32] == 16'h0880 && lines32_seq==0) || pixel0_reg[47:32] == 16'h0FF4 ||
	            (pixel1_reg[47:32] == 16'h0880 && lines32_seq==0) || pixel1_reg[47:32] == 16'h0FF4) && misc0[7:5]==BPC_12 && pixel_mode==0) ||
                 ( (pixel0_reg[47:32] == 16'h087F || pixel0_reg[47:32] == 16'h0FF0 ||
	            pixel1_reg[47:32] == 16'h087F || pixel1_reg[47:32] == 16'h0FF0) && misc0[7:5]==BPC_12 && pixel_mode==1) ||
		 ( ((pixel0_reg[47:32] == 16'h8080 && lines32_seq==0) || pixel0_reg[47:32] == 16'hFF04 ||
	            (pixel1_reg[47:32] == 16'h8080 && lines32_seq==0) || pixel1_reg[47:32] == 16'hFF04) && misc0[7:5]==BPC_16 && pixel_mode==0)  ||
		 ( (pixel0_reg[47:32] == 16'h807F || pixel0_reg[47:32] == 16'hFF00 ||
	            pixel1_reg[47:32] == 16'h807F || pixel1_reg[47:32] == 16'hFF00) && misc0[7:5]==BPC_16  && pixel_mode==1)) begin
                  pixel0_reg[47:32]        <= #pTCQ (ramp_index==STATE_RED || ramp_index==STATE_WHITE)?ramp_mid_value:0;
                  pixel1_reg[47:32]        <= #pTCQ (ramp_index==STATE_RED || ramp_index==STATE_WHITE)?ramp_mid_value+step_size:0;
		  //if(lines32 & ~lines32_q)$display("[INFO] pixel0_reg[47:32] = %x @ %1d (ns)\n",pixel0_reg[47:32], $time());
              end else if(vid_enable && (ramp_index == STATE_WHITE || ramp_index == STATE_RED)) begin
                pixel0_reg[47:32] <= #pTCQ pixel0_reg[47:32] + ((step_size)<<(pixel_mode));
                pixel1_reg[47:32] <= #pTCQ pixel1_reg[47:32] + ((step_size)<<(pixel_mode));
       	      end else if(vid_enable && (ramp_index == STATE_GREEN || ramp_index == STATE_BLUE)) begin
                pixel0_reg[47:32] <= #pTCQ pixel_reset_value_r;
                pixel1_reg[47:32] <= #pTCQ pixel_reset_value_r;
              end		
        
	     // GREEN Pixel Ramp 
              if( pixel0_reg[31:16] == max_color_value[31:16] ||(~hsync & hsync_q) ||(lines32_seq & ~lines32_seq_q) ||
                 ( ((pixel0_reg[31:16] == 16'h0280 && lines32_seq==0) || pixel0_reg[31:16] == 16'h0400 ||
	            (pixel1_reg[31:16] == 16'h0280 && lines32_seq==0) || pixel1_reg[31:16] == 16'h0400) && misc0[7:5]==BPC_10 && pixel_mode==0) ||
                 ( ((pixel0_reg[31:16] == 16'h027F) || pixel0_reg[31:16] == 16'h03FC ||
	            (pixel1_reg[31:16] == 16'h027F) || pixel1_reg[31:16] == 16'h03FC) && misc0[7:5]==BPC_10 && pixel_mode==1) ||
                 ( ((pixel0_reg[31:16] == 16'h0880 && lines32_seq==0) || pixel0_reg[31:16] == 16'h0FF4 ||
	            (pixel1_reg[31:16] == 16'h0880 && lines32_seq==0) || pixel1_reg[31:16] == 16'h0FF4) && misc0[7:5]==BPC_12 && pixel_mode==0) ||
                 ( (pixel0_reg[31:16] == 16'h087F || pixel0_reg[31:16] == 16'h0FF0 ||
	            pixel1_reg[31:16] == 16'h087F || pixel1_reg[31:16] == 16'h0FF0) && misc0[7:5]==BPC_12 && pixel_mode==1 ) ||
		 ( ((pixel0_reg[31:16] == 16'h8080 && lines32_seq==0) || pixel0_reg[31:16] == 16'hFF04 ||
	            (pixel1_reg[31:16] == 16'h8080 && lines32_seq==0) || pixel1_reg[31:16] == 16'hFF04) && misc0[7:5]==BPC_16 && pixel_mode==0) ||
		 ( (pixel0_reg[31:16] == 16'h807F || pixel0_reg[31:16] == 16'hFF00 ||
	            pixel1_reg[31:16] == 16'h807F || pixel1_reg[31:16] == 16'hFF00) && misc0[7:5]==BPC_16&& pixel_mode==1 ) ) begin
                  pixel0_reg[31:16]        <= #pTCQ (ramp_index==STATE_GREEN || ramp_index==STATE_WHITE)?ramp_mid_value:0;
                  pixel1_reg[31:16]        <= #pTCQ (ramp_index==STATE_GREEN || ramp_index==STATE_WHITE)?ramp_mid_value+step_size:0;
              end else if(vid_enable && (ramp_index == STATE_WHITE || ramp_index == STATE_GREEN)) begin
                pixel0_reg[31:16] <= #pTCQ pixel0_reg[31:16] + (step_size<<(pixel_mode));
                pixel1_reg[31:16] <= #pTCQ pixel1_reg[31:16] + (step_size<<(pixel_mode));
	      end else if(vid_enable && (ramp_index == STATE_RED || ramp_index == STATE_BLUE)) begin 
                pixel0_reg[31:16] <= #pTCQ pixel_reset_value_g;
                pixel1_reg[31:16] <= #pTCQ pixel_reset_value_g;
              end		
              
	     // BLUE Pixel Ramp 
              if( pixel0_reg[15:0] == max_color_value[15:0] ||(~hsync & hsync_q) ||(lines32_seq & ~lines32_seq_q) ||
                 ( ((pixel0_reg[15:0] == 16'h0280 && lines32_seq==0) || pixel0_reg[15:0] == 16'h0400 ||
	            (pixel1_reg[15:0] == 16'h0280 && lines32_seq==0) || pixel1_reg[15:0] == 16'h0400) && misc0[7:5]==BPC_10 && pixel_mode==0) ||
                 ( ((pixel0_reg[15:0] == 16'h027F) || pixel0_reg[15:0] == 16'h03FC ||
	            (pixel1_reg[15:0] == 16'h027F) || pixel1_reg[15:0] == 16'h03FC) && misc0[7:5]==BPC_10 && pixel_mode==1) ||
                 ( ((pixel0_reg[15:0] == 16'h0880 && lines32_seq==0) || pixel0_reg[15:0] == 16'h0FF4 ||
	            (pixel1_reg[15:0] == 16'h0880 && lines32_seq==0) || pixel1_reg[15:0] == 16'h0FF4) && misc0[7:5]==BPC_12 && pixel_mode==0) ||
                 ( (pixel0_reg[15:0] == 16'h087F || pixel0_reg[15:0] == 16'h0FF0 ||
	            pixel1_reg[15:0] == 16'h087F || pixel1_reg[15:0] == 16'h0FF0) && misc0[7:5]==BPC_12 && pixel_mode==1) ||
		 ( ((pixel0_reg[15:0] == 16'h8080 && lines32_seq==0) || pixel0_reg[15:0] == 16'hFF04 ||
	            (pixel1_reg[15:0] == 16'h8080 && lines32_seq==0) || pixel1_reg[15:0] == 16'hFF04) && misc0[7:5]==BPC_16 && pixel_mode==0) ||
		 ( (pixel0_reg[15:0] == 16'h807F || pixel0_reg[15:0] == 16'hFF00 ||
	            pixel1_reg[15:0] == 16'h807F || pixel1_reg[15:0] == 16'hFF00) && misc0[7:5]==BPC_16 && pixel_mode==1) ) begin
                  pixel0_reg[15:0]        <= #pTCQ (ramp_index==STATE_BLUE || ramp_index==STATE_WHITE)?ramp_mid_value:0;
                  pixel1_reg[15:0]        <= #pTCQ (ramp_index==STATE_BLUE || ramp_index==STATE_WHITE)?ramp_mid_value+step_size:0;
              end else if(vid_enable && (ramp_index == STATE_WHITE || ramp_index == STATE_BLUE)) begin
                pixel0_reg[15:0] <= #pTCQ pixel0_reg[15:0] + (step_size<<(pixel_mode));
                pixel1_reg[15:0] <= #pTCQ pixel1_reg[15:0] + (step_size<<(pixel_mode));
	      end else if(vid_enable && (ramp_index == STATE_RED || ramp_index == STATE_GREEN))begin
                pixel0_reg[15:0] <= #pTCQ pixel_reset_value_b;
                pixel1_reg[15:0] <= #pTCQ pixel_reset_value_b;
              end	

          end //end lines64

    end else if(pattern == BW_VERTICAL_LINES_PATTERN) begin
      if(hsync & ~hsync_q) begin	    
        bw_toggle    <= #pTCQ TOGGLE_BLACK;  
        color_square <= #pTCQ COLOR_BLACK;
      end else if(vid_enable) begin      
        if(bw_toggle==TOGGLE_BLACK) begin
          color_square <= #pTCQ COLOR_BLACK;
        end else begin
          color_square <= #pTCQ COLOR_WHITE;
        end
        bw_toggle <= #pTCQ ~bw_toggle;  
      end     
        pixel0_reg[47:32] <= #pTCQ max_color_value[47:32];	      
        pixel0_reg[31:16] <= #pTCQ max_color_value[31:16];	      
        pixel0_reg[15:0]  <= #pTCQ max_color_value[15:0];	      
        pixel1_reg[47:32] <= #pTCQ pixel0_reg[47:32];     
        pixel1_reg[31:16] <= #pTCQ pixel0_reg[31:16];     
        pixel1_reg[15:0]  <= #pTCQ pixel0_reg[15:0];      

    end else if(pattern == COLOR_SQUARE_PATTERN) begin
	// Select appropriate color as per sequence defined in spec
        if(hlines64_q & ~hlines64_qq) begin
	  case(color_square_cnt) 
            0: color_square <= #pTCQ  (~color_square_seq)?COLOR_WHITE:COLOR_BLUE; 	  
            1: color_square <= #pTCQ  (~color_square_seq)?COLOR_YELLOW:COLOR_RED; 	  
            2: color_square <= #pTCQ  (~color_square_seq)?COLOR_CYAN:COLOR_MAGENTA; 	  
            3: color_square <= #pTCQ  COLOR_GREEN; 	  
            4: color_square <= #pTCQ  (~color_square_seq)?COLOR_MAGENTA:COLOR_CYAN; 	  
            5: color_square <= #pTCQ  (~color_square_seq)?COLOR_RED:COLOR_YELLOW; 	  
            6: color_square <= #pTCQ  (~color_square_seq)?COLOR_BLUE:COLOR_WHITE; 	  
            7: color_square <= #pTCQ  COLOR_BLACK; 	  
          endcase	  
	end//end hlines64_q	  
       
//	if(lines64 & ~lines64_q) 
        if(lines64_ahead & (hsync & ~hsync_q)) 
   	  color_square_seq <= #pTCQ ~color_square_seq;

//      if(vid_enable) begin
        pixel0_reg[47:32] <= #pTCQ max_color_value[47:32];	      
        pixel0_reg[31:16] <= #pTCQ max_color_value[31:16];	      
        pixel0_reg[15:0]  <= #pTCQ max_color_value[15:0];	      

        pixel1_reg[47:32] <= #pTCQ max_color_value[47:32];	      
        pixel1_reg[31:16] <= #pTCQ max_color_value[31:16];	      
        pixel1_reg[15:0]  <= #pTCQ max_color_value[15:0];	      

//      end
    end else if(pattern == FLAT_RED_PATTERN) begin
        color_square     <= #pTCQ COLOR_RED;
        pixel0_reg[47:32] <= #pTCQ max_color_value[47:32];	      
        pixel0_reg[31:16] <= #pTCQ max_color_value[31:16];	      
        pixel0_reg[15:0]  <= #pTCQ max_color_value[15:0];	      

        pixel1_reg[47:32] <= #pTCQ max_color_value[47:32];	      
        pixel1_reg[31:16] <= #pTCQ max_color_value[31:16];	      
        pixel1_reg[15:0]  <= #pTCQ max_color_value[15:0];	      
    end else if(pattern == FLAT_GREEN_PATTERN) begin
        color_square     <= #pTCQ COLOR_GREEN;
        pixel0_reg[47:32] <= #pTCQ max_color_value[47:32];	      
        pixel0_reg[31:16] <= #pTCQ max_color_value[31:16];	      
        pixel0_reg[15:0]  <= #pTCQ max_color_value[15:0];	      

        pixel1_reg[47:32] <= #pTCQ max_color_value[47:32];	      
        pixel1_reg[31:16] <= #pTCQ max_color_value[31:16];	      
        pixel1_reg[15:0]  <= #pTCQ max_color_value[15:0];	     
    end else if(pattern == FLAT_BLUE_PATTERN) begin
        color_square     <= #pTCQ COLOR_BLUE;
        pixel0_reg[47:32] <= #pTCQ max_color_value[47:32];	      
        pixel0_reg[31:16] <= #pTCQ max_color_value[31:16];	      
        pixel0_reg[15:0]  <= #pTCQ max_color_value[15:0];	      

        pixel1_reg[47:32] <= #pTCQ max_color_value[47:32];	      
        pixel1_reg[31:16] <= #pTCQ max_color_value[31:16];	      
        pixel1_reg[15:0]  <= #pTCQ max_color_value[15:0];	             	
    end else if(pattern == FLAT_YELLOW_PATTERN) begin
        color_square     <= #pTCQ COLOR_YELLOW;
        pixel0_reg[47:32] <= #pTCQ max_color_value[47:32];	      
        pixel0_reg[31:16] <= #pTCQ max_color_value[31:16];	      
        pixel0_reg[15:0]  <= #pTCQ max_color_value[15:0];	      

        pixel1_reg[47:32] <= #pTCQ max_color_value[47:32];	      
        pixel1_reg[31:16] <= #pTCQ max_color_value[31:16];	      
        pixel1_reg[15:0]  <= #pTCQ max_color_value[15:0];	      	
    end // end pattern
  end // end rst
end //end always

// YCBCR422 - Chroma bus is shared
reg cr_cb;
reg [47:0] pixel0_i, pixel0_i_q;
reg [47:0] pixel1_i, pixel1_i_q;
reg [47:0] pixel0_order, pixel1_order;

always@(*) begin
  pixel0_order = (misc0[2:1]==2'b01)? ( (cr_cb)?pixel0_i:{pixel0_i_q[15:0],pixel0_i[31:0]} )
                              : pixel0_i;  
  pixel1_order = (misc0[2:1]==2'b01)? ( (cr_cb)?pixel1_i:{pixel1_i_q[15:0],pixel1_i[31:0]} )
                              : pixel1_i;  
end  

always@(*) begin
  //if(misc0[2:1] == 2'b10) begin //YCBCR422
  //  pixel0 = {pixel0_order[15:0], pixel0_order[31:16], pixel0_order[47:32]};    
  //  pixel1 = {pixel1_order[15:0], pixel1_order[31:16], pixel1_order[47:32]};    
  //end else begin
    pixel0 = pixel0_order;    
    pixel1 = pixel1_order;    
  //end    
end  

// Signals to generate YCBCR422 timing
always@(posedge clk) begin
  pixel0_i_q <= #pTCQ pixel0_i;    
  pixel1_i_q <= #pTCQ pixel1_i;    
end  

always@(posedge clk) begin
  if(rst || vsync || hsync) begin
    cr_cb <= #pTCQ 0;      
  end else if(vid_enable_adj) begin
    cr_cb <= #pTCQ ~cr_cb;      
  end    
end  

always@(posedge clk) begin
  if(rst) begin
    pixel0_i <= 0;
    pixel1_i <= 0;    
  end else begin  
    case(misc0[7:5])
      BPC_6: begin 
        pixel0_i[47:32] <= #pTCQ {pixel0_reg[37:32],10'b0};    
        pixel0_i[31:16] <= #pTCQ {pixel0_reg[21:16],10'b0};    
        pixel0_i[15:0]  <= #pTCQ {pixel0_reg[5:0],10'b0};    

        pixel1_i[47:32] <= #pTCQ {pixel1_reg[37:32],10'b0};    
        pixel1_i[31:16] <= #pTCQ {pixel1_reg[21:16],10'b0};    
        pixel1_i[15:0]  <= #pTCQ {pixel1_reg[5:0],10'b0};    
      end      

      BPC_8: begin 
        pixel0_i[47:32] <= #pTCQ {pixel0_reg[39:32],8'b0};    
        pixel0_i[31:16] <= #pTCQ {pixel0_reg[23:16],8'b0};    
        pixel0_i[15:0]  <= #pTCQ {pixel0_reg[7:0],8'b0};    

        pixel1_i[47:32] <= #pTCQ {pixel1_reg[39:32],8'b0};    
        pixel1_i[31:16] <= #pTCQ {pixel1_reg[23:16],8'b0};    
        pixel1_i[15:0]  <= #pTCQ {pixel1_reg[7:0],8'b0};    
      end      

      BPC_10: begin 
        pixel0_i[47:32] <= #pTCQ {pixel0_reg[41:32],6'b0};    
        pixel0_i[31:16] <= #pTCQ {pixel0_reg[25:16],6'b0};    
        pixel0_i[15:0]  <= #pTCQ {pixel0_reg[9:0],6'b0};    

        pixel1_i[47:32] <= #pTCQ {pixel1_reg[41:32],6'b0};    
        pixel1_i[31:16] <= #pTCQ {pixel1_reg[25:16],6'b0};    
        pixel1_i[15:0]  <= #pTCQ {pixel1_reg[9:0],6'b0};    
      end	

      BPC_12: begin 
        pixel0_i[47:32] <= #pTCQ {pixel0_reg[43:32],4'b0};    
        pixel0_i[31:16] <= #pTCQ {pixel0_reg[27:16],4'b0};    
        pixel0_i[15:0]  <= #pTCQ {pixel0_reg[11:0],4'b0};    

        pixel1_i[47:32] <= #pTCQ {pixel1_reg[43:32],4'b0};    
        pixel1_i[31:16] <= #pTCQ {pixel1_reg[27:16],4'b0};    
        pixel1_i[15:0]  <= #pTCQ {pixel1_reg[11:0],4'b0};    
      end	

      BPC_16: begin 
        pixel0_i[47:32] <= #pTCQ pixel0_reg[47:32];    
        pixel0_i[31:16] <= #pTCQ pixel0_reg[31:16];    
        pixel0_i[15:0]  <= #pTCQ pixel0_reg[15:0];    

        pixel1_i[47:32] <= #pTCQ pixel1_reg[47:32];    
        pixel1_i[31:16] <= #pTCQ pixel1_reg[31:16];    
        pixel1_i[15:0]  <= #pTCQ pixel1_reg[15:0];    
      end	
    endcase  
  end  
end


// ROM
// Contains Color Definitions of RGB (VESA/CEA) Range, YCbCr (VESA/CEA) Range
// Used by pattern generation logic to determine various color values

always@(posedge clk) begin
  case(color_def_addr)
    // ************************************************************ RGB VESA ******************************************************************* //
          // RGB, VESA, White  
    {COLOR_WHITE, BPC_6, YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <= 63;    data_out[31:16] <= 63;    data_out[15:0] <= 63;    end
    {COLOR_WHITE, BPC_8, YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <= 255;   data_out[31:16] <= 255;   data_out[15:0] <= 255;   end
    {COLOR_WHITE, BPC_10,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <= 1023;  data_out[31:16] <= 1023;  data_out[15:0] <= 1023;  end
    {COLOR_WHITE, BPC_12,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <= 4095;  data_out[31:16] <= 4095;  data_out[15:0] <= 4095;  end
    {COLOR_WHITE, BPC_16,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <= 65535; data_out[31:16] <= 65535; data_out[15:0] <= 65535; end
          // RGB, VESA, Yellow
    {COLOR_YELLOW, BPC_6, YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:   begin data_out[47:32] <= 63;    data_out[31:16] <= 63;    data_out[15:0] <= 0;     end
    {COLOR_YELLOW, BPC_8, YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:   begin data_out[47:32] <= 255;   data_out[31:16] <= 255;   data_out[15:0] <= 0;     end
    {COLOR_YELLOW, BPC_10,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:   begin data_out[47:32] <= 1023;  data_out[31:16] <= 1023;  data_out[15:0] <= 0;     end
    {COLOR_YELLOW, BPC_12,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:   begin data_out[47:32] <= 4095;  data_out[31:16] <= 4095;  data_out[15:0] <= 0;     end
    {COLOR_YELLOW, BPC_16,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:   begin data_out[47:32] <= 65535; data_out[31:16] <= 65535; data_out[15:0] <= 0;     end
          // RGB, VESA, Cyan    
    {COLOR_CYAN, BPC_6, YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 0;     data_out[31:16] <= 63;    data_out[15:0] <= 63;    end
    {COLOR_CYAN, BPC_8, YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 0;     data_out[31:16] <= 255;   data_out[15:0] <= 255;   end
    {COLOR_CYAN, BPC_10,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 0;     data_out[31:16] <= 1023;  data_out[15:0] <= 1023;  end
    {COLOR_CYAN, BPC_12,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 0;     data_out[31:16] <= 4095;  data_out[15:0] <= 4095;  end
    {COLOR_CYAN, BPC_16,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 0;     data_out[31:16] <= 65535; data_out[15:0] <= 65535; end
          // RGB, VESA, Green  
    {COLOR_GREEN, BPC_6, YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <= 0;     data_out[31:16] <= 63;    data_out[15:0] <= 0;     end
    {COLOR_GREEN, BPC_8, YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <= 0;     data_out[31:16] <= 255;   data_out[15:0] <= 0;     end
    {COLOR_GREEN, BPC_10,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <= 0;     data_out[31:16] <= 1023;  data_out[15:0] <= 0;     end
    {COLOR_GREEN, BPC_12,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <= 0;     data_out[31:16] <= 4095;  data_out[15:0] <= 0;     end
    {COLOR_GREEN, BPC_16,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <= 0;     data_out[31:16] <= 65535; data_out[15:0] <= 0;     end
          // RGB, VESA, Magenta
    {COLOR_MAGENTA, BPC_6, YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:  begin data_out[47:32] <= 63;    data_out[31:16] <= 0;     data_out[15:0] <= 63;    end
    {COLOR_MAGENTA, BPC_8, YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:  begin data_out[47:32] <= 255;   data_out[31:16] <= 0;     data_out[15:0] <= 255;   end
    {COLOR_MAGENTA, BPC_10,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:  begin data_out[47:32] <= 1023;  data_out[31:16] <= 0;     data_out[15:0] <= 1023;  end
    {COLOR_MAGENTA, BPC_12,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:  begin data_out[47:32] <= 4095;  data_out[31:16] <= 0;     data_out[15:0] <= 4095;  end
    {COLOR_MAGENTA, BPC_16,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:  begin data_out[47:32] <= 65535; data_out[31:16] <= 0;     data_out[15:0] <= 65535; end
          // RGB, VESA, Red    
    {COLOR_RED, BPC_6, YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:      begin data_out[47:32] <= 63;    data_out[31:16] <= 0;     data_out[15:0] <= 0;     end
    {COLOR_RED, BPC_8, YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:      begin data_out[47:32] <= 255;   data_out[31:16] <= 0;     data_out[15:0] <= 0;     end
    {COLOR_RED, BPC_10,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:      begin data_out[47:32] <= 1023;  data_out[31:16] <= 0;     data_out[15:0] <= 0;     end
    {COLOR_RED, BPC_12,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:      begin data_out[47:32] <= 4095;  data_out[31:16] <= 0;     data_out[15:0] <= 0;     end
    {COLOR_RED, BPC_16,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:      begin data_out[47:32] <= 65535; data_out[31:16] <= 0;     data_out[15:0] <= 0;     end
          // RGB, VESA, Blue   
    {COLOR_BLUE, BPC_6, YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 0;     data_out[31:16] <= 0;     data_out[15:0] <= 63;    end
    {COLOR_BLUE, BPC_8, YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 0;     data_out[31:16] <= 0;     data_out[15:0] <= 255;   end
    {COLOR_BLUE, BPC_10,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 0;     data_out[31:16] <= 0;     data_out[15:0] <= 1023;  end
    {COLOR_BLUE, BPC_12,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 0;     data_out[31:16] <= 0;     data_out[15:0] <= 4095;  end
    {COLOR_BLUE, BPC_16,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 0;     data_out[31:16] <= 0;     data_out[15:0] <= 65535; end
          // RGB, VESA, Black  
    {COLOR_BLACK, BPC_6, YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <= 0;     data_out[31:16] <= 0;     data_out[15:0] <= 0;     end
    {COLOR_BLACK, BPC_8, YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <= 0;     data_out[31:16] <= 0;     data_out[15:0] <= 0;     end
    {COLOR_BLACK, BPC_10,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <= 0;     data_out[31:16] <= 0;     data_out[15:0] <= 0;     end
    {COLOR_BLACK, BPC_12,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <= 0;     data_out[31:16] <= 0;     data_out[15:0] <= 0;     end
    {COLOR_BLACK, BPC_16,YCBCR_ITU_R_BT601, VESA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <= 0;     data_out[31:16] <= 0;     data_out[15:0] <= 0;     end
          
    // ************************************************************ RGB CEA ******************************************************************* //
          // RGB, CEA, White  
    {COLOR_WHITE, BPC_8, YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 235;   data_out[31:16] <= 235;   data_out[15:0] <= 235;   end
    {COLOR_WHITE, BPC_10,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 940;   data_out[31:16] <=  940;  data_out[15:0] <=  940;  end
    {COLOR_WHITE, BPC_12,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 3760;  data_out[31:16] <= 3760;  data_out[15:0] <= 3760;  end
    {COLOR_WHITE, BPC_16,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 60160; data_out[31:16] <= 60160; data_out[15:0] <= 60160; end
          // RGB, CEA, Yellow
    {COLOR_YELLOW, BPC_8, YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <= 235;   data_out[31:16] <= 235;   data_out[15:0] <= 16;    end
    {COLOR_YELLOW, BPC_10,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <=  940;  data_out[31:16] <=  940;  data_out[15:0] <= 64;    end
    {COLOR_YELLOW, BPC_12,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <= 3760;  data_out[31:16] <= 3760;  data_out[15:0] <= 256;   end
    {COLOR_YELLOW, BPC_16,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:    begin data_out[47:32] <= 60160; data_out[31:16] <= 60160; data_out[15:0] <= 4096;  end
          // RGB, CEA, Cyan                                             
    {COLOR_CYAN, BPC_8, YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:      begin data_out[47:32] <= 16;    data_out[31:16] <= 235;   data_out[15:0] <= 255;   end
    {COLOR_CYAN, BPC_10,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:      begin data_out[47:32] <= 64;    data_out[31:16] <= 940;   data_out[15:0] <= 1023;  end
    {COLOR_CYAN, BPC_12,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:      begin data_out[47:32] <= 256;   data_out[31:16] <= 3760;  data_out[15:0] <= 4095;  end
    {COLOR_CYAN, BPC_16,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:      begin data_out[47:32] <= 4096;  data_out[31:16] <= 60160; data_out[15:0] <= 65535; end
          // RGB, CEA, Green                                            
    {COLOR_GREEN, BPC_8, YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 16;    data_out[31:16] <= 235;   data_out[15:0] <= 16;    end
    {COLOR_GREEN, BPC_10,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 64;    data_out[31:16] <= 940;   data_out[15:0] <= 64;    end
    {COLOR_GREEN, BPC_12,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 256;   data_out[31:16] <= 3760;  data_out[15:0] <= 256;   end
    {COLOR_GREEN, BPC_16,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 4096;  data_out[31:16] <= 60160; data_out[15:0] <= 4096;  end
          // RGB, CEA, Magenta                                          
    {COLOR_MAGENTA, BPC_8, YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:   begin data_out[47:32] <= 235;   data_out[31:16] <= 16;    data_out[15:0] <= 235;   end
    {COLOR_MAGENTA, BPC_10,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:   begin data_out[47:32] <= 940;   data_out[31:16] <= 64;    data_out[15:0] <= 940;   end
    {COLOR_MAGENTA, BPC_12,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:   begin data_out[47:32] <= 3760;  data_out[31:16] <= 256;   data_out[15:0] <= 3760;  end
    {COLOR_MAGENTA, BPC_16,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:   begin data_out[47:32] <= 60160; data_out[31:16] <= 4096;  data_out[15:0] <= 60160; end
          // RGB, CEA, Red                                              
    {COLOR_RED, BPC_8, YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:       begin data_out[47:32] <= 235;   data_out[31:16] <= 16;    data_out[15:0] <= 16;    end
    {COLOR_RED, BPC_10,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:       begin data_out[47:32] <= 940;   data_out[31:16] <= 64;    data_out[15:0] <= 64;    end
    {COLOR_RED, BPC_12,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:       begin data_out[47:32] <= 3760;  data_out[31:16] <= 256;   data_out[15:0] <= 256;   end
    {COLOR_RED, BPC_16,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:       begin data_out[47:32] <= 60160; data_out[31:16] <= 4096;  data_out[15:0] <= 4096;  end
          // RGB, CEA, Blue                                             
    {COLOR_BLUE, BPC_8, YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:      begin data_out[47:32] <= 16;    data_out[31:16] <= 16;    data_out[15:0] <= 235;   end
    {COLOR_BLUE, BPC_10,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:      begin data_out[47:32] <= 64;    data_out[31:16] <= 64;    data_out[15:0] <= 940;   end
    {COLOR_BLUE, BPC_12,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:      begin data_out[47:32] <= 256;   data_out[31:16] <= 256;   data_out[15:0] <= 3760;  end
    {COLOR_BLUE, BPC_16,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:      begin data_out[47:32] <= 4096;  data_out[31:16] <= 4096;  data_out[15:0] <= 60160; end
          // RGB, CEA, Black                                            
    {COLOR_BLACK, BPC_8, YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 16;    data_out[31:16] <= 16;    data_out[15:0] <= 16;    end
    {COLOR_BLACK, BPC_10,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 64;    data_out[31:16] <= 64;    data_out[15:0] <= 64;    end
    {COLOR_BLACK, BPC_12,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 256;   data_out[31:16] <= 256;   data_out[15:0] <= 256;   end
    {COLOR_BLACK, BPC_16,YCBCR_ITU_R_BT601, CEA_RANGE, RGB_FORMAT}:     begin data_out[47:32] <= 4096;  data_out[31:16] <= 4096;  data_out[15:0] <= 4096;  end

    // ************************************************************ YCbCr 601 ******************************************************************* //
          // YCbCr, White  
    {COLOR_WHITE, BPC_8, YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <= 128;   data_out[31:16] <= 235;   data_out[15:0] <= 128;   end
    {COLOR_WHITE, BPC_10,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <= 940;   data_out[31:16] <= 940;   data_out[15:0] <= 940;   end
    {COLOR_WHITE, BPC_12,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <= 2048;  data_out[31:16] <= 3760;  data_out[15:0] <= 2048;  end
    {COLOR_WHITE, BPC_16,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <= 32768; data_out[31:16] <= 60160; data_out[15:0] <= 32768; end    
          // YCbCr, Yellow
    {COLOR_YELLOW, BPC_8, YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}: begin data_out[47:32] <= 146;   data_out[31:16] <= 210;   data_out[15:0] <=  16;   end
    {COLOR_YELLOW, BPC_10,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}: begin data_out[47:32] <=  64;   data_out[31:16] <= 940;   data_out[15:0] <= 940;   end
    {COLOR_YELLOW, BPC_12,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}: begin data_out[47:32] <= 2339;  data_out[31:16] <= 3361;  data_out[15:0] <=  257;  end
    {COLOR_YELLOW, BPC_16,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}: begin data_out[47:32] <= 37421; data_out[31:16] <= 53769; data_out[15:0] <=  4119; end    
          // YCbCr, Cyan
    {COLOR_CYAN, BPC_8, YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:   begin data_out[47:32] <=  16;   data_out[31:16] <= 170;   data_out[15:0] <= 166;   end
    {COLOR_CYAN, BPC_10,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:   begin data_out[47:32] <= 940;   data_out[31:16] <=  64;   data_out[15:0] <= 940;   end
    {COLOR_CYAN, BPC_12,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:   begin data_out[47:32] <=  257;  data_out[31:16] <= 2712;  data_out[15:0] <= 2651;  end
    {COLOR_CYAN, BPC_16,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:   begin data_out[47:32] <=  4119; data_out[31:16] <= 43397; data_out[15:0] <= 42411; end    
          // YCbCr, Green
    {COLOR_GREEN, BPC_8, YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <=  34;   data_out[31:16] <= 145;   data_out[15:0] <=  54;   end
    {COLOR_GREEN, BPC_10,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <=  64;   data_out[31:16] <=  64;   data_out[15:0] <= 940;   end
    {COLOR_GREEN, BPC_12,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <=  548;  data_out[31:16] <= 2313;  data_out[15:0] <=  860;  end
    {COLOR_GREEN, BPC_16,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <=  8773; data_out[31:16] <= 37006; data_out[15:0] <= 13762; end    
          // YCbCr, Magenta
    {COLOR_MAGENTA, BPC_8, YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:begin data_out[47:32] <= 222;   data_out[31:16] <= 106;   data_out[15:0] <= 202;   end
    {COLOR_MAGENTA, BPC_10,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:begin data_out[47:32] <= 940;   data_out[31:16] <= 940;   data_out[15:0] <=  64;   end
    {COLOR_MAGENTA, BPC_12,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:begin data_out[47:32] <= 3548;  data_out[31:16] <= 1703;  data_out[15:0] <= 3236;  end
    {COLOR_MAGENTA, BPC_16,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:begin data_out[47:32] <= 56763; data_out[31:16] <= 27250; data_out[15:0] <= 51774; end    
          // YCbCr, Red
    {COLOR_RED, BPC_8, YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:    begin data_out[47:32] <= 240;   data_out[31:16] <=  81;   data_out[15:0] <=  90;   end
    {COLOR_RED, BPC_10,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:    begin data_out[47:32] <=  64;   data_out[31:16] <= 940;   data_out[15:0] <=  64;   end
    {COLOR_RED, BPC_12,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:    begin data_out[47:32] <= 3839;  data_out[31:16] <= 1304;  data_out[15:0] <= 1445;  end
    {COLOR_RED, BPC_16,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:    begin data_out[47:32] <= 61417; data_out[31:16] <= 20859; data_out[15:0] <= 23125; end    
          // YCbCr, Blue
    {COLOR_BLUE, BPC_8, YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:   begin data_out[47:32] <= 110;   data_out[31:16] <=  41;   data_out[15:0] <= 240;   end
    {COLOR_BLUE, BPC_10,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:   begin data_out[47:32] <= 940;   data_out[31:16] <=  64;   data_out[15:0] <=  64;   end
    {COLOR_BLUE, BPC_12,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:   begin data_out[47:32] <= 1757;  data_out[31:16] <=  655;  data_out[15:0] <= 3839;  end
    {COLOR_BLUE, BPC_16,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:   begin data_out[47:32] <= 28115; data_out[31:16] <= 10487; data_out[15:0] <= 61417; end    
          // YCbCr, Black
    {COLOR_BLACK, BPC_8, YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <= 128;   data_out[31:16] <=  16;   data_out[15:0] <= 128;   end
    {COLOR_BLACK, BPC_10,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <=  64;   data_out[31:16] <=  64;   data_out[15:0] <=  64;   end
    {COLOR_BLACK, BPC_12,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <= 2048;  data_out[31:16] <=  256;  data_out[15:0] <= 2048;  end
    {COLOR_BLACK, BPC_16,YCBCR_ITU_R_BT601, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <= 32768; data_out[31:16] <=  4096; data_out[15:0] <= 32768; end    

    // ************************************************************ YCbCr 709 ******************************************************************* //
          // YCbCr, White  
    {COLOR_WHITE, BPC_8, YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <= 128;   data_out[31:16] <= 235;   data_out[15:0] <= 128;   end
    {COLOR_WHITE, BPC_10,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <= 512;   data_out[31:16] <= 940;   data_out[15:0] <= 512;   end
    {COLOR_WHITE, BPC_12,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <= 2048;  data_out[31:16] <= 3760;  data_out[15:0] <= 2048;  end
    {COLOR_WHITE, BPC_16,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <= 32768; data_out[31:16] <= 60160; data_out[15:0] <= 32768; end    
          // YCbCr, Yellow
    {COLOR_YELLOW, BPC_8, YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}: begin data_out[47:32] <= 138;   data_out[31:16] <= 219;   data_out[15:0] <=  16;   end
    {COLOR_YELLOW, BPC_10,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}: begin data_out[47:32] <= 585;   data_out[31:16] <= 840;   data_out[15:0] <=  64;   end
    {COLOR_YELLOW, BPC_12,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}: begin data_out[47:32] <= 2213;  data_out[31:16] <= 3508;  data_out[15:0] <=  257;  end
    {COLOR_YELLOW, BPC_16,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}: begin data_out[47:32] <= 35403; data_out[31:16] <= 56123; data_out[15:0] <=  4119; end    
          // YCbCr, Cyan
    {COLOR_CYAN, BPC_8, YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:   begin data_out[47:32] <=  16;   data_out[31:16] <= 188;   data_out[15:0] <= 154;   end
    {COLOR_CYAN, BPC_10,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:   begin data_out[47:32] <=  64;   data_out[31:16] <= 678;   data_out[15:0] <= 663;   end
    {COLOR_CYAN, BPC_12,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:   begin data_out[47:32] <=  257;  data_out[31:16] <= 3014;  data_out[15:0] <= 2458;  end
    {COLOR_CYAN, BPC_16,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:   begin data_out[47:32] <=  4119; data_out[31:16] <= 48218; data_out[15:0] <= 39327; end    
          // YCbCr, Green
    {COLOR_GREEN, BPC_8, YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <=  26;   data_out[31:16] <= 173;   data_out[15:0] <=  42;   end
    {COLOR_GREEN, BPC_10,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <= 137;   data_out[31:16] <= 578;   data_out[15:0] <= 215;   end
    {COLOR_GREEN, BPC_12,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <=  422;  data_out[31:16] <= 2761;  data_out[15:0] <=  667;  end
    {COLOR_GREEN, BPC_16,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <=  6754; data_out[31:16] <= 44182; data_out[15:0] <= 10679; end    
          // YCbCr, Magenta
    {COLOR_MAGENTA, BPC_8, YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:begin data_out[47:32] <= 230;   data_out[31:16] <=  78;   data_out[15:0] <= 214;   end
    {COLOR_MAGENTA, BPC_10,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:begin data_out[47:32] <= 887;   data_out[31:16] <= 426;   data_out[15:0] <= 809;   end
    {COLOR_MAGENTA, BPC_12,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:begin data_out[47:32] <= 3674;  data_out[31:16] <= 1255;  data_out[15:0] <= 3429;  end
    {COLOR_MAGENTA, BPC_16,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:begin data_out[47:32] <= 58782; data_out[31:16] <= 20074; data_out[15:0] <= 54857; end    
          // YCbCr, Red
    {COLOR_RED, BPC_8, YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:    begin data_out[47:32] <= 240;   data_out[31:16] <=  63;   data_out[15:0] <= 102;   end
    {COLOR_RED, BPC_10,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:    begin data_out[47:32] <= 960;   data_out[31:16] <= 326;   data_out[15:0] <= 361;   end
    {COLOR_RED, BPC_12,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:    begin data_out[47:32] <= 3839;  data_out[31:16] <= 1002;  data_out[15:0] <= 1638;  end
    {COLOR_RED, BPC_16,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:    begin data_out[47:32] <= 61417; data_out[31:16] <= 16038; data_out[15:0] <= 26209; end    
          // YCbCr, Blue
    {COLOR_BLUE, BPC_8, YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:   begin data_out[47:32] <= 118;   data_out[31:16] <=  32;   data_out[15:0] <= 240;   end
    {COLOR_BLUE, BPC_10,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:   begin data_out[47:32] <= 439;   data_out[31:16] <= 164;   data_out[15:0] <= 960;   end
    {COLOR_BLUE, BPC_12,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:   begin data_out[47:32] <= 1883;  data_out[31:16] <=  508;  data_out[15:0] <= 3839;  end
    {COLOR_BLUE, BPC_16,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:   begin data_out[47:32] <= 30133; data_out[31:16] <=  8133; data_out[15:0] <= 61417; end    
          // YCbCr, Black
    {COLOR_BLACK, BPC_8, YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <= 128;   data_out[31:16] <=  16;   data_out[15:0] <= 128;   end
    {COLOR_BLACK, BPC_10,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <= 512;   data_out[31:16] <=  64;   data_out[15:0] <= 512;   end
    {COLOR_BLACK, BPC_12,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <= 2048;  data_out[31:16] <=  256;  data_out[15:0] <= 2048;  end
    {COLOR_BLACK, BPC_16,YCBCR_ITU_R_BT709, VESA_RANGE, YCBCR_FORMAT}:  begin data_out[47:32] <= 32768; data_out[31:16] <=  4096; data_out[15:0] <= 32768; end    
    //default
    default:  begin data_out[47:32] <= 0; data_out[31:16] <=  0; data_out[15:0] <= 0; end    


  endcase
end


endmodule







