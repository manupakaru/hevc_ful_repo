
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:23:12 08/07/2014 
// Design Name: 
// Module Name:    displayport_video_parameters 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////



//----------------------------------------------------------------	2160p30		2160p24		1080p30		1080p24		1080p60		1080p60(RB)	
//														clock(MHz)	338.75		266.75		79.75		63  		173         173
`define 	HSW              		24'd44				//			408 		400 		192			192 		200         200
`define 	HBP              		24'd148				//			648 		616 		248			240 		328         328
`define 	HFP              		24'd88				//			240 		216 		56			48  		128         128
`define 	HRES             		24'd1920			//			3840		3840		1920		1920		1920        1920
`define 	VSW              		24'd5				//			5   		5   		5 			5   		5           5
`define 	VBP              		24'd36				//			32  		24  		14			10  		32          32
`define 	VFP              		24'd4				//			3   		3   		3			3   		3           3
`define 	VRES             		24'd1080			//			2160		2160		1080		1080		1080        1080
//----------------------------------------------------------------------------------------------------------------


`define 	HTOTAL           		`HFP + `HBP + `HSW + `HRES	
`define 	VTOTAL           		`VFP + `VBP + `VSW + `VRES	


// Parameters for Displayport video tx top controller

localparam		VIDEO_CLK_FREQ		= 	150 ;
localparam		DP_FIFO_WIDTH		= 	64 ;

localparam		FRAME_PCLK_COUNT			= 	((`HTOTAL) * (`VTOTAL)) / 2				;
localparam		HSYNC_PCLK_COUNT			= 	(`HTOTAL) / 2                       	;
localparam		DE_START_PCLK_COUNT			= 	((`VSW +`VBP) * (`HTOTAL)) / 2        	;
localparam		DE_STOP_PCLK_COUNT			= 	((`VSW +`VBP +`VRES) * (`HTOTAL))/2 	;
localparam		HSYNC_HIGH_PCLK_COUNT		= 	(`HSW) / 2                            	;
localparam		VSYNC_HIGH_PCLK_COUNT		= 	((`VSW) * (`HTOTAL)) / 2              	;
localparam		DE_HIGH_PCLK_COUNT			= 	(`HSW + `HBP ) / 2                    	;
localparam		DE_LOW_PCLK_COUNT			= 	(`HSW + `HBP + (`HRES) ) / 2          	;













