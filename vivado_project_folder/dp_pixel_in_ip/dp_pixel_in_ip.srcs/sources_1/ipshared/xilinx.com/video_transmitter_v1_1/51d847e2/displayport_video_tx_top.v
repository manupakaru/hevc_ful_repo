`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/15/2014 11:57:47 AM
// Design Name: 
// Module Name: video_transmitter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module video_transmitter(
        start,
		vid_clk		,
		vid_run_clk ,
		reset       ,
		config_done ,
		vid_clk_en  ,
		vid_hsync   ,
		vid_vsync   ,
		vid_enable  ,
		vid_oddeven ,
		vid_pixel0  ,
		vid_pixel1  ,
		vid_pixel2  ,
		vid_pixel3  
		,pclk_count 
        ,hsync_count
        ,DE_count   
		
		
    );
	
	`include "displayport_video_parameters.v"
	
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input start;
    input                               vid_clk;
    input                               vid_run_clk;
    input                               reset;
    input                               config_done;
	
	output	reg							vid_clk_en;
	output	reg							vid_hsync;
	output	reg							vid_vsync;
	output	reg							vid_enable;
	output								vid_oddeven;
	
	output		[47:0]					vid_pixel0 ;
	output		[47:0]					vid_pixel1 ;
	output		[47:0]					vid_pixel2 ;
	output		[47:0]					vid_pixel3 ;
	
	
	
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    
	localparam					STATE_INIT						=	0 ;
	localparam                  STATE_VIDEO_TX                  = 	1 ;
	localparam                  STATE_VIDEO_TX_1                = 	2 ;
	localparam                  STATE_VIDEO_TX_2                = 	3 ;
	localparam                  STATE_VIDEO_TX_3                = 	4 ;
	localparam                  STATE_VIDEO_TX_4                = 	5 ;
	
	
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    
	(* MARK_DEBUG *) output reg			[23:0]					pclk_count  ;
	(* MARK_DEBUG *) output reg			[12:0]					hsync_count ;
	(* MARK_DEBUG *) output reg			[12:0]					DE_count    ;
	reg			[DP_FIFO_WIDTH-1:0]		video_fifo_data_in 	;
	
	reg 	[23:0]				D_out;
	reg		[19:0]				disp_count;
	
	(* MARK_DEBUG *) integer						state;
	
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
    
	assign	vid_oddeven  =  1'b0 ;
	
	assign	vid_pixel0  =  {D_out[23:16], 8'd0, D_out[15:8] , 8'd0, D_out[7:0]  , 8'd0} ;
	assign	vid_pixel1  =  {D_out[23:16], 8'd0, D_out[15:8] , 8'd0, D_out[7:0]  , 8'd0} ;
	assign	vid_pixel2  =  48'd0 ;
	assign	vid_pixel3  =  48'd0 ;
	
	
	//---------------------- Main Stream Video Transmission SM --------------------------------
	
	always @(posedge vid_run_clk or posedge reset) begin
		if(reset) begin
			vid_clk_en <= 1'b0 ;
			disp_count <= 20'd0;
			state	<= STATE_INIT;
		end
		else begin
			case(state)
				
				STATE_INIT : begin	
				    if(start) begin	                 
					   state	<= STATE_VIDEO_TX;    
                    end
				end
				
				//-----------Transmit video data--------------------------------------------------// //
				
				STATE_VIDEO_TX : begin
					vid_clk_en <= 1'b1;				// wait till data is available
					D_out <= {8'd200,8'd0,8'd0};
					state <= STATE_VIDEO_TX_1;
				end
				
				STATE_VIDEO_TX_1 : begin
					if(vid_enable) begin
						if(disp_count == 20'd76798 ) begin		// 230398 for 120 rows
							disp_count <= 20'd0;
							D_out <= {8'd0,8'd200,8'd0};
							state <= STATE_VIDEO_TX_2;
						end
						else begin
							disp_count <= disp_count + 2'd2;
							D_out <= {8'd200,8'd0,8'd0};
							state <= STATE_VIDEO_TX_1;		// coincide with rising edge of pclk
						end	
					end
				end
				
				STATE_VIDEO_TX_2 : begin
					if(vid_enable) begin
						if(disp_count == 20'd76798 ) begin		// 230398 for 120 rows
							disp_count <= 20'd0;
							D_out <= {8'd0,8'd0,8'd200};
							state <= STATE_VIDEO_TX_3;
						end
						else begin
							disp_count <= disp_count + 2'd2;
							D_out <= {8'd0,8'd200,8'd0};
							state <= STATE_VIDEO_TX_2;		// coincide with rising edge of pclk
						end	
					end
				end
				
				STATE_VIDEO_TX_3 : begin
					if(vid_enable) begin
						if(disp_count == 20'd76798 ) begin		// 230398 for 120 rows
							disp_count <= 20'd0;
							D_out <= {8'd200,8'd0,8'd0};
							state <= STATE_VIDEO_TX_1;
						end
						else begin
							disp_count <= disp_count + 2'd2;
							D_out <= {8'd0,8'd0,8'd200};
							state <= STATE_VIDEO_TX_3;		// coincide with rising edge of pclk
						end	
					end
				end
				
			endcase
		end
	end
	
	
	
	
	//-------------------- Main stream video framing control signal generation ----------------------------
	always @(posedge vid_clk or posedge reset) begin
		if(reset) begin
			pclk_count  <= 24'd0 ;
			hsync_count <= 13'd0 ;
			DE_count    <= 13'd0 ;
		end
		else begin
			// pixel clock counter
			if(pclk_count == FRAME_PCLK_COUNT) begin
				pclk_count <= 24'd1;
			end
			else begin
				pclk_count <= pclk_count + 1'b1 ;
			end
			// Hsync counter
			if(hsync_count == HSYNC_PCLK_COUNT) begin
				hsync_count <= 13'd1;
			end
			else begin
				hsync_count <= hsync_count + 1'b1 ;
			end
			// Data Enable counter
			if(pclk_count >= DE_START_PCLK_COUNT && pclk_count < DE_STOP_PCLK_COUNT) begin
				if(DE_count == HSYNC_PCLK_COUNT) begin
					DE_count <= 13'd1;
				end
				else begin
					DE_count <= DE_count + 1'b1;
				end
			end
			else begin
				DE_count <= 13'd0;
			end
		end
	end
	
	
	//----------------- Combinational block to generate Sync and Enable signals ---------------------------
	always @(*) begin
		// Generate Hsync signal
		if(hsync_count < HSYNC_HIGH_PCLK_COUNT || hsync_count == HSYNC_PCLK_COUNT) begin
			vid_hsync  =  1'b1 ;
		end
		else begin
			vid_hsync  =  1'b0 ;
		end
		// Generate Vsync signal
		if(pclk_count < VSYNC_HIGH_PCLK_COUNT || pclk_count == FRAME_PCLK_COUNT) begin
			vid_vsync  =  1'b1 ;
		end
		else begin
			vid_vsync  =  1'b0 ;
		end
		// Generate Data Enable signal
		if(DE_count >= DE_HIGH_PCLK_COUNT &&  DE_count < DE_LOW_PCLK_COUNT) begin
			vid_enable  =  1'b1 ;
		end
		else begin
			vid_enable  =  1'b0 ;
		end
	end
	
	
endmodule
