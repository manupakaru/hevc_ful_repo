
`timescale 1ns / 1ps

module eth_hevc_top_to_pred
    (
      	clk,
      	reset,
		eth_state_out,
		
		config_fifo_out,
		config_input_fifo_is_full,	
		config_write_en_out,
		
		y_residual_fifo_out,
		y_residual_fifo_is_full_in,		
		y_residual_fifo_write_en_out,		
				
		cb_residual_fifo_out,		
		cb_residual_fifo_is_full_in,		
		cb_residual_fifo_write_en_out,		
				
		cr_residual_fifo_out,		
		cr_residual_fifo_is_full_in,		
		cr_residual_fifo_write_en_out,		
		
		

		rx_axis_fifo_tvalid,
		rx_axis_fifo_tdata,
		rx_axis_fifo_tlast,
		rx_axis_fifo_tready,

		tx_axis_fifo_tvalid,
		tx_axis_fifo_tdata,
		tx_axis_fifo_tlast,
		tx_axis_fifo_tready
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
`include "../sim/pred_def.v"

	parameter FIFO_IN_WIDTH = 32;
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
	localparam						HEVC_STATE_INIT = 0;
	localparam						HEVC_STATE_ETH_HEVC_HEADER1 = 1;
	localparam						HEVC_STATE_ETH_HEVC_HEADER2 = 2;
	localparam						HEVC_STATE_WRONG_PACKET = 3;
	localparam						HEVC_STATE_MSB = 4;
	localparam						HEVC_STATE_LSB = 5;
	localparam						HEVC_STATE_DONE = 6;
	localparam						HEVC_STATE_SRC_MAC_TX = 7;
	localparam						HEVC_STATE_DST_MAC_TX = 8;
	localparam						HEVC_STATE_DATA_REQUEST_TX = 9;
    localparam                      HEVC_STATE_TX_INIT = 10;

    localparam                      HEVC_STATE_BYTE_3 = 11;
    localparam                      HEVC_STATE_BYTE_2 = 12;
    localparam                      HEVC_STATE_MISMATCH_SERVED = 13;
	
	
    localparam                      HEVC_STATE_RX_INIT = 14;
    localparam                      HEVC_STATE_WAIT_WRONG_PACKET_TX = 15;
    localparam                      HEVC_STATE_CONFIG_RECEIVE = 16;
    localparam                      HEVC_STATE_Y_RES_RECEIVE = 17;
    localparam                      HEVC_STATE_CB_RES_RECEIVE = 18;
    localparam                      HEVC_STATE_CR_RES_RECEIVE = 19;
	localparam                      HEVC_STATE_CONFIG_FUL_WAIT = 20;
    localparam                      HEVC_STATE_Y_RES_FUL_WAIT = 21;
    localparam                      HEVC_STATE_CB_RES_FUL_WAIT = 22;
    localparam                      HEVC_STATE_CR_RES_FUL_WAIT = 23;
    
    localparam                      TXSTATE_SRC_MAC_TX = 1;
    localparam                      TXSTATE_DST_MAC_TX = 2;
    localparam                      TXSTATE_DATA_REQUEST_TX = 3;
    localparam                      TXSTATE_TX_INIT = 0;
    localparam                      TXSTATE_DATA_WAIT1 = 4;
    localparam                      TXSTATE_DATA_WAIT2 = 5;

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input clk;
    input reset;
	output [7:0] eth_state_out;

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
	integer hevc_state;
    // integer txstate; 
    // reg     tx_first_time;

	assign eth_state_out = hevc_state;
	
    integer rx_fsm_counter;
    // reg     rx_done;
    // reg     tx_done;
    // reg     tx_stop_packet_sent;
    
    // integer recieved_counter;
    reg     packet_order_mismatch;
    
	
	
	// needs to be connected to ethernet core and ultimately CABAC
    output reg          [FIFO_IN_WIDTH-1:0]  	config_fifo_out;
    input                                  		config_input_fifo_is_full;
    output reg                             		config_write_en_out;
    //luma residual fifo interface
    output       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0] 	y_residual_fifo_out;
    input                                                                      		y_residual_fifo_is_full_in;
    output reg                                                                 		y_residual_fifo_write_en_out;
    //cb residual fifo interface
    output       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]	cb_residual_fifo_out;
    input                                                                      	cb_residual_fifo_is_full_in;
    output reg                                                             		cb_residual_fifo_write_en_out;
    //cr residual fifo interface                                
    output       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]	cr_residual_fifo_out;
    input                                                                      	cr_residual_fifo_is_full_in;
    output reg                                                              	cr_residual_fifo_write_en_out;
	
	
	input 							rx_axis_fifo_tvalid;
	input 	[8 - 1 :0]				rx_axis_fifo_tdata;
	input 							rx_axis_fifo_tlast;
	output          				rx_axis_fifo_tready;

	output reg						tx_axis_fifo_tvalid;
	output reg	[8 - 1 :0]			tx_axis_fifo_tdata;
	output reg						tx_axis_fifo_tlast;
	input 							tx_axis_fifo_tready;

	reg [7:0] yy_residual_fifo_read[23:0];
	reg [7:0] cb_residual_fifo_read[23:0];
	reg [7:0] cr_residual_fifo_read[23:0];
	
     	assign cb_residual_fifo_out = {
				cb_residual_fifo_read[21][0],
				cb_residual_fifo_read[20],
				cb_residual_fifo_read[19][2:0],
				cb_residual_fifo_read[18],
				cb_residual_fifo_read[17],
				cb_residual_fifo_read[16],
				cb_residual_fifo_read[15][2:0],
				cb_residual_fifo_read[14],
				cb_residual_fifo_read[13],
				cb_residual_fifo_read[12],
				cb_residual_fifo_read[11][2:0],
				cb_residual_fifo_read[10],
				cb_residual_fifo_read[ 9],
				cb_residual_fifo_read[ 8],
				cb_residual_fifo_read[ 7][2:0],
				cb_residual_fifo_read[ 6],
				cb_residual_fifo_read[ 5],
				cb_residual_fifo_read[ 4],
				cb_residual_fifo_read[ 3][2:0],
				cb_residual_fifo_read[ 2],
				cb_residual_fifo_read[ 1],
				cb_residual_fifo_read[ 0]
	};
     	assign cr_residual_fifo_out = {
				cr_residual_fifo_read[21][0],
				cr_residual_fifo_read[20],
				cr_residual_fifo_read[19][2:0],
				cr_residual_fifo_read[18],
				cr_residual_fifo_read[17],
				cr_residual_fifo_read[16],
				cr_residual_fifo_read[15][2:0],
				cr_residual_fifo_read[14],
				cr_residual_fifo_read[13],
				cr_residual_fifo_read[12],
				cr_residual_fifo_read[11][2:0],
				cr_residual_fifo_read[10],
				cr_residual_fifo_read[ 9],
				cr_residual_fifo_read[ 8],
				cr_residual_fifo_read[ 7][2:0],
				cr_residual_fifo_read[ 6],
				cr_residual_fifo_read[ 5],
				cr_residual_fifo_read[ 4],
				cr_residual_fifo_read[ 3][2:0],
				cr_residual_fifo_read[ 2],
				cr_residual_fifo_read[ 1],
				cr_residual_fifo_read[ 0]
	};
     	assign y_residual_fifo_out = {
				yy_residual_fifo_read[21][0],
				yy_residual_fifo_read[20],
				yy_residual_fifo_read[19][2:0],
				yy_residual_fifo_read[18],
				yy_residual_fifo_read[17],
				yy_residual_fifo_read[16],
				yy_residual_fifo_read[15][2:0],
				yy_residual_fifo_read[14],
				yy_residual_fifo_read[13],
				yy_residual_fifo_read[12],
				yy_residual_fifo_read[11][2:0],
				yy_residual_fifo_read[10],
				yy_residual_fifo_read[ 9],
				yy_residual_fifo_read[ 8],
				yy_residual_fifo_read[ 7][2:0],
				yy_residual_fifo_read[ 6],
				yy_residual_fifo_read[ 5],
				yy_residual_fifo_read[ 4],
				yy_residual_fifo_read[ 3][2:0],
				yy_residual_fifo_read[ 2],
				yy_residual_fifo_read[ 1],
				yy_residual_fifo_read[ 0]
	};

//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------

assign rx_axis_fifo_tready = 1'b1;

	 always @(posedge clk) begin
         if (reset) begin


                rx_fsm_counter <= 0;
					 
                
               tx_axis_fifo_tvalid <= 1'b0;
               tx_axis_fifo_tdata <= 8'd0;
               tx_axis_fifo_tlast <= 1'b0;
               hevc_state <= HEVC_STATE_RX_INIT;
               
               config_write_en_out <= 0;
			   y_residual_fifo_write_en_out <= 0;
			   cb_residual_fifo_write_en_out <= 0;
			   cr_residual_fifo_write_en_out <= 0;
			   
               packet_order_mismatch <= 1'b0;

         end
         else begin
            case(hevc_state)
					HEVC_STATE_RX_INIT : begin    
                        if(rx_axis_fifo_tvalid) begin
							if(rx_fsm_counter ==11) begin
								rx_fsm_counter <= 0;
                                hevc_state <= HEVC_STATE_ETH_HEVC_HEADER1;
							end
							else begin
								rx_fsm_counter <= rx_fsm_counter + 1;
							end
						end
            		end
					HEVC_STATE_ETH_HEVC_HEADER1 : begin
            			if(rx_axis_fifo_tvalid) begin
            				if(rx_axis_fifo_tdata == 8'hcc) begin
            					hevc_state <= HEVC_STATE_ETH_HEVC_HEADER2;
            				end
            				else begin
            					hevc_state <= HEVC_STATE_WAIT_WRONG_PACKET_TX;// TODO wrong packet ack
            				end
            			end
            		end
					HEVC_STATE_ETH_HEVC_HEADER2 : begin
            			if(rx_axis_fifo_tvalid) begin
            				if(rx_axis_fifo_tdata == 8'hcc) begin  // different ethernet type
								hevc_state <= HEVC_STATE_INIT;
            				end
            				else begin
            					hevc_state <= HEVC_STATE_WAIT_WRONG_PACKET_TX;
            				end
            			end
            		end
					HEVC_STATE_WAIT_WRONG_PACKET_TX: begin
						if(rx_axis_fifo_tlast) begin
							hevc_state <= HEVC_STATE_TX_INIT;
							packet_order_mismatch <= 1;
						end
					end
					HEVC_STATE_INIT : begin    
                        if(rx_axis_fifo_tvalid) begin
							rx_fsm_counter <= 0;
							case(rx_axis_fifo_tdata)
								1: begin
									hevc_state <= HEVC_STATE_CONFIG_RECEIVE;
								end
								2: begin
									hevc_state <= HEVC_STATE_Y_RES_RECEIVE;
								end
								3: begin
									hevc_state <= HEVC_STATE_CB_RES_RECEIVE;
								end
								4: begin
									hevc_state <= HEVC_STATE_CR_RES_RECEIVE;
								end
							endcase
						end
					end
					HEVC_STATE_Y_RES_RECEIVE: begin
						if(rx_axis_fifo_tvalid) begin
							rx_fsm_counter <= rx_fsm_counter + 1;
							if(rx_fsm_counter <23) begin
								yy_residual_fifo_read[rx_fsm_counter] <= rx_axis_fifo_tdata;
							end
							if(rx_axis_fifo_tlast) begin
								if(!y_residual_fifo_is_full_in) begin
									hevc_state <= HEVC_STATE_TX_INIT;
									rx_fsm_counter <= 0;
									y_residual_fifo_write_en_out <= 1;
								end
								else begin
									hevc_state <= HEVC_STATE_Y_RES_FUL_WAIT;
								end
							end
						end
					end
					HEVC_STATE_Y_RES_FUL_WAIT: begin
						if(!y_residual_fifo_is_full_in) begin
							hevc_state <= HEVC_STATE_TX_INIT;
							rx_fsm_counter <= 0;
							y_residual_fifo_write_en_out <= 1;
						end
					end
					HEVC_STATE_CB_RES_RECEIVE: begin
						if(rx_axis_fifo_tvalid) begin
							rx_fsm_counter <= rx_fsm_counter + 1;
							if(rx_fsm_counter <23) begin
								cb_residual_fifo_read[rx_fsm_counter] <= rx_axis_fifo_tdata;
							end
							if(rx_axis_fifo_tlast) begin
								if(!cb_residual_fifo_is_full_in) begin
									hevc_state <= HEVC_STATE_TX_INIT;
									rx_fsm_counter <= 0;
									cb_residual_fifo_write_en_out <= 1;
								end
								else begin
									hevc_state <= HEVC_STATE_CB_RES_FUL_WAIT;
								end
							end
						end
					end
					HEVC_STATE_CB_RES_FUL_WAIT: begin
						if(!cb_residual_fifo_is_full_in) begin
							hevc_state <= HEVC_STATE_TX_INIT;
							rx_fsm_counter <= 0;
							cb_residual_fifo_write_en_out <= 1;
						end
					end
					HEVC_STATE_CR_RES_RECEIVE: begin
						if(rx_axis_fifo_tvalid) begin
							rx_fsm_counter <= rx_fsm_counter + 1;
							if(rx_fsm_counter <23) begin
								cr_residual_fifo_read[rx_fsm_counter] <= rx_axis_fifo_tdata;
							end
							if(rx_axis_fifo_tlast) begin
								if(!cr_residual_fifo_is_full_in) begin
									hevc_state <= HEVC_STATE_TX_INIT;
									rx_fsm_counter <= 0;
									cr_residual_fifo_write_en_out <= 1;
								end
								else begin
									hevc_state <= HEVC_STATE_CR_RES_FUL_WAIT;
								end
							end
						end
					end
					HEVC_STATE_CR_RES_FUL_WAIT: begin
						if(!cr_residual_fifo_is_full_in) begin
							hevc_state <= HEVC_STATE_TX_INIT;
							rx_fsm_counter <= 0;
							cr_residual_fifo_write_en_out <= 1;
						end
					end
            		HEVC_STATE_CONFIG_RECEIVE: begin
						if(rx_axis_fifo_tvalid) begin
							rx_fsm_counter <= rx_fsm_counter + 1;
							case(rx_fsm_counter)
								0: begin
									config_fifo_out[7:0]  <= rx_axis_fifo_tdata;
								end
								1: begin
									config_fifo_out[15:8] <= rx_axis_fifo_tdata;
								end
								2: begin
									config_fifo_out[23:16] <= rx_axis_fifo_tdata;
								end
								3: begin
									config_fifo_out[31:24] <= rx_axis_fifo_tdata;
								end
							endcase
							if(rx_axis_fifo_tlast) begin
								if(!config_input_fifo_is_full) begin
									hevc_state <= HEVC_STATE_TX_INIT;
									rx_fsm_counter <= 0;
									config_write_en_out <= 1;
								end
								else begin
									hevc_state <= HEVC_STATE_CONFIG_FUL_WAIT;
								end
							end
						end
					end
					HEVC_STATE_CONFIG_FUL_WAIT: begin
						if(!config_input_fifo_is_full) begin
							hevc_state <= HEVC_STATE_TX_INIT;
							rx_fsm_counter <= 0;
							config_write_en_out <= 1;							
						end
					end
                    HEVC_STATE_TX_INIT : begin
							config_write_en_out <= 0;
						   y_residual_fifo_write_en_out <= 0;
						   cb_residual_fifo_write_en_out <= 0;
						   cr_residual_fifo_write_en_out <= 0;
                            tx_axis_fifo_tdata   <= 8'hcd;
                            tx_axis_fifo_tvalid  <= 1'b1;
                            hevc_state <= HEVC_STATE_SRC_MAC_TX;
                    end
                    HEVC_STATE_SRC_MAC_TX : begin

                        if(tx_axis_fifo_tready == 1'b1) begin                            
                            if(rx_fsm_counter == 5) begin
                                rx_fsm_counter     <= 0;
                                hevc_state  <= HEVC_STATE_DST_MAC_TX;
                                tx_axis_fifo_tdata   <= 8'hcc;
                                tx_axis_fifo_tvalid  <= 1'b1;
                            end
                            else begin
                                rx_fsm_counter     <= rx_fsm_counter + 1;
                            end
                        end

                    end
                    HEVC_STATE_DST_MAC_TX : begin

                            tx_axis_fifo_tdata   <= 8'hcd;
                            tx_axis_fifo_tvalid  <= 1'b1;

                        if(tx_axis_fifo_tready == 1'b1) begin
                            if(rx_fsm_counter == 5) begin
                                rx_fsm_counter     <= 0;
                                hevc_state  <= HEVC_STATE_DATA_REQUEST_TX;
                                tx_axis_fifo_tdata   <= 8'hcc;
                                tx_axis_fifo_tvalid  <= 1'b1;
                            end
                            else begin
                                rx_fsm_counter     <= rx_fsm_counter + 1;
                            end
                        end
                       
                    end
                    HEVC_STATE_DATA_REQUEST_TX : begin
                        if(tx_axis_fifo_tready == 1'b1) begin
                           
                            if(rx_fsm_counter == 1) begin
                                tx_axis_fifo_tdata <= 8'hbb;
                                rx_fsm_counter <= rx_fsm_counter + 1;
                            end
                            else if(rx_fsm_counter == 2) begin
                                tx_axis_fifo_tdata <= 8'hbb;
                                rx_fsm_counter <= rx_fsm_counter + 1;
                            end
                            else if(rx_fsm_counter == 3) begin
                                tx_axis_fifo_tdata <= 8'hbb;
                                rx_fsm_counter <= rx_fsm_counter + 1;
                            end
                            else if(rx_fsm_counter == 4) begin
                                tx_axis_fifo_tdata <= 8'hbb;
                                rx_fsm_counter <= rx_fsm_counter + 1;
                            end
                            else if(rx_fsm_counter == 5) begin
                                tx_axis_fifo_tlast <= 1'b1;
                                rx_fsm_counter <= rx_fsm_counter + 1;
									if(packet_order_mismatch) begin
										tx_axis_fifo_tdata <= 8'hff;
									end
									else begin
										tx_axis_fifo_tdata <= 8'haa;
									end
                            end
                            else if(rx_fsm_counter == 6) begin
                                tx_axis_fifo_tlast <= 1'b0;
                                tx_axis_fifo_tvalid <= 1'b0;
                                rx_fsm_counter <= 0;
                                hevc_state <= HEVC_STATE_RX_INIT;
								packet_order_mismatch <= 0;
                            end
                            else begin
                                rx_fsm_counter <= rx_fsm_counter + 1;
                            end
                        end

                    end
     
            endcase
         end
     end
     
    // always @(posedge clk) begin : tx_engine
    //     if (reset) begin
    //         txstate <= TXSTATE_TX_INIT;
    //         tx_first_time <= 1'b1;

    //        tx_axis_fifo_tvalid <= 1'b0;
    //        tx_axis_fifo_tdata <= 8'd0;
    //        tx_axis_fifo_tlast <= 1'b0;

    //        tx_counter <= 0;
    //        tx_done <= 1'b0;
    //        tx_stop_packet_sent <= 1'b0;
    //     end
    //     else begin
    //         case(txstate)
    //            TXSTATE_TX_INIT : begin
    //                tx_axis_fifo_tdata   <= 8'hcd;
    //                tx_axis_fifo_tvalid  <= 1'b1;
    //                txstate <= TXSTATE_SRC_MAC_TX;
    //                tx_done <= 1'b0;
    //            end
    //            TXSTATE_SRC_MAC_TX : begin
    //                tx_axis_fifo_tdata   <= 8'hcd;
    //                tx_axis_fifo_tvalid  <= 1'b1;

    //                if(tx_axis_fifo_tready == 1'b1) begin                            
    //                    if(tx_counter == 5) begin
    //                        tx_counter  <= 0;
    //                        txstate  <= TXSTATE_DST_MAC_TX;
    //                        tx_axis_fifo_tdata   <= 8'hcc;
    //                        tx_axis_fifo_tvalid  <= 1'b1;
    //                    end
    //                    else begin
    //                        tx_counter     <= tx_counter + 1;
    //                    end
    //                end 
    //            end
    //            TXSTATE_DST_MAC_TX : begin

    //                tx_axis_fifo_tdata   <= 8'hcd;
    //                tx_axis_fifo_tvalid  <= 1'b1;

    //                if(tx_axis_fifo_tready == 1'b1) begin
    //                    if(tx_counter == 5) begin
    //                        tx_counter  <= 0;
    //                        txstate     <=  TXSTATE_DATA_REQUEST_TX;
    //                        tx_axis_fifo_tdata   <= 8'hcc;
    //                        tx_axis_fifo_tvalid  <= 1'b1;
    //                    end
    //                    else begin
    //                        tx_counter     <= tx_counter + 1;
    //                    end
    //                end
                   
    //            end
    //            TXSTATE_DATA_REQUEST_TX: begin
    //                if(tx_axis_fifo_tready == 1'b1) begin
                        
    //                    if(tx_counter == 0) begin
    //                        if((rx_done == 1'b0) && (tx_first_time == 1'b0) && (tx_stop_packet_sent == 1'b0)) begin
    //                            tx_axis_fifo_tvalid <= 1'b0;
    //                            txstate <= TXSTATE_DATA_WAIT1;
    //                        end
    //                        else begin
    //                            tx_first_time <= 1'b0;
    //                            tx_axis_fifo_tvalid <= 1'b1;
    //                            tx_axis_fifo_tlast  <= 1'b1;
                               
    //                            if(output_fifo_full_in == 1'b0) begin
    //                                 tx_axis_fifo_tdata <= 8'hdd;
    //                                 tx_stop_packet_sent <= 1'b0;
    //                            end
    //                            else begin
    //                                 tx_axis_fifo_tdata <= 8'hee;
    //                                 tx_stop_packet_sent <= 1'b1;
    //                            end
    //                            tx_counter <= tx_counter + 1;
    //                        end
    //                    end
    //                    else if(tx_counter == 1) begin
    //                        tx_axis_fifo_tlast <= 1'b0;
    //                        tx_axis_fifo_tvalid <= 1'b0;
    //                        tx_counter <= 0;
    //                        tx_done <= 1'b1;
    //                        txstate <= TXSTATE_TX_INIT;
    //                    end
    //                    else begin
    //                        tx_counter <= tx_counter + 1;
    //                    end

    //                end
    //            end
    //            TXSTATE_DATA_WAIT1 : begin
    //                if((rx_done == 1'b1)) begin
    //                    tx_axis_fifo_tvalid <= 1'b1;
    //                    tx_axis_fifo_tlast  <= 1'b1;
                       
    //                    if(output_fifo_full_in == 1'b0) begin
    //                         tx_axis_fifo_tdata <= 8'hdd;
    //                         tx_stop_packet_sent <= 1'b0;
    //                    end
    //                    else begin
    //                         tx_axis_fifo_tdata <= 8'hee;
    //                         tx_stop_packet_sent <= 1'b1;
    //                    end

    //                    txstate <= TXSTATE_DATA_WAIT2;
    //                end
    //            end
    //            TXSTATE_DATA_WAIT2 : begin
    //                if(tx_axis_fifo_tready == 1'b1) begin
    //                    tx_axis_fifo_tlast <= 1'b0;
    //                    tx_axis_fifo_tvalid <= 1'b0;
    //                    tx_counter <= 0;
    //                    tx_done <= 1'b1;
    //                    txstate <= TXSTATE_TX_INIT;
    //                end
    //            end
    //         endcase
    //     end
    // end

     

    
   
    
    
endmodule