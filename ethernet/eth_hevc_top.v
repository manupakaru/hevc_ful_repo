
`timescale 1ns / 1ps

module eth_hevc_top
    (
      	clk,
      	reset,

      	output_fifo_data_out,
		output_fifo_write_en_out,
		output_fifo_full_in,

		rx_axis_fifo_tvalid,
		rx_axis_fifo_tdata,
		rx_axis_fifo_tlast,
		rx_axis_fifo_tready,

		tx_axis_fifo_tvalid,
		tx_axis_fifo_tdata,
		tx_axis_fifo_tlast,
		tx_axis_fifo_tready
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
	localparam						HEVC_STATE_INIT = 0;
	localparam						HEVC_STATE_ETH_HEVC_HEADER1 = 1;
	localparam						HEVC_STATE_ETH_HEVC_HEADER2 = 2;
	localparam						HEVC_STATE_WRONG_PACKET = 3;
	localparam						HEVC_STATE_MSB = 4;
	localparam						HEVC_STATE_LSB = 5;
	localparam						HEVC_STATE_DONE = 6;
	localparam						HEVC_STATE_SRC_MAC_TX = 7;
	localparam						HEVC_STATE_DST_MAC_TX = 8;
	localparam						HEVC_STATE_DATA_REQUEST_TX = 9;
    localparam                      HEVC_STATE_TX_INIT = 10;

    localparam                      HEVC_STATE_BYTE_3 = 11;
    localparam                      HEVC_STATE_BYTE_2 = 12;
    localparam                      HEVC_STATE_MISMATCH_SERVED = 13;
    
    localparam                      TXSTATE_SRC_MAC_TX = 1;
    localparam                      TXSTATE_DST_MAC_TX = 2;
    localparam                      TXSTATE_DATA_REQUEST_TX = 3;
    localparam                      TXSTATE_TX_INIT = 0;
    localparam                      TXSTATE_DATA_WAIT1 = 4;
    localparam                      TXSTATE_DATA_WAIT2 = 5;

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input clk;
    input reset;

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
	integer hevc_state;
    integer txstate; 
    reg     tx_first_time;

    integer tx_counter;
    integer rx_counter;
    integer rx_fsm_counter;
    reg     rx_done;
    reg     tx_done;
    reg     tx_stop_packet_sent;
    
    integer recieved_counter;
    reg     packet_order_mismatch;
    
    reg     output_fifo_full_reg;

	output reg [32 - 1 :0]			output_fifo_data_out;
	output reg 						output_fifo_write_en_out;
	input 							output_fifo_full_in;

	input 							rx_axis_fifo_tvalid;
	input 	[8 - 1 :0]				rx_axis_fifo_tdata;
	input 							rx_axis_fifo_tlast;
	output          				rx_axis_fifo_tready;

	output reg						tx_axis_fifo_tvalid;
	output reg	[8 - 1 :0]			tx_axis_fifo_tdata;
	output reg						tx_axis_fifo_tlast;
	input 							tx_axis_fifo_tready;



     
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------

assign rx_axis_fifo_tready = 1'b1;

	 always @(posedge clk) begin
         if (reset) begin
                output_fifo_data_out <= 32'd0;
                output_fifo_write_en_out <= 1'b0;

                rx_fsm_counter <= 0;
                rx_counter <= 0;
//                rx_axis_fifo_tready <= 1'b0;
                rx_done <= 1'b0;
                
               tx_axis_fifo_tvalid <= 1'b0;
               tx_axis_fifo_tdata <= 8'd0;
               tx_axis_fifo_tlast <= 1'b0;
               hevc_state <= HEVC_STATE_TX_INIT;
               
               output_fifo_full_reg <= 1'b0;
               packet_order_mismatch <= 1'b0;

         end
         else begin
            case(hevc_state)
                    HEVC_STATE_TX_INIT : begin
                        if((output_fifo_full_in == 1'b0)||(packet_order_mismatch == 1'b1)) begin
                            tx_axis_fifo_tdata   <= 8'hcd;
                            tx_axis_fifo_tvalid  <= 1'b1;
                            hevc_state <= HEVC_STATE_SRC_MAC_TX;
                        end
                    end
            		HEVC_STATE_INIT : begin     
                        rx_done <= 1'b0;
                        if(rx_axis_fifo_tvalid) begin
                            if(rx_fsm_counter == 0) begin
                                recieved_counter[31:24] <= rx_axis_fifo_tdata;
                                rx_fsm_counter <= rx_fsm_counter + 1;
                            end
                            else if(rx_fsm_counter == 1)begin
                                recieved_counter[23:16] <= rx_axis_fifo_tdata;
                                rx_fsm_counter <= rx_fsm_counter + 1;
                            end
                            else if(rx_fsm_counter == 2)begin
                                recieved_counter[15:8] <= rx_axis_fifo_tdata;
                                rx_fsm_counter <= rx_fsm_counter + 1;
                            end
                            else if(rx_fsm_counter == 3)begin
                                recieved_counter[7:0] <= rx_axis_fifo_tdata;
                                rx_fsm_counter <= rx_fsm_counter + 1;   
                            end
                            else if(rx_fsm_counter == 4)begin
                                if(recieved_counter != rx_counter + 1) begin
//                                    hevc_state <= HEVC_STATE_WRONG_PACKET;
//                                    hevc_state <= HEVC_STATE_TX_INIT;
                                    
//                                    rx_fsm_counter <= 0;
                                    
                                     if(packet_order_mismatch == 1'b1) begin
                                        rx_fsm_counter <= 1'b0;
                                        hevc_state <= HEVC_STATE_MISMATCH_SERVED;
                                     end
                                     else begin
                                        rx_fsm_counter <= rx_fsm_counter + 1;
                                        packet_order_mismatch <= 1'b1;
                                     end
                                end
                                else begin
                                    packet_order_mismatch <= 1'b0;
                                    rx_fsm_counter <= rx_fsm_counter + 1; 
                                end
                            end
                        	else if(rx_fsm_counter == 11) begin
                        		rx_fsm_counter <= 0;
                                hevc_state <= HEVC_STATE_ETH_HEVC_HEADER1;
                        	end
                        	else begin
                        		rx_fsm_counter <= rx_fsm_counter + 1;
                        	end
//                            rx_axis_fifo_tready           <= 1'b1;
                        end
            		end
            		HEVC_STATE_ETH_HEVC_HEADER1 : begin
            			if(rx_axis_fifo_tvalid) begin
            				if(rx_axis_fifo_tdata == 8'hcc) begin
            					hevc_state <= HEVC_STATE_ETH_HEVC_HEADER2;
            				end
            				else begin
                                packet_order_mismatch <= 1'b0;
            					hevc_state <= HEVC_STATE_WRONG_PACKET;
            				end
            			end
            		end
            		HEVC_STATE_ETH_HEVC_HEADER2 : begin
            			if(rx_axis_fifo_tvalid) begin
            				if(rx_axis_fifo_tdata == 8'hcc) begin  // different ethernet type
                                if(packet_order_mismatch == 1'b1) begin
                                    hevc_state <= HEVC_STATE_WRONG_PACKET;
                                end
                                else begin
                                    hevc_state <= HEVC_STATE_MSB;
                                end
            				end
            				else begin
                                packet_order_mismatch <= 1'b0;
            					hevc_state <= HEVC_STATE_WRONG_PACKET;
            				end
            			end
            		end
                    HEVC_STATE_MISMATCH_SERVED : begin
                        if(rx_axis_fifo_tvalid) begin
            				if(rx_axis_fifo_tlast) begin
                                hevc_state <= HEVC_STATE_INIT;
                            end
                        end
                    end
            		HEVC_STATE_WRONG_PACKET : begin
            			if(rx_axis_fifo_tvalid) begin
            				if(rx_axis_fifo_tlast) begin
                                if(packet_order_mismatch == 1'b1) begin
                                    hevc_state <= HEVC_STATE_TX_INIT;
                                end
                                else begin
                                    hevc_state <= HEVC_STATE_INIT;
                                end
            				end
            			end
            		end
                    HEVC_STATE_MSB : begin
                        output_fifo_write_en_out      <= 1'b0;
                        if(rx_axis_fifo_tvalid) begin
//                            rx_axis_fifo_tready           <= 1'b1;
                            output_fifo_data_out[31:24] <= rx_axis_fifo_tdata;

                            hevc_state <= HEVC_STATE_BYTE_3;
                        end
                    end
                    HEVC_STATE_BYTE_3 : begin
                        output_fifo_write_en_out      <= 1'b0;
                        if(rx_axis_fifo_tvalid) begin
                            output_fifo_data_out[23:16] <= rx_axis_fifo_tdata;

                            hevc_state <= HEVC_STATE_BYTE_2;
                        end
                    end
                    HEVC_STATE_BYTE_2 : begin
                        output_fifo_write_en_out      <= 1'b0;
                        if(rx_axis_fifo_tvalid) begin
                            output_fifo_data_out[15:8] <= rx_axis_fifo_tdata;

                            hevc_state <= HEVC_STATE_LSB;
                        end
                    end
                    HEVC_STATE_LSB : begin

                        if(rx_axis_fifo_tvalid) begin
                            output_fifo_data_out[7:0] <= rx_axis_fifo_tdata;
                            
                            if(rx_axis_fifo_tlast == 1'b1) begin
                                hevc_state <= HEVC_STATE_DONE;
                            end
                            else begin
                                hevc_state <= HEVC_STATE_MSB;
                            end
                            output_fifo_write_en_out      <= 1'b1;
//                            rx_axis_fifo_tready           <= 1'b1;
                                
                        end     

                    end
                    HEVC_STATE_DONE : begin
                        rx_done <= 1'b1;
                        rx_counter <= rx_counter + 1'b1;
                        output_fifo_write_en_out      <= 1'b0;
//                        rx_axis_fifo_tready           <= 1'b0;
                        // if(tx_done == 1'b1) begin
                            tx_axis_fifo_tdata   <= 8'hcd;
                            tx_axis_fifo_tvalid  <= 1'b1;
                            hevc_state <= HEVC_STATE_SRC_MAC_TX;
                        // end
                    end
                    HEVC_STATE_SRC_MAC_TX : begin

                        tx_axis_fifo_tdata   <= 8'hcd;
                        tx_axis_fifo_tvalid  <= 1'b1;

                        if(tx_axis_fifo_tready == 1'b1) begin                            
                            if(rx_fsm_counter == 5) begin
                                rx_fsm_counter     <= 0;
                                hevc_state  <= HEVC_STATE_DST_MAC_TX;
                                tx_axis_fifo_tdata   <= 8'hcc;
                                tx_axis_fifo_tvalid  <= 1'b1;
                            end
                            else begin
                                rx_fsm_counter     <= rx_fsm_counter + 1;
                            end
                        end

                    end
                    HEVC_STATE_DST_MAC_TX : begin

                            tx_axis_fifo_tdata   <= 8'hcd;
                            tx_axis_fifo_tvalid  <= 1'b1;

                        if(tx_axis_fifo_tready == 1'b1) begin
                            if(rx_fsm_counter == 5) begin
                                rx_fsm_counter     <= 0;
                                hevc_state  <= HEVC_STATE_DATA_REQUEST_TX;
                                tx_axis_fifo_tdata   <= 8'hcc;
                                tx_axis_fifo_tvalid  <= 1'b1;
                            end
                            else begin
                                rx_fsm_counter     <= rx_fsm_counter + 1;
                            end
                        end
                       
                    end
                    HEVC_STATE_DATA_REQUEST_TX : begin

//                            tx_axis_fifo_tdata   <= 8'hcc;
//                            tx_axis_fifo_tvalid  <= 1'b1;

                        if(tx_axis_fifo_tready == 1'b1) begin
                           
                            if(rx_fsm_counter == 1) begin
                                tx_axis_fifo_tdata <= rx_counter[31:24];
                                rx_fsm_counter <= rx_fsm_counter + 1;
                            end
                            else if(rx_fsm_counter == 2) begin
                                tx_axis_fifo_tdata <= rx_counter[23:16];
                                rx_fsm_counter <= rx_fsm_counter + 1;
                            end
                            else if(rx_fsm_counter == 3) begin
                                tx_axis_fifo_tdata <= rx_counter[15:8];
                                rx_fsm_counter <= rx_fsm_counter + 1;
                            end
                            else if(rx_fsm_counter == 4) begin
                                tx_axis_fifo_tdata <= rx_counter[7:0];
                                rx_fsm_counter <= rx_fsm_counter + 1;
                            end
                            else if(rx_fsm_counter == 5) begin
                                tx_axis_fifo_tlast <= 1'b1;
                                rx_fsm_counter <= rx_fsm_counter + 1;
                                
                                if(packet_order_mismatch == 1'b1) begin
                                    tx_axis_fifo_tdata <= 8'haa;
//                                    packet_order_mismatch <= 1'b0;
                                end
                                else if(output_fifo_full_in == 1'b0) begin
                                    tx_axis_fifo_tdata <= 8'hdd;
                                    output_fifo_full_reg <= 1'b0;
                                end
                                else begin
                                    tx_axis_fifo_tdata <= 8'hee;
                                    output_fifo_full_reg <= 1'b1;
                                end
                            end
                            else if(rx_fsm_counter == 6) begin
                                tx_axis_fifo_tlast <= 1'b0;
                                tx_axis_fifo_tvalid <= 1'b0;
                                rx_fsm_counter <= 0;
                                if(output_fifo_full_reg == 1'b0) begin
                                    hevc_state <= HEVC_STATE_INIT;
                                end
                                else begin
                                    hevc_state <= HEVC_STATE_TX_INIT;
                                end
                            end
                            else begin
                                rx_fsm_counter <= rx_fsm_counter + 1;
                            end
                        end

                    end
            endcase
         end
     end
     
    // always @(posedge clk) begin : tx_engine
    //     if (reset) begin
    //         txstate <= TXSTATE_TX_INIT;
    //         tx_first_time <= 1'b1;

    //        tx_axis_fifo_tvalid <= 1'b0;
    //        tx_axis_fifo_tdata <= 8'd0;
    //        tx_axis_fifo_tlast <= 1'b0;

    //        tx_counter <= 0;
    //        tx_done <= 1'b0;
    //        tx_stop_packet_sent <= 1'b0;
    //     end
    //     else begin
    //         case(txstate)
    //            TXSTATE_TX_INIT : begin
    //                tx_axis_fifo_tdata   <= 8'hcd;
    //                tx_axis_fifo_tvalid  <= 1'b1;
    //                txstate <= TXSTATE_SRC_MAC_TX;
    //                tx_done <= 1'b0;
    //            end
    //            TXSTATE_SRC_MAC_TX : begin
    //                tx_axis_fifo_tdata   <= 8'hcd;
    //                tx_axis_fifo_tvalid  <= 1'b1;

    //                if(tx_axis_fifo_tready == 1'b1) begin                            
    //                    if(tx_counter == 5) begin
    //                        tx_counter  <= 0;
    //                        txstate  <= TXSTATE_DST_MAC_TX;
    //                        tx_axis_fifo_tdata   <= 8'hcc;
    //                        tx_axis_fifo_tvalid  <= 1'b1;
    //                    end
    //                    else begin
    //                        tx_counter     <= tx_counter + 1;
    //                    end
    //                end 
    //            end
    //            TXSTATE_DST_MAC_TX : begin

    //                tx_axis_fifo_tdata   <= 8'hcd;
    //                tx_axis_fifo_tvalid  <= 1'b1;

    //                if(tx_axis_fifo_tready == 1'b1) begin
    //                    if(tx_counter == 5) begin
    //                        tx_counter  <= 0;
    //                        txstate     <=  TXSTATE_DATA_REQUEST_TX;
    //                        tx_axis_fifo_tdata   <= 8'hcc;
    //                        tx_axis_fifo_tvalid  <= 1'b1;
    //                    end
    //                    else begin
    //                        tx_counter     <= tx_counter + 1;
    //                    end
    //                end
                   
    //            end
    //            TXSTATE_DATA_REQUEST_TX: begin
    //                if(tx_axis_fifo_tready == 1'b1) begin
                        
    //                    if(tx_counter == 0) begin
    //                        if((rx_done == 1'b0) && (tx_first_time == 1'b0) && (tx_stop_packet_sent == 1'b0)) begin
    //                            tx_axis_fifo_tvalid <= 1'b0;
    //                            txstate <= TXSTATE_DATA_WAIT1;
    //                        end
    //                        else begin
    //                            tx_first_time <= 1'b0;
    //                            tx_axis_fifo_tvalid <= 1'b1;
    //                            tx_axis_fifo_tlast  <= 1'b1;
                               
    //                            if(output_fifo_full_in == 1'b0) begin
    //                                 tx_axis_fifo_tdata <= 8'hdd;
    //                                 tx_stop_packet_sent <= 1'b0;
    //                            end
    //                            else begin
    //                                 tx_axis_fifo_tdata <= 8'hee;
    //                                 tx_stop_packet_sent <= 1'b1;
    //                            end
    //                            tx_counter <= tx_counter + 1;
    //                        end
    //                    end
    //                    else if(tx_counter == 1) begin
    //                        tx_axis_fifo_tlast <= 1'b0;
    //                        tx_axis_fifo_tvalid <= 1'b0;
    //                        tx_counter <= 0;
    //                        tx_done <= 1'b1;
    //                        txstate <= TXSTATE_TX_INIT;
    //                    end
    //                    else begin
    //                        tx_counter <= tx_counter + 1;
    //                    end

    //                end
    //            end
    //            TXSTATE_DATA_WAIT1 : begin
    //                if((rx_done == 1'b1)) begin
    //                    tx_axis_fifo_tvalid <= 1'b1;
    //                    tx_axis_fifo_tlast  <= 1'b1;
                       
    //                    if(output_fifo_full_in == 1'b0) begin
    //                         tx_axis_fifo_tdata <= 8'hdd;
    //                         tx_stop_packet_sent <= 1'b0;
    //                    end
    //                    else begin
    //                         tx_axis_fifo_tdata <= 8'hee;
    //                         tx_stop_packet_sent <= 1'b1;
    //                    end

    //                    txstate <= TXSTATE_DATA_WAIT2;
    //                end
    //            end
    //            TXSTATE_DATA_WAIT2 : begin
    //                if(tx_axis_fifo_tready == 1'b1) begin
    //                    tx_axis_fifo_tlast <= 1'b0;
    //                    tx_axis_fifo_tvalid <= 1'b0;
    //                    tx_counter <= 0;
    //                    tx_done <= 1'b1;
    //                    txstate <= TXSTATE_TX_INIT;
    //                end
    //            end
    //         endcase
    //     end
    // end

     

    
   
    
    
endmodule