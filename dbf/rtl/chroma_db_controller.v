`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:40:28 11/17/2013 
// Design Name: 
// Module Name:    chroma_db_controller 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module chroma_db_controller(
        clk,
        reset,
        start,
        done,
        cb_q_block_1_in,
        cb_q_block_2_in,
        cb_q_block_3_in,
        cb_q_block_4_in,
        cb_A_block_in,  
        cb_B_block_in,  
        cr_q_block_1_in,
        cr_q_block_2_in,
        cr_q_block_3_in,
        cr_q_block_4_in,
        cr_A_block_in,  
        cr_B_block_in,  
        Xc,
        Yc,
        qp_1,
        qp_A,
        qp_C,
        qp_D,
        BS_v1,
        BS_v2,
        BS_h1,
        BS_h2,
        BS_h0,
        pps_cb_qp_offset,
        pps_cr_qp_offset,
        slice_tc_offset,
        pic_width_in,
        pic_height_in,
		ver_edge_dbf_disable_in,
		hor_edge_dbf_disable_in,
		h0_edge_dbf_disable_in,
		pcm_bypass_AB_dbf_disable_in,
		pcm_bypass_dbf_disable_in,
		pcm_bypass_C_dbf_disable_in,
		pcm_bypass_D_dbf_disable_in,
        cb_q_block_1_out,
        cb_q_block_2_out,
        cb_q_block_3_out,
        cb_q_block_4_out,
        cb_A_block_out,  
        cb_B_block_out,  
        cb_C_block_out,  
        cb_D_block_out,
        cb_E_block_out,
        cr_q_block_1_out,
        cr_q_block_2_out,
        cr_q_block_3_out,
        cr_q_block_4_out,
        cr_A_block_out,  
        cr_B_block_out,  
        cr_C_block_out,  
        cr_D_block_out,
        cr_E_block_out
    );
	
	`include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

    localparam                          VERT_UP             = 	3'b000;
    localparam                          VERT_DOWN           = 	3'b001;
    localparam                          HOR_LEFT            = 	3'b010;
    localparam                          HOR_RIGHT           = 	3'b011;
    localparam                          HOR_FAR_RIGHT       = 	3'b100;
    localparam                          CB                  = 	0     ;
    localparam                          CR                  = 	1     ;
    
	localparam                          SAMPLE_ROW_BUF_DATA_WIDTH 	= 	(BIT_DEPTH*4) ;
    
    localparam                          STATE_INIT          = 	0 ; 
    localparam                          STATE_1             = 	1 ;
    localparam                          STATE_1_WAIT        = 	2 ;
    localparam                          STATE_2             = 	3 ;
    localparam                          STATE_2_WAIT        = 	4 ;
    localparam                          STATE_3             = 	5 ;
    localparam                          STATE_3_WAIT        = 	6 ;
    localparam                          STATE_4             = 	7 ;
    localparam                          STATE_4_WAIT        = 	8 ;
    localparam                          STATE_5             = 	9 ;
    localparam                          STATE_5_WAIT        = 	10;
	
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input                               clk;
    input                               reset;
    
    input                               start;
    output	reg					        done;
    
    // Pixel inputs
    input       	[31:0]      		cb_q_block_1_in ;
    input       	[31:0]      		cb_q_block_2_in ;
    input       	[31:0]      		cb_q_block_3_in ;
    input       	[31:0]      		cb_q_block_4_in ;
    input       	[31:0]      		cb_A_block_in   ;
    input       	[31:0]      		cb_B_block_in   ;
	
    input       	[31:0]      		cr_q_block_1_in ;
    input       	[31:0]      		cr_q_block_2_in ;
    input       	[31:0]      		cr_q_block_3_in ;
    input       	[31:0]      		cr_q_block_4_in ;
    input       	[31:0]      		cr_A_block_in   ;
    input       	[31:0]      		cr_B_block_in   ;
    
    // Parameter inputs
    input           [11:0]                  Xc;
    input           [11:0]                  Yc;
    input			[QP_WIDTH-1:0]			qp_1;
	input			[QP_WIDTH-1:0]			qp_A;
	input			[QP_WIDTH-1:0]			qp_C;		// ATTN - should be read from BRAM, not input
	input			[QP_WIDTH-1:0]			qp_D;		// ATTN - should be read from BRAM, not input
	input           [BS_WIDTH-1:0]          BS_v1;
    input           [BS_WIDTH-1:0]          BS_v2;
    input           [BS_WIDTH-1:0]          BS_h1;
    input           [BS_WIDTH-1:0]          BS_h2;
    input           [BS_WIDTH-1:0]          BS_h0;  
	input			[7:0]				    pps_cb_qp_offset;
    input			[7:0]				    pps_cr_qp_offset;
	input			[7:0]				    slice_tc_offset;
    input           [11:0]                  pic_width_in;
    input           [11:0]                  pic_height_in;
	input									ver_edge_dbf_disable_in;
	input                                   hor_edge_dbf_disable_in;
	input									h0_edge_dbf_disable_in;
	input                                   pcm_bypass_AB_dbf_disable_in;
	input									pcm_bypass_dbf_disable_in;
	input									pcm_bypass_C_dbf_disable_in;
	input									pcm_bypass_D_dbf_disable_in;
    
    // Pixel outputs
    output          [31:0]          	cb_q_block_1_out;
    output          [31:0]          	cb_q_block_2_out;
    output          [31:0]          	cb_q_block_3_out;
    output          [31:0]          	cb_q_block_4_out;
    output          [31:0]          	cb_A_block_out  ;
    output          [31:0]          	cb_B_block_out  ;
    output          [31:0]          	cb_C_block_out  ;
    output          [31:0]          	cb_D_block_out  ;
    output          [31:0]          	cb_E_block_out  ;
		
    output          [31:0]          	cr_q_block_1_out;
    output          [31:0]          	cr_q_block_2_out;
    output          [31:0]          	cr_q_block_3_out;
    output          [31:0]          	cr_q_block_4_out;
    output          [31:0]          	cr_A_block_out  ;
    output          [31:0]          	cr_B_block_out  ;
    output          [31:0]          	cr_C_block_out  ;
    output          [31:0]          	cr_D_block_out  ;
    output          [31:0]          	cr_E_block_out  ;

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/global_para.v"


    
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    
    // Pixel Registers
    reg             [7:0]           cb_q_block_1 [3:0];
    reg             [7:0]           cb_q_block_2 [3:0];
    reg             [7:0]           cb_q_block_3 [3:0];
    reg             [7:0]           cb_q_block_4 [3:0];
    reg             [7:0]           cb_A_block   [3:0];
    reg             [7:0]           cb_B_block   [3:0];
    reg             [7:0]           cb_C_block   [3:0];
    reg             [7:0]           cb_D_block   [3:0];	
    reg             [7:0]           cb_E_block   [3:0];	
    
    reg             [7:0]           cr_q_block_1 [3:0];
    reg             [7:0]           cr_q_block_2 [3:0];
    reg             [7:0]           cr_q_block_3 [3:0];
    reg             [7:0]           cr_q_block_4 [3:0];
    reg             [7:0]           cr_A_block   [3:0];
    reg             [7:0]           cr_B_block   [3:0];
    reg             [7:0]           cr_C_block   [3:0];
    reg             [7:0]           cr_D_block   [3:0];	
    reg             [7:0]           cr_E_block   [3:0];	
    
    // Signals for BRAM row buffers
    reg                             						we_a;  
    reg                             						we_b;
    reg                             						en_a;
    reg                             						en_b;
    reg             [9:0]           						addr_a;
    reg             [9:0]           						addr_b;
    reg             [SAMPLE_ROW_BUF_DATA_WIDTH-1:0]    		cb_data_in_a;
    reg             [SAMPLE_ROW_BUF_DATA_WIDTH-1:0]    		cb_data_in_b;
    wire            [SAMPLE_ROW_BUF_DATA_WIDTH-1:0]    		cb_data_out_a;
    wire            [SAMPLE_ROW_BUF_DATA_WIDTH-1:0]    		cb_data_out_b;
    reg             [SAMPLE_ROW_BUF_DATA_WIDTH-1:0]    		cr_data_in_a;
    reg             [SAMPLE_ROW_BUF_DATA_WIDTH-1:0]    		cr_data_in_b;
    wire            [SAMPLE_ROW_BUF_DATA_WIDTH-1:0]    		cr_data_out_a;
    wire            [SAMPLE_ROW_BUF_DATA_WIDTH-1:0]    		cr_data_out_b;
    
    // Signals for chroma edge filter modules
    reg                             start_filters;
    reg           [2:0]             edge_type_filter;
    wire                            done_filter_cb;
    wire                            done_filter_cr;
    
    wire          [7:0]             cb_p_blk_0  ;
    wire          [7:0]             cb_p_blk_1  ;
    wire          [7:0]             cb_p_blk_2  ;
    wire          [7:0]             cb_p_blk_3  ;
    wire          [7:0]             cb_q_blk_0  ;
    wire          [7:0]             cb_q_blk_1  ;
    wire          [7:0]             cb_q_blk_2  ;
    wire          [7:0]             cb_q_blk_3  ;
    
    wire          [7:0]             cr_p_blk_0  ;
    wire          [7:0]             cr_p_blk_1  ;
    wire          [7:0]             cr_p_blk_2  ;
    wire          [7:0]             cr_p_blk_3  ;
    wire          [7:0]             cr_q_blk_0  ;
    wire          [7:0]             cr_q_blk_1  ;
    wire          [7:0]             cr_q_blk_2  ;
    wire          [7:0]             cr_q_blk_3  ;
    
    integer                         state;
    
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
    
    chroma_edge_filter 
		#(.CHR_ID(1'b0))
	cb_filter_submodule(
        .clk(clk),
        .reset(reset),
        .start(start_filters),
        .edge_type(edge_type_filter),
        .done(done_filter_cb),
        .q_block_1_in( { cb_q_block_1[3],
                         cb_q_block_1[2],
                         cb_q_block_1[1],
                         cb_q_block_1[0] }
            ),
        .q_block_2_in( { cb_q_block_2[3],
                         cb_q_block_2[2],
                         cb_q_block_2[1],
                         cb_q_block_2[0] }
            ),
        .q_block_3_in( { cb_q_block_3[3],
                         cb_q_block_3[2],
                         cb_q_block_3[1],
                         cb_q_block_3[0] }
            ),
        .q_block_4_in( { cb_q_block_4[3],
                         cb_q_block_4[2],
                         cb_q_block_4[1],
                         cb_q_block_4[0] }
            ),
        .A_block_in( {   cb_A_block[3],
                         cb_A_block[2],
                         cb_A_block[1],
                         cb_A_block[0] }
            ),
        .B_block_in( {   cb_B_block[3],
                         cb_B_block[2],
                         cb_B_block[1],
                         cb_B_block[0] }
            ),
        .C_block_in( {   cb_C_block[3],
                         cb_C_block[2],
                         cb_C_block[1],
                         cb_C_block[0] }
            ),
        .D_block_in( {   cb_D_block[3],
                         cb_D_block[2],
                         cb_D_block[1],
                         cb_D_block[0] }
            ),
        .E_block_in( {   cb_E_block[3],
                         cb_E_block[2],
                         cb_E_block[1],
                         cb_E_block[0] }
            ),    
        .qp_1(qp_1),
        .qp_A(qp_A),
        .qp_C(qp_C),
        .qp_D(qp_D),
        .pps_cb_qp_offset(pps_cb_qp_offset),
        .pps_cr_qp_offset(pps_cr_qp_offset),
        .slice_tc_offset(slice_tc_offset),
        .p_blk_0(cb_p_blk_0),
        .p_blk_1(cb_p_blk_1),
        .p_blk_2(cb_p_blk_2),
        .p_blk_3(cb_p_blk_3),
        .q_blk_0(cb_q_blk_0),
        .q_blk_1(cb_q_blk_1),
        .q_blk_2(cb_q_blk_2),
        .q_blk_3(cb_q_blk_3)
    );
    
    chroma_edge_filter 
		#(.CHR_ID(1'b1))
	cr_filter_submodule(
        .clk(clk),
        .reset(reset),
        .start(start_filters),
        .edge_type(edge_type_filter),
        .done(done_filter_cr),
        .q_block_1_in( { cr_q_block_1[3],
                         cr_q_block_1[2],
                         cr_q_block_1[1],
                         cr_q_block_1[0] }
            ),
        .q_block_2_in( { cr_q_block_2[3],
                         cr_q_block_2[2],
                         cr_q_block_2[1],
                         cr_q_block_2[0] }
            ),
        .q_block_3_in( { cr_q_block_3[3],
                         cr_q_block_3[2],
                         cr_q_block_3[1],
                         cr_q_block_3[0] }
            ),
        .q_block_4_in( { cr_q_block_4[3],
                         cr_q_block_4[2],
                         cr_q_block_4[1],
                         cr_q_block_4[0] }
            ),
        .A_block_in( {   cr_A_block[3],
                         cr_A_block[2],
                         cr_A_block[1],
                         cr_A_block[0] }
            ),
        .B_block_in( {   cr_B_block[3],
                         cr_B_block[2],
                         cr_B_block[1],
                         cr_B_block[0] }
            ),
        .C_block_in( {   cr_C_block[3],
                         cr_C_block[2],
                         cr_C_block[1],
                         cr_C_block[0] }
            ),
        .D_block_in( {   cr_D_block[3],
                         cr_D_block[2],
                         cr_D_block[1],
                         cr_D_block[0] }
            ),
        .E_block_in( {   cr_E_block[3],
                         cr_E_block[2],
                         cr_E_block[1],
                         cr_E_block[0] }
            ),    
        .qp_1(qp_1),
        .qp_A(qp_A),
        .qp_C(qp_C),
        .qp_D(qp_D),
        .pps_cb_qp_offset(pps_cb_qp_offset),
        .pps_cr_qp_offset(pps_cr_qp_offset),
        .slice_tc_offset(slice_tc_offset),
        .p_blk_0(cr_p_blk_0),
        .p_blk_1(cr_p_blk_1),
        .p_blk_2(cr_p_blk_2),
        .p_blk_3(cr_p_blk_3),
        .q_blk_0(cr_q_blk_0),
        .q_blk_1(cr_q_blk_1),
        .q_blk_2(cr_q_blk_2),
        .q_blk_3(cr_q_blk_3)
    );
    
    // Row buffer BRAMs
    BRAM_dual_port #(
		.ADDR_WIDTH	(10),
		.DATA_WIDTH	(SAMPLE_ROW_BUF_DATA_WIDTH),
		.RAM_DEPTH	(MAX_PIC_WIDTH/4)
	)
	cb_row_buffer(
        .clk(clk),
        .we_a(we_a),
        .we_b(we_b),
        .en_a(en_a),
        .en_b(en_b),
        .addr_a(addr_a),
        .addr_b(addr_b),
        .data_in_a(cb_data_in_a),
        .data_in_b(cb_data_in_b),
        .data_out_a(cb_data_out_a),
        .data_out_b(cb_data_out_b)
    );
    
    BRAM_dual_port #(
		.ADDR_WIDTH	(10),
		.DATA_WIDTH	(SAMPLE_ROW_BUF_DATA_WIDTH),
		.RAM_DEPTH	(MAX_PIC_WIDTH/4)
	) 
	cr_row_buffer(
        .clk(clk),
        .we_a(we_a),
        .we_b(we_b),
        .en_a(en_a),
        .en_b(en_b),
        .addr_a(addr_a),
        .addr_b(addr_b),
        .data_in_a(cr_data_in_a),
        .data_in_b(cr_data_in_b),
        .data_out_a(cr_data_out_a),
        .data_out_b(cr_data_out_b)
    );
    
    // Output assignments
    assign  cb_q_block_1_out  = ( { cb_q_block_1[3], cb_q_block_1[2], cb_q_block_1[1], cb_q_block_1[0] } ) ;
    assign  cb_q_block_2_out  = ( { cb_q_block_2[3], cb_q_block_2[2], cb_q_block_2[1], cb_q_block_2[0] } ) ;
    assign  cb_q_block_3_out  = ( { cb_q_block_3[3], cb_q_block_3[2], cb_q_block_3[1], cb_q_block_3[0] } ) ;
    assign  cb_q_block_4_out  = ( { cb_q_block_4[3], cb_q_block_4[2], cb_q_block_4[1], cb_q_block_4[0] } ) ;
    assign  cb_A_block_out    = ( { cb_A_block[3],   cb_A_block[2],   cb_A_block[1],   cb_A_block[0] } ) ;
    assign  cb_B_block_out    = ( { cb_B_block[3],   cb_B_block[2],   cb_B_block[1],   cb_B_block[0] } ) ;
    assign  cb_C_block_out    = ( { cb_C_block[3],   cb_C_block[2],   cb_C_block[1],   cb_C_block[0] } ) ;
    assign  cb_D_block_out    = ( { cb_D_block[3],   cb_D_block[2],   cb_D_block[1],   cb_D_block[0] } ) ;
    assign  cb_E_block_out    = ( { cb_E_block[3],   cb_E_block[2],   cb_E_block[1],   cb_E_block[0] } ) ;
    assign  cr_q_block_1_out  = ( { cr_q_block_1[3], cr_q_block_1[2], cr_q_block_1[1], cr_q_block_1[0] } ) ;
    assign  cr_q_block_2_out  = ( { cr_q_block_2[3], cr_q_block_2[2], cr_q_block_2[1], cr_q_block_2[0] } ) ;
    assign  cr_q_block_3_out  = ( { cr_q_block_3[3], cr_q_block_3[2], cr_q_block_3[1], cr_q_block_3[0] } ) ;
    assign  cr_q_block_4_out  = ( { cr_q_block_4[3], cr_q_block_4[2], cr_q_block_4[1], cr_q_block_4[0] } ) ;
    assign  cr_A_block_out    = ( { cr_A_block[3],   cr_A_block[2],   cr_A_block[1],   cr_A_block[0] } ) ;
    assign  cr_B_block_out    = ( { cr_B_block[3],   cr_B_block[2],   cr_B_block[1],   cr_B_block[0] } ) ;
    assign  cr_C_block_out    = ( { cr_C_block[3],   cr_C_block[2],   cr_C_block[1],   cr_C_block[0] } ) ;
    assign  cr_D_block_out    = ( { cr_D_block[3],   cr_D_block[2],   cr_D_block[1],   cr_D_block[0] } ) ;
    assign  cr_E_block_out    = ( { cr_E_block[3],   cr_E_block[2],   cr_E_block[1],   cr_E_block[0] } ) ;
    
    
    always @(posedge clk or posedge reset) begin
        if(reset) begin
            state <= STATE_INIT;
            done  <= 1'b0;
//            start_filters <= 1'b0;
//            en_a <= 1'b0;
//            en_b <= 1'b0;
        end
        else begin
            case (state)
                STATE_INIT : begin
                    if(start) begin
                        done  <= 1'b0;
						
                        // Assigning input CB pixels to registers
                        cb_q_block_1[0]  <=  cb_q_block_1_in[7:0];
                        cb_q_block_2[0]  <=  cb_q_block_2_in[7:0];
                        cb_q_block_3[0]  <=  cb_q_block_3_in[7:0];
                        cb_q_block_4[0]  <=  cb_q_block_4_in[7:0];
                        cb_A_block  [0]  <=  cb_A_block_in  [7:0];
                        cb_B_block  [0]  <=  cb_B_block_in  [7:0];
                        
                        cb_q_block_1[1]  <=  cb_q_block_1_in[15:8];
                        cb_q_block_2[1]  <=  cb_q_block_2_in[15:8];
                        cb_q_block_3[1]  <=  cb_q_block_3_in[15:8];
                        cb_q_block_4[1]  <=  cb_q_block_4_in[15:8];
                        cb_A_block  [1]  <=  cb_A_block_in  [15:8];
                        cb_B_block  [1]  <=  cb_B_block_in  [15:8];
                        
                        cb_q_block_1[2]  <=  cb_q_block_1_in[23:16];
                        cb_q_block_2[2]  <=  cb_q_block_2_in[23:16];
                        cb_q_block_3[2]  <=  cb_q_block_3_in[23:16];
                        cb_q_block_4[2]  <=  cb_q_block_4_in[23:16];
                        cb_A_block  [2]  <=  cb_A_block_in  [23:16];
                        cb_B_block  [2]  <=  cb_B_block_in  [23:16];
                        
                        cb_q_block_1[3]  <=  cb_q_block_1_in[31:24];
                        cb_q_block_2[3]  <=  cb_q_block_2_in[31:24];
                        cb_q_block_3[3]  <=  cb_q_block_3_in[31:24];
                        cb_q_block_4[3]  <=  cb_q_block_4_in[31:24];
                        cb_A_block  [3]  <=  cb_A_block_in  [31:24];
                        cb_B_block  [3]  <=  cb_B_block_in  [31:24];
                        
                        // Assigning input CR pixels to registers
                        cr_q_block_1[0]  <=  cr_q_block_1_in[7:0];
                        cr_q_block_2[0]  <=  cr_q_block_2_in[7:0];
                        cr_q_block_3[0]  <=  cr_q_block_3_in[7:0];
                        cr_q_block_4[0]  <=  cr_q_block_4_in[7:0];
                        cr_A_block  [0]  <=  cr_A_block_in  [7:0];
                        cr_B_block  [0]  <=  cr_B_block_in  [7:0];
                        
                        cr_q_block_1[1]  <=  cr_q_block_1_in[15:8];
                        cr_q_block_2[1]  <=  cr_q_block_2_in[15:8];
                        cr_q_block_3[1]  <=  cr_q_block_3_in[15:8];
                        cr_q_block_4[1]  <=  cr_q_block_4_in[15:8];
                        cr_A_block  [1]  <=  cr_A_block_in  [15:8];
                        cr_B_block  [1]  <=  cr_B_block_in  [15:8];
                        
                        cr_q_block_1[2]  <=  cr_q_block_1_in[23:16];
                        cr_q_block_2[2]  <=  cr_q_block_2_in[23:16];
                        cr_q_block_3[2]  <=  cr_q_block_3_in[23:16];
                        cr_q_block_4[2]  <=  cr_q_block_4_in[23:16];
                        cr_A_block  [2]  <=  cr_A_block_in  [23:16];
                        cr_B_block  [2]  <=  cr_B_block_in  [23:16];
                        
                        cr_q_block_1[3]  <=  cr_q_block_1_in[31:24];
                        cr_q_block_2[3]  <=  cr_q_block_2_in[31:24];
                        cr_q_block_3[3]  <=  cr_q_block_3_in[31:24];
                        cr_q_block_4[3]  <=  cr_q_block_4_in[31:24];
                        cr_A_block  [3]  <=  cr_A_block_in  [31:24];
                        cr_B_block  [3]  <=  cr_B_block_in  [31:24];
                        
                        state <= STATE_1;
                    end
                end
				
				STATE_1 : begin
					// Check for C,D blocks reading in combinational block
					if((BS_v1==2) && (Xc>0) && (Xc[3:0]==4'b0) && (ver_edge_dbf_disable_in==0)) begin
                        state <= STATE_1_WAIT;
                    end
                    else begin
                        state <= STATE_2;
                    end
				end
				STATE_1_WAIT : begin
					if(done_filter_cb & done_filter_cr) begin
						// Assign output samples
						if(pcm_bypass_AB_dbf_disable_in==0) begin
							cb_A_block[0]    <=    cb_p_blk_0 ;
							cb_A_block[1]    <=    cb_p_blk_1 ;
							cb_A_block[2]    <=    cb_p_blk_2 ;
							cb_A_block[3]    <=    cb_p_blk_3 ;
							cr_A_block[0]    <=    cr_p_blk_0 ;
							cr_A_block[1]    <=    cr_p_blk_1 ;
							cr_A_block[2]    <=    cr_p_blk_2 ;
							cr_A_block[3]    <=    cr_p_blk_3 ;
						end
						if(pcm_bypass_dbf_disable_in==0) begin
							cb_q_block_1[0]  <=    cb_q_blk_0 ;
							cb_q_block_1[1]  <=    cb_q_blk_1 ;
							cb_q_block_1[2]  <=    cb_q_blk_2 ;
							cb_q_block_1[3]  <=    cb_q_blk_3 ;
							cr_q_block_1[0]  <=    cr_q_blk_0 ;
							cr_q_block_1[1]  <=    cr_q_blk_1 ;
							cr_q_block_1[2]  <=    cr_q_blk_2 ;
							cr_q_block_1[3]  <=    cr_q_blk_3 ;
						end
						// Assign to C,D blocks from BRAM
						if((Xc>0) && (Yc>0)) begin
							cb_C_block[0]    <=    cb_data_out_a[7 :0 ];
							cb_C_block[1]    <=    cb_data_out_a[15:8 ];
							cb_C_block[2]    <=    cb_data_out_a[23:16];
							cb_C_block[3]    <=    cb_data_out_a[31:24];
							cr_C_block[0]    <=    cr_data_out_a[7 :0 ];
							cr_C_block[1]    <=    cr_data_out_a[15:8 ];
							cr_C_block[2]    <=    cr_data_out_a[23:16];
							cr_C_block[3]    <=    cr_data_out_a[31:24];
						end
						if(Yc>0) begin
							cb_D_block[0]    <=    cb_data_out_b[7 :0 ];
							cb_D_block[1]    <=    cb_data_out_b[15:8 ];
							cb_D_block[2]    <=    cb_data_out_b[23:16];
							cb_D_block[3]    <=    cb_data_out_b[31:24];
							cr_D_block[0]    <=    cr_data_out_b[7 :0 ];
							cr_D_block[1]    <=    cr_data_out_b[15:8 ];
							cr_D_block[2]    <=    cr_data_out_b[23:16];
							cr_D_block[3]    <=    cr_data_out_b[31:24];
						end
						// Check for E block reading in combinational block
						if((BS_v2==2) && (Xc>0) && (Xc[3:0]==4'b0) && (ver_edge_dbf_disable_in==0)) begin
							state <= STATE_2_WAIT;
						end
						else begin
							state <= STATE_3;
						end
					end
				end
				
				STATE_2 : begin
					// Assign to C,D blocks from BRAM
					if((Xc>0) && (Yc>0)) begin
                        cb_C_block[0]    <=    cb_data_out_a[7 :0 ];
                        cb_C_block[1]    <=    cb_data_out_a[15:8 ];
                        cb_C_block[2]    <=    cb_data_out_a[23:16];
                        cb_C_block[3]    <=    cb_data_out_a[31:24];
                        cr_C_block[0]    <=    cr_data_out_a[7 :0 ];
                        cr_C_block[1]    <=    cr_data_out_a[15:8 ];
                        cr_C_block[2]    <=    cr_data_out_a[23:16];
                        cr_C_block[3]    <=    cr_data_out_a[31:24];
                    end
                    if(Yc>0) begin
                        cb_D_block[0]    <=    cb_data_out_b[7 :0 ];
                        cb_D_block[1]    <=    cb_data_out_b[15:8 ];
                        cb_D_block[2]    <=    cb_data_out_b[23:16];
                        cb_D_block[3]    <=    cb_data_out_b[31:24];
                        cr_D_block[0]    <=    cr_data_out_b[7 :0 ];
                        cr_D_block[1]    <=    cr_data_out_b[15:8 ];
                        cr_D_block[2]    <=    cr_data_out_b[23:16];
                        cr_D_block[3]    <=    cr_data_out_b[31:24];
                    end
					// Check for E block reading in combinational block
					if((BS_v2==2) && (Xc>0) && (Xc[3:0]==4'b0) && (ver_edge_dbf_disable_in==0)) begin
                        state <= STATE_2_WAIT;
                    end
                    else begin
                        state <= STATE_3;
                    end
				end
				STATE_2_WAIT : begin
					if(done_filter_cb & done_filter_cr) begin
						// Assign output samples
						if(pcm_bypass_AB_dbf_disable_in==0) begin
							cb_B_block[0]    <=    cb_p_blk_0 ;
							cb_B_block[1]    <=    cb_p_blk_1 ;
							cb_B_block[2]    <=    cb_p_blk_2 ;
							cb_B_block[3]    <=    cb_p_blk_3 ;
							cr_B_block[0]    <=    cr_p_blk_0 ;
							cr_B_block[1]    <=    cr_p_blk_1 ;
							cr_B_block[2]    <=    cr_p_blk_2 ;
							cr_B_block[3]    <=    cr_p_blk_3 ;
						end
						if(pcm_bypass_dbf_disable_in==0) begin
							cb_q_block_3[0]  <=    cb_q_blk_0 ;
							cb_q_block_3[1]  <=    cb_q_blk_1 ;
							cb_q_block_3[2]  <=    cb_q_blk_2 ;
							cb_q_block_3[3]  <=    cb_q_blk_3 ;
							cr_q_block_3[0]  <=    cr_q_blk_0 ;
							cr_q_block_3[1]  <=    cr_q_blk_1 ;
							cr_q_block_3[2]  <=    cr_q_blk_2 ;
							cr_q_block_3[3]  <=    cr_q_blk_3 ;
						end
						// Assign to E block from BRAM if necessary
						if((Yc>0) && (Xc ==(pic_width_in - 4'd8))) begin
							cb_E_block[0]    <=    cb_data_out_a[7 :0 ];
							cb_E_block[1]    <=    cb_data_out_a[15:8 ];
							cb_E_block[2]    <=    cb_data_out_a[23:16];
							cb_E_block[3]    <=    cb_data_out_a[31:24];
							cr_E_block[0]    <=    cr_data_out_a[7 :0 ];
							cr_E_block[1]    <=    cr_data_out_a[15:8 ];
							cr_E_block[2]    <=    cr_data_out_a[23:16];
							cr_E_block[3]    <=    cr_data_out_a[31:24];
						end
						// Write B,3 blocks(C,D) to BRAM in combinational block
						if((BS_h0==2) && (Yc>0) && (Yc[3:0]==4'b0) && (h0_edge_dbf_disable_in==0)) begin
							state <= STATE_3_WAIT;
						end
						else begin
							state <= STATE_4;
						end
					end
				end
				
				STATE_3 : begin
					// Assign to E block from BRAM if necessary
					if((Yc>0) && (Xc ==(pic_width_in - 4'd8))) begin
                        cb_E_block[0]    <=    cb_data_out_a[7 :0 ];
                        cb_E_block[1]    <=    cb_data_out_a[15:8 ];
                        cb_E_block[2]    <=    cb_data_out_a[23:16];
                        cb_E_block[3]    <=    cb_data_out_a[31:24];
                        cr_E_block[0]    <=    cr_data_out_a[7 :0 ];
                        cr_E_block[1]    <=    cr_data_out_a[15:8 ];
                        cr_E_block[2]    <=    cr_data_out_a[23:16];
                        cr_E_block[3]    <=    cr_data_out_a[31:24];
                    end
					// Write B,3 blocks(C,D) to BRAM in combinational block
					if((BS_h0==2) && (Yc>0) && (Yc[3:0]==4'b0) && (h0_edge_dbf_disable_in==0)) begin
                        state <= STATE_3_WAIT;
                    end
                    else begin
                        state <= STATE_4;
                    end
				end
				STATE_3_WAIT : begin
					if(done_filter_cb & done_filter_cr) begin
						// Assign output samples
						if(pcm_bypass_C_dbf_disable_in==0) begin
							cb_C_block[0]    <=    cb_p_blk_0 ;
							cb_C_block[1]    <=    cb_p_blk_1 ;
							cb_C_block[2]    <=    cb_p_blk_2 ;
							cb_C_block[3]    <=    cb_p_blk_3 ;
							cr_C_block[0]    <=    cr_p_blk_0 ;
							cr_C_block[1]    <=    cr_p_blk_1 ;
							cr_C_block[2]    <=    cr_p_blk_2 ;
							cr_C_block[3]    <=    cr_p_blk_3 ;
						end
						if(pcm_bypass_AB_dbf_disable_in==0) begin
							cb_A_block[0]    <=    cb_q_blk_0 ;
							cb_A_block[1]    <=    cb_q_blk_1 ;
							cb_A_block[2]    <=    cb_q_blk_2 ;
							cb_A_block[3]    <=    cb_q_blk_3 ;
							cr_A_block[0]    <=    cr_q_blk_0 ;
							cr_A_block[1]    <=    cr_q_blk_1 ;
							cr_A_block[2]    <=    cr_q_blk_2 ;
							cr_A_block[3]    <=    cr_q_blk_3 ;
						end
						// Write 4 block(E) to BRAM in combinational block
						if((BS_h1==2) && (Yc>0) && (Yc[3:0]==4'b0) && (hor_edge_dbf_disable_in==0)) begin
							state <= STATE_4_WAIT;
						end
						else begin
							state <= STATE_5;
						end
					end
				end
				
				STATE_4 : begin
					// Write 4 block(E) to BRAM in combinational block
					if((BS_h1==2) && (Yc>0) && (Yc[3:0]==4'b0) && (hor_edge_dbf_disable_in==0)) begin
                        state <= STATE_4_WAIT;
                    end
                    else begin
                        state <= STATE_5;
                    end
				end
				STATE_4_WAIT : begin
					if(done_filter_cb & done_filter_cr) begin
						// Assign output samples
						if(pcm_bypass_D_dbf_disable_in==0) begin
							cb_D_block[0]    <=    cb_p_blk_0 ;
							cb_D_block[1]    <=    cb_p_blk_1 ;
							cb_D_block[2]    <=    cb_p_blk_2 ;
							cb_D_block[3]    <=    cb_p_blk_3 ;
							cr_D_block[0]    <=    cr_p_blk_0 ;
							cr_D_block[1]    <=    cr_p_blk_1 ;
							cr_D_block[2]    <=    cr_p_blk_2 ;
							cr_D_block[3]    <=    cr_p_blk_3 ;
						end
						if(pcm_bypass_dbf_disable_in==0) begin
							cb_q_block_1[0]  <=    cb_q_blk_0 ;
							cb_q_block_1[1]  <=    cb_q_blk_1 ;
							cb_q_block_1[2]  <=    cb_q_blk_2 ;
							cb_q_block_1[3]  <=    cb_q_blk_3 ;
							cr_q_block_1[0]  <=    cr_q_blk_0 ;
							cr_q_block_1[1]  <=    cr_q_blk_1 ;
							cr_q_block_1[2]  <=    cr_q_blk_2 ;
							cr_q_block_1[3]  <=    cr_q_blk_3 ;
						end
						// Check for edge h2 filtering
						if((BS_h2==2) && (Yc>0) && (Yc[3:0]==4'b0) && (Xc==(pic_width_in-4'd8)) && (hor_edge_dbf_disable_in==0)) begin
							state <= STATE_5_WAIT;
						end
						else begin
							state <= STATE_INIT;
							done  <= 1'b1;
						end
					end
				end
				
				STATE_5 : begin
					if((BS_h2==2) && (Yc>0) && (Yc[3:0]==4'b0) && (Xc==(pic_width_in-4'd8)) && (hor_edge_dbf_disable_in==0)) begin
                        state <= STATE_5_WAIT;
                    end
                    else begin
                        state <= STATE_INIT;
                        done  <= 1'b1;
                    end
				end
				STATE_5_WAIT : begin
					if(done_filter_cb & done_filter_cr) begin
						// Assign outputs to registers
						if(pcm_bypass_D_dbf_disable_in==0) begin
							cb_E_block[0]    <=    cb_p_blk_0 ;
							cb_E_block[1]    <=    cb_p_blk_1 ;
							cb_E_block[2]    <=    cb_p_blk_2 ;
							cb_E_block[3]    <=    cb_p_blk_3 ;
							cr_E_block[0]    <=    cr_p_blk_0 ;
							cr_E_block[1]    <=    cr_p_blk_1 ;
							cr_E_block[2]    <=    cr_p_blk_2 ;
							cr_E_block[3]    <=    cr_p_blk_3 ;
						end
						if(pcm_bypass_dbf_disable_in==0) begin
							cb_q_block_2[0]  <=    cb_q_blk_0 ;
							cb_q_block_2[1]  <=    cb_q_blk_1 ;
							cb_q_block_2[2]  <=    cb_q_blk_2 ;
							cb_q_block_2[3]  <=    cb_q_blk_3 ;
							cr_q_block_2[0]  <=    cr_q_blk_0 ;
							cr_q_block_2[1]  <=    cr_q_blk_1 ;
							cr_q_block_2[2]  <=    cr_q_blk_2 ;
							cr_q_block_2[3]  <=    cr_q_blk_3 ;
						end
						state <= STATE_INIT;
                        done  <= 1'b1;
					end
				end
				
            endcase
        end
    end
	
	
	//------------- Combinational block to read and write to BRAM ----------------------
	always @(*) begin
		we_a  			=	1'b0 ;
		we_b            =	1'b0 ;
		en_a            =	1'b0 ;
		en_b            =	1'b0 ;
		addr_a          =	{10{1'bx}} ;
		addr_b          =	{10{1'bx}} ;
		cb_data_in_a    =	{SAMPLE_ROW_BUF_DATA_WIDTH{1'bx}} ;
		cb_data_in_b    =	{SAMPLE_ROW_BUF_DATA_WIDTH{1'bx}} ;
		cr_data_in_a    =	{SAMPLE_ROW_BUF_DATA_WIDTH{1'bx}} ;
		cr_data_in_b    =	{SAMPLE_ROW_BUF_DATA_WIDTH{1'bx}} ;
		case(state)
			STATE_1 : begin
				if((Xc>0) && (Yc>0)) begin
                    we_a 	=  1'b0 ;
                    en_a 	=  1'b1 ;
                    addr_a 	=  (Xc[11:2] - 1'b1) ;
                end
                if(Yc>0) begin
                    we_b 	=  1'b0 ;
                    en_b 	=  1'b1 ;
                    addr_b 	=  Xc[11:2] ;
                end
			end
			STATE_1_WAIT : begin
				if(done_filter_cb & done_filter_cr) begin
					if((Yc>0) && (Xc ==(pic_width_in - 4'd8))) begin
						we_a 	=  1'b0 ;
						en_a 	=  1'b1 ;
						addr_a 	=  (Xc[11:2] + 1'b1) ;
					end
				end
				else begin
					if((Xc>0) && (Yc>0)) begin
						we_a 	=  1'b0 ;
						en_a 	=  1'b1 ;
						addr_a 	=  (Xc[11:2] - 1'b1) ;
					end
					if(Yc>0) begin
						we_b 	=  1'b0 ;
						en_b 	=  1'b1 ;
						addr_b 	=  Xc[11:2] ;
					end
				end
			end
			STATE_2 : begin
				if((Yc>0) && (Xc ==(pic_width_in - 4'd8))) begin
                    we_a 	=  1'b0 ;
                    en_a 	=  1'b1 ;
                    addr_a 	=  (Xc[11:2] + 1'b1) ;
                end
			end
			STATE_2_WAIT : begin
				if(done_filter_cb & done_filter_cr) begin
					if(Xc>0) begin
						we_a 	=  1'b1 ;
						en_a 	=  1'b1 ;
						addr_a 	=  (Xc[11:2] - 1'b1) ;
						cb_data_in_a  = ( { cb_p_blk_3 ,
											cb_p_blk_2 ,
											cb_p_blk_1 ,
											cb_p_blk_0  
											} );
						cr_data_in_a  = ( { cr_p_blk_3 ,
											cr_p_blk_2 ,
											cr_p_blk_1 ,
											cr_p_blk_0  
											} );                    
					end
					we_b 	=  1'b1 ;
					en_b 	=  1'b1 ;
					addr_b 	=  Xc[11:2] ;
					cb_data_in_b  = ( { cb_q_blk_3 ,
										cb_q_blk_2 ,
										cb_q_blk_1 ,
										cb_q_blk_0  
										} );
					cr_data_in_b  = ( { cr_q_blk_3 ,
										cr_q_blk_2 ,
										cr_q_blk_1 ,
										cr_q_blk_0  
										} );      
				end
				else begin
					if((Yc>0) && (Xc ==(pic_width_in - 4'd8))) begin
						we_a 	=  1'b0 ;
						en_a 	=  1'b1 ;
						addr_a 	=  (Xc[11:2] + 1'b1) ;
					end
				end
			end
			STATE_3 : begin
				if(Xc>0) begin
                    we_a 	=  1'b1 ;
                    en_a 	=  1'b1 ;
                    addr_a 	=  (Xc[11:2] - 1'b1) ;
                    cb_data_in_a  = ( { cb_B_block[ 3] ,
                                        cb_B_block[ 2] ,
                                        cb_B_block[ 1] ,
                                        cb_B_block[ 0]  
                                        } );
                    cr_data_in_a  = ( { cr_B_block[ 3] ,
                                        cr_B_block[ 2] ,
                                        cr_B_block[ 1] ,
                                        cr_B_block[ 0]  
                                        } );                    
                end
                we_b 	=  1'b1 ;
                en_b 	=  1'b1 ;
                addr_b 	=  Xc[11:2] ;
                cb_data_in_b  = ( { cb_q_block_3[ 3] ,
                                    cb_q_block_3[ 2] ,
                                    cb_q_block_3[ 1] ,
                                    cb_q_block_3[ 0]  
                                    } );
                cr_data_in_b  = ( { cr_q_block_3[ 3] ,
                                    cr_q_block_3[ 2] ,
                                    cr_q_block_3[ 1] ,
                                    cr_q_block_3[ 0]  
                                    } );      
			end
			STATE_3_WAIT : begin
				if(done_filter_cb & done_filter_cr) begin
					if(Xc ==(pic_width_in - 4'd8)) begin
						we_a 	=  1'b1 ;
						en_a 	=  1'b1 ;
						addr_a 	=  (Xc[11:2] + 1'b1) ;
						cb_data_in_a  = ( { cb_q_block_4[ 3] ,
											cb_q_block_4[ 2] ,
											cb_q_block_4[ 1] ,
											cb_q_block_4[ 0]  
											} );
						cr_data_in_a  = ( { cr_q_block_4[ 3] ,
											cr_q_block_4[ 2] ,
											cr_q_block_4[ 1] ,
											cr_q_block_4[ 0]  
											} );  
					end
				end
			end
			STATE_4 : begin
				if(Xc ==(pic_width_in - 4'd8)) begin
                    we_a 	=  1'b1 ;
                    en_a 	=  1'b1 ;
                    addr_a 	=  (Xc[11:2] + 1'b1) ;
                    cb_data_in_a  = ( { cb_q_block_4[ 3] ,
                                        cb_q_block_4[ 2] ,
                                        cb_q_block_4[ 1] ,
                                        cb_q_block_4[ 0]  
                                        } );
                    cr_data_in_a  = ( { cr_q_block_4[ 3] ,
                                        cr_q_block_4[ 2] ,
                                        cr_q_block_4[ 1] ,
                                        cr_q_block_4[ 0]  
                                        } );  
                end
			end
		endcase
	end
	
	//-------------- Combinational block to activate filter module ----------------------
	always @(*) begin
		start_filters 	  =  1'b0 ;
		edge_type_filter  =  3'bxxx ;
		case(state)
			STATE_1 : begin
				if((BS_v1==2) && (Xc>0) && (Xc[3:0]==4'b0) && (ver_edge_dbf_disable_in==0)) begin
                    start_filters 	  =  1'b1 ;
					edge_type_filter  =  VERT_UP ;
                end
			end
			STATE_1_WAIT : begin
				if(done_filter_cb & done_filter_cr) begin
					if((BS_v2==2) && (Xc>0) && (Xc[3:0]==4'b0) && (ver_edge_dbf_disable_in==0)) begin
						start_filters 	  =  1'b1 ;
						edge_type_filter  =  VERT_DOWN ;
					end
				end
				else begin
					edge_type_filter  =  VERT_UP ;
				end
			end
			STATE_2 : begin
				if((BS_v2==2) && (Xc>0) && (Xc[3:0]==4'b0) && (ver_edge_dbf_disable_in==0)) begin
                    start_filters 	  =  1'b1 ;
					edge_type_filter  =  VERT_DOWN ;
                end
			end
			STATE_2_WAIT : begin
				if(done_filter_cb & done_filter_cr) begin
					if((BS_h0==2) && (Yc>0) && (Yc[3:0]==4'b0) && (h0_edge_dbf_disable_in==0)) begin
						start_filters 	  =  1'b1 ;
						edge_type_filter  =  HOR_LEFT ;
					end
				end
				else begin
					edge_type_filter  =  VERT_DOWN ;
				end
			end
			STATE_3 : begin
				if((BS_h0==2) && (Yc>0) && (Yc[3:0]==4'b0) && (h0_edge_dbf_disable_in==0)) begin
                    start_filters 	  =  1'b1 ;
					edge_type_filter  =  HOR_LEFT ;
                end
			end
			STATE_3_WAIT : begin
				if(done_filter_cb & done_filter_cr) begin
					if((BS_h1==2) && (Yc>0) && (Yc[3:0]==4'b0) && (hor_edge_dbf_disable_in==0)) begin
						start_filters 	  =  1'b1 ;
						edge_type_filter  =  HOR_RIGHT ;
					end
				end
				else begin
					edge_type_filter  =  HOR_LEFT ;
				end
			end
			STATE_4 : begin
				if((BS_h1==2) && (Yc>0) && (Yc[3:0]==4'b0) && (hor_edge_dbf_disable_in==0)) begin
                    start_filters 	  =  1'b1 ;
					edge_type_filter  =  HOR_RIGHT ;
                end
			end
			STATE_4_WAIT : begin
				if(done_filter_cb & done_filter_cr) begin
					if((BS_h2==2) && (Yc>0) && (Yc[3:0]==4'b0) && (Xc==(pic_width_in-4'd8)) && (hor_edge_dbf_disable_in==0)) begin
						start_filters 	  =  1'b1 ;
						edge_type_filter  =  HOR_FAR_RIGHT ;
					end
				end
				else begin
					edge_type_filter  =  HOR_RIGHT ;
				end
			end
			STATE_5 : begin
				if((BS_h2==2) && (Yc>0) && (Yc[3:0]==4'b0) && (Xc==(pic_width_in-4'd8)) && (hor_edge_dbf_disable_in==0)) begin
                    start_filters 	  =  1'b1 ;
					edge_type_filter  =  HOR_FAR_RIGHT ;
                end
			end
			STATE_5_WAIT : begin
				edge_type_filter  =  HOR_FAR_RIGHT ;
			end
		endcase
	end


endmodule
