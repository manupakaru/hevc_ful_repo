`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:33:56 07/11/2014 
// Design Name: 
// Module Name:    BRAM_dual_port 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module BRAM_dual_port(
		clk,
        we_a,
        we_b,
        en_a,
        en_b,
        addr_a,
        addr_b,
        data_in_a,
        data_in_b,
        data_out_a,
        data_out_b
    );
	
	parameter		ADDR_WIDTH	=	8 ;
	parameter		DATA_WIDTH	=	256 ;
	parameter		RAM_DEPTH	=	128 ;
	
	input                       			clk;
    input                       			we_a;
    input                       			we_b;
    input                       			en_a;
    input                       			en_b;
    input    		[ADDR_WIDTH-1:0]       	addr_a;
    input    		[ADDR_WIDTH-1:0]       	addr_b;
    input         	[DATA_WIDTH-1:0]     	data_in_a;
    input         	[DATA_WIDTH-1:0]     	data_in_b;
    output reg    	[DATA_WIDTH-1:0]     	data_out_a;
    output reg    	[DATA_WIDTH-1:0]     	data_out_b;
    
    reg             [DATA_WIDTH-1:0]    	RAM [RAM_DEPTH-1:0];
    
    always @(posedge clk) begin
        if(en_a) begin
            if(we_a) begin
                RAM[addr_a] <= data_in_a;
            end
            else begin
                data_out_a <= RAM[addr_a];
            end
        end
    end
    always @(posedge clk) begin
        if(en_b) begin
            if(we_b) begin
                RAM[addr_b] <= data_in_b;
            end
            else begin
                data_out_b <= RAM[addr_b];
            end
        end
    end


endmodule
