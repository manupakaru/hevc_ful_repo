`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:59:23 11/23/2013 
// Design Name: 
// Module Name:    main_db_stage 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module main_db_stage(
        clk,
        reset,
        data_valid_in,
        ready_out,
        ready_in,
        valid_out,
        y_q_block_1_in ,
        y_q_block_2_in ,
        y_q_block_3_in ,
        y_q_block_4_in ,
        y_A_block_in   ,
        y_B_block_in   ,
        cb_q_block_1_in,
        cb_q_block_2_in,
        cb_q_block_3_in,
        cb_q_block_4_in,
        cb_A_block_in  ,
        cb_B_block_in  ,
        cr_q_block_1_in,
        cr_q_block_2_in,
        cr_q_block_3_in,
        cr_q_block_4_in,
        cr_A_block_in  ,
        cr_B_block_in  ,
        Xc_in,
        Yc_in,
        qp_1_in,
        qp_A_in,
        BS_v1_in,
        BS_v2_in,
        BS_h1_in,
        BS_h2_in,
        BS_h0_in,
        pps_cb_qp_offset_in,
        pps_cr_qp_offset_in,
        slice_tc_offset_div2_in,
        slice_beta_offset_div2_in,
        pic_width_in,
        pic_height_in,
		ctb_size_in,
		log2_ctb_size_in,
		tile_id_in,
		slice_id_in,
		h0_edge_dbf_disable_in,
		pcm_bypass_AB_dbf_disable_in,
		
		slice_dbf_disable_in,
		pcm_loop_filter_disabled_in,
		loop_filter_accross_tiles_enabled_in,
		loop_filter_accross_slices_enabled_in,
		tile_x_in  ,
		tile_y_in  ,
		slice_x_in ,
		slice_y_in ,
		actual_slice_x_in ,
		actual_slice_y_in ,
    
		sao_luma_in,
		sao_chroma_in,
		pcm_in,
		bypass_in,
	
        y_sao_type_in,
        y_bandpos_or_eoclass_in,
        y_sao_offset_1_in,
        y_sao_offset_2_in,
        y_sao_offset_3_in,
        y_sao_offset_4_in,
        cb_sao_type_in,
        cb_bandpos_or_eoclass_in,
        cb_sao_offset_1_in,
        cb_sao_offset_2_in,
        cb_sao_offset_3_in,
        cb_sao_offset_4_in,
        cr_sao_type_in,
        cr_bandpos_or_eoclass_in,
        cr_sao_offset_1_in,
        cr_sao_offset_2_in,
        cr_sao_offset_3_in,
        cr_sao_offset_4_in,
		

		poc_in, 
		current_dpb_idx_in,
		
		poc_out, 
		current_dpb_idx_out,
		
		Xc_out,
		Yc_out,
		pic_width_out,
		pic_height_out,
		ctb_size_out,
		tile_id_out,
		slice_id_out,
		
		log2_ctb_size_out,
		pcm_bypass_dbf_disable,
		pcm_loop_filter_disabled_out,
		loop_filter_accross_tiles_enabled_out,
		loop_filter_accross_slices_enabled_out,
		tile_left_boundary ,
		tile_top_boundary  ,
		slice_left_boundary,
		slice_top_boundary ,
		tile_x_out  ,
		tile_y_out  ,
		slice_x_out ,
		slice_y_out ,
		actual_slice_x_out ,
		actual_slice_y_out ,
		
		sao_luma_out,
		sao_chroma_out,
		pcm_out,
		bypass_out,
		y_sao_type_out,
		y_bandpos_or_eoclass_out,
		y_sao_offset_1_out,
		y_sao_offset_2_out,
		y_sao_offset_3_out,
		y_sao_offset_4_out,
		cb_sao_type_out,
		cb_bandpos_or_eoclass_out,
		cb_sao_offset_1_out,
		cb_sao_offset_2_out,
		cb_sao_offset_3_out,
		cb_sao_offset_4_out,
		cr_sao_type_out,
		cr_bandpos_or_eoclass_out,
		cr_sao_offset_1_out,
		cr_sao_offset_2_out,
		cr_sao_offset_3_out,
		cr_sao_offset_4_out,		
		
        y_q_block_1_out ,
        y_q_block_2_out ,
        y_q_block_3_out ,
        y_q_block_4_out ,
        y_A_block_out   ,
        y_B_block_out   ,
        y_C_block_out   ,
        y_D_block_out   ,
        y_E_block_out   ,
        cb_q_block_1_out,
        cb_q_block_2_out,
        cb_q_block_3_out,
        cb_q_block_4_out,
        cb_A_block_out  ,
        cb_B_block_out  ,
        cb_C_block_out  ,
        cb_D_block_out  ,
        cb_E_block_out  ,
        cr_q_block_1_out,
        cr_q_block_2_out,
        cr_q_block_3_out,
        cr_q_block_4_out,
        cr_A_block_out  ,
        cr_B_block_out  ,
        cr_C_block_out  ,
        cr_D_block_out  ,
        cr_E_block_out      // Some more outputs should be added (Xc,Yc..)
		
	
		,a_block_in_data		
		,a_block_in_valid	
		,a_block_out_valid	
		,luma_db_state	
		,a_block_out_data	
		
		
    );

	
    `include "../sim/pred_def.v"
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------


    localparam                          VERT_UP             = 3'b000;
    localparam                          VERT_DOWN           = 3'b001;
    localparam                          HOR_LEFT            = 3'b010;
    localparam                          HOR_RIGHT           = 3'b011;
    localparam                          HOR_FAR_RIGHT       = 3'b100;
    localparam                          CB                  = 0     ;
    localparam                          CR                  = 1     ;
    // localparam                          MAX_PIC_WIDTH       = 1920  ;
    // localparam                          MAX_PIC_HEIGHT      = 1080  ;
    
    localparam                          STATE_INIT          = 0;    // the state that is entered upon reset
    localparam                          STATE_1             = 1;
    localparam                          STATE_2             = 2;
    localparam                          STATE_3             = 3;
    localparam                          STATE_4             = 4;
    localparam                          STATE_5             = 5;
    localparam                          STATE_END           = 6;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input                       clk;
    input                       reset;
    
    // handshake signals to and from the Read Data stage
(* MARK_DEBUG *)    input                	    data_valid_in;	    // connected to valid out of the Read Data stage
(* MARK_DEBUG *)    output  reg                 ready_out;          // connected to ready in of the Read Data stage
    
    // handshake signals to and from the stage below
(* MARK_DEBUG *)    input                       ready_in;           // connected to ready out of the stage below
(* MARK_DEBUG *)    output	reg					valid_out;          // connected to data valid in of the stage below
    
    // Inputs from the Read Data stage
    
    input       	[127:0]     y_q_block_1_in ;
    input       	[127:0]     y_q_block_2_in ;
    input       	[127:0]     y_q_block_3_in ;
    input       	[127:0]     y_q_block_4_in ;
    input       	[127:0]     y_A_block_in   ;
    input       	[127:0]     y_B_block_in   ;
    
    input       	[31:0]      cb_q_block_1_in ;
    input       	[31:0]      cb_q_block_2_in ;
    input       	[31:0]      cb_q_block_3_in ;
    input       	[31:0]      cb_q_block_4_in ;
    input       	[31:0]      cb_A_block_in   ;
    input       	[31:0]      cb_B_block_in   ;
    
    input       	[31:0]      cr_q_block_1_in ;
    input       	[31:0]      cr_q_block_2_in ;
    input       	[31:0]      cr_q_block_3_in ;
    input       	[31:0]      cr_q_block_4_in ;
    input       	[31:0]      cr_A_block_in   ;
    input       	[31:0]      cr_B_block_in   ;
    
    input           [X_ADDR_WDTH -1:LOG2_MIN_DU_SIZE]         		Xc_in;
    input           [X_ADDR_WDTH -1:LOG2_MIN_DU_SIZE]         		Yc_in;
    input			[QP_WIDTH-1:0]									qp_1_in;
	input			[QP_WIDTH-1:0]									qp_A_in;
	input           [BS_WIDTH-1:0]          						BS_v1_in;
    input           [BS_WIDTH-1:0]          						BS_v2_in;
    input           [BS_WIDTH-1:0]          						BS_h1_in;
    input           [BS_WIDTH-1:0]          						BS_h2_in;
    input           [BS_WIDTH-1:0]          						BS_h0_in;  
	input			[PPS_CB_QP_OFFSET_WIDTH-1:0]					pps_cb_qp_offset_in;		//---- ATTN - 5 bit signed 2's comp---------
    input			[PPS_CR_QP_OFFSET_WIDTH-1:0]					pps_cr_qp_offset_in;		//---- ATTN - 5 bit signed 2's comp---------
	input			[PPS_TC_OFFSET_DIV2_WIDTH-1:0]					slice_tc_offset_div2_in;	//---- ATTN - 4 bit signed 2's comp---------
    input           [PPS_BETA_OFFSET_DIV2_WIDTH-1:0]				slice_beta_offset_div2_in;	//---- ATTN - 4 bit signed 2's comp---------
    input           [PIC_DIM_WIDTH-1:0]                    			pic_width_in;
    input           [PIC_DIM_WIDTH-1:0]                    			pic_height_in;
    input           [CTB_SIZE_WIDTH :0]                   			ctb_size_in;
    input			[LOG2_CTB_WIDTH:0]    							log2_ctb_size_in;
    input           [7:0]				    						tile_id_in;
    input           [7:0]				    						slice_id_in;
    input       	[31:0]                  						poc_in;
	input     		[DPB_ADDR_WIDTH-1:0]   							current_dpb_idx_in;
	input													h0_edge_dbf_disable_in;
	input													pcm_bypass_AB_dbf_disable_in;
    
	output reg       [31:0]                  	poc_out;
	output reg     	[DPB_ADDR_WIDTH-1:0]   		current_dpb_idx_out;
    
	input													slice_dbf_disable_in;
	input													pcm_loop_filter_disabled_in;
	input													loop_filter_accross_tiles_enabled_in;
	input													loop_filter_accross_slices_enabled_in;
	input			[XC_WIDTH-1:0]							tile_x_in  ;
	input			[XC_WIDTH-1:0]							tile_y_in  ;
	input			[XC_WIDTH-1:0]							slice_x_in ;
	input			[XC_WIDTH-1:0]							slice_y_in ;
	input			[XC_WIDTH-1:0]							actual_slice_x_in ;
	input			[XC_WIDTH-1:0]							actual_slice_y_in ;
	
    input                              	    				sao_luma_in;
    input                              	    				sao_chroma_in;
    input                              	    				pcm_in;
    input                              	    				bypass_in;
    input           [SAOTYPE_WIDTH -1:0]                  	y_sao_type_in;
    input           [BANDPOS_EO_WIDTH -1:0]               	y_bandpos_or_eoclass_in;
    input           [SAO_OFFSET_WIDTH -1:0]               	y_sao_offset_1_in;		//---- ATTN - 8 bit signed 2's comp---------
    input           [SAO_OFFSET_WIDTH -1:0]               	y_sao_offset_2_in;      //---- ATTN - 8 bit signed 2's comp---------
    input           [SAO_OFFSET_WIDTH -1:0]               	y_sao_offset_3_in;      //---- ATTN - 8 bit signed 2's comp---------
    input           [SAO_OFFSET_WIDTH -1:0]               	y_sao_offset_4_in;      //---- ATTN - 8 bit signed 2's comp---------
    input           [SAOTYPE_WIDTH -1:0]                  	cb_sao_type_in;
    input           [BANDPOS_EO_WIDTH -1:0]               	cb_bandpos_or_eoclass_in;
    input           [SAO_OFFSET_WIDTH -1:0]               	cb_sao_offset_1_in;		//---- ATTN - 8 bit signed 2's comp---------
    input           [SAO_OFFSET_WIDTH -1:0]               	cb_sao_offset_2_in;     //---- ATTN - 8 bit signed 2's comp---------
    input           [SAO_OFFSET_WIDTH -1:0]               	cb_sao_offset_3_in;     //---- ATTN - 8 bit signed 2's comp---------
    input           [SAO_OFFSET_WIDTH -1:0]               	cb_sao_offset_4_in;     //---- ATTN - 8 bit signed 2's comp---------
    input           [SAOTYPE_WIDTH -1:0]                  	cr_sao_type_in;
    input           [BANDPOS_EO_WIDTH -1:0]               	cr_bandpos_or_eoclass_in;
    input           [SAO_OFFSET_WIDTH -1:0]               	cr_sao_offset_1_in;		//---- ATTN - 8 bit signed 2's comp---------
    input           [SAO_OFFSET_WIDTH -1:0]               	cr_sao_offset_2_in;     //---- ATTN - 8 bit signed 2's comp---------
    input           [SAO_OFFSET_WIDTH -1:0]               	cr_sao_offset_3_in;     //---- ATTN - 8 bit signed 2's comp---------
    input           [SAO_OFFSET_WIDTH -1:0]               	cr_sao_offset_4_in;     //---- ATTN - 8 bit signed 2's comp---------
    
    
    // Outputs to stage below
    
    output          [127:0]         y_q_block_1_out;
    output          [127:0]         y_q_block_2_out;
    output          [127:0]         y_q_block_3_out;
    output          [127:0]         y_q_block_4_out;
    output          [127:0]         y_A_block_out  ;
    output          [127:0]         y_B_block_out  ;
    output          [127:0]         y_C_block_out  ;
    output          [127:0]         y_D_block_out  ;
    output          [127:0]         y_E_block_out  ;
    
    output          [31:0]          cb_q_block_1_out;
    output          [31:0]          cb_q_block_2_out;
    output          [31:0]          cb_q_block_3_out;
    output          [31:0]          cb_q_block_4_out;
    output          [31:0]          cb_A_block_out  ;
    output          [31:0]          cb_B_block_out  ;
    output          [31:0]          cb_C_block_out  ;
    output          [31:0]          cb_D_block_out  ;
    output          [31:0]          cb_E_block_out  ;
    
    output          [31:0]          cr_q_block_1_out;
    output          [31:0]          cr_q_block_2_out;
    output          [31:0]          cr_q_block_3_out;
    output          [31:0]          cr_q_block_4_out;
    output          [31:0]          cr_A_block_out  ;
    output          [31:0]          cr_B_block_out  ;
    output          [31:0]          cr_C_block_out  ;
    output          [31:0]          cr_D_block_out  ;
    output          [31:0]          cr_E_block_out  ;
    
    output  reg     [11:0]                  Xc_out;
    output  reg     [11:0]                  Yc_out;
    output  reg     [11:0]                  pic_width_out;
    output  reg     [11:0]                  pic_height_out;
    output  reg     [CTB_SIZE_WIDTH:0]    	ctb_size_out;
    output  reg     [LOG2_CTB_WIDTH:0]    	log2_ctb_size_out;
    output  reg     [7:0]				    tile_id_out;
    output  reg     [7:0]				    slice_id_out;
	
	output									pcm_bypass_dbf_disable;
	output	reg								pcm_loop_filter_disabled_out;
	output	reg								loop_filter_accross_tiles_enabled_out;
	output	reg								loop_filter_accross_slices_enabled_out;
	output	reg								tile_left_boundary ;
	output	reg                         	tile_top_boundary  ;
	output	reg								slice_left_boundary;
	output	reg								slice_top_boundary ;
	output	reg		[XC_WIDTH-1:0]			tile_x_out  ;
	output	reg		[XC_WIDTH-1:0]			tile_y_out  ;
	output	reg		[XC_WIDTH-1:0]			slice_x_out ;
	output	reg		[XC_WIDTH-1:0]			slice_y_out ;
	output	reg		[XC_WIDTH-1:0]			actual_slice_x_out ;
	output	reg		[XC_WIDTH-1:0]			actual_slice_y_out ;
    
    output  reg                        	    sao_luma_out;
	output  reg                        	    sao_chroma_out;
	output  reg                        	    pcm_out;
	output  reg                        	    bypass_out;
	output  reg     [SAOTYPE_WIDTH -1:0]          		y_sao_type_out;
    output  reg     [BANDPOS_EO_WIDTH -1:0]       		y_bandpos_or_eoclass_out;
    output  reg     [SAO_OFFSET_WIDTH -1:0]       		y_sao_offset_1_out;
    output  reg     [SAO_OFFSET_WIDTH -1:0]       		y_sao_offset_2_out;
    output  reg     [SAO_OFFSET_WIDTH -1:0]       		y_sao_offset_3_out;
    output  reg     [SAO_OFFSET_WIDTH -1:0]       		y_sao_offset_4_out;
    output  reg     [SAOTYPE_WIDTH -1:0]          		cb_sao_type_out;
    output  reg     [BANDPOS_EO_WIDTH -1:0]       		cb_bandpos_or_eoclass_out;
    output  reg     [SAO_OFFSET_WIDTH -1:0]       		cb_sao_offset_1_out;
    output  reg     [SAO_OFFSET_WIDTH -1:0]       		cb_sao_offset_2_out;
    output  reg     [SAO_OFFSET_WIDTH -1:0]       		cb_sao_offset_3_out;
    output  reg     [SAO_OFFSET_WIDTH -1:0]       		cb_sao_offset_4_out;
    output  reg     [SAOTYPE_WIDTH -1:0]          		cr_sao_type_out;
    output  reg     [BANDPOS_EO_WIDTH -1:0]       		cr_bandpos_or_eoclass_out;
    output  reg     [SAO_OFFSET_WIDTH -1:0]       		cr_sao_offset_1_out;
    output  reg     [SAO_OFFSET_WIDTH -1:0]       		cr_sao_offset_2_out;
    output  reg     [SAO_OFFSET_WIDTH -1:0]       		cr_sao_offset_3_out;
    output  reg     [SAO_OFFSET_WIDTH -1:0]       		cr_sao_offset_4_out;
    
	
	output [128-1:0] 	a_block_in_data;
	output 				a_block_in_valid;
	output 				a_block_out_valid;
	output 	[7:0]		luma_db_state;
	output [128-1:0]	a_block_out_data;	
	
	
	
    wire filter_applied_out;
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/global_para.v"


    
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    
    reg             [QP_WIDTH-1:0]			qp_1;       //  ATTENTION    some of these should be output regs
	reg             [QP_WIDTH-1:0]			qp_A;
    wire            [QP_WIDTH-1:0]          qp_C_out;
    wire            [QP_WIDTH-1:0]          qp_D_out;
	reg             [BS_WIDTH-1:0]          BS_v1;
    reg             [BS_WIDTH-1:0]          BS_v2;
    reg             [BS_WIDTH-1:0]          BS_h1;
    reg             [BS_WIDTH-1:0]          BS_h2;
    reg             [BS_WIDTH-1:0]          BS_h0;  
	reg             [7:0]				    pps_cb_qp_offset;
    reg             [7:0]				    pps_cr_qp_offset;
	reg             [7:0]				    slice_tc_offset;
    reg             [7:0]                   slice_beta_offset;
	reg										slice_dbf_disable;
	reg										h0_edge_dbf_disable;
	reg                                     pcm_bypass_AB_dbf_disable;
	
	wire									tile_left_loop_filter_disable ;
	wire									tile_top_loop_filter_disable  ;
	wire									slice_left_loop_filter_disable;
	wire									slice_top_loop_filter_disable ;
	wire									ver_edge_dbf_disable ;
	wire									hor_edge_dbf_disable ;
	wire									pcm_bypass_C_dbf_disable;
	wire                                    pcm_bypass_D_dbf_disable;
    
    reg                                     start_db_modules;
    wire                                    done_db_luma;
    wire                                    done_db_chroma;
    
// (* MARK_DEBUG *)    	integer                                 state;
						integer                                 state;
    
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
    
    luma_db_controller luma_db_controller_module(
        .clk  (clk  ),
        .reset(reset),
        .start(start_db_modules),
		.filter_applied_out(filter_applied_out),
        .done (done_db_luma),
        .q_block_1_in(y_q_block_1_in),
        .q_block_2_in(y_q_block_2_in),
        .q_block_3_in(y_q_block_3_in),
        .q_block_4_in(y_q_block_4_in),
        .A_block_in  (y_A_block_in  ),
        .B_block_in  (y_B_block_in  ),
		.Xc(Xc_out),
		.Yc(Yc_out),
        .qp_1 (qp_1),
        .qp_A (qp_A),
        .qp_C (qp_C_out),
        .qp_D (qp_D_out),
        .BS_v1(BS_v1),
        .BS_v2(BS_v2),
        .BS_h1(BS_h1),
        .BS_h2(BS_h2),
        .BS_h0(BS_h0),
        .slice_tc_offset  (slice_tc_offset  ),
        .slice_beta_offset(slice_beta_offset),
        .pic_width_in (pic_width_out ),
        .pic_height_in(pic_height_out),
		.ver_edge_dbf_disable_in(ver_edge_dbf_disable),
		.hor_edge_dbf_disable_in(hor_edge_dbf_disable),
		.h0_edge_dbf_disable_in(h0_edge_dbf_disable),
		.pcm_bypass_AB_dbf_disable_in(pcm_bypass_AB_dbf_disable),
		.pcm_bypass_dbf_disable_in(pcm_bypass_dbf_disable),
		.pcm_bypass_C_dbf_disable(pcm_bypass_C_dbf_disable),
		.pcm_bypass_D_dbf_disable(pcm_bypass_D_dbf_disable),
		.q_block_1_out(y_q_block_1_out),
		.q_block_2_out(y_q_block_2_out),
		.q_block_3_out(y_q_block_3_out),
		.q_block_4_out(y_q_block_4_out),
		.A_block_out  (y_A_block_out  ),  
		.B_block_out  (y_B_block_out  ),  
		.C_block_out  (y_C_block_out  ),  
		.D_block_out  (y_D_block_out  ),
        .E_block_out  (y_E_block_out  )
	
		,.a_block_in_data	(a_block_in_data	)
		,.a_block_in_valid	(a_block_in_valid	)
		,.a_block_out_valid	(a_block_out_valid	)
		,.luma_db_state	    (luma_db_state		)
		,.a_block_out_data	(a_block_out_data	)
		
		
    );
    
    chroma_db_controller chroma_db_controller_module(
        .clk  (clk  ),
        .reset(reset),
        .start(start_db_modules),
        .done (done_db_chroma),
        .cb_q_block_1_in(cb_q_block_1_in),
        .cb_q_block_2_in(cb_q_block_2_in),
        .cb_q_block_3_in(cb_q_block_3_in),
        .cb_q_block_4_in(cb_q_block_4_in),
        .cb_A_block_in  (cb_A_block_in  ),  
        .cb_B_block_in  (cb_B_block_in  ),  
        .cr_q_block_1_in(cr_q_block_1_in),
        .cr_q_block_2_in(cr_q_block_2_in),
        .cr_q_block_3_in(cr_q_block_3_in),
        .cr_q_block_4_in(cr_q_block_4_in),
        .cr_A_block_in  (cr_A_block_in  ),  
        .cr_B_block_in  (cr_B_block_in  ),  
        .Xc(Xc_out),
        .Yc(Yc_out),
        .qp_1 (qp_1),
        .qp_A (qp_A),
        .qp_C (qp_C_out),
        .qp_D (qp_D_out),
        .BS_v1(BS_v1),
        .BS_v2(BS_v2),
        .BS_h1(BS_h1),
        .BS_h2(BS_h2),
        .BS_h0(BS_h0),
        .pps_cb_qp_offset(pps_cb_qp_offset),
        .pps_cr_qp_offset(pps_cr_qp_offset),
        .slice_tc_offset(slice_tc_offset),
        .pic_width_in (pic_width_out ),
        .pic_height_in(pic_height_out),
		.ver_edge_dbf_disable_in(ver_edge_dbf_disable),
		.hor_edge_dbf_disable_in(hor_edge_dbf_disable),
		.h0_edge_dbf_disable_in(h0_edge_dbf_disable),
		.pcm_bypass_AB_dbf_disable_in(pcm_bypass_AB_dbf_disable),
		.pcm_bypass_dbf_disable_in(pcm_bypass_dbf_disable),
		.pcm_bypass_C_dbf_disable_in(pcm_bypass_C_dbf_disable),
		.pcm_bypass_D_dbf_disable_in(pcm_bypass_D_dbf_disable),
        .cb_q_block_1_out(cb_q_block_1_out),
        .cb_q_block_2_out(cb_q_block_2_out),
        .cb_q_block_3_out(cb_q_block_3_out),
        .cb_q_block_4_out(cb_q_block_4_out),
        .cb_A_block_out  (cb_A_block_out  ),  
        .cb_B_block_out  (cb_B_block_out  ),  
        .cb_C_block_out  (cb_C_block_out  ),  
        .cb_D_block_out  (cb_D_block_out  ),
        .cb_E_block_out  (cb_E_block_out  ),
        .cr_q_block_1_out(cr_q_block_1_out),
        .cr_q_block_2_out(cr_q_block_2_out),
        .cr_q_block_3_out(cr_q_block_3_out),
        .cr_q_block_4_out(cr_q_block_4_out),
        .cr_A_block_out  (cr_A_block_out  ),  
        .cr_B_block_out  (cr_B_block_out  ),  
        .cr_C_block_out  (cr_C_block_out  ),  
        .cr_D_block_out  (cr_D_block_out  ),
        .cr_E_block_out  (cr_E_block_out  )
    );
    
    
	//-----------------Main DB stage controller SM---------------------------------------
	
    always @(posedge clk or posedge reset) begin : db_stage_controller_fsm
        if(reset) begin
            state <= STATE_INIT;
            ready_out <= 1'b0;
        end
		
        else begin
            case (state)
			
                STATE_INIT : begin								// One cycle taken
                    if(data_valid_in & ready_out) begin
                        Xc_out      <=    {1'b0,Xc_in,3'b000};
                        Yc_out      <=    {1'b0,Yc_in,3'b000};
                        qp_1    	<=    qp_1_in;
                        qp_A    	<=    qp_A_in;
                        BS_v1   	<=    BS_v1_in;
                        BS_v2   	<=    BS_v2_in;
                        BS_h1   	<=    BS_h1_in;
                        BS_h2   	<=    BS_h2_in;
                        BS_h0   	<=    BS_h0_in;
                        pps_cb_qp_offset  	<=  {{3{pps_cb_qp_offset_in[4]}},pps_cb_qp_offset_in} ;
                        pps_cr_qp_offset  	<=  {{3{pps_cr_qp_offset_in[4]}},pps_cr_qp_offset_in} ;
                        slice_tc_offset	    <=  {{3{slice_tc_offset_div2_in[3]}},slice_tc_offset_div2_in, 1'b0} ; 
                        slice_beta_offset  	<=  {{3{slice_beta_offset_div2_in[3]}},slice_beta_offset_div2_in, 1'b0} ;
                        pic_width_out  		<=  pic_width_in;
                        pic_height_out 		<=  pic_height_in;
                        ctb_size_out  		<= 	ctb_size_in;
                        poc_out				<= 	poc_in;
						slice_dbf_disable	 					<=  slice_dbf_disable_in;
						pcm_loop_filter_disabled_out  			<=  pcm_loop_filter_disabled_in;
						loop_filter_accross_tiles_enabled_out	<=  loop_filter_accross_tiles_enabled_in;
						loop_filter_accross_slices_enabled_out  <=  loop_filter_accross_slices_enabled_in;
						tile_x_out  				<=		tile_x_in  ;
						tile_y_out  				<=		tile_y_in  ;
						slice_x_out 				<=		slice_x_in ;
						slice_y_out 				<=		slice_y_in ;
						actual_slice_x_out 			<=		actual_slice_x_in ;
						actual_slice_y_out 			<=		actual_slice_y_in ;
						current_dpb_idx_out			<=		current_dpb_idx_in;
                        tile_id_out                 <=      tile_id_in;
                        slice_id_out                <=      slice_id_in;
						h0_edge_dbf_disable			<=		h0_edge_dbf_disable_in;
						pcm_bypass_AB_dbf_disable	<=		pcm_bypass_AB_dbf_disable_in;
                        sao_luma_out                <=      sao_luma_in;
                        sao_chroma_out              <=      sao_chroma_in;
                        pcm_out                     <=      pcm_in;
                        bypass_out                  <=      bypass_in;
                        y_sao_type_out              <=      y_sao_type_in;
                        y_bandpos_or_eoclass_out    <=      y_bandpos_or_eoclass_in;
                        y_sao_offset_1_out          <=      y_sao_offset_1_in;
                        y_sao_offset_2_out          <=      y_sao_offset_2_in;
                        y_sao_offset_3_out          <=      y_sao_offset_3_in;
                        y_sao_offset_4_out          <=      y_sao_offset_4_in;
                        cb_sao_type_out             <=      cb_sao_type_in;
                        cb_bandpos_or_eoclass_out   <=      cb_bandpos_or_eoclass_in;
                        cb_sao_offset_1_out         <=      cb_sao_offset_1_in;
                        cb_sao_offset_2_out         <=      cb_sao_offset_2_in;
                        cb_sao_offset_3_out         <=      cb_sao_offset_3_in;
                        cb_sao_offset_4_out         <=      cb_sao_offset_4_in;
                        cr_sao_type_out             <=      cr_sao_type_in;
                        cr_bandpos_or_eoclass_out   <=      cr_bandpos_or_eoclass_in;
                        cr_sao_offset_1_out         <=      cr_sao_offset_1_in;
                        cr_sao_offset_2_out         <=      cr_sao_offset_2_in;
                        cr_sao_offset_3_out         <=      cr_sao_offset_3_in;
                        cr_sao_offset_4_out         <=      cr_sao_offset_4_in;
						log2_ctb_size_out 			<= 		log2_ctb_size_in;
                        // start luma and chroma db controllers
                        ready_out <= 1'b0;
                        state <= STATE_1;
                    end
                    else begin
                        ready_out <= 1'b1;
                    end
                end
				
                STATE_1 : begin
                    if(valid_out & ready_in) begin
                        ready_out <= 1'b1;
                        state <= STATE_INIT;
                    end
                end
				
            endcase
        end
    end
	
	
	//---------Combinational Block to generate valid out ---------------//
	always @(*) begin
		case(state)
			STATE_1 : begin
				if(done_db_luma & done_db_chroma) begin		// done signals will be held high until instructed to restart
					valid_out = 1'b1;						// required since the 2 modules can finish at different times
				end
				else begin
					valid_out = 1'b0;
				end
			end
			default : begin
				valid_out = 1'b0;
			end
		endcase
	end
	
	//---------Combinational Block to generate valid out ---------------//
	always @(*) begin
		case(state)
			STATE_INIT : begin
				if(data_valid_in & ready_out) begin
					start_db_modules = 1'b1;
				end
				else begin
					start_db_modules = 1'b0;
				end
			end
			default : begin
				start_db_modules = 1'b0;
			end
		endcase
	end
	
	
	//-------------- Combinational block to detect tile boundaries ----------------------
	always @(*) begin
		if(Xc_out == tile_x_out) begin
			tile_left_boundary = 1'b1;
		end
		else begin
			tile_left_boundary = 1'b0;
		end
		if(Yc_out == tile_y_out) begin
			tile_top_boundary = 1'b1;
		end
		else begin
			tile_top_boundary = 1'b0;
		end
	end
	
	//-------------- Combinational block to detect slice boundaries ----------------------
	always @(*) begin
		if( (Yc_out >= slice_y_out) && (Yc_out < slice_y_out + ctb_size_out) ) begin
			if(Xc_out == slice_x_out) begin
				slice_left_boundary = 1'b1;
			end
			else begin
				slice_left_boundary = 1'b0;
			end
		end
		else if((slice_y_out != actual_slice_y_out)) begin
			if( (Xc_out == tile_x_out) && (Yc_out < actual_slice_y_out) ) begin
				slice_left_boundary = 1'b1;
			end
			else begin
				slice_left_boundary = 1'b0;
			end
		end
		else begin
			if(Xc_out == tile_x_out) begin
				slice_left_boundary = 1'b1;
			end
			else begin
				slice_left_boundary = 1'b0;
			end
		end
		
		if(Yc_out == slice_y_out + ctb_size_out) begin
			if(Xc_out < slice_x_out) begin
				slice_top_boundary = 1'b1;
			end
			else begin
				slice_top_boundary = 1'b0;
			end
		end
		else begin
			if(Yc_out == slice_y_out) begin
				slice_top_boundary = 1'b1;
			end
			else begin
				slice_top_boundary = 1'b0;
			end
		end
	end
	
	//-------------- Generate DBF disable condition flags ----------------------
	
	assign	tile_left_loop_filter_disable  	=  	( tile_left_boundary & (~loop_filter_accross_tiles_enabled_out) ) ;
	assign	tile_top_loop_filter_disable	=	( tile_top_boundary  & (~loop_filter_accross_tiles_enabled_out) ) ;
	assign	slice_left_loop_filter_disable	=	( slice_left_boundary & (~loop_filter_accross_slices_enabled_out) ) ;
	assign	slice_top_loop_filter_disable	=	( slice_top_boundary  & (~loop_filter_accross_slices_enabled_out) ) ;
	
	assign	ver_edge_dbf_disable	=	( tile_left_loop_filter_disable | slice_left_loop_filter_disable | slice_dbf_disable ) ;
	assign	hor_edge_dbf_disable	=	( tile_top_loop_filter_disable  | slice_top_loop_filter_disable  | slice_dbf_disable ) ;
	assign	pcm_bypass_dbf_disable	=	( (pcm_out & pcm_loop_filter_disabled_out) | bypass_out ) ;
    
    

endmodule
