`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:34:35 07/11/2014 
// Design Name: 
// Module Name:    BRAM_single_port 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module BRAM_single_port(
		clk,
        we_a,
        en_a,
        addr_a,
        data_in_a,
        data_out_a
    );
	
	parameter		ADDR_WIDTH	=	8 ;
	parameter		DATA_WIDTH	=	256 ;
	parameter		RAM_DEPTH	=	128 ;
	
	input                       			clk;
    input                       			we_a;
    input                       			en_a;
    input           [ADDR_WIDTH-1:0]       	addr_a;
    input           [DATA_WIDTH-1:0]     	data_in_a;
    output reg      [DATA_WIDTH-1:0]     	data_out_a;
    
    reg   			[DATA_WIDTH-1:0]     	RAM [RAM_DEPTH-1:0];
    
    always @(posedge clk) begin
        if(en_a) begin
            if(we_a) begin
                RAM[addr_a] <= data_in_a;
            end
            else begin
                data_out_a <= RAM[addr_a];
            end
        end
    end


endmodule
