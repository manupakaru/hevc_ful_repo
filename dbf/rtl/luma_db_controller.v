`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:53:14 11/15/2013 
// Design Name: 
// Module Name:    luma_db_controller 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module luma_db_controller(
        clk,
        reset,
        start,
        done,
        q_block_1_in,
        q_block_2_in,
        q_block_3_in,
        q_block_4_in,
        A_block_in,
        B_block_in,
		
		filter_applied_out,
		  Xc,
		  Yc,
        qp_1,
        qp_A,
        qp_C,
        qp_D,
        BS_v1,
        BS_v2,
        BS_h1,
        BS_h2,
        BS_h0,
        slice_tc_offset,
        slice_beta_offset,
        pic_width_in,
        pic_height_in,
		ver_edge_dbf_disable_in,
		hor_edge_dbf_disable_in,
		h0_edge_dbf_disable_in,
		pcm_bypass_AB_dbf_disable_in,
		pcm_bypass_dbf_disable_in,
		pcm_bypass_C_dbf_disable,
		pcm_bypass_D_dbf_disable,
		  q_block_1_out,
		  q_block_2_out,
		  q_block_3_out,
		  q_block_4_out,
		  A_block_out,  
		  B_block_out,  
		  C_block_out,  
		  D_block_out,
          E_block_out
		  
		,a_block_in_data	
		,a_block_in_valid	
		,a_block_out_valid	
		,luma_db_state	
		,a_block_out_data		  
    );

`include "../sim/pred_def.v"
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    
    localparam                          VERT_UP             = 3'b000;
    localparam                          VERT_DOWN           = 3'b001;
    localparam                          HOR_LEFT            = 3'b010;
    localparam                          HOR_RIGHT           = 3'b011;
    localparam                          HOR_FAR_RIGHT       = 3'b100;
    localparam                          CB                  = 0     ;
    localparam                          CR                  = 1     ;
    
    localparam                          SAMPLE_ROW_BUF_DATA_WIDTH 	= 	((BIT_DEPTH*16) + 16) ;
    
    localparam                          STATE_INIT         	= 	0 ;    
    localparam                          STATE_1            	= 	1 ;
    localparam                          STATE_1_WAIT       	= 	2 ;
    localparam                          STATE_2            	= 	3 ;
    localparam                          STATE_2_WAIT       	= 	4 ;
    localparam                          STATE_3            	= 	5 ;
    localparam                          STATE_3_WAIT     	= 	6 ;
    localparam                          STATE_4            	= 	7 ;
    localparam                          STATE_4_WAIT     	= 	8 ;
    localparam                          STATE_5            	= 	9 ;
    localparam                          STATE_5_WAIT    	= 	10;
	
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input                               clk;
    input                               reset;
    
    input                               start;
    output	reg					        done;
    
    // Pixel inputs
    input       	[127:0]         q_block_1_in ;
    input       	[127:0]         q_block_2_in ;
    input       	[127:0]         q_block_3_in ;
    input       	[127:0]         q_block_4_in ;
    input       	[127:0]         A_block_in   ;
    input       	[127:0]         B_block_in   ;
    
    // Parameter inputs
    input           [11:0]                  Xc;
    input           [11:0]                  Yc;
    input			[QP_WIDTH-1:0]			qp_1;
	input			[QP_WIDTH-1:0]			qp_A;
	output  reg	    [QP_WIDTH-1:0]			qp_C;
	output  reg	    [QP_WIDTH-1:0]			qp_D;
    input           [BS_WIDTH-1:0]          BS_v1;
    input           [BS_WIDTH-1:0]          BS_v2;
    input           [BS_WIDTH-1:0]          BS_h1;
    input           [BS_WIDTH-1:0]          BS_h2;
    input           [BS_WIDTH-1:0]          BS_h0;  // BS from previous block
	input			[7:0]				    slice_tc_offset;
    input           [7:0]                   slice_beta_offset;
    input           [11:0]                  pic_width_in;
    input           [11:0]                  pic_height_in;
	input									ver_edge_dbf_disable_in;
	input                                   hor_edge_dbf_disable_in;
	input									h0_edge_dbf_disable_in;
	input                                   pcm_bypass_AB_dbf_disable_in;
	input									pcm_bypass_dbf_disable_in;
	
	output 	reg								pcm_bypass_C_dbf_disable;
	output 	reg								pcm_bypass_D_dbf_disable;
    
    // Pixel outputs
    output          [127:0]         q_block_1_out;
    output          [127:0]         q_block_2_out;
    output          [127:0]         q_block_3_out;
    output          [127:0]         q_block_4_out;
    output          [127:0]         A_block_out  ;
    output          [127:0]         B_block_out  ;
    output          [127:0]         C_block_out  ;
    output          [127:0]         D_block_out  ;
    output          [127:0]         E_block_out  ;
	
	output							filter_applied_out;
	
	// Test
	
	output [128-1:0] 	a_block_in_data;
	output 				a_block_in_valid;
	output 				a_block_out_valid;
	output 	[7:0]		luma_db_state;
	output [128-1:0]	a_block_out_data;
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/global_para.v"


    
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    
    reg             [7:0]           q_block_1 [0:15];
    reg             [7:0]           q_block_2 [0:15];
    reg             [7:0]           q_block_3 [0:15];
    reg             [7:0]           q_block_4 [0:15];
    reg             [7:0]           A_block [15:0];
    reg             [7:0]           B_block [0:15];
    reg             [7:0]           C_block [0:15];
    reg             [7:0]           D_block [0:15];
    reg             [7:0]           E_block [0:15];
	
    wire            [7:0]           B_block_wire[0:15];

generate
        genvar i;
        
        for (i = 0 ; i < 16 ; i = i + 1) begin : loop_i
                assign  B_block_wire[i] = B_block[i];
        end
endgenerate    
    
    // Signals for BRAM row buffer
    reg                             						we_a;  
    reg                             						we_b;
    reg                             						en_a;
    reg                             						en_b;
    reg             [9:0]           						addr_a;
    reg             [9:0]           						addr_b;
    reg             [SAMPLE_ROW_BUF_DATA_WIDTH-1:0]         data_in_a;
    reg             [SAMPLE_ROW_BUF_DATA_WIDTH-1:0]         data_in_b;
    wire            [SAMPLE_ROW_BUF_DATA_WIDTH-1:0]         data_out_a;
    wire            [SAMPLE_ROW_BUF_DATA_WIDTH-1:0]         data_out_b;
    
    // Signals for luma edge filter module
    reg                             start_edge_filter;
    reg           [2:0]             edge_type_filter;
    wire                            done_filter;
	wire							filter_applied;

    wire          [7:0]             p_blk_0 ;
    wire          [7:0]             p_blk_1 ;
    wire          [7:0]             p_blk_2 ;
    wire          [7:0]             p_blk_3 ;
    wire          [7:0]             p_blk_4 ;
    wire          [7:0]             p_blk_5 ;
    wire          [7:0]             p_blk_6 ;
    wire          [7:0]             p_blk_7 ;
    wire          [7:0]             p_blk_8 ;
    wire          [7:0]             p_blk_9 ;
    wire          [7:0]             p_blk_10;
    wire          [7:0]             p_blk_11;
    wire          [7:0]             p_blk_12;
    wire          [7:0]             p_blk_13;
    wire          [7:0]             p_blk_14;
    wire          [7:0]             p_blk_15;
    wire          [7:0]             q_blk_0 ;
    wire          [7:0]             q_blk_1 ;
    wire          [7:0]             q_blk_2 ;
    wire          [7:0]             q_blk_3 ;
    wire          [7:0]             q_blk_4 ;
    wire          [7:0]             q_blk_5 ;
    wire          [7:0]             q_blk_6 ;
    wire          [7:0]             q_blk_7 ;
    wire          [7:0]             q_blk_8 ;
    wire          [7:0]             q_blk_9 ;
    wire          [7:0]             q_blk_10;
    wire          [7:0]             q_blk_11;
    wire          [7:0]             q_blk_12;
    wire          [7:0]             q_blk_13;
    wire          [7:0]             q_blk_14;
    wire          [7:0]             q_blk_15;
    
    integer                         state;
    
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

	assign a_block_in_data = A_block_in;
	assign a_block_in_valid = start;
	assign a_block_out_valid = (state !=0);
	assign luma_db_state = state;
	assign a_block_out_data = A_block_out;  
   
    assign filter_applied_out = filter_applied;
    //------------ Filtering submodule ----------------
	
    luma_edge_filter filtering_submodule(
        .clk(clk),
        .reset(reset),
        .start(start_edge_filter),
        .edge_type(edge_type_filter),
        .done(done_filter),
		.filter_applied(filter_applied),
        .q_block_1_in( { q_block_1[15],   
                         q_block_1[14],
                         q_block_1[13],
                         q_block_1[12],
                         q_block_1[11],
                         q_block_1[10],
                         q_block_1[9 ],
                         q_block_1[8 ],
                         q_block_1[7 ],
                         q_block_1[6 ],
                         q_block_1[5 ],
                         q_block_1[4 ],
                         q_block_1[3 ],
                         q_block_1[2 ],
                         q_block_1[1 ],
                         q_block_1[0 ]  }
                ),
        .q_block_2_in( { q_block_2[15],   
                         q_block_2[14],
                         q_block_2[13],
                         q_block_2[12],
                         q_block_2[11],
                         q_block_2[10],
                         q_block_2[9 ],
                         q_block_2[8 ],
                         q_block_2[7 ],
                         q_block_2[6 ],
                         q_block_2[5 ],
                         q_block_2[4 ],
                         q_block_2[3 ],
                         q_block_2[2 ],
                         q_block_2[1 ],
                         q_block_2[0 ]  }
                ),
        .q_block_3_in( { q_block_3[15],   
                         q_block_3[14],
                         q_block_3[13],
                         q_block_3[12],
                         q_block_3[11],
                         q_block_3[10],
                         q_block_3[9 ],
                         q_block_3[8 ],
                         q_block_3[7 ],
                         q_block_3[6 ],
                         q_block_3[5 ],
                         q_block_3[4 ],
                         q_block_3[3 ],
                         q_block_3[2 ],
                         q_block_3[1 ],
                         q_block_3[0 ]  }
                ),
        .q_block_4_in( { q_block_4[15],   
                         q_block_4[14],
                         q_block_4[13],
                         q_block_4[12],
                         q_block_4[11],
                         q_block_4[10],
                         q_block_4[9 ],
                         q_block_4[8 ],
                         q_block_4[7 ],
                         q_block_4[6 ],
                         q_block_4[5 ],
                         q_block_4[4 ],
                         q_block_4[3 ],
                         q_block_4[2 ],
                         q_block_4[1 ],
                         q_block_4[0 ]  }
                ),
        .A_block_in( {   A_block[15],   
                         A_block[14],
                         A_block[13],
                         A_block[12],
                         A_block[11],
                         A_block[10],
                         A_block[9 ],
                         A_block[8 ],
                         A_block[7 ],
                         A_block[6 ],
                         A_block[5 ],
                         A_block[4 ],
                         A_block[3 ],
                         A_block[2 ],
                         A_block[1 ],
                         A_block[0 ]  }
                ),
        .B_block_in( {   B_block[15],   
                         B_block[14],
                         B_block[13],
                         B_block[12],
                         B_block[11],
                         B_block[10],
                         B_block[9 ],
                         B_block[8 ],
                         B_block[7 ],
                         B_block[6 ],
                         B_block[5 ],
                         B_block[4 ],
                         B_block[3 ],
                         B_block[2 ],
                         B_block[1 ],
                         B_block[0 ]  }
                ),
        .C_block_in( {   C_block[15],   
                         C_block[14],
                         C_block[13],
                         C_block[12],
                         C_block[11],
                         C_block[10],
                         C_block[9 ],
                         C_block[8 ],
                         C_block[7 ],
                         C_block[6 ],
                         C_block[5 ],
                         C_block[4 ],
                         C_block[3 ],
                         C_block[2 ],
                         C_block[1 ],
                         C_block[0 ]  }
                ),
        .D_block_in( {   D_block[15],   
                         D_block[14],
                         D_block[13],
                         D_block[12],
                         D_block[11],
                         D_block[10],
                         D_block[9 ],
                         D_block[8 ],
                         D_block[7 ],
                         D_block[6 ],
                         D_block[5 ],
                         D_block[4 ],
                         D_block[3 ],
                         D_block[2 ],
                         D_block[1 ],
                         D_block[0 ]  }
                ),
        .E_block_in( {   E_block[15],   
                         E_block[14],
                         E_block[13],
                         E_block[12],
                         E_block[11],
                         E_block[10],
                         E_block[9 ],
                         E_block[8 ],
                         E_block[7 ],
                         E_block[6 ],
                         E_block[5 ],
                         E_block[4 ],
                         E_block[3 ],
                         E_block[2 ],
                         E_block[1 ],
                         E_block[0 ]  }
                ),        
        .qp_1 (qp_1 ),
        .qp_A (qp_A ),
        .qp_C (qp_C ),
        .qp_D (qp_D ),
        .BS_v1(BS_v1),
        .BS_v2(BS_v2),
        .BS_h1(BS_h1),
        .BS_h2(BS_h2),
        .BS_h0(BS_h0),
        .slice_tc_offset(slice_tc_offset),
        .slice_beta_offset(slice_beta_offset),
        .p_blk_out0 (p_blk_0 ),
        .p_blk_out1 (p_blk_1 ),
        .p_blk_out2 (p_blk_2 ),
        .p_blk_out3 (p_blk_3 ),
        .p_blk_out4 (p_blk_4 ),
        .p_blk_out5 (p_blk_5 ),
        .p_blk_out6 (p_blk_6 ),
        .p_blk_out7 (p_blk_7 ),
        .p_blk_out8 (p_blk_8 ),
        .p_blk_out9 (p_blk_9 ),
        .p_blk_out10(p_blk_10),
        .p_blk_out11(p_blk_11),
        .p_blk_out12(p_blk_12),
        .p_blk_out13(p_blk_13),
        .p_blk_out14(p_blk_14),
        .p_blk_out15(p_blk_15),
        .q_blk_out0 (q_blk_0 ),
        .q_blk_out1 (q_blk_1 ),
        .q_blk_out2 (q_blk_2 ),
        .q_blk_out3 (q_blk_3 ),
        .q_blk_out4 (q_blk_4 ),
        .q_blk_out5 (q_blk_5 ),
        .q_blk_out6 (q_blk_6 ),
        .q_blk_out7 (q_blk_7 ),
        .q_blk_out8 (q_blk_8 ),
        .q_blk_out9 (q_blk_9 ),
        .q_blk_out10(q_blk_10),
        .q_blk_out11(q_blk_11),
        .q_blk_out12(q_blk_12),
        .q_blk_out13(q_blk_13),
        .q_blk_out14(q_blk_14),
        .q_blk_out15(q_blk_15)
    );
    
    //---------- Row buffer BRAM ----------------
    BRAM_dual_port #(
		.ADDR_WIDTH	(10),
		.DATA_WIDTH	(SAMPLE_ROW_BUF_DATA_WIDTH),
		.RAM_DEPTH	(MAX_PIC_WIDTH/4)
	)
	luma_row_buffer(
        .clk(clk),
        .we_a(we_a),
        .we_b(we_b),
        .en_a(en_a),
        .en_b(en_b),
        .addr_a(addr_a),
        .addr_b(addr_b),
        .data_in_a(data_in_a),
        .data_in_b(data_in_b),
        .data_out_a(data_out_a),
        .data_out_b(data_out_b)
    );
    
    //------------ Output assignments -----------------------
	
    assign  q_block_1_out = ( { q_block_1[15],   
                             q_block_1[14],
                             q_block_1[13],
                             q_block_1[12],
                             q_block_1[11],
                             q_block_1[10],
                             q_block_1[9 ],
                             q_block_1[8 ],
                             q_block_1[7 ],
                             q_block_1[6 ],
                             q_block_1[5 ],
                             q_block_1[4 ],
                             q_block_1[3 ],
                             q_block_1[2 ],
                             q_block_1[1 ],
                             q_block_1[0 ]  }
                );         
    assign  q_block_2_out = ( { q_block_2[15],   
                             q_block_2[14],
                             q_block_2[13],
                             q_block_2[12],
                             q_block_2[11],
                             q_block_2[10],
                             q_block_2[9 ],
                             q_block_2[8 ],
                             q_block_2[7 ],
                             q_block_2[6 ],
                             q_block_2[5 ],
                             q_block_2[4 ],
                             q_block_2[3 ],
                             q_block_2[2 ],
                             q_block_2[1 ],
                             q_block_2[0 ]  }
                );
    assign  q_block_3_out = ( { q_block_3[15],   
                             q_block_3[14],
                             q_block_3[13],
                             q_block_3[12],
                             q_block_3[11],
                             q_block_3[10],
                             q_block_3[9 ],
                             q_block_3[8 ],
                             q_block_3[7 ],
                             q_block_3[6 ],
                             q_block_3[5 ],
                             q_block_3[4 ],
                             q_block_3[3 ],
                             q_block_3[2 ],
                             q_block_3[1 ],
                             q_block_3[0 ]  }
                );
    assign  q_block_4_out = ( { q_block_4[15],   
                             q_block_4[14],
                             q_block_4[13],
                             q_block_4[12],
                             q_block_4[11],
                             q_block_4[10],
                             q_block_4[9 ],
                             q_block_4[8 ],
                             q_block_4[7 ],
                             q_block_4[6 ],
                             q_block_4[5 ],
                             q_block_4[4 ],
                             q_block_4[3 ],
                             q_block_4[2 ],
                             q_block_4[1 ],
                             q_block_4[0 ]  }
                );
    assign  A_block_out = ( {  A_block[15],   
                            A_block[14],
                            A_block[13],
                            A_block[12],
                            A_block[11],
                            A_block[10],
                            A_block[9 ],
                            A_block[8 ],
                            A_block[7 ],
                            A_block[6 ],
                            A_block[5 ],
                            A_block[4 ],
                            A_block[3 ],
                            A_block[2 ],
                            A_block[1 ],
                            A_block[0 ]  }
                );        
    assign  B_block_out = ( {  B_block[15],   
                            B_block[14],
                            B_block[13],
                            B_block[12],
                            B_block[11],
                            B_block[10],
                            B_block[9 ],
                            B_block[8 ],
                            B_block[7 ],
                            B_block[6 ],
                            B_block[5 ],
                            B_block[4 ],
                            B_block[3 ],
                            B_block[2 ],
                            B_block[1 ],
                            B_block[0 ]  }
                );
    assign  C_block_out = ( {  C_block[15],   
                            C_block[14],
                            C_block[13],
                            C_block[12],
                            C_block[11],
                            C_block[10],
                            C_block[9 ],
                            C_block[8 ],
                            C_block[7 ],
                            C_block[6 ],
                            C_block[5 ],
                            C_block[4 ],
                            C_block[3 ],
                            C_block[2 ],
                            C_block[1 ],
                            C_block[0 ]  }
                );
    assign  D_block_out = ( {  D_block[15],   
                            D_block[14],
                            D_block[13],
                            D_block[12],
                            D_block[11],
                            D_block[10],
                            D_block[9 ],
                            D_block[8 ],
                            D_block[7 ],
                            D_block[6 ],
                            D_block[5 ],
                            D_block[4 ],
                            D_block[3 ],
                            D_block[2 ],
                            D_block[1 ],
                            D_block[0 ]  }
                );
    assign  E_block_out = ( {  E_block[15],   
                            E_block[14],
                            E_block[13],
                            E_block[12],
                            E_block[11],
                            E_block[10],
                            E_block[9 ],
                            E_block[8 ],
                            E_block[7 ],
                            E_block[6 ],
                            E_block[5 ],
                            E_block[4 ],
                            E_block[3 ],
                            E_block[2 ],
                            E_block[1 ],
                            E_block[0 ]  }
                );            
    
	
	//------------- Main state machine for 8x8 luma edge filtering --------------------------
	
    always @(posedge clk or posedge reset) begin
        if(reset) begin
            state <= STATE_INIT;
            done  <= 1'b0;
        end
        else begin
            case (state)
                STATE_INIT : begin
                    if(start) begin									// done in parallel with main stage state init cycle
                        done  <= 1'b0;
						
                        // Assigning input samples to registers
						
                        q_block_1[0]  <=  q_block_1_in[7:0];
                        q_block_2[0]  <=  q_block_2_in[7:0];
                        q_block_3[0]  <=  q_block_3_in[7:0];
                        q_block_4[0]  <=  q_block_4_in[7:0];
                        A_block  [0]  <=  A_block_in  [7:0];
                        B_block  [0]  <=  B_block_in  [7:0];
						
                        q_block_1[1]  <=  q_block_1_in[15:8];
                        q_block_2[1]  <=  q_block_2_in[15:8];
                        q_block_3[1]  <=  q_block_3_in[15:8];
                        q_block_4[1]  <=  q_block_4_in[15:8];
                        A_block  [1]  <=  A_block_in  [15:8];
                        B_block  [1]  <=  B_block_in  [15:8];
                        
                        q_block_1[2]  <=  q_block_1_in[23:16];
                        q_block_2[2]  <=  q_block_2_in[23:16];
                        q_block_3[2]  <=  q_block_3_in[23:16];
                        q_block_4[2]  <=  q_block_4_in[23:16];
                        A_block  [2]  <=  A_block_in  [23:16];
                        B_block  [2]  <=  B_block_in  [23:16];
                        
                        q_block_1[3]  <=  q_block_1_in[31:24];
                        q_block_2[3]  <=  q_block_2_in[31:24];
                        q_block_3[3]  <=  q_block_3_in[31:24];
                        q_block_4[3]  <=  q_block_4_in[31:24];
                        A_block  [3]  <=  A_block_in  [31:24];
                        B_block  [3]  <=  B_block_in  [31:24];
                        
                        q_block_1[4]  <=  q_block_1_in[39:32];
                        q_block_2[4]  <=  q_block_2_in[39:32];
                        q_block_3[4]  <=  q_block_3_in[39:32];
                        q_block_4[4]  <=  q_block_4_in[39:32];
                        A_block  [4]  <=  A_block_in  [39:32];
                        B_block  [4]  <=  B_block_in  [39:32];
                        
                        q_block_1[5]  <=  q_block_1_in[47:40];
                        q_block_2[5]  <=  q_block_2_in[47:40];
                        q_block_3[5]  <=  q_block_3_in[47:40];
                        q_block_4[5]  <=  q_block_4_in[47:40];
                        A_block  [5]  <=  A_block_in  [47:40];
                        B_block  [5]  <=  B_block_in  [47:40];
                        
                        q_block_1[6]  <=  q_block_1_in[55:48];
                        q_block_2[6]  <=  q_block_2_in[55:48];
                        q_block_3[6]  <=  q_block_3_in[55:48];
                        q_block_4[6]  <=  q_block_4_in[55:48];
                        A_block  [6]  <=  A_block_in  [55:48];
                        B_block  [6]  <=  B_block_in  [55:48];
                        
                        q_block_1[7]  <=  q_block_1_in[63:56];
                        q_block_2[7]  <=  q_block_2_in[63:56];
                        q_block_3[7]  <=  q_block_3_in[63:56];
                        q_block_4[7]  <=  q_block_4_in[63:56];
                        A_block  [7]  <=  A_block_in  [63:56];
                        B_block  [7]  <=  B_block_in  [63:56];
                        
                        q_block_1[8]  <=  q_block_1_in[71:64];
                        q_block_2[8]  <=  q_block_2_in[71:64];
                        q_block_3[8]  <=  q_block_3_in[71:64];
                        q_block_4[8]  <=  q_block_4_in[71:64];
                        A_block  [8]  <=  A_block_in  [71:64];
                        B_block  [8]  <=  B_block_in  [71:64];
                        
                        q_block_1[9]  <=  q_block_1_in[79:72];
                        q_block_2[9]  <=  q_block_2_in[79:72];
                        q_block_3[9]  <=  q_block_3_in[79:72];
                        q_block_4[9]  <=  q_block_4_in[79:72];
                        A_block  [9]  <=  A_block_in  [79:72];
                        B_block  [9]  <=  B_block_in  [79:72];
                        
                        q_block_1[10]  <=  q_block_1_in[87:80];
                        q_block_2[10]  <=  q_block_2_in[87:80];
                        q_block_3[10]  <=  q_block_3_in[87:80];
                        q_block_4[10]  <=  q_block_4_in[87:80];
                        A_block  [10]  <=  A_block_in  [87:80];
                        B_block  [10]  <=  B_block_in  [87:80];
                        
                        q_block_1[11]  <=  q_block_1_in[95:88];
                        q_block_2[11]  <=  q_block_2_in[95:88];
                        q_block_3[11]  <=  q_block_3_in[95:88];
                        q_block_4[11]  <=  q_block_4_in[95:88];
                        A_block  [11]  <=  A_block_in  [95:88];
                        B_block  [11]  <=  B_block_in  [95:88];
                        
                        q_block_1[12]  <=  q_block_1_in[103:96];
                        q_block_2[12]  <=  q_block_2_in[103:96];
                        q_block_3[12]  <=  q_block_3_in[103:96];
                        q_block_4[12]  <=  q_block_4_in[103:96];
                        A_block  [12]  <=  A_block_in  [103:96];
                        B_block  [12]  <=  B_block_in  [103:96];
                        
                        q_block_1[13]  <=  q_block_1_in[111:104];
                        q_block_2[13]  <=  q_block_2_in[111:104];
                        q_block_3[13]  <=  q_block_3_in[111:104];
                        q_block_4[13]  <=  q_block_4_in[111:104];
                        A_block  [13]  <=  A_block_in  [111:104];
                        B_block  [13]  <=  B_block_in  [111:104];
                        
                        q_block_1[14]  <=  q_block_1_in[119:112];
                        q_block_2[14]  <=  q_block_2_in[119:112];
                        q_block_3[14]  <=  q_block_3_in[119:112];
                        q_block_4[14]  <=  q_block_4_in[119:112];
                        A_block  [14]  <=  A_block_in  [119:112];
                        B_block  [14]  <=  B_block_in  [119:112];
                        
                        q_block_1[15]  <=  q_block_1_in[127:120];
                        q_block_2[15]  <=  q_block_2_in[127:120];
                        q_block_3[15]  <=  q_block_3_in[127:120];
                        q_block_4[15]  <=  q_block_4_in[127:120];
                        A_block  [15]  <=  A_block_in  [127:120];
                        B_block  [15]  <=  B_block_in  [127:120];
                        
                        state <= STATE_1;
                    end
                end
				
				STATE_1 : begin
					// Check for C,D blocks reading in combinational block
					if((Xc>0) && (BS_v1>0) && (ver_edge_dbf_disable_in==0)) begin
						state <= STATE_1_WAIT;
					end
					else begin
						state <= STATE_2;
					end
				end
				STATE_1_WAIT : begin
					if(done_filter) begin
						// Assign output samples 
						if(filter_applied) begin
							if(pcm_bypass_AB_dbf_disable_in==0) begin
								A_block[3]     <=   p_blk_0  ;
								A_block[2]     <=   p_blk_1  ;
								A_block[1]     <=   p_blk_2  ;
								A_block[0]     <=   p_blk_3  ;
								A_block[7]     <=   p_blk_4  ;
								A_block[6]     <=   p_blk_5  ;
								A_block[5]     <=   p_blk_6  ;
								A_block[4]     <=   p_blk_7  ;
								A_block[11]    <=   p_blk_8  ;
								A_block[10]    <=   p_blk_9  ;
								A_block[9]     <=   p_blk_10 ;
								A_block[8]     <=   p_blk_11 ;
								A_block[15]    <=   p_blk_12 ;
								A_block[14]    <=   p_blk_13 ;
								A_block[13]    <=   p_blk_14 ;
								A_block[12]    <=   p_blk_15 ;
							end
							if(pcm_bypass_dbf_disable_in==0) begin
								q_block_1[0]   <=   q_blk_0  ;
								q_block_1[1]   <=   q_blk_1  ;
								q_block_1[2]   <=   q_blk_2  ;
								q_block_1[3]   <=   q_blk_3  ;
								q_block_1[4]   <=   q_blk_4  ;
								q_block_1[5]   <=   q_blk_5  ;
								q_block_1[6]   <=   q_blk_6  ;
								q_block_1[7]   <=   q_blk_7  ;
								q_block_1[8]   <=   q_blk_8  ;
								q_block_1[9]   <=   q_blk_9  ;
								q_block_1[10]  <=   q_blk_10 ;
								q_block_1[11]  <=   q_blk_11 ;
								q_block_1[12]  <=   q_blk_12 ;
								q_block_1[13]  <=   q_blk_13 ;
								q_block_1[14]  <=   q_blk_14 ;
								q_block_1[15]  <=   q_blk_15 ;
							end
						end
						// Assign to C,D blocks from BRAM
						if((Xc>0) && (Yc>0)) begin
							C_block[0]    <=    data_out_a[7  :0  ];
							C_block[1]    <=    data_out_a[15 :8  ];
							C_block[2]    <=    data_out_a[23 :16 ];
							C_block[3]    <=    data_out_a[31 :24 ];
							C_block[4]    <=    data_out_a[39 :32 ];
							C_block[5]    <=    data_out_a[47 :40 ];
							C_block[6]    <=    data_out_a[55 :48 ];
							C_block[7]    <=    data_out_a[63 :56 ];
							C_block[8]    <=    data_out_a[71 :64 ];
							C_block[9]    <=    data_out_a[79 :72 ];
							C_block[10]   <=    data_out_a[87 :80 ];
							C_block[11]   <=    data_out_a[95 :88 ];
							C_block[12]   <=    data_out_a[103:96 ];
							C_block[13]   <=    data_out_a[111:104];
							C_block[14]   <=    data_out_a[119:112];
							C_block[15]   <=    data_out_a[127:120];
							qp_C    <=   data_out_a[133:128];               // ATTN - where is qp and others stored
							pcm_bypass_C_dbf_disable <= data_out_a[134];
						end
						if(Yc>0) begin
							D_block[0]    <=    data_out_b[7  :0  ];
							D_block[1]    <=    data_out_b[15 :8  ];
							D_block[2]    <=    data_out_b[23 :16 ];
							D_block[3]    <=    data_out_b[31 :24 ];
							D_block[4]    <=    data_out_b[39 :32 ];
							D_block[5]    <=    data_out_b[47 :40 ];
							D_block[6]    <=    data_out_b[55 :48 ];
							D_block[7]    <=    data_out_b[63 :56 ];
							D_block[8]    <=    data_out_b[71 :64 ];
							D_block[9]    <=    data_out_b[79 :72 ];
							D_block[10]   <=    data_out_b[87 :80 ];
							D_block[11]   <=    data_out_b[95 :88 ];
							D_block[12]   <=    data_out_b[103:96 ];
							D_block[13]   <=    data_out_b[111:104];
							D_block[14]   <=    data_out_b[119:112];
							D_block[15]   <=    data_out_b[127:120];
							qp_D    <=   data_out_b[133:128];
							pcm_bypass_D_dbf_disable <= data_out_b[134];
						end
						// Check for E block reading in combinational block
						if((Xc>0) && (BS_v2>0) && (ver_edge_dbf_disable_in==0)) begin
							state <= STATE_2_WAIT;
						end
						else begin
							state <= STATE_3;
						end
					end
					else begin
						if((Xc>0) && (Yc>0)) begin
							qp_C  <= data_out_a[133:128];
							pcm_bypass_C_dbf_disable <= data_out_a[134];
						end
						if(Yc>0) begin
							qp_D  <= data_out_b[133:128];
							pcm_bypass_D_dbf_disable <= data_out_b[134];
						end
					end
				end
				
				STATE_2 : begin
					// Assign to C,D blocks from BRAM
					if((Xc>0) && (Yc>0)) begin
                        C_block[0]    <=    data_out_a[7  :0  ];
                        C_block[1]    <=    data_out_a[15 :8  ];
                        C_block[2]    <=    data_out_a[23 :16 ];
                        C_block[3]    <=    data_out_a[31 :24 ];
                        C_block[4]    <=    data_out_a[39 :32 ];
                        C_block[5]    <=    data_out_a[47 :40 ];
                        C_block[6]    <=    data_out_a[55 :48 ];
                        C_block[7]    <=    data_out_a[63 :56 ];
                        C_block[8]    <=    data_out_a[71 :64 ];
                        C_block[9]    <=    data_out_a[79 :72 ];
                        C_block[10]   <=    data_out_a[87 :80 ];
                        C_block[11]   <=    data_out_a[95 :88 ];
                        C_block[12]   <=    data_out_a[103:96 ];
                        C_block[13]   <=    data_out_a[111:104];
                        C_block[14]   <=    data_out_a[119:112];
                        C_block[15]   <=    data_out_a[127:120];
                        qp_C    <=   data_out_a[133:128];               // ATTN - where is qp and others stored
						pcm_bypass_C_dbf_disable <= data_out_a[134];
                    end
                    if(Yc>0) begin
                        D_block[0]    <=    data_out_b[7  :0  ];
                        D_block[1]    <=    data_out_b[15 :8  ];
                        D_block[2]    <=    data_out_b[23 :16 ];
                        D_block[3]    <=    data_out_b[31 :24 ];
                        D_block[4]    <=    data_out_b[39 :32 ];
                        D_block[5]    <=    data_out_b[47 :40 ];
                        D_block[6]    <=    data_out_b[55 :48 ];
                        D_block[7]    <=    data_out_b[63 :56 ];
                        D_block[8]    <=    data_out_b[71 :64 ];
                        D_block[9]    <=    data_out_b[79 :72 ];
                        D_block[10]   <=    data_out_b[87 :80 ];
                        D_block[11]   <=    data_out_b[95 :88 ];
                        D_block[12]   <=    data_out_b[103:96 ];
                        D_block[13]   <=    data_out_b[111:104];
                        D_block[14]   <=    data_out_b[119:112];
                        D_block[15]   <=    data_out_b[127:120];
                        qp_D    <=   data_out_b[133:128];
						pcm_bypass_D_dbf_disable <= data_out_b[134];
                    end
					// Check for E block reading in combinational block
					if((Xc>0) && (BS_v2>0) && (ver_edge_dbf_disable_in==0)) begin
						state <= STATE_2_WAIT;
                    end
                    else begin
						state <= STATE_3;
                    end
				end
				STATE_2_WAIT : begin
					if(done_filter) begin
						// Assign output samples 
						if(filter_applied) begin
							if(pcm_bypass_AB_dbf_disable_in==0) begin
								B_block[3]      <=   p_blk_0  ;
								B_block[2]      <=   p_blk_1  ;
								B_block[1]      <=   p_blk_2  ;
								B_block[0]      <=   p_blk_3  ;
								B_block[7]      <=   p_blk_4  ;
								B_block[6]      <=   p_blk_5  ;
								B_block[5]      <=   p_blk_6  ;
								B_block[4]      <=   p_blk_7  ;
								B_block[11]     <=   p_blk_8  ;
								B_block[10]     <=   p_blk_9  ;
								B_block[9]      <=   p_blk_10 ;
								B_block[8]      <=   p_blk_11 ;
								B_block[15]     <=   p_blk_12 ;
								B_block[14]     <=   p_blk_13 ;
								B_block[13]     <=   p_blk_14 ;
								B_block[12]     <=   p_blk_15 ;
							end
							if(pcm_bypass_dbf_disable_in==0) begin
								q_block_3[0]    <=   q_blk_0  ;
								q_block_3[1]    <=   q_blk_1  ;
								q_block_3[2]    <=   q_blk_2  ;
								q_block_3[3]    <=   q_blk_3  ;
								q_block_3[4]    <=   q_blk_4  ;
								q_block_3[5]    <=   q_blk_5  ;
								q_block_3[6]    <=   q_blk_6  ;
								q_block_3[7]    <=   q_blk_7  ;
								q_block_3[8]    <=   q_blk_8  ;
								q_block_3[9]    <=   q_blk_9  ;
								q_block_3[10]   <=   q_blk_10 ;
								q_block_3[11]   <=   q_blk_11 ;
								q_block_3[12]   <=   q_blk_12 ;
								q_block_3[13]   <=   q_blk_13 ;
								q_block_3[14]   <=   q_blk_14 ;
								q_block_3[15]   <=   q_blk_15 ;
							end
						end
						// Assign to E block from BRAM if necessary
						if((Yc>0) && (Xc ==(pic_width_in - 8))) begin
							E_block[0]    <=    data_out_a[7  :0  ];
							E_block[1]    <=    data_out_a[15 :8  ];
							E_block[2]    <=    data_out_a[23 :16 ];
							E_block[3]    <=    data_out_a[31 :24 ];
							E_block[4]    <=    data_out_a[39 :32 ];
							E_block[5]    <=    data_out_a[47 :40 ];
							E_block[6]    <=    data_out_a[55 :48 ];
							E_block[7]    <=    data_out_a[63 :56 ];
							E_block[8]    <=    data_out_a[71 :64 ];
							E_block[9]    <=    data_out_a[79 :72 ];
							E_block[10]   <=    data_out_a[87 :80 ];
							E_block[11]   <=    data_out_a[95 :88 ];
							E_block[12]   <=    data_out_a[103:96 ];
							E_block[13]   <=    data_out_a[111:104];
							E_block[14]   <=    data_out_a[119:112];
							E_block[15]   <=    data_out_a[127:120];
						end
						// Write B,3 blocks(C,D) to BRAM in combinational block
						if((Yc>0) && (BS_h0>0) && (h0_edge_dbf_disable_in==0)) begin
							state <= STATE_3_WAIT;
						end
						else begin
							state <= STATE_4;
						end
					end
				end
				
				STATE_3 : begin
					// Assign to E block from BRAM if necessary
					if((Yc>0) && (Xc ==(pic_width_in - 8))) begin
                        E_block[0]    <=    data_out_a[7  :0  ];
                        E_block[1]    <=    data_out_a[15 :8  ];
                        E_block[2]    <=    data_out_a[23 :16 ];
                        E_block[3]    <=    data_out_a[31 :24 ];
                        E_block[4]    <=    data_out_a[39 :32 ];
                        E_block[5]    <=    data_out_a[47 :40 ];
                        E_block[6]    <=    data_out_a[55 :48 ];
                        E_block[7]    <=    data_out_a[63 :56 ];
                        E_block[8]    <=    data_out_a[71 :64 ];
                        E_block[9]    <=    data_out_a[79 :72 ];
                        E_block[10]   <=    data_out_a[87 :80 ];
                        E_block[11]   <=    data_out_a[95 :88 ];
                        E_block[12]   <=    data_out_a[103:96 ];
                        E_block[13]   <=    data_out_a[111:104];
                        E_block[14]   <=    data_out_a[119:112];
                        E_block[15]   <=    data_out_a[127:120];
                    end
					// Write B,3 blocks(C,D) to BRAM in combinational block
					if((Yc>0) && (BS_h0>0) && (h0_edge_dbf_disable_in==0)) begin
                        state <= STATE_3_WAIT;
                    end
                    else begin
                        state <= STATE_4;
                    end
				end
				STATE_3_WAIT : begin
					if(done_filter) begin
						// Assign output samples 
						if(filter_applied) begin
							if(pcm_bypass_C_dbf_disable==0) begin
								C_block[12]     <=   p_blk_0  ;
								C_block[8]      <=   p_blk_1  ;
								C_block[4]      <=   p_blk_2  ;
								C_block[0]      <=   p_blk_3  ;
								C_block[13]     <=   p_blk_4  ;
								C_block[9]      <=   p_blk_5  ;
								C_block[5]      <=   p_blk_6  ;
								C_block[1]      <=   p_blk_7  ;
								C_block[14]     <=   p_blk_8  ;
								C_block[10]     <=   p_blk_9  ;
								C_block[6]      <=   p_blk_10 ;
								C_block[2]      <=   p_blk_11 ;
								C_block[15]     <=   p_blk_12 ;
								C_block[11]     <=   p_blk_13 ;
								C_block[7]      <=   p_blk_14 ;
								C_block[3]      <=   p_blk_15 ;
							end
							if(pcm_bypass_AB_dbf_disable_in==0) begin
								A_block[0]      <=   q_blk_0  ;
								A_block[4]      <=   q_blk_1  ;
								A_block[8]      <=   q_blk_2  ;
								A_block[12]     <=   q_blk_3  ;
								A_block[1]      <=   q_blk_4  ;
								A_block[5]      <=   q_blk_5  ;
								A_block[9]      <=   q_blk_6  ;
								A_block[13]     <=   q_blk_7  ;
								A_block[2]      <=   q_blk_8  ;
								A_block[6]      <=   q_blk_9  ;
								A_block[10]     <=   q_blk_10 ;
								A_block[14]     <=   q_blk_11 ;
								A_block[3]      <=   q_blk_12 ;
								A_block[7]      <=   q_blk_13 ;
								A_block[11]     <=   q_blk_14 ;
								A_block[15]     <=   q_blk_15 ;
							end
						end
						// Write 4 block(E) to BRAM in combinational block
						if((Yc>0) && (BS_h1>0) && (hor_edge_dbf_disable_in==0)) begin
							state <= STATE_4_WAIT;
						end
						else begin
							state <= STATE_5;
						end
					end
				end
				
				STATE_4 : begin
					// Write 4 block(E) to BRAM in combinational block
					if((Yc>0) && (BS_h1>0) && (hor_edge_dbf_disable_in==0)) begin
                        state <= STATE_4_WAIT;
                    end
                    else begin
                        state <= STATE_5;
                    end
				end
				STATE_4_WAIT : begin
					if(done_filter) begin
						// Assign output samples
						if(filter_applied) begin
							if(pcm_bypass_D_dbf_disable==0) begin
								D_block[12]     <=   p_blk_0  ;
								D_block[8]      <=   p_blk_1  ;
								D_block[4]      <=   p_blk_2  ;
								D_block[0]      <=   p_blk_3  ;
								D_block[13]     <=   p_blk_4  ;
								D_block[9]      <=   p_blk_5  ;
								D_block[5]      <=   p_blk_6  ;
								D_block[1]      <=   p_blk_7  ;
								D_block[14]     <=   p_blk_8  ;
								D_block[10]     <=   p_blk_9  ;
								D_block[6]      <=   p_blk_10 ;
								D_block[2]      <=   p_blk_11 ;
								D_block[15]     <=   p_blk_12 ;
								D_block[11]     <=   p_blk_13 ;
								D_block[7]      <=   p_blk_14 ;
								D_block[3]      <=   p_blk_15 ;
							end
							if(pcm_bypass_dbf_disable_in==0) begin
								q_block_1[0]    <=   q_blk_0  ;
								q_block_1[4]    <=   q_blk_1  ;
								q_block_1[8]    <=   q_blk_2  ;
								q_block_1[12]   <=   q_blk_3  ;
								q_block_1[1]    <=   q_blk_4  ;
								q_block_1[5]    <=   q_blk_5  ;
								q_block_1[9]    <=   q_blk_6  ;
								q_block_1[13]   <=   q_blk_7  ;
								q_block_1[2]    <=   q_blk_8  ;
								q_block_1[6]    <=   q_blk_9  ;
								q_block_1[10]   <=   q_blk_10 ;
								q_block_1[14]   <=   q_blk_11 ;
								q_block_1[3]    <=   q_blk_12 ;
								q_block_1[7]    <=   q_blk_13 ;
								q_block_1[11]   <=   q_blk_14 ;
								q_block_1[15]   <=   q_blk_15 ; 
							end
						end
						// Check for edge h2 filtering
						if((Yc>0) && (BS_h2>0) && (Xc==(pic_width_in - 8)) && (hor_edge_dbf_disable_in==0)) begin
							state <= STATE_5_WAIT;
						end
						else begin
							state <= STATE_INIT;
							done  <= 1'b1;
						end
					end
				end
				
				STATE_5 : begin
					if((Yc>0) && (BS_h2>0) && (Xc==(pic_width_in - 8)) && (hor_edge_dbf_disable_in==0)) begin
                        state <= STATE_5_WAIT;
                    end
                    else begin
                        state <= STATE_INIT;
						done  <= 1'b1;
                    end
				end
				STATE_5_WAIT : begin
					if(done_filter) begin
						// Assign output samples
						if(filter_applied) begin
							if(pcm_bypass_D_dbf_disable==0) begin
								E_block[12]     <=   p_blk_0  ;
								E_block[8]      <=   p_blk_1  ;
								E_block[4]      <=   p_blk_2  ;
								E_block[0]      <=   p_blk_3  ;
								E_block[13]     <=   p_blk_4  ;
								E_block[9]      <=   p_blk_5  ;
								E_block[5]      <=   p_blk_6  ;
								E_block[1]      <=   p_blk_7  ;
								E_block[14]     <=   p_blk_8  ;
								E_block[10]     <=   p_blk_9  ;
								E_block[6]      <=   p_blk_10 ;
								E_block[2]      <=   p_blk_11 ;
								E_block[15]     <=   p_blk_12 ;
								E_block[11]     <=   p_blk_13 ;
								E_block[7]      <=   p_blk_14 ;
								E_block[3]      <=   p_blk_15 ;
							end
							if(pcm_bypass_dbf_disable_in==0) begin
								q_block_2[0]    <=   q_blk_0  ;
								q_block_2[4]    <=   q_blk_1  ;
								q_block_2[8]    <=   q_blk_2  ;
								q_block_2[12]   <=   q_blk_3  ;
								q_block_2[1]    <=   q_blk_4  ;
								q_block_2[5]    <=   q_blk_5  ;
								q_block_2[9]    <=   q_blk_6  ;
								q_block_2[13]   <=   q_blk_7  ;
								q_block_2[2]    <=   q_blk_8  ;
								q_block_2[6]    <=   q_blk_9  ;
								q_block_2[10]   <=   q_blk_10 ;
								q_block_2[14]   <=   q_blk_11 ;
								q_block_2[3]    <=   q_blk_12 ;
								q_block_2[7]    <=   q_blk_13 ;
								q_block_2[11]   <=   q_blk_14 ;
								q_block_2[15]   <=   q_blk_15 ; 
							end
						end
						state <= STATE_INIT;
						done  <= 1'b1;
					end
				end
				
            endcase
        end
    end
	
	
	//--------------- Combinational block to read and write to BRAM -----------------------
	always @(*) begin
		we_a 		= 	1'b0 ;
		en_a 		= 	1'b0 ;
		addr_a 		= 	{10{1'bx}} ;
		data_in_a	=	{SAMPLE_ROW_BUF_DATA_WIDTH{1'bx}} ;
		we_b 		= 	1'b0 ;
		en_b 		= 	1'b0 ;
		addr_b 		= 	{10{1'bx}} ;
		data_in_b	=	{SAMPLE_ROW_BUF_DATA_WIDTH{1'bx}} ;
		case(state)
			STATE_1 : begin
				if((Xc>0) && (Yc>0)) begin
					we_a 		= 	1'b0 ;
					en_a 		= 	1'b1 ;
					addr_a 		= 	(Xc[11:2] - 1'b1) ;
				end
				if(Yc>0) begin
					we_b 		= 	1'b0 ;
					en_b 		= 	1'b1 ;
					addr_b 		= 	Xc[11:2] ;
				end
			end
			STATE_1_WAIT : begin
				if(done_filter) begin
					if((Yc>0) && (Xc ==(pic_width_in - 8))) begin
						we_a 		= 	1'b0 ;
						en_a 		= 	1'b1 ;
						addr_a 		= 	(Xc[11:2] + 1'b1) ;
					end
				end
				else begin
					if((Xc>0) && (Yc>0)) begin
						we_a 		= 	1'b0 ;
						en_a 		= 	1'b1 ;
						addr_a 		= 	(Xc[11:2] - 1'b1) ;
					end
					if(Yc>0) begin
						we_b 		= 	1'b0 ;
						en_b 		= 	1'b1 ;
						addr_b 		= 	Xc[11:2] ;
					end
				end
			end
			STATE_2 : begin
				if((Yc>0) && (Xc ==(pic_width_in - 8))) begin
					we_a 		= 	1'b0 ;
					en_a 		= 	1'b1 ;
					addr_a 		= 	(Xc[11:2] + 1'b1) ;
                end
			end
			STATE_2_WAIT : begin
				if(done_filter) begin
					if(Xc>0) begin
						we_a 		= 	1'b1 ;
						en_a 		= 	1'b1 ;
						addr_a 		= 	(Xc[11:2] - 1'b1) ;
						if(filter_applied) begin
							data_in_a 	= ( {    9'b0,              // ATTN - Assign qp and other parameters
												pcm_bypass_AB_dbf_disable_in,
												qp_A,
												p_blk_12 ,
												p_blk_13 ,
												p_blk_14 ,
												p_blk_15 ,
												p_blk_8  ,
												p_blk_9  ,
												p_blk_10 ,
												p_blk_11 ,
												p_blk_4  ,
												p_blk_5  ,
												p_blk_6  ,
												p_blk_7  ,
												p_blk_0  ,
												p_blk_1  ,
												p_blk_2  ,
												p_blk_3
											} );
						end
						else begin
							data_in_a 	= ( {    9'b0,              // ATTN - Assign qp and other parameters
												pcm_bypass_AB_dbf_disable_in,
												qp_A,
												B_block[15] ,
												B_block[14] ,
												B_block[13] ,
												B_block[12] ,
												B_block[11] ,
												B_block[10] ,
												B_block[ 9] ,
												B_block[ 8] ,
												B_block[ 7] ,
												B_block[ 6] ,
												B_block[ 5] ,
												B_block[ 4] ,
												B_block[ 3] ,
												B_block[ 2] ,
												B_block[ 1] ,
												B_block[ 0]
										} );
						end
					end
					we_b 		= 	1'b1 ;
					en_b 		= 	1'b1 ;
					addr_b 		= 	Xc[11:2] ;
					if(filter_applied) begin
						data_in_b 	= ( {    9'b0,               // ATTN  -  Assign qp and other parameters
											pcm_bypass_dbf_disable_in,
											qp_1,
											q_blk_15 ,
											q_blk_14 ,
											q_blk_13 ,
											q_blk_12 ,
											q_blk_11 ,
											q_blk_10 ,
											q_blk_9  ,
											q_blk_8  ,
											q_blk_7  ,
											q_blk_6  ,
											q_blk_5  ,
											q_blk_4  ,
											q_blk_3  ,
											q_blk_2  ,
											q_blk_1  ,
											q_blk_0
										} );
					end
					else begin
						data_in_b 	= ( {    9'b0,               // ATTN  -  Assign qp and other parameters
											pcm_bypass_dbf_disable_in,
											qp_1,
											q_block_3[15] ,
											q_block_3[14] ,
											q_block_3[13] ,
											q_block_3[12] ,
											q_block_3[11] ,
											q_block_3[10] ,
											q_block_3[ 9] ,
											q_block_3[ 8] ,
											q_block_3[ 7] ,
											q_block_3[ 6] ,
											q_block_3[ 5] ,
											q_block_3[ 4] ,
											q_block_3[ 3] ,
											q_block_3[ 2] ,
											q_block_3[ 1] ,
											q_block_3[ 0]
									} );
					end
				end
				else begin
					if((Yc>0) && (Xc ==(pic_width_in - 8))) begin
						we_a 		= 	1'b0 ;
						en_a 		= 	1'b1 ;
						addr_a 		= 	(Xc[11:2] + 1'b1) ;
					end
				end
			end
			STATE_3 : begin
				if(Xc>0) begin
					we_a 		= 	1'b1 ;
					en_a 		= 	1'b1 ;
					addr_a 		= 	(Xc[11:2] - 1'b1) ;
                    data_in_a 	= ( {    9'b0,              // ATTN - Assign qp and other parameters
                                         pcm_bypass_AB_dbf_disable_in,
										 qp_A,
										B_block[15] ,
                                        B_block[14] ,
                                        B_block[13] ,
                                        B_block[12] ,
                                        B_block[11] ,
                                        B_block[10] ,
                                        B_block[ 9] ,
                                        B_block[ 8] ,
                                        B_block[ 7] ,
                                        B_block[ 6] ,
                                        B_block[ 5] ,
                                        B_block[ 4] ,
                                        B_block[ 3] ,
                                        B_block[ 2] ,
                                        B_block[ 1] ,
                                        B_block[ 0]
                                    } );
                end
				we_b 		= 	1'b1 ;
				en_b 		= 	1'b1 ;
				addr_b 		= 	Xc[11:2] ;
                data_in_b 	= ( {    9'b0,               // ATTN  -  Assign qp and other parameters
                                     pcm_bypass_dbf_disable_in,
									 qp_1,
									q_block_3[15] ,
                                    q_block_3[14] ,
                                    q_block_3[13] ,
                                    q_block_3[12] ,
                                    q_block_3[11] ,
                                    q_block_3[10] ,
                                    q_block_3[ 9] ,
                                    q_block_3[ 8] ,
                                    q_block_3[ 7] ,
                                    q_block_3[ 6] ,
                                    q_block_3[ 5] ,
                                    q_block_3[ 4] ,
                                    q_block_3[ 3] ,
                                    q_block_3[ 2] ,
                                    q_block_3[ 1] ,
                                    q_block_3[ 0]
                                } );
			end
			STATE_3_WAIT : begin
				if(done_filter) begin
					if(Xc ==(pic_width_in - 8)) begin
						we_a 		= 	1'b1 ;
						en_a 		= 	1'b1 ;
						addr_a 		= 	(Xc[11:2] + 1'b1) ;
						data_in_a 	= ( {    9'b0,              // ATTENTION    Assign qp and other parameters
											pcm_bypass_dbf_disable_in,
											qp_1,
										q_block_4[15] ,
										q_block_4[14] ,
										q_block_4[13] ,
										q_block_4[12] ,
										q_block_4[11] ,
										q_block_4[10] ,
										q_block_4[ 9] ,
										q_block_4[ 8] ,
										q_block_4[ 7] ,
										q_block_4[ 6] ,
										q_block_4[ 5] ,
										q_block_4[ 4] ,
										q_block_4[ 3] ,
										q_block_4[ 2] ,
										q_block_4[ 1] ,
										q_block_4[ 0]
									} );
					end
				end
			end
			STATE_4 : begin
				if(Xc ==(pic_width_in - 8)) begin
					we_a 		= 	1'b1 ;
					en_a 		= 	1'b1 ;
					addr_a 		= 	(Xc[11:2] + 1'b1) ;
                    data_in_a 	= ( {    9'b0,              // ATTENTION    Assign qp and other parameters
										pcm_bypass_dbf_disable_in,
										qp_1,
									q_block_4[15] ,
                                    q_block_4[14] ,
                                    q_block_4[13] ,
                                    q_block_4[12] ,
                                    q_block_4[11] ,
                                    q_block_4[10] ,
                                    q_block_4[ 9] ,
                                    q_block_4[ 8] ,
                                    q_block_4[ 7] ,
                                    q_block_4[ 6] ,
                                    q_block_4[ 5] ,
                                    q_block_4[ 4] ,
                                    q_block_4[ 3] ,
                                    q_block_4[ 2] ,
                                    q_block_4[ 1] ,
                                    q_block_4[ 0]
                                } );
                end
			end
		endcase
	end
	
	//-------------- Combinational block to activate filter module ----------------------
	always @(*) begin
		start_edge_filter  =  1'b0 ;
		edge_type_filter   =  3'bxxx ;
		case(state)
			STATE_1 : begin
				if((Xc>0) && (BS_v1>0) && (ver_edge_dbf_disable_in==0)) begin
					start_edge_filter  =  1'b1 ;
					edge_type_filter   =  VERT_UP ;
				end
				else begin
					start_edge_filter  =  1'b0 ;
					edge_type_filter   =  VERT_UP ;
				end
			end
			STATE_1_WAIT : begin
				if(done_filter) begin
					if((Xc>0) && (BS_v2>0) && (ver_edge_dbf_disable_in==0)) begin
						start_edge_filter  =  1'b1 ;
						edge_type_filter   =  VERT_DOWN ;
					end
					else begin
						start_edge_filter  =  1'b0 ;
						edge_type_filter   =  VERT_DOWN ;
					end
				end
				else begin
					start_edge_filter  =  1'b0 ;
					edge_type_filter   =  3'bxxx ;
				end
			end
			STATE_2 : begin
				if((Xc>0) && (BS_v2>0) && (ver_edge_dbf_disable_in==0)) begin
                    start_edge_filter  =  1'b1 ;
					edge_type_filter   =  VERT_DOWN ;
                end
                else begin
                    start_edge_filter  =  1'b0 ;
					edge_type_filter   =  VERT_DOWN ;
                end
			end
			STATE_2_WAIT : begin
				if(done_filter) begin
					if((Yc>0) && (BS_h0>0) && (h0_edge_dbf_disable_in==0)) begin
						start_edge_filter  =  1'b1 ;
						edge_type_filter   =  HOR_LEFT ;
					end
					else begin
						start_edge_filter  =  1'b0 ;
						edge_type_filter   =  HOR_LEFT ;
					end
				end
				else begin
					start_edge_filter  =  1'b0 ;
					edge_type_filter   =  3'bxxx ;
				end
			end
			STATE_3 : begin
				if((Yc>0) && (BS_h0>0) && (h0_edge_dbf_disable_in==0)) begin
                    start_edge_filter  =  1'b1 ;
					edge_type_filter   =  HOR_LEFT ;
                end
                else begin
                    start_edge_filter  =  1'b0 ;
					edge_type_filter   =  HOR_LEFT ;
                end
			end
			STATE_3_WAIT : begin
				if(done_filter) begin
					if((Yc>0) && (BS_h1>0) && (hor_edge_dbf_disable_in==0)) begin
						start_edge_filter  =  1'b1 ;
						edge_type_filter   =  HOR_RIGHT ;
					end
					else begin
						start_edge_filter  =  1'b0 ;
						edge_type_filter   =  HOR_RIGHT ;
					end
				end
				else begin
					start_edge_filter  =  1'b0 ;
					edge_type_filter   =  3'bxxx ;
				end
			end
			STATE_4 : begin
				if((Yc>0) && (BS_h1>0) && (hor_edge_dbf_disable_in==0)) begin
                    start_edge_filter  =  1'b1 ;
					edge_type_filter   =  HOR_RIGHT ;
                end
                else begin
                    start_edge_filter  =  1'b0 ;
					edge_type_filter   =  HOR_RIGHT ;
                end
			end
			STATE_4_WAIT : begin
				if(done_filter) begin
					if((Yc>0) && (BS_h2>0) && (Xc==(pic_width_in - 8)) && (hor_edge_dbf_disable_in==0)) begin
						start_edge_filter  =  1'b1 ;
						edge_type_filter   =  HOR_FAR_RIGHT ;
					end
					else begin
						start_edge_filter  =  1'b0 ;
						edge_type_filter   =  HOR_FAR_RIGHT ;
					end
				end
				else begin
					start_edge_filter  =  1'b0 ;
					edge_type_filter   =  3'bxxx ;
				end
			end
			STATE_5 : begin
				if((Yc>0) && (BS_h2>0) && (Xc==(pic_width_in - 8)) && (hor_edge_dbf_disable_in==0)) begin
                    start_edge_filter  =  1'b1 ;
					edge_type_filter   =  HOR_FAR_RIGHT ;
                end
                else begin
                    start_edge_filter  =  1'b0 ;
					edge_type_filter   =  HOR_FAR_RIGHT ;
                end
			end
			
			default : begin
				start_edge_filter  =  1'b0 ;
				edge_type_filter   =  3'bxxx ;
			end
		endcase
	end
   

endmodule
