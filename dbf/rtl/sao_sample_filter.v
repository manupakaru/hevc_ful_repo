`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:08:22 06/26/2014 
// Design Name: 
// Module Name:    sao_sample_filter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module sao_sample_filter(
		clk,
		reset,
		pix_1_in,
		pix_2_in,
		pix_3_in,
		pix_4_in,
		pix_5_in,
		pix_6_in,
		pix_7_in,
		pix_8_in,
		pix_c_in,
		sao_type,
		sao_eo_class,
		sao_bandpos,
		sao_offset_1,
		sao_offset_2,
		sao_offset_3,
		sao_offset_4,
		pix_c_out
    );
	
	`include "../sim/pred_def.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
	
	
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
	
	
	
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
	input                       	clk;
    input                       	reset;
	
	// Input sample values
	input		[BIT_DEPTH-1:0]					pix_1_in ;
	input		[BIT_DEPTH-1:0]					pix_2_in ;
	input		[BIT_DEPTH-1:0]					pix_3_in ;
	input		[BIT_DEPTH-1:0]					pix_4_in ;
	input		[BIT_DEPTH-1:0]					pix_5_in ;
	input		[BIT_DEPTH-1:0]					pix_6_in ;
	input		[BIT_DEPTH-1:0]					pix_7_in ;
	input		[BIT_DEPTH-1:0]					pix_8_in ;
	input		[BIT_DEPTH-1:0]					pix_c_in ;
	
	// SAO parameter inputs
	input		[1:0]							sao_type ;
	input		[1:0]							sao_eo_class;
	input		[4:0]							sao_bandpos;
	input		[SAO_OFFSET_WIDTH -1:0]			sao_offset_1;
	input		[SAO_OFFSET_WIDTH -1:0]			sao_offset_2;
	input		[SAO_OFFSET_WIDTH -1:0]			sao_offset_3;
	input		[SAO_OFFSET_WIDTH -1:0]			sao_offset_4;
	
	// SAO output sample
	output		[BIT_DEPTH-1:0]					pix_c_out;
	
	
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    
	reg			[BIT_DEPTH-1:0]					pix_a;
	reg			[BIT_DEPTH-1:0]					pix_b;
	reg			[SAO_OFFSET_WIDTH -1:0]			sao_bo_offset;
	reg			[SAO_OFFSET_WIDTH -1:0]			sao_eo_offset;
	reg			[SAO_OFFSET_WIDTH -1:0]			sao_offset;
	reg			[1:0]							sign_1;
	reg			[1:0]							sign_2;
	reg			[2:0]							edge_idx;
	reg			[BIT_DEPTH-1:0]					sao_clip_sample;
	
	wire		[4:0]							pix_band;
	wire		[3:0]							band_type;
	wire		[BIT_DEPTH:0]					sao_add_sample;
	
	
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
    
	//------------ SAO Band offset calculation -----------------------------//
	                                                                        //
	assign	pix_band = pix_c_in >> (BIT_DEPTH-5) ;                          //
	                                                                        //
	assign	band_type  =  { 	( pix_band == sao_bandpos 	  	 ) ,        //
								( pix_band == sao_bandpos + 3'd1 ) ,        //
								( pix_band == sao_bandpos + 3'd2 ) ,        //
								( pix_band == sao_bandpos + 3'd3 )          //
						  } ;                                               //
	                                                                        //
	always @(*) begin                                                       //
		case(band_type)                                                     //
			4'b1000 : begin                                                 //
				sao_bo_offset = sao_offset_1 ;                              //
			end                                                             //
			4'b0100 : begin                                                 //
				sao_bo_offset = sao_offset_2 ;                              //
			end                                                             //
			4'b0010 : begin                                                 //
				sao_bo_offset = sao_offset_3 ;                              //
			end                                                             //
			4'b0001 : begin                                                 //
				sao_bo_offset = sao_offset_4 ;                              //
			end                                                             //
			default : begin                                                 //
				sao_bo_offset = 4'd0;                                       //
			end                                                             //
		endcase                                                             //
	end                                                                     //
	                                                                        //
	//----------------------------------------------------------------------//
	
	
	//------------- SAO Edge offset calculation ------------------------//
	                                                                    //
	always @(*) begin                                                   //
		case(sao_eo_class)                                              //
			2'b00 : begin                                               //
				pix_a  =  pix_4_in ;                                    //
				pix_b  =  pix_5_in ;                                    //
			end                                                         //
			2'b01 : begin                                               //
				pix_a  =  pix_2_in ;                                    //
				pix_b  =  pix_7_in ;                                    //
			end                                                         //
			2'b10 : begin                                               //
				pix_a  =  pix_1_in ;                                    //
				pix_b  =  pix_8_in ;                                    //
			end                                                         //
			2'b11 : begin                                               //
				pix_a  =  pix_3_in ;                                    //
				pix_b  =  pix_6_in ;                                    //
			end                                                         //
		endcase                                                         //
	end                                                                 //
	                                                                    //
	always @(*) begin                                                   //
		if(pix_c_in == pix_a) begin                                     //
			sign_1 = 2'b00 ;                                            //
		end                                                             //
		else if(pix_c_in < pix_a) begin                                 //
			sign_1 = 2'b11 ;                                            //
		end                                                             //
		else begin                                                      //
			sign_1 = 2'b01 ;                                            //
		end                                                             //
		                                                                //
		if(pix_c_in == pix_b) begin                                     //
			sign_2 = 2'b00 ;                                            //
		end                                                             //
		else if(pix_c_in < pix_b) begin                                 //
			sign_2 = 2'b11 ;                                            //
		end                                                             //
		else begin                                                      //
			sign_2 = 2'b01 ;                                            //
		end                                                             //
	end                                                                 //
	                                                                    //
	always @(*) begin                                                   //
		case({sign_1,sign_2})                                           //
			4'b0000 : begin                                             //
				edge_idx = 3'd0 ;										//
			end                                                         //
			4'b0001 : begin                                             //
				edge_idx = 3'd3 ;                                       //
			end                                                         //
			4'b0011 : begin                                             //
				edge_idx = 3'd2 ;                                       //
			end                                                         //
			4'b0100 : begin                                             //
				edge_idx = 3'd3 ;                                       //
			end                                                         //
			4'b0101 : begin                                             //
				edge_idx = 3'd4 ;                                       //
			end                                                         //
			4'b0111 : begin                                             //
				edge_idx = 3'd0 ;                                       //
			end                                                         //
			4'b1100 : begin                                             //
				edge_idx = 3'd2 ;                                       //
			end                                                         //
			4'b1101 : begin                                             //
				edge_idx = 3'd0 ;                                       //
			end                                                         //
			4'b1111 : begin                                             //
				edge_idx = 3'd1 ;                                       //
			end                                                         //
			default : begin                                             //
				edge_idx = 3'd0 ;                                       //
			end                                                         //
		endcase                                                         //
	end                                                                 //
	                                                                    //
	always @(*) begin                                                   //
		case(edge_idx)                                                  //
			3'd1 : begin                                                //
				sao_eo_offset = sao_offset_1 ;                          //
			end                                                         //
			3'd2 : begin                                                //
				sao_eo_offset = sao_offset_2 ;                          //
			end                                                         //
			3'd3 : begin                                                //
				sao_eo_offset = sao_offset_3 ;                          //
			end                                                         //
			3'd4 : begin                                                //
				sao_eo_offset = sao_offset_4 ;                          //
			end                                                         //
			default : begin                                             //
				sao_eo_offset = 4'd0 ;                                  //
			end                                                         //
		endcase                                                         //
	end                                                                 //
	                                                                    //
	//------------------------------------------------------------------//
	
	
	//----------- Final SAO offset addition -----------------------------------
	
	always @(*) begin
		case(sao_type)
			2'd1 : begin
				sao_offset = sao_bo_offset ;
			end
			2'd2 : begin
				sao_offset = sao_eo_offset ;
			end
			default : begin
				sao_offset = 4'd0 ;
			end
		endcase
	end
	
	assign	sao_add_sample  =  {1'b0,pix_c_in}  +  { {(2+BIT_DEPTH-SAO_OFFSET_WIDTH){sao_offset[SAO_OFFSET_WIDTH-1]}}, sao_offset[SAO_OFFSET_WIDTH-2:0]} ;
	
	always @(*) begin
		if(sao_offset[SAO_OFFSET_WIDTH-1]==0) begin
			sao_clip_sample  =  (sao_add_sample[BIT_DEPTH]==1)  ?  {(BIT_DEPTH){1'b1}}  :  sao_add_sample[BIT_DEPTH-1:0] ;
		end
		else begin
			sao_clip_sample  =  (sao_add_sample[BIT_DEPTH]==1)  ?  {(BIT_DEPTH){1'b0}}  :  sao_add_sample[BIT_DEPTH-1:0] ;
		end
	end
	
	assign	pix_c_out  =  sao_clip_sample ;
	
	


endmodule
