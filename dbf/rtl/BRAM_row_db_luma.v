`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:58:13 11/21/2013 
// Design Name: 
// Module Name:    BRAM_row_db_luma 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module BRAM_row_db_luma(
        clk,
        we_a,
        we_b,
        en_a,
        en_b,
        addr_a,
        addr_b,
        data_in_a,
        data_in_b,
        data_out_a,
        data_out_b
    );
    
    
    input                       clk;
    input                       we_a;
    input                       we_b;
    input                       en_a;
    input                       en_b;
    input           [8:0]       addr_a;
    input           [8:0]       addr_b;
    input           [143:0]     data_in_a;
    input           [143:0]     data_in_b;
    output reg      [143:0]     data_out_a;
    output reg      [143:0]     data_out_b;
    
    reg             [143:0]     RAM [511:0];
    
    always @(posedge clk) begin
        if(en_a) begin
            if(we_a) begin
                RAM[addr_a] <= data_in_a;
            end
            else begin
                data_out_a <= RAM[addr_a];
            end
        end
        if(en_b) begin
            if(we_b) begin
                RAM[addr_b] <= data_in_b;
            end
            else begin
                data_out_b <= RAM[addr_b];
            end
        end
    end
    

endmodule
