`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:04:50 05/20/2014 
// Design Name: 
// Module Name:    luma_sao_controller 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module luma_sao_controller(
		clk,
        reset,
        start,
        done,
		q_block_1_in,
        q_block_2_in,
        q_block_3_in,
        q_block_4_in,
        A_block_in,
        B_block_in,
		C_block_in,
		D_block_in,
		E_block_in,
		Xc_in,
		Yc_in,
		pic_width_in,
        pic_height_in,
		slice_id_in,
		sao_luma_in,
		tile_left_boundary_in ,
		tile_top_boundary_in  ,    
		slice_left_boundary_in,    
		slice_top_boundary_in ,    
		pcm_bypass_sao_disable_in,  
		loop_filter_accross_tiles_enabled_in ,
		loop_filter_accross_slices_enabled_in,
		y_sao_type_in	 ,
		y_eoclass_in     ,
		y_bandpos_in     ,
		y_sao_offset_1_in,
		y_sao_offset_2_in,
		y_sao_offset_3_in,
		y_sao_offset_4_in,
		
		q_block_1_out,
		q_block_2_out,
		q_block_3_out,
		q_block_4_out,
		A_block_out  ,
		B_block_out  ,
		C_block_out  ,
		D_block_out  ,
		E_block_out  ,
		J_block_out	 ,
		K_block_out	 ,
		M_block_out	 ,
		N_block_out	 ,
		G_block_out	 ,
		H_block_out	 ,
		I_block_out	
		//,luma_con_in_valid
		//,luma_con_out_valid	
		//,luma_con_in_data
		//,luma_con_out_data	
		
    );
	
	`include "../sim/pred_def.v"
	
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input                               clk;
    input                               reset;
    
	input                               start;
    output	reg					        done;
	
	// Pixel inputs
	input       	[127:0]         q_block_1_in ;
    input       	[127:0]         q_block_2_in ;
    input       	[127:0]         q_block_3_in ;
    input       	[127:0]         q_block_4_in ;
    input       	[127:0]         A_block_in   ;
    input       	[127:0]         B_block_in   ;
    input			[127:0]         C_block_in   ;
    input			[127:0]         D_block_in   ;
    input			[127:0]         E_block_in   ;
	
	// Other inputs
	input			[11:0]			Xc_in ;
	input			[11:0]			Yc_in ;
	input			[11:0]			pic_width_in ;
	input			[11:0]			pic_height_in;
	input			[7:0]			slice_id_in;
	input							sao_luma_in;
	input							tile_left_boundary_in  ;
	input							tile_top_boundary_in   ;    
	input							slice_left_boundary_in ;    
	input							slice_top_boundary_in  ;    
	input							pcm_bypass_sao_disable_in ;  
	input							loop_filter_accross_tiles_enabled_in ;
	input							loop_filter_accross_slices_enabled_in;
	
	input			[1:0]   							y_sao_type_in	  ;
	input			[1:0]								y_eoclass_in      ;
	input			[4:0]								y_bandpos_in      ;
	input			[SAO_OFFSET_WIDTH -1:0]				y_sao_offset_1_in ;
	input			[SAO_OFFSET_WIDTH -1:0]				y_sao_offset_2_in ;
	input			[SAO_OFFSET_WIDTH -1:0]				y_sao_offset_3_in ;
	input			[SAO_OFFSET_WIDTH -1:0]				y_sao_offset_4_in ;
	
	// Pixel outputs
	output       	[127:0]         q_block_1_out ;
    output       	[127:0]         q_block_2_out ;
    output       	[127:0]         q_block_3_out ;
    output       	[127:0]         q_block_4_out ;
    output       	[127:0]         A_block_out   ;
    output       	[127:0]         B_block_out   ;
    output			[127:0]         C_block_out   ;
    output			[127:0]         D_block_out   ;
    output 			[127:0]         E_block_out   ;
	output			[127:0]			J_block_out	  ;
	output			[127:0]			K_block_out	  ;
	output			[127:0]			M_block_out	  ;
	output			[127:0]			N_block_out	  ;
	output			[127:0]			G_block_out	  ;
	output			[127:0]			H_block_out	  ;
	output			[127:0]			I_block_out	  ;
	
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    
    localparam                          STATE_INIT        	= 0;    // the state that is entered upon reset
    localparam                          STATE_1           	= 1;
	localparam                          STATE_2           	= 2;
	localparam                          STATE_3           	= 3;
	localparam                          STATE_4           	= 4;
	localparam                          STATE_5           	= 5;
	localparam                          STATE_6           	= 6;
	localparam                          STATE_7           	= 7;
	localparam                          STATE_8           	= 8;
	localparam                          STATE_9           	= 9;
	localparam                          STATE_10          	= 10;
	localparam                          STATE_11           	= 11;
	localparam                          STATE_12           	= 12;
	localparam                          STATE_13           	= 13;
	localparam                          STATE_14           	= 14;
	localparam                          STATE_15           	= 15;
	localparam                          STATE_16           	= 16;
	localparam                          STATE_17           	= 17;
	localparam                          STATE_18           	= 18;
	localparam                          STATE_19           	= 19;
	localparam                          STATE_20          	= 20;
	localparam                          STATE_21           	= 21;
	localparam                          STATE_22           	= 22;
	localparam                          STATE_23           	= 23;
	localparam                          STATE_24           	= 24;
	localparam                          STATE_25           	= 25;
	localparam                          STATE_26           	= 26;
	localparam                          STATE_27           	= 27;
	localparam                          STATE_28           	= 28;
	localparam                          STATE_29           	= 29;
	localparam                          STATE_30          	= 30;
	localparam                          STATE_31           	= 31;
	localparam                          STATE_32           	= 32;
	localparam                          STATE_33           	= 33;
	localparam                          STATE_34           	= 34;
	localparam                          STATE_35           	= 35;
	localparam                          STATE_36           	= 36;
	
	localparam			SAMPLE_COL_BUF_DATA_WIDTH			=	((BIT_DEPTH*16) + 40) ;
	localparam			SAMPLE_ROW_BUF_DATA_WIDTH			=	((BIT_DEPTH*32) + 40) ;
	localparam			DECISION_ROW_BUF_DATA_WIDTH			=	((BIT_DEPTH*10) + 10) ;
	localparam			LEFT_DECISION_COL_BUF_DATA_WIDTH	=	((BIT_DEPTH*9 ) + 9 ) ;
	localparam			RIGHT_DECISION_COL_BUF_DATA_WIDTH	=	((BIT_DEPTH*4 ) + 11) ;
    
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    
	reg             [7:0]           		q_block_1 [15:0] ;
    reg             [7:0]           		q_block_2 [15:0] ;
    reg             [7:0]           		q_block_3 [15:0] ;
    reg             [7:0]           		q_block_4 [15:0] ;
    reg             [7:0]           		A_block   [15:0] ;
    reg             [7:0]           		B_block   [15:0] ;
    reg             [7:0]           		C_block   [15:0] ;
    reg             [7:0]           		D_block   [15:0] ;
    reg             [7:0]           		E_block   [15:0] ;
	reg             [7:0]           		J_block   [15:0] ;
    reg             [7:0]           		K_block   [15:0] ;
    reg             [7:0]           		M_block   [15:0] ;
    reg             [7:0]           		N_block   [15:0] ;
	reg             [7:0]           		G_block   [15:0] ;
    reg             [7:0]           		H_block   [15:0] ;
    reg             [7:0]           		I_block   [15:0] ;
	
	reg				[7:0]					decision_row		[9:0] ;
	reg				[7:0]					decision_col_left 	[8:0] ;
	reg				[7:0]					decision_col_right 	[3:0] ;
	
	wire			[XC_WIDTH-4:0]			J_block_X8 ;
	wire			[XC_WIDTH-4:0]			J_block_Y8 ;
	wire			[XC_WIDTH-4:0]			H_block_X8 ;
	wire			[XC_WIDTH-4:0]			H_block_Y8 ;
	wire			[XC_WIDTH-4:0]			M_block_X8 ;
	wire			[XC_WIDTH-4:0]			M_block_Y8 ;
	wire			[XC_WIDTH-4:0]			J_block_TR_X8 ;
	wire			[XC_WIDTH-4:0]			J_block_TR_Y8 ;
	wire			[XC_WIDTH-4:0]			J_block_BL_X8 ;
	wire			[XC_WIDTH-4:0]			J_block_BL_Y8 ;
	
	wire			[2*XC_WIDTH-9:0]		J_block_zs_addr	;
	wire			[2*XC_WIDTH-9:0]		H_block_zs_addr	;
	wire			[2*XC_WIDTH-9:0]		M_block_zs_addr	;
	wire			[2*XC_WIDTH-9:0]		J_block_TR_zs_addr ;
	wire			[2*XC_WIDTH-9:0]		J_block_BL_zs_addr ;
	wire			[2*XC_WIDTH-9:0]		H_block_TR_zs_addr ;
	wire			[2*XC_WIDTH-9:0]		M_block_BL_zs_addr ;
	
	reg										H_block_sao_across_slices_enable_flag ;
	reg										H_block_pcm_bypass_sao_disable_flag ;
	reg										H_block_tile_left_flag	;
	reg										H_block_tile_top_flag   ;
	reg										H_block_slice_left_flag ;
	reg										H_block_slice_top_flag  ;
	reg										H_block_sao_luma		;
	reg				[1:0]					H_block_y_sao_type      ;
	reg				[1:0]					H_block_y_eoclass       ;
	reg				[4:0]					H_block_y_bandpos       ;
	reg				[3:0]					H_block_y_sao_offset_1  ;
	reg				[3:0]					H_block_y_sao_offset_2  ;
	reg				[3:0]					H_block_y_sao_offset_3  ;
	reg				[3:0]					H_block_y_sao_offset_4  ;
	reg				[7:0]					H_block_slice_id  		;
	
	reg										J_block_sao_across_slices_enable_flag ;
	reg										J_block_pcm_bypass_sao_disable_flag ;
	reg										J_block_tile_left_flag	;
	reg										J_block_tile_top_flag   ;
	reg										J_block_slice_left_flag ;
	reg										J_block_slice_top_flag  ;
	reg										J_block_sao_luma		;
	reg				[1:0]					J_block_y_sao_type      ;
	reg				[1:0]					J_block_y_eoclass       ;
	reg				[4:0]					J_block_y_bandpos       ;
	reg				[3:0]					J_block_y_sao_offset_1  ;
	reg				[3:0]					J_block_y_sao_offset_2  ;
	reg				[3:0]					J_block_y_sao_offset_3  ;
	reg				[3:0]					J_block_y_sao_offset_4  ;
	reg				[7:0]					J_block_slice_id  		;
	
	reg										M_block_sao_across_slices_enable_flag ;
	reg										M_block_pcm_bypass_sao_disable_flag ;
	reg										M_block_tile_left_flag	;
	reg										M_block_tile_top_flag   ;
	reg										M_block_slice_left_flag ;
	reg										M_block_slice_top_flag  ;
	reg										M_block_sao_luma		;
	reg				[1:0]					M_block_y_sao_type      ;
	reg				[1:0]					M_block_y_eoclass       ;
	reg				[4:0]					M_block_y_bandpos       ;
	reg				[3:0]					M_block_y_sao_offset_1  ;
	reg				[3:0]					M_block_y_sao_offset_2  ;
	reg				[3:0]					M_block_y_sao_offset_3  ;
	reg				[3:0]					M_block_y_sao_offset_4  ;
	reg				[7:0]					M_block_slice_id  		;
	
	reg										decision_top_left_slice_boundary ;
	reg										decision_top_right_sao_across_slices_enable ;
	reg										decision_bottom_left_sao_across_slices_enable ;
	reg				[7:0]					decision_top_right_slice_id ;
	reg				[7:0]					decision_bottom_left_slice_id ;
	
	reg										J_block_sao_apply ;
	reg										H_block_sao_apply ;
	reg										M_block_sao_apply ;
	reg										q1_block_sao_apply ;
	
	reg										J_block_top_right_sao_across_slices_enable_flag	  ;
	reg										J_block_bottom_left_sao_across_slices_enable_flag ;
	reg										H_block_top_right_sao_across_slices_enable_flag   ;
	reg										M_block_bottom_left_sao_across_slices_enable_flag ;
	
	wire									J_block_cross_boundary_left			;
	wire									J_block_cross_boundary_right		;
	wire									J_block_cross_boundary_top			;
	wire									J_block_cross_boundary_bottom	    ;
	wire									J_block_cross_boundary_top_left		;
	wire									J_block_cross_boundary_top_right	;
	wire									J_block_cross_boundary_bottom_left	;
	wire									J_block_cross_boundary_bottom_right	;
	
	wire									H_block_cross_boundary_left			;
	wire									H_block_cross_boundary_right		;
	wire									H_block_cross_boundary_top			;
	wire									H_block_cross_boundary_bottom	    ;
	wire									H_block_cross_boundary_top_left		;
	wire									H_block_cross_boundary_top_right	;
	wire									H_block_cross_boundary_bottom_left	;
	wire									H_block_cross_boundary_bottom_right	;
	
	wire									M_block_cross_boundary_left			;
	wire									M_block_cross_boundary_right		;
	wire									M_block_cross_boundary_top			;
	wire									M_block_cross_boundary_bottom	    ;
	wire									M_block_cross_boundary_top_left		;
	wire									M_block_cross_boundary_top_right	;
	wire									M_block_cross_boundary_bottom_left	;
	wire									M_block_cross_boundary_bottom_right	;
	
	wire									q_block_cross_boundary_left			;
	wire									q_block_cross_boundary_right		;
	wire									q_block_cross_boundary_top			;
	wire									q_block_cross_boundary_bottom	    ;
	wire									q_block_cross_boundary_top_left		;
	wire									q_block_cross_boundary_top_right	;
	wire									q_block_cross_boundary_bottom_left	;
	wire									q_block_cross_boundary_bottom_right	;
	
	// SAO Block Filter connections
	reg										filter_1_start_sao_filter ;
	reg										filter_1_cross_boundary_left_flag_sao   ;
	reg										filter_1_cross_boundary_right_flag_sao  ;
	reg										filter_1_cross_boundary_top_flag_sao	   ;
	reg										filter_1_cross_boundary_bottom_flag_sao ;
	reg										filter_1_cross_boundary_top_left_flag_sao	;
	reg										filter_1_cross_boundary_top_right_flag_sao	;
	reg										filter_1_cross_boundary_bottom_left_flag_sao ;
	reg										filter_1_cross_boundary_bottom_right_flag_sao;
	reg				[7:0]					filter_1_p_0_0_in_sao ;
	reg				[7:0]					filter_1_p_0_1_in_sao ;
	reg				[7:0]					filter_1_p_0_2_in_sao ;
	reg				[7:0]					filter_1_p_0_3_in_sao ;
	reg				[7:0]					filter_1_p_0_4_in_sao ;
	reg				[7:0]					filter_1_p_0_5_in_sao ;
	reg				[7:0]					filter_1_p_1_0_in_sao ;
	reg				[7:0]					filter_1_p_1_1_in_sao ;
	reg				[7:0]					filter_1_p_1_2_in_sao ;
	reg				[7:0]					filter_1_p_1_3_in_sao ;
	reg				[7:0]					filter_1_p_1_4_in_sao ;
	reg				[7:0]					filter_1_p_1_5_in_sao ;
	reg				[7:0]					filter_1_p_2_0_in_sao ;
	reg				[7:0]					filter_1_p_2_1_in_sao ;
	reg				[7:0]					filter_1_p_2_2_in_sao ;
	reg				[7:0]					filter_1_p_2_3_in_sao ;
	reg				[7:0]					filter_1_p_2_4_in_sao ;
	reg				[7:0]					filter_1_p_2_5_in_sao ;
	reg				[7:0]					filter_1_p_3_0_in_sao ;
	reg				[7:0]					filter_1_p_3_1_in_sao ;
	reg				[7:0]					filter_1_p_3_2_in_sao ;
	reg				[7:0]					filter_1_p_3_3_in_sao ;
	reg				[7:0]					filter_1_p_3_4_in_sao ;
	reg				[7:0]					filter_1_p_3_5_in_sao ;
	reg				[7:0]                   filter_1_p_4_0_in_sao ;
	reg				[7:0]                   filter_1_p_4_1_in_sao ;
	reg				[7:0]                   filter_1_p_4_2_in_sao ;
	reg				[7:0]                   filter_1_p_4_3_in_sao ;
	reg				[7:0]                   filter_1_p_4_4_in_sao ;
	reg				[7:0]                   filter_1_p_4_5_in_sao ;
	reg				[7:0]                   filter_1_p_5_0_in_sao ;
	reg				[7:0]                   filter_1_p_5_1_in_sao ;
	reg				[7:0]                   filter_1_p_5_2_in_sao ;
	reg				[7:0]                   filter_1_p_5_3_in_sao ;
	reg				[7:0]                   filter_1_p_5_4_in_sao ;
	reg				[7:0]                   filter_1_p_5_5_in_sao ;
	reg				[1:0]					filter_1_sao_type_in_sao	 ;
	reg				[1:0]                   filter_1_sao_eo_class_in_sao ;
	reg				[4:0]                   filter_1_sao_bandpos_in_sao  ;
	reg				[3:0]                   filter_1_sao_offset_1_in_sao ;
	reg				[3:0]                   filter_1_sao_offset_2_in_sao ;
	reg				[3:0]                   filter_1_sao_offset_3_in_sao ;
	reg				[3:0]                   filter_1_sao_offset_4_in_sao ;
	wire			[7:0]					filter_1_pix_1_1_out_sao ;
	wire			[7:0]                   filter_1_pix_1_2_out_sao ;
	wire			[7:0]                   filter_1_pix_1_3_out_sao ;
	wire			[7:0]                   filter_1_pix_1_4_out_sao ;
	wire			[7:0]                   filter_1_pix_2_1_out_sao ;
	wire			[7:0]                   filter_1_pix_2_2_out_sao ;
	wire			[7:0]                   filter_1_pix_2_3_out_sao ;
	wire			[7:0]                   filter_1_pix_2_4_out_sao ;
	wire			[7:0]                   filter_1_pix_3_1_out_sao ;
	wire			[7:0]                   filter_1_pix_3_2_out_sao ;
	wire			[7:0]                   filter_1_pix_3_3_out_sao ;
	wire			[7:0]                   filter_1_pix_3_4_out_sao ;
	wire			[7:0]                   filter_1_pix_4_1_out_sao ;
	wire			[7:0]                   filter_1_pix_4_2_out_sao ;
	wire			[7:0]                   filter_1_pix_4_3_out_sao ;
	wire			[7:0]                   filter_1_pix_4_4_out_sao ;
	
	reg										filter_2_start_sao_filter ;
	reg										filter_2_cross_boundary_left_flag_sao   ;
	reg										filter_2_cross_boundary_right_flag_sao  ;
	reg										filter_2_cross_boundary_top_flag_sao	   ;
	reg										filter_2_cross_boundary_bottom_flag_sao ;
	reg										filter_2_cross_boundary_top_left_flag_sao	;
	reg										filter_2_cross_boundary_top_right_flag_sao	;
	reg										filter_2_cross_boundary_bottom_left_flag_sao ;
	reg										filter_2_cross_boundary_bottom_right_flag_sao;
	reg				[7:0]					filter_2_p_0_0_in_sao ;
	reg				[7:0]					filter_2_p_0_1_in_sao ;
	reg				[7:0]					filter_2_p_0_2_in_sao ;
	reg				[7:0]					filter_2_p_0_3_in_sao ;
	reg				[7:0]					filter_2_p_0_4_in_sao ;
	reg				[7:0]					filter_2_p_0_5_in_sao ;
	reg				[7:0]					filter_2_p_1_0_in_sao ;
	reg				[7:0]					filter_2_p_1_1_in_sao ;
	reg				[7:0]					filter_2_p_1_2_in_sao ;
	reg				[7:0]					filter_2_p_1_3_in_sao ;
	reg				[7:0]					filter_2_p_1_4_in_sao ;
	reg				[7:0]					filter_2_p_1_5_in_sao ;
	reg				[7:0]					filter_2_p_2_0_in_sao ;
	reg				[7:0]					filter_2_p_2_1_in_sao ;
	reg				[7:0]					filter_2_p_2_2_in_sao ;
	reg				[7:0]					filter_2_p_2_3_in_sao ;
	reg				[7:0]					filter_2_p_2_4_in_sao ;
	reg				[7:0]					filter_2_p_2_5_in_sao ;
	reg				[7:0]					filter_2_p_3_0_in_sao ;
	reg				[7:0]					filter_2_p_3_1_in_sao ;
	reg				[7:0]					filter_2_p_3_2_in_sao ;
	reg				[7:0]					filter_2_p_3_3_in_sao ;
	reg				[7:0]					filter_2_p_3_4_in_sao ;
	reg				[7:0]					filter_2_p_3_5_in_sao ;
	reg				[7:0]                   filter_2_p_4_0_in_sao ;
	reg				[7:0]                   filter_2_p_4_1_in_sao ;
	reg				[7:0]                   filter_2_p_4_2_in_sao ;
	reg				[7:0]                   filter_2_p_4_3_in_sao ;
	reg				[7:0]                   filter_2_p_4_4_in_sao ;
	reg				[7:0]                   filter_2_p_4_5_in_sao ;
	reg				[7:0]                   filter_2_p_5_0_in_sao ;
	reg				[7:0]                   filter_2_p_5_1_in_sao ;
	reg				[7:0]                   filter_2_p_5_2_in_sao ;
	reg				[7:0]                   filter_2_p_5_3_in_sao ;
	reg				[7:0]                   filter_2_p_5_4_in_sao ;
	reg				[7:0]                   filter_2_p_5_5_in_sao ;
	reg				[1:0]					filter_2_sao_type_in_sao	 ;
	reg				[1:0]                   filter_2_sao_eo_class_in_sao ;
	reg				[4:0]                   filter_2_sao_bandpos_in_sao  ;
	reg				[3:0]                   filter_2_sao_offset_1_in_sao ;
	reg				[3:0]                   filter_2_sao_offset_2_in_sao ;
	reg				[3:0]                   filter_2_sao_offset_3_in_sao ;
	reg				[3:0]                   filter_2_sao_offset_4_in_sao ;
	wire			[7:0]					filter_2_pix_1_1_out_sao ;
	wire			[7:0]                   filter_2_pix_1_2_out_sao ;
	wire			[7:0]                   filter_2_pix_1_3_out_sao ;
	wire			[7:0]                   filter_2_pix_1_4_out_sao ;
	wire			[7:0]                   filter_2_pix_2_1_out_sao ;
	wire			[7:0]                   filter_2_pix_2_2_out_sao ;
	wire			[7:0]                   filter_2_pix_2_3_out_sao ;
	wire			[7:0]                   filter_2_pix_2_4_out_sao ;
	wire			[7:0]                   filter_2_pix_3_1_out_sao ;
	wire			[7:0]                   filter_2_pix_3_2_out_sao ;
	wire			[7:0]                   filter_2_pix_3_3_out_sao ;
	wire			[7:0]                   filter_2_pix_3_4_out_sao ;
	wire			[7:0]                   filter_2_pix_4_1_out_sao ;
	wire			[7:0]                   filter_2_pix_4_2_out_sao ;
	wire			[7:0]                   filter_2_pix_4_3_out_sao ;
	wire			[7:0]                   filter_2_pix_4_4_out_sao ;
	
	
	// Interconnect wires
	reg 													row_en_a 		;
	reg 													row_we_a 		;
	reg 			[8:0]									row_addr_a 		;
	reg 			[SAMPLE_ROW_BUF_DATA_WIDTH-1:0]			row_data_in_a  	;
	wire			[SAMPLE_ROW_BUF_DATA_WIDTH-1:0]			row_data_out_a 	;
	
	reg 													col_we_a 		;
	reg 													col_en_a 		;
	reg 													col_we_b 		;
	reg 													col_en_b 		;
	reg 			[9:0]									col_addr_a 		;
	reg 			[9:0]									col_addr_b 		;
	reg 			[SAMPLE_COL_BUF_DATA_WIDTH-1:0]			col_data_in_a  	;
	reg 			[SAMPLE_COL_BUF_DATA_WIDTH-1:0]			col_data_in_b  	;
	wire			[SAMPLE_COL_BUF_DATA_WIDTH-1:0]			col_data_out_a 	;
	wire			[SAMPLE_COL_BUF_DATA_WIDTH-1:0]			col_data_out_b 	;
	
	reg															d_row_buf_we_a       ;
	reg		        											d_row_buf_en_a       ;
	reg		  		[8:0]										d_row_buf_addr_a     ;
	reg				[DECISION_ROW_BUF_DATA_WIDTH-1:0]  			d_row_buf_data_in_a  ;
	wire	    	[DECISION_ROW_BUF_DATA_WIDTH-1:0]  			d_row_buf_data_out_a ;
	
	reg															d_left_col_buf_we_a       ;
	reg		        											d_left_col_buf_en_a       ;
	reg		  		[8:0]										d_left_col_buf_addr_a     ;
	reg				[LEFT_DECISION_COL_BUF_DATA_WIDTH-1:0]  	d_left_col_buf_data_in_a  ;
	wire	    	[LEFT_DECISION_COL_BUF_DATA_WIDTH-1:0]  	d_left_col_buf_data_out_a ;
	
	reg															d_right_col_buf_we_a       ;
	reg		        											d_right_col_buf_en_a       ;
	reg		  		[8:0]										d_right_col_buf_addr_a     ;
	reg				[RIGHT_DECISION_COL_BUF_DATA_WIDTH-1:0]  	d_right_col_buf_data_in_a  ;
	wire	    	[RIGHT_DECISION_COL_BUF_DATA_WIDTH-1:0]  	d_right_col_buf_data_out_a ;
	
	integer 								state;
    
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
    
	//------------ Output assignments -----------------------
	
    assign  q_block_1_out = ( { q_block_1[15],   
                             q_block_1[14],
                             q_block_1[13],
                             q_block_1[12],
                             q_block_1[11],
                             q_block_1[10],
                             q_block_1[9 ],
                             q_block_1[8 ],
                             q_block_1[7 ],
                             q_block_1[6 ],
                             q_block_1[5 ],
                             q_block_1[4 ],
                             q_block_1[3 ],
                             q_block_1[2 ],
                             q_block_1[1 ],
                             q_block_1[0 ]  }
                );         
    assign  q_block_2_out = ( { q_block_2[15],   
                             q_block_2[14],
                             q_block_2[13],
                             q_block_2[12],
                             q_block_2[11],
                             q_block_2[10],
                             q_block_2[9 ],
                             q_block_2[8 ],
                             q_block_2[7 ],
                             q_block_2[6 ],
                             q_block_2[5 ],
                             q_block_2[4 ],
                             q_block_2[3 ],
                             q_block_2[2 ],
                             q_block_2[1 ],
                             q_block_2[0 ]  }
                );
    assign  q_block_3_out = ( { q_block_3[15],   
                             q_block_3[14],
                             q_block_3[13],
                             q_block_3[12],
                             q_block_3[11],
                             q_block_3[10],
                             q_block_3[9 ],
                             q_block_3[8 ],
                             q_block_3[7 ],
                             q_block_3[6 ],
                             q_block_3[5 ],
                             q_block_3[4 ],
                             q_block_3[3 ],
                             q_block_3[2 ],
                             q_block_3[1 ],
                             q_block_3[0 ]  }
                );
    assign  q_block_4_out = ( { q_block_4[15],   
                             q_block_4[14],
                             q_block_4[13],
                             q_block_4[12],
                             q_block_4[11],
                             q_block_4[10],
                             q_block_4[9 ],
                             q_block_4[8 ],
                             q_block_4[7 ],
                             q_block_4[6 ],
                             q_block_4[5 ],
                             q_block_4[4 ],
                             q_block_4[3 ],
                             q_block_4[2 ],
                             q_block_4[1 ],
                             q_block_4[0 ]  }
                );
    assign  A_block_out = ( {  A_block[15],   
                            A_block[14],
                            A_block[13],
                            A_block[12],
                            A_block[11],
                            A_block[10],
                            A_block[9 ],
                            A_block[8 ],
                            A_block[7 ],
                            A_block[6 ],
                            A_block[5 ],
                            A_block[4 ],
                            A_block[3 ],
                            A_block[2 ],
                            A_block[1 ],
                            A_block[0 ]  }
                );        
    assign  B_block_out = ( {  B_block[15],   
                            B_block[14],
                            B_block[13],
                            B_block[12],
                            B_block[11],
                            B_block[10],
                            B_block[9 ],
                            B_block[8 ],
                            B_block[7 ],
                            B_block[6 ],
                            B_block[5 ],
                            B_block[4 ],
                            B_block[3 ],
                            B_block[2 ],
                            B_block[1 ],
                            B_block[0 ]  }
                );
    assign  C_block_out = ( {  C_block[15],   
                            C_block[14],
                            C_block[13],
                            C_block[12],
                            C_block[11],
                            C_block[10],
                            C_block[9 ],
                            C_block[8 ],
                            C_block[7 ],
                            C_block[6 ],
                            C_block[5 ],
                            C_block[4 ],
                            C_block[3 ],
                            C_block[2 ],
                            C_block[1 ],
                            C_block[0 ]  }
                );
    assign  D_block_out = ( {  D_block[15],   
                            D_block[14],
                            D_block[13],
                            D_block[12],
                            D_block[11],
                            D_block[10],
                            D_block[9 ],
                            D_block[8 ],
                            D_block[7 ],
                            D_block[6 ],
                            D_block[5 ],
                            D_block[4 ],
                            D_block[3 ],
                            D_block[2 ],
                            D_block[1 ],
                            D_block[0 ]  }
                );
    assign  E_block_out = ( {  E_block[15],   
                            E_block[14],
                            E_block[13],
                            E_block[12],
                            E_block[11],
                            E_block[10],
                            E_block[9 ],
                            E_block[8 ],
                            E_block[7 ],
                            E_block[6 ],
                            E_block[5 ],
                            E_block[4 ],
                            E_block[3 ],
                            E_block[2 ],
                            E_block[1 ],
                            E_block[0 ]  }
                );
	assign  J_block_out = ( {  	J_block[15],   
								J_block[14],
								J_block[13],
								J_block[12],
								J_block[11],
								J_block[10],
								J_block[9 ],
								J_block[8 ],
								J_block[7 ],
								J_block[6 ],
								J_block[5 ],
								J_block[4 ],
								J_block[3 ],
								J_block[2 ],
								J_block[1 ],
								J_block[0 ]  }
                );
	assign  K_block_out = ( {  	K_block[15],   
								K_block[14],
								K_block[13],
								K_block[12],
								K_block[11],
								K_block[10],
								K_block[9 ],
								K_block[8 ],
								K_block[7 ],
								K_block[6 ],
								K_block[5 ],
								K_block[4 ],
								K_block[3 ],
								K_block[2 ],
								K_block[1 ],
								K_block[0 ]  }
                );
	assign  M_block_out = ( {  	M_block[15],   
								M_block[14],
								M_block[13],
								M_block[12],
								M_block[11],
								M_block[10],
								M_block[9 ],
								M_block[8 ],
								M_block[7 ],
								M_block[6 ],
								M_block[5 ],
								M_block[4 ],
								M_block[3 ],
								M_block[2 ],
								M_block[1 ],
								M_block[0 ]  }
                );
	assign  N_block_out = ( {  	N_block[15],   
								N_block[14],
								N_block[13],
								N_block[12],
								N_block[11],
								N_block[10],
								N_block[9 ],
								N_block[8 ],
								N_block[7 ],
								N_block[6 ],
								N_block[5 ],
								N_block[4 ],
								N_block[3 ],
								N_block[2 ],
								N_block[1 ],
								N_block[0 ]  }
                );
	assign  G_block_out = ( {  	G_block[15],   
								G_block[14],
								G_block[13],
								G_block[12],
								G_block[11],
								G_block[10],
								G_block[9 ],
								G_block[8 ],
								G_block[7 ],
								G_block[6 ],
								G_block[5 ],
								G_block[4 ],
								G_block[3 ],
								G_block[2 ],
								G_block[1 ],
								G_block[0 ]  }
                );
	assign  H_block_out = ( {  	H_block[15],   
								H_block[14],
								H_block[13],
								H_block[12],
								H_block[11],
								H_block[10],
								H_block[9 ],
								H_block[8 ],
								H_block[7 ],
								H_block[6 ],
								H_block[5 ],
								H_block[4 ],
								H_block[3 ],
								H_block[2 ],
								H_block[1 ],
								H_block[0 ]  }
                );
	assign  I_block_out = ( {  	I_block[15],   
								I_block[14],
								I_block[13],
								I_block[12],
								I_block[11],
								I_block[10],
								I_block[9 ],
								I_block[8 ],
								I_block[7 ],
								I_block[6 ],
								I_block[5 ],
								I_block[4 ],
								I_block[3 ],
								I_block[2 ],
								I_block[1 ],
								I_block[0 ]  }
                );
	
	// SAO Block filter module for 4x4 block
	sao_block_filter luma_sao_block_filter_1(
		.clk	(clk),
        .reset	(reset),
		.start	(filter_1_start_sao_filter),
		.p_0_0_in (filter_1_p_0_0_in_sao),
		.p_0_1_in (filter_1_p_0_1_in_sao),
		.p_0_2_in (filter_1_p_0_2_in_sao),
		.p_0_3_in (filter_1_p_0_3_in_sao),
		.p_0_4_in (filter_1_p_0_4_in_sao),
		.p_0_5_in (filter_1_p_0_5_in_sao),
		.p_1_0_in (filter_1_p_1_0_in_sao),
		.p_1_1_in (filter_1_p_1_1_in_sao),
		.p_1_2_in (filter_1_p_1_2_in_sao),
		.p_1_3_in (filter_1_p_1_3_in_sao),
		.p_1_4_in (filter_1_p_1_4_in_sao),
		.p_1_5_in (filter_1_p_1_5_in_sao),
		.p_2_0_in (filter_1_p_2_0_in_sao),
		.p_2_1_in (filter_1_p_2_1_in_sao),
		.p_2_2_in (filter_1_p_2_2_in_sao),
		.p_2_3_in (filter_1_p_2_3_in_sao),
		.p_2_4_in (filter_1_p_2_4_in_sao),
		.p_2_5_in (filter_1_p_2_5_in_sao),
		.p_3_0_in (filter_1_p_3_0_in_sao),
		.p_3_1_in (filter_1_p_3_1_in_sao),
		.p_3_2_in (filter_1_p_3_2_in_sao),
		.p_3_3_in (filter_1_p_3_3_in_sao),
		.p_3_4_in (filter_1_p_3_4_in_sao),
		.p_3_5_in (filter_1_p_3_5_in_sao),
		.p_4_0_in (filter_1_p_4_0_in_sao),
		.p_4_1_in (filter_1_p_4_1_in_sao),
		.p_4_2_in (filter_1_p_4_2_in_sao),
		.p_4_3_in (filter_1_p_4_3_in_sao),
		.p_4_4_in (filter_1_p_4_4_in_sao),
		.p_4_5_in (filter_1_p_4_5_in_sao),
		.p_5_0_in (filter_1_p_5_0_in_sao),
		.p_5_1_in (filter_1_p_5_1_in_sao),
		.p_5_2_in (filter_1_p_5_2_in_sao),
		.p_5_3_in (filter_1_p_5_3_in_sao),
		.p_5_4_in (filter_1_p_5_4_in_sao),
		.p_5_5_in (filter_1_p_5_5_in_sao),
		.cross_boundary_left_flag_in		(filter_1_cross_boundary_left_flag_sao		),
		.cross_boundary_right_flag_in		(filter_1_cross_boundary_right_flag_sao		),
		.cross_boundary_top_flag_in			(filter_1_cross_boundary_top_flag_sao		),
		.cross_boundary_bottom_flag_in		(filter_1_cross_boundary_bottom_flag_sao		),
		.cross_boundary_top_left_flag_in	(filter_1_cross_boundary_top_left_flag_sao	),
		.cross_boundary_top_right_flag_in	(filter_1_cross_boundary_top_right_flag_sao	),
		.cross_boundary_bottom_left_flag_in	(filter_1_cross_boundary_bottom_left_flag_sao),
		.cross_boundary_bottom_right_flag_in(filter_1_cross_boundary_bottom_right_flag_sao),
		.sao_type_in	 (filter_1_sao_type_in_sao	 ),
		.sao_eo_class_in (filter_1_sao_eo_class_in_sao),
		.sao_bandpos_in	 (filter_1_sao_bandpos_in_sao ),
		.sao_offset_1_in (filter_1_sao_offset_1_in_sao),
		.sao_offset_2_in (filter_1_sao_offset_2_in_sao),
		.sao_offset_3_in (filter_1_sao_offset_3_in_sao),
		.sao_offset_4_in (filter_1_sao_offset_4_in_sao),
		.pix_1_1_out (filter_1_pix_1_1_out_sao),
		.pix_1_2_out (filter_1_pix_1_2_out_sao),
		.pix_1_3_out (filter_1_pix_1_3_out_sao),
		.pix_1_4_out (filter_1_pix_1_4_out_sao),
		.pix_2_1_out (filter_1_pix_2_1_out_sao),
		.pix_2_2_out (filter_1_pix_2_2_out_sao),
		.pix_2_3_out (filter_1_pix_2_3_out_sao),
		.pix_2_4_out (filter_1_pix_2_4_out_sao),
		.pix_3_1_out (filter_1_pix_3_1_out_sao),
		.pix_3_2_out (filter_1_pix_3_2_out_sao),
		.pix_3_3_out (filter_1_pix_3_3_out_sao),
		.pix_3_4_out (filter_1_pix_3_4_out_sao),
		.pix_4_1_out (filter_1_pix_4_1_out_sao),
		.pix_4_2_out (filter_1_pix_4_2_out_sao),
		.pix_4_3_out (filter_1_pix_4_3_out_sao),
		.pix_4_4_out (filter_1_pix_4_4_out_sao)
    );
	
	sao_block_filter luma_sao_block_filter_2(
		.clk	(clk),
        .reset	(reset),
		.start	(filter_2_start_sao_filter),
		.p_0_0_in (filter_2_p_0_0_in_sao),
		.p_0_1_in (filter_2_p_0_1_in_sao),
		.p_0_2_in (filter_2_p_0_2_in_sao),
		.p_0_3_in (filter_2_p_0_3_in_sao),
		.p_0_4_in (filter_2_p_0_4_in_sao),
		.p_0_5_in (filter_2_p_0_5_in_sao),
		.p_1_0_in (filter_2_p_1_0_in_sao),
		.p_1_1_in (filter_2_p_1_1_in_sao),
		.p_1_2_in (filter_2_p_1_2_in_sao),
		.p_1_3_in (filter_2_p_1_3_in_sao),
		.p_1_4_in (filter_2_p_1_4_in_sao),
		.p_1_5_in (filter_2_p_1_5_in_sao),
		.p_2_0_in (filter_2_p_2_0_in_sao),
		.p_2_1_in (filter_2_p_2_1_in_sao),
		.p_2_2_in (filter_2_p_2_2_in_sao),
		.p_2_3_in (filter_2_p_2_3_in_sao),
		.p_2_4_in (filter_2_p_2_4_in_sao),
		.p_2_5_in (filter_2_p_2_5_in_sao),
		.p_3_0_in (filter_2_p_3_0_in_sao),
		.p_3_1_in (filter_2_p_3_1_in_sao),
		.p_3_2_in (filter_2_p_3_2_in_sao),
		.p_3_3_in (filter_2_p_3_3_in_sao),
		.p_3_4_in (filter_2_p_3_4_in_sao),
		.p_3_5_in (filter_2_p_3_5_in_sao),
		.p_4_0_in (filter_2_p_4_0_in_sao),
		.p_4_1_in (filter_2_p_4_1_in_sao),
		.p_4_2_in (filter_2_p_4_2_in_sao),
		.p_4_3_in (filter_2_p_4_3_in_sao),
		.p_4_4_in (filter_2_p_4_4_in_sao),
		.p_4_5_in (filter_2_p_4_5_in_sao),
		.p_5_0_in (filter_2_p_5_0_in_sao),
		.p_5_1_in (filter_2_p_5_1_in_sao),
		.p_5_2_in (filter_2_p_5_2_in_sao),
		.p_5_3_in (filter_2_p_5_3_in_sao),
		.p_5_4_in (filter_2_p_5_4_in_sao),
		.p_5_5_in (filter_2_p_5_5_in_sao),
		.cross_boundary_left_flag_in		(filter_2_cross_boundary_left_flag_sao		),
		.cross_boundary_right_flag_in		(filter_2_cross_boundary_right_flag_sao		),
		.cross_boundary_top_flag_in			(filter_2_cross_boundary_top_flag_sao		),
		.cross_boundary_bottom_flag_in		(filter_2_cross_boundary_bottom_flag_sao		),
		.cross_boundary_top_left_flag_in	(filter_2_cross_boundary_top_left_flag_sao	),
		.cross_boundary_top_right_flag_in	(filter_2_cross_boundary_top_right_flag_sao	),
		.cross_boundary_bottom_left_flag_in	(filter_2_cross_boundary_bottom_left_flag_sao),
		.cross_boundary_bottom_right_flag_in(filter_2_cross_boundary_bottom_right_flag_sao),
		.sao_type_in	 (filter_2_sao_type_in_sao	 ),
		.sao_eo_class_in (filter_2_sao_eo_class_in_sao),
		.sao_bandpos_in	 (filter_2_sao_bandpos_in_sao ),
		.sao_offset_1_in (filter_2_sao_offset_1_in_sao),
		.sao_offset_2_in (filter_2_sao_offset_2_in_sao),
		.sao_offset_3_in (filter_2_sao_offset_3_in_sao),
		.sao_offset_4_in (filter_2_sao_offset_4_in_sao),
		.pix_1_1_out (filter_2_pix_1_1_out_sao),
		.pix_1_2_out (filter_2_pix_1_2_out_sao),
		.pix_1_3_out (filter_2_pix_1_3_out_sao),
		.pix_1_4_out (filter_2_pix_1_4_out_sao),
		.pix_2_1_out (filter_2_pix_2_1_out_sao),
		.pix_2_2_out (filter_2_pix_2_2_out_sao),
		.pix_2_3_out (filter_2_pix_2_3_out_sao),
		.pix_2_4_out (filter_2_pix_2_4_out_sao),
		.pix_3_1_out (filter_2_pix_3_1_out_sao),
		.pix_3_2_out (filter_2_pix_3_2_out_sao),
		.pix_3_3_out (filter_2_pix_3_3_out_sao),
		.pix_3_4_out (filter_2_pix_3_4_out_sao),
		.pix_4_1_out (filter_2_pix_4_1_out_sao),
		.pix_4_2_out (filter_2_pix_4_2_out_sao),
		.pix_4_3_out (filter_2_pix_4_3_out_sao),
		.pix_4_4_out (filter_2_pix_4_4_out_sao)
    );
	
	
	// SAO sample Row buffer BRAM
	BRAM_single_port #(
		.ADDR_WIDTH	(9),
		.DATA_WIDTH	(SAMPLE_ROW_BUF_DATA_WIDTH),
		.RAM_DEPTH	(MAX_PIC_WIDTH/8)
	)
	sao_luma_sample_row_buffer(
		.clk        (clk),
		.we_a       (row_we_a      ),
		.en_a       (row_en_a      ),
		.addr_a     (row_addr_a    ),
		.data_in_a  (row_data_in_a ),
		.data_out_a (row_data_out_a)
	);
	
	// SAO sample Column buffer BRAM
	BRAM_dual_port #(
		.ADDR_WIDTH	(9),
		.DATA_WIDTH	(SAMPLE_COL_BUF_DATA_WIDTH),
		.RAM_DEPTH	(MAX_PIC_HEIGHT/4)
	)
	sao_luma_sample_col_buffer(
		.clk        (clk),
		.we_a       (col_we_a      ),
		.we_b       (col_we_b      ),
		.en_a       (col_en_a      ),
		.en_b       (col_en_b      ),
		.addr_a     (col_addr_a    ),
		.addr_b     (col_addr_b    ),
		.data_in_a  (col_data_in_a ),
		.data_in_b  (col_data_in_b ),
		.data_out_a (col_data_out_a),
		.data_out_b (col_data_out_b)
	);
	
	// SAO Decision Row buffer BRAM
	BRAM_single_port #(
		.ADDR_WIDTH	(9),
		.DATA_WIDTH	(DECISION_ROW_BUF_DATA_WIDTH),
		.RAM_DEPTH	(MAX_PIC_WIDTH/8)
	)
	sao_luma_decision_row_buffer(
		.clk        (clk),
		.we_a       (d_row_buf_we_a      ),
		.en_a       (d_row_buf_en_a      ),
		.addr_a     (d_row_buf_addr_a    ),
		.data_in_a  (d_row_buf_data_in_a ),
		.data_out_a (d_row_buf_data_out_a)
	);
	
	// SAO Left Decision Column buffer BRAM
	BRAM_single_port #(
		.ADDR_WIDTH	(9),
		.DATA_WIDTH	(LEFT_DECISION_COL_BUF_DATA_WIDTH),
		.RAM_DEPTH	(MAX_PIC_HEIGHT/8)
	)
	sao_luma_left_decision_col_buffer(
		.clk        (clk),
		.we_a       (d_left_col_buf_we_a      ),
		.en_a       (d_left_col_buf_en_a      ),
		.addr_a     (d_left_col_buf_addr_a    ),
		.data_in_a  (d_left_col_buf_data_in_a ),
		.data_out_a (d_left_col_buf_data_out_a)
	);
	
	// SAO Right Decision Column buffer BRAM - This is actually horizontal(Xc addressed)
	BRAM_single_port #(
		.ADDR_WIDTH	(9),
		.DATA_WIDTH	(RIGHT_DECISION_COL_BUF_DATA_WIDTH),
		.RAM_DEPTH	(MAX_PIC_WIDTH/8)
	)
	sao_luma_right_decision_col_buffer(
		.clk        (clk),
		.we_a       (d_right_col_buf_we_a      ),
		.en_a       (d_right_col_buf_en_a      ),
		.addr_a     (d_right_col_buf_addr_a    ),
		.data_in_a  (d_right_col_buf_data_in_a ),
		.data_out_a (d_right_col_buf_data_out_a)
	);
	
	
	// X8,Y8 calculation for other SAO Blocks
	assign	J_block_X8	=	Xc_in[XC_WIDTH-1:3] - 1'b1 ;		// Do not use for decision making(use Xc_in, Yc_in)
	assign	J_block_Y8	=	Yc_in[XC_WIDTH-1:3] - 1'b1 ;		// Only for comparison purposes, can be negative
	assign	H_block_X8	=	Xc_in[XC_WIDTH-1:3] - 1'b1 ;
	assign	H_block_Y8	=	Yc_in[XC_WIDTH-1:3];
	assign	M_block_X8	=	Xc_in[XC_WIDTH-1:3];
	assign	M_block_Y8	=	Yc_in[XC_WIDTH-1:3] - 1'b1 ;
	assign	J_block_TR_X8	=	Xc_in[XC_WIDTH-1:3] ;
	assign	J_block_TR_Y8	=	Yc_in[XC_WIDTH-1:3] - 2'd2 ;
	assign	J_block_BL_X8	=	Xc_in[XC_WIDTH-1:3] - 2'd2 ;
	assign	J_block_BL_Y8	=	Xc_in[XC_WIDTH-1:3] ;
	
	
	//------------- Main state machine for 8x8 luma sao filtering -------------------------
	
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			state <= STATE_INIT;
            done  <= 1'b0;
		end
		else begin
			case(state)
				STATE_INIT : begin
					if(start) begin
						// Assigning input samples to registers
						q_block_1[0]  	<=	  q_block_1_in [8*0 + 7 : 8*0] ;
                        q_block_2[0]  	<=	  q_block_2_in [8*0 + 7 : 8*0] ;
                        q_block_3[0]  	<=	  q_block_3_in [8*0 + 7 : 8*0] ;
                        q_block_4[0]  	<=	  q_block_4_in [8*0 + 7 : 8*0] ;
                        A_block  [0]  	<=	  A_block_in   [8*0 + 7 : 8*0] ;
                        B_block  [0]  	<=	  B_block_in   [8*0 + 7 : 8*0] ;
						C_block	 [0]  	<=	  C_block_in   [8*0 + 7 : 8*0] ;
						D_block  [0]  	<=	  D_block_in   [8*0 + 7 : 8*0] ;
						E_block  [0]  	<=	  E_block_in   [8*0 + 7 : 8*0] ;
						
						q_block_1[1]  	<=	  q_block_1_in [8*1 + 7 : 8*1] ;
                        q_block_2[1]  	<=	  q_block_2_in [8*1 + 7 : 8*1] ;
                        q_block_3[1]  	<=	  q_block_3_in [8*1 + 7 : 8*1] ;
                        q_block_4[1]  	<=	  q_block_4_in [8*1 + 7 : 8*1] ;
                        A_block  [1]  	<=	  A_block_in   [8*1 + 7 : 8*1] ;
                        B_block  [1]  	<=	  B_block_in   [8*1 + 7 : 8*1] ;
						C_block	 [1]  	<=	  C_block_in   [8*1 + 7 : 8*1] ;
						D_block  [1]  	<=	  D_block_in   [8*1 + 7 : 8*1] ;
						E_block  [1]  	<=	  E_block_in   [8*1 + 7 : 8*1] ;
						
						q_block_1[2]  	<=	  q_block_1_in [8*2 + 7 : 8*2] ;
                        q_block_2[2]  	<=	  q_block_2_in [8*2 + 7 : 8*2] ;
                        q_block_3[2]  	<=	  q_block_3_in [8*2 + 7 : 8*2] ;
                        q_block_4[2]  	<=	  q_block_4_in [8*2 + 7 : 8*2] ;
                        A_block  [2]  	<=	  A_block_in   [8*2 + 7 : 8*2] ;
                        B_block  [2]  	<=	  B_block_in   [8*2 + 7 : 8*2] ;
						C_block	 [2]  	<=	  C_block_in   [8*2 + 7 : 8*2] ;
						D_block  [2]  	<=	  D_block_in   [8*2 + 7 : 8*2] ;
						E_block  [2]  	<=	  E_block_in   [8*2 + 7 : 8*2] ;
						
						q_block_1[3]  	<=	  q_block_1_in [8*3 + 7 : 8*3] ;
                        q_block_2[3]  	<=	  q_block_2_in [8*3 + 7 : 8*3] ;
                        q_block_3[3]  	<=	  q_block_3_in [8*3 + 7 : 8*3] ;
                        q_block_4[3]  	<=	  q_block_4_in [8*3 + 7 : 8*3] ;
                        A_block  [3]  	<=	  A_block_in   [8*3 + 7 : 8*3] ;
                        B_block  [3]  	<=	  B_block_in   [8*3 + 7 : 8*3] ;
						C_block	 [3]  	<=	  C_block_in   [8*3 + 7 : 8*3] ;
						D_block  [3]  	<=	  D_block_in   [8*3 + 7 : 8*3] ;
						E_block  [3]  	<=	  E_block_in   [8*3 + 7 : 8*3] ;
						
						
						q_block_1[4]  	<=	  q_block_1_in [8*4 + 7 : 8*4] ;
                        q_block_2[4]  	<=	  q_block_2_in [8*4 + 7 : 8*4] ;
                        q_block_3[4]  	<=	  q_block_3_in [8*4 + 7 : 8*4] ;
                        q_block_4[4]  	<=	  q_block_4_in [8*4 + 7 : 8*4] ;
                        A_block  [4]  	<=	  A_block_in   [8*4 + 7 : 8*4] ;
                        B_block  [4]  	<=	  B_block_in   [8*4 + 7 : 8*4] ;
						C_block	 [4]  	<=	  C_block_in   [8*4 + 7 : 8*4] ;
						D_block  [4]  	<=	  D_block_in   [8*4 + 7 : 8*4] ;
						E_block  [4]  	<=	  E_block_in   [8*4 + 7 : 8*4] ;
						
						q_block_1[5]  	<=	  q_block_1_in [8*5 + 7 : 8*5] ;
                        q_block_2[5]  	<=	  q_block_2_in [8*5 + 7 : 8*5] ;
                        q_block_3[5]  	<=	  q_block_3_in [8*5 + 7 : 8*5] ;
                        q_block_4[5]  	<=	  q_block_4_in [8*5 + 7 : 8*5] ;
                        A_block  [5]  	<=	  A_block_in   [8*5 + 7 : 8*5] ;
                        B_block  [5]  	<=	  B_block_in   [8*5 + 7 : 8*5] ;
						C_block	 [5]  	<=	  C_block_in   [8*5 + 7 : 8*5] ;
						D_block  [5]  	<=	  D_block_in   [8*5 + 7 : 8*5] ;
						E_block  [5]  	<=	  E_block_in   [8*5 + 7 : 8*5] ;
						
						q_block_1[6]  	<=	  q_block_1_in [8*6 + 7 : 8*6] ;
                        q_block_2[6]  	<=	  q_block_2_in [8*6 + 7 : 8*6] ;
                        q_block_3[6]  	<=	  q_block_3_in [8*6 + 7 : 8*6] ;
                        q_block_4[6]  	<=	  q_block_4_in [8*6 + 7 : 8*6] ;
                        A_block  [6]  	<=	  A_block_in   [8*6 + 7 : 8*6] ;
                        B_block  [6]  	<=	  B_block_in   [8*6 + 7 : 8*6] ;
						C_block	 [6]  	<=	  C_block_in   [8*6 + 7 : 8*6] ;
						D_block  [6]  	<=	  D_block_in   [8*6 + 7 : 8*6] ;
						E_block  [6]  	<=	  E_block_in   [8*6 + 7 : 8*6] ;
						
						q_block_1[7]  	<=	  q_block_1_in [8*7 + 7 : 8*7] ;
                        q_block_2[7]  	<=	  q_block_2_in [8*7 + 7 : 8*7] ;
                        q_block_3[7]  	<=	  q_block_3_in [8*7 + 7 : 8*7] ;
                        q_block_4[7]  	<=	  q_block_4_in [8*7 + 7 : 8*7] ;
                        A_block  [7]  	<=	  A_block_in   [8*7 + 7 : 8*7] ;
                        B_block  [7]  	<=	  B_block_in   [8*7 + 7 : 8*7] ;
						C_block	 [7]  	<=	  C_block_in   [8*7 + 7 : 8*7] ;
						D_block  [7]  	<=	  D_block_in   [8*7 + 7 : 8*7] ;
						E_block  [7]  	<=	  E_block_in   [8*7 + 7 : 8*7] ;
						
						q_block_1[8]  	<=	  q_block_1_in [8*8 + 7 : 8*8] ;
                        q_block_2[8]  	<=	  q_block_2_in [8*8 + 7 : 8*8] ;
                        q_block_3[8]  	<=	  q_block_3_in [8*8 + 7 : 8*8] ;
                        q_block_4[8]  	<=	  q_block_4_in [8*8 + 7 : 8*8] ;
                        A_block  [8]  	<=	  A_block_in   [8*8 + 7 : 8*8] ;
                        B_block  [8]  	<=	  B_block_in   [8*8 + 7 : 8*8] ;
						C_block	 [8]  	<=	  C_block_in   [8*8 + 7 : 8*8] ;
						D_block  [8]  	<=	  D_block_in   [8*8 + 7 : 8*8] ;
						E_block  [8]  	<=	  E_block_in   [8*8 + 7 : 8*8] ;
						
						q_block_1[9]  	<=	  q_block_1_in [8*9 + 7 : 8*9] ;
                        q_block_2[9]  	<=	  q_block_2_in [8*9 + 7 : 8*9] ;
                        q_block_3[9]  	<=	  q_block_3_in [8*9 + 7 : 8*9] ;
                        q_block_4[9]  	<=	  q_block_4_in [8*9 + 7 : 8*9] ;
                        A_block  [9]  	<=	  A_block_in   [8*9 + 7 : 8*9] ;
                        B_block  [9]  	<=	  B_block_in   [8*9 + 7 : 8*9] ;
						C_block	 [9]  	<=	  C_block_in   [8*9 + 7 : 8*9] ;
						D_block  [9]  	<=	  D_block_in   [8*9 + 7 : 8*9] ;
						E_block  [9]  	<=	  E_block_in   [8*9 + 7 : 8*9] ;
						
						q_block_1[10]  	<=	  q_block_1_in [8*10 + 7 : 8*10] ;
                        q_block_2[10]  	<=	  q_block_2_in [8*10 + 7 : 8*10] ;
                        q_block_3[10]  	<=	  q_block_3_in [8*10 + 7 : 8*10] ;
                        q_block_4[10]  	<=	  q_block_4_in [8*10 + 7 : 8*10] ;
                        A_block  [10]  	<=	  A_block_in   [8*10 + 7 : 8*10] ;
                        B_block  [10]  	<=	  B_block_in   [8*10 + 7 : 8*10] ;
						C_block	 [10]  	<=	  C_block_in   [8*10 + 7 : 8*10] ;
						D_block  [10]  	<=	  D_block_in   [8*10 + 7 : 8*10] ;
						E_block  [10]  	<=	  E_block_in   [8*10 + 7 : 8*10] ;
						
						q_block_1[11]  	<=	  q_block_1_in [8*11 + 7 : 8*11] ;
                        q_block_2[11]  	<=	  q_block_2_in [8*11 + 7 : 8*11] ;
                        q_block_3[11]  	<=	  q_block_3_in [8*11 + 7 : 8*11] ;
                        q_block_4[11]  	<=	  q_block_4_in [8*11 + 7 : 8*11] ;
                        A_block  [11]  	<=	  A_block_in   [8*11 + 7 : 8*11] ;
                        B_block  [11]  	<=	  B_block_in   [8*11 + 7 : 8*11] ;
						C_block	 [11]  	<=	  C_block_in   [8*11 + 7 : 8*11] ;
						D_block  [11]  	<=	  D_block_in   [8*11 + 7 : 8*11] ;
						E_block  [11]  	<=	  E_block_in   [8*11 + 7 : 8*11] ;
						
						q_block_1[12]  	<=	  q_block_1_in [8*12 + 7 : 8*12] ;
                        q_block_2[12]  	<=	  q_block_2_in [8*12 + 7 : 8*12] ;
                        q_block_3[12]  	<=	  q_block_3_in [8*12 + 7 : 8*12] ;
                        q_block_4[12]  	<=	  q_block_4_in [8*12 + 7 : 8*12] ;
                        A_block  [12]  	<=	  A_block_in   [8*12 + 7 : 8*12] ;
                        B_block  [12]  	<=	  B_block_in   [8*12 + 7 : 8*12] ;
						C_block	 [12]  	<=	  C_block_in   [8*12 + 7 : 8*12] ;
						D_block  [12]  	<=	  D_block_in   [8*12 + 7 : 8*12] ;
						E_block  [12]  	<=	  E_block_in   [8*12 + 7 : 8*12] ;
						
						q_block_1[13]  	<=	  q_block_1_in [8*13 + 7 : 8*13] ;
                        q_block_2[13]  	<=	  q_block_2_in [8*13 + 7 : 8*13] ;
                        q_block_3[13]  	<=	  q_block_3_in [8*13 + 7 : 8*13] ;
                        q_block_4[13]  	<=	  q_block_4_in [8*13 + 7 : 8*13] ;
                        A_block  [13]  	<=	  A_block_in   [8*13 + 7 : 8*13] ;
                        B_block  [13]  	<=	  B_block_in   [8*13 + 7 : 8*13] ;
						C_block	 [13]  	<=	  C_block_in   [8*13 + 7 : 8*13] ;
						D_block  [13]  	<=	  D_block_in   [8*13 + 7 : 8*13] ;
						E_block  [13]  	<=	  E_block_in   [8*13 + 7 : 8*13] ;
						
						q_block_1[14]  	<=	  q_block_1_in [8*14 + 7 : 8*14] ;
                        q_block_2[14]  	<=	  q_block_2_in [8*14 + 7 : 8*14] ;
                        q_block_3[14]  	<=	  q_block_3_in [8*14 + 7 : 8*14] ;
                        q_block_4[14]  	<=	  q_block_4_in [8*14 + 7 : 8*14] ;
                        A_block  [14]  	<=	  A_block_in   [8*14 + 7 : 8*14] ;
                        B_block  [14]  	<=	  B_block_in   [8*14 + 7 : 8*14] ;
						C_block	 [14]  	<=	  C_block_in   [8*14 + 7 : 8*14] ;
						D_block  [14]  	<=	  D_block_in   [8*14 + 7 : 8*14] ;
						E_block  [14]  	<=	  E_block_in   [8*14 + 7 : 8*14] ;
						
						q_block_1[15]  	<=	  q_block_1_in [8*15 + 7 : 8*15] ;
                        q_block_2[15]  	<=	  q_block_2_in [8*15 + 7 : 8*15] ;
                        q_block_3[15]  	<=	  q_block_3_in [8*15 + 7 : 8*15] ;
                        q_block_4[15]  	<=	  q_block_4_in [8*15 + 7 : 8*15] ;
                        A_block  [15]  	<=	  A_block_in   [8*15 + 7 : 8*15] ;
                        B_block  [15]  	<=	  B_block_in   [8*15 + 7 : 8*15] ;
						C_block	 [15]  	<=	  C_block_in   [8*15 + 7 : 8*15] ;
						D_block  [15]  	<=	  D_block_in   [8*15 + 7 : 8*15] ;
						E_block  [15]  	<=	  E_block_in   [8*15 + 7 : 8*15] ;
						
						done  <= 1'b0;
						state <= STATE_1;
					end
				end
				STATE_1 : begin
					// Check for J,K blocks reading in combinational block
					// Check for G,H block reading in combinational block	
					// Check for decision buffers reading in combinational block
					state <= STATE_2;
				end
				STATE_2 : begin
					// Assign to G block from BRAM
					if((Xc_in>0) && (Yc_in>0)) begin
						G_block[0]    <=    col_data_out_a [8*0  + 7 : 8*0 ] ;
                        G_block[1]    <=    col_data_out_a [8*1  + 7 : 8*1 ] ;
                        G_block[2]    <=    col_data_out_a [8*2  + 7 : 8*2 ] ;
                        G_block[3]    <=    col_data_out_a [8*3  + 7 : 8*3 ] ;
                        G_block[4]    <=    col_data_out_a [8*4  + 7 : 8*4 ] ;
                        G_block[5]    <=    col_data_out_a [8*5  + 7 : 8*5 ] ;
                        G_block[6]    <=    col_data_out_a [8*6  + 7 : 8*6 ] ;
                        G_block[7]    <=    col_data_out_a [8*7  + 7 : 8*7 ] ;
                        G_block[8]    <=    col_data_out_a [8*8  + 7 : 8*8 ] ;
                        G_block[9]    <=    col_data_out_a [8*9  + 7 : 8*9 ] ;
                        G_block[10]   <=    col_data_out_a [8*10 + 7 : 8*10] ;
                        G_block[11]   <=    col_data_out_a [8*11 + 7 : 8*11] ;
                        G_block[12]   <=    col_data_out_a [8*12 + 7 : 8*12] ;
                        G_block[13]   <=    col_data_out_a [8*13 + 7 : 8*13] ;
                        G_block[14]   <=    col_data_out_a [8*14 + 7 : 8*14] ;
                        G_block[15]   <=    col_data_out_a [8*15 + 7 : 8*15] ;
					end
					// Assign to H block from BRAM
					if(Xc_in>0) begin
						H_block[0]    <=    col_data_out_b [8*0  + 7 : 8*0 ] ;
                        H_block[1]    <=    col_data_out_b [8*1  + 7 : 8*1 ] ;
                        H_block[2]    <=    col_data_out_b [8*2  + 7 : 8*2 ] ;
                        H_block[3]    <=    col_data_out_b [8*3  + 7 : 8*3 ] ;
                        H_block[4]    <=    col_data_out_b [8*4  + 7 : 8*4 ] ;
                        H_block[5]    <=    col_data_out_b [8*5  + 7 : 8*5 ] ;
                        H_block[6]    <=    col_data_out_b [8*6  + 7 : 8*6 ] ;
                        H_block[7]    <=    col_data_out_b [8*7  + 7 : 8*7 ] ;
                        H_block[8]    <=    col_data_out_b [8*8  + 7 : 8*8 ] ;
                        H_block[9]    <=    col_data_out_b [8*9  + 7 : 8*9 ] ;
                        H_block[10]   <=    col_data_out_b [8*10 + 7 : 8*10] ;
                        H_block[11]   <=    col_data_out_b [8*11 + 7 : 8*11] ;
                        H_block[12]   <=    col_data_out_b [8*12 + 7 : 8*12] ;
                        H_block[13]   <=    col_data_out_b [8*13 + 7 : 8*13] ;
                        H_block[14]   <=    col_data_out_b [8*14 + 7 : 8*14] ;
                        H_block[15]   <=    col_data_out_b [8*15 + 7 : 8*15] ;
						H_block_sao_across_slices_enable_flag	<=	col_data_out_b [8*16] ;
						H_block_pcm_bypass_sao_disable_flag		<=	col_data_out_b [8*16 + 1] ;
						H_block_tile_left_flag	<=	col_data_out_b [8*16 + 2] ;
						H_block_tile_top_flag   <=	col_data_out_b [8*16 + 3] ;
						H_block_slice_left_flag <=	col_data_out_b [8*16 + 4] ;
						H_block_slice_top_flag  <=	col_data_out_b [8*16 + 5] ;
						H_block_sao_luma		<=	col_data_out_b [8*16 + 6] ;
						H_block_y_sao_type      <=	col_data_out_b [8*16 + 8  : 8*16 + 7]  ;
						H_block_y_eoclass       <=	col_data_out_b [8*16 + 10 : 8*16 + 9]  ;
						H_block_y_bandpos       <=	col_data_out_b [8*16 + 15 : 8*16 + 11] ;
						H_block_y_sao_offset_1  <=	col_data_out_b [8*16 + 19 : 8*16 + 16] ;
						H_block_y_sao_offset_2  <=	col_data_out_b [8*16 + 23 : 8*16 + 20] ;
						H_block_y_sao_offset_3  <=	col_data_out_b [8*16 + 27 : 8*16 + 24] ;
						H_block_y_sao_offset_4  <=	col_data_out_b [8*16 + 31 : 8*16 + 28] ;
						H_block_slice_id  		<=	col_data_out_b [8*16 + 39 : 8*16 + 32] ;
					end
					// Assign to J,K blocks from BRAM
					if((Xc_in>0) && (Yc_in>0)) begin
						J_block[0]    <=    row_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
                        J_block[1]    <=    row_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        J_block[2]    <=    row_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        J_block[3]    <=    row_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        J_block[4]    <=    row_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        J_block[5]    <=    row_data_out_a [BIT_DEPTH*6  - 1 : BIT_DEPTH*5 ] ;
                        J_block[6]    <=    row_data_out_a [BIT_DEPTH*7  - 1 : BIT_DEPTH*6 ] ;
                        J_block[7]    <=    row_data_out_a [BIT_DEPTH*8  - 1 : BIT_DEPTH*7 ] ;
                        J_block[8]    <=    row_data_out_a [BIT_DEPTH*9  - 1 : BIT_DEPTH*8 ] ;
                        J_block[9]    <=    row_data_out_a [BIT_DEPTH*10 - 1 : BIT_DEPTH*9 ] ;
                        J_block[10]   <=    row_data_out_a [BIT_DEPTH*11 - 1 : BIT_DEPTH*10] ;
                        J_block[11]   <=    row_data_out_a [BIT_DEPTH*12 - 1 : BIT_DEPTH*11] ;
                        J_block[12]   <=    row_data_out_a [BIT_DEPTH*13 - 1 : BIT_DEPTH*12] ;
                        J_block[13]   <=    row_data_out_a [BIT_DEPTH*14 - 1 : BIT_DEPTH*13] ;
                        J_block[14]   <=    row_data_out_a [BIT_DEPTH*15 - 1 : BIT_DEPTH*14] ;
                        J_block[15]   <=    row_data_out_a [BIT_DEPTH*16 - 1 : BIT_DEPTH*15] ;
						K_block[0]    <=    row_data_out_a [BIT_DEPTH*17 - 1 : BIT_DEPTH*16] ;
                        K_block[1]    <=    row_data_out_a [BIT_DEPTH*18 - 1 : BIT_DEPTH*17] ;
                        K_block[2]    <=    row_data_out_a [BIT_DEPTH*19 - 1 : BIT_DEPTH*18] ;
                        K_block[3]    <=    row_data_out_a [BIT_DEPTH*20 - 1 : BIT_DEPTH*19] ;
                        K_block[4]    <=    row_data_out_a [BIT_DEPTH*21 - 1 : BIT_DEPTH*20] ;
                        K_block[5]    <=    row_data_out_a [BIT_DEPTH*22 - 1 : BIT_DEPTH*21] ;
                        K_block[6]    <=    row_data_out_a [BIT_DEPTH*23 - 1 : BIT_DEPTH*22] ;
                        K_block[7]    <=    row_data_out_a [BIT_DEPTH*24 - 1 : BIT_DEPTH*23] ;
                        K_block[8]    <=    row_data_out_a [BIT_DEPTH*25 - 1 : BIT_DEPTH*24] ;
                        K_block[9]    <=    row_data_out_a [BIT_DEPTH*26 - 1 : BIT_DEPTH*25] ;
                        K_block[10]   <=    row_data_out_a [BIT_DEPTH*27 - 1 : BIT_DEPTH*26] ;
                        K_block[11]   <=    row_data_out_a [BIT_DEPTH*28 - 1 : BIT_DEPTH*27] ;
                        K_block[12]   <=    row_data_out_a [BIT_DEPTH*29 - 1 : BIT_DEPTH*28] ;
                        K_block[13]   <=    row_data_out_a [BIT_DEPTH*30 - 1 : BIT_DEPTH*29] ;
                        K_block[14]   <=    row_data_out_a [BIT_DEPTH*31 - 1 : BIT_DEPTH*30] ;
                        K_block[15]   <=    row_data_out_a [BIT_DEPTH*32 - 1 : BIT_DEPTH*31] ;
						J_block_sao_across_slices_enable_flag	<=	row_data_out_a [8*32] ;
						J_block_pcm_bypass_sao_disable_flag		<=	row_data_out_a [8*32 + 1] ;
						J_block_tile_left_flag	<=	row_data_out_a [8*32 + 2] ;
						J_block_tile_top_flag   <=	row_data_out_a [8*32 + 3] ;
						J_block_slice_left_flag <=	row_data_out_a [8*32 + 4] ;
						J_block_slice_top_flag  <=	row_data_out_a [8*32 + 5] ;
						J_block_sao_luma		<=	row_data_out_a [8*32 + 6] ;
						J_block_y_sao_type      <=	row_data_out_a [8*32 + 8  : 8*32 + 7]  ;
						J_block_y_eoclass       <=	row_data_out_a [8*32 + 10 : 8*32 + 9]  ;
						J_block_y_bandpos       <=	row_data_out_a [8*32 + 15 : 8*32 + 11] ;
						J_block_y_sao_offset_1  <=	row_data_out_a [8*32 + 19 : 8*32 + 16] ;
						J_block_y_sao_offset_2  <=	row_data_out_a [8*32 + 23 : 8*32 + 20] ;
						J_block_y_sao_offset_3  <=	row_data_out_a [8*32 + 27 : 8*32 + 24] ;
						J_block_y_sao_offset_4  <=	row_data_out_a [8*32 + 31 : 8*32 + 28] ;
						J_block_slice_id  		<=	row_data_out_a [8*32 + 39 : 8*32 + 32] ;
					end
					// Assign decision samples from BRAM to registers
					if((Xc_in>0) && (Yc_in>8)) begin
						decision_row[0]	<=	d_row_buf_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						decision_row[1]	<= 	d_row_buf_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        decision_row[2]	<= 	d_row_buf_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        decision_row[3]	<= 	d_row_buf_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        decision_row[4]	<= 	d_row_buf_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        decision_row[5]	<= 	d_row_buf_data_out_a [BIT_DEPTH*6  - 1 : BIT_DEPTH*5 ] ;
                        decision_row[6]	<= 	d_row_buf_data_out_a [BIT_DEPTH*7  - 1 : BIT_DEPTH*6 ] ;
                        decision_row[7]	<= 	d_row_buf_data_out_a [BIT_DEPTH*8  - 1 : BIT_DEPTH*7 ] ;
                        decision_row[8]	<= 	d_row_buf_data_out_a [BIT_DEPTH*9  - 1 : BIT_DEPTH*8 ] ;
                        decision_row[9]	<= 	d_row_buf_data_out_a [BIT_DEPTH*10 - 1 : BIT_DEPTH*9 ] ;
						decision_top_left_slice_boundary  <=  d_row_buf_data_out_a [BIT_DEPTH*10] ;
						decision_top_right_slice_id  <=  d_row_buf_data_out_a [BIT_DEPTH*10 + 8 : BIT_DEPTH*10 + 1] ;
						decision_top_right_sao_across_slices_enable  <=  d_row_buf_data_out_a [BIT_DEPTH*10 + 9] ;
					end
					if((Xc_in>8) && (Yc_in>0)) begin
						decision_col_left[0]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						decision_col_left[1]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        decision_col_left[2]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        decision_col_left[3]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        decision_col_left[4]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        decision_col_left[5]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*6  - 1 : BIT_DEPTH*5 ] ;
                        decision_col_left[6]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*7  - 1 : BIT_DEPTH*6 ] ;
                        decision_col_left[7]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*8  - 1 : BIT_DEPTH*7 ] ;
                        decision_col_left[8]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*9  - 1 : BIT_DEPTH*8 ] ;
						decision_bottom_left_slice_id  <=  d_left_col_buf_data_out_a [BIT_DEPTH*9 + 7 : BIT_DEPTH*9] ;
						decision_bottom_left_sao_across_slices_enable  <=  d_left_col_buf_data_out_a [BIT_DEPTH*9 + 8] ;
					end
					if((Xc_in>0) && (Yc_in>0)) begin
						decision_col_right[0]  <=  d_right_col_buf_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						decision_col_right[1]  <=  d_right_col_buf_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        decision_col_right[2]  <=  d_right_col_buf_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        decision_col_right[3]  <=  d_right_col_buf_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
						M_block_sao_across_slices_enable_flag	<=	d_right_col_buf_data_out_a [BIT_DEPTH*4] ;
						M_block_tile_left_flag	<=	d_right_col_buf_data_out_a [BIT_DEPTH*4 + 1] ;
						M_block_slice_left_flag <=	d_right_col_buf_data_out_a [BIT_DEPTH*4 + 2] ;
						M_block_slice_id  		<=	d_right_col_buf_data_out_a [BIT_DEPTH*4 + 10 : BIT_DEPTH*4 + 3 ] ;
					end
					// Check for M,N blocks reading in combinational block	
					// Check for I block reading in combinational block		
					
					state <= STATE_3;
				end
				STATE_3 : begin
					// Write H,A blocks to row buffer in combinational block  
					// Write 1,D blocks to col buffer in combinational block  
					// Memory should be READ BEFORE WRITE 
					
					// Assign to I block from BRAM
					if((Xc_in>0) && (Yc_in == (pic_height_in - 8))) begin
						I_block[0]    <=    col_data_out_a [8*0  + 7 : 8*0 ] ;
                        I_block[1]    <=    col_data_out_a [8*1  + 7 : 8*1 ] ;
                        I_block[2]    <=    col_data_out_a [8*2  + 7 : 8*2 ] ;
                        I_block[3]    <=    col_data_out_a [8*3  + 7 : 8*3 ] ;
                        I_block[4]    <=    col_data_out_a [8*4  + 7 : 8*4 ] ;
                        I_block[5]    <=    col_data_out_a [8*5  + 7 : 8*5 ] ;
                        I_block[6]    <=    col_data_out_a [8*6  + 7 : 8*6 ] ;
                        I_block[7]    <=    col_data_out_a [8*7  + 7 : 8*7 ] ;
                        I_block[8]    <=    col_data_out_a [8*8  + 7 : 8*8 ] ;
                        I_block[8]    <=    col_data_out_a [8*8  + 7 : 8*8 ] ;
                        I_block[8]    <=    col_data_out_a [8*8  + 7 : 8*8 ] ;
                        I_block[9]    <=    col_data_out_a [8*9  + 7 : 8*9 ] ;
                        I_block[10]   <=    col_data_out_a [8*10 + 7 : 8*10] ;
                        I_block[11]   <=    col_data_out_a [8*11 + 7 : 8*11] ;
                        I_block[12]   <=    col_data_out_a [8*12 + 7 : 8*12] ;
                        I_block[13]   <=    col_data_out_a [8*13 + 7 : 8*13] ;
                        I_block[14]   <=    col_data_out_a [8*14 + 7 : 8*14] ;
                        I_block[15]   <=    col_data_out_a [8*15 + 7 : 8*15] ;
					end
					// Assign to M,N blocks from BRAM
					if((Xc_in == (pic_width_in - 8)) && (Yc_in>0)) begin
						M_block[0]    <=    row_data_out_a [8*0  + 7 : 8*0 ] ;
                        M_block[1]    <=    row_data_out_a [8*1  + 7 : 8*1 ] ;
                        M_block[2]    <=    row_data_out_a [8*2  + 7 : 8*2 ] ;
                        M_block[3]    <=    row_data_out_a [8*3  + 7 : 8*3 ] ;
                        M_block[4]    <=    row_data_out_a [8*4  + 7 : 8*4 ] ;
                        M_block[5]    <=    row_data_out_a [8*5  + 7 : 8*5 ] ;
                        M_block[6]    <=    row_data_out_a [8*6  + 7 : 8*6 ] ;
                        M_block[7]    <=    row_data_out_a [8*7  + 7 : 8*7 ] ;
                        M_block[8]    <=    row_data_out_a [8*8  + 7 : 8*8 ] ;
                        M_block[9]    <=    row_data_out_a [8*9  + 7 : 8*9 ] ;
                        M_block[10]   <=    row_data_out_a [8*10 + 7 : 8*10] ;
                        M_block[11]   <=    row_data_out_a [8*11 + 7 : 8*11] ;
                        M_block[12]   <=    row_data_out_a [8*12 + 7 : 8*12] ;
                        M_block[13]   <=    row_data_out_a [8*13 + 7 : 8*13] ;
                        M_block[14]   <=    row_data_out_a [8*14 + 7 : 8*14] ;
                        M_block[15]   <=    row_data_out_a [8*15 + 7 : 8*15] ;
						N_block[0]    <=    row_data_out_a [8*16 + 7 : 8*16] ;
                        N_block[1]    <=    row_data_out_a [8*17 + 7 : 8*17] ;
                        N_block[2]    <=    row_data_out_a [8*18 + 7 : 8*18] ;
                        N_block[3]    <=    row_data_out_a [8*19 + 7 : 8*19] ;
                        N_block[4]    <=    row_data_out_a [8*20 + 7 : 8*20] ;
                        N_block[5]    <=    row_data_out_a [8*21 + 7 : 8*21] ;
                        N_block[6]    <=    row_data_out_a [8*22 + 7 : 8*22] ;
                        N_block[7]    <=    row_data_out_a [8*23 + 7 : 8*23] ;
                        N_block[8]    <=    row_data_out_a [8*24 + 7 : 8*24] ;
                        N_block[9]    <=    row_data_out_a [8*25 + 7 : 8*25] ;
                        N_block[10]   <=    row_data_out_a [8*26 + 7 : 8*26] ;
                        N_block[11]   <=    row_data_out_a [8*27 + 7 : 8*27] ;
                        N_block[12]   <=    row_data_out_a [8*28 + 7 : 8*28] ;
                        N_block[13]   <=    row_data_out_a [8*29 + 7 : 8*29] ;
                        N_block[14]   <=    row_data_out_a [8*30 + 7 : 8*30] ;
                        N_block[15]   <=    row_data_out_a [8*31 + 7 : 8*31] ;
						M_block_sao_across_slices_enable_flag	<=	row_data_out_a [8*32] ;
						M_block_pcm_bypass_sao_disable_flag		<=	row_data_out_a [8*32 + 1] ;
						M_block_tile_left_flag	<=	row_data_out_a [8*32 + 2] ;
						M_block_tile_top_flag   <=	row_data_out_a [8*32 + 3] ;
						M_block_slice_left_flag <=	row_data_out_a [8*32 + 4] ;
						M_block_slice_top_flag  <=	row_data_out_a [8*32 + 5] ;
						M_block_sao_luma		<=	row_data_out_a [8*32 + 6] ;
						M_block_y_sao_type      <=	row_data_out_a [8*32 + 8  : 8*32 + 7]  ;
						M_block_y_eoclass       <=	row_data_out_a [8*32 + 10 : 8*32 + 9]  ;
						M_block_y_bandpos       <=	row_data_out_a [8*32 + 15 : 8*32 + 11] ;
						M_block_y_sao_offset_1  <=	row_data_out_a [8*32 + 19 : 8*32 + 16] ;
						M_block_y_sao_offset_2  <=	row_data_out_a [8*32 + 23 : 8*32 + 20] ;
						M_block_y_sao_offset_3  <=	row_data_out_a [8*32 + 27 : 8*32 + 24] ;
						M_block_y_sao_offset_4  <=	row_data_out_a [8*32 + 31 : 8*32 + 28] ;
						M_block_slice_id  		<=	row_data_out_a [8*32 + 39 : 8*32 + 32] ;
					end
					state <= STATE_4;
				end
				STATE_4 : begin
					// Write 1,2 blocks to row buffer if necessary in combinational block 
					// Write 3 block to col buffer if necessary in combinational block 
					// Memory should be READ BEFORE WRITE
					// Write new decision samples to decision BRAMs
					if((Xc_in>0) && (Yc_in>0)) begin
						state <= STATE_5;
					end
					else begin
						state <= STATE_INIT;
						done  <= 1'b1;
					end
				end
				STATE_5 : begin
					state <= STATE_6;
				end
				STATE_6 : begin
					// assign SAO block filter outputs
					if(J_block_sao_apply) begin
						J_block[0]    <= 	filter_1_pix_1_1_out_sao ;
						J_block[1]    <= 	filter_1_pix_1_2_out_sao ;
						J_block[2]    <= 	filter_1_pix_1_3_out_sao ;
						J_block[3]    <= 	filter_1_pix_1_4_out_sao ;
						J_block[4]    <= 	filter_1_pix_2_1_out_sao ;
						J_block[5]    <= 	filter_1_pix_2_2_out_sao ;
						J_block[6]    <= 	filter_1_pix_2_3_out_sao ;
						J_block[7]    <= 	filter_1_pix_2_4_out_sao ;
						J_block[8]    <= 	filter_1_pix_3_1_out_sao ;
						J_block[9]    <= 	filter_1_pix_3_2_out_sao ;
						J_block[10]   <= 	filter_1_pix_3_3_out_sao ;
						J_block[11]   <= 	filter_1_pix_3_4_out_sao ;
						J_block[12]   <= 	filter_1_pix_4_1_out_sao ;
						J_block[13]   <= 	filter_1_pix_4_2_out_sao ;
						J_block[14]   <= 	filter_1_pix_4_3_out_sao ;
						J_block[15]   <= 	filter_1_pix_4_4_out_sao ;
						K_block[0]    <= 	filter_2_pix_1_1_out_sao ;
						K_block[1]    <= 	filter_2_pix_1_2_out_sao ;
						K_block[2]    <= 	filter_2_pix_1_3_out_sao ;
						K_block[3]    <= 	filter_2_pix_1_4_out_sao ;
						K_block[4]    <= 	filter_2_pix_2_1_out_sao ;
						K_block[5]    <= 	filter_2_pix_2_2_out_sao ;
						K_block[6]    <= 	filter_2_pix_2_3_out_sao ;
						K_block[7]    <= 	filter_2_pix_2_4_out_sao ;
						K_block[8]    <= 	filter_2_pix_3_1_out_sao ;
						K_block[9]    <= 	filter_2_pix_3_2_out_sao ;
						K_block[10]   <= 	filter_2_pix_3_3_out_sao ;
						K_block[11]   <= 	filter_2_pix_3_4_out_sao ;
						K_block[12]   <= 	filter_2_pix_4_1_out_sao ;
						K_block[13]   <= 	filter_2_pix_4_2_out_sao ;
						K_block[14]   <= 	filter_2_pix_4_3_out_sao ;
						K_block[15]   <= 	filter_2_pix_4_4_out_sao ;
					end
					// Make sure to update decision buffers with pre SAO samples appropriately
					state <= STATE_7;
				end
				STATE_7 : begin
					// Assign decision samples from BRAM to registers for Right block
					if((Xc_in==(pic_width_in - 8)) && (Yc_in>8)) begin
						decision_row[0]	<=	d_row_buf_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						decision_row[1]	<= 	d_row_buf_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        decision_row[2]	<= 	d_row_buf_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        decision_row[3]	<= 	d_row_buf_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        decision_row[4]	<= 	d_row_buf_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        decision_row[5]	<= 	d_row_buf_data_out_a [BIT_DEPTH*6  - 1 : BIT_DEPTH*5 ] ;
                        decision_row[6]	<= 	d_row_buf_data_out_a [BIT_DEPTH*7  - 1 : BIT_DEPTH*6 ] ;
                        decision_row[7]	<= 	d_row_buf_data_out_a [BIT_DEPTH*8  - 1 : BIT_DEPTH*7 ] ;
                        decision_row[8]	<= 	d_row_buf_data_out_a [BIT_DEPTH*9  - 1 : BIT_DEPTH*8 ] ;
                        decision_row[9]	<= 	d_row_buf_data_out_a [BIT_DEPTH*10 - 1 : BIT_DEPTH*9 ] ;
						decision_top_left_slice_boundary  <=  d_row_buf_data_out_a [BIT_DEPTH*10] ;
						decision_top_right_slice_id  <=  d_row_buf_data_out_a [BIT_DEPTH*10 + 8 : BIT_DEPTH*10 + 1] ;
						decision_top_right_sao_across_slices_enable  <=  d_row_buf_data_out_a [BIT_DEPTH*10 + 9] ;
					end
					if((Xc_in==(pic_width_in - 8)) && (Yc_in>0)) begin
						decision_col_left[0]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						decision_col_left[1]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        decision_col_left[2]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        decision_col_left[3]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        decision_col_left[4]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        decision_col_left[5]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*6  - 1 : BIT_DEPTH*5 ] ;
                        decision_col_left[6]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*7  - 1 : BIT_DEPTH*6 ] ;
                        decision_col_left[7]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*8  - 1 : BIT_DEPTH*7 ] ;
                        decision_col_left[8]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*9  - 1 : BIT_DEPTH*8 ] ;
						decision_bottom_left_slice_id  <=  d_left_col_buf_data_out_a [BIT_DEPTH*9 + 7 : BIT_DEPTH*9] ;
						decision_bottom_left_sao_across_slices_enable  <=  d_left_col_buf_data_out_a [BIT_DEPTH*9 + 8] ;
					end
					state <= STATE_8;
				end
				STATE_8 : begin
					if(J_block_sao_apply) begin
						G_block[0]    <= 	filter_1_pix_1_1_out_sao ;
						G_block[1]    <= 	filter_1_pix_1_2_out_sao ;
						G_block[2]    <= 	filter_1_pix_1_3_out_sao ;
						G_block[3]    <= 	filter_1_pix_1_4_out_sao ;
						G_block[4]    <= 	filter_1_pix_2_1_out_sao ;
						G_block[5]    <= 	filter_1_pix_2_2_out_sao ;
						G_block[6]    <= 	filter_1_pix_2_3_out_sao ;
						G_block[7]    <= 	filter_1_pix_2_4_out_sao ;
						G_block[8]    <= 	filter_1_pix_3_1_out_sao ;
						G_block[9]    <= 	filter_1_pix_3_2_out_sao ;
						G_block[10]   <= 	filter_1_pix_3_3_out_sao ;
						G_block[11]   <= 	filter_1_pix_3_4_out_sao ;
						G_block[12]   <= 	filter_1_pix_4_1_out_sao ;
						G_block[13]   <= 	filter_1_pix_4_2_out_sao ;
						G_block[14]   <= 	filter_1_pix_4_3_out_sao ;
						G_block[15]   <= 	filter_1_pix_4_4_out_sao ;
						C_block[0]    <= 	filter_2_pix_1_1_out_sao ;
						C_block[1]    <= 	filter_2_pix_1_2_out_sao ;
						C_block[2]    <= 	filter_2_pix_1_3_out_sao ;
						C_block[3]    <= 	filter_2_pix_1_4_out_sao ;
						C_block[4]    <= 	filter_2_pix_2_1_out_sao ;
						C_block[5]    <= 	filter_2_pix_2_2_out_sao ;
						C_block[6]    <= 	filter_2_pix_2_3_out_sao ;
						C_block[7]    <= 	filter_2_pix_2_4_out_sao ;
						C_block[8]    <= 	filter_2_pix_3_1_out_sao ;
						C_block[9]    <= 	filter_2_pix_3_2_out_sao ;
						C_block[10]   <= 	filter_2_pix_3_3_out_sao ;
						C_block[11]   <= 	filter_2_pix_3_4_out_sao ;
						C_block[12]   <= 	filter_2_pix_4_1_out_sao ;
						C_block[13]   <= 	filter_2_pix_4_2_out_sao ;
						C_block[14]   <= 	filter_2_pix_4_3_out_sao ;
						C_block[15]   <= 	filter_2_pix_4_4_out_sao ;
					end
					// Check for Right or Bottom block SAO conditions
					if(Xc_in == (pic_width_in - 8)) begin
						state <= STATE_9;
					end
					else if(Yc_in == (pic_height_in - 8)) begin
						state <= STATE_10;							// jump to bottom block
					end
					else begin
						state <= STATE_INIT;
						done  <= 1'b1;
					end
				end
				STATE_9 : begin
					state <= STATE_10;
				end
				STATE_10 : begin
					if((Xc_in==(pic_width_in - 8)) && (Yc_in>0) && (M_block_sao_apply==1)) begin
						M_block[0]    <= 	filter_1_pix_1_1_out_sao ;
						M_block[1]    <= 	filter_1_pix_1_2_out_sao ;
						M_block[2]    <= 	filter_1_pix_1_3_out_sao ;
						M_block[3]    <= 	filter_1_pix_1_4_out_sao ;
						M_block[4]    <= 	filter_1_pix_2_1_out_sao ;
						M_block[5]    <= 	filter_1_pix_2_2_out_sao ;
						M_block[6]    <= 	filter_1_pix_2_3_out_sao ;
						M_block[7]    <= 	filter_1_pix_2_4_out_sao ;
						M_block[8]    <= 	filter_1_pix_3_1_out_sao ;
						M_block[9]    <= 	filter_1_pix_3_2_out_sao ;
						M_block[10]   <= 	filter_1_pix_3_3_out_sao ;
						M_block[11]   <= 	filter_1_pix_3_4_out_sao ;
						M_block[12]   <= 	filter_1_pix_4_1_out_sao ;
						M_block[13]   <= 	filter_1_pix_4_2_out_sao ;
						M_block[14]   <= 	filter_1_pix_4_3_out_sao ;
						M_block[15]   <= 	filter_1_pix_4_4_out_sao ;
						N_block[0]    <= 	filter_2_pix_1_1_out_sao ;
						N_block[1]    <= 	filter_2_pix_1_2_out_sao ;
						N_block[2]    <= 	filter_2_pix_1_3_out_sao ;
						N_block[3]    <= 	filter_2_pix_1_4_out_sao ;
						N_block[4]    <= 	filter_2_pix_2_1_out_sao ;
						N_block[5]    <= 	filter_2_pix_2_2_out_sao ;
						N_block[6]    <= 	filter_2_pix_2_3_out_sao ;
						N_block[7]    <= 	filter_2_pix_2_4_out_sao ;
						N_block[8]    <= 	filter_2_pix_3_1_out_sao ;
						N_block[9]    <= 	filter_2_pix_3_2_out_sao ;
						N_block[10]   <= 	filter_2_pix_3_3_out_sao ;
						N_block[11]   <= 	filter_2_pix_3_4_out_sao ;
						N_block[12]   <= 	filter_2_pix_4_1_out_sao ;
						N_block[13]   <= 	filter_2_pix_4_2_out_sao ;
						N_block[14]   <= 	filter_2_pix_4_3_out_sao ;
						N_block[15]   <= 	filter_2_pix_4_4_out_sao ;
					end
					state <= STATE_11;
				end
				STATE_11 : begin
					// Assign decision samples from BRAM to registers for Bottom block
					if((Xc_in>0) && (Yc_in==(pic_height_in - 8))) begin
						decision_row[0]	<=	d_row_buf_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						decision_row[1]	<= 	d_row_buf_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        decision_row[2]	<= 	d_row_buf_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        decision_row[3]	<= 	d_row_buf_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        decision_row[4]	<= 	d_row_buf_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        decision_row[5]	<= 	d_row_buf_data_out_a [BIT_DEPTH*6  - 1 : BIT_DEPTH*5 ] ;
                        decision_row[6]	<= 	d_row_buf_data_out_a [BIT_DEPTH*7  - 1 : BIT_DEPTH*6 ] ;
                        decision_row[7]	<= 	d_row_buf_data_out_a [BIT_DEPTH*8  - 1 : BIT_DEPTH*7 ] ;
                        decision_row[8]	<= 	d_row_buf_data_out_a [BIT_DEPTH*9  - 1 : BIT_DEPTH*8 ] ;
                        decision_row[9]	<= 	d_row_buf_data_out_a [BIT_DEPTH*10 - 1 : BIT_DEPTH*9 ] ;
						decision_top_left_slice_boundary  <=  d_row_buf_data_out_a [BIT_DEPTH*10] ;
						decision_top_right_slice_id  <=  d_row_buf_data_out_a [BIT_DEPTH*10 + 8 : BIT_DEPTH*10 + 1] ;
						decision_top_right_sao_across_slices_enable  <=  d_row_buf_data_out_a [BIT_DEPTH*10 + 9] ;
					end
					if((Xc_in>8) && (Yc_in==(pic_height_in - 8))) begin
						decision_col_left[0]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						decision_col_left[1]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        decision_col_left[2]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        decision_col_left[3]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        decision_col_left[4]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        decision_col_left[5]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*6  - 1 : BIT_DEPTH*5 ] ;
                        decision_col_left[6]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*7  - 1 : BIT_DEPTH*6 ] ;
                        decision_col_left[7]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*8  - 1 : BIT_DEPTH*7 ] ;
                        decision_col_left[8]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*9  - 1 : BIT_DEPTH*8 ] ;
						decision_bottom_left_slice_id  <=  d_left_col_buf_data_out_a [BIT_DEPTH*9 + 7 : BIT_DEPTH*9] ;
						decision_bottom_left_sao_across_slices_enable  <=  d_left_col_buf_data_out_a [BIT_DEPTH*9 + 8] ;
					end
					state <= STATE_12;
				end
				STATE_12 : begin
					if((Xc_in==(pic_width_in - 8)) && (Yc_in>0) && (M_block_sao_apply==1)) begin
						D_block[0]    <= 	filter_1_pix_1_1_out_sao ;
						D_block[1]    <= 	filter_1_pix_1_2_out_sao ;
						D_block[2]    <= 	filter_1_pix_1_3_out_sao ;
						D_block[3]    <= 	filter_1_pix_1_4_out_sao ;
						D_block[4]    <= 	filter_1_pix_2_1_out_sao ;
						D_block[5]    <= 	filter_1_pix_2_2_out_sao ;
						D_block[6]    <= 	filter_1_pix_2_3_out_sao ;
						D_block[7]    <= 	filter_1_pix_2_4_out_sao ;
						D_block[8]    <= 	filter_1_pix_3_1_out_sao ;
						D_block[9]    <= 	filter_1_pix_3_2_out_sao ;
						D_block[10]   <= 	filter_1_pix_3_3_out_sao ;
						D_block[11]   <= 	filter_1_pix_3_4_out_sao ;
						D_block[12]   <= 	filter_1_pix_4_1_out_sao ;
						D_block[13]   <= 	filter_1_pix_4_2_out_sao ;
						D_block[14]   <= 	filter_1_pix_4_3_out_sao ;
						D_block[15]   <= 	filter_1_pix_4_4_out_sao ;
						E_block[0]    <= 	filter_2_pix_1_1_out_sao ;
						E_block[1]    <= 	filter_2_pix_1_2_out_sao ;
						E_block[2]    <= 	filter_2_pix_1_3_out_sao ;
						E_block[3]    <= 	filter_2_pix_1_4_out_sao ;
						E_block[4]    <= 	filter_2_pix_2_1_out_sao ;
						E_block[5]    <= 	filter_2_pix_2_2_out_sao ;
						E_block[6]    <= 	filter_2_pix_2_3_out_sao ;
						E_block[7]    <= 	filter_2_pix_2_4_out_sao ;
						E_block[8]    <= 	filter_2_pix_3_1_out_sao ;
						E_block[9]    <= 	filter_2_pix_3_2_out_sao ;
						E_block[10]   <= 	filter_2_pix_3_3_out_sao ;
						E_block[11]   <= 	filter_2_pix_3_4_out_sao ;
						E_block[12]   <= 	filter_2_pix_4_1_out_sao ;
						E_block[13]   <= 	filter_2_pix_4_2_out_sao ;
						E_block[14]   <= 	filter_2_pix_4_3_out_sao ;
						E_block[15]   <= 	filter_2_pix_4_4_out_sao ;
					end
					// Check for Bottom block SAO conditions
					if(Yc_in == (pic_height_in - 8)) begin
						state <= STATE_13;
					end
					else begin
						state <= STATE_INIT;
						done  <= 1'b1;
					end
				end
				STATE_13 : begin
					state <= STATE_14;
				end
				STATE_14 : begin
					if(H_block_sao_apply) begin
						H_block[0]    <= 	filter_1_pix_1_1_out_sao ;
						H_block[1]    <= 	filter_1_pix_1_2_out_sao ;
						H_block[2]    <= 	filter_1_pix_1_3_out_sao ;
						H_block[3]    <= 	filter_1_pix_1_4_out_sao ;
						H_block[4]    <= 	filter_1_pix_2_1_out_sao ;
						H_block[5]    <= 	filter_1_pix_2_2_out_sao ;
						H_block[6]    <= 	filter_1_pix_2_3_out_sao ;
						H_block[7]    <= 	filter_1_pix_2_4_out_sao ;
						H_block[8]    <= 	filter_1_pix_3_1_out_sao ;
						H_block[9]    <= 	filter_1_pix_3_2_out_sao ;
						H_block[10]   <= 	filter_1_pix_3_3_out_sao ;
						H_block[11]   <= 	filter_1_pix_3_4_out_sao ;
						H_block[12]   <= 	filter_1_pix_4_1_out_sao ;
						H_block[13]   <= 	filter_1_pix_4_2_out_sao ;
						H_block[14]   <= 	filter_1_pix_4_3_out_sao ;
						H_block[15]   <= 	filter_1_pix_4_4_out_sao ;
						A_block[0]    <= 	filter_2_pix_1_1_out_sao ;
						A_block[1]    <= 	filter_2_pix_1_2_out_sao ;
						A_block[2]    <= 	filter_2_pix_1_3_out_sao ;
						A_block[3]    <= 	filter_2_pix_1_4_out_sao ;
						A_block[4]    <= 	filter_2_pix_2_1_out_sao ;
						A_block[5]    <= 	filter_2_pix_2_2_out_sao ;
						A_block[6]    <= 	filter_2_pix_2_3_out_sao ;
						A_block[7]    <= 	filter_2_pix_2_4_out_sao ;
						A_block[8]    <= 	filter_2_pix_3_1_out_sao ;
						A_block[9]    <= 	filter_2_pix_3_2_out_sao ;
						A_block[10]   <= 	filter_2_pix_3_3_out_sao ;
						A_block[11]   <= 	filter_2_pix_3_4_out_sao ;
						A_block[12]   <= 	filter_2_pix_4_1_out_sao ;
						A_block[13]   <= 	filter_2_pix_4_2_out_sao ;
						A_block[14]   <= 	filter_2_pix_4_3_out_sao ;
						A_block[15]   <= 	filter_2_pix_4_4_out_sao ;
					end
					state <= STATE_15;
				end
				STATE_15 : begin
					// Assign decision samples from BRAM to registers for Bottom Right block
					if((Xc_in==(pic_width_in - 8)) && (Yc_in==(pic_height_in - 8))) begin
						decision_row[0]	<=	d_row_buf_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						decision_row[1]	<= 	d_row_buf_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        decision_row[2]	<= 	d_row_buf_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        decision_row[3]	<= 	d_row_buf_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        decision_row[4]	<= 	d_row_buf_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        decision_row[5]	<= 	d_row_buf_data_out_a [BIT_DEPTH*6  - 1 : BIT_DEPTH*5 ] ;
                        decision_row[6]	<= 	d_row_buf_data_out_a [BIT_DEPTH*7  - 1 : BIT_DEPTH*6 ] ;
                        decision_row[7]	<= 	d_row_buf_data_out_a [BIT_DEPTH*8  - 1 : BIT_DEPTH*7 ] ;
                        decision_row[8]	<= 	d_row_buf_data_out_a [BIT_DEPTH*9  - 1 : BIT_DEPTH*8 ] ;
                        decision_row[9]	<= 	d_row_buf_data_out_a [BIT_DEPTH*10 - 1 : BIT_DEPTH*9 ] ;
						decision_top_left_slice_boundary  <=  d_row_buf_data_out_a [BIT_DEPTH*10] ;
						decision_top_right_slice_id  <=  d_row_buf_data_out_a [BIT_DEPTH*10 + 8 : BIT_DEPTH*10 + 1] ;
						decision_top_right_sao_across_slices_enable  <=  d_row_buf_data_out_a [BIT_DEPTH*10 + 9] ;
					end
					if((Xc_in==(pic_width_in - 8)) && (Yc_in==(pic_height_in - 8))) begin
						decision_col_left[0]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						decision_col_left[1]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        decision_col_left[2]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        decision_col_left[3]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        decision_col_left[4]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        decision_col_left[5]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*6  - 1 : BIT_DEPTH*5 ] ;
                        decision_col_left[6]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*7  - 1 : BIT_DEPTH*6 ] ;
                        decision_col_left[7]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*8  - 1 : BIT_DEPTH*7 ] ;
                        decision_col_left[8]  <=  d_left_col_buf_data_out_a [BIT_DEPTH*9  - 1 : BIT_DEPTH*8 ] ;
						decision_bottom_left_slice_id  <=  d_left_col_buf_data_out_a [BIT_DEPTH*9 + 7 : BIT_DEPTH*9] ;
						decision_bottom_left_sao_across_slices_enable  <=  d_left_col_buf_data_out_a [BIT_DEPTH*9 + 8] ;
					end
					state <= STATE_16;
				end
				STATE_16 : begin
					if(H_block_sao_apply) begin
						I_block[0]    <= 	filter_1_pix_1_1_out_sao ;
						I_block[1]    <= 	filter_1_pix_1_2_out_sao ;
						I_block[2]    <= 	filter_1_pix_1_3_out_sao ;
						I_block[3]    <= 	filter_1_pix_1_4_out_sao ;
						I_block[4]    <= 	filter_1_pix_2_1_out_sao ;
						I_block[5]    <= 	filter_1_pix_2_2_out_sao ;
						I_block[6]    <= 	filter_1_pix_2_3_out_sao ;
						I_block[7]    <= 	filter_1_pix_2_4_out_sao ;
						I_block[8]    <= 	filter_1_pix_3_1_out_sao ;
						I_block[9]    <= 	filter_1_pix_3_2_out_sao ;
						I_block[10]   <= 	filter_1_pix_3_3_out_sao ;
						I_block[11]   <= 	filter_1_pix_3_4_out_sao ;
						I_block[12]   <= 	filter_1_pix_4_1_out_sao ;
						I_block[13]   <= 	filter_1_pix_4_2_out_sao ;
						I_block[14]   <= 	filter_1_pix_4_3_out_sao ;
						I_block[15]   <= 	filter_1_pix_4_4_out_sao ;
						B_block[0]    <= 	filter_2_pix_1_1_out_sao ;
						B_block[1]    <= 	filter_2_pix_1_2_out_sao ;
						B_block[2]    <= 	filter_2_pix_1_3_out_sao ;
						B_block[3]    <= 	filter_2_pix_1_4_out_sao ;
						B_block[4]    <= 	filter_2_pix_2_1_out_sao ;
						B_block[5]    <= 	filter_2_pix_2_2_out_sao ;
						B_block[6]    <= 	filter_2_pix_2_3_out_sao ;
						B_block[7]    <= 	filter_2_pix_2_4_out_sao ;
						B_block[8]    <= 	filter_2_pix_3_1_out_sao ;
						B_block[9]    <= 	filter_2_pix_3_2_out_sao ;
						B_block[10]   <= 	filter_2_pix_3_3_out_sao ;
						B_block[11]   <= 	filter_2_pix_3_4_out_sao ;
						B_block[12]   <= 	filter_2_pix_4_1_out_sao ;
						B_block[13]   <= 	filter_2_pix_4_2_out_sao ;
						B_block[14]   <= 	filter_2_pix_4_3_out_sao ;
						B_block[15]   <= 	filter_2_pix_4_4_out_sao ;
					end
					// Check for Bottom Right block SAO conditions
					if((Xc_in==(pic_width_in - 8)) && (Yc_in==(pic_height_in - 8))) begin
						state <= STATE_17;
					end
					else begin
						state <= STATE_INIT;
						done  <= 1'b1;
					end
				end
				STATE_17 : begin
					if(q1_block_sao_apply) begin
						state <= STATE_18;
					end
					else begin
						state <= STATE_INIT;
						done  <= 1'b1;
					end
				end
				STATE_18 : begin
					q_block_1[0]    <= 	filter_1_pix_1_1_out_sao ;
                    q_block_1[1]    <= 	filter_1_pix_1_2_out_sao ;
                    q_block_1[2]    <= 	filter_1_pix_1_3_out_sao ;
                    q_block_1[3]    <= 	filter_1_pix_1_4_out_sao ;
                    q_block_1[4]    <= 	filter_1_pix_2_1_out_sao ;
                    q_block_1[5]    <= 	filter_1_pix_2_2_out_sao ;
                    q_block_1[6]    <= 	filter_1_pix_2_3_out_sao ;
                    q_block_1[7]    <= 	filter_1_pix_2_4_out_sao ;
                    q_block_1[8]    <= 	filter_1_pix_3_1_out_sao ;
                    q_block_1[9]    <= 	filter_1_pix_3_2_out_sao ;
                    q_block_1[10]   <= 	filter_1_pix_3_3_out_sao ;
                    q_block_1[11]   <= 	filter_1_pix_3_4_out_sao ;
                    q_block_1[12]   <= 	filter_1_pix_4_1_out_sao ;
                    q_block_1[13]   <= 	filter_1_pix_4_2_out_sao ;
                    q_block_1[14]   <= 	filter_1_pix_4_3_out_sao ;
                    q_block_1[15]   <= 	filter_1_pix_4_4_out_sao ;
					q_block_2[0]    <= 	filter_2_pix_1_1_out_sao ;
                    q_block_2[1]    <= 	filter_2_pix_1_2_out_sao ;
                    q_block_2[2]    <= 	filter_2_pix_1_3_out_sao ;
                    q_block_2[3]    <= 	filter_2_pix_1_4_out_sao ;
                    q_block_2[4]    <= 	filter_2_pix_2_1_out_sao ;
                    q_block_2[5]    <= 	filter_2_pix_2_2_out_sao ;
                    q_block_2[6]    <= 	filter_2_pix_2_3_out_sao ;
                    q_block_2[7]    <= 	filter_2_pix_2_4_out_sao ;
                    q_block_2[8]    <= 	filter_2_pix_3_1_out_sao ;
                    q_block_2[9]    <= 	filter_2_pix_3_2_out_sao ;
                    q_block_2[10]   <= 	filter_2_pix_3_3_out_sao ;
                    q_block_2[11]   <= 	filter_2_pix_3_4_out_sao ;
                    q_block_2[12]   <= 	filter_2_pix_4_1_out_sao ;
                    q_block_2[13]   <= 	filter_2_pix_4_2_out_sao ;
                    q_block_2[14]   <= 	filter_2_pix_4_3_out_sao ;
                    q_block_2[15]   <= 	filter_2_pix_4_4_out_sao ;
					state <= STATE_19;
				end
				STATE_19 : begin
					state <= STATE_20;
				end
				STATE_20 : begin
					q_block_3[0]    <= 	filter_1_pix_1_1_out_sao ;
                    q_block_3[1]    <= 	filter_1_pix_1_2_out_sao ;
                    q_block_3[2]    <= 	filter_1_pix_1_3_out_sao ;
                    q_block_3[3]    <= 	filter_1_pix_1_4_out_sao ;
                    q_block_3[4]    <= 	filter_1_pix_2_1_out_sao ;
                    q_block_3[5]    <= 	filter_1_pix_2_2_out_sao ;
                    q_block_3[6]    <= 	filter_1_pix_2_3_out_sao ;
                    q_block_3[7]    <= 	filter_1_pix_2_4_out_sao ;
                    q_block_3[8]    <= 	filter_1_pix_3_1_out_sao ;
                    q_block_3[9]    <= 	filter_1_pix_3_2_out_sao ;
                    q_block_3[10]   <= 	filter_1_pix_3_3_out_sao ;
                    q_block_3[11]   <= 	filter_1_pix_3_4_out_sao ;
                    q_block_3[12]   <= 	filter_1_pix_4_1_out_sao ;
                    q_block_3[13]   <= 	filter_1_pix_4_2_out_sao ;
                    q_block_3[14]   <= 	filter_1_pix_4_3_out_sao ;
                    q_block_3[15]   <= 	filter_1_pix_4_4_out_sao ;
					q_block_4[0]    <= 	filter_2_pix_1_1_out_sao ;
                    q_block_4[1]    <= 	filter_2_pix_1_2_out_sao ;
                    q_block_4[2]    <= 	filter_2_pix_1_3_out_sao ;
                    q_block_4[3]    <= 	filter_2_pix_1_4_out_sao ;
                    q_block_4[4]    <= 	filter_2_pix_2_1_out_sao ;
                    q_block_4[5]    <= 	filter_2_pix_2_2_out_sao ;
                    q_block_4[6]    <= 	filter_2_pix_2_3_out_sao ;
                    q_block_4[7]    <= 	filter_2_pix_2_4_out_sao ;
                    q_block_4[8]    <= 	filter_2_pix_3_1_out_sao ;
                    q_block_4[9]    <= 	filter_2_pix_3_2_out_sao ;
                    q_block_4[10]   <= 	filter_2_pix_3_3_out_sao ;
                    q_block_4[11]   <= 	filter_2_pix_3_4_out_sao ;
                    q_block_4[12]   <= 	filter_2_pix_4_1_out_sao ;
                    q_block_4[13]   <= 	filter_2_pix_4_2_out_sao ;
                    q_block_4[14]   <= 	filter_2_pix_4_3_out_sao ;
                    q_block_4[15]   <= 	filter_2_pix_4_4_out_sao ;
					state <= STATE_INIT;
					done  <= 1'b1;
				end
			endcase
		end
	end
	
	
	//--------------- Combinational block to set up and activate SAO block filter ----------------------
	always @(*) begin
		filter_1_start_sao_filter	=	1'b0 ;
		filter_1_cross_boundary_left_flag_sao   		  =	  1'bx ;
		filter_1_cross_boundary_right_flag_sao         =	  1'bx ;
		filter_1_cross_boundary_top_flag_sao	          =	  1'bx ;
		filter_1_cross_boundary_bottom_flag_sao        =	  1'bx ;
		filter_1_cross_boundary_top_left_flag_sao	  =	  1'bx ;
		filter_1_cross_boundary_top_right_flag_sao	  =	  1'bx ;
		filter_1_cross_boundary_bottom_left_flag_sao   =	  1'bx ;
		filter_1_cross_boundary_bottom_right_flag_sao  =	  1'bx ;
		filter_1_sao_type_in_sao	   		=	2'bxx ;
		filter_1_sao_eo_class_in_sao		=	2'bxx ;
		filter_1_sao_bandpos_in_sao 		=	5'bxxxxx;
		filter_1_sao_offset_1_in_sao		=	4'bxxxx ;
		filter_1_sao_offset_2_in_sao		=	4'bxxxx ;
		filter_1_sao_offset_3_in_sao		=	4'bxxxx ;
		filter_1_sao_offset_4_in_sao		=	4'bxxxx ;
		filter_1_p_0_0_in_sao			=	8'bxxxxxxxx ;
		filter_1_p_0_1_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_0_2_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_0_3_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_0_4_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_0_5_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_1_0_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_1_1_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_1_2_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_1_3_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_1_4_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_1_5_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_2_0_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_2_1_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_2_2_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_2_3_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_2_4_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_2_5_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_3_0_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_3_1_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_3_2_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_3_3_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_3_4_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_3_5_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_4_0_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_4_1_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_4_2_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_4_3_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_4_4_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_4_5_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_5_0_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_5_1_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_5_2_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_5_3_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_5_4_in_sao            =	8'bxxxxxxxx ;
		filter_1_p_5_5_in_sao            =	8'bxxxxxxxx ;
		
		filter_2_start_sao_filter	=	1'b0 ;
		filter_2_cross_boundary_left_flag_sao   		  =	  1'bx ;
		filter_2_cross_boundary_right_flag_sao         =	  1'bx ;
		filter_2_cross_boundary_top_flag_sao	          =	  1'bx ;
		filter_2_cross_boundary_bottom_flag_sao        =	  1'bx ;
		filter_2_cross_boundary_top_left_flag_sao	  =	  1'bx ;
		filter_2_cross_boundary_top_right_flag_sao	  =	  1'bx ;
		filter_2_cross_boundary_bottom_left_flag_sao   =	  1'bx ;
		filter_2_cross_boundary_bottom_right_flag_sao  =	  1'bx ;
		filter_2_sao_type_in_sao	   		=	2'bxx ;
		filter_2_sao_eo_class_in_sao		=	2'bxx ;
		filter_2_sao_bandpos_in_sao 		=	5'bxxxxx;
		filter_2_sao_offset_1_in_sao		=	4'bxxxx ;
		filter_2_sao_offset_2_in_sao		=	4'bxxxx ;
		filter_2_sao_offset_3_in_sao		=	4'bxxxx ;
		filter_2_sao_offset_4_in_sao		=	4'bxxxx ;
		filter_2_p_0_0_in_sao			=	8'bxxxxxxxx ;
		filter_2_p_0_1_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_0_2_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_0_3_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_0_4_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_0_5_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_1_0_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_1_1_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_1_2_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_1_3_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_1_4_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_1_5_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_2_0_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_2_1_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_2_2_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_2_3_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_2_4_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_2_5_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_3_0_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_3_1_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_3_2_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_3_3_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_3_4_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_3_5_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_4_0_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_4_1_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_4_2_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_4_3_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_4_4_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_4_5_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_5_0_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_5_1_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_5_2_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_5_3_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_5_4_in_sao            =	8'bxxxxxxxx ;
		filter_2_p_5_5_in_sao            =	8'bxxxxxxxx ;
		
		case(state)
			STATE_4 : begin
				if((Xc_in>0) && (Yc_in>0) && (J_block_sao_apply==1)) begin					// filter J block
					filter_1_start_sao_filter  =  1'b1 ;
				end
				filter_1_cross_boundary_left_flag_sao   		  =	  J_block_cross_boundary_left 	;
				filter_1_cross_boundary_right_flag_sao         =	  1'b1 	;
				filter_1_cross_boundary_top_flag_sao	          =	  J_block_cross_boundary_top 	;
				filter_1_cross_boundary_bottom_flag_sao        =	  1'b1 ;
				filter_1_cross_boundary_top_left_flag_sao	  =	  J_block_cross_boundary_top_left ;
				filter_1_cross_boundary_top_right_flag_sao	  =	  J_block_cross_boundary_top ;
				filter_1_cross_boundary_bottom_left_flag_sao   =	  J_block_cross_boundary_left ;
				filter_1_cross_boundary_bottom_right_flag_sao  =	  1'b1 ;
				filter_1_sao_type_in_sao	   		=	J_block_y_sao_type     ;
				filter_1_sao_eo_class_in_sao		=	J_block_y_eoclass      ;
				filter_1_sao_bandpos_in_sao 		=	J_block_y_bandpos      ;
				filter_1_sao_offset_1_in_sao		=	J_block_y_sao_offset_1 ;
				filter_1_sao_offset_2_in_sao		=	J_block_y_sao_offset_2 ;
				filter_1_sao_offset_3_in_sao		=	J_block_y_sao_offset_3 ;
				filter_1_sao_offset_4_in_sao		=	J_block_y_sao_offset_4 ;
				filter_1_p_0_0_in_sao			=	decision_row[0] ;
				filter_1_p_0_1_in_sao            =	decision_row[1] ;
				filter_1_p_0_2_in_sao            =	decision_row[2] ;
				filter_1_p_0_3_in_sao            =	decision_row[3] ;
				filter_1_p_0_4_in_sao            =	decision_row[4] ;
				filter_1_p_0_5_in_sao            =	decision_row[5] ;
				filter_1_p_1_0_in_sao            =	decision_col_left[0] ;
				filter_1_p_1_1_in_sao            =	J_block[0] ;
				filter_1_p_1_2_in_sao            =	J_block[1] ;
				filter_1_p_1_3_in_sao            =	J_block[2] ;
				filter_1_p_1_4_in_sao            =	J_block[3] ;
				filter_1_p_1_5_in_sao            =	K_block[0] ;
				filter_1_p_2_0_in_sao            =	decision_col_left[1] ;
				filter_1_p_2_1_in_sao            =	J_block[4] ;
				filter_1_p_2_2_in_sao            =	J_block[5] ;
				filter_1_p_2_3_in_sao            =	J_block[6] ;
				filter_1_p_2_4_in_sao            =	J_block[7] ;
				filter_1_p_2_5_in_sao            =	K_block[4] ;
				filter_1_p_3_0_in_sao            =	decision_col_left[2] ;
				filter_1_p_3_1_in_sao            =	J_block[8]  ;
				filter_1_p_3_2_in_sao            =	J_block[9]  ;
				filter_1_p_3_3_in_sao            =	J_block[10] ;
				filter_1_p_3_4_in_sao            =	J_block[11] ;
				filter_1_p_3_5_in_sao            =	K_block[8]  ;
				filter_1_p_4_0_in_sao            =	decision_col_left[3] ;
				filter_1_p_4_1_in_sao            =	J_block[12] ;
				filter_1_p_4_2_in_sao            =	J_block[13] ;
				filter_1_p_4_3_in_sao            =	J_block[14] ;
				filter_1_p_4_4_in_sao            =	J_block[15] ;
				filter_1_p_4_5_in_sao            =	K_block[12] ;
				filter_1_p_5_0_in_sao            =	decision_col_left[4] ;
				filter_1_p_5_1_in_sao            =	G_block[0] ;
				filter_1_p_5_2_in_sao            =	G_block[1] ;
				filter_1_p_5_3_in_sao            =	G_block[2] ;
				filter_1_p_5_4_in_sao            =	G_block[3] ;
				filter_1_p_5_5_in_sao            =	C_block[0] ;
				if((Xc_in>0) && (Yc_in>0) && (J_block_sao_apply==1)) begin					// filter K block
					filter_2_start_sao_filter  =  1'b1 ;
				end
				filter_2_cross_boundary_left_flag_sao   		  =	  1'b1 ;
				filter_2_cross_boundary_right_flag_sao         =	  J_block_cross_boundary_right ;
				filter_2_cross_boundary_top_flag_sao	          =	  J_block_cross_boundary_top   ;
				filter_2_cross_boundary_bottom_flag_sao        =	  1'b1 ;
				filter_2_cross_boundary_top_left_flag_sao	  =	  J_block_cross_boundary_top   ;
				filter_2_cross_boundary_top_right_flag_sao	  =	  J_block_cross_boundary_top_right ;
				filter_2_cross_boundary_bottom_left_flag_sao   =	  1'b1 ;
				filter_2_cross_boundary_bottom_right_flag_sao  =	  J_block_cross_boundary_right ;
				filter_2_sao_type_in_sao	   		=	J_block_y_sao_type     ;
				filter_2_sao_eo_class_in_sao		=	J_block_y_eoclass      ;
				filter_2_sao_bandpos_in_sao 		=	J_block_y_bandpos      ;
				filter_2_sao_offset_1_in_sao		=	J_block_y_sao_offset_1 ;
				filter_2_sao_offset_2_in_sao		=	J_block_y_sao_offset_2 ;
				filter_2_sao_offset_3_in_sao		=	J_block_y_sao_offset_3 ;
				filter_2_sao_offset_4_in_sao		=	J_block_y_sao_offset_4 ;
				filter_2_p_0_0_in_sao			=	decision_row[4] ;
				filter_2_p_0_1_in_sao            =	decision_row[5] ;
				filter_2_p_0_2_in_sao            =	decision_row[6] ;
				filter_2_p_0_3_in_sao            =	decision_row[7] ;
				filter_2_p_0_4_in_sao            =	decision_row[8] ;
				filter_2_p_0_5_in_sao            =	decision_row[9] ;
				filter_2_p_1_0_in_sao            =	J_block[3] ;
				filter_2_p_1_1_in_sao            =	K_block[0] ;
				filter_2_p_1_2_in_sao            =	K_block[1] ;
				filter_2_p_1_3_in_sao            =	K_block[2] ;
				filter_2_p_1_4_in_sao            =	K_block[3] ;
				filter_2_p_1_5_in_sao            =	decision_col_right[0] ;
				filter_2_p_2_0_in_sao            =	J_block[7] ;
				filter_2_p_2_1_in_sao            =	K_block[4] ;
				filter_2_p_2_2_in_sao            =	K_block[5] ;
				filter_2_p_2_3_in_sao            =	K_block[6] ;
				filter_2_p_2_4_in_sao            =	K_block[7] ;
				filter_2_p_2_5_in_sao            =	decision_col_right[1] ;
				filter_2_p_3_0_in_sao            =	J_block[11] ;
				filter_2_p_3_1_in_sao            =	K_block[8]  ;
				filter_2_p_3_2_in_sao            =	K_block[9]  ;
				filter_2_p_3_3_in_sao            =	K_block[10] ;
				filter_2_p_3_4_in_sao            =	K_block[11] ;
				filter_2_p_3_5_in_sao            =	decision_col_right[2]  ;
				filter_2_p_4_0_in_sao            =	J_block[15] ;
				filter_2_p_4_1_in_sao            =	K_block[12] ;
				filter_2_p_4_2_in_sao            =	K_block[13] ;
				filter_2_p_4_3_in_sao            =	K_block[14] ;
				filter_2_p_4_4_in_sao            =	K_block[15] ;
				filter_2_p_4_5_in_sao            =	decision_col_right[3] ;
				filter_2_p_5_0_in_sao            =	G_block[3] ;
				filter_2_p_5_1_in_sao            =	C_block[0] ;
				filter_2_p_5_2_in_sao            =	C_block[1] ;
				filter_2_p_5_3_in_sao            =	C_block[2] ;
				filter_2_p_5_4_in_sao            =	C_block[3] ;
				filter_2_p_5_5_in_sao            =	D_block[0] ;
			end
			STATE_6 : begin
				if((Xc_in>0) && (Yc_in>0) && (J_block_sao_apply==1)) begin					// filter G block
					filter_1_start_sao_filter  =  1'b1 ;
				end
				filter_1_cross_boundary_left_flag_sao   		  =	  J_block_cross_boundary_left 	;
				filter_1_cross_boundary_right_flag_sao         =	  1'b1 	;
				filter_1_cross_boundary_top_flag_sao	          =	  1'b1 	;
				filter_1_cross_boundary_bottom_flag_sao        =	  J_block_cross_boundary_bottom ;
				filter_1_cross_boundary_top_left_flag_sao	  =	  J_block_cross_boundary_left 	;
				filter_1_cross_boundary_top_right_flag_sao	  =	  1'b1 ;
				filter_1_cross_boundary_bottom_left_flag_sao   =	  J_block_cross_boundary_bottom_left ;
				filter_1_cross_boundary_bottom_right_flag_sao  =	  J_block_cross_boundary_bottom ;
				filter_1_sao_type_in_sao	   		=	J_block_y_sao_type     ;
				filter_1_sao_eo_class_in_sao		=	J_block_y_eoclass      ;
				filter_1_sao_bandpos_in_sao 		=	J_block_y_bandpos      ;
				filter_1_sao_offset_1_in_sao		=	J_block_y_sao_offset_1 ;
				filter_1_sao_offset_2_in_sao		=	J_block_y_sao_offset_2 ;
				filter_1_sao_offset_3_in_sao		=	J_block_y_sao_offset_3 ;
				filter_1_sao_offset_4_in_sao		=	J_block_y_sao_offset_4 ;
				filter_1_p_0_0_in_sao			=	decision_col_left[3] ;
				filter_1_p_0_1_in_sao            =	J_block[12] ;
				filter_1_p_0_2_in_sao            =	J_block[13] ;
				filter_1_p_0_3_in_sao            =	J_block[14] ;
				filter_1_p_0_4_in_sao            =	J_block[15] ;
				filter_1_p_0_5_in_sao            =	K_block[12] ;
				filter_1_p_1_0_in_sao            =	decision_col_left[4] ;
				filter_1_p_1_1_in_sao            =	G_block[0] ;
				filter_1_p_1_2_in_sao            =	G_block[1] ;
				filter_1_p_1_3_in_sao            =	G_block[2] ;
				filter_1_p_1_4_in_sao            =	G_block[3] ;
				filter_1_p_1_5_in_sao            =	C_block[0] ;
				filter_1_p_2_0_in_sao            =	decision_col_left[5] ;
				filter_1_p_2_1_in_sao            =	G_block[4] ;
				filter_1_p_2_2_in_sao            =	G_block[5] ;
				filter_1_p_2_3_in_sao            =	G_block[6] ;
				filter_1_p_2_4_in_sao            =	G_block[7] ;
				filter_1_p_2_5_in_sao            =	C_block[4] ;
				filter_1_p_3_0_in_sao            =	decision_col_left[6] ;
				filter_1_p_3_1_in_sao            =	G_block[8]  ;
				filter_1_p_3_2_in_sao            =	G_block[9]  ;
				filter_1_p_3_3_in_sao            =	G_block[10] ;
				filter_1_p_3_4_in_sao            =	G_block[11] ;
				filter_1_p_3_5_in_sao            =	C_block[8]  ;
				filter_1_p_4_0_in_sao            =	decision_col_left[7] ;
				filter_1_p_4_1_in_sao            =	G_block[12] ;
				filter_1_p_4_2_in_sao            =	G_block[13] ;
				filter_1_p_4_3_in_sao            =	G_block[14] ;
				filter_1_p_4_4_in_sao            =	G_block[15] ;
				filter_1_p_4_5_in_sao            =	C_block[12] ;
				filter_1_p_5_0_in_sao            =	decision_col_left[8] ;
				filter_1_p_5_1_in_sao            =	H_block[0] ;
				filter_1_p_5_2_in_sao            =	H_block[1] ;
				filter_1_p_5_3_in_sao            =	H_block[2] ;
				filter_1_p_5_4_in_sao            =	H_block[3] ;
				filter_1_p_5_5_in_sao            =	A_block[0] ;
				if((Xc_in>0) && (Yc_in>0) && (J_block_sao_apply==1)) begin					// filter C block
					filter_2_start_sao_filter  =  1'b1 ;
				end
				filter_2_cross_boundary_left_flag_sao   		  =	  1'b1 ;
				filter_2_cross_boundary_right_flag_sao         =	  J_block_cross_boundary_right 	;
				filter_2_cross_boundary_top_flag_sao	          =	  1'b1 ;
				filter_2_cross_boundary_bottom_flag_sao        =	  J_block_cross_boundary_bottom	;
				filter_2_cross_boundary_top_left_flag_sao	  =	  1'b1 ;
				filter_2_cross_boundary_top_right_flag_sao	  =	  J_block_cross_boundary_right 	;
				filter_2_cross_boundary_bottom_left_flag_sao   =	  J_block_cross_boundary_bottom	;
				filter_2_cross_boundary_bottom_right_flag_sao  =	  J_block_cross_boundary_bottom_right ;
				filter_2_sao_type_in_sao	   		=	J_block_y_sao_type     ;
				filter_2_sao_eo_class_in_sao		=	J_block_y_eoclass      ;
				filter_2_sao_bandpos_in_sao 		=	J_block_y_bandpos      ;
				filter_2_sao_offset_1_in_sao		=	J_block_y_sao_offset_1 ;
				filter_2_sao_offset_2_in_sao		=	J_block_y_sao_offset_2 ;
				filter_2_sao_offset_3_in_sao		=	J_block_y_sao_offset_3 ;
				filter_2_sao_offset_4_in_sao		=	J_block_y_sao_offset_4 ;
				filter_2_p_0_0_in_sao			=	J_block[15] ;
				filter_2_p_0_1_in_sao            =	K_block[12] ;
				filter_2_p_0_2_in_sao            =	K_block[13] ;
				filter_2_p_0_3_in_sao            =	K_block[14] ;
				filter_2_p_0_4_in_sao            =	K_block[15] ;
				filter_2_p_0_5_in_sao            =	decision_col_right[3] ;
				filter_2_p_1_0_in_sao            =	G_block[3] ;
				filter_2_p_1_1_in_sao            =	C_block[0] ;
				filter_2_p_1_2_in_sao            =	C_block[1] ;
				filter_2_p_1_3_in_sao            =	C_block[2] ;
				filter_2_p_1_4_in_sao            =	C_block[3] ;
				filter_2_p_1_5_in_sao            =	D_block[0] ;
				filter_2_p_2_0_in_sao            =	G_block[7] ;
				filter_2_p_2_1_in_sao            =	C_block[4] ;
				filter_2_p_2_2_in_sao            =	C_block[5] ;
				filter_2_p_2_3_in_sao            =	C_block[6] ;
				filter_2_p_2_4_in_sao            =	C_block[7] ;
				filter_2_p_2_5_in_sao            =	D_block[4] ;
				filter_2_p_3_0_in_sao            =	G_block[11] ;
				filter_2_p_3_1_in_sao            =	C_block[8]  ;
				filter_2_p_3_2_in_sao            =	C_block[9]  ;
				filter_2_p_3_3_in_sao            =	C_block[10] ;
				filter_2_p_3_4_in_sao            =	C_block[11] ;
				filter_2_p_3_5_in_sao            =	D_block[8]  ;
				filter_2_p_4_0_in_sao            =	G_block[15] ;
				filter_2_p_4_1_in_sao            =	C_block[12] ;
				filter_2_p_4_2_in_sao            =	C_block[13] ;
				filter_2_p_4_3_in_sao            =	C_block[14] ;
				filter_2_p_4_4_in_sao            =	C_block[15] ;
				filter_2_p_4_5_in_sao            =	D_block[12] ;
				filter_2_p_5_0_in_sao            =	H_block[3] ;
				filter_2_p_5_1_in_sao            =	A_block[0] ;
				filter_2_p_5_2_in_sao            =	A_block[1] ;
				filter_2_p_5_3_in_sao            =	A_block[2] ;
				filter_2_p_5_4_in_sao            =	A_block[3] ;
				filter_2_p_5_5_in_sao            =	q_block_1[0] ;
			end
			STATE_8 : begin
				if((Xc_in==(pic_width_in - 8)) && (Yc_in>0) && (M_block_sao_apply==1)) begin			// filter M block
					filter_1_start_sao_filter  =  1'b1 ;
				end
				filter_1_cross_boundary_left_flag_sao   		  =	  M_block_cross_boundary_left 	;
				filter_1_cross_boundary_right_flag_sao         =	  1'b1 	;
				filter_1_cross_boundary_top_flag_sao	          =	  M_block_cross_boundary_top 	;
				filter_1_cross_boundary_bottom_flag_sao        =	  1'b1 ;
				filter_1_cross_boundary_top_left_flag_sao	  =	  M_block_cross_boundary_top_left ;
				filter_1_cross_boundary_top_right_flag_sao	  =	  M_block_cross_boundary_top ;
				filter_1_cross_boundary_bottom_left_flag_sao   =	  M_block_cross_boundary_left ;
				filter_1_cross_boundary_bottom_right_flag_sao  =	  1'b1 ;
				filter_1_sao_type_in_sao	   		=	M_block_y_sao_type     ;
				filter_1_sao_eo_class_in_sao		=	M_block_y_eoclass      ;
				filter_1_sao_bandpos_in_sao 		=	M_block_y_bandpos      ;
				filter_1_sao_offset_1_in_sao		=	M_block_y_sao_offset_1 ;
				filter_1_sao_offset_2_in_sao		=	M_block_y_sao_offset_2 ;
				filter_1_sao_offset_3_in_sao		=	M_block_y_sao_offset_3 ;
				filter_1_sao_offset_4_in_sao		=	M_block_y_sao_offset_4 ;
				filter_1_p_0_0_in_sao			=	decision_row[0] ;
				filter_1_p_0_1_in_sao            =	decision_row[1] ;
				filter_1_p_0_2_in_sao            =	decision_row[2] ;
				filter_1_p_0_3_in_sao            =	decision_row[3] ;
				filter_1_p_0_4_in_sao            =	decision_row[4] ;
				filter_1_p_0_5_in_sao            =	decision_row[5] ;
				filter_1_p_1_0_in_sao            =	decision_col_left[0] ;
				filter_1_p_1_1_in_sao            =	M_block[0] ;
				filter_1_p_1_2_in_sao            =	M_block[1] ;
				filter_1_p_1_3_in_sao            =	M_block[2] ;
				filter_1_p_1_4_in_sao            =	M_block[3] ;
				filter_1_p_1_5_in_sao            =	N_block[0] ;
				filter_1_p_2_0_in_sao            =	decision_col_left[1] ;
				filter_1_p_2_1_in_sao            =	M_block[4] ;
				filter_1_p_2_2_in_sao            =	M_block[5] ;
				filter_1_p_2_3_in_sao            =	M_block[6] ;
				filter_1_p_2_4_in_sao            =	M_block[7] ;
				filter_1_p_2_5_in_sao            =	N_block[4] ;
				filter_1_p_3_0_in_sao            =	decision_col_left[2] ;
				filter_1_p_3_1_in_sao            =	M_block[8]  ;
				filter_1_p_3_2_in_sao            =	M_block[9]  ;
				filter_1_p_3_3_in_sao            =	M_block[10] ;
				filter_1_p_3_4_in_sao            =	M_block[11] ;
				filter_1_p_3_5_in_sao            =	N_block[8]  ;
				filter_1_p_4_0_in_sao            =	decision_col_left[3] ;
				filter_1_p_4_1_in_sao            =	M_block[12] ;
				filter_1_p_4_2_in_sao            =	M_block[13] ;
				filter_1_p_4_3_in_sao            =	M_block[14] ;
				filter_1_p_4_4_in_sao            =	M_block[15] ;
				filter_1_p_4_5_in_sao            =	N_block[12] ;
				filter_1_p_5_0_in_sao            =	decision_col_left[4] ;
				filter_1_p_5_1_in_sao            =	D_block[0] ;
				filter_1_p_5_2_in_sao            =	D_block[1] ;
				filter_1_p_5_3_in_sao            =	D_block[2] ;
				filter_1_p_5_4_in_sao            =	D_block[3] ;
				filter_1_p_5_5_in_sao            =	E_block[0] ;
				if((Xc_in==(pic_width_in - 8)) && (Yc_in>0) && (M_block_sao_apply==1)) begin			// filter N block
					filter_2_start_sao_filter  =  1'b1 ;
				end
				filter_2_cross_boundary_left_flag_sao   		  =	  1'b1 ;
				filter_2_cross_boundary_right_flag_sao         =	  M_block_cross_boundary_right ;
				filter_2_cross_boundary_top_flag_sao	          =	  M_block_cross_boundary_top   ;
				filter_2_cross_boundary_bottom_flag_sao        =	  1'b1 ;
				filter_2_cross_boundary_top_left_flag_sao	  =	  M_block_cross_boundary_top   ;
				filter_2_cross_boundary_top_right_flag_sao	  =	  M_block_cross_boundary_top_right ;
				filter_2_cross_boundary_bottom_left_flag_sao   =	  1'b1 ;
				filter_2_cross_boundary_bottom_right_flag_sao  =	  M_block_cross_boundary_right ;
				filter_2_sao_type_in_sao	   		=	M_block_y_sao_type     ;
				filter_2_sao_eo_class_in_sao		=	M_block_y_eoclass      ;
				filter_2_sao_bandpos_in_sao 		=	M_block_y_bandpos      ;
				filter_2_sao_offset_1_in_sao		=	M_block_y_sao_offset_1 ;
				filter_2_sao_offset_2_in_sao		=	M_block_y_sao_offset_2 ;
				filter_2_sao_offset_3_in_sao		=	M_block_y_sao_offset_3 ;
				filter_2_sao_offset_4_in_sao		=	M_block_y_sao_offset_4 ;
				filter_2_p_0_0_in_sao			=	decision_row[4] ;
				filter_2_p_0_1_in_sao            =	decision_row[5] ;
				filter_2_p_0_2_in_sao            =	decision_row[6] ;
				filter_2_p_0_3_in_sao            =	decision_row[7] ;
				filter_2_p_0_4_in_sao            =	decision_row[8] ;
				filter_2_p_0_5_in_sao            =	decision_row[9] ;
				filter_2_p_1_0_in_sao            =	M_block[3] ;
				filter_2_p_1_1_in_sao            =	N_block[0] ;
				filter_2_p_1_2_in_sao            =	N_block[1] ;
				filter_2_p_1_3_in_sao            =	N_block[2] ;
				filter_2_p_1_4_in_sao            =	N_block[3] ;
				filter_2_p_1_5_in_sao            =	8'd0 ;
				filter_2_p_2_0_in_sao            =	M_block[7] ;
				filter_2_p_2_1_in_sao            =	N_block[4] ;
				filter_2_p_2_2_in_sao            =	N_block[5] ;
				filter_2_p_2_3_in_sao            =	N_block[6] ;
				filter_2_p_2_4_in_sao            =	N_block[7] ;
				filter_2_p_2_5_in_sao            =	8'd0 ;
				filter_2_p_3_0_in_sao            =	M_block[11] ;
				filter_2_p_3_1_in_sao            =	N_block[8]  ;
				filter_2_p_3_2_in_sao            =	N_block[9]  ;
				filter_2_p_3_3_in_sao            =	N_block[10] ;
				filter_2_p_3_4_in_sao            =	N_block[11] ;
				filter_2_p_3_5_in_sao            =	8'd0 ;
				filter_2_p_4_0_in_sao            =	M_block[15] ;
				filter_2_p_4_1_in_sao            =	N_block[12] ;
				filter_2_p_4_2_in_sao            =	N_block[13] ;
				filter_2_p_4_3_in_sao            =	N_block[14] ;
				filter_2_p_4_4_in_sao            =	N_block[15] ;
				filter_2_p_4_5_in_sao            =	8'd0 ;
				filter_2_p_5_0_in_sao            =	D_block[3] ;
				filter_2_p_5_1_in_sao            =	E_block[0] ;
				filter_2_p_5_2_in_sao            =	E_block[1] ;
				filter_2_p_5_3_in_sao            =	E_block[2] ;
				filter_2_p_5_4_in_sao            =	E_block[3] ;
				filter_2_p_5_5_in_sao            =	8'd0 ;
			end
			STATE_10 : begin
				if((Xc_in==(pic_width_in - 8)) && (Yc_in>0) && (M_block_sao_apply==1)) begin			// filter D block
					filter_1_start_sao_filter  =  1'b1 ;
				end
				filter_1_cross_boundary_left_flag_sao   		  =	  M_block_cross_boundary_left 	;
				filter_1_cross_boundary_right_flag_sao         =	  1'b1 	;
				filter_1_cross_boundary_top_flag_sao	          =	  1'b1 	;
				filter_1_cross_boundary_bottom_flag_sao        =	  M_block_cross_boundary_bottom ;
				filter_1_cross_boundary_top_left_flag_sao	  =	  M_block_cross_boundary_left 	;
				filter_1_cross_boundary_top_right_flag_sao	  =	  1'b1 ;
				filter_1_cross_boundary_bottom_left_flag_sao   =	  M_block_cross_boundary_bottom_left ;
				filter_1_cross_boundary_bottom_right_flag_sao  =	  M_block_cross_boundary_bottom ;
				filter_1_sao_type_in_sao	   		=	M_block_y_sao_type     ;
				filter_1_sao_eo_class_in_sao		=	M_block_y_eoclass      ;
				filter_1_sao_bandpos_in_sao 		=	M_block_y_bandpos      ;
				filter_1_sao_offset_1_in_sao		=	M_block_y_sao_offset_1 ;
				filter_1_sao_offset_2_in_sao		=	M_block_y_sao_offset_2 ;
				filter_1_sao_offset_3_in_sao		=	M_block_y_sao_offset_3 ;
				filter_1_sao_offset_4_in_sao		=	M_block_y_sao_offset_4 ;
				filter_1_p_0_0_in_sao			=	decision_col_left[3] ;
				filter_1_p_0_1_in_sao            =	M_block[12] ;
				filter_1_p_0_2_in_sao            =	M_block[13] ;
				filter_1_p_0_3_in_sao            =	M_block[14] ;
				filter_1_p_0_4_in_sao            =	M_block[15] ;
				filter_1_p_0_5_in_sao            =	N_block[12] ;
				filter_1_p_1_0_in_sao            =	decision_col_left[4] ;
				filter_1_p_1_1_in_sao            =	D_block[0] ;
				filter_1_p_1_2_in_sao            =	D_block[1] ;
				filter_1_p_1_3_in_sao            =	D_block[2] ;
				filter_1_p_1_4_in_sao            =	D_block[3] ;
				filter_1_p_1_5_in_sao            =	E_block[0] ;
				filter_1_p_2_0_in_sao            =	decision_col_left[5] ;
				filter_1_p_2_1_in_sao            =	D_block[4] ;
				filter_1_p_2_2_in_sao            =	D_block[5] ;
				filter_1_p_2_3_in_sao            =	D_block[6] ;
				filter_1_p_2_4_in_sao            =	D_block[7] ;
				filter_1_p_2_5_in_sao            =	E_block[4] ;
				filter_1_p_3_0_in_sao            =	decision_col_left[6] ;
				filter_1_p_3_1_in_sao            =	D_block[8]  ;
				filter_1_p_3_2_in_sao            =	D_block[9]  ;
				filter_1_p_3_3_in_sao            =	D_block[10] ;
				filter_1_p_3_4_in_sao            =	D_block[11] ;
				filter_1_p_3_5_in_sao            =	E_block[8]  ;
				filter_1_p_4_0_in_sao            =	decision_col_left[7] ;
				filter_1_p_4_1_in_sao            =	D_block[12] ;
				filter_1_p_4_2_in_sao            =	D_block[13] ;
				filter_1_p_4_3_in_sao            =	D_block[14] ;
				filter_1_p_4_4_in_sao            =	D_block[15] ;
				filter_1_p_4_5_in_sao            =	E_block[12] ;
				filter_1_p_5_0_in_sao            =	decision_col_left[8] ;
				filter_1_p_5_1_in_sao            =	q_block_1[0] ;
				filter_1_p_5_2_in_sao            =	q_block_1[1] ;
				filter_1_p_5_3_in_sao            =	q_block_1[2] ;
				filter_1_p_5_4_in_sao            =	q_block_1[3] ;
				filter_1_p_5_5_in_sao            =	q_block_2[0] ;
				if((Xc_in==(pic_width_in - 8)) && (Yc_in>0) && (M_block_sao_apply==1)) begin			// filter E block
					filter_2_start_sao_filter  =  1'b1 ;
				end
				filter_2_cross_boundary_left_flag_sao   		  =	  1'b1 ;
				filter_2_cross_boundary_right_flag_sao         =	  M_block_cross_boundary_right 	;
				filter_2_cross_boundary_top_flag_sao	          =	  1'b1 ;
				filter_2_cross_boundary_bottom_flag_sao        =	  M_block_cross_boundary_bottom	;
				filter_2_cross_boundary_top_left_flag_sao	  =	  1'b1 ;
				filter_2_cross_boundary_top_right_flag_sao	  =	  M_block_cross_boundary_right 	;
				filter_2_cross_boundary_bottom_left_flag_sao   =	  M_block_cross_boundary_bottom	;
				filter_2_cross_boundary_bottom_right_flag_sao  =	  M_block_cross_boundary_bottom_right ;
				filter_2_sao_type_in_sao	   		=	M_block_y_sao_type     ;
				filter_2_sao_eo_class_in_sao		=	M_block_y_eoclass      ;
				filter_2_sao_bandpos_in_sao 		=	M_block_y_bandpos      ;
				filter_2_sao_offset_1_in_sao		=	M_block_y_sao_offset_1 ;
				filter_2_sao_offset_2_in_sao		=	M_block_y_sao_offset_2 ;
				filter_2_sao_offset_3_in_sao		=	M_block_y_sao_offset_3 ;
				filter_2_sao_offset_4_in_sao		=	M_block_y_sao_offset_4 ;
				filter_2_p_0_0_in_sao			=	M_block[15] ;
				filter_2_p_0_1_in_sao            =	N_block[12] ;
				filter_2_p_0_2_in_sao            =	N_block[13] ;
				filter_2_p_0_3_in_sao            =	N_block[14] ;
				filter_2_p_0_4_in_sao            =	N_block[15] ;
				filter_2_p_0_5_in_sao            =	8'd0 ;
				filter_2_p_1_0_in_sao            =	D_block[3] ;
				filter_2_p_1_1_in_sao            =	E_block[0] ;
				filter_2_p_1_2_in_sao            =	E_block[1] ;
				filter_2_p_1_3_in_sao            =	E_block[2] ;
				filter_2_p_1_4_in_sao            =	E_block[3] ;
				filter_2_p_1_5_in_sao            =	8'd0 ;
				filter_2_p_2_0_in_sao            =	D_block[7] ;
				filter_2_p_2_1_in_sao            =	E_block[4] ;
				filter_2_p_2_2_in_sao            =	E_block[5] ;
				filter_2_p_2_3_in_sao            =	E_block[6] ;
				filter_2_p_2_4_in_sao            =	E_block[7] ;
				filter_2_p_2_5_in_sao            =	8'd0 ;
				filter_2_p_3_0_in_sao            =	D_block[11] ;
				filter_2_p_3_1_in_sao            =	E_block[8]  ;
				filter_2_p_3_2_in_sao            =	E_block[9]  ;
				filter_2_p_3_3_in_sao            =	E_block[10] ;
				filter_2_p_3_4_in_sao            =	E_block[11] ;
				filter_2_p_3_5_in_sao            =	8'd0 ;
				filter_2_p_4_0_in_sao            =	D_block[15] ;
				filter_2_p_4_1_in_sao            =	E_block[12] ;
				filter_2_p_4_2_in_sao            =	E_block[13] ;
				filter_2_p_4_3_in_sao            =	E_block[14] ;
				filter_2_p_4_4_in_sao            =	E_block[15] ;
				filter_2_p_4_5_in_sao            =	8'd0 ;
				filter_2_p_5_0_in_sao            =	q_block_1[3] ;
				filter_2_p_5_1_in_sao            =	q_block_2[0] ;
				filter_2_p_5_2_in_sao            =	q_block_2[1] ;
				filter_2_p_5_3_in_sao            =	q_block_2[2] ;
				filter_2_p_5_4_in_sao            =	q_block_2[3] ;
				filter_2_p_5_5_in_sao            =	8'd0 ;
			end
			STATE_12 : begin
				if((Xc_in>0) && (Yc_in==(pic_height_in - 8)) && (H_block_sao_apply==1)) begin			// filter H block
					filter_1_start_sao_filter  =  1'b1 ;
				end
				filter_1_cross_boundary_left_flag_sao   		  =	  H_block_cross_boundary_left 	;
				filter_1_cross_boundary_right_flag_sao         =	  1'b1 	;
				filter_1_cross_boundary_top_flag_sao	          =	  H_block_cross_boundary_top 	;
				filter_1_cross_boundary_bottom_flag_sao        =	  1'b1 ;
				filter_1_cross_boundary_top_left_flag_sao	  =	  H_block_cross_boundary_top_left ;
				filter_1_cross_boundary_top_right_flag_sao	  =	  H_block_cross_boundary_top ;
				filter_1_cross_boundary_bottom_left_flag_sao   =	  H_block_cross_boundary_left ;
				filter_1_cross_boundary_bottom_right_flag_sao  =	  1'b1 ;
				filter_1_sao_type_in_sao	   		=	H_block_y_sao_type     ;
				filter_1_sao_eo_class_in_sao		=	H_block_y_eoclass      ;
				filter_1_sao_bandpos_in_sao 		=	H_block_y_bandpos      ;
				filter_1_sao_offset_1_in_sao		=	H_block_y_sao_offset_1 ;
				filter_1_sao_offset_2_in_sao		=	H_block_y_sao_offset_2 ;
				filter_1_sao_offset_3_in_sao		=	H_block_y_sao_offset_3 ;
				filter_1_sao_offset_4_in_sao		=	H_block_y_sao_offset_4 ;
				filter_1_p_0_0_in_sao			=	decision_row[0] ;
				filter_1_p_0_1_in_sao            =	decision_row[1] ;
				filter_1_p_0_2_in_sao            =	decision_row[2] ;
				filter_1_p_0_3_in_sao            =	decision_row[3] ;
				filter_1_p_0_4_in_sao            =	decision_row[4] ;
				filter_1_p_0_5_in_sao            =	decision_row[5] ;
				filter_1_p_1_0_in_sao            =	decision_col_left[0] ;
				filter_1_p_1_1_in_sao            =	H_block[0] ;
				filter_1_p_1_2_in_sao            =	H_block[1] ;
				filter_1_p_1_3_in_sao            =	H_block[2] ;
				filter_1_p_1_4_in_sao            =	H_block[3] ;
				filter_1_p_1_5_in_sao            =	A_block[0] ;
				filter_1_p_2_0_in_sao            =	decision_col_left[1] ;
				filter_1_p_2_1_in_sao            =	H_block[4] ;
				filter_1_p_2_2_in_sao            =	H_block[5] ;
				filter_1_p_2_3_in_sao            =	H_block[6] ;
				filter_1_p_2_4_in_sao            =	H_block[7] ;
				filter_1_p_2_5_in_sao            =	A_block[4] ;
				filter_1_p_3_0_in_sao            =	decision_col_left[2] ;
				filter_1_p_3_1_in_sao            =	H_block[8]  ;
				filter_1_p_3_2_in_sao            =	H_block[9]  ;
				filter_1_p_3_3_in_sao            =	H_block[10] ;
				filter_1_p_3_4_in_sao            =	H_block[11] ;
				filter_1_p_3_5_in_sao            =	A_block[8]  ;
				filter_1_p_4_0_in_sao            =	decision_col_left[3] ;
				filter_1_p_4_1_in_sao            =	H_block[12] ;
				filter_1_p_4_2_in_sao            =	H_block[13] ;
				filter_1_p_4_3_in_sao            =	H_block[14] ;
				filter_1_p_4_4_in_sao            =	H_block[15] ;
				filter_1_p_4_5_in_sao            =	A_block[12] ;
				filter_1_p_5_0_in_sao            =	decision_col_left[4] ;
				filter_1_p_5_1_in_sao            =	I_block[0] ;
				filter_1_p_5_2_in_sao            =	I_block[1] ;
				filter_1_p_5_3_in_sao            =	I_block[2] ;
				filter_1_p_5_4_in_sao            =	I_block[3] ;
				filter_1_p_5_5_in_sao            =	B_block[0] ;
				if((Xc_in>0) && (Yc_in==(pic_height_in - 8)) && (H_block_sao_apply==1)) begin			// filter A block
					filter_2_start_sao_filter  =  1'b1 ;
				end
				filter_2_cross_boundary_left_flag_sao   		  =	  1'b1 ;
				filter_2_cross_boundary_right_flag_sao         =	  H_block_cross_boundary_right ;
				filter_2_cross_boundary_top_flag_sao	          =	  H_block_cross_boundary_top   ;
				filter_2_cross_boundary_bottom_flag_sao        =	  1'b1 ;
				filter_2_cross_boundary_top_left_flag_sao	  =	  H_block_cross_boundary_top   ;
				filter_2_cross_boundary_top_right_flag_sao	  =	  H_block_cross_boundary_top_right ;
				filter_2_cross_boundary_bottom_left_flag_sao   =	  1'b1 ;
				filter_2_cross_boundary_bottom_right_flag_sao  =	  H_block_cross_boundary_right ;
				filter_2_sao_type_in_sao	   		=	H_block_y_sao_type     ;
				filter_2_sao_eo_class_in_sao		=	H_block_y_eoclass      ;
				filter_2_sao_bandpos_in_sao 		=	H_block_y_bandpos      ;
				filter_2_sao_offset_1_in_sao		=	H_block_y_sao_offset_1 ;
				filter_2_sao_offset_2_in_sao		=	H_block_y_sao_offset_2 ;
				filter_2_sao_offset_3_in_sao		=	H_block_y_sao_offset_3 ;
				filter_2_sao_offset_4_in_sao		=	H_block_y_sao_offset_4 ;
				filter_2_p_0_0_in_sao			=	decision_row[4] ;
				filter_2_p_0_1_in_sao            =	decision_row[5] ;
				filter_2_p_0_2_in_sao            =	decision_row[6] ;
				filter_2_p_0_3_in_sao            =	decision_row[7] ;
				filter_2_p_0_4_in_sao            =	decision_row[8] ;
				filter_2_p_0_5_in_sao            =	decision_row[9] ;
				filter_2_p_1_0_in_sao            =	H_block[3] ;
				filter_2_p_1_1_in_sao            =	A_block[0] ;
				filter_2_p_1_2_in_sao            =	A_block[1] ;
				filter_2_p_1_3_in_sao            =	A_block[2] ;
				filter_2_p_1_4_in_sao            =	A_block[3] ;
				filter_2_p_1_5_in_sao            =	q_block_1[0] ;
				filter_2_p_2_0_in_sao            =	H_block[7] ;
				filter_2_p_2_1_in_sao            =	A_block[4] ;
				filter_2_p_2_2_in_sao            =	A_block[5] ;
				filter_2_p_2_3_in_sao            =	A_block[6] ;
				filter_2_p_2_4_in_sao            =	A_block[7] ;
				filter_2_p_2_5_in_sao            =	q_block_1[4] ;
				filter_2_p_3_0_in_sao            =	H_block[11] ;
				filter_2_p_3_1_in_sao            =	A_block[8]  ;
				filter_2_p_3_2_in_sao            =	A_block[9]  ;
				filter_2_p_3_3_in_sao            =	A_block[10] ;
				filter_2_p_3_4_in_sao            =	A_block[11] ;
				filter_2_p_3_5_in_sao            =	q_block_1[8]  ;
				filter_2_p_4_0_in_sao            =	H_block[15] ;
				filter_2_p_4_1_in_sao            =	A_block[12] ;
				filter_2_p_4_2_in_sao            =	A_block[13] ;
				filter_2_p_4_3_in_sao            =	A_block[14] ;
				filter_2_p_4_4_in_sao            =	A_block[15] ;
				filter_2_p_4_5_in_sao            =	q_block_1[12] ;
				filter_2_p_5_0_in_sao            =	I_block[3] ;
				filter_2_p_5_1_in_sao            =	B_block[0] ;
				filter_2_p_5_2_in_sao            =	B_block[1] ;
				filter_2_p_5_3_in_sao            =	B_block[2] ;
				filter_2_p_5_4_in_sao            =	B_block[3] ;
				filter_2_p_5_5_in_sao            =	q_block_3[0] ;
			end
			STATE_14 : begin
				if((Xc_in>0) && (Yc_in==(pic_height_in - 8)) && (H_block_sao_apply==1)) begin			// filter I block
					filter_1_start_sao_filter  =  1'b1 ;
				end
				filter_1_cross_boundary_left_flag_sao   		  =	  H_block_cross_boundary_left 	;
				filter_1_cross_boundary_right_flag_sao         =	  1'b1 	;
				filter_1_cross_boundary_top_flag_sao	          =	  1'b1 	;
				filter_1_cross_boundary_bottom_flag_sao        =	  H_block_cross_boundary_bottom ;
				filter_1_cross_boundary_top_left_flag_sao	  =	  H_block_cross_boundary_left 	;
				filter_1_cross_boundary_top_right_flag_sao	  =	  1'b1 ;
				filter_1_cross_boundary_bottom_left_flag_sao   =	  H_block_cross_boundary_bottom_left ;
				filter_1_cross_boundary_bottom_right_flag_sao  =	  H_block_cross_boundary_bottom ;
				filter_1_sao_type_in_sao	   		=	H_block_y_sao_type     ;
				filter_1_sao_eo_class_in_sao		=	H_block_y_eoclass      ;
				filter_1_sao_bandpos_in_sao 		=	H_block_y_bandpos      ;
				filter_1_sao_offset_1_in_sao		=	H_block_y_sao_offset_1 ;
				filter_1_sao_offset_2_in_sao		=	H_block_y_sao_offset_2 ;
				filter_1_sao_offset_3_in_sao		=	H_block_y_sao_offset_3 ;
				filter_1_sao_offset_4_in_sao		=	H_block_y_sao_offset_4 ;
				filter_1_p_0_0_in_sao			=	decision_col_left[3] ;
				filter_1_p_0_1_in_sao            =	H_block[12] ;
				filter_1_p_0_2_in_sao            =	H_block[13] ;
				filter_1_p_0_3_in_sao            =	H_block[14] ;
				filter_1_p_0_4_in_sao            =	H_block[15] ;
				filter_1_p_0_5_in_sao            =	A_block[12] ;
				filter_1_p_1_0_in_sao            =	decision_col_left[4] ;
				filter_1_p_1_1_in_sao            =	I_block[0] ;
				filter_1_p_1_2_in_sao            =	I_block[1] ;
				filter_1_p_1_3_in_sao            =	I_block[2] ;
				filter_1_p_1_4_in_sao            =	I_block[3] ;
				filter_1_p_1_5_in_sao            =	B_block[0] ;
				filter_1_p_2_0_in_sao            =	decision_col_left[5] ;
				filter_1_p_2_1_in_sao            =	I_block[4] ;
				filter_1_p_2_2_in_sao            =	I_block[5] ;
				filter_1_p_2_3_in_sao            =	I_block[6] ;
				filter_1_p_2_4_in_sao            =	I_block[7] ;
				filter_1_p_2_5_in_sao            =	B_block[4] ;
				filter_1_p_3_0_in_sao            =	decision_col_left[6] ;
				filter_1_p_3_1_in_sao            =	I_block[8]  ;
				filter_1_p_3_2_in_sao            =	I_block[9]  ;
				filter_1_p_3_3_in_sao            =	I_block[10] ;
				filter_1_p_3_4_in_sao            =	I_block[11] ;
				filter_1_p_3_5_in_sao            =	B_block[8]  ;
				filter_1_p_4_0_in_sao            =	decision_col_left[7] ;
				filter_1_p_4_1_in_sao            =	I_block[12] ;
				filter_1_p_4_2_in_sao            =	I_block[13] ;
				filter_1_p_4_3_in_sao            =	I_block[14] ;
				filter_1_p_4_4_in_sao            =	I_block[15] ;
				filter_1_p_4_5_in_sao            =	B_block[12] ;
				filter_1_p_5_0_in_sao            =	decision_col_left[8] ;
				filter_1_p_5_1_in_sao            =	8'd0 ;
				filter_1_p_5_2_in_sao            =	8'd0 ;
				filter_1_p_5_3_in_sao            =	8'd0 ;
				filter_1_p_5_4_in_sao            =	8'd0 ;
				filter_1_p_5_5_in_sao            =	8'd0 ;
				if((Xc_in>0) && (Yc_in==(pic_height_in - 8)) && (H_block_sao_apply==1)) begin			// filter B block
					filter_2_start_sao_filter  =  1'b1 ;
				end
				filter_2_cross_boundary_left_flag_sao   		  =	  1'b1 ;
				filter_2_cross_boundary_right_flag_sao         =	  H_block_cross_boundary_right 	;
				filter_2_cross_boundary_top_flag_sao	          =	  1'b1 ;
				filter_2_cross_boundary_bottom_flag_sao        =	  H_block_cross_boundary_bottom	;
				filter_2_cross_boundary_top_left_flag_sao	  =	  1'b1 ;
				filter_2_cross_boundary_top_right_flag_sao	  =	  H_block_cross_boundary_right 	;
				filter_2_cross_boundary_bottom_left_flag_sao   =	  H_block_cross_boundary_bottom	;
				filter_2_cross_boundary_bottom_right_flag_sao  =	  H_block_cross_boundary_bottom_right ;
				filter_2_sao_type_in_sao	   		=	H_block_y_sao_type     ;
				filter_2_sao_eo_class_in_sao		=	H_block_y_eoclass      ;
				filter_2_sao_bandpos_in_sao 		=	H_block_y_bandpos      ;
				filter_2_sao_offset_1_in_sao		=	H_block_y_sao_offset_1 ;
				filter_2_sao_offset_2_in_sao		=	H_block_y_sao_offset_2 ;
				filter_2_sao_offset_3_in_sao		=	H_block_y_sao_offset_3 ;
				filter_2_sao_offset_4_in_sao		=	H_block_y_sao_offset_4 ;
				filter_2_p_0_0_in_sao			=	H_block[15] ;
				filter_2_p_0_1_in_sao            =	A_block[12] ;
				filter_2_p_0_2_in_sao            =	A_block[13] ;
				filter_2_p_0_3_in_sao            =	A_block[14] ;
				filter_2_p_0_4_in_sao            =	A_block[15] ;
				filter_2_p_0_5_in_sao            =	q_block_1[12] ;
				filter_2_p_1_0_in_sao            =	I_block[3] ;
				filter_2_p_1_1_in_sao            =	B_block[0] ;
				filter_2_p_1_2_in_sao            =	B_block[1] ;
				filter_2_p_1_3_in_sao            =	B_block[2] ;
				filter_2_p_1_4_in_sao            =	B_block[3] ;
				filter_2_p_1_5_in_sao            =	q_block_3[0] ;
				filter_2_p_2_0_in_sao            =	I_block[7] ;
				filter_2_p_2_1_in_sao            =	B_block[4] ;
				filter_2_p_2_2_in_sao            =	B_block[5] ;
				filter_2_p_2_3_in_sao            =	B_block[6] ;
				filter_2_p_2_4_in_sao            =	B_block[7] ;
				filter_2_p_2_5_in_sao            =	q_block_3[4] ;
				filter_2_p_3_0_in_sao            =	I_block[11] ;
				filter_2_p_3_1_in_sao            =	B_block[8]  ;
				filter_2_p_3_2_in_sao            =	B_block[9]  ;
				filter_2_p_3_3_in_sao            =	B_block[10] ;
				filter_2_p_3_4_in_sao            =	B_block[11] ;
				filter_2_p_3_5_in_sao            =	q_block_3[8] ;
				filter_2_p_4_0_in_sao            =	I_block[15] ;
				filter_2_p_4_1_in_sao            =	B_block[12] ;
				filter_2_p_4_2_in_sao            =	B_block[13] ;
				filter_2_p_4_3_in_sao            =	B_block[14] ;
				filter_2_p_4_4_in_sao            =	B_block[15] ;
				filter_2_p_4_5_in_sao            =	q_block_3[12] ;
				filter_2_p_5_0_in_sao            =	8'd0 ;
				filter_2_p_5_1_in_sao            =	8'd0 ;
				filter_2_p_5_2_in_sao            =	8'd0 ;
				filter_2_p_5_3_in_sao            =	8'd0 ;
				filter_2_p_5_4_in_sao            =	8'd0 ;
				filter_2_p_5_5_in_sao            =	8'd0 ;
			end
			STATE_16 : begin
				if((Xc_in==(pic_width_in - 8)) && (Yc_in==(pic_height_in - 8)) && (q1_block_sao_apply==1)) begin		// filter q1 block
					filter_1_start_sao_filter  =  1'b1 ;
				end
				filter_1_cross_boundary_left_flag_sao   		  =	  q_block_cross_boundary_left 	;
				filter_1_cross_boundary_right_flag_sao         =	  1'b1 	;
				filter_1_cross_boundary_top_flag_sao	          =	  q_block_cross_boundary_top 	;
				filter_1_cross_boundary_bottom_flag_sao        =	  1'b1 ;
				filter_1_cross_boundary_top_left_flag_sao	  =	  q_block_cross_boundary_top_left ;
				filter_1_cross_boundary_top_right_flag_sao	  =	  q_block_cross_boundary_top ;
				filter_1_cross_boundary_bottom_left_flag_sao   =	  q_block_cross_boundary_left ;
				filter_1_cross_boundary_bottom_right_flag_sao  =	  1'b1 ;
				filter_1_sao_type_in_sao	   		=	y_sao_type_in	  ;
				filter_1_sao_eo_class_in_sao		=	y_eoclass_in      ;
				filter_1_sao_bandpos_in_sao 		=	y_bandpos_in      ;
				filter_1_sao_offset_1_in_sao		=	y_sao_offset_1_in ;
				filter_1_sao_offset_2_in_sao		=	y_sao_offset_2_in ;
				filter_1_sao_offset_3_in_sao		=	y_sao_offset_3_in ;
				filter_1_sao_offset_4_in_sao		=	y_sao_offset_4_in ;
				filter_1_p_0_0_in_sao			=	decision_row[0] ;
				filter_1_p_0_1_in_sao            =	decision_row[1] ;
				filter_1_p_0_2_in_sao            =	decision_row[2] ;
				filter_1_p_0_3_in_sao            =	decision_row[3] ;
				filter_1_p_0_4_in_sao            =	decision_row[4] ;
				filter_1_p_0_5_in_sao            =	decision_row[5] ;
				filter_1_p_1_0_in_sao            =	decision_col_left[0] ;
				filter_1_p_1_1_in_sao            =	q_block_1[0] ;
				filter_1_p_1_2_in_sao            =	q_block_1[1] ;
				filter_1_p_1_3_in_sao            =	q_block_1[2] ;
				filter_1_p_1_4_in_sao            =	q_block_1[3] ;
				filter_1_p_1_5_in_sao            =	q_block_2[0] ;
				filter_1_p_2_0_in_sao            =	decision_col_left[1] ;
				filter_1_p_2_1_in_sao            =	q_block_1[4] ;
				filter_1_p_2_2_in_sao            =	q_block_1[5] ;
				filter_1_p_2_3_in_sao            =	q_block_1[6] ;
				filter_1_p_2_4_in_sao            =	q_block_1[7] ;
				filter_1_p_2_5_in_sao            =	q_block_2[4] ;
				filter_1_p_3_0_in_sao            =	decision_col_left[2] ;
				filter_1_p_3_1_in_sao            =	q_block_1[8]  ;
				filter_1_p_3_2_in_sao            =	q_block_1[9]  ;
				filter_1_p_3_3_in_sao            =	q_block_1[10] ;
				filter_1_p_3_4_in_sao            =	q_block_1[11] ;
				filter_1_p_3_5_in_sao            =	q_block_2[8]  ;
				filter_1_p_4_0_in_sao            =	decision_col_left[3] ;
				filter_1_p_4_1_in_sao            =	q_block_1[12] ;
				filter_1_p_4_2_in_sao            =	q_block_1[13] ;
				filter_1_p_4_3_in_sao            =	q_block_1[14] ;
				filter_1_p_4_4_in_sao            =	q_block_1[15] ;
				filter_1_p_4_5_in_sao            =	q_block_2[12] ;
				filter_1_p_5_0_in_sao            =	decision_col_left[4] ;
				filter_1_p_5_1_in_sao            =	q_block_3[0] ;
				filter_1_p_5_2_in_sao            =	q_block_3[1] ;
				filter_1_p_5_3_in_sao            =	q_block_3[2] ;
				filter_1_p_5_4_in_sao            =	q_block_3[3] ;
				filter_1_p_5_5_in_sao            =	q_block_4[0] ;
				if((Xc_in==(pic_width_in - 8)) && (Yc_in==(pic_height_in - 8)) && (q1_block_sao_apply==1)) begin		// filter q2 block
					filter_2_start_sao_filter  =  1'b1 ;
				end
				filter_2_cross_boundary_left_flag_sao   		  =	  1'b1 ;
				filter_2_cross_boundary_right_flag_sao         =	  q_block_cross_boundary_right ;
				filter_2_cross_boundary_top_flag_sao	          =	  q_block_cross_boundary_top   ;
				filter_2_cross_boundary_bottom_flag_sao        =	  1'b1 ;
				filter_2_cross_boundary_top_left_flag_sao	  =	  q_block_cross_boundary_top   ;
				filter_2_cross_boundary_top_right_flag_sao	  =	  q_block_cross_boundary_top_right ;
				filter_2_cross_boundary_bottom_left_flag_sao   =	  1'b1 ;
				filter_2_cross_boundary_bottom_right_flag_sao  =	  q_block_cross_boundary_right ;
				filter_2_sao_type_in_sao	   		=	y_sao_type_in	  ;
				filter_2_sao_eo_class_in_sao		=	y_eoclass_in      ;
				filter_2_sao_bandpos_in_sao 		=	y_bandpos_in      ;
				filter_2_sao_offset_1_in_sao		=	y_sao_offset_1_in ;
				filter_2_sao_offset_2_in_sao		=	y_sao_offset_2_in ;
				filter_2_sao_offset_3_in_sao		=	y_sao_offset_3_in ;
				filter_2_sao_offset_4_in_sao		=	y_sao_offset_4_in ;
				filter_2_p_0_0_in_sao			=	decision_row[4] ;
				filter_2_p_0_1_in_sao            =	decision_row[5] ;
				filter_2_p_0_2_in_sao            =	decision_row[6] ;
				filter_2_p_0_3_in_sao            =	decision_row[7] ;
				filter_2_p_0_4_in_sao            =	decision_row[8] ;
				filter_2_p_0_5_in_sao            =	decision_row[9] ;
				filter_2_p_1_0_in_sao            =	q_block_1[3] ;
				filter_2_p_1_1_in_sao            =	q_block_2[0] ;
				filter_2_p_1_2_in_sao            =	q_block_2[1] ;
				filter_2_p_1_3_in_sao            =	q_block_2[2] ;
				filter_2_p_1_4_in_sao            =	q_block_2[3] ;
				filter_2_p_1_5_in_sao            =	8'd0 ;
				filter_2_p_2_0_in_sao            =	q_block_1[7] ;
				filter_2_p_2_1_in_sao            =	q_block_2[4] ;
				filter_2_p_2_2_in_sao            =	q_block_2[5] ;
				filter_2_p_2_3_in_sao            =	q_block_2[6] ;
				filter_2_p_2_4_in_sao            =	q_block_2[7] ;
				filter_2_p_2_5_in_sao            =	8'd0 ;
				filter_2_p_3_0_in_sao            =	q_block_1[11] ;
				filter_2_p_3_1_in_sao            =	q_block_2[8]  ;
				filter_2_p_3_2_in_sao            =	q_block_2[9]  ;
				filter_2_p_3_3_in_sao            =	q_block_2[10] ;
				filter_2_p_3_4_in_sao            =	q_block_2[11] ;
				filter_2_p_3_5_in_sao            =	8'd0 ;
				filter_2_p_4_0_in_sao            =	q_block_1[15] ;
				filter_2_p_4_1_in_sao            =	q_block_2[12] ;
				filter_2_p_4_2_in_sao            =	q_block_2[13] ;
				filter_2_p_4_3_in_sao            =	q_block_2[14] ;
				filter_2_p_4_4_in_sao            =	q_block_2[15] ;
				filter_2_p_4_5_in_sao            =	8'd0 ;
				filter_2_p_5_0_in_sao            =	q_block_3[3] ;
				filter_2_p_5_1_in_sao            =	q_block_4[0] ;
				filter_2_p_5_2_in_sao            =	q_block_4[1] ;
				filter_2_p_5_3_in_sao            =	q_block_4[2] ;
				filter_2_p_5_4_in_sao            =	q_block_4[3] ;
				filter_2_p_5_5_in_sao            =	8'd0 ;
			end
			STATE_18 : begin
				if((Xc_in==(pic_width_in - 8)) && (Yc_in==(pic_height_in - 8)) && (q1_block_sao_apply==1)) begin		// filter q3 block
					filter_1_start_sao_filter  =  1'b1 ;
				end
				filter_1_cross_boundary_left_flag_sao   		  =	  q_block_cross_boundary_left 	;
				filter_1_cross_boundary_right_flag_sao         =	  1'b1 	;
				filter_1_cross_boundary_top_flag_sao	          =	  1'b1 	;
				filter_1_cross_boundary_bottom_flag_sao        =	  q_block_cross_boundary_bottom ;
				filter_1_cross_boundary_top_left_flag_sao	  =	  q_block_cross_boundary_left 	;
				filter_1_cross_boundary_top_right_flag_sao	  =	  1'b1 ;
				filter_1_cross_boundary_bottom_left_flag_sao   =	  q_block_cross_boundary_bottom_left ;
				filter_1_cross_boundary_bottom_right_flag_sao  =	  q_block_cross_boundary_bottom ;
				filter_1_sao_type_in_sao	   		=	y_sao_type_in	  ;
				filter_1_sao_eo_class_in_sao		=	y_eoclass_in      ;
				filter_1_sao_bandpos_in_sao 		=	y_bandpos_in      ;
				filter_1_sao_offset_1_in_sao		=	y_sao_offset_1_in ;
				filter_1_sao_offset_2_in_sao		=	y_sao_offset_2_in ;
				filter_1_sao_offset_3_in_sao		=	y_sao_offset_3_in ;
				filter_1_sao_offset_4_in_sao		=	y_sao_offset_4_in ;
				filter_1_p_0_0_in_sao			=	decision_col_left[3] ;
				filter_1_p_0_1_in_sao            =	q_block_1[12] ;
				filter_1_p_0_2_in_sao            =	q_block_1[13] ;
				filter_1_p_0_3_in_sao            =	q_block_1[14] ;
				filter_1_p_0_4_in_sao            =	q_block_1[15] ;
				filter_1_p_0_5_in_sao            =	q_block_2[12] ;
				filter_1_p_1_0_in_sao            =	decision_col_left[4] ;
				filter_1_p_1_1_in_sao            =	q_block_3[0] ;
				filter_1_p_1_2_in_sao            =	q_block_3[1] ;
				filter_1_p_1_3_in_sao            =	q_block_3[2] ;
				filter_1_p_1_4_in_sao            =	q_block_3[3] ;
				filter_1_p_1_5_in_sao            =	q_block_4[0] ;
				filter_1_p_2_0_in_sao            =	decision_col_left[5] ;
				filter_1_p_2_1_in_sao            =	q_block_3[4] ;
				filter_1_p_2_2_in_sao            =	q_block_3[5] ;
				filter_1_p_2_3_in_sao            =	q_block_3[6] ;
				filter_1_p_2_4_in_sao            =	q_block_3[7] ;
				filter_1_p_2_5_in_sao            =	q_block_4[4] ;
				filter_1_p_3_0_in_sao            =	decision_col_left[6] ;
				filter_1_p_3_1_in_sao            =	q_block_3[8]  ;
				filter_1_p_3_2_in_sao            =	q_block_3[9]  ;
				filter_1_p_3_3_in_sao            =	q_block_3[10] ;
				filter_1_p_3_4_in_sao            =	q_block_3[11] ;
				filter_1_p_3_5_in_sao            =	q_block_4[8]  ;
				filter_1_p_4_0_in_sao            =	decision_col_left[7] ;
				filter_1_p_4_1_in_sao            =	q_block_3[12] ;
				filter_1_p_4_2_in_sao            =	q_block_3[13] ;
				filter_1_p_4_3_in_sao            =	q_block_3[14] ;
				filter_1_p_4_4_in_sao            =	q_block_3[15] ;
				filter_1_p_4_5_in_sao            =	q_block_4[12] ;
				filter_1_p_5_0_in_sao            =	decision_col_left[8] ;
				filter_1_p_5_1_in_sao            =	8'd0 ;
				filter_1_p_5_2_in_sao            =	8'd0 ;
				filter_1_p_5_3_in_sao            =	8'd0 ;
				filter_1_p_5_4_in_sao            =	8'd0 ;
				filter_1_p_5_5_in_sao            =	8'd0 ;
				if((Xc_in==(pic_width_in - 8)) && (Yc_in==(pic_height_in - 8)) && (q1_block_sao_apply==1)) begin		// filter q4 block
					filter_2_start_sao_filter  =  1'b1 ;
				end
				filter_2_cross_boundary_left_flag_sao   		  =	  1'b1 ;
				filter_2_cross_boundary_right_flag_sao         =	  q_block_cross_boundary_right 	;
				filter_2_cross_boundary_top_flag_sao	          =	  1'b1 ;
				filter_2_cross_boundary_bottom_flag_sao        =	  q_block_cross_boundary_bottom	;
				filter_2_cross_boundary_top_left_flag_sao	  =	  1'b1 ;
				filter_2_cross_boundary_top_right_flag_sao	  =	  q_block_cross_boundary_right 	;
				filter_2_cross_boundary_bottom_left_flag_sao   =	  q_block_cross_boundary_bottom	;
				filter_2_cross_boundary_bottom_right_flag_sao  =	  q_block_cross_boundary_bottom_right ;
				filter_2_sao_type_in_sao	   		=	y_sao_type_in	  ;
				filter_2_sao_eo_class_in_sao		=	y_eoclass_in      ;
				filter_2_sao_bandpos_in_sao 		=	y_bandpos_in      ;
				filter_2_sao_offset_1_in_sao		=	y_sao_offset_1_in ;
				filter_2_sao_offset_2_in_sao		=	y_sao_offset_2_in ;
				filter_2_sao_offset_3_in_sao		=	y_sao_offset_3_in ;
				filter_2_sao_offset_4_in_sao		=	y_sao_offset_4_in ;
				filter_2_p_0_0_in_sao			=	q_block_1[15] ;
				filter_2_p_0_1_in_sao            =	q_block_2[12] ;
				filter_2_p_0_2_in_sao            =	q_block_2[13] ;
				filter_2_p_0_3_in_sao            =	q_block_2[14] ;
				filter_2_p_0_4_in_sao            =	q_block_2[15] ;
				filter_2_p_0_5_in_sao            =	8'd0 ;
				filter_2_p_1_0_in_sao            =	q_block_3[3] ;
				filter_2_p_1_1_in_sao            =	q_block_4[0] ;
				filter_2_p_1_2_in_sao            =	q_block_4[1] ;
				filter_2_p_1_3_in_sao            =	q_block_4[2] ;
				filter_2_p_1_4_in_sao            =	q_block_4[3] ;
				filter_2_p_1_5_in_sao            =	8'd0 ;
				filter_2_p_2_0_in_sao            =	q_block_3[7] ;
				filter_2_p_2_1_in_sao            =	q_block_4[4] ;
				filter_2_p_2_2_in_sao            =	q_block_4[5] ;
				filter_2_p_2_3_in_sao            =	q_block_4[6] ;
				filter_2_p_2_4_in_sao            =	q_block_4[7] ;
				filter_2_p_2_5_in_sao            =	8'd0 ;
				filter_2_p_3_0_in_sao            =	q_block_3[11] ;
				filter_2_p_3_1_in_sao            =	q_block_4[8]  ;
				filter_2_p_3_2_in_sao            =	q_block_4[9]  ;
				filter_2_p_3_3_in_sao            =	q_block_4[10] ;
				filter_2_p_3_4_in_sao            =	q_block_4[11] ;
				filter_2_p_3_5_in_sao            =	8'd0 ;
				filter_2_p_4_0_in_sao            =	q_block_3[15] ;
				filter_2_p_4_1_in_sao            =	q_block_4[12] ;
				filter_2_p_4_2_in_sao            =	q_block_4[13] ;
				filter_2_p_4_3_in_sao            =	q_block_4[14] ;
				filter_2_p_4_4_in_sao            =	q_block_4[15] ;
				filter_2_p_4_5_in_sao            =	8'd0 ;
				filter_2_p_5_0_in_sao            =	8'd0 ;
				filter_2_p_5_1_in_sao            =	8'd0 ;
				filter_2_p_5_2_in_sao            =	8'd0 ;
				filter_2_p_5_3_in_sao            =	8'd0 ;
				filter_2_p_5_4_in_sao            =	8'd0 ;
				filter_2_p_5_5_in_sao            =	8'd0 ;
			end
		endcase
	end
	
	
	//--------------- Combinational block to check SAO applying conditions for 8x8 blocks -----------------
	always @(*) begin
		if((J_block_pcm_bypass_sao_disable_flag==0) && (J_block_sao_luma==1) && (J_block_y_sao_type>0)) begin
			J_block_sao_apply  =  1'b1 ;
		end
		else begin
			J_block_sao_apply  =  1'b0 ;
		end
		if((H_block_pcm_bypass_sao_disable_flag==0) && (H_block_sao_luma==1) && (H_block_y_sao_type>0)) begin
			H_block_sao_apply  =  1'b1 ;
		end
		else begin
			H_block_sao_apply  =  1'b0 ;
		end
		if((M_block_pcm_bypass_sao_disable_flag==0) && (M_block_sao_luma==1) && (M_block_y_sao_type>0)) begin
			M_block_sao_apply  =  1'b1 ;
		end
		else begin
			M_block_sao_apply  =  1'b0 ;
		end
		if((pcm_bypass_sao_disable_in==0) && (sao_luma_in==1) && (y_sao_type_in>0)) begin
			q1_block_sao_apply  =  1'b1 ;
		end
		else begin
			q1_block_sao_apply  =  1'b0 ;
		end
	end
	
	
	//--------------- Combinational block to calculate boundary crossing flags --------------------------
	always @(*) begin
		if(J_block_zs_addr < J_block_TR_zs_addr) begin
			J_block_top_right_sao_across_slices_enable_flag  =  decision_top_right_sao_across_slices_enable ;
		end
		else begin
			J_block_top_right_sao_across_slices_enable_flag  =  J_block_sao_across_slices_enable_flag ;
		end
		if(J_block_zs_addr < J_block_BL_zs_addr) begin
			J_block_bottom_left_sao_across_slices_enable_flag  =  decision_bottom_left_sao_across_slices_enable ;
		end
		else begin
			J_block_bottom_left_sao_across_slices_enable_flag  =  J_block_sao_across_slices_enable_flag ;
		end
		if(H_block_zs_addr < H_block_TR_zs_addr) begin
			H_block_top_right_sao_across_slices_enable_flag  =  decision_top_right_sao_across_slices_enable ;
		end
		else begin
			H_block_top_right_sao_across_slices_enable_flag  =  H_block_sao_across_slices_enable_flag ;
		end
		if(M_block_zs_addr < M_block_BL_zs_addr) begin
			M_block_bottom_left_sao_across_slices_enable_flag  =  decision_bottom_left_sao_across_slices_enable ;
		end
		else begin
			M_block_bottom_left_sao_across_slices_enable_flag  =  M_block_sao_across_slices_enable_flag ;
		end
	end
	
	assign	J_block_zs_addr		=	{ J_block_Y8[8],J_block_X8[8],J_block_Y8[7],J_block_X8[7],J_block_Y8[6],J_block_X8[6],J_block_Y8[5],J_block_X8[5],J_block_Y8[4],J_block_X8[4],J_block_Y8[3],J_block_X8[3],J_block_Y8[2],J_block_X8[2],J_block_Y8[1],J_block_X8[1] } ;
	assign	H_block_zs_addr		=	{ H_block_Y8[8],H_block_X8[8],H_block_Y8[7],H_block_X8[7],H_block_Y8[6],H_block_X8[6],H_block_Y8[5],H_block_X8[5],H_block_Y8[4],H_block_X8[4],H_block_Y8[3],H_block_X8[3],H_block_Y8[2],H_block_X8[2],H_block_Y8[1],H_block_X8[1] } ;
	assign	M_block_zs_addr		=	{ M_block_Y8[8],M_block_X8[8],M_block_Y8[7],M_block_X8[7],M_block_Y8[6],M_block_X8[6],M_block_Y8[5],M_block_X8[5],M_block_Y8[4],M_block_X8[4],M_block_Y8[3],M_block_X8[3],M_block_Y8[2],M_block_X8[2],M_block_Y8[1],M_block_X8[1] } ;
	
	assign	J_block_TR_zs_addr	=	{ J_block_TR_Y8[8],J_block_TR_X8[8],J_block_TR_Y8[7],J_block_TR_X8[7],J_block_TR_Y8[6],J_block_TR_X8[6],J_block_TR_Y8[5],J_block_TR_X8[5],J_block_TR_Y8[4],J_block_TR_X8[4],J_block_TR_Y8[3],J_block_TR_X8[3],J_block_TR_Y8[2],J_block_TR_X8[2],J_block_TR_Y8[1],J_block_TR_X8[1] } ;
	assign	J_block_BL_zs_addr	=	{ J_block_BL_Y8[8],J_block_BL_X8[8],J_block_BL_Y8[7],J_block_BL_X8[7],J_block_BL_Y8[6],J_block_BL_X8[6],J_block_BL_Y8[5],J_block_BL_X8[5],J_block_BL_Y8[4],J_block_BL_X8[4],J_block_BL_Y8[3],J_block_BL_X8[3],J_block_BL_Y8[2],J_block_BL_X8[2],J_block_BL_Y8[1],J_block_BL_X8[1] } ;
	assign	H_block_TR_zs_addr	=	M_block_zs_addr ;
	assign	M_block_BL_zs_addr	=	H_block_zs_addr ;
	
	
	assign	J_block_cross_boundary_left  		=  	~( (Xc_in[11:3] < 2) | (J_block_tile_left_flag & (~loop_filter_accross_tiles_enabled_in)) | (J_block_slice_left_flag & (~J_block_sao_across_slices_enable_flag)) ) ;
	assign	J_block_cross_boundary_top  		=  	~( (Yc_in[11:3] < 2) | (J_block_tile_top_flag  & (~loop_filter_accross_tiles_enabled_in)) | (J_block_slice_top_flag  & (~J_block_sao_across_slices_enable_flag)) ) ;
	assign	J_block_cross_boundary_right  		=  	~( (M_block_tile_left_flag & (~loop_filter_accross_tiles_enabled_in)) | (M_block_slice_left_flag & (~M_block_sao_across_slices_enable_flag)) ) ;
	assign	J_block_cross_boundary_bottom  		=  	~( (H_block_tile_top_flag  & (~loop_filter_accross_tiles_enabled_in)) | (H_block_slice_top_flag  & (~H_block_sao_across_slices_enable_flag)) ) ;
	assign	J_block_cross_boundary_top_left 	=  	~( (Xc_in[11:3] < 2) | (Yc_in[11:3] < 2) | ((J_block_tile_top_flag | J_block_tile_left_flag) & (~loop_filter_accross_tiles_enabled_in)) | ((J_block_slice_top_flag | decision_top_left_slice_boundary) & (~J_block_sao_across_slices_enable_flag)) ) ;
	assign	J_block_cross_boundary_bottom_right	=  	~( ((tile_left_boundary_in | tile_top_boundary_in)  & (~loop_filter_accross_tiles_enabled_in)) | ((slice_top_boundary_in | M_block_slice_left_flag)  & (~loop_filter_accross_slices_enabled_in)) ) ;
	assign	J_block_cross_boundary_top_right 	=  	~( (Yc_in[11:3] < 2) | ((J_block_tile_top_flag | M_block_tile_left_flag) & (~loop_filter_accross_tiles_enabled_in)) | ((J_block_slice_id != decision_top_right_slice_id) & (~J_block_top_right_sao_across_slices_enable_flag)) ) ;
	assign	J_block_cross_boundary_bottom_left 	=  	~( (Xc_in[11:3] < 2) | ((H_block_tile_top_flag | H_block_tile_left_flag) & (~loop_filter_accross_tiles_enabled_in)) | ((J_block_slice_id != decision_bottom_left_slice_id) & (~J_block_bottom_left_sao_across_slices_enable_flag)) ) ;
	
	assign	H_block_cross_boundary_left  		=  	~( (Xc_in[11:3] < 2) | (H_block_tile_left_flag & (~loop_filter_accross_tiles_enabled_in)) | (H_block_slice_left_flag & (~H_block_sao_across_slices_enable_flag)) ) ;
	assign	H_block_cross_boundary_top  		=  	~( (H_block_tile_top_flag  & (~loop_filter_accross_tiles_enabled_in)) | (H_block_slice_top_flag  & (~H_block_sao_across_slices_enable_flag)) ) ;
	assign	H_block_cross_boundary_right  		=  	~( (tile_left_boundary_in  & (~loop_filter_accross_tiles_enabled_in)) | (slice_left_boundary_in  & (~loop_filter_accross_slices_enabled_in)) ) ;
	assign	H_block_cross_boundary_bottom  		=  	1'b0 ;
	assign	H_block_cross_boundary_top_left 	=  	~( (Xc_in[11:3] < 2) | ((H_block_tile_top_flag | H_block_tile_left_flag) & (~loop_filter_accross_tiles_enabled_in)) | ((H_block_slice_top_flag | decision_top_left_slice_boundary) & (~H_block_sao_across_slices_enable_flag)) ) ;
	assign	H_block_cross_boundary_bottom_right	=  	1'b0 ;
	assign	H_block_cross_boundary_top_right 	=  	~( ((tile_left_boundary_in | tile_top_boundary_in) & (~loop_filter_accross_tiles_enabled_in)) | ((H_block_slice_id != decision_top_right_slice_id) & (~H_block_top_right_sao_across_slices_enable_flag)) ) ;
	assign	H_block_cross_boundary_bottom_left 	=  	1'b0 ;
	
	assign	M_block_cross_boundary_left  		=  	~( (M_block_tile_left_flag & (~loop_filter_accross_tiles_enabled_in)) | (M_block_slice_left_flag & (~M_block_sao_across_slices_enable_flag)) ) ;
	assign	M_block_cross_boundary_top  		=  	~( (Yc_in[11:3] < 2) | (M_block_tile_top_flag  & (~loop_filter_accross_tiles_enabled_in)) | (M_block_slice_top_flag  & (~M_block_sao_across_slices_enable_flag)) ) ;
	assign	M_block_cross_boundary_right  		=  	1'b0 ;
	assign	M_block_cross_boundary_bottom  		=  	~( (tile_top_boundary_in   & (~loop_filter_accross_tiles_enabled_in)) | (slice_top_boundary_in  & (~loop_filter_accross_slices_enabled_in)) ) ;
	assign	M_block_cross_boundary_top_left 	=  	~( (Yc_in[11:3] < 2) | ((M_block_tile_top_flag | M_block_tile_left_flag) & (~loop_filter_accross_tiles_enabled_in)) | ((M_block_slice_top_flag | decision_top_left_slice_boundary) & (~M_block_sao_across_slices_enable_flag)) ) ;
	assign	M_block_cross_boundary_bottom_right	=  	1'b0 ;
	assign	M_block_cross_boundary_top_right 	=  	1'b0 ;
	assign	M_block_cross_boundary_bottom_left 	=  	~( ((tile_left_boundary_in | tile_top_boundary_in) & (~loop_filter_accross_tiles_enabled_in)) | ((M_block_slice_id != decision_bottom_left_slice_id) & (~M_block_bottom_left_sao_across_slices_enable_flag)) ) ;
	
	assign	q_block_cross_boundary_left  		=  	~( (tile_left_boundary_in & (~loop_filter_accross_tiles_enabled_in)) | (slice_left_boundary_in & (~loop_filter_accross_slices_enabled_in)) ) ;
	assign	q_block_cross_boundary_top  		=  	~( (tile_top_boundary_in  & (~loop_filter_accross_tiles_enabled_in)) | (slice_top_boundary_in  & (~loop_filter_accross_slices_enabled_in)) ) ;
	assign	q_block_cross_boundary_right  		=  	1'b0 ;
	assign	q_block_cross_boundary_bottom  		=  	1'b0 ;
	assign	q_block_cross_boundary_top_left 	=  	~( ((tile_left_boundary_in | tile_top_boundary_in) & (~loop_filter_accross_tiles_enabled_in)) | ((slice_top_boundary_in | decision_top_left_slice_boundary) & (~loop_filter_accross_slices_enabled_in)) ) ;
	assign	q_block_cross_boundary_bottom_right	=  	1'b0 ;
	assign	q_block_cross_boundary_top_right 	=  	1'b0 ;
	assign	q_block_cross_boundary_bottom_left 	=  	1'b0 ;
	
	
	
	//--------------- Combinational block to read and write to column buffer BRAM -----------------------
	
	always @(*) begin
		col_we_a 		= 	1'b0 ;
		col_en_a 		= 	1'b0 ;
		col_addr_a 		= 	{10{1'bx}} ;
		col_data_in_a	=	{SAMPLE_COL_BUF_DATA_WIDTH{1'bx}} ;
		col_we_b 		= 	1'b0 ;
		col_en_b 		= 	1'b0 ;
		col_addr_b 		= 	{10{1'bx}} ;
		col_data_in_b	=	{SAMPLE_COL_BUF_DATA_WIDTH{1'bx}} ;
		
		case(state)
			STATE_1 : begin
				if((Xc_in>0) && (Yc_in>0)) begin		// G block read
					col_we_a 		= 	1'b0 ;
					col_en_a 		= 	1'b1 ;
					col_addr_a 		= 	(Yc_in[11:2] - 1'b1) ;
				end
				if(Xc_in>0) begin						// H block read
					col_we_b 		= 	1'b0 ;
					col_en_b 		= 	1'b1 ;
					col_addr_b 		= 	Yc_in[11:2] ;
				end
			end
			STATE_2 : begin
				if((Xc_in>0) && (Yc_in == (pic_height_in - 8))) begin		// I block read
					col_we_a 		= 	1'b0 ;
					col_en_a 		= 	1'b1 ;
					col_addr_a 		= 	(Yc_in[11:2] + 1'b1) ;
				end
			end
			STATE_3 : begin
				if(Yc_in>0) begin						// D block write
					col_we_a 		= 	1'b1 ;
					col_en_a 		= 	1'b1 ;
					col_addr_a 		= 	(Yc_in[11:2] - 1'b1) ;
					col_data_in_a	=	{	
											{(SAMPLE_COL_BUF_DATA_WIDTH - BIT_DEPTH*16){1'b0}} ,
											D_block[15] ,
											D_block[14] ,
											D_block[13] ,
											D_block[12] ,
											D_block[11] ,
											D_block[10] ,
											D_block[ 9] ,
											D_block[ 8] ,
											D_block[ 7] ,
											D_block[ 6] ,
											D_block[ 5] ,
											D_block[ 4] ,
											D_block[ 3] ,
											D_block[ 2] ,
											D_block[ 1] ,
											D_block[ 0]
										} ;
				end
				col_we_b 		= 	1'b1 ;				// q1 block write
				col_en_b 		= 	1'b1 ;
				col_addr_b 		= 	Yc_in[11:2] ;
				col_data_in_b	=	{	
										slice_id_in			   ,
										y_sao_offset_4_in  	   ,
										y_sao_offset_3_in  	   ,
										y_sao_offset_2_in  	   ,
										y_sao_offset_1_in  	   ,
										y_bandpos_in       	   ,
										y_eoclass_in       	   ,
										y_sao_type_in      	   ,
										sao_luma_in        	   ,
										slice_top_boundary_in  ,
										slice_left_boundary_in ,
										tile_top_boundary_in   ,
										tile_left_boundary_in  ,
										pcm_bypass_sao_disable_in ,
										loop_filter_accross_slices_enabled_in ,
										q_block_1[15] ,
										q_block_1[14] ,
										q_block_1[13] ,
										q_block_1[12] ,
										q_block_1[11] ,
										q_block_1[10] ,
										q_block_1[ 9] ,
										q_block_1[ 8] ,
										q_block_1[ 7] ,
										q_block_1[ 6] ,
										q_block_1[ 5] ,
										q_block_1[ 4] ,
										q_block_1[ 3] ,
										q_block_1[ 2] ,
										q_block_1[ 1] ,
										q_block_1[ 0]
									} ;
			end
			STATE_4 : begin
				if(Yc_in == (pic_height_in - 8)) begin
					col_we_a 		= 	1'b1 ;
					col_en_a 		= 	1'b1 ;
					col_addr_a 		= 	(Yc_in[11:2] + 1'b1) ;
					col_data_in_a	=	{	
											{(SAMPLE_COL_BUF_DATA_WIDTH - BIT_DEPTH*16){1'b0}} ,
											q_block_3[15] ,
											q_block_3[14] ,
											q_block_3[13] ,
											q_block_3[12] ,
											q_block_3[11] ,
											q_block_3[10] ,
											q_block_3[ 9] ,
											q_block_3[ 8] ,
											q_block_3[ 7] ,
											q_block_3[ 6] ,
											q_block_3[ 5] ,
											q_block_3[ 4] ,
											q_block_3[ 3] ,
											q_block_3[ 2] ,
											q_block_3[ 1] ,
											q_block_3[ 0]
										} ;
				end
			end
		endcase
	end
	
	
	//--------------- Combinational block to read and write to row buffer BRAM -----------------------
	
	always @(*) begin
		row_we_a 		= 	1'b0 ;
		row_en_a 		= 	1'b0 ;
		row_addr_a 		= 	{9{1'bx}} ;
		row_data_in_a	=	{SAMPLE_ROW_BUF_DATA_WIDTH{1'bx}} ;
		
		case(state)
			STATE_1 : begin
				if((Xc_in>0) && (Yc_in>0)) begin		// J,K blocks read
					row_we_a 		= 	1'b0 ;
					row_en_a 		= 	1'b1 ;
					row_addr_a 		= 	(Xc_in[11:3] - 1'b1) ;
				end
			end
			STATE_2 : begin
				if((Xc_in == (pic_width_in - 8)) && (Yc_in>0)) begin		// M,N blocks read
					row_we_a 		= 	1'b0 ;
					row_en_a 		= 	1'b1 ;
					row_addr_a 		= 	Xc_in[11:3] ;
				end
			end
			STATE_3 : begin
				if(Xc_in>0) begin									// H,A blocks write
					row_we_a 		= 	1'b1 ;
					row_en_a 		= 	1'b1 ;
					row_addr_a 		= 	(Xc_in[11:3] - 1'b1) ;
					row_data_in_a	=	{	
											H_block_slice_id		,
											H_block_y_sao_offset_4  ,
											H_block_y_sao_offset_3  ,
											H_block_y_sao_offset_2  ,
											H_block_y_sao_offset_1  ,
											H_block_y_bandpos       ,
											H_block_y_eoclass       ,
											H_block_y_sao_type      ,
											H_block_sao_luma        ,
											H_block_slice_top_flag  ,
											H_block_slice_left_flag ,
											H_block_tile_top_flag   ,
											H_block_tile_left_flag  ,
											H_block_pcm_bypass_sao_disable_flag ,
											H_block_sao_across_slices_enable_flag ,
											A_block[15] ,
											A_block[14] ,
											A_block[13] ,
											A_block[12] ,
											A_block[11] ,
											A_block[10] ,
											A_block[ 9] ,
											A_block[ 8] ,
											A_block[ 7] ,
											A_block[ 6] ,
											A_block[ 5] ,
											A_block[ 4] ,
											A_block[ 3] ,
											A_block[ 2] ,
											A_block[ 1] ,
											A_block[ 0] ,
											H_block[15] ,
											H_block[14] ,
											H_block[13] ,
											H_block[12] ,
											H_block[11] ,
											H_block[10] ,
											H_block[ 9] ,
											H_block[ 8] ,
											H_block[ 7] ,
											H_block[ 6] ,
											H_block[ 5] ,
											H_block[ 4] ,
											H_block[ 3] ,
											H_block[ 2] ,
											H_block[ 1] ,
											H_block[ 0]
										} ;
				end
			end
			STATE_4 : begin
				if(Xc_in == (pic_width_in - 8)) begin				// q1,q2 blocks write
					row_we_a 		= 	1'b1 ;
					row_en_a 		= 	1'b1 ;
					row_addr_a 		= 	Xc_in[11:3] ;
					row_data_in_a	=	{	
											slice_id_in			   ,
											y_sao_offset_4_in  	   ,
											y_sao_offset_3_in  	   ,
											y_sao_offset_2_in  	   ,
											y_sao_offset_1_in  	   ,
											y_bandpos_in       	   ,
											y_eoclass_in       	   ,
											y_sao_type_in      	   ,
											sao_luma_in        	   ,
											slice_top_boundary_in  ,
											slice_left_boundary_in ,
											tile_top_boundary_in   ,
											tile_left_boundary_in  ,
											pcm_bypass_sao_disable_in ,
											loop_filter_accross_slices_enabled_in ,
											q_block_2[15] ,
											q_block_2[14] ,
											q_block_2[13] ,
											q_block_2[12] ,
											q_block_2[11] ,
											q_block_2[10] ,
											q_block_2[ 9] ,
											q_block_2[ 8] ,
											q_block_2[ 7] ,
											q_block_2[ 6] ,
											q_block_2[ 5] ,
											q_block_2[ 4] ,
											q_block_2[ 3] ,
											q_block_2[ 2] ,
											q_block_2[ 1] ,
											q_block_2[ 0] ,
											q_block_1[15] ,
											q_block_1[14] ,
											q_block_1[13] ,
											q_block_1[12] ,
											q_block_1[11] ,
											q_block_1[10] ,
											q_block_1[ 9] ,
											q_block_1[ 8] ,
											q_block_1[ 7] ,
											q_block_1[ 6] ,
											q_block_1[ 5] ,
											q_block_1[ 4] ,
											q_block_1[ 3] ,
											q_block_1[ 2] ,
											q_block_1[ 1] ,
											q_block_1[ 0]
										} ;
				end
			end
		endcase
	end
	
	
	//----------- Combinational block to read and write to decision buffer BRAM ----------------------
	
	always @(*) begin
		d_row_buf_we_a       		=	1'b0 ;
		d_row_buf_en_a       		=	1'b0 ;
		d_row_buf_addr_a     		=	{9{1'bx}} ;
		d_row_buf_data_in_a  		=	{DECISION_ROW_BUF_DATA_WIDTH{1'bx}} ;
		d_left_col_buf_we_a      	=	1'b0 ;
		d_left_col_buf_en_a      	=	1'b0 ;
		d_left_col_buf_addr_a    	=	{9{1'bx}} ;
		d_left_col_buf_data_in_a 	=	{LEFT_DECISION_COL_BUF_DATA_WIDTH{1'bx}} ;
		d_right_col_buf_we_a      	=	1'b0 ;
		d_right_col_buf_en_a      	=	1'b0 ;
		d_right_col_buf_addr_a    	=	{9{1'bx}} ;
		d_right_col_buf_data_in_a 	=	{RIGHT_DECISION_COL_BUF_DATA_WIDTH{1'bx}} ;
		
		case(state)
			STATE_1 : begin
				if((Xc_in>0) && (Yc_in>8)) begin			// read decision row buffer
					d_row_buf_we_a   	=	1'b0 ;
					d_row_buf_en_a   	=	1'b1 ;
				end
				d_row_buf_addr_a 	=	(Xc_in[11:3] - 1'b1) ;
				if((Xc_in>8) && (Yc_in>0)) begin			// read left decision column buffer
					d_left_col_buf_we_a    	=	1'b0 ;
					d_left_col_buf_en_a    	=	1'b1 ;
				end
				d_left_col_buf_addr_a  	=	(Yc_in[11:3] - 1'b1) ;
				if((Xc_in>0) && (Yc_in>0)) begin			// read right decision column buffer
					d_right_col_buf_we_a   	=	1'b0 ;
					d_right_col_buf_en_a   	=	1'b1 ;
				end
				d_right_col_buf_addr_a 	=	Xc_in[11:3] ;		// Actually horizontally addressed
			end
			STATE_4 : begin
				if((Xc_in>0) && (Yc_in>0)) begin			// write to decision row buffer
					d_row_buf_we_a   	=	1'b1 ;
					d_row_buf_en_a   	=	1'b1 ;
				end
				d_row_buf_addr_a 	=	(Xc_in[11:3] - 1'b1) ;
				d_row_buf_data_in_a  =	{
											M_block_sao_across_slices_enable_flag ,
											M_block_slice_id ,
											J_block_slice_left_flag ,
											D_block[12] ,
											C_block[15] ,
											C_block[14] ,
											C_block[13] ,
											C_block[12] ,
											G_block[15] ,
											G_block[14] ,
											G_block[13] ,
											G_block[12] ,
											decision_col_left[7]
										} ;
				if((Xc_in>0) && (Yc_in>0)) begin			// write to left decision column buffer
					d_left_col_buf_we_a    	=	1'b1 ;
					d_left_col_buf_en_a    	=	1'b1 ;
				end
				d_left_col_buf_addr_a  	=	(Yc_in[11:3] - 1'b1) ;
				d_left_col_buf_data_in_a  =	{
												H_block_sao_across_slices_enable_flag ,
												H_block_slice_id ,
												A_block[3]  ,
												C_block[15] ,
												C_block[11] ,
												C_block[7]  ,
												C_block[3]  ,
												K_block[15] ,
												K_block[11] ,
												K_block[7]  ,
												K_block[3] 
											} ;
				d_right_col_buf_we_a   	=	1'b1 ;			// write to right decision column buffer
				d_right_col_buf_en_a   	=	1'b1 ;
				d_right_col_buf_addr_a 	=	Xc_in[11:3] ;
				d_right_col_buf_data_in_a  	=  	{
													slice_id_in ,
													slice_left_boundary_in ,
													tile_left_boundary_in ,
													loop_filter_accross_slices_enabled_in ,
													q_block_1[12],
													q_block_1[8] ,
													q_block_1[4] ,
													q_block_1[0] 
												} ;
			end
			STATE_6 : begin
				if((Xc_in==(pic_width_in - 8)) && (Yc_in>8)) begin			// read decision row buffer
					d_row_buf_we_a   	=	1'b0 ;
					d_row_buf_en_a   	=	1'b1 ;
				end
				d_row_buf_addr_a 	=	Xc_in[11:3] ;
				if((Xc_in==(pic_width_in - 8)) && (Yc_in>0)) begin			// read left decision column buffer
					d_left_col_buf_we_a    	=	1'b0 ;
					d_left_col_buf_en_a    	=	1'b1 ;
				end
				d_left_col_buf_addr_a  	=	(Yc_in[11:3] - 1'b1) ;
			end
			STATE_7 : begin
				if((Xc_in==(pic_width_in - 8)) && (Yc_in>0)) begin			// write to decision row buffer
					d_row_buf_we_a   	=	1'b1 ;
					d_row_buf_en_a   	=	1'b1 ;
				end
				d_row_buf_addr_a 	=	Xc_in[11:3] ;
				d_row_buf_data_in_a  =	{
											1'b0 ,
											8'd0 ,
											M_block_slice_left_flag ,
											8'd0 ,
											E_block[15] ,
											E_block[14] ,
											E_block[13] ,
											E_block[12] ,
											D_block[15] ,
											D_block[14] ,
											D_block[13] ,
											D_block[12] ,
											C_block[15]				// This is the pre SAO sample
										} ;
			end
			STATE_10 : begin
				if((Xc_in>0) && (Yc_in==(pic_height_in - 8))) begin			// read decision row buffer
					d_row_buf_we_a   	=	1'b0 ;
					d_row_buf_en_a   	=	1'b1 ;
				end
				d_row_buf_addr_a 	=	(Xc_in[11:3] - 1'b1) ;
				if((Xc_in>8) && (Yc_in==(pic_height_in - 8))) begin			// read left decision column buffer
					d_left_col_buf_we_a    	=	1'b0 ;
					d_left_col_buf_en_a    	=	1'b1 ;
				end
				d_left_col_buf_addr_a  	=	Yc_in[11:3] ;
			end
			STATE_11 : begin
				if((Xc_in>0) && (Yc_in==(pic_height_in - 8))) begin			// write to left decision column buffer
					d_left_col_buf_we_a    	=	1'b1 ;
					d_left_col_buf_en_a    	=	1'b1 ;
				end
				d_left_col_buf_addr_a  	=	Yc_in[11:3] ;
				d_left_col_buf_data_in_a  =	{
												1'b0 ,
												8'd0 ,
												8'd0 ,
												B_block[15] ,
												B_block[11] ,
												B_block[7]  ,
												B_block[3]  ,
												A_block[15] ,
												A_block[11] ,
												A_block[7]  ,
												A_block[3] 
											} ;
			end
			STATE_14 : begin
				if((Xc_in==(pic_width_in - 8)) && (Yc_in==(pic_height_in - 8))) begin		// read decision row buffer
					d_row_buf_we_a   	=	1'b0 ;
					d_row_buf_en_a   	=	1'b1 ;
				end
				d_row_buf_addr_a 	=	Xc_in[11:3] ;
				if((Xc_in==(pic_width_in - 8)) && (Yc_in==(pic_height_in - 8))) begin		// read left decision column buffer
					d_left_col_buf_we_a    	=	1'b0 ;
					d_left_col_buf_en_a    	=	1'b1 ;
				end
				d_left_col_buf_addr_a  	=	Yc_in[11:3] ;
			end
		endcase
	end
	
	
	

endmodule
