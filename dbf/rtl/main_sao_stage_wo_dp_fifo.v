`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:04:20 05/20/2014 
// Design Name: 
// Module Name:    main_sao_stage 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module main_sao_stage_wo_dp_fifo(
		clk,
        reset,
        data_valid_in,
        ready_out,
        valid_out,
		y_q_block_1_in ,
		y_q_block_2_in ,
		y_q_block_3_in ,
		y_q_block_4_in ,
		y_A_block_in   ,
		y_B_block_in   ,
		y_C_block_in   ,
		y_D_block_in   ,
		y_E_block_in   ,
		cb_q_block_1_in,
		cb_q_block_2_in,
		cb_q_block_3_in,
		cb_q_block_4_in,
		cb_A_block_in  ,
		cb_B_block_in  ,
		cb_C_block_in  ,
		cb_D_block_in  ,
		cb_E_block_in  ,
		cr_q_block_1_in,
		cr_q_block_2_in,
		cr_q_block_3_in,
		cr_q_block_4_in,
		cr_A_block_in  ,
		cr_B_block_in  ,
		cr_C_block_in  ,
		cr_D_block_in  ,
		cr_E_block_in  ,
		Xc_in,
		Yc_in,
		pic_width_in,
        pic_height_in,
		log2_ctb_size_in,
		tile_id_in,
		slice_id_in,
		pcm_bypass_sao_disable_in,
		loop_filter_accross_tiles_enabled_in,
		loop_filter_accross_slices_enabled_in,
		tile_left_boundary_in ,
		tile_top_boundary_in  ,
		slice_left_boundary_in,
		slice_top_boundary_in ,
		sao_luma_in,
		sao_chroma_in,
		pcm_in,
		bypass_in,
		y_sao_type_in,
        y_bandpos_or_eoclass_in,
        y_sao_offset_1_in,
        y_sao_offset_2_in,
        y_sao_offset_3_in,
        y_sao_offset_4_in,
        cb_sao_type_in,
        cb_bandpos_or_eoclass_in,
        cb_sao_offset_1_in,
        cb_sao_offset_2_in,
        cb_sao_offset_3_in,
        cb_sao_offset_4_in,
        cr_sao_type_in,
        cr_bandpos_or_eoclass_in,
        cr_sao_offset_1_in,
        cr_sao_offset_2_in,
        cr_sao_offset_3_in,
        cr_sao_offset_4_in,
		poc_in,
		dbp_idx_in	,
		
		
		pic_width_out,
		pic_height_out,
		tile_id_out,
		slice_id_out,

		display_fifo_is_full,
		display_fifo_data_wr_en,		
		display_fifo_data,

		dpb_fifo_is_empty_out,
		dpb_fifo_rd_en_in,		
		dpb_fifo_data_out,
		
		dpb_axi_addr_out,
		poc_out,
		log2_ctu_size_out
		
		,controller_in_valid 
		,controller_in 		
		,controller_out_valid
		,controller_out 	
		,sao_wr_en_out 		
		,sao_rd_en_out 		
		,sao_wr_data 		
		,sao_rd_data 		
		,display_buf_ful
		
		
    );
	
	`include "../sim/pred_def.v"
	`include "../sim/inter_axi_def.v"
	
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input                               clk;
    input                               reset;
    
    // handshake signals to and from the Main Deblocking stage
(* MARK_DEBUG *)    input                	    data_valid_in;	    // connected to valid out of the above stage
(* MARK_DEBUG *)    output  reg                 ready_out;          // connected to ready in of the above stage
    
    // handshake signals to and from the stage below
(* MARK_DEBUG *)    output	reg					valid_out;          // connected to data valid in of the stage below
    
    // Inputs from Main Deblocking stage
	
	input			[127:0]         		y_q_block_1_in;
    input			[127:0]         		y_q_block_2_in;
    input			[127:0]         		y_q_block_3_in;
    input			[127:0]         		y_q_block_4_in;
    input			[127:0]         		y_A_block_in  ;
    input			[127:0]         		y_B_block_in  ;
    input			[127:0]         		y_C_block_in  ;
    input			[127:0]         		y_D_block_in  ;
    input			[127:0]         		y_E_block_in  ;
	
    input			[31:0]          		cb_q_block_1_in;
    input			[31:0]          		cb_q_block_2_in;
    input			[31:0]          		cb_q_block_3_in;
    input			[31:0]          		cb_q_block_4_in;
    input			[31:0]          		cb_A_block_in  ;
    input			[31:0]          		cb_B_block_in  ;
    input			[31:0]          		cb_C_block_in  ;
    input			[31:0]          		cb_D_block_in  ;
    input			[31:0]          		cb_E_block_in  ;
	
    input			[31:0]          		cr_q_block_1_in;
    input			[31:0]          		cr_q_block_2_in;
    input			[31:0]          		cr_q_block_3_in;
    input			[31:0]          		cr_q_block_4_in;
    input			[31:0]          		cr_A_block_in  ;
    input			[31:0]          		cr_B_block_in  ;
    input			[31:0]          		cr_C_block_in  ;
    input			[31:0]          		cr_D_block_in  ;
    input			[31:0]          		cr_E_block_in  ;
	
    input			[X_ADDR_WDTH -1:0]                  Xc_in;
    input			[X_ADDR_WDTH -1:0]                  Yc_in;
    input			[X_ADDR_WDTH -1:0]                  pic_width_in;
    input			[X_ADDR_WDTH -1:0]                  pic_height_in;
    input			[2:0]                   log2_ctb_size_in;
    input			[7:0]				    tile_id_in;
    input			[7:0]				    slice_id_in;
	
	
	input		[DPB_ADDR_WIDTH -1:0]			dbp_idx_in;
	input 		[POC_WIDTH-1:0]							poc_in;
	
	output 	reg 	[AXI_ADDR_WDTH -1:0]			dpb_axi_addr_out;
	output 	reg 	[POC_WIDTH -1:0]			poc_out;
	
	input									pcm_bypass_sao_disable_in;
	input									loop_filter_accross_tiles_enabled_in;
	input									loop_filter_accross_slices_enabled_in;
	input									tile_left_boundary_in  ;
	input									tile_top_boundary_in   ;
	input									slice_left_boundary_in ;
	input									slice_top_boundary_in  ;
	
    input			                   	    				sao_luma_in;
	input			                   	    				sao_chroma_in;
	input			                   	    				pcm_in;
	input			                   	    				bypass_in;
	input			[SAOTYPE_WIDTH -1:0]                  	y_sao_type_in;
    input			[BANDPOS_EO_WIDTH -1:0]               	y_bandpos_or_eoclass_in;
    input			[SAO_OFFSET_WIDTH -1:0]               	y_sao_offset_1_in;			//---- ATTN - decide width ------
    input			[SAO_OFFSET_WIDTH -1:0]               	y_sao_offset_2_in;         	//---- ATTN - decide width ------
    input			[SAO_OFFSET_WIDTH -1:0]               	y_sao_offset_3_in;         	//---- ATTN - decide width ------
    input			[SAO_OFFSET_WIDTH -1:0]               	y_sao_offset_4_in;         	//---- ATTN - decide width ------
    input			[SAOTYPE_WIDTH -1:0]                  	cb_sao_type_in;	
    input			[BANDPOS_EO_WIDTH -1:0]               	cb_bandpos_or_eoclass_in;
    input			[SAO_OFFSET_WIDTH -1:0]               	cb_sao_offset_1_in;
    input			[SAO_OFFSET_WIDTH -1:0]               	cb_sao_offset_2_in;
    input			[SAO_OFFSET_WIDTH -1:0]               	cb_sao_offset_3_in;
    input			[SAO_OFFSET_WIDTH -1:0]               	cb_sao_offset_4_in;
    input			[SAOTYPE_WIDTH -1:0]                  	cr_sao_type_in;
    input			[BANDPOS_EO_WIDTH -1:0]               	cr_bandpos_or_eoclass_in;
    input			[SAO_OFFSET_WIDTH -1:0]               	cr_sao_offset_1_in;
    input			[SAO_OFFSET_WIDTH -1:0]               	cr_sao_offset_2_in;
    input			[SAO_OFFSET_WIDTH -1:0]               	cr_sao_offset_3_in;
    input			[SAO_OFFSET_WIDTH -1:0]               	cr_sao_offset_4_in;
	
	// Outputs to stage below
	
    
    output  reg     [11:0]          							  pic_width_out;
    output  reg     [11:0]          							  pic_height_out;
    output  reg     [7:0]										  tile_id_out;
    output  reg     [7:0]										  slice_id_out;
    output reg     [MAX_LOG2CTBSIZE_WIDTH - 1:0]              	  log2_ctu_size_out;
	
// Test Interface
	output controller_in_valid;
	output [128-1:0] controller_in;
	output controller_out_valid;
	output [128-1:0]	controller_out;
	output sao_wr_en_out;
	output [256-1:0] sao_wr_data;
	output sao_rd_en_out;
	output [256-1:0] sao_rd_data;
	output display_buf_ful;

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    
    localparam                          STATE_INIT          		= 0;    // the state that is entered upon reset
    localparam                          STATE_1             		= 1;
    // localparam                          STATE_BOTTOM_ONLY          	= 2;
    localparam                          STATE_RIGHT_ONLY          	= 2;
    localparam                          STATE_BOTTOM_RIGHT        	= 3;
    // localparam                          STATE_RIGHT             	= 4;
    localparam                          STATE_BOTTOM             	= 4;
    localparam                          STATE_WAIT_TILL_DONE    	= 5;
    
    
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    
	reg                                     				start_sao_modules;
    wire                                    				done_sao_luma;
    wire                                    				done_sao_chroma;
	
	reg			                   	    					sao_luma;
	reg			                   	    					sao_chroma;
	reg				[SAOTYPE_WIDTH -1:0]   					y_sao_type            ;
	reg				[BANDPOS_EO_WIDTH -1:0]					y_bandpos_or_eoclass  ;
	reg				[SAO_OFFSET_WIDTH -1:0]					y_sao_offset_1        ;
	reg				[SAO_OFFSET_WIDTH -1:0]					y_sao_offset_2        ;
	reg				[SAO_OFFSET_WIDTH -1:0]					y_sao_offset_3        ;
	reg				[SAO_OFFSET_WIDTH -1:0]					y_sao_offset_4        ;
	reg				[SAOTYPE_WIDTH -1:0]   					cb_sao_type	          ;
	reg				[BANDPOS_EO_WIDTH -1:0]					cb_bandpos_or_eoclass ;
	reg				[SAO_OFFSET_WIDTH -1:0]					cb_sao_offset_1	      ;
	reg				[SAO_OFFSET_WIDTH -1:0]					cb_sao_offset_2	      ;
	reg				[SAO_OFFSET_WIDTH -1:0]					cb_sao_offset_3	      ;
	reg				[SAO_OFFSET_WIDTH -1:0]					cb_sao_offset_4	      ;
	reg				[SAOTYPE_WIDTH -1:0]   					cr_sao_type           ;
	reg				[BANDPOS_EO_WIDTH -1:0]					cr_bandpos_or_eoclass ;
	reg				[SAO_OFFSET_WIDTH -1:0]					cr_sao_offset_1       ;
	reg				[SAO_OFFSET_WIDTH -1:0]					cr_sao_offset_2       ;
	reg				[SAO_OFFSET_WIDTH -1:0]					cr_sao_offset_3       ;
	reg				[SAO_OFFSET_WIDTH -1:0]					cr_sao_offset_4       ;
	
	reg											pcm_bypass_sao_disable;
	reg											loop_filter_accross_tiles_enabled;
	reg											loop_filter_accross_slices_enabled;
	reg											tile_left_boundary  ;
	reg											tile_top_boundary   ;
	reg											slice_left_boundary ;
	reg											slice_top_boundary  ;
	
	integer 								state;
	
	wire [PIXEL_WIDTH*DBF_YY_BLOCK_SIZE*DBF_YY_BLOCK_SIZE-1:0]	y_q_block_1_out;
	wire [PIXEL_WIDTH*DBF_YY_BLOCK_SIZE*DBF_YY_BLOCK_SIZE-1:0]	y_q_block_2_out;
	wire [PIXEL_WIDTH*DBF_YY_BLOCK_SIZE*DBF_YY_BLOCK_SIZE-1:0]	y_q_block_3_out;
	wire [PIXEL_WIDTH*DBF_YY_BLOCK_SIZE*DBF_YY_BLOCK_SIZE-1:0]	y_q_block_4_out;
	wire [PIXEL_WIDTH*DBF_YY_BLOCK_SIZE*DBF_YY_BLOCK_SIZE-1:0]	y_A_block_out  ;
	wire [PIXEL_WIDTH*DBF_YY_BLOCK_SIZE*DBF_YY_BLOCK_SIZE-1:0]	y_B_block_out  ;
	wire [PIXEL_WIDTH*DBF_YY_BLOCK_SIZE*DBF_YY_BLOCK_SIZE-1:0]	y_C_block_out  ;
	wire [PIXEL_WIDTH*DBF_YY_BLOCK_SIZE*DBF_YY_BLOCK_SIZE-1:0]	y_D_block_out  ;
	wire [PIXEL_WIDTH*DBF_YY_BLOCK_SIZE*DBF_YY_BLOCK_SIZE-1:0]	y_E_block_out  ;
	wire [PIXEL_WIDTH*DBF_YY_BLOCK_SIZE*DBF_YY_BLOCK_SIZE-1:0]	y_J_block_out	 ;
	wire [PIXEL_WIDTH*DBF_YY_BLOCK_SIZE*DBF_YY_BLOCK_SIZE-1:0]	y_K_block_out	 ;
	wire [PIXEL_WIDTH*DBF_YY_BLOCK_SIZE*DBF_YY_BLOCK_SIZE-1:0]	y_M_block_out	 ;
	wire [PIXEL_WIDTH*DBF_YY_BLOCK_SIZE*DBF_YY_BLOCK_SIZE-1:0]	y_N_block_out	 ;
	wire [PIXEL_WIDTH*DBF_YY_BLOCK_SIZE*DBF_YY_BLOCK_SIZE-1:0]	y_G_block_out	 ;
	wire [PIXEL_WIDTH*DBF_YY_BLOCK_SIZE*DBF_YY_BLOCK_SIZE-1:0]	y_H_block_out	 ;
	wire [PIXEL_WIDTH*DBF_YY_BLOCK_SIZE*DBF_YY_BLOCK_SIZE-1:0]	y_I_block_out  ;
	
	
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cb_q_block_1_out;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cb_q_block_2_out;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cb_q_block_3_out;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cb_q_block_4_out;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cb_A_block_out  ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cb_B_block_out  ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cb_C_block_out  ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cb_D_block_out  ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cb_E_block_out  ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cb_J_block_out	 ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cb_K_block_out	 ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cb_M_block_out	 ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cb_N_block_out	 ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cb_G_block_out	 ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cb_H_block_out	 ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cb_I_block_out  ;
	
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cr_q_block_1_out;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cr_q_block_2_out;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cr_q_block_3_out;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cr_q_block_4_out;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cr_A_block_out  ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cr_B_block_out  ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cr_C_block_out  ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cr_D_block_out  ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cr_E_block_out  ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cr_J_block_out	 ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cr_K_block_out	 ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cr_M_block_out	 ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cr_N_block_out	 ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cr_G_block_out	 ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cr_H_block_out	 ;
	wire [PIXEL_WIDTH*DBF_CH_BLOCK_SIZE*DBF_CH_BLOCK_SIZE-1:0]	cr_I_block_out  ;	
	
	output reg 	[SAO_OUT_FIFO_WIDTH-1:0]	display_fifo_data;
(* MARK_DEBUG *)	output reg 								display_fifo_data_wr_en;
	input 								display_fifo_is_full;
// (* MARK_DEBUG *)	output 								display_fifo_is_empty_out;
// (* MARK_DEBUG *)	input  								display_fifo_rd_en_in;
	// output  [SAO_OUT_FIFO_WIDTH-1:0]	display_fifo_data_out;
	
(* MARK_DEBUG *)	wire 								dpb_fifo_is_full;
(* MARK_DEBUG *)	output 								dpb_fifo_is_empty_out;
(* MARK_DEBUG *)	input  								dpb_fifo_rd_en_in;
	output  [SAO_OUT_FIFO_WIDTH-1:0]	dpb_fifo_data_out;
	

	reg [X_ADDR_WDTH -LOG2_MIN_DU_SIZE -1: 0]				Xc_out_8x8;
	reg [X_ADDR_WDTH -LOG2_MIN_DU_SIZE -1: 0]				Yc_out_8x8;	
	
	reg [X_ADDR_WDTH-1: 0]				Xc_out;
	reg [X_ADDR_WDTH-1: 0]				Yc_out;	



//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

`ifdef CHIPSCOPE_DEBUG
	// assign controller_in_valid = data_valid_in;
	// assign controller_in = y_q_block_2_in;
	// assign controller_out_valid = valid_out;
	// assign controller_out = y_K_block_out;
	assign sao_wr_en_out = display_fifo_data_wr_en;
	// assign sao_rd_en_out = dpb_fifo_rd_en_in;
	assign sao_rd_en_out = data_valid_in;
	assign sao_wr_data = display_fifo_data[512-1:256];
	//assign sao_rd_data = dpb_fifo_data_out[512-1:256];
	assign sao_rd_data = {128'd0,y_A_block_in};

`endif
	assign display_buf_ful = display_fifo_is_full;
	
// fifo_sao_out display_buffer (
  // .clk		(clk), // input clk
  // .rst		(reset), // input rst
  // .din		(display_fifo_data), // input [783 : 0] din
  // .wr_en	(display_fifo_data_wr_en), // input wr_en
  // .rd_en	(display_fifo_rd_en_in), // input rd_en
  // .dout		(display_fifo_data_out), // output [783 : 0] dout
  // .full		(), // output full
  // .empty	(display_fifo_is_empty_out), // output empty
  // .prog_full(display_fifo_is_full) // output prog_full
// );

//`ifdef HEVC_4K
fifo_sao_out_4k dpb_buffer (
//`else
//fifo_sao_out dpb_buffer (
//`endif
  .clk		(clk), // input clk
  .rst		(reset), // input rst
  .din		(display_fifo_data), // input [783 : 0] din
  .wr_en	(display_fifo_data_wr_en), // input wr_en
  .rd_en	(dpb_fifo_rd_en_in), // input rd_en
  .dout		(dpb_fifo_data_out), // output [783 : 0] dout
  .full		(), // output full
  .empty	(dpb_fifo_is_empty_out), // output empty
  .prog_full(dpb_fifo_is_full) // output prog_full
);
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
    
	luma_sao_controller  luma_sao_controller_module(
		.clk			(clk			),
        .reset			(reset			),
        .start			(start_sao_modules),
        .done			(done_sao_luma	),
		.q_block_1_in	(y_q_block_1_in	),
        .q_block_2_in	(y_q_block_2_in	),
        .q_block_3_in	(y_q_block_3_in	),
        .q_block_4_in	(y_q_block_4_in	),
        .A_block_in		(y_A_block_in	),
        .B_block_in		(y_B_block_in	),
		.C_block_in		(y_C_block_in	),
		.D_block_in		(y_D_block_in	),
		.E_block_in		(y_E_block_in	),
		.Xc_in			(Xc_out			),
		.Yc_in			(Yc_out			),
		.pic_width_in	(pic_width_out	),
        .pic_height_in	(pic_height_out	),
		.slice_id_in	(slice_id_out),
		.sao_luma_in	(sao_luma),
		.tile_left_boundary_in 	(tile_left_boundary ),
		.tile_top_boundary_in  	(tile_top_boundary  ),    
		.slice_left_boundary_in	(slice_left_boundary),    
		.slice_top_boundary_in 	(slice_top_boundary ),    
		.pcm_bypass_sao_disable_in (pcm_bypass_sao_disable),  
		.loop_filter_accross_tiles_enabled_in  (loop_filter_accross_tiles_enabled ),
		.loop_filter_accross_slices_enabled_in (loop_filter_accross_slices_enabled),
		.y_sao_type_in	   	(y_sao_type),
		.y_eoclass_in      	(y_bandpos_or_eoclass[1:0]),
		.y_bandpos_in      	(y_bandpos_or_eoclass),
		.y_sao_offset_1_in 	(y_sao_offset_1),
		.y_sao_offset_2_in 	(y_sao_offset_2),
		.y_sao_offset_3_in 	(y_sao_offset_3),
		.y_sao_offset_4_in 	(y_sao_offset_4),
		.q_block_1_out	(y_q_block_1_out),
		.q_block_2_out	(y_q_block_2_out),
		.q_block_3_out	(y_q_block_3_out),
		.q_block_4_out	(y_q_block_4_out),
		.A_block_out  	(y_A_block_out  ),
		.B_block_out  	(y_B_block_out  ),
		.C_block_out  	(y_C_block_out  ),
		.D_block_out  	(y_D_block_out  ),
		.E_block_out  	(y_E_block_out  ),
		.J_block_out	(y_J_block_out	),
		.K_block_out	(y_K_block_out	),
		.M_block_out	(y_M_block_out	),
		.N_block_out	(y_N_block_out	),
		.G_block_out	(y_G_block_out	),
		.H_block_out	(y_H_block_out	),
		.I_block_out	(y_I_block_out  )
		//,.luma_con_in_valid		(controller_in_valid	)
		//,.luma_con_out_valid	(controller_out_valid	)
		//,.luma_con_in_data      (controller_in	)
		//,.luma_con_out_data		(controller_out	)	
		
		
    );
	
	chroma_sao_controller  chroma_sao_controller_module(
		.clk			  	(clk			  	),	
        .reset		        (reset		        ),
        .start		        (start_sao_modules	),
        .done		        (done_sao_chroma	),
		.cb_q_block_1_in	(cb_q_block_1_in	),	
        .cb_q_block_2_in	(cb_q_block_2_in	),	
        .cb_q_block_3_in	(cb_q_block_3_in	),	
        .cb_q_block_4_in	(cb_q_block_4_in	),	
        .cb_A_block_in		(cb_A_block_in		),
        .cb_B_block_in		(cb_B_block_in		),
		.cb_C_block_in		(cb_C_block_in		),
		.cb_D_block_in		(cb_D_block_in		),
		.cb_E_block_in		(cb_E_block_in		),
		.cr_q_block_1_in	(cr_q_block_1_in	),	
        .cr_q_block_2_in	(cr_q_block_2_in	),	
        .cr_q_block_3_in	(cr_q_block_3_in	),	
        .cr_q_block_4_in	(cr_q_block_4_in	),	
        .cr_A_block_in		(cr_A_block_in		),
        .cr_B_block_in		(cr_B_block_in		),
		.cr_C_block_in		(cr_C_block_in		),
		.cr_D_block_in		(cr_D_block_in		),
		.cr_E_block_in		(cr_E_block_in		),
		.Xc_in		        (Xc_out 	        ),
		.Yc_in		        (Yc_out 	        ),
		.pic_width_in		(pic_width_out		),
        .pic_height_in		(pic_height_out		),
		.slice_id_in		(slice_id_out),
		.sao_chroma_in		(sao_chroma),
		.tile_left_boundary_in 	   (tile_left_boundary ),
		.tile_top_boundary_in  	   (tile_top_boundary  ),    
		.slice_left_boundary_in	   (slice_left_boundary),    
		.slice_top_boundary_in 	   (slice_top_boundary ),    
		.pcm_bypass_sao_disable_in (pcm_bypass_sao_disable),  
		.loop_filter_accross_tiles_enabled_in  (loop_filter_accross_tiles_enabled ),
		.loop_filter_accross_slices_enabled_in (loop_filter_accross_slices_enabled),
		.cb_sao_type_in	   	(cb_sao_type),
		.cb_eoclass_in      (cb_bandpos_or_eoclass[1:0]),
		.cb_bandpos_in      (cb_bandpos_or_eoclass),
		.cb_sao_offset_1_in (cb_sao_offset_1),
		.cb_sao_offset_2_in (cb_sao_offset_2),
		.cb_sao_offset_3_in (cb_sao_offset_3),
		.cb_sao_offset_4_in (cb_sao_offset_4),
		.cr_sao_type_in	   	(cr_sao_type),
		.cr_eoclass_in      (cr_bandpos_or_eoclass[1:0]),
		.cr_bandpos_in      (cr_bandpos_or_eoclass),
		.cr_sao_offset_1_in (cr_sao_offset_1),
		.cr_sao_offset_2_in (cr_sao_offset_2),
		.cr_sao_offset_3_in (cr_sao_offset_3),
		.cr_sao_offset_4_in (cr_sao_offset_4),
		.cb_q_block_1_out	(cb_q_block_1_out	),	
		.cb_q_block_2_out	(cb_q_block_2_out	),	
		.cb_q_block_3_out	(cb_q_block_3_out	),	
		.cb_q_block_4_out	(cb_q_block_4_out	),	
		.cb_A_block_out  	(cb_A_block_out  	),	
		.cb_B_block_out  	(cb_B_block_out  	),	
		.cb_C_block_out  	(cb_C_block_out  	),	
		.cb_D_block_out  	(cb_D_block_out  	),	
		.cb_E_block_out  	(cb_E_block_out  	),	
		.cb_J_block_out		(cb_J_block_out		),	
		.cb_K_block_out		(cb_K_block_out		),	
		.cb_M_block_out		(cb_M_block_out		),	
		.cb_N_block_out		(cb_N_block_out		),	
		.cb_G_block_out		(cb_G_block_out		),	
		.cb_H_block_out		(cb_H_block_out		),	
		.cb_I_block_out		(cb_I_block_out		),	
		.cr_q_block_1_out	(cr_q_block_1_out	),	
		.cr_q_block_2_out	(cr_q_block_2_out	),	
		.cr_q_block_3_out	(cr_q_block_3_out	),	
		.cr_q_block_4_out	(cr_q_block_4_out	),	
		.cr_A_block_out  	(cr_A_block_out  	),	
		.cr_B_block_out  	(cr_B_block_out  	),	
		.cr_C_block_out  	(cr_C_block_out  	),	
		.cr_D_block_out  	(cr_D_block_out  	),	
		.cr_E_block_out  	(cr_E_block_out  	),	
		.cr_J_block_out		(cr_J_block_out		),	
		.cr_K_block_out		(cr_K_block_out		),	
		.cr_M_block_out		(cr_M_block_out		),	
		.cr_N_block_out		(cr_N_block_out		),	
		.cr_G_block_out		(cr_G_block_out		),	
		.cr_H_block_out		(cr_H_block_out		),	
		.cr_I_block_out		(cr_I_block_out		)
    );
	
	
	//----------------- Main SAO stage controller SM ---------------------------------------
	
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			state <= STATE_INIT;
            ready_out <= 1'b0;
			display_fifo_data_wr_en <= 0;
		end
		
		else begin
			case(state)
				STATE_INIT : begin
					display_fifo_data_wr_en <= 0;
					if(data_valid_in & ready_out) begin
						Xc_out				    <= 	Xc_in;
						Yc_out				    <= 	Yc_in;
						Xc_out_8x8			    <= 	Xc_in[X11_ADDR_WDTH -1: LOG2_MIN_DU_SIZE]-1'b1;
						Yc_out_8x8              <= 	Yc_in[X11_ADDR_WDTH -1: LOG2_MIN_DU_SIZE]-1'b1;
						pic_width_out   		<= 	pic_width_in;
						pic_height_out  		<= 	pic_height_in;
						tile_id_out     		<= 	tile_id_in;
						slice_id_out    		<= 	slice_id_in;
						sao_luma				<=	sao_luma_in;
						sao_chroma				<=	sao_chroma_in;
						y_sao_type             	<=	y_sao_type_in;
                        y_bandpos_or_eoclass   	<=	y_bandpos_or_eoclass_in;
                        y_sao_offset_1         	<=	y_sao_offset_1_in;		
                        y_sao_offset_2         	<=	y_sao_offset_2_in;		
                        y_sao_offset_3         	<=	y_sao_offset_3_in;       
                        y_sao_offset_4         	<=	y_sao_offset_4_in;       
                        cb_sao_type	           	<=	cb_sao_type_in;
                        cb_bandpos_or_eoclass  	<=	cb_bandpos_or_eoclass_in;
                        cb_sao_offset_1	       	<=	cb_sao_offset_1_in;		
                        cb_sao_offset_2	       	<=	cb_sao_offset_2_in;      
                        cb_sao_offset_3	       	<=	cb_sao_offset_3_in;      
                        cb_sao_offset_4	       	<=	cb_sao_offset_4_in;      
                        cr_sao_type            	<=	cr_sao_type_in;
                        cr_bandpos_or_eoclass  	<=	cr_bandpos_or_eoclass_in;
                        cr_sao_offset_1        	<=	cr_sao_offset_1_in;		
                        cr_sao_offset_2        	<=	cr_sao_offset_2_in;      
                        cr_sao_offset_3        	<=	cr_sao_offset_3_in;      
                        cr_sao_offset_4        	<=	cr_sao_offset_4_in;
						tile_left_boundary  	<=	tile_left_boundary_in  ;
						tile_top_boundary       <=	tile_top_boundary_in   ;              
						slice_left_boundary     <=	slice_left_boundary_in ;              
						slice_top_boundary      <=	slice_top_boundary_in  ;              
						pcm_bypass_sao_disable  <=	pcm_bypass_sao_disable_in ;            
						loop_filter_accross_tiles_enabled  	<= 	loop_filter_accross_tiles_enabled_in;    
						loop_filter_accross_slices_enabled  <= 	loop_filter_accross_slices_enabled_in;
						// start luma and chroma sao controllers
						ready_out <= 1'b0;
						dpb_axi_addr_out <= (dbp_idx_in 	* `REF_PIX_FRAME_OFFSET_VAL)<<(`REF_PIX_FRAME_OFFSET_SHIFT);
						poc_out <= (poc_in );
						log2_ctu_size_out <= log2_ctb_size_in;
						if(Xc_in ==0 || Yc_in ==0) begin
							state <= STATE_WAIT_TILL_DONE;
						end
						else begin
							state <= STATE_1;
						end
					end
					else begin
						ready_out <= 1'b1;
					end
				end
				STATE_WAIT_TILL_DONE: begin
					if(done_sao_luma & done_sao_chroma) begin
						ready_out <= 1'b1;
						state  <= STATE_INIT;
					end
				end
				STATE_1 : begin
					if(valid_out) begin                      
                
						
						display_fifo_data <= {	Xc_out_8x8,Yc_out_8x8,
												 y_C_block_out[4*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_G_block_out[4*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_C_block_out[3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_G_block_out[3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_C_block_out[2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_G_block_out[2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_C_block_out[1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_G_block_out[1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
											                                                                                       
												 y_K_block_out[4*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_J_block_out[4*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_K_block_out[3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_J_block_out[3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_K_block_out[2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_J_block_out[2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_K_block_out[1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_J_block_out[1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												
												cb_C_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cb_G_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
												cb_C_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cb_G_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
											
												cb_K_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cb_J_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
												cb_K_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cb_J_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
					
												cr_C_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cr_G_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
												cr_C_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cr_G_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
											
												cr_K_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cr_J_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
												cr_K_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cr_J_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE]};
						
						if( (display_fifo_is_full == 0) && (dpb_fifo_is_full == 0)) begin
							display_fifo_data_wr_en <= 1;
							if ( ( (Xc_out_8x8+2'd2) < (pic_width_out>>3)) && ((Yc_out_8x8+2'd2) < (pic_height_out>>3)) )  begin
								state <= STATE_INIT;
								ready_out <= 1'b1;
							end
							else if( ((Yc_out_8x8+2'd2) >= (pic_height_out>>3)))  begin
								state <= STATE_BOTTOM;
							end
							else begin
								state <= STATE_RIGHT_ONLY;
							end
						end
						else begin
							display_fifo_data_wr_en <= 0;
						end
						
				
					end
				end
				STATE_RIGHT_ONLY: begin	
					if( (display_fifo_is_full == 0) && (dpb_fifo_is_full == 0)) begin
						display_fifo_data_wr_en <= 1;
						display_fifo_data <= {	Xc_out_8x8+1'b1,Yc_out_8x8,
												 y_E_block_out[4*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_D_block_out[4*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_E_block_out[3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_D_block_out[3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_E_block_out[2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_D_block_out[2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_E_block_out[1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_D_block_out[1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
											                                                                                       
												 y_N_block_out[4*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_M_block_out[4*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_N_block_out[3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_M_block_out[3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_N_block_out[2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_M_block_out[2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_N_block_out[1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_M_block_out[1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												
												cb_E_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cb_D_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
												cb_E_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cb_D_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
											
												cb_N_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cb_M_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
												cb_N_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cb_M_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
					
												cr_E_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cr_D_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
												cr_E_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cr_D_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
											
												cr_N_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cr_M_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
												cr_N_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cr_M_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE]};
						// if( ((Yc_out_8x8+2'd2) >= (pic_height_out>>3)))  begin
							// state <= STATE_BOTTOM_RIGHT;
						// end
						// else begin
							state <= STATE_INIT;
							ready_out <= 1'b1;
						// end
					end	
					else begin
						display_fifo_data_wr_en <= 0;
					end
				end
				STATE_BOTTOM: begin
					if( (display_fifo_is_full == 0) && (dpb_fifo_is_full == 0)) begin
						display_fifo_data_wr_en <= 1;
						display_fifo_data <= {	Xc_out_8x8,Yc_out_8x8+1'b1,
												 y_B_block_out[4*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_I_block_out[4*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_B_block_out[3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_I_block_out[3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_B_block_out[2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_I_block_out[2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_B_block_out[1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_I_block_out[1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
											                                                                                       
												 y_A_block_out[4*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_H_block_out[4*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_A_block_out[3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_H_block_out[3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_A_block_out[2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_H_block_out[2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_A_block_out[1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_H_block_out[1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												
												cb_B_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cb_I_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
												cb_B_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cb_I_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
											
												cb_A_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cb_H_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
												cb_A_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cb_H_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
					
												cr_B_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cr_I_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
												cr_B_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cr_I_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
											
												cr_A_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cr_H_block_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
												cr_A_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cr_H_block_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE]};
						
						if( ( (Xc_out_8x8+2'd2) >= (pic_width_out>>3)))  begin
							state <= STATE_BOTTOM_RIGHT;
						end
						else begin
							state <= STATE_INIT;
							ready_out <= 1'b1;
						end
					end	
					else begin
						display_fifo_data_wr_en <= 0;
					end						
				end
				
				
				
				
				
				STATE_BOTTOM_RIGHT: begin
					if( (display_fifo_is_full == 0) && (dpb_fifo_is_full == 0)) begin
						display_fifo_data_wr_en <= 1;
						display_fifo_data <= {	 Xc_out_8x8+1'b1,Yc_out_8x8+1'b1,
												 y_q_block_4_out[4*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_q_block_3_out[4*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_q_block_4_out[3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_q_block_3_out[3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_q_block_4_out[2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_q_block_3_out[2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_q_block_4_out[1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_q_block_3_out[1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
											                                                                                       
												 y_q_block_2_out[4*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_q_block_1_out[4*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_q_block_2_out[3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_q_block_1_out[3*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_q_block_2_out[2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_q_block_1_out[2*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												 y_q_block_2_out[1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE], y_q_block_1_out[1*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_YY_BLOCK_SIZE],
												
												cb_q_block_4_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cb_q_block_3_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
												cb_q_block_4_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cb_q_block_3_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
											
												cb_q_block_2_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cb_q_block_1_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
												cb_q_block_2_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cb_q_block_1_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
					
												cr_q_block_4_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cr_q_block_3_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
												cr_q_block_4_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cr_q_block_3_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
											
												cr_q_block_2_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cr_q_block_1_out[2*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],
												cr_q_block_2_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE],cr_q_block_1_out[1*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE-1:0*PIXEL_WIDTH*DBF_CH_BLOCK_SIZE]};
						
						state <= STATE_RIGHT_ONLY;
					end	
					else begin
						display_fifo_data_wr_en <= 0;
					end			
				end
			endcase
		end
	end
	
	
	//---------Combinational Block to generate valid out ---------------//
	always @(*) begin
		case(state)
			STATE_1 : begin
				if(done_sao_luma & done_sao_chroma) begin		// done signals will be held high until instructed to restart
					valid_out = 1'b1;							// required since the 2 modules can finish at different times
				end
				else begin
					valid_out = 1'b0;
				end
			end
			default : begin
				valid_out = 1'b0;
			end
		endcase
	end
	
	//---------Combinational Block to generate start signal for submodules ---------------//
	always @(*) begin
		case(state)
			STATE_INIT : begin
				if(data_valid_in & ready_out) begin
					start_sao_modules = 1'b1;
				end
				else begin
					start_sao_modules = 1'b0;
				end
			end
			default : begin
				start_sao_modules = 1'b0;
			end
		endcase
	end
	
	

endmodule
