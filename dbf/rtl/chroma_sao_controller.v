`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:05:19 05/20/2014 
// Design Name: 
// Module Name:    chroma_sao_controller 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module chroma_sao_controller(
		clk,
        reset,
        start,
        done,
		cb_q_block_1_in,
        cb_q_block_2_in,
        cb_q_block_3_in,
        cb_q_block_4_in,
        cb_A_block_in,
        cb_B_block_in,
		cb_C_block_in,
		cb_D_block_in,
		cb_E_block_in,
		cr_q_block_1_in,
        cr_q_block_2_in,
        cr_q_block_3_in,
        cr_q_block_4_in,
        cr_A_block_in,
        cr_B_block_in,
		cr_C_block_in,
		cr_D_block_in,
		cr_E_block_in,
		Xc_in,
		Yc_in,
		pic_width_in,
        pic_height_in,
		slice_id_in,
		sao_chroma_in,
		tile_left_boundary_in ,
		tile_top_boundary_in  ,    
		slice_left_boundary_in,    
		slice_top_boundary_in ,    
		pcm_bypass_sao_disable_in,  
		loop_filter_accross_tiles_enabled_in ,
		loop_filter_accross_slices_enabled_in,
		cb_sao_type_in	  ,
		cb_eoclass_in     ,
		cb_bandpos_in     ,
		cb_sao_offset_1_in,
		cb_sao_offset_2_in,
		cb_sao_offset_3_in,
		cb_sao_offset_4_in,
		cr_sao_type_in	  ,
		cr_eoclass_in     ,
		cr_bandpos_in     ,
		cr_sao_offset_1_in,
		cr_sao_offset_2_in,
		cr_sao_offset_3_in,
		cr_sao_offset_4_in,
		
		cb_q_block_1_out,
		cb_q_block_2_out,
		cb_q_block_3_out,
		cb_q_block_4_out,
		cb_A_block_out  ,
		cb_B_block_out  ,
		cb_C_block_out  ,
		cb_D_block_out  ,
		cb_E_block_out  ,
		cb_J_block_out	,
		cb_K_block_out	,
		cb_M_block_out	,
		cb_N_block_out	,
		cb_G_block_out	,
		cb_H_block_out	,
		cb_I_block_out	,
		cr_q_block_1_out,
		cr_q_block_2_out,
		cr_q_block_3_out,
		cr_q_block_4_out,
		cr_A_block_out  ,
		cr_B_block_out  ,
		cr_C_block_out  ,
		cr_D_block_out  ,
		cr_E_block_out  ,
		cr_J_block_out	,
		cr_K_block_out	,
		cr_M_block_out	,
		cr_N_block_out	,
		cr_G_block_out	,
		cr_H_block_out	,
		cr_I_block_out	
    );
	
	`include "../sim/pred_def.v"
	
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input                               clk;
    input                               reset;
    
	input                               start;
    output	reg					        done;
	
	// Pixel inputs
	input       	[31:0]         	cb_q_block_1_in ;
    input       	[31:0]         	cb_q_block_2_in ;
    input       	[31:0]         	cb_q_block_3_in ;
    input       	[31:0]         	cb_q_block_4_in ;
    input       	[31:0]         	cb_A_block_in   ;
    input       	[31:0]         	cb_B_block_in   ;
    input			[31:0]         	cb_C_block_in   ;
    input			[31:0]         	cb_D_block_in   ;
    input			[31:0]         	cb_E_block_in   ;
	
	input       	[31:0]         	cr_q_block_1_in ;
    input       	[31:0]         	cr_q_block_2_in ;
    input       	[31:0]         	cr_q_block_3_in ;
    input       	[31:0]         	cr_q_block_4_in ;
    input       	[31:0]         	cr_A_block_in   ;
    input       	[31:0]         	cr_B_block_in   ;
    input			[31:0]         	cr_C_block_in   ;
    input			[31:0]         	cr_D_block_in   ;
    input			[31:0]         	cr_E_block_in   ;
	
	// Other inputs
	input			[11:0]			Xc_in ;
	input			[11:0]			Yc_in ;
	input			[11:0]			pic_width_in ;
	input			[11:0]			pic_height_in;
	input			[7:0]			slice_id_in;
	input							sao_chroma_in;
	input							tile_left_boundary_in  ;
	input							tile_top_boundary_in   ;    
	input							slice_left_boundary_in ;    
	input							slice_top_boundary_in  ;    
	input							pcm_bypass_sao_disable_in ;  
	input							loop_filter_accross_tiles_enabled_in ;
	input							loop_filter_accross_slices_enabled_in;
	
	input			[1:0]   							cb_sao_type_in	   ;
	input			[1:0]								cb_eoclass_in      ;
	input			[4:0]								cb_bandpos_in      ;
	input			[SAO_OFFSET_WIDTH -1:0]				cb_sao_offset_1_in ;
	input			[SAO_OFFSET_WIDTH -1:0]				cb_sao_offset_2_in ;
	input			[SAO_OFFSET_WIDTH -1:0]				cb_sao_offset_3_in ;
	input			[SAO_OFFSET_WIDTH -1:0]				cb_sao_offset_4_in ;
	
	input			[1:0]   							cr_sao_type_in	   ;
	input			[1:0]								cr_eoclass_in      ;
	input			[4:0]								cr_bandpos_in      ;
	input			[SAO_OFFSET_WIDTH -1:0]				cr_sao_offset_1_in ;
	input			[SAO_OFFSET_WIDTH -1:0]				cr_sao_offset_2_in ;
	input			[SAO_OFFSET_WIDTH -1:0]				cr_sao_offset_3_in ;
	input			[SAO_OFFSET_WIDTH -1:0]				cr_sao_offset_4_in ;
	
	// Pixel outputs
	output       	[31:0]         	cb_q_block_1_out ;
    output       	[31:0]         	cb_q_block_2_out ;
    output       	[31:0]         	cb_q_block_3_out ;
    output       	[31:0]         	cb_q_block_4_out ;
    output       	[31:0]         	cb_A_block_out   ;
    output       	[31:0]         	cb_B_block_out   ;
    output			[31:0]         	cb_C_block_out   ;
    output			[31:0]         	cb_D_block_out   ;
    output 			[31:0]         	cb_E_block_out   ;
	output			[31:0]			cb_J_block_out	 ;
	output			[31:0]			cb_K_block_out	 ;
	output			[31:0]			cb_M_block_out	 ;
	output			[31:0]			cb_N_block_out	 ;
	output			[31:0]			cb_G_block_out	 ;
	output			[31:0]			cb_H_block_out	 ;
	output			[31:0]			cb_I_block_out	 ;
	
	output       	[31:0]         	cr_q_block_1_out ;
    output       	[31:0]         	cr_q_block_2_out ;
    output       	[31:0]         	cr_q_block_3_out ;
    output       	[31:0]         	cr_q_block_4_out ;
    output       	[31:0]         	cr_A_block_out   ;
    output       	[31:0]         	cr_B_block_out   ;
    output			[31:0]         	cr_C_block_out   ;
    output			[31:0]         	cr_D_block_out   ;
    output 			[31:0]         	cr_E_block_out   ;
	output			[31:0]			cr_J_block_out	 ;
	output			[31:0]			cr_K_block_out	 ;
	output			[31:0]			cr_M_block_out	 ;
	output			[31:0]			cr_N_block_out	 ;
	output			[31:0]			cr_G_block_out	 ;
	output			[31:0]			cr_H_block_out	 ;
	output			[31:0]			cr_I_block_out	 ;
	
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    
    localparam                          STATE_INIT 	=  0 ;    // the state that is entered upon reset
    localparam                          STATE_1    	=  1 ;
    localparam                          STATE_2    	=  2 ;
	localparam                          STATE_3    	=  3 ;
	localparam                          STATE_4     =  4 ;
	localparam                          STATE_5     =  5 ;
	localparam                          STATE_6    	=  6 ;
	localparam                          STATE_7    	=  7 ;
	localparam                          STATE_8     =  8 ;
	localparam                          STATE_9     =  9 ;
	localparam                          STATE_10    =  10 ;
	localparam                          STATE_11    =  11 ;
    localparam                          STATE_12    =  12 ;
	localparam                          STATE_13    =  13 ;
	localparam                          STATE_14    =  14 ;
	
	localparam			SAMPLE_COL_BUF_DATA_WIDTH			=	((BIT_DEPTH*4) + 33) ;
	localparam			SAMPLE_ROW_BUF_DATA_WIDTH			=	((BIT_DEPTH*8) + 33) ;
	localparam			DECISION_ROW_BUF_DATA_WIDTH			=	((BIT_DEPTH*6) + 5) ;
	localparam			LEFT_DECISION_COL_BUF_DATA_WIDTH	=	((BIT_DEPTH*5) + 5) ;
	localparam			RIGHT_DECISION_COL_BUF_DATA_WIDTH	=	((BIT_DEPTH*2) + 5) ;
    
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    
	// Pixel Registers
	
	reg             [7:0]           		cb_q_block_1 [3:0]	;
    reg             [7:0]           		cb_q_block_2 [3:0]	;
    reg             [7:0]           		cb_q_block_3 [3:0]	;
    reg             [7:0]           		cb_q_block_4 [3:0]	;
    reg             [7:0]           		cb_A_block 	 [3:0]	;
    reg             [7:0]           		cb_B_block 	 [3:0]	;
    reg             [7:0]           		cb_C_block 	 [3:0]	;
    reg             [7:0]           		cb_D_block 	 [3:0]	;
    reg             [7:0]           		cb_E_block 	 [3:0]	;
	reg             [7:0]           		cb_J_block 	 [3:0]	;
    reg             [7:0]           		cb_K_block 	 [3:0]	;
    reg             [7:0]           		cb_M_block 	 [3:0]	;
    reg             [7:0]           		cb_N_block 	 [3:0]	;
	reg             [7:0]           		cb_G_block 	 [3:0]	;
    reg             [7:0]           		cb_H_block 	 [3:0]	;
    reg             [7:0]           		cb_I_block 	 [3:0]	;
	
	reg             [7:0]           		cr_q_block_1 [3:0]	;
    reg             [7:0]           		cr_q_block_2 [3:0]	;
    reg             [7:0]           		cr_q_block_3 [3:0]	;
    reg             [7:0]           		cr_q_block_4 [3:0]	;
    reg             [7:0]           		cr_A_block 	 [3:0]	;
    reg             [7:0]           		cr_B_block 	 [3:0]	;
    reg             [7:0]           		cr_C_block 	 [3:0]	;
    reg             [7:0]           		cr_D_block 	 [3:0]	;
    reg             [7:0]           		cr_E_block 	 [3:0]	;
	reg             [7:0]           		cr_J_block 	 [3:0]	;
    reg             [7:0]           		cr_K_block 	 [3:0]	;
    reg             [7:0]           		cr_M_block 	 [3:0]	;
    reg             [7:0]           		cr_N_block 	 [3:0]	;
	reg             [7:0]           		cr_G_block 	 [3:0]	;
    reg             [7:0]           		cr_H_block 	 [3:0]	;
    reg             [7:0]           		cr_I_block 	 [3:0]	;
	
	reg				[7:0]					cb_decision_row		  [5:0] ;
	reg				[7:0]					cb_decision_col_left  [4:0] ;
	reg				[7:0]					cb_decision_col_right [1:0] ;
	reg				[7:0]					cr_decision_row		  [5:0] ;
	reg				[7:0]					cr_decision_col_left  [4:0] ;
	reg				[7:0]					cr_decision_col_right [1:0] ;
	
	wire			[XC_WIDTH-4:0]			J_block_X8 ;
	wire			[XC_WIDTH-4:0]			J_block_Y8 ;
	wire			[XC_WIDTH-4:0]			H_block_X8 ;
	wire			[XC_WIDTH-4:0]			H_block_Y8 ;
	wire			[XC_WIDTH-4:0]			M_block_X8 ;
	wire			[XC_WIDTH-4:0]			M_block_Y8 ;
	wire			[XC_WIDTH-4:0]			J_block_TR_X8 ;
	wire			[XC_WIDTH-4:0]			J_block_TR_Y8 ;
	wire			[XC_WIDTH-4:0]			J_block_BL_X8 ;
	wire			[XC_WIDTH-4:0]			J_block_BL_Y8 ;
	
	wire			[2*XC_WIDTH-9:0]		J_block_zs_addr	;
	wire			[2*XC_WIDTH-9:0]		H_block_zs_addr	;
	wire			[2*XC_WIDTH-9:0]		M_block_zs_addr	;
	wire			[2*XC_WIDTH-9:0]		J_block_TR_zs_addr ;
	wire			[2*XC_WIDTH-9:0]		J_block_BL_zs_addr ;
	wire			[2*XC_WIDTH-9:0]		H_block_TR_zs_addr ;
	wire			[2*XC_WIDTH-9:0]		M_block_BL_zs_addr ;
	
	reg										H_block_sao_across_slices_enable_flag ;
	reg										H_block_pcm_bypass_sao_disable_flag ;
	reg										H_block_tile_left_flag	;
	reg										H_block_tile_top_flag   ;
	reg										H_block_slice_left_flag ;
	reg										H_block_slice_top_flag  ;
	reg										H_block_sao_chroma		;
	reg				[7:0]					H_block_slice_id  		;
	reg				[1:0]					H_block_cb_sao_type     ;
	reg				[1:0]					H_block_cb_eoclass      ;
	reg				[4:0]					H_block_cb_bandpos      ;
	reg				[3:0]					H_block_cb_sao_offset_1 ;
	reg				[3:0]					H_block_cb_sao_offset_2 ;
	reg				[3:0]					H_block_cb_sao_offset_3 ;
	reg				[3:0]					H_block_cb_sao_offset_4 ;
	reg				[1:0]					H_block_cr_sao_type     ;
	reg				[1:0]					H_block_cr_eoclass      ;
	reg				[4:0]					H_block_cr_bandpos      ;
	reg				[3:0]					H_block_cr_sao_offset_1 ;
	reg				[3:0]					H_block_cr_sao_offset_2 ;
	reg				[3:0]					H_block_cr_sao_offset_3 ;
	reg				[3:0]					H_block_cr_sao_offset_4 ;
	
	reg										J_block_sao_across_slices_enable_flag ;
	reg										J_block_pcm_bypass_sao_disable_flag ;
	reg										J_block_tile_left_flag	;
	reg										J_block_tile_top_flag   ;
	reg										J_block_slice_left_flag ;
	reg										J_block_slice_top_flag  ;
	reg										J_block_sao_chroma		;
	reg				[7:0]					J_block_slice_id  		;
	reg				[1:0]					J_block_cb_sao_type     ;
	reg				[1:0]					J_block_cb_eoclass      ;
	reg				[4:0]					J_block_cb_bandpos      ;
	reg				[3:0]					J_block_cb_sao_offset_1 ;
	reg				[3:0]					J_block_cb_sao_offset_2 ;
	reg				[3:0]					J_block_cb_sao_offset_3 ;
	reg				[3:0]					J_block_cb_sao_offset_4 ;
	reg				[1:0]					J_block_cr_sao_type     ;
	reg				[1:0]					J_block_cr_eoclass      ;
	reg				[4:0]					J_block_cr_bandpos      ;
	reg				[3:0]					J_block_cr_sao_offset_1 ;
	reg				[3:0]					J_block_cr_sao_offset_2 ;
	reg				[3:0]					J_block_cr_sao_offset_3 ;
	reg				[3:0]					J_block_cr_sao_offset_4 ;
	
	reg										M_block_sao_across_slices_enable_flag ;
	reg										M_block_pcm_bypass_sao_disable_flag ;
	reg										M_block_tile_left_flag	;
	reg										M_block_tile_top_flag   ;
	reg										M_block_slice_left_flag ;
	reg										M_block_slice_top_flag  ;
	reg										M_block_sao_chroma		;
	reg				[7:0]					M_block_slice_id  		;
	reg				[1:0]					M_block_cb_sao_type     ;
	reg				[1:0]					M_block_cb_eoclass      ;
	reg				[4:0]					M_block_cb_bandpos      ;
	reg				[3:0]					M_block_cb_sao_offset_1 ;
	reg				[3:0]					M_block_cb_sao_offset_2 ;
	reg				[3:0]					M_block_cb_sao_offset_3 ;
	reg				[3:0]					M_block_cb_sao_offset_4 ;
	reg				[1:0]					M_block_cr_sao_type     ;
	reg				[1:0]					M_block_cr_eoclass      ;
	reg				[4:0]					M_block_cr_bandpos      ;
	reg				[3:0]					M_block_cr_sao_offset_1 ;
	reg				[3:0]					M_block_cr_sao_offset_2 ;
	reg				[3:0]					M_block_cr_sao_offset_3 ;
	reg				[3:0]					M_block_cr_sao_offset_4 ;
	
	reg										decision_top_left_slice_boundary ;
	reg										decision_top_right_sao_across_slices_enable ;
	reg										decision_bottom_left_sao_across_slices_enable ;
	reg				[7:0]					decision_top_right_slice_id ;
	reg				[7:0]					decision_bottom_left_slice_id ;
	
	reg										J_block_cb_sao_apply ;
	reg										H_block_cb_sao_apply ;
	reg										M_block_cb_sao_apply ;
	reg										q1_block_cb_sao_apply ;
	reg										J_block_cr_sao_apply ;
	reg										H_block_cr_sao_apply ;
	reg										M_block_cr_sao_apply ;
	reg										q1_block_cr_sao_apply ;
	
	reg										J_block_top_right_sao_across_slices_enable_flag	  ;
	reg										J_block_bottom_left_sao_across_slices_enable_flag ;
	reg										H_block_top_right_sao_across_slices_enable_flag   ;
	reg										M_block_bottom_left_sao_across_slices_enable_flag ;
	
	wire									J_block_cross_boundary_left			;
	wire									J_block_cross_boundary_right		;
	wire									J_block_cross_boundary_top			;
	wire									J_block_cross_boundary_bottom	    ;
	wire									J_block_cross_boundary_top_left		;
	wire									J_block_cross_boundary_top_right	;
	wire									J_block_cross_boundary_bottom_left	;
	wire									J_block_cross_boundary_bottom_right	;
	
	wire									H_block_cross_boundary_left			;
	wire									H_block_cross_boundary_right		;
	wire									H_block_cross_boundary_top			;
	wire									H_block_cross_boundary_bottom	    ;
	wire									H_block_cross_boundary_top_left		;
	wire									H_block_cross_boundary_top_right	;
	wire									H_block_cross_boundary_bottom_left	;
	wire									H_block_cross_boundary_bottom_right	;
	
	wire									M_block_cross_boundary_left			;
	wire									M_block_cross_boundary_right		;
	wire									M_block_cross_boundary_top			;
	wire									M_block_cross_boundary_bottom	    ;
	wire									M_block_cross_boundary_top_left		;
	wire									M_block_cross_boundary_top_right	;
	wire									M_block_cross_boundary_bottom_left	;
	wire									M_block_cross_boundary_bottom_right	;
	
	wire									q_block_cross_boundary_left			;
	wire									q_block_cross_boundary_right		;
	wire									q_block_cross_boundary_top			;
	wire									q_block_cross_boundary_bottom	    ;
	wire									q_block_cross_boundary_top_left		;
	wire									q_block_cross_boundary_top_right	;
	wire									q_block_cross_boundary_bottom_left	;
	wire									q_block_cross_boundary_bottom_right	;
	
	// SAO Block Filter connections
	reg										start_cb_sao_filter ;
	reg										start_cr_sao_filter ;
	reg										cross_boundary_left_flag_sao   ;
	reg										cross_boundary_right_flag_sao  ;
	reg										cross_boundary_top_flag_sao	   ;
	reg										cross_boundary_bottom_flag_sao ;
	reg										cross_boundary_top_left_flag_sao	;
	reg										cross_boundary_top_right_flag_sao	;
	reg										cross_boundary_bottom_left_flag_sao ;
	reg										cross_boundary_bottom_right_flag_sao;
	
	reg				[7:0]					cb_p_0_0_in_sao ;
	reg				[7:0]					cb_p_0_1_in_sao ;
	reg				[7:0]					cb_p_0_2_in_sao ;
	reg				[7:0]					cb_p_0_3_in_sao ;
	reg				[7:0]					cb_p_0_4_in_sao ;
	reg				[7:0]					cb_p_0_5_in_sao ;
	reg				[7:0]					cb_p_1_0_in_sao ;
	reg				[7:0]					cb_p_1_1_in_sao ;
	reg				[7:0]					cb_p_1_2_in_sao ;
	reg				[7:0]					cb_p_1_3_in_sao ;
	reg				[7:0]					cb_p_1_4_in_sao ;
	reg				[7:0]					cb_p_1_5_in_sao ;
	reg				[7:0]					cb_p_2_0_in_sao ;
	reg				[7:0]					cb_p_2_1_in_sao ;
	reg				[7:0]					cb_p_2_2_in_sao ;
	reg				[7:0]					cb_p_2_3_in_sao ;
	reg				[7:0]					cb_p_2_4_in_sao ;
	reg				[7:0]					cb_p_2_5_in_sao ;
	reg				[7:0]					cb_p_3_0_in_sao ;
	reg				[7:0]					cb_p_3_1_in_sao ;
	reg				[7:0]					cb_p_3_2_in_sao ;
	reg				[7:0]					cb_p_3_3_in_sao ;
	reg				[7:0]					cb_p_3_4_in_sao ;
	reg				[7:0]					cb_p_3_5_in_sao ;
	reg				[7:0]                   cb_p_4_0_in_sao ;
	reg				[7:0]                   cb_p_4_1_in_sao ;
	reg				[7:0]                   cb_p_4_2_in_sao ;
	reg				[7:0]                   cb_p_4_3_in_sao ;
	reg				[7:0]                   cb_p_4_4_in_sao ;
	reg				[7:0]                   cb_p_4_5_in_sao ;
	reg				[7:0]                   cb_p_5_0_in_sao ;
	reg				[7:0]                   cb_p_5_1_in_sao ;
	reg				[7:0]                   cb_p_5_2_in_sao ;
	reg				[7:0]                   cb_p_5_3_in_sao ;
	reg				[7:0]                   cb_p_5_4_in_sao ;
	reg				[7:0]                   cb_p_5_5_in_sao ;
	reg				[1:0]					cb_sao_type_in_sao	   ;
	reg				[1:0]                   cb_sao_eo_class_in_sao ;
	reg				[4:0]                   cb_sao_bandpos_in_sao  ;
	reg				[3:0]                   cb_sao_offset_1_in_sao ;
	reg				[3:0]                   cb_sao_offset_2_in_sao ;
	reg				[3:0]                   cb_sao_offset_3_in_sao ;
	reg				[3:0]                   cb_sao_offset_4_in_sao ;
	wire			[7:0]					cb_pix_1_1_out_sao ;
	wire			[7:0]                   cb_pix_1_2_out_sao ;
	wire			[7:0]                   cb_pix_1_3_out_sao ;
	wire			[7:0]                   cb_pix_1_4_out_sao ;
	wire			[7:0]                   cb_pix_2_1_out_sao ;
	wire			[7:0]                   cb_pix_2_2_out_sao ;
	wire			[7:0]                   cb_pix_2_3_out_sao ;
	wire			[7:0]                   cb_pix_2_4_out_sao ;
	wire			[7:0]                   cb_pix_3_1_out_sao ;
	wire			[7:0]                   cb_pix_3_2_out_sao ;
	wire			[7:0]                   cb_pix_3_3_out_sao ;
	wire			[7:0]                   cb_pix_3_4_out_sao ;
	wire			[7:0]                   cb_pix_4_1_out_sao ;
	wire			[7:0]                   cb_pix_4_2_out_sao ;
	wire			[7:0]                   cb_pix_4_3_out_sao ;
	wire			[7:0]                   cb_pix_4_4_out_sao ;
	
	reg				[7:0]					cr_p_0_0_in_sao ;
	reg				[7:0]					cr_p_0_1_in_sao ;
	reg				[7:0]					cr_p_0_2_in_sao ;
	reg				[7:0]					cr_p_0_3_in_sao ;
	reg				[7:0]					cr_p_0_4_in_sao ;
	reg				[7:0]					cr_p_0_5_in_sao ;
	reg				[7:0]					cr_p_1_0_in_sao ;
	reg				[7:0]					cr_p_1_1_in_sao ;
	reg				[7:0]					cr_p_1_2_in_sao ;
	reg				[7:0]					cr_p_1_3_in_sao ;
	reg				[7:0]					cr_p_1_4_in_sao ;
	reg				[7:0]					cr_p_1_5_in_sao ;
	reg				[7:0]					cr_p_2_0_in_sao ;
	reg				[7:0]					cr_p_2_1_in_sao ;
	reg				[7:0]					cr_p_2_2_in_sao ;
	reg				[7:0]					cr_p_2_3_in_sao ;
	reg				[7:0]					cr_p_2_4_in_sao ;
	reg				[7:0]					cr_p_2_5_in_sao ;
	reg				[7:0]					cr_p_3_0_in_sao ;
	reg				[7:0]					cr_p_3_1_in_sao ;
	reg				[7:0]					cr_p_3_2_in_sao ;
	reg				[7:0]					cr_p_3_3_in_sao ;
	reg				[7:0]					cr_p_3_4_in_sao ;
	reg				[7:0]					cr_p_3_5_in_sao ;
	reg				[7:0]                   cr_p_4_0_in_sao ;
	reg				[7:0]                   cr_p_4_1_in_sao ;
	reg				[7:0]                   cr_p_4_2_in_sao ;
	reg				[7:0]                   cr_p_4_3_in_sao ;
	reg				[7:0]                   cr_p_4_4_in_sao ;
	reg				[7:0]                   cr_p_4_5_in_sao ;
	reg				[7:0]                   cr_p_5_0_in_sao ;
	reg				[7:0]                   cr_p_5_1_in_sao ;
	reg				[7:0]                   cr_p_5_2_in_sao ;
	reg				[7:0]                   cr_p_5_3_in_sao ;
	reg				[7:0]                   cr_p_5_4_in_sao ;
	reg				[7:0]                   cr_p_5_5_in_sao ;
	reg				[1:0]					cr_sao_type_in_sao	   ;
	reg				[1:0]                   cr_sao_eo_class_in_sao ;
	reg				[4:0]                   cr_sao_bandpos_in_sao  ;
	reg				[3:0]                   cr_sao_offset_1_in_sao ;
	reg				[3:0]                   cr_sao_offset_2_in_sao ;
	reg				[3:0]                   cr_sao_offset_3_in_sao ;
	reg				[3:0]                   cr_sao_offset_4_in_sao ;
	wire			[7:0]					cr_pix_1_1_out_sao ;
	wire			[7:0]                   cr_pix_1_2_out_sao ;
	wire			[7:0]                   cr_pix_1_3_out_sao ;
	wire			[7:0]                   cr_pix_1_4_out_sao ;
	wire			[7:0]                   cr_pix_2_1_out_sao ;
	wire			[7:0]                   cr_pix_2_2_out_sao ;
	wire			[7:0]                   cr_pix_2_3_out_sao ;
	wire			[7:0]                   cr_pix_2_4_out_sao ;
	wire			[7:0]                   cr_pix_3_1_out_sao ;
	wire			[7:0]                   cr_pix_3_2_out_sao ;
	wire			[7:0]                   cr_pix_3_3_out_sao ;
	wire			[7:0]                   cr_pix_3_4_out_sao ;
	wire			[7:0]                   cr_pix_4_1_out_sao ;
	wire			[7:0]                   cr_pix_4_2_out_sao ;
	wire			[7:0]                   cr_pix_4_3_out_sao ;
	wire			[7:0]                   cr_pix_4_4_out_sao ;
	
	// Interconnect wires
	reg 													row_we_a 		  ;
	reg 													row_en_a 		  ;
	reg 			[8:0]									row_addr_a 		  ;
	reg 			[SAMPLE_ROW_BUF_DATA_WIDTH-1:0]			row_cb_data_in_a  ;
	reg 			[SAMPLE_ROW_BUF_DATA_WIDTH-1:0]			row_cr_data_in_a  ;
	wire			[SAMPLE_ROW_BUF_DATA_WIDTH-1:0]			row_cb_data_out_a ;
	wire			[SAMPLE_ROW_BUF_DATA_WIDTH-1:0]			row_cr_data_out_a ;
	
	reg 													col_we_a 		  ;
	reg 													col_en_a 		  ;
	reg 													col_we_b 		  ;
	reg 													col_en_b 		  ;
	reg 			[9:0]									col_addr_a 		  ;
	reg 			[9:0]									col_addr_b 		  ;
	reg 			[SAMPLE_COL_BUF_DATA_WIDTH-1:0]			col_cb_data_in_a  ;
	reg 			[SAMPLE_COL_BUF_DATA_WIDTH-1:0]			col_cb_data_in_b  ;
	reg 			[SAMPLE_COL_BUF_DATA_WIDTH-1:0]			col_cr_data_in_a  ;
	reg 			[SAMPLE_COL_BUF_DATA_WIDTH-1:0]			col_cr_data_in_b  ;
	wire			[SAMPLE_COL_BUF_DATA_WIDTH-1:0]			col_cb_data_out_a ;
	wire			[SAMPLE_COL_BUF_DATA_WIDTH-1:0]			col_cb_data_out_b ;
	wire			[SAMPLE_COL_BUF_DATA_WIDTH-1:0]			col_cr_data_out_a ;
	wire			[SAMPLE_COL_BUF_DATA_WIDTH-1:0]			col_cr_data_out_b ;
	
	reg															d_row_buf_we_a       	;
	reg		        											d_row_buf_en_a       	;
	reg		  		[8:0]										d_row_buf_addr_a     	;
	reg				[DECISION_ROW_BUF_DATA_WIDTH-1:0]  			d_row_buf_cb_data_in_a  ;
	reg				[DECISION_ROW_BUF_DATA_WIDTH-1:0]  			d_row_buf_cr_data_in_a  ;
	wire	    	[DECISION_ROW_BUF_DATA_WIDTH-1:0]  			d_row_buf_cb_data_out_a ;
	wire	    	[DECISION_ROW_BUF_DATA_WIDTH-1:0]  			d_row_buf_cr_data_out_a ;
	
	reg															d_left_col_buf_we_a       	;
	reg		        											d_left_col_buf_en_a       	;
	reg		  		[8:0]										d_left_col_buf_addr_a     	;
	reg				[LEFT_DECISION_COL_BUF_DATA_WIDTH-1:0]  	d_left_col_buf_cb_data_in_a  ;
	reg				[LEFT_DECISION_COL_BUF_DATA_WIDTH-1:0]  	d_left_col_buf_cr_data_in_a  ;
	wire	    	[LEFT_DECISION_COL_BUF_DATA_WIDTH-1:0]  	d_left_col_buf_cb_data_out_a ;
	wire	    	[LEFT_DECISION_COL_BUF_DATA_WIDTH-1:0]  	d_left_col_buf_cr_data_out_a ;
	
	reg															d_right_col_buf_we_a       	  ;
	reg		        											d_right_col_buf_en_a       	  ;
	reg		  		[8:0]										d_right_col_buf_addr_a     	  ;
	reg				[RIGHT_DECISION_COL_BUF_DATA_WIDTH-1:0]  	d_right_col_buf_cb_data_in_a  ;
	reg				[RIGHT_DECISION_COL_BUF_DATA_WIDTH-1:0]  	d_right_col_buf_cr_data_in_a  ;
	wire	    	[RIGHT_DECISION_COL_BUF_DATA_WIDTH-1:0]  	d_right_col_buf_cb_data_out_a ;
	wire	    	[RIGHT_DECISION_COL_BUF_DATA_WIDTH-1:0]  	d_right_col_buf_cr_data_out_a ;
	
	
	integer 								state;
    
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
    
	//------------ Output assignments -----------------------
	
	assign   cb_q_block_1_out  	= 	( { cb_q_block_1[3], 	cb_q_block_1[2], 	cb_q_block_1[1], 	cb_q_block_1[0]  } ) ;
    assign   cb_q_block_2_out  	= 	( { cb_q_block_2[3], 	cb_q_block_2[2], 	cb_q_block_2[1], 	cb_q_block_2[0]  } ) ;
    assign   cb_q_block_3_out  	= 	( { cb_q_block_3[3], 	cb_q_block_3[2], 	cb_q_block_3[1], 	cb_q_block_3[0]  } ) ;
    assign   cb_q_block_4_out  	= 	( { cb_q_block_4[3], 	cb_q_block_4[2], 	cb_q_block_4[1], 	cb_q_block_4[0]  } ) ;
    assign   cb_A_block_out    	= 	( { cb_A_block	[3], 	cb_A_block	[2],   	cb_A_block	[1],   	cb_A_block	[0]  } ) ;
    assign   cb_B_block_out    	= 	( { cb_B_block	[3], 	cb_B_block	[2],   	cb_B_block	[1],   	cb_B_block	[0]  } ) ;
    assign   cb_C_block_out    	= 	( { cb_C_block	[3], 	cb_C_block	[2],   	cb_C_block	[1],   	cb_C_block	[0]  } ) ;
    assign   cb_D_block_out    	= 	( { cb_D_block	[3], 	cb_D_block	[2],   	cb_D_block	[1],   	cb_D_block	[0]  } ) ;
    assign   cb_E_block_out    	= 	( { cb_E_block	[3], 	cb_E_block	[2],   	cb_E_block	[1],   	cb_E_block	[0]  } ) ;
	assign   cb_J_block_out    	= 	( { cb_J_block	[3], 	cb_J_block	[2],   	cb_J_block	[1],   	cb_J_block	[0]  } ) ;
    assign   cb_K_block_out    	= 	( { cb_K_block	[3], 	cb_K_block	[2],   	cb_K_block	[1],   	cb_K_block	[0]  } ) ;
    assign   cb_M_block_out    	= 	( { cb_M_block	[3], 	cb_M_block	[2],   	cb_M_block	[1],   	cb_M_block	[0]  } ) ;
    assign   cb_N_block_out    	= 	( { cb_N_block	[3], 	cb_N_block	[2],   	cb_N_block	[1],   	cb_N_block	[0]  } ) ;
    assign   cb_G_block_out    	= 	( { cb_G_block	[3], 	cb_G_block	[2],   	cb_G_block	[1],   	cb_G_block	[0]  } ) ;
	assign   cb_H_block_out    	= 	( { cb_H_block	[3], 	cb_H_block	[2],   	cb_H_block	[1],   	cb_H_block	[0]  } ) ;
    assign   cb_I_block_out    	= 	( { cb_I_block	[3], 	cb_I_block	[2],   	cb_I_block	[1],   	cb_I_block	[0]  } ) ;
	
    assign   cr_q_block_1_out  	= 	( { cr_q_block_1[3], 	cr_q_block_1[2], 	cr_q_block_1[1], 	cr_q_block_1[0]  } ) ;
    assign   cr_q_block_2_out  	= 	( { cr_q_block_2[3], 	cr_q_block_2[2], 	cr_q_block_2[1], 	cr_q_block_2[0]  } ) ;
    assign   cr_q_block_3_out  	= 	( { cr_q_block_3[3], 	cr_q_block_3[2], 	cr_q_block_3[1], 	cr_q_block_3[0]  } ) ;
    assign   cr_q_block_4_out  	= 	( { cr_q_block_4[3], 	cr_q_block_4[2], 	cr_q_block_4[1], 	cr_q_block_4[0]  } ) ;
    assign   cr_A_block_out    	= 	( { cr_A_block	[3], 	cr_A_block	[2],   	cr_A_block	[1],   	cr_A_block	[0]  } ) ;
    assign   cr_B_block_out    	= 	( { cr_B_block	[3], 	cr_B_block	[2],   	cr_B_block	[1],   	cr_B_block	[0]  } ) ;
    assign   cr_C_block_out    	= 	( { cr_C_block	[3], 	cr_C_block	[2],   	cr_C_block	[1],   	cr_C_block	[0]  } ) ;
    assign   cr_D_block_out    	= 	( { cr_D_block	[3], 	cr_D_block	[2],   	cr_D_block	[1],   	cr_D_block	[0]  } ) ;
    assign   cr_E_block_out    	= 	( { cr_E_block	[3], 	cr_E_block	[2],   	cr_E_block	[1],   	cr_E_block	[0]  } ) ;
	assign   cr_J_block_out    	= 	( { cr_J_block	[3], 	cr_J_block	[2],   	cr_J_block	[1],   	cr_J_block	[0]  } ) ;
    assign   cr_K_block_out    	= 	( { cr_K_block	[3], 	cr_K_block	[2],   	cr_K_block	[1],   	cr_K_block	[0]  } ) ;
    assign   cr_M_block_out    	= 	( { cr_M_block	[3], 	cr_M_block	[2],   	cr_M_block	[1],   	cr_M_block	[0]  } ) ;
    assign   cr_N_block_out    	= 	( { cr_N_block	[3], 	cr_N_block	[2],   	cr_N_block	[1],   	cr_N_block	[0]  } ) ;
    assign   cr_G_block_out    	= 	( { cr_G_block	[3], 	cr_G_block	[2],   	cr_G_block	[1],   	cr_G_block	[0]  } ) ;
	assign   cr_H_block_out    	= 	( { cr_H_block	[3], 	cr_H_block	[2],   	cr_H_block	[1],   	cr_H_block	[0]  } ) ;
    assign   cr_I_block_out    	= 	( { cr_I_block	[3], 	cr_I_block	[2],   	cr_I_block	[1],   	cr_I_block	[0]  } ) ;
	
	
	// SAO CB Block filter module for 4x4 block
	sao_block_filter cb_sao_block_filter(
		.clk	(clk),
        .reset	(reset),
		.start	(start_cb_sao_filter),
		.p_0_0_in (cb_p_0_0_in_sao),
		.p_0_1_in (cb_p_0_1_in_sao),
		.p_0_2_in (cb_p_0_2_in_sao),
		.p_0_3_in (cb_p_0_3_in_sao),
		.p_0_4_in (cb_p_0_4_in_sao),
		.p_0_5_in (cb_p_0_5_in_sao),
		.p_1_0_in (cb_p_1_0_in_sao),
		.p_1_1_in (cb_p_1_1_in_sao),
		.p_1_2_in (cb_p_1_2_in_sao),
		.p_1_3_in (cb_p_1_3_in_sao),
		.p_1_4_in (cb_p_1_4_in_sao),
		.p_1_5_in (cb_p_1_5_in_sao),
		.p_2_0_in (cb_p_2_0_in_sao),
		.p_2_1_in (cb_p_2_1_in_sao),
		.p_2_2_in (cb_p_2_2_in_sao),
		.p_2_3_in (cb_p_2_3_in_sao),
		.p_2_4_in (cb_p_2_4_in_sao),
		.p_2_5_in (cb_p_2_5_in_sao),
		.p_3_0_in (cb_p_3_0_in_sao),
		.p_3_1_in (cb_p_3_1_in_sao),
		.p_3_2_in (cb_p_3_2_in_sao),
		.p_3_3_in (cb_p_3_3_in_sao),
		.p_3_4_in (cb_p_3_4_in_sao),
		.p_3_5_in (cb_p_3_5_in_sao),
		.p_4_0_in (cb_p_4_0_in_sao),
		.p_4_1_in (cb_p_4_1_in_sao),
		.p_4_2_in (cb_p_4_2_in_sao),
		.p_4_3_in (cb_p_4_3_in_sao),
		.p_4_4_in (cb_p_4_4_in_sao),
		.p_4_5_in (cb_p_4_5_in_sao),
		.p_5_0_in (cb_p_5_0_in_sao),
		.p_5_1_in (cb_p_5_1_in_sao),
		.p_5_2_in (cb_p_5_2_in_sao),
		.p_5_3_in (cb_p_5_3_in_sao),
		.p_5_4_in (cb_p_5_4_in_sao),
		.p_5_5_in (cb_p_5_5_in_sao),
		.cross_boundary_left_flag_in		(cross_boundary_left_flag_sao		),
		.cross_boundary_right_flag_in		(cross_boundary_right_flag_sao		),
		.cross_boundary_top_flag_in			(cross_boundary_top_flag_sao		),
		.cross_boundary_bottom_flag_in		(cross_boundary_bottom_flag_sao		),
		.cross_boundary_top_left_flag_in	(cross_boundary_top_left_flag_sao	),
		.cross_boundary_top_right_flag_in	(cross_boundary_top_right_flag_sao	),
		.cross_boundary_bottom_left_flag_in	(cross_boundary_bottom_left_flag_sao),
		.cross_boundary_bottom_right_flag_in(cross_boundary_bottom_right_flag_sao),
		.sao_type_in	 (cb_sao_type_in_sao	),
		.sao_eo_class_in (cb_sao_eo_class_in_sao),
		.sao_bandpos_in	 (cb_sao_bandpos_in_sao ),
		.sao_offset_1_in (cb_sao_offset_1_in_sao),
		.sao_offset_2_in (cb_sao_offset_2_in_sao),
		.sao_offset_3_in (cb_sao_offset_3_in_sao),
		.sao_offset_4_in (cb_sao_offset_4_in_sao),
		.pix_1_1_out (cb_pix_1_1_out_sao),
		.pix_1_2_out (cb_pix_1_2_out_sao),
		.pix_1_3_out (cb_pix_1_3_out_sao),
		.pix_1_4_out (cb_pix_1_4_out_sao),
		.pix_2_1_out (cb_pix_2_1_out_sao),
		.pix_2_2_out (cb_pix_2_2_out_sao),
		.pix_2_3_out (cb_pix_2_3_out_sao),
		.pix_2_4_out (cb_pix_2_4_out_sao),
		.pix_3_1_out (cb_pix_3_1_out_sao),
		.pix_3_2_out (cb_pix_3_2_out_sao),
		.pix_3_3_out (cb_pix_3_3_out_sao),
		.pix_3_4_out (cb_pix_3_4_out_sao),
		.pix_4_1_out (cb_pix_4_1_out_sao),
		.pix_4_2_out (cb_pix_4_2_out_sao),
		.pix_4_3_out (cb_pix_4_3_out_sao),
		.pix_4_4_out (cb_pix_4_4_out_sao)
    );
	
	// SAO CR Block filter module for 4x4 block
	sao_block_filter cr_sao_block_filter(
		.clk	(clk),
        .reset	(reset),
		.start	(start_cr_sao_filter),
		.p_0_0_in (cr_p_0_0_in_sao),
		.p_0_1_in (cr_p_0_1_in_sao),
		.p_0_2_in (cr_p_0_2_in_sao),
		.p_0_3_in (cr_p_0_3_in_sao),
		.p_0_4_in (cr_p_0_4_in_sao),
		.p_0_5_in (cr_p_0_5_in_sao),
		.p_1_0_in (cr_p_1_0_in_sao),
		.p_1_1_in (cr_p_1_1_in_sao),
		.p_1_2_in (cr_p_1_2_in_sao),
		.p_1_3_in (cr_p_1_3_in_sao),
		.p_1_4_in (cr_p_1_4_in_sao),
		.p_1_5_in (cr_p_1_5_in_sao),
		.p_2_0_in (cr_p_2_0_in_sao),
		.p_2_1_in (cr_p_2_1_in_sao),
		.p_2_2_in (cr_p_2_2_in_sao),
		.p_2_3_in (cr_p_2_3_in_sao),
		.p_2_4_in (cr_p_2_4_in_sao),
		.p_2_5_in (cr_p_2_5_in_sao),
		.p_3_0_in (cr_p_3_0_in_sao),
		.p_3_1_in (cr_p_3_1_in_sao),
		.p_3_2_in (cr_p_3_2_in_sao),
		.p_3_3_in (cr_p_3_3_in_sao),
		.p_3_4_in (cr_p_3_4_in_sao),
		.p_3_5_in (cr_p_3_5_in_sao),
		.p_4_0_in (cr_p_4_0_in_sao),
		.p_4_1_in (cr_p_4_1_in_sao),
		.p_4_2_in (cr_p_4_2_in_sao),
		.p_4_3_in (cr_p_4_3_in_sao),
		.p_4_4_in (cr_p_4_4_in_sao),
		.p_4_5_in (cr_p_4_5_in_sao),
		.p_5_0_in (cr_p_5_0_in_sao),
		.p_5_1_in (cr_p_5_1_in_sao),
		.p_5_2_in (cr_p_5_2_in_sao),
		.p_5_3_in (cr_p_5_3_in_sao),
		.p_5_4_in (cr_p_5_4_in_sao),
		.p_5_5_in (cr_p_5_5_in_sao),
		.cross_boundary_left_flag_in		(cross_boundary_left_flag_sao		),
		.cross_boundary_right_flag_in		(cross_boundary_right_flag_sao		),
		.cross_boundary_top_flag_in			(cross_boundary_top_flag_sao		),
		.cross_boundary_bottom_flag_in		(cross_boundary_bottom_flag_sao		),
		.cross_boundary_top_left_flag_in	(cross_boundary_top_left_flag_sao	),
		.cross_boundary_top_right_flag_in	(cross_boundary_top_right_flag_sao	),
		.cross_boundary_bottom_left_flag_in	(cross_boundary_bottom_left_flag_sao),
		.cross_boundary_bottom_right_flag_in(cross_boundary_bottom_right_flag_sao),
		.sao_type_in	 (cr_sao_type_in_sao	),
		.sao_eo_class_in (cr_sao_eo_class_in_sao),
		.sao_bandpos_in	 (cr_sao_bandpos_in_sao ),
		.sao_offset_1_in (cr_sao_offset_1_in_sao),
		.sao_offset_2_in (cr_sao_offset_2_in_sao),
		.sao_offset_3_in (cr_sao_offset_3_in_sao),
		.sao_offset_4_in (cr_sao_offset_4_in_sao),
		.pix_1_1_out (cr_pix_1_1_out_sao),
		.pix_1_2_out (cr_pix_1_2_out_sao),
		.pix_1_3_out (cr_pix_1_3_out_sao),
		.pix_1_4_out (cr_pix_1_4_out_sao),
		.pix_2_1_out (cr_pix_2_1_out_sao),
		.pix_2_2_out (cr_pix_2_2_out_sao),
		.pix_2_3_out (cr_pix_2_3_out_sao),
		.pix_2_4_out (cr_pix_2_4_out_sao),
		.pix_3_1_out (cr_pix_3_1_out_sao),
		.pix_3_2_out (cr_pix_3_2_out_sao),
		.pix_3_3_out (cr_pix_3_3_out_sao),
		.pix_3_4_out (cr_pix_3_4_out_sao),
		.pix_4_1_out (cr_pix_4_1_out_sao),
		.pix_4_2_out (cr_pix_4_2_out_sao),
		.pix_4_3_out (cr_pix_4_3_out_sao),
		.pix_4_4_out (cr_pix_4_4_out_sao)
    );
	
	
	// SAO sample Row buffer BRAM
	BRAM_single_port #(
		.ADDR_WIDTH	(9),
		.DATA_WIDTH	(SAMPLE_ROW_BUF_DATA_WIDTH),
		.RAM_DEPTH	(MAX_PIC_WIDTH/8)
	)
	sao_cb_sample_row_buffer(
		.clk        (clk),
		.we_a       (row_we_a      ),
		.en_a       (row_en_a      ),
		.addr_a     (row_addr_a    ),
		.data_in_a  (row_cb_data_in_a ),
		.data_out_a (row_cb_data_out_a)
	);
	
	BRAM_single_port #(
		.ADDR_WIDTH	(9),
		.DATA_WIDTH	(SAMPLE_ROW_BUF_DATA_WIDTH),
		.RAM_DEPTH	(MAX_PIC_WIDTH/8)
	)
	sao_cr_sample_row_buffer(
		.clk        (clk),
		.we_a       (row_we_a      ),
		.en_a       (row_en_a      ),
		.addr_a     (row_addr_a    ),
		.data_in_a  (row_cr_data_in_a ),
		.data_out_a (row_cr_data_out_a)
	);
	
	// SAO sample Column buffer BRAM
	BRAM_dual_port #(
		.ADDR_WIDTH	(9),
		.DATA_WIDTH	(SAMPLE_COL_BUF_DATA_WIDTH),
		.RAM_DEPTH	(MAX_PIC_HEIGHT/4)
	)
	sao_cb_sample_col_buffer(
		.clk        (clk),
		.we_a       (col_we_a      ),
		.we_b       (col_we_b      ),
		.en_a       (col_en_a      ),
		.en_b       (col_en_b      ),
		.addr_a     (col_addr_a    ),
		.addr_b     (col_addr_b    ),
		.data_in_a  (col_cb_data_in_a ),
		.data_in_b  (col_cb_data_in_b ),
		.data_out_a (col_cb_data_out_a),
		.data_out_b (col_cb_data_out_b)
	);
	
	BRAM_dual_port #(
		.ADDR_WIDTH	(9),
		.DATA_WIDTH	(SAMPLE_COL_BUF_DATA_WIDTH),
		.RAM_DEPTH	(MAX_PIC_HEIGHT/4)
	)
	sao_cr_sample_col_buffer(
		.clk        (clk),
		.we_a       (col_we_a      ),
		.we_b       (col_we_b      ),
		.en_a       (col_en_a      ),
		.en_b       (col_en_b      ),
		.addr_a     (col_addr_a    ),
		.addr_b     (col_addr_b    ),
		.data_in_a  (col_cr_data_in_a ),
		.data_in_b  (col_cr_data_in_b ),
		.data_out_a (col_cr_data_out_a),
		.data_out_b (col_cr_data_out_b)
	);
	
	// SAO Decision Row Buffer BRAM
	BRAM_single_port #(
		.ADDR_WIDTH	(9),
		.DATA_WIDTH	(DECISION_ROW_BUF_DATA_WIDTH),
		.RAM_DEPTH	(MAX_PIC_WIDTH/8)
	)
	sao_cb_decision_row_buffer(
		.clk        (clk),
		.we_a       (d_row_buf_we_a      ),
		.en_a       (d_row_buf_en_a      ),
		.addr_a     (d_row_buf_addr_a    ),
		.data_in_a  (d_row_buf_cb_data_in_a ),
		.data_out_a (d_row_buf_cb_data_out_a)
	);
	
	BRAM_single_port #(
		.ADDR_WIDTH	(9),
		.DATA_WIDTH	(DECISION_ROW_BUF_DATA_WIDTH),
		.RAM_DEPTH	(MAX_PIC_WIDTH/8)
	)
	sao_cr_decision_row_buffer(
		.clk        (clk),
		.we_a       (d_row_buf_we_a      ),
		.en_a       (d_row_buf_en_a      ),
		.addr_a     (d_row_buf_addr_a    ),
		.data_in_a  (d_row_buf_cr_data_in_a ),
		.data_out_a (d_row_buf_cr_data_out_a)
	);
	
	// SAO Left Decision Column buffer BRAM
	BRAM_single_port #(
		.ADDR_WIDTH	(9),
		.DATA_WIDTH	(LEFT_DECISION_COL_BUF_DATA_WIDTH),
		.RAM_DEPTH	(MAX_PIC_HEIGHT/8)
	)
	sao_cb_left_decision_col_buffer(
		.clk        (clk),
		.we_a       (d_left_col_buf_we_a      ),
		.en_a       (d_left_col_buf_en_a      ),
		.addr_a     (d_left_col_buf_addr_a    ),
		.data_in_a  (d_left_col_buf_cb_data_in_a ),
		.data_out_a (d_left_col_buf_cb_data_out_a)
	);
	
	BRAM_single_port #(
		.ADDR_WIDTH	(9),
		.DATA_WIDTH	(LEFT_DECISION_COL_BUF_DATA_WIDTH),
		.RAM_DEPTH	(MAX_PIC_HEIGHT/8)
	)
	sao_cr_left_decision_col_buffer(
		.clk        (clk),
		.we_a       (d_left_col_buf_we_a      ),
		.en_a       (d_left_col_buf_en_a      ),
		.addr_a     (d_left_col_buf_addr_a    ),
		.data_in_a  (d_left_col_buf_cr_data_in_a ),
		.data_out_a (d_left_col_buf_cr_data_out_a)
	);
	
	// SAO Right Decision Column buffer BRAM - This is actually horizontal(Xc addressed)
	BRAM_single_port #(
		.ADDR_WIDTH	(9),
		.DATA_WIDTH	(RIGHT_DECISION_COL_BUF_DATA_WIDTH),
		.RAM_DEPTH	(MAX_PIC_WIDTH/8)
	)
	sao_cb_right_decision_col_buffer(
		.clk        (clk),
		.we_a       (d_right_col_buf_we_a      ),
		.en_a       (d_right_col_buf_en_a      ),
		.addr_a     (d_right_col_buf_addr_a    ),
		.data_in_a  (d_right_col_buf_cb_data_in_a ),
		.data_out_a (d_right_col_buf_cb_data_out_a)
	);
	
	BRAM_single_port #(
		.ADDR_WIDTH	(9),
		.DATA_WIDTH	(RIGHT_DECISION_COL_BUF_DATA_WIDTH),
		.RAM_DEPTH	(MAX_PIC_WIDTH/8)
	)
	sao_cr_right_decision_col_buffer(
		.clk        (clk),
		.we_a       (d_right_col_buf_we_a      ),
		.en_a       (d_right_col_buf_en_a      ),
		.addr_a     (d_right_col_buf_addr_a    ),
		.data_in_a  (d_right_col_buf_cr_data_in_a ),
		.data_out_a (d_right_col_buf_cr_data_out_a)
	);
	
	
	// X8,Y8 calculation for other SAO Blocks
	assign	J_block_X8	=	Xc_in[XC_WIDTH-1:3] - 1'b1 ;		// Do not use for decision making(use Xc_in, Yc_in)
	assign	J_block_Y8	=	Yc_in[XC_WIDTH-1:3] - 1'b1 ;		// Only for comparison purposes, can be negative
	assign	H_block_X8	=	Xc_in[XC_WIDTH-1:3] - 1'b1 ;
	assign	H_block_Y8	=	Yc_in[XC_WIDTH-1:3];
	assign	M_block_X8	=	Xc_in[XC_WIDTH-1:3];
	assign	M_block_Y8	=	Yc_in[XC_WIDTH-1:3] - 1'b1 ;
	assign	J_block_TR_X8	=	Xc_in[XC_WIDTH-1:3] ;
	assign	J_block_TR_Y8	=	Yc_in[XC_WIDTH-1:3] - 2'd2 ;
	assign	J_block_BL_X8	=	Xc_in[XC_WIDTH-1:3] - 2'd2 ;
	assign	J_block_BL_Y8	=	Xc_in[XC_WIDTH-1:3] ;
	
	
	//------------- Main state machine for 8x8 chroma sao filtering -------------------------
	
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			state <= STATE_INIT;
            done  <= 1'b0;
		end
		else begin
			case(state)
				STATE_INIT : begin
					if(start) begin
						// Assigning input samples to registers
						cb_q_block_1[0]  	<=	  cb_q_block_1_in [8*0 + 7 : 8*0] ;
                        cb_q_block_2[0]  	<=	  cb_q_block_2_in [8*0 + 7 : 8*0] ;
                        cb_q_block_3[0]  	<=	  cb_q_block_3_in [8*0 + 7 : 8*0] ;
                        cb_q_block_4[0]  	<=	  cb_q_block_4_in [8*0 + 7 : 8*0] ;
                        cb_A_block  [0]  	<=	  cb_A_block_in   [8*0 + 7 : 8*0] ;
                        cb_B_block  [0]  	<=	  cb_B_block_in   [8*0 + 7 : 8*0] ;
						cb_C_block	[0]  	<=	  cb_C_block_in   [8*0 + 7 : 8*0] ;
						cb_D_block  [0]  	<=	  cb_D_block_in   [8*0 + 7 : 8*0] ;
						cb_E_block  [0]  	<=	  cb_E_block_in   [8*0 + 7 : 8*0] ;
						
						cb_q_block_1[1]  	<=	  cb_q_block_1_in [8*1 + 7 : 8*1] ;
                        cb_q_block_2[1]  	<=	  cb_q_block_2_in [8*1 + 7 : 8*1] ;
                        cb_q_block_3[1]  	<=	  cb_q_block_3_in [8*1 + 7 : 8*1] ;
                        cb_q_block_4[1]  	<=	  cb_q_block_4_in [8*1 + 7 : 8*1] ;
                        cb_A_block  [1]  	<=	  cb_A_block_in   [8*1 + 7 : 8*1] ;
                        cb_B_block  [1]  	<=	  cb_B_block_in   [8*1 + 7 : 8*1] ;
						cb_C_block	[1]  	<=	  cb_C_block_in   [8*1 + 7 : 8*1] ;
						cb_D_block  [1]  	<=	  cb_D_block_in   [8*1 + 7 : 8*1] ;
						cb_E_block  [1]  	<=	  cb_E_block_in   [8*1 + 7 : 8*1] ;
						
						cb_q_block_1[2]  	<=	  cb_q_block_1_in [8*2 + 7 : 8*2] ;
                        cb_q_block_2[2]  	<=	  cb_q_block_2_in [8*2 + 7 : 8*2] ;
                        cb_q_block_3[2]  	<=	  cb_q_block_3_in [8*2 + 7 : 8*2] ;
                        cb_q_block_4[2]  	<=	  cb_q_block_4_in [8*2 + 7 : 8*2] ;
                        cb_A_block  [2]  	<=	  cb_A_block_in   [8*2 + 7 : 8*2] ;
                        cb_B_block  [2]  	<=	  cb_B_block_in   [8*2 + 7 : 8*2] ;
						cb_C_block	[2]  	<=	  cb_C_block_in   [8*2 + 7 : 8*2] ;
						cb_D_block  [2]  	<=	  cb_D_block_in   [8*2 + 7 : 8*2] ;
						cb_E_block  [2]  	<=	  cb_E_block_in   [8*2 + 7 : 8*2] ;
						
						cb_q_block_1[3]  	<=	  cb_q_block_1_in [8*3 + 7 : 8*3] ;
                        cb_q_block_2[3]  	<=	  cb_q_block_2_in [8*3 + 7 : 8*3] ;
                        cb_q_block_3[3]  	<=	  cb_q_block_3_in [8*3 + 7 : 8*3] ;
                        cb_q_block_4[3]  	<=	  cb_q_block_4_in [8*3 + 7 : 8*3] ;
                        cb_A_block  [3]  	<=	  cb_A_block_in   [8*3 + 7 : 8*3] ;
                        cb_B_block  [3]  	<=	  cb_B_block_in   [8*3 + 7 : 8*3] ;
						cb_C_block	[3]  	<=	  cb_C_block_in   [8*3 + 7 : 8*3] ;
						cb_D_block  [3]  	<=	  cb_D_block_in   [8*3 + 7 : 8*3] ;
						cb_E_block  [3]  	<=	  cb_E_block_in   [8*3 + 7 : 8*3] ;
						
						cr_q_block_1[0]  	<=	  cr_q_block_1_in [8*0 + 7 : 8*0] ;
                        cr_q_block_2[0]  	<=	  cr_q_block_2_in [8*0 + 7 : 8*0] ;
                        cr_q_block_3[0]  	<=	  cr_q_block_3_in [8*0 + 7 : 8*0] ;
                        cr_q_block_4[0]  	<=	  cr_q_block_4_in [8*0 + 7 : 8*0] ;
                        cr_A_block  [0]  	<=	  cr_A_block_in   [8*0 + 7 : 8*0] ;
                        cr_B_block  [0]  	<=	  cr_B_block_in   [8*0 + 7 : 8*0] ;
						cr_C_block	[0]  	<=	  cr_C_block_in   [8*0 + 7 : 8*0] ;
						cr_D_block  [0]  	<=	  cr_D_block_in   [8*0 + 7 : 8*0] ;
						cr_E_block  [0]  	<=	  cr_E_block_in   [8*0 + 7 : 8*0] ;
						
						cr_q_block_1[1]  	<=	  cr_q_block_1_in [8*1 + 7 : 8*1] ;
                        cr_q_block_2[1]  	<=	  cr_q_block_2_in [8*1 + 7 : 8*1] ;
                        cr_q_block_3[1]  	<=	  cr_q_block_3_in [8*1 + 7 : 8*1] ;
                        cr_q_block_4[1]  	<=	  cr_q_block_4_in [8*1 + 7 : 8*1] ;
                        cr_A_block  [1]  	<=	  cr_A_block_in   [8*1 + 7 : 8*1] ;
                        cr_B_block  [1]  	<=	  cr_B_block_in   [8*1 + 7 : 8*1] ;
						cr_C_block	[1]  	<=	  cr_C_block_in   [8*1 + 7 : 8*1] ;
						cr_D_block  [1]  	<=	  cr_D_block_in   [8*1 + 7 : 8*1] ;
						cr_E_block  [1]  	<=	  cr_E_block_in   [8*1 + 7 : 8*1] ;
						
						cr_q_block_1[2]  	<=	  cr_q_block_1_in [8*2 + 7 : 8*2] ;
                        cr_q_block_2[2]  	<=	  cr_q_block_2_in [8*2 + 7 : 8*2] ;
                        cr_q_block_3[2]  	<=	  cr_q_block_3_in [8*2 + 7 : 8*2] ;
                        cr_q_block_4[2]  	<=	  cr_q_block_4_in [8*2 + 7 : 8*2] ;
                        cr_A_block  [2]  	<=	  cr_A_block_in   [8*2 + 7 : 8*2] ;
                        cr_B_block  [2]  	<=	  cr_B_block_in   [8*2 + 7 : 8*2] ;
						cr_C_block	[2]  	<=	  cr_C_block_in   [8*2 + 7 : 8*2] ;
						cr_D_block  [2]  	<=	  cr_D_block_in   [8*2 + 7 : 8*2] ;
						cr_E_block  [2]  	<=	  cr_E_block_in   [8*2 + 7 : 8*2] ;
						
						cr_q_block_1[3]  	<=	  cr_q_block_1_in [8*3 + 7 : 8*3] ;
                        cr_q_block_2[3]  	<=	  cr_q_block_2_in [8*3 + 7 : 8*3] ;
                        cr_q_block_3[3]  	<=	  cr_q_block_3_in [8*3 + 7 : 8*3] ;
                        cr_q_block_4[3]  	<=	  cr_q_block_4_in [8*3 + 7 : 8*3] ;
                        cr_A_block  [3]  	<=	  cr_A_block_in   [8*3 + 7 : 8*3] ;
                        cr_B_block  [3]  	<=	  cr_B_block_in   [8*3 + 7 : 8*3] ;
						cr_C_block	[3]  	<=	  cr_C_block_in   [8*3 + 7 : 8*3] ;
						cr_D_block  [3]  	<=	  cr_D_block_in   [8*3 + 7 : 8*3] ;
						cr_E_block  [3]  	<=	  cr_E_block_in   [8*3 + 7 : 8*3] ;
						
						done  <= 1'b0;
						state <= STATE_1;
					end
				end
				STATE_1 : begin
					// Check for J,K blocks reading in combinational block	- ATTN -
					// Check for G,H block reading in combinational block		- ATTN -
					// Check for decision buffers reading in combinational block
					state <= STATE_2;
				end
				STATE_2 : begin
					// Assign to G block from BRAM
					if((Xc_in>0) && (Yc_in>0)) begin
						cb_G_block[0]    <=    col_cb_data_out_a [8*0  + 7 : 8*0 ] ;
                        cb_G_block[1]    <=    col_cb_data_out_a [8*1  + 7 : 8*1 ] ;
                        cb_G_block[2]    <=    col_cb_data_out_a [8*2  + 7 : 8*2 ] ;
                        cb_G_block[3]    <=    col_cb_data_out_a [8*3  + 7 : 8*3 ] ;
                        cr_G_block[0]    <=    col_cr_data_out_a [8*0  + 7 : 8*0 ] ;
                        cr_G_block[1]    <=    col_cr_data_out_a [8*1  + 7 : 8*1 ] ;
                        cr_G_block[2]    <=    col_cr_data_out_a [8*2  + 7 : 8*2 ] ;
                        cr_G_block[3]    <=    col_cr_data_out_a [8*3  + 7 : 8*3 ] ;
					end
					// Assign to H block from BRAM
					if(Xc_in>0) begin
						cb_H_block[0]    <=    col_cb_data_out_b [8*0  + 7 : 8*0 ] ;
                        cb_H_block[1]    <=    col_cb_data_out_b [8*1  + 7 : 8*1 ] ;
                        cb_H_block[2]    <=    col_cb_data_out_b [8*2  + 7 : 8*2 ] ;
                        cb_H_block[3]    <=    col_cb_data_out_b [8*3  + 7 : 8*3 ] ;
                        cr_H_block[0]    <=    col_cr_data_out_b [8*0  + 7 : 8*0 ] ;
                        cr_H_block[1]    <=    col_cr_data_out_b [8*1  + 7 : 8*1 ] ;
                        cr_H_block[2]    <=    col_cr_data_out_b [8*2  + 7 : 8*2 ] ;
                        cr_H_block[3]    <=    col_cr_data_out_b [8*3  + 7 : 8*3 ] ;
						H_block_cb_sao_type     <=	col_cb_data_out_b [8*4 + 1  : 8*4 ]  ;
						H_block_cb_eoclass      <=	col_cb_data_out_b [8*4 + 3  : 8*4 + 2]  ;
						H_block_cb_bandpos      <=	col_cb_data_out_b [8*4 + 8  : 8*4 + 4]  ;
						H_block_cb_sao_offset_1 <=	col_cb_data_out_b [8*4 + 12 : 8*4 + 9]  ;
						H_block_cb_sao_offset_2 <=	col_cb_data_out_b [8*4 + 16 : 8*4 + 13] ;
						H_block_cb_sao_offset_3 <=	col_cb_data_out_b [8*4 + 20 : 8*4 + 17] ;
						H_block_cb_sao_offset_4 <=	col_cb_data_out_b [8*4 + 24 : 8*4 + 21] ;
						H_block_sao_chroma		<=	col_cb_data_out_b [8*4 + 25] ;
						H_block_tile_left_flag	<=	col_cb_data_out_b [8*4 + 26] ;
						H_block_tile_top_flag   <=	col_cb_data_out_b [8*4 + 27] ;
						H_block_slice_left_flag <=	col_cb_data_out_b [8*4 + 28] ;
						H_block_slice_top_flag  <=	col_cb_data_out_b [8*4 + 29] ;
						H_block_sao_across_slices_enable_flag	<=	col_cb_data_out_b [8*4 + 30] ;
						H_block_pcm_bypass_sao_disable_flag		<=	col_cb_data_out_b [8*4 + 31] ;
						// unnecessary memory allocation for cr BRAM, 16 bits of common parameters 		- ATTN -
						H_block_cr_sao_type     <=	col_cr_data_out_b [8*4 + 1  : 8*4 ]  ;
						H_block_cr_eoclass      <=	col_cr_data_out_b [8*4 + 3  : 8*4 + 2]  ;
						H_block_cr_bandpos      <=	col_cr_data_out_b [8*4 + 8  : 8*4 + 4]  ;
						H_block_cr_sao_offset_1 <=	col_cr_data_out_b [8*4 + 12 : 8*4 + 9]  ;
						H_block_cr_sao_offset_2 <=	col_cr_data_out_b [8*4 + 16 : 8*4 + 13] ;
						H_block_cr_sao_offset_3 <=	col_cr_data_out_b [8*4 + 20 : 8*4 + 17] ;
						H_block_cr_sao_offset_4 <=	col_cr_data_out_b [8*4 + 24 : 8*4 + 21] ;
						H_block_slice_id  		<=	col_cr_data_out_b [8*4 + 32 : 8*4 + 25] ;
					end
					// Assign to J,K blocks from BRAM
					if((Xc_in>0) && (Yc_in>0)) begin
						cb_J_block[0]    <=    row_cb_data_out_a [8*0  + 7 : 8*0 ] ;
                        cb_J_block[1]    <=    row_cb_data_out_a [8*1  + 7 : 8*1 ] ;
                        cb_J_block[2]    <=    row_cb_data_out_a [8*2  + 7 : 8*2 ] ;
                        cb_J_block[3]    <=    row_cb_data_out_a [8*3  + 7 : 8*3 ] ;
						cb_K_block[0]    <=    row_cb_data_out_a [8*4  + 7 : 8*4 ] ;
                        cb_K_block[1]    <=    row_cb_data_out_a [8*5  + 7 : 8*5 ] ;
                        cb_K_block[2]    <=    row_cb_data_out_a [8*6  + 7 : 8*6 ] ;
                        cb_K_block[3]    <=    row_cb_data_out_a [8*7  + 7 : 8*7 ] ;
                        cr_J_block[0]    <=    row_cr_data_out_a [8*0  + 7 : 8*0 ] ;
                        cr_J_block[1]    <=    row_cr_data_out_a [8*1  + 7 : 8*1 ] ;
                        cr_J_block[2]    <=    row_cr_data_out_a [8*2  + 7 : 8*2 ] ;
                        cr_J_block[3]    <=    row_cr_data_out_a [8*3  + 7 : 8*3 ] ;
						cr_K_block[0]    <=    row_cr_data_out_a [8*4  + 7 : 8*4 ] ;
                        cr_K_block[1]    <=    row_cr_data_out_a [8*5  + 7 : 8*5 ] ;
                        cr_K_block[2]    <=    row_cr_data_out_a [8*6  + 7 : 8*6 ] ;
                        cr_K_block[3]    <=    row_cr_data_out_a [8*7  + 7 : 8*7 ] ;
						J_block_cb_sao_type     <=	row_cb_data_out_a [8*8 + 1  : 8*8 ]  ;
						J_block_cb_eoclass      <=	row_cb_data_out_a [8*8 + 3  : 8*8 + 2]  ;
						J_block_cb_bandpos      <=	row_cb_data_out_a [8*8 + 8  : 8*8 + 4]  ;
						J_block_cb_sao_offset_1 <=	row_cb_data_out_a [8*8 + 12 : 8*8 + 9]  ;
						J_block_cb_sao_offset_2 <=	row_cb_data_out_a [8*8 + 16 : 8*8 + 13] ;
						J_block_cb_sao_offset_3 <=	row_cb_data_out_a [8*8 + 20 : 8*8 + 17] ;
						J_block_cb_sao_offset_4 <=	row_cb_data_out_a [8*8 + 24 : 8*8 + 21] ;
						J_block_sao_chroma		<=	row_cb_data_out_a [8*8 + 25] ;
						J_block_tile_left_flag	<=	row_cb_data_out_a [8*8 + 26] ;
						J_block_tile_top_flag   <=	row_cb_data_out_a [8*8 + 27] ;
						J_block_slice_left_flag <=	row_cb_data_out_a [8*8 + 28] ;
						J_block_slice_top_flag  <=	row_cb_data_out_a [8*8 + 29] ;
						J_block_sao_across_slices_enable_flag	<=	row_cb_data_out_a [8*8 + 30] ;
						J_block_pcm_bypass_sao_disable_flag		<=	row_cb_data_out_a [8*8 + 31] ;
						// unnecessary memory allocation for cr BRAM, 16 bits of common parameters 		- ATTN -
						J_block_cr_sao_type     <=	row_cr_data_out_a [8*8 + 1  : 8*8 ]  ;
						J_block_cr_eoclass      <=	row_cr_data_out_a [8*8 + 3  : 8*8 + 2]  ;
						J_block_cr_bandpos      <=	row_cr_data_out_a [8*8 + 8  : 8*8 + 4]  ;
						J_block_cr_sao_offset_1 <=	row_cr_data_out_a [8*8 + 12 : 8*8 + 9]  ;
						J_block_cr_sao_offset_2 <=	row_cr_data_out_a [8*8 + 16 : 8*8 + 13] ;
						J_block_cr_sao_offset_3 <=	row_cr_data_out_a [8*8 + 20 : 8*8 + 17] ;
						J_block_cr_sao_offset_4 <=	row_cr_data_out_a [8*8 + 24 : 8*8 + 21] ;
						J_block_slice_id  		<=	row_cr_data_out_a [8*8 + 32 : 8*8 + 25] ;
					end
					// Assign decision samples from BRAM to registers
					if((Xc_in>0) && (Yc_in>8)) begin
						cb_decision_row[0]	<=	d_row_buf_cb_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						cb_decision_row[1]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        cb_decision_row[2]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        cb_decision_row[3]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        cb_decision_row[4]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        cb_decision_row[5]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*6  - 1 : BIT_DEPTH*5 ] ;
                        cr_decision_row[0]	<=	d_row_buf_cr_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						cr_decision_row[1]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        cr_decision_row[2]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        cr_decision_row[3]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        cr_decision_row[4]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        cr_decision_row[5]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*6  - 1 : BIT_DEPTH*5 ] ;
						decision_top_left_slice_boundary  <=  d_row_buf_cb_data_out_a [BIT_DEPTH*6] ;
						decision_top_right_slice_id[3:0]  <=  d_row_buf_cb_data_out_a [BIT_DEPTH*6 + 4 : BIT_DEPTH*6 + 1 ] ;
						decision_top_right_sao_across_slices_enable  <=  d_row_buf_cr_data_out_a [BIT_DEPTH*6] ;
						decision_top_right_slice_id[7:4]  <=  d_row_buf_cr_data_out_a [BIT_DEPTH*6 + 4 : BIT_DEPTH*6 + 1 ] ;
					end
					if((Xc_in>8) && (Yc_in>0)) begin
						cb_decision_col_left[0]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						cb_decision_col_left[1]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        cb_decision_col_left[2]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        cb_decision_col_left[3]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        cb_decision_col_left[4]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        cr_decision_col_left[0]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						cr_decision_col_left[1]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        cr_decision_col_left[2]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        cr_decision_col_left[3]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        cr_decision_col_left[4]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
						decision_bottom_left_sao_across_slices_enable  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*5] ;
						decision_bottom_left_slice_id[3:0]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*5 + 4 : BIT_DEPTH*5 + 1 ] ;
						decision_bottom_left_slice_id[7:4]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*5 + 3 : BIT_DEPTH*5 ] ;
					end
					if((Xc_in>0) && (Yc_in>0)) begin
						cb_decision_col_right[0]  <=  d_right_col_buf_cb_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						cb_decision_col_right[1]  <=  d_right_col_buf_cb_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        cr_decision_col_right[0]  <=  d_right_col_buf_cr_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						cr_decision_col_right[1]  <=  d_right_col_buf_cr_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
						M_block_sao_across_slices_enable_flag	<=	d_right_col_buf_cb_data_out_a [BIT_DEPTH*2] ;
						M_block_slice_left_flag <=	d_right_col_buf_cr_data_out_a [BIT_DEPTH*2] ;
						M_block_slice_id[3:0]	<=	d_right_col_buf_cb_data_out_a [BIT_DEPTH*2 + 4 : BIT_DEPTH*2 + 1 ] ;
						M_block_slice_id[7:4]	<=	d_right_col_buf_cr_data_out_a [BIT_DEPTH*2 + 4 : BIT_DEPTH*2 + 1 ] ;
					end
					// Check for M,N blocks reading in combinational block	- ATTN -
					// Check for I block reading in combinational block		- ATTN -
					
					state <= STATE_3;
				end
				STATE_3 : begin
					// Write H,A blocks to row buffer in combinational block  - ATTN -
					// Write 1,D blocks to col buffer in combinational block  - ATTN -
					// Memory should be READ BEFORE WRITE  - ATTN -
					
					// Assign to I block from BRAM
					if((Xc_in>0) && (Yc_in == (pic_height_in - 8))) begin
						cb_I_block[0]    <=    col_cb_data_out_a [8*0  + 7 : 8*0 ] ;
                        cb_I_block[1]    <=    col_cb_data_out_a [8*1  + 7 : 8*1 ] ;
                        cb_I_block[2]    <=    col_cb_data_out_a [8*2  + 7 : 8*2 ] ;
                        cb_I_block[3]    <=    col_cb_data_out_a [8*3  + 7 : 8*3 ] ;
                        cr_I_block[0]    <=    col_cr_data_out_a [8*0  + 7 : 8*0 ] ;
                        cr_I_block[1]    <=    col_cr_data_out_a [8*1  + 7 : 8*1 ] ;
                        cr_I_block[2]    <=    col_cr_data_out_a [8*2  + 7 : 8*2 ] ;
                        cr_I_block[3]    <=    col_cr_data_out_a [8*3  + 7 : 8*3 ] ;
					end
					// Assign to M,N blocks from BRAM
					if((Xc_in == (pic_width_in - 8)) && (Yc_in>0)) begin
						cb_M_block[0]    <=    row_cb_data_out_a [8*0  + 7 : 8*0 ] ;
                        cb_M_block[1]    <=    row_cb_data_out_a [8*1  + 7 : 8*1 ] ;
                        cb_M_block[2]    <=    row_cb_data_out_a [8*2  + 7 : 8*2 ] ;
                        cb_M_block[3]    <=    row_cb_data_out_a [8*3  + 7 : 8*3 ] ;
						cb_N_block[0]    <=    row_cb_data_out_a [8*4  + 7 : 8*4 ] ;
                        cb_N_block[1]    <=    row_cb_data_out_a [8*5  + 7 : 8*5 ] ;
                        cb_N_block[2]    <=    row_cb_data_out_a [8*6  + 7 : 8*6 ] ;
                        cb_N_block[3]    <=    row_cb_data_out_a [8*7  + 7 : 8*7 ] ;
                        cr_M_block[0]    <=    row_cr_data_out_a [8*0  + 7 : 8*0 ] ;
                        cr_M_block[1]    <=    row_cr_data_out_a [8*1  + 7 : 8*1 ] ;
                        cr_M_block[2]    <=    row_cr_data_out_a [8*2  + 7 : 8*2 ] ;
                        cr_M_block[3]    <=    row_cr_data_out_a [8*3  + 7 : 8*3 ] ;
                        cr_N_block[0]    <=    row_cr_data_out_a [8*4  + 7 : 8*4 ] ;
                        cr_N_block[1]    <=    row_cr_data_out_a [8*5  + 7 : 8*5 ] ;
                        cr_N_block[2]    <=    row_cr_data_out_a [8*6  + 7 : 8*6 ] ;
                        cr_N_block[3]    <=    row_cr_data_out_a [8*7  + 7 : 8*7 ] ;
						M_block_cb_sao_type     <=	row_cb_data_out_a [8*8 + 1  : 8*8 ]  ;
						M_block_cb_eoclass      <=	row_cb_data_out_a [8*8 + 3  : 8*8 + 2]  ;
						M_block_cb_bandpos      <=	row_cb_data_out_a [8*8 + 8  : 8*8 + 4]  ;
						M_block_cb_sao_offset_1 <=	row_cb_data_out_a [8*8 + 12 : 8*8 + 9]  ;
						M_block_cb_sao_offset_2 <=	row_cb_data_out_a [8*8 + 16 : 8*8 + 13] ;
						M_block_cb_sao_offset_3 <=	row_cb_data_out_a [8*8 + 20 : 8*8 + 17] ;
						M_block_cb_sao_offset_4 <=	row_cb_data_out_a [8*8 + 24 : 8*8 + 21] ;
						M_block_sao_chroma		<=	row_cb_data_out_a [8*8 + 25] ;
						M_block_tile_left_flag	<=	row_cb_data_out_a [8*8 + 26] ;
						M_block_tile_top_flag   <=	row_cb_data_out_a [8*8 + 27] ;
						M_block_slice_left_flag <=	row_cb_data_out_a [8*8 + 28] ;
						M_block_slice_top_flag  <=	row_cb_data_out_a [8*8 + 29] ;
						M_block_sao_across_slices_enable_flag	<=	row_cb_data_out_a [8*8 + 30] ;
						M_block_pcm_bypass_sao_disable_flag		<=	row_cb_data_out_a [8*8 + 31] ;
						// unnecessary memory allocation for cr BRAM, 16 bits of common parameters 		- ATTN -
						M_block_cr_sao_type     <=	row_cr_data_out_a [8*8 + 1  : 8*8 ]  ;
						M_block_cr_eoclass      <=	row_cr_data_out_a [8*8 + 3  : 8*8 + 2]  ;
						M_block_cr_bandpos      <=	row_cr_data_out_a [8*8 + 8  : 8*8 + 4]  ;
						M_block_cr_sao_offset_1 <=	row_cr_data_out_a [8*8 + 12 : 8*8 + 9]  ;
						M_block_cr_sao_offset_2 <=	row_cr_data_out_a [8*8 + 16 : 8*8 + 13] ;
						M_block_cr_sao_offset_3 <=	row_cr_data_out_a [8*8 + 20 : 8*8 + 17] ;
						M_block_cr_sao_offset_4 <=	row_cr_data_out_a [8*8 + 24 : 8*8 + 21] ;
						M_block_slice_id  		<=	row_cr_data_out_a [8*8 + 32 : 8*8 + 25] ;
					end
					state <= STATE_4;
				end
				STATE_4 : begin
					// Write 1,2 blocks to row buffer if necessary in combinational block  - ATTN -
					// Write 3 block to col buffer if necessary in combinational block  - ATTN -
					// Memory should be READ BEFORE WRITE  - ATTN -
					// Write new decision samples to decision BRAMs
					if((Xc_in>0) && (Yc_in>0)) begin
						state <= STATE_5;
					end
					else begin
						state <= STATE_INIT;
						done  <= 1'b1;
					end
				end
				STATE_5 : begin
					state <= STATE_6;
				end
				STATE_6 : begin
					// Assign decision samples from BRAM to registers for Right block
					if((Xc_in==(pic_width_in - 8)) && (Yc_in>8)) begin
						cb_decision_row[0]	<=	d_row_buf_cb_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						cb_decision_row[1]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        cb_decision_row[2]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        cb_decision_row[3]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        cb_decision_row[4]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        cb_decision_row[5]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*6  - 1 : BIT_DEPTH*5 ] ;
                        cr_decision_row[0]	<=	d_row_buf_cr_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						cr_decision_row[1]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        cr_decision_row[2]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        cr_decision_row[3]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        cr_decision_row[4]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        cr_decision_row[5]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*6  - 1 : BIT_DEPTH*5 ] ;
						decision_top_left_slice_boundary  <=  d_row_buf_cb_data_out_a [BIT_DEPTH*6] ;
						decision_top_right_slice_id[3:0]  <=  d_row_buf_cb_data_out_a [BIT_DEPTH*6 + 4 : BIT_DEPTH*6 + 1 ] ;
						decision_top_right_sao_across_slices_enable  <=  d_row_buf_cr_data_out_a [BIT_DEPTH*6] ;
						decision_top_right_slice_id[7:4]  <=  d_row_buf_cr_data_out_a [BIT_DEPTH*6 + 4 : BIT_DEPTH*6 + 1 ] ;
					end
					if((Xc_in==(pic_width_in - 8)) && (Yc_in>0)) begin
						cb_decision_col_left[0]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						cb_decision_col_left[1]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        cb_decision_col_left[2]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        cb_decision_col_left[3]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        cb_decision_col_left[4]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        cr_decision_col_left[0]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						cr_decision_col_left[1]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        cr_decision_col_left[2]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        cr_decision_col_left[3]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        cr_decision_col_left[4]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
						decision_bottom_left_sao_across_slices_enable  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*5] ;
						decision_bottom_left_slice_id[3:0]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*5 + 4 : BIT_DEPTH*5 + 1 ] ;
						decision_bottom_left_slice_id[7:4]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*5 + 3 : BIT_DEPTH*5 ] ;
					end
					state <= STATE_7;
				end
				STATE_7 : begin
					if(J_block_cb_sao_apply) begin
						cb_J_block[0]    <=    cb_pix_1_1_out_sao ;
                        cb_J_block[1]    <=    cb_pix_1_2_out_sao ;
						cb_K_block[0]    <=    cb_pix_1_3_out_sao ;
                        cb_K_block[1]    <=    cb_pix_1_4_out_sao ;
                        cb_J_block[2]    <=    cb_pix_2_1_out_sao ;
                        cb_J_block[3]    <=    cb_pix_2_2_out_sao ;
                        cb_K_block[2]    <=    cb_pix_2_3_out_sao ;
                        cb_K_block[3]    <=    cb_pix_2_4_out_sao ;
						cb_G_block[0]    <=    cb_pix_3_1_out_sao ;
                        cb_G_block[1]    <=    cb_pix_3_2_out_sao ;
						cb_C_block[0]    <=    cb_pix_3_3_out_sao ;
                        cb_C_block[1]    <=    cb_pix_3_4_out_sao ;
                        cb_G_block[2]    <=    cb_pix_4_1_out_sao ;
                        cb_G_block[3]    <=    cb_pix_4_2_out_sao ;
                        cb_C_block[2]    <=    cb_pix_4_3_out_sao ;
                        cb_C_block[3]    <=    cb_pix_4_4_out_sao ;
					end
					if(J_block_cr_sao_apply) begin
						cr_J_block[0]    <=    cr_pix_1_1_out_sao ;
                        cr_J_block[1]    <=    cr_pix_1_2_out_sao ;
						cr_K_block[0]    <=    cr_pix_1_3_out_sao ;
                        cr_K_block[1]    <=    cr_pix_1_4_out_sao ;
                        cr_J_block[2]    <=    cr_pix_2_1_out_sao ;
                        cr_J_block[3]    <=    cr_pix_2_2_out_sao ;
                        cr_K_block[2]    <=    cr_pix_2_3_out_sao ;
                        cr_K_block[3]    <=    cr_pix_2_4_out_sao ;
						cr_G_block[0]    <=    cr_pix_3_1_out_sao ;
                        cr_G_block[1]    <=    cr_pix_3_2_out_sao ;
						cr_C_block[0]    <=    cr_pix_3_3_out_sao ;
                        cr_C_block[1]    <=    cr_pix_3_4_out_sao ;
                        cr_G_block[2]    <=    cr_pix_4_1_out_sao ;
                        cr_G_block[3]    <=    cr_pix_4_2_out_sao ;
                        cr_C_block[2]    <=    cr_pix_4_3_out_sao ;
                        cr_C_block[3]    <=    cr_pix_4_4_out_sao ;
					end
					// Check for Right or Bottom block SAO conditions
					if(Xc_in == (pic_width_in - 8)) begin
						state <= STATE_8;
					end
					else if(Yc_in == (pic_height_in - 8)) begin
						state <= STATE_8;							// jump to bottom block
					end
					else begin
						state <= STATE_INIT;
						done  <= 1'b1;
					end
				end
				STATE_8 : begin
					// Assign decision samples from BRAM to registers for Bottom block
					if((Xc_in>0) && (Yc_in==(pic_height_in - 8))) begin
						cb_decision_row[0]	<=	d_row_buf_cb_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						cb_decision_row[1]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        cb_decision_row[2]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        cb_decision_row[3]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        cb_decision_row[4]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        cb_decision_row[5]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*6  - 1 : BIT_DEPTH*5 ] ;
                        cr_decision_row[0]	<=	d_row_buf_cr_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						cr_decision_row[1]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        cr_decision_row[2]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        cr_decision_row[3]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        cr_decision_row[4]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        cr_decision_row[5]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*6  - 1 : BIT_DEPTH*5 ] ;
						decision_top_left_slice_boundary  <=  d_row_buf_cb_data_out_a [BIT_DEPTH*6] ;
						decision_top_right_slice_id[3:0]  <=  d_row_buf_cb_data_out_a [BIT_DEPTH*6 + 4 : BIT_DEPTH*6 + 1 ] ;
						decision_top_right_sao_across_slices_enable  <=  d_row_buf_cr_data_out_a [BIT_DEPTH*6] ;
						decision_top_right_slice_id[7:4]  <=  d_row_buf_cr_data_out_a [BIT_DEPTH*6 + 4 : BIT_DEPTH*6 + 1 ] ;
					end
					if((Xc_in>8) && (Yc_in==(pic_height_in - 8))) begin
						cb_decision_col_left[0]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						cb_decision_col_left[1]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        cb_decision_col_left[2]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        cb_decision_col_left[3]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        cb_decision_col_left[4]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        cr_decision_col_left[0]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						cr_decision_col_left[1]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        cr_decision_col_left[2]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        cr_decision_col_left[3]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        cr_decision_col_left[4]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
						decision_bottom_left_sao_across_slices_enable  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*5] ;
						decision_bottom_left_slice_id[3:0]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*5 + 4 : BIT_DEPTH*5 + 1 ] ;
						decision_bottom_left_slice_id[7:4]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*5 + 3 : BIT_DEPTH*5 ] ;
					end
					state <= STATE_9;
				end
				STATE_9 : begin
					if((Xc_in==(pic_width_in - 8)) && (Yc_in>0) && (M_block_cb_sao_apply==1)) begin
						cb_M_block[0]    <=    cb_pix_1_1_out_sao ;
                        cb_M_block[1]    <=    cb_pix_1_2_out_sao ;
						cb_N_block[0]    <=    cb_pix_1_3_out_sao ;
                        cb_N_block[1]    <=    cb_pix_1_4_out_sao ;
                        cb_M_block[2]    <=    cb_pix_2_1_out_sao ;
                        cb_M_block[3]    <=    cb_pix_2_2_out_sao ;
                        cb_N_block[2]    <=    cb_pix_2_3_out_sao ;
                        cb_N_block[3]    <=    cb_pix_2_4_out_sao ;
						cb_D_block[0]    <=    cb_pix_3_1_out_sao ;
                        cb_D_block[1]    <=    cb_pix_3_2_out_sao ;
						cb_E_block[0]    <=    cb_pix_3_3_out_sao ;
                        cb_E_block[1]    <=    cb_pix_3_4_out_sao ;
                        cb_D_block[2]    <=    cb_pix_4_1_out_sao ;
                        cb_D_block[3]    <=    cb_pix_4_2_out_sao ;
                        cb_E_block[2]    <=    cb_pix_4_3_out_sao ;
                        cb_E_block[3]    <=    cb_pix_4_4_out_sao ;
					end
					if((Xc_in==(pic_width_in - 8)) && (Yc_in>0) && (M_block_cb_sao_apply==1)) begin
						cr_M_block[0]    <=    cr_pix_1_1_out_sao ;
                        cr_M_block[1]    <=    cr_pix_1_2_out_sao ;
						cr_N_block[0]    <=    cr_pix_1_3_out_sao ;
                        cr_N_block[1]    <=    cr_pix_1_4_out_sao ;
                        cr_M_block[2]    <=    cr_pix_2_1_out_sao ;
                        cr_M_block[3]    <=    cr_pix_2_2_out_sao ;
                        cr_N_block[2]    <=    cr_pix_2_3_out_sao ;
                        cr_N_block[3]    <=    cr_pix_2_4_out_sao ;
						cr_D_block[0]    <=    cr_pix_3_1_out_sao ;
                        cr_D_block[1]    <=    cr_pix_3_2_out_sao ;
						cr_E_block[0]    <=    cr_pix_3_3_out_sao ;
                        cr_E_block[1]    <=    cr_pix_3_4_out_sao ;
                        cr_D_block[2]    <=    cr_pix_4_1_out_sao ;
                        cr_D_block[3]    <=    cr_pix_4_2_out_sao ;
                        cr_E_block[2]    <=    cr_pix_4_3_out_sao ;
                        cr_E_block[3]    <=    cr_pix_4_4_out_sao ;
					end
					// Check for Bottom block SAO conditions
					if(Yc_in == (pic_height_in - 8)) begin
						state <= STATE_10;
					end
					else begin
						state <= STATE_INIT;
						done  <= 1'b1;
					end
				end
				STATE_10 : begin
					// Assign decision samples from BRAM to registers for Bottom Right block
					if((Xc_in==(pic_width_in - 8)) && (Yc_in==(pic_height_in - 8))) begin
						cb_decision_row[0]	<=	d_row_buf_cb_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						cb_decision_row[1]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        cb_decision_row[2]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        cb_decision_row[3]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        cb_decision_row[4]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        cb_decision_row[5]	<= 	d_row_buf_cb_data_out_a [BIT_DEPTH*6  - 1 : BIT_DEPTH*5 ] ;
                        cr_decision_row[0]	<=	d_row_buf_cr_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						cr_decision_row[1]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        cr_decision_row[2]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        cr_decision_row[3]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        cr_decision_row[4]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        cr_decision_row[5]	<= 	d_row_buf_cr_data_out_a [BIT_DEPTH*6  - 1 : BIT_DEPTH*5 ] ;
						decision_top_left_slice_boundary  <=  d_row_buf_cb_data_out_a [BIT_DEPTH*6] ;
						decision_top_right_slice_id[3:0]  <=  d_row_buf_cb_data_out_a [BIT_DEPTH*6 + 4 : BIT_DEPTH*6 + 1 ] ;
						decision_top_right_sao_across_slices_enable  <=  d_row_buf_cr_data_out_a [BIT_DEPTH*6] ;
						decision_top_right_slice_id[7:4]  <=  d_row_buf_cr_data_out_a [BIT_DEPTH*6 + 4 : BIT_DEPTH*6 + 1 ] ;
					end
					if((Xc_in==(pic_width_in - 8)) && (Yc_in==(pic_height_in - 8))) begin
						cb_decision_col_left[0]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						cb_decision_col_left[1]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        cb_decision_col_left[2]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        cb_decision_col_left[3]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        cb_decision_col_left[4]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
                        cr_decision_col_left[0]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*1  - 1 : BIT_DEPTH*0 ] ;
						cr_decision_col_left[1]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*2  - 1 : BIT_DEPTH*1 ] ;
                        cr_decision_col_left[2]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*3  - 1 : BIT_DEPTH*2 ] ;
                        cr_decision_col_left[3]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*4  - 1 : BIT_DEPTH*3 ] ;
                        cr_decision_col_left[4]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*5  - 1 : BIT_DEPTH*4 ] ;
						decision_bottom_left_sao_across_slices_enable  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*5] ;
						decision_bottom_left_slice_id[3:0]  <=  d_left_col_buf_cb_data_out_a [BIT_DEPTH*5 + 4 : BIT_DEPTH*5 + 1 ] ;
						decision_bottom_left_slice_id[7:4]  <=  d_left_col_buf_cr_data_out_a [BIT_DEPTH*5 + 3 : BIT_DEPTH*5 ] ;
					end
					state <= STATE_11;
				end
				STATE_11 : begin
					if((Xc_in>0) && (Yc_in==(pic_height_in - 8)) && (H_block_cb_sao_apply==1)) begin
						cb_H_block[0]    <=    cb_pix_1_1_out_sao ;
                        cb_H_block[1]    <=    cb_pix_1_2_out_sao ;
						cb_A_block[0]    <=    cb_pix_1_3_out_sao ;
                        cb_A_block[1]    <=    cb_pix_1_4_out_sao ;
                        cb_H_block[2]    <=    cb_pix_2_1_out_sao ;
                        cb_H_block[3]    <=    cb_pix_2_2_out_sao ;
                        cb_A_block[2]    <=    cb_pix_2_3_out_sao ;
                        cb_A_block[3]    <=    cb_pix_2_4_out_sao ;
						cb_I_block[0]    <=    cb_pix_3_1_out_sao ;
                        cb_I_block[1]    <=    cb_pix_3_2_out_sao ;
						cb_B_block[0]    <=    cb_pix_3_3_out_sao ;
                        cb_B_block[1]    <=    cb_pix_3_4_out_sao ;
                        cb_I_block[2]    <=    cb_pix_4_1_out_sao ;
                        cb_I_block[3]    <=    cb_pix_4_2_out_sao ;
                        cb_B_block[2]    <=    cb_pix_4_3_out_sao ;
                        cb_B_block[3]    <=    cb_pix_4_4_out_sao ;
					end
					if((Xc_in>0) && (Yc_in==(pic_height_in - 8)) && (H_block_cr_sao_apply==1)) begin
						cr_H_block[0]    <=    cr_pix_1_1_out_sao ;
                        cr_H_block[1]    <=    cr_pix_1_2_out_sao ;
						cr_A_block[0]    <=    cr_pix_1_3_out_sao ;
                        cr_A_block[1]    <=    cr_pix_1_4_out_sao ;
                        cr_H_block[2]    <=    cr_pix_2_1_out_sao ;
                        cr_H_block[3]    <=    cr_pix_2_2_out_sao ;
                        cr_A_block[2]    <=    cr_pix_2_3_out_sao ;
                        cr_A_block[3]    <=    cr_pix_2_4_out_sao ;
						cr_I_block[0]    <=    cr_pix_3_1_out_sao ;
                        cr_I_block[1]    <=    cr_pix_3_2_out_sao ;
						cr_B_block[0]    <=    cr_pix_3_3_out_sao ;
                        cr_B_block[1]    <=    cr_pix_3_4_out_sao ;
                        cr_I_block[2]    <=    cr_pix_4_1_out_sao ;
                        cr_I_block[3]    <=    cr_pix_4_2_out_sao ;
                        cr_B_block[2]    <=    cr_pix_4_3_out_sao ;
                        cr_B_block[3]    <=    cr_pix_4_4_out_sao ;
					end
					// Check for Bottom Right block SAO conditions
					if((Xc_in==(pic_width_in - 8)) && (Yc_in==(pic_height_in - 8))) begin
						state <= STATE_12;
					end
					else begin
						state <= STATE_INIT;
						done  <= 1'b1;
					end
				end
				STATE_12 : begin
					state <= STATE_13;
				end
				STATE_13 : begin
					if((Xc_in==(pic_width_in - 8)) && (Yc_in==(pic_height_in - 8)) && (q1_block_cb_sao_apply==1)) begin
						cb_q_block_1[0]    <=    cb_pix_1_1_out_sao ;
                        cb_q_block_1[1]    <=    cb_pix_1_2_out_sao ;
						cb_q_block_2[0]    <=    cb_pix_1_3_out_sao ;
                        cb_q_block_2[1]    <=    cb_pix_1_4_out_sao ;
                        cb_q_block_1[2]    <=    cb_pix_2_1_out_sao ;
                        cb_q_block_1[3]    <=    cb_pix_2_2_out_sao ;
                        cb_q_block_2[2]    <=    cb_pix_2_3_out_sao ;
                        cb_q_block_2[3]    <=    cb_pix_2_4_out_sao ;
						cb_q_block_3[0]    <=    cb_pix_3_1_out_sao ;
                        cb_q_block_3[1]    <=    cb_pix_3_2_out_sao ;
						cb_q_block_4[0]    <=    cb_pix_3_3_out_sao ;
                        cb_q_block_4[1]    <=    cb_pix_3_4_out_sao ;
                        cb_q_block_3[2]    <=    cb_pix_4_1_out_sao ;
                        cb_q_block_3[3]    <=    cb_pix_4_2_out_sao ;
                        cb_q_block_4[2]    <=    cb_pix_4_3_out_sao ;
                        cb_q_block_4[3]    <=    cb_pix_4_4_out_sao ;
					end
					if((Xc_in==(pic_width_in - 8)) && (Yc_in==(pic_height_in - 8)) && (q1_block_cr_sao_apply==1)) begin
						cr_q_block_1[0]    <=    cr_pix_1_1_out_sao ;
                        cr_q_block_1[1]    <=    cr_pix_1_2_out_sao ;
						cr_q_block_2[0]    <=    cr_pix_1_3_out_sao ;
                        cr_q_block_2[1]    <=    cr_pix_1_4_out_sao ;
                        cr_q_block_1[2]    <=    cr_pix_2_1_out_sao ;
                        cr_q_block_1[3]    <=    cr_pix_2_2_out_sao ;
                        cr_q_block_2[2]    <=    cr_pix_2_3_out_sao ;
                        cr_q_block_2[3]    <=    cr_pix_2_4_out_sao ;
						cr_q_block_3[0]    <=    cr_pix_3_1_out_sao ;
                        cr_q_block_3[1]    <=    cr_pix_3_2_out_sao ;
						cr_q_block_4[0]    <=    cr_pix_3_3_out_sao ;
                        cr_q_block_4[1]    <=    cr_pix_3_4_out_sao ;
                        cr_q_block_3[2]    <=    cr_pix_4_1_out_sao ;
                        cr_q_block_3[3]    <=    cr_pix_4_2_out_sao ;
                        cr_q_block_4[2]    <=    cr_pix_4_3_out_sao ;
                        cr_q_block_4[3]    <=    cr_pix_4_4_out_sao ;
					end
					state <= STATE_INIT;
					done  <= 1'b1;
				end
			endcase
		end
	end
	
	
	//--------------- Combinational block to check SAO applying conditions for 8x8 blocks -----------------
	always @(*) begin
		if((J_block_pcm_bypass_sao_disable_flag==0) && (J_block_sao_chroma==1) && (J_block_cb_sao_type>0)) begin
			J_block_cb_sao_apply  =  1'b1 ;
		end
		else begin
			J_block_cb_sao_apply  =  1'b0 ;
		end
		if((J_block_pcm_bypass_sao_disable_flag==0) && (J_block_sao_chroma==1) && (J_block_cr_sao_type>0)) begin
			J_block_cr_sao_apply  =  1'b1 ;
		end
		else begin
			J_block_cr_sao_apply  =  1'b0 ;
		end
		if((H_block_pcm_bypass_sao_disable_flag==0) && (H_block_sao_chroma==1) && (H_block_cb_sao_type>0)) begin
			H_block_cb_sao_apply  =  1'b1 ;
		end
		else begin
			H_block_cb_sao_apply  =  1'b0 ;
		end
		if((H_block_pcm_bypass_sao_disable_flag==0) && (H_block_sao_chroma==1) && (H_block_cr_sao_type>0)) begin
			H_block_cr_sao_apply  =  1'b1 ;
		end
		else begin
			H_block_cr_sao_apply  =  1'b0 ;
		end
		if((M_block_pcm_bypass_sao_disable_flag==0) && (M_block_sao_chroma==1) && (M_block_cb_sao_type>0)) begin
			M_block_cb_sao_apply  =  1'b1 ;
		end
		else begin
			M_block_cb_sao_apply  =  1'b0 ;
		end
		if((M_block_pcm_bypass_sao_disable_flag==0) && (M_block_sao_chroma==1) && (M_block_cr_sao_type>0)) begin
			M_block_cr_sao_apply  =  1'b1 ;
		end
		else begin
			M_block_cr_sao_apply  =  1'b0 ;
		end
		if((pcm_bypass_sao_disable_in==0) && (sao_chroma_in==1) && (cb_sao_type_in>0)) begin
			q1_block_cb_sao_apply  =  1'b1 ;
		end
		else begin
			q1_block_cb_sao_apply  =  1'b0 ;
		end
		if((pcm_bypass_sao_disable_in==0) && (sao_chroma_in==1) && (cr_sao_type_in>0)) begin
			q1_block_cr_sao_apply  =  1'b1 ;
		end
		else begin
			q1_block_cr_sao_apply  =  1'b0 ;
		end
	end
	
	
	//--------------- Combinational block to calculate boundary crossing flags --------------------------
	always @(*) begin
		if(J_block_zs_addr < J_block_TR_zs_addr) begin
			J_block_top_right_sao_across_slices_enable_flag  =  decision_top_right_sao_across_slices_enable ;
		end
		else begin
			J_block_top_right_sao_across_slices_enable_flag  =  J_block_sao_across_slices_enable_flag ;
		end
		if(J_block_zs_addr < J_block_BL_zs_addr) begin
			J_block_bottom_left_sao_across_slices_enable_flag  =  decision_bottom_left_sao_across_slices_enable ;
		end
		else begin
			J_block_bottom_left_sao_across_slices_enable_flag  =  J_block_sao_across_slices_enable_flag ;
		end
		if(H_block_zs_addr < H_block_TR_zs_addr) begin
			H_block_top_right_sao_across_slices_enable_flag  =  decision_top_right_sao_across_slices_enable ;
		end
		else begin
			H_block_top_right_sao_across_slices_enable_flag  =  H_block_sao_across_slices_enable_flag ;
		end
		if(M_block_zs_addr < M_block_BL_zs_addr) begin
			M_block_bottom_left_sao_across_slices_enable_flag  =  decision_bottom_left_sao_across_slices_enable ;
		end
		else begin
			M_block_bottom_left_sao_across_slices_enable_flag  =  M_block_sao_across_slices_enable_flag ;
		end
	end
	
	assign	J_block_zs_addr		=	{ J_block_Y8[8],J_block_X8[8],J_block_Y8[7],J_block_X8[7],J_block_Y8[6],J_block_X8[6],J_block_Y8[5],J_block_X8[5],J_block_Y8[4],J_block_X8[4],J_block_Y8[3],J_block_X8[3],J_block_Y8[2],J_block_X8[2],J_block_Y8[1],J_block_X8[1] } ;
	assign	H_block_zs_addr		=	{ H_block_Y8[8],H_block_X8[8],H_block_Y8[7],H_block_X8[7],H_block_Y8[6],H_block_X8[6],H_block_Y8[5],H_block_X8[5],H_block_Y8[4],H_block_X8[4],H_block_Y8[3],H_block_X8[3],H_block_Y8[2],H_block_X8[2],H_block_Y8[1],H_block_X8[1] } ;
	assign	M_block_zs_addr		=	{ M_block_Y8[8],M_block_X8[8],M_block_Y8[7],M_block_X8[7],M_block_Y8[6],M_block_X8[6],M_block_Y8[5],M_block_X8[5],M_block_Y8[4],M_block_X8[4],M_block_Y8[3],M_block_X8[3],M_block_Y8[2],M_block_X8[2],M_block_Y8[1],M_block_X8[1] } ;
	
	assign	J_block_TR_zs_addr	=	{ J_block_TR_Y8[8],J_block_TR_X8[8],J_block_TR_Y8[7],J_block_TR_X8[7],J_block_TR_Y8[6],J_block_TR_X8[6],J_block_TR_Y8[5],J_block_TR_X8[5],J_block_TR_Y8[4],J_block_TR_X8[4],J_block_TR_Y8[3],J_block_TR_X8[3],J_block_TR_Y8[2],J_block_TR_X8[2],J_block_TR_Y8[1],J_block_TR_X8[1] } ;
	assign	J_block_BL_zs_addr	=	{ J_block_BL_Y8[8],J_block_BL_X8[8],J_block_BL_Y8[7],J_block_BL_X8[7],J_block_BL_Y8[6],J_block_BL_X8[6],J_block_BL_Y8[5],J_block_BL_X8[5],J_block_BL_Y8[4],J_block_BL_X8[4],J_block_BL_Y8[3],J_block_BL_X8[3],J_block_BL_Y8[2],J_block_BL_X8[2],J_block_BL_Y8[1],J_block_BL_X8[1] } ;
	assign	H_block_TR_zs_addr	=	M_block_zs_addr ;
	assign	M_block_BL_zs_addr	=	H_block_zs_addr ;
	
	
	assign	J_block_cross_boundary_left  		=  	~( (Xc_in[11:3] < 2) | (J_block_tile_left_flag & (~loop_filter_accross_tiles_enabled_in)) | (J_block_slice_left_flag & (~J_block_sao_across_slices_enable_flag)) ) ;
	assign	J_block_cross_boundary_top  		=  	~( (Yc_in[11:3] < 2) | (J_block_tile_top_flag  & (~loop_filter_accross_tiles_enabled_in)) | (J_block_slice_top_flag  & (~J_block_sao_across_slices_enable_flag)) ) ;
	assign	J_block_cross_boundary_right  		=  	~( (tile_left_boundary_in & (~loop_filter_accross_tiles_enabled_in)) | (M_block_slice_left_flag & (~M_block_sao_across_slices_enable_flag)) ) ;
	assign	J_block_cross_boundary_bottom  		=  	~( (H_block_tile_top_flag  & (~loop_filter_accross_tiles_enabled_in)) | (H_block_slice_top_flag  & (~H_block_sao_across_slices_enable_flag)) ) ;
	assign	J_block_cross_boundary_top_left 	=  	~( (Xc_in[11:3] < 2) | (Yc_in[11:3] < 2) | ((J_block_tile_top_flag | J_block_tile_left_flag) & (~loop_filter_accross_tiles_enabled_in)) | ((J_block_slice_top_flag | decision_top_left_slice_boundary) & (~J_block_sao_across_slices_enable_flag)) ) ;
	assign	J_block_cross_boundary_bottom_right	=  	~( ((tile_left_boundary_in | tile_top_boundary_in)  & (~loop_filter_accross_tiles_enabled_in)) | ((slice_top_boundary_in | M_block_slice_left_flag)  & (~loop_filter_accross_slices_enabled_in)) ) ;
	assign	J_block_cross_boundary_top_right 	=  	~( (Yc_in[11:3] < 2) | ((J_block_tile_top_flag | tile_left_boundary_in) & (~loop_filter_accross_tiles_enabled_in)) | ((J_block_slice_id != decision_top_right_slice_id) & (~J_block_top_right_sao_across_slices_enable_flag)) ) ;
	assign	J_block_cross_boundary_bottom_left 	=  	~( (Xc_in[11:3] < 2) | ((H_block_tile_top_flag | H_block_tile_left_flag) & (~loop_filter_accross_tiles_enabled_in)) | ((J_block_slice_id != decision_bottom_left_slice_id) & (~J_block_bottom_left_sao_across_slices_enable_flag)) ) ;
	
	assign	H_block_cross_boundary_left  		=  	~( (Xc_in[11:3] < 2) | (H_block_tile_left_flag & (~loop_filter_accross_tiles_enabled_in)) | (H_block_slice_left_flag & (~H_block_sao_across_slices_enable_flag)) ) ;
	assign	H_block_cross_boundary_top  		=  	~( (H_block_tile_top_flag  & (~loop_filter_accross_tiles_enabled_in)) | (H_block_slice_top_flag  & (~H_block_sao_across_slices_enable_flag)) ) ;
	assign	H_block_cross_boundary_right  		=  	~( (tile_left_boundary_in  & (~loop_filter_accross_tiles_enabled_in)) | (slice_left_boundary_in  & (~loop_filter_accross_slices_enabled_in)) ) ;
	assign	H_block_cross_boundary_bottom  		=  	1'b0 ;
	assign	H_block_cross_boundary_top_left 	=  	~( (Xc_in[11:3] < 2) | ((H_block_tile_top_flag | H_block_tile_left_flag) & (~loop_filter_accross_tiles_enabled_in)) | ((H_block_slice_top_flag | decision_top_left_slice_boundary) & (~H_block_sao_across_slices_enable_flag)) ) ;
	assign	H_block_cross_boundary_bottom_right	=  	1'b0 ;
	assign	H_block_cross_boundary_top_right 	=  	~( ((tile_left_boundary_in | tile_top_boundary_in) & (~loop_filter_accross_tiles_enabled_in)) | ((H_block_slice_id != decision_top_right_slice_id) & (~H_block_top_right_sao_across_slices_enable_flag)) ) ;
	assign	H_block_cross_boundary_bottom_left 	=  	1'b0 ;
	
	assign	M_block_cross_boundary_left  		=  	~( (M_block_tile_left_flag & (~loop_filter_accross_tiles_enabled_in)) | (M_block_slice_left_flag & (~M_block_sao_across_slices_enable_flag)) ) ;
	assign	M_block_cross_boundary_top  		=  	~( (Yc_in[11:3] < 2) | (M_block_tile_top_flag  & (~loop_filter_accross_tiles_enabled_in)) | (M_block_slice_top_flag  & (~M_block_sao_across_slices_enable_flag)) ) ;
	assign	M_block_cross_boundary_right  		=  	1'b0 ;
	assign	M_block_cross_boundary_bottom  		=  	~( (tile_top_boundary_in   & (~loop_filter_accross_tiles_enabled_in)) | (slice_top_boundary_in  & (~loop_filter_accross_slices_enabled_in)) ) ;
	assign	M_block_cross_boundary_top_left 	=  	~( (Yc_in[11:3] < 2) | ((M_block_tile_top_flag | M_block_tile_left_flag) & (~loop_filter_accross_tiles_enabled_in)) | ((M_block_slice_top_flag | decision_top_left_slice_boundary) & (~M_block_sao_across_slices_enable_flag)) ) ;
	assign	M_block_cross_boundary_bottom_right	=  	1'b0 ;
	assign	M_block_cross_boundary_top_right 	=  	1'b0 ;
	assign	M_block_cross_boundary_bottom_left 	=  	~( ((tile_left_boundary_in | tile_top_boundary_in) & (~loop_filter_accross_tiles_enabled_in)) | ((M_block_slice_id != decision_bottom_left_slice_id) & (~M_block_bottom_left_sao_across_slices_enable_flag)) ) ;
	
	assign	q_block_cross_boundary_left  		=  	~( (tile_left_boundary_in & (~loop_filter_accross_tiles_enabled_in)) | (slice_left_boundary_in & (~loop_filter_accross_slices_enabled_in)) ) ;
	assign	q_block_cross_boundary_top  		=  	~( (tile_top_boundary_in  & (~loop_filter_accross_tiles_enabled_in)) | (slice_top_boundary_in  & (~loop_filter_accross_slices_enabled_in)) ) ;
	assign	q_block_cross_boundary_right  		=  	1'b0 ;
	assign	q_block_cross_boundary_bottom  		=  	1'b0 ;
	assign	q_block_cross_boundary_top_left 	=  	~( ((tile_left_boundary_in | tile_top_boundary_in) & (~loop_filter_accross_tiles_enabled_in)) | ((slice_top_boundary_in | decision_top_left_slice_boundary) & (~loop_filter_accross_slices_enabled_in)) ) ;
	assign	q_block_cross_boundary_bottom_right	=  	1'b0 ;
	assign	q_block_cross_boundary_top_right 	=  	1'b0 ;
	assign	q_block_cross_boundary_bottom_left 	=  	1'b0 ;
	
	
	//--------------- Combinational block to set up and activate SAO block filter ----------------------
	always @(*) begin
		start_cb_sao_filter		=	1'b0 ;
		start_cr_sao_filter		=	1'b0 ;
		cross_boundary_left_flag_sao   		  =	  1'bx ;
		cross_boundary_right_flag_sao         =	  1'bx ;
		cross_boundary_top_flag_sao	          =	  1'bx ;
		cross_boundary_bottom_flag_sao        =	  1'bx ;
		cross_boundary_top_left_flag_sao	  =	  1'bx ;
		cross_boundary_top_right_flag_sao	  =	  1'bx ;
		cross_boundary_bottom_left_flag_sao   =	  1'bx ;
		cross_boundary_bottom_right_flag_sao  =	  1'bx ;
		cb_sao_type_in_sao	   	=	2'bxx ;
		cb_sao_eo_class_in_sao	=	2'bxx ;
		cb_sao_bandpos_in_sao 	=	5'bxxxxx;
		cb_sao_offset_1_in_sao	=	4'bxxxx ;
		cb_sao_offset_2_in_sao	=	4'bxxxx ;
		cb_sao_offset_3_in_sao	=	4'bxxxx ;
		cb_sao_offset_4_in_sao	=	4'bxxxx ;
		cb_p_0_0_in_sao			=	8'bxxxxxxxx ;
		cb_p_0_1_in_sao       	=	8'bxxxxxxxx ;
		cb_p_0_2_in_sao       	=	8'bxxxxxxxx ;
		cb_p_0_3_in_sao       	=	8'bxxxxxxxx ;
		cb_p_0_4_in_sao       	=	8'bxxxxxxxx ;
		cb_p_0_5_in_sao       	=	8'bxxxxxxxx ;
		cb_p_1_0_in_sao       	=	8'bxxxxxxxx ;
		cb_p_1_1_in_sao       	=	8'bxxxxxxxx ;
		cb_p_1_2_in_sao       	=	8'bxxxxxxxx ;
		cb_p_1_3_in_sao       	=	8'bxxxxxxxx ;
		cb_p_1_4_in_sao       	=	8'bxxxxxxxx ;
		cb_p_1_5_in_sao       	=	8'bxxxxxxxx ;
		cb_p_2_0_in_sao       	=	8'bxxxxxxxx ;
		cb_p_2_1_in_sao       	=	8'bxxxxxxxx ;
		cb_p_2_2_in_sao       	=	8'bxxxxxxxx ;
		cb_p_2_3_in_sao       	=	8'bxxxxxxxx ;
		cb_p_2_4_in_sao       	=	8'bxxxxxxxx ;
		cb_p_2_5_in_sao       	=	8'bxxxxxxxx ;
		cb_p_3_0_in_sao       	=	8'bxxxxxxxx ;
		cb_p_3_1_in_sao       	=	8'bxxxxxxxx ;
		cb_p_3_2_in_sao       	=	8'bxxxxxxxx ;
		cb_p_3_3_in_sao       	=	8'bxxxxxxxx ;
		cb_p_3_4_in_sao       	=	8'bxxxxxxxx ;
		cb_p_3_5_in_sao       	=	8'bxxxxxxxx ;
		cb_p_4_0_in_sao       	=	8'bxxxxxxxx ;
		cb_p_4_1_in_sao       	=	8'bxxxxxxxx ;
		cb_p_4_2_in_sao       	=	8'bxxxxxxxx ;
		cb_p_4_3_in_sao       	=	8'bxxxxxxxx ;
		cb_p_4_4_in_sao       	=	8'bxxxxxxxx ;
		cb_p_4_5_in_sao       	=	8'bxxxxxxxx ;
		cb_p_5_0_in_sao       	=	8'bxxxxxxxx ;
		cb_p_5_1_in_sao       	=	8'bxxxxxxxx ;
		cb_p_5_2_in_sao       	=	8'bxxxxxxxx ;
		cb_p_5_3_in_sao       	=	8'bxxxxxxxx ;
		cb_p_5_4_in_sao       	=	8'bxxxxxxxx ;
		cb_p_5_5_in_sao       	=	8'bxxxxxxxx ;
		cr_sao_type_in_sao	   	=	2'bxx ;
		cr_sao_eo_class_in_sao	=	2'bxx ;
		cr_sao_bandpos_in_sao 	=	5'bxxxxx;
		cr_sao_offset_1_in_sao	=	4'bxxxx ;
		cr_sao_offset_2_in_sao	=	4'bxxxx ;
		cr_sao_offset_3_in_sao	=	4'bxxxx ;
		cr_sao_offset_4_in_sao	=	4'bxxxx ;
		cr_p_0_0_in_sao			=	8'bxxxxxxxx ;
		cr_p_0_1_in_sao       	=	8'bxxxxxxxx ;
		cr_p_0_2_in_sao       	=	8'bxxxxxxxx ;
		cr_p_0_3_in_sao       	=	8'bxxxxxxxx ;
		cr_p_0_4_in_sao       	=	8'bxxxxxxxx ;
		cr_p_0_5_in_sao       	=	8'bxxxxxxxx ;
		cr_p_1_0_in_sao       	=	8'bxxxxxxxx ;
		cr_p_1_1_in_sao       	=	8'bxxxxxxxx ;
		cr_p_1_2_in_sao       	=	8'bxxxxxxxx ;
		cr_p_1_3_in_sao       	=	8'bxxxxxxxx ;
		cr_p_1_4_in_sao       	=	8'bxxxxxxxx ;
		cr_p_1_5_in_sao       	=	8'bxxxxxxxx ;
		cr_p_2_0_in_sao       	=	8'bxxxxxxxx ;
		cr_p_2_1_in_sao       	=	8'bxxxxxxxx ;
		cr_p_2_2_in_sao       	=	8'bxxxxxxxx ;
		cr_p_2_3_in_sao       	=	8'bxxxxxxxx ;
		cr_p_2_4_in_sao       	=	8'bxxxxxxxx ;
		cr_p_2_5_in_sao       	=	8'bxxxxxxxx ;
		cr_p_3_0_in_sao       	=	8'bxxxxxxxx ;
		cr_p_3_1_in_sao       	=	8'bxxxxxxxx ;
		cr_p_3_2_in_sao       	=	8'bxxxxxxxx ;
		cr_p_3_3_in_sao       	=	8'bxxxxxxxx ;
		cr_p_3_4_in_sao       	=	8'bxxxxxxxx ;
		cr_p_3_5_in_sao       	=	8'bxxxxxxxx ;
		cr_p_4_0_in_sao       	=	8'bxxxxxxxx ;
		cr_p_4_1_in_sao       	=	8'bxxxxxxxx ;
		cr_p_4_2_in_sao       	=	8'bxxxxxxxx ;
		cr_p_4_3_in_sao       	=	8'bxxxxxxxx ;
		cr_p_4_4_in_sao       	=	8'bxxxxxxxx ;
		cr_p_4_5_in_sao       	=	8'bxxxxxxxx ;
		cr_p_5_0_in_sao       	=	8'bxxxxxxxx ;
		cr_p_5_1_in_sao       	=	8'bxxxxxxxx ;
		cr_p_5_2_in_sao       	=	8'bxxxxxxxx ;
		cr_p_5_3_in_sao       	=	8'bxxxxxxxx ;
		cr_p_5_4_in_sao       	=	8'bxxxxxxxx ;
		cr_p_5_5_in_sao       	=	8'bxxxxxxxx ;
		
		case(state)
			STATE_4 : begin
				if((Xc_in>0) && (Yc_in>0) && (J_block_cb_sao_apply==1)) begin
					start_cb_sao_filter  =  1'b1 ;
				end
				if((Xc_in>0) && (Yc_in>0) && (J_block_cr_sao_apply==1)) begin
					start_cr_sao_filter  =  1'b1 ;
				end
				cross_boundary_left_flag_sao   		  =	  J_block_cross_boundary_left		  ;
				cross_boundary_right_flag_sao         =	  J_block_cross_boundary_right		  ;
				cross_boundary_top_flag_sao	          =	  J_block_cross_boundary_top		  ;
				cross_boundary_bottom_flag_sao        =	  J_block_cross_boundary_bottom	      ;
				cross_boundary_top_left_flag_sao	  =	  J_block_cross_boundary_top_left	  ;
				cross_boundary_top_right_flag_sao	  =	  J_block_cross_boundary_top_right	  ;
				cross_boundary_bottom_left_flag_sao   =	  J_block_cross_boundary_bottom_left  ;
				cross_boundary_bottom_right_flag_sao  =	  J_block_cross_boundary_bottom_right ;
				cb_sao_type_in_sao	   	=	J_block_cb_sao_type     ;
				cb_sao_eo_class_in_sao	=	J_block_cb_eoclass      ;
				cb_sao_bandpos_in_sao 	=	J_block_cb_bandpos      ;
				cb_sao_offset_1_in_sao	=	J_block_cb_sao_offset_1 ;
				cb_sao_offset_2_in_sao	=	J_block_cb_sao_offset_2 ;
				cb_sao_offset_3_in_sao	=	J_block_cb_sao_offset_3 ;
				cb_sao_offset_4_in_sao	=	J_block_cb_sao_offset_4 ;
				cb_p_0_0_in_sao			=	cb_decision_row[0] ;
				cb_p_0_1_in_sao       	=	cb_decision_row[1] ;
				cb_p_0_2_in_sao       	=	cb_decision_row[2] ;
				cb_p_0_3_in_sao       	=	cb_decision_row[3] ;
				cb_p_0_4_in_sao       	=	cb_decision_row[4] ;
				cb_p_0_5_in_sao       	=	cb_decision_row[5] ;
				cb_p_1_0_in_sao       	=	cb_decision_col_left[0] ;
				cb_p_1_1_in_sao       	=	cb_J_block[0] ;
				cb_p_1_2_in_sao       	=	cb_J_block[1] ;
				cb_p_1_3_in_sao       	=	cb_K_block[0] ;
				cb_p_1_4_in_sao       	=	cb_K_block[1] ;
				cb_p_1_5_in_sao       	=	cb_decision_col_right[0] ;
				cb_p_2_0_in_sao       	=	cb_decision_col_left[1] ;
				cb_p_2_1_in_sao       	=	cb_J_block[2] ;
				cb_p_2_2_in_sao       	=	cb_J_block[3] ;
				cb_p_2_3_in_sao       	=	cb_K_block[2] ;
				cb_p_2_4_in_sao       	=	cb_K_block[3] ;
				cb_p_2_5_in_sao       	=	cb_decision_col_right[1] ;
				cb_p_3_0_in_sao       	=	cb_decision_col_left[2] ;
				cb_p_3_1_in_sao       	=	cb_G_block[0] ;
				cb_p_3_2_in_sao       	=	cb_G_block[1] ;
				cb_p_3_3_in_sao       	=	cb_C_block[0] ;
				cb_p_3_4_in_sao       	=	cb_C_block[1] ;
				cb_p_3_5_in_sao       	=	cb_D_block[0] ;
				cb_p_4_0_in_sao       	=	cb_decision_col_left[3] ;
				cb_p_4_1_in_sao       	=	cb_G_block[2] ;
				cb_p_4_2_in_sao       	=	cb_G_block[3] ;
				cb_p_4_3_in_sao       	=	cb_C_block[2] ;
				cb_p_4_4_in_sao       	=	cb_C_block[3] ;
				cb_p_4_5_in_sao       	=	cb_D_block[2] ;
				cb_p_5_0_in_sao       	=	cb_decision_col_left[4] ;
				cb_p_5_1_in_sao       	=	cb_H_block[0] ;
				cb_p_5_2_in_sao       	=	cb_H_block[1] ;
				cb_p_5_3_in_sao       	=	cb_A_block[0] ;
				cb_p_5_4_in_sao       	=	cb_A_block[1] ;
				cb_p_5_5_in_sao       	=	cb_q_block_1[0] ;
				cr_sao_type_in_sao	   	=	J_block_cr_sao_type     ;
				cr_sao_eo_class_in_sao	=	J_block_cr_eoclass      ;
				cr_sao_bandpos_in_sao 	=	J_block_cr_bandpos      ;
				cr_sao_offset_1_in_sao	=	J_block_cr_sao_offset_1 ;
				cr_sao_offset_2_in_sao	=	J_block_cr_sao_offset_2 ;
				cr_sao_offset_3_in_sao	=	J_block_cr_sao_offset_3 ;
				cr_sao_offset_4_in_sao	=	J_block_cr_sao_offset_4 ;
				cr_p_0_0_in_sao			=	cr_decision_row[0] ;
				cr_p_0_1_in_sao       	=	cr_decision_row[1] ;
				cr_p_0_2_in_sao       	=	cr_decision_row[2] ;
				cr_p_0_3_in_sao       	=	cr_decision_row[3] ;
				cr_p_0_4_in_sao       	=	cr_decision_row[4] ;
				cr_p_0_5_in_sao       	=	cr_decision_row[5] ;
				cr_p_1_0_in_sao       	=	cr_decision_col_left[0] ;
				cr_p_1_1_in_sao       	=	cr_J_block[0] ;
				cr_p_1_2_in_sao       	=	cr_J_block[1] ;
				cr_p_1_3_in_sao       	=	cr_K_block[0] ;
				cr_p_1_4_in_sao       	=	cr_K_block[1] ;
				cr_p_1_5_in_sao       	=	cr_decision_col_right[0] ;
				cr_p_2_0_in_sao       	=	cr_decision_col_left[1] ;
				cr_p_2_1_in_sao       	=	cr_J_block[2] ;
				cr_p_2_2_in_sao       	=	cr_J_block[3] ;
				cr_p_2_3_in_sao       	=	cr_K_block[2] ;
				cr_p_2_4_in_sao       	=	cr_K_block[3] ;
				cr_p_2_5_in_sao       	=	cr_decision_col_right[1] ;
				cr_p_3_0_in_sao       	=	cr_decision_col_left[2] ;
				cr_p_3_1_in_sao       	=	cr_G_block[0] ;
				cr_p_3_2_in_sao       	=	cr_G_block[1] ;
				cr_p_3_3_in_sao       	=	cr_C_block[0] ;
				cr_p_3_4_in_sao       	=	cr_C_block[1] ;
				cr_p_3_5_in_sao       	=	cr_D_block[0] ;
				cr_p_4_0_in_sao       	=	cr_decision_col_left[3] ;
				cr_p_4_1_in_sao       	=	cr_G_block[2] ;
				cr_p_4_2_in_sao       	=	cr_G_block[3] ;
				cr_p_4_3_in_sao       	=	cr_C_block[2] ;
				cr_p_4_4_in_sao       	=	cr_C_block[3] ;
				cr_p_4_5_in_sao       	=	cr_D_block[2] ;
				cr_p_5_0_in_sao       	=	cr_decision_col_left[4] ;
				cr_p_5_1_in_sao       	=	cr_H_block[0] ;
				cr_p_5_2_in_sao       	=	cr_H_block[1] ;
				cr_p_5_3_in_sao       	=	cr_A_block[0] ;
				cr_p_5_4_in_sao       	=	cr_A_block[1] ;
				cr_p_5_5_in_sao       	=	cr_q_block_1[0] ;
			end
			STATE_7 : begin
				if((Xc_in==(pic_width_in - 8)) && (Yc_in>0) && (M_block_cb_sao_apply==1)) begin
					start_cb_sao_filter  =  1'b1 ;
				end
				if((Xc_in==(pic_width_in - 8)) && (Yc_in>0) && (M_block_cr_sao_apply==1)) begin
					start_cr_sao_filter  =  1'b1 ;
				end
				cross_boundary_left_flag_sao   		  =	  M_block_cross_boundary_left		  ;
				cross_boundary_right_flag_sao         =	  M_block_cross_boundary_right		  ;
				cross_boundary_top_flag_sao	          =	  M_block_cross_boundary_top		  ;
				cross_boundary_bottom_flag_sao        =	  M_block_cross_boundary_bottom	      ;
				cross_boundary_top_left_flag_sao	  =	  M_block_cross_boundary_top_left	  ;
				cross_boundary_top_right_flag_sao	  =	  M_block_cross_boundary_top_right	  ;
				cross_boundary_bottom_left_flag_sao   =	  M_block_cross_boundary_bottom_left  ;
				cross_boundary_bottom_right_flag_sao  =	  M_block_cross_boundary_bottom_right ;
				cb_sao_type_in_sao	   	=	M_block_cb_sao_type     ;
				cb_sao_eo_class_in_sao	=	M_block_cb_eoclass      ;
				cb_sao_bandpos_in_sao 	=	M_block_cb_bandpos      ;
				cb_sao_offset_1_in_sao	=	M_block_cb_sao_offset_1 ;
				cb_sao_offset_2_in_sao	=	M_block_cb_sao_offset_2 ;
				cb_sao_offset_3_in_sao	=	M_block_cb_sao_offset_3 ;
				cb_sao_offset_4_in_sao	=	M_block_cb_sao_offset_4 ;
				cb_p_0_0_in_sao			=	cb_decision_row[0] ;
				cb_p_0_1_in_sao       	=	cb_decision_row[1] ;
				cb_p_0_2_in_sao       	=	cb_decision_row[2] ;
				cb_p_0_3_in_sao       	=	cb_decision_row[3] ;
				cb_p_0_4_in_sao       	=	cb_decision_row[4] ;
				cb_p_0_5_in_sao       	=	cb_decision_row[5] ;
				cb_p_1_0_in_sao       	=	cb_decision_col_left[0] ;
				cb_p_1_1_in_sao       	=	cb_M_block[0] ;
				cb_p_1_2_in_sao       	=	cb_M_block[1] ;
				cb_p_1_3_in_sao       	=	cb_N_block[0] ;
				cb_p_1_4_in_sao       	=	cb_N_block[1] ;
				cb_p_1_5_in_sao       	=	8'd0 ;
				cb_p_2_0_in_sao       	=	cb_decision_col_left[1] ;
				cb_p_2_1_in_sao       	=	cb_M_block[2] ;
				cb_p_2_2_in_sao       	=	cb_M_block[3] ;
				cb_p_2_3_in_sao       	=	cb_N_block[2] ;
				cb_p_2_4_in_sao       	=	cb_N_block[3] ;
				cb_p_2_5_in_sao       	=	8'd0 ;
				cb_p_3_0_in_sao       	=	cb_decision_col_left[2] ;
				cb_p_3_1_in_sao       	=	cb_D_block[0] ;
				cb_p_3_2_in_sao       	=	cb_D_block[1] ;
				cb_p_3_3_in_sao       	=	cb_E_block[0] ;
				cb_p_3_4_in_sao       	=	cb_E_block[1] ;
				cb_p_3_5_in_sao       	=	8'd0 ;
				cb_p_4_0_in_sao       	=	cb_decision_col_left[3] ;
				cb_p_4_1_in_sao       	=	cb_D_block[2] ;
				cb_p_4_2_in_sao       	=	cb_D_block[3] ;
				cb_p_4_3_in_sao       	=	cb_E_block[2] ;
				cb_p_4_4_in_sao       	=	cb_E_block[3] ;
				cb_p_4_5_in_sao       	=	8'd0 ;
				cb_p_5_0_in_sao       	=	cb_decision_col_left[4] ;
				cb_p_5_1_in_sao       	=	cb_q_block_1[0] ;
				cb_p_5_2_in_sao       	=	cb_q_block_1[1] ;
				cb_p_5_3_in_sao       	=	cb_q_block_2[0] ;
				cb_p_5_4_in_sao       	=	cb_q_block_2[1] ;
				cb_p_5_5_in_sao       	=	8'd0 ;
				cr_sao_type_in_sao	   	=	M_block_cr_sao_type     ;
				cr_sao_eo_class_in_sao	=	M_block_cr_eoclass      ;
				cr_sao_bandpos_in_sao 	=	M_block_cr_bandpos      ;
				cr_sao_offset_1_in_sao	=	M_block_cr_sao_offset_1 ;
				cr_sao_offset_2_in_sao	=	M_block_cr_sao_offset_2 ;
				cr_sao_offset_3_in_sao	=	M_block_cr_sao_offset_3 ;
				cr_sao_offset_4_in_sao	=	M_block_cr_sao_offset_4 ;
				cr_p_0_0_in_sao			=	cr_decision_row[0] ;
				cr_p_0_1_in_sao       	=	cr_decision_row[1] ;
				cr_p_0_2_in_sao       	=	cr_decision_row[2] ;
				cr_p_0_3_in_sao       	=	cr_decision_row[3] ;
				cr_p_0_4_in_sao       	=	cr_decision_row[4] ;
				cr_p_0_5_in_sao       	=	cr_decision_row[5] ;
				cr_p_1_0_in_sao       	=	cr_decision_col_left[0] ;
				cr_p_1_1_in_sao       	=	cr_M_block[0] ;
				cr_p_1_2_in_sao       	=	cr_M_block[1] ;
				cr_p_1_3_in_sao       	=	cr_N_block[0] ;
				cr_p_1_4_in_sao       	=	cr_N_block[1] ;
				cr_p_1_5_in_sao       	=	8'd0 ;
				cr_p_2_0_in_sao       	=	cr_decision_col_left[1] ;
				cr_p_2_1_in_sao       	=	cr_M_block[2] ;
				cr_p_2_2_in_sao       	=	cr_M_block[3] ;
				cr_p_2_3_in_sao       	=	cr_N_block[2] ;
				cr_p_2_4_in_sao       	=	cr_N_block[3] ;
				cr_p_2_5_in_sao       	=	8'd0 ;
				cr_p_3_0_in_sao       	=	cr_decision_col_left[2] ;
				cr_p_3_1_in_sao       	=	cr_D_block[0] ;
				cr_p_3_2_in_sao       	=	cr_D_block[1] ;
				cr_p_3_3_in_sao       	=	cr_E_block[0] ;
				cr_p_3_4_in_sao       	=	cr_E_block[1] ;
				cr_p_3_5_in_sao       	=	8'd0 ;
				cr_p_4_0_in_sao       	=	cr_decision_col_left[3] ;
				cr_p_4_1_in_sao       	=	cr_D_block[2] ;
				cr_p_4_2_in_sao       	=	cr_D_block[3] ;
				cr_p_4_3_in_sao       	=	cr_E_block[2] ;
				cr_p_4_4_in_sao       	=	cr_E_block[3] ;
				cr_p_4_5_in_sao       	=	8'd0 ;
				cr_p_5_0_in_sao       	=	cr_decision_col_left[4] ;
				cr_p_5_1_in_sao       	=	cr_q_block_1[0] ;
				cr_p_5_2_in_sao       	=	cr_q_block_1[1] ;
				cr_p_5_3_in_sao       	=	cr_q_block_2[0] ;
				cr_p_5_4_in_sao       	=	cr_q_block_2[1] ;
				cr_p_5_5_in_sao       	=	8'd0 ;
			end
			STATE_9 : begin
				if((Xc_in>0) && (Yc_in==(pic_height_in - 8)) && (H_block_cb_sao_apply==1)) begin
					start_cb_sao_filter  =  1'b1 ;
				end
				if((Xc_in>0) && (Yc_in==(pic_height_in - 8)) && (H_block_cr_sao_apply==1)) begin
					start_cr_sao_filter  =  1'b1 ;
				end
				cross_boundary_left_flag_sao   		  =	  H_block_cross_boundary_left		  ;
				cross_boundary_right_flag_sao         =	  H_block_cross_boundary_right		  ;
				cross_boundary_top_flag_sao	          =	  H_block_cross_boundary_top		  ;
				cross_boundary_bottom_flag_sao        =	  H_block_cross_boundary_bottom	      ;
				cross_boundary_top_left_flag_sao	  =	  H_block_cross_boundary_top_left	  ;
				cross_boundary_top_right_flag_sao	  =	  H_block_cross_boundary_top_right	  ;
				cross_boundary_bottom_left_flag_sao   =	  H_block_cross_boundary_bottom_left  ;
				cross_boundary_bottom_right_flag_sao  =	  H_block_cross_boundary_bottom_right ;
				cb_sao_type_in_sao	   	=	H_block_cb_sao_type     ;
				cb_sao_eo_class_in_sao	=	H_block_cb_eoclass      ;
				cb_sao_bandpos_in_sao 	=	H_block_cb_bandpos      ;
				cb_sao_offset_1_in_sao	=	H_block_cb_sao_offset_1 ;
				cb_sao_offset_2_in_sao	=	H_block_cb_sao_offset_2 ;
				cb_sao_offset_3_in_sao	=	H_block_cb_sao_offset_3 ;
				cb_sao_offset_4_in_sao	=	H_block_cb_sao_offset_4 ;
				cb_p_0_0_in_sao			=	cb_decision_row[0] ;
				cb_p_0_1_in_sao       	=	cb_decision_row[1] ;
				cb_p_0_2_in_sao       	=	cb_decision_row[2] ;
				cb_p_0_3_in_sao       	=	cb_decision_row[3] ;
				cb_p_0_4_in_sao       	=	cb_decision_row[4] ;
				cb_p_0_5_in_sao       	=	cb_decision_row[5] ;
				cb_p_1_0_in_sao       	=	cb_decision_col_left[0] ;
				cb_p_1_1_in_sao       	=	cb_H_block[0] ;
				cb_p_1_2_in_sao       	=	cb_H_block[1] ;
				cb_p_1_3_in_sao       	=	cb_A_block[0] ;
				cb_p_1_4_in_sao       	=	cb_A_block[1] ;
				cb_p_1_5_in_sao       	=	cb_q_block_1[0] ;
				cb_p_2_0_in_sao       	=	cb_decision_col_left[1] ;
				cb_p_2_1_in_sao       	=	cb_H_block[2] ;
				cb_p_2_2_in_sao       	=	cb_H_block[3] ;
				cb_p_2_3_in_sao       	=	cb_A_block[2] ;
				cb_p_2_4_in_sao       	=	cb_A_block[3] ;
				cb_p_2_5_in_sao       	=	cb_q_block_1[2] ;
				cb_p_3_0_in_sao       	=	cb_decision_col_left[2] ;
				cb_p_3_1_in_sao       	=	cb_I_block[0] ;
				cb_p_3_2_in_sao       	=	cb_I_block[1] ;
				cb_p_3_3_in_sao       	=	cb_B_block[0] ;
				cb_p_3_4_in_sao       	=	cb_B_block[1] ;
				cb_p_3_5_in_sao       	=	cb_q_block_3[0] ;
				cb_p_4_0_in_sao       	=	cb_decision_col_left[3] ;
				cb_p_4_1_in_sao       	=	cb_I_block[2] ;
				cb_p_4_2_in_sao       	=	cb_I_block[3] ;
				cb_p_4_3_in_sao       	=	cb_B_block[2] ;
				cb_p_4_4_in_sao       	=	cb_B_block[3] ;
				cb_p_4_5_in_sao       	=	cb_q_block_3[2] ;
				cb_p_5_0_in_sao       	=	8'd0 ;
				cb_p_5_1_in_sao       	=	8'd0 ;
				cb_p_5_2_in_sao       	=	8'd0 ;
				cb_p_5_3_in_sao       	=	8'd0 ;
				cb_p_5_4_in_sao       	=	8'd0 ;
				cb_p_5_5_in_sao       	=	8'd0 ;
				cr_sao_type_in_sao	   	=	H_block_cr_sao_type     ;
				cr_sao_eo_class_in_sao	=	H_block_cr_eoclass      ;
				cr_sao_bandpos_in_sao 	=	H_block_cr_bandpos      ;
				cr_sao_offset_1_in_sao	=	H_block_cr_sao_offset_1 ;
				cr_sao_offset_2_in_sao	=	H_block_cr_sao_offset_2 ;
				cr_sao_offset_3_in_sao	=	H_block_cr_sao_offset_3 ;
				cr_sao_offset_4_in_sao	=	H_block_cr_sao_offset_4 ;
				cr_p_0_0_in_sao			=	cr_decision_row[0] ;
				cr_p_0_1_in_sao       	=	cr_decision_row[1] ;
				cr_p_0_2_in_sao       	=	cr_decision_row[2] ;
				cr_p_0_3_in_sao       	=	cr_decision_row[3] ;
				cr_p_0_4_in_sao       	=	cr_decision_row[4] ;
				cr_p_0_5_in_sao       	=	cr_decision_row[5] ;
				cr_p_1_0_in_sao       	=	cr_decision_col_left[0] ;
				cr_p_1_1_in_sao       	=	cr_H_block[0] ;
				cr_p_1_2_in_sao       	=	cr_H_block[1] ;
				cr_p_1_3_in_sao       	=	cr_A_block[0] ;
				cr_p_1_4_in_sao       	=	cr_A_block[1] ;
				cr_p_1_5_in_sao       	=	cr_q_block_1[0] ;
				cr_p_2_0_in_sao       	=	cr_decision_col_left[1] ;
				cr_p_2_1_in_sao       	=	cr_H_block[2] ;
				cr_p_2_2_in_sao       	=	cr_H_block[3] ;
				cr_p_2_3_in_sao       	=	cr_A_block[2] ;
				cr_p_2_4_in_sao       	=	cr_A_block[3] ;
				cr_p_2_5_in_sao       	=	cr_q_block_1[2] ;
				cr_p_3_0_in_sao       	=	cr_decision_col_left[2] ;
				cr_p_3_1_in_sao       	=	cr_I_block[0] ;
				cr_p_3_2_in_sao       	=	cr_I_block[1] ;
				cr_p_3_3_in_sao       	=	cr_B_block[0] ;
				cr_p_3_4_in_sao       	=	cr_B_block[1] ;
				cr_p_3_5_in_sao       	=	cr_q_block_3[0] ;
				cr_p_4_0_in_sao       	=	cr_decision_col_left[3] ;
				cr_p_4_1_in_sao       	=	cr_I_block[2] ;
				cr_p_4_2_in_sao       	=	cr_I_block[3] ;
				cr_p_4_3_in_sao       	=	cr_B_block[2] ;
				cr_p_4_4_in_sao       	=	cr_B_block[3] ;
				cr_p_4_5_in_sao       	=	cr_q_block_3[2] ;
				cr_p_5_0_in_sao       	=	8'd0 ;
				cr_p_5_1_in_sao       	=	8'd0 ;
				cr_p_5_2_in_sao       	=	8'd0 ;
				cr_p_5_3_in_sao       	=	8'd0 ;
				cr_p_5_4_in_sao       	=	8'd0 ;
				cr_p_5_5_in_sao       	=	8'd0 ;
			end
			STATE_11 : begin
				if((Xc_in==(pic_width_in - 8)) && (Yc_in==(pic_height_in - 8)) && (q1_block_cb_sao_apply==1)) begin
					start_cb_sao_filter  =  1'b1 ;
				end
				if((Xc_in==(pic_width_in - 8)) && (Yc_in==(pic_height_in - 8)) && (q1_block_cr_sao_apply==1)) begin
					start_cr_sao_filter  =  1'b1 ;
				end
				cross_boundary_left_flag_sao   		  =	  q_block_cross_boundary_left		  ;
				cross_boundary_right_flag_sao         =	  q_block_cross_boundary_right		  ;
				cross_boundary_top_flag_sao	          =	  q_block_cross_boundary_top		  ;
				cross_boundary_bottom_flag_sao        =	  q_block_cross_boundary_bottom	      ;
				cross_boundary_top_left_flag_sao	  =	  q_block_cross_boundary_top_left	  ;
				cross_boundary_top_right_flag_sao	  =	  q_block_cross_boundary_top_right	  ;
				cross_boundary_bottom_left_flag_sao   =	  q_block_cross_boundary_bottom_left  ;
				cross_boundary_bottom_right_flag_sao  =	  q_block_cross_boundary_bottom_right ;
				cb_sao_type_in_sao	   	=	cb_sao_type_in	   ;
				cb_sao_eo_class_in_sao	=	cb_eoclass_in      ;
				cb_sao_bandpos_in_sao 	=	cb_bandpos_in      ;
				cb_sao_offset_1_in_sao	=	cb_sao_offset_1_in ;
				cb_sao_offset_2_in_sao	=	cb_sao_offset_2_in ;
				cb_sao_offset_3_in_sao	=	cb_sao_offset_3_in ;
				cb_sao_offset_4_in_sao	=	cb_sao_offset_4_in ;
				cb_p_0_0_in_sao			=	cb_decision_row[0] ;
				cb_p_0_1_in_sao       	=	cb_decision_row[1] ;
				cb_p_0_2_in_sao       	=	cb_decision_row[2] ;
				cb_p_0_3_in_sao       	=	cb_decision_row[3] ;
				cb_p_0_4_in_sao       	=	cb_decision_row[4] ;
				cb_p_0_5_in_sao       	=	cb_decision_row[5] ;
				cb_p_1_0_in_sao       	=	cb_decision_col_left[0] ;
				cb_p_1_1_in_sao       	=	cb_q_block_1[0] ;
				cb_p_1_2_in_sao       	=	cb_q_block_1[1] ;
				cb_p_1_3_in_sao       	=	cb_q_block_2[0] ;
				cb_p_1_4_in_sao       	=	cb_q_block_2[1] ;
				cb_p_1_5_in_sao       	=	8'd0 ;
				cb_p_2_0_in_sao       	=	cb_decision_col_left[1] ;
				cb_p_2_1_in_sao       	=	cb_q_block_1[2] ;
				cb_p_2_2_in_sao       	=	cb_q_block_1[3] ;
				cb_p_2_3_in_sao       	=	cb_q_block_2[2] ;
				cb_p_2_4_in_sao       	=	cb_q_block_2[3] ;
				cb_p_2_5_in_sao       	=	8'd0 ;
				cb_p_3_0_in_sao       	=	cb_decision_col_left[2] ;
				cb_p_3_1_in_sao       	=	cb_q_block_3[0] ;
				cb_p_3_2_in_sao       	=	cb_q_block_3[1] ;
				cb_p_3_3_in_sao       	=	cb_q_block_4[0] ;
				cb_p_3_4_in_sao       	=	cb_q_block_4[1] ;
				cb_p_3_5_in_sao       	=	8'd0 ;
				cb_p_4_0_in_sao       	=	cb_decision_col_left[3] ;
				cb_p_4_1_in_sao       	=	cb_q_block_3[2] ;
				cb_p_4_2_in_sao       	=	cb_q_block_3[3] ;
				cb_p_4_3_in_sao       	=	cb_q_block_4[2] ;
				cb_p_4_4_in_sao       	=	cb_q_block_4[3] ;
				cb_p_4_5_in_sao       	=	8'd0 ;
				cb_p_5_0_in_sao       	=	8'd0 ;
				cb_p_5_1_in_sao       	=	8'd0 ;
				cb_p_5_2_in_sao       	=	8'd0 ;
				cb_p_5_3_in_sao       	=	8'd0 ;
				cb_p_5_4_in_sao       	=	8'd0 ;
				cb_p_5_5_in_sao       	=	8'd0 ;
				cr_sao_type_in_sao	   	=	cr_sao_type_in	   ;
				cr_sao_eo_class_in_sao	=	cr_eoclass_in      ;
				cr_sao_bandpos_in_sao 	=	cr_bandpos_in      ;
				cr_sao_offset_1_in_sao	=	cr_sao_offset_1_in ;
				cr_sao_offset_2_in_sao	=	cr_sao_offset_2_in ;
				cr_sao_offset_3_in_sao	=	cr_sao_offset_3_in ;
				cr_sao_offset_4_in_sao	=	cr_sao_offset_4_in ;
				cr_p_0_0_in_sao			=	cr_decision_row[0] ;
				cr_p_0_1_in_sao       	=	cr_decision_row[1] ;
				cr_p_0_2_in_sao       	=	cr_decision_row[2] ;
				cr_p_0_3_in_sao       	=	cr_decision_row[3] ;
				cr_p_0_4_in_sao       	=	cr_decision_row[4] ;
				cr_p_0_5_in_sao       	=	cr_decision_row[5] ;
				cr_p_1_0_in_sao       	=	cr_decision_col_left[0] ;
				cr_p_1_1_in_sao       	=	cr_q_block_1[0] ;
				cr_p_1_2_in_sao       	=	cr_q_block_1[1] ;
				cr_p_1_3_in_sao       	=	cr_q_block_2[0] ;
				cr_p_1_4_in_sao       	=	cr_q_block_2[1] ;
				cr_p_1_5_in_sao       	=	8'd0 ;
				cr_p_2_0_in_sao       	=	cr_decision_col_left[1] ;
				cr_p_2_1_in_sao       	=	cr_q_block_1[2] ;
				cr_p_2_2_in_sao       	=	cr_q_block_1[3] ;
				cr_p_2_3_in_sao       	=	cr_q_block_2[2] ;
				cr_p_2_4_in_sao       	=	cr_q_block_2[3] ;
				cr_p_2_5_in_sao       	=	8'd0 ;
				cr_p_3_0_in_sao       	=	cr_decision_col_left[2] ;
				cr_p_3_1_in_sao       	=	cr_q_block_3[0] ;
				cr_p_3_2_in_sao       	=	cr_q_block_3[1] ;
				cr_p_3_3_in_sao       	=	cr_q_block_4[0] ;
				cr_p_3_4_in_sao       	=	cr_q_block_4[1] ;
				cr_p_3_5_in_sao       	=	8'd0 ;
				cr_p_4_0_in_sao       	=	cr_decision_col_left[3] ;
				cr_p_4_1_in_sao       	=	cr_q_block_3[2] ;
				cr_p_4_2_in_sao       	=	cr_q_block_3[3] ;
				cr_p_4_3_in_sao       	=	cr_q_block_4[2] ;
				cr_p_4_4_in_sao       	=	cr_q_block_4[3] ;
				cr_p_4_5_in_sao       	=	8'd0 ;
				cr_p_5_0_in_sao       	=	8'd0 ;
				cr_p_5_1_in_sao       	=	8'd0 ;
				cr_p_5_2_in_sao       	=	8'd0 ;
				cr_p_5_3_in_sao       	=	8'd0 ;
				cr_p_5_4_in_sao       	=	8'd0 ;
				cr_p_5_5_in_sao       	=	8'd0 ;
			end
		endcase
	end
	
	
	//--------------- Combinational block to read and write to column buffer BRAM -----------------------
	
	always @(*) begin
		col_we_a 			= 	1'b0 ;
		col_en_a 			= 	1'b0 ;
		col_addr_a 			= 	{10{1'bx}} ;
		col_we_b 			= 	1'b0 ;
		col_en_b 			= 	1'b0 ;
		col_addr_b 			= 	{10{1'bx}} ;
		col_cb_data_in_a	=	{SAMPLE_COL_BUF_DATA_WIDTH{1'bx}} ;
		col_cb_data_in_b	=	{SAMPLE_COL_BUF_DATA_WIDTH{1'bx}} ;
		col_cr_data_in_a	=	{SAMPLE_COL_BUF_DATA_WIDTH{1'bx}} ;
		col_cr_data_in_b	=	{SAMPLE_COL_BUF_DATA_WIDTH{1'bx}} ;
		
		case(state)
			STATE_1 : begin
				if((Xc_in>0) && (Yc_in>0)) begin		// G block read
					col_we_a 		= 	1'b0 ;
					col_en_a 		= 	1'b1 ;
					col_addr_a 		= 	(Yc_in[11:2] - 1'b1) ;
				end
				if(Xc_in>0) begin						// H block read
					col_we_b 		= 	1'b0 ;
					col_en_b 		= 	1'b1 ;
					col_addr_b 		= 	Yc_in[11:2] ;
				end
			end
			STATE_2 : begin
				if((Xc_in>0) && (Yc_in == (pic_height_in - 8))) begin		// I block read
					col_we_a 		= 	1'b0 ;
					col_en_a 		= 	1'b1 ;
					col_addr_a 		= 	(Yc_in[11:2] + 1'b1) ;
				end
			end
			STATE_3 : begin
				if(Yc_in>0) begin								// D block write
					col_we_a 			= 	1'b1 ;
					col_en_a 			= 	1'b1 ;
					col_addr_a 			= 	(Yc_in[11:2] - 1'b1) ;
					col_cb_data_in_a	=	{	
												cb_D_block[ 3] ,
												cb_D_block[ 2] ,
												cb_D_block[ 1] ,
												cb_D_block[ 0]
											} ;
					col_cr_data_in_a	=	{	
												cr_D_block[ 3] ,
												cr_D_block[ 2] ,
												cr_D_block[ 1] ,
												cr_D_block[ 0]
											} ;						
				end
				col_we_b 			= 	1'b1 ;				// q1 block write
				col_en_b 			= 	1'b1 ;
				col_addr_b 			= 	Yc_in[11:2] ;
				col_cb_data_in_b	=	{	
											1'b0 ,
											pcm_bypass_sao_disable_in ,
											loop_filter_accross_slices_enabled_in ,
											slice_top_boundary_in  ,
											slice_left_boundary_in ,
											tile_top_boundary_in   ,
											tile_left_boundary_in  ,
											sao_chroma_in          ,
											cb_sao_offset_4_in     ,
											cb_sao_offset_3_in     ,
											cb_sao_offset_2_in     ,
											cb_sao_offset_1_in     ,
											cb_bandpos_in          ,
											cb_eoclass_in          ,
											cb_sao_type_in         ,
											cb_q_block_1[ 3] ,
											cb_q_block_1[ 2] ,
											cb_q_block_1[ 1] ,
											cb_q_block_1[ 0]
										} ;
				col_cr_data_in_b	=	{	
											slice_id_in			,
											cr_sao_offset_4_in  ,
											cr_sao_offset_3_in  ,
											cr_sao_offset_2_in  ,
											cr_sao_offset_1_in  ,
											cr_bandpos_in       ,
											cr_eoclass_in       ,
											cr_sao_type_in      ,
											cr_q_block_1[ 3] ,
											cr_q_block_1[ 2] ,
											cr_q_block_1[ 1] ,
											cr_q_block_1[ 0]
										} ;						
			end
			STATE_4 : begin
				if(Yc_in == (pic_height_in - 8)) begin
					col_we_a 			= 	1'b1 ;
					col_en_a 			= 	1'b1 ;
					col_addr_a 			= 	(Yc_in[11:2] + 1'b1) ;
					col_cb_data_in_a	=	{	
												cb_q_block_3[ 3] ,
												cb_q_block_3[ 2] ,
												cb_q_block_3[ 1] ,
												cb_q_block_3[ 0]
											} ;
					col_cr_data_in_a	=	{	
												cr_q_block_3[ 3] ,
												cr_q_block_3[ 2] ,
												cr_q_block_3[ 1] ,
												cr_q_block_3[ 0]
											} ;						
				end
			end
		endcase
	end
	
	
	//--------------- Combinational block to read and write to row buffer BRAM -----------------------
	
	always @(*) begin
		row_we_a 			= 	1'b0 ;
		row_en_a 			= 	1'b0 ;
		row_addr_a 			= 	{9{1'bx}} ;
		row_cb_data_in_a	=	{SAMPLE_ROW_BUF_DATA_WIDTH{1'bx}} ;
		row_cr_data_in_a	=	{SAMPLE_ROW_BUF_DATA_WIDTH{1'bx}} ;
		
		case(state)
			STATE_1 : begin
				if((Xc_in>0) && (Yc_in>0)) begin			// J,K blocks read
					row_we_a 		= 	1'b0 ;
					row_en_a 		= 	1'b1 ;
					row_addr_a 		= 	(Xc_in[11:3] - 1'b1) ;
				end
			end
			STATE_2 : begin
				if((Xc_in == (pic_width_in - 8)) && (Yc_in>0)) begin		// M,N blocks read
					row_we_a 		= 	1'b0 ;
					row_en_a 		= 	1'b1 ;
					row_addr_a 		= 	Xc_in[11:3] ;
				end
			end
			STATE_3 : begin
				if(Xc_in>0) begin									// H,A blocks write
					row_we_a 			= 	1'b1 ;
					row_en_a 			= 	1'b1 ;
					row_addr_a 			= 	(Xc_in[11:3] - 1'b1) ;
					row_cb_data_in_a	=	{	
												1'b0 ,
												H_block_pcm_bypass_sao_disable_flag ,
												H_block_sao_across_slices_enable_flag ,
												H_block_slice_top_flag  ,
												H_block_slice_left_flag ,
												H_block_tile_top_flag   ,
												H_block_tile_left_flag  ,
												H_block_sao_chroma      ,
												H_block_cb_sao_offset_4 ,
												H_block_cb_sao_offset_3 ,
												H_block_cb_sao_offset_2 ,
												H_block_cb_sao_offset_1 ,
												H_block_cb_bandpos      ,
												H_block_cb_eoclass      ,
												H_block_cb_sao_type     ,
												cb_A_block[ 3] ,
												cb_A_block[ 2] ,
												cb_A_block[ 1] ,
												cb_A_block[ 0] ,
												cb_H_block[ 3] ,
												cb_H_block[ 2] ,
												cb_H_block[ 1] ,
												cb_H_block[ 0]
											} ;
					row_cr_data_in_a	=	{	
												H_block_slice_id		,
												H_block_cr_sao_offset_4 ,
												H_block_cr_sao_offset_3 ,
												H_block_cr_sao_offset_2 ,
												H_block_cr_sao_offset_1 ,
												H_block_cr_bandpos      ,
												H_block_cr_eoclass      ,
												H_block_cr_sao_type     ,
												cr_A_block[ 3] ,
												cr_A_block[ 2] ,
												cr_A_block[ 1] ,
												cr_A_block[ 0] ,
												cr_H_block[ 3] ,
												cr_H_block[ 2] ,
												cr_H_block[ 1] ,
												cr_H_block[ 0]
											} ;
				end
			end
			STATE_4 : begin
				if(Xc_in == (pic_width_in - 8)) begin						// q1,q2 blocks write
					row_we_a 			= 	1'b1 ;
					row_en_a 			= 	1'b1 ;
					row_addr_a 			= 	Xc_in[11:3] ;
					row_cb_data_in_a	=	{	
												1'b0 ,
												pcm_bypass_sao_disable_in ,
												loop_filter_accross_slices_enabled_in ,
												slice_top_boundary_in  ,
												slice_left_boundary_in ,
												tile_top_boundary_in   ,
												tile_left_boundary_in  ,
												sao_chroma_in          ,
												cb_sao_offset_4_in     ,
												cb_sao_offset_3_in     ,
												cb_sao_offset_2_in     ,
												cb_sao_offset_1_in     ,
												cb_bandpos_in          ,
												cb_eoclass_in          ,
												cb_sao_type_in         ,
												cb_q_block_2[ 3] ,
												cb_q_block_2[ 2] ,
												cb_q_block_2[ 1] ,
												cb_q_block_2[ 0] ,
												cb_q_block_1[ 3] ,
												cb_q_block_1[ 2] ,
												cb_q_block_1[ 1] ,
												cb_q_block_1[ 0]
											} ;
					row_cr_data_in_a	=	{	
												slice_id_in 		,
												cr_sao_offset_4_in  ,
												cr_sao_offset_3_in  ,
												cr_sao_offset_2_in  ,
												cr_sao_offset_1_in  ,
												cr_bandpos_in       ,
												cr_eoclass_in       ,
												cr_sao_type_in      ,
												cr_q_block_2[ 3] ,
												cr_q_block_2[ 2] ,
												cr_q_block_2[ 1] ,
												cr_q_block_2[ 0] ,
												cr_q_block_1[ 3] ,
												cr_q_block_1[ 2] ,
												cr_q_block_1[ 1] ,
												cr_q_block_1[ 0]
											} ;
				end
			end
		endcase
	end
	
	
	//----------- Combinational block to read and write to decision buffer BRAM ----------------------
	
	always @(*) begin
		d_row_buf_we_a       			=	1'b0 ;
		d_row_buf_en_a       			=	1'b0 ;
		d_row_buf_addr_a     			=	{9{1'bx}} ;
		d_row_buf_cb_data_in_a  		=	{DECISION_ROW_BUF_DATA_WIDTH{1'bx}} ;
		d_row_buf_cr_data_in_a  		=	{DECISION_ROW_BUF_DATA_WIDTH{1'bx}} ;
		d_left_col_buf_we_a      		=	1'b0 ;
		d_left_col_buf_en_a      		=	1'b0 ;
		d_left_col_buf_addr_a    		=	{9{1'bx}} ;
		d_left_col_buf_cb_data_in_a 	=	{LEFT_DECISION_COL_BUF_DATA_WIDTH{1'bx}} ;
		d_left_col_buf_cr_data_in_a 	=	{LEFT_DECISION_COL_BUF_DATA_WIDTH{1'bx}} ;
		d_right_col_buf_we_a      		=	1'b0 ;
		d_right_col_buf_en_a      		=	1'b0 ;
		d_right_col_buf_addr_a    		=	{9{1'bx}} ;
		d_right_col_buf_cb_data_in_a 	=	{RIGHT_DECISION_COL_BUF_DATA_WIDTH{1'bx}} ;
		d_right_col_buf_cr_data_in_a 	=	{RIGHT_DECISION_COL_BUF_DATA_WIDTH{1'bx}} ;
		
		case(state)
			STATE_1 : begin
				if((Xc_in>0) && (Yc_in>8)) begin			// read decision row buffer
					d_row_buf_we_a   	=	1'b0 ;
					d_row_buf_en_a   	=	1'b1 ;
				end
				d_row_buf_addr_a 	=	(Xc_in[11:3] - 1'b1) ;
				if((Xc_in>8) && (Yc_in>0)) begin			// read left decision column buffer
					d_left_col_buf_we_a    	=	1'b0 ;
					d_left_col_buf_en_a    	=	1'b1 ;
				end
				d_left_col_buf_addr_a  	=	(Yc_in[11:3] - 1'b1) ;
				if((Xc_in>0) && (Yc_in>0)) begin			// read right decision column buffer
					d_right_col_buf_we_a   	=	1'b0 ;
					d_right_col_buf_en_a   	=	1'b1 ;
				end
				d_right_col_buf_addr_a 	=	Xc_in[11:3] ;		// Actually horizontally addressed
			end
			STATE_4 : begin
				if((Xc_in>0) && (Yc_in>0)) begin			// write to decision row buffer
					d_row_buf_we_a   	=	1'b1 ;
					d_row_buf_en_a   	=	1'b1 ;
				end
				d_row_buf_addr_a 	=	(Xc_in[11:3] - 1'b1) ;
				d_row_buf_cb_data_in_a  =	{
												M_block_slice_id[3:0] ,
												J_block_slice_left_flag ,
												cb_D_block[2] ,
												cb_C_block[3] ,
												cb_C_block[2] ,
												cb_G_block[3] ,
												cb_G_block[2] ,
												cb_decision_col_left[3]
											} ;
				d_row_buf_cr_data_in_a  =	{
												M_block_slice_id[7:4] ,
												M_block_sao_across_slices_enable_flag ,
												cr_D_block[2] ,
												cr_C_block[3] ,
												cr_C_block[2] ,
												cr_G_block[3] ,
												cr_G_block[2] ,
												cr_decision_col_left[3]
											} ;
				if((Xc_in>0) && (Yc_in>0)) begin			// write to left decision column buffer
					d_left_col_buf_we_a    	=	1'b1 ;
					d_left_col_buf_en_a    	=	1'b1 ;
				end
				d_left_col_buf_addr_a  	=	(Yc_in[11:3] - 1'b1) ;
				d_left_col_buf_cb_data_in_a  =	{
													H_block_slice_id[3:0] ,
													H_block_sao_across_slices_enable_flag ,
													cb_A_block[1]  ,
													cb_C_block[3]  ,
													cb_C_block[1]  ,
													cb_K_block[3]  ,
													cb_K_block[1] 
												} ;
				d_left_col_buf_cr_data_in_a  =	{
													1'b0 ,
													H_block_slice_id[7:4] ,
													cr_A_block[1]  ,
													cr_C_block[3]  ,
													cr_C_block[1]  ,
													cr_K_block[3]  ,
													cr_K_block[1] 
												} ;
				d_right_col_buf_we_a   	=	1'b1 ;			// write to right decision column buffer
				d_right_col_buf_en_a   	=	1'b1 ;
				d_right_col_buf_addr_a 	=	Xc_in[11:3] ;
				d_right_col_buf_cb_data_in_a  	=  	{
														slice_id_in[3:0] ,
														loop_filter_accross_slices_enabled_in ,
														cb_q_block_1[2],
														cb_q_block_1[0] 
													} ;
				d_right_col_buf_cr_data_in_a  	=  	{
														slice_id_in[7:4] ,
														slice_left_boundary_in ,
														cr_q_block_1[2],
														cr_q_block_1[0] 
													} ;
			end
			STATE_5 : begin
				if((Xc_in==(pic_width_in - 8)) && (Yc_in>8)) begin			// read decision row buffer
					d_row_buf_we_a   	=	1'b0 ;
					d_row_buf_en_a   	=	1'b1 ;
				end
				d_row_buf_addr_a 	=	Xc_in[11:3] ;
				if((Xc_in==(pic_width_in - 8)) && (Yc_in>0)) begin			// read left decision column buffer
					d_left_col_buf_we_a    	=	1'b0 ;
					d_left_col_buf_en_a    	=	1'b1 ;
				end
				d_left_col_buf_addr_a  	=	(Yc_in[11:3] - 1'b1) ;
			end
			STATE_6 : begin
				if((Xc_in==(pic_width_in - 8)) && (Yc_in>0)) begin			// write to decision row buffer
					d_row_buf_we_a   	=	1'b1 ;
					d_row_buf_en_a   	=	1'b1 ;
				end
				d_row_buf_addr_a 	=	Xc_in[11:3] ;
				d_row_buf_cb_data_in_a  =	{
												4'd0 ,
												M_block_slice_left_flag ,
												8'd0 ,
												cb_E_block[3] ,
												cb_E_block[2] ,
												cb_D_block[3] ,
												cb_D_block[2] ,
												cb_C_block[3]
											} ;
				d_row_buf_cr_data_in_a  =	{
												4'd0 ,
												1'b0 ,
												8'd0 ,
												cr_E_block[3] ,
												cr_E_block[2] ,
												cr_D_block[3] ,
												cr_D_block[2] ,
												cr_C_block[3]
											} ;
			end
			STATE_7 : begin
				if((Xc_in>0) && (Yc_in==(pic_height_in - 8))) begin			// read decision row buffer
					d_row_buf_we_a   	=	1'b0 ;
					d_row_buf_en_a   	=	1'b1 ;
				end
				d_row_buf_addr_a 	=	(Xc_in[11:3] - 1'b1) ;
				if((Xc_in>8) && (Yc_in==(pic_height_in - 8))) begin			// read left decision column buffer
					d_left_col_buf_we_a    	=	1'b0 ;
					d_left_col_buf_en_a    	=	1'b1 ;
				end
				d_left_col_buf_addr_a  	=	Yc_in[11:3] ;
			end
			STATE_8 : begin
				if((Xc_in>0) && (Yc_in==(pic_height_in - 8))) begin			// write to left decision column buffer
					d_left_col_buf_we_a    	=	1'b1 ;
					d_left_col_buf_en_a    	=	1'b1 ;
				end
				d_left_col_buf_addr_a  	=	Yc_in[11:3] ;
				d_left_col_buf_cb_data_in_a  =	{
													4'd0 ,
													1'b0 ,
													8'd0 ,
													cb_B_block[3] ,
													cb_B_block[1] ,
													cb_A_block[3] ,
													cb_A_block[1] 
												} ;
				d_left_col_buf_cr_data_in_a  =	{
													1'b0 ,
													4'd0 ,
													8'd0 ,
													cr_B_block[3] ,
													cr_B_block[1] ,
													cr_A_block[3] ,
													cr_A_block[1] 
												} ;
			end
			STATE_9 : begin
				if((Xc_in==(pic_width_in - 8)) && (Yc_in==(pic_height_in - 8))) begin		// read decision row buffer
					d_row_buf_we_a   	=	1'b0 ;
					d_row_buf_en_a   	=	1'b1 ;
				end
				d_row_buf_addr_a 	=	Xc_in[11:3] ;
				if((Xc_in==(pic_width_in - 8)) && (Yc_in==(pic_height_in - 8))) begin		// read left decision column buffer
					d_left_col_buf_we_a    	=	1'b0 ;
					d_left_col_buf_en_a    	=	1'b1 ;
				end
				d_left_col_buf_addr_a  	=	Yc_in[11:3] ;
			end
		endcase
	end
	
	

endmodule
