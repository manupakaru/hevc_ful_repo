`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:27:21 11/23/2013 
// Design Name: 
// Module Name:    read_data_stage
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module read_data_stage(
        clk,
        reset,
        ready_in,
        valid_out,


        //bs_interface
        bs_fifo_data_in        ,
        bs_fifo_wr_en_in       ,
        bs_fifo_full_out       ,

        //luma dbf fifo interface

        cnfig_dbf_fifo_is_full_out,
        cnfig_dbf_fifo_data_in,
        cnfig_dbf_fifo_wr_en_in,
        
        y_dbf_fifo_is_full_out,
        y_dbf_fifo_data_in,
        y_dbf_fifo_wr_en_in,
        
        //cb dbf fifo interface
        cb_dbf_fifo_is_full_out,
        cb_dbf_fifo_data_in,
        cb_dbf_fifo_wr_en_in,
        
        //cr dbf fifo interface
        cr_dbf_fifo_is_full_out,
        cr_dbf_fifo_data_in,
        cr_dbf_fifo_wr_en_in,

        y_q_block_1_out, 
        y_q_block_2_out, 
        y_q_block_3_out, 
        y_q_block_4_out, 
        y_A_block_out,   
        y_B_block_out,   
        cb_q_block_1_out,
        cb_q_block_2_out,
        cb_q_block_3_out,
        cb_q_block_4_out,
        cb_A_block_out,
        cb_B_block_out,
        cr_q_block_1_out,
        cr_q_block_2_out,
        cr_q_block_3_out,
        cr_q_block_4_out,
        cr_A_block_out, 
        cr_B_block_out,
        Xc_out,
        Yc_out,
        qp_1_out,
        qp_A_out,
        BS_v1_out,
        BS_v2_out,
        BS_h1_out,
        BS_h2_out,
        BS_h0_out,  


        // parameter set
        pps_cb_qp_offset,
        pps_cr_qp_offset,
        slice_tc_offset_div2,
        slice_beta_offset_div2,
        pic_width,
        pic_height,
        ctb_size,
		h0_edge_dbf_disable,
		pcm_bypass_AB_dbf_disable,
        slice_dbf_disable,
		pcm_loop_filter_disabled,
		loop_filter_accross_tiles_enabled,
		loop_filter_accross_slices_enabled,
		tile_x  ,
		tile_y  ,
		slice_x ,
		slice_y ,
		actual_slice_x,
		actual_slice_y,
        poc_out,
		current_dpb_idx_out,
        tile_id,
        slice_id,
		sao_luma,
		sao_chroma,
		pcm,
		bypass,
        y_sao_type,
        y_bandpos_or_eoclass,
        y_sao_offset_1,
        y_sao_offset_2,
        y_sao_offset_3,
        y_sao_offset_4,
        cb_sao_type,
        cb_bandpos_or_eoclass,
        cb_sao_offset_1,
        cb_sao_offset_2,
        cb_sao_offset_3,
        cb_sao_offset_4,
        cr_sao_type,
        cr_bandpos_or_eoclass,
        cr_sao_offset_1,
        cr_sao_offset_2,
        cr_sao_offset_3,
        cr_sao_offset_4

		,pred_2_dbf_rd_inf 
		,pred_2_dbf_yy 
		,pred_2_dbf_cb 
		,pred_2_dbf_cr 
		,read_state_q2_we 
	
    );

    `include "../sim/pred_def.v"
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES :
     * Define all localparams after parameters
     */

    localparam                          FIFO_IN_WIDTH       = 32;
    localparam                          FIFO_OUT_WIDTH      = 32;  
    localparam                          Y_FIFO_WIDTH        = 134;
    localparam                          CH_FIFO_WIDTH        = 128;


     
    localparam                          STATE_READ_WAIT             = 0;    // the state that is entered upon reset
    localparam                          STATE_ACTIVE                = 1;
    localparam                          STATE_READ_Y1_SET_R_ADDR_AB = 2;
    localparam                          STATE_READ_Y2               = 3;
    localparam                          STATE_READ_Y3_BS_LOAD_AB    = 4;
    localparam                          STATE_READ_Y4_CH_WRITE_Y2_Y4 = 5;
    localparam                          STATE_POC_READ_WAIT = 6;
    localparam                          STATE_CURRENT_POC = 7;
    localparam                          STATE_ERROR = 8;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input                       clk;
    input                       reset;
    
    // handshake signals to and from the stage below
    input                       ready_in;           // connected to ready out of the stage below
    output	reg					valid_out;          // connected to data valid in of the stage below
    
    // FIFO queue inputs and outputs
    input  [BS_FIFO_WIDTH-1:0]    bs_fifo_data_in        ;
    input                         bs_fifo_wr_en_in       ;
    output                        bs_fifo_full_out       ;
        
    output                                                cnfig_dbf_fifo_is_full_out;
    input  [FIFO_IN_WIDTH-1:0]                            cnfig_dbf_fifo_data_in;
    input                                                 cnfig_dbf_fifo_wr_en_in;
	
    output                                                y_dbf_fifo_is_full_out;
    input  [Y_FIFO_WIDTH-1:0]                             y_dbf_fifo_data_in;
    input                                                 y_dbf_fifo_wr_en_in;
	
    output                                                 cb_dbf_fifo_is_full_out;
    input    [CH_FIFO_WIDTH-1:0]                           cb_dbf_fifo_data_in;
    input                                                  cb_dbf_fifo_wr_en_in;
	
    output                                               cr_dbf_fifo_is_full_out;
    input  [CH_FIFO_WIDTH-1:0]                           cr_dbf_fifo_data_in;
    input                                                cr_dbf_fifo_wr_en_in; 
    
    // Outputs to Main DB stage

 
    output  reg     [127:0]     			y_q_block_1_out ;
    output  reg     [127:0]     			y_q_block_2_out ;
    output  reg     [127:0]     			y_q_block_3_out ;
    output  reg     [127:0]     			y_q_block_4_out ;
    output  reg     [127:0]     			y_A_block_out   ;
    output  reg     [127:0]     			y_B_block_out   ;
	
    output  reg     [31:0]      			cb_q_block_1_out ;
    output  reg     [31:0]      			cb_q_block_2_out ;
    output  reg     [31:0]      			cb_q_block_3_out ;
    output  reg     [31:0]      			cb_q_block_4_out ;
    output  reg     [31:0]      			cb_A_block_out   ;
    output  reg     [31:0]      			cb_B_block_out   ;
	
    output  reg     [31:0]      			cr_q_block_1_out ;
    output  reg     [31:0]      			cr_q_block_2_out ;
    output  reg     [31:0]      			cr_q_block_3_out ;
    output  reg     [31:0]      			cr_q_block_4_out ;
    output  reg     [31:0]      			cr_A_block_out   ;
    output  reg     [31:0]      			cr_B_block_out    ;
    
    output  reg     [X11_ADDR_WDTH -1:LOG2_MIN_DU_SIZE]                  Xc_out ;
    output  reg     [X11_ADDR_WDTH -1:LOG2_MIN_DU_SIZE]                  Yc_out ;
    output  reg     [QP_WIDTH-1:0]			qp_1_out;
	output  reg     [QP_WIDTH-1:0]			qp_A_out;
	output  reg     [BS_WIDTH-1:0]          BS_v1_out;
    output  reg     [BS_WIDTH-1:0]          BS_v2_out;
    output  reg     [BS_WIDTH-1:0]          BS_h1_out;
    output  reg     [BS_WIDTH-1:0]          BS_h2_out;
    output  reg     [BS_WIDTH-1:0]          BS_h0_out;  

    output reg         [PIC_DIM_WIDTH-1:0]                 pic_width;
    output reg         [PIC_DIM_WIDTH-1:0]                 pic_height;
    output reg         [CTB_SIZE_WIDTH : 0]             	ctb_size;

	output  reg     [PPS_CB_QP_OFFSET_WIDTH-1:0]				    pps_cb_qp_offset;  // for deblocking, only pps_cb_qp_offset is used
    output  reg     [PPS_CR_QP_OFFSET_WIDTH-1:0]				    pps_cr_qp_offset;
	output  reg     [PPS_TC_OFFSET_DIV2_WIDTH-1:0]				    slice_tc_offset_div2;
    output  reg     [PPS_BETA_OFFSET_DIV2_WIDTH-1:0]                slice_beta_offset_div2;

    output	reg												h0_edge_dbf_disable;
	output	reg												pcm_bypass_AB_dbf_disable;
	output 	reg 											slice_dbf_disable;
	output	reg												pcm_loop_filter_disabled;
	output	reg												loop_filter_accross_tiles_enabled;
	output	reg												loop_filter_accross_slices_enabled;
	output	reg		[XC_WIDTH-1:0]							tile_x  ;
	output	reg		[XC_WIDTH-1:0]							tile_y  ;
	output	reg		[XC_WIDTH-1:0]							slice_x ;
	output	reg		[XC_WIDTH-1:0]							slice_y ;
	output	reg		[XC_WIDTH-1:0]							actual_slice_x ;
	output	reg		[XC_WIDTH-1:0]							actual_slice_y ;
    
    output       	[31:0]                 	poc_out;
	output 	reg     [DPB_ADDR_WIDTH-1:0]   	current_dpb_idx_out;
    output  reg     [TILE_ID_WIDTH-1:0]				         tile_id;
    output  reg     [SLICE_ID_WIDTH-1:0]				    slice_id;
    
    output  reg                        	    sao_luma;
	output  reg                        	    sao_chroma;
	output  reg                        	    pcm;
	output  reg                        	    bypass;
	output  reg     [SAOTYPE_WIDTH -1:0]                        y_sao_type;
    output  reg     [BANDPOS_EO_WIDTH -1:0]                     y_bandpos_or_eoclass;
    output  reg     [7:0]                   y_sao_offset_1;
    output  reg     [7:0]                   y_sao_offset_2;
    output  reg     [7:0]                   y_sao_offset_3;
    output  reg     [7:0]                   y_sao_offset_4;
    output  reg     [SAOTYPE_WIDTH -1:0]                      cb_sao_type;
    output  reg     [BANDPOS_EO_WIDTH -1:0]                   cb_bandpos_or_eoclass;
    output  reg     [7:0]                   cb_sao_offset_1;
    output  reg     [7:0]                   cb_sao_offset_2;
    output  reg     [7:0]                   cb_sao_offset_3;
    output  reg     [7:0]                   cb_sao_offset_4;
    output  reg     [SAOTYPE_WIDTH -1:0]                      cr_sao_type;
    output  reg     [BANDPOS_EO_WIDTH -1:0]                   cr_bandpos_or_eoclass;
    output  reg     [7:0]                   cr_sao_offset_1;
    output  reg     [7:0]                   cr_sao_offset_2;
    output  reg     [7:0]                   cr_sao_offset_3;
    output  reg     [7:0]                   cr_sao_offset_4;
	
	output [7:0] pred_2_dbf_rd_inf ;
	output [134-1:0] pred_2_dbf_yy ;
	output [128-1:0] pred_2_dbf_cb ;
	output [128-1:0] pred_2_dbf_cr ;
	
	output read_state_q2_we;
						
	
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------



    
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    
    // Signals for luma BRAM column buffer
    reg                             y_we_a;  
    reg                             y_en_a;
    reg             [7:0]           y_addr_a;
    reg             [265:0]         y_data_in_a;
    wire            [265:0]         y_data_out_a;
    
    // Signals for chroma BRAM column buffers
    reg                             cb_we_a;  
    reg                             cb_en_a;
    reg             [7:0]           cb_addr_a;
    reg             [63:0]          cb_data_in_a;
    wire            [63:0]          cb_data_out_a;
    
    reg                             cr_we_a;  
    reg                             cr_en_a;
    reg             [7:0]           cr_addr_a;
    reg             [63:0]          cr_data_in_a;
    wire            [63:0]          cr_data_out_a;
	 
	integer							state;


    wire                                        y_dbf_fifo_is_empty;
    wire                                        y_dbf_fifo_is_prog_empty;
    wire [Y_FIFO_WIDTH-1:0]                     y_dbf_fifo_data;
    reg                                         y_dbf_fifo_rd_en_out;
            
    wire                                        cb_dbf_fifo_is_empty;
    wire  [CH_FIFO_WIDTH-1:0]                   cb_dbf_fifo_data;
    reg                                         cb_dbf_fifo_rd_en_out;
            
    wire                                        cr_dbf_fifo_is_empty;
    wire  [CH_FIFO_WIDTH-1:0]                   cr_dbf_fifo_data;
    reg                                         cr_dbf_fifo_rd_en_out;

    reg  [X11_ADDR_WDTH - 1:0]     ctb_xx;
    reg  [X11_ADDR_WDTH - 1:0]     ctb_yy;

    wire [FIFO_IN_WIDTH-1:0] cnfig_dbf_fifo_data_out;
    reg     config_fifo_rd_en;
    wire    is_config_fifo_empty;
    wire    is_config_fifo_prog_empty;

    wire      	[LOG2_CTB_WIDTH - 1: 0]      	log2_ctb_size;
	wire		[XC_WIDTH-1:0]					Xc;
	wire		[YC_WIDTH-1:0]					Yc;
	
	reg                                			tile_top_boundary  ;
	reg											slice_top_boundary ;
	wire										tile_top_loop_filter_disable  ;
	wire										slice_top_loop_filter_disable ;
	wire										hor_edge_dbf_disable;
	wire                            			pcm_bypass_dbf_disable;	

    reg 		[CTB_SIZE_WIDTH-3 -1:0] 		du_size;
    wire 		[DU_SIZE_WIDTH-1:0] 			log2_du_size;

    wire		[1:0]				sao_type ;
	wire		[5:0]				bandpos_or_eoclass ;
	wire 							sao_sign_1 	;
    wire 							sao_sign_2 	;
    wire 							sao_sign_3 	;
    wire 							sao_sign_4 	;
    wire 		[2:0]				sao_abs_1 	;
    wire 		[2:0]				sao_abs_2 	;
    wire 		[2:0]				sao_abs_3 	;
    wire 		[2:0]				sao_abs_4 	; 
	wire 		[3:0]				n_sao_abs_1 ;
    wire 		[3:0]				n_sao_abs_2 ;
    wire 		[3:0]				n_sao_abs_3 ;
    wire 		[3:0]				n_sao_abs_4 ;   
	
	reg     	[SAO_OFFSET_WIDTH -1:0]     	sao_offset_1 ;
	reg     	[SAO_OFFSET_WIDTH -1:0]     	sao_offset_2 ;
	reg     	[SAO_OFFSET_WIDTH -1:0]     	sao_offset_3 ;
	reg     	[SAO_OFFSET_WIDTH -1:0]     	sao_offset_4 ;

    wire  [BS_FIFO_WIDTH-1:0]   bs_fifo_data_out        ;
    reg                         bs_fifo_rd_en       ;
    wire                        bs_fifo_is_empty       ;

    reg     [PPS_TC_OFFSET_DIV2_WIDTH-1:0]                      pps_tc_offset_div2;
    reg     [PPS_BETA_OFFSET_DIV2_WIDTH-1:0]                    pps_beta_offset_div2;
    
    
    reg [CTB_SIZE_WIDTH-1:0] du_xx, du_yy;
    reg end_of_du;
    reg         [POC_WIDTH -1: 0]                   current__poc;
    
    integer next_state;
    
	integer wait_count;
    assign read_state_q2_we = y_we_a;
	
	
    // assign poc_out = current__poc[3:0];
    assign poc_out = current__poc;

    assign log2_du_size = cnfig_dbf_fifo_data_out[HEADER_WIDTH + DU_SIZE_WIDTH-1: HEADER_WIDTH];
	
	assign	sao_type  			= 	cnfig_dbf_fifo_data_out[HEADER_WIDTH + SAOTYPE_WIDTH -1: 
                                                            HEADER_WIDTH];
	
    assign  bandpos_or_eoclass  =   cnfig_dbf_fifo_data_out[HEADER_WIDTH + SAOTYPE_WIDTH + BANDPOS_EO_WIDTH -1: 
                                                            HEADER_WIDTH + SAOTYPE_WIDTH];
	
    assign sao_abs_1  = cnfig_dbf_fifo_data_out[HEADER_WIDTH + SAOTYPE_WIDTH + BANDPOS_EO_WIDTH + SAO_OFFSET_ABS_0_WIDTH -1:
                                                HEADER_WIDTH + SAOTYPE_WIDTH + BANDPOS_EO_WIDTH ];

    assign sao_sign_1 = cnfig_dbf_fifo_data_out[HEADER_WIDTH + SAOTYPE_WIDTH + BANDPOS_EO_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH -1:
                                                HEADER_WIDTH + SAOTYPE_WIDTH + BANDPOS_EO_WIDTH + SAO_OFFSET_ABS_0_WIDTH ];

    assign sao_abs_2  = cnfig_dbf_fifo_data_out[HEADER_WIDTH + SAOTYPE_WIDTH + BANDPOS_EO_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH -1:
                                                HEADER_WIDTH + SAOTYPE_WIDTH + BANDPOS_EO_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH ];

    assign sao_sign_2 = cnfig_dbf_fifo_data_out[HEADER_WIDTH + SAOTYPE_WIDTH + BANDPOS_EO_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH -1:
                                                HEADER_WIDTH + SAOTYPE_WIDTH + BANDPOS_EO_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH];

    assign sao_abs_3  = cnfig_dbf_fifo_data_out[HEADER_WIDTH + SAOTYPE_WIDTH + BANDPOS_EO_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH -1:
                                                HEADER_WIDTH + SAOTYPE_WIDTH + BANDPOS_EO_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH];

    assign sao_sign_3 = cnfig_dbf_fifo_data_out[HEADER_WIDTH + SAOTYPE_WIDTH + BANDPOS_EO_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH -1:
                                                HEADER_WIDTH + SAOTYPE_WIDTH + BANDPOS_EO_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH];

    assign sao_abs_4  = cnfig_dbf_fifo_data_out[HEADER_WIDTH + SAOTYPE_WIDTH + BANDPOS_EO_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH -1:
                                                HEADER_WIDTH + SAOTYPE_WIDTH + BANDPOS_EO_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH];

    assign sao_sign_4 = cnfig_dbf_fifo_data_out[HEADER_WIDTH + SAOTYPE_WIDTH + BANDPOS_EO_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH -1:
                                                 HEADER_WIDTH + SAOTYPE_WIDTH + BANDPOS_EO_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH + SAO_OFFSET_SIGN_0_WIDTH + SAO_OFFSET_ABS_0_WIDTH];
	
	assign	n_sao_abs_1  =  {1'b1,~sao_abs_1} + 4'b1 ;
	assign	n_sao_abs_2  =  {1'b1,~sao_abs_2} + 4'b1 ;
	assign	n_sao_abs_3  =  {1'b1,~sao_abs_3} + 4'b1 ;
	assign	n_sao_abs_4  =  {1'b1,~sao_abs_4} + 4'b1 ;
	
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
    
    BRAM_col_db_luma luma_column_buffer(
        .clk(clk),
        .we_a(y_we_a),
        .en_a(y_en_a),
        .addr_a(y_addr_a),
        .data_in_a(y_data_in_a),
        .data_out_a(y_data_out_a)
    );
    
    BRAM_col_db_chroma cb_column_buffer(
        .clk(clk),
        .we_a(cb_we_a),
        .en_a(cb_en_a),
        .addr_a(cb_addr_a),
        .data_in_a(cb_data_in_a),
        .data_out_a(cb_data_out_a)
    );
    
    BRAM_col_db_chroma cr_column_buffer(
        .clk(clk),
        .we_a(cr_we_a),
        .en_a(cr_en_a),
        .addr_a(cr_addr_a),
        .data_in_a(cr_data_in_a),
        .data_out_a(cr_data_out_a)
    );

	
	wire is_config_fifo_full;
		// synthesis translate_off
fifo_inout_monitor 
	#(
		.WIDTH 		  (32),
		.FILE_IN_WIDTH(32), 
		.FILE_NAME    ("pred_to_dbf_config"),
		.OUT_VERIFY	   (1),
		.DEBUG 		   (0)
	)
	dbf_config_fifo_monitor_block (
    .clk(clk), 
    .reset(reset), 
    .in(cnfig_dbf_fifo_data_in), 
    .out(cnfig_dbf_fifo_data_out), 
    .full(is_config_fifo_full), 
    .empty(is_config_fifo_empty), 
    .rd_en(config_fifo_rd_en), 
    .wr_en(cnfig_dbf_fifo_wr_en_in)
    );
// synthesis translate_on

dbf_config_fifo dbf_config_fifo_block (
  .clk(clk), // input clk
  .rst(reset), // input rst
  .din(cnfig_dbf_fifo_data_in), // input [31 : 0] din
  .wr_en(cnfig_dbf_fifo_wr_en_in), // input wr_en
  .rd_en(config_fifo_rd_en), // input rd_en
  .dout(cnfig_dbf_fifo_data_out), // output [31 : 0] dout
  .full(is_config_fifo_full), // output full
  .empty(is_config_fifo_empty), // output empty
  .prog_full(cnfig_dbf_fifo_is_full_out),
	.almost_empty	(is_config_fifo_prog_empty)
);

// synthesis translate_off
pred_to_dbf_y_fifo_monitor
#(
	.WIDTH 			(134),
	.FILE_IN_WIDTH 	(128),
	.FILE_NAME      ("pred_to_dbf_yy"),
	.OUT_VERIFY  	(1),
	.DEBUG 			(0)
)
dbf_yy_fifo_monitor_block
(
    .clk	(clk),
    .reset	(reset),
    .out	(y_dbf_fifo_data),
	.empty	(y_dbf_fifo_is_empty),
	.rd_en	(y_dbf_fifo_rd_en_out)
);
// synthesis translate_on

dbf_yy_fifo dbf_in_y_fifo (
  .clk(clk), // input clk
  .rst(reset), // input rst
  .din(y_dbf_fifo_data_in), // input [134 : 0] din
  .wr_en(y_dbf_fifo_wr_en_in), // input wr_en
  .rd_en(y_dbf_fifo_rd_en_out), // input rd_en
  .dout( {y_dbf_fifo_data}), // output [134 : 0] dout
  .full(), // output full
  .empty(y_dbf_fifo_is_empty), // output empty
  .prog_full(y_dbf_fifo_is_full_out) // output prog_full
  ,.almost_empty	(y_dbf_fifo_is_prog_empty)
);

	wire is_dbf_cb_fifo_full;
		// synthesis translate_off
fifo_inout_monitor 
	#(
		.WIDTH 		  (128),
		.FILE_IN_WIDTH(128), 
		.FILE_NAME    ("pred_to_dbf_cb"),
		.OUT_VERIFY	   	(1),
		.DEBUG			(0)
	)
	dbf_cb_fifo_monitor_block (
    .clk(clk), 
    .reset(reset), 
    .in(cb_dbf_fifo_data_in), 
    .out(cb_dbf_fifo_data), 
    .full(is_dbf_cb_fifo_full), 
    .empty(cb_dbf_fifo_is_empty), 
    .rd_en(cb_dbf_fifo_rd_en_out), 
    .wr_en(cb_dbf_fifo_wr_en_in)
    );
// synthesis translate_on

dbf_ch_fifo dbf_in_cb_fifo (
  .clk(clk), // input clk
  .rst(reset), // input rst
  .din({cb_dbf_fifo_data_in}), // input [127 : 0] din
  .wr_en(cb_dbf_fifo_wr_en_in), // input wr_en
  .rd_en(cb_dbf_fifo_rd_en_out), // input rd_en
  .dout(cb_dbf_fifo_data), // output [127 : 0] dout
  .full(is_dbf_cb_fifo_full), // output full
  .empty(cb_dbf_fifo_is_empty), // output empty
  .prog_full(cb_dbf_fifo_is_full_out) // output prog_full
);


	wire is_dbf_cr_fifo_full;
		// synthesis translate_off
fifo_inout_monitor 
	#(
		.WIDTH 		  (128),
		.FILE_IN_WIDTH(128), 
		.FILE_NAME    ("pred_to_dbf_cr"),
		.OUT_VERIFY	   (1),
		.DEBUG		   (0)
	)
	dbf_cr_fifo_monitor_block (
    .clk(clk), 
    .reset(reset), 
    .in(cr_dbf_fifo_data_in), 
    .out(cr_dbf_fifo_data), 
    .full(is_dbf_cr_fifo_full), 
    .empty(cr_dbf_fifo_is_empty), 
    .rd_en(cr_dbf_fifo_rd_en_out), 
    .wr_en(cr_dbf_fifo_wr_en_in)
    );
// synthesis translate_on


dbf_ch_fifo dbf_in_cr_fifo (
  .clk(clk), // input clk
  .rst(reset), // input rst
  .din({cr_dbf_fifo_data_in}), // input [127 : 0] din
  .wr_en(cr_dbf_fifo_wr_en_in), // input wr_en
  .rd_en(cr_dbf_fifo_rd_en_out), // input rd_en
  .dout(cr_dbf_fifo_data), // output [127 : 0] dout
  .full(is_dbf_cr_fifo_full), // output full
  .empty(cr_dbf_fifo_is_empty), // output empty
  .prog_full(cr_dbf_fifo_is_full_out) // output prog_full
);


	wire is_bs_fifo_full;
		// synthesis translate_off
fifo_inout_monitor 
	#(
		.WIDTH 		  (8),
		.FILE_IN_WIDTH(8), 
		.FILE_NAME    ("pred_to_dbf_bs"),
		.OUT_VERIFY	   (0),
		.DEBUG		   (0)
	)
	dbf_bs_fifo_monitor_block (
    .clk(clk), 
    .reset(reset), 
    .in({bs_fifo_data_in[01:00],bs_fifo_data_in[03:02],bs_fifo_data_in[05:04],bs_fifo_data_in[07:06]}), 
    .out({bs_fifo_data_out[01:00],bs_fifo_data_out[03:02],bs_fifo_data_out[05:04],bs_fifo_data_out[07:06]}), 
    .full(is_bs_fifo_full), 
    .empty(bs_fifo_is_empty), 
    .rd_en(bs_fifo_rd_en), 
    .wr_en(bs_fifo_wr_en_in)
    );
	// synthesis translate_on
	
	
dbf_bs_fifo dbf_bs_fifo_block (
  .clk(clk), // input clk
  .rst(reset), // input rst
  .din(bs_fifo_data_in), // input [145 : 0] din
  .wr_en(bs_fifo_wr_en_in), // input wr_en
  .rd_en(bs_fifo_rd_en), // input rd_en
  .dout( bs_fifo_data_out), // output [145 : 0] dout
  .almost_full(bs_fifo_full_out), // output full
  .full(is_bs_fifo_full),
  .empty(bs_fifo_is_empty) // output empty
);

	assign pred_2_dbf_rd_inf = {config_fifo_rd_en,y_dbf_fifo_rd_en_out,cb_dbf_fifo_rd_en_out,cr_dbf_fifo_rd_en_out
								,is_config_fifo_prog_empty,y_dbf_fifo_is_prog_empty,cb_dbf_fifo_is_empty,cr_dbf_fifo_is_empty};
    
	assign pred_2_dbf_yy = y_dbf_fifo_data;
	assign pred_2_dbf_cb = cb_dbf_fifo_data;
	assign pred_2_dbf_cr = cr_dbf_fifo_data;
	
	
	always @(*) begin
		if(sao_type == 2'd2) begin
			sao_offset_1  =  {1'b0,sao_abs_1} ;
			sao_offset_2  =  {1'b0,sao_abs_2} ;
			sao_offset_3  =  n_sao_abs_3 ;
			sao_offset_4  =  n_sao_abs_4 ;
		end
		else begin
			sao_offset_1  =  (sao_sign_1 == 0)  ?  {1'b0,sao_abs_1}  :  n_sao_abs_1 ;
			sao_offset_2  =  (sao_sign_2 == 0)  ?  {1'b0,sao_abs_2}  :  n_sao_abs_2 ;
			sao_offset_3  =  (sao_sign_3 == 0)  ?  {1'b0,sao_abs_3}  :  n_sao_abs_3 ;
			sao_offset_4  =  (sao_sign_4 == 0)  ?  {1'b0,sao_abs_4}  :  n_sao_abs_4 ;
		end
	end


  always@(*) begin
      if ( ((du_xx[5:3] + du_size) == (Xc_out[CTB_SIZE_WIDTH-1: LOG2_MIN_DU_SIZE] + 1'b1)) && ((du_yy[5:3] + du_size) == (Yc_out[CTB_SIZE_WIDTH-1: LOG2_MIN_DU_SIZE] + 1'b1))) begin
          end_of_du = 1;
      end
      else begin
          end_of_du = 0;
      end
  end


    assign log2_ctb_size  =  cnfig_dbf_fifo_data_out[ HEADER_WIDTH + LOG2CTBSIZEY_WIDTH -1 : HEADER_WIDTH];

   always@(posedge clk) begin
        if(reset) begin
            state <= STATE_READ_WAIT;
        end
        else begin
            state <= next_state;
        end
    end


	
always @(posedge clk ) begin : pred_main_fsm
        if(reset) begin
            current__poc <= {POC_WIDTH{1'b1}};  
            pps_cb_qp_offset <= 0;
            pps_cr_qp_offset <= 0;
            pps_tc_offset_div2 <= 0;
            pps_beta_offset_div2 <= 0;
			loop_filter_accross_tiles_enabled <= 0;
			loop_filter_accross_slices_enabled <= 0;
			slice_dbf_disable <= 0;
			tile_x  <= 0 ;
			tile_y  <= 0 ;
			slice_x <= 0 ;
			slice_y <= 0 ;
			actual_slice_x <= 0;
			actual_slice_y <= 0;
			
			
            y_we_a  <= 1'b0;
            y_en_a  <= 1'b0;
            cb_we_a <= 1'b0;
            cb_en_a <= 1'b0;
            cr_we_a <= 1'b0;
            cr_en_a <= 1'b0;
            valid_out <= 1'b0;
			wait_count <= 0;
        end 
        else begin
            case(state)                
                STATE_READ_WAIT: begin
                    y_we_a  <= 1'b0;
                    y_en_a  <= 1'b0;
                    cb_we_a <= 1'b0;
                    cb_en_a <= 1'b0;
                    cr_we_a <= 1'b0;
                    cr_en_a <= 1'b0;
                    valid_out <= 1'b0;
					if(is_config_fifo_prog_empty == 0) begin
						wait_count <= wait_count + 1;
						if(wait_count == 12512) begin
							wait_count <= 0;
						end
					end
                end
                STATE_ACTIVE: begin
                    y_we_a  <= 1'b0;
                    y_en_a  <= 1'b0;
                    cb_we_a <= 1'b0;
                    cb_en_a <= 1'b0;
                    cr_we_a <= 1'b0;
                    cr_en_a <= 1'b0;
                    valid_out <= 1'b0;
                    case(cnfig_dbf_fifo_data_out[HEADER_WIDTH -1:0])

                        `HEADER_PARAMETERS_0: begin
                            pic_width <=   (cnfig_dbf_fifo_data_out[PIC_WIDTH_WIDTH + HEADER_WIDTH -1: HEADER_WIDTH]);
                            pic_height  <=   (cnfig_dbf_fifo_data_out[HEADER_WIDTH + PIC_WIDTH_WIDTH + PIC_HEIGHT_WIDTH- 1: HEADER_WIDTH + PIC_WIDTH_WIDTH]);

                        end
                        `HEADER_PARAMETERS_1: begin
							pcm_loop_filter_disabled  <=  cnfig_dbf_fifo_data_out[ PCM_LOOP_FILTER_DISABLED_FLAG_POSITION ] ;
                            pps_cb_qp_offset <=      cnfig_dbf_fifo_data_out[       HEADER_WIDTH + LOG2CTBSIZEY_WIDTH + PPS_CR_QP_OFFSET_WIDTH-1: 
                                                                                    HEADER_WIDTH + LOG2CTBSIZEY_WIDTH   ];           
                            pps_cr_qp_offset <=      cnfig_dbf_fifo_data_out[       HEADER_WIDTH + LOG2CTBSIZEY_WIDTH + PPS_CR_QP_OFFSET_WIDTH + PPS_CR_QP_OFFSET_WIDTH -1: 
                                                                                    HEADER_WIDTH + LOG2CTBSIZEY_WIDTH + PPS_CR_QP_OFFSET_WIDTH  ]; 
                            pps_beta_offset_div2 <=    cnfig_dbf_fifo_data_out[     HEADER_WIDTH + LOG2CTBSIZEY_WIDTH + PPS_CR_QP_OFFSET_WIDTH + PPS_CR_QP_OFFSET_WIDTH + PPS_BETA_OFFSET_DIV2_WIDTH-1: 
                                                                                    HEADER_WIDTH + LOG2CTBSIZEY_WIDTH + PPS_CR_QP_OFFSET_WIDTH + PPS_CR_QP_OFFSET_WIDTH ]; 
                            pps_tc_offset_div2 <=    cnfig_dbf_fifo_data_out[       HEADER_WIDTH + LOG2CTBSIZEY_WIDTH + PPS_CR_QP_OFFSET_WIDTH + PPS_CR_QP_OFFSET_WIDTH + PPS_BETA_OFFSET_DIV2_WIDTH + PPS_TC_OFFSET_DIV2_WIDTH-1: 
                                                                                    HEADER_WIDTH + LOG2CTBSIZEY_WIDTH + PPS_CR_QP_OFFSET_WIDTH + PPS_CR_QP_OFFSET_WIDTH + PPS_BETA_OFFSET_DIV2_WIDTH]; 
                            case(log2_ctb_size)
                                3'd6: begin
                                    ctb_size <= 7'd64;
                                end
                                3'd5: begin
                                    ctb_size <= 7'd32;
                                end
                                3'd4: begin
                                    ctb_size <= 7'd16;
                                end
                                default: begin
                                    ctb_size <= 7'd8;
                                end
                            endcase
                        end
                        `HEADER_PARAMETERS_2: begin
							loop_filter_accross_tiles_enabled  <=  cnfig_dbf_fifo_data_out[LOOP_FILTER_ACCROSS_TILES_FLAG_POSITION] ;
                        end
                        `HEADER_PARAMETERS_3: begin
                           
                        end
                        `HEADER_PARAMETERS_4: begin
                            
                        end
                        `HEADER_PARAMETERS_5: begin
                            
                        end
                        `HEADER_SLICE_0: begin
							// New Picture
                            current_dpb_idx_out <= cnfig_dbf_fifo_data_out[ HEADER_WIDTH + DPB_ADDR_WIDTH -1:
                                                                            HEADER_WIDTH   ];   
                        end

                        `HEADER_SLICE_1: begin
                            sao_luma        <=   cnfig_dbf_fifo_data_out[   HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH - 1:
                                                                            HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH   ];   
                            sao_chroma      <=   cnfig_dbf_fifo_data_out[   HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH - 1:
                                                                            HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH   ];   
                            
                        end
                        `HEADER_SLICE_2: begin
                            slice_dbf_disable <=       	cnfig_dbf_fifo_data_out[       HEADER_WIDTH + PPS_CR_QP_OFFSET_WIDTH + PPS_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH -1: 
                                                                                        HEADER_WIDTH + PPS_CR_QP_OFFSET_WIDTH + PPS_CR_QP_OFFSET_WIDTH ]; 
                            slice_beta_offset_div2 <=    cnfig_dbf_fifo_data_out[       HEADER_WIDTH + PPS_CR_QP_OFFSET_WIDTH + PPS_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH + PPS_BETA_OFFSET_DIV2_WIDTH-1: 
                                                                                        HEADER_WIDTH + PPS_CR_QP_OFFSET_WIDTH + PPS_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH]; 
                            slice_tc_offset_div2 <=      cnfig_dbf_fifo_data_out[       HEADER_WIDTH + PPS_CR_QP_OFFSET_WIDTH + PPS_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH + PPS_BETA_OFFSET_DIV2_WIDTH + PPS_TC_OFFSET_DIV2_WIDTH-1: 
                                                                                        HEADER_WIDTH + PPS_CR_QP_OFFSET_WIDTH + PPS_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH + PPS_BETA_OFFSET_DIV2_WIDTH]; 
							loop_filter_accross_slices_enabled  <=  cnfig_dbf_fifo_data_out[LOOP_FILTER_ACCROSS_SLICES_FLAG_POSITION] ;
                        end
                        `HEADER_SLICE_3: begin 
                            
                        end
                        `HEADER_SLICE_4: begin
                            
                        end
                        `HEADER_SLICE_5: begin
                            // New Slice
                            slice_tc_offset_div2 <= pps_tc_offset_div2;
                            slice_beta_offset_div2 <= pps_beta_offset_div2;
							slice_x  <=  cnfig_dbf_fifo_data_out[ HEADER_WIDTH + XC_WIDTH - 1 : HEADER_WIDTH ] ;
							slice_y  <=  cnfig_dbf_fifo_data_out[ HEADER_WIDTH + XC_WIDTH + YC_WIDTH - 1 : HEADER_WIDTH + XC_WIDTH ] ;
							actual_slice_x  <=  cnfig_dbf_fifo_data_out[ HEADER_WIDTH + XC_WIDTH - 1 : HEADER_WIDTH ] ;
							actual_slice_y  <=  cnfig_dbf_fifo_data_out[ HEADER_WIDTH + XC_WIDTH + YC_WIDTH - 1 : HEADER_WIDTH + XC_WIDTH ] ;
                        end
                        `HEADER_SLICE_6: begin
                            // New Tile
							tile_x  <=  cnfig_dbf_fifo_data_out[ HEADER_WIDTH + XC_WIDTH - 1 : HEADER_WIDTH ] ;
							tile_y  <=  cnfig_dbf_fifo_data_out[ HEADER_WIDTH + XC_WIDTH + YC_WIDTH - 1 : HEADER_WIDTH + XC_WIDTH ] ;
							slice_x  <=  cnfig_dbf_fifo_data_out[ HEADER_WIDTH + XC_WIDTH - 1 : HEADER_WIDTH ] ;
							slice_y  <=  cnfig_dbf_fifo_data_out[ HEADER_WIDTH + XC_WIDTH + YC_WIDTH - 1 : HEADER_WIDTH + XC_WIDTH ] ;
							actual_slice_x  <=  slice_x ;
							actual_slice_y  <=  slice_y ;
                        end    
                        `HEADER_SLICE_7: begin
                            
                        end
                        `HEADER_CTU_0: begin
                            Xc_out[X11_ADDR_WDTH-1:LOG2_MIN_DU_SIZE] <= cnfig_dbf_fifo_data_out[HEADER_WIDTH + X_ADDR_WDTH - 1  -1: HEADER_WIDTH + LOG2_MIN_DU_SIZE];                 // 12-bit input assigned to 11-3 bit output MSB dropped
                            Yc_out[X11_ADDR_WDTH-1:LOG2_MIN_DU_SIZE] <= cnfig_dbf_fifo_data_out[HEADER_WIDTH + 2*X_ADDR_WDTH - 1 -1: HEADER_WIDTH + X_ADDR_WDTH + LOG2_MIN_DU_SIZE];
                        end
                        `HEADER_CTU_1: begin
                            tile_id <=  cnfig_dbf_fifo_data_out[HEADER_WIDTH + TILE_ID_WIDTH -1: HEADER_WIDTH];                 // 12-bit input assigned to 11- bit output MSB dropped
                            slice_id <= cnfig_dbf_fifo_data_out[HEADER_WIDTH + TILE_ID_WIDTH + SLICE_ID_WIDTH -1: HEADER_WIDTH + TILE_ID_WIDTH];
                        
                        end
                        `HEADER_CTU_2_Y: begin
                            y_sao_type 	<=  sao_type ;
                            y_bandpos_or_eoclass  <=  bandpos_or_eoclass ;
							y_sao_offset_1	<=  sao_offset_1 ;
							y_sao_offset_2	<=  sao_offset_2 ;
							y_sao_offset_3	<=  sao_offset_3 ;
							y_sao_offset_4	<=  sao_offset_4 ;
                        end
                        `HEADER_CTU_2_CB: begin
                            cb_sao_type 	<=  sao_type ;
                            cb_bandpos_or_eoclass  <=  bandpos_or_eoclass ;
							cb_sao_offset_1	 <=  sao_offset_1 ;
							cb_sao_offset_2	 <=  sao_offset_2 ;
							cb_sao_offset_3	 <=  sao_offset_3 ;
							cb_sao_offset_4	 <=  sao_offset_4 ;
                        end
                        `HEADER_CTU_2_CR: begin
                            cr_sao_type 	<=  sao_type ;
                            cr_bandpos_or_eoclass  <=  bandpos_or_eoclass ;
							cr_sao_offset_1	 <=  sao_offset_1 ;
							cr_sao_offset_2	 <=  sao_offset_2 ;
							cr_sao_offset_3	 <=  sao_offset_3 ;
							cr_sao_offset_4	 <=  sao_offset_4 ;
                        end
                        `HEADER_CU_0: begin
                        end
                        `HEADER_PU_0: begin
                        end
                        `HEADER_RU_0: begin
                        end
                        `HEADER_RU_1: begin  
                        end   
                        `HEADER_DU_0: begin
                            case(log2_du_size) 
                              3'd3: begin
                                  du_size <= 3'd1;// size 8
                              end
                              3'd4: begin
                                  du_size <= 3'd2; // size 16
                              end
                              3'd5: begin
                                  du_size <= 3'd4;// size 32
                              end
                              default: begin
                                  // synthesis translate_off
                                      $display(" du size should take one of 8,16,32!");
                                      #100;
                                      $stop;
                                  // synthesis translate_on
                              end
                            endcase
                            qp_1_out <= cnfig_dbf_fifo_data_out [   HEADER_WIDTH + DU_SIZE_WIDTH + QP_WIDTH -1: 
                                                                    HEADER_WIDTH + DU_SIZE_WIDTH];
                            pcm     <= cnfig_dbf_fifo_data_out  [   HEADER_WIDTH + DU_SIZE_WIDTH + QP_WIDTH + 1 -1: 
                                                                    HEADER_WIDTH + DU_SIZE_WIDTH + QP_WIDTH];
                            bypass <= cnfig_dbf_fifo_data_out [     HEADER_WIDTH + DU_SIZE_WIDTH + QP_WIDTH + 1 + 1 -1:
                                                                    HEADER_WIDTH + DU_SIZE_WIDTH + QP_WIDTH + 1];

                            du_xx <= cnfig_dbf_fifo_data_out[       HEADER_WIDTH + DU_SIZE_WIDTH + QP_WIDTH + 2 + CTB_SIZE_WIDTH -1:
                                                                    HEADER_WIDTH + DU_SIZE_WIDTH + QP_WIDTH + 2];
                            du_yy <= cnfig_dbf_fifo_data_out[       HEADER_WIDTH + DU_SIZE_WIDTH + QP_WIDTH + 2 + CTB_SIZE_WIDTH + CTB_SIZE_WIDTH -1:
                                                                    HEADER_WIDTH + DU_SIZE_WIDTH + QP_WIDTH + 2 + CTB_SIZE_WIDTH];
                                                                                                            
                        end 
                        default: begin
                        end
                    endcase
                end
                STATE_CURRENT_POC: begin
                    current__poc <=  cnfig_dbf_fifo_data_out[    FIFO_IN_WIDTH - 1:
                                                FIFO_IN_WIDTH - FIFO_OUT_WIDTH];
                end
                STATE_READ_Y1_SET_R_ADDR_AB: begin
                    valid_out <= 1'b0;
                    if(y_dbf_fifo_is_prog_empty ==0) begin
                        {Yc_out[CTB_SIZE_WIDTH-1: LOG2_MIN_DU_SIZE], Xc_out[CTB_SIZE_WIDTH-1: LOG2_MIN_DU_SIZE] } <= y_dbf_fifo_data[ PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE + DBF_SAMPLE_XY_ADDR + DBF_SAMPLE_XY_ADDR - 1:PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE] ;
                        y_q_block_1_out <= y_dbf_fifo_data[PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE-1:0];
                        
                        // Read blocks A and B from BRAM
                        y_addr_a <= {Yc_out[X11_ADDR_WDTH-1:CTB_SIZE_WIDTH], y_dbf_fifo_data[ PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE + DBF_SAMPLE_XY_ADDR + DBF_SAMPLE_XY_ADDR - 1:DBF_SAMPLE_XY_ADDR + PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE] };
                        cb_addr_a <= {Yc_out[X11_ADDR_WDTH-1:CTB_SIZE_WIDTH], y_dbf_fifo_data[ PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE + DBF_SAMPLE_XY_ADDR + DBF_SAMPLE_XY_ADDR - 1:DBF_SAMPLE_XY_ADDR + PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE] };
                        cr_addr_a <= {Yc_out[X11_ADDR_WDTH-1:CTB_SIZE_WIDTH], y_dbf_fifo_data[ PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE + DBF_SAMPLE_XY_ADDR + DBF_SAMPLE_XY_ADDR - 1:DBF_SAMPLE_XY_ADDR + PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE] };
                        y_we_a  <= 1'b0;
                        y_en_a  <= 1'b1;
                        cb_we_a <= 1'b0;
                        cb_en_a <= 1'b1;
                        cr_we_a <= 1'b0;
                        cr_en_a <= 1'b1;
                    end
                    else begin
                        y_we_a  <= 1'b0;
                        y_en_a  <= 1'b0;
                        cb_we_a <= 1'b0;
                        cb_en_a <= 1'b0;
                        cr_we_a <= 1'b0;
                        cr_en_a <= 1'b0;
                    end
                end
                STATE_READ_Y2: begin
                    if(y_dbf_fifo_is_prog_empty == 0) begin
                        y_q_block_2_out <= y_dbf_fifo_data[PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE-1:0];
                    end
                end
                STATE_READ_Y3_BS_LOAD_AB: begin
                    if((y_dbf_fifo_is_prog_empty ==0) && (bs_fifo_is_empty ==0) )begin
                        y_q_block_3_out <= y_dbf_fifo_data[PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE-1:0];
                        { {BS_h1_out},  {BS_h2_out},    {BS_v1_out},    {BS_v2_out}} <= bs_fifo_data_out;

                        h0_edge_dbf_disable			<=	y_data_out_a [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE*2 + 2 + QP_WIDTH + 1];
						pcm_bypass_AB_dbf_disable	<=	y_data_out_a [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE*2 + 2 + QP_WIDTH];
						qp_A_out   <=   y_data_out_a        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE*2 + 2 + QP_WIDTH -1:PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE*2 + 2];
                        BS_h0_out  <=   y_data_out_a        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE*2 + 2 -1:PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE*2];
                        y_A_block_out   <=  y_data_out_a    [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE*2 -1:PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE]; 
                        y_B_block_out   <=  y_data_out_a    [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE -1:0];
                        cb_A_block_out  <=  cb_data_out_a[63:32];
                        cb_B_block_out  <=  cb_data_out_a[31:0];
                        cr_A_block_out  <=  cr_data_out_a[63:32];
                        cr_B_block_out  <=  cr_data_out_a[31:0];
                    end
                end
                STATE_READ_Y4_CH_WRITE_Y2_Y4: begin
                    if((y_dbf_fifo_is_prog_empty ==0) && (cb_dbf_fifo_is_empty == 0) && ( cr_dbf_fifo_is_empty == 0) ) begin         // reading chroma last to ensure minimum waiting 
                        if(ready_in) begin
                            y_q_block_4_out <= y_dbf_fifo_data[PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE-1:0];
                            cb_q_block_1_out <= {cb_dbf_fifo_data[16*3-1:16*2],cb_dbf_fifo_data[16*1-1:16*0]};
                            cb_q_block_2_out <= {cb_dbf_fifo_data[16*4-1:16*3],cb_dbf_fifo_data[16*2-1:16*1]};
                            cb_q_block_3_out <= {cb_dbf_fifo_data[16*7-1:16*6],cb_dbf_fifo_data[16*5-1:16*4]};
                            cb_q_block_4_out <= {cb_dbf_fifo_data[16*8-1:16*7],cb_dbf_fifo_data[16*6-1:16*5]};
                            
                            cr_q_block_1_out <= {cr_dbf_fifo_data[16*3-1:16*2],cr_dbf_fifo_data[16*1-1:16*0]};
                            cr_q_block_2_out <= {cr_dbf_fifo_data[16*4-1:16*3],cr_dbf_fifo_data[16*2-1:16*1]};
                            cr_q_block_3_out <= {cr_dbf_fifo_data[16*7-1:16*6],cr_dbf_fifo_data[16*5-1:16*4]};
                            cr_q_block_4_out <= {cr_dbf_fifo_data[16*8-1:16*7],cr_dbf_fifo_data[16*6-1:16*5]};

                            y_data_in_a <= {      	hor_edge_dbf_disable,
                                                    pcm_bypass_dbf_disable,	 
                                                    qp_1_out,
                                                    BS_h2_out,
                                                    y_q_block_2_out,
                                                    y_dbf_fifo_data[PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE-1:0]
                                                } ;

                            cb_data_in_a <= { cb_dbf_fifo_data[16*4-1:16*3],cb_dbf_fifo_data[16*2-1:16*1], cb_dbf_fifo_data[16*8-1:16*7],cb_dbf_fifo_data[16*6-1:16*5] } ;
                            cr_data_in_a <= { cr_dbf_fifo_data[16*4-1:16*3],cr_dbf_fifo_data[16*2-1:16*1], cr_dbf_fifo_data[16*8-1:16*7],cr_dbf_fifo_data[16*6-1:16*5] } ;
                            cr_we_a <= 1'b1;
                            cb_we_a <= 1'b1;
                            y_we_a <= 1'b1;
                            valid_out <= 1'b1;
                        end
                    end
                end
            endcase
        end 
    end


always@(*) begin    : next_state_logic
        next_state = state;                
        config_fifo_rd_en = 1'b0;
        cr_dbf_fifo_rd_en_out = 1'b0;
        cb_dbf_fifo_rd_en_out = 1'b0;
        y_dbf_fifo_rd_en_out = 1'b0;
        bs_fifo_rd_en = 1'b0;
        case(state) 
            STATE_READ_WAIT: begin
                if(is_config_fifo_prog_empty == 0) begin
					if(wait_count == 12512) begin
						next_state = STATE_ACTIVE;
					end
                end
            end
            STATE_POC_READ_WAIT: begin
                if(is_config_fifo_prog_empty == 0) begin
                    next_state = STATE_CURRENT_POC;
                end
            end    
            STATE_CURRENT_POC: begin
                config_fifo_rd_en = 1'b1;
				if(is_config_fifo_prog_empty == 0) begin
                    next_state = STATE_ACTIVE;
                end
				else begin
					next_state = STATE_READ_WAIT;
				end
            end        
            STATE_ACTIVE: begin
                case(cnfig_dbf_fifo_data_out[HEADER_WIDTH- 1:0])

                    `HEADER_PARAMETERS_0: begin
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end
                        config_fifo_rd_en = 1;
                    end
                    `HEADER_PARAMETERS_1: begin
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                        config_fifo_rd_en = 1;
                    end
                    `HEADER_PARAMETERS_2: begin
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end  
                        config_fifo_rd_en = 1;
                    end
                    `HEADER_PARAMETERS_3: begin
                        config_fifo_rd_en = 1;
                        
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end                          
                    end
                    `HEADER_PARAMETERS_4: begin
                        config_fifo_rd_en = 1;
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;                                
                        end
                    end
                    `HEADER_PARAMETERS_5: begin
                        config_fifo_rd_en = 1;
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;                                
                        end
                    end
                    `HEADER_SLICE_0: begin
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_POC_READ_WAIT;                                
                        end
                        else begin
                            next_state = STATE_CURRENT_POC;
                        end
                            config_fifo_rd_en = 1;
                    end
                    `HEADER_SLICE_1: begin
                        config_fifo_rd_en = 1;
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end
                    `HEADER_SLICE_2: begin
                        config_fifo_rd_en = 1;
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end
                    end
                    `HEADER_SLICE_3: begin
                        config_fifo_rd_en = 1;
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end
                    end
                    `HEADER_SLICE_4: begin
                        config_fifo_rd_en = 1;
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end
                    end 
                    `HEADER_SLICE_5: begin
                        config_fifo_rd_en = 1;
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end                    
                    end
                    `HEADER_SLICE_6: begin
                        config_fifo_rd_en = 1;
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end                    
                    end
                    `HEADER_SLICE_7: begin
                        config_fifo_rd_en = 1;
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end                    
                    end
                    `HEADER_CTU_0: begin
                        config_fifo_rd_en = 1;
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end   
                    end
                    `HEADER_CTU_1: begin
                        config_fifo_rd_en = 1;
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end
                    `HEADER_CTU_2_Y: begin
                        config_fifo_rd_en = 1;
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end
                    `HEADER_CTU_2_CB: begin
                        config_fifo_rd_en = 1;
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end
                    `HEADER_CTU_2_CR: begin
                        config_fifo_rd_en = 1;
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end 
                    end                    
                    `HEADER_CU_0: begin
                        config_fifo_rd_en = 1;
                        if(is_config_fifo_prog_empty == 1) begin
                            next_state = STATE_READ_WAIT;
                        end
                    end
                    `HEADER_PU_0: begin
                        config_fifo_rd_en = 1;
                        next_state = STATE_ERROR;
                    end
                    `HEADER_RU_0: begin
                        config_fifo_rd_en = 1;
                        next_state = STATE_ERROR;
                    end
                    `HEADER_RU_1: begin
                        config_fifo_rd_en = 1;
                        next_state = STATE_ERROR;
                    end    
                    `HEADER_DU_0: begin
                        config_fifo_rd_en = 1;
                        next_state = STATE_READ_Y1_SET_R_ADDR_AB;
                    end                      
                    default: begin
                        config_fifo_rd_en = 1;
                        next_state = STATE_ERROR;
                    end
                endcase
            end
            STATE_READ_Y1_SET_R_ADDR_AB: begin
                if(y_dbf_fifo_is_prog_empty == 0) begin
                    next_state = STATE_READ_Y2;
                    y_dbf_fifo_rd_en_out = 1'b1;
                end
            end
            STATE_READ_Y2: begin
                if(y_dbf_fifo_is_prog_empty == 0) begin
                    next_state = STATE_READ_Y3_BS_LOAD_AB;
                    y_dbf_fifo_rd_en_out = 1'b1;
                end
            end
            STATE_READ_Y3_BS_LOAD_AB: begin
                if((y_dbf_fifo_is_prog_empty ==0) && (bs_fifo_is_empty ==0) )begin
                    next_state = STATE_READ_Y4_CH_WRITE_Y2_Y4;
                    y_dbf_fifo_rd_en_out = 1'b1;
                    bs_fifo_rd_en = 1'b1;
                end
            end
            STATE_READ_Y4_CH_WRITE_Y2_Y4: begin
                if((y_dbf_fifo_is_prog_empty ==0) && (cb_dbf_fifo_is_empty == 0) && ( cr_dbf_fifo_is_empty == 0) ) begin         // reading chroma last to ensure minimum waiting 
                    if(ready_in) begin
                        cr_dbf_fifo_rd_en_out = 1'b1;
                        cb_dbf_fifo_rd_en_out = 1'b1;
                        y_dbf_fifo_rd_en_out = 1'b1;
                        if(end_of_du) begin
                            if(is_config_fifo_prog_empty == 0) begin
                                next_state = STATE_ACTIVE;
                            end
                            else begin
                                next_state = STATE_READ_WAIT;
                            end
                        end
                        else begin
                            next_state = STATE_READ_Y1_SET_R_ADDR_AB;
                        end
                    end
                end
            end
      endcase
  end
  
  
	//-------------- Combinational block to detect tile boundaries ----------------------
	assign	Xc	=	{1'b0,Xc_out,3'b000} ;
	assign	Yc	=	{1'b0,Yc_out,3'b000} ;
	
	always @(*) begin
		if(Yc == tile_y) begin
			tile_top_boundary = 1'b1;
		end
		else begin
			tile_top_boundary = 1'b0;
		end
	end
	
	//-------------- Combinational block to detect slice boundaries ----------------------
	always @(*) begin
		if(Yc == slice_y + ctb_size) begin
			if(Xc < slice_x) begin
				slice_top_boundary = 1'b1;
			end
			else begin
				slice_top_boundary = 1'b0;
			end
		end
		else begin
			if(Yc == slice_y) begin
				slice_top_boundary = 1'b1;
			end
			else begin
				slice_top_boundary = 1'b0;
			end
		end
	end
	
	//-------------- Generate DBF disable condition flags ----------------------
	
	assign	tile_top_loop_filter_disable	=	( tile_top_boundary  & (~loop_filter_accross_tiles_enabled ) ) ;
	assign	slice_top_loop_filter_disable	=	( slice_top_boundary & (~loop_filter_accross_slices_enabled) ) ;
	
	assign	hor_edge_dbf_disable	=	( tile_top_loop_filter_disable  | slice_top_loop_filter_disable  | slice_dbf_disable ) ;
	assign	pcm_bypass_dbf_disable	=	( (pcm & pcm_loop_filter_disabled) | bypass ) ;
	


endmodule
