`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:46:31 06/26/2014 
// Design Name: 
// Module Name:    sao_block_filter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module sao_block_filter(
		clk,
        reset,
		start,
		p_0_0_in,
		p_0_1_in,
		p_0_2_in,
		p_0_3_in,
		p_0_4_in,
		p_0_5_in,
		p_1_0_in,
		p_1_1_in,
		p_1_2_in,
		p_1_3_in,
		p_1_4_in,
		p_1_5_in,
		p_2_0_in,
		p_2_1_in,
		p_2_2_in,
		p_2_3_in,
		p_2_4_in,
		p_2_5_in,
		p_3_0_in,
		p_3_1_in,
		p_3_2_in,
		p_3_3_in,
		p_3_4_in,
		p_3_5_in,
		p_4_0_in,
		p_4_1_in,
		p_4_2_in,
		p_4_3_in,
		p_4_4_in,
		p_4_5_in,
		p_5_0_in,
		p_5_1_in,
		p_5_2_in,
		p_5_3_in,
		p_5_4_in,
		p_5_5_in,
		cross_boundary_left_flag_in,
		cross_boundary_right_flag_in,
		cross_boundary_top_flag_in,
		cross_boundary_bottom_flag_in,
		cross_boundary_top_left_flag_in,
		cross_boundary_top_right_flag_in,
		cross_boundary_bottom_left_flag_in,
		cross_boundary_bottom_right_flag_in,
		sao_type_in,
		sao_eo_class_in,
		sao_bandpos_in,
		sao_offset_1_in,
		sao_offset_2_in,
		sao_offset_3_in,
		sao_offset_4_in,
		pix_1_1_out,
		pix_1_2_out,
		pix_1_3_out,
		pix_1_4_out,
		pix_2_1_out,
		pix_2_2_out,
		pix_2_3_out,
		pix_2_4_out,
		pix_3_1_out,
		pix_3_2_out,
		pix_3_3_out,
		pix_3_4_out,
		pix_4_1_out,
		pix_4_2_out,
		pix_4_3_out,
		pix_4_4_out
    );
	
	`include "../sim/pred_def.v"
	
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input                               clk;
    input                               reset;
	
	input								start;
	
    
	// Input sample values
	input		[BIT_DEPTH-1:0]					p_0_0_in ;
	input		[BIT_DEPTH-1:0]					p_0_1_in ;
	input		[BIT_DEPTH-1:0]					p_0_2_in ;
	input		[BIT_DEPTH-1:0]					p_0_3_in ;
	input		[BIT_DEPTH-1:0]					p_0_4_in ;
	input		[BIT_DEPTH-1:0]					p_0_5_in ;
	input		[BIT_DEPTH-1:0]					p_1_0_in ;
	input		[BIT_DEPTH-1:0]					p_1_1_in ;
	input		[BIT_DEPTH-1:0]					p_1_2_in ;
	input		[BIT_DEPTH-1:0]					p_1_3_in ;
	input		[BIT_DEPTH-1:0]					p_1_4_in ;
	input		[BIT_DEPTH-1:0]					p_1_5_in ;
	input		[BIT_DEPTH-1:0]					p_2_0_in ;
	input		[BIT_DEPTH-1:0]					p_2_1_in ;
	input		[BIT_DEPTH-1:0]					p_2_2_in ;
	input		[BIT_DEPTH-1:0]					p_2_3_in ;
	input		[BIT_DEPTH-1:0]					p_2_4_in ;
	input		[BIT_DEPTH-1:0]					p_2_5_in ;
	input		[BIT_DEPTH-1:0]					p_3_0_in ;
	input		[BIT_DEPTH-1:0]					p_3_1_in ;
	input		[BIT_DEPTH-1:0]					p_3_2_in ;
	input		[BIT_DEPTH-1:0]					p_3_3_in ;
	input		[BIT_DEPTH-1:0]					p_3_4_in ;
	input		[BIT_DEPTH-1:0]					p_3_5_in ;
	input		[BIT_DEPTH-1:0]					p_4_0_in ;
	input		[BIT_DEPTH-1:0]					p_4_1_in ;
	input		[BIT_DEPTH-1:0]					p_4_2_in ;
	input		[BIT_DEPTH-1:0]					p_4_3_in ;
	input		[BIT_DEPTH-1:0]					p_4_4_in ;
	input		[BIT_DEPTH-1:0]					p_4_5_in ;
	input		[BIT_DEPTH-1:0]					p_5_0_in ;
	input		[BIT_DEPTH-1:0]					p_5_1_in ;
	input		[BIT_DEPTH-1:0]					p_5_2_in ;
	input		[BIT_DEPTH-1:0]					p_5_3_in ;
	input		[BIT_DEPTH-1:0]					p_5_4_in ;
	input		[BIT_DEPTH-1:0]					p_5_5_in ;
	
	// SAO parameter inputs
	input										cross_boundary_left_flag_in;
	input										cross_boundary_right_flag_in;
	input										cross_boundary_top_flag_in;
	input										cross_boundary_bottom_flag_in;
	input										cross_boundary_top_left_flag_in;
	input										cross_boundary_top_right_flag_in;
	input										cross_boundary_bottom_left_flag_in;
	input										cross_boundary_bottom_right_flag_in;
	input		[1:0]							sao_type_in ;
	input		[1:0]							sao_eo_class_in;
	input		[4:0]							sao_bandpos_in;
	input		[SAO_OFFSET_WIDTH-1:0]			sao_offset_1_in;
	input		[SAO_OFFSET_WIDTH-1:0]			sao_offset_2_in;
	input		[SAO_OFFSET_WIDTH-1:0]			sao_offset_3_in;
	input		[SAO_OFFSET_WIDTH-1:0]			sao_offset_4_in;
	
	
	// SAO output samples
	output	reg		[BIT_DEPTH-1:0]				pix_1_1_out;
	output	reg		[BIT_DEPTH-1:0]				pix_1_2_out;
	output	reg		[BIT_DEPTH-1:0]				pix_1_3_out;
	output	reg		[BIT_DEPTH-1:0]				pix_1_4_out;
	output	reg		[BIT_DEPTH-1:0]				pix_2_1_out;
	output	reg		[BIT_DEPTH-1:0]				pix_2_2_out;
	output	reg		[BIT_DEPTH-1:0]				pix_2_3_out;
	output	reg		[BIT_DEPTH-1:0]				pix_2_4_out;
	output	reg		[BIT_DEPTH-1:0]				pix_3_1_out;
	output	reg		[BIT_DEPTH-1:0]				pix_3_2_out;
	output	reg		[BIT_DEPTH-1:0]				pix_3_3_out;
	output	reg		[BIT_DEPTH-1:0]				pix_3_4_out;
	output	reg		[BIT_DEPTH-1:0]				pix_4_1_out;
	output	reg		[BIT_DEPTH-1:0]				pix_4_2_out;
	output	reg		[BIT_DEPTH-1:0]				pix_4_3_out;
	output	reg		[BIT_DEPTH-1:0]				pix_4_4_out;
	
	
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    
    localparam                          STATE_INIT          = 0;    // the state that is entered upon reset
    localparam                          STATE_1             = 1;
	
	localparam                          EO_HOR  	= 	4'b1000 ;
	localparam                          EO_VER  	= 	4'b1001 ;
	localparam                          EO_D_45  	= 	4'b1011 ;
	localparam                          EO_D_135  	= 	4'b1010 ;
	localparam                          BO  		= 	4'b01xx ;
    
    
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    
	reg			[BIT_DEPTH-1:0]				pix_0_0_reg ;
	reg			[BIT_DEPTH-1:0]				pix_0_1_reg ;
	reg			[BIT_DEPTH-1:0]				pix_0_2_reg ;
	reg			[BIT_DEPTH-1:0]				pix_0_3_reg ;
	reg			[BIT_DEPTH-1:0]				pix_0_4_reg ;
	reg			[BIT_DEPTH-1:0]				pix_0_5_reg ;
	reg			[BIT_DEPTH-1:0]				pix_1_0_reg ;
	reg			[BIT_DEPTH-1:0]				pix_1_5_reg ;
	reg			[BIT_DEPTH-1:0]				pix_2_0_reg ;
	reg			[BIT_DEPTH-1:0]				pix_2_5_reg ;
	reg			[BIT_DEPTH-1:0]				pix_3_0_reg ;
	reg			[BIT_DEPTH-1:0]				pix_3_5_reg ;
	reg			[BIT_DEPTH-1:0]				pix_4_0_reg ;
	reg			[BIT_DEPTH-1:0]				pix_4_5_reg ;
	reg			[BIT_DEPTH-1:0]				pix_5_0_reg ;
	reg			[BIT_DEPTH-1:0]				pix_5_1_reg ;
	reg			[BIT_DEPTH-1:0]				pix_5_2_reg ;
	reg			[BIT_DEPTH-1:0]				pix_5_3_reg ;
	reg			[BIT_DEPTH-1:0]				pix_5_4_reg ;
	reg			[BIT_DEPTH-1:0]				pix_5_5_reg ;
	
	reg										cross_boundary_left_flag  ;
	reg										cross_boundary_right_flag ;
	reg										cross_boundary_top_flag   ;
	reg										cross_boundary_bottom_flag;
	reg										cross_boundary_top_left_flag  ;
	reg										cross_boundary_top_right_flag ;
	reg										cross_boundary_bottom_left_flag  ;
	reg										cross_boundary_bottom_right_flag ;
	reg			[1:0]						sao_type		;
	reg			[1:0]						sao_eo_class	;
	reg			[4:0]						sao_bandpos		;
	reg			[SAO_OFFSET_WIDTH-1:0]		sao_offset_1	;
	reg			[SAO_OFFSET_WIDTH-1:0]		sao_offset_2	;
	reg			[SAO_OFFSET_WIDTH-1:0]		sao_offset_3	;
	reg			[SAO_OFFSET_WIDTH-1:0]		sao_offset_4	;
	
	wire		[BIT_DEPTH-1:0]				sao_pix_1_1 ;
	wire		[BIT_DEPTH-1:0]				sao_pix_1_2 ;
	wire		[BIT_DEPTH-1:0]				sao_pix_1_3 ;
	wire		[BIT_DEPTH-1:0]				sao_pix_1_4 ;
	wire		[BIT_DEPTH-1:0]				sao_pix_2_1 ;
	wire		[BIT_DEPTH-1:0]				sao_pix_2_2 ;
	wire		[BIT_DEPTH-1:0]				sao_pix_2_3 ;
	wire		[BIT_DEPTH-1:0]				sao_pix_2_4 ;
	wire		[BIT_DEPTH-1:0]				sao_pix_3_1 ;
	wire		[BIT_DEPTH-1:0]				sao_pix_3_2 ;
	wire		[BIT_DEPTH-1:0]				sao_pix_3_3 ;
	wire		[BIT_DEPTH-1:0]				sao_pix_3_4 ;
	wire		[BIT_DEPTH-1:0]				sao_pix_4_1 ;
	wire		[BIT_DEPTH-1:0]				sao_pix_4_2 ;
	wire		[BIT_DEPTH-1:0]				sao_pix_4_3 ;
	wire		[BIT_DEPTH-1:0]				sao_pix_4_4 ;
	
	integer 								state;
    
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
    
	
	// SAO sample filter submodules
	
	sao_sample_filter sao_filter_p_1_1(
		.pix_1_in		(pix_0_0_reg ),
		.pix_2_in		(pix_0_1_reg ),
		.pix_3_in		(pix_0_2_reg ),
		.pix_4_in		(pix_1_0_reg ),
		.pix_c_in		(pix_1_1_out ),
		.pix_5_in		(pix_1_2_out ),
		.pix_6_in		(pix_2_0_reg ),
		.pix_7_in		(pix_2_1_out ),
		.pix_8_in		(pix_2_2_out ),
		.sao_type		(sao_type	 ),
		.sao_eo_class	(sao_eo_class),
		.sao_bandpos	(sao_bandpos ),
		.sao_offset_1	(sao_offset_1),
		.sao_offset_2	(sao_offset_2),
		.sao_offset_3	(sao_offset_3),
		.sao_offset_4	(sao_offset_4),
		.pix_c_out      (sao_pix_1_1 )
    );
	
	sao_sample_filter sao_filter_p_1_2(
		.pix_1_in		(pix_0_1_reg ),
		.pix_2_in		(pix_0_2_reg ),
		.pix_3_in		(pix_0_3_reg ),
		.pix_4_in		(pix_1_1_out ),
		.pix_c_in		(pix_1_2_out ),
		.pix_5_in		(pix_1_3_out ),
		.pix_6_in		(pix_2_1_out ),
		.pix_7_in		(pix_2_2_out ),
		.pix_8_in		(pix_2_3_out ),
		.sao_type		(sao_type	 ),
		.sao_eo_class	(sao_eo_class),
		.sao_bandpos	(sao_bandpos ),
		.sao_offset_1	(sao_offset_1),
		.sao_offset_2	(sao_offset_2),
		.sao_offset_3	(sao_offset_3),
		.sao_offset_4	(sao_offset_4),
		.pix_c_out      (sao_pix_1_2 )
    );
	
	sao_sample_filter sao_filter_p_1_3(
		.pix_1_in		(pix_0_2_reg ),
		.pix_2_in		(pix_0_3_reg ),
		.pix_3_in		(pix_0_4_reg ),
		.pix_4_in		(pix_1_2_out ),
		.pix_c_in		(pix_1_3_out ),
		.pix_5_in		(pix_1_4_out ),
		.pix_6_in		(pix_2_2_out ),
		.pix_7_in		(pix_2_3_out ),
		.pix_8_in		(pix_2_4_out ),
		.sao_type		(sao_type	 ),
		.sao_eo_class	(sao_eo_class),
		.sao_bandpos	(sao_bandpos ),
		.sao_offset_1	(sao_offset_1),
		.sao_offset_2	(sao_offset_2),
		.sao_offset_3	(sao_offset_3),
		.sao_offset_4	(sao_offset_4),
		.pix_c_out      (sao_pix_1_3 )
    );
	
	sao_sample_filter sao_filter_p_1_4(
		.pix_1_in		(pix_0_3_reg ),
		.pix_2_in		(pix_0_4_reg ),
		.pix_3_in		(pix_0_5_reg ),
		.pix_4_in		(pix_1_3_out ),
		.pix_c_in		(pix_1_4_out ),
		.pix_5_in		(pix_1_5_reg ),
		.pix_6_in		(pix_2_3_out ),
		.pix_7_in		(pix_2_4_out ),
		.pix_8_in		(pix_2_5_reg ),
		.sao_type		(sao_type	 ),
		.sao_eo_class	(sao_eo_class),
		.sao_bandpos	(sao_bandpos ),
		.sao_offset_1	(sao_offset_1),
		.sao_offset_2	(sao_offset_2),
		.sao_offset_3	(sao_offset_3),
		.sao_offset_4	(sao_offset_4),
		.pix_c_out      (sao_pix_1_4 )
    );
	
	sao_sample_filter sao_filter_p_2_1(
		.pix_1_in		(pix_1_0_reg ),
		.pix_2_in		(pix_1_1_out ),
		.pix_3_in		(pix_1_2_out ),
		.pix_4_in		(pix_2_0_reg ),
		.pix_c_in		(pix_2_1_out ),
		.pix_5_in		(pix_2_2_out ),
		.pix_6_in		(pix_3_0_reg ),
		.pix_7_in		(pix_3_1_out ),
		.pix_8_in		(pix_3_2_out ),
		.sao_type		(sao_type	 ),
		.sao_eo_class	(sao_eo_class),
		.sao_bandpos	(sao_bandpos ),
		.sao_offset_1	(sao_offset_1),
		.sao_offset_2	(sao_offset_2),
		.sao_offset_3	(sao_offset_3),
		.sao_offset_4	(sao_offset_4),
		.pix_c_out      (sao_pix_2_1 )
    );
	
	sao_sample_filter sao_filter_p_2_2(
		.pix_1_in		(pix_1_1_out ),
		.pix_2_in		(pix_1_2_out ),
		.pix_3_in		(pix_1_3_out ),
		.pix_4_in		(pix_2_1_out ),
		.pix_c_in		(pix_2_2_out ),
		.pix_5_in		(pix_2_3_out ),
		.pix_6_in		(pix_3_1_out ),
		.pix_7_in		(pix_3_2_out ),
		.pix_8_in		(pix_3_3_out ),
		.sao_type		(sao_type	 ),
		.sao_eo_class	(sao_eo_class),
		.sao_bandpos	(sao_bandpos ),
		.sao_offset_1	(sao_offset_1),
		.sao_offset_2	(sao_offset_2),
		.sao_offset_3	(sao_offset_3),
		.sao_offset_4	(sao_offset_4),
		.pix_c_out      (sao_pix_2_2 )
    );
	
	sao_sample_filter sao_filter_p_2_3(
		.pix_1_in		(pix_1_2_out ),
		.pix_2_in		(pix_1_3_out ),
		.pix_3_in		(pix_1_4_out ),
		.pix_4_in		(pix_2_2_out ),
		.pix_c_in		(pix_2_3_out ),
		.pix_5_in		(pix_2_4_out ),
		.pix_6_in		(pix_3_2_out ),
		.pix_7_in		(pix_3_3_out ),
		.pix_8_in		(pix_3_4_out ),
		.sao_type		(sao_type	 ),
		.sao_eo_class	(sao_eo_class),
		.sao_bandpos	(sao_bandpos ),
		.sao_offset_1	(sao_offset_1),
		.sao_offset_2	(sao_offset_2),
		.sao_offset_3	(sao_offset_3),
		.sao_offset_4	(sao_offset_4),
		.pix_c_out      (sao_pix_2_3 )
    );
	
	sao_sample_filter sao_filter_p_2_4(
		.pix_1_in		(pix_1_3_out ),
		.pix_2_in		(pix_1_4_out ),
		.pix_3_in		(pix_1_5_reg ),
		.pix_4_in		(pix_2_3_out ),
		.pix_c_in		(pix_2_4_out ),
		.pix_5_in		(pix_2_5_reg ),
		.pix_6_in		(pix_3_3_out ),
		.pix_7_in		(pix_3_4_out ),
		.pix_8_in		(pix_3_5_reg ),
		.sao_type		(sao_type	 ),
		.sao_eo_class	(sao_eo_class),
		.sao_bandpos	(sao_bandpos ),
		.sao_offset_1	(sao_offset_1),
		.sao_offset_2	(sao_offset_2),
		.sao_offset_3	(sao_offset_3),
		.sao_offset_4	(sao_offset_4),
		.pix_c_out      (sao_pix_2_4 )
    );
	
	sao_sample_filter sao_filter_p_3_1(
		.pix_1_in		(pix_2_0_reg ),
		.pix_2_in		(pix_2_1_out ),
		.pix_3_in		(pix_2_2_out ),
		.pix_4_in		(pix_3_0_reg ),
		.pix_c_in		(pix_3_1_out ),
		.pix_5_in		(pix_3_2_out ),
		.pix_6_in		(pix_4_0_reg ),
		.pix_7_in		(pix_4_1_out ),
		.pix_8_in		(pix_4_2_out ),
		.sao_type		(sao_type	 ),
		.sao_eo_class	(sao_eo_class),
		.sao_bandpos	(sao_bandpos ),
		.sao_offset_1	(sao_offset_1),
		.sao_offset_2	(sao_offset_2),
		.sao_offset_3	(sao_offset_3),
		.sao_offset_4	(sao_offset_4),
		.pix_c_out      (sao_pix_3_1 )
    );
	
	sao_sample_filter sao_filter_p_3_2(
		.pix_1_in		(pix_2_1_out ),
		.pix_2_in		(pix_2_2_out ),
		.pix_3_in		(pix_2_3_out ),
		.pix_4_in		(pix_3_1_out ),
		.pix_c_in		(pix_3_2_out ),
		.pix_5_in		(pix_3_3_out ),
		.pix_6_in		(pix_4_1_out ),
		.pix_7_in		(pix_4_2_out ),
		.pix_8_in		(pix_4_3_out ),
		.sao_type		(sao_type	 ),
		.sao_eo_class	(sao_eo_class),
		.sao_bandpos	(sao_bandpos ),
		.sao_offset_1	(sao_offset_1),
		.sao_offset_2	(sao_offset_2),
		.sao_offset_3	(sao_offset_3),
		.sao_offset_4	(sao_offset_4),
		.pix_c_out      (sao_pix_3_2 )
    );
	
	sao_sample_filter sao_filter_p_3_3(
		.pix_1_in		(pix_2_2_out ),
		.pix_2_in		(pix_2_3_out ),
		.pix_3_in		(pix_2_4_out ),
		.pix_4_in		(pix_3_2_out ),
		.pix_c_in		(pix_3_3_out ),
		.pix_5_in		(pix_3_4_out ),
		.pix_6_in		(pix_4_2_out ),
		.pix_7_in		(pix_4_3_out ),
		.pix_8_in		(pix_4_4_out ),
		.sao_type		(sao_type	 ),
		.sao_eo_class	(sao_eo_class),
		.sao_bandpos	(sao_bandpos ),
		.sao_offset_1	(sao_offset_1),
		.sao_offset_2	(sao_offset_2),
		.sao_offset_3	(sao_offset_3),
		.sao_offset_4	(sao_offset_4),
		.pix_c_out      (sao_pix_3_3 )
    );
	
	sao_sample_filter sao_filter_p_3_4(
		.pix_1_in		(pix_2_3_out ),
		.pix_2_in		(pix_2_4_out ),
		.pix_3_in		(pix_2_5_reg ),
		.pix_4_in		(pix_3_3_out ),
		.pix_c_in		(pix_3_4_out ),
		.pix_5_in		(pix_3_5_reg ),
		.pix_6_in		(pix_4_3_out ),
		.pix_7_in		(pix_4_4_out ),
		.pix_8_in		(pix_4_5_reg ),
		.sao_type		(sao_type	 ),
		.sao_eo_class	(sao_eo_class),
		.sao_bandpos	(sao_bandpos ),
		.sao_offset_1	(sao_offset_1),
		.sao_offset_2	(sao_offset_2),
		.sao_offset_3	(sao_offset_3),
		.sao_offset_4	(sao_offset_4),
		.pix_c_out      (sao_pix_3_4 )
    );
	
	sao_sample_filter sao_filter_p_4_1(
		.pix_1_in		(pix_3_0_reg ),
		.pix_2_in		(pix_3_1_out ),
		.pix_3_in		(pix_3_2_out ),
		.pix_4_in		(pix_4_0_reg ),
		.pix_c_in		(pix_4_1_out ),
		.pix_5_in		(pix_4_2_out ),
		.pix_6_in		(pix_5_0_reg ),
		.pix_7_in		(pix_5_1_reg ),
		.pix_8_in		(pix_5_2_reg ),
		.sao_type		(sao_type	 ),
		.sao_eo_class	(sao_eo_class),
		.sao_bandpos	(sao_bandpos ),
		.sao_offset_1	(sao_offset_1),
		.sao_offset_2	(sao_offset_2),
		.sao_offset_3	(sao_offset_3),
		.sao_offset_4	(sao_offset_4),
		.pix_c_out      (sao_pix_4_1 )
    );
	
	sao_sample_filter sao_filter_p_4_2(
		.pix_1_in		(pix_3_1_out ),
		.pix_2_in		(pix_3_2_out ),
		.pix_3_in		(pix_3_3_out ),
		.pix_4_in		(pix_4_1_out ),
		.pix_c_in		(pix_4_2_out ),
		.pix_5_in		(pix_4_3_out ),
		.pix_6_in		(pix_5_1_reg ),
		.pix_7_in		(pix_5_2_reg ),
		.pix_8_in		(pix_5_3_reg ),
		.sao_type		(sao_type	 ),
		.sao_eo_class	(sao_eo_class),
		.sao_bandpos	(sao_bandpos ),
		.sao_offset_1	(sao_offset_1),
		.sao_offset_2	(sao_offset_2),
		.sao_offset_3	(sao_offset_3),
		.sao_offset_4	(sao_offset_4),
		.pix_c_out      (sao_pix_4_2 )
    );
	
	sao_sample_filter sao_filter_p_4_3(
		.pix_1_in		(pix_3_2_out ),
		.pix_2_in		(pix_3_3_out ),
		.pix_3_in		(pix_3_4_out ),
		.pix_4_in		(pix_4_2_out ),
		.pix_c_in		(pix_4_3_out ),
		.pix_5_in		(pix_4_4_out ),
		.pix_6_in		(pix_5_2_reg ),
		.pix_7_in		(pix_5_3_reg ),
		.pix_8_in		(pix_5_4_reg ),
		.sao_type		(sao_type	 ),
		.sao_eo_class	(sao_eo_class),
		.sao_bandpos	(sao_bandpos ),
		.sao_offset_1	(sao_offset_1),
		.sao_offset_2	(sao_offset_2),
		.sao_offset_3	(sao_offset_3),
		.sao_offset_4	(sao_offset_4),
		.pix_c_out      (sao_pix_4_3 )
    );
	
	sao_sample_filter sao_filter_p_4_4(
		.pix_1_in		(pix_3_3_out ),
		.pix_2_in		(pix_3_4_out ),
		.pix_3_in		(pix_3_5_reg ),
		.pix_4_in		(pix_4_3_out ),
		.pix_c_in		(pix_4_4_out ),
		.pix_5_in		(pix_4_5_reg ),
		.pix_6_in		(pix_5_3_reg ),
		.pix_7_in		(pix_5_4_reg ),
		.pix_8_in		(pix_5_5_reg ),
		.sao_type		(sao_type	 ),
		.sao_eo_class	(sao_eo_class),
		.sao_bandpos	(sao_bandpos ),
		.sao_offset_1	(sao_offset_1),
		.sao_offset_2	(sao_offset_2),
		.sao_offset_3	(sao_offset_3),
		.sao_offset_4	(sao_offset_4),
		.pix_c_out      (sao_pix_4_4 )
    );
	
	
	//------------- SAO Block Filter Main SM ------------------------------
	
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			state <= STATE_INIT;
		end
		else begin
			case(state)
				STATE_INIT : begin
					if(start) begin
						pix_0_0_reg	<=	p_0_0_in ;
						pix_0_1_reg	<=	p_0_1_in ;
						pix_0_2_reg	<=	p_0_2_in ;
						pix_0_3_reg	<=	p_0_3_in ;
						pix_0_4_reg	<=	p_0_4_in ;
						pix_0_5_reg	<=	p_0_5_in ;
						pix_1_0_reg	<=	p_1_0_in ;
						pix_1_1_out	<=	p_1_1_in ;
						pix_1_2_out	<=	p_1_2_in ;
						pix_1_3_out	<=	p_1_3_in ;
						pix_1_4_out	<=	p_1_4_in ;
						pix_1_5_reg	<=	p_1_5_in ;
						pix_2_0_reg	<=	p_2_0_in ;
						pix_2_1_out	<=	p_2_1_in ;
						pix_2_2_out	<=	p_2_2_in ;
						pix_2_3_out	<=	p_2_3_in ;
						pix_2_4_out	<=	p_2_4_in ;
						pix_2_5_reg	<=	p_2_5_in ;
						pix_3_0_reg	<=	p_3_0_in ;
						pix_3_1_out	<=	p_3_1_in ;
						pix_3_2_out	<=	p_3_2_in ;
						pix_3_3_out	<=	p_3_3_in ;
						pix_3_4_out	<=	p_3_4_in ;
						pix_3_5_reg	<=	p_3_5_in ;
						pix_4_0_reg	<=	p_4_0_in ;
						pix_4_1_out	<=	p_4_1_in ;
						pix_4_2_out	<=	p_4_2_in ;
						pix_4_3_out	<=	p_4_3_in ;
						pix_4_4_out	<=	p_4_4_in ;
						pix_4_5_reg	<=	p_4_5_in ;
						pix_5_0_reg	<=	p_5_0_in ;
						pix_5_1_reg	<=	p_5_1_in ;
						pix_5_2_reg	<=	p_5_2_in ;
						pix_5_3_reg	<=	p_5_3_in ;
						pix_5_4_reg	<=	p_5_4_in ;
						pix_5_5_reg	<=	p_5_5_in ;
						sao_type		<=	sao_type_in	 ;
						sao_eo_class	<=	sao_eo_class_in ;
						sao_bandpos		<=	sao_bandpos_in	;
						sao_offset_1	<=	sao_offset_1_in ;
						sao_offset_2	<=	sao_offset_2_in ;
						sao_offset_3	<=	sao_offset_3_in ;
						sao_offset_4	<=	sao_offset_4_in ;
						cross_boundary_left_flag   <= cross_boundary_left_flag_in;
						cross_boundary_right_flag  <= cross_boundary_right_flag_in;
						cross_boundary_top_flag    <= cross_boundary_top_flag_in;
						cross_boundary_bottom_flag <= cross_boundary_bottom_flag_in;
						cross_boundary_top_left_flag   <=  cross_boundary_top_left_flag_in ;
						cross_boundary_top_right_flag  <=  cross_boundary_top_right_flag_in;
						cross_boundary_bottom_left_flag  <= cross_boundary_bottom_left_flag_in ;
						cross_boundary_bottom_right_flag <= cross_boundary_bottom_right_flag_in;
						state <= STATE_1;
					end
				end
				STATE_1 : begin
					casex( {sao_type, sao_eo_class} )
						EO_HOR : begin
							pix_2_2_out	<= sao_pix_2_2 ;
							pix_2_3_out <= sao_pix_2_3 ;
							pix_3_2_out <= sao_pix_3_2 ;
							pix_3_3_out <= sao_pix_3_3 ;
							if(cross_boundary_left_flag) begin
								pix_1_1_out <= sao_pix_1_1 ;
								pix_2_1_out <= sao_pix_2_1 ;
								pix_3_1_out <= sao_pix_3_1 ;
								pix_4_1_out <= sao_pix_4_1 ;
							end
							if(cross_boundary_right_flag) begin
								pix_1_4_out <= sao_pix_1_4 ;
								pix_2_4_out <= sao_pix_2_4 ;
								pix_3_4_out <= sao_pix_3_4 ;
								pix_4_4_out <= sao_pix_4_4 ;
							end
							pix_1_2_out <= sao_pix_1_2 ;
							pix_1_3_out <= sao_pix_1_3 ;
							pix_4_2_out <= sao_pix_4_2 ;
							pix_4_3_out <= sao_pix_4_3 ;
						end
						EO_VER : begin
							pix_2_2_out	<= sao_pix_2_2 ;
							pix_2_3_out <= sao_pix_2_3 ;
							pix_3_2_out <= sao_pix_3_2 ;
							pix_3_3_out <= sao_pix_3_3 ;
							if(cross_boundary_top_flag) begin
								pix_1_1_out <= sao_pix_1_1 ;
								pix_1_2_out <= sao_pix_1_2 ;
								pix_1_3_out <= sao_pix_1_3 ;
								pix_1_4_out <= sao_pix_1_4 ;
							end
							if(cross_boundary_bottom_flag) begin
								pix_4_1_out <= sao_pix_4_1 ;
								pix_4_2_out <= sao_pix_4_2 ;
								pix_4_3_out <= sao_pix_4_3 ;
								pix_4_4_out <= sao_pix_4_4 ;
							end
							pix_2_1_out <= sao_pix_2_1 ;
							pix_3_1_out <= sao_pix_3_1 ;
							pix_2_4_out <= sao_pix_2_4 ;
							pix_3_4_out <= sao_pix_3_4 ;
						end
						EO_D_45 : begin
							pix_2_2_out	<= sao_pix_2_2 ;
							pix_2_3_out <= sao_pix_2_3 ;
							pix_3_2_out <= sao_pix_3_2 ;
							pix_3_3_out <= sao_pix_3_3 ;
							if(cross_boundary_top_left_flag) begin
								pix_1_1_out <= sao_pix_1_1 ;
							end
							if(cross_boundary_top_flag) begin
								pix_1_2_out <= sao_pix_1_2 ;
								pix_1_3_out <= sao_pix_1_3 ;
							end
							if(cross_boundary_top_right_flag) begin
								pix_1_4_out <= sao_pix_1_4 ;
							end
							if(cross_boundary_left_flag) begin
								pix_2_1_out <= sao_pix_2_1 ;
								pix_3_1_out <= sao_pix_3_1 ;
							end
							if(cross_boundary_right_flag) begin
								pix_2_4_out <= sao_pix_2_4 ;
								pix_3_4_out <= sao_pix_3_4 ;
							end
							if(cross_boundary_bottom_left_flag) begin
								pix_4_1_out <= sao_pix_4_1 ;
							end
							if(cross_boundary_bottom_flag) begin
								pix_4_2_out <= sao_pix_4_2 ;
								pix_4_3_out <= sao_pix_4_3 ;
							end
							if(cross_boundary_bottom_right_flag) begin
								pix_4_4_out <= sao_pix_4_4 ;
							end
						end
						EO_D_135 : begin
							pix_2_2_out	<= sao_pix_2_2 ;
							pix_2_3_out <= sao_pix_2_3 ;
							pix_3_2_out <= sao_pix_3_2 ;
							pix_3_3_out <= sao_pix_3_3 ;
							if(cross_boundary_top_left_flag) begin
								pix_1_1_out <= sao_pix_1_1 ;
							end
							if(cross_boundary_top_flag) begin
								pix_1_2_out <= sao_pix_1_2 ;
								pix_1_3_out <= sao_pix_1_3 ;
							end
							if(cross_boundary_top_right_flag) begin
								pix_1_4_out <= sao_pix_1_4 ;
							end
							if(cross_boundary_left_flag) begin
								pix_2_1_out <= sao_pix_2_1 ;
								pix_3_1_out <= sao_pix_3_1 ;
							end
							if(cross_boundary_right_flag) begin
								pix_2_4_out <= sao_pix_2_4 ;
								pix_3_4_out <= sao_pix_3_4 ;
							end
							if(cross_boundary_bottom_left_flag) begin
								pix_4_1_out <= sao_pix_4_1 ;
							end
							if(cross_boundary_bottom_flag) begin
								pix_4_2_out <= sao_pix_4_2 ;
								pix_4_3_out <= sao_pix_4_3 ;
							end
							if(cross_boundary_bottom_right_flag) begin
								pix_4_4_out <= sao_pix_4_4 ;
							end
						end
						BO : begin
							pix_1_1_out <= sao_pix_1_1 ;
							pix_1_2_out <= sao_pix_1_2 ;
							pix_1_3_out <= sao_pix_1_3 ;
							pix_1_4_out <= sao_pix_1_4 ;
							pix_2_1_out <= sao_pix_2_1 ;
							pix_2_2_out <= sao_pix_2_2 ;
							pix_2_3_out <= sao_pix_2_3 ;
							pix_2_4_out <= sao_pix_2_4 ;
							pix_3_1_out <= sao_pix_3_1 ;
							pix_3_2_out <= sao_pix_3_2 ;
							pix_3_3_out <= sao_pix_3_3 ;
							pix_3_4_out <= sao_pix_3_4 ;
							pix_4_1_out <= sao_pix_4_1 ;
							pix_4_2_out <= sao_pix_4_2 ;
							pix_4_3_out <= sao_pix_4_3 ;
							pix_4_4_out <= sao_pix_4_4 ;
						end
					endcase
					state <= STATE_INIT;
				end
			endcase
		end
	end
	


endmodule
