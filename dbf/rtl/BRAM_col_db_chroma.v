`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:22:04 11/23/2013 
// Design Name: 
// Module Name:    BRAM_col_db_chroma 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module BRAM_col_db_chroma(
        clk,
        we_a,
        en_a,
        addr_a,
        data_in_a,
        data_out_a
    );

    
    input                       clk;
    input                       we_a;
    input                       en_a;
    input           [7:0]       addr_a;
    input           [63:0]      data_in_a;
    output reg      [63:0]      data_out_a;
    
    reg             [63:0]      RAM [143:0];
    
    always @(posedge clk) begin
        if(en_a) begin
            if(we_a) begin
                RAM[addr_a] <= data_in_a;
            end
            else begin
                data_out_a <= RAM[addr_a];
            end
        end
    end
    

endmodule
