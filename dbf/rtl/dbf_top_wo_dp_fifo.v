`timescale 1ns / 1ps
module dbf_top_wo_dp_fifo(
    clk,
    reset,
	
    //bs_interface
    bs_fifo_data_in        ,
    bs_fifo_wr_en_in       ,
    bs_fifo_full_out       ,

    //luma dbf fifo interface

    cnfig_dbf_fifo_is_full_out	,
    cnfig_dbf_fifo_data_in		,
    cnfig_dbf_fifo_wr_en_in		,
    
    y_dbf_fifo_is_full_out		,
    y_dbf_fifo_data_in			,
    y_dbf_fifo_wr_en_in			,
    
    //cb dbf fifo interface
    cb_dbf_fifo_is_full_out		,
    cb_dbf_fifo_data_in			,
    cb_dbf_fifo_wr_en_in		,
    
    //cr dbf fifo interface
    cr_dbf_fifo_is_full_out		,
    cr_dbf_fifo_data_in			,
    cr_dbf_fifo_wr_en_in		,
	
	display_fifo_is_full	,
	display_fifo_data_wr_en		,	
	display_fifo_data		,	
		
	dpb_fifo_is_empty_out		,	
	dpb_fifo_rd_en_in			,	
	dpb_fifo_data_out			,    	
	
	pic_width_out				,	
	pic_height_out				,	
	log2_ctu_size_out			,	
	
	dpb_axi_addr_out			,
	sao_poc_out			
	,Xc_Yc_out
	,pred_2_dbf_rd_inf
	,pred_2_dbf_yy
	,pred_2_dbf_cb
	,pred_2_dbf_cr
	,dbf_main_in_valid
	,dbf_main_out_valid
	
	,controller_in_valid 	
	,controller_in 			
	,controller_out_valid 
	,controller_out 		
	,sao_wr_en_out 		
	,sao_rd_en_out 		
	,sao_wr_data 			
	,sao_rd_data 				

	,dbf_to_sao_out_valid
	,dbf_to_sao_out_x
	,dbf_to_sao_out_y
	,dbf_to_sao_out_q1
	,display_buf_ful
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"
    `include "../sim/inter_axi_def.v"
    localparam                          FIFO_IN_WIDTH       = 32;
    localparam                          FIFO_OUT_WIDTH      = 32;  
    localparam                          Y_FIFO_WIDTH        = 134;
    localparam                          CH_FIFO_WIDTH        = 128;
	
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
  
  
	
    input  [BS_FIFO_WIDTH-1:0]    bs_fifo_data_in        ;
    input                         bs_fifo_wr_en_in       ;
    output                        bs_fifo_full_out       ;
        
    output                                                	cnfig_dbf_fifo_is_full_out;
    input  [FIFO_IN_WIDTH-1:0]                            	cnfig_dbf_fifo_data_in;
    input                                                 	cnfig_dbf_fifo_wr_en_in;
                    
    output                                                	y_dbf_fifo_is_full_out;
    input  [Y_FIFO_WIDTH-1:0]                             	y_dbf_fifo_data_in;
    input                                                 	y_dbf_fifo_wr_en_in;
                    
    output                                            		cb_dbf_fifo_is_full_out;
    input    [CH_FIFO_WIDTH-1:0]                      		cb_dbf_fifo_data_in;
    input                                             		cb_dbf_fifo_wr_en_in;
                    
    output                                               	cr_dbf_fifo_is_full_out;
    input  [CH_FIFO_WIDTH-1:0]                           	cr_dbf_fifo_data_in;
    input                                                	cr_dbf_fifo_wr_en_in; 


    input clk;
    input reset;

	input 								display_fifo_is_full;
	output  								display_fifo_data_wr_en;
	output  [SAO_OUT_FIFO_WIDTH-1:0]	display_fifo_data;
	
	output 								dpb_fifo_is_empty_out;
	input  								dpb_fifo_rd_en_in;
	output  [SAO_OUT_FIFO_WIDTH-1:0]	dpb_fifo_data_out;    
	
	output [AXI_ADDR_WDTH -1:0]			dpb_axi_addr_out;
	output [POC_WIDTH-1 -1:0]			sao_poc_out;
	
    output     [PIC_WIDTH_WIDTH-1:0]          							  pic_width_out;
    output     [PIC_WIDTH_WIDTH-1:0]          							  pic_height_out;
    output     [MAX_LOG2CTBSIZE_WIDTH - 1:0]              	  			  log2_ctu_size_out;
	
  	
	output [24-1:0] Xc_Yc_out;
	output [7:0] pred_2_dbf_rd_inf ;
	output [134-1:0] pred_2_dbf_yy ;
	output [128-1:0] pred_2_dbf_cb ;
	output [128-1:0] pred_2_dbf_cr ;
	
	output dbf_main_in_valid;
	output dbf_main_out_valid;
	

	output controller_in_valid 	;
	output [128-1:0] controller_in 		;
	output controller_out_valid ;
	output [128-1:0] controller_out 		;
	output sao_wr_en_out 		;
	output sao_rd_en_out 		;
	output [256-1:0] sao_wr_data 			;
	output [256-1:0] sao_rd_data 			;
	
	
	output dbf_to_sao_out_valid;
	output [12-1:0] dbf_to_sao_out_x;
	output [12-1:0] dbf_to_sao_out_y;
	output [128-1:0] dbf_to_sao_out_q1;
	output display_buf_ful;
	
	
	
	
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    wire     [127:0]     			y_q_block_1 ;
    wire     [127:0]     			y_q_block_2 ;
    wire     [127:0]     			y_q_block_3 ;
    wire     [127:0]     			y_q_block_4 ;
    wire     [127:0]     			y_A_block   ;
    wire     [127:0]     			y_B_block   ;
				
    wire     [31:0]      			cb_q_block_1 ;
    wire     [31:0]      			cb_q_block_2 ;
    wire     [31:0]      			cb_q_block_3 ;
    wire     [31:0]      			cb_q_block_4 ;
    wire     [31:0]      			cb_A_block   ;
    wire     [31:0]      			cb_B_block   ;
				
    wire     [31:0]      			cr_q_block_1 ;
    wire     [31:0]      			cr_q_block_2 ;
    wire     [31:0]      			cr_q_block_3 ;
    wire     [31:0]      			cr_q_block_4 ;
    wire     [31:0]      			cr_A_block   ;
    wire     [31:0]      			cr_B_block    ;
    
    wire     [X11_ADDR_WDTH -1:LOG2_MIN_DU_SIZE]                  Xc ;
    wire     [X11_ADDR_WDTH -1:LOG2_MIN_DU_SIZE]                  Yc ;
    wire     [QP_WIDTH-1:0]			qp_1;
	wire     [QP_WIDTH-1:0]			qp_A;
	wire     [BS_WIDTH-1:0]          BS_v1 ;
    wire     [BS_WIDTH-1:0]          BS_v2 ;
    wire     [BS_WIDTH-1:0]          BS_h1 ;
    wire     [BS_WIDTH-1:0]          BS_h2 ;
    wire     [BS_WIDTH-1:0]          BS_h0 ;  

    wire         [PIC_DIM_WIDTH-1:0]                 pic_width;
    wire         [PIC_DIM_WIDTH-1:0]                 pic_height;
    wire         [CTB_SIZE_WIDTH    : 0]             ctb_size;

	wire     [PPS_CB_QP_OFFSET_WIDTH-1:0]				   	pps_cb_qp_offset;  // for deblocking, only pps_cb_qp_offset is used
    wire     [PPS_CR_QP_OFFSET_WIDTH-1:0]				   	pps_cr_qp_offset;
	wire     [PPS_TC_OFFSET_DIV2_WIDTH-1:0]				   	slice_tc_offset_div2;
    wire     [PPS_BETA_OFFSET_DIV2_WIDTH-1:0]              	slice_beta_offset_div2;

    wire										h0_edge_dbf_disable;	
	wire										pcm_bypass_AB_dbf_disable;
	wire  										slice_dbf_disable;
	wire										pcm_loop_filter_disabled;
	wire										loop_filter_accross_tiles_enabled;
	wire										loop_filter_accross_slices_enabled;
	wire	[XC_WIDTH-1:0]						tile_x  ;
	wire	[XC_WIDTH-1:0]						tile_y  ;
	wire	[XC_WIDTH-1:0]						slice_x ;
	wire	[XC_WIDTH-1:0]						slice_y ;
	wire	[XC_WIDTH-1:0]						actual_slice_x ;
	wire	[XC_WIDTH-1:0]						actual_slice_y ;
	
    wire 	[31:0] 								poc_read_to_main;
	wire 	[DPB_ADDR_WIDTH -1:0] 				dpb_idx_read_to_main;
	wire 	[DPB_ADDR_WIDTH -1:0] 				dpb_idx_dbf_to_sao;
	
    wire     [TILE_ID_WIDTH-1:0]				tile_id;
    wire     [SLICE_ID_WIDTH-1:0]				slice_id;
    
    wire                        	              		sao_luma;
	wire                        	              		sao_chroma;
	wire                        	              		pcm;
	wire                        	              		bypass;
	wire     [SAOTYPE_WIDTH -1:0]                     	y_sao_type;
    wire     [BANDPOS_EO_WIDTH -1:0]                  	y_bandpos_or_eoclass;
    wire     [SAO_OFFSET_WIDTH -1:0]                   	y_sao_offset_1;
    wire     [SAO_OFFSET_WIDTH -1:0]                   	y_sao_offset_2;
    wire     [SAO_OFFSET_WIDTH -1:0]                   	y_sao_offset_3;
    wire     [SAO_OFFSET_WIDTH -1:0]                   	y_sao_offset_4;
    wire     [SAOTYPE_WIDTH -1:0]    					cb_sao_type;
    wire     [BANDPOS_EO_WIDTH -1:0] 					cb_bandpos_or_eoclass;
    wire     [SAO_OFFSET_WIDTH -1:0]                   	cb_sao_offset_1;
    wire     [SAO_OFFSET_WIDTH -1:0]                   	cb_sao_offset_2;
    wire     [SAO_OFFSET_WIDTH -1:0]                   	cb_sao_offset_3;
    wire     [SAO_OFFSET_WIDTH -1:0]                   	cb_sao_offset_4;
    wire     [SAOTYPE_WIDTH -1:0]    					cr_sao_type;
    wire     [BANDPOS_EO_WIDTH -1:0] 					cr_bandpos_or_eoclass;
    wire     [SAO_OFFSET_WIDTH -1:0]                   	cr_sao_offset_1;
    wire     [SAO_OFFSET_WIDTH -1:0]                   	cr_sao_offset_2;
    wire     [SAO_OFFSET_WIDTH -1:0]                   	cr_sao_offset_3;
    wire     [SAO_OFFSET_WIDTH -1:0]                   	cr_sao_offset_4;
	 
	wire     [MAX_LOG2CTBSIZE_WIDTH - 1:0]              	  			  log2_ctb_size_to_dbf;
    wire     [MAX_LOG2CTBSIZE_WIDTH - 1:0]              	  			  log2_ctb_size_to_sao;
		 
	 wire read_stage_valid;
	 wire main_ready;
	 wire dbf_main_to_sao_valid;
	 
    wire     [11:0]                  Xc_dbf_to_sao;
    wire     [11:0]                  Yc_dbf_to_sao;
	wire	 [11:0]					 pic_width_out_dbf_to_sao;
	wire	 [11:0]					 pic_height_out_dbf_to_sao;
	wire	 [7:0]					 slice_id_dbf_to_sao;
	
	wire     	                     pcm_bypass_dbf_to_sao;
    wire                             loop_filter_accross_tiles_enabled_dbf_to_sao;
	wire                             loop_filter_accross_slices_enabled_dbf_to_sao;
    wire                             tile_left_boundary_dbf_to_sao;
	wire                             tile_top_boundary_dbf_to_sao;
    wire                             slice_left_boundary_dbf_to_sao;
	wire                             slice_top_boundary_dbf_to_sao;
	
    wire                             				sao_luma_dbf_to_sao;
	wire                             				sao_chroma_dbf_to_sao;
	wire		[SAOTYPE_WIDTH -1:0]   				y_sao_type_dbf_to_sao;
	wire		[BANDPOS_EO_WIDTH -1:0]				y_bandpos_or_eoclass_dbf_to_sao;
	wire		[SAO_OFFSET_WIDTH -1:0]				y_sao_offset_1_dbf_to_sao;
	wire		[SAO_OFFSET_WIDTH -1:0]				y_sao_offset_2_dbf_to_sao;
	wire		[SAO_OFFSET_WIDTH -1:0]				y_sao_offset_3_dbf_to_sao;
	wire		[SAO_OFFSET_WIDTH -1:0]				y_sao_offset_4_dbf_to_sao;
	wire		[SAOTYPE_WIDTH -1:0]   				cb_sao_type_dbf_to_sao;
	wire		[BANDPOS_EO_WIDTH -1:0]				cb_bandpos_or_eoclass_dbf_to_sao;
	wire		[SAO_OFFSET_WIDTH -1:0]				cb_sao_offset_1_dbf_to_sao;
	wire		[SAO_OFFSET_WIDTH -1:0]				cb_sao_offset_2_dbf_to_sao;
	wire		[SAO_OFFSET_WIDTH -1:0]				cb_sao_offset_3_dbf_to_sao;
	wire		[SAO_OFFSET_WIDTH -1:0]				cb_sao_offset_4_dbf_to_sao;
	wire		[SAOTYPE_WIDTH -1:0]   				cr_sao_type_dbf_to_sao;
	wire		[BANDPOS_EO_WIDTH -1:0]				cr_bandpos_or_eoclass_dbf_to_sao;
	wire		[SAO_OFFSET_WIDTH -1:0]				cr_sao_offset_1_dbf_to_sao;
	wire		[SAO_OFFSET_WIDTH -1:0]				cr_sao_offset_2_dbf_to_sao;
	wire		[SAO_OFFSET_WIDTH -1:0]				cr_sao_offset_3_dbf_to_sao;
	wire		[SAO_OFFSET_WIDTH -1:0]				cr_sao_offset_4_dbf_to_sao;
	
	wire     [11:0]                  Xc_out_sao;
    wire     [11:0]                  Yc_out_sao;
	wire	 [11:0]					 pic_width_out_sao ;
	wire	 [11:0]				     pic_height_out_sao;
	
    wire          [127:0]         y_q_block_1_dbf_to_sao;
    wire          [127:0]         y_q_block_2_dbf_to_sao;
    wire          [127:0]         y_q_block_3_dbf_to_sao;
    wire          [127:0]         y_q_block_4_dbf_to_sao;
    wire          [127:0]         y_A_block_dbf_to_sao  ;
    wire          [127:0]         y_B_block_dbf_to_sao  ;
    wire          [127:0]         y_C_block_dbf_to_sao  ;
    wire          [127:0]         y_D_block_dbf_to_sao  ;
    wire          [127:0]         y_E_block_dbf_to_sao  ;

    wire          [31:0]          cb_q_block_1_dbf_to_sao;
    wire          [31:0]          cb_q_block_2_dbf_to_sao;
    wire          [31:0]          cb_q_block_3_dbf_to_sao;
    wire          [31:0]          cb_q_block_4_dbf_to_sao;
    wire          [31:0]          cb_A_block_dbf_to_sao  ;
    wire          [31:0]          cb_B_block_dbf_to_sao  ;
    wire          [31:0]          cb_C_block_dbf_to_sao  ;
    wire          [31:0]          cb_D_block_dbf_to_sao  ;
    wire          [31:0]          cb_E_block_dbf_to_sao  ;

    wire          [31:0]          cr_q_block_1_dbf_to_sao;
    wire          [31:0]          cr_q_block_2_dbf_to_sao;
    wire          [31:0]          cr_q_block_3_dbf_to_sao;
    wire          [31:0]          cr_q_block_4_dbf_to_sao;
    wire          [31:0]          cr_A_block_dbf_to_sao  ;
    wire          [31:0]          cr_B_block_dbf_to_sao  ;
    wire          [31:0]          cr_C_block_dbf_to_sao  ;
    wire          [31:0]          cr_D_block_dbf_to_sao  ;
    wire          [31:0]          cr_E_block_dbf_to_sao  ;
	
	wire 	[POC_WIDTH-1:0] 	POC_dbf_to_sao;
	wire		[127:0]			y_q_block_1_out_sao  ;
	wire		[127:0]			y_q_block_2_out_sao  ;
	wire		[127:0]			y_q_block_3_out_sao  ;
	wire		[127:0]			y_q_block_4_out_sao  ;
	wire		[127:0]			y_A_block_out_sao    ;
	wire		[127:0]			y_B_block_out_sao    ;
	wire		[127:0]			y_C_block_out_sao    ;
	wire		[127:0]			y_D_block_out_sao    ;
	wire		[127:0]			y_E_block_out_sao    ;
	wire		[127:0]			y_J_block_out_sao    ;
	wire		[127:0]			y_K_block_out_sao    ;
	wire		[127:0]			y_M_block_out_sao    ;
	wire		[127:0]			y_N_block_out_sao    ;
	wire		[127:0]			y_G_block_out_sao    ;
	wire		[127:0]			y_H_block_out_sao    ;
	wire		[127:0]			y_I_block_out_sao    ;
	
	wire		[31:0]			cb_q_block_1_out_sao ;
	wire		[31:0]			cb_q_block_2_out_sao ;
	wire		[31:0]			cb_q_block_3_out_sao ;
	wire		[31:0]			cb_q_block_4_out_sao ;
	wire		[31:0]			cb_A_block_out_sao   ;
	wire		[31:0]			cb_B_block_out_sao   ;
	wire		[31:0]			cb_C_block_out_sao   ;
	wire		[31:0]			cb_D_block_out_sao   ;
	wire		[31:0]			cb_E_block_out_sao   ;
	wire		[31:0]			cb_J_block_out_sao   ;
	wire		[31:0]			cb_K_block_out_sao   ;
	wire		[31:0]			cb_M_block_out_sao   ;
	wire		[31:0]			cb_N_block_out_sao   ;
	wire		[31:0]			cb_G_block_out_sao   ;
	wire		[31:0]			cb_H_block_out_sao   ;
	wire		[31:0]			cb_I_block_out_sao   ;
	
	wire		[31:0]			cr_q_block_1_out_sao ;
	wire		[31:0]			cr_q_block_2_out_sao ;
	wire		[31:0]			cr_q_block_3_out_sao ;
	wire		[31:0]			cr_q_block_4_out_sao ;
	wire		[31:0]			cr_A_block_out_sao   ;
	wire		[31:0]			cr_B_block_out_sao   ;
	wire		[31:0]		    cr_C_block_out_sao   ;
	wire		[31:0]			cr_D_block_out_sao   ;
	wire		[31:0]			cr_E_block_out_sao   ;
	wire		[31:0]			cr_J_block_out_sao   ;
	wire		[31:0]			cr_K_block_out_sao   ;
	wire		[31:0]			cr_M_block_out_sao   ;
	wire		[31:0]			cr_N_block_out_sao   ;
	wire		[31:0]			cr_G_block_out_sao   ;
	wire		[31:0]			cr_H_block_out_sao   ;
	wire		[31:0]			cr_I_block_out_sao   ;	
	
	wire filter_applied_dbf_out;
	wire [7:0] luma_db_state;
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
    assign Xc_Yc_out = {4'd0,Xc, 4'd0,Yc};
	assign pic_width_out = pic_width_out_sao;
	assign pic_height_out = pic_height_out_sao;
	
	assign pred_2_dbf_cb = y_q_block_2;
	assign pred_2_dbf_cr = y_q_block_2_dbf_to_sao;
	
	assign dbf_main_in_valid = read_stage_valid;
	assign dbf_main_out_valid = dbf_main_to_sao_valid;
	
	
	assign dbf_to_sao_out_valid = dbf_main_to_sao_valid;
	assign dbf_to_sao_out_x = Xc_dbf_to_sao;
	assign dbf_to_sao_out_y = Yc_dbf_to_sao;
	assign dbf_to_sao_out_q1 = y_q_block_1_dbf_to_sao;
	
	// assign  controller_in = y_q_block_2		;
	// assign controller_out_valid = read_stage_valid & main_ready;
	// assign 	controller_out 	= y_A_block	;
	assign sao_wr_data = luma_db_state;
	
 integer hash_fp;
//synthesis translate_off

	initial begin
		hash_fp = $fopen("hash.csv");
		$fdisplay(hash_fp,"time,data");
	end

	always @(display_fifo_data) begin
		$fdisplay(hash_fp,"%d,%h",$time,display_fifo_data);
	end
//synthesis translate_on
	
`ifdef DBF_DISCONNECTED
	assign bs_fifo_full_out = 0;
	// assign cnfig_dbf_fifo_is_full_out = 0;
	assign y_dbf_fifo_is_full_out = 0;
	assign cb_dbf_fifo_is_full_out = 0;
	assign cr_dbf_fifo_is_full_out = 0;
	
    wire [BS_FIFO_WIDTH-1:0]    internal_bs_fifo_data_in        ;
    wire                        internal_bs_fifo_wr_en_in       ;
    wire                        internal_bs_fifo_full_out       ;
    
    // wire                                                	internal_cnfig_dbf_fifo_is_full_out;
    // wire [FIFO_IN_WIDTH-1:0]                            	internal_cnfig_dbf_fifo_data_in;
    // wire                                                	internal_cnfig_dbf_fifo_wr_en_in;
	
    wire                                                	internal_y_dbf_fifo_is_full_out;
    wire [Y_FIFO_WIDTH-1:0]                             	internal_y_dbf_fifo_data_in;
    wire                                                	internal_y_dbf_fifo_wr_en_in;
	
    wire                                            		internal_cb_dbf_fifo_is_full_out;
    wire   [CH_FIFO_WIDTH-1:0]                      		internal_cb_dbf_fifo_data_in;
    wire                                            		internal_cb_dbf_fifo_wr_en_in;
	
    wire                                               	internal_cr_dbf_fifo_is_full_out;
    wire [CH_FIFO_WIDTH-1:0]                           	internal_cr_dbf_fifo_data_in;
    wire                                               	internal_cr_dbf_fifo_wr_en_in; 
	
// fifo_write_driver 
	// #(
		// .WIDTH 		(32),
		// .FILE_NAME  ("pred_to_dbf_config"),
		// .EMPTY_MODEL(0)
	// )
	// pred_to_dbf_config_driver (
    // .clk(clk), 
    // .reset(reset), 
    // .out(internal_cnfig_dbf_fifo_data_in), 
    // .prog_full(internal_cnfig_dbf_fifo_is_full_out), 
    // .wr_en(internal_cnfig_dbf_fifo_wr_en_in)
    // );


fifo_write_driver 
	#(
		.WIDTH 		(8),
		.FILE_NAME  ("pred_to_dbf_bs"),
		.EMPTY_MODEL(0)
	)
	pred_to_dbf_bs_driver (
    .clk(clk), 
    .reset(reset), 
    .out({internal_bs_fifo_data_in[01:00],internal_bs_fifo_data_in[03:02],internal_bs_fifo_data_in[05:04],internal_bs_fifo_data_in[07:06]}), 
    .prog_full(internal_bs_fifo_full_out), 
    .wr_en(internal_bs_fifo_wr_en_in)
    );	

	wire [160-1:0] internal_y_dbf_fifo_data_in_long;
	assign internal_y_dbf_fifo_data_in = {internal_y_dbf_fifo_data_in_long[2:0],internal_y_dbf_fifo_data_in_long[16+2:16],internal_y_dbf_fifo_data_in_long[128+32-1:32]};
fifo_write_driver 
	#(
		.WIDTH 		(160),
		.FILE_NAME  ("pred_to_dbf_yy"),
		.EMPTY_MODEL(0)
	)
	pred_to_dbf_yy_driver (
    .clk(clk), 
    .reset(reset), 
    .out(internal_y_dbf_fifo_data_in_long), 
    .prog_full(internal_y_dbf_fifo_is_full_out), 
    .wr_en(internal_y_dbf_fifo_wr_en_in)
    );	
	
fifo_write_driver 
	#(
		.WIDTH 		(128),
		.FILE_NAME  ("pred_to_dbf_cb"),
		.EMPTY_MODEL(0)
	)
	pred_to_dbf_cb_driver (
    .clk(clk), 
    .reset(reset), 
    .out(internal_cb_dbf_fifo_data_in), 
    .prog_full(internal_cb_dbf_fifo_is_full_out), 
    .wr_en(internal_cb_dbf_fifo_wr_en_in)
    );	

fifo_write_driver 
	#(
		.WIDTH 		(128),
		.FILE_NAME  ("pred_to_dbf_cr"),
		.EMPTY_MODEL(0)
	)
	pred_to_dbf_cr_driver (
    .clk(clk), 
    .reset(reset), 
    .out(internal_cr_dbf_fifo_data_in), 
    .prog_full(internal_cr_dbf_fifo_is_full_out), 
    .wr_en(internal_cr_dbf_fifo_wr_en_in)
    );	
`endif
	
// Instantiate the module
read_data_stage read_stage_block (
    .clk(clk), 
    .reset(reset), 
    
    .ready_in(main_ready), 
    .valid_out(read_stage_valid), 
	

    
    .cnfig_dbf_fifo_is_full_out(cnfig_dbf_fifo_is_full_out), 
    .cnfig_dbf_fifo_data_in(cnfig_dbf_fifo_data_in), 
    .cnfig_dbf_fifo_wr_en_in(cnfig_dbf_fifo_wr_en_in), 
`ifndef DBF_DISCONNECTED

    .bs_fifo_data_in(bs_fifo_data_in), 
    .bs_fifo_wr_en_in(bs_fifo_wr_en_in), 
    .bs_fifo_full_out(bs_fifo_full_out), 
	
    .y_dbf_fifo_is_full_out(y_dbf_fifo_is_full_out), 
    .y_dbf_fifo_data_in(y_dbf_fifo_data_in), 
    .y_dbf_fifo_wr_en_in(y_dbf_fifo_wr_en_in), 
    
    .cb_dbf_fifo_is_full_out(cb_dbf_fifo_is_full_out), 
    .cb_dbf_fifo_data_in(cb_dbf_fifo_data_in), 
    .cb_dbf_fifo_wr_en_in(cb_dbf_fifo_wr_en_in), 
    
    .cr_dbf_fifo_is_full_out(cr_dbf_fifo_is_full_out), 
    .cr_dbf_fifo_data_in(cr_dbf_fifo_data_in), 
    .cr_dbf_fifo_wr_en_in(cr_dbf_fifo_wr_en_in), 
    
`else
    .bs_fifo_data_in(internal_bs_fifo_data_in), 
    .bs_fifo_wr_en_in(internal_bs_fifo_wr_en_in), 
    .bs_fifo_full_out(internal_bs_fifo_full_out), 
    
    // .cnfig_dbf_fifo_is_full_out(internal_cnfig_dbf_fifo_is_full_out), 
    // .cnfig_dbf_fifo_data_in(internal_cnfig_dbf_fifo_data_in), 
    // .cnfig_dbf_fifo_wr_en_in(internal_cnfig_dbf_fifo_wr_en_in), 
    
    .y_dbf_fifo_is_full_out(internal_y_dbf_fifo_is_full_out), 
    .y_dbf_fifo_data_in(internal_y_dbf_fifo_data_in), 
    .y_dbf_fifo_wr_en_in(internal_y_dbf_fifo_wr_en_in), 
    
    .cb_dbf_fifo_is_full_out(internal_cb_dbf_fifo_is_full_out), 
    .cb_dbf_fifo_data_in(internal_cb_dbf_fifo_data_in), 
    .cb_dbf_fifo_wr_en_in(internal_cb_dbf_fifo_wr_en_in), 
    
    .cr_dbf_fifo_is_full_out(internal_cr_dbf_fifo_is_full_out), 
    .cr_dbf_fifo_data_in(internal_cr_dbf_fifo_data_in), 
    .cr_dbf_fifo_wr_en_in(internal_cr_dbf_fifo_wr_en_in), 
`endif
    .y_q_block_1_out(y_q_block_1), 
    .y_q_block_2_out(y_q_block_2), 
    .y_q_block_3_out(y_q_block_3), 
    .y_q_block_4_out(y_q_block_4), 
    
    .y_A_block_out(y_A_block), 
    .y_B_block_out(y_B_block), 
    .cb_q_block_1_out(cb_q_block_1), 
    .cb_q_block_2_out(cb_q_block_2), 
    .cb_q_block_3_out(cb_q_block_3), 
    .cb_q_block_4_out(cb_q_block_4), 
    .cb_A_block_out(cb_A_block), 
    .cb_B_block_out(cb_B_block), 
    .cr_q_block_1_out(cr_q_block_1), 
    .cr_q_block_2_out(cr_q_block_2), 
    .cr_q_block_3_out(cr_q_block_3), 
    .cr_q_block_4_out(cr_q_block_4), 
    .cr_A_block_out(cr_A_block), 
    .cr_B_block_out(cr_B_block),
    
    .Xc_out(Xc), 
    .Yc_out(Yc), 
    
    .qp_1_out(qp_1), 
    .qp_A_out(qp_A), 
    .BS_v1_out(BS_v1), 
    .BS_v2_out(BS_v2), 
    .BS_h1_out(BS_h1), 
    .BS_h2_out(BS_h2), 
    .BS_h0_out(BS_h0), 
    
    .pps_cb_qp_offset		(pps_cb_qp_offset), 
    .pps_cr_qp_offset		(pps_cr_qp_offset), 
    .slice_tc_offset_div2	(slice_tc_offset_div2), 
    .slice_beta_offset_div2	(slice_beta_offset_div2), 
    .pic_width				(pic_width), 
    .pic_height				(pic_height), 
    .ctb_size				(ctb_size), 
    .log2_ctb_size_out		(log2_ctb_size_to_dbf), 
	.h0_edge_dbf_disable		(h0_edge_dbf_disable),
	.pcm_bypass_AB_dbf_disable	(pcm_bypass_AB_dbf_disable),
    .slice_dbf_disable	(slice_dbf_disable),
	.pcm_loop_filter_disabled(pcm_loop_filter_disabled),
	.loop_filter_accross_tiles_enabled (loop_filter_accross_tiles_enabled),
	.loop_filter_accross_slices_enabled(loop_filter_accross_slices_enabled),
	.tile_x (tile_x),
	.tile_y (tile_y),
	.slice_x(slice_x),
	.slice_y(slice_y),
	.actual_slice_x(actual_slice_x),
	.actual_slice_y(actual_slice_y),
	
    .poc_out				(poc_read_to_main), 
	.current_dpb_idx_out	(dpb_idx_read_to_main),
    .tile_id				(tile_id), 
    .slice_id				(slice_id), 
    .sao_luma				(sao_luma), 
    .sao_chroma				(sao_chroma), 
    .pcm					(pcm), 
    .bypass					(bypass), 
    .y_sao_type				(y_sao_type), 
    .y_bandpos_or_eoclass	(y_bandpos_or_eoclass), 
    .y_sao_offset_1			(y_sao_offset_1), 
    .y_sao_offset_2			(y_sao_offset_2), 
    .y_sao_offset_3			(y_sao_offset_3), 
    .y_sao_offset_4			(y_sao_offset_4), 
    .cb_sao_type(cb_sao_type), 
    .cb_bandpos_or_eoclass(cb_bandpos_or_eoclass), 
    .cb_sao_offset_1(cb_sao_offset_1), 
    .cb_sao_offset_2(cb_sao_offset_2), 
    .cb_sao_offset_3(cb_sao_offset_3), 
    .cb_sao_offset_4(cb_sao_offset_4), 
    .cr_sao_type(cr_sao_type), 
    .cr_bandpos_or_eoclass(cr_bandpos_or_eoclass), 
    .cr_sao_offset_1(cr_sao_offset_1), 
    .cr_sao_offset_2(cr_sao_offset_2), 
    .cr_sao_offset_3(cr_sao_offset_3), 
    .cr_sao_offset_4(cr_sao_offset_4)
	
    ,.pred_2_dbf_rd_inf(pred_2_dbf_rd_inf)
    ,.pred_2_dbf_yy(pred_2_dbf_yy)
    // ,.read_state_q2_we(controller_in_valid)
    // ,.pred_2_dbf_cb(pred_2_dbf_cb)
    // ,.pred_2_dbf_cr(pred_2_dbf_cr)

	
	
    );




// Instantiate the module
main_db_stage main_stage_block (
    .clk(clk), 
    .reset(reset), 
    .data_valid_in(read_stage_valid), 
    .ready_out(main_ready), 
    .ready_in(sao_ready), 
    .valid_out(dbf_main_to_sao_valid), 
    .y_q_block_1_in(y_q_block_1), 
    .y_q_block_2_in(y_q_block_2), 
    .y_q_block_3_in(y_q_block_3), 
    .y_q_block_4_in(y_q_block_4), 
    .y_A_block_in(y_A_block), 
    .y_B_block_in(y_B_block), 
    .cb_q_block_1_in(cb_q_block_1), 
    .cb_q_block_2_in(cb_q_block_2), 
    .cb_q_block_3_in(cb_q_block_3), 
    .cb_q_block_4_in(cb_q_block_4), 
    .cb_A_block_in(cb_A_block), 
    .cb_B_block_in(cb_B_block), 
    .cr_q_block_1_in(cr_q_block_1), 
    .cr_q_block_2_in(cr_q_block_2), 
    .cr_q_block_3_in(cr_q_block_3), 
    .cr_q_block_4_in(cr_q_block_4), 
    .cr_A_block_in(cr_A_block), 
    .cr_B_block_in(cr_B_block), 
    .Xc_in(Xc), 
    .Yc_in(Yc), 
    .qp_1_in(qp_1), 
    .qp_A_in(qp_A), 
    .BS_v1_in(BS_v1), 
    .BS_v2_in(BS_v2), 
    .BS_h1_in(BS_h1), 
    .BS_h2_in(BS_h2), 
    .BS_h0_in(BS_h0), 
    .pps_cb_qp_offset_in(pps_cb_qp_offset), 
    .pps_cr_qp_offset_in(pps_cr_qp_offset), 
    .slice_tc_offset_div2_in(slice_tc_offset_div2), 
    .slice_beta_offset_div2_in(slice_beta_offset_div2), 
    .pic_width_in(pic_width), 
    .pic_height_in(pic_height),
	.ctb_size_in(ctb_size),
	.log2_ctb_size_in		(log2_ctb_size_to_dbf), 
	.tile_id_in (tile_id),
	.slice_id_in(slice_id),
	.h0_edge_dbf_disable_in(h0_edge_dbf_disable),
	.pcm_bypass_AB_dbf_disable_in(pcm_bypass_AB_dbf_disable),
	.slice_dbf_disable_in(slice_dbf_disable),
	.pcm_loop_filter_disabled_in(pcm_loop_filter_disabled),
	.loop_filter_accross_tiles_enabled_in (loop_filter_accross_tiles_enabled),
	.loop_filter_accross_slices_enabled_in(loop_filter_accross_slices_enabled),
	.tile_x_in (tile_x ),
	.tile_y_in (tile_y ),
	.slice_x_in(slice_x),
	.slice_y_in(slice_y),
	.actual_slice_x_in(actual_slice_x),
	.actual_slice_y_in(actual_slice_y),
	.sao_luma_in(sao_luma),
	.sao_chroma_in(sao_chroma),
	.pcm_in(pcm),
	.bypass_in(bypass),
    .y_sao_type_in(y_sao_type),
    .y_bandpos_or_eoclass_in(y_bandpos_or_eoclass),
    .y_sao_offset_1_in(y_sao_offset_1),
    .y_sao_offset_2_in(y_sao_offset_2),
    .y_sao_offset_3_in(y_sao_offset_3),
    .y_sao_offset_4_in(y_sao_offset_4),
    .cb_sao_type_in(cb_sao_type),
    .cb_bandpos_or_eoclass_in(cb_bandpos_or_eoclass),
    .cb_sao_offset_1_in(cb_sao_offset_1),
    .cb_sao_offset_2_in(cb_sao_offset_2),
    .cb_sao_offset_3_in(cb_sao_offset_3),
    .cb_sao_offset_4_in(cb_sao_offset_4),
    .cr_sao_type_in(cr_sao_type),
    .cr_bandpos_or_eoclass_in(cr_bandpos_or_eoclass),
    .cr_sao_offset_1_in(cr_sao_offset_1),
    .cr_sao_offset_2_in(cr_sao_offset_2),
    .cr_sao_offset_3_in(cr_sao_offset_3),
    .cr_sao_offset_4_in(cr_sao_offset_4),
    .poc_in(poc_read_to_main), 
	.current_dpb_idx_in(dpb_idx_read_to_main),
	
	.poc_out(POC_dbf_to_sao),
	.current_dpb_idx_out(dpb_idx_dbf_to_sao),
	.Xc_out(Xc_dbf_to_sao),
	.Yc_out(Yc_dbf_to_sao),
	.pic_width_out(pic_width_out_dbf_to_sao), 
    .pic_height_out(pic_height_out_dbf_to_sao),
	.ctb_size_out(),
	.log2_ctb_size_out		(log2_ctb_size_to_sao), 
	.tile_id_out (),
	.slice_id_out(slice_id_dbf_to_sao),
	.pcm_bypass_dbf_disable(pcm_bypass_dbf_to_sao),
	.pcm_loop_filter_disabled_out(),
	.loop_filter_accross_tiles_enabled_out (loop_filter_accross_tiles_enabled_dbf_to_sao ),
	.loop_filter_accross_slices_enabled_out(loop_filter_accross_slices_enabled_dbf_to_sao),
	.tile_left_boundary (tile_left_boundary_dbf_to_sao ),
	.tile_top_boundary  (tile_top_boundary_dbf_to_sao  ),
	.slice_left_boundary(slice_left_boundary_dbf_to_sao),
	.slice_top_boundary (slice_top_boundary_dbf_to_sao ),
	.tile_x_out (),
	.tile_y_out (),
	.slice_x_out(),
	.slice_y_out(),
	.actual_slice_x_out(),
	.actual_slice_y_out(),
	.sao_luma_out  (sao_luma_dbf_to_sao),
	.sao_chroma_out(sao_chroma_dbf_to_sao),
	.y_sao_type_out				(y_sao_type_dbf_to_sao), 
	.y_bandpos_or_eoclass_out	(y_bandpos_or_eoclass_dbf_to_sao), 
	.y_sao_offset_1_out			(y_sao_offset_1_dbf_to_sao), 
	.y_sao_offset_2_out			(y_sao_offset_2_dbf_to_sao), 
	.y_sao_offset_3_out			(y_sao_offset_3_dbf_to_sao), 
	.y_sao_offset_4_out			(y_sao_offset_4_dbf_to_sao), 
	.cb_sao_type_out			(cb_sao_type_dbf_to_sao), 
	.cb_bandpos_or_eoclass_out	(cb_bandpos_or_eoclass_dbf_to_sao), 
	.cb_sao_offset_1_out		(cb_sao_offset_1_dbf_to_sao), 
	.cb_sao_offset_2_out		(cb_sao_offset_2_dbf_to_sao), 
	.cb_sao_offset_3_out		(cb_sao_offset_3_dbf_to_sao), 
	.cb_sao_offset_4_out		(cb_sao_offset_4_dbf_to_sao), 
	.cr_sao_type_out			(cr_sao_type_dbf_to_sao), 
	.cr_bandpos_or_eoclass_out	(cr_bandpos_or_eoclass_dbf_to_sao), 
	.cr_sao_offset_1_out		(cr_sao_offset_1_dbf_to_sao), 
	.cr_sao_offset_2_out		(cr_sao_offset_2_dbf_to_sao), 
	.cr_sao_offset_3_out		(cr_sao_offset_3_dbf_to_sao), 
	.cr_sao_offset_4_out		(cr_sao_offset_4_dbf_to_sao),
    .y_q_block_1_out(y_q_block_1_dbf_to_sao), 
    .y_q_block_2_out(y_q_block_2_dbf_to_sao), 
    .y_q_block_3_out(y_q_block_3_dbf_to_sao), 
    .y_q_block_4_out(y_q_block_4_dbf_to_sao), 
    .y_A_block_out(y_A_block_dbf_to_sao), 
    .y_B_block_out(y_B_block_dbf_to_sao), 
    .y_C_block_out(y_C_block_dbf_to_sao), 
    .y_D_block_out(y_D_block_dbf_to_sao), 
    .y_E_block_out(y_E_block_dbf_to_sao), 
    .cb_q_block_1_out(cb_q_block_1_dbf_to_sao), 
    .cb_q_block_2_out(cb_q_block_2_dbf_to_sao), 
    .cb_q_block_3_out(cb_q_block_3_dbf_to_sao), 
    .cb_q_block_4_out(cb_q_block_4_dbf_to_sao), 
    .cb_A_block_out(cb_A_block_dbf_to_sao), 
    .cb_B_block_out(cb_B_block_dbf_to_sao), 
    .cb_C_block_out(cb_C_block_dbf_to_sao), 
    .cb_D_block_out(cb_D_block_dbf_to_sao), 
    .cb_E_block_out(cb_E_block_dbf_to_sao), 
    .cr_q_block_1_out(cr_q_block_1_dbf_to_sao), 
    .cr_q_block_2_out(cr_q_block_2_dbf_to_sao), 
    .cr_q_block_3_out(cr_q_block_3_dbf_to_sao), 
    .cr_q_block_4_out(cr_q_block_4_dbf_to_sao), 
    .cr_A_block_out(cr_A_block_dbf_to_sao), 
    .cr_B_block_out(cr_B_block_dbf_to_sao), 
    .cr_C_block_out(cr_C_block_dbf_to_sao), 
    .cr_D_block_out(cr_D_block_dbf_to_sao), 
    .cr_E_block_out(cr_E_block_dbf_to_sao)
	
	,.a_block_in_data	(controller_in	)
	,.a_block_in_valid	(controller_in_valid	)
	,.a_block_out_valid	(controller_out_valid	)
	,.luma_db_state	    (luma_db_state		)
	,.a_block_out_data	(controller_out	)	
	
	
    );
	
// Instantiate the module
main_sao_stage_wo_dp_fifo main_sao_block(
		.clk                      (clk ),																	
        .reset                    (reset ),                                                                 
        .data_valid_in            (dbf_main_to_sao_valid ),                                                 
        .ready_out                (sao_ready ),                                                             

		.y_q_block_1_in           (y_q_block_1_dbf_to_sao),                                         		
		.y_q_block_2_in           (y_q_block_2_dbf_to_sao),                                         		
		.y_q_block_3_in           (y_q_block_3_dbf_to_sao),                                         		
		.y_q_block_4_in           (y_q_block_4_dbf_to_sao),                                         		
		.y_A_block_in             (y_A_block_dbf_to_sao),                                           		
		.y_B_block_in             (y_B_block_dbf_to_sao),                                           		
		.y_C_block_in             (y_C_block_dbf_to_sao),                                           		
		.y_D_block_in             (y_D_block_dbf_to_sao),                                           		
		.y_E_block_in             (y_E_block_dbf_to_sao),                                           		
		.cb_q_block_1_in          (cb_q_block_1_dbf_to_sao),                                        		
		.cb_q_block_2_in          (cb_q_block_2_dbf_to_sao),                                        		
		.cb_q_block_3_in          (cb_q_block_3_dbf_to_sao),                                        		
		.cb_q_block_4_in          (cb_q_block_4_dbf_to_sao),                                        		
		.cb_A_block_in            (cb_A_block_dbf_to_sao),                                          		
		.cb_B_block_in            (cb_B_block_dbf_to_sao),                                          		
		.cb_C_block_in            (cb_C_block_dbf_to_sao),                                          		
		.cb_D_block_in            (cb_D_block_dbf_to_sao),                                          		
		.cb_E_block_in            (cb_E_block_dbf_to_sao),                                          		
		.cr_q_block_1_in          (cr_q_block_1_dbf_to_sao),                                        		
		.cr_q_block_2_in          (cr_q_block_2_dbf_to_sao),                                        		
		.cr_q_block_3_in          (cr_q_block_3_dbf_to_sao),                                        		
		.cr_q_block_4_in          (cr_q_block_4_dbf_to_sao),                                        		
		.cr_A_block_in            (cr_A_block_dbf_to_sao),                                          		
		.cr_B_block_in            (cr_B_block_dbf_to_sao),                                          		
		.cr_C_block_in            (cr_C_block_dbf_to_sao),                                          		
		.cr_D_block_in            (cr_D_block_dbf_to_sao),                                          		
		.cr_E_block_in            (cr_E_block_dbf_to_sao),                                          		
		.Xc_in                    (Xc_dbf_to_sao ),                                                 		
		.Yc_in                    (Yc_dbf_to_sao ),                                                 		
		.pic_width_in             (pic_width_out_dbf_to_sao ),                                              
        .pic_height_in            (pic_height_out_dbf_to_sao ),                                     		
		.poc_in			  		  (POC_dbf_to_sao),                                            		
		.dbp_idx_in				  (dpb_idx_dbf_to_sao),                                             		
		.log2_ctb_size_in         (log2_ctb_size_to_sao ),                                                              		
		.tile_id_in               ( ),                                                              		
		.slice_id_in              (slice_id_dbf_to_sao),   
		.pcm_bypass_sao_disable_in(pcm_bypass_dbf_to_sao),
		.loop_filter_accross_tiles_enabled_in (loop_filter_accross_tiles_enabled_dbf_to_sao ),
		.loop_filter_accross_slices_enabled_in(loop_filter_accross_slices_enabled_dbf_to_sao),
		.tile_left_boundary_in 	  (tile_left_boundary_dbf_to_sao ),
		.tile_top_boundary_in     (tile_top_boundary_dbf_to_sao  ),
		.slice_left_boundary_in   (slice_left_boundary_dbf_to_sao),
		.slice_top_boundary_in    (slice_top_boundary_dbf_to_sao ),
		.sao_luma_in              (sao_luma_dbf_to_sao  ),
		.sao_chroma_in            (sao_chroma_dbf_to_sao),
		.y_sao_type_in            (y_sao_type_dbf_to_sao),                                                                       
        .y_bandpos_or_eoclass_in  (y_bandpos_or_eoclass_dbf_to_sao),                                                                       
        .y_sao_offset_1_in        (y_sao_offset_1_dbf_to_sao),                                                                       
        .y_sao_offset_2_in        (y_sao_offset_2_dbf_to_sao),                                                                       
        .y_sao_offset_3_in        (y_sao_offset_3_dbf_to_sao),                                                                       
        .y_sao_offset_4_in        (y_sao_offset_4_dbf_to_sao),                                                                       
        .cb_sao_type_in           (cb_sao_type_dbf_to_sao),                                                                       
        .cb_bandpos_or_eoclass_in (cb_bandpos_or_eoclass_dbf_to_sao),                                                                       
        .cb_sao_offset_1_in       (cb_sao_offset_1_dbf_to_sao),                                                                       
        .cb_sao_offset_2_in       (cb_sao_offset_2_dbf_to_sao),                                                                       
        .cb_sao_offset_3_in       (cb_sao_offset_3_dbf_to_sao),                                                                       
        .cb_sao_offset_4_in       (cb_sao_offset_4_dbf_to_sao),                                                                       
        .cr_sao_type_in           (cr_sao_type_dbf_to_sao),                                                                       
        .cr_bandpos_or_eoclass_in (cr_bandpos_or_eoclass_dbf_to_sao),                                                                       
        .cr_sao_offset_1_in       (cr_sao_offset_1_dbf_to_sao),                                                                       
        .cr_sao_offset_2_in       (cr_sao_offset_2_dbf_to_sao),                                                               		
        .cr_sao_offset_3_in       (cr_sao_offset_3_dbf_to_sao),                                                               		
        .cr_sao_offset_4_in       (cr_sao_offset_4_dbf_to_sao),                                                              		
		.pic_width_out            (pic_width_out_sao ),                                             		
		.pic_height_out           (pic_height_out_sao ),                                            		
		.tile_id_out              ( ),                                                              		
		.slice_id_out             ( ),
		.display_fifo_is_full		(display_fifo_is_full),
		.display_fifo_data_wr_en	(display_fifo_data_wr_en),		
		.display_fifo_data			(display_fifo_data),		
		.dpb_fifo_is_empty_out		(dpb_fifo_is_empty_out),
		.dpb_fifo_rd_en_in			(dpb_fifo_rd_en_in),		
		.dpb_fifo_data_out			(dpb_fifo_data_out),
		.dpb_axi_addr_out			(dpb_axi_addr_out),
		.poc_out			(sao_poc_out)  ,
		.log2_ctu_size_out			(log2_ctu_size_out)  
		
		// ,.controller_in_valid 	(controller_in_valid 	)
		// ,.controller_in 		(controller_in 			)
		// ,.controller_out_valid 	(controller_out_valid 	)
		// ,.controller_out 		(controller_out 		)
		,.sao_wr_en_out 		(sao_wr_en_out 			)
		,.sao_rd_en_out 		(sao_rd_en_out 			)
		// ,.sao_wr_data 			(sao_wr_data 			)
		,.sao_rd_data 			(sao_rd_data 			)
		,.display_buf_ful		(display_buf_ful)
    );                                                                                                      

	// synthesis translate_off                                                                          		
integer correct_sao_file;                                                                           		

initial begin                                                                                       		
	correct_sao_file = $fopen("BQSquare_416x240_60_qp37_416x240_8bit_pre-sao.yuv","rb");            		
end                                                                                                 		

always@(posedge clk) begin                                                                                  
	if(dbf_main_to_sao_valid) begin 
`ifndef VERIFY_NONE
		sao_in_verify();
`endif
	end
	if(read_stage_block.cnfig_dbf_fifo_data_out [07:00] == 8'hff) begin
		 $display("%d, end of file stop",$time);
		 $stop;
	end
end


task sao_in_verify;
	begin
		case({Yc_dbf_to_sao == 0, Xc_dbf_to_sao == 0, Yc_dbf_to_sao == (pic_height-8), Xc_dbf_to_sao == (pic_width -8)})
			// case( top, left, bottom , right)
			4'b0000: begin
				sao_4x4_verify(y_C_block_dbf_to_sao,	cb_C_block_dbf_to_sao,		cr_C_block_dbf_to_sao,										Xc_dbf_to_sao-4,Yc_dbf_to_sao-4);
				#1;
				sao_4x4_verify(y_D_block_dbf_to_sao,	cb_D_block_dbf_to_sao,		cr_D_block_dbf_to_sao,										Xc_dbf_to_sao,Yc_dbf_to_sao-4);
				#1;
				sao_4x4_verify(y_A_block_dbf_to_sao,	cb_A_block_dbf_to_sao,		cr_A_block_dbf_to_sao,										Xc_dbf_to_sao-4,Yc_dbf_to_sao);
				#1;
				sao_4x4_verify(y_q_block_1_dbf_to_sao,	cb_q_block_1_dbf_to_sao,	cr_q_block_1_dbf_to_sao,									Xc_dbf_to_sao,Yc_dbf_to_sao);
			end
			4'b1100: begin
				sao_4x4_verify(y_q_block_1_dbf_to_sao,	cb_q_block_1_dbf_to_sao,	cr_q_block_1_dbf_to_sao,									Xc_dbf_to_sao,Yc_dbf_to_sao);
			end
			4'b0100: begin
				sao_4x4_verify(y_q_block_1_dbf_to_sao,	cb_q_block_1_dbf_to_sao,	cr_q_block_1_dbf_to_sao,									Xc_dbf_to_sao,Yc_dbf_to_sao);
				sao_4x4_verify(y_D_block_dbf_to_sao,	cb_D_block_dbf_to_sao,		cr_D_block_dbf_to_sao,										Xc_dbf_to_sao,Yc_dbf_to_sao-4);
			end
			4'b1000: begin
				sao_4x4_verify(y_q_block_1_dbf_to_sao,	cb_q_block_1_dbf_to_sao,	cr_q_block_1_dbf_to_sao,									Xc_dbf_to_sao,Yc_dbf_to_sao);
				sao_4x4_verify(y_A_block_dbf_to_sao,	cb_A_block_dbf_to_sao,		cr_A_block_dbf_to_sao,										Xc_dbf_to_sao-4,Yc_dbf_to_sao);
			end
			4'b0010: begin
				sao_4x4_verify(y_C_block_dbf_to_sao,	cb_C_block_dbf_to_sao,		cr_C_block_dbf_to_sao,										Xc_dbf_to_sao-4,Yc_dbf_to_sao-4);
				sao_4x4_verify(y_D_block_dbf_to_sao,	cb_D_block_dbf_to_sao,		cr_D_block_dbf_to_sao,										Xc_dbf_to_sao,Yc_dbf_to_sao-4);
				sao_4x4_verify(y_A_block_dbf_to_sao,	cb_A_block_dbf_to_sao,		cr_A_block_dbf_to_sao,										Xc_dbf_to_sao-4,Yc_dbf_to_sao);
				sao_4x4_verify(y_q_block_1_dbf_to_sao,	cb_q_block_1_dbf_to_sao,	cr_q_block_1_dbf_to_sao,									Xc_dbf_to_sao,Yc_dbf_to_sao);
				sao_4x4_verify(y_B_block_dbf_to_sao,	cb_B_block_dbf_to_sao,		cr_B_block_dbf_to_sao,										Xc_dbf_to_sao-4,Yc_dbf_to_sao+4);
				sao_4x4_verify(y_q_block_3_dbf_to_sao,	cb_q_block_3_dbf_to_sao,	cr_q_block_3_dbf_to_sao,									Xc_dbf_to_sao,Yc_dbf_to_sao+4);
			end
			4'b0110: begin
				sao_4x4_verify(y_D_block_dbf_to_sao,	cb_D_block_dbf_to_sao,		cr_D_block_dbf_to_sao,										Xc_dbf_to_sao,Yc_dbf_to_sao-4);
				sao_4x4_verify(y_q_block_1_dbf_to_sao,	cb_q_block_1_dbf_to_sao,	cr_q_block_1_dbf_to_sao,									Xc_dbf_to_sao,Yc_dbf_to_sao);
				sao_4x4_verify(y_q_block_3_dbf_to_sao,	cb_q_block_3_dbf_to_sao,	cr_q_block_3_dbf_to_sao,									Xc_dbf_to_sao,Yc_dbf_to_sao+4);
			end
			4'b0011: begin
				sao_4x4_verify(y_C_block_dbf_to_sao,	cb_C_block_dbf_to_sao,		cr_C_block_dbf_to_sao,										Xc_dbf_to_sao-4,Yc_dbf_to_sao-4);
				sao_4x4_verify(y_D_block_dbf_to_sao,	cb_D_block_dbf_to_sao,		cr_D_block_dbf_to_sao,										Xc_dbf_to_sao,Yc_dbf_to_sao-4);
				sao_4x4_verify(y_A_block_dbf_to_sao,	cb_A_block_dbf_to_sao,		cr_A_block_dbf_to_sao,										Xc_dbf_to_sao-4,Yc_dbf_to_sao);
				sao_4x4_verify(y_q_block_1_dbf_to_sao,	cb_q_block_1_dbf_to_sao,	cr_q_block_1_dbf_to_sao,									Xc_dbf_to_sao,Yc_dbf_to_sao);
				sao_4x4_verify(y_B_block_dbf_to_sao,	cb_B_block_dbf_to_sao,		cr_B_block_dbf_to_sao,										Xc_dbf_to_sao-4,Yc_dbf_to_sao+4);
				sao_4x4_verify(y_q_block_3_dbf_to_sao,	cb_q_block_3_dbf_to_sao,	cr_q_block_3_dbf_to_sao,									Xc_dbf_to_sao,Yc_dbf_to_sao+4);
				sao_4x4_verify(y_E_block_dbf_to_sao,	cb_E_block_dbf_to_sao,		cr_E_block_dbf_to_sao,										Xc_dbf_to_sao+4,Yc_dbf_to_sao-4);
				sao_4x4_verify(y_q_block_2_dbf_to_sao,	cb_q_block_2_dbf_to_sao,	cr_q_block_2_dbf_to_sao,									Xc_dbf_to_sao+4,Yc_dbf_to_sao);
				sao_4x4_verify(y_q_block_4_dbf_to_sao,	cb_q_block_4_dbf_to_sao,	cr_q_block_4_dbf_to_sao,									Xc_dbf_to_sao+4,Yc_dbf_to_sao+4);
			end
			4'b1001: begin
				sao_4x4_verify(y_A_block_dbf_to_sao,	cb_A_block_dbf_to_sao,		cr_A_block_dbf_to_sao,										Xc_dbf_to_sao-4,Yc_dbf_to_sao);
				sao_4x4_verify(y_q_block_1_dbf_to_sao,	cb_q_block_1_dbf_to_sao,	cr_q_block_1_dbf_to_sao,									Xc_dbf_to_sao,Yc_dbf_to_sao);
				sao_4x4_verify(y_q_block_2_dbf_to_sao,	cb_q_block_2_dbf_to_sao,	cr_q_block_2_dbf_to_sao,									Xc_dbf_to_sao+4,Yc_dbf_to_sao);
			end
			4'b0001: begin
				sao_4x4_verify(y_C_block_dbf_to_sao,	cb_C_block_dbf_to_sao,		cr_C_block_dbf_to_sao,										Xc_dbf_to_sao-4,Yc_dbf_to_sao-4);
				sao_4x4_verify(y_D_block_dbf_to_sao,	cb_D_block_dbf_to_sao,		cr_D_block_dbf_to_sao,										Xc_dbf_to_sao,Yc_dbf_to_sao-4);
				sao_4x4_verify(y_A_block_dbf_to_sao,	cb_A_block_dbf_to_sao,		cr_A_block_dbf_to_sao,										Xc_dbf_to_sao-4,Yc_dbf_to_sao);
				sao_4x4_verify(y_q_block_1_dbf_to_sao,	cb_q_block_1_dbf_to_sao,	cr_q_block_1_dbf_to_sao,									Xc_dbf_to_sao,Yc_dbf_to_sao);
				sao_4x4_verify(y_E_block_dbf_to_sao,	cb_E_block_dbf_to_sao,		cr_E_block_dbf_to_sao,										Xc_dbf_to_sao+4,Yc_dbf_to_sao-4);
				sao_4x4_verify(y_q_block_2_dbf_to_sao,	cb_q_block_2_dbf_to_sao,	cr_q_block_2_dbf_to_sao,									Xc_dbf_to_sao+4,Yc_dbf_to_sao);
			end			
		endcase
	end
endtask

task sao_4x4_verify;
	input [128-1:0] sao_luma_4x4_hard;
	input [32-1:0] sao_cb_4x4_hard;
	input [32-1:0] sao_cr_4x4_hard;
	input [11:0] x_loc;
	input [11:0] y_loc;
	integer  frame_offset_sao_in;
	integer i,j;
	reg [7:0] luma_soft [0:15];
	reg [7:0] chma_soft [0:3];
	reg [7:0] temp;
	begin
		frame_offset_sao_in = pic_height*pic_width * POC_dbf_to_sao *3 /2;
		//$display("dbf out block x=%d y=%d filter_applied=%d",x_loc,y_loc,uut.main_stage_block.filter_applied_out);
		for (j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin
			for (i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin
				temp =$fseek(correct_sao_file,(frame_offset_sao_in + (pic_width)*(j + y_loc) + x_loc + i),0);
				luma_soft[j*OUTPUT_BLOCK_SIZE + i] = $fgetc(correct_sao_file);
			end
		end		
		if({luma_soft[15], luma_soft[14],
			luma_soft[13], luma_soft[12],
			luma_soft[11], luma_soft[10],
			luma_soft[ 9], luma_soft[ 8],
			luma_soft[ 7], luma_soft[ 6],
			luma_soft[ 5], luma_soft[ 4],
			luma_soft[ 3], luma_soft[ 2],
			luma_soft[ 1], luma_soft[ 0]} == sao_luma_4x4_hard) begin
			
			// $display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[15], sao_luma_4x4_hard[16*8-1: 15*8]);
			// $display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[14], sao_luma_4x4_hard[15*8-1: 14*8]);
			// $display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[13], sao_luma_4x4_hard[14*8-1: 13*8]);
			// $display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[12], sao_luma_4x4_hard[13*8-1: 12*8]);
			// $display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[11], sao_luma_4x4_hard[12*8-1: 11*8]);
			// $display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[10], sao_luma_4x4_hard[11*8-1: 10*8]);
			// $display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 9], sao_luma_4x4_hard[10*8-1:  9*8]);
			// $display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 8], sao_luma_4x4_hard[ 9*8-1:  8*8]);
			// $display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 7], sao_luma_4x4_hard[ 8*8-1:  7*8]);
			// $display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 6], sao_luma_4x4_hard[ 7*8-1:  6*8]);
			// $display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 5], sao_luma_4x4_hard[ 6*8-1:  5*8]);
			// $display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 4], sao_luma_4x4_hard[ 5*8-1:  4*8]);
			// $display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 3], sao_luma_4x4_hard[ 4*8-1:  3*8]);
			// $display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 2], sao_luma_4x4_hard[ 3*8-1:  2*8]);
			// $display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 1], sao_luma_4x4_hard[ 2*8-1:  1*8]);
			// $display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 0], sao_luma_4x4_hard[ 1*8-1:  0*8]);
			
		end
		else begin
		   $display("dbf out block x=%d y=%d filter_applied=%d",x_loc,y_loc,main_stage_block.filter_applied_out);
			$display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[15], sao_luma_4x4_hard[16*8-1: 15*8]);
			$display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[14], sao_luma_4x4_hard[15*8-1: 14*8]);
			$display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[13], sao_luma_4x4_hard[14*8-1: 13*8]);
			$display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[12], sao_luma_4x4_hard[13*8-1: 12*8]);
			$display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[11], sao_luma_4x4_hard[12*8-1: 11*8]);
			$display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[10], sao_luma_4x4_hard[11*8-1: 10*8]);
			$display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 9], sao_luma_4x4_hard[10*8-1:  9*8]);
			$display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 8], sao_luma_4x4_hard[ 9*8-1:  8*8]);
			$display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 7], sao_luma_4x4_hard[ 8*8-1:  7*8]);
			$display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 6], sao_luma_4x4_hard[ 7*8-1:  6*8]);
			$display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 5], sao_luma_4x4_hard[ 6*8-1:  5*8]);
			$display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 4], sao_luma_4x4_hard[ 5*8-1:  4*8]);
			$display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 3], sao_luma_4x4_hard[ 4*8-1:  3*8]);
			$display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 2], sao_luma_4x4_hard[ 3*8-1:  2*8]);
			$display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 1], sao_luma_4x4_hard[ 2*8-1:  1*8]);
			$display("Y - correct_sao_file : %x , hard out: %x ",	luma_soft[ 0], sao_luma_4x4_hard[ 1*8-1:  0*8]);
			$stop;			
		end

		
		
		
		
		frame_offset_sao_in = pic_height*pic_width * POC_dbf_to_sao *3 /2 + pic_height*pic_width;
		for (j = 0 ; j < OUTPUT_BLOCK_SIZE/2 ; j = j + 1) begin
			for (i = 0 ; i < OUTPUT_BLOCK_SIZE/2 ; i = i + 1) begin
				temp =$fseek(correct_sao_file,(frame_offset_sao_in + (pic_width/2)*(j + y_loc/2) + x_loc/2 + i),0);
				chma_soft[j*OUTPUT_BLOCK_SIZE/2 + i] = $fgetc(correct_sao_file);
			end
		end
		
		if({chma_soft[ 3], chma_soft[ 2],
			chma_soft[ 1], chma_soft[ 0]} == sao_cb_4x4_hard) begin
			// $display("cb - correct_sao_file : %x , hard out: %x ",	chma_soft[ 3], sao_cb_4x4_hard[ 4*8-1:  3*8]);
			// $display("cb - correct_sao_file : %x , hard out: %x ",	chma_soft[ 2], sao_cb_4x4_hard[ 3*8-1:  2*8]);
			// $display("cb - correct_sao_file : %x , hard out: %x ",	chma_soft[ 1], sao_cb_4x4_hard[ 2*8-1:  1*8]);
			// $display("cb - correct_sao_file : %x , hard out: %x ",	chma_soft[ 0], sao_cb_4x4_hard[ 1*8-1:  0*8]);
			
		end
		
		else begin
			$display("cb - correct_sao_file : %x , hard out: %x ",	chma_soft[ 3], sao_cb_4x4_hard[ 4*8-1:  3*8]);
			$display("cb - correct_sao_file : %x , hard out: %x ",	chma_soft[ 2], sao_cb_4x4_hard[ 3*8-1:  2*8]);
			$display("cb - correct_sao_file : %x , hard out: %x ",	chma_soft[ 1], sao_cb_4x4_hard[ 2*8-1:  1*8]);
			$display("%d cb - correct_sao_file : %x , hard out: %x ",	$time, chma_soft[ 0], sao_cb_4x4_hard[ 1*8-1:  0*8]);
			$stop;
			
		end
		frame_offset_sao_in = pic_height*pic_width * POC_dbf_to_sao *3 /2 + pic_height*pic_width + pic_height*pic_width /4;
		
		
		for (j = 0 ; j < OUTPUT_BLOCK_SIZE/2 ; j = j + 1) begin
			for (i = 0 ; i < OUTPUT_BLOCK_SIZE/2 ; i = i + 1) begin
				temp =$fseek(correct_sao_file,(frame_offset_sao_in + (pic_width/2)*(j + y_loc/2) + x_loc/2 + i),0);
				chma_soft[j*OUTPUT_BLOCK_SIZE/2 + i] = $fgetc(correct_sao_file);
			end
		end
		
		if({chma_soft[ 3], chma_soft[ 2],
			chma_soft[ 1], chma_soft[ 0]} == sao_cr_4x4_hard) begin
			// $display("cr - correct_sao_file : %x , hard out: %x ",	chma_soft[ 3], sao_cr_4x4_hard[ 4*8-1:  3*8]);
			// $display("cr - correct_sao_file : %x , hard out: %x ",	chma_soft[ 2], sao_cr_4x4_hard[ 3*8-1:  2*8]);
			// $display("cr - correct_sao_file : %x , hard out: %x ",	chma_soft[ 1], sao_cr_4x4_hard[ 2*8-1:  1*8]);
			// $display("cr - correct_sao_file : %x , hard out: %x ",	chma_soft[ 0], sao_cr_4x4_hard[ 1*8-1:  0*8]);
			//$display("OK");
			//$stop;
		end
		else begin
				$display("cr - correct_sao_file : %x , hard out: %x ",	chma_soft[ 3], sao_cr_4x4_hard[ 4*8-1:  3*8]);
				$display("cr - correct_sao_file : %x , hard out: %x ",	chma_soft[ 2], sao_cr_4x4_hard[ 3*8-1:  2*8]);
				$display("cr - correct_sao_file : %x , hard out: %x ",	chma_soft[ 1], sao_cr_4x4_hard[ 2*8-1:  1*8]);
				$display("%d cr - correct_sao_file : %x , hard out: %x ",	$time, chma_soft[ 0], sao_cr_4x4_hard[ 1*8-1:  0*8]);
				$stop;
		end
	end
endtask




// synthesis translate_on
	
	
endmodule