`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:37:29 11/12/2013 
// Design Name: 
// Module Name:    luma_edge_filter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module luma_edge_filter(
        clk,
        reset,
        start,
        edge_type,
        done,
		filter_applied,
        q_block_1_in,
        q_block_2_in,
        q_block_3_in,
        q_block_4_in,
        A_block_in,
        B_block_in,
        C_block_in,
        D_block_in,
        E_block_in,
        qp_1,
        qp_A,
        qp_C,
        qp_D,
        BS_v1,
        BS_v2,
        BS_h1,
        BS_h2,
        BS_h0,
        slice_tc_offset,
        slice_beta_offset,
		p_blk_out0 	,
		p_blk_out1  ,
		p_blk_out2  ,
		p_blk_out3  ,
		p_blk_out4  ,
		p_blk_out5  ,
		p_blk_out6  ,
		p_blk_out7  ,
		p_blk_out8  ,
		p_blk_out9  ,
		p_blk_out10 ,
		p_blk_out11 ,
		p_blk_out12 ,
		p_blk_out13 ,
		p_blk_out14 ,
		p_blk_out15 ,
		q_blk_out0  ,
		q_blk_out1  ,
		q_blk_out2  ,
		q_blk_out3  ,
		q_blk_out4  ,
		q_blk_out5  ,
		q_blk_out6  ,
		q_blk_out7  ,
		q_blk_out8  ,
		q_blk_out9  ,
		q_blk_out10 ,
		q_blk_out11 ,
		q_blk_out12 ,
		q_blk_out13 ,
		q_blk_out14 ,
		q_blk_out15
    );


//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    
    localparam                          QP_WIDTH            = 6     ;
    localparam                          BS_WIDTH            = 2     ;
    localparam                          VERT_UP             = 3'b000;
    localparam                          VERT_DOWN           = 3'b001;
    localparam                          HOR_LEFT            = 3'b010;
    localparam                          HOR_RIGHT           = 3'b011;
    localparam                          HOR_FAR_RIGHT       = 3'b100;
    localparam                          CB                  = 0     ;
    localparam                          CR                  = 1     ;
    localparam                          MAX_PIC_WIDTH       = 1920  ;
    localparam                          MAX_PIC_HEIGHT      = 1080  ;
    
    localparam                          STATE_INIT          = 0;    // the state that is entered upon reset
    localparam                          STATE_1             = 1;
    localparam                          STATE_2             = 2;
    localparam                          STATE_3             = 3;
    localparam                          STATE_4             = 4;
    localparam                          STATE_5             = 5;
    localparam                          STATE_END           = 6;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input                               clk;
    input                               reset;
    
    // signals from main stage controller
    input                	    start;				// start signal to start operation
	input			[2:0]		edge_type;			// type of edge to be filtered (v1,v2,h1,h2)
    
    output	reg					done;
	output	reg					filter_applied;
    
    // 4x4 blocks of pixels
    input       	[127:0]         q_block_1_in ;
    input       	[127:0]         q_block_2_in ;
    input       	[127:0]         q_block_3_in ;
    input       	[127:0]         q_block_4_in ;
    input       	[127:0]         A_block_in   ;
    input       	[127:0]         B_block_in   ;
    input       	[127:0]         C_block_in   ;
    input       	[127:0]         D_block_in   ;
    input       	[127:0]         E_block_in   ;

    // Input parameters
    input			[QP_WIDTH-1:0]			qp_1;
	input			[QP_WIDTH-1:0]			qp_A;
	input			[QP_WIDTH-1:0]			qp_C;
	input			[QP_WIDTH-1:0]			qp_D;
    input           [BS_WIDTH-1:0]          BS_v1;
    input           [BS_WIDTH-1:0]          BS_v2;
    input           [BS_WIDTH-1:0]          BS_h1;
    input           [BS_WIDTH-1:0]          BS_h2;
    input           [BS_WIDTH-1:0]          BS_h0;  // BS from previous block
	input			[7:0]				    slice_tc_offset;	// 8 bit signed
    input           [7:0]                   slice_beta_offset;	// 8 bit signed
    
    // Outputs of Filter modules
    output          [7:0]        p_blk_out0 ;
    output          [7:0]        p_blk_out1 ;
    output          [7:0]        p_blk_out2 ;
    output          [7:0]        p_blk_out3 ;
    output          [7:0]        p_blk_out4 ;
    output          [7:0]        p_blk_out5 ;
    output          [7:0]        p_blk_out6 ;
    output          [7:0]        p_blk_out7 ;
    output          [7:0]        p_blk_out8 ;
    output          [7:0]        p_blk_out9 ;
    output          [7:0]        p_blk_out10;
    output          [7:0]        p_blk_out11;
    output          [7:0]        p_blk_out12;
    output          [7:0]        p_blk_out13;
    output          [7:0]        p_blk_out14;
    output          [7:0]        p_blk_out15;
    output          [7:0]        q_blk_out0 ;
    output          [7:0]        q_blk_out1 ;
    output          [7:0]        q_blk_out2 ;
    output          [7:0]        q_blk_out3 ;
    output          [7:0]        q_blk_out4 ;
    output          [7:0]        q_blk_out5 ;
    output          [7:0]        q_blk_out6 ;
    output          [7:0]        q_blk_out7 ;
    output          [7:0]        q_blk_out8 ;
    output          [7:0]        q_blk_out9 ;
    output          [7:0]        q_blk_out10;
    output          [7:0]        q_blk_out11;
    output          [7:0]        q_blk_out12;
    output          [7:0]        q_blk_out13;
    output          [7:0]        q_blk_out14;
    output          [7:0]        q_blk_out15;
	
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/global_para.v"

    
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    
    reg				[QP_WIDTH-1:0]		qp_p;
    reg				[QP_WIDTH-1:0]		qp_q;
    
    reg		        [7:0]		        p_blk [7:0];
    reg		        [7:0]		        q_blk [7:0];
    reg             [7:0]               beta_table;
    reg             [7:0]               tc_table;
    reg             [BS_WIDTH-1:0]      BS;
    
	reg				[7:0]				qp;
	reg				[7:0]				BS_2;
	reg		        [7:0]               beta_sel;
    reg		        [7:0]               tc_sel;
	reg				[7:0]				beta_shift3    ;
	reg				[10:0]				beta_shift2    ; 
	reg				[7:0]				beta_shift_add ;
	reg				[7:0]				tc_shift_add   ;
	
	reg				[8:0]				dp0_add1 ;
	reg				[8:0]				dp3_add1 ;
	reg				[8:0]				dq0_add1 ;
	reg				[8:0]				dq3_add1 ;
	reg				[8:0]				dp0_add2 ;
	reg				[8:0]				dp3_add2 ;
	reg				[8:0]				dq0_add2 ;
	reg				[8:0]				dq3_add2 ;
	
	reg				[8:0]				dp0_sel	 ;
	reg				[8:0]				dp3_sel	 ;
	reg				[8:0]				dq0_sel	 ;
	reg				[8:0]				dq3_sel	 ;
	reg				[7:0]				dpp0_sel ;
	reg				[7:0]				dqq0_sel ;
	reg				[7:0]				dpq0_sel ;
	reg				[7:0]				dpp3_sel ;
	reg				[7:0]				dqq3_sel ;
	reg				[7:0]				dpq3_sel ;
    
    reg 			[9:0]               dp;
    reg 			[9:0]               dq;
    reg 			[9:0]               d0;
    reg 			[9:0]               d3;
    reg 			[10:0]              d03;
    reg                                 dSam0;
    reg                                 dSam3;
    reg		        [8:0]               dpp0qq0;
    reg		        [8:0]               dpp3qq3;
    reg		        [7:0]               dpq0;
    reg		        [7:0]               dpq3;
	
	reg				[7:0]       		beta;
	reg				[7:0]       		tc	;
	reg				[1:0]       		dE	;
	reg				 		       		dEp	;
	reg						       		dEq	;
	wire			[8:0]       		n_tc;
	wire			[8:0]       		n_ten_tc;
	reg				[8:0]       		ten_tc;
	reg				[7:0]       		ten_tc_table;
	
	// Filters
	reg		        [7:0]		        p_blk_filt [15:0];
    reg		        [7:0]		        q_blk_filt [15:0];
	reg				[7:0]				p0_new_f1;
    reg				[7:0]				q0_new_f1;
    reg				[7:0]				p1_new_f1;
    reg				[7:0]				q1_new_f1;
    reg				[7:0]				p2_new_f1;
    reg				[7:0]				q2_new_f1;
    reg				[7:0]				p0_new_f2;
    reg				[7:0]				q0_new_f2;
    reg				[7:0]				p1_new_f2;
    reg				[7:0]				q1_new_f2;
    reg				[7:0]				p2_new_f2;
    reg				[7:0]				q2_new_f2;
    reg				[7:0]				p0_new_f3;
    reg				[7:0]				q0_new_f3;
    reg				[7:0]				p1_new_f3;
    reg				[7:0]				q1_new_f3;
    reg				[7:0]				p2_new_f3;
    reg				[7:0]				q2_new_f3;
    reg				[7:0]				p0_new_f4;
    reg				[7:0]				q0_new_f4;
    reg				[7:0]				p1_new_f4;
	reg				[7:0]				q1_new_f4;
	reg				[7:0]				p2_new_f4;
	reg				[7:0]				q2_new_f4;
	reg				[10:0]				p0_new_f1_add;
    reg				[10:0]				q0_new_f1_add;
    reg				[10:0]				p1_new_f1_add;
    reg				[10:0]				q1_new_f1_add;
    reg				[10:0]				p2_new_f1_add;
    reg				[10:0]				q2_new_f1_add;
    reg				[10:0]				p0_new_f2_add;
    reg				[10:0]				q0_new_f2_add;
    reg				[10:0]				p1_new_f2_add;
    reg				[10:0]				q1_new_f2_add;
    reg				[10:0]				p2_new_f2_add;
    reg				[10:0]				q2_new_f2_add;
    reg				[10:0]				p0_new_f3_add;
    reg				[10:0]				q0_new_f3_add;
    reg				[10:0]				p1_new_f3_add;
    reg				[10:0]				q1_new_f3_add;
    reg				[10:0]				p2_new_f3_add;
    reg				[10:0]				q2_new_f3_add;
    reg				[10:0]				p0_new_f4_add;
    reg				[10:0]				q0_new_f4_add;
    reg				[10:0]				p1_new_f4_add;
	reg				[10:0]				q1_new_f4_add;
	reg				[10:0]				p2_new_f4_add;
	reg				[10:0]				q2_new_f4_add;
	reg				[8:0]				delta_f1 ;
	reg				[8:0]				delta_f2 ;
	reg				[8:0]				delta_f3 ;
	reg				[8:0]				delta_f4 ;
	reg				[8:0]				delta_f1_clip ;
	reg				[8:0]				delta_f2_clip ;
	reg				[8:0]				delta_f3_clip ;
	reg				[8:0]				delta_f4_clip ;
	reg									delta_f1_ten_tc_flag ;
	reg									delta_f2_ten_tc_flag ;
	reg									delta_f3_ten_tc_flag ;
	reg									delta_f4_ten_tc_flag ;
	reg									delta_f1_ten_tc_in	 ;
	reg									delta_f2_ten_tc_in   ;
	reg									delta_f3_ten_tc_in   ;
	reg									delta_f4_ten_tc_in   ;
	
	reg				[8:0]				delta_f1_sub_1 ;
	reg				[8:0]				delta_f2_sub_1 ;
	reg				[8:0]				delta_f3_sub_1 ;
	reg				[8:0]				delta_f4_sub_1 ;
	reg				[10:0]				delta_f1_sub_2 ;
	reg				[10:0]				delta_f2_sub_2 ;
	reg				[10:0]				delta_f3_sub_2 ;
	reg				[10:0]				delta_f4_sub_2 ;
	reg				[12:0]				delta_f1_mul_1 ;
	reg				[12:0]				delta_f2_mul_1 ;
	reg				[12:0]				delta_f3_mul_1 ;
	reg				[12:0]				delta_f4_mul_1 ;
	reg				[12:0]				delta_f1_mul_2 ;
	reg				[12:0]				delta_f2_mul_2 ;
	reg				[12:0]				delta_f3_mul_2 ;
	reg				[12:0]				delta_f4_mul_2 ;
	reg				[12:0]				delta_f1_sum   ;
	reg				[12:0]				delta_f2_sum   ;
	reg				[12:0]				delta_f3_sum   ;
	reg				[12:0]				delta_f4_sum   ;
	
	reg				[8:0]				delta_p_f1 ;
	reg				[8:0]				delta_p_f2 ;
	reg				[8:0]				delta_p_f3 ;
	reg				[8:0]				delta_p_f4 ;
	reg				[8:0]				delta_q_f1 ;
	reg				[8:0]				delta_q_f2 ;
	reg				[8:0]				delta_q_f3 ;
	reg				[8:0]				delta_q_f4 ;
	
	reg				[8:0]				delta_p_f1_add1  ;
	reg				[8:0]				delta_p_f2_add1  ;
	reg				[8:0]				delta_p_f3_add1  ;
	reg				[8:0]				delta_p_f4_add1  ;
	reg				[8:0]				delta_q_f1_add1  ;
	reg				[8:0]				delta_q_f2_add1  ;
	reg				[8:0]				delta_q_f3_add1  ;
	reg				[8:0]				delta_q_f4_add1  ;
	
	reg				[8:0]				delta_p_f1_sub1  ;
	reg				[8:0]				delta_p_f2_sub1  ;
	reg				[8:0]				delta_p_f3_sub1  ;
	reg				[8:0]				delta_p_f4_sub1  ;
	reg				[8:0]				delta_q_f1_sub1  ;
	reg				[8:0]				delta_q_f2_sub1  ;
	reg				[8:0]				delta_q_f3_sub1  ;
	reg				[8:0]				delta_q_f4_sub1  ;
	
	reg				[8:0]				delta_p_f1_add2  ;
	reg				[8:0]				delta_p_f2_add2  ;
	reg				[8:0]				delta_p_f3_add2  ;
	reg				[8:0]				delta_p_f4_add2  ;
	reg				[8:0]				delta_q_f1_add2  ;
	reg				[8:0]				delta_q_f2_add2  ;
	reg				[8:0]				delta_q_f3_add2  ;
	reg				[8:0]				delta_q_f4_add2  ;
	
	reg				[8:0]				delta_p_f1_shift ;
	reg				[8:0]				delta_p_f2_shift ;
	reg				[8:0]				delta_p_f3_shift ;
	reg				[8:0]				delta_p_f4_shift ;
	reg				[8:0]				delta_q_f1_shift ;
	reg				[8:0]				delta_q_f2_shift ;
	reg				[8:0]				delta_q_f3_shift ;
	reg				[8:0]				delta_q_f4_shift ;
	
	reg                                 start_filters;
	wire                                done_e1;
	wire                                done_e2;
	wire                                done_e3;
	wire                                done_e4;
    
    // Wires for luma samples
    wire        	[7:0]       q_block_1 [15:0];
    wire        	[7:0]       q_block_2 [15:0];
    wire        	[7:0]       q_block_3 [15:0];
    wire        	[7:0]       q_block_4 [15:0];
    wire        	[7:0]       A_block [15:0];
    wire        	[7:0]       B_block [15:0];
    wire        	[7:0]       C_block [15:0];
    wire        	[7:0]       D_block [15:0];
    wire        	[7:0]       E_block [15:0];
    
	integer							state;
	genvar							i;

//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
    
    // Assigning input samples
    generate
        for(i=0;i<16;i=i+1) begin : loop_i
            assign    q_block_1[i] =  q_block_1_in[(8*i+7):(8*i)];
            assign    q_block_2[i] =  q_block_2_in[(8*i+7):(8*i)];
            assign    q_block_3[i] =  q_block_3_in[(8*i+7):(8*i)];
            assign    q_block_4[i] =  q_block_4_in[(8*i+7):(8*i)];
            assign    A_block  [i] =  A_block_in  [(8*i+7):(8*i)];
            assign    B_block  [i] =  B_block_in  [(8*i+7):(8*i)];
            assign    C_block  [i] =  C_block_in  [(8*i+7):(8*i)];
            assign    D_block  [i] =  D_block_in  [(8*i+7):(8*i)];
            assign    E_block  [i] =  E_block_in  [(8*i+7):(8*i)];
        end
    endgenerate
	
	
	//------------- Filtering modules --------------------------
	
	luma_sample_filter filter_1
    (
        .clk(clk),
        .reset(reset),
        .start(start_filters),
        .done(done_e1),
        .p0(p_blk_filt[0]),
        .p1(p_blk_filt[1]),
        .p2(p_blk_filt[2]),
        .p3(p_blk_filt[3]),
        .q0(q_blk_filt[0]),
        .q1(q_blk_filt[1]),
        .q2(q_blk_filt[2]),
        .q3(q_blk_filt[3]),
        .beta(beta),
        .tc(tc),
        .dE(dE),
        .dEp(dEp),
        .dEq(dEq),
		.p0_new_filt_in(p0_new_f1),
		.q0_new_filt_in(q0_new_f1),
		.p1_new_filt_in(p1_new_f1),
		.q1_new_filt_in(q1_new_f1),
		.p2_new_filt_in(p2_new_f1),
		.q2_new_filt_in(q2_new_f1),
		.delta_filt_in ( delta_f1),
		.delta_ten_tc_flag_in(delta_f1_ten_tc_in),
		.delta_p_in(delta_p_f1),
		.delta_q_in(delta_q_f1),
        .p0_new(p_blk_out0),
        .p1_new(p_blk_out1),
        .p2_new(p_blk_out2),
        .p3_new(p_blk_out3),
        .q0_new(q_blk_out0),
        .q1_new(q_blk_out1),
        .q2_new(q_blk_out2),
        .q3_new(q_blk_out3)
    );
    
    luma_sample_filter filter_2
    (
        .clk(clk),
        .reset(reset),
        .start(start_filters),
        .done(done_e2),
        .p0(p_blk_filt[4]),
        .p1(p_blk_filt[5]),
        .p2(p_blk_filt[6]),
        .p3(p_blk_filt[7]),
        .q0(q_blk_filt[4]),
        .q1(q_blk_filt[5]),
        .q2(q_blk_filt[6]),
        .q3(q_blk_filt[7]),
        .beta(beta),
        .tc(tc),
        .dE(dE),
        .dEp(dEp),
        .dEq(dEq),
		.p0_new_filt_in(p0_new_f2),
		.q0_new_filt_in(q0_new_f2),
		.p1_new_filt_in(p1_new_f2),
		.q1_new_filt_in(q1_new_f2),
		.p2_new_filt_in(p2_new_f2),
		.q2_new_filt_in(q2_new_f2),
		.delta_filt_in ( delta_f2),
		.delta_ten_tc_flag_in(delta_f2_ten_tc_in),
		.delta_p_in(delta_p_f2),
		.delta_q_in(delta_q_f2),
        .p0_new(p_blk_out4),
        .p1_new(p_blk_out5),
        .p2_new(p_blk_out6),
        .p3_new(p_blk_out7),
        .q0_new(q_blk_out4),
        .q1_new(q_blk_out5),
        .q2_new(q_blk_out6),
        .q3_new(q_blk_out7)
    );
    
    luma_sample_filter filter_3
    (
        .clk(clk),
        .reset(reset),
        .start(start_filters),
        .done(done_e3),
        .p0(p_blk_filt[8] ),
        .p1(p_blk_filt[9] ),
        .p2(p_blk_filt[10]),
        .p3(p_blk_filt[11]),
        .q0(q_blk_filt[8] ),
        .q1(q_blk_filt[9] ),
        .q2(q_blk_filt[10]),
        .q3(q_blk_filt[11]),
        .beta(beta),
        .tc(tc),
        .dE(dE),
        .dEp(dEp),
        .dEq(dEq),
		.p0_new_filt_in(p0_new_f3),
		.q0_new_filt_in(q0_new_f3),
		.p1_new_filt_in(p1_new_f3),
		.q1_new_filt_in(q1_new_f3),
		.p2_new_filt_in(p2_new_f3),
		.q2_new_filt_in(q2_new_f3),
		.delta_filt_in ( delta_f3),
		.delta_ten_tc_flag_in(delta_f3_ten_tc_in),
		.delta_p_in(delta_p_f3),
		.delta_q_in(delta_q_f3),
        .p0_new(p_blk_out8),
        .p1_new(p_blk_out9),
        .p2_new(p_blk_out10),
        .p3_new(p_blk_out11),
        .q0_new(q_blk_out8),
        .q1_new(q_blk_out9),
        .q2_new(q_blk_out10),
        .q3_new(q_blk_out11)
    );
    
    luma_sample_filter filter_4
    (
        .clk(clk),
        .reset(reset),
        .start(start_filters),
        .done(done_e4),
        .p0(p_blk_filt[12]),
        .p1(p_blk_filt[13]),
        .p2(p_blk_filt[14]),
        .p3(p_blk_filt[15]),
        .q0(q_blk_filt[12]),
        .q1(q_blk_filt[13]),
        .q2(q_blk_filt[14]),
        .q3(q_blk_filt[15]),
        .beta(beta),
        .tc(tc),
        .dE(dE),
        .dEp(dEp),
        .dEq(dEq),
		.p0_new_filt_in(p0_new_f4),
		.q0_new_filt_in(q0_new_f4),
		.p1_new_filt_in(p1_new_f4),
		.q1_new_filt_in(q1_new_f4),
		.p2_new_filt_in(p2_new_f4),
		.q2_new_filt_in(q2_new_f4),
		.delta_filt_in ( delta_f4),
		.delta_ten_tc_flag_in(delta_f4_ten_tc_in),
		.delta_p_in(delta_p_f4),
		.delta_q_in(delta_q_f4),
        .p0_new(p_blk_out12),
        .p1_new(p_blk_out13),
        .p2_new(p_blk_out14),
        .p3_new(p_blk_out15),
        .q0_new(q_blk_out12),
        .q1_new(q_blk_out13),
        .q2_new(q_blk_out14),
        .q3_new(q_blk_out15)
    );
    
    
    
    always @(posedge clk or posedge reset) begin
        if(reset) begin
            state <= STATE_INIT;
			done  <= 1'b0;
			filter_applied <= 1'b0;
			start_filters <= 1'b0;	
        end
        else begin
            case (state)
                STATE_INIT : begin
                    if(start) begin
                        state <= STATE_1;
						filter_applied <= 1'b0;
                        done  <= 1'b0;
                        case(edge_type)
                            VERT_UP : begin
                                qp_p <= qp_A;
                                qp_q <= qp_1;
                                BS <= BS_v1;
                                p_blk[0] <= A_block[3];
                                p_blk[1] <= A_block[2];
                                p_blk[2] <= A_block[1];
                                p_blk[3] <= A_block[0];
                                p_blk[4] <= A_block[15];
                                p_blk[5] <= A_block[14];
                                p_blk[6] <= A_block[13];
                                p_blk[7] <= A_block[12];
                                q_blk[0] <= q_block_1[0];
                                q_blk[1] <= q_block_1[1];
                                q_blk[2] <= q_block_1[2];
                                q_blk[3] <= q_block_1[3];
                                q_blk[4] <= q_block_1[12];
                                q_blk[5] <= q_block_1[13];
                                q_blk[6] <= q_block_1[14];
                                q_blk[7] <= q_block_1[15];
                            end
                            VERT_DOWN : begin
                                qp_p <= qp_A;       // qp is same for A and B blocks
                                qp_q <= qp_1;
                                BS <= BS_v2;
                                p_blk[0] <= B_block[3];
                                p_blk[1] <= B_block[2];
                                p_blk[2] <= B_block[1];
                                p_blk[3] <= B_block[0];
                                p_blk[4] <= B_block[15];
                                p_blk[5] <= B_block[14];
                                p_blk[6] <= B_block[13];
                                p_blk[7] <= B_block[12];
                                q_blk[0] <= q_block_3[0];
                                q_blk[1] <= q_block_3[1];
                                q_blk[2] <= q_block_3[2];
                                q_blk[3] <= q_block_3[3];
                                q_blk[4] <= q_block_3[12];
                                q_blk[5] <= q_block_3[13];
                                q_blk[6] <= q_block_3[14];
                                q_blk[7] <= q_block_3[15];
                            end
                            HOR_LEFT : begin
                                qp_p <= qp_C;
                                qp_q <= qp_A;
                                BS <= BS_h0;
                                p_blk[0] <= C_block[12];
                                p_blk[1] <= C_block[8];
                                p_blk[2] <= C_block[4];
                                p_blk[3] <= C_block[0];
                                p_blk[4] <= C_block[15];
                                p_blk[5] <= C_block[11];
                                p_blk[6] <= C_block[7];
                                p_blk[7] <= C_block[3];
                                q_blk[0] <= A_block[0];
                                q_blk[1] <= A_block[4];
                                q_blk[2] <= A_block[8];
                                q_blk[3] <= A_block[12];
                                q_blk[4] <= A_block[3];
                                q_blk[5] <= A_block[7];
                                q_blk[6] <= A_block[11];
                                q_blk[7] <= A_block[15];
                            end
                            HOR_RIGHT : begin
                                qp_p <= qp_D;
                                qp_q <= qp_1;
                                BS <= BS_h1;
                                p_blk[0] <= D_block[12];
                                p_blk[1] <= D_block[8];
                                p_blk[2] <= D_block[4];
                                p_blk[3] <= D_block[0];
                                p_blk[4] <= D_block[15];
                                p_blk[5] <= D_block[11];
                                p_blk[6] <= D_block[7];
                                p_blk[7] <= D_block[3];
                                q_blk[0] <= q_block_1[0];
                                q_blk[1] <= q_block_1[4];
                                q_blk[2] <= q_block_1[8];
                                q_blk[3] <= q_block_1[12];
                                q_blk[4] <= q_block_1[3];
                                q_blk[5] <= q_block_1[7];
                                q_blk[6] <= q_block_1[11];
                                q_blk[7] <= q_block_1[15];
                            end
                            HOR_FAR_RIGHT : begin
                                qp_p <= qp_D;
                                qp_q <= qp_1;
                                BS <= BS_h2;
                                p_blk[0] <= E_block[12];
                                p_blk[1] <= E_block[8];
                                p_blk[2] <= E_block[4];
                                p_blk[3] <= E_block[0];
                                p_blk[4] <= E_block[15];
                                p_blk[5] <= E_block[11];
                                p_blk[6] <= E_block[7];
                                p_blk[7] <= E_block[3];
                                q_blk[0] <= q_block_2[0];
                                q_blk[1] <= q_block_2[4];
                                q_blk[2] <= q_block_2[8];
                                q_blk[3] <= q_block_2[12];
                                q_blk[4] <= q_block_2[3];
                                q_blk[5] <= q_block_2[7];
                                q_blk[6] <= q_block_2[11];
                                q_blk[7] <= q_block_2[15];
                            end
                        endcase
						
						// Inputs for Filters
						case(edge_type)
							VERT_UP : begin
								p_blk_filt[0]  	<=	 A_block[3];
								p_blk_filt[1]  	<=	 A_block[2];
								p_blk_filt[2]  	<=	 A_block[1];
								p_blk_filt[3]  	<=	 A_block[0];
								p_blk_filt[4]  	<=	 A_block[7];
								p_blk_filt[5]  	<=	 A_block[6];
								p_blk_filt[6]  	<=	 A_block[5];
								p_blk_filt[7]  	<=	 A_block[4];
								p_blk_filt[8]  	<=	 A_block[11];
								p_blk_filt[9]  	<=	 A_block[10];
								p_blk_filt[10] 	<=	 A_block[9];
								p_blk_filt[11] 	<=	 A_block[8];
								p_blk_filt[12] 	<=	 A_block[15];
								p_blk_filt[13] 	<=	 A_block[14];
								p_blk_filt[14] 	<=	 A_block[13];
								p_blk_filt[15] 	<=	 A_block[12];
								q_blk_filt[0]  	<=	 q_block_1[0];
								q_blk_filt[1]  	<=	 q_block_1[1];
								q_blk_filt[2]  	<=	 q_block_1[2];
								q_blk_filt[3]  	<=	 q_block_1[3];
								q_blk_filt[4]  	<=	 q_block_1[4] ;
								q_blk_filt[5]  	<=	 q_block_1[5] ;
								q_blk_filt[6]  	<=	 q_block_1[6] ;
								q_blk_filt[7]  	<=	 q_block_1[7] ;
								q_blk_filt[8]  	<=	 q_block_1[8] ; 
								q_blk_filt[9]  	<=	 q_block_1[9] ;
								q_blk_filt[10] 	<=	 q_block_1[10];
								q_blk_filt[11] 	<=	 q_block_1[11];
								q_blk_filt[12] 	<=	 q_block_1[12];
								q_blk_filt[13] 	<=	 q_block_1[13];
								q_blk_filt[14] 	<=	 q_block_1[14];
								q_blk_filt[15] 	<=	 q_block_1[15];
							end
							VERT_DOWN : begin
								p_blk_filt[0]  	<=	 B_block[3];
								p_blk_filt[1]  	<=	 B_block[2];
								p_blk_filt[2]  	<=	 B_block[1];
								p_blk_filt[3]  	<=	 B_block[0];
								p_blk_filt[4]  	<=	 B_block[7];
								p_blk_filt[5]  	<=	 B_block[6];
								p_blk_filt[6]  	<=	 B_block[5];
								p_blk_filt[7]  	<=	 B_block[4];
								p_blk_filt[8]  	<=	 B_block[11];
								p_blk_filt[9]  	<=	 B_block[10];
								p_blk_filt[10] 	<=	 B_block[9];
								p_blk_filt[11] 	<=	 B_block[8];
								p_blk_filt[12] 	<=	 B_block[15];
								p_blk_filt[13] 	<=	 B_block[14];
								p_blk_filt[14] 	<=	 B_block[13];
								p_blk_filt[15] 	<=	 B_block[12];
								q_blk_filt[0]  	<=	 q_block_3[0];
								q_blk_filt[1]  	<=	 q_block_3[1];
								q_blk_filt[2]  	<=	 q_block_3[2];
								q_blk_filt[3]  	<=	 q_block_3[3];
								q_blk_filt[4]  	<=	 q_block_3[4] ;
								q_blk_filt[5]  	<=	 q_block_3[5] ;
								q_blk_filt[6]  	<=	 q_block_3[6] ;
								q_blk_filt[7]  	<=	 q_block_3[7] ;
								q_blk_filt[8]  	<=	 q_block_3[8] ; 
								q_blk_filt[9]  	<=	 q_block_3[9] ;
								q_blk_filt[10] 	<=	 q_block_3[10];
								q_blk_filt[11] 	<=	 q_block_3[11];
								q_blk_filt[12] 	<=	 q_block_3[12];
								q_blk_filt[13] 	<=	 q_block_3[13];
								q_blk_filt[14] 	<=	 q_block_3[14];
								q_blk_filt[15] 	<=	 q_block_3[15];
							end
							HOR_LEFT : begin
								p_blk_filt[0]  	<=	 C_block[12];
								p_blk_filt[1]  	<=	 C_block[8];
								p_blk_filt[2]  	<=	 C_block[4];
								p_blk_filt[3]  	<=	 C_block[0];
								p_blk_filt[4]  	<=	 C_block[13];
								p_blk_filt[5]  	<=	 C_block[9];
								p_blk_filt[6]  	<=	 C_block[5];
								p_blk_filt[7]  	<=	 C_block[1];
								p_blk_filt[8]  	<=	 C_block[14];
								p_blk_filt[9]  	<=	 C_block[10];
								p_blk_filt[10] 	<=	 C_block[6];
								p_blk_filt[11] 	<=	 C_block[2];
								p_blk_filt[12] 	<=	 C_block[15];
								p_blk_filt[13] 	<=	 C_block[11];
								p_blk_filt[14] 	<=	 C_block[7];
								p_blk_filt[15] 	<=	 C_block[3];
								q_blk_filt[0]  	<=	 A_block[0];
								q_blk_filt[1]  	<=	 A_block[4];
								q_blk_filt[2]  	<=	 A_block[8];
								q_blk_filt[3]  	<=	 A_block[12];
								q_blk_filt[4]  	<=	 A_block[1];
								q_blk_filt[5]  	<=	 A_block[5];
								q_blk_filt[6]  	<=	 A_block[9];
								q_blk_filt[7]  	<=	 A_block[13];
								q_blk_filt[8]  	<=	 A_block[2];
								q_blk_filt[9]  	<=	 A_block[6];
								q_blk_filt[10] 	<=	 A_block[10];
								q_blk_filt[11] 	<=	 A_block[14];
								q_blk_filt[12] 	<=	 A_block[3];
								q_blk_filt[13] 	<=	 A_block[7];
								q_blk_filt[14] 	<=	 A_block[11];
								q_blk_filt[15] 	<=	 A_block[15];
							end
							HOR_RIGHT : begin
								p_blk_filt[0]  	<=	 D_block[12];
								p_blk_filt[1]  	<=	 D_block[8];
								p_blk_filt[2]  	<=	 D_block[4];
								p_blk_filt[3]  	<=	 D_block[0];
								p_blk_filt[4]  	<=	 D_block[13];
								p_blk_filt[5]  	<=	 D_block[9];
								p_blk_filt[6]  	<=	 D_block[5];
								p_blk_filt[7]  	<=	 D_block[1];
								p_blk_filt[8]  	<=	 D_block[14];
								p_blk_filt[9]  	<=	 D_block[10];
								p_blk_filt[10] 	<=	 D_block[6];
								p_blk_filt[11] 	<=	 D_block[2];
								p_blk_filt[12] 	<=	 D_block[15];
								p_blk_filt[13] 	<=	 D_block[11];
								p_blk_filt[14] 	<=	 D_block[7];
								p_blk_filt[15] 	<=	 D_block[3];
								q_blk_filt[0]  	<=	 q_block_1[0];
								q_blk_filt[1]  	<=	 q_block_1[4];
								q_blk_filt[2]  	<=	 q_block_1[8];
								q_blk_filt[3]  	<=	 q_block_1[12];
								q_blk_filt[4]  	<=	 q_block_1[1];
								q_blk_filt[5]  	<=	 q_block_1[5];
								q_blk_filt[6]  	<=	 q_block_1[9];
								q_blk_filt[7]  	<=	 q_block_1[13];
								q_blk_filt[8]  	<=	 q_block_1[2];
								q_blk_filt[9]  	<=	 q_block_1[6];
								q_blk_filt[10] 	<=	 q_block_1[10];
								q_blk_filt[11] 	<=	 q_block_1[14];
								q_blk_filt[12] 	<=	 q_block_1[3];
								q_blk_filt[13] 	<=	 q_block_1[7];
								q_blk_filt[14] 	<=	 q_block_1[11];
								q_blk_filt[15] 	<=	 q_block_1[15];
							end
							default : begin							// Hor far right, h2
								p_blk_filt[0]  	<=	 E_block[12];
								p_blk_filt[1]  	<=	 E_block[8];
								p_blk_filt[2]  	<=	 E_block[4];
								p_blk_filt[3]  	<=	 E_block[0];
								p_blk_filt[4]  	<=	 E_block[13];
								p_blk_filt[5]  	<=	 E_block[9];
								p_blk_filt[6]  	<=	 E_block[5];
								p_blk_filt[7]  	<=	 E_block[1];
								p_blk_filt[8]  	<=	 E_block[14];
								p_blk_filt[9]  	<=	 E_block[10];
								p_blk_filt[10] 	<=	 E_block[6];
								p_blk_filt[11] 	<=	 E_block[2];
								p_blk_filt[12] 	<=	 E_block[15];
								p_blk_filt[13] 	<=	 E_block[11];
								p_blk_filt[14] 	<=	 E_block[7];
								p_blk_filt[15] 	<=	 E_block[3];
								q_blk_filt[0]  	<=	 q_block_2[0];
								q_blk_filt[1]  	<=	 q_block_2[4];
								q_blk_filt[2]  	<=	 q_block_2[8];
								q_blk_filt[3]  	<=	 q_block_2[12];
								q_blk_filt[4]  	<=	 q_block_2[1];
								q_blk_filt[5]  	<=	 q_block_2[5];
								q_blk_filt[6]  	<=	 q_block_2[9];
								q_blk_filt[7]  	<=	 q_block_2[13];
								q_blk_filt[8]  	<=	 q_block_2[2];
								q_blk_filt[9]  	<=	 q_block_2[6];
								q_blk_filt[10] 	<=	 q_block_2[10];
								q_blk_filt[11] 	<=	 q_block_2[14];
								q_blk_filt[12] 	<=	 q_block_2[3];
								q_blk_filt[13] 	<=	 q_block_2[7];
								q_blk_filt[14] 	<=	 q_block_2[11];
								q_blk_filt[15] 	<=	 q_block_2[15];
							end
						endcase
                    end
                end
                STATE_1 : begin
                    beta <= beta_table ; 
                    tc   <= tc_table   ;
					ten_tc <= {1'b0,ten_tc_table} ;
					// Calculated from combinational blocks
					dp <= dp0_sel + dp3_sel;
                    dq <= dq0_sel + dq3_sel;
                    d0 <= dp0_sel + dq0_sel;
                    d3 <= dp3_sel + dq3_sel;
                    d03 <= dp0_sel + dq0_sel + dp3_sel + dq3_sel;
					dE  <= 2'b0;
                    dEp <= 1'b0;
                    dEq <= 1'b0;
					dpp0qq0 <= dpp0_sel + dqq0_sel ;
					dpp3qq3 <= dpp3_sel + dqq3_sel ;
                    dpq0 <= dpq0_sel ;
                    dpq3 <= dpq3_sel ;
					// Inputs for filters
					//--------Filter 1 --------------------------
					p0_new_f1 	<= 	p0_new_f1_add[10:3] ;
					q0_new_f1 	<= 	q0_new_f1_add[10:3] ;
					p1_new_f1 	<= 	p1_new_f1_add[9:2] ;
					q1_new_f1 	<= 	q1_new_f1_add[9:2] ;
					p2_new_f1  	<= 	p2_new_f1_add[10:3] ;
					q2_new_f1  	<= 	q2_new_f1_add[10:3] ;
					delta_f1 	<= 	delta_f1_sum[12:4];
					//--------Filter 2 --------------------------
					p0_new_f2 	<= 	p0_new_f2_add[10:3] ;
					q0_new_f2 	<= 	q0_new_f2_add[10:3] ;
					p1_new_f2 	<= 	p1_new_f2_add[9:2] ;
					q1_new_f2 	<= 	q1_new_f2_add[9:2] ;
					p2_new_f2  	<= 	p2_new_f2_add[10:3] ;
					q2_new_f2  	<= 	q2_new_f2_add[10:3] ;
					delta_f2 	<= 	delta_f2_sum[12:4];
					//--------Filter 3 --------------------------
					p0_new_f3 	<= 	p0_new_f3_add[10:3] ;
					q0_new_f3 	<= 	q0_new_f3_add[10:3] ;
					p1_new_f3 	<= 	p1_new_f3_add[9:2] ;
					q1_new_f3 	<= 	q1_new_f3_add[9:2] ;
					p2_new_f3  	<= 	p2_new_f3_add[10:3] ;
					q2_new_f3  	<= 	q2_new_f3_add[10:3] ;
					delta_f3 	<= 	delta_f3_sum[12:4];
					//--------Filter 4 --------------------------
					p0_new_f4 	<= 	p0_new_f4_add[10:3] ;
					q0_new_f4 	<= 	q0_new_f4_add[10:3] ;
					p1_new_f4 	<= 	p1_new_f4_add[9:2] ;
					q1_new_f4 	<= 	q1_new_f4_add[9:2] ;
					p2_new_f4  	<= 	p2_new_f4_add[10:3] ;
					q2_new_f4  	<= 	q2_new_f4_add[10:3] ;
					delta_f4 	<= 	delta_f4_sum[12:4];		// 9 bit signed
                    state <= STATE_2;
                end
                STATE_2 : begin
                    if(d03 < beta) begin
                        dSam0 <= (({d0,1'b0} < beta_shift2) && (dpp0qq0 < beta_shift3) && (dpq0 < tc_shift_add)) ? 1'b1 : 1'b0 ;
                        dSam3 <= (({d3,1'b0} < beta_shift2) && (dpp3qq3 < beta_shift3) && (dpq3 < tc_shift_add)) ? 1'b1 : 1'b0 ;
                        dE <= 2'b1;
                        state <= STATE_3;
                    end
                    else begin
                        done  <= 1'b1;
						filter_applied <= 1'b0;
                        state <= STATE_INIT;
                    end
					// clip delta, final delta calculation
					delta_f1 <= delta_f1_clip;
					delta_f2 <= delta_f2_clip;
					delta_f3 <= delta_f3_clip;
					delta_f4 <= delta_f4_clip;
					delta_f1_ten_tc_in <= delta_f1_ten_tc_flag;
					delta_f2_ten_tc_in <= delta_f2_ten_tc_flag;
					delta_f3_ten_tc_in <= delta_f3_ten_tc_flag;
					delta_f4_ten_tc_in <= delta_f4_ten_tc_flag;
                end
                STATE_3 : begin
                    if(dSam0==1 && dSam3==1) begin
                        dE <= 2'b10;
                    end
                    else begin
                        if (dp < beta_shift_add) begin
                            dEp <= 1'b1;
                        end
                        else begin
                            dEp <= 1'b0;
                        end
                        if (dq < beta_shift_add) begin
                            dEq <= 1'b1;
                        end
                        else begin
                            dEq <= 1'b0;
                        end
                    end
					// delta_p, delta_q calculation
					delta_p_f1	<=  delta_p_f1_shift ;
					delta_p_f2  <=  delta_p_f2_shift ;
					delta_p_f3  <=  delta_p_f3_shift ;
					delta_p_f4  <=  delta_p_f4_shift ;
					delta_q_f1  <=  delta_q_f1_shift ;
					delta_q_f2  <=  delta_q_f2_shift ;
					delta_q_f3  <=  delta_q_f3_shift ;
					delta_q_f4  <=  delta_q_f4_shift ;
					start_filters <= 1'b1;		// Filtering start here
                    state <= STATE_4;			// Go to filtering state, only case to go for filter
                end
                STATE_4 : begin
                    done  <= 1'b1;				// Filtering modules complete in exactly one cycle
					filter_applied <= 1'b1;
					start_filters <= 1'b0;	
					state <= STATE_INIT;
                end
            endcase
        end
    end
    
    
    always @(*) begin
        casex(beta_sel)
            8'd16 :  beta_table = 8'd6 ;
            8'd17 :  beta_table = 8'd7 ;
            8'd18 :  beta_table = 8'd8 ;
            8'd19 :  beta_table = 8'd9 ;
            8'd20 :  beta_table = 8'd10 ;
            8'd21 :  beta_table = 8'd11 ;
            8'd22 :  beta_table = 8'd12 ;
            8'd23 :  beta_table = 8'd13 ;
            8'd24 :  beta_table = 8'd14 ;
            8'd25 :  beta_table = 8'd15 ;
            8'd26 :  beta_table = 8'd16 ;
            8'd27 :  beta_table = 8'd17 ;
            8'd28 :  beta_table = 8'd18 ;
            8'd29 :  beta_table = 8'd20 ;
            8'd30 :  beta_table = 8'd22 ;
            8'd31 :  beta_table = 8'd24 ;
            8'd32 :  beta_table = 8'd26 ;
            8'd33 :  beta_table = 8'd28 ;
            8'd34 :  beta_table = 8'd30 ;
            8'd35 :  beta_table = 8'd32 ;
            8'd36 :  beta_table = 8'd34 ;
            8'd37 :  beta_table = 8'd36 ;
            8'd38 :  beta_table = 8'd38 ;
            8'd39 :  beta_table = 8'd40 ;
            8'd40 :  beta_table = 8'd42 ;
            8'd41 :  beta_table = 8'd44 ;
            8'd42 :  beta_table = 8'd46 ;
            8'd43 :  beta_table = 8'd48 ;
            8'd44 :  beta_table = 8'd50 ;
            8'd45 :  beta_table = 8'd52 ;
            8'd46 :  beta_table = 8'd54 ;
            8'd47 :  beta_table = 8'd56 ;
            8'd48 :  beta_table = 8'd58 ;
            8'd49 :  beta_table = 8'd60 ;
            8'd50 :  beta_table = 8'd62 ;
            8'd51 :  beta_table = 8'd64 ;
			8'd52 :  beta_table = 8'd64 ;
			8'd53 :  beta_table = 8'd64 ;
			8'd54 :  beta_table = 8'd64 ;
			8'd55 :  beta_table = 8'd64 ;
			8'd56 :  beta_table = 8'd64 ;
			8'd57 :  beta_table = 8'd64 ;
			8'd58 :  beta_table = 8'd64 ;
			8'd59 :  beta_table = 8'd64 ;
			8'd60 :  beta_table = 8'd62 ;
            8'd61 :  beta_table = 8'd64 ;
			8'd62 :  beta_table = 8'd64 ;
			8'd63 :  beta_table = 8'd64 ;
			8'b01xxxxxx :  beta_table = 8'd64 ;
            default :  beta_table = 8'd0 ;
        endcase
    end
    
    always @(*) begin
        casex(tc_sel)
            8'd18 :  tc_table = 8'd1 ;
            8'd19 :  tc_table = 8'd1 ;
            8'd20 :  tc_table = 8'd1 ;
            8'd21 :  tc_table = 8'd1 ;
            8'd22 :  tc_table = 8'd1 ;
            8'd23 :  tc_table = 8'd1 ;
            8'd24 :  tc_table = 8'd1 ;
            8'd25 :  tc_table = 8'd1 ;
            8'd26 :  tc_table = 8'd1 ;
            8'd27 :  tc_table = 8'd2 ;
            8'd28 :  tc_table = 8'd2 ;
            8'd29 :  tc_table = 8'd2 ;
            8'd30 :  tc_table = 8'd2 ;
            8'd31 :  tc_table = 8'd3 ;
            8'd32 :  tc_table = 8'd3 ;
            8'd33 :  tc_table = 8'd3 ;
            8'd34 :  tc_table = 8'd3 ;
            8'd35 :  tc_table = 8'd4 ;
            8'd36 :  tc_table = 8'd4 ;
            8'd37 :  tc_table = 8'd4 ;
            8'd38 :  tc_table = 8'd5 ;
            8'd39 :  tc_table = 8'd5 ;
            8'd40 :  tc_table = 8'd6 ;
            8'd41 :  tc_table = 8'd6 ;
            8'd42 :  tc_table = 8'd7 ;
            8'd43 :  tc_table = 8'd8 ;
            8'd44 :  tc_table = 8'd9 ;
            8'd45 :  tc_table = 8'd10 ;
            8'd46 :  tc_table = 8'd11 ;
            8'd47 :  tc_table = 8'd13 ;
            8'd48 :  tc_table = 8'd14 ;
            8'd49 :  tc_table = 8'd16 ;
            8'd50 :  tc_table = 8'd18 ;
            8'd51 :  tc_table = 8'd20 ;
            8'd52 :  tc_table = 8'd22 ;
            8'd53 :  tc_table = 8'd24 ;
			8'd54 :  tc_table = 8'd24 ;
			8'd55 :  tc_table = 8'd24 ;
			8'd56 :  tc_table = 8'd24 ;
			8'd57 :  tc_table = 8'd24 ;
			8'd58 :  tc_table = 8'd24 ;
			8'd59 :  tc_table = 8'd24 ;
			8'd60 :  tc_table = 8'd24 ;
            8'd61 :  tc_table = 8'd24 ;
			8'd62 :  tc_table = 8'd24 ;
			8'd63 :  tc_table = 8'd24 ;
			8'b01xxxxxx : tc_table = 8'd24 ;
            default :  tc_table = 8'd0 ;
        endcase
    end
	
	//------------ Combinational blocks to add and clip beta and tc ---------------- 
	always @(*) begin
		qp 	= ((qp_p + qp_q + 1)>>1);
		case(BS)
			2'b10 : begin
				BS_2 = 8'd2;
			end
			default : begin
				BS_2 = 8'd0;
			end
		endcase
	end
	
	always @(*) begin
		beta_sel = qp + slice_beta_offset ;
        tc_sel   = qp + BS_2 + slice_tc_offset ;
	end
	
	always @(*) begin
        casex(tc_sel)
            8'd18 :  ten_tc_table = 8'd10 ;
            8'd19 :  ten_tc_table = 8'd10 ;
            8'd20 :  ten_tc_table = 8'd10 ;
            8'd21 :  ten_tc_table = 8'd10 ;
            8'd22 :  ten_tc_table = 8'd10 ;
            8'd23 :  ten_tc_table = 8'd10 ;
            8'd24 :  ten_tc_table = 8'd10 ;
            8'd25 :  ten_tc_table = 8'd10 ;
            8'd26 :  ten_tc_table = 8'd10 ;
            8'd27 :  ten_tc_table = 8'd20 ;
            8'd28 :  ten_tc_table = 8'd20 ;
            8'd29 :  ten_tc_table = 8'd20 ;
            8'd30 :  ten_tc_table = 8'd20 ;
            8'd31 :  ten_tc_table = 8'd30 ;
            8'd32 :  ten_tc_table = 8'd30 ;
            8'd33 :  ten_tc_table = 8'd30 ;
            8'd34 :  ten_tc_table = 8'd30 ;
            8'd35 :  ten_tc_table = 8'd40 ;
            8'd36 :  ten_tc_table = 8'd40 ;
            8'd37 :  ten_tc_table = 8'd40 ;
            8'd38 :  ten_tc_table = 8'd50 ;
            8'd39 :  ten_tc_table = 8'd50 ;
            8'd40 :  ten_tc_table = 8'd60 ;
            8'd41 :  ten_tc_table = 8'd60 ;
            8'd42 :  ten_tc_table = 8'd70 ;
            8'd43 :  ten_tc_table = 8'd80 ;
            8'd44 :  ten_tc_table = 8'd90 ;
            8'd45 :  ten_tc_table = 8'd100 ;
            8'd46 :  ten_tc_table = 8'd110 ;
            8'd47 :  ten_tc_table = 8'd130 ;
            8'd48 :  ten_tc_table = 8'd140 ;
            8'd49 :  ten_tc_table = 8'd160 ;
            8'd50 :  ten_tc_table = 8'd180 ;
            8'd51 :  ten_tc_table = 8'd200 ;
            8'd52 :  ten_tc_table = 8'd220 ;
            8'd53 :  ten_tc_table = 8'd240 ;
			8'd54 :  ten_tc_table = 8'd240 ;
			8'd55 :  ten_tc_table = 8'd240 ;
			8'd56 :  ten_tc_table = 8'd240 ;
			8'd57 :  ten_tc_table = 8'd240 ;
			8'd58 :  ten_tc_table = 8'd240 ;
			8'd59 :  ten_tc_table = 8'd240 ;
			8'd60 :  ten_tc_table = 8'd240 ;
            8'd61 :  ten_tc_table = 8'd240 ;
			8'd62 :  ten_tc_table = 8'd240 ;
			8'd63 :  ten_tc_table = 8'd240 ;
			8'b01xxxxxx : ten_tc_table = 8'd240 ;
            default :  ten_tc_table = 8'd0 ;
        endcase
    end
	
	//------------ Combinational blocks to calculate differences dxx ---------------
	always @(*) begin
		dp0_add1  =  p_blk[0] + p_blk[2] ;
		dp3_add1  =  p_blk[4] + p_blk[6] ;
		dq0_add1  =  q_blk[0] + q_blk[2] ;
		dq3_add1  =  q_blk[4] + q_blk[6] ;
		dp0_add2  =  p_blk[1]<<1 ;
		dp3_add2  =  p_blk[5]<<1 ;
		dq0_add2  =  q_blk[1]<<1 ;
		dq3_add2  =  q_blk[5]<<1 ;
	end
	
	always @(*) begin
		dp0_sel	 =	(dp0_add1 > dp0_add2) ? (dp0_add1 - dp0_add2) : (dp0_add2 - dp0_add1) ;
		dp3_sel	 =	(dp3_add1 > dp3_add2) ? (dp3_add1 - dp3_add2) : (dp3_add2 - dp3_add1) ;
		dq0_sel	 =	(dq0_add1 > dq0_add2) ? (dq0_add1 - dq0_add2) : (dq0_add2 - dq0_add1) ;
		dq3_sel	 =	(dq3_add1 > dq3_add2) ? (dq3_add1 - dq3_add2) : (dq3_add2 - dq3_add1) ;
	end
	
	always @(*) begin
		dpp0_sel  =  (p_blk[3] > p_blk[0]) ? (p_blk[3] - p_blk[0]) : (p_blk[0] - p_blk[3]) ;
        dqq0_sel  =  (q_blk[0] > q_blk[3]) ? (q_blk[0] - q_blk[3]) : (q_blk[3] - q_blk[0]) ;
        dpq0_sel  =  (p_blk[0] > q_blk[0]) ? (p_blk[0] - q_blk[0]) : (q_blk[0] - p_blk[0]) ;
        dpp3_sel  =  (p_blk[7] > p_blk[4]) ? (p_blk[7] - p_blk[4]) : (p_blk[4] - p_blk[7]) ;
        dqq3_sel  =  (q_blk[4] > q_blk[7]) ? (q_blk[4] - q_blk[7]) : (q_blk[7] - q_blk[4]) ;
        dpq3_sel  =  (p_blk[4] > q_blk[4]) ? (p_blk[4] - q_blk[4]) : (q_blk[4] - p_blk[4]) ;
	end
	
	//------------- Combinational blocks to calculate beta shifts --------------------
	always @(*) begin
		beta_shift3     =  (beta>>3);
		beta_shift2     =  {5'b0,(beta>>2)};
		beta_shift_add  =  ((beta +(beta>>1))>>3);
		tc_shift_add	=  ((5*tc+1)>>1);
	end
	
	//------------- Combinational blocks to calculate delta inputs -----------------------
	always @(*) begin
		delta_f1_sub_1	=  {1'b0,q_blk_filt[0]}  - {1'b0,p_blk_filt[0]} ;			// 9 bit signed
		delta_f2_sub_1	=  {1'b0,q_blk_filt[4]}  - {1'b0,p_blk_filt[4]} ;
		delta_f3_sub_1	=  {1'b0,q_blk_filt[8]}  - {1'b0,p_blk_filt[8]} ;
		delta_f4_sub_1	=  {1'b0,q_blk_filt[12]} - {1'b0,p_blk_filt[12]} ;
		delta_f1_sub_2	=  {3'b000,p_blk_filt[1]}  - {3'b000,q_blk_filt[1]}  ;		// 11 bit signed
		delta_f2_sub_2	=  {3'b000,p_blk_filt[5]}  - {3'b000,q_blk_filt[5]}  ;
		delta_f3_sub_2	=  {3'b000,p_blk_filt[9]}  - {3'b000,q_blk_filt[9]}  ;
		delta_f4_sub_2	=  {3'b000,p_blk_filt[13]} - {3'b000,q_blk_filt[13]} ;
	end
	
	always @(*) begin
		delta_f1_mul_1	=	{delta_f1_sub_1[8],delta_f1_sub_1,3'b000}  +  {{4{delta_f1_sub_1[8]}},delta_f1_sub_1} ;		// 13 bit signed
		delta_f2_mul_1	=   {delta_f2_sub_1[8],delta_f2_sub_1,3'b000}  +  {{4{delta_f2_sub_1[8]}},delta_f2_sub_1} ;
		delta_f3_mul_1	=   {delta_f3_sub_1[8],delta_f3_sub_1,3'b000}  +  {{4{delta_f3_sub_1[8]}},delta_f3_sub_1} ;
		delta_f4_mul_1	=   {delta_f4_sub_1[8],delta_f4_sub_1,3'b000}  +  {{4{delta_f4_sub_1[8]}},delta_f4_sub_1} ;
		delta_f1_mul_2	=	{delta_f1_sub_2[10],delta_f1_sub_2,1'b0}  +  {{2{delta_f1_sub_2[10]}},delta_f1_sub_2} ;
		delta_f2_mul_2	=   {delta_f2_sub_2[10],delta_f2_sub_2,1'b0}  +  {{2{delta_f2_sub_2[10]}},delta_f2_sub_2} ;
		delta_f3_mul_2	=   {delta_f3_sub_2[10],delta_f3_sub_2,1'b0}  +  {{2{delta_f3_sub_2[10]}},delta_f3_sub_2} ;
		delta_f4_mul_2	=   {delta_f4_sub_2[10],delta_f4_sub_2,1'b0}  +  {{2{delta_f4_sub_2[10]}},delta_f4_sub_2} ;
	end
	
	always @(*) begin
		delta_f1_sum  =  delta_f1_mul_1 + delta_f1_mul_2 + 8 ;				// 13 bit signed
		delta_f2_sum  =  delta_f2_mul_1 + delta_f2_mul_2 + 8 ;
		delta_f3_sum  =  delta_f3_mul_1 + delta_f3_mul_2 + 8 ;
		delta_f4_sum  =  delta_f4_mul_1 + delta_f4_mul_2 + 8 ;
	end
	
	// Clip delta values between -tc and tc
	assign	n_tc = {1'b1,~tc} + 9'b1 ;
	assign  n_ten_tc = (~ten_tc) + 9'b1 ;
	
	always @(*) begin
		if(tc == 9'd0) begin
			delta_f1_clip  =  9'd0 ;
			delta_f1_ten_tc_flag  =  1'b0 ;
			delta_f2_clip  =  9'd0 ;
			delta_f2_ten_tc_flag  =  1'b0 ;
			delta_f3_clip  =  9'd0 ;
			delta_f3_ten_tc_flag  =  1'b0 ;
			delta_f4_clip  =  9'd0 ;
			delta_f4_ten_tc_flag  =  1'b0 ;
		end
		else begin
			if(delta_f1[8] == 0) begin
				delta_f1_clip = (delta_f1 > tc) ? tc : delta_f1 ;
				delta_f1_ten_tc_flag  =  (delta_f1 < ten_tc) ?  1'b1  :  1'b0 ;
			end
			else begin
				delta_f1_clip = (delta_f1 < n_tc) ? n_tc : delta_f1 ;
				delta_f1_ten_tc_flag  =  (delta_f1 > n_ten_tc) ?  1'b1  :  1'b0 ;
			end
			if(delta_f2[8] == 0) begin
				delta_f2_clip = (delta_f2 > tc) ? tc : delta_f2 ;
				delta_f2_ten_tc_flag  =  (delta_f2 < ten_tc) ?  1'b1  :  1'b0 ;
			end
			else begin
				delta_f2_clip = (delta_f2 < n_tc) ? n_tc : delta_f2 ;
				delta_f2_ten_tc_flag  =  (delta_f2 > n_ten_tc) ?  1'b1  :  1'b0 ;
			end
			if(delta_f3[8] == 0) begin
				delta_f3_clip = (delta_f3 > tc) ? tc : delta_f3 ;
				delta_f3_ten_tc_flag  =  (delta_f3 < ten_tc) ?  1'b1  :  1'b0 ;
			end
			else begin
				delta_f3_clip = (delta_f3 < n_tc) ? n_tc : delta_f3 ;
				delta_f3_ten_tc_flag  =  (delta_f3 > n_ten_tc) ?  1'b1  :  1'b0 ;
			end
			if(delta_f4[8] == 0) begin
				delta_f4_clip = (delta_f4 > tc) ? tc : delta_f4 ;
				delta_f4_ten_tc_flag  =  (delta_f4 < ten_tc) ?  1'b1  :  1'b0 ;
			end
			else begin
				delta_f4_clip = (delta_f4 < n_tc) ? n_tc : delta_f4 ;
				delta_f4_ten_tc_flag  =  (delta_f4 > n_ten_tc) ?  1'b1  :  1'b0 ;
			end
		end
	end
	
	//------------- Combinational block to calculate new sample values ------------------
	always @(*) begin
		p0_new_f1_add 	= 	( (p_blk_filt[1]<<1) + (p_blk_filt[0]<<1) + (q_blk_filt[0]<<1) + (p_blk_filt[2] + q_blk_filt[1] + 4)) ;
		q0_new_f1_add 	= 	( (q_blk_filt[1]<<1) + (q_blk_filt[0]<<1) + (p_blk_filt[0]<<1) + (q_blk_filt[2] + p_blk_filt[1] + 4)) ;
		p1_new_f1_add 	= 	(  p_blk_filt[2] + p_blk_filt[1] + p_blk_filt[0] + q_blk_filt[0] + 2) ;                          
		q1_new_f1_add 	= 	(  q_blk_filt[2] + q_blk_filt[1] + q_blk_filt[0] + p_blk_filt[0] + 2) ;                          
		p2_new_f1_add  	= 	( (p_blk_filt[3]<<1) + (3*p_blk_filt[2]) + p_blk_filt[1] + p_blk_filt[0] + q_blk_filt[0] + 4) ;  
		q2_new_f1_add  	= 	( (q_blk_filt[3]<<1) + (3*q_blk_filt[2]) + q_blk_filt[1] + q_blk_filt[0] + p_blk_filt[0] + 4) ;  
		
		p0_new_f2_add 	= 	( (p_blk_filt[5]<<1) + (p_blk_filt[4]<<1) + (q_blk_filt[4]<<1) + (p_blk_filt[6] + q_blk_filt[5] + 4)) ;
		q0_new_f2_add 	= 	( (q_blk_filt[5]<<1) + (q_blk_filt[4]<<1) + (p_blk_filt[4]<<1) + (q_blk_filt[6] + p_blk_filt[5] + 4)) ;
		p1_new_f2_add 	= 	(  p_blk_filt[6] + p_blk_filt[5] + p_blk_filt[4] + q_blk_filt[4] + 2) ;
		q1_new_f2_add 	= 	(  q_blk_filt[6] + q_blk_filt[5] + q_blk_filt[4] + p_blk_filt[4] + 2) ;
		p2_new_f2_add  	= 	( (p_blk_filt[7]<<1) + (3*p_blk_filt[6]) + p_blk_filt[5] + p_blk_filt[4] + q_blk_filt[4] + 4) ;
		q2_new_f2_add  	= 	( (q_blk_filt[7]<<1) + (3*q_blk_filt[6]) + q_blk_filt[5] + q_blk_filt[4] + p_blk_filt[4] + 4) ;
		
		p0_new_f3_add 	= 	( (p_blk_filt[9]<<1) + (p_blk_filt[8]<<1) + (q_blk_filt[8]<<1) + (p_blk_filt[10] + q_blk_filt[9] + 4)) ;
		q0_new_f3_add 	= 	( (q_blk_filt[9]<<1) + (q_blk_filt[8]<<1) + (p_blk_filt[8]<<1) + (q_blk_filt[10] + p_blk_filt[9] + 4)) ;
		p1_new_f3_add 	= 	(  p_blk_filt[10] + p_blk_filt[9] + p_blk_filt[8] + q_blk_filt[8] + 2) ;
		q1_new_f3_add 	= 	(  q_blk_filt[10] + q_blk_filt[9] + q_blk_filt[8] + p_blk_filt[8] + 2) ;
		p2_new_f3_add  	= 	( (p_blk_filt[11]<<1) + (3*p_blk_filt[10]) + p_blk_filt[9] + p_blk_filt[8] + q_blk_filt[8] + 4) ;
		q2_new_f3_add  	= 	( (q_blk_filt[11]<<1) + (3*q_blk_filt[10]) + q_blk_filt[9] + q_blk_filt[8] + p_blk_filt[8] + 4) ;
		
		p0_new_f4_add 	= 	( (p_blk_filt[13]<<1) + (p_blk_filt[12]<<1) + (q_blk_filt[12]<<1) + (p_blk_filt[14] + q_blk_filt[13] + 4)) ;
		q0_new_f4_add 	= 	( (q_blk_filt[13]<<1) + (q_blk_filt[12]<<1) + (p_blk_filt[12]<<1) + (q_blk_filt[14] + p_blk_filt[13] + 4)) ;
		p1_new_f4_add 	= 	(  p_blk_filt[14] + p_blk_filt[13] + p_blk_filt[12] + q_blk_filt[12] + 2) ;
		q1_new_f4_add 	= 	(  q_blk_filt[14] + q_blk_filt[13] + q_blk_filt[12] + p_blk_filt[12] + 2) ;
		p2_new_f4_add  	= 	( (p_blk_filt[15]<<1) + (3*p_blk_filt[14]) + p_blk_filt[13] + p_blk_filt[12] + q_blk_filt[12] + 4) ;
		q2_new_f4_add  	= 	( (q_blk_filt[15]<<1) + (3*q_blk_filt[14]) + q_blk_filt[13] + q_blk_filt[12] + p_blk_filt[12] + 4) ;
	end
	
	//------------ Combinational blocks to calculate delta p,q ---------------------
	always @(*) begin
		delta_p_f1_add1  =  ( p_blk_filt[0]  +  p_blk_filt[2]  + 1 ) ;
		delta_p_f2_add1  =  ( p_blk_filt[4]  +  p_blk_filt[6]  + 1 ) ;
		delta_p_f3_add1  =  ( p_blk_filt[8]  +  p_blk_filt[10] + 1 ) ;
		delta_p_f4_add1  =  ( p_blk_filt[12] +  p_blk_filt[14] + 1 ) ;
		delta_q_f1_add1  =  ( q_blk_filt[0]  +  q_blk_filt[2]  + 1 ) ;
		delta_q_f2_add1  =  ( q_blk_filt[4]  +  q_blk_filt[6]  + 1 ) ;
		delta_q_f3_add1  =  ( q_blk_filt[8]  +  q_blk_filt[10] + 1 ) ;
		delta_q_f4_add1  =  ( q_blk_filt[12] +  q_blk_filt[14] + 1 ) ;
	end
	
	always @(*) begin
		delta_p_f1_sub1  =  delta_f1 - {1'b0,p_blk_filt[1]}  ;					// 9 bit sign extended
		delta_p_f2_sub1  =  delta_f2 - {1'b0,p_blk_filt[5]}  ;
		delta_p_f3_sub1  =  delta_f3 - {1'b0,p_blk_filt[9]}  ;
		delta_p_f4_sub1  =  delta_f4 - {1'b0,p_blk_filt[13]} ;
		delta_q_f1_sub1  =  delta_f1 + {1'b0,q_blk_filt[1]}  ;
		delta_q_f2_sub1  =  delta_f2 + {1'b0,q_blk_filt[5]}  ;
		delta_q_f3_sub1  =  delta_f3 + {1'b0,q_blk_filt[9]}  ;
		delta_q_f4_sub1  =  delta_f4 + {1'b0,q_blk_filt[13]} ;
	end
	
	always @(*) begin
		delta_p_f1_add2  =  {1'b0,delta_p_f1_add1[8:1]} + delta_p_f1_sub1 ;			// 9 bit signed
		delta_p_f2_add2  =  {1'b0,delta_p_f2_add1[8:1]} + delta_p_f2_sub1 ;
		delta_p_f3_add2  =  {1'b0,delta_p_f3_add1[8:1]} + delta_p_f3_sub1 ;
		delta_p_f4_add2  =  {1'b0,delta_p_f4_add1[8:1]} + delta_p_f4_sub1 ;
		delta_q_f1_add2  =  {1'b0,delta_q_f1_add1[8:1]} - delta_q_f1_sub1 ;
		delta_q_f2_add2  =  {1'b0,delta_q_f2_add1[8:1]} - delta_q_f2_sub1 ;
		delta_q_f3_add2  =  {1'b0,delta_q_f3_add1[8:1]} - delta_q_f3_sub1 ;
		delta_q_f4_add2  =  {1'b0,delta_q_f4_add1[8:1]} - delta_q_f4_sub1 ;
	end
	
	always @(*) begin
		delta_p_f1_shift  =  {delta_p_f1_add2[8],delta_p_f1_add2[8:1]} ;		// 9 bit signed
		delta_p_f2_shift  =  {delta_p_f2_add2[8],delta_p_f2_add2[8:1]} ;
		delta_p_f3_shift  =  {delta_p_f3_add2[8],delta_p_f3_add2[8:1]} ;
		delta_p_f4_shift  =  {delta_p_f4_add2[8],delta_p_f4_add2[8:1]} ;
		delta_q_f1_shift  =  {delta_q_f1_add2[8],delta_q_f1_add2[8:1]} ;
		delta_q_f2_shift  =  {delta_q_f2_add2[8],delta_q_f2_add2[8:1]} ;
		delta_q_f3_shift  =  {delta_q_f3_add2[8],delta_q_f3_add2[8:1]} ;
		delta_q_f4_shift  =  {delta_q_f4_add2[8],delta_q_f4_add2[8:1]} ;
	end
    

endmodule
