`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:09:34 11/15/2013 
// Design Name: 
// Module Name:    luma_sample_filter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module luma_sample_filter(
        clk,
        reset,
        start,
        done,
        p0,
        p1,
        p2,
        p3,
        q0,
        q1,
        q2,
        q3,
        beta,
        tc,
        dE,
        dEp,
        dEq,
		p0_new_filt_in,
		q0_new_filt_in, 
		p1_new_filt_in, 
		q1_new_filt_in, 
		p2_new_filt_in, 
		q2_new_filt_in, 
		delta_filt_in, 
		delta_ten_tc_flag_in,
		delta_p_in,
		delta_q_in,
        p0_new,
        p1_new,
        p2_new,
        p3_new,
        q0_new,
        q1_new,
        q2_new,
        q3_new
    );

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input                               clk;
    input                               reset;
    
    // signals from main stage controller
    input                	    start;				// start signal to start operation   
    output	reg					done;
    
    // 4x4 blocks of pixels
    input       	[7:0]       p0;
    input       	[7:0]       p1;
    input       	[7:0]       p2;
    input       	[7:0]       p3;
    input       	[7:0]       q0;
    input       	[7:0]       q1;
    input       	[7:0]       q2;
    input       	[7:0]       q3;
    
    input           [7:0]       beta;
    input           [7:0]       tc;
    input           [1:0]       dE;
    input                 		dEp;
    input           	       	dEq;
	
	input			[7:0]		p0_new_filt_in ;
	input			[7:0]		q0_new_filt_in ;
	input			[7:0]		p1_new_filt_in ;
	input			[7:0]		q1_new_filt_in ;
	input			[7:0]		p2_new_filt_in ;
	input			[7:0]		q2_new_filt_in ;
	input			[8:0]		delta_filt_in  ;
	input						delta_ten_tc_flag_in ;
	input			[8:0]		delta_p_in ;
	input			[8:0]		delta_q_in ;
    
    // Output modified samples
    output  reg         [7:0]       p0_new;
    output  reg         [7:0]       p1_new;
    output  reg         [7:0]       p2_new;
    output  reg         [7:0]       p3_new;
    output  reg         [7:0]       q0_new;
    output  reg         [7:0]       q1_new;
    output  reg         [7:0]       q2_new;
    output  reg         [7:0]       q3_new;
    
    
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/global_para.v"

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
       
    localparam                          STATE_INIT          = 0;    // the state that is entered upon reset
    localparam                          STATE_1             = 1;
    localparam                          STATE_2             = 2;
    localparam                          STATE_3             = 3;
    localparam                          STATE_4             = 4;
    localparam                          STATE_5             = 5;
    localparam                          STATE_6             = 6;
    localparam                          STATE_END           = 10;
    
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    
	wire			[8:0]					p0_delta;
	wire			[8:0]					q0_delta;
	wire			[8:0]					p1_delta;
	wire			[8:0]					q1_delta;
	wire			[8:0]					n_tc_by2;
	wire			[8:0]					tc_by2;
	
	reg				[7:0]					p0_new_weak ;
	reg				[7:0]					q0_new_weak ;
	reg				[7:0]					p1_new_weak ;
	reg				[7:0]					q1_new_weak ;
	reg				[8:0]					delta_p ;
	reg				[8:0]					delta_q ;
	
	reg				[7:0]					p0_new_low  ;
	reg				[7:0]					p0_new_high ;
	reg				[7:0]					q0_new_low  ;
	reg				[7:0]					q0_new_high ;
	reg				[7:0]					p1_new_low  ;
	reg				[7:0]					p1_new_high ;
	reg				[7:0]					q1_new_low  ;
	reg				[7:0]					q1_new_high ;
	reg				[7:0]					p2_new_low  ;
	reg				[7:0]					p2_new_high ;
	reg				[7:0]					q2_new_low  ;
	reg				[7:0]					q2_new_high ;
	
	integer 							state;
    
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
    
    always @(posedge clk or posedge reset) begin
        if(reset) begin
            state <= STATE_INIT;
			done  <= 1'b0;
        end
        else begin
            case (state)
                STATE_INIT : begin
                    if(start) begin 
                        if(dE==2'b10) begin
                            p0_new <= (p0_new_filt_in < p0_new_low) ? p0_new_low : ((p0_new_filt_in > p0_new_high) ? p0_new_high : p0_new_filt_in);
							q0_new <= (q0_new_filt_in < q0_new_low) ? q0_new_low : ((q0_new_filt_in > q0_new_high) ? q0_new_high : q0_new_filt_in);
							p1_new <= (p1_new_filt_in < p1_new_low) ? p1_new_low : ((p1_new_filt_in > p1_new_high) ? p1_new_high : p1_new_filt_in);
							q1_new <= (q1_new_filt_in < q1_new_low) ? q1_new_low : ((q1_new_filt_in > q1_new_high) ? q1_new_high : q1_new_filt_in);
							p2_new <= (p2_new_filt_in < p2_new_low) ? p2_new_low : ((p2_new_filt_in > p2_new_high) ? p2_new_high : p2_new_filt_in);
							q2_new <= (q2_new_filt_in < q2_new_low) ? q2_new_low : ((q2_new_filt_in > q2_new_high) ? q2_new_high : q2_new_filt_in);
                            p3_new <=  p3;
                            q3_new <=  q3;
                            done  <= 1'b1;
							state <= STATE_INIT;
                        end
						else if(delta_ten_tc_flag_in) begin
							p0_new <= p0_new_weak;
							q0_new <= q0_new_weak;
							if(dEp==1) begin
								p1_new <= p1_new_weak ;
							end
							else begin
								p1_new <= p1;
							end
							if(dEq==1) begin
								q1_new <= q1_new_weak ;
							end
							else begin
								q1_new <= q1;
							end
							p2_new <= p2;
							q2_new <= q2;
							p3_new <= p3;
							q3_new <= q3;
							done  <= 1'b1;
							state <= STATE_INIT;
						end
                        else begin
							p0_new <= p0;
							q0_new <= q0;
							p1_new <= p1;
							q1_new <= q1;
							p2_new <= p2;
							q2_new <= q2;
							p3_new <= p3;
							q3_new <= q3;
                            done  <= 1'b1;
							state <= STATE_INIT;
                        end
                    end
                end
            endcase
        end
    end
	
	
	//------------ Combinational block to calculate high and low bounds ----------------
	always @(*) begin
		p0_new_low 	 =  p0 - {tc[6:0],1'b0} ;
		p0_new_high	 =  p0 + {tc[6:0],1'b0} ;
		q0_new_low 	 =  q0 - {tc[6:0],1'b0} ;
		q0_new_high	 =  q0 + {tc[6:0],1'b0} ;
		p1_new_low 	 =  p1 - {tc[6:0],1'b0} ;
		p1_new_high	 =  p1 + {tc[6:0],1'b0} ;
		q1_new_low 	 =  q1 - {tc[6:0],1'b0} ;
		q1_new_high	 =  q1 + {tc[6:0],1'b0} ;
		p2_new_low 	 =  p2 - {tc[6:0],1'b0} ;
		p2_new_high	 =  p2 + {tc[6:0],1'b0} ;
		q2_new_low 	 =  q2 - {tc[6:0],1'b0} ;
		q2_new_high	 =  q2 + {tc[6:0],1'b0} ;
	end
	
	
	//------------ Combinational blocks to add and clip p0,q0 -----------------------
	assign	p0_delta  =  {1'b0,p0} + delta_filt_in ;
	assign	q0_delta  =  {1'b0,q0} - delta_filt_in ;
	
	always @(*) begin
		if(delta_filt_in[8]==0) begin
			p0_new_weak  =  (p0_delta[8]==1) ? 8'd255 : p0_delta[7:0] ;
			q0_new_weak  =  (q0_delta[8]==1) ? 8'd0 : q0_delta[7:0] ;
		end
		else begin
			p0_new_weak  =  (p0_delta[8]==1) ? 8'd0 : p0_delta[7:0] ;
			q0_new_weak  =  (q0_delta[8]==1) ? 8'd255 : q0_delta[7:0] ;
		end
	end
	
	//------------ Combinational block to clip delta p and delta q --------------------
	assign	tc_by2    =  {2'b00,tc[7:1]} ;
	assign	n_tc_by2  =  ~{2'b00,tc[7:1]} + 9'b1 ;
	
	always @(*) begin
		if(tc_by2 == 9'd0) begin
			delta_p  =  9'd0 ;
			delta_q  =  9'd0 ;
		end
		else begin
			if(delta_p_in[8]==0) begin
				delta_p  =  (delta_p_in > tc_by2) ? tc_by2 : delta_p_in ;
			end
			else begin
				delta_p  =  (delta_p_in < n_tc_by2) ? n_tc_by2 : delta_p_in ;
			end
			if(delta_q_in[8]==0) begin
				delta_q  =  (delta_q_in > tc_by2) ? tc_by2 : delta_q_in ;
			end
			else begin
				delta_q  =  (delta_q_in < n_tc_by2) ? n_tc_by2 : delta_q_in ;
			end
		end
	end
	
	//------------ Combinational block to calculate p1,q1 weak -----------------------
	assign  p1_delta  =  {1'b0,p1} + delta_p ;
	assign  q1_delta  =  {1'b0,q1} + delta_q ;
	
	always @(*) begin
		if(delta_p[8]==0) begin
			p1_new_weak  =  (p1_delta[8]==1) ? 8'd255 : p1_delta[7:0] ;
		end
		else begin
			p1_new_weak  =  (p1_delta[8]==1) ? 8'd0 : p1_delta[7:0] ;
		end
		if(delta_q[8]==0) begin
			q1_new_weak  =  (q1_delta[8]==1) ? 8'd255 : q1_delta[7:0] ;
		end
		else begin
			q1_new_weak  =  (q1_delta[8]==1) ? 8'd0 : q1_delta[7:0] ;
		end
	end
	

endmodule
