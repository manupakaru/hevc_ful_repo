`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:40:38 10/20/2013 
// Design Name: 
// Module Name:    chroma_edge_filter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module chroma_edge_filter(
        clk,
        reset,
        start,
        edge_type,
        done,
        q_block_1_in,
        q_block_2_in,
        q_block_3_in,
        q_block_4_in,
        A_block_in,
        B_block_in,
        C_block_in,
        D_block_in,
        E_block_in,
        qp_1,
        qp_A,
        qp_C,
        qp_D,
        pps_cb_qp_offset,
        pps_cr_qp_offset,
        slice_tc_offset,
        p_blk_0,
        p_blk_1,
        p_blk_2,
        p_blk_3,
        q_blk_0,
        q_blk_1,
        q_blk_2,
        q_blk_3
    );

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    
    localparam                          QP_WIDTH            = 	6     ;
    localparam                          BS_WIDTH            = 	2     ;
    localparam                          VERT_UP             = 	3'b000;
    localparam                          VERT_DOWN           = 	3'b001;
    localparam                          HOR_LEFT            = 	3'b010;
    localparam                          HOR_RIGHT           = 	3'b011;
    localparam                          HOR_FAR_RIGHT       = 	3'b100;
    localparam                          CB                  = 	0     ;
    localparam                          CR                  = 	1     ;
    localparam                          MAX_PIC_WIDTH       = 	1920  ;
    localparam                          MAX_PIC_HEIGHT      = 	1080  ;
		
    localparam                          STATE_INIT          = 	0;
    localparam                          STATE_1             = 	1;
    localparam                          STATE_2             = 	2;

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input                               clk;
    input                               reset;
	 
	 // signals from main stage controller
    input                	    start;				// start signal to start operation
	input			[2:0]		edge_type;			// type of edge to be filtered (v1,v2,h1,h2)
	output	reg					done;					// indicate end of operation cycle to stage controller

    // 4x4 pixel register array interface
    input       	[31:0]      q_block_1_in ;
    input       	[31:0]      q_block_2_in ;
    input       	[31:0]      q_block_3_in ;
    input       	[31:0]      q_block_4_in ;
    input       	[31:0]      A_block_in   ;
    input       	[31:0]      B_block_in   ;
    input       	[31:0]      C_block_in   ;
    input       	[31:0]      D_block_in   ;
    input       	[31:0]      E_block_in   ;
	 
	// input paramters
	input			[QP_WIDTH-1:0]			qp_1;
	input			[QP_WIDTH-1:0]			qp_A;
	input			[QP_WIDTH-1:0]			qp_C;
	input			[QP_WIDTH-1:0]			qp_D;
	 
	input			[4:0]				    pps_cb_qp_offset;
    input			[4:0]				    pps_cr_qp_offset;
	input			[7:0]				    slice_tc_offset ;		// 8 bit signed
    
    // output 2x2 block registers
	output  reg		[7:0]		p_blk_0  ;
    output  reg		[7:0]		p_blk_1  ;
    output  reg		[7:0]		p_blk_2  ;
    output  reg		[7:0]		p_blk_3  ;
    output  reg		[7:0]		q_blk_0  ;
    output  reg		[7:0]		q_blk_1  ;
    output  reg		[7:0]		q_blk_2  ;
    output  reg		[7:0]		q_blk_3  ;
    
    
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/global_para.v"
	parameter							CHR_ID		=		0 		;



//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    
	reg				[5:0]					qp_p;			
	reg				[5:0]					qp_q;		
	wire			[5:0]					qp_avg;
	reg				[7:0]					qp_avg_7;
	reg				[5:0]					qp_sel;
	reg				[7:0]					tc_sel_reg;
	reg             [8:0]                   tc;
	wire            [8:0]                   n_tc;
    
    reg				[8:0]					delta1_sub1 ;
	reg				[8:0]					delta1_sub2 ;
	reg				[8:0]					delta2_sub1 ;
	reg				[8:0]					delta2_sub2 ;
	reg				[11:0]					delta1_add ;
	reg				[11:0]					delta2_add ;
	reg				[11:0]					delta1_add_reg ;
	reg				[11:0]					delta2_add_reg ;
	reg				[8:0]					delta1_clip ;
	reg				[8:0]					delta2_clip ;
	reg				[8:0]					delta1 ;
	reg				[8:0]					delta2 ;
    
    reg             [5:0]                   qpc_table;
    reg             [5:0]                   tc_table;
	
	reg				[8:0]					sample_0 ;
	reg				[8:0]					sample_1 ;
	reg				[8:0]					sample_2 ;
	reg				[8:0]					sample_3 ;
	reg				[7:0]					sample_0_clip ;
	reg				[7:0]					sample_1_clip ;
	reg				[7:0]					sample_2_clip ;
	reg				[7:0]					sample_3_clip ;
    
    // Wires to assign input samples
    wire            [7:0]       q_block_1 [3:0];
    wire            [7:0]       q_block_2 [3:0];
    wire            [7:0]       q_block_3 [3:0];
    wire            [7:0]       q_block_4 [3:0];
    wire            [7:0]       A_block [3:0];
    wire            [7:0]       B_block [3:0];
    wire            [7:0]       C_block [3:0];
    wire            [7:0]       D_block [3:0];
    wire            [7:0]       E_block [3:0];
    
    integer                                     state;                      // the internal state of the module
    genvar													i;
	 
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
    
    // Assigning input samples
	 generate
			for(i=0;i<4;i=i+1) begin : loop_i
				assign    q_block_1[i] =  q_block_1_in[(8*i+7):(8*i)];
				assign    q_block_2[i] =  q_block_2_in[(8*i+7):(8*i)];
				assign    q_block_3[i] =  q_block_3_in[(8*i+7):(8*i)];
				assign    q_block_4[i] =  q_block_4_in[(8*i+7):(8*i)];
				assign    A_block  [i] =  A_block_in  [(8*i+7):(8*i)];
				assign    B_block  [i] =  B_block_in  [(8*i+7):(8*i)];
				assign    C_block  [i] =  C_block_in  [(8*i+7):(8*i)];
				assign    D_block  [i] =  D_block_in  [(8*i+7):(8*i)];
                assign    E_block  [i] =  E_block_in  [(8*i+7):(8*i)];
			end
	 endgenerate	
    
	
    
    always @(posedge clk or posedge reset) begin : filtering_fsm
        if(reset) begin
            state <= STATE_INIT;
			done  <= 1'b0;
        end
        else  begin
            case (state)
					STATE_INIT : begin
						if(start) begin
							state <= STATE_1;
                            done  <= 1'b0;
                            case(edge_type)
                                VERT_UP : begin
                                    p_blk_0 <= A_block[0];
                                    p_blk_1 <= A_block[1];
                                    p_blk_2 <= A_block[2];
                                    p_blk_3 <= A_block[3];
                                    q_blk_0 <= q_block_1[0];
                                    q_blk_1 <= q_block_1[1];
                                    q_blk_2 <= q_block_1[2];
                                    q_blk_3 <= q_block_1[3];
                                end
                                VERT_DOWN : begin
                                    p_blk_0 <= B_block[0];
                                    p_blk_1 <= B_block[1];
                                    p_blk_2 <= B_block[2];
                                    p_blk_3 <= B_block[3];
                                    q_blk_0 <= q_block_3[0];
                                    q_blk_1 <= q_block_3[1];
                                    q_blk_2 <= q_block_3[2];
                                    q_blk_3 <= q_block_3[3];
                                end
                                HOR_LEFT : begin
                                    p_blk_0 <= C_block[0];
                                    p_blk_1 <= C_block[1];
                                    p_blk_2 <= C_block[2];
                                    p_blk_3 <= C_block[3];
                                    q_blk_0 <= A_block[0];
                                    q_blk_1 <= A_block[1];
                                    q_blk_2 <= A_block[2];
                                    q_blk_3 <= A_block[3];
                                end
                                HOR_RIGHT : begin
                                    p_blk_0 <= D_block[0];
                                    p_blk_1 <= D_block[1];
                                    p_blk_2 <= D_block[2];
                                    p_blk_3 <= D_block[3];
                                    q_blk_0 <= q_block_1[0];
                                    q_blk_1 <= q_block_1[1];
                                    q_blk_2 <= q_block_1[2];
                                    q_blk_3 <= q_block_1[3];
                                end
                                HOR_FAR_RIGHT : begin
                                    p_blk_0 <= E_block[0];
                                    p_blk_1 <= E_block[1];
                                    p_blk_2 <= E_block[2];
                                    p_blk_3 <= E_block[3];
                                    q_blk_0 <= q_block_2[0];
                                    q_blk_1 <= q_block_2[1];
                                    q_blk_2 <= q_block_2[2];
                                    q_blk_3 <= q_block_2[3];
                                end
                            endcase
							tc_sel_reg <= {2'b00,qpc_table} + slice_tc_offset ;
							delta1_add_reg <= delta1_add ;
							delta2_add_reg <= delta2_add ;
						end	
					end
					STATE_1 : begin
						delta1 <= delta1_clip ;
						delta2 <= delta2_clip ;
						state <= STATE_2;
					end
					STATE_2 : begin
						if(edge_type==VERT_UP || edge_type==VERT_DOWN) begin
                            q_blk_0 <= sample_0_clip ;
                            p_blk_1 <= sample_1_clip ;
                            q_blk_2 <= sample_2_clip ;
                            p_blk_3 <= sample_3_clip ;
                        end
                        else begin
                            q_blk_0 <= sample_0_clip ;
                            p_blk_2 <= sample_1_clip ;
                            q_blk_1 <= sample_2_clip ;
                            p_blk_3 <= sample_3_clip ;
                        end
						state <= STATE_INIT;
						done <= 1'b1;
					end
			endcase
        end
    end
	
    
    always @(*) begin
        case(qp_sel)							// table values are qpi + 2
			6'd0  :  qpc_table = 6'd2  ;
            6'd1  :  qpc_table = 6'd3  ;
            6'd2  :  qpc_table = 6'd4  ;
            6'd3  :  qpc_table = 6'd5  ;
            6'd4  :  qpc_table = 6'd6  ;
            6'd5  :  qpc_table = 6'd7  ;
            6'd6  :  qpc_table = 6'd8  ;
            6'd7  :  qpc_table = 6'd9  ;
            6'd8  :  qpc_table = 6'd10 ;
            6'd9  :  qpc_table = 6'd11 ;
            6'd10 :  qpc_table = 6'd12 ;
            6'd11 :  qpc_table = 6'd13 ;
            6'd12 :  qpc_table = 6'd14 ;
            6'd13 :  qpc_table = 6'd15 ;
			6'd14 :  qpc_table = 6'd16 ;
            6'd15 :  qpc_table = 6'd17 ;
            6'd16 :  qpc_table = 6'd18 ;
            6'd17 :  qpc_table = 6'd19 ;
            6'd18 :  qpc_table = 6'd20 ;
            6'd19 :  qpc_table = 6'd21 ;
            6'd20 :  qpc_table = 6'd22 ;
            6'd21 :  qpc_table = 6'd23 ;
            6'd22 :  qpc_table = 6'd24 ;
            6'd23 :  qpc_table = 6'd25 ;
			6'd24 :  qpc_table = 6'd26 ;
			6'd25 :  qpc_table = 6'd27 ;
			6'd26 :  qpc_table = 6'd28 ;
			6'd27 :  qpc_table = 6'd29 ;
			6'd28 :  qpc_table = 6'd30 ;
			6'd29 :  qpc_table = 6'd31 ;
			
            6'd30 :  qpc_table = 6'd31 ;
            6'd31 :  qpc_table = 6'd32 ;
            6'd32 :  qpc_table = 6'd33 ;
            6'd33 :  qpc_table = 6'd34 ;
            6'd34 :  qpc_table = 6'd35 ;
            6'd35 :  qpc_table = 6'd35 ;
            6'd36 :  qpc_table = 6'd36 ;
            6'd37 :  qpc_table = 6'd36 ;
            6'd38 :  qpc_table = 6'd37 ;
            6'd39 :  qpc_table = 6'd37 ;
            6'd40 :  qpc_table = 6'd38 ;
            6'd41 :  qpc_table = 6'd38 ;
            6'd42 :  qpc_table = 6'd39 ;
            6'd43 :  qpc_table = 6'd39 ;
			
			6'd44 :  qpc_table = 6'd40 ;
            6'd45 :  qpc_table = 6'd41 ;
            6'd46 :  qpc_table = 6'd42 ;
            6'd47 :  qpc_table = 6'd43 ;
            6'd48 :  qpc_table = 6'd44 ;
            6'd49 :  qpc_table = 6'd45 ;
            6'd50 :  qpc_table = 6'd46 ;
            6'd51 :  qpc_table = 6'd47 ;
            6'd52 :  qpc_table = 6'd48 ;
            6'd53 :  qpc_table = 6'd49 ;
			6'd54 :  qpc_table = 6'd50 ;
			6'd55 :  qpc_table = 6'd51 ;
			6'd56 :  qpc_table = 6'd52 ;
			6'd57 :  qpc_table = 6'd53 ;
			6'd58 :  qpc_table = 6'd54 ;
			6'd59 :  qpc_table = 6'd55 ;
			6'd60 :  qpc_table = 6'd56 ;
            6'd61 :  qpc_table = 6'd57 ;
			6'd62 :  qpc_table = 6'd58 ;
			6'd63 :  qpc_table = 6'd59 ;
        endcase
    end
    
    always @(*) begin
        case(tc_sel_reg[5:0])
            6'd18 :  tc_table = 6'd1 ;
            6'd19 :  tc_table = 6'd1 ;
            6'd20 :  tc_table = 6'd1 ;
            6'd21 :  tc_table = 6'd1 ;
            6'd22 :  tc_table = 6'd1 ;
            6'd23 :  tc_table = 6'd1 ;
            6'd24 :  tc_table = 6'd1 ;
            6'd25 :  tc_table = 6'd1 ;
            6'd26 :  tc_table = 6'd1 ;
            6'd27 :  tc_table = 6'd2 ;
            6'd28 :  tc_table = 6'd2 ;
            6'd29 :  tc_table = 6'd2 ;
            6'd30 :  tc_table = 6'd2 ;
            6'd31 :  tc_table = 6'd3 ;
            6'd32 :  tc_table = 6'd3 ;
            6'd33 :  tc_table = 6'd3 ;
            6'd34 :  tc_table = 6'd3 ;
            6'd35 :  tc_table = 6'd4 ;
            6'd36 :  tc_table = 6'd4 ;
            6'd37 :  tc_table = 6'd4 ;
            6'd38 :  tc_table = 6'd5 ;
            6'd39 :  tc_table = 6'd5 ;
            6'd40 :  tc_table = 6'd6 ;
            6'd41 :  tc_table = 6'd6 ;
            6'd42 :  tc_table = 6'd7 ;
            6'd43 :  tc_table = 6'd8 ;
            6'd44 :  tc_table = 6'd9 ;
            6'd45 :  tc_table = 6'd10 ;
            6'd46 :  tc_table = 6'd11 ;
            6'd47 :  tc_table = 6'd13 ;
            6'd48 :  tc_table = 6'd14 ;
            6'd49 :  tc_table = 6'd16 ;
            6'd50 :  tc_table = 6'd18 ;
            6'd51 :  tc_table = 6'd20 ;
            6'd52 :  tc_table = 6'd22 ;
            6'd53 :  tc_table = 6'd24 ;
			6'd54 :  tc_table = 6'd24 ;
			6'd55 :  tc_table = 6'd24 ;
			6'd56 :  tc_table = 6'd24 ;
			6'd57 :  tc_table = 6'd24 ;
			6'd58 :  tc_table = 6'd24 ;
			6'd59 :  tc_table = 6'd24 ;
			6'd60 :  tc_table = 6'd24 ;
            6'd61 :  tc_table = 6'd24 ;
			6'd62 :  tc_table = 6'd24 ;
			6'd63 :  tc_table = 6'd24 ;
            default : tc_table = 6'd0 ;
        endcase
    end
	
	
	//-------------- Combinational blocks to calculate qp select -------------------
	always @(*) begin
		case(edge_type)
			VERT_UP : begin
				qp_p = qp_A;
				qp_q = qp_1;
			end
			VERT_DOWN : begin
				qp_p = qp_A;
				qp_q = qp_1;
			end
			HOR_LEFT : begin
				qp_p = qp_C;
				qp_q = qp_A;
			end
			HOR_RIGHT : begin
				qp_p = qp_D;
				qp_q = qp_1;
			end
			HOR_FAR_RIGHT : begin
				qp_p = qp_D;
				qp_q = qp_1;
			end
			default : begin
				qp_p = 5'd0;
				qp_q = 5'd0;
			end
		endcase
	end
	
	always @(*) begin
		qp_avg_7  =  (qp_p + qp_q + 1'b1) ;		// 8 bit unsigned
	end
	
	assign	qp_avg  =  qp_avg_7[6:1] ;
	
	always @(*) begin
		if(CHR_ID == CB) begin
            qp_sel  =  qp_avg  +  {pps_cb_qp_offset[4],pps_cb_qp_offset} ;		// 6 bit unsigned
        end    
        else begin
            qp_sel  =  qp_avg  +  {pps_cr_qp_offset[4],pps_cr_qp_offset} ;
        end
	end
	
	//------------- Combinational block to calculate tc ------------------------
	assign  n_tc  =  (~tc) + 9'b1 ;
	always @(*) begin
		if(tc_sel_reg[7]==1) begin
			tc = 9'd0 ;
		end
		else begin
			tc = {3'b000,tc_table} ;
		end
	end
	
	//------------- Combinational blocks to calculate delta 1 and 2 -----------------
	always @(*) begin
		case(edge_type)
            VERT_UP : begin
				delta1_sub1  =  {1'b0, q_block_1[0]} -  {1'b0, A_block[1]  } ;		// 9 bit signed
				delta1_sub2	 =  {1'b0, A_block[0]  } -  {1'b0, q_block_1[1]} ;
				delta2_sub1  =  {1'b0, q_block_1[2]} -  {1'b0, A_block[3]  } ;
				delta2_sub2  =  {1'b0, A_block[2]  } -  {1'b0, q_block_1[3]} ;
            end
            VERT_DOWN : begin
				delta1_sub1  =  {1'b0, q_block_3[0]} -  {1'b0, B_block[1]  } ;
				delta1_sub2	 =  {1'b0, B_block[0]  } -  {1'b0, q_block_3[1]} ;
				delta2_sub1  =  {1'b0, q_block_3[2]} -  {1'b0, B_block[3]  } ;
				delta2_sub2  =  {1'b0, B_block[2]  } -  {1'b0, q_block_3[3]} ;
            end
            HOR_LEFT : begin
				delta1_sub1  =  {1'b0, A_block[0]}   -  {1'b0, C_block[2]} ;
				delta1_sub2	 =  {1'b0, C_block[0]  } -  {1'b0, A_block[2]} ;
				delta2_sub1  =  {1'b0, A_block[1]}   -  {1'b0, C_block[3]} ;
				delta2_sub2  =  {1'b0, C_block[1]  } -  {1'b0, A_block[3]} ;
            end
            HOR_RIGHT : begin
				delta1_sub1  =  {1'b0, q_block_1[0]} -  {1'b0, D_block[2]  } ;
				delta1_sub2	 =  {1'b0, D_block[0]  } -  {1'b0, q_block_1[2]} ;
				delta2_sub1  =  {1'b0, q_block_1[1]} -  {1'b0, D_block[3]  } ;
				delta2_sub2  =  {1'b0, D_block[1]  } -  {1'b0, q_block_1[3]} ;
            end
            HOR_FAR_RIGHT : begin
				delta1_sub1  =  {1'b0, q_block_2[0]} -  {1'b0, E_block[2]  } ;
				delta1_sub2	 =  {1'b0, E_block[0]  } -  {1'b0, q_block_2[2]} ;
				delta2_sub1  =  {1'b0, q_block_2[1]} -  {1'b0, E_block[3]  } ;
				delta2_sub2  =  {1'b0, E_block[1]  } -  {1'b0, q_block_2[3]} ;
            end
			default : begin
				delta1_sub1  =  9'd0 ;
				delta1_sub2	 =  9'd0 ;
				delta2_sub1  =  9'd0 ;
				delta2_sub2  =  9'd0 ;
			end
        endcase
	end
	
	always @(*) begin
		delta1_add  =  {delta1_sub1[8],delta1_sub1,2'b00} + {{3{delta1_sub2[8]}},delta1_sub2} + 9'd4 ;		// 12 bit signed
		delta2_add  =  {delta2_sub1[8],delta2_sub1,2'b00} + {{3{delta2_sub2[8]}},delta2_sub2} + 9'd4 ;
	end
	
	always @(*) begin
		if(delta1_add_reg[11]==0) begin
			delta1_clip  =  (delta1_add_reg[11:3] > tc)  ?  tc  :  delta1_add_reg[11:3] ;
		end
		else begin
			if(n_tc == 9'd0) begin
				delta1_clip  =  9'd0 ;
			end
			else begin
				delta1_clip  =  (delta1_add_reg[11:3] < n_tc)  ?  n_tc  :  delta1_add_reg[11:3] ;
			end
		end
		if(delta2_add_reg[11]==0) begin
			delta2_clip  =  (delta2_add_reg[11:3] > tc)  ?  tc  :  delta2_add_reg[11:3] ;
		end
		else begin
			if(n_tc == 9'd0) begin
				delta2_clip  =  9'd0 ;
			end
			else begin
				delta2_clip  =  (delta2_add_reg[11:3] < n_tc)  ?  n_tc  :  delta2_add_reg[11:3] ;
			end
		end
	end
	
	//------------ Combinational blocks to calculate filter samples ----------------
	always @(*) begin
		if(edge_type==VERT_UP || edge_type==VERT_DOWN) begin
            sample_0  =  {1'b0,q_blk_0}  -  delta1 ;
            sample_1  =  {1'b0,p_blk_1}  +  delta1 ;
            sample_2  =  {1'b0,q_blk_2}  -  delta2 ;
            sample_3  =  {1'b0,p_blk_3}  +  delta2 ;
        end                                
        else begin                         
            sample_0  =  {1'b0,q_blk_0}  -  delta1 ;
            sample_1  =  {1'b0,p_blk_2}  +  delta1 ;
            sample_2  =  {1'b0,q_blk_1}  -  delta2 ;
            sample_3  =  {1'b0,p_blk_3}  +  delta2 ;
        end
	end
	
	always @(*) begin
		if(delta1[8]==0) begin
			sample_0_clip  =  (sample_0[8]==1) ? 8'd0 	: sample_0[7:0] ;
			sample_1_clip  =  (sample_1[8]==1) ? 8'd255 : sample_1[7:0] ;
		end
		else begin
			sample_0_clip  =  (sample_0[8]==1) ? 8'd255 : sample_0[7:0] ;
			sample_1_clip  =  (sample_1[8]==1) ? 8'd0 	: sample_1[7:0] ;
		end
		if(delta2[8]==0) begin
			sample_2_clip  =  (sample_2[8]==1) ? 8'd0 	: sample_2[7:0] ;
			sample_3_clip  =  (sample_3[8]==1) ? 8'd255 : sample_3[7:0] ;
		end
		else begin
			sample_2_clip  =  (sample_2[8]==1) ? 8'd255 : sample_2[7:0] ;
			sample_3_clip  =  (sample_3[8]==1) ? 8'd0 	: sample_3[7:0] ;
		end
	end
	
	
endmodule
