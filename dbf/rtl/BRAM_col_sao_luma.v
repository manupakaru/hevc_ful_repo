`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:07:59 05/20/2014 
// Design Name: 
// Module Name:    BRAM_col_sao_luma 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module BRAM_col_sao_luma(
		clk,
        we_a,
        we_b,
        en_a,
        en_b,
        addr_a,
        addr_b,
        data_in_a,
        data_in_b,
        data_out_a,
        data_out_b
    );
	
	`include "../sim/pred_def.v"
	
	input                       clk;
    input                       we_a;
    input                       we_b;
    input                       en_a;
    input                       en_b;
    input           [8:0]       addr_a;
    input           [8:0]       addr_b;
    input           [127:0]     data_in_a;
    input           [127:0]     data_in_b;
    output reg      [127:0]     data_out_a;
    output reg      [127:0]     data_out_b;
    
    reg             [127:0]     RAM [MAX_PIC_HEIGHT/4 - 1:0];
    
    always @(posedge clk) begin
        if(en_a) begin
            if(we_a) begin
                RAM[addr_a] <= data_in_a;
            end
            else begin
                data_out_a <= RAM[addr_a];
            end
        end
        if(en_b) begin
            if(we_b) begin
                RAM[addr_b] <= data_in_b;
            end
            else begin
                data_out_b <= RAM[addr_b];
            end
        end
    end


endmodule
