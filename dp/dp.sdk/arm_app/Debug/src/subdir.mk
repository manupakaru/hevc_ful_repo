################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/displayport_lpm.c \
../src/displayport_tx_drv.c \
../src/main.c \
../src/xil_ccc_app.c \
../src/xil_displayport.c \
../src/xlib_string.c 

LD_SRCS += \
../src/lscript.ld 

OBJS += \
./src/displayport_lpm.o \
./src/displayport_tx_drv.o \
./src/main.o \
./src/xil_ccc_app.o \
./src/xil_displayport.o \
./src/xlib_string.o 

C_DEPS += \
./src/displayport_lpm.d \
./src/displayport_tx_drv.d \
./src/main.d \
./src/xil_ccc_app.d \
./src/xil_displayport.d \
./src/xlib_string.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -I../../arm_app_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


