# file: displayport_0_clocks.xdc (IP Level XDC)

#-----------------------------------------------------------------
# cross clock constraints 
#-----------------------------------------------------------------
set_false_path -from [ get_clocks -of_objects [get_ports s_axi_aclk] ] -to   [get_clocks -of_objects [get_ports tx_vid_clk] ] 
set_false_path -from [ get_clocks -of_objects [get_ports s_axi_aclk] ] -to   [get_clocks -of_objects [get_pins -hierarchical -filter {name=~*gt0*TXOUTCLK}] ]
set_false_path -from [ get_clocks -of_objects [get_ports tx_vid_clk] ] -to   [ get_clocks -of_objects [get_ports s_axi_aclk] ]  
set_false_path -from [ get_clocks -of_objects [get_ports tx_vid_clk] ] -to   [get_clocks -of_objects [get_pins -hierarchical -filter {name=~*gt0*TXOUTCLK}] ]
set_false_path -from [get_clocks -of_objects [get_pins -hierarchical -filter {name=~*gt0*TXOUTCLK}] ] -to [ get_clocks -of_objects [get_ports s_axi_aclk] ]
set_false_path -from [get_clocks -of_objects [get_pins -hierarchical -filter {name=~*gt0*TXOUTCLK}] ] -to [ get_clocks -of_objects [get_ports tx_vid_clk] ]

