//Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2014.2 (win64) Build 932637 Wed Jun 11 13:33:10 MDT 2014
//Date        : Thu Dec 18 18:23:22 2014
//Host        : GCLAP4 running 64-bit major release  (build 9200)
//Command     : generate_target dp_system_wrapper.bd
//Design      : dp_system_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module dp_system_wrapper
   (DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    SYS_CLK_clk_n,
    SYS_CLK_clk_p,
    apb_m_paddr,
    apb_m_pclk,
    apb_m_penable,
    apb_m_prdata,
    apb_m_pready,
    apb_m_presetn,
    apb_m_psel,
    apb_m_pslverr,
    apb_m_pwdata,
    apb_m_pwrite,
    aux_tx_io_n,
    aux_tx_io_p,
    clk_out1,
    clk_out2,
    clk_out3,
    dp_mainlink_lnk_clk,
    dp_mainlink_lnk_clk_n,
    dp_mainlink_lnk_clk_p,
    dp_mainlink_lnk_tx_lane_n,
    dp_mainlink_lnk_tx_lane_p,
    dp_tx_vid_intf_str0_tx_vid_clk,
    dp_tx_vid_intf_str0_tx_vid_enable,
    dp_tx_vid_intf_str0_tx_vid_hsync,
    dp_tx_vid_intf_str0_tx_vid_oddeven,
    dp_tx_vid_intf_str0_tx_vid_pixel0,
    dp_tx_vid_intf_str0_tx_vid_pixel1,
    dp_tx_vid_intf_str0_tx_vid_rst,
    dp_tx_vid_intf_str0_tx_vid_vsync,
    gpo,
    iic2inctc_irpt_scl_io,
    iic2inctc_irpt_sda_io,
    s_axi_aresetn,
    sys_rst,
    tx_hpd);
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  input SYS_CLK_clk_n;
  input SYS_CLK_clk_p;
  output [31:0]apb_m_paddr;
  output apb_m_pclk;
  output apb_m_penable;
  input [31:0]apb_m_prdata;
  input [0:0]apb_m_pready;
  output [0:0]apb_m_presetn;
  output [0:0]apb_m_psel;
  input [0:0]apb_m_pslverr;
  output [31:0]apb_m_pwdata;
  output apb_m_pwrite;
  inout aux_tx_io_n;
  inout aux_tx_io_p;
  output clk_out1;
  output clk_out2;
  output clk_out3;
  output dp_mainlink_lnk_clk;
  input dp_mainlink_lnk_clk_n;
  input dp_mainlink_lnk_clk_p;
  output [3:0]dp_mainlink_lnk_tx_lane_n;
  output [3:0]dp_mainlink_lnk_tx_lane_p;
  input dp_tx_vid_intf_str0_tx_vid_clk;
  input dp_tx_vid_intf_str0_tx_vid_enable;
  input dp_tx_vid_intf_str0_tx_vid_hsync;
  input dp_tx_vid_intf_str0_tx_vid_oddeven;
  input [47:0]dp_tx_vid_intf_str0_tx_vid_pixel0;
  input [47:0]dp_tx_vid_intf_str0_tx_vid_pixel1;
  input dp_tx_vid_intf_str0_tx_vid_rst;
  input dp_tx_vid_intf_str0_tx_vid_vsync;
  output [1:0]gpo;
  inout iic2inctc_irpt_scl_io;
  inout iic2inctc_irpt_sda_io;
  input s_axi_aresetn;
  output [0:0]sys_rst;
  input tx_hpd;

  wire [14:0]DDR_addr;
  wire [2:0]DDR_ba;
  wire DDR_cas_n;
  wire DDR_ck_n;
  wire DDR_ck_p;
  wire DDR_cke;
  wire DDR_cs_n;
  wire [3:0]DDR_dm;
  wire [31:0]DDR_dq;
  wire [3:0]DDR_dqs_n;
  wire [3:0]DDR_dqs_p;
  wire DDR_odt;
  wire DDR_ras_n;
  wire DDR_reset_n;
  wire DDR_we_n;
  wire FIXED_IO_ddr_vrn;
  wire FIXED_IO_ddr_vrp;
  wire [53:0]FIXED_IO_mio;
  wire FIXED_IO_ps_clk;
  wire FIXED_IO_ps_porb;
  wire FIXED_IO_ps_srstb;
  wire SYS_CLK_clk_n;
  wire SYS_CLK_clk_p;
  wire [31:0]apb_m_paddr;
  wire apb_m_pclk;
  wire apb_m_penable;
  wire [31:0]apb_m_prdata;
  wire [0:0]apb_m_pready;
  wire [0:0]apb_m_presetn;
  wire [0:0]apb_m_psel;
  wire [0:0]apb_m_pslverr;
  wire [31:0]apb_m_pwdata;
  wire apb_m_pwrite;
  wire aux_tx_io_n;
  wire aux_tx_io_p;
  wire clk_out1;
  wire clk_out2;
  wire clk_out3;
  wire dp_mainlink_lnk_clk;
  wire dp_mainlink_lnk_clk_n;
  wire dp_mainlink_lnk_clk_p;
  wire [3:0]dp_mainlink_lnk_tx_lane_n;
  wire [3:0]dp_mainlink_lnk_tx_lane_p;
  wire dp_tx_vid_intf_str0_tx_vid_clk;
  wire dp_tx_vid_intf_str0_tx_vid_enable;
  wire dp_tx_vid_intf_str0_tx_vid_hsync;
  wire dp_tx_vid_intf_str0_tx_vid_oddeven;
  wire [47:0]dp_tx_vid_intf_str0_tx_vid_pixel0;
  wire [47:0]dp_tx_vid_intf_str0_tx_vid_pixel1;
  wire dp_tx_vid_intf_str0_tx_vid_rst;
  wire dp_tx_vid_intf_str0_tx_vid_vsync;
  wire [1:0]gpo;
  wire iic2inctc_irpt_scl_i;
  wire iic2inctc_irpt_scl_io;
  wire iic2inctc_irpt_scl_o;
  wire iic2inctc_irpt_scl_t;
  wire iic2inctc_irpt_sda_i;
  wire iic2inctc_irpt_sda_io;
  wire iic2inctc_irpt_sda_o;
  wire iic2inctc_irpt_sda_t;
  wire s_axi_aresetn;
  wire [0:0]sys_rst;
  wire tx_hpd;

dp_system dp_system_i
       (.DDR_addr(DDR_addr),
        .DDR_ba(DDR_ba),
        .DDR_cas_n(DDR_cas_n),
        .DDR_ck_n(DDR_ck_n),
        .DDR_ck_p(DDR_ck_p),
        .DDR_cke(DDR_cke),
        .DDR_cs_n(DDR_cs_n),
        .DDR_dm(DDR_dm),
        .DDR_dq(DDR_dq),
        .DDR_dqs_n(DDR_dqs_n),
        .DDR_dqs_p(DDR_dqs_p),
        .DDR_odt(DDR_odt),
        .DDR_ras_n(DDR_ras_n),
        .DDR_reset_n(DDR_reset_n),
        .DDR_we_n(DDR_we_n),
        .FIXED_IO_ddr_vrn(FIXED_IO_ddr_vrn),
        .FIXED_IO_ddr_vrp(FIXED_IO_ddr_vrp),
        .FIXED_IO_mio(FIXED_IO_mio),
        .FIXED_IO_ps_clk(FIXED_IO_ps_clk),
        .FIXED_IO_ps_porb(FIXED_IO_ps_porb),
        .FIXED_IO_ps_srstb(FIXED_IO_ps_srstb),
        .SYS_CLK_clk_n(SYS_CLK_clk_n),
        .SYS_CLK_clk_p(SYS_CLK_clk_p),
        .apb_m_paddr(apb_m_paddr),
        .apb_m_pclk(apb_m_pclk),
        .apb_m_penable(apb_m_penable),
        .apb_m_prdata(apb_m_prdata),
        .apb_m_pready(apb_m_pready),
        .apb_m_presetn(apb_m_presetn),
        .apb_m_psel(apb_m_psel),
        .apb_m_pslverr(apb_m_pslverr),
        .apb_m_pwdata(apb_m_pwdata),
        .apb_m_pwrite(apb_m_pwrite),
        .aux_tx_io_n(aux_tx_io_n),
        .aux_tx_io_p(aux_tx_io_p),
        .clk_out1(clk_out1),
        .clk_out2(clk_out2),
        .clk_out3(clk_out3),
        .dp_mainlink_lnk_clk(dp_mainlink_lnk_clk),
        .dp_mainlink_lnk_clk_n(dp_mainlink_lnk_clk_n),
        .dp_mainlink_lnk_clk_p(dp_mainlink_lnk_clk_p),
        .dp_mainlink_lnk_tx_lane_n(dp_mainlink_lnk_tx_lane_n),
        .dp_mainlink_lnk_tx_lane_p(dp_mainlink_lnk_tx_lane_p),
        .dp_tx_vid_intf_str0_tx_vid_clk(dp_tx_vid_intf_str0_tx_vid_clk),
        .dp_tx_vid_intf_str0_tx_vid_enable(dp_tx_vid_intf_str0_tx_vid_enable),
        .dp_tx_vid_intf_str0_tx_vid_hsync(dp_tx_vid_intf_str0_tx_vid_hsync),
        .dp_tx_vid_intf_str0_tx_vid_oddeven(dp_tx_vid_intf_str0_tx_vid_oddeven),
        .dp_tx_vid_intf_str0_tx_vid_pixel0(dp_tx_vid_intf_str0_tx_vid_pixel0),
        .dp_tx_vid_intf_str0_tx_vid_pixel1(dp_tx_vid_intf_str0_tx_vid_pixel1),
        .dp_tx_vid_intf_str0_tx_vid_rst(dp_tx_vid_intf_str0_tx_vid_rst),
        .dp_tx_vid_intf_str0_tx_vid_vsync(dp_tx_vid_intf_str0_tx_vid_vsync),
        .gpo(gpo),
        .iic2inctc_irpt_scl_i(iic2inctc_irpt_scl_i),
        .iic2inctc_irpt_scl_o(iic2inctc_irpt_scl_o),
        .iic2inctc_irpt_scl_t(iic2inctc_irpt_scl_t),
        .iic2inctc_irpt_sda_i(iic2inctc_irpt_sda_i),
        .iic2inctc_irpt_sda_o(iic2inctc_irpt_sda_o),
        .iic2inctc_irpt_sda_t(iic2inctc_irpt_sda_t),
        .s_axi_aresetn(s_axi_aresetn),
        .sys_rst(sys_rst),
        .tx_hpd(tx_hpd));
IOBUF iic2inctc_irpt_scl_iobuf
       (.I(iic2inctc_irpt_scl_o),
        .IO(iic2inctc_irpt_scl_io),
        .O(iic2inctc_irpt_scl_i),
        .T(iic2inctc_irpt_scl_t));
IOBUF iic2inctc_irpt_sda_iobuf
       (.I(iic2inctc_irpt_sda_o),
        .IO(iic2inctc_irpt_sda_io),
        .O(iic2inctc_irpt_sda_i),
        .T(iic2inctc_irpt_sda_t));
endmodule
