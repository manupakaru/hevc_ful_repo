/******************************************************************************
-- (c) Copyright 1995 - 2013 Xilinx, Inc. All rights reserved.
--
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and 
-- international copyright and other intellectual property
-- laws.
--
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
*******************************************************************************/

`timescale 1ns / 1ps

module dp_src_pm_v1_0
    (
    // output [13:0]DDR3_addr,
    // output [2:0]DDR3_ba,
    // output DDR3_cas_n,
    // output [0:0]DDR3_ck_n,
    // output [0:0]DDR3_ck_p,
    // output [0:0]DDR3_cke,
    // output [0:0]DDR3_cs_n,
    // output [7:0]DDR3_dm,
    // inout [63:0]DDR3_dq,
    // inout [7:0]DDR3_dqs_n,
    // inout [7:0]DDR3_dqs_p,
    // output [0:0]DDR3_odt,
    // output DDR3_ras_n,
    // output DDR3_reset_n,
    // output DDR3_we_n,
    inout [14:0]DDR_addr,
    inout [2:0]DDR_ba,
    inout DDR_cas_n,
    inout DDR_ck_n,
    inout DDR_ck_p,
    inout DDR_cke,
    inout DDR_cs_n,
    inout [3:0]DDR_dm,
    inout [31:0]DDR_dq,
    inout [3:0]DDR_dqs_n,
    inout [3:0]DDR_dqs_p,
    inout DDR_odt,
    inout DDR_ras_n,
    inout DDR_reset_n,
    inout DDR_we_n,
    //input [0:0]Dp_Int,
    inout FIXED_IO_ddr_vrn,
    inout FIXED_IO_ddr_vrp,
    inout [53:0]FIXED_IO_mio,
    inout FIXED_IO_ps_clk,
    inout FIXED_IO_ps_porb,
    inout FIXED_IO_ps_srstb,
      input  wire        clk_200_p,
      input  wire        clk_200_n,
      input  wire        ext_aud_clk_p,
      input  wire        ext_aud_clk_n,

      input  wire        lnk_clk_p,
      input  wire        lnk_clk_n,
     // input  wire        reset,

      output wire [4-1:0]  lnk_tx_lane_p,
      output wire [4-1:0]  lnk_tx_lane_n,

      inout  wire        aux_tx_io_p,
      inout wire        aux_tx_io_n,
      input  wire        hpd,
      input  wire        RS232_Uart_1_RX,
      output wire        RS232_Uart_1_TX,
      inout  wire        kc705_iic_Scl,
      inout  wire        kc705_iic_Sda,
      // output wire       ddr3_we_n,
      // output wire       ddr3_ras_n,
      // output wire       ddr3_odt,
      // inout wire [7:0]  ddr3_dqs_n,
      // inout wire [7:0]  ddr3_dqs_p,
      // inout wire [63:0]  ddr3_dq,
      // output wire [7:0] ddr3_dm,
      // output wire       ddr3_reset_n,
      // output wire       ddr3_cs_n,
      // output wire       ddr3_ck_n,
      // output wire       ddr3_ck_p,
      // output wire       ddr3_cke,
      // output wire       ddr3_cas_n,
      // output wire [2:0] ddr3_ba,
      // output wire [13:0] ddr3_addr,
      output	clk_int_135_out_n, //135output non-cleaned, going to SI5324
      output	clk_int_135_out_p,
      output wire [1:0]  gpo

    );

    wire        reset;
  wire        lnk_clk;
  wire        reset_n = !reset;

//  wire  [31:0] axi_ext_slave_conn_dp_tx_M_AXI_AWADDR_pin;
//  wire  [2:0] axi_ext_slave_conn_dp_tx_M_AXI_AWPROT_pin;
//  wire  axi_ext_slave_conn_dp_tx_M_AXI_AWVALID_pin;
//  wire axi_ext_slave_conn_dp_tx_M_AXI_AWREADY_pin;
//  wire  [31:0] axi_ext_slave_conn_dp_tx_M_AXI_WDATA_pin;
//  wire  [3:0] axi_ext_slave_conn_dp_tx_M_AXI_WSTRB_pin;
//  wire  axi_ext_slave_conn_dp_tx_M_AXI_WVALID_pin;
//  wire axi_ext_slave_conn_dp_tx_M_AXI_WREADY_pin;
//  wire [1:0] axi_ext_slave_conn_dp_tx_M_AXI_BRESP_pin;
//  wire axi_ext_slave_conn_dp_tx_M_AXI_BVALID_pin;
//  wire  axi_ext_slave_conn_dp_tx_M_AXI_BREADY_pin;
//  wire  [31:0] axi_ext_slave_conn_dp_tx_M_AXI_ARADDR_pin;
//  wire  [2:0] axi_ext_slave_conn_dp_tx_M_AXI_ARPROT_pin;
//  wire  axi_ext_slave_conn_dp_tx_M_AXI_ARVALID_pin;
//  wire axi_ext_slave_conn_dp_tx_M_AXI_ARREADY_pin;
//  wire [31:0] axi_ext_slave_conn_dp_tx_M_AXI_RDATA_pin;
//  wire [1:0] axi_ext_slave_conn_dp_tx_M_AXI_RRESP_pin;
//  wire axi_ext_slave_conn_dp_tx_M_AXI_RVALID_pin;
//  wire  axi_ext_slave_conn_dp_tx_M_AXI_RREADY_pin;

  wire clk_int_50;
  wire clk_int_135;
  wire clk_int_200;
  wire clk_int_135_out;
  wire dp_tx_vid_clk;

  wire        dp_tx_vsync;
  wire        dp_tx_hsync;
  wire        dp_tx_de;
  wire        dp_tx_oddeven;
  wire [47:0] dp_tx_vid_pixel0;
  wire [47:0] dp_tx_vid_pixel1;
  wire [47:0] dp_tx_vid_pixel2;
  wire [47:0] dp_tx_vid_pixel3;

  reg [3:0] axilite_reset_q; 
  reg [3:0] dp_tx_vid_reset_q; 

  wire axilite_reset;
  wire axilite_reset_n;
  wire dp_tx_vid_reset;

 
    IBUFGDS #(
      .DIFF_TERM("FALSE"), // Differential Termination
      .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE" 
      .IOSTANDARD("DEFAULT") // Specifies the I/O standard for this buffer
   ) IBUFGDS_aud_inst_ext_clk (
      .O(ext_aud_clk),  // Clock buffer output
      .I(ext_aud_clk_p),  // Diff_p clock buffer input
      .IB(ext_aud_clk_n) // Diff_n clock buffer input
   );

  wire ext_aud_reset;
  wire ext_aud_reset_n;
  reg  [3:0] ext_aud_reset_q;
  wire aud_start; 
  
   always@(posedge ext_aud_clk or posedge axilite_reset) begin
     if(axilite_reset) begin
       ext_aud_reset_q <= 4'b1111;
     end else begin
       ext_aud_reset_q <= {ext_aud_reset_q[2:0], 1'b0};
     end
   end
  
   assign aud_start          = ~ext_aud_reset; 
   
  always@(posedge clk_int_50 or posedge reset) begin
    if(reset) begin
      axilite_reset_q <= 4'b1111;
    end else begin
      axilite_reset_q <= {axilite_reset_q[2:0], 1'b0};
    end
  end 

  always@(posedge dp_tx_vid_clk or posedge reset) begin
    if(reset) begin
      dp_tx_vid_reset_q <= 4'b1111;
    end else begin
      dp_tx_vid_reset_q <= {dp_tx_vid_reset_q[2:0], 1'b0};
    end
  end 

  assign ext_aud_reset     =  ext_aud_reset_q[3];
  assign axilite_reset     =  axilite_reset_q[3];
  assign axilite_reset_n   = ~axilite_reset_q[3];
  assign dp_tx_vid_reset   =  dp_tx_vid_reset_q[3];
  assign ext_aud_reset_n   = ~ext_aud_reset_q[3];

  wire  axi_apb_bridge_video_pat_gen_M_APB_PCLK_pin;
  wire  axi_apb_bridge_video_pat_gen_M_APB_PRESETN_pin;
  wire  [31:0] axi_apb_bridge_video_pat_gen_M_APB_PADDR_pin;
  wire  axi_apb_bridge_video_pat_gen_M_APB_PSEL_pin;
  wire  axi_apb_bridge_video_pat_gen_M_APB_PENABLE_pin;
  wire  axi_apb_bridge_video_pat_gen_M_APB_PWRITE_pin;
  wire  [31:0] axi_apb_bridge_video_pat_gen_M_APB_PWDATA_pin;
  wire axi_apb_bridge_video_pat_gen_M_APB_PREADY_pin;
  wire [31:0] axi_apb_bridge_video_pat_gen_M_APB_PRDATA_pin;
  wire axi_apb_bridge_video_pat_gen_M_APB_PSLVERR_pin;

  wire [19:0] d0, d1; // dummy wires
  wire vid_clk_sel;
  wire sw_vid_reset;
  wire vid_clk_prog;
  wire [7:0] vid_clk_M;
  wire [15:0] vid_clk_D;
  wire vid_clk_lckd;
  
//  wire iic2inctc_irpt_scl_i;
//  wire iic2inctc_irpt_scl_o;
//  wire iic2inctc_irpt_scl_t;
//  wire iic2inctc_irpt_sda_i;
//  wire iic2inctc_irpt_sda_o;
//  wire iic2inctc_irpt_sda_t;
  
    (* MARK_DEBUG="true" *) wire [31:0] s_axis_audio_ingress_tdata;
    (* MARK_DEBUG="true" *) wire [2:0]  s_axis_audio_ingress_tid;
    (* MARK_DEBUG="true" *) wire        s_axis_audio_ingress_tvalid;
    (* MARK_DEBUG="true" *) wire        s_axis_audio_ingress_tready;
  
  assign axi_apb_bridge_video_pat_gen_M_APB_PSLVERR_pin  = 1'b0;
  assign axi_apb_bridge_video_pat_gen_M_APB_PREADY_pin = 1'b1;
  assign axi_apb_bridge_video_pat_gen2_M_APB_PSLVERR_pin  = 1'b0;
  assign axi_apb_bridge_video_pat_gen2_M_APB_PREADY_pin = 1'b1;
  assign axi_apb_bridge_video_pat_gen3_M_APB_PSLVERR_pin  = 1'b0;
  assign axi_apb_bridge_video_pat_gen3_M_APB_PREADY_pin = 1'b1;
  assign axi_apb_bridge_video_pat_gen4_M_APB_PSLVERR_pin  = 1'b0;
  assign axi_apb_bridge_video_pat_gen4_M_APB_PREADY_pin = 1'b1;

  video_pat_gen video_pat_gen_inst 
     (
     .apb_clk       (clk_int_50),
     .apb_reset     (axilite_reset),
     .apb_select    (axi_apb_bridge_video_pat_gen_M_APB_PSEL_pin),
     .apb_enable    (axi_apb_bridge_video_pat_gen_M_APB_PENABLE_pin),
     .apb_write     (axi_apb_bridge_video_pat_gen_M_APB_PWRITE_pin),
     .apb_addr      (axi_apb_bridge_video_pat_gen_M_APB_PADDR_pin[11:0]),
     .apb_wdata     (axi_apb_bridge_video_pat_gen_M_APB_PWDATA_pin),
     .apb_rdata     (axi_apb_bridge_video_pat_gen_M_APB_PRDATA_pin),
     .vid_clk_sel   (vid_clk_sel),
     .vid_clk_M     (vid_clk_M),
     .vid_clk_D     (vid_clk_D),
     .vid_clk_prog  (vid_clk_prog),
     .sw_vid_reset  (sw_vid_reset),

     .run_pattern   (1'b0),
     .vid_clk       (dp_tx_vid_clk),
     .vid_reset     (!vid_clk_lckd),
     //.vid_enable    (dp_tx_de),
     //.vid_vsync     (dp_tx_vsync),
     //.vid_hsync     (dp_tx_hsync),
     //.vid_oddeven   (dp_tx_oddeven),
     //.vid_pixel0    (dp_tx_vid_pixel0),
     //.vid_pixel1    (dp_tx_vid_pixel1),
     .patt_done     ()
     );

video_transmitter new_pat_gen(
		.vid_clk      (dp_tx_vid_clk),
		.vid_run_clk  (dp_tx_vid_clk) ,
		.reset        (~vid_clk_lckd) ,
		.config_done  (1) ,
		.vid_clk_en   ()  ,
		.vid_hsync(dp_tx_hsync)   ,
		.vid_vsync(dp_tx_vsync)   ,
		.vid_enable(dp_tx_de)  ,
		.vid_oddeven(dp_tx_oddeven) ,
		.vid_pixel0(dp_tx_vid_pixel0)  ,
		.vid_pixel1(dp_tx_vid_pixel1)  
		//vid_pixel2  ,
		//vid_pixel3  
		//,pclk_count 
        //,hsync_count
        //,DE_count   
		
		
    );

  vid_clock new_vid_clock
 (
 // Clock in ports
  .clk_in1(clk_int_200),      // input clk_in1
  // Clock out ports
  .clk_out1(dp_tx_vid_clk),     // output clk_out1
  // Status and control signals
  .reset(axilite_reset), // input reset
  .locked(vid_clk_lckd)
);      // output locked






  vid_clkgen vid_clkgen_inst(
    .reset_i(sw_vid_reset),
    .refclk_i(lnk_clk),         //reference clock for synthesis
    .progclk_i(clk_int_200),      //progamming interface clock
    .prog_i(vid_clk_prog),      //one cycle long programming kick off
    .M_i(vid_clk_M),            //M
    .D_i(vid_clk_D),            //D
    .vid_clk_o(),  //vid_clk output
    .locked_o()     //vid_clk is locked
  );
  
  wire [175:0] debug_port_aud;


//IPI Design
dp_system_wrapper dp_policy_maker_inst
  (
       .s_axi_aresetn        (axilite_reset_n),

       .dp_tx_vid_intf_str0_tx_vid_clk           (dp_tx_vid_clk),
       .dp_tx_vid_intf_str0_tx_vid_rst           (dp_tx_vid_reset),
       .dp_tx_vid_intf_str0_tx_vid_vsync         (dp_tx_vsync),
       .dp_tx_vid_intf_str0_tx_vid_hsync         (dp_tx_hsync),
       .dp_tx_vid_intf_str0_tx_vid_oddeven       (dp_tx_oddeven),
       .dp_tx_vid_intf_str0_tx_vid_enable        (dp_tx_de),
       .dp_tx_vid_intf_str0_tx_vid_pixel0        (dp_tx_vid_pixel0),
       .dp_tx_vid_intf_str0_tx_vid_pixel1        (dp_tx_vid_pixel1),

       .dp_mainlink_lnk_clk_p            (lnk_clk_p),
       .dp_mainlink_lnk_clk_n            (lnk_clk_n),
       .dp_mainlink_lnk_clk              (lnk_clk),
       .dp_mainlink_lnk_tx_lane_p        (lnk_tx_lane_p),
       .dp_mainlink_lnk_tx_lane_n        (lnk_tx_lane_n),

       .aux_tx_io_p (aux_tx_io_p),
       .aux_tx_io_n (aux_tx_io_n),
       //.aux_tx_channel_out_p (), // OUT
       //.aux_tx_channel_out_n (), // OUT
       //.aux_tx_channel_in_p  (),  // IN
       //.aux_tx_channel_in_n  (),
       .tx_hpd               (hpd),

   .DDR_addr(DDR_addr),
   .DDR_ba(DDR_ba),
   .DDR_cas_n(DDR_cas_n),
   .DDR_ck_n(DDR_ck_n),
   .DDR_ck_p(DDR_ck_p),
   .DDR_cke(DDR_cke),
   .DDR_cs_n(DDR_cs_n),
   .DDR_dm(DDR_dm),
   .DDR_dq(DDR_dq),
   .DDR_dqs_n(DDR_dqs_n),
   .DDR_dqs_p(DDR_dqs_p),
   .DDR_odt(DDR_odt),
   .DDR_ras_n(DDR_ras_n),
   .DDR_reset_n(DDR_reset_n),
   .DDR_we_n(DDR_we_n),
   //.Dp_Int(Dp_Int),
   .FIXED_IO_ddr_vrn(FIXED_IO_ddr_vrn),
   .FIXED_IO_ddr_vrp(FIXED_IO_ddr_vrp),
   .FIXED_IO_mio(FIXED_IO_mio),
   .FIXED_IO_ps_clk(FIXED_IO_ps_clk),
   .FIXED_IO_ps_porb(FIXED_IO_ps_porb),
   .FIXED_IO_ps_srstb(FIXED_IO_ps_srstb),
    .sys_rst                       (reset),
    .iic2inctc_irpt_sda_io                    (kc705_iic_Sda),
    .iic2inctc_irpt_scl_io                    (kc705_iic_Scl),
    //.iic2inctc_irpt_scl_i(iic2inctc_irpt_scl_i),
    //.iic2inctc_irpt_scl_o(iic2inctc_irpt_scl_o),
    //.iic2inctc_irpt_scl_t(iic2inctc_irpt_scl_t),
    //.iic2inctc_irpt_sda_i(iic2inctc_irpt_sda_i),
    //.iic2inctc_irpt_sda_o(iic2inctc_irpt_sda_o),
    //.iic2inctc_irpt_sda_t(iic2inctc_irpt_sda_t),
    
    .SYS_CLK_clk_p                     (clk_200_p),
    .SYS_CLK_clk_n                     (clk_200_n),
    
    //.Dp_Int                        (dp_tx_axi_int),
    
    .gpo                           (gpo),
    
    .clk_out1                     (clk_int_200),
    .clk_out2                     (clk_int_135),
    .clk_out3                     (clk_int_50),

//    .M05_AXI_awaddr               (axi_ext_slave_conn_dp_tx_M_AXI_AWADDR_pin),
//    .M05_AXI_awprot               (axi_ext_slave_conn_dp_tx_M_AXI_AWPROT_pin),
//    .M05_AXI_awvalid              (axi_ext_slave_conn_dp_tx_M_AXI_AWVALID_pin),
//    .M05_AXI_awready              (axi_ext_slave_conn_dp_tx_M_AXI_AWREADY_pin),
//    .M05_AXI_wdata                (axi_ext_slave_conn_dp_tx_M_AXI_WDATA_pin),
//    .M05_AXI_wstrb                (axi_ext_slave_conn_dp_tx_M_AXI_WSTRB_pin),
//    .M05_AXI_wvalid               (axi_ext_slave_conn_dp_tx_M_AXI_WVALID_pin),
//    .M05_AXI_wready               (axi_ext_slave_conn_dp_tx_M_AXI_WREADY_pin),
//    .M05_AXI_bresp                (axi_ext_slave_conn_dp_tx_M_AXI_BRESP_pin),
//    .M05_AXI_bvalid               (axi_ext_slave_conn_dp_tx_M_AXI_BVALID_pin),
//    .M05_AXI_bready               (axi_ext_slave_conn_dp_tx_M_AXI_BREADY_pin),
//    .M05_AXI_araddr               (axi_ext_slave_conn_dp_tx_M_AXI_ARADDR_pin),
//    .M05_AXI_arprot               (axi_ext_slave_conn_dp_tx_M_AXI_ARPROT_pin),
//    .M05_AXI_arvalid              (axi_ext_slave_conn_dp_tx_M_AXI_ARVALID_pin),
//    .M05_AXI_arready              (axi_ext_slave_conn_dp_tx_M_AXI_ARREADY_pin),
//    .M05_AXI_rdata                (axi_ext_slave_conn_dp_tx_M_AXI_RDATA_pin),
//    .M05_AXI_rresp                (axi_ext_slave_conn_dp_tx_M_AXI_RRESP_pin),
//    .M05_AXI_rvalid               (axi_ext_slave_conn_dp_tx_M_AXI_RVALID_pin),
//    .M05_AXI_rready               (axi_ext_slave_conn_dp_tx_M_AXI_RREADY_pin),

    .apb_m_pclk                    (axi_apb_bridge_video_pat_gen_M_APB_PCLK_pin),         
    .apb_m_presetn                 (axi_apb_bridge_video_pat_gen_M_APB_PRESETN_pin),      
    .apb_m_paddr                   (axi_apb_bridge_video_pat_gen_M_APB_PADDR_pin),        
    .apb_m_psel                    (axi_apb_bridge_video_pat_gen_M_APB_PSEL_pin),         
    .apb_m_penable                 (axi_apb_bridge_video_pat_gen_M_APB_PENABLE_pin),      
    .apb_m_pwrite                  (axi_apb_bridge_video_pat_gen_M_APB_PWRITE_pin),       
    .apb_m_pwdata                  (axi_apb_bridge_video_pat_gen_M_APB_PWDATA_pin),       
    .apb_m_pready                  (axi_apb_bridge_video_pat_gen_M_APB_PREADY_pin),       
    .apb_m_prdata                  (axi_apb_bridge_video_pat_gen_M_APB_PRDATA_pin),       
    .apb_m_pslverr                 (axi_apb_bridge_video_pat_gen_M_APB_PSLVERR_pin));

//IOBUF iic2inctc_irpt_scl_iobuf
//       (.I(iic2inctc_irpt_scl_o),
//        .IO(kc705_iic_Scl),
//        .O(iic2inctc_irpt_scl_i),
 //       .T(iic2inctc_irpt_scl_t));
//IOBUF iic2inctc_irpt_sda_iobuf
//       (.I(iic2inctc_irpt_sda_o),
//        .IO(kc705_iic_Sda),
//        .O(iic2inctc_irpt_sda_i),
//        .T(iic2inctc_irpt_sda_t));
        
//generation of 135MHz through ODDR to si5324 input
ODDR clk_out_oddr
   (.Q  (clk_int_135_out),
    .C  (clk_int_135),
    .CE (1'b1),
    .D1 (1'b1),
    .D2 (1'b0),
    .R  (1'b0),
    .S  (1'b0));

////135output non-cleaned, going to SI5324 - Clock Output Buffer
OBUFDS
 #(.IOSTANDARD ("LVDS_25"))
clk135out_int_inst
  (.O          (clk_int_135_out_p),
   .OB         (clk_int_135_out_n),
   .I          (clk_int_135_out));

   
audio_pat_gen
   #(.pTCQ (1))
   audio_pat_gen_inst
   (
     .aud_clk                   (ext_aud_clk),      // input wire        
     .aud_reset                 (ext_aud_reset),    // input wire        
     .aud_start                 (aud_start),                 // input wire        
     .aud_sample_rate           (4'h1),                 // input wire [3:0]  
     .aud_channel_count         (4'h2),                 // input wire [3:0]  
     .aud_spdif_channel_status  (36'h0),                 // input wire [35:0] 
     .aud_pattern1              (2'b10), // input wire [1:0]  
     .aud_pattern2              (2'b10), // input wire [1:0]  
     .aud_pattern3              (2'b00), // input wire [1:0]  
     .aud_pattern4              (2'b00), // input wire [1:0]  
     .aud_pattern5              (2'b00), // input wire [1:0]  
     .aud_pattern6              (2'b00), // input wire [1:0]  
     .aud_pattern7              (2'b00), // input wire [1:0]  
     .aud_pattern8              (2'b00), // input wire [1:0]  
     .aud_period_ch1            (4'h2), // input wire [3:0]  
     .aud_period_ch2            (4'h2), // input wire [3:0]  
     .aud_period_ch3            (4'h0), // input wire [3:0]  
     .aud_period_ch4            (4'h0), // input wire [3:0]  
     .aud_period_ch5            (4'h0), // input wire [3:0]  
     .aud_period_ch6            (4'h0), // input wire [3:0]  
     .aud_period_ch7            (4'h0), // input wire [3:0]  
     .aud_period_ch8            (4'h0), // input wire [3:0]  
     .aud_config_update         (1'b1),//pulse to update the config  input wire
   
     // AXI Streaming Signals
     .axis_clk                  (ext_aud_clk),          // input  wire        
     .axis_resetn               (ext_aud_reset_n),        // input  wire        
     .axis_data_egress          (s_axis_audio_ingress_tdata),   // output reg  [31:0] 
     .axis_id_egress            (s_axis_audio_ingress_tid),     // output reg  [2:0]  
     .axis_tvalid               (s_axis_audio_ingress_tvalid),        // output reg         
     .axis_tready               (s_axis_audio_ingress_tready),        // input  wire        
     .debug_port                () // output wire [198:0]
   );
   
endmodule
