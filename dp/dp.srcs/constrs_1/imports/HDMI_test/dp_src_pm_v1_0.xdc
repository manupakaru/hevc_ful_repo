# (c) Copyright 2013 Xilinx, Inc. All rights reserved.
#
# This file contains confidential and proprietary information
# of Xilinx, Inc. and is protected under U.S. and
# international copyright and other intellectual property
# laws.
#
# DISCLAIMER
# This disclaimer is not a license and does not grant any
# rights to the materials distributed herewith. Except as
# otherwise provided in a valid license issued to you by
# Xilinx, and to the maximum extent permitted by applicable
# law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
# WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
# AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
# BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
# INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
# (2) Xilinx shall not be liable (whether in contract or tort,
# including negligence, or under any other theory of
# liability) for any loss or damage of any kind or nature
# related to, arising under or in connection with these
# materials, including for any direct, or any indirect,
# special, incidental, or consequential loss or damage
# (including loss of data, profits, goodwill, or any type of
# loss or damage suffered as a result of any action brought
# by a third party) even if such damage or loss was
# reasonably foreseeable or Xilinx had been advised of the
# possibility of the same.
#
# CRITICAL APPLICATIONS
# Xilinx products are not designed or intended to be fail-
# safe, or for use in any application requiring fail-safe
# performance, such as life-support or safety devices or
# systems, Class III medical devices, nuclear facilities,
# applications related to the deployment of airbags, or any
# other applications that could lead to death, personal
# injury, or severe property or environmental damage
# (individually and collectively, "Critical
# Applications"). Customer assumes the sole risk and
# liability of any use of Xilinx products in Critical
# Applications, subject only to applicable laws and
# regulations governing limitations on product liability.
#
# THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
# PART OF THIS FILE AT ALL TIMES.
#--------------------------------------------------------------------------
#
# KC705 XDC constraints for IPI Example Design
#

#-----------------------------------------------------------------
## Clock Constraints
#-----------------------------------------------------------------
set_property IOSTANDARD LVDS [get_ports clk_200_n]

set_property PACKAGE_PIN G9 [get_ports clk_200_n]
set_property IOSTANDARD LVDS [get_ports clk_200_p]

create_clock -period 5.000 -name CLK_IN1_Pin [get_ports clk_200_p]

# MGT refclk for DP FMC
set_property PACKAGE_PIN AC8 [get_ports lnk_clk_p]
create_clock -period 7.407 -name lnk_clk_p [get_ports lnk_clk_p]


set_property IOSTANDARD LVDS_25 [get_ports ext_aud_clk_n]
set_property PACKAGE_PIN AG14 [get_ports ext_aud_clk_n]
set_property IOSTANDARD LVDS_25 [get_ports ext_aud_clk_p]
create_clock -period 40.000 -name ext_aud_clk_p [get_ports ext_aud_clk_p]

#-----------------------------------------------------------------
## Reset Constraints bank 33
#-----------------------------------------------------------------
#set_property PACKAGE_PIN A8 [get_ports reset]
#set_property IOSTANDARD LVCMOS15 [get_ports reset]

#-----------------------------------------------------------------
## UART Constraints
#-----------------------------------------------------------------
#UART TX RX connected to FMC HPC header LA06P - TX
#UART TX RX connected to FMC HPC header LA06N - RX
#set_property PACKAGE_PIN AH22 [get_ports RS232_Uart_1_RX]
#set_property IOSTANDARD LVCMOS25 [get_ports RS232_Uart_1_RX]

#set_property PACKAGE_PIN AG22 [get_ports RS232_Uart_1_TX]
#set_property IOSTANDARD LVCMOS25 [get_ports RS232_Uart_1_TX]

#-----------------------------------------------------------------
## KC705 IIC Constraints
#-----------------------------------------------------------------
set_property PACKAGE_PIN AJ14 [get_ports kc705_iic_Scl]
set_property IOSTANDARD LVCMOS25 [get_ports kc705_iic_Scl]
set_property SLEW SLOW [get_ports kc705_iic_Scl]
set_property DRIVE 8 [get_ports kc705_iic_Scl]
set_property PACKAGE_PIN AJ18 [get_ports kc705_iic_Sda]
set_property IOSTANDARD LVCMOS25 [get_ports kc705_iic_Sda]
set_property SLEW SLOW [get_ports kc705_iic_Sda]
set_property DRIVE 8 [get_ports kc705_iic_Sda]


#-----------------------------------------------------------------
## DisplayPort Constraints
#-----------------------------------------------------------------
create_clock -period 20.000 -name dp_axi_aclk [get_nets *clk_int_50]
create_clock -period 7.407 -name dp_tx_vid_clk [get_nets dp_tx_vid_clk]
create_generated_clock -name lnk_clk -source [get_ports lnk_clk_p] -divide_by 4 -multiply_by 5 [get_pins -of_objects [get_cells -hier *ref_clk_out_bufg] -filter {direction == out}]
create_clock -period 3.703 -name dp_lnk_clk [get_nets lnk_clk]
set_false_path -from [get_clocks *axi_aclk] -to [get_clocks *vid_clk]
set_false_path -from [get_clocks *vid_clk] -to [get_clocks *axi_aclk]
set_false_path -from [get_clocks *axi_aclk] -to [get_clocks *lnk_clk]
set_false_path -from [get_clocks *lnk_clk] -to [get_clocks *axi_aclk]
set_false_path -from [get_clocks *vid_clk] -to [get_clocks *lnk_clk]
set_false_path -from [get_clocks *lnk_clk] -to [get_clocks *vid_clk]

#FMC_HPC_DP0_C2M_P,N
# set_property LOC GTXE2_CHANNEL_X0Y12 [get_cells displayport_v4_0_tx_inst/inst/dport_tx_phy_inst/gt_wrapper_inst/gt0_gt_7_series_wrapper_4_i/gtxe2_i]
set_property PACKAGE_PIN AK10 [get_ports {lnk_tx_lane_p[0]}]
#FMC_HPC_DP1_C2M_P,N
# set_property LOC GTXE2_CHANNEL_X0Y13 [get_cells displayport_v4_0_tx_inst/inst/dport_tx_phy_inst/gt_wrapper_inst/gt1_gt_7_series_wrapper_4_i/gtxe2_i]
set_property PACKAGE_PIN AK6 [get_ports {lnk_tx_lane_p[1]}]
#FMC_HPC_DP2_C2M_P,N
# set_property LOC GTXE2_CHANNEL_X0Y14 [get_cells displayport_v4_0_tx_inst/inst/dport_tx_phy_inst/gt_wrapper_inst/gt2_gt_7_series_wrapper_4_i/gtxe2_i]
set_property PACKAGE_PIN AJ4 [get_ports {lnk_tx_lane_p[2]}]
#FMC_HPC_DP3_C2M_P,N
# set_property LOC GTXE2_CHANNEL_X0Y15 [get_cells displayport_v4_0_tx_inst/inst/dport_tx_phy_inst/gt_wrapper_inst/gt3_gt_7_series_wrapper_4_i/gtxe2_i]
set_property PACKAGE_PIN AK2 [get_ports {lnk_tx_lane_p[3]}]

# DP HPD
set_property PACKAGE_PIN AJ20 [get_ports hpd]
set_property IOSTANDARD LVCMOS25 [get_ports hpd]
set_property PULLDOWN true [get_ports hpd]

# AUX channel pins
set_property IOSTANDARD BLVDS_25 [get_ports aux_tx_io_n]
set_property IOSTANDARD BLVDS_25 [get_ports aux_tx_io_p]
set_property PACKAGE_PIN AJ23 [get_ports aux_tx_io_p]

#-------------------clocks of 135MHz (generated by clk_wiz) going to Si5324------------
set_property IOSTANDARD LVDS_25 [get_ports clk_int_135_out_p]
#set_property LOC OLOGIC_X0Y96 [get_cells clk_out_oddr]
set_property PACKAGE_PIN AE20 [get_ports clk_int_135_out_n]
set_property IOSTANDARD LVDS_25 [get_ports clk_int_135_out_n]

#set_property PACKAGE_PIN F20 [get_ports {gpo[0]}]
#PS side pin
set_property PACKAGE_PIN W21 [get_ports {gpo[0]}]

set_property IOSTANDARD LVCMOS25 [get_ports {gpo[0]}]

set_property PACKAGE_PIN W23 [get_ports {gpo[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {gpo[1]}]


#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_data[0]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_data[1]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_data[2]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_data[3]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_data[4]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_data[5]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_data[6]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_data[7]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_data[8]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_data[9]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_data[10]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_data[11]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_data[12]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_data[13]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_data[14]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_data[15]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_data[0]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_data[1]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_data[2]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_data[3]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_data[4]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_data[5]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_data[6]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_data[7]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_data[8]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_data[9]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_data[10]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_data[11]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_data[12]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_data[13]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_data[14]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_data[15]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_data[0]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_data[1]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_data[2]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_data[3]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_data[4]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_data[5]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_data[6]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_data[7]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_data[8]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_data[9]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_data[10]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_data[11]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_data[12]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_data[13]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_data[14]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_data[15]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_data[0]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_data[1]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_data[2]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_data[3]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_data[4]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_data[5]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_data[6]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_data[7]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_data[8]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_data[9]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_data[10]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_data[11]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_data[12]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_data[13]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_data[14]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_data[15]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_override_disparity[0]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_override_disparity[1]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_override_disparity[2]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_override_disparity[3]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_k_char[0]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane0_k_char[1]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_k_char[0]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_k_char[1]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_override_disparity[0]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_override_disparity[1]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_override_disparity[2]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane1_override_disparity[3]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_override_disparity[0]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_override_disparity[1]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_override_disparity[2]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_override_disparity[3]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_k_char[0]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane2_k_char[1]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_override_disparity[0]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_override_disparity[1]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_override_disparity[2]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_override_disparity[3]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_k_char[0]}]
#set_property MARK_DEBUG true [get_nets {displayport_v4_0_tx_inst/inst/dport_link_inst/tx_lnk_tx_lane3_k_char[1]}]



connect_debug_port u_ila_1_0/probe2 [get_nets [list {dp_tx_vid_pixel2[0]} {dp_tx_vid_pixel2[1]} {dp_tx_vid_pixel2[2]} {dp_tx_vid_pixel2[3]} {dp_tx_vid_pixel2[4]} {dp_tx_vid_pixel2[5]} {dp_tx_vid_pixel2[6]} {dp_tx_vid_pixel2[7]} {dp_tx_vid_pixel2[8]} {dp_tx_vid_pixel2[9]} {dp_tx_vid_pixel2[10]} {dp_tx_vid_pixel2[11]} {dp_tx_vid_pixel2[12]} {dp_tx_vid_pixel2[13]} {dp_tx_vid_pixel2[14]} {dp_tx_vid_pixel2[15]} {dp_tx_vid_pixel2[16]} {dp_tx_vid_pixel2[17]} {dp_tx_vid_pixel2[18]} {dp_tx_vid_pixel2[19]} {dp_tx_vid_pixel2[20]} {dp_tx_vid_pixel2[21]} {dp_tx_vid_pixel2[22]} {dp_tx_vid_pixel2[23]} {dp_tx_vid_pixel2[24]} {dp_tx_vid_pixel2[25]} {dp_tx_vid_pixel2[26]} {dp_tx_vid_pixel2[27]} {dp_tx_vid_pixel2[28]} {dp_tx_vid_pixel2[29]} {dp_tx_vid_pixel2[30]} {dp_tx_vid_pixel2[31]} {dp_tx_vid_pixel2[32]} {dp_tx_vid_pixel2[33]} {dp_tx_vid_pixel2[34]} {dp_tx_vid_pixel2[35]} {dp_tx_vid_pixel2[36]} {dp_tx_vid_pixel2[37]} {dp_tx_vid_pixel2[38]} {dp_tx_vid_pixel2[39]} {dp_tx_vid_pixel2[40]} {dp_tx_vid_pixel2[41]} {dp_tx_vid_pixel2[42]} {dp_tx_vid_pixel2[43]} {dp_tx_vid_pixel2[44]} {dp_tx_vid_pixel2[45]} {dp_tx_vid_pixel2[46]} {dp_tx_vid_pixel2[47]}]]
connect_debug_port u_ila_1_0/probe3 [get_nets [list {dp_tx_vid_pixel3[0]} {dp_tx_vid_pixel3[1]} {dp_tx_vid_pixel3[2]} {dp_tx_vid_pixel3[3]} {dp_tx_vid_pixel3[4]} {dp_tx_vid_pixel3[5]} {dp_tx_vid_pixel3[6]} {dp_tx_vid_pixel3[7]} {dp_tx_vid_pixel3[8]} {dp_tx_vid_pixel3[9]} {dp_tx_vid_pixel3[10]} {dp_tx_vid_pixel3[11]} {dp_tx_vid_pixel3[12]} {dp_tx_vid_pixel3[13]} {dp_tx_vid_pixel3[14]} {dp_tx_vid_pixel3[15]} {dp_tx_vid_pixel3[16]} {dp_tx_vid_pixel3[17]} {dp_tx_vid_pixel3[18]} {dp_tx_vid_pixel3[19]} {dp_tx_vid_pixel3[20]} {dp_tx_vid_pixel3[21]} {dp_tx_vid_pixel3[22]} {dp_tx_vid_pixel3[23]} {dp_tx_vid_pixel3[24]} {dp_tx_vid_pixel3[25]} {dp_tx_vid_pixel3[26]} {dp_tx_vid_pixel3[27]} {dp_tx_vid_pixel3[28]} {dp_tx_vid_pixel3[29]} {dp_tx_vid_pixel3[30]} {dp_tx_vid_pixel3[31]} {dp_tx_vid_pixel3[32]} {dp_tx_vid_pixel3[33]} {dp_tx_vid_pixel3[34]} {dp_tx_vid_pixel3[35]} {dp_tx_vid_pixel3[36]} {dp_tx_vid_pixel3[37]} {dp_tx_vid_pixel3[38]} {dp_tx_vid_pixel3[39]} {dp_tx_vid_pixel3[40]} {dp_tx_vid_pixel3[41]} {dp_tx_vid_pixel3[42]} {dp_tx_vid_pixel3[43]} {dp_tx_vid_pixel3[44]} {dp_tx_vid_pixel3[45]} {dp_tx_vid_pixel3[46]} {dp_tx_vid_pixel3[47]}]]
connect_debug_port u_ila_1_0/probe6 [get_nets [list dp_tx_oddeven]]

connect_debug_port u_ila_0_0/probe1 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[3]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[4]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[5]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[6]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[7]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[8]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[9]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[10]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[11]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[12]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[13]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[14]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[15]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[16]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[17]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[18]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[19]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[20]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[21]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[22]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[23]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[24]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[25]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[26]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[27]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[28]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[29]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[30]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARADDR[31]}]]
connect_debug_port u_ila_0_0/probe2 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARBURST[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARBURST[1]}]]
connect_debug_port u_ila_0_0/probe3 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARCACHE[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARCACHE[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARCACHE[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARCACHE[3]}]]
connect_debug_port u_ila_0_0/probe4 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARLEN[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARLEN[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARLEN[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARLEN[3]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARLEN[4]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARLEN[5]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARLEN[6]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARLEN[7]}]]
connect_debug_port u_ila_0_0/probe5 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARPROT[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARPROT[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARPROT[2]}]]
connect_debug_port u_ila_0_0/probe6 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARQOS[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARQOS[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARQOS[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARQOS[3]}]]
connect_debug_port u_ila_0_0/probe7 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARREGION[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARREGION[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARREGION[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARREGION[3]}]]
connect_debug_port u_ila_0_0/probe8 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARSIZE[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARSIZE[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARSIZE[2]}]]
connect_debug_port u_ila_0_0/probe9 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[3]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[4]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[5]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[6]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[7]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[8]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[9]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[10]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[11]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[12]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[13]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[14]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[15]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[16]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[17]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[18]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[19]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[20]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[21]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[22]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[23]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[24]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[25]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[26]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[27]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[28]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[29]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[30]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWADDR[31]}]]
connect_debug_port u_ila_0_0/probe10 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWBURST[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWBURST[1]}]]
connect_debug_port u_ila_0_0/probe11 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWCACHE[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWCACHE[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWCACHE[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWCACHE[3]}]]
connect_debug_port u_ila_0_0/probe12 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWLEN[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWLEN[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWLEN[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWLEN[3]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWLEN[4]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWLEN[5]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWLEN[6]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWLEN[7]}]]
connect_debug_port u_ila_0_0/probe13 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWPROT[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWPROT[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWPROT[2]}]]
connect_debug_port u_ila_0_0/probe14 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWQOS[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWQOS[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWQOS[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWQOS[3]}]]
connect_debug_port u_ila_0_0/probe15 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWREGION[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWREGION[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWREGION[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWREGION[3]}]]
connect_debug_port u_ila_0_0/probe16 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWSIZE[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWSIZE[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWSIZE[2]}]]
connect_debug_port u_ila_0_0/probe17 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_BRESP[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_BRESP[1]}]]
connect_debug_port u_ila_0_0/probe18 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[3]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[4]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[5]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[6]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[7]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[8]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[9]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[10]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[11]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[12]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[13]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[14]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[15]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[16]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[17]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[18]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[19]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[20]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[21]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[22]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[23]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[24]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[25]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[26]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[27]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[28]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[29]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[30]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RDATA[31]}]]
connect_debug_port u_ila_0_0/probe19 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RRESP[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RRESP[1]}]]
connect_debug_port u_ila_0_0/probe20 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[3]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[4]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[5]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[6]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[7]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[8]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[9]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[10]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[11]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[12]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[13]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[14]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[15]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[16]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[17]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[18]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[19]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[20]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[21]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[22]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[23]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[24]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[25]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[26]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[27]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[28]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[29]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[30]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WDATA[31]}]]
connect_debug_port u_ila_0_0/probe21 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WSTRB[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WSTRB[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WSTRB[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WSTRB[3]}]]
connect_debug_port u_ila_0_0/probe22 [get_nets [list dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARLOCK]]
connect_debug_port u_ila_0_0/probe23 [get_nets [list dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARREADY]]
connect_debug_port u_ila_0_0/probe24 [get_nets [list dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_ARVALID]]
connect_debug_port u_ila_0_0/probe25 [get_nets [list dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWLOCK]]
connect_debug_port u_ila_0_0/probe26 [get_nets [list dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWREADY]]
connect_debug_port u_ila_0_0/probe27 [get_nets [list dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_AWVALID]]
connect_debug_port u_ila_0_0/probe28 [get_nets [list dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_BREADY]]
connect_debug_port u_ila_0_0/probe29 [get_nets [list dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_BVALID]]
connect_debug_port u_ila_0_0/probe30 [get_nets [list dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RLAST]]
connect_debug_port u_ila_0_0/probe31 [get_nets [list dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RREADY]]
connect_debug_port u_ila_0_0/probe32 [get_nets [list dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_RVALID]]
connect_debug_port u_ila_0_0/probe33 [get_nets [list dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WLAST]]
connect_debug_port u_ila_0_0/probe34 [get_nets [list dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WREADY]]
connect_debug_port u_ila_0_0/probe35 [get_nets [list dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M05_AXI_WVALID]]



connect_debug_port u_ila_0/probe27 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARBURST[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARBURST[1]}]]
connect_debug_port u_ila_0/probe28 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARCACHE[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARCACHE[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARCACHE[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARCACHE[3]}]]
connect_debug_port u_ila_0/probe29 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARLEN[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARLEN[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARLEN[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARLEN[3]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARLEN[4]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARLEN[5]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARLEN[6]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARLEN[7]}]]
connect_debug_port u_ila_0/probe31 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARQOS[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARQOS[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARQOS[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARQOS[3]}]]
connect_debug_port u_ila_0/probe32 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARREGION[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARREGION[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARREGION[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARREGION[3]}]]
connect_debug_port u_ila_0/probe33 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARSIZE[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARSIZE[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARSIZE[2]}]]
connect_debug_port u_ila_0/probe35 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWBURST[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWBURST[1]}]]
connect_debug_port u_ila_0/probe36 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWCACHE[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWCACHE[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWCACHE[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWCACHE[3]}]]
connect_debug_port u_ila_0/probe37 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWLEN[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWLEN[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWLEN[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWLEN[3]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWLEN[4]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWLEN[5]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWLEN[6]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWLEN[7]}]]
connect_debug_port u_ila_0/probe39 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWQOS[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWQOS[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWQOS[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWQOS[3]}]]
connect_debug_port u_ila_0/probe40 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWREGION[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWREGION[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWREGION[2]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWREGION[3]}]]
connect_debug_port u_ila_0/probe41 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWSIZE[0]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWSIZE[1]} {dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWSIZE[2]}]]
connect_debug_port u_ila_0/probe49 [get_nets [list dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_ARLOCK]]
connect_debug_port u_ila_0/probe52 [get_nets [list dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_AWLOCK]]
connect_debug_port u_ila_0/probe57 [get_nets [list dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_RLAST]]
connect_debug_port u_ila_0/probe60 [get_nets [list dp_policy_maker_inst/dp_system_i/axi_interconnect_1_M03_AXI_WLAST]]


connect_debug_port u_ila_1_0/probe1 [get_nets [list {dp_tx_vid_pixel1[0]} {dp_tx_vid_pixel1[1]} {dp_tx_vid_pixel1[2]} {dp_tx_vid_pixel1[3]} {dp_tx_vid_pixel1[4]} {dp_tx_vid_pixel1[5]} {dp_tx_vid_pixel1[6]} {dp_tx_vid_pixel1[7]} {dp_tx_vid_pixel1[8]} {dp_tx_vid_pixel1[9]} {dp_tx_vid_pixel1[10]} {dp_tx_vid_pixel1[11]} {dp_tx_vid_pixel1[12]} {dp_tx_vid_pixel1[13]} {dp_tx_vid_pixel1[14]} {dp_tx_vid_pixel1[15]} {dp_tx_vid_pixel1[16]} {dp_tx_vid_pixel1[17]} {dp_tx_vid_pixel1[18]} {dp_tx_vid_pixel1[19]} {dp_tx_vid_pixel1[20]} {dp_tx_vid_pixel1[21]} {dp_tx_vid_pixel1[22]} {dp_tx_vid_pixel1[23]} {dp_tx_vid_pixel1[24]} {dp_tx_vid_pixel1[25]} {dp_tx_vid_pixel1[26]} {dp_tx_vid_pixel1[27]} {dp_tx_vid_pixel1[28]} {dp_tx_vid_pixel1[29]} {dp_tx_vid_pixel1[30]} {dp_tx_vid_pixel1[31]} {dp_tx_vid_pixel1[32]} {dp_tx_vid_pixel1[33]} {dp_tx_vid_pixel1[34]} {dp_tx_vid_pixel1[35]} {dp_tx_vid_pixel1[36]} {dp_tx_vid_pixel1[37]} {dp_tx_vid_pixel1[38]} {dp_tx_vid_pixel1[39]} {dp_tx_vid_pixel1[40]} {dp_tx_vid_pixel1[41]} {dp_tx_vid_pixel1[42]} {dp_tx_vid_pixel1[43]} {dp_tx_vid_pixel1[44]} {dp_tx_vid_pixel1[45]} {dp_tx_vid_pixel1[46]} {dp_tx_vid_pixel1[47]}]]


create_debug_core u_ila_0 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
set_property ALL_PROBE_SAME_MU_CNT 2 [get_debug_cores u_ila_0]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
set_property C_DATA_DEPTH 16384 [get_debug_cores u_ila_0]
set_property C_EN_STRG_QUAL true [get_debug_cores u_ila_0]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
set_property port_width 1 [get_debug_ports u_ila_0/clk]
connect_debug_port u_ila_0/clk [get_nets [list clk_int_50]]
set_property port_width 5 [get_debug_ports u_ila_0/probe0]
connect_debug_port u_ila_0/probe0 [get_nets [list {dp_policy_maker_inst/dp_system_i/axi4_lite_peripherals/xlconcat_1_dout[0]} {dp_policy_maker_inst/dp_system_i/axi4_lite_peripherals/xlconcat_1_dout[1]} {dp_policy_maker_inst/dp_system_i/axi4_lite_peripherals/xlconcat_1_dout[2]} {dp_policy_maker_inst/dp_system_i/axi4_lite_peripherals/xlconcat_1_dout[3]} {dp_policy_maker_inst/dp_system_i/axi4_lite_peripherals/xlconcat_1_dout[4]}]]
create_debug_core u_ila_1_0 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_1_0]
set_property ALL_PROBE_SAME_MU_CNT 2 [get_debug_cores u_ila_1_0]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_1_0]
set_property C_DATA_DEPTH 16384 [get_debug_cores u_ila_1_0]
set_property C_EN_STRG_QUAL true [get_debug_cores u_ila_1_0]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_1_0]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_1_0]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_1_0]
set_property port_width 1 [get_debug_ports u_ila_1_0/clk]
connect_debug_port u_ila_1_0/clk [get_nets [list dp_tx_vid_clk]]
set_property port_width 3 [get_debug_ports u_ila_1_0/probe0]
connect_debug_port u_ila_1_0/probe0 [get_nets [list {dp_tx_vid_pixel0[15]} {dp_tx_vid_pixel0[31]} {dp_tx_vid_pixel0[47]}]]
create_debug_core u_ila_2_0 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_2_0]
set_property ALL_PROBE_SAME_MU_CNT 2 [get_debug_cores u_ila_2_0]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_2_0]
set_property C_DATA_DEPTH 16384 [get_debug_cores u_ila_2_0]
set_property C_EN_STRG_QUAL true [get_debug_cores u_ila_2_0]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_2_0]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_2_0]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_2_0]
set_property port_width 1 [get_debug_ports u_ila_2_0/clk]
connect_debug_port u_ila_2_0/clk [get_nets [list ext_aud_clk_BUFG]]
set_property port_width 32 [get_debug_ports u_ila_2_0/probe0]
connect_debug_port u_ila_2_0/probe0 [get_nets [list {s_axis_audio_ingress_tdata[0]} {s_axis_audio_ingress_tdata[1]} {s_axis_audio_ingress_tdata[2]} {s_axis_audio_ingress_tdata[3]} {s_axis_audio_ingress_tdata[4]} {s_axis_audio_ingress_tdata[5]} {s_axis_audio_ingress_tdata[6]} {s_axis_audio_ingress_tdata[7]} {s_axis_audio_ingress_tdata[8]} {s_axis_audio_ingress_tdata[9]} {s_axis_audio_ingress_tdata[10]} {s_axis_audio_ingress_tdata[11]} {s_axis_audio_ingress_tdata[12]} {s_axis_audio_ingress_tdata[13]} {s_axis_audio_ingress_tdata[14]} {s_axis_audio_ingress_tdata[15]} {s_axis_audio_ingress_tdata[16]} {s_axis_audio_ingress_tdata[17]} {s_axis_audio_ingress_tdata[18]} {s_axis_audio_ingress_tdata[19]} {s_axis_audio_ingress_tdata[20]} {s_axis_audio_ingress_tdata[21]} {s_axis_audio_ingress_tdata[22]} {s_axis_audio_ingress_tdata[23]} {s_axis_audio_ingress_tdata[24]} {s_axis_audio_ingress_tdata[25]} {s_axis_audio_ingress_tdata[26]} {s_axis_audio_ingress_tdata[27]} {s_axis_audio_ingress_tdata[28]} {s_axis_audio_ingress_tdata[29]} {s_axis_audio_ingress_tdata[30]} {s_axis_audio_ingress_tdata[31]}]]
create_debug_port u_ila_0 probe
set_property port_width 48 [get_debug_ports u_ila_0/probe1]
connect_debug_port u_ila_0/probe1 [get_nets [list {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[0]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[1]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[2]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[3]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[4]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[5]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[6]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[7]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[8]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[9]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[10]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[11]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[12]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[13]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[14]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[15]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[16]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[17]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[18]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[19]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[20]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[21]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[22]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[23]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[24]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[25]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[26]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[27]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[28]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[29]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[30]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[31]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[32]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[33]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[34]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[35]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[36]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[37]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[38]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[39]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[40]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[41]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[42]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[43]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[44]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[45]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[46]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL0[47]}]]
create_debug_port u_ila_0 probe
set_property port_width 48 [get_debug_ports u_ila_0/probe2]
connect_debug_port u_ila_0/probe2 [get_nets [list {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[0]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[1]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[2]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[3]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[4]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[5]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[6]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[7]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[8]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[9]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[10]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[11]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[12]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[13]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[14]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[15]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[16]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[17]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[18]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[19]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[20]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[21]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[22]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[23]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[24]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[25]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[26]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[27]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[28]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[29]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[30]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[31]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[32]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[33]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[34]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[35]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[36]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[37]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[38]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[39]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[40]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[41]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[42]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[43]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[44]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[45]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[46]} {dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_PIXEL1[47]}]]
create_debug_port u_ila_0 probe
set_property port_width 1 [get_debug_ports u_ila_0/probe3]
connect_debug_port u_ila_0/probe3 [get_nets [list dp_policy_maker_inst/dp_system_i/axi4_lite_peripherals_FCLK_RESET0_N]]
create_debug_port u_ila_1_0 probe
set_property port_width 1 [get_debug_ports u_ila_1_0/probe1]
connect_debug_port u_ila_1_0/probe1 [get_nets [list dp_tx_de]]
create_debug_port u_ila_1_0 probe
set_property port_width 1 [get_debug_ports u_ila_1_0/probe2]
connect_debug_port u_ila_1_0/probe2 [get_nets [list dp_tx_hsync]]
create_debug_port u_ila_1_0 probe
set_property port_width 1 [get_debug_ports u_ila_1_0/probe3]
connect_debug_port u_ila_1_0/probe3 [get_nets [list dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_CLK]]
create_debug_port u_ila_1_0 probe
set_property port_width 1 [get_debug_ports u_ila_1_0/probe4]
connect_debug_port u_ila_1_0/probe4 [get_nets [list dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_ENABLE]]
create_debug_port u_ila_1_0 probe
set_property port_width 1 [get_debug_ports u_ila_1_0/probe5]
connect_debug_port u_ila_1_0/probe5 [get_nets [list dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_HSYNC]]
create_debug_port u_ila_1_0 probe
set_property port_width 1 [get_debug_ports u_ila_1_0/probe6]
connect_debug_port u_ila_1_0/probe6 [get_nets [list dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_ODDEVEN]]
create_debug_port u_ila_1_0 probe
set_property port_width 1 [get_debug_ports u_ila_1_0/probe7]
connect_debug_port u_ila_1_0/probe7 [get_nets [list dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_RST]]
create_debug_port u_ila_1_0 probe
set_property port_width 1 [get_debug_ports u_ila_1_0/probe8]
connect_debug_port u_ila_1_0/probe8 [get_nets [list dp_policy_maker_inst/dp_system_i/dp_tx_vid_intf_str0_1_TX_VID_VSYNC]]
create_debug_port u_ila_1_0 probe
set_property port_width 1 [get_debug_ports u_ila_1_0/probe9]
connect_debug_port u_ila_1_0/probe9 [get_nets [list dp_tx_vsync]]
create_debug_port u_ila_1_0 probe
set_property port_width 1 [get_debug_ports u_ila_1_0/probe10]
connect_debug_port u_ila_1_0/probe10 [get_nets [list vid_clk_lckd]]
create_debug_port u_ila_2_0 probe
set_property port_width 3 [get_debug_ports u_ila_2_0/probe1]
connect_debug_port u_ila_2_0/probe1 [get_nets [list {s_axis_audio_ingress_tid[0]} {s_axis_audio_ingress_tid[1]} {s_axis_audio_ingress_tid[2]}]]
create_debug_port u_ila_2_0 probe
set_property port_width 1 [get_debug_ports u_ila_2_0/probe2]
connect_debug_port u_ila_2_0/probe2 [get_nets [list s_axis_audio_ingress_tready]]
create_debug_port u_ila_2_0 probe
set_property port_width 1 [get_debug_ports u_ila_2_0/probe3]
connect_debug_port u_ila_2_0/probe3 [get_nets [list s_axis_audio_ingress_tvalid]]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk_int_50]
