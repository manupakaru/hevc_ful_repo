#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define HAVE_REMOTE

#include <pcap.h>
#include <remote-ext.h>

#define PIC_H 1080
#define PIC_W 1920

const long cb_offset = PIC_H*PIC_W;
const long cr_offset = PIC_H*PIC_W + (PIC_H/2)*(PIC_W/2);
const long frame_offset = 2*PIC_H*PIC_W;

FILE* input_y_file;
FILE* input_cb_file;
FILE* input_cr_file;

FILE* input_file;
FILE* debug_file;

long y_counter = 0;
long cb_counter = 0;
long cr_counter = 0;
u_char counter = 0;

pcap_t *fp;
char errbuf[PCAP_ERRBUF_SIZE];
u_char packet[1024];
			 
u_char pic_buf [400000000];
	int count;

int rx_counter = 0;
int rx_id;
int tx_counter = 0;
int last_tx_counter = -1;

pcap_if_t *alldevs;
pcap_if_t *d;

//FILETIME t1;
//FILETIME t2;
SYSTEMTIME t1;
SYSTEMTIME t2;

pcap_pkthdr *header;
const u_char *rx_packet;
void packet_handler(u_char *param, const struct pcap_pkthdr *header, const u_char *pkt_data);
void send_yuv_packet(int, int);
void arrange_next_packet();

int bit_size=1;

const int SET_SIZE = 8192*2;

void main(int argc, char **argv)
{

	//input_file = fopen("timelapse_QP34_cropped.hevc","rb");
	//input_file = fopen("axi_trans_timelapse","rb");
	input_file = fopen("4k_hobbit_12.hevc","rb");
	//input_file = fopen("hobbit1080.hevc","rb");
	//input_file = fopen("axi_trans_hobbit","rb");
	debug_file = fopen("debug.hex","wb");
	//input_file = fopen("axi_trans_scapes_ld","rb");
	//input_file = fopen("axi_trans_scapes_intra","rb");
	//input_file = fopen("axi_trans_nscapes_ra","rb");
	//input_file = fopen("axi_trans_nhobbit","rb");
	//input_file = fopen("axi_trans_nscapes_ai","rb");
	


	    /* Retrieve the device list from the local machine */
    if (pcap_findalldevs_ex(PCAP_SRC_IF_STRING, NULL /* auth is not needed */, &alldevs, errbuf) == -1)
    {
        fprintf(stderr,"Error in pcap_findalldevs_ex: %s\n", errbuf);
        exit(1);
    }
    
    /* Print the list */
	int i=0;
    for(d= alldevs; d != NULL; d= d->next)
    {
        printf("%d. %s", ++i, d->name);
        if (d->description)
            printf(" (%s)\n", d->description);
        else
            printf(" (No description available)\n");
    }

	/* Check the validity of the command line */
    //if (argc != 2)
    //{
    //    printf("usage: %s interface (e.g. 'rpcap://eth0')", argv[0]);
    //    return;
    //}

	d = alldevs;
	//d = d->next;
    
    /* Open the output device */
	if ( (fp= pcap_open(d->name,            // name of the device
                        100,                // portion of the packet to capture (only the first 100 bytes)
                        PCAP_OPENFLAG_PROMISCUOUS,  // promiscuous mode
                        1000,               // read timeout
                        NULL,               // authentication on the remote machine
                        errbuf              // error buffer
                        ) ) == NULL)
    {
        fprintf(stderr,"\nUnable to open the adapter. %s is not supported by WinPcap\n", argv[1]);
        return;
    }
	printf("opened %s\n",d->description);

	struct bpf_program comp_filt;		/* The compiled filter expression */
	char filter_exp[] = "ether src aa:aa:aa:aa:aa:aa";	/* The filter expression */
	if (pcap_compile(fp, &comp_filt, filter_exp, 0, NULL) == -1) {
		 fprintf(stderr, "Couldn't parse filter %s: %s\n", filter_exp, pcap_geterr(fp));
		 return;
	 }
	 if (pcap_setfilter(fp, &comp_filt) == -1) {
		 fprintf(stderr, "Couldn't install filter %s: %s\n", filter_exp, pcap_geterr(fp));
		 return;
	 }

    GetLocalTime(&t1);

	int frame_start = 74; //56026886
	int frame_size = 300000000-frame_start;//72316055

	bit_size = 74 + (frame_size-1);
	fread(pic_buf,1,74,input_file);
	fseek(input_file,frame_start,0);
	fread(pic_buf+74,1,frame_size,input_file);
	printf("%x\n",pic_buf[frame_start+frame_size-1]);
	fwrite(pic_buf,1,100000,debug_file);
	fclose(debug_file);
	//send_yuv_packet();
	int set =0;
	while (1) {
		
		while(1) {
			int ret = pcap_next_ex(fp,&header,&rx_packet);
			if (ret==1) printf("Header %d\n",header->len);
			if(ret ==1 && header->len >= 18 && rx_packet[16]==100 && rx_packet[17] ==200) {
				int ret = pcap_next_ex(fp,&header,&rx_packet); // account for the duplicate
				int tx_counter_temp = rx_packet[14]*256+rx_packet[15]*SET_SIZE;
				set = rx_packet[14]*256+rx_packet[15];
				//set = set % 10;
				if (tx_counter_temp != tx_counter) {
					printf("tx counter mismatch %d %d\n",tx_counter, tx_counter_temp);
				}
				tx_counter = tx_counter_temp;
				if (tx_counter==0) set = 0;
				break;	
			}
		}
	
		
		//fseek(input_file,frame_start,0);
	
		int ii;
		for (ii=0;ii<SET_SIZE;ii++) {
			//printf("Tx packet %d \n",ii);
			send_yuv_packet(set , ii);
			//Sleep(2);
		}
		printf("Set %d done\n",set);
		set++;
		//while(1);
	}
	while(1);
	
	int sleep_counter = 0;
	
    return;
}


void send_yuv_packet(int set, int frame) {
	
	int i=0;
	///* Supposing to be on ethernet, set mac destination to 1:1:1:1:1:1 */
    for (i=0;i<6;i++) {
		packet[i] =  0xaa;
		packet[6+i] =  0xbb;
	}
	packet[12] = 0xdd;
	packet[13] = 0xdd;
	packet[14] = frame /256;
	packet[15] = frame %256;
    
    /* Fill the rest of the packet */
	tx_counter = set*SET_SIZE + frame;
	for(i = 0; i < 1000 ; i++) {
		//if (ftell(input_file)==70990) 
		//if (feof(input_file))
			//fseek(input_file,SEEK_SET,SEEK_SET);
		packet[16+i] = pic_buf[ ( (tx_counter)*1000+i)];  //workin 5836355
	}  
	if (frame==0) {
		for (i=16;i<32;i++) {
			printf("%02x",packet[i]);
			if (i%4==3) printf(" ");
		}
		printf("\n");
	}
	++tx_counter;
	last_tx_counter = tx_counter;

	if (pcap_sendpacket(fp, packet, (1024) /* size */) != 0)
    {
        fprintf(stderr,"\nError sending the packet: \n", pcap_geterr(fp));
        return;
    }
	//t1 = time(NULL);
	//GetSystemTimeAsFileTime(&t1);

	//arrange_next_packet();

}