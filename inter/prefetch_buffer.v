`timescale 1ns / 1ps
module prefetch_buffer(
    clk,
    reset,
    enable,
	collocated_dpb_idx,
    axi_col_mv_frame_offet_in,
    is_new_pic_col_ref_idx,        // set this for a single cycle whenever there is a new col_ref_idx or a change in picture
    current_dpb_idx,    // expect this to come before ref_pic_data

    from_top_ref_pic_list_poc_data_in,
    from_top_ref_pic_list_poc_wr_en,
    from_top_ref_pic_list0_poc_addr,
    from_top_ref_pic_list1_poc_addr,
    
    ref_idx_l0_col,
    ref_idx_l0_col_poc_out,
    ref_idx_l1_col,
    ref_idx_l1_col_poc_out,
    
    col_x_addr,
    col_y_addr,
    
    col_mv_field_pred_flag_l0_out,
    col_mv_field_pred_flag_l1_out,
    col_mv_field_ref_idx_l0_out,
    col_mv_field_ref_idx_l1_out,
    col_mv_field_mv_x_l0_out,
    col_mv_field_mv_x_l1_out,
    col_mv_field_mv_y_l0_out,
    col_mv_field_mv_y_l1_out,
    
    mv_pref_axi_araddr      ,
    mv_pref_axi_arlen       ,
    mv_pref_axi_arsize      ,
    mv_pref_axi_arburst     ,
    mv_pref_axi_arprot      ,
    mv_pref_axi_arvalid     ,
    mv_pref_axi_arready     ,
    
    mv_pref_axi_rdata       ,
    mv_pref_axi_rresp       ,
    mv_pref_axi_rlast       ,
    mv_pref_axi_rvalid      ,
    mv_pref_axi_rready      ,
    
    mv_pref_axi_arlock      ,
    mv_pref_axi_arid        ,
    mv_pref_axi_arcache     ,
    
    x_ctb_addr, // feed x_ctb_addr and y_ctb_addr 1 ctb ahead if new tile dectected wait 2 cycles before sending thie new_ctu_start_out signal
    y_ctb_addr,
    new_ctu_start_in,
    axi_ctu_read_done_out
    
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"
    `include "../sim/inter_axi_def.v" // always define below pred_def
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                   MV_COL_AXI_AX_SIZE  = `AX_SIZE_64;
    parameter                   MV_COL_AXI_AX_LEN   = `AX_LEN_5;
 

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                          STATE_AXI_READ_IDLE = 0;
    localparam                          STATE_AXI_READ_ADDRESS_WAIT = 2'd1;
    localparam                          STATE_AXI_READ_DATA_READ = 2'd2;
    localparam                          STATE_AXI_READ_ADDRESS = 2'd3;

    localparam  PIC_LIST_ADDR_OFFSET = 16;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
    input enable;
    input                                           is_new_pic_col_ref_idx;
    input   [DPB_ADDR_WIDTH -1: 0]                  current_dpb_idx;
    input   [DPB_ADDR_WIDTH -1: 0]                  collocated_dpb_idx;
	input   [AXI_ADDR_WDTH -1:0] 					axi_col_mv_frame_offet_in;
    
    input       [DPB_ADDR_WIDTH -1: 0]                      ref_idx_l0_col;
    output      [REF_PIC_LIST_POC_DATA_WIDTH -1: 0]         ref_idx_l0_col_poc_out;
    input       [DPB_ADDR_WIDTH -1: 0]                      ref_idx_l1_col;
    output      [REF_PIC_LIST_POC_DATA_WIDTH -1: 0]         ref_idx_l1_col_poc_out;
    
    input [REF_PIC_LIST_POC_DATA_WIDTH-1:0]     from_top_ref_pic_list_poc_data_in;
    input                                       from_top_ref_pic_list_poc_wr_en  ;
    input [NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]     from_top_ref_pic_list0_poc_addr  ;
    input [NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]     from_top_ref_pic_list1_poc_addr  ;

    
    output	reg [AXI_ADDR_WDTH-1:0]		            mv_pref_axi_araddr  ;
    output wire [7:0]					            mv_pref_axi_arlen   ;
    output wire	[2:0]					            mv_pref_axi_arsize  ;
    output wire [1:0]					            mv_pref_axi_arburst ;
    output wire [2:0]					            mv_pref_axi_arprot  ;
    output reg							            mv_pref_axi_arvalid ;
    input 								            mv_pref_axi_arready ;
                
    input		[MV_COL_AXI_DATA_WIDTH-1:0]		    mv_pref_axi_rdata   ;
    input		[1:0]					            mv_pref_axi_rresp   ;
    input 								            mv_pref_axi_rlast   ;
    input								            mv_pref_axi_rvalid  ;
    output 						                    mv_pref_axi_rready  ;
    
    output                        	                mv_pref_axi_arlock  ;
	output                                          mv_pref_axi_arid    ;    
    output      [3:0]                               mv_pref_axi_arcache ;    
    
    input    [X11_ADDR_WDTH - CTB_SIZE_WIDTH -1:0]  x_ctb_addr;
    input    [X11_ADDR_WDTH - CTB_SIZE_WIDTH -1:0]  y_ctb_addr;
    input                                           new_ctu_start_in;
    
    input     [(CTB_SIZE_WIDTH - LOG2_MIN_COLLOCATE_SIZE) -1 + 1:0]   col_x_addr;
    input     [(CTB_SIZE_WIDTH - LOG2_MIN_COLLOCATE_SIZE) -1	:0]   col_y_addr;
    
    output                           col_mv_field_pred_flag_l0_out;
    output                           col_mv_field_pred_flag_l1_out;
    output [REF_IDX_LX_WIDTH -1:0]   col_mv_field_ref_idx_l0_out;
    output [REF_IDX_LX_WIDTH -1:0]   col_mv_field_ref_idx_l1_out;
    output [MVD_WIDTH -1:0]          col_mv_field_mv_x_l0_out;
    output [MVD_WIDTH -1:0]          col_mv_field_mv_y_l0_out;
    output [MVD_WIDTH -1:0]          col_mv_field_mv_x_l1_out;
    output [MVD_WIDTH -1:0]          col_mv_field_mv_y_l1_out;
    
//-----------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    reg [2:0]             state_axi_read;       // why not FSM inferred??
    
    
    
    reg [31:0] axi_ctu_row_offset;
    reg [31:0] axi_ctu_offset;    
    reg [3-1:0] mv_row_offset;
    
    reg [DPB_ADDR_WIDTH -1: 0]                  collocated_dpb_idx_d;
    
    output reg                                  axi_ctu_read_done_out;
    reg                                         pref_buf_wr_en;
    
    reg [4*MV_FIELD_AXI_DATA_WIDTH -1: 0]       pref_buf_w_data;
    wire [MV_FIELD_AXI_DATA_WIDTH -1: 0]         col_mv_field_r_data_out;

    // reg                                          new_ctu_start_in_d;
    // reg                                          new_ctu_start_in_2d;    

    wire    [2*NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]      dpb_idx_ref_poc_l0_w_addr = {current_dpb_idx,from_top_ref_pic_list0_poc_addr};    // expect current_dpb_idx to be ready when ref_pic_list is written
    wire    [2*NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]      dpb_idx_ref_poc_l0_r_addr = {collocated_dpb_idx,ref_idx_l0_col};
    wire    [2*NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]      dpb_idx_ref_poc_l1_w_addr = {current_dpb_idx,from_top_ref_pic_list1_poc_addr};    
    wire    [2*NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]      dpb_idx_ref_poc_l1_r_addr = {collocated_dpb_idx,ref_idx_l1_col};
    
    wire [4:0] col_mv_buffer_w_addr = (col_y_addr + col_x_addr *4); 
    wire [MV_FIELD_AXI_DATA_WIDTH -MV_FIELD_DATA_WIDTH -1:0] garbage;
    reg [MV_FIELD_AXI_DATA_WIDTH -MV_FIELD_DATA_WIDTH -1:0] garbage_d;
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

col_pref_mv_buffer_mem col_pref_ctu_mem (
    .clk(clk), 
    .r_addr_in(col_mv_buffer_w_addr), 
    .w_addr_in(mv_row_offset), 
    .r_data_out(col_mv_field_r_data_out), 
    .w_data_in(pref_buf_w_data), 
    .w_en_in(pref_buf_wr_en), 
    .enable(enable)
    );

d_sync_ram  #(
    .DATA_WIDTH(REF_PIC_LIST_POC_DATA_WIDTH),
    .ADDR_WIDTH(NUM_REF_IDX_L0_MINUS1_WIDTH*2),
    .DATA_DEPTH(1<<(NUM_REF_IDX_L0_MINUS1_WIDTH*2))
) dpb_idx_ref_poc_list0_mem
(
    .clk(clk), 
    .r_addr_in(dpb_idx_ref_poc_l0_r_addr), 
    .w_addr_in(dpb_idx_ref_poc_l0_w_addr),
    .r_data_out(ref_idx_l0_col_poc_out), 
    .w_data_in(from_top_ref_pic_list_poc_data_in), 
    .w_en_in(from_top_ref_pic_list_poc_wr_en), 
    .enable(enable)
);


d_sync_ram  #(
    .DATA_WIDTH(REF_PIC_LIST_POC_DATA_WIDTH),
    .ADDR_WIDTH(NUM_REF_IDX_L0_MINUS1_WIDTH*2),
    .DATA_DEPTH(1<<(NUM_REF_IDX_L0_MINUS1_WIDTH*2))
) dpb_idx_ref_poc_list1_mem
(
    .clk(clk), 
    .r_addr_in(dpb_idx_ref_poc_l1_r_addr), 
    .w_addr_in(dpb_idx_ref_poc_l1_w_addr),
    .r_data_out(ref_idx_l1_col_poc_out), 
    .w_data_in(from_top_ref_pic_list_poc_data_in), 
    .w_en_in(from_top_ref_pic_list_poc_wr_en), 
    .enable(enable)
);


    assign                  mv_pref_axi_arid     = 0;
    assign                  mv_pref_axi_arlen    = MV_COL_AXI_AX_LEN;
    assign                  mv_pref_axi_arsize   = MV_COL_AXI_AX_SIZE;
    assign                  mv_pref_axi_arburst  = `AX_BURST_INC;
    assign    	            mv_pref_axi_arlock   = `AX_LOCK_DEFAULT;
    assign                  mv_pref_axi_arcache  = `AX_CACHE_DEFAULT;
    assign                  mv_pref_axi_arprot   = `AX_PROT_DATA;    
    assign                  mv_pref_axi_rready = 1;
    
    
    assign {
        
        col_mv_field_pred_flag_l0_out,
        col_mv_field_pred_flag_l1_out,
        col_mv_field_ref_idx_l0_out,
        col_mv_field_ref_idx_l1_out,
        col_mv_field_mv_x_l0_out,
        col_mv_field_mv_y_l0_out,
        col_mv_field_mv_x_l1_out,
        col_mv_field_mv_y_l1_out,
        garbage
                                    } = col_mv_field_r_data_out;//[MV_FIELD_DATA_WIDTH-1:0];


    // always@(posedge clk) begin
        // if(garbage_d !=0 || is_new_pic_col_ref_idx) begin
            // axi_col_mv_frame_offet <= `COL_MV_ADDR_OFFSET + ((`COL_MV_FRAME_OFFSET*collocated_dpb_idx));//%(1<<31));
        // end
        // garbage_d <= garbage;
    // end

    always@(posedge clk) begin
		// new_ctu_start_in_d <= new_ctu_start_in;
		axi_ctu_row_offset <= (axi_col_mv_frame_offet_in + `COL_MV_CTU_ROW_OFFSET* y_ctb_addr);
    end

    
    
    always@(posedge clk) begin
        if(reset) begin
            axi_ctu_read_done_out <= 1;
            pref_buf_wr_en <= 0;
            state_axi_read <= STATE_AXI_READ_IDLE;
            mv_pref_axi_araddr <= {AXI_ADDR_WDTH{1'b0}};
            mv_pref_axi_arvalid <= 0;
        end
        else if(enable) begin
            case(state_axi_read)
                STATE_AXI_READ_IDLE: begin
                    pref_buf_wr_en <= 0;
                    if(new_ctu_start_in) begin
                        axi_ctu_read_done_out <= 0;
                        state_axi_read <= STATE_AXI_READ_ADDRESS;
                        pref_buf_wr_en <= 0;
                    end
                    else begin
                        axi_ctu_read_done_out <= 1;
                        mv_pref_axi_arvalid <= 0;
                    end
                end
				STATE_AXI_READ_ADDRESS: begin
					mv_pref_axi_araddr <= (axi_ctu_row_offset + `COL_MV_CTU_OFFSET * x_ctb_addr);//%(1<<(31));
					mv_pref_axi_arvalid <= 1;
					state_axi_read <= STATE_AXI_READ_ADDRESS_WAIT;
				end
                STATE_AXI_READ_ADDRESS_WAIT: begin
                    if(mv_pref_axi_arready) begin
                        mv_pref_axi_arvalid <= 0;
                        state_axi_read <= STATE_AXI_READ_DATA_READ;
                        mv_row_offset <= 3'b111; // set addr so that when incremented by one reaches zero
                    end
                end
                STATE_AXI_READ_DATA_READ: begin
                    if(mv_pref_axi_rvalid) begin
                        mv_row_offset <= (mv_row_offset + 1'b1);
                        pref_buf_w_data     <= mv_pref_axi_rdata[4*MV_FIELD_AXI_DATA_WIDTH-1:0];
                        if(mv_pref_axi_rresp == `XRESP_SLAV_ERROR) begin
                            mv_pref_axi_arvalid <= 1;
                            state_axi_read <= STATE_AXI_READ_ADDRESS_WAIT;
                            pref_buf_wr_en <= 0;
                        end
                        else begin
                            pref_buf_wr_en <= 1;
                            if(mv_pref_axi_rlast) begin
                                state_axi_read <= STATE_AXI_READ_IDLE;
                            end
                        end
                    end
                    else begin
                        pref_buf_wr_en <= 0;
                    end
                end
            endcase
        end
    end
	

endmodule