`timescale 1ns / 1ps
module bs_core(
    clk,
    reset,

    intra_tu_4by4_valid_in  ,
    x0_tu_in_min_tus_in,
    y0_tu_in_min_tus_in,

    top_row_mvs_in          ,
    left_row_mvs_in         , 
    bs_left_wr_en_in        , 
    bs_top_wr_en_in         ,
    
    cand0_mv_field_mv_x_l0 ,
    cand0_mv_field_mv_y_l0 ,
    cand0_mv_field_mv_x_l1 ,
    cand0_mv_field_mv_y_l1 ,   
    cand0_dpb_idx_l0       ,    
    cand0_dpb_idx_l1       ,  
    
    cand1_mv_field_mv_x_l0 ,
    cand1_mv_field_mv_y_l0 ,
    cand1_mv_field_mv_x_l1 ,
    cand1_mv_field_mv_y_l1 ,   
    cand1_dpb_idx_l0       ,    
    cand1_dpb_idx_l1       ,  

    cand2_mv_field_mv_x_l0 ,
    cand2_mv_field_mv_y_l0 ,
    cand2_mv_field_mv_x_l1 ,
    cand2_mv_field_mv_y_l1 ,   
    cand2_dpb_idx_l0       ,    
    cand2_dpb_idx_l1       ,      
    
    
    cand0_mv_field_pred_flag_l0 ,
    cand0_mv_field_pred_flag_l1 ,
    
    cand1_mv_field_pred_flag_l0 ,
    cand1_mv_field_pred_flag_l1 ,
    
    cand2_mv_field_pred_flag_l0 ,
    cand2_mv_field_pred_flag_l1 ,

    cand_mv_cache_valid     ,


    q_mv_field_pred_flagl0,
    q_mv_field_pred_flagl1,
    q_mv_field_mv_x_l0    ,
    q_mv_field_mv_y_l0    ,
    q_mv_field_mv_x_l1    ,
    q_mv_field_mv_y_l1    ,
    q_dpb_idx_l0          ,
    q_dpb_idx_l1          ,
    
    // is_cand_mv_in_range0    ,
    is_cand_mv_in_range1    ,
    is_cand_mv_in_range2    ,
    is_cand_mv_in_range3    ,
    
    // xx_pb0_strt_min_tus_in  ,
    xx_pb1_strt_min_tus_in  ,
    xx_pb2_strt_min_tus_in  ,
    xx_pb3_strt_min_tus_in  ,


    // yy_pb0_strt_min_tus_in  ,
    yy_pb1_strt_min_tus_in  ,
    yy_pb2_strt_min_tus_in  ,
    yy_pb3_strt_min_tus_in  ,

    xT_in_min_tus_in        ,
    yT_in_min_tus_in        ,
    
    cb_yy_min_tus_in        ,
    cb_xx_min_tus_in        ,    

    res_present_in          ,
    res_present_pu_range_4by4_valid       ,
    
    bs_fifo_data_out        ,
    bs_fifo_wr_en_out       ,
    bs_fifo_full_in          // should ensure this line never asserted
    
    
    
    
    
    
    
    
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam BS_STATE_RES_PRES_READ   = 0;
    localparam BS_STATE_MV_READ_TU_4    = 1;
    localparam BS_STATE_MV_READ_TU_NOT_4       = 2;
    localparam BS_STATE_MV_COMPARE       = 3;
    localparam BS_INTRA_STATE_1       = 4;
    localparam BS_INTRA_STATE_2       = 5;
    localparam BS_INTRA_STATE_3       = 6;
    localparam BS_STATE_MV_BS_WRITE_1       = 7;
    localparam BS_STATE_MV_BS_WRITE_2       = 8;
    localparam BS_STATE_MV_BS_WRITE_3       = 9;
    localparam BS_STATE_MV_READ_1           = 10;
    localparam BS_STATE_MV_READ_2           = 11;
    localparam BS_STATE_MV_READ_3           = 12;
    localparam BS_STATE_PU_RANGE_READ_2           = 13;
    localparam BS_STATE_PU_RANGE_READ_3           = 14;

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;

    input intra_tu_4by4_valid_in;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_in_min_tus_in;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_in_min_tus_in;
    
    input signed [MVD_WIDTH -1:0]          cand0_mv_field_mv_x_l0 ;
    input signed [MVD_WIDTH -1:0]          cand0_mv_field_mv_y_l0 ;
    input signed [MVD_WIDTH -1:0]          cand0_mv_field_mv_x_l1 ;
    input signed [MVD_WIDTH -1:0]          cand0_mv_field_mv_y_l1 ;   
    input [DPB_ADDR_WIDTH -1:0]            cand0_dpb_idx_l0       ;    
    input [DPB_ADDR_WIDTH -1:0]            cand0_dpb_idx_l1       ; 
    
    input signed [MVD_WIDTH -1:0]          cand1_mv_field_mv_x_l0 ;
    input signed [MVD_WIDTH -1:0]          cand1_mv_field_mv_y_l0 ;
    input signed [MVD_WIDTH -1:0]          cand1_mv_field_mv_x_l1 ;
    input signed [MVD_WIDTH -1:0]          cand1_mv_field_mv_y_l1 ;   
    input [DPB_ADDR_WIDTH -1:0]            cand1_dpb_idx_l0       ;    
    input [DPB_ADDR_WIDTH -1:0]            cand1_dpb_idx_l1       ;  
    
    input signed [MVD_WIDTH -1:0]          cand2_mv_field_mv_x_l0 ;
    input signed [MVD_WIDTH -1:0]          cand2_mv_field_mv_y_l0 ;
    input signed [MVD_WIDTH -1:0]          cand2_mv_field_mv_x_l1 ;
    input signed [MVD_WIDTH -1:0]          cand2_mv_field_mv_y_l1 ;   
    input [DPB_ADDR_WIDTH -1:0]            cand2_dpb_idx_l0       ;    
    input [DPB_ADDR_WIDTH -1:0]            cand2_dpb_idx_l1       ;   
    
    
    input                                   cand0_mv_field_pred_flag_l0 ;
    input                                   cand0_mv_field_pred_flag_l1 ;

    input                                   cand1_mv_field_pred_flag_l0 ;
    input                                   cand1_mv_field_pred_flag_l1 ;

    input                                   cand2_mv_field_pred_flag_l0 ;
    input                                   cand2_mv_field_pred_flag_l1 ;

    input                                  cand_mv_cache_valid    ;
    
    // input                                   is_cand_mv_in_range0    ;
    input                                   is_cand_mv_in_range1    ;
    input                                   is_cand_mv_in_range2    ;
    input                                   is_cand_mv_in_range3    ;

    // input  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]         xx_pb0_strt_min_tus_in;
    // input  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]         yy_pb0_strt_min_tus_in;

    input  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]         xx_pb1_strt_min_tus_in;
    input  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]         yy_pb1_strt_min_tus_in;

    input  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]         xx_pb2_strt_min_tus_in;
    input  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]         yy_pb2_strt_min_tus_in;

    input  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]         xx_pb3_strt_min_tus_in;
    input  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]         yy_pb3_strt_min_tus_in;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]          xT_in_min_tus_in;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]          yT_in_min_tus_in;
    
    input res_present_in          ;
    input res_present_pu_range_4by4_valid       ;

    output [BS_FIFO_WIDTH-1:0]   bs_fifo_data_out        ;
    output reg                  bs_fifo_wr_en_out       ;
    input                       bs_fifo_full_in         ; // should ensure this line never asserted    
    
    
    input [MV_FIELD_DATA_WIDTH -1:0] top_row_mvs_in       ;
    input [MV_FIELD_DATA_WIDTH -1:0] left_row_mvs_in      ; 
    input bs_left_wr_en_in                                ; 
    input bs_top_wr_en_in                                 ;
    
    input  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] cb_yy_min_tus_in;
    input  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] cb_xx_min_tus_in;

    input                                  q_mv_field_pred_flagl0   ;
    input                                  q_mv_field_pred_flagl1   ;
    input signed [MVD_WIDTH -1:0]          q_mv_field_mv_x_l0       ;
    input signed [MVD_WIDTH -1:0]          q_mv_field_mv_y_l0       ;
    input signed [MVD_WIDTH -1:0]          q_mv_field_mv_x_l1       ;
    input signed [MVD_WIDTH -1:0]          q_mv_field_mv_y_l1       ;   
    input [DPB_ADDR_WIDTH -1:0]            q_dpb_idx_l0             ;    
    input [DPB_ADDR_WIDTH -1:0]            q_dpb_idx_l1             ;  

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

    
    integer bs_state;
    
    reg [1:0] bs_v1;
    reg [1:0] bs_v2;
    reg [1:0] bs_h1;
    reg [1:0] bs_h2;
    
    
    

    
    
    reg                                  p_mv_field_pred_flagl0;
    reg                                  p_mv_field_pred_flagl1;
    reg signed [MVD_WIDTH -1:0]          p_mv_field_mv_x_l0 ;
    reg signed [MVD_WIDTH -1:0]          p_mv_field_mv_y_l0 ;
    reg signed [MVD_WIDTH -1:0]          p_mv_field_mv_x_l1 ;
    reg signed [MVD_WIDTH -1:0]          p_mv_field_mv_y_l1 ;   
    reg [DPB_ADDR_WIDTH -1:0]            p_dpb_idx_l0       ;    
    reg [DPB_ADDR_WIDTH -1:0]            p_dpb_idx_l1       ;  
    
    
    reg bs_v_same_mv;
    reg bs_h_same_mv;
    
    reg  is_num_mv_diff;
    wire is_mv_p_bi_pred;
    
    wire is_ref_pic_p_l0_l1_diff;


    
    reg signed [MVD_WIDTH -1:0] mv_p_l0_q_l0_x_diff;
    reg signed [MVD_WIDTH -1:0] mv_p_l0_q_l1_x_diff;
    reg signed [MVD_WIDTH -1:0] mv_p_l1_q_l0_x_diff;
    reg signed [MVD_WIDTH -1:0] mv_p_l1_q_l1_x_diff;
    
    reg signed [MVD_WIDTH -1:0] mv_p_l0_q_l0_x_diff_minus;
    reg signed [MVD_WIDTH -1:0] mv_p_l0_q_l1_x_diff_minus;
    reg signed [MVD_WIDTH -1:0] mv_p_l1_q_l0_x_diff_minus;
    reg signed [MVD_WIDTH -1:0] mv_p_l1_q_l1_x_diff_minus;
    
    
    reg signed [MVD_WIDTH -1:0] mv_p_l0_q_l0_y_diff;
    reg signed [MVD_WIDTH -1:0] mv_p_l0_q_l1_y_diff;
    reg signed [MVD_WIDTH -1:0] mv_p_l1_q_l0_y_diff;
    reg signed [MVD_WIDTH -1:0] mv_p_l1_q_l1_y_diff;
    
    reg signed [MVD_WIDTH -1:0] mv_p_l0_q_l0_y_diff_minus;
    reg signed [MVD_WIDTH -1:0] mv_p_l0_q_l1_y_diff_minus;
    reg signed [MVD_WIDTH -1:0] mv_p_l1_q_l0_y_diff_minus;
    reg signed [MVD_WIDTH -1:0] mv_p_l1_q_l1_y_diff_minus;
    
    
    reg is_mv_p_l0_q_l0_x_abs_diff_great_4;
    reg is_mv_p_l0_q_l1_x_abs_diff_great_4;
    reg is_mv_p_l1_q_l0_x_abs_diff_great_4;
    reg is_mv_p_l1_q_l1_x_abs_diff_great_4;

    reg is_mv_p_l0_q_l0_y_abs_diff_great_4;
    reg is_mv_p_l0_q_l1_y_abs_diff_great_4;
    reg is_mv_p_l1_q_l0_y_abs_diff_great_4;
    reg is_mv_p_l1_q_l1_y_abs_diff_great_4;
    
    
    wire is_top_mvs_empty;
    wire is_top_mvs_full;
    wire is_left_mvs_empty;
    wire is_left_mvs_full;
    
    wire  [MV_FIELD_DATA_WIDTH -1:0] top_row_mvs_fifo_out   ;
    wire  [MV_FIELD_DATA_WIDTH -1:0] left_row_mvs_fifo_out  ; 


    wire res_top_pres_out;
    wire res_lef_pres_out;
    
    reg res_pres_top_wr_en;
    reg res_pres_lef_wr_en;

    reg current_res_present;



    reg bs_left_rd_en_in                                ; 
    reg bs_top_rd_en_in                                 ;


    wire                                  top_mv_field_pred_flag_l0;
    wire                                  top_mv_field_pred_flag_l1;
    wire [REF_IDX_LX_WIDTH -1:0]          top_mv_field_ref_idx_l0;
    wire [REF_IDX_LX_WIDTH -1:0]          top_mv_field_ref_idx_l1;
    wire signed [MVD_WIDTH -1:0]          top_mv_field_mv_x_l0;
    wire signed [MVD_WIDTH -1:0]          top_mv_field_mv_y_l0;
    wire signed [MVD_WIDTH -1:0]          top_mv_field_mv_x_l1;
    wire signed [MVD_WIDTH -1:0]          top_mv_field_mv_y_l1;    

    wire                                  left_mv_field_pred_flag_l0;
    wire                                  left_mv_field_pred_flag_l1;
    wire [REF_IDX_LX_WIDTH -1:0]          left_mv_field_ref_idx_l0;
    wire [REF_IDX_LX_WIDTH -1:0]          left_mv_field_ref_idx_l1;
    wire signed [MVD_WIDTH -1:0]          left_mv_field_mv_x_l0;
    wire signed [MVD_WIDTH -1:0]          left_mv_field_mv_y_l0;
    wire signed [MVD_WIDTH -1:0]          left_mv_field_mv_x_l1;
    wire signed [MVD_WIDTH -1:0]          left_mv_field_mv_y_l1;    

    wire is_top_mv_field_intra, is_left_mv_field_intra;

    reg p_mv_available;
    

//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
    assign  is_top_mv_field_intra = ~(top_mv_field_pred_flag_l1 | top_mv_field_pred_flag_l0);
    assign  is_left_mv_field_intra = ~(left_mv_field_pred_flag_l1 | left_mv_field_pred_flag_l0);

   geet_fifo #(
        .FIFO_DATA_WIDTH(MV_FIELD_DATA_WIDTH)
    ) top_row_mvs (
        .clk(clk), 
        .reset(reset), 
        .wr_en(bs_top_wr_en_in), 
        .rd_en(bs_top_rd_en_in), 
        .d_in(top_row_mvs_in), 
        .d_out(top_row_mvs_fifo_out), 
        .empty(is_top_mvs_empty), 
        .full(is_top_mvs_full)
        );
    
   geet_fifo #(
        .FIFO_DATA_WIDTH(MV_FIELD_DATA_WIDTH)
    ) left_row_mvs (
        .clk(clk), 
        .reset(reset), 
        .wr_en(bs_left_wr_en_in), 
        .rd_en(bs_left_rd_en_in), 
        .d_in(left_row_mvs_in), 
        .d_out(left_row_mvs_fifo_out), 
        .empty(is_left_mvs_empty), 
        .full(is_left_mvs_full)
        );


s_sync_ram_res_present  #(
    .DATA_WIDTH(1'b1),
    .ADDR_WIDTH((X11_ADDR_WDTH - LOG2_MIN_PU_SIZE)),
    .DATA_DEPTH((MAX_PIC_WIDTH>>LOG2_MIN_PU_SIZE))
) res_pres_top_line_buffer
(
    .clk(clk), 
    .addr_in(xT_in_min_tus_in),
    .r_data_out(res_top_pres_out), 
    .w_data_in(res_present_in), 
    .w_en_in(res_pres_top_wr_en), 
    .enable(1'b1)
);


s_sync_ram_res_present  #(
    .DATA_WIDTH(1'b1),
    .ADDR_WIDTH((X11_ADDR_WDTH - LOG2_MIN_PU_SIZE)),
    .DATA_DEPTH((MAX_PIC_WIDTH>>LOG2_MIN_PU_SIZE))
) res_pres_left_line_buffer
(
    .clk(clk), 
    .addr_in(yT_in_min_tus_in),
    .r_data_out(res_lef_pres_out), 
    .w_data_in(res_present_in), 
    .w_en_in(res_pres_lef_wr_en), 
    .enable(1'b1)
);



// synthesis translate_off
    always@(posedge clk) begin
		if(reset) begin
		end
		else begin
			// if(bs_fifo_full_in) begin
				// $display("%d bs_fifo_can never be full!!!!!",$time);
				// $stop;
			// end
			if(is_top_mvs_full) begin
				$display("%d top mvs _can never be full!!!!!",$time);
				$stop;
			end
			if(is_left_mvs_full) begin
				$display("%d left mvs _can never be full!!!!!",$time);
				$stop;
			end
		end
	end
// synthesis translate_on

   assign                   {   top_mv_field_pred_flag_l0   ,
                                top_mv_field_pred_flag_l1   ,
                                top_mv_field_ref_idx_l0   ,
                                top_mv_field_ref_idx_l1   ,
                                top_mv_field_mv_x_l0   ,
                                top_mv_field_mv_y_l0   ,
                                top_mv_field_mv_x_l1   ,
                                top_mv_field_mv_y_l1                } =   top_row_mvs_fifo_out;

   assign                   {   left_mv_field_pred_flag_l0   ,
                                left_mv_field_pred_flag_l1   ,
                                left_mv_field_ref_idx_l0   ,
                                left_mv_field_ref_idx_l1   ,
                                left_mv_field_mv_x_l0   ,
                                left_mv_field_mv_y_l0   ,
                                left_mv_field_mv_x_l1   ,
                                left_mv_field_mv_y_l1               } =   left_row_mvs_fifo_out;


    assign is_mv_p_bi_pred = (p_mv_field_pred_flagl0 & p_mv_field_pred_flagl1) ? 1'b1: 1'b0;
    
    assign is_ref_pic_p_l0l1_q_l0l1_diff  = ({p_dpb_idx_l0,p_dpb_idx_l1} == {q_dpb_idx_l0,q_dpb_idx_l1})? (1'b0) : (1'b1);
    assign is_ref_pic_p_l0l1_q_l1l0_diff  = ({p_dpb_idx_l0,p_dpb_idx_l1} == {q_dpb_idx_l1,q_dpb_idx_l0})? (1'b0) : (1'b1);

    assign is_ref_pic_p_l0_l1_diff =    (p_dpb_idx_l0 == p_dpb_idx_l1)? (1'b0) : (1'b1);
    
    
    
    
    
    
    always@(*) begin
        if({p_mv_field_pred_flagl0, p_mv_field_pred_flagl1} == {q_mv_field_pred_flagl0, q_mv_field_pred_flagl1}) begin
            is_num_mv_diff = 1'b0;
        end
        else begin
            is_num_mv_diff = 1'b1;
        end
    end
    
    
    always@(*) begin
        mv_p_l0_q_l0_x_diff = p_mv_field_mv_x_l0 - q_mv_field_mv_x_l0;
        mv_p_l0_q_l1_x_diff = p_mv_field_mv_x_l0 - q_mv_field_mv_x_l1;
        mv_p_l1_q_l0_x_diff = p_mv_field_mv_x_l1 - q_mv_field_mv_x_l0;
        mv_p_l1_q_l1_x_diff = p_mv_field_mv_x_l1 - q_mv_field_mv_x_l1;

        mv_p_l0_q_l0_x_diff_minus = q_mv_field_mv_x_l0 - p_mv_field_mv_x_l0;
        mv_p_l0_q_l1_x_diff_minus = q_mv_field_mv_x_l1 - p_mv_field_mv_x_l0;
        mv_p_l1_q_l0_x_diff_minus = q_mv_field_mv_x_l0 - p_mv_field_mv_x_l1;
        mv_p_l1_q_l1_x_diff_minus = q_mv_field_mv_x_l1 - p_mv_field_mv_x_l1;
        
        
        mv_p_l0_q_l0_y_diff = p_mv_field_mv_y_l0 - q_mv_field_mv_y_l0;
        mv_p_l0_q_l1_y_diff = p_mv_field_mv_y_l0 - q_mv_field_mv_y_l1;
        mv_p_l1_q_l0_y_diff = p_mv_field_mv_y_l1 - q_mv_field_mv_y_l0;
        mv_p_l1_q_l1_y_diff = p_mv_field_mv_y_l1 - q_mv_field_mv_y_l1;

        mv_p_l0_q_l0_y_diff_minus = q_mv_field_mv_y_l0 - p_mv_field_mv_y_l0;
        mv_p_l0_q_l1_y_diff_minus = q_mv_field_mv_y_l1 - p_mv_field_mv_y_l0;
        mv_p_l1_q_l0_y_diff_minus = q_mv_field_mv_y_l0 - p_mv_field_mv_y_l1;
        mv_p_l1_q_l1_y_diff_minus = q_mv_field_mv_y_l1 - p_mv_field_mv_y_l1;
        
    end
    
    always@(*) begin
        if(mv_p_l0_q_l0_x_diff[MVD_WIDTH-1]) begin
            if(mv_p_l0_q_l0_x_diff_minus >= 4) begin
                is_mv_p_l0_q_l0_x_abs_diff_great_4 = 1;
            end
            else begin
                is_mv_p_l0_q_l0_x_abs_diff_great_4 = 0;
            end
        end
        else begin // differnce is positive
            if(mv_p_l0_q_l0_x_diff >= 4) begin
                is_mv_p_l0_q_l0_x_abs_diff_great_4 = 1;
            end
            else begin
                is_mv_p_l0_q_l0_x_abs_diff_great_4 = 0;
            end
        end
        
        if(mv_p_l0_q_l1_x_diff[MVD_WIDTH-1]) begin
            if(mv_p_l0_q_l1_x_diff_minus >= 4) begin
                is_mv_p_l0_q_l1_x_abs_diff_great_4 = 1;
            end
            else begin
                is_mv_p_l0_q_l1_x_abs_diff_great_4 = 0;
            end
        end
        else begin // differnce is positive
            if(mv_p_l0_q_l1_x_diff >= 4) begin
                is_mv_p_l0_q_l1_x_abs_diff_great_4 = 1;
            end
            else begin
                is_mv_p_l0_q_l1_x_abs_diff_great_4 = 0;
            end
        end
        
        if(mv_p_l1_q_l1_x_diff[MVD_WIDTH-1]) begin
            if(mv_p_l1_q_l1_x_diff_minus >= 4) begin
                is_mv_p_l1_q_l1_x_abs_diff_great_4 = 1;
            end
            else begin
                is_mv_p_l1_q_l1_x_abs_diff_great_4 = 0;
            end
        end
        else begin // differnce is positive
            if(mv_p_l1_q_l1_x_diff >= 4) begin
                is_mv_p_l1_q_l1_x_abs_diff_great_4 = 1;
            end
            else begin
                is_mv_p_l1_q_l1_x_abs_diff_great_4 = 0;
            end
        end
        
        if(mv_p_l1_q_l0_x_diff[MVD_WIDTH-1]) begin
            if(mv_p_l1_q_l0_x_diff_minus >= 4) begin
                is_mv_p_l1_q_l0_x_abs_diff_great_4 = 1;
            end
            else begin
                is_mv_p_l1_q_l0_x_abs_diff_great_4 = 0;
            end
        end
        else begin // differnce is positive
            if(mv_p_l1_q_l0_x_diff >= 4) begin
                is_mv_p_l1_q_l0_x_abs_diff_great_4 = 1;
            end
            else begin
                is_mv_p_l1_q_l0_x_abs_diff_great_4 = 0;
            end
        end
    end
    

    always@(*) begin
        if(mv_p_l0_q_l0_y_diff[MVD_WIDTH-1]) begin
            if(mv_p_l0_q_l0_y_diff_minus >= 4) begin
                is_mv_p_l0_q_l0_y_abs_diff_great_4 = 1;
            end
            else begin
                is_mv_p_l0_q_l0_y_abs_diff_great_4 = 0;
            end
        end
        else begin // differnce is positive
            if(mv_p_l0_q_l0_y_diff >= 4) begin
                is_mv_p_l0_q_l0_y_abs_diff_great_4 = 1;
            end
            else begin
                is_mv_p_l0_q_l0_y_abs_diff_great_4 = 0;
            end
        end
        
        if(mv_p_l0_q_l1_y_diff[MVD_WIDTH-1]) begin
            if(mv_p_l0_q_l1_y_diff_minus >= 4) begin
                is_mv_p_l0_q_l1_y_abs_diff_great_4 = 1;
            end
            else begin
                is_mv_p_l0_q_l1_y_abs_diff_great_4 = 0;
            end
        end
        else begin // differnce is positive
            if(mv_p_l0_q_l1_y_diff >= 4) begin
                is_mv_p_l0_q_l1_y_abs_diff_great_4 = 1;
            end
            else begin
                is_mv_p_l0_q_l1_y_abs_diff_great_4 = 0;
            end
        end
        
        if(mv_p_l1_q_l1_y_diff[MVD_WIDTH-1]) begin
            if(mv_p_l1_q_l1_y_diff_minus >= 4) begin
                is_mv_p_l1_q_l1_y_abs_diff_great_4 = 1;
            end
            else begin
                is_mv_p_l1_q_l1_y_abs_diff_great_4 = 0;
            end
        end
        else begin // differnce is positive
            if(mv_p_l1_q_l1_y_diff >= 4) begin
                is_mv_p_l1_q_l1_y_abs_diff_great_4 = 1;
            end
            else begin
                is_mv_p_l1_q_l1_y_abs_diff_great_4 = 0;
            end
        end
        
        if(mv_p_l1_q_l0_y_diff[MVD_WIDTH-1]) begin
            if(mv_p_l1_q_l0_y_diff_minus >= 4) begin
                is_mv_p_l1_q_l0_y_abs_diff_great_4 = 1;
            end
            else begin
                is_mv_p_l1_q_l0_y_abs_diff_great_4 = 0;
            end
        end
        else begin // differnce is positive
            if(mv_p_l1_q_l0_y_diff >= 4) begin
                is_mv_p_l1_q_l0_y_abs_diff_great_4 = 1;
            end
            else begin
                is_mv_p_l1_q_l0_y_abs_diff_great_4 = 0;
            end
        end
    end
    
    assign bs_fifo_data_out = { {bs_h1},	{bs_h2},	{bs_v1},	{bs_v2}};


always@(*) begin
    res_pres_lef_wr_en = 1'b0;
    res_pres_top_wr_en = 1'b0;
    if(res_present_pu_range_4by4_valid || intra_tu_4by4_valid_in) begin
        if(y0_tu_in_min_tus_in == yT_in_min_tus_in) begin
            res_pres_top_wr_en = 1'b1;
        end
        if(x0_tu_in_min_tus_in == xT_in_min_tus_in) begin
            res_pres_lef_wr_en = 1'b1;
        end
    end

end

always@(*) begin 

    bs_left_rd_en_in  = 1'b0;
    bs_top_rd_en_in   = 1'b0;
    if(cand_mv_cache_valid || intra_tu_4by4_valid_in) begin
        if(cb_yy_min_tus_in == yT_in_min_tus_in) begin
            bs_top_rd_en_in = 1'b1;
        end
        if(cb_xx_min_tus_in == xT_in_min_tus_in) begin
            bs_left_rd_en_in = 1'b1;
        end
    end

end


always@(posedge clk) begin 
    if(reset) begin
        bs_state <= BS_STATE_RES_PRES_READ;
        bs_fifo_wr_en_out <= 0;
    end
   else begin
       case(bs_state)
           BS_STATE_RES_PRES_READ: begin
                bs_fifo_wr_en_out <= 0;
               if(res_present_pu_range_4by4_valid) begin
                    bs_h1 <= 2'd0;                        
                    bs_h2 <= 2'd0;                        
                    bs_v1 <= 2'd0;                        
                    bs_v2 <= 2'd0; 
                    bs_state <= BS_STATE_MV_READ_1;
                    current_res_present <= res_present_in;

                    if((yT_in_min_tus_in !=0)) begin
                        if(yT_in_min_tus_in == cb_yy_min_tus_in) begin
                            p_mv_field_pred_flagl0        <=   top_mv_field_pred_flag_l0   ;    
                            p_mv_field_pred_flagl1        <=   top_mv_field_pred_flag_l1   ; 
                            p_mv_field_mv_x_l0            <=   top_mv_field_mv_x_l0     ; 
                            p_mv_field_mv_y_l0            <=   top_mv_field_mv_y_l0     ; 
                            p_mv_field_mv_x_l1            <=   top_mv_field_mv_x_l1     ;     
                            p_mv_field_mv_y_l1            <=   top_mv_field_mv_y_l1     ; 
                            p_dpb_idx_l0                  <=   top_mv_field_ref_idx_l0        ; 
                            p_dpb_idx_l1                  <=   top_mv_field_ref_idx_l1        ; 
                            p_mv_available <= 1;                        
                        end
                        else if (is_cand_mv_in_range1 && (yT_in_min_tus_in == yy_pb1_strt_min_tus_in)) begin 
                            p_mv_field_pred_flagl0        <=    cand0_mv_field_pred_flag_l0   ;    
                            p_mv_field_pred_flagl1        <=    cand0_mv_field_pred_flag_l1   ; 
                            p_mv_field_mv_x_l0            <=    cand0_mv_field_mv_x_l0       ;  
                            p_mv_field_mv_y_l0            <=    cand0_mv_field_mv_y_l0       ;  
                            p_mv_field_mv_x_l1            <=    cand0_mv_field_mv_x_l1       ;     
                            p_mv_field_mv_y_l1            <=    cand0_mv_field_mv_y_l1       ; 
                            p_dpb_idx_l0                  <=    cand0_dpb_idx_l0      ;
                            p_dpb_idx_l1                  <=    cand0_dpb_idx_l1      ;   
                            p_mv_available <= 1;
                        end
                        else if (is_cand_mv_in_range2 && (yT_in_min_tus_in == yy_pb2_strt_min_tus_in)) begin 
                            p_mv_field_pred_flagl0        <=   cand0_mv_field_pred_flag_l0   ;    
                            p_mv_field_pred_flagl1        <=   cand0_mv_field_pred_flag_l1   ; 
                            p_mv_field_mv_x_l0            <=   cand0_mv_field_mv_x_l0       ;  
                            p_mv_field_mv_y_l0            <=   cand0_mv_field_mv_y_l0       ;  
                            p_mv_field_mv_x_l1            <=   cand0_mv_field_mv_x_l1       ;     
                            p_mv_field_mv_y_l1            <=   cand0_mv_field_mv_y_l1       ; 
                            p_dpb_idx_l0                  <=   cand0_dpb_idx_l0      ;
                            p_dpb_idx_l1                  <=   cand0_dpb_idx_l1      ;  
                            p_mv_available <= 1; 
                        end
                        else if (is_cand_mv_in_range3 && (yT_in_min_tus_in == yy_pb3_strt_min_tus_in)) begin 
                            p_mv_field_pred_flagl0        <=   cand1_mv_field_pred_flag_l0   ;    
                            p_mv_field_pred_flagl1        <=   cand1_mv_field_pred_flag_l1   ; 
                            p_mv_field_mv_x_l0            <=   cand1_mv_field_mv_x_l0       ;  
                            p_mv_field_mv_y_l0            <=   cand1_mv_field_mv_y_l0       ;  
                            p_mv_field_mv_x_l1            <=   cand1_mv_field_mv_x_l1       ;     
                            p_mv_field_mv_y_l1            <=   cand1_mv_field_mv_y_l1       ; 
                            p_dpb_idx_l0                  <=   cand1_dpb_idx_l0      ;
                            p_dpb_idx_l1                  <=   cand1_dpb_idx_l1      ; 
                            p_mv_available <= 1;  
                        end
                        else begin
                            p_mv_available <= 0;
                        end
                    end
                    else begin
                        p_mv_available <= 0;
                    end
                        
               end
               else if(intra_tu_4by4_valid_in) begin
                    bs_state <= BS_INTRA_STATE_1;
                    if(x0_tu_in_min_tus_in == xT_in_min_tus_in) begin
                       bs_v1 <= 2'd2;
                    end
                    else begin
                       bs_v1 <= 2'd0;
                    end

                    if(y0_tu_in_min_tus_in == yT_in_min_tus_in) begin
                       bs_h1 <= 2'd2;
                    end
                    else begin
                       bs_h1 <= 2'd0;
                    end

                    
                        // synthesis translate_off
                    if((yT_in_min_tus_in == cb_yy_min_tus_in)) begin
                        if( (yT_in_min_tus_in !=0) && (!is_top_mvs_empty))  begin
                            if(is_top_mv_field_intra == 0) begin
                                    
                            end
                            else if(is_top_mv_field_intra == 1) begin
                                
                            end
                            else begin
                               $display("%d intra flag cant be x !!",$time);
                               #100;
                               $stop;
                            end
                            if(res_top_pres_out == 0) begin
                                    
                            end 
                            else if(res_top_pres_out == 1) begin
                                
                            end
                            else begin
                               $display("%d res preseent cant be x !!",$time);
                               #100;
                               $stop; 
                            end
                        end
                    end
                        // synthesis translate_on


                    // synthesis translate_off
                    if((xT_in_min_tus_in == cb_xx_min_tus_in)) begin
                        if( (xT_in_min_tus_in !=0) && (!is_left_mvs_empty))  begin
                            if(is_left_mv_field_intra == 0) begin
                                    
                            end
                            else if(is_left_mv_field_intra == 1) begin
                                
                            end
                            else begin
                               $display("%d intra flag cant be x !!",$time);
                               #100;
                               $stop;
                            end
                            if(res_lef_pres_out == 0) begin
                                    
                            end 
                            else if(res_lef_pres_out == 1) begin
                                
                            end
                            else begin
                               $display("%d res preseent cant be x !!",$time);
                               #100;
                               $stop; 
                            end
                        end
                    end
                    // synthesis translate_on
                    

                   bs_h2 <= 2'd0;
                   bs_v2 <= 2'd0;
               end
               else begin
                   bs_h1 <= 2'd0;                        
                   bs_h2 <= 2'd0;                        
                   bs_v1 <= 2'd0;                        
                   bs_v2 <= 2'd0;             
               end
            end
            BS_STATE_MV_READ_1: begin
                if(cand_mv_cache_valid) begin
                    if( (yT_in_min_tus_in !=0))  begin
                        if(is_top_mv_field_intra && (yT_in_min_tus_in == cb_yy_min_tus_in) ) begin // from second frame on, mv buffer is filled properly.... so there can't be x in pred_flag fields
                            bs_h1 <= 2'd2;    
                        end
                        else if((current_res_present || (res_top_pres_out  ) ) && (y0_tu_in_min_tus_in == yT_in_min_tus_in))begin
                            // res present of current 4x4 block or res present of upper 4x4 block given upper block block available and y cord is at the tu boundary
                            bs_h1 <= 2'd1;
                        end
                        else if(p_mv_available) begin
                            if(is_num_mv_diff) begin
                                bs_h1 <= 2'd1;
                            end
                            else if(is_mv_p_bi_pred) begin
                                if(is_ref_pic_p_l0l1_q_l0l1_diff && is_ref_pic_p_l0l1_q_l1l0_diff) begin
                                    bs_h1 <= 2'd1;
                                end
                                else if(is_ref_pic_p_l0_l1_diff) begin
                                    if((~is_ref_pic_p_l0l1_q_l0l1_diff)) begin
                                        if(  (is_mv_p_l0_q_l0_x_abs_diff_great_4 || is_mv_p_l0_q_l0_y_abs_diff_great_4) || (is_mv_p_l1_q_l1_x_abs_diff_great_4 || is_mv_p_l1_q_l1_y_abs_diff_great_4) ) begin
                                            bs_h1 <= 2'd1;
                                        end
                                    end
                                    else if(~(is_ref_pic_p_l0l1_q_l1l0_diff)) begin
                                        if(  (is_mv_p_l0_q_l1_x_abs_diff_great_4 || is_mv_p_l0_q_l1_y_abs_diff_great_4) || (is_mv_p_l1_q_l0_x_abs_diff_great_4 || is_mv_p_l1_q_l0_y_abs_diff_great_4) ) begin
                                            bs_h1 <= 2'd1;
                                        end
                                    end
                                end
                                else begin // pl0 == pl1 == ql0 == ql1
                                    if( (is_mv_p_l0_q_l0_x_abs_diff_great_4 || is_mv_p_l0_q_l0_y_abs_diff_great_4 || is_mv_p_l1_q_l1_x_abs_diff_great_4 || is_mv_p_l1_q_l1_y_abs_diff_great_4)  &&
                                        (is_mv_p_l0_q_l1_x_abs_diff_great_4 || is_mv_p_l0_q_l1_y_abs_diff_great_4 || is_mv_p_l1_q_l0_x_abs_diff_great_4 || is_mv_p_l1_q_l0_y_abs_diff_great_4)
                                        ) begin
                                        bs_h1 <= 2'd1;
                                    end
                                end
                            end
                            else begin
                                case({p_mv_field_pred_flagl0,p_mv_field_pred_flagl1, q_mv_field_pred_flagl0,q_mv_field_pred_flagl1 })
                                    4'b1010: begin
                                        if(p_dpb_idx_l0 != q_dpb_idx_l0) begin
                                            bs_h1 <= 2'd1;
                                        end
                                        else if(is_mv_p_l0_q_l0_x_abs_diff_great_4 || is_mv_p_l0_q_l0_y_abs_diff_great_4) begin
                                            bs_h1 <= 2'd1;
                                        end
                                    end
                                    4'b0110: begin
                                        if(p_dpb_idx_l1 != q_dpb_idx_l0) begin
                                            bs_h1 <= 2'd1;
                                        end
                                        else if(is_mv_p_l1_q_l0_x_abs_diff_great_4 || is_mv_p_l1_q_l0_y_abs_diff_great_4) begin
                                            bs_h1 <= 2'd1;
                                        end
                                    end
                                    4'b1001: begin
                                        if(p_dpb_idx_l0 != q_dpb_idx_l1) begin
                                            bs_h1 <= 2'd1;
                                        end
                                        else if(is_mv_p_l0_q_l1_x_abs_diff_great_4 || is_mv_p_l0_q_l1_y_abs_diff_great_4) begin
                                            bs_h1 <= 2'd1;
                                        end
                                    end
                                    4'b0101: begin
                                        if(p_dpb_idx_l1 != q_dpb_idx_l1) begin
                                            bs_h1 <= 2'd1;
                                        end
                                        else if(is_mv_p_l1_q_l1_x_abs_diff_great_4 || is_mv_p_l1_q_l1_y_abs_diff_great_4) begin
                                            bs_h1 <= 2'd1;
                                        end
                                    end
                                    default: begin
                                        // synthesis translate_off
                                            $display("%d cant reach this state!!",$time);
                                            #100;
                                            $stop;
                                        // synthesis translate_on
                                    end
                                endcase
                            end
                        end

                        // synthesis translate_off
                        if((yT_in_min_tus_in == cb_yy_min_tus_in)) begin
                            if( (yT_in_min_tus_in !=0) && (!is_top_mvs_empty))  begin
                                if(is_top_mv_field_intra == 0) begin
                                    
                                end
                                else if(is_top_mv_field_intra == 1) begin
                                    
                                end
                                else begin
                                   $display("%d intra flag cant be x !!",$time);
                                   #100;
                                   $stop;
                                end
                                if(res_top_pres_out == 0) begin
                                    
                                end 
                                else if(res_top_pres_out == 1) begin
                                    
                                end
                                else begin
                                   $display("%d res preseent cant be x !!",$time);
                                   #100;
                                   $stop; 
                                end
                            end
                        end
                        // synthesis translate_on

                    end
                    // else if(current_res_present && (y0_tu_in_min_tus_in == yT_in_min_tus_in)) begin
                    //     bs_h1 <= 2'd1;
                    // end
                    
                    if((xT_in_min_tus_in !=0)) begin
                        if(is_left_mv_field_intra && (xT_in_min_tus_in == cb_xx_min_tus_in) ) begin
                            bs_v1 <= 2'd2;
                        end
                        else  if((current_res_present || (res_lef_pres_out)) && (x0_tu_in_min_tus_in == xT_in_min_tus_in))begin
                            bs_v1 <= 2'd1;
                        end
                        // synthesis translate_off
                        if((xT_in_min_tus_in == cb_xx_min_tus_in)) begin
                            if( (xT_in_min_tus_in !=0) && (!is_left_mvs_empty))  begin

                                if(is_left_mv_field_intra == 0) begin
                                    
                                end
                                else if(is_left_mv_field_intra == 1) begin
                                    
                                end
                                else begin
                                   $display("%d intra flag cant be x !!",$time);
                                   #100;
                                   $stop;
                                end
                                if(res_lef_pres_out == 0) begin
                                    
                                end 
                                else if(res_lef_pres_out == 1) begin
                                    
                                end
                                else begin
                                   $display("%d res preseent cant be x !!",$time);
                                   #100;
                                   $stop; 
                                end
                            end
                        end
                        // synthesis translate_on
                    end


                    if((xT_in_min_tus_in !=0)) begin
                        if(xT_in_min_tus_in == cb_xx_min_tus_in) begin
                            p_mv_field_pred_flagl0        <=   left_mv_field_pred_flag_l0   ;    
                            p_mv_field_pred_flagl1        <=   left_mv_field_pred_flag_l1   ; 
                            p_mv_field_mv_x_l0            <=   left_mv_field_mv_x_l0     ; 
                            p_mv_field_mv_y_l0            <=   left_mv_field_mv_y_l0     ; 
                            p_mv_field_mv_x_l1            <=   left_mv_field_mv_x_l1     ;     
                            p_mv_field_mv_y_l1            <=   left_mv_field_mv_y_l1     ; 
                            p_dpb_idx_l0                  <=   left_mv_field_ref_idx_l0        ; 
                            p_dpb_idx_l1                  <=   left_mv_field_ref_idx_l1        ;
                            p_mv_available <= 1;                       
                        end
                        else if (is_cand_mv_in_range1 && (xT_in_min_tus_in == xx_pb1_strt_min_tus_in)) begin 
                            p_mv_field_pred_flagl0        <=   cand0_mv_field_pred_flag_l0   ;    
                            p_mv_field_pred_flagl1        <=   cand0_mv_field_pred_flag_l1   ; 
                            p_mv_field_mv_x_l0            <=   cand0_mv_field_mv_x_l0       ;  
                            p_mv_field_mv_y_l0            <=   cand0_mv_field_mv_y_l0       ;  
                            p_mv_field_mv_x_l1            <=   cand0_mv_field_mv_x_l1       ;     
                            p_mv_field_mv_y_l1            <=   cand0_mv_field_mv_y_l1       ; 
                            p_dpb_idx_l0                  <=   cand0_dpb_idx_l0      ;
                            p_dpb_idx_l1                  <=   cand0_dpb_idx_l1      ;   
                            p_mv_available <= 1; 
                        end
                        // synthesis translate_off
                        else if (is_cand_mv_in_range2 && (xT_in_min_tus_in == xx_pb2_strt_min_tus_in)) begin 
                                
                                   $display("%d cant reach this because it is already covered with xT_in_min_tus_in == cb_xx_min_tus_in",$time);
                                   #100; 
                                   $stop;
                                
                        end
                        // synthesis translate_on 
                        else if (is_cand_mv_in_range3 && (xT_in_min_tus_in == xx_pb3_strt_min_tus_in)) begin 
                            p_mv_field_pred_flagl0        <=   cand2_mv_field_pred_flag_l0   ;    
                            p_mv_field_pred_flagl1        <=   cand2_mv_field_pred_flag_l1   ; 
                            p_mv_field_mv_x_l0            <=   cand2_mv_field_mv_x_l0       ;  
                            p_mv_field_mv_y_l0            <=   cand2_mv_field_mv_y_l0       ;  
                            p_mv_field_mv_x_l1            <=   cand2_mv_field_mv_x_l1       ;     
                            p_mv_field_mv_y_l1            <=   cand2_mv_field_mv_y_l1       ; 
                            p_dpb_idx_l0                  <=   cand2_dpb_idx_l0      ;
                            p_dpb_idx_l1                  <=   cand2_dpb_idx_l1      ;   
                            p_mv_available <= 1; 
                        end
                        else begin
                            p_mv_available <= 0;
                        end
                    end
                    else begin
                        p_mv_available <= 0;
                    end

                        
                    bs_state <= BS_STATE_MV_BS_WRITE_1;
                end
                // synthesis translate_off
                else begin
                   $display("%d cand_mv_cache_valid should be asserted in immediate next cycle!!",$time);
                   #100;
                   $stop;
                end
                // synthesis translate_on


                // load first two candidates
            end
            BS_STATE_MV_BS_WRITE_1: begin
                if(res_present_pu_range_4by4_valid) begin
                    bs_state <= BS_STATE_MV_READ_2;
                    current_res_present <= res_present_in;
                    if((yT_in_min_tus_in !=0)) begin
                        if(yT_in_min_tus_in == cb_yy_min_tus_in) begin
                            p_mv_field_pred_flagl0        <=   top_mv_field_pred_flag_l0   ;    
                            p_mv_field_pred_flagl1        <=   top_mv_field_pred_flag_l1   ; 
                            p_mv_field_mv_x_l0            <=   top_mv_field_mv_x_l0     ; 
                            p_mv_field_mv_y_l0            <=   top_mv_field_mv_y_l0     ; 
                            p_mv_field_mv_x_l1            <=   top_mv_field_mv_x_l1     ;     
                            p_mv_field_mv_y_l1            <=   top_mv_field_mv_y_l1     ; 
                            p_dpb_idx_l0                  <=   top_mv_field_ref_idx_l0        ; 
                            p_dpb_idx_l1                  <=   top_mv_field_ref_idx_l1        ; 
                            p_mv_available <= 1;                        
                        end
                        else if (is_cand_mv_in_range1 && (yT_in_min_tus_in == yy_pb1_strt_min_tus_in)) begin 
                            p_mv_field_pred_flagl0        <=    cand0_mv_field_pred_flag_l0   ;    
                            p_mv_field_pred_flagl1        <=    cand0_mv_field_pred_flag_l1   ; 
                            p_mv_field_mv_x_l0            <=    cand0_mv_field_mv_x_l0       ;  
                            p_mv_field_mv_y_l0            <=    cand0_mv_field_mv_y_l0       ;  
                            p_mv_field_mv_x_l1            <=    cand0_mv_field_mv_x_l1       ;     
                            p_mv_field_mv_y_l1            <=    cand0_mv_field_mv_y_l1       ; 
                            p_dpb_idx_l0                  <=    cand0_dpb_idx_l0      ;
                            p_dpb_idx_l1                  <=    cand0_dpb_idx_l1      ;   
                            p_mv_available <= 1;
                        end
                        else if (is_cand_mv_in_range2 && (yT_in_min_tus_in == yy_pb2_strt_min_tus_in)) begin 
                            p_mv_field_pred_flagl0        <=   cand0_mv_field_pred_flag_l0   ;    
                            p_mv_field_pred_flagl1        <=   cand0_mv_field_pred_flag_l1   ; 
                            p_mv_field_mv_x_l0            <=   cand0_mv_field_mv_x_l0       ;  
                            p_mv_field_mv_y_l0            <=   cand0_mv_field_mv_y_l0       ;  
                            p_mv_field_mv_x_l1            <=   cand0_mv_field_mv_x_l1       ;     
                            p_mv_field_mv_y_l1            <=   cand0_mv_field_mv_y_l1       ; 
                            p_dpb_idx_l0                  <=   cand0_dpb_idx_l0      ;
                            p_dpb_idx_l1                  <=   cand0_dpb_idx_l1      ;  
                            p_mv_available <= 1; 
                        end
                        else if (is_cand_mv_in_range3 && (yT_in_min_tus_in == yy_pb3_strt_min_tus_in)) begin 
                            p_mv_field_pred_flagl0        <=   cand1_mv_field_pred_flag_l0   ;    
                            p_mv_field_pred_flagl1        <=   cand1_mv_field_pred_flag_l1   ; 
                            p_mv_field_mv_x_l0            <=   cand1_mv_field_mv_x_l0       ;  
                            p_mv_field_mv_y_l0            <=   cand1_mv_field_mv_y_l0       ;  
                            p_mv_field_mv_x_l1            <=   cand1_mv_field_mv_x_l1       ;     
                            p_mv_field_mv_y_l1            <=   cand1_mv_field_mv_y_l1       ; 
                            p_dpb_idx_l0                  <=   cand1_dpb_idx_l0      ;
                            p_dpb_idx_l1                  <=   cand1_dpb_idx_l1      ; 
                            p_mv_available <= 1;  
                        end
                        else begin
                            p_mv_available <= 0;
                        end
                    end
                    else begin
                        p_mv_available <= 0;
                    end
                end
                else begin
                    bs_state <= BS_STATE_PU_RANGE_READ_2;
                end

                if(p_mv_available & bs_v1 == 2'd0) begin
                    if(~res_present_pu_range_4by4_valid) begin
                        p_mv_available <= 1'b0;    
                    end                    

                            if(is_num_mv_diff) begin
                                bs_v1 <= 2'd1;
                            end
                            else if(is_mv_p_bi_pred) begin
                                if(is_ref_pic_p_l0l1_q_l0l1_diff && is_ref_pic_p_l0l1_q_l1l0_diff) begin
                                    bs_v1 <= 2'd1;
                                end
                                else if(is_ref_pic_p_l0_l1_diff) begin
                                    if((~is_ref_pic_p_l0l1_q_l0l1_diff)) begin
                                        if(  (is_mv_p_l0_q_l0_x_abs_diff_great_4 || is_mv_p_l0_q_l0_y_abs_diff_great_4) || (is_mv_p_l1_q_l1_x_abs_diff_great_4 || is_mv_p_l1_q_l1_y_abs_diff_great_4) ) begin
                                            bs_v1 <= 2'd1;
                                        end
                                    end
                                    else if(~(is_ref_pic_p_l0l1_q_l1l0_diff)) begin
                                        if(  (is_mv_p_l0_q_l1_x_abs_diff_great_4 || is_mv_p_l0_q_l1_y_abs_diff_great_4) || (is_mv_p_l1_q_l0_x_abs_diff_great_4 || is_mv_p_l1_q_l0_y_abs_diff_great_4) ) begin
                                            bs_v1 <= 2'd1;
                                        end
                                    end
                                end
                                else begin // pl0 == pl1 == ql0 == ql1
                                    if( (is_mv_p_l0_q_l0_x_abs_diff_great_4 || is_mv_p_l0_q_l0_y_abs_diff_great_4 || is_mv_p_l1_q_l1_x_abs_diff_great_4 || is_mv_p_l1_q_l1_y_abs_diff_great_4)  &&
                                        (is_mv_p_l0_q_l1_x_abs_diff_great_4 || is_mv_p_l0_q_l1_y_abs_diff_great_4 || is_mv_p_l1_q_l0_x_abs_diff_great_4 || is_mv_p_l1_q_l0_y_abs_diff_great_4)
                                        ) begin
                                        bs_v1 <= 2'd1;
                                    end
                                end
                            end
                            else begin
                                case({p_mv_field_pred_flagl0,p_mv_field_pred_flagl1, q_mv_field_pred_flagl0,q_mv_field_pred_flagl1 })
                                    4'b1010: begin
                                        if(p_dpb_idx_l0 != q_dpb_idx_l0) begin
                                            bs_v1 <= 2'd1;
                                        end
                                        else if(is_mv_p_l0_q_l0_x_abs_diff_great_4 || is_mv_p_l0_q_l0_y_abs_diff_great_4) begin
                                            bs_v1 <= 2'd1;
                                        end
                                    end
                                    4'b0110: begin
                                        if(p_dpb_idx_l1 != q_dpb_idx_l0) begin
                                            bs_v1 <= 2'd1;
                                        end
                                        else if(is_mv_p_l1_q_l0_x_abs_diff_great_4 || is_mv_p_l1_q_l0_y_abs_diff_great_4) begin
                                            bs_v1 <= 2'd1;
                                        end
                                    end
                                    4'b1001: begin
                                        if(p_dpb_idx_l0 != q_dpb_idx_l1) begin
                                            bs_v1 <= 2'd1;
                                        end
                                        else if(is_mv_p_l0_q_l1_x_abs_diff_great_4 || is_mv_p_l0_q_l1_y_abs_diff_great_4) begin
                                            bs_v1 <= 2'd1;
                                        end
                                    end
                                    4'b0101: begin
                                        if(p_dpb_idx_l1 != q_dpb_idx_l1) begin
                                            bs_v1 <= 2'd1;
                                        end
                                        else if(is_mv_p_l1_q_l1_x_abs_diff_great_4 || is_mv_p_l1_q_l1_y_abs_diff_great_4) begin
                                            bs_v1 <= 2'd1;
                                        end
                                    end
                                    default: begin
                                        // synthesis translate_off
                                            $display("%d cant reach this state!!",$time);
                                            #100;
                                            $stop;
                                        // synthesis translate_on
                                    end
                                endcase
                            end


                end
            end
            BS_STATE_PU_RANGE_READ_2: begin
                if(res_present_pu_range_4by4_valid) begin
                    current_res_present <= res_present_in;
                    bs_state <= BS_STATE_MV_READ_2;
                    if((yT_in_min_tus_in !=0)) begin
                        if(yT_in_min_tus_in == cb_yy_min_tus_in) begin
                            p_mv_field_pred_flagl0        <=   top_mv_field_pred_flag_l0   ;    
                            p_mv_field_pred_flagl1        <=   top_mv_field_pred_flag_l1   ; 
                            p_mv_field_mv_x_l0            <=   top_mv_field_mv_x_l0     ; 
                            p_mv_field_mv_y_l0            <=   top_mv_field_mv_y_l0     ; 
                            p_mv_field_mv_x_l1            <=   top_mv_field_mv_x_l1     ;     
                            p_mv_field_mv_y_l1            <=   top_mv_field_mv_y_l1     ; 
                            p_dpb_idx_l0                  <=   top_mv_field_ref_idx_l0        ; 
                            p_dpb_idx_l1                  <=   top_mv_field_ref_idx_l1        ; 
                            p_mv_available <= 1;                        
                        end
                        else if (is_cand_mv_in_range1 && (yT_in_min_tus_in == yy_pb1_strt_min_tus_in)) begin 
                            p_mv_field_pred_flagl0        <=    cand0_mv_field_pred_flag_l0   ;    
                            p_mv_field_pred_flagl1        <=    cand0_mv_field_pred_flag_l1   ; 
                            p_mv_field_mv_x_l0            <=    cand0_mv_field_mv_x_l0       ;  
                            p_mv_field_mv_y_l0            <=    cand0_mv_field_mv_y_l0       ;  
                            p_mv_field_mv_x_l1            <=    cand0_mv_field_mv_x_l1       ;     
                            p_mv_field_mv_y_l1            <=    cand0_mv_field_mv_y_l1       ; 
                            p_dpb_idx_l0                  <=    cand0_dpb_idx_l0      ;
                            p_dpb_idx_l1                  <=    cand0_dpb_idx_l1      ;   
                            p_mv_available <= 1;
                        end
                        else if (is_cand_mv_in_range2 && (yT_in_min_tus_in == yy_pb2_strt_min_tus_in)) begin 
                            p_mv_field_pred_flagl0        <=   cand0_mv_field_pred_flag_l0   ;    
                            p_mv_field_pred_flagl1        <=   cand0_mv_field_pred_flag_l1   ; 
                            p_mv_field_mv_x_l0            <=   cand0_mv_field_mv_x_l0       ;  
                            p_mv_field_mv_y_l0            <=   cand0_mv_field_mv_y_l0       ;  
                            p_mv_field_mv_x_l1            <=   cand0_mv_field_mv_x_l1       ;     
                            p_mv_field_mv_y_l1            <=   cand0_mv_field_mv_y_l1       ; 
                            p_dpb_idx_l0                  <=   cand0_dpb_idx_l0      ;
                            p_dpb_idx_l1                  <=   cand0_dpb_idx_l1      ;  
                            p_mv_available <= 1; 
                        end
                        else if (is_cand_mv_in_range3 && (yT_in_min_tus_in == yy_pb3_strt_min_tus_in)) begin 
                            p_mv_field_pred_flagl0        <=   cand1_mv_field_pred_flag_l0   ;    
                            p_mv_field_pred_flagl1        <=   cand1_mv_field_pred_flag_l1   ; 
                            p_mv_field_mv_x_l0            <=   cand1_mv_field_mv_x_l0       ;  
                            p_mv_field_mv_y_l0            <=   cand1_mv_field_mv_y_l0       ;  
                            p_mv_field_mv_x_l1            <=   cand1_mv_field_mv_x_l1       ;     
                            p_mv_field_mv_y_l1            <=   cand1_mv_field_mv_y_l1       ; 
                            p_dpb_idx_l0                  <=   cand1_dpb_idx_l0      ;
                            p_dpb_idx_l1                  <=   cand1_dpb_idx_l1      ; 
                            p_mv_available <= 1;  
                        end
                        else begin
                            p_mv_available <= 0;
                        end
                    end
                    else begin
                        p_mv_available <= 0;
                    end
                end
            end
            BS_STATE_MV_READ_2: begin
                if(cand_mv_cache_valid) begin
                    if( (yT_in_min_tus_in !=0))  begin
                        if(is_top_mv_field_intra && (yT_in_min_tus_in == cb_yy_min_tus_in) ) begin
                            bs_h2 <= 2'd2;    
                        end
                        else if((current_res_present || (res_top_pres_out  ) ) && (y0_tu_in_min_tus_in == yT_in_min_tus_in))begin
                            // res present of current 4x4 block or res present of upper 4x4 block given upper block block available and y cord is at the tu boundary
                            bs_h2 <= 2'd1;
                        end
                        else if(p_mv_available) begin
                            if(is_num_mv_diff) begin
                                bs_h2 <= 2'd1;
                            end
                            else if(is_mv_p_bi_pred) begin
                                if(is_ref_pic_p_l0l1_q_l0l1_diff && is_ref_pic_p_l0l1_q_l1l0_diff) begin
                                    bs_h2 <= 2'd1;
                                end
                                else if(is_ref_pic_p_l0_l1_diff) begin
                                    if((~is_ref_pic_p_l0l1_q_l0l1_diff)) begin
                                        if(  (is_mv_p_l0_q_l0_x_abs_diff_great_4 || is_mv_p_l0_q_l0_y_abs_diff_great_4) || (is_mv_p_l1_q_l1_x_abs_diff_great_4 || is_mv_p_l1_q_l1_y_abs_diff_great_4) ) begin
                                            bs_h2 <= 2'd1;
                                        end
                                    end
                                    else if(~(is_ref_pic_p_l0l1_q_l1l0_diff)) begin
                                        if(  (is_mv_p_l0_q_l1_x_abs_diff_great_4 || is_mv_p_l0_q_l1_y_abs_diff_great_4) || (is_mv_p_l1_q_l0_x_abs_diff_great_4 || is_mv_p_l1_q_l0_y_abs_diff_great_4) ) begin
                                            bs_h2 <= 2'd1;
                                        end
                                    end
                                end
                                else begin // pl0 == pl1 == ql0 == ql1
                                    if( (is_mv_p_l0_q_l0_x_abs_diff_great_4 || is_mv_p_l0_q_l0_y_abs_diff_great_4 || is_mv_p_l1_q_l1_x_abs_diff_great_4 || is_mv_p_l1_q_l1_y_abs_diff_great_4)  &&
                                        (is_mv_p_l0_q_l1_x_abs_diff_great_4 || is_mv_p_l0_q_l1_y_abs_diff_great_4 || is_mv_p_l1_q_l0_x_abs_diff_great_4 || is_mv_p_l1_q_l0_y_abs_diff_great_4)
                                        ) begin
                                        bs_h2 <= 2'd1;
                                    end
                                end
                            end
                            else begin
                                case({p_mv_field_pred_flagl0,p_mv_field_pred_flagl1, q_mv_field_pred_flagl0,q_mv_field_pred_flagl1 })
                                    4'b1010: begin
                                        if(p_dpb_idx_l0 != q_dpb_idx_l0) begin
                                            bs_h2 <= 2'd1;
                                        end
                                        else if(is_mv_p_l0_q_l0_x_abs_diff_great_4 || is_mv_p_l0_q_l0_y_abs_diff_great_4) begin
                                            bs_h2 <= 2'd1;
                                        end
                                    end
                                    4'b0110: begin
                                        if(p_dpb_idx_l1 != q_dpb_idx_l0) begin
                                            bs_h2 <= 2'd1;
                                        end
                                        else if(is_mv_p_l1_q_l0_x_abs_diff_great_4 || is_mv_p_l1_q_l0_y_abs_diff_great_4) begin
                                            bs_h2 <= 2'd1;
                                        end
                                    end
                                    4'b1001: begin
                                        if(p_dpb_idx_l0 != q_dpb_idx_l1) begin
                                            bs_h2 <= 2'd1;
                                        end
                                        else if(is_mv_p_l0_q_l1_x_abs_diff_great_4 || is_mv_p_l0_q_l1_y_abs_diff_great_4) begin
                                            bs_h2 <= 2'd1;
                                        end
                                    end
                                    4'b0101: begin
                                        if(p_dpb_idx_l1 != q_dpb_idx_l1) begin
                                            bs_h2 <= 2'd1;
                                        end
                                        else if(is_mv_p_l1_q_l1_x_abs_diff_great_4 || is_mv_p_l1_q_l1_y_abs_diff_great_4) begin
                                            bs_h2 <= 2'd1;
                                        end
                                    end
                                    default: begin
                                        // synthesis translate_off
                                            $display("%d cant reach this state!!",$time);
                                            #100;
                                            $stop;
                                        // synthesis translate_on
                                    end
                                endcase
                            end
                        end

                        // synthesis translate_off
                        if((yT_in_min_tus_in == cb_yy_min_tus_in)) begin
                            if( (yT_in_min_tus_in !=0) && (!is_top_mvs_empty))  begin
                                if(is_top_mv_field_intra == 0) begin
                                    
                                end
                                else if(is_top_mv_field_intra == 1) begin
                                    
                                end
                                else begin
                                   $display("%d intra flag cant be x !!",$time);
                                   #100;
                                   $stop;
                                end

                                if(res_top_pres_out == 0) begin
                                    
                                end 
                                else if(res_top_pres_out == 1) begin
                                    
                                end
                                else begin
                                   $display("%d res preseent cant be x !!",$time);
                                   #100;
                                   $stop; 
                                end
                            end
                        end
                        // synthesis translate_on
                    end
                    // else if(current_res_present && (y0_tu_in_min_tus_in == yT_in_min_tus_in)) begin
                    //     bs_h2 <= 2'd1;
                    // end

                   bs_state <= BS_STATE_PU_RANGE_READ_3;
                end
                // synthesis translate_off
                else begin
                   $display("%d cand_mv_cache_valid should be asserted in immediate next cycle!!",$time);
                   #100;
                   $stop;
                end
                // synthesis translate_on


            end
            // BS_STATE_MV_BS_WRITE_2: begin
            //     if(res_present_pu_range_4by4_valid) begin
            //         bs_state <= BS_STATE_MV_READ_3;
            //     end
            //     else begin
            //         bs_state <= BS_STATE_PU_RANGE_READ_3;
            //     end
            // end
            BS_STATE_PU_RANGE_READ_3: begin
                if(res_present_pu_range_4by4_valid) begin
                    current_res_present <= res_present_in;
                    bs_state <= BS_STATE_MV_READ_3;

                    if((xT_in_min_tus_in !=0)) begin
                        if(xT_in_min_tus_in == cb_xx_min_tus_in) begin
                            p_mv_field_pred_flagl0        <=   left_mv_field_pred_flag_l0   ;    
                            p_mv_field_pred_flagl1        <=   left_mv_field_pred_flag_l1   ; 
                            p_mv_field_mv_x_l0            <=   left_mv_field_mv_x_l0     ; 
                            p_mv_field_mv_y_l0            <=   left_mv_field_mv_y_l0     ; 
                            p_mv_field_mv_x_l1            <=   left_mv_field_mv_x_l1     ;     
                            p_mv_field_mv_y_l1            <=   left_mv_field_mv_y_l1     ; 
                            p_dpb_idx_l0                  <=   left_mv_field_ref_idx_l0        ; 
                            p_dpb_idx_l1                  <=   left_mv_field_ref_idx_l1        ;
                            p_mv_available <= 1;                       
                        end
                        else if (is_cand_mv_in_range1 && (xT_in_min_tus_in == xx_pb1_strt_min_tus_in)) begin 
                            p_mv_field_pred_flagl0        <=   cand0_mv_field_pred_flag_l0   ;    
                            p_mv_field_pred_flagl1        <=   cand0_mv_field_pred_flag_l1   ; 
                            p_mv_field_mv_x_l0            <=   cand0_mv_field_mv_x_l0       ;  
                            p_mv_field_mv_y_l0            <=   cand0_mv_field_mv_y_l0       ;  
                            p_mv_field_mv_x_l1            <=   cand0_mv_field_mv_x_l1       ;     
                            p_mv_field_mv_y_l1            <=   cand0_mv_field_mv_y_l1       ; 
                            p_dpb_idx_l0                  <=   cand0_dpb_idx_l0      ;
                            p_dpb_idx_l1                  <=   cand0_dpb_idx_l1      ;   
                            p_mv_available <= 1; 
                        end
                        // synthesis translate_off
                        else if (is_cand_mv_in_range2 && (xT_in_min_tus_in == xx_pb2_strt_min_tus_in)) begin 
                                
                                   $display("%d can reach this !!",$time);
                                   #100;
                                   $stop;
                                
                        end
                        // synthesis translate_on 
                        else if (is_cand_mv_in_range3 && (xT_in_min_tus_in == xx_pb3_strt_min_tus_in)) begin 
                            p_mv_field_pred_flagl0        <=   cand2_mv_field_pred_flag_l0   ;    
                            p_mv_field_pred_flagl1        <=   cand2_mv_field_pred_flag_l1   ; 
                            p_mv_field_mv_x_l0            <=   cand2_mv_field_mv_x_l0       ;  
                            p_mv_field_mv_y_l0            <=   cand2_mv_field_mv_y_l0       ;  
                            p_mv_field_mv_x_l1            <=   cand2_mv_field_mv_x_l1       ;     
                            p_mv_field_mv_y_l1            <=   cand2_mv_field_mv_y_l1       ; 
                            p_dpb_idx_l0                  <=   cand2_dpb_idx_l0      ;
                            p_dpb_idx_l1                  <=   cand2_dpb_idx_l1      ;   
                            p_mv_available <= 1; 
                        end
                        else begin
                            p_mv_available <= 0;
                        end
                    end
                    else begin
                        p_mv_available <= 0;
                    end


                end
            end
            BS_STATE_MV_READ_3: begin
                if(cand_mv_cache_valid) begin

                   if((xT_in_min_tus_in !=0)) begin
                        if(is_left_mv_field_intra && (xT_in_min_tus_in == cb_xx_min_tus_in)) begin
                            bs_v2 <= 2'd2;
                        end
                        else  if((current_res_present || (res_lef_pres_out)) && (x0_tu_in_min_tus_in == xT_in_min_tus_in))begin
                            bs_v2 <= 2'd1;
                        end

                        else if(p_mv_available) begin
                            if(is_num_mv_diff) begin
                                bs_v2 <= 2'd1;
                            end
                            else if(is_mv_p_bi_pred) begin
                                if(is_ref_pic_p_l0l1_q_l0l1_diff && is_ref_pic_p_l0l1_q_l1l0_diff) begin
                                    bs_v2 <= 2'd1;
                                end
                                else if(is_ref_pic_p_l0_l1_diff) begin
                                    if((~is_ref_pic_p_l0l1_q_l0l1_diff)) begin
                                        if(  (is_mv_p_l0_q_l0_x_abs_diff_great_4 || is_mv_p_l0_q_l0_y_abs_diff_great_4) || (is_mv_p_l1_q_l1_x_abs_diff_great_4 || is_mv_p_l1_q_l1_y_abs_diff_great_4) ) begin
                                            bs_v2 <= 2'd1;
                                        end
                                    end
                                    else if(~(is_ref_pic_p_l0l1_q_l1l0_diff)) begin
                                        if(  (is_mv_p_l0_q_l1_x_abs_diff_great_4 || is_mv_p_l0_q_l1_y_abs_diff_great_4) || (is_mv_p_l1_q_l0_x_abs_diff_great_4 || is_mv_p_l1_q_l0_y_abs_diff_great_4) ) begin
                                            bs_v2 <= 2'd1;
                                        end
                                    end
                                end
                                else begin // pl0 == pl1 == ql0 == ql1
                                    if( (is_mv_p_l0_q_l0_x_abs_diff_great_4 || is_mv_p_l0_q_l0_y_abs_diff_great_4 || is_mv_p_l1_q_l1_x_abs_diff_great_4 || is_mv_p_l1_q_l1_y_abs_diff_great_4)  &&
                                        (is_mv_p_l0_q_l1_x_abs_diff_great_4 || is_mv_p_l0_q_l1_y_abs_diff_great_4 || is_mv_p_l1_q_l0_x_abs_diff_great_4 || is_mv_p_l1_q_l0_y_abs_diff_great_4)
                                        ) begin
                                        bs_v2 <= 2'd1;
                                    end
                                end
                            end
                            else begin
                                case({p_mv_field_pred_flagl0,p_mv_field_pred_flagl1, q_mv_field_pred_flagl0,q_mv_field_pred_flagl1 })
                                    4'b1010: begin
                                        if(p_dpb_idx_l0 != q_dpb_idx_l0) begin
                                            bs_v2 <= 2'd1;
                                        end
                                        else if(is_mv_p_l0_q_l0_x_abs_diff_great_4 || is_mv_p_l0_q_l0_y_abs_diff_great_4) begin
                                            bs_v2 <= 2'd1;
                                        end
                                    end
                                    4'b0110: begin
                                        if(p_dpb_idx_l1 != q_dpb_idx_l0) begin
                                            bs_v2 <= 2'd1;
                                        end
                                        else if(is_mv_p_l1_q_l0_x_abs_diff_great_4 || is_mv_p_l1_q_l0_y_abs_diff_great_4) begin
                                            bs_v2 <= 2'd1;
                                        end
                                    end
                                    4'b1001: begin
                                        if(p_dpb_idx_l0 != q_dpb_idx_l1) begin
                                            bs_v2 <= 2'd1;
                                        end
                                        else if(is_mv_p_l0_q_l1_x_abs_diff_great_4 || is_mv_p_l0_q_l1_y_abs_diff_great_4) begin
                                            bs_v2 <= 2'd1;
                                        end
                                    end
                                    4'b0101: begin
                                        if(p_dpb_idx_l1 != q_dpb_idx_l1) begin
                                            bs_v2 <= 2'd1;
                                        end
                                        else if(is_mv_p_l1_q_l1_x_abs_diff_great_4 || is_mv_p_l1_q_l1_y_abs_diff_great_4) begin
                                            bs_v2 <= 2'd1;
                                        end
                                    end
                                    default: begin
                                        // synthesis translate_off
                                            $display("%d cant reach this state!!",$time);
                                            #100;
                                            $stop;
                                        // synthesis translate_on
                                    end
                                endcase
                            end
                        end


                        // synthesis translate_off
                        if((xT_in_min_tus_in == cb_xx_min_tus_in)) begin
                            if( (xT_in_min_tus_in !=0) && (!is_left_mvs_empty))  begin
                                if(is_left_mv_field_intra == 0) begin
                                    
                                end
                                else if(is_left_mv_field_intra == 1) begin
                                    
                                end
                                else begin
                                   $display("%d intra flag cant be x !!",$time);
                                   #100;
                                   $stop;
                                end
                                if(res_lef_pres_out == 0) begin
                                    
                                end 
                                else if(res_lef_pres_out == 1) begin
                                    
                                end
                                else begin
                                   $display("%d res preseent cant be x !!",$time);
                                   #100;
                                   $stop; 
                                end
                            end
                        end
                        // synthesis translate_on
                    end
                    // else if(current_res_present && (x0_tu_in_min_tus_in == xT_in_min_tus_in) ) begin
                    //     bs_v2 <= 2'd1;
                    // end

                    bs_state <= BS_STATE_MV_BS_WRITE_3;
                end
                // synthesis translate_off
                else begin
                   $display("%d cand_mv_cache_valid should be asserted in immediate next cycle!!",$time);
                   #100;
                   $stop;
                end
                // synthesis translate_on

                // load third two candidates
            end
            BS_STATE_MV_BS_WRITE_3: begin
                if(res_present_pu_range_4by4_valid) begin
					if(!bs_fifo_full_in) begin
						bs_state <= BS_STATE_RES_PRES_READ;
						bs_fifo_wr_en_out <= 1;
					end
				end
            end
            BS_INTRA_STATE_1: begin
                if(intra_tu_4by4_valid_in)begin
                    if(y0_tu_in_min_tus_in == yT_in_min_tus_in) begin
                        bs_h2 <= 2'd2;
                    end
                    
                    // synthesis translate_off
                    if((yT_in_min_tus_in == cb_yy_min_tus_in)) begin
                        if( (yT_in_min_tus_in !=0) && (!is_top_mvs_empty))  begin
                        
                            if(is_top_mv_field_intra == 0) begin
                                
                            end
                            else if(is_top_mv_field_intra == 1) begin
                                
                            end
                            else begin
                               $display("%d intra flag cant be x !!",$time);
                               #100;
                               $stop;
                            end
                            if(res_top_pres_out == 0) begin
                                
                            end 
                            else if(res_top_pres_out == 1) begin
                                
                            end
                            else begin
                               $display("%d res preseent cant be x !!",$time);
                               #100;
                               $stop; 
                            end
                        end
                    end
                    // synthesis translate_on
                    
                   bs_state <= BS_INTRA_STATE_2;
                end
                else begin
                end
            end
            BS_INTRA_STATE_2: begin
               if(intra_tu_4by4_valid_in) begin
                   if(x0_tu_in_min_tus_in == xT_in_min_tus_in) begin
                       bs_v2 <= 2'd2;
                   end

                    // synthesis translate_off
                    if((xT_in_min_tus_in == cb_xx_min_tus_in)) begin
                        if( (xT_in_min_tus_in !=0) && (!is_left_mvs_empty))  begin
                            if(is_left_mv_field_intra == 0) begin
                                    
                            end
                            else if(is_left_mv_field_intra == 1) begin
                                
                            end
                            else begin
                               $display("%d intra flag cant be x !!",$time);
                               #100;
                               $stop;
                            end
                            if(res_lef_pres_out == 0) begin
                                    
                            end 
                            else if(res_lef_pres_out == 1) begin
                                
                            end
                            else begin
                               $display("%d res preseent cant be x !!",$time);
                               #100;
                               $stop; 
                            end
                        end
                    end
                    // synthesis translate_on


                   bs_state <= BS_INTRA_STATE_3;
               end
               else begin
               end
            end
            BS_INTRA_STATE_3: begin
               if(intra_tu_4by4_valid_in) begin
					if(!bs_fifo_full_in) begin
						bs_state <= BS_STATE_RES_PRES_READ;
						bs_fifo_wr_en_out <= 1;
					end
               end
               else begin
               end
            end

       endcase
   end
       
end


// synthesis translate_off


    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]          xT_in_min_tus_in_d;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]          yT_in_min_tus_in_d;


    integer bs_state_d;

    reg                                  q_mv_field_pred_flagl0_d   ;
    reg                                  q_mv_field_pred_flagl1_d   ;
    reg signed [MVD_WIDTH -1:0]          q_mv_field_mv_x_l0_d       ;
    reg signed [MVD_WIDTH -1:0]          q_mv_field_mv_y_l0_d       ;
    reg signed [MVD_WIDTH -1:0]          q_mv_field_mv_x_l1_d       ;
    reg signed [MVD_WIDTH -1:0]          q_mv_field_mv_y_l1_d       ;   
    reg [DPB_ADDR_WIDTH -1:0]            q_dpb_idx_l0_d             ;    
    reg [DPB_ADDR_WIDTH -1:0]            q_dpb_idx_l1_d             ;  

    always @(posedge clk) begin
            bs_state_d <= bs_state;
            q_mv_field_pred_flagl0_d <=  q_mv_field_pred_flagl0   ;
            q_mv_field_pred_flagl1_d <=  q_mv_field_pred_flagl1   ;
            q_mv_field_mv_x_l0_d     <=  q_mv_field_mv_x_l0       ;
            q_mv_field_mv_y_l0_d     <=  q_mv_field_mv_y_l0       ;
            q_mv_field_mv_x_l1_d     <=  q_mv_field_mv_x_l1       ;
            q_mv_field_mv_y_l1_d     <=  q_mv_field_mv_y_l1       ;   
            q_dpb_idx_l0_d           <=  q_dpb_idx_l0             ;    
            q_dpb_idx_l1_d           <=  q_dpb_idx_l1             ;  
            xT_in_min_tus_in_d <= xT_in_min_tus_in;
            yT_in_min_tus_in_d <= yT_in_min_tus_in;

    end

    always@(posedge clk) begin
        if(bs_state_d == BS_STATE_MV_READ_1 && bs_state == BS_STATE_MV_BS_WRITE_1)begin
            if({    q_mv_field_pred_flagl0_d,
                    q_mv_field_pred_flagl1_d,
                    q_mv_field_mv_x_l0_d    ,
                    q_mv_field_mv_y_l0_d    ,
                    q_mv_field_mv_x_l1_d    ,
                    q_mv_field_mv_y_l1_d    ,
                    q_dpb_idx_l0_d          ,
                    q_dpb_idx_l1_d          

                } != {
                        q_mv_field_pred_flagl0 ,
                        q_mv_field_pred_flagl1 ,
                        q_mv_field_mv_x_l0     ,
                        q_mv_field_mv_y_l0     ,
                        q_mv_field_mv_x_l1     ,
                        q_mv_field_mv_y_l1     ,
                        q_dpb_idx_l0           ,
                        q_dpb_idx_l1           


                    }) begin
                        $display("%d q mv should settle for one more clock cycle for bs_v1 calculation!",$time);
                        #100;
                        $stop;
                    end

            if(xT_in_min_tus_in_d != xT_in_min_tus_in) begin
                $display("%d xT mv should settle for one more clock cycle for bs_v1 calculation!",$time);
                #100;
                $stop;
            end
             if(yT_in_min_tus_in_d != yT_in_min_tus_in) begin
                $display("%d xT mv should settle for one more clock cycle for bs_v1 calculation!",$time);
                #100;
                $stop;
            end
        end

        if(res_present_pu_range_4by4_valid || intra_tu_4by4_valid_in) begin
            if(yT_in_min_tus_in == cb_yy_min_tus_in) begin
                if(is_top_mvs_empty) begin
                    $display( "%d top fifo should not be empty at this stage!!",$time) ;
                    #100;
                    $stop;
                end
            end
            if(xT_in_min_tus_in == cb_xx_min_tus_in) begin
                if(is_left_mvs_empty) begin
                    $display( "%d left fifo should not be empty at this stage!!",$time) ;
                    #100;
                    $stop;
                end
            end
        end
    end

// synthesis translate_on


//  no need for this as monitor is not attached at the read stage

 // synthesis translate_off
`ifdef DBF_IN_BS_VERIFY

    integer bs_file;
    reg [1:0] bs_v1_soft;
    reg [1:0] bs_v2_soft;
    reg [1:0] bs_h1_soft;
    reg [1:0] bs_h2_soft;

    reg [7:0] read_val;
    reg [7:0] bs_soft;
    initial begin
        bs_file = $fopen("pred_to_dbf_bs","rb");
    end

    always@(posedge clk) begin
        if(bs_fifo_wr_en_out) begin
            read_val = $fgetc( bs_file);
			if(read_val == 8'hff) begin
				$display("end of bs file");
				$stop;
			end
            {bs_v2_soft,bs_v1_soft,bs_h2_soft, bs_h1_soft} = read_val;
             bs_soft = {bs_h1_soft[1:0],bs_h2_soft[1:0],bs_v1_soft[1:0], bs_v2_soft[1:0]};
            if(( {bs_h1_soft[1:0],bs_h2_soft[1:0]} != bs_fifo_data_out[7:4]) & ((yT_in_min_tus_in_d>>1) != 0)) begin
                $display("%d BS values wrong!! @ x= %d y=%d soft %b hard %b",$time,(xT_in_min_tus_in_d>>1)*8,(yT_in_min_tus_in_d>>1)*8,bs_soft,bs_fifo_data_out  );
                #10;
                $stop;
            end
            // else begin
            //   $display("bs_good!  @ x= %d y=%d soft %b hard %b",(xT_in_min_tus_in_d>>1)*8,(yT_in_min_tus_in_d>>1)*8,bs_soft,bs_fifo_data_out );
            // end

            if(( {bs_v1_soft[1:0], bs_v2_soft[1:0]} != bs_fifo_data_out[3:0]) & ((xT_in_min_tus_in_d>>1) != 0))begin
                $display("%d BS values wrong!! @ x= %d y=%d soft %b hard %b",$time,(xT_in_min_tus_in_d>>1)*8,(yT_in_min_tus_in_d>>1)*8,bs_soft,bs_fifo_data_out  );
                #10;
                $stop;
            end
            // else begin
            //   $display("bs_good!  @ x= %d y=%d soft %b hard %b",xT_in_min_tus_in*4,yT_in_min_tus_in*4,bs_soft,bs_fifo_data_out );
            // end

            

        end
        
    end


`endif

// synthesis translate_on
 


endmodule