`timescale 1ns / 1ps
module chroma_vert_4_tap_filter_paramrized(
    clk,
    filt_type,
	level_in,
    a_ref_pixel_in ,
    b_ref_pixel_in ,
    c_ref_pixel_in ,
    d_ref_pixel_in ,
    pred_pixel_out
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter FILTER_PIXEL_WIDTH_IN = FILTER_PIXEL_WIDTH;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
    localparam   FRAC_WIDTH = 2'd3;
    parameter 	SHIFT2 = 3'd6;
    parameter   SHIFT3 = 3'd6;

//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input [FRAC_WIDTH-1:0] filt_type;
    input level_in;
    input [FILTER_PIXEL_WIDTH_IN -1: 0] a_ref_pixel_in;
    input [FILTER_PIXEL_WIDTH_IN -1: 0] b_ref_pixel_in;
    input [FILTER_PIXEL_WIDTH_IN -1: 0] c_ref_pixel_in;
    input [FILTER_PIXEL_WIDTH_IN -1: 0] d_ref_pixel_in;
    output reg [FILTER_PIXEL_WIDTH -1: 0] pred_pixel_out;
    

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    reg [FILTER_PIXEL_WIDTH_IN -1: 0] a_ref_pixel;
    reg [FILTER_PIXEL_WIDTH_IN -1: 0] b_ref_pixel;
    reg [FILTER_PIXEL_WIDTH_IN -1: 0] c_ref_pixel;
    reg [FILTER_PIXEL_WIDTH_IN -1: 0] d_ref_pixel;
    
    reg [FILTER_PIXEL_WIDTH_IN +7 -1: 0] pred_pixel_ch_out_inmd1;
    reg [FILTER_PIXEL_WIDTH_IN +7 -1: 0] pred_pixel_ch_out_inmd2;
    reg [FILTER_PIXEL_WIDTH_IN +7 -1: 0] pred_pixel_ch_out_inmd3;
    reg [FILTER_PIXEL_WIDTH_IN +7 -1: 0] pred_pixel_ch_out_inmd4;
	
	// reg [FRAC_WIDTH-1:0] filt_type_d;
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module

    reg signed [FILTER_PIXEL_WIDTH_IN +6 -1: 0]    coef_a;       // max -6
    reg [6 -1:0]        coef_b;               // max 58
    reg [6 -1:0]        coef_c;               // max 36
    reg signed [FILTER_PIXEL_WIDTH_IN +6 -1: 0]    coef_d;        // max -4
    
    reg level_d;
	reg [FRAC_WIDTH-1:0] filt_type_d;
	
always@(posedge clk) begin    
    level_d <= level_in;
	filt_type_d <= filt_type;
end

always@(posedge clk) begin
    case(filt_type)
        3'b000: begin
            coef_a <= 1;//-22'd2;
            coef_b <= 0;
            coef_c <= 0;
            coef_d <= 0;//-22'd2;
        end
        3'b001: begin
            coef_a <= {{(FILTER_PIXEL_WIDTH_IN+6-4){1'b1}}, 4'b1110};//-22'd2;
            coef_b <= 58;
            coef_c <= 10;
            coef_d <= {{(FILTER_PIXEL_WIDTH_IN+6-4){1'b1}}, 4'b1110};//-22'd2;
        end
        3'b010: begin
            coef_a <= {{(FILTER_PIXEL_WIDTH_IN+6-4){1'b1}}, 4'b1100};//-22'd4;
            coef_b <= 54;
            coef_c <= 16;
            coef_d <= {{(FILTER_PIXEL_WIDTH_IN+6-4){1'b1}}, 4'b1110};//-22'd2;
        end
        3'b011: begin
            coef_a <= {{(FILTER_PIXEL_WIDTH_IN+6-4){1'b1}}, 4'b1010};//-22'd6;
            coef_b <= 46;
            coef_c <= 28;
            coef_d <=  {{(FILTER_PIXEL_WIDTH_IN+6-4){1'b1}}, 4'b1100};//-22'd4;
        end
        3'b100: begin
            coef_a <=  {{(FILTER_PIXEL_WIDTH_IN+6-4){1'b1}}, 4'b1100};//-22'd4;
            coef_b <= 36;
            coef_c <= 36;
            coef_d <=  {{(FILTER_PIXEL_WIDTH_IN+6-4){1'b1}}, 4'b1100};//-22'd4;
        end
        3'b101: begin
            coef_d <= {{(FILTER_PIXEL_WIDTH_IN+6-4){1'b1}}, 4'b1010};//-22'd6;
            coef_c <= 46;
            coef_b <= 28;
            coef_a <=  {{(FILTER_PIXEL_WIDTH_IN+6-4){1'b1}}, 4'b1100};//-22'd4;
        end
		3'b110: begin
            coef_d <= {{(FILTER_PIXEL_WIDTH_IN+6-4){1'b1}}, 4'b1100};//-22'd4;
            coef_c <= 54;
            coef_b <= 16;
            coef_a <= {{(FILTER_PIXEL_WIDTH_IN+6-4){1'b1}}, 4'b1110};//-22'd2;
        end
        3'b111: begin
            coef_d <= {{(FILTER_PIXEL_WIDTH_IN+6-4){1'b1}}, 4'b1110};//-22'd2;
            coef_c <= 58;
            coef_b <= 10;
            coef_a <= {{(FILTER_PIXEL_WIDTH_IN+6-4){1'b1}}, 4'b1110};//-22'd2;
        end
		default: begin
            coef_d <= 0;//-22'd2;
            coef_c <= 0;
            coef_b <= 0;
            coef_a <= 0;//-22'd2;
		end
    endcase
end


always@(posedge clk) begin
    pred_pixel_ch_out_inmd1 <= (a_ref_pixel) * coef_a;
    pred_pixel_ch_out_inmd2 <= (b_ref_pixel) * coef_b;
    pred_pixel_ch_out_inmd3 <= (c_ref_pixel) * coef_c;
    pred_pixel_ch_out_inmd4 <= (d_ref_pixel) * coef_d;
	
	a_ref_pixel		<= a_ref_pixel_in ;
	b_ref_pixel    	<= b_ref_pixel_in ;
	c_ref_pixel    	<= c_ref_pixel_in ;
	d_ref_pixel    	<= d_ref_pixel_in ;
	
end

always@(posedge clk) begin
	if(level_d) begin
		if(filt_type_d == 0) begin
			pred_pixel_out <= pred_pixel_ch_out_inmd1<<SHIFT3 ;  
		end
		else begin
			pred_pixel_out <= (pred_pixel_ch_out_inmd1 + pred_pixel_ch_out_inmd2 + pred_pixel_ch_out_inmd3 + pred_pixel_ch_out_inmd4);  
		end
	end
	else begin
		if(filt_type_d == 0) begin
			pred_pixel_out <= (pred_pixel_ch_out_inmd1);  
		end
		else begin
			pred_pixel_out <= (pred_pixel_ch_out_inmd1 + pred_pixel_ch_out_inmd2 + pred_pixel_ch_out_inmd3 + pred_pixel_ch_out_inmd4)>>SHIFT2;  
		end
	end
end


endmodule 