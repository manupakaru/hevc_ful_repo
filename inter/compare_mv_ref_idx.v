module compare_mv_ref_idx(
    clk,
    // reset,
    in1_pred_flag_l0 ,
    in1_pred_flag_l1 ,
    in2_pred_flag_l0 ,
    in2_pred_flag_l1 ,
    
    in1_mv_ref_idx_l0   ,
    in1_mv_ref_idx_l1   ,
    in2_mv_ref_idx_l0   ,
    in2_mv_ref_idx_l1   ,
    in1_mv_x_l0         ,
    in1_mv_y_l0         ,
    in1_mv_x_l1         ,
    in1_mv_y_l1         ,
    in2_mv_x_l0         ,
    in2_mv_y_l0         ,
    in2_mv_x_l1         ,
    in2_mv_y_l1         ,
    
    compare_in1in2_ref_idx,
    compare_in1in2_mv
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    // input reset;
    input in1_pred_flag_l0 ;
    input in1_pred_flag_l1 ;
    input in2_pred_flag_l0 ;
    input in2_pred_flag_l1 ;

    input [REF_IDX_LX_WIDTH -1:0]   in1_mv_ref_idx_l0   ;
    input [REF_IDX_LX_WIDTH -1:0]   in1_mv_ref_idx_l1   ;
    input [REF_IDX_LX_WIDTH -1:0]   in2_mv_ref_idx_l0   ;
    input [REF_IDX_LX_WIDTH -1:0]   in2_mv_ref_idx_l1   ;
    input [MVD_WIDTH -1:0]          in1_mv_x_l0         ;
    input [MVD_WIDTH -1:0]          in1_mv_y_l0         ;
    input [MVD_WIDTH -1:0]          in1_mv_x_l1         ;
    input [MVD_WIDTH -1:0]          in1_mv_y_l1         ;
    input [MVD_WIDTH -1:0]          in2_mv_x_l0         ;
    input [MVD_WIDTH -1:0]          in2_mv_y_l0         ;
    input [MVD_WIDTH -1:0]          in2_mv_x_l1         ;
    input [MVD_WIDTH -1:0]          in2_mv_y_l1         ;

    
    output reg compare_in1in2_ref_idx;
    output compare_in1in2_mv;

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module


    reg compare_in1in2_x_mv, compare_in1in2_y_mv;
    assign compare_in1in2_mv = compare_in1in2_x_mv & compare_in1in2_y_mv;

    always@(posedge clk) begin    
        case({in1_pred_flag_l0,in1_pred_flag_l1, in2_pred_flag_l0,in2_pred_flag_l1})
            4'b1111: begin
                if(in1_mv_x_l0 == in2_mv_x_l0 && in1_mv_x_l1 == in2_mv_x_l1) begin
                    compare_in1in2_x_mv <= 1;
                end 
                else begin
                    compare_in1in2_x_mv <= 0;
                end
                if (in1_mv_y_l0 == in2_mv_y_l0 && in1_mv_y_l1 == in2_mv_y_l1) begin
                    compare_in1in2_y_mv <= 1;
                end
                else begin
                    compare_in1in2_y_mv <= 0;
                end
                if(in1_mv_ref_idx_l0 == in2_mv_ref_idx_l0 && in1_mv_ref_idx_l1 == in2_mv_ref_idx_l1) begin
                    compare_in1in2_ref_idx <= 1;
                end
                else begin
                    compare_in1in2_ref_idx <= 0;
                end
            end
            4'b1010: begin
                if(in1_mv_x_l0 == in2_mv_x_l0) begin
                    compare_in1in2_x_mv <= 1;
                end 
                else begin
                    compare_in1in2_x_mv <= 0;
                end
                if (in1_mv_y_l0 == in2_mv_y_l0) begin
                    compare_in1in2_y_mv <= 1;
                end
                else begin
                    compare_in1in2_y_mv <= 0;
                end
                if(in1_mv_ref_idx_l0 == in2_mv_ref_idx_l0) begin
                    compare_in1in2_ref_idx <= 1;
                end
                else begin
                    compare_in1in2_ref_idx <= 0;
                end
            end
            4'b0101: begin
                if(in1_mv_x_l1 == in2_mv_x_l1) begin
                    compare_in1in2_x_mv <= 1;
                end 
                else begin
                    compare_in1in2_x_mv <= 0;
                end
                if (in1_mv_y_l1 == in2_mv_y_l1) begin
                    compare_in1in2_y_mv <= 1;
                end
                else begin
                    compare_in1in2_y_mv <= 0;
                end
                if(in1_mv_ref_idx_l1 == in2_mv_ref_idx_l1) begin
                    compare_in1in2_ref_idx <= 1;
                end
                else begin
                    compare_in1in2_ref_idx <= 0;
                end            
            end
            default: begin
                compare_in1in2_ref_idx <= 0;
                compare_in1in2_x_mv <= 0;
                compare_in1in2_y_mv <= 0;
            end
        endcase
    end


endmodule