
`timescale 1ns / 1ps
module col_mv_buffer_mem
(
    clk,
    r_addr_in,
    clear_pred_flags,
    w_addr_in,
	r_data_out,
	w_data_in,
	w_en_in,
    enable

);

    
    //---------------------------------------------------------------------------------------------------------------------
    // parameter definitions
    //---------------------------------------------------------------------------------------------------------------------
    	parameter       RDATA_WIDTH = 320;
    	parameter       WDATA_WIDTH = 80;
        parameter       RADDR_WIDTH = 2;
        parameter       WADDR_WIDTH = 4;
        
        parameter       DATA_DEPTH = 1<<RADDR_WIDTH;
    //---------------------------------------------------------------------------------------------------------------------
    // localparam definitions
    //---------------------------------------------------------------------------------------------------------------------
    
    
   	
    //---------------------------------------------------------------------------------------------------------------------
    // I/O signals
    //---------------------------------------------------------------------------------------------------------------------
    

    input                           					clk;
    input                                               clear_pred_flags;
	input	[WDATA_WIDTH - 1:0]             		    w_data_in;
	input	[RADDR_WIDTH - 1:0]				            r_addr_in;			
	input	[WADDR_WIDTH - 1:0]				            w_addr_in;			
	input												w_en_in;
    input                                               enable;
	output  reg [RDATA_WIDTH - 1:0]	        		    r_data_out;
	
	
	//---------------------------------------------------------------------------------------------------------------------
    // Internal wires and registers
    //---------------------------------------------------------------------------------------------------------------------
  
    reg     [RDATA_WIDTH - 1:0]                          col_mem[0 : DATA_DEPTH -1];
    // ---------------------------------------------------------------------------------------------------------------------
    // Implmentation
    // ---------------------------------------------------------------------------------------------------------------------
    
    
	always @(posedge clk) begin
        if(enable) begin
            if(w_en_in) begin
                case(w_addr_in[1:0])
                    2'd0: begin
                        col_mem[w_addr_in[3:2]][1*WDATA_WIDTH -1: 0]              <= w_data_in;
                    end
                    2'd1: begin
                        col_mem[w_addr_in[3:2]][2*WDATA_WIDTH -1:   WDATA_WIDTH]    <= w_data_in;
                    end
                    2'd2: begin
                        col_mem[w_addr_in[3:2]][3*WDATA_WIDTH -1: 2*WDATA_WIDTH]    <= w_data_in;
                    end
                    2'd3: begin
                        col_mem[w_addr_in[3:2]][4*WDATA_WIDTH -1: 3*WDATA_WIDTH]    <= w_data_in;
                    end
                endcase
            end
            else if(clear_pred_flags)begin
            
                col_mem[0][1*WDATA_WIDTH-1:1*WDATA_WIDTH-2] <= 2'd0;
                col_mem[0][2*WDATA_WIDTH-1:2*WDATA_WIDTH-2] <= 2'd0;
                col_mem[0][3*WDATA_WIDTH-1:3*WDATA_WIDTH-2] <= 2'd0;
                col_mem[0][4*WDATA_WIDTH-1:4*WDATA_WIDTH-2] <= 2'd0;
                
                col_mem[1][1*WDATA_WIDTH-1:1*WDATA_WIDTH-2] <= 2'd0;
                col_mem[1][2*WDATA_WIDTH-1:2*WDATA_WIDTH-2] <= 2'd0;
                col_mem[1][3*WDATA_WIDTH-1:3*WDATA_WIDTH-2] <= 2'd0;
                col_mem[1][4*WDATA_WIDTH-1:4*WDATA_WIDTH-2] <= 2'd0;

                col_mem[2][1*WDATA_WIDTH-1:1*WDATA_WIDTH-2] <= 2'd0;
                col_mem[2][2*WDATA_WIDTH-1:2*WDATA_WIDTH-2] <= 2'd0;
                col_mem[2][3*WDATA_WIDTH-1:3*WDATA_WIDTH-2] <= 2'd0;
                col_mem[2][4*WDATA_WIDTH-1:4*WDATA_WIDTH-2] <= 2'd0;

                col_mem[3][1*WDATA_WIDTH-1:1*WDATA_WIDTH-2] <= 2'd0;
                col_mem[3][2*WDATA_WIDTH-1:2*WDATA_WIDTH-2] <= 2'd0;
                col_mem[3][3*WDATA_WIDTH-1:3*WDATA_WIDTH-2] <= 2'd0;
                col_mem[3][4*WDATA_WIDTH-1:4*WDATA_WIDTH-2] <= 2'd0;

                
            end
                r_data_out  <= col_mem[r_addr_in];
        end
	end	
	


endmodule 