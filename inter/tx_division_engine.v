`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:58:02 11/24/2013 
// Design Name: 
// Module Name:    division_wrapper 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module tx_division_engine(
    clk,
    enable,
//input ce,
//input nd,
//output rdy,
//output rfd,
//input signed [15 : 0] dividend,
    divisor,
    quotient

    );
`include "../sim/pred_def.v"

input clk;
input enable;
//input ce;
//input nd;
//output rdy;
//output rfd;
//input signed [15 : 0] dividend;
input signed [7 : 0] divisor;
output reg signed [TX_WIDTH -1 : 0] quotient;
 
always @(posedge clk) begin
    if(enable) begin
        case(divisor)
        
          -128  : begin quotient <=     -128; end
          -127  : begin quotient <=     -129; end
          -126  : begin quotient <=     -130; end
          -125  : begin quotient <=     -131; end
          -124  : begin quotient <=     -132; end
          -123  : begin quotient <=     -133; end
          -122  : begin quotient <=     -134; end
          -121  : begin quotient <=     -135; end
          -120  : begin quotient <=     -137; end
          -119  : begin quotient <=     -138; end
          -118  : begin quotient <=     -139; end
          -117  : begin quotient <=     -140; end
          -116  : begin quotient <=     -141; end
          -115  : begin quotient <=     -142; end
          -114  : begin quotient <=     -144; end
          -113  : begin quotient <=     -145; end
          -112  : begin quotient <=     -146; end
          -111  : begin quotient <=     -148; end
          -110  : begin quotient <=     -149; end
          -109  : begin quotient <=     -150; end
          -108  : begin quotient <=     -152; end
          -107  : begin quotient <=     -153; end
          -106  : begin quotient <=     -155; end
          -105  : begin quotient <=     -156; end
          -104  : begin quotient <=     -158; end
          -103  : begin quotient <=     -159; end
          -102  : begin quotient <=     -161; end
          -101  : begin quotient <=     -162; end
          -100  : begin quotient <=     -164; end
           -99  : begin quotient <=     -165; end
           -98  : begin quotient <=     -167; end
           -97  : begin quotient <=     -169; end
           -96  : begin quotient <=     -171; end
           -95  : begin quotient <=     -172; end
           -94  : begin quotient <=     -174; end
           -93  : begin quotient <=     -176; end
           -92  : begin quotient <=     -178; end
           -91  : begin quotient <=     -180; end
           -90  : begin quotient <=     -182; end
           -89  : begin quotient <=     -184; end
           -88  : begin quotient <=     -186; end
           -87  : begin quotient <=     -188; end
           -86  : begin quotient <=     -191; end
           -85  : begin quotient <=     -193; end
           -84  : begin quotient <=     -195; end
           -83  : begin quotient <=     -197; end
           -82  : begin quotient <=     -200; end
           -81  : begin quotient <=     -202; end
           -80  : begin quotient <=     -205; end
           -79  : begin quotient <=     -207; end
           -78  : begin quotient <=     -210; end
           -77  : begin quotient <=     -213; end
           -76  : begin quotient <=     -216; end
           -75  : begin quotient <=     -218; end
           -74  : begin quotient <=     -221; end
           -73  : begin quotient <=     -224; end
           -72  : begin quotient <=     -228; end
           -71  : begin quotient <=     -231; end
           -70  : begin quotient <=     -234; end
           -69  : begin quotient <=     -237; end
           -68  : begin quotient <=     -241; end
           -67  : begin quotient <=     -245; end
           -66  : begin quotient <=     -248; end
           -65  : begin quotient <=     -252; end
           -64  : begin quotient <=     -256; end
           -63  : begin quotient <=     -260; end
           -62  : begin quotient <=     -264; end
           -61  : begin quotient <=     -269; end
           -60  : begin quotient <=     -273; end
           -59  : begin quotient <=     -278; end
           -58  : begin quotient <=     -282; end
           -57  : begin quotient <=     -287; end
           -56  : begin quotient <=     -293; end
           -55  : begin quotient <=     -298; end
           -54  : begin quotient <=     -303; end
           -53  : begin quotient <=     -309; end
           -52  : begin quotient <=     -315; end
           -51  : begin quotient <=     -321; end
           -50  : begin quotient <=     -328; end
           -49  : begin quotient <=     -334; end
           -48  : begin quotient <=     -341; end
           -47  : begin quotient <=     -349; end
           -46  : begin quotient <=     -356; end
           -45  : begin quotient <=     -364; end
           -44  : begin quotient <=     -372; end
           -43  : begin quotient <=     -381; end
           -42  : begin quotient <=     -390; end
           -41  : begin quotient <=     -400; end
           -40  : begin quotient <=     -410; end
           -39  : begin quotient <=     -420; end
           -38  : begin quotient <=     -431; end
           -37  : begin quotient <=     -443; end
           -36  : begin quotient <=     -455; end
           -35  : begin quotient <=     -468; end
           -34  : begin quotient <=     -482; end
           -33  : begin quotient <=     -496; end
           -32  : begin quotient <=     -512; end
           -31  : begin quotient <=     -529; end
           -30  : begin quotient <=     -546; end
           -29  : begin quotient <=     -565; end
           -28  : begin quotient <=     -585; end
           -27  : begin quotient <=     -607; end
           -26  : begin quotient <=     -630; end
           -25  : begin quotient <=     -655; end
           -24  : begin quotient <=     -683; end
           -23  : begin quotient <=     -712; end
           -22  : begin quotient <=     -745; end
           -21  : begin quotient <=     -780; end
           -20  : begin quotient <=     -819; end
           -19  : begin quotient <=     -862; end
           -18  : begin quotient <=     -910; end
           -17  : begin quotient <=     -964; end
           -16  : begin quotient <=    -1024; end
           -15  : begin quotient <=    -1092; end
           -14  : begin quotient <=    -1170; end
           -13  : begin quotient <=    -1260; end
           -12  : begin quotient <=    -1365; end
           -11  : begin quotient <=    -1489; end
           -10  : begin quotient <=    -1638; end
            -9  : begin quotient <=    -1820; end
            -8  : begin quotient <=    -2048; end
            -7  : begin quotient <=    -2341; end
            -6  : begin quotient <=    -2731; end
            -5  : begin quotient <=    -3277; end
            -4  : begin quotient <=    -4096; end
            -3  : begin quotient <=    -5461; end
            -2  : begin quotient <=    -8192; end
            -1  : begin quotient <=   -16384; end
             0  : begin quotient <=     0;    end
             1  : begin quotient <=    16384; end
             2  : begin quotient <=     8192; end
             3  : begin quotient <=     5461; end
             4  : begin quotient <=     4096; end
             5  : begin quotient <=     3277; end
             6  : begin quotient <=     2731; end
             7  : begin quotient <=     2341; end
             8  : begin quotient <=     2048; end
             9  : begin quotient <=     1820; end
            10  : begin quotient <=     1638; end
            11  : begin quotient <=     1489; end
            12  : begin quotient <=     1365; end
            13  : begin quotient <=     1260; end
            14  : begin quotient <=     1170; end
            15  : begin quotient <=     1092; end
            16  : begin quotient <=     1024; end
            17  : begin quotient <=      964; end
            18  : begin quotient <=      910; end
            19  : begin quotient <=      862; end
            20  : begin quotient <=      819; end
            21  : begin quotient <=      780; end
            22  : begin quotient <=      745; end
            23  : begin quotient <=      712; end
            24  : begin quotient <=      683; end
            25  : begin quotient <=      655; end
            26  : begin quotient <=      630; end
            27  : begin quotient <=      607; end
            28  : begin quotient <=      585; end
            29  : begin quotient <=      565; end
            30  : begin quotient <=      546; end
            31  : begin quotient <=      529; end
            32  : begin quotient <=      512; end
            33  : begin quotient <=      496; end
            34  : begin quotient <=      482; end
            35  : begin quotient <=      468; end
            36  : begin quotient <=      455; end
            37  : begin quotient <=      443; end
            38  : begin quotient <=      431; end
            39  : begin quotient <=      420; end
            40  : begin quotient <=      410; end
            41  : begin quotient <=      400; end
            42  : begin quotient <=      390; end
            43  : begin quotient <=      381; end
            44  : begin quotient <=      372; end
            45  : begin quotient <=      364; end
            46  : begin quotient <=      356; end
            47  : begin quotient <=      349; end
            48  : begin quotient <=      341; end
            49  : begin quotient <=      334; end
            50  : begin quotient <=      328; end
            51  : begin quotient <=      321; end
            52  : begin quotient <=      315; end
            53  : begin quotient <=      309; end
            54  : begin quotient <=      303; end
            55  : begin quotient <=      298; end
            56  : begin quotient <=      293; end
            57  : begin quotient <=      287; end
            58  : begin quotient <=      282; end
            59  : begin quotient <=      278; end
            60  : begin quotient <=      273; end
            61  : begin quotient <=      269; end
            62  : begin quotient <=      264; end
            63  : begin quotient <=      260; end
            64  : begin quotient <=      256; end
            65  : begin quotient <=      252; end
            66  : begin quotient <=      248; end
            67  : begin quotient <=      245; end
            68  : begin quotient <=      241; end
            69  : begin quotient <=      237; end
            70  : begin quotient <=      234; end
            71  : begin quotient <=      231; end
            72  : begin quotient <=      228; end
            73  : begin quotient <=      224; end
            74  : begin quotient <=      221; end
            75  : begin quotient <=      218; end
            76  : begin quotient <=      216; end
            77  : begin quotient <=      213; end
            78  : begin quotient <=      210; end
            79  : begin quotient <=      207; end
            80  : begin quotient <=      205; end
            81  : begin quotient <=      202; end
            82  : begin quotient <=      200; end
            83  : begin quotient <=      197; end
            84  : begin quotient <=      195; end
            85  : begin quotient <=      193; end
            86  : begin quotient <=      191; end
            87  : begin quotient <=      188; end
            88  : begin quotient <=      186; end
            89  : begin quotient <=      184; end
            90  : begin quotient <=      182; end
            91  : begin quotient <=      180; end
            92  : begin quotient <=      178; end
            93  : begin quotient <=      176; end
            94  : begin quotient <=      174; end
            95  : begin quotient <=      172; end
            96  : begin quotient <=      171; end
            97  : begin quotient <=      169; end
            98  : begin quotient <=      167; end
            99  : begin quotient <=      165; end
           100  : begin quotient <=      164; end
           101  : begin quotient <=      162; end
           102  : begin quotient <=      161; end
           103  : begin quotient <=      159; end
           104  : begin quotient <=      158; end
           105  : begin quotient <=      156; end
           106  : begin quotient <=      155; end
           107  : begin quotient <=      153; end
           108  : begin quotient <=      152; end
           109  : begin quotient <=      150; end
           110  : begin quotient <=      149; end
           111  : begin quotient <=      148; end
           112  : begin quotient <=      146; end
           113  : begin quotient <=      145; end
           114  : begin quotient <=      144; end
           115  : begin quotient <=      142; end
           116  : begin quotient <=      141; end
           117  : begin quotient <=      140; end
           118  : begin quotient <=      139; end
           119  : begin quotient <=      138; end
           120  : begin quotient <=      137; end
           121  : begin quotient <=      135; end
           122  : begin quotient <=      134; end
           123  : begin quotient <=      133; end
           124  : begin quotient <=      132; end
           125  : begin quotient <=      131; end
           126  : begin quotient <=      130; end
           127  : begin quotient <=      129; end    
        endcase
    end
end
 
endmodule
