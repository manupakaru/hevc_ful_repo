
`timescale 1ns / 1ps
module cache_data_mem_dual
(
		clk,
		w_addr_in,
		r_addr_in,
	r_data_out,
	w_data_in,
	w_en_in

);

		`include "../sim/cache_configs_def.v"
		
		//---------------------------------------------------------------------------------------------------------------------
		// parameter definitions
		//---------------------------------------------------------------------------------------------------------------------
		parameter NUM_CACHE_LINES                               = (1<<(SET_ADDR_WDTH+C_N_WAY));
	
		//---------------------------------------------------------------------------------------------------------------------
		// localparam definitions
		//---------------------------------------------------------------------------------------------------------------------
		

		
		//---------------------------------------------------------------------------------------------------------------------
		// I/O signals
		//---------------------------------------------------------------------------------------------------------------------
		

	input                                                  	clk;
	input	[PIXEL_BITS*CACHE_LINE_WDTH-1:0]           		w_data_in;
	input	[SET_ADDR_WDTH+C_N_WAY-1:0]				       	w_addr_in;			
	input	[SET_ADDR_WDTH+C_N_WAY-1:0]				       	r_addr_in;			
	input													w_en_in;
	output  reg [PIXEL_BITS*CACHE_LINE_WDTH-1:0]	     		r_data_out;
	
	
	//---------------------------------------------------------------------------------------------------------------------
		// Internal wires and registers
		//---------------------------------------------------------------------------------------------------------------------
		reg     [PIXEL_BITS*CACHE_LINE_WDTH-1:0]                                r_data_out_reg;
//`ifdef simulate
		reg     [PIXEL_BITS*(CACHE_LINE_WDTH)-1:0]      				        mem [NUM_CACHE_LINES-1:0];      // 48 bytes in cache line
		

	
		// ---------------------------------------------------------------------------------------------------------------------
		// Implmentation
		// ---------------------------------------------------------------------------------------------------------------------
		
		
	always @(posedge clk) begin
			 if(w_en_in) begin
					 mem[w_addr_in] <= w_data_in;
			 end
			 // else begin
					 r_data_out_reg <= mem[r_addr_in];
			 // end
	end	
	
	// assign 	r_data_out = (w_addr_in== r_addr_in) ?	w_data_in: r_data_out_reg;
	always@(*) begin
		if((w_addr_in == r_addr_in) && w_en_in) begin
			r_data_out = w_data_in;
		end
		else begin
			r_data_out = r_data_out_reg;
		end
	end

endmodule