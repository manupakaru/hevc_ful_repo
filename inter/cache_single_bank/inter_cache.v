`timescale 1ns / 1ps
module inter_cache
(
    clk,
    reset,
    luma_ref_start_x_in 	,
	luma_ref_start_y_in    ,
    chma_ref_start_x_in 	,
    chma_ref_start_y_in 	,
    
    res_present_in,
    res_present_out,
    bi_pred_block_cache_in,
    bi_pred_block_cache_out,
    x0_tu_end_in_min_tus_in,
    x0_tu_end_in_min_tus_out,
    y0_tu_end_in_min_tus_in,
    y0_tu_end_in_min_tus_out,

    luma_ref_width_x_in   ,
    chma_ref_width_x_in   ,
    luma_ref_height_y_in  ,
    chma_ref_height_y_in  ,

    luma_ref_start_x_out     ,
    luma_ref_start_y_out    ,
    chma_ref_start_x_out     ,
    chma_ref_start_y_out     ,
    
    luma_ref_width_x_out   ,
    chma_ref_width_x_out   ,
    luma_ref_height_y_out  ,
    chma_ref_height_y_out  ,

    cache_idle_out,
	
    block_x_offset_luma ,
    block_y_offset_luma ,
    block_x_offset_chma ,
    block_y_offset_chma ,
    block_x_end_luma    ,
    block_y_end_luma    ,
    block_x_end_chma    ,
    block_y_end_chma    ,




    pic_width,
    pic_height,

    ch_frac_x   ,
    ch_frac_y   ,
    ch_frac_x_out,
    ch_frac_y_out,

    xT_in_min_tus_in,
    yT_in_min_tus_in,
    xT_in_min_tus_out,
    yT_in_min_tus_out,

    filer_idle_in,
	ref_idx_in_in,
    valid_in,
    block_121_out,
    block_25cb_out,
    block_25cr_out,
    block_ready,
    
    ref_pix_axi_ar_addr,
    ref_pix_axi_ar_len,
    ref_pix_axi_ar_size,
    ref_pix_axi_ar_burst,
    ref_pix_axi_ar_prot,
    ref_pix_axi_ar_valid,
    ref_pix_axi_ar_ready,
    ref_pix_axi_r_data,
    ref_pix_axi_r_resp,
    ref_pix_axi_r_last,
    ref_pix_axi_r_valid,
    ref_pix_axi_r_ready

	,test_cache_addr
	,test_cache_luma_data

);

    `include "../sim/cache_configs_def.v"
    `include "../sim/pred_def.v"
    `include "../sim/inter_axi_def.v"

    //---------------------------------------------------------------------------------------------------------------------
    // parameter definitions
    //---------------------------------------------------------------------------------------------------------------------
    
    parameter                           LUMA_DIM_WDTH		    	= 4;        // out block dimension  max 11
    parameter                           LUMA_DIM_ADDR_WDTH          = 7;        //max 121
    parameter                           CHMA_DIM_WDTH               = 3;        // max 5 
    parameter                           CHMA_DIM_ADDR_WDTH          = 5;        // max 25 
    parameter                           LUMA_REF_BLOCK_WIDTH        = 4'd11;
    parameter                           CHMA_REF_BLOCK_WIDTH        = 3'd5;

    parameter                           CACHE_LINE_LUMA_OFFSET      = 0;
    parameter                           CACHE_LINE_CB_OFFSET      = CACHE_LINE_WDTH * LUMA_BITS;
    parameter                           CACHE_LINE_CR_OFFSET      = CACHE_LINE_CB_OFFSET + ((CACHE_LINE_WDTH * LUMA_BITS)>>2);

    parameter                           REF_PIX_AXI_AX_SIZE  = `AX_SIZE_64;
    parameter                           REF_PIX_AXI_AX_LEN   = `AX_LEN_1;

    
    //---------------------------------------------------------------------------------------------------------------------
    // localparam definitions
    //---------------------------------------------------------------------------------------------------------------------
    
    localparam                          STATE_SET_INPUTS                = 0;    // the state that is entered upon reset
	localparam                          STATE_TAG_READ                  = 1;
    localparam                          STATE_TAG_COMPARE               = 2;
    localparam                          STATE_C_LINE_HIT_FETCH          = 3;
    localparam                          STATE_C_LINE_MISS_FETCH         = 4;
    localparam                          STATE_DEST_FILL_HIT             = 5;
    localparam                          STATE_CACHE_READY               = 6;
    localparam                          STATE_MISS_AR_REDY_WAIT         = 7;
    localparam                          STATE_DEST_FILL_MIS		        = 8;


    //---------------------------------------------------------------------------------------------------------------------
    // I/O signals
    //---------------------------------------------------------------------------------------------------------------------
    
    
    input                                           clk;
    input                                           reset;
                
    // inter prediction filter interface            
    input                                           valid_in;
    output	reg							            block_ready;   // assuming cache_block_ready is single cylce
	output	reg										cache_idle_out;
    input                                           filer_idle_in;

    input res_present_in;
    output reg res_present_out;

    input bi_pred_block_cache_in;
    output reg bi_pred_block_cache_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      x0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      y0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus_in;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus_out;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus_out;

	
	input  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] 	luma_ref_start_x_in;	
	input  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] 	luma_ref_start_y_in;
	input  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] 	chma_ref_start_x_in;	
	input  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] 	chma_ref_start_y_in;	
	
	input  [LUMA_DIM_WDTH - 1:0]   			chma_ref_width_x_in            ;	
    input  [LUMA_DIM_WDTH - 1:0]            chma_ref_height_y_in           ;   
	input  [LUMA_DIM_WDTH - 1:0]   			luma_ref_width_x_in            ;	
    input  [LUMA_DIM_WDTH - 1:0]            luma_ref_height_y_in           ;   


    output reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  luma_ref_start_x_out   ;
    output reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  luma_ref_start_y_out   ;
    output reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  chma_ref_start_x_out   ;
    output reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  chma_ref_start_y_out   ;
                    
    output reg  [LUMA_DIM_WDTH - 1:0]                   chma_ref_width_x_out   ;
    output reg  [LUMA_DIM_WDTH - 1:0]                   chma_ref_height_y_out  ;
    output reg  [LUMA_DIM_WDTH - 1:0]                   luma_ref_width_x_out   ;
    output reg  [LUMA_DIM_WDTH - 1:0]                   luma_ref_height_y_out  ;


    input  [MV_C_FRAC_WIDTH_HIGH -1:0]      ch_frac_x;
    input  [MV_C_FRAC_WIDTH_HIGH -1:0]      ch_frac_y;
    output reg  [MV_C_FRAC_WIDTH_HIGH -1:0]      ch_frac_x_out;
    output reg  [MV_C_FRAC_WIDTH_HIGH -1:0]      ch_frac_y_out;

	input			[REF_ADDR_WDTH-1:0]		            ref_idx_in_in;
	
    input  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   pic_width;   
    input  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   pic_height;   
	
	
    output [LUMA_BITS* LUMA_REF_BLOCK_WIDTH* LUMA_REF_BLOCK_WIDTH -1:0]     block_121_out;
    output [CHMA_BITS* CHMA_REF_BLOCK_WIDTH* CHMA_REF_BLOCK_WIDTH -1:0]     block_25cb_out;
    output [CHMA_BITS* CHMA_REF_BLOCK_WIDTH* CHMA_REF_BLOCK_WIDTH -1:0]     block_25cr_out;



               
    // axi master interface         
    output	reg [AXI_ADDR_WDTH-1:0]		            ref_pix_axi_ar_addr;
    output wire [7:0]					            ref_pix_axi_ar_len;
    output wire	[2:0]					            ref_pix_axi_ar_size;
    output wire [1:0]					            ref_pix_axi_ar_burst;
    output wire [2:0]					            ref_pix_axi_ar_prot;
    output reg							            ref_pix_axi_ar_valid;
    input 								            ref_pix_axi_ar_ready;
                
    input		[AXI_CACHE_DATA_WDTH-1:0]		    ref_pix_axi_r_data;

    input		[1:0]					            ref_pix_axi_r_resp;
    input 								            ref_pix_axi_r_last;
    input								            ref_pix_axi_r_valid;
    output  							            ref_pix_axi_r_ready;
		
    output reg         [LUMA_DIM_WDTH-1:0]                 block_x_offset_luma;
    output reg         [LUMA_DIM_WDTH-1:0]                 block_y_offset_luma;
    output reg         [CHMA_DIM_WDTH-1:0]                 block_x_offset_chma;
    output reg         [CHMA_DIM_WDTH-1:0]                 block_y_offset_chma; 

    output reg         [LUMA_DIM_WDTH-1:0]                 block_x_end_luma;
    output reg         [LUMA_DIM_WDTH-1:0]                 block_y_end_luma;
    output reg         [CHMA_DIM_WDTH-1:0]                 block_x_end_chma;
    output reg         [CHMA_DIM_WDTH-1:0]                 block_y_end_chma; 

	output [7-1:0] test_cache_addr;
	output [256-1:0] test_cache_luma_data;	
	
    integer next_state, state;
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   luma_ref_end_x = luma_ref_start_x_in + luma_ref_width_x_in  ;   
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   luma_ref_end_y = luma_ref_start_y_in + luma_ref_height_y_in ;
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   chma_ref_end_x = chma_ref_start_x_in + chma_ref_width_x_in;   
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   chma_ref_end_y = chma_ref_start_y_in + chma_ref_height_y_in ;		
		
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   luma_start_x_rel_pic_width =  pic_width  - 1'b1 -      luma_ref_start_x_in;   
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   luma_start_y_rel_pic_height = pic_height - 1'b1 -      luma_ref_start_y_in;
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   chma_start_x_rel_pic_width =  pic_width  - 1'b1 -     chma_ref_start_x_in;  
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   chma_start_y_rel_pic_height = pic_height - 1'b1 -     chma_ref_start_y_in;      

    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   luma_start_x_rel_0 =  0  -        luma_ref_start_x_in;   
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   luma_start_y_rel_0 = 0   -        luma_ref_start_y_in;
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   chma_start_x_rel_0 =  0  -       chma_ref_start_x_in;  
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   chma_start_y_rel_0 = 0   -       chma_ref_start_y_in;      
  

    reg         [REF_ADDR_WDTH-1:0]                 ref_idx_in;
    reg         [LUMA_DIM_WDTH-1:0]                 rf_blk_hgt_in;
    reg         [LUMA_DIM_WDTH-1:0]                 rf_blk_wdt_in;
    reg         [CHMA_DIM_WDTH-1:0]                 rf_blk_hgt_ch;
    reg         [CHMA_DIM_WDTH-1:0]                 rf_blk_wdt_ch;       


    wire    [1:0]                                   delta_x;    // possible 0,1,2
    wire    [1:0]                                   delta_y;    //possible 0,1,2,3
 
    wire    [1:0]                                   delta_x_luma;    // possible 0,1,2
    wire    [1:0]                                   delta_y_luma;    //possible 0,1,2,3   
    wire                                            delta_x_chma; // possible 0,1,2
    wire    [1:0]                                   delta_y_chma; //possible 0,1,2,3

        		
    reg       [X_ADDR_WDTH-1:0]                     start_great_x_in;
    reg       [Y_ADDR_WDTH-1:0]                     start_great_y_in;
    reg       [LUMA_DIM_WDTH-1:0]                   rf_blk_great_hgt_in;
    reg       [LUMA_DIM_WDTH-1:0]                   rf_blk_great_wdt_in;    
    
    reg       [X_ADDR_WDTH-1:0]                     start_x_in;
    reg       [Y_ADDR_WDTH-1:0]                     start_y_in;
    reg       [X_ADDR_WDTH-2:0]                     start_x_ch; //% value after division by two
    reg       [Y_ADDR_WDTH-2:0]                     start_y_ch;

    reg     [1:0]                                   curr_x; // possible 0,1,2
    reg     [1:0]                                   curr_y; // possible 0,1,2,3

    reg     [1:0]                                   curr_x_d; // possible 0,1,2
    reg     [1:0]                                   curr_y_d; // possible 0,1,2,3

    reg     [1:0]                                   curr_x_luma; // possible 0,1,2
    reg     [1:0]                                   curr_y_luma; // possible 0,1,2,3

    reg     [1:0]                                   curr_x_chma; // possible 0,1,2
    reg     [1:0]                                   curr_y_chma; // possible 0,1,2,3

    reg    [SET_ADDR_WDTH -1:0]                     set_addr;
    reg    [SET_ADDR_WDTH -1:0]                     set_addr_d;



    wire    [X_ADDR_WDTH - C_L_H_SIZE -1: 0]          curr_x_addr;
    wire    [Y_ADDR_WDTH - C_L_V_SIZE -1: 0]          curr_y_addr;

    reg    [X_ADDR_WDTH - C_L_H_SIZE -1: 0]          curr_x_addr_reg;
    reg    [Y_ADDR_WDTH - C_L_V_SIZE -1: 0]          curr_y_addr_reg;
    reg    [X_ADDR_WDTH - C_L_H_SIZE -1: 0]          curr_x_addr_reg_1;
    reg    [Y_ADDR_WDTH - C_L_V_SIZE -1: 0]          curr_y_addr_reg_1;
    reg     [TAG_ADDR_WDTH-1:0]                     tag_addr;

    wire    [(1<<C_N_WAY)-1:0]                      set_vld_bits;
    wire    [(1<<C_N_WAY)*TAG_ADDR_WDTH-1:0]        tag_rdata_set;
    wire                                            is_hit;
    reg                                             is_hit_d;
    wire    [C_N_WAY-1:0]                           set_idx;
    reg     [C_N_WAY-1:0]                           set_idx_d;
    wire    [C_N_WAY-1:0]                           set_idx_miss;
    reg     [C_N_WAY-1:0]                           set_idx_miss_d;

    reg     [SET_ADDR_WDTH+C_N_WAY-1:0]             cache_addr;
    wire    [PIXEL_BITS*CACHE_LINE_WDTH-1:0]        cache_rdata;
    reg                                             cache_wr_en;

    wire    [(1<<C_N_WAY)*C_N_WAY-1:0]              age_val_set;
    wire    [(1<<C_N_WAY)*C_N_WAY-1:0]              new_age_set;

    reg     luma_dest_enable_wire;
    reg     luma_dest_enable_reg;
    reg     chma_dest_enable_wire;
    reg     chma_dest_enable_reg;

    reg     [1:0]                                   next_curr_x_luma; // possible 0,1,2
    reg     [1:0]                                   next_curr_y_luma; // possible 0,1,2,3

    reg     [1:0]                                   next_curr_x_chma; // possible 0,1,2
    reg     [1:0]                                   next_curr_y_chma; // possible 0,1,2,3

    reg     [1:0]                                   next_curr_x; // possible 0,1,2
    reg     [1:0]                                   next_curr_y; // possible 0,1,2,3

    integer i,j,k;          
    
    wire    [C_L_H_SIZE-1:0]                        cl_strt_x_luma;
    reg     [C_L_H_SIZE-1:0]                        cl_strt_x_luma_d;
    wire    [C_L_H_SIZE_C-1:0]                      cl_strt_x_chma;
    reg     [C_L_H_SIZE_C-1:0]                      cl_strt_x_chma_d;
    wire    [C_L_V_SIZE-1:0]                        cl_strt_y_luma;
    reg     [C_L_V_SIZE-1:0]                        cl_strt_y_luma_d;
    wire    [C_L_V_SIZE_C-1:0]                      cl_strt_y_chma;
    reg     [C_L_V_SIZE_C-1:0]                      cl_strt_y_chma_d;
    
    //wire    [C_L_H_SIZE-1:0]                        cl_end__x_luma;    
    //reg     [C_L_H_SIZE-1:0]                        cl_end__x_luma_d;    
    //wire    [C_L_H_SIZE_C-1:0]                      cl_end__x_chma;
    //reg     [C_L_H_SIZE_C-1:0]                      cl_end__x_chma_d;

    //wire    [C_L_H_SIZE-1:0]                        cl_end__y_luma;    
    //reg     [C_L_H_SIZE-1:0]                        cl_end__y_luma_d;    
    //wire    [C_L_H_SIZE_C-1:0]                      cl_end__y_chma;
    //reg     [C_L_H_SIZE_C-1:0]                      cl_end__y_chma_d;

    reg     [LUMA_DIM_WDTH-1:0] next_dst_strt_x_luma;
    reg     [LUMA_DIM_WDTH-1:0] next_dest_end_x_luma;
    reg     [LUMA_DIM_WDTH-1:0] next_dst_strt_y_luma;
    reg     [LUMA_DIM_WDTH-1:0] next_dest_end_y_luma;


    reg     [LUMA_DIM_WDTH-1:0] dst_strt_x_luma;
    reg     [LUMA_DIM_WDTH-1:0] dest_end_x_luma;
    reg     [LUMA_DIM_WDTH-1:0] dst_strt_y_luma;
    reg     [LUMA_DIM_WDTH-1:0] dest_end_y_luma;


    reg     [CHMA_DIM_WDTH-1:0] next_dst_strt_x_chma;
    reg     [CHMA_DIM_WDTH-1:0] next_dest_end_x_chma;
    reg     [CHMA_DIM_WDTH-1:0] next_dst_strt_y_chma;
    reg     [CHMA_DIM_WDTH-1:0] next_dest_end_y_chma;


    reg     [CHMA_DIM_WDTH-1:0] dst_strt_x_chma;
    reg     [CHMA_DIM_WDTH-1:0] dest_end_x_chma;
    reg     [CHMA_DIM_WDTH-1:0] dst_strt_y_chma;
    reg     [CHMA_DIM_WDTH-1:0] dest_end_y_chma;

    reg  [C_L_H_SIZE -1:0] dest_fill_x_loc_luma[LUMA_REF_BLOCK_WIDTH -1:0];
    reg  [C_L_V_SIZE -1:0] dest_fill_y_loc_luma[LUMA_REF_BLOCK_WIDTH -1:0];

    reg                    dest_fill_x_mask_luma[LUMA_REF_BLOCK_WIDTH -1:0];
    reg                    dest_fill_y_mask_luma[LUMA_REF_BLOCK_WIDTH -1:0];

    reg  [C_L_H_SIZE_C -1:0] dest_fill_x_loc_chma[CHMA_REF_BLOCK_WIDTH -1:0];
    reg  [C_L_V_SIZE_C -1:0] dest_fill_y_loc_chma[CHMA_REF_BLOCK_WIDTH -1:0];

    reg                      dest_fill_x_mask_chma[CHMA_REF_BLOCK_WIDTH -1:0];
    reg                      dest_fill_y_mask_chma[CHMA_REF_BLOCK_WIDTH -1:0];

    reg  [LUMA_BITS-1:0]     block_11x11 [0:LUMA_REF_BLOCK_WIDTH -1][0:LUMA_REF_BLOCK_WIDTH -1];
    reg  [CHMA_BITS-1:0]     block_5x5_cb[0:CHMA_REF_BLOCK_WIDTH -1][0:CHMA_REF_BLOCK_WIDTH -1];
    reg  [CHMA_BITS-1:0]     block_5x5_cr[0:CHMA_REF_BLOCK_WIDTH -1][0:CHMA_REF_BLOCK_WIDTH -1];

    reg age_wr_en;


    wire [CTB_SIZE_WIDTH - C_L_V_SIZE-1:0] 		bu_idx 		;
    wire [CTB_SIZE_WIDTH - C_L_H_SIZE-1:0] 		bu_row_idx 	;
    wire [X_ADDR_WDTH -CTB_SIZE_WIDTH-1:0]  	iu_idx 		= {curr_x_addr_reg[X_ADDR_WDTH - C_L_H_SIZE -1:CTB_SIZE_WIDTH - C_L_H_SIZE]};
    wire [Y_ADDR_WDTH -CTB_SIZE_WIDTH-1:0]  	iu_row_idx 	= {curr_y_addr_reg[Y_ADDR_WDTH - C_L_V_SIZE -1:CTB_SIZE_WIDTH - C_L_V_SIZE]};

    reg [15: 0]  bu_idx_val;
    reg [20: 0]  iu_idx_val;
    reg [25: 0]  iu_idx_row_val;
    reg [31: 0]  ref_idx_val;

    reg luma_dest_enable_reg_d;
    reg chma_dest_enable_reg_d;
    
    reg luma_dest_enable_x,luma_dest_enable_y;
    reg chma_dest_enable_x,chma_dest_enable_y;   

    reg cur_xy_changed_luma, cur_xy_changed_chma;
    
    wire [LUMA_BITS-1:0] cache_w_data_arr [(PIXEL_BITS*CACHE_LINE_WDTH/LUMA_BITS)-1:0];
    wire [LUMA_BITS-1:0] cache_rdata_arr    [(PIXEL_BITS*CACHE_LINE_WDTH/LUMA_BITS)-1:0];
	
	wire		[CACHE_LINE_WDTH*PIXEL_BITS -1:0]		    cache_w_port;
	reg 		[CACHE_LINE_WDTH*PIXEL_BITS/2 -1:0]		    cache_w_port_old_reg;

	generate  
		if(C_SIZE == 13) begin
			assign cache_w_port = {
			            ref_pix_axi_r_data	[CACHE_LINE_WDTH*PIXEL_BITS/2-1:CACHE_LINE_CR_OFFSET/2],
									cache_w_port_old_reg[CACHE_LINE_WDTH*PIXEL_BITS/2-1:CACHE_LINE_CR_OFFSET/2],			            
			            ref_pix_axi_r_data	[CACHE_LINE_CR_OFFSET/2-1:CACHE_LINE_CB_OFFSET/2],
									cache_w_port_old_reg[CACHE_LINE_CR_OFFSET/2-1:CACHE_LINE_CB_OFFSET/2],
									ref_pix_axi_r_data[CACHE_LINE_CB_OFFSET/2 -1:0],cache_w_port_old_reg[CACHE_LINE_CB_OFFSET/2-1:0]};
			// assign cache_w_port = {ref_pix_axi_r_data[CACHE_LINE_WDTH*PIXEL_BITS -1:AXI_CACHE_DATA_WDTH/2],ref_pix_axi_r_data[CACHE_LINE_WDTH*PIXEL_BITS/2 -1:0]};
			assign                  ref_pix_axi_ar_len    = `AX_LEN_2;
			assign                  ref_pix_axi_ar_size   = `AX_SIZE_64;
			assign 					bu_idx 		= {curr_x_addr_reg[2:0]};
			assign 					bu_row_idx 	= {curr_y_addr_reg[2:0]};
		end
		else begin
			assign cache_w_port = ref_pix_axi_r_data[CACHE_LINE_WDTH*PIXEL_BITS -1:0];
			assign                  ref_pix_axi_ar_len    = `AX_LEN_1;
			assign                  ref_pix_axi_ar_size   = `AX_SIZE_64;
			assign 					bu_idx 		= {curr_x_addr_reg[2:0],curr_y_addr_reg[0]};
			assign 					bu_row_idx 	= {curr_y_addr_reg[3:1]};
		end
	endgenerate
		

    num_val_clines_generator num_val_clines_block_luma (
    .start_x_in(start_x_in[C_L_H_SIZE+LUMA_DIM_WDTH-1:0]), 
    .start_y_in(start_y_in[C_L_V_SIZE+LUMA_DIM_WDTH-1:0]), 
    .rf_blk_wdt_in(rf_blk_wdt_in), 
    .rf_blk_hgt_in(rf_blk_hgt_in), 
    .delta_x_out(delta_x_luma), 
    .delta_y_out(delta_y_luma)
    );
    
    
    num_val_clines_generator_ch num_val_clines_block_chma (
    .start_x_in(start_x_ch[C_L_H_SIZE_C+CHMA_DIM_WDTH-1:0]), 
    .start_y_in(start_y_ch[C_L_V_SIZE_C+CHMA_DIM_WDTH-1:0]), 
    .rf_blk_wdt_in(rf_blk_wdt_ch), 
    .rf_blk_hgt_in(rf_blk_hgt_ch), 
    .delta_x_out(delta_x_chma), 
    .delta_y_out(delta_y_chma)
    );

    num_val_clines_generator num_val_clines_block_great (
    .start_x_in(start_great_x_in[C_L_H_SIZE+LUMA_DIM_WDTH-1:0]), 
    .start_y_in(start_great_y_in[C_L_V_SIZE+LUMA_DIM_WDTH-1:0]), 
    .rf_blk_wdt_in(rf_blk_great_wdt_in), 
    .rf_blk_hgt_in(rf_blk_great_hgt_in), 
    .delta_x_out(delta_x), 
    .delta_y_out(delta_y)
    );


	tag_memory tag_block (
    .clk(clk), 
    .reset(reset), 
    .r_addr_in(set_addr), 
    .w_addr_in(cache_addr), 
    .r_data_out(tag_rdata_set), 
    .w_data_in(tag_addr), 
    .w_en_in(cache_wr_en), 
    .valid_bits_out(set_vld_bits)
    );
	

	age_memory age_block (
    .clk(clk), 
    .reset(reset), 
    .r_addr_in(set_addr_d), 
    .w_addr_in(set_addr_d), 
    .r_data_out(age_val_set), 
    .w_data_in(new_age_set), 
    .w_en_in(age_wr_en)
    );


    compare_tags tag_compare_block (
    .tags_set_in(tag_rdata_set), 
    .the_tag_in(tag_addr), 
    .ishit(is_hit), 
    .set_idx(set_idx),
    .valid_bits_in(set_vld_bits)
    );
    
    new_age_converter age_conv_block (
    .ishit_in(is_hit_d), 
    .set_idx_in(set_idx_d), 
    .age_vals_in(age_val_set), 
    .new_age_vals_out(new_age_set), 
    .set_idx_miss_bnk_out(set_idx_miss)
    );    

    cache_data_mem cache_mem_block(
    .clk(clk), 
    .addr_in(cache_addr), 
    .r_data_out(cache_rdata), 
    .w_data_in(cache_w_port[CACHE_LINE_WDTH*PIXEL_BITS -1:0]), 
    .w_en_in(cache_wr_en)
    );

	assign test_cache_addr = cache_addr;
	assign test_cache_luma_data = cache_rdata[512-1:0];	
	
	
    assign                  ref_pix_axi_ar_burst  = `AX_BURST_INC;
//    assign                  mv_pref_axi_arlock   = `AX_LOCK_DEFAULT;
//    assign                  mv_pref_axi_arcache  = `AX_CACHE_DEFAULT;
    assign                  ref_pix_axi_ar_prot   = `AX_PROT_DATA;    
    assign                  ref_pix_axi_r_ready = 1;
    

assign curr_x_addr = curr_x + (start_great_x_in >> C_L_H_SIZE); 
assign curr_y_addr = curr_y + (start_great_y_in >> C_L_V_SIZE); 
		
assign cl_strt_x_luma    = (curr_x_luma == 2'b00)                ? start_x_in[C_L_H_SIZE-1:0]                                : {C_L_H_SIZE{1'b0}};
//assign cl_end__x_luma    = (curr_x_luma == delta_x_luma)         ? start_x_in[C_L_H_SIZE-1:0] + rf_blk_wdt_in[C_L_H_SIZE-1:0]: {C_L_H_SIZE{1'b1}};

assign cl_strt_y_luma    = (curr_y_luma == 2'b00)                ? start_y_in[C_L_V_SIZE-1:0]                                : {C_L_V_SIZE{1'b0}};
//assign cl_end__y_luma    = (curr_y_luma == delta_y_luma)         ? start_y_in[C_L_H_SIZE-1:0] + rf_blk_hgt_in[C_L_H_SIZE-1:0]: {C_L_H_SIZE{1'b1}};

assign cl_strt_x_chma    = (curr_x_chma == 0)                   ? start_x_ch[C_L_H_SIZE_C-1:0]                                  : {C_L_H_SIZE_C{1'b0}};
//assign cl_end__x_chma    = (curr_x_chma == delta_x_chma)        ? start_x_ch[C_L_H_SIZE_C-1:0] + rf_blk_wdt_ch[C_L_H_SIZE_C-1:0]: {C_L_H_SIZE_C{1'b1}};

assign cl_strt_y_chma    = (curr_y_chma == 0)                   ? start_y_ch[C_L_V_SIZE_C-1:0]                                  : {C_L_V_SIZE_C{1'b0}};
//assign cl_end__y_chma    = (curr_y_chma == delta_y_chma)        ? start_y_ch[C_L_H_SIZE_C-1:0] + rf_blk_hgt_ch[C_L_H_SIZE_C-1:0]: {C_L_H_SIZE_C{1'b1}};
 		

        
        
    generate
        genvar ii;
        genvar jj;
        
        for(jj=0;jj <LUMA_REF_BLOCK_WIDTH ; jj=jj+1 ) begin : row_iteration
            for(ii=0 ; ii < LUMA_REF_BLOCK_WIDTH ; ii = ii+1) begin : column_iteration
                assign  block_121_out[(jj*LUMA_REF_BLOCK_WIDTH + ii +1)*(LUMA_BITS)-1: (jj*LUMA_REF_BLOCK_WIDTH + ii)*LUMA_BITS ] =  block_11x11[jj][ii];
            end
        end
        
        for(jj=0;jj <CHMA_REF_BLOCK_WIDTH ; jj=jj+1 ) begin
            for(ii=0 ; ii < CHMA_REF_BLOCK_WIDTH ; ii = ii+1) begin
                assign    block_25cb_out[(jj*CHMA_REF_BLOCK_WIDTH + ii +1)*(CHMA_BITS)-1: (jj*CHMA_REF_BLOCK_WIDTH + ii)*CHMA_BITS] = block_5x5_cb[jj][ii];
            end
        end
        
        for(jj=0;jj <CHMA_REF_BLOCK_WIDTH ; jj=jj+1 ) begin
            for(ii=0 ; ii < CHMA_REF_BLOCK_WIDTH ; ii = ii+1) begin
                assign    block_25cr_out[(jj*CHMA_REF_BLOCK_WIDTH + ii +1)*(CHMA_BITS)-1: (jj*CHMA_REF_BLOCK_WIDTH + ii)*CHMA_BITS] = block_5x5_cr[jj][ii];
            end
        end
        
        for(ii=0;ii < ((PIXEL_BITS*CACHE_LINE_WDTH/LUMA_BITS)); ii=ii+1) begin
            assign    cache_w_data_arr[ii] = cache_w_port[LUMA_BITS*(ii+1)-1:LUMA_BITS*ii];
        end
        for(ii=0; ii<(PIXEL_BITS*CACHE_LINE_WDTH/LUMA_BITS) ; ii=ii+1) begin
            assign    cache_rdata_arr[ii] = cache_rdata[LUMA_BITS*(ii+1)-1:LUMA_BITS*ii];
        end
               
    endgenerate

    

                        


always@(*) begin
    if(curr_x_chma == 0) begin
        next_dst_strt_x_chma = block_x_offset_chma;
        if(curr_x_chma == delta_x_chma) begin
            next_dest_end_x_chma = block_x_offset_chma + rf_blk_wdt_ch;
        end
        else begin
            next_dest_end_x_chma = {1'b0,({C_L_H_SIZE_C{1'b1}} - start_x_ch[C_L_H_SIZE_C-1:0])} + block_x_offset_chma;
        end
    end
    else begin
        next_dst_strt_x_chma = dest_end_x_chma + 1'b1;   // max 4
        if(curr_x_chma == delta_x_chma) begin
            next_dest_end_x_chma = block_x_offset_chma + rf_blk_wdt_ch;
        end
        else begin
            next_dest_end_x_chma = dest_end_x_chma + {1'b1,{C_L_H_SIZE_C{1'b0}}};
        end
    end
    if(curr_x_luma == 0) begin
        next_dst_strt_x_luma = block_x_offset_luma;
        if(curr_x_luma == delta_x_luma) begin
            next_dest_end_x_luma = block_x_offset_luma + rf_blk_wdt_in;
        end
        else begin
            next_dest_end_x_luma = {1'b0,({C_L_H_SIZE{1'b1}} - start_x_in[C_L_H_SIZE-1:0])} + block_x_offset_luma;
        end
    end
    else begin
        next_dst_strt_x_luma = dest_end_x_luma + 1'b1;
        if(curr_x_luma == delta_x_luma) begin
            next_dest_end_x_luma = block_x_offset_luma + rf_blk_wdt_in;
        end
        else begin
            next_dest_end_x_luma = dest_end_x_luma + {1'b1,{C_L_H_SIZE{1'b0}}}; 
        end
    end
    if(curr_y_chma == 0) begin
        next_dst_strt_y_chma = block_y_offset_chma;
        if(curr_y_chma == delta_y_chma) begin
            next_dest_end_y_chma = block_y_offset_chma + rf_blk_hgt_ch;
        end
        else begin
            next_dest_end_y_chma = {1'b0,({C_L_V_SIZE_C{1'b1}} - start_y_ch[C_L_V_SIZE_C-1:0])} + block_y_offset_chma;
        end
    end
    else if(curr_x_chma == 0)begin
        next_dst_strt_y_chma = dest_end_y_chma + 1'b1;  
        if(curr_y_chma == delta_y_chma) begin
            next_dest_end_y_chma = block_y_offset_chma + rf_blk_hgt_ch;
        end
        else begin 
            next_dest_end_y_chma = dest_end_y_chma + {1'b1,{C_L_V_SIZE_C{1'b0}}};
        end
    end
    else begin
        next_dst_strt_y_chma = dst_strt_y_chma;
        next_dest_end_y_chma = dest_end_y_chma;        
    end
    if(curr_y_luma == 0) begin
        next_dst_strt_y_luma = block_y_offset_luma;
        if(curr_y_luma == delta_y_luma) begin
            next_dest_end_y_luma = block_y_offset_luma + rf_blk_hgt_in;
        end
        else begin
            next_dest_end_y_luma = {1'b0,({C_L_V_SIZE{1'b1}} - start_y_in[C_L_V_SIZE-1:0])} + block_y_offset_luma;
        end
    end
    else if(curr_x_luma == 0)begin
        next_dst_strt_y_luma = dest_end_y_luma + 1'b1;
        if(curr_y_luma == delta_y_luma) begin
            next_dest_end_y_luma = block_y_offset_luma + rf_blk_hgt_in;
        end
        else begin
            next_dest_end_y_luma = dest_end_y_luma + {1'b1,{C_L_V_SIZE{1'b0}}}; 
        end
    end
    else begin
        next_dst_strt_y_luma = dst_strt_y_luma;
        next_dest_end_y_luma = dest_end_y_luma;
    end
end

always @(* ) begin
    if ( ((curr_x_addr_reg << C_L_H_SIZE) <= (start_x_in + rf_blk_wdt_in) ) && (( (curr_x_addr_reg_1) << C_L_H_SIZE) > start_x_in )) begin
        luma_dest_enable_x = 1;
    end
    else begin
        luma_dest_enable_x = 0;
    end
end

always @(* ) begin
    if ( ((curr_y_addr_reg << C_L_V_SIZE) <= (start_y_in  + rf_blk_hgt_in) ) && (( (curr_y_addr_reg_1) << C_L_V_SIZE) > start_y_in )) begin
        luma_dest_enable_y = 1;
    end
    else begin
        luma_dest_enable_y = 0;
    end
end
always@(*) begin
    if(luma_dest_enable_x & luma_dest_enable_y ) begin
        luma_dest_enable_wire = 1;
    end
    else begin
        luma_dest_enable_wire = 0;
    end
end

always @(*) begin
    if ( ((curr_x_addr_reg << C_L_H_SIZE_C) <= (start_x_ch + rf_blk_wdt_ch) ) && (( (curr_x_addr_reg_1) << C_L_H_SIZE_C) > start_x_ch )) begin
        chma_dest_enable_x = 1;
    end
    else begin
        chma_dest_enable_x = 0;
    end
end

always @(*) begin
    if ( ((curr_y_addr_reg << C_L_V_SIZE_C) <= (start_y_ch  + rf_blk_hgt_ch) ) && (( (curr_y_addr_reg_1) << C_L_V_SIZE_C) > start_y_ch )) begin
        chma_dest_enable_y = 1;
    end
    else begin
        chma_dest_enable_y = 0;
    end
end
always@(*) begin
    if( chma_dest_enable_x && chma_dest_enable_y) begin
        chma_dest_enable_wire = 1;
    end
    else begin
        chma_dest_enable_wire = 0;
    end
end

always@(*) begin
    if(curr_x == delta_x) begin
        next_curr_x = 0;
        next_curr_y = curr_y + 1'b1;
    end
    else begin
        next_curr_x = curr_x + 1'b1;
        next_curr_y = curr_y;
    end
end

always@(*) begin
    if(curr_x_luma == delta_x_luma) begin
        next_curr_x_luma = 0;
        next_curr_y_luma = curr_y_luma + 1'b1;
    end
    else begin
        next_curr_x_luma = curr_x_luma + 1'b1;
        next_curr_y_luma = curr_y_luma;
    end
end

always@(*) begin
    if(curr_x_chma == delta_x_chma) begin
        next_curr_x_chma = 0;
        next_curr_y_chma = curr_y_chma + 1'b1;
    end
    else begin
        next_curr_x_chma = curr_x_chma + 1'b1;
        next_curr_y_chma = curr_y_chma;
    end
end

always@(posedge clk) begin
    luma_dest_enable_reg_d <= luma_dest_enable_reg;
    chma_dest_enable_reg_d <= chma_dest_enable_reg; 
    curr_x_d <= curr_x;
    curr_y_d <= curr_y;
end

always@(*) begin
    block_ready = 0;
	next_state = state;
	cache_idle_out = 0;
    set_addr = {SET_ADDR_WDTH{1'bx}};
    age_wr_en = 0;
    cache_wr_en = 0;
    cache_addr = {(SET_ADDR_WDTH+C_N_WAY) {1'bx}};
	case(state) 
		STATE_SET_INPUTS: begin
			if(valid_in) begin
				next_state = STATE_TAG_READ;
			end
            cache_idle_out = 1;
		end
		STATE_TAG_READ: begin
            next_state = STATE_TAG_COMPARE;
            set_addr = {curr_y_addr[1:0],curr_x_addr[1:0]};  
        end
        STATE_TAG_COMPARE: begin
            if(is_hit) begin
                next_state = STATE_C_LINE_HIT_FETCH;
            end
            else begin
                next_state = STATE_C_LINE_MISS_FETCH;
            end        
        end
        STATE_C_LINE_HIT_FETCH: begin
            cache_addr = {set_addr_d,set_idx_d};
            next_state = STATE_DEST_FILL_HIT;
            age_wr_en = 1;
        end
        STATE_DEST_FILL_HIT: begin
			set_addr = {curr_y_addr[1:0],curr_x_addr[1:0]};
			if(curr_x_d == delta_x && curr_y_d == delta_y) begin
				next_state = STATE_CACHE_READY;
			end
			else begin  
				next_state = STATE_TAG_COMPARE;
			end
        end
		STATE_DEST_FILL_MIS: begin
			if(ref_pix_axi_r_valid) begin
				if(ref_pix_axi_r_last) begin
					cache_wr_en = 1;
					cache_addr = {set_addr_d,set_idx_miss_d};
					if(curr_x == delta_x && curr_y == delta_y) begin
						next_state = STATE_CACHE_READY;
					end
					else begin
						next_state = STATE_TAG_READ;
					end
				end
			end
		end
        STATE_CACHE_READY: begin
            if(filer_idle_in) begin
                next_state = STATE_SET_INPUTS;
                block_ready  = 1;
            end
        end
        STATE_C_LINE_MISS_FETCH: begin
            next_state = STATE_MISS_AR_REDY_WAIT;
            age_wr_en = 1;
        end
        STATE_MISS_AR_REDY_WAIT: begin
            if(ref_pix_axi_ar_ready) begin
                next_state = STATE_DEST_FILL_MIS;
            end
        end
	endcase
end 


always@(posedge clk) begin
    if(reset) begin
        curr_x <= 0;
        curr_y <= 0;
        ref_pix_axi_ar_valid <= 0;
        ref_pix_axi_ar_addr <= 0;
    end
	case(state)
		STATE_SET_INPUTS: begin
			if(valid_in) begin
                xT_in_min_tus_out <= xT_in_min_tus_in;
                yT_in_min_tus_out <= yT_in_min_tus_in;

                res_present_out <= res_present_in;
                bi_pred_block_cache_out <= bi_pred_block_cache_in;
                x0_tu_end_in_min_tus_out <= x0_tu_end_in_min_tus_in;
                y0_tu_end_in_min_tus_out <= y0_tu_end_in_min_tus_in;

                luma_ref_start_x_out    <=  luma_ref_start_x_in  ;
                luma_ref_start_y_out    <=  luma_ref_start_y_in  ;   
                chma_ref_start_x_out    <=  chma_ref_start_x_in >>>1 ;       
                chma_ref_start_y_out    <=  chma_ref_start_y_in >>>1 ;       
                chma_ref_width_x_out    <=  chma_ref_width_x_in >>1 ;   
                chma_ref_height_y_out   <=  chma_ref_height_y_in >>1;       
                luma_ref_width_x_out    <=  luma_ref_width_x_in  ;       
                luma_ref_height_y_out   <=  luma_ref_height_y_in ;   
                ch_frac_x_out <= ch_frac_x;
                ch_frac_y_out <= ch_frac_y;    

				if(luma_ref_start_x_in[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] == 1) begin
					start_x_in <= 0;
				end
				else if(luma_ref_start_x_in >= pic_width)  begin
                    start_x_in <= pic_width -1;
                end
                else begin
					start_x_in <= luma_ref_start_x_in[X_ADDR_WDTH - 1:0];
				end

				if(luma_ref_start_y_in[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] == 1) begin
					start_y_in <= 0;
				end
				else if(luma_ref_start_y_in >= pic_height) begin
                    start_y_in <= pic_height -1;
                end
                else begin
					start_y_in <= luma_ref_start_y_in[X_ADDR_WDTH - 1:0];
				end

				if(chma_ref_start_x_in[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] == 1) begin
					start_x_ch <= 0;
				end
				else if(chma_ref_start_x_in >= pic_width) begin
                    start_x_ch <= (pic_width>>1)-1'b1;
                end
                else begin
					start_x_ch <= chma_ref_start_x_in[X_ADDR_WDTH - 1:0]>>1;
				end

				if(chma_ref_start_y_in[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] == 1) begin
					start_y_ch <= 0;
				end
                else if(chma_ref_start_y_in >= pic_height) begin
                    start_y_ch <= (pic_height>>1) -1'b1;
                end
				else begin
					start_y_ch <= chma_ref_start_y_in[X_ADDR_WDTH - 1:0]>>1;
				end
				
				if(ch_frac_x == 3'b100) begin
					if(chma_ref_start_x_in[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] == 1) begin
						start_great_x_in <= 0;
					end
                    else if(chma_ref_start_x_in >= pic_width) begin
                        start_great_x_in <= pic_width -1'b1;
                    end
					else begin
						start_great_x_in <= chma_ref_start_x_in[X_ADDR_WDTH - 1:0];
					end
				end
				else begin
					if(luma_ref_start_x_in[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] == 1) begin
						start_great_x_in <= 0;
					end
					else if(luma_ref_start_x_in >= pic_width) begin
                        start_great_x_in <= pic_width -1;
                    end
                    else begin
						start_great_x_in <= luma_ref_start_x_in[X_ADDR_WDTH - 1:0];
					end
				end
				
				if(ch_frac_y == 3'b100) begin
					if(chma_ref_start_y_in[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] == 1) begin
						start_great_y_in <= 0;
					end
					else if(chma_ref_start_y_in >= pic_height)begin
                        start_great_y_in <= pic_height -1;
                    end
                    else begin
						start_great_y_in <= chma_ref_start_y_in[X_ADDR_WDTH - 1:0];
					end
				end
				else begin
					if(luma_ref_start_y_in[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] == 1) begin
						start_great_y_in <= 0;
					end
					else if(luma_ref_start_y_in >= pic_height) begin
                        start_great_y_in <= pic_height -1;
                    end
                    else begin
						start_great_y_in <= luma_ref_start_y_in[X_ADDR_WDTH - 1:0];
					end
				end
				
                //--------------------set luma width

                if(luma_ref_start_x_in[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] == 1) begin
                    block_x_end_luma <= LUMA_REF_BLOCK_WIDTH -1;
                    if(luma_ref_end_x[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] == 1) begin
                        block_x_offset_luma <= LUMA_REF_BLOCK_WIDTH -1;
                        
                        rf_blk_wdt_in <= 0;
                        if(ch_frac_x != 3'b100) begin
                            rf_blk_great_wdt_in <= 0;
                        end
                    end
                    else begin
                        if(ch_frac_x != 3'b100) begin
                            rf_blk_great_wdt_in <= luma_ref_end_x[LUMA_DIM_WDTH-1:0];
                        end
                        rf_blk_wdt_in <= luma_ref_end_x[LUMA_DIM_WDTH-1:0];
                        block_x_offset_luma <= luma_start_x_rel_0[LUMA_DIM_WDTH-1:0];
                    end
                end
                
                else if(luma_ref_end_x >= pic_width) begin
                    block_x_offset_luma <= 0;
                    if(luma_start_x_rel_pic_width[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] ==1) begin
                        if(ch_frac_x != 3'b100) begin
                            rf_blk_great_wdt_in <= 0;
                        end
                        rf_blk_wdt_in <= 0;
                        block_x_end_luma <= 0;
                    end
                    else begin
                        if(ch_frac_x != 3'b100) begin
                            rf_blk_great_wdt_in <= luma_start_x_rel_pic_width[LUMA_DIM_WDTH-1:0];
                        end
                        rf_blk_wdt_in <= luma_start_x_rel_pic_width[LUMA_DIM_WDTH-1:0];    
                        block_x_end_luma <= luma_start_x_rel_pic_width[LUMA_DIM_WDTH-1:0];                      
                    end
                end
                else begin
                    if(ch_frac_x != 3'b100) begin
                        rf_blk_great_wdt_in <= luma_ref_width_x_in;
                    end
                    rf_blk_wdt_in <= luma_ref_width_x_in;
                    block_x_offset_luma <= 0;
                    block_x_end_luma <= luma_ref_width_x_in;
                end

                //--------------------set luma height
                if(luma_ref_start_y_in[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] == 1) begin
                    block_y_end_luma <= LUMA_REF_BLOCK_WIDTH -1;
                    if(luma_ref_end_y[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] == 1) begin
                        rf_blk_hgt_in <= 0;
                        block_y_offset_luma <= LUMA_REF_BLOCK_WIDTH -1;
                        if(ch_frac_y != 3'b100) begin
                            rf_blk_great_hgt_in <= 0;
                        end
                    end
                    else begin
                        if(ch_frac_y != 3'b100) begin
                            rf_blk_great_hgt_in <= luma_ref_end_y[LUMA_DIM_WDTH-1:0];
                        end
                        rf_blk_hgt_in <= luma_ref_end_y[LUMA_DIM_WDTH-1:0];
                        block_y_offset_luma <= luma_start_y_rel_0[LUMA_DIM_WDTH-1:0];
                    end
                end

                else if(luma_ref_end_y >= pic_height) begin
                    block_y_offset_luma <= 0;
                    if(luma_start_y_rel_pic_height[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] ==1) begin
                        if(ch_frac_y != 3'b100) begin
                            rf_blk_great_hgt_in <= 0;
                        end
                        rf_blk_hgt_in <= 0;
                        block_y_end_luma <= 0;
                    end
                    else begin
                        if(ch_frac_y != 3'b100) begin
                            rf_blk_great_hgt_in <= luma_start_y_rel_pic_height[LUMA_DIM_WDTH-1:0];
                        end
                        rf_blk_hgt_in <= luma_start_y_rel_pic_height[LUMA_DIM_WDTH-1:0];
                        block_y_end_luma <= luma_start_y_rel_pic_height[LUMA_DIM_WDTH-1:0];
                    end
                end
                else begin
                    if(ch_frac_y != 3'b100) begin
                        rf_blk_great_hgt_in <= luma_ref_height_y_in;
                    end
                    rf_blk_hgt_in <= luma_ref_height_y_in;
                    block_y_offset_luma <= 0;
                    block_y_end_luma <= luma_ref_height_y_in;
                end

                //--------------------set chma width
                if(chma_ref_start_x_in[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] == 1) begin
                    block_x_end_chma <= CHMA_REF_BLOCK_WIDTH -1;
                    if(chma_ref_end_x[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] == 1) begin
                        block_x_offset_chma <= CHMA_REF_BLOCK_WIDTH -1;
                        rf_blk_wdt_ch <= 0;
                        if(ch_frac_x == 3'b100) begin
                            rf_blk_great_wdt_in <= 0;
                        end
                    end
                    else begin
                        if(ch_frac_x == 3'b100) begin
                            rf_blk_great_wdt_in <= chma_ref_end_x[LUMA_DIM_WDTH-1:0];
                        end
                        rf_blk_wdt_ch <= chma_ref_end_x[LUMA_DIM_WDTH-1:0] >> 1;
                        block_x_offset_chma <= chma_start_x_rel_0[LUMA_DIM_WDTH-1:0] >>1;
                    end
                end
                else if(chma_ref_end_x >= pic_width) begin
                    block_x_offset_chma <= 0;
                    if(chma_start_x_rel_pic_width[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] ==1) begin
                        if(ch_frac_x == 3'b100) begin
                            rf_blk_great_wdt_in <= 0;
                        end
                        rf_blk_wdt_ch <= 0;
                        block_x_end_chma <= 0;
                    end
                    else begin
                        if(ch_frac_x == 3'b100) begin
                            rf_blk_great_wdt_in <= chma_start_x_rel_pic_width[LUMA_DIM_WDTH-1:0];
                        end
                        rf_blk_wdt_ch <= chma_start_x_rel_pic_width[LUMA_DIM_WDTH-1:0] >> 1;
                        block_x_end_chma <= chma_start_x_rel_pic_width[LUMA_DIM_WDTH-1:0] >> 1;
                    end
                end
                else begin
                    if(ch_frac_x == 3'b100) begin
                        rf_blk_great_wdt_in <= chma_ref_width_x_in;
                    end
                    rf_blk_wdt_ch <= chma_ref_width_x_in >> 1;
                    block_x_offset_chma <= 0;
                    block_x_end_chma <= chma_ref_width_x_in >> 1;
                end
				
                //--------------------set chma height
                if(chma_ref_start_y_in[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] == 1) begin
                    block_y_end_chma <= CHMA_REF_BLOCK_WIDTH -1;
                    if(chma_ref_end_y[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] == 1) begin
                        block_y_offset_chma <= CHMA_REF_BLOCK_WIDTH -1;
                        rf_blk_hgt_ch <= 0;
                        if(ch_frac_y == 3'b100) begin
                            rf_blk_great_hgt_in <= 0;
                        end
                    end
                    else begin
                        if(ch_frac_y == 3'b100) begin
                            rf_blk_great_hgt_in <= chma_ref_end_y[LUMA_DIM_WDTH-1:0];
                        end
                        rf_blk_hgt_ch <= chma_ref_end_y[LUMA_DIM_WDTH-1:0] >> 1;
                        block_y_offset_chma <= chma_start_y_rel_0[LUMA_DIM_WDTH-1:0] >>1;
                    end
                end
                else if(chma_ref_end_y >= pic_height) begin
                    block_y_offset_chma <= 0;
                    if(chma_start_y_rel_pic_height[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] ==1) begin
                        if(ch_frac_y == 3'b100) begin
                            rf_blk_great_hgt_in <= 0;
                        end
                        rf_blk_hgt_ch <= 0;
                        block_y_end_chma <= 0;
                    end
                    else begin
                        if(ch_frac_y == 3'b100) begin
                            rf_blk_great_hgt_in <= chma_start_y_rel_pic_height[LUMA_DIM_WDTH-1:0];
                        end
                        rf_blk_hgt_ch <= chma_start_y_rel_pic_height[LUMA_DIM_WDTH-1:0] >> 1;
                        block_y_end_chma <= chma_start_y_rel_pic_height[LUMA_DIM_WDTH-1:0] >> 1;
                    end
                end
                else begin
                    if(ch_frac_y == 3'b100) begin
                        rf_blk_great_hgt_in <= chma_ref_height_y_in;
                    end
                    rf_blk_hgt_ch <= chma_ref_height_y_in >> 1;
                    block_y_offset_chma <= 0;
                    block_y_end_chma <= chma_ref_height_y_in >> 1;
                end

				ref_idx_in <= ref_idx_in_in;
                curr_x <= 0;
                curr_y <= 0;

                curr_x_luma <= 0;
                curr_y_luma <= 0;
                curr_x_chma <= 0;
                curr_y_chma <= 0;
                cur_xy_changed_luma <= 1;
                cur_xy_changed_chma <= 1;
			end
		end
        STATE_TAG_READ: begin
            curr_x_addr_reg     <= curr_x_addr;
            curr_y_addr_reg     <= curr_y_addr; 
            curr_y_addr_reg_1     <= curr_y_addr +1; 
            curr_x_addr_reg_1     <= curr_x_addr +1; 

            set_addr_d <= set_addr;
            tag_addr <= {ref_idx_in,curr_y_addr[Y_ADDR_WDTH - C_L_V_SIZE -1:2],curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
            if(cur_xy_changed_luma) begin
                cl_strt_x_luma_d <= cl_strt_x_luma;
                //cl_end__x_luma_d <= cl_end__x_luma;
                cl_strt_y_luma_d <= cl_strt_y_luma;
                //cl_end__y_luma_d <= cl_end__y_luma;

                dest_end_x_luma <= next_dest_end_x_luma;
                dst_strt_x_luma <= next_dst_strt_x_luma;
                dest_end_y_luma <= next_dest_end_y_luma;
                dst_strt_y_luma <= next_dst_strt_y_luma;
            end
            if(cur_xy_changed_chma) begin
                cl_strt_x_chma_d <= cl_strt_x_chma;
                //cl_end__x_chma_d <= cl_end__x_chma;
                cl_strt_y_chma_d <= cl_strt_y_chma;
                //cl_end__y_chma_d <= cl_end__y_chma;

                dest_end_x_chma <= next_dest_end_x_chma;
                dst_strt_x_chma <= next_dst_strt_x_chma;
                dest_end_y_chma <= next_dest_end_y_chma;
                dst_strt_y_chma <= next_dst_strt_y_chma;
            end
        end
        STATE_TAG_COMPARE: begin
			if(C_SIZE == 13) begin
				bu_idx_val <= bu_idx * `REF_PIX_BU_OFFSET + bu_row_idx * `REF_PIX_BU_ROW_OFFSET;
			end
			else begin
				bu_idx_val <= bu_idx * `REF_PIX_BU_OFFSET_OLD + bu_row_idx * `REF_PIX_BU_ROW_OFFSET;
			end
            
            iu_idx_val <= iu_idx * `REF_PIX_IU_OFFSET;
            iu_idx_row_val <= iu_row_idx * `REF_PIX_IU_ROW_OFFSET;
            ref_idx_val <= ref_idx_in * `REF_PIX_FRAME_OFFSET;
            is_hit_d <= is_hit;
            set_idx_d <= set_idx;
            luma_dest_enable_reg <= luma_dest_enable_wire;
            chma_dest_enable_reg <= chma_dest_enable_wire;
            //if(luma_dest_enable_reg == 1) begin
                for(j=0;j < LUMA_REF_BLOCK_WIDTH; j = j+1) begin // horizontal span
                    if((dst_strt_x_luma <= j[LUMA_DIM_WDTH -1:0]) && (j[LUMA_DIM_WDTH -1:0] <= dest_end_x_luma) ) begin
                        dest_fill_x_mask_luma[j] <= 1;
                        dest_fill_x_loc_luma[j] <= (j[LUMA_DIM_WDTH -1:0] - dst_strt_x_luma) + cl_strt_x_luma_d;
                    end
                    else begin
                        dest_fill_x_mask_luma[j] <= 0;
                    end
                end
                for(j=0;j < LUMA_REF_BLOCK_WIDTH; j = j+1) begin // vertical span
                    if((dst_strt_y_luma <= j[LUMA_DIM_WDTH -1:0]) && (j[LUMA_DIM_WDTH -1:0] <= dest_end_y_luma)) begin
                        dest_fill_y_mask_luma[j] <= 1;
                        dest_fill_y_loc_luma[j] <= (j[LUMA_DIM_WDTH -1:0] - dst_strt_y_luma) + cl_strt_y_luma_d;
                    end
                    else begin
                        dest_fill_y_mask_luma[j] <= 0;
                    end
                end
            //end

            //if(chma_dest_enable_reg == 1) begin
                for(j=0;j < CHMA_REF_BLOCK_WIDTH; j = j+1) begin // horizontal span
                    if((dst_strt_x_chma <= j[CHMA_DIM_WDTH -1:0]) && (j[CHMA_DIM_WDTH -1:0] <= dest_end_x_chma) ) begin
                        dest_fill_x_mask_chma[j] <= 1;
                        dest_fill_x_loc_chma[j] <= (j[CHMA_DIM_WDTH -1:0] - dst_strt_x_chma) + cl_strt_x_chma_d;
                    end
                    else begin
                        dest_fill_x_mask_chma[j] <= 0;
                    end
                end
                for(j=0;j < CHMA_REF_BLOCK_WIDTH; j = j+1) begin // vertical span
                    if((dst_strt_y_chma <= j[CHMA_DIM_WDTH -1:0]) && (j[CHMA_DIM_WDTH -1:0] <= dest_end_y_chma)) begin
                        dest_fill_y_mask_chma[j] <= 1;
                        dest_fill_y_loc_chma[j] <= (j[CHMA_DIM_WDTH -1:0] - dst_strt_y_chma) + cl_strt_y_chma_d;
                    end
                    else begin
                        dest_fill_y_mask_chma[j] <= 0;
                    end
                end
            //end
        end
        STATE_DEST_FILL_HIT: begin
			if(luma_dest_enable_reg) begin
				for(i=0;i<LUMA_REF_BLOCK_WIDTH; i=i+1) begin
					for(j=0;j<LUMA_REF_BLOCK_WIDTH; j=j+1) begin
						if(dest_fill_x_mask_luma[j] & dest_fill_y_mask_luma[i]) begin
							block_11x11[i][j] <= cache_rdata_arr[(CACHE_LINE_LUMA_OFFSET/LUMA_BITS) + (dest_fill_y_loc_luma[i] * (1<<C_L_H_SIZE) + dest_fill_x_loc_luma[j])];
						end
					end
				end
			end
			
			if(chma_dest_enable_reg) begin
				for(i=0;i<CHMA_REF_BLOCK_WIDTH; i=i+1) begin
					for(j=0;j<CHMA_REF_BLOCK_WIDTH; j=j+1) begin
						if(dest_fill_x_mask_chma[j] & dest_fill_y_mask_chma[i]) begin
							block_5x5_cb[i][j] <= cache_rdata_arr[(CACHE_LINE_CB_OFFSET/LUMA_BITS) + (dest_fill_y_loc_chma[i] * (1<<C_L_H_SIZE_C) + dest_fill_x_loc_chma[j])];
						end
					end
				end
			end

			if(chma_dest_enable_reg) begin
				for(i=0;i<CHMA_REF_BLOCK_WIDTH; i=i+1) begin
					for(j=0;j<CHMA_REF_BLOCK_WIDTH; j=j+1) begin
						if(dest_fill_x_mask_chma[j] & dest_fill_y_mask_chma[i]) begin
							block_5x5_cr[i][j] <= cache_rdata_arr[(CACHE_LINE_CR_OFFSET/LUMA_BITS) + (dest_fill_y_loc_chma[i] * (1<<C_L_H_SIZE_C) + dest_fill_x_loc_chma[j])];
						end
					end
				end
			end  
	 
			curr_x_addr_reg     <= curr_x_addr;
			curr_y_addr_reg     <= curr_y_addr; 
			curr_y_addr_reg_1     <= curr_y_addr +1'b1; 
			curr_x_addr_reg_1     <= curr_x_addr +1'b1; 

			set_addr_d <= set_addr;
			tag_addr <= {ref_idx_in,curr_y_addr[Y_ADDR_WDTH - C_L_V_SIZE -1:2],curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
			if(cur_xy_changed_luma) begin
				cl_strt_x_luma_d <= cl_strt_x_luma;
				//cl_end__x_luma_d <= cl_end__x_luma;
				cl_strt_y_luma_d <= cl_strt_y_luma;
				//cl_end__y_luma_d <= cl_end__y_luma;

				dest_end_x_luma <= next_dest_end_x_luma;
				dst_strt_x_luma <= next_dst_strt_x_luma;
				dest_end_y_luma <= next_dest_end_y_luma;
				dst_strt_y_luma <= next_dst_strt_y_luma;
			end
			if(cur_xy_changed_chma) begin
				cl_strt_x_chma_d <= cl_strt_x_chma;
				//cl_end__x_chma_d <= cl_end__x_chma;
				cl_strt_y_chma_d <= cl_strt_y_chma;
				//cl_end__y_chma_d <= cl_end__y_chma;

				dest_end_x_chma <= next_dest_end_x_chma;
				dst_strt_x_chma <= next_dst_strt_x_chma;
				dest_end_y_chma <= next_dest_end_y_chma;
				dst_strt_y_chma <= next_dst_strt_y_chma;
			end
        end
		STATE_DEST_FILL_MIS: begin
			if(ref_pix_axi_r_valid) begin
				if(ref_pix_axi_r_last) begin
					if(luma_dest_enable_reg) begin
						for(i=0;i<LUMA_REF_BLOCK_WIDTH; i=i+1) begin
							for(j=0;j<LUMA_REF_BLOCK_WIDTH; j=j+1) begin
								if(dest_fill_x_mask_luma[j] & dest_fill_y_mask_luma[i]) begin
									block_11x11[i][j] <= cache_w_data_arr[(CACHE_LINE_LUMA_OFFSET/LUMA_BITS) + (dest_fill_y_loc_luma[i] * (1<<C_L_H_SIZE) + dest_fill_x_loc_luma[j])];
								end
							end
						end
					end

					if(chma_dest_enable_reg) begin
						for(i=0;i<CHMA_REF_BLOCK_WIDTH; i=i+1) begin
							for(j=0;j<CHMA_REF_BLOCK_WIDTH; j=j+1) begin
								if(dest_fill_x_mask_chma[j] & dest_fill_y_mask_chma[i]) begin
									block_5x5_cb[i][j] <= cache_w_data_arr[(CACHE_LINE_CB_OFFSET/LUMA_BITS) + (dest_fill_y_loc_chma[i] * (1<<C_L_H_SIZE_C) + dest_fill_x_loc_chma[j])];
								end
							end
						end
					end  
					
					if(chma_dest_enable_reg) begin
						for(i=0;i<CHMA_REF_BLOCK_WIDTH; i=i+1) begin
							for(j=0;j<CHMA_REF_BLOCK_WIDTH; j=j+1) begin
								if(dest_fill_x_mask_chma[j] & dest_fill_y_mask_chma[i]) begin
									block_5x5_cr[i][j] <= cache_w_data_arr[(CACHE_LINE_CR_OFFSET/LUMA_BITS) + (dest_fill_y_loc_chma[i] * (1<<C_L_H_SIZE_C) + dest_fill_x_loc_chma[j])];
								end
							end
						end
					end
					 
					if(luma_dest_enable_reg) begin
						curr_x_luma <= next_curr_x_luma;
						curr_y_luma <= next_curr_y_luma;
						cur_xy_changed_luma <= 1;

					end
					else begin
						cur_xy_changed_luma <= 0;
					end
					if(chma_dest_enable_reg) begin
						curr_x_chma <= next_curr_x_chma;
						curr_y_chma <= next_curr_y_chma;
						cur_xy_changed_chma <= 1;  
					end
					else begin
						cur_xy_changed_chma <= 0;
					end
					curr_x <= next_curr_x;
					curr_y <= next_curr_y;                    
					
				end
				else begin
					if(C_SIZE ==13) begin
						cache_w_port_old_reg[CACHE_LINE_WDTH*PIXEL_BITS/2 -1:0]	<= ref_pix_axi_r_data[CACHE_LINE_WDTH*PIXEL_BITS/2 -1:0]	    ;
					end
				end
			end   			
		end
        STATE_C_LINE_HIT_FETCH: begin
            curr_x <= next_curr_x;
            curr_y <= next_curr_y;
            if(luma_dest_enable_reg) begin
                curr_x_luma <= next_curr_x_luma;
                curr_y_luma <= next_curr_y_luma;
                cur_xy_changed_luma <= 1;

            end
            else begin
                cur_xy_changed_luma <= 0;
            end
            if(chma_dest_enable_reg) begin
                curr_x_chma <= next_curr_x_chma;
                curr_y_chma <= next_curr_y_chma;
                cur_xy_changed_chma <= 1;  
            end
            else begin
                cur_xy_changed_chma <= 0;
            end        
        end
        STATE_C_LINE_MISS_FETCH: begin
            set_idx_miss_d <= set_idx_miss;
            ref_pix_axi_ar_valid <= 1;
            ref_pix_axi_ar_addr <= bu_idx_val  + iu_idx_val + iu_idx_row_val + ref_idx_val;
        end
        STATE_MISS_AR_REDY_WAIT: begin
            if(ref_pix_axi_ar_ready) begin
                ref_pix_axi_ar_valid <= 0;
            end
        end
	endcase
end	
	
always@(posedge clk) begin
    if(reset) begin
        state <= STATE_SET_INPUTS;
    end
    else begin
        state <= next_state;
    end
end
	
endmodule
