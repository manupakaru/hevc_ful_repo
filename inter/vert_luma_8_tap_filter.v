`timescale 1ns / 1ps
module vert_luma_8_tap_filter(
    clk,
    filt_type,
    filt_type_x,
    a_ref_pixel_in ,
    b_ref_pixel_in ,
    c_ref_pixel_in ,
    d_ref_pixel_in ,
    e_ref_pixel_in ,
    f_ref_pixel_in ,
    g_ref_pixel_in ,
    h_ref_pixel_in ,
    pred_pixel_out
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"
    
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
	parameter IN_PIXEL_WIDTH = FILTER_PIXEL_WIDTH;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter SHIFT2 = 3'd6;
    parameter   SHIFT3 = 3'd6;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input [1:0] filt_type;
    input [1:0] filt_type_x;
    input [IN_PIXEL_WIDTH -1: 0] a_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] b_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] c_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] d_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] e_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] f_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] g_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] h_ref_pixel_in;
    output reg [FILTER_PIXEL_WIDTH -1:0]pred_pixel_out;

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    reg [IN_PIXEL_WIDTH -1: 0] a_ref_pixel_d;
	
    reg [IN_PIXEL_WIDTH -1: 0] a_ref_pixel;
    reg [IN_PIXEL_WIDTH -1: 0] b_ref_pixel;
    reg [IN_PIXEL_WIDTH -1: 0] c_ref_pixel;
    reg [IN_PIXEL_WIDTH -1: 0] d_ref_pixel;
    reg [IN_PIXEL_WIDTH -1: 0] e_ref_pixel;
    reg [IN_PIXEL_WIDTH -1: 0] f_ref_pixel;
    reg [IN_PIXEL_WIDTH -1: 0] g_ref_pixel;
    reg [IN_PIXEL_WIDTH -1: 0] h_ref_pixel;
	

	reg signed [IN_PIXEL_WIDTH + 6 -1: 0] com_1_level1_adder;
	reg signed [IN_PIXEL_WIDTH + 6 -1: 0] com_2_level1_adder;
	reg signed [IN_PIXEL_WIDTH + 6 -1: 0] com_3_level1_adder;
	reg signed [IN_PIXEL_WIDTH + 6 -1: 0] com_1_level2_adder;
	reg signed [IN_PIXEL_WIDTH + 6 -1: 0] com_2_level2_adder;
	reg signed [IN_PIXEL_WIDTH + 6 -1: 0] com_1_level3_adder;

	reg signed [IN_PIXEL_WIDTH +6 -1: 0] type1_1_level1_adder;
	reg signed [IN_PIXEL_WIDTH +6 -1: 0] type1_2_level1_adder;
	reg signed [IN_PIXEL_WIDTH +6 -1: 0] type1_3_level1_adder;
	reg signed [IN_PIXEL_WIDTH +6 -1: 0] type1_1_level2_adder;
	reg signed [IN_PIXEL_WIDTH +6 -1: 0] type1_1_level3_adder;


	reg signed [IN_PIXEL_WIDTH +6 -1: 0] type2_1_level1_adder;
	reg signed [IN_PIXEL_WIDTH +6 -1: 0] type2_2_level1_adder;
	reg signed [IN_PIXEL_WIDTH +6 -1: 0] type2_3_level1_adder;
	reg signed [IN_PIXEL_WIDTH +6 -1: 0] type2_1_level2_adder;
	reg signed [IN_PIXEL_WIDTH +6 -1: 0] type2_2_level2_adder;
	reg signed [IN_PIXEL_WIDTH +6 -1: 0] type2_1_level3_adder;

	reg [1:0] filt_type_d;
	reg level_in_d;
	reg [1:0] filt_type_x_d;
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module



always@(*) begin
	if(filt_type != 2'b11) begin
		a_ref_pixel = a_ref_pixel_in;
		b_ref_pixel = b_ref_pixel_in;
		c_ref_pixel = c_ref_pixel_in;
		d_ref_pixel = d_ref_pixel_in;
		e_ref_pixel = e_ref_pixel_in;
		f_ref_pixel = f_ref_pixel_in;
		g_ref_pixel = g_ref_pixel_in;
		h_ref_pixel = h_ref_pixel_in;
	end
	else begin
		a_ref_pixel = h_ref_pixel_in;
		b_ref_pixel = g_ref_pixel_in;
		c_ref_pixel = f_ref_pixel_in;
		d_ref_pixel = e_ref_pixel_in;
		e_ref_pixel = d_ref_pixel_in;
		f_ref_pixel = c_ref_pixel_in;
		g_ref_pixel = b_ref_pixel_in;
		h_ref_pixel = a_ref_pixel_in;	
	end
end
always@(posedge clk) begin
    filt_type_d <= filt_type;
	a_ref_pixel_d <= a_ref_pixel_in;
	filt_type_x_d <= filt_type_x;
end


always@(*) begin
    type2_1_level1_adder = ((-$signed(f_ref_pixel))<<1) + ($signed(g_ref_pixel) << 2); 
    type2_2_level1_adder = (($signed(c_ref_pixel)) + ($signed(h_ref_pixel)));   //+1
    type2_3_level1_adder = $signed(e_ref_pixel) + (-$signed(f_ref_pixel));  //+1
    
    
end


always@(posedge clk) begin
    type2_1_level2_adder <= (-type2_2_level1_adder) + ($signed(e_ref_pixel)<<5);  
    type2_2_level2_adder <= type2_1_level1_adder + (type2_3_level1_adder<<3);
end

always@(*) begin
    type2_1_level3_adder = type2_1_level2_adder + type2_2_level2_adder;
end


always@(*) begin
    
    type1_2_level1_adder = $signed(e_ref_pixel) + ($signed(d_ref_pixel) << 1);
    type1_3_level1_adder = ((-$signed(f_ref_pixel)) << 2) + $signed(g_ref_pixel);

    
end

always@(posedge clk) begin
    type1_1_level2_adder <= (type1_2_level1_adder) + (type1_3_level1_adder);
    type1_1_level1_adder <= $signed(d_ref_pixel ) + $signed(e_ref_pixel);
end

always@(*) begin
    type1_1_level3_adder = (type1_1_level2_adder) + (type1_1_level1_adder << 4);
end



always@(*) begin    
    com_1_level1_adder = $signed(a_ref_pixel) + $signed(f_ref_pixel);
    com_2_level1_adder = (-$signed(c_ref_pixel)) + $signed(d_ref_pixel);
    com_3_level1_adder =  ((-$signed(c_ref_pixel))<<1) + ($signed(b_ref_pixel)<<2);
    
    // com_1_level2_adder = (-com_1_level1_adder) + (com_2_level1_adder<<3);
    // com_2_level2_adder = (d_ref_pixel<<5) + (com_3_level1_adder);
    
    
end

always@(posedge clk) begin
    com_1_level2_adder <= (-com_1_level1_adder) + (com_2_level1_adder<<3);
    com_2_level2_adder <= ($signed(d_ref_pixel)<<5) + (com_3_level1_adder);
end


always@(*) begin
    com_1_level3_adder = com_1_level2_adder + com_2_level2_adder;
end


always@(posedge clk) begin
	if(filt_type_d == 2'b00) begin
		if(filt_type_x_d == 2'd0) begin
			pred_pixel_out <= {a_ref_pixel_d<<SHIFT3};
		end
		else begin
			pred_pixel_out <= {a_ref_pixel_d};
		end
	end
    else if(filt_type_d == 2'b10) begin
		if(filt_type_x_d != 2'd0) begin
			pred_pixel_out <= (com_1_level3_adder + type2_1_level3_adder)>>SHIFT2;
		end
		else begin
			pred_pixel_out <= (com_1_level3_adder + type2_1_level3_adder);
		end
    end
    else begin
		if(filt_type_x_d != 2'd0) begin
			pred_pixel_out <= (com_1_level3_adder + type1_1_level3_adder) >> SHIFT2;
		end
		else begin
			pred_pixel_out <= (com_1_level3_adder + type1_1_level3_adder);
		end
    end
    
end

endmodule 