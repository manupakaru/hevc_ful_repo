`timescale 1ns / 1ps
module chroma_weight_pred(
    clk,
    reset,
    bi_pred_block_in,
    x0_tu_end_in_min_tus_in,
    x0_tu_end_in_min_tus_out,
    y0_tu_end_in_min_tus_in,
    y0_tu_end_in_min_tus_out,

    xT_in_min_tus,
    yT_in_min_tus,
    xT_out_min_tus,
    yT_out_min_tus,
    valid_in,
    pred_pix_in_4x4,
    pred_pix_out_4x4,
    valid_out
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter BLOCK_WIDTH_2x2 = 2'd2;
    parameter BLOCK_WIDTH_4x4 = 3'd4;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam  STATE_TOP_LEFT = 0;
    localparam  STATE_TOP_RIGHT = 1;
    localparam  STATE_BOT_LEFT = 2;
    localparam  STATE_BOT_RIGHT = 3;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
    input bi_pred_block_in;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      x0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      y0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_out_min_tus;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_out_min_tus;
    input valid_in;
    input signed [BLOCK_WIDTH_2x2 *  BLOCK_WIDTH_2x2 * FILTER_PIXEL_WIDTH -1:0]         pred_pix_in_4x4;    
    output reg [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 * PIXEL_WIDTH -1:0]                  pred_pix_out_4x4;    
    output reg valid_out;

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    wire [BLOCK_WIDTH_2x2 *  BLOCK_WIDTH_2x2 * PIXEL_WIDTH -1:0]         chma_wgt_pred_out; 
    
    reg [PIXEL_WIDTH -1:0]  chma_wgt_pred_2x2_reg [BLOCK_WIDTH_2x2-1:0][BLOCK_WIDTH_2x2 -1:0];
    reg [PIXEL_WIDTH -1:0]  chma_wgt_pred_4x4_reg [BLOCK_WIDTH_4x4-1:0][BLOCK_WIDTH_4x4 -1:0];
    
    integer i,j,k;
    integer state;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_chma_out;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_chma_out;
    
    wire chma_wght_pred_valid;

    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      x0_tu_end_in_min_tus_chma_out;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      y0_tu_end_in_min_tus_chma_out;

//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module
weighted_sample_prediction #
(
    .BLOCK_WIDTH_4x4(BLOCK_WIDTH_2x2)
)
chroma_2x2_pred_block (
    .clk(clk), 
    .reset(reset), 
    .bi_pred_block_in(bi_pred_block_in),
    .res_present_in(1'b0),
    .res_present_out(),
    .xT_in_min_tus(xT_in_min_tus), 
    .yT_in_min_tus(yT_in_min_tus), 
    .xT_out_min_tus(xT_in_min_chma_out), 
    .yT_out_min_tus(yT_in_min_chma_out), 
    .x0_tu_end_in_min_tus_in (x0_tu_end_in_min_tus_in),
    .x0_tu_end_in_min_tus_out(x0_tu_end_in_min_tus_chma_out),
    .y0_tu_end_in_min_tus_in (y0_tu_end_in_min_tus_in),
    .y0_tu_end_in_min_tus_out(y0_tu_end_in_min_tus_chma_out),

    .valid_in(valid_in), 
    .pred_pix_in_4x4(pred_pix_in_4x4), 
    .pred_pix_out_4x4(chma_wgt_pred_out), 
    .valid_out(chma_wght_pred_valid)
    );


always@(*) begin
    for(j=0;j <BLOCK_WIDTH_2x2 ; j=j+1 ) begin
        for(i=0 ; i < BLOCK_WIDTH_2x2 ; i = i+1) begin
            for(k=0; k< PIXEL_WIDTH ; k = k+1) begin
                chma_wgt_pred_2x2_reg[j][i][k] = chma_wgt_pred_out[(j*BLOCK_WIDTH_2x2 + i)*PIXEL_WIDTH + k];
            end
        end
    end
end

always@(*) begin
    for(j=0;j <BLOCK_WIDTH_4x4 ; j=j+1 ) begin
        for(i=0 ; i < BLOCK_WIDTH_4x4 ; i = i+1) begin
            for(k=0; k< PIXEL_WIDTH ; k = k+1) begin
                pred_pix_out_4x4[(j*BLOCK_WIDTH_4x4 + i)*PIXEL_WIDTH + k] = chma_wgt_pred_4x4_reg[j][i][k];
            end
        end
    end
end


always@(posedge clk) begin
    if (reset) begin
        state <= STATE_TOP_LEFT;
        valid_out <= 0;
    end
    else begin
        case(state)
            STATE_TOP_LEFT: begin
                valid_out <= 0;
                if (chma_wght_pred_valid) begin 
                    state <= STATE_TOP_RIGHT;
                    for(j=0;j <BLOCK_WIDTH_2x2 ; j=j+1 ) begin
                        for(i=0 ; i < BLOCK_WIDTH_2x2 ; i = i+1) begin
                            chma_wgt_pred_4x4_reg[j+ (0 * BLOCK_WIDTH_2x2)][i + (0 * BLOCK_WIDTH_2x2)] <= chma_wgt_pred_2x2_reg[j][i];
                        end
                    end
                end

            end
            STATE_TOP_RIGHT: begin
                if (chma_wght_pred_valid) begin 
                    state <= STATE_BOT_LEFT;
                    for(j=0;j <BLOCK_WIDTH_2x2 ; j=j+1 ) begin
                        for(i=0 ; i < BLOCK_WIDTH_2x2 ; i = i+1) begin
                            chma_wgt_pred_4x4_reg[j+ (0 * BLOCK_WIDTH_2x2)][i + (1 * BLOCK_WIDTH_2x2)] <= chma_wgt_pred_2x2_reg[j][i];
                        end
                    end
                end
            end
            STATE_BOT_LEFT: begin
                if (chma_wght_pred_valid) begin 
                    state <= STATE_BOT_RIGHT;
                    for(j=0;j <BLOCK_WIDTH_2x2 ; j=j+1 ) begin
                        for(i=0 ; i < BLOCK_WIDTH_2x2 ; i = i+1) begin
                            chma_wgt_pred_4x4_reg[j+ (1 * BLOCK_WIDTH_2x2)][i + (0 * BLOCK_WIDTH_2x2)] <= chma_wgt_pred_2x2_reg[j][i];
                        end
                    end
                end
            end
            STATE_BOT_RIGHT: begin
                if (chma_wght_pred_valid) begin 
                    state <= STATE_TOP_LEFT;
                    xT_out_min_tus <= xT_in_min_chma_out;
                    yT_out_min_tus <= yT_in_min_chma_out;
                    x0_tu_end_in_min_tus_out <= x0_tu_end_in_min_tus_chma_out;
                    y0_tu_end_in_min_tus_out <= y0_tu_end_in_min_tus_chma_out;
                    for(j=0;j <BLOCK_WIDTH_2x2 ; j=j+1 ) begin
                        for(i=0 ; i < BLOCK_WIDTH_2x2 ; i = i+1) begin
                            chma_wgt_pred_4x4_reg[j+ (1 * BLOCK_WIDTH_2x2)][i + (1 * BLOCK_WIDTH_2x2)] <= chma_wgt_pred_2x2_reg[j][i];
                        end
                    end
                    valid_out <= 1;
                end
            end
        endcase
    end
end


endmodule   