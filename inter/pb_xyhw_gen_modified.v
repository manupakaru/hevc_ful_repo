`timescale 1ns / 1ps
module pb_xyhw_gen_modified(
    clk,
    // reset,
    cb_part_mode ,
    pb_part_idx,
    cb_size,
    cb_xx,
    cb_yy,
    xx_pb_start,
    yy_pb_start,
    yy_pb_end,
    xx_pb_end
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    // input reset;
    input [PART_IDX_WIDTH :0]         pb_part_idx;
    input [PARTMODE_WIDTH -1:0]         cb_part_mode;
    input [CB_SIZE_WIDTH - LOG2_MIN_TU_SIZE -1:0 ]         cb_size;
    input  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]        cb_xx;
    input  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]        cb_yy;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]    xx_pb_start;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]    yy_pb_start;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]    xx_pb_end;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]    yy_pb_end;

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module


always@(posedge clk) begin    
    case(cb_part_mode)
        `PART_2Nx2N: begin
			if(pb_part_idx == 1) begin
				xx_pb_start <= cb_xx;
				yy_pb_start <= cb_yy;
				yy_pb_end <= cb_yy + cb_size;
				xx_pb_end <= cb_xx + cb_size;
			end
			else begin
				xx_pb_start <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				yy_pb_start <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				yy_pb_end <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				xx_pb_end <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
			end
        end
        `PART_2NxN: begin
				
            if (pb_part_idx == 1) begin
                yy_pb_start <= cb_yy;
                yy_pb_end <= (cb_size >> 1) + cb_yy;
				xx_pb_end <= cb_xx+ cb_size;
				xx_pb_start <= cb_xx;
            end
            else if(pb_part_idx == 2) begin
                yy_pb_start <= (cb_yy + (cb_size >> 1))%(1<<(Y11_ADDR_WDTH-LOG2_MIN_TU_SIZE));
                yy_pb_end <= (cb_yy + (cb_size))%(1<<(Y11_ADDR_WDTH-LOG2_MIN_TU_SIZE));
				xx_pb_end <= cb_xx+ cb_size;
				xx_pb_start <= cb_xx;
            end
			else begin
				xx_pb_start <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				yy_pb_start <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				yy_pb_end <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				xx_pb_end <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};			
			end
        end
        `PART_Nx2N: begin
            
            if (pb_part_idx == 1) begin
                xx_pb_start <= cb_xx;
                xx_pb_end <= (cb_xx + (cb_size>>1))%(1<<(X11_ADDR_WDTH-LOG2_MIN_TU_SIZE));
                yy_pb_end <= cb_yy + cb_size;
				yy_pb_start <= cb_yy;
            end
            else if(pb_part_idx == 2) begin
                xx_pb_start <= (cb_xx + (cb_size>>1))%(1<<(X11_ADDR_WDTH-LOG2_MIN_TU_SIZE));
                xx_pb_end <= (cb_xx + (cb_size))%(1<<(X11_ADDR_WDTH-LOG2_MIN_TU_SIZE));
				yy_pb_end <= cb_yy + cb_size;
				yy_pb_start <= cb_yy;
            end
			else begin
				xx_pb_start <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				yy_pb_start <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				yy_pb_end <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				xx_pb_end <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};	
			end
        end
        `PART_NxN: begin
            case(pb_part_idx)
                1: begin
                    xx_pb_start <= cb_xx;
                    xx_pb_end <= cb_xx + (cb_size >> 1);
                    yy_pb_start <= cb_yy;
                    yy_pb_end <= cb_yy + (cb_size >> 1);
                end
                2: begin
                    xx_pb_start <= (cb_xx + (cb_size>>1))%(1<<(X11_ADDR_WDTH-LOG2_MIN_TU_SIZE));
                    xx_pb_end <= (cb_xx + (cb_size))%(1<<(X11_ADDR_WDTH-LOG2_MIN_TU_SIZE));
                    yy_pb_start <= cb_yy;
                    yy_pb_end <= cb_yy + (cb_size >> 1);
                end
                3: begin
                    xx_pb_start <= cb_xx;
                    xx_pb_end <= cb_xx + (cb_size >> 1);
                    yy_pb_start <= (cb_yy + (cb_size >> 1))%(1<<(Y11_ADDR_WDTH-LOG2_MIN_TU_SIZE));
                    yy_pb_end <= cb_yy + (cb_size );
                end
                4: begin
                    xx_pb_start <= (cb_xx + (cb_size>>1))%(1<<(X11_ADDR_WDTH-LOG2_MIN_TU_SIZE));
                    xx_pb_end <= (cb_xx + (cb_size))%(1<<(X11_ADDR_WDTH-LOG2_MIN_TU_SIZE));
                    yy_pb_start <= (cb_yy + (cb_size >> 1))%(1<<(Y11_ADDR_WDTH-LOG2_MIN_TU_SIZE));
                    yy_pb_end <= (cb_yy + (cb_size ))%(1<<(Y11_ADDR_WDTH-LOG2_MIN_TU_SIZE));
                end
            endcase
        end
        `PART_nLx2N: begin
            
            if (pb_part_idx == 1) begin
                xx_pb_start <= cb_xx;
                xx_pb_end <= cb_xx + (cb_size >> 2);
				yy_pb_end <= cb_size + cb_yy;
				yy_pb_start <= cb_yy;
            end
            else if(pb_part_idx == 2) begin
				xx_pb_start <= (cb_xx + (cb_size >>2));
				xx_pb_end <= cb_xx + (cb_size);
				yy_pb_end <= cb_size + cb_yy;
				yy_pb_start <= cb_yy;
            end
			else begin
				xx_pb_start <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				yy_pb_start <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				yy_pb_end <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				xx_pb_end <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};	
			end
        end
        `PART_nRx2N: begin
            if(pb_part_idx == 1) begin
                xx_pb_start <= cb_xx;
                xx_pb_end <= cb_xx + (cb_size>>2) + (cb_size>>1);
				yy_pb_end <= cb_yy +  cb_size;
				yy_pb_start <= cb_yy;

            end
            else if(pb_part_idx == 2) begin
                xx_pb_start <= cb_xx + (cb_size >>1) + (cb_size >> 2);
                xx_pb_end <= cb_xx + (cb_size);
				yy_pb_end <= cb_yy +  cb_size;
				yy_pb_start <= cb_yy;
            end
			else begin
				xx_pb_start <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				yy_pb_start <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				yy_pb_end <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				xx_pb_end <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};	
			end
        end
        `PART_2NxnD: begin
            if (pb_part_idx == 1) begin
				yy_pb_start <= cb_yy;
				yy_pb_end <= cb_yy +  (cb_size>>2) + (cb_size >>1);
				xx_pb_end <= cb_xx + cb_size;
				xx_pb_start <= cb_xx;
			  
            end
            else if(pb_part_idx == 2) begin
				yy_pb_start <= cb_yy + (cb_size >> 2) + (cb_size >> 1);
				yy_pb_end <= cb_yy +  (cb_size);
				xx_pb_end <= cb_xx + cb_size;
				xx_pb_start <= cb_xx;
			end
			else begin
				xx_pb_start <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				yy_pb_start <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				yy_pb_end <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				xx_pb_end <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};	
			end
        end
        `PART_2NxnU: begin
            if (pb_part_idx == 1) begin
                yy_pb_start <= cb_yy;
                yy_pb_end <= cb_yy +  (cb_size >> 2);
				xx_pb_end <= cb_xx + cb_size;
				xx_pb_start <= cb_xx;
			end
            else if(pb_part_idx == 2) begin
                yy_pb_start <= cb_yy + (cb_size >> 2);
                yy_pb_end <= cb_yy +  (cb_size );
				xx_pb_end <= cb_xx + cb_size;
				xx_pb_start <= cb_xx;
			end
			else begin
				xx_pb_start <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				yy_pb_start <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				yy_pb_end <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};
				xx_pb_end <= {(X11_ADDR_WDTH - LOG2_MIN_TU_SIZE){1'b1}};	
			end
        end
    endcase
end
 
endmodule 