`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:12:43 11/16/2013 
// Design Name: 
// Module Name:    check_prediction_block_available 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module check_prediction_block_available(
        clk,
        config_bus,
        config_bus_mode,
        xPpu_in,
        yPpu_in,
        xN_in,
        yN_in,
        xCb_start_in,
        xCb_end_in,     // xC + cb_size
        yCb_start_in,
        yCb_end_in,     // yC + cb_size
        xN_pu_out,
        yN_pu_out,
        avaialble_flag_out,
        same_cb_check
    );


//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES :
     * Always include global constant header files that have constant definitions before all other items
     * these files will contain constants in the form of localparams of `define directives
     */
    `include "../sim/pred_def.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES :
     * Define all paramters
     */
 
 
     
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES :
     * Define all localparams after parameters
     */
 
    input                                                     clk;
    input           [X11_ADDR_WDTH + X_ADDR_WDTH -1 :0]       config_bus;
    input           [AVAILABLE_CONFIG_BUS_WIDTH -1:0]         config_bus_mode;
    input           [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE - 1:0]  xPpu_in;
    input           [Y11_ADDR_WDTH - LOG2_MIN_PU_SIZE - 1:0]  yPpu_in;
    input signed    [X_CAND_WDTH - 1:0]                       xN_in;
    input signed    [Y_CAND_WDTH - 1:0]                       yN_in;
    input         [X11_ADDR_WDTH - 1:0]                       xCb_start_in;
    input         [X11_ADDR_WDTH - 1:0]                       xCb_end_in;     // xC + cb_size
    input         [Y11_ADDR_WDTH - 1:0]                       yCb_start_in;
    input         [Y11_ADDR_WDTH - 1:0]                       yCb_end_in;     // yC + cb_size
    output  reg     [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   xN_pu_out;
    output  reg     [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   yN_pu_out;
    output  reg                                               avaialble_flag_out;
    output reg                                                same_cb_check ;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES
     * Eacho wire and register must be defined on a seperate line
     * Although Verilog allows for implicit decleration of variables, all signals must be declared explicitly.
     * Group variables where necessary. Comment your variable by block and by signal.
     */
     
   reg  [X11_ADDR_WDTH - 1:0]     slice_x;
   reg  [Y11_ADDR_WDTH - 1:0]     slice_y;
   reg  [X11_ADDR_WDTH - 1:0]     tile_x;
   reg  [Y11_ADDR_WDTH - 1:0]     tile_y;
   
   reg  [X11_ADDR_WDTH - 1:0]     pic_width;
   reg  [Y11_ADDR_WDTH - 1:0]     pic_height;
   
   reg  slice_id_check;
   reg  tile_id_check;
   reg  xyN_with_in_pic_check;
   // reg  yN_with_in_pic_check;
   // reg  xN_with_in_pic_check;

   //reg left_check;
   //reg top_check;
   
   reg  min_tb_address_z_check;
   
   reg  [X11_ADDR_WDTH + Y11_ADDR_WDTH - 2*LOG2_MIN_PU_SIZE - 1:0] min_tb_address_z_P;
   reg  [X11_ADDR_WDTH + Y11_ADDR_WDTH - 2*LOG2_MIN_PU_SIZE - 1:0] min_tb_address_z_N;

   reg          [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   xN_pu_mid;
   reg          [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   yN_pu_mid;

   wire inner_ctb_x_zero_wire = !(|xPpu_in[CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE -1:0]);
   wire inner_ctb_y_zero_wire = !(|yPpu_in[CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE -1:0]);
   
   reg inner_ctb_y_zero;
   reg inner_ctb_x_zero;

   wire [X11_ADDR_WDTH-1:CTB_SIZE_WIDTH] outer_ctb_xP = xPpu_in[X11_ADDR_WDTH - LOG2_MIN_PU_SIZE-1:CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE];
   wire [X11_ADDR_WDTH-1:CTB_SIZE_WIDTH] outer_ctb_yP = yPpu_in[X11_ADDR_WDTH - LOG2_MIN_PU_SIZE-1:CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE];
   wire [X11_ADDR_WDTH-1:CTB_SIZE_WIDTH] outer_ctb_xN = xN_in[X11_ADDR_WDTH -1:CTB_SIZE_WIDTH ];
   wire [X11_ADDR_WDTH-1:CTB_SIZE_WIDTH] outer_ctb_yN = yN_in[X11_ADDR_WDTH -1:CTB_SIZE_WIDTH ];

   reg yN_outer_smaller;
   reg xN_outer_smaller;
   reg yN_outer_smaller_eq;

   reg  cb_x_low_check,cb_x_high_check,cb_y_high_check,cb_y_low_check;

   reg min_tb_valid;

//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
    integer count;

    always@(posedge clk) begin
        inner_ctb_y_zero <= inner_ctb_y_zero_wire;
        inner_ctb_x_zero <= inner_ctb_x_zero_wire;
        if((outer_ctb_yP > outer_ctb_yN) ) begin
            yN_outer_smaller <= 1;
        end
        else begin
            yN_outer_smaller <= 0;
        end

        if((outer_ctb_xP > outer_ctb_xN) ) begin
            xN_outer_smaller <= 1;
        end
        else begin
            xN_outer_smaller <= 0;
        end

        if((outer_ctb_yP >= outer_ctb_yN) ) begin
            yN_outer_smaller_eq <= 1;
        end
        else begin
            yN_outer_smaller_eq <= 0;
        end

        if((outer_ctb_xP == outer_ctb_xN) && (outer_ctb_yP==outer_ctb_yN)) begin
            min_tb_valid <= 1;
        end
        else begin
            min_tb_valid <= 0;
        end
    end
    always@(*) begin
        for(count = 0; count < X11_ADDR_WDTH - LOG2_MIN_PU_SIZE ; count = count +1) begin
            min_tb_address_z_N[2*count] = xN_in[count + LOG2_MIN_PU_SIZE];
            min_tb_address_z_N[2*count+1] = yN_in[count + LOG2_MIN_PU_SIZE];
            
            min_tb_address_z_P[2*count] = xPpu_in[count];
            min_tb_address_z_P[2*count+1] = yPpu_in[count];
        end
    end

    always@(posedge clk) begin
        case(config_bus_mode)
            `AVAILABLE_CONFIG_PIC_DIM: begin
                pic_width <=  config_bus[X11_ADDR_WDTH + X_ADDR_WDTH -1: X_ADDR_WDTH];
                pic_height <= config_bus[  X11_ADDR_WDTH -1: 0];               
            end
            `AVAILABLE_CONFIG_PIC_SLICE: begin 
                slice_x <=  config_bus[X11_ADDR_WDTH + X_ADDR_WDTH -1: X_ADDR_WDTH];
                slice_y <=  config_bus[  X11_ADDR_WDTH -1: 0];     
            end
            `AVAILABLE_CONFIG_PIC_TILE: begin
                tile_x <=   config_bus[X11_ADDR_WDTH + X_ADDR_WDTH -1: X_ADDR_WDTH];
                tile_y <=   config_bus[  X11_ADDR_WDTH -1: 0];     
            end 
        endcase
    end

    

    
    always@(posedge clk) begin
        if(min_tb_address_z_N < min_tb_address_z_P) begin
            min_tb_address_z_check <= 1;
        end
        else begin
            min_tb_address_z_check <= 0;
        end
    end
    
    always@(posedge clk) begin
        if( (slice_x < xN_in) && (slice_y < yN_in)) begin
            slice_id_check <= 1;
        end
        else begin
            slice_id_check <= 0;
        end
    end
 
    // always@(posedge clk) begin
        // if( yN_in < slice_y) begin
            // slice_id_check <= 0;
        // end
        // else if( )begin  /// yN_in <ctu_size+ slice_y 
			// if(xN_in < slice_x) begin
				// slice_id_check <= 0;
			// end
            // else begin
				// slice_id_check <= 1;			
			// end
        // end
		// else begin
			// slice_id_check <= 1;
		// end
    // end
 
     always@(posedge clk) begin
        if( (tile_x < xN_in) && (tile_y < yN_in)) begin
            tile_id_check <= 1;
        end
        else begin
            tile_id_check <= 0;
        end
    end   
    
    always@(posedge clk) begin
        if( (xN_in >= 0) && (yN_in >=0) && (xN_in < pic_width) && (yN_in < pic_height)) begin
            xyN_with_in_pic_check <= 1;
        end
        else begin
            xyN_with_in_pic_check <= 0;
        end
        // if( (xN_in >= 0)  && (xN_in < pic_width)) begin
            // xN_with_in_pic_check <= 1;
        // end
        // else begin
            // xN_with_in_pic_check <= 0;
        // end
        // if((yN_in >=0) &&  (yN_in < pic_height)) begin
            // yN_with_in_pic_check <= 1;
        // end
        // else begin
            // yN_with_in_pic_check <= 0;
        // end
    end
    
    always@(posedge clk) begin
        cb_x_low_check <= (xCb_start_in <= xN_in)? 1'b1 :1'b0;
        cb_x_high_check <= (xCb_end_in > xN_in)? 1'b1 :1'b0;
        cb_y_low_check <= (yCb_start_in <= yN_in)? 1'b1: 1'b0;
        cb_y_high_check <= (yCb_end_in > yN_in)? 1'b1: 1'b0;
        
        if(cb_x_high_check & cb_x_low_check & cb_y_low_check & cb_y_high_check) begin
            same_cb_check <= 1;
        end
        else begin
            same_cb_check <= 0;
        end
    end
    
    
    always@(posedge clk) begin
        xN_pu_mid <= xN_in[X11_ADDR_WDTH -1:LOG2_MIN_PU_SIZE];
        yN_pu_mid <= yN_in[X11_ADDR_WDTH -1:LOG2_MIN_PU_SIZE];
        
        xN_pu_out <= xN_pu_mid;
        yN_pu_out <= yN_pu_mid;
    end
    
    always@(posedge clk) begin
        if(slice_id_check & tile_id_check  & ((min_tb_address_z_check & min_tb_valid )| (xN_outer_smaller & yN_outer_smaller_eq & inner_ctb_x_zero) | (yN_outer_smaller & inner_ctb_y_zero)) & xyN_with_in_pic_check) begin
            avaialble_flag_out <= 1;
        end
        else begin
            avaialble_flag_out <= 0;
        end
    end
endmodule
