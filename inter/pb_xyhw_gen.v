`timescale 1ns / 1ps
module pb_xyhw_gen(
    clk,
    // reset,
    cb_part_mode ,
    pb_part_idx,
    cb_size,
    cb_xx,
    cb_yy,
    xx_pb,
    yy_pb,
    hh_pb,
    ww_pb
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    // input reset;
    input [PART_IDX_WIDTH -1:0]         pb_part_idx;
    input [PARTMODE_WIDTH -1:0]         cb_part_mode;
    input [CB_SIZE_WIDTH -1:0 ]         cb_size;
    input  [X11_ADDR_WDTH - 1:0]        cb_xx;
    input  [X11_ADDR_WDTH - 1:0]        cb_yy;
    output reg [X11_ADDR_WDTH - 1:0]    xx_pb;
    output reg [Y11_ADDR_WDTH - 1:0]    yy_pb;
    output reg [CB_SIZE_WIDTH -1:0 ]    hh_pb;
    output reg [CB_SIZE_WIDTH -1:0 ]    ww_pb;

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module


always@(posedge clk) begin    
    case(cb_part_mode)
        `PART_2Nx2N: begin
            xx_pb <= cb_xx;
            yy_pb <= cb_yy;
            hh_pb <= cb_size;
            ww_pb <= cb_size;
        end
        `PART_2NxN: begin
            hh_pb <= cb_size >> 1;
            ww_pb <= cb_size;
            xx_pb <= cb_xx;
            if (pb_part_idx == 0) begin
                yy_pb <= cb_yy;
            end
            else begin
                yy_pb <= (cb_yy + (cb_size >> 1))%(1<<Y11_ADDR_WDTH);
            end
        end
        `PART_Nx2N: begin
            hh_pb <= cb_size;
            ww_pb <= cb_size >> 1;
            yy_pb <= cb_yy;
            if (pb_part_idx == 0) begin
                xx_pb <= cb_xx;
            end
            else begin
                xx_pb <= (cb_xx + (cb_size>>1))%(1<<X11_ADDR_WDTH);
            end
        end
        `PART_NxN: begin
            hh_pb <= cb_size >> 1;
            ww_pb <= cb_size >> 1;
            case(pb_part_idx)
                0: begin
                    xx_pb <= cb_xx;
                    yy_pb <= cb_yy;
                end
                1: begin
                    xx_pb <= (cb_xx + (cb_size>>1))%(1<<X11_ADDR_WDTH);
                    yy_pb <= cb_yy;
                end
                2: begin
                    xx_pb <= cb_xx;
                    yy_pb <= (cb_yy + (cb_size >> 1))%(1<<Y11_ADDR_WDTH);
                end
                3: begin
                    xx_pb <= (cb_xx + (cb_size>>1))%(1<<X11_ADDR_WDTH);
                    yy_pb <= (cb_yy + (cb_size >> 1))%(1<<Y11_ADDR_WDTH);
                end
            endcase
        end
        `PART_nLx2N: begin
            hh_pb <= cb_size;
            yy_pb <= cb_yy;
            if (pb_part_idx == 0) begin
              xx_pb <= cb_xx;
              ww_pb <= cb_size >> 2;
            end
            else begin
              xx_pb <= (cb_xx + (cb_size >>2))%(1<< X11_ADDR_WDTH);
              ww_pb <= ((cb_size >> 1) + (cb_size >> 2))%(1<<CB_SIZE_WIDTH);
            end
        end
        `PART_nRx2N: begin
            hh_pb <= cb_size;
            yy_pb <= cb_yy;
            if(pb_part_idx == 0) begin
                xx_pb <= cb_xx;
                ww_pb <= (cb_size>>2) + (cb_size>>1);
            end
            else begin
                xx_pb <= cb_xx + (cb_size >>1) + (cb_size >> 2);
                ww_pb <= cb_size >> 2;
            end
        end
        `PART_2NxnD: begin
            ww_pb <= cb_size;
            xx_pb <= cb_xx;
            if (pb_part_idx == 0) begin
              yy_pb <= cb_yy;
              hh_pb <= (cb_size>>2) + (cb_size >>1);
            end
            else begin
              yy_pb <= cb_yy + (cb_size >> 2) + (cb_size >> 1);
              hh_pb <= cb_size >>2;
            end
        end
        `PART_2NxnU: begin
            ww_pb <= cb_size;
            xx_pb <= cb_xx;
            if (pb_part_idx == 0) begin
                yy_pb <= cb_yy;
                hh_pb <= (cb_size >> 2);
            end
            else begin
                yy_pb <= cb_yy + (cb_size >> 2);
                hh_pb <= (cb_size >>2)+ (cb_size >>1);
            end
        end
    endcase
end
 
endmodule 