`timescale 1ns / 1ps
module inter_pred_sample_gen(
    clk,
    reset,
    config_data_bus_in ,
    config_mode_in ,
    tu_empty,
    tu_data_in,

    bs_fifo_data_out    ,
    bs_fifo_wr_en_out   ,
    bs_fifo_full_in     ,
	
	y_dbf_fifo_is_full_hold,
	cb_dbf_fifo_is_full_hold,
	cr_dbf_fifo_is_full_hold,

    next_mv_field_pred_flag_l0_in,
    next_mv_field_pred_flag_l1_in,

    next_mv_field_mv_x_l0_in,
    next_mv_field_mv_y_l0_in,
    next_mv_field_mv_x_l1_in,
    next_mv_field_mv_y_l1_in,    
    next_dpb_idx_l0_in,    
    next_dpb_idx_l1_in,    
    next_mv_field_valid_in, 

    xT_in_min_luma_out ,
    yT_in_min_luma_out ,
    xT_in_min_cr_out   ,
    yT_in_min_cr_out   ,
    yT_in_min_cb_out   ,
    xT_in_min_cb_out   ,
    luma_wgt_pred_out  ,
    cb_wgt_pred_out    ,
    cr_wgt_pred_out    , 
    luma_wght_pred_valid ,
    cb_wght_pred_valid   ,
    cr_wght_pred_valid   ,

    tu_res_pres_cb_rd_en      ,         
    tu_res_pres_cb_out        ,  
    tu_res_pres_cb_empty      ,  

    tu_res_pres_cr_rd_en      ,  
    tu_res_pres_cr_out        ,  
    tu_res_pres_cr_empty      ,  

    y_res_present_out           ,
    cb_res_present_out          ,
    cr_res_present_out          ,

    ref_pix_axi_ar_addr,
    ref_pix_axi_ar_len,
    ref_pix_axi_ar_size,
    ref_pix_axi_ar_burst,
    ref_pix_axi_ar_prot,
    ref_pix_axi_ar_valid,
    ref_pix_axi_ar_ready,
    ref_pix_axi_r_data,
    ref_pix_axi_r_resp,
    ref_pix_axi_r_last,
    ref_pix_axi_r_valid,
    ref_pix_axi_r_ready,    


    top_mvs_to_bs_in        , 
    left_mvs_to_bs_in       ,
    x_rel_top_in            ,
    y_rel_left_in           ,
    bs_left_valid_in        ,
    bs_top_valid_in         ,

    inter_y_predsample_4by4_last_row        ,
    inter_y_predsample_4by4_last_col        ,
    inter_cb_predsample_4by4_last_row       ,
    inter_cb_predsample_4by4_last_col       ,
    inter_cr_predsample_4by4_last_row       ,
    inter_cr_predsample_4by4_last_col       ,
    
    inter_filter_cache_idle_out,
    pred_pixl_gen_idle_out_to_pred_top,
    pred_pixl_gen_idle_out_to_mv_derive
    ,state_8bit_out
	,test_xT_in_min_luma_filt
	,test_xT_in_min_luma_cache
	,test_yT_in_min_luma_filt
	,test_yT_in_min_luma_cache
	,test_luma_filter_out
	,test_luma_filter_ready
 	,test_cache_addr
 	,test_cache_en
	,test_cache_luma_data
	,test_cache_valid_in
	,test_ref_block_en
	,test_ref_luma_data_4x4
    
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"
    `include "../sim/inter_axi_def.v" // always define below pred_def
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                          STATE_INTER_PRED_SAMPLE_CONFIG_UPDATE    = 0;
    localparam                          STATE_INTER_PRED_SAMPLE_FILL_TU_DATA     = 1;
    localparam                          STATE_INTER_PRED_SAMPLE_X_8_Y_8_UPDATE   = 2;
    localparam                          STATE_INTER_PRED_SAMPLE_PU_SELECT        = 3;
    localparam                          STATE_INTER_PRED_MV_INT_DECODE           = 4;
    localparam                          STATE_INTER_PRED_MV_FRAC_DECODE          = 5;
    localparam                          STATE_INTER_PRED_MV_DECODE_READY_BI      = 6;
    localparam                          STATE_INTER_PRED_MV_DECODE_READY         = 7;
    localparam                          STATE_INTER_PRED_MV_FRAC_BI_DECODE       = 8;
    localparam                          STATE_INTRA_PRED_SAMPLE_X_4_Y_4_UPDATE1       = 9;
    localparam                          STATE_INTRA_PRED_SAMPLE_X_4_Y_4_UPDATE2       = 10;
    localparam                          STATE_INTER_PRED_PU_SELECT_WAIT       		= 11;
    localparam                          STATE_INTER_PRED_SAMPLE_MV_VALID_WAIT    	= 12;
    
 	localparam 							STATE_IDLE = 0;
	localparam 							STATE_ACTIVE = 1;
	
    parameter                           REF_BLOCK_DIM_WDTH		    	= 4;
    parameter                           BLOCK_WIDTH_4x4                 = 3'd4;
    parameter                           BLOCK_WIDTH_2x2                 = 2'd2;
    parameter                           CHMA_DIM_WDTH               = 3;        // max 5 
    parameter                           LUMA_DIM_WDTH               = 4;        // max 11 
    parameter                           LUMA_REF_BLOCK_WIDTH        = 4'd11;
    parameter                           CHMA_REF_BLOCK_WIDTH        = 3'd5;
    
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
    input tu_empty;
    input [INTER_TOP_CONFIG_BUS_WIDTH-1:0] tu_data_in;
    input [INTER_TOP_CONFIG_BUS_WIDTH -1:0] config_data_bus_in;
    input [INTER_TOP_CONFIG_BUS_MODE_WIDTH -1 :0] config_mode_in;

    input                                  next_mv_field_pred_flag_l0_in;
    input                                  next_mv_field_pred_flag_l1_in;
    // input [REF_IDX_LX_WIDTH -1:0]          next_mv_field_ref_idx_l0_in;
    // input [REF_IDX_LX_WIDTH -1:0]          next_mv_field_ref_idx_l1_in;
    input signed [MVD_WIDTH -1:0]          next_mv_field_mv_x_l0_in;
    input signed [MVD_WIDTH -1:0]          next_mv_field_mv_y_l0_in;
    input signed [MVD_WIDTH -1:0]          next_mv_field_mv_x_l1_in;
    input signed [MVD_WIDTH -1:0]          next_mv_field_mv_y_l1_in;    
    input [DPB_ADDR_WIDTH -1:0]            next_dpb_idx_l0_in;    
    input [DPB_ADDR_WIDTH -1:0]            next_dpb_idx_l1_in;    
    input                                  next_mv_field_valid_in; 
    
    // input  [X11_ADDR_WDTH - 1:0]           next_xx_pb_in;
    // input  [CB_SIZE_WIDTH -1:0 ]           next_hh_pb_in;
    // input  [Y11_ADDR_WDTH - 1:0]           next_yy_pb_in;
    // input  [CB_SIZE_WIDTH -1:0 ]           next_ww_pb_in;

    input    [MV_FIELD_DATA_WIDTH -1:0]      top_mvs_to_bs_in;
    input    [MV_FIELD_DATA_WIDTH -1:0]      left_mvs_to_bs_in;
    input    [CTB_SIZE_WIDTH -1:0]           x_rel_top_in;
    input    [CTB_SIZE_WIDTH -1:0]           y_rel_left_in;
    input                                    bs_left_valid_in;
    input                                    bs_top_valid_in;


    
    output wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                               xT_in_min_luma_out       ;
    output wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                               yT_in_min_luma_out       ;
    output wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                               xT_in_min_cr_out         ;
    output wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                               yT_in_min_cr_out         ;
    output wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                               yT_in_min_cb_out         ;
    output wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                               xT_in_min_cb_out         ;
    output        [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 *     PIXEL_WIDTH -1:0]          luma_wgt_pred_out        ;    
    output        [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 *     PIXEL_WIDTH -1:0]          cb_wgt_pred_out          ;    
    output        [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 *     PIXEL_WIDTH -1:0]          cr_wgt_pred_out          ;     
    output                                                                             luma_wght_pred_valid     ;
    output                                                                             cb_wght_pred_valid       ;
    output                                                                             cr_wght_pred_valid       ;

    output  reg                             pred_pixl_gen_idle_out_to_pred_top;
    output  reg                             pred_pixl_gen_idle_out_to_mv_derive;

    // axi master interface         
    output      [AXI_ADDR_WDTH-1:0]                 ref_pix_axi_ar_addr;
    output      [7:0]                               ref_pix_axi_ar_len;
    output      [2:0]                               ref_pix_axi_ar_size;
    output      [1:0]                               ref_pix_axi_ar_burst;
    output      [2:0]                               ref_pix_axi_ar_prot;
    output                                          ref_pix_axi_ar_valid;
    input                                           ref_pix_axi_ar_ready;
                
    input       [AXI_CACHE_DATA_WDTH-1:0]           ref_pix_axi_r_data;
    input       [1:0]                               ref_pix_axi_r_resp;
    input                                           ref_pix_axi_r_last;
    input                                           ref_pix_axi_r_valid;
    output                                          ref_pix_axi_r_ready;

    output reg  tu_res_pres_cb_rd_en ;
    input       tu_res_pres_cb_out   ;
    input       tu_res_pres_cb_empty ;
 
    output reg  tu_res_pres_cr_rd_en ;
    input       tu_res_pres_cr_out   ;
    input       tu_res_pres_cr_empty ;
    output      inter_filter_cache_idle_out;

   output       cb_res_present_out;

   output       cr_res_present_out;


    output reg inter_y_predsample_4by4_last_row;
    output reg inter_y_predsample_4by4_last_col;

    output reg inter_cb_predsample_4by4_last_row;
    output reg inter_cb_predsample_4by4_last_col;

    output reg inter_cr_predsample_4by4_last_row;
    output reg inter_cr_predsample_4by4_last_col;

    output [BS_FIFO_WIDTH-1:0]   bs_fifo_data_out       ;
    output                      bs_fifo_wr_en_out       ;
    input                       bs_fifo_full_in         ; // should ensure this line never asserted    

	input y_dbf_fifo_is_full_hold;
	input cb_dbf_fifo_is_full_hold;
	input cr_dbf_fifo_is_full_hold;
	
	output [7:0] state_8bit_out;
	
    output [9 - 1:0] test_xT_in_min_luma_filt;
    output [9 - 1:0] test_xT_in_min_luma_cache;
    output [9 - 1:0] test_yT_in_min_luma_cache;
    output [9 - 1:0] test_yT_in_min_luma_filt;	
    output        									test_luma_filter_ready;
    output        [256 -1:0]         test_luma_filter_out;    
    output        [128 -1:0]         test_ref_luma_data_4x4;    
	output [7-1:0] test_cache_addr;
	output test_cache_en;
	output [512-1:0] test_cache_luma_data;
	output test_cache_valid_in;
	output test_ref_block_en;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    integer inter_pred_sample_state;
    integer inter_pred_sample_next_state;
    
    reg                                  prev_mv_field_pred_flag_l0 [MAX_MV_PER_CU -1: 0];
    reg                                  prev_mv_field_pred_flag_l1 [MAX_MV_PER_CU -1: 0];
    // reg [REF_IDX_LX_WIDTH -1:0]          prev_mv_field_ref_idx_l0 [MAX_MV_PER_CU -1: 0];
    // reg [REF_IDX_LX_WIDTH -1:0]          prev_mv_field_ref_idx_l1 [MAX_MV_PER_CU -1: 0];
    reg signed [MVD_WIDTH -1:0]          prev_mv_field_mv_x_l0 [MAX_MV_PER_CU -1: 0];
    reg signed [MVD_WIDTH -1:0]          prev_mv_field_mv_y_l0 [MAX_MV_PER_CU -1: 0];
    reg signed [MVD_WIDTH -1:0]          prev_mv_field_mv_x_l1 [MAX_MV_PER_CU -1: 0];
    reg signed [MVD_WIDTH -1:0]          prev_mv_field_mv_y_l1 [MAX_MV_PER_CU -1: 0];    
    reg [DPB_ADDR_WIDTH -1:0]            prev_dpb_idx_l0 [MAX_MV_PER_CU -1: 0];    
    reg [DPB_ADDR_WIDTH -1:0]            prev_dpb_idx_l1 [MAX_MV_PER_CU -1: 0];    
    reg                                  prev_mv_field_valid [MAX_MV_PER_CU -1: 0]; 
    
    reg                                  cand_mv_field_pred_flag_l0;
    reg                                  cand_mv_field_pred_flag_l1;
    // reg [REF_IDX_LX_WIDTH -1:0]          cand_mv_field_ref_idx_l0;
    // reg [REF_IDX_LX_WIDTH -1:0]          cand_mv_field_ref_idx_l1;
    reg signed [MVD_WIDTH -1:0]          cand_mv_field_mv_x_l0;
    reg signed [MVD_WIDTH -1:0]          cand_mv_field_mv_y_l0;
    reg signed [MVD_WIDTH -1:0]          cand_mv_field_mv_x_l1;
    reg signed [MVD_WIDTH -1:0]          cand_mv_field_mv_y_l1;    
    reg [DPB_ADDR_WIDTH -1:0]            cand_dpb_idx_l0;    
    reg [DPB_ADDR_WIDTH -1:0]            cand_dpb_idx_l1;    
    
 

    
    
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           xx_pb0_strt_in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           xx_pb0_end__in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           yy_pb0_strt_in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           yy_pb0_end__in_min_tus;

    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           xx_pb1_strt_in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           xx_pb1_end__in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           yy_pb1_strt_in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           yy_pb1_end__in_min_tus;
    
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           xx_pb2_strt_in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           xx_pb2_end__in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           yy_pb2_strt_in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           yy_pb2_end__in_min_tus;
    
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           xx_pb3_strt_in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           xx_pb3_end__in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           yy_pb3_strt_in_min_tus;
    wire  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]           yy_pb3_end__in_min_tus;
    
    
    reg  [PART_IDX_WIDTH:0]  pb_part_idx;
    reg  [MAX_MV_PER_CU -1:0]   pb_part_idx_decode;
    
    reg pu_0_in_range;
    reg pu_1_in_range;
    reg pu_2_in_range;
    reg pu_3_in_range;
    
    reg                         now_cb_pred_mode;    
    reg                         next_cb_pred_mode;    
    reg  [PARTMODE_WIDTH -1:0]  now_cb_part_mode;
    reg  [PARTMODE_WIDTH -1:0]  next_cb_part_mode;
    
    reg  [CB_SIZE_WIDTH - LOG2_MIN_TU_SIZE -1:0 ]  now_cb_size_in_min_tus;
    reg  [CB_SIZE_WIDTH - LOG2_MIN_TU_SIZE -1:0 ]  next_cb_size_in_min_tus;
    
    reg  [CB_SIZE_WIDTH - LOG2_MIN_TU_SIZE -1:0 ]  cb_size_wire_in_min_pus;
    wire [LOG2_CB_SIZE_WIDTH -1:0 ]  log2_cb_size_wire;
    
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] now_cb_xx_end_in_min_tus;
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] now_cb_yy_end_in_min_tus;
    
    
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] now_ctb_xx_in_min_tus;
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] next_ctb_xx_in_min_tus;
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] now_ctb_yy_in_min_tus;
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] next_ctb_yy_in_min_tus;
    
    
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] next_cb_xx_in_min_tus;
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] next_cb_yy_in_min_tus;
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] now_cb_yy_in_min_tus;
    reg  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] now_cb_xx_in_min_tus;
    
    wire  [XT_WIDTH -1:0 ]          x0_tu_in_ctu_wire;
    wire  [XT_WIDTH -1:0 ]          y0_tu_in_ctu_wire;    
    
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] next_xT_in_min_tus;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] next_yT_in_min_tus;
    
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus_out;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus_out;

    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_luma;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_luma;

    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_cb;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_cb;

    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_cr;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_cr;

    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x_8x8_loc;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] next_x_8x8_loc;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y_8x8_loc;    
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] next_y_8x8_loc;  

    reg inner_x_8x8_loc;
    reg next_inner_x_8x8_loc;
    reg inner_y_8x8_loc;
    reg next_inner_y_8x8_loc;
    
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_in_min_tus;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_in_min_tus;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus;
    
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH:0] luma_l0_ref_start_x_wire;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH:0] luma_l1_ref_start_x_wire;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH:0] luma_l0_ref_start_y_wire;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH:0] luma_l1_ref_start_y_wire;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH:0] chma_l0_ref_start_x_wire;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH:0] chma_l0_ref_start_y_wire;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH:0] chma_l1_ref_start_x_wire;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH:0] chma_l1_ref_start_y_wire;
    
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] luma_ref_start_x;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] luma_ref_start_y;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] chma_ref_start_x;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] chma_ref_start_y;

    
    reg [REF_BLOCK_DIM_WDTH - 1:0]   chma_ref_width_x            ;
    reg [REF_BLOCK_DIM_WDTH - 1:0]   chma_ref_height_y           ;
    reg [REF_BLOCK_DIM_WDTH - 1:0]   luma_ref_width_x            ;
    reg [REF_BLOCK_DIM_WDTH - 1:0]   luma_ref_height_y           ;

    reg [REF_BLOCK_DIM_WDTH - 1:0]   chma_l0_ref_width_x_wire    ;
    reg [REF_BLOCK_DIM_WDTH - 1:0]   chma_l1_ref_width_x_wire    ;
    reg [REF_BLOCK_DIM_WDTH - 1:0]   chma_l0_ref_height_y_wire   ;
    reg [REF_BLOCK_DIM_WDTH - 1:0]   chma_l1_ref_height_y_wire   ;
    
    reg [REF_BLOCK_DIM_WDTH - 1:0]   luma_l0_ref_width_x_wire    ;
    reg [REF_BLOCK_DIM_WDTH - 1:0]   luma_l1_ref_width_x_wire    ;
    reg [REF_BLOCK_DIM_WDTH - 1:0]   luma_l0_ref_height_y_wire   ;
    reg [REF_BLOCK_DIM_WDTH - 1:0]   luma_l1_ref_height_y_wire   ;
    
    reg                              ref_range_valid_out         ;
    reg [DPB_ADDR_WIDTH -1:0]        ref_dpb_idx;   
        
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] luma_l0_mid_x_wire;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] luma_l0_mid_y_wire;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] chma_l0_mid_x_wire;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] chma_l0_mid_y_wire;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] luma_l1_mid_x_wire;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] luma_l1_mid_y_wire;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] chma_l1_mid_x_wire;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] chma_l1_mid_y_wire;
    
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] luma_l0_mid_x;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] luma_l0_mid_y;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] chma_l0_mid_x;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] chma_l0_mid_y;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] luma_l1_mid_x;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] luma_l1_mid_y;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] chma_l1_mid_x;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0] chma_l1_mid_y;

    wire  [CIDX_WIDTH -1:0 ]  c_idx_wire;

    wire  res_present_wire;
    reg      y_res_present;
    reg     cb_res_present;
    reg     cr_res_present;
    
    reg end_of_cu;
    reg end_of_cu_d;
    reg end_of_tu;

    

    reg start_of_tu_reg1;
    reg start_of_tu_reg2;
    reg start_of_tu_reg3_cb;
    reg start_of_tu_reg3_cr;

   wire          y_res_present_reg2;
   wire          y_res_present_reg3;
   output       y_res_present_out;

    reg is_pu_header_cus_last;
    
    reg         [PIC_DIM_WIDTH-1:0]                 pic_width;
    reg         [PIC_DIM_WIDTH-1:0]                 pic_height;
    
    wire cache_idle_out;
    wire cache_idle_in;// =(!ref_dpb_idx)?1:cache_idle_out;//= 1;  // to do connect this to cache
    assign  cache_idle_in = cache_idle_out;
    wire [LOG2_CB_SIZE_WIDTH -1:0 ]  log2_tb_size_wire;

    reg [MV_C_FRAC_WIDTH_HIGH -1:0] ch_frac_x;
    reg [MV_C_FRAC_WIDTH_HIGH -1:0] ch_frac_y;

    wire cache_block_ready;


    wire  [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  luma_ref_start_x_out   ;
    wire  [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  luma_ref_start_y_out   ;
    wire  [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  chma_ref_start_x_out   ;
    wire  [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  chma_ref_start_y_out   ;

    wire   [REF_BLOCK_DIM_WDTH - 1:0]                   chma_ref_width_x_out   ;
    wire   [REF_BLOCK_DIM_WDTH - 1:0]                   chma_ref_height_y_out  ;
    wire   [REF_BLOCK_DIM_WDTH - 1:0]                   luma_ref_width_x_out   ;
    wire   [REF_BLOCK_DIM_WDTH - 1:0]                   luma_ref_height_y_out  ;

    wire  [PIXEL_WIDTH* LUMA_REF_BLOCK_WIDTH* LUMA_REF_BLOCK_WIDTH -1:0]     block_121_out ;
    wire  [PIXEL_WIDTH* CHMA_REF_BLOCK_WIDTH* CHMA_REF_BLOCK_WIDTH -1:0]     block_25cb_out;
    wire  [PIXEL_WIDTH* CHMA_REF_BLOCK_WIDTH* CHMA_REF_BLOCK_WIDTH -1:0]     block_25cr_out;

    wire         [LUMA_DIM_WDTH-1:0]                 block_x_offset_luma;
    wire         [LUMA_DIM_WDTH-1:0]                 block_y_offset_luma;
    wire         [CHMA_DIM_WDTH-1:0]                 block_x_offset_chma;
    wire         [CHMA_DIM_WDTH-1:0]                 block_y_offset_chma; 

    wire         [LUMA_DIM_WDTH-1:0]                 block_x_end_luma;
    wire         [LUMA_DIM_WDTH-1:0]                 block_y_end_luma;
    wire         [CHMA_DIM_WDTH-1:0]                 block_x_end_chma;
    wire         [CHMA_DIM_WDTH-1:0]                 block_y_end_chma; 


    wire        luma_filter_idle;
    wire        cb_filter_idle;
    wire        cr_filter_idle;

    wire        [MV_C_FRAC_WIDTH_HIGH -1:0]         ch_frac_x_cache_out;
    wire        [MV_C_FRAC_WIDTH_HIGH -1:0]         ch_frac_y_cache_out;

    wire        luma_filter_ready;
    wire        cb_filter_ready;
    wire        cr_filter_ready;


    wire        [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 * FILTER_PIXEL_WIDTH -1:0]         luma_filter_out;    
    wire        [BLOCK_WIDTH_2x2 *  BLOCK_WIDTH_2x2 * FILTER_PIXEL_WIDTH -1:0]         cb_filter_out;    
    wire        [BLOCK_WIDTH_2x2 *  BLOCK_WIDTH_2x2 * FILTER_PIXEL_WIDTH -1:0]         cr_filter_out;    


    reg  bi_pred_block_cache_in;
    wire bi_pred_block_filter_in;
    wire bi_pred_block_y_filter_out;
    wire bi_pred_block_cb_filter_out;
    wire bi_pred_block_cr_filter_out;

    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_cache_out;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_yy_filt_out;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_cb_filt_out;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_cr_filt_out;

    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_yy_wgt_out;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_cb_wgt_out;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_cr_wgt_out;

    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_cache_out;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_yy_filt_out;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_cb_filt_out;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_cr_filt_out;

    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_yy_wgt_out;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_cb_wgt_out;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_cr_wgt_out;

    reg end_of_tu_cr_rd_en_stage_2;
    reg end_of_tu_cr_rd_en;
    reg end_of_tu_cb_rd_en;

    reg cand_mv_cache_valid;
    reg res_present_pu_range_4by4_valid;

    wire intra_tu_4by4_valid_to_bs;
    reg  intra_tu_4by4_valid_to_bs_d;
	reg  [TB_SIZE_WIDTH -LOG2_MIN_TU_SIZE:0 ]  tb_size_wire_in_min_tus;
	reg inter_idle_to_intra;
	wire miss_elem_fifo_empty_out;
	
	wire tu_res_pres_cb_in_stage_2;
	wire tu_res_pres_cr_in_stage_2;
	wire tu_res_pres_cb_wr_en_stage_2;
	reg  tu_res_pres_cb_rd_en_stage_2;
	wire tu_res_pres_cr_wr_en_stage_2;	
	reg  tu_res_pres_cr_rd_en_stage_2;	
	
	reg first_mv_of_cu_ready;
	reg [3:0] bs_fill_started_range;
	reg [3:0] bs_fill_started_range_d;
	reg [1:0] bs_fill_range_case;
	reg [1:0] bs_fill_range_idx;
	
	reg dbf_fifo_is_full_hold_d;
	
	wire luma_full_filter_idle;
	wire cb_full_filter_idle;
	wire cr_full_filter_idle;
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module
`ifdef CHIPSCOPE_DEBUG
	assign state_8bit_out = inter_pred_sample_state[7:0];
	// assign test_xT_in_min_luma_cache = xT_in_min_tus_out;  // out xy of cache used because it is not updated with intra
	assign test_xT_in_min_luma_filt = xT_in_min_luma;
	// assign test_yT_in_min_luma_cache = yT_in_min_tus_out;
	assign test_yT_in_min_luma_filt = yT_in_min_luma;
	assign test_luma_filter_out = luma_filter_out;
	assign test_luma_filter_ready = luma_filter_ready;
	assign test_cache_valid_in = ref_range_valid_out;
`endif
// 

// synthesis translate_off
ctu_monitor ctu_monitor_block(
    .clk(clk),
    .reset(reset),
    .valid_in({intra_tu_4by4_valid_to_bs_d || res_present_pu_range_4by4_valid}),
	.ar_valid(ref_pix_axi_ar_valid),
	.ar_ready(ref_pix_axi_ar_ready),
    .xT_4x4_in(xT_in_min_tus),
    .bi_pred_block_cache_in(bi_pred_block_cache_in),
    .yT_4x4_in(yT_in_min_tus)
);
// synthesis translate_on

	assign tu_res_pres_cb_in_stage_2 = tu_res_pres_cb_out;
	assign tu_res_pres_cr_in_stage_2 = tu_res_pres_cr_out;
	
	assign tu_res_pres_cb_wr_en_stage_2 = ref_range_valid_out & (bi_pred_block_cache_in==0) & xT_in_min_tus[0]==1 & yT_in_min_tus[0]==1;//write blocks when xy both are odd, to account for cb cr blocks being 4 times less than luma
	assign tu_res_pres_cr_wr_en_stage_2 = ref_range_valid_out & (bi_pred_block_cache_in==0) & xT_in_min_tus[0]==1 & yT_in_min_tus[0]==1;
	
// synthesis translate_off

	wire tu_res_pres_cb_full_stage_2;
	wire tu_res_pres_cb_empty_stage_2;
	
fifo_inout_monitor
	#(
		.WIDTH 		  (1),
		.FILE_IN_WIDTH(1),
		.FILE_NAME    (""),
		.OUT_VERIFY	   	(0),
		.DEBUG			(0)
	)
	cb_res_fifo_monitor_block (
    .clk(clk),
    .reset(reset),
    .in(tu_res_pres_cb_in_stage_2),
    .out(tu_res_pres_cb_out_stage_2),
    .full(tu_res_pres_cb_full_stage_2),
    .empty(tu_res_pres_cb_empty_stage_2),
    .rd_en(tu_res_pres_cb_rd_en_stage_2),
    .wr_en(tu_res_pres_cb_wr_en_stage_2)
    );
// synthesis translate_on
       geet_fifo #(
        .FIFO_DATA_WIDTH(1),
		.LOG2_FIFO_DEPTH(6)
    ) res_present_cb_block_stage_2 (
        .clk(clk),
        .reset(reset),
        .wr_en	(tu_res_pres_cb_wr_en_stage_2),
        .rd_en	(tu_res_pres_cb_rd_en_stage_2),
        .d_in	(tu_res_pres_cb_in_stage_2),
        .d_out	(tu_res_pres_cb_out_stage_2),
        .empty	(tu_res_pres_cb_empty_stage_2),
        .full	(tu_res_pres_cb_full_stage_2)
        );
		
// synthesis translate_off

	wire tu_res_pres_cr_full_stage_2;
	wire tu_res_pres_cr_empty_stage_2;
	

	
fifo_inout_monitor
	#(
		.WIDTH 		  (1),
		.FILE_IN_WIDTH(1),
		.FILE_NAME    (""),
		.OUT_VERIFY	   	(0),
		.DEBUG			(0)
	)
	cr_res_fifo_monitor_block (
    .clk(clk),
    .reset(reset),
    .in(tu_res_pres_cr_in_stage_2),
    .out(tu_res_pres_cr_out_stage_2),
    .full(tu_res_pres_cr_full_stage_2),
    .empty(tu_res_pres_cr_empty_stage_2),
    .rd_en(tu_res_pres_cr_rd_en_stage_2),
    .wr_en(tu_res_pres_cr_wr_en_stage_2)
    );
// synthesis translate_on		
       geet_fifo #(
        .FIFO_DATA_WIDTH(1),
		.LOG2_FIFO_DEPTH(6)
    ) res_present_cr_block_stage_2 (
        .clk(clk),
        .reset(reset),
        .wr_en(tu_res_pres_cr_wr_en_stage_2),
        .rd_en(tu_res_pres_cr_rd_en_stage_2),
        .d_in(tu_res_pres_cr_in_stage_2),
        .d_out(tu_res_pres_cr_out_stage_2),
        .empty(tu_res_pres_cr_empty_stage_2),
        .full(tu_res_pres_cr_full_stage_2)
        );

// Instantiate the module
bs_core bs_core_block (
    .clk(clk), 
    .reset(reset), 
    .intra_tu_4by4_valid_in(intra_tu_4by4_valid_to_bs_d), 
    .x0_tu_in_min_tus_in(x0_tu_in_min_tus), 
    .y0_tu_in_min_tus_in(y0_tu_in_min_tus),     
    .cand0_mv_field_mv_x_l0     (prev_mv_field_mv_x_l0[0]), 
    .cand0_mv_field_mv_y_l0     (prev_mv_field_mv_y_l0[0]), 
    .cand0_mv_field_mv_x_l1     (prev_mv_field_mv_x_l1[0]), 
    .cand0_mv_field_mv_y_l1     (prev_mv_field_mv_y_l1[0]), 
    .cand0_dpb_idx_l0           (prev_dpb_idx_l0[0]), 
    .cand0_dpb_idx_l1           (prev_dpb_idx_l1[0]), 
    .cand1_mv_field_mv_x_l0     (prev_mv_field_mv_x_l0[1]), 
    .cand1_mv_field_mv_y_l0     (prev_mv_field_mv_y_l0[1]), 
    .cand1_mv_field_mv_x_l1     (prev_mv_field_mv_x_l1[1]), 
    .cand1_mv_field_mv_y_l1     (prev_mv_field_mv_y_l1[1]), 
    .cand1_dpb_idx_l0           (prev_dpb_idx_l0[1]), 
    .cand1_dpb_idx_l1           (prev_dpb_idx_l1[1]), 

    .cand2_mv_field_mv_x_l0     (prev_mv_field_mv_x_l0[2]), 
    .cand2_mv_field_mv_y_l0     (prev_mv_field_mv_y_l0[2]), 
    .cand2_mv_field_mv_x_l1     (prev_mv_field_mv_x_l1[2]), 
    .cand2_mv_field_mv_y_l1     (prev_mv_field_mv_y_l1[2]), 
    .cand2_dpb_idx_l0           (prev_dpb_idx_l0[2]), 
    .cand2_dpb_idx_l1           (prev_dpb_idx_l1[2]), 

    .cand0_mv_field_pred_flag_l0(prev_mv_field_pred_flag_l0[0]), 
    .cand0_mv_field_pred_flag_l1(prev_mv_field_pred_flag_l1[0]), 
    .cand1_mv_field_pred_flag_l0(prev_mv_field_pred_flag_l0[1]), 
    .cand1_mv_field_pred_flag_l1(prev_mv_field_pred_flag_l1[1]), 
    .cand2_mv_field_pred_flag_l0(prev_mv_field_pred_flag_l0[2]), 
    .cand2_mv_field_pred_flag_l1(prev_mv_field_pred_flag_l1[2]), 

    .q_mv_field_pred_flagl0      (cand_mv_field_pred_flag_l0)  ,         
    .q_mv_field_pred_flagl1      (cand_mv_field_pred_flag_l1)  ,         
    .q_mv_field_mv_x_l0          (cand_mv_field_mv_x_l0     )  ,         
    .q_mv_field_mv_y_l0          (cand_mv_field_mv_y_l0     )  ,         
    .q_mv_field_mv_x_l1          (cand_mv_field_mv_x_l1     )  ,         
    .q_mv_field_mv_y_l1          (cand_mv_field_mv_y_l1     )  ,         
    .q_dpb_idx_l0                (cand_dpb_idx_l0           )  , 
    .q_dpb_idx_l1                (cand_dpb_idx_l1           )  , 
    .cand_mv_cache_valid        (cand_mv_cache_valid), 

    // .is_cand_mv_in_range0       (pu_0_in_range), 
    .is_cand_mv_in_range1       (pu_1_in_range), 
    .is_cand_mv_in_range2       (pu_2_in_range), 
    .is_cand_mv_in_range3       (pu_3_in_range), 
    .xx_pb1_strt_min_tus_in     (xx_pb1_strt_in_min_tus),
    .yy_pb1_strt_min_tus_in     (yy_pb1_strt_in_min_tus), 
    .xx_pb2_strt_min_tus_in     (xx_pb2_strt_in_min_tus),
    .yy_pb2_strt_min_tus_in     (yy_pb2_strt_in_min_tus), 
    .xx_pb3_strt_min_tus_in     (xx_pb3_strt_in_min_tus),
    .yy_pb3_strt_min_tus_in     (yy_pb3_strt_in_min_tus), 
    .xT_in_min_tus_in           (xT_in_min_tus), 
    .yT_in_min_tus_in           (yT_in_min_tus), 
    .res_present_in             (y_res_present), 

    .top_row_mvs_in           (top_mvs_to_bs_in),                   
    .left_row_mvs_in          (left_mvs_to_bs_in),   
    .bs_left_wr_en_in           (bs_left_valid_in),       
    .bs_top_wr_en_in            (bs_top_valid_in),   

    .cb_yy_min_tus_in   (now_cb_yy_in_min_tus)    ,
    .cb_xx_min_tus_in   (now_cb_xx_in_min_tus)    ,

    .res_present_pu_range_4by4_valid    (res_present_pu_range_4by4_valid), 
    .bs_fifo_data_out                   (bs_fifo_data_out), 
    .bs_fifo_wr_en_out                  (bs_fifo_wr_en_out), 
    .bs_fifo_full_in                    (bs_fifo_full_in)
    );



wire       	       luma_filter_ready2; 
wire       	       luma_filter_idle2; 
wire        [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 * FILTER_PIXEL_WIDTH -1:0]         luma_filter_out2; 
	`ifdef INTER_PL_TEST    
	new_interpl_filter_verfiy block1(
    .clk				(clk),
    .xT_4x4_in			(xT_in_min_luma),
    .yT_4x4_in			(yT_in_min_luma),
    .luma_filter_ready2	(luma_filter_ready2),
    .luma_filter_out2 	(luma_filter_out2) ,
    .luma_filter_ready	(luma_filter_ready) ,
    .luma_filter_out	(luma_filter_out)
);
`endif
	
`ifdef CACHE_PIPE
inter_cache_pipe_mod cache_block
`elsif CACHE_PIPE_PIPE
inter_cache_pipe cache_block
`elsif CACHE_PIPE_PIPE_HIT_FIFO
inter_cache_pipe_hit_pipe cache_block
`else
inter_cache cache_block
`endif
(
    .clk(clk),
    .reset(reset),
	.test_cache_addr 			(test_cache_addr 		),
	.test_cache_luma_data       (test_cache_luma_data   ),
`ifdef CACHE_PIPE

`elsif CACHE_PIPE_PIPE
	.test_cache_en				(test_cache_en),
	.miss_elem_fifo_empty_out	(miss_elem_fifo_empty_out),
`elsif CACHE_PIPE_PIPE_HIT_FIFO
	.test_cache_en				(test_cache_en),
	.miss_elem_fifo_empty_out	(miss_elem_fifo_empty_out),
	.test_ref_block_en			(test_ref_block_en),
	.test_ref_luma_data_4x4			(test_ref_luma_data_4x4),
	.test_yT_in_min_luma_cache	(test_yT_in_min_luma_cache),
	.test_xT_in_min_luma_cache	(test_xT_in_min_luma_cache),
`else
`endif
    .luma_ref_start_x_in         (luma_ref_start_x)  ,
    .luma_ref_start_y_in         (luma_ref_start_y)  ,
    .chma_ref_start_x_in         (chma_ref_start_x)  ,
    .chma_ref_start_y_in         (chma_ref_start_y)  ,
        
    .chma_ref_width_x_in         (chma_ref_width_x)  ,
    .luma_ref_height_y_in        (luma_ref_height_y)  ,
    .chma_ref_height_y_in        (chma_ref_height_y)  ,
    .luma_ref_width_x_in         (luma_ref_width_x)  ,

    .luma_ref_start_x_out      (luma_ref_start_x_out    )              ,
    .luma_ref_start_y_out      (luma_ref_start_y_out    )              ,
    .chma_ref_start_x_out      (chma_ref_start_x_out    )              ,
    .chma_ref_start_y_out      (chma_ref_start_y_out    )              ,
    .luma_ref_width_x_out      (luma_ref_width_x_out    )             ,
    .chma_ref_width_x_out      (chma_ref_width_x_out    )             ,
    .luma_ref_height_y_out     (luma_ref_height_y_out   )             ,
    .chma_ref_height_y_out     (chma_ref_height_y_out   )             ,

    .block_x_offset_luma        (    block_x_offset_luma ) ,
    .block_y_offset_luma        (    block_y_offset_luma ) ,
    .block_x_offset_chma        (    block_x_offset_chma ) ,
    .block_y_offset_chma        (    block_y_offset_chma ) ,
    .block_x_end_luma           (    block_x_end_luma    ) ,
    .block_y_end_luma           (    block_y_end_luma    ) ,
    .block_x_end_chma           (    block_x_end_chma    ) ,
    .block_y_end_chma           (    block_y_end_chma    ) ,

    .res_present_in         (y_res_present),
    .res_present_out        (y_res_present_reg2),
    .bi_pred_block_cache_in (bi_pred_block_cache_in),
    .bi_pred_block_cache_out(bi_pred_block_filter_in),
    .x0_tu_end_in_min_tus_in (x0_tu_end_in_min_tus),
    .x0_tu_end_in_min_tus_out(x0_tu_end_in_min_tus_cache_out),
    .y0_tu_end_in_min_tus_in (y0_tu_end_in_min_tus),
    .y0_tu_end_in_min_tus_out(y0_tu_end_in_min_tus_cache_out),
    .cache_idle_out           (cache_idle_out)  ,
    
    .pic_width                ({3'd0,pic_width})  ,
    .pic_height               ({3'd0,pic_height})  ,
            
    .ch_frac_x                (ch_frac_x)  ,
    .ch_frac_y                (ch_frac_y)  ,
    .ch_frac_x_out            (ch_frac_x_cache_out)  ,
    .ch_frac_y_out            (ch_frac_y_cache_out),           
    .xT_in_min_tus_in         (xT_in_min_tus)  ,
    .yT_in_min_tus_in         (yT_in_min_tus)  ,
    .xT_in_min_tus_out        (xT_in_min_tus_out)  ,
    .yT_in_min_tus_out        (yT_in_min_tus_out)  ,

	`ifndef INTER_PL_TEST
    // .filer_idle_in            (luma_filter_idle & cb_filter_idle & cr_filter_idle & !tu_res_pres_cb_empty & !tu_res_pres_cr_empty )  ,
    .filer_idle_in            (luma_filter_idle & cb_filter_idle & cr_filter_idle )  ,
	`else
    .filer_idle_in            (luma_filter_idle2 & luma_filter_idle & cb_filter_idle & cr_filter_idle & !tu_res_pres_cb_empty & !tu_res_pres_cr_empty )  ,
	`endif
    .ref_idx_in_in            (ref_dpb_idx)  ,
    .valid_in                 (ref_range_valid_out)  ,
    .block_121_out            (block_121_out )  ,
    .block_25cb_out           (block_25cb_out)  ,
    .block_25cr_out           (block_25cr_out)  ,
    .block_ready              (cache_block_ready)  ,
            
    .ref_pix_axi_ar_addr      (ref_pix_axi_ar_addr   )  ,
    .ref_pix_axi_ar_len       (ref_pix_axi_ar_len    )  ,
    .ref_pix_axi_ar_size      (ref_pix_axi_ar_size   )  ,
    .ref_pix_axi_ar_burst     (ref_pix_axi_ar_burst  )  ,
    .ref_pix_axi_ar_prot      (ref_pix_axi_ar_prot   )  ,   
    .ref_pix_axi_ar_valid     (ref_pix_axi_ar_valid  )  ,
    .ref_pix_axi_ar_ready     (ref_pix_axi_ar_ready  )  ,
    .ref_pix_axi_r_data       (ref_pix_axi_r_data    )  ,
    .ref_pix_axi_r_resp       (ref_pix_axi_r_resp    )  ,
    .ref_pix_axi_r_last       (ref_pix_axi_r_last    )  ,
    .ref_pix_axi_r_valid      (ref_pix_axi_r_valid   )  ,
    .ref_pix_axi_r_ready      (ref_pix_axi_r_ready   )


);


`ifdef INTER_PL_PIPE
luma_filter_wrapper_modi luma_filter_wrapper_block(
`else 
luma_filter_wrapper luma_filter_wrapper_block(
`endif
    .clk                   (clk)      ,
    .reset                 (reset)      ,
`ifndef DBF_DISCONNECTED
    .res_present_in         (y_res_present_reg2),
    .bi_pred_block_filter_in(bi_pred_block_filter_in),
    .xT_4x4_in             (xT_in_min_tus_out)      ,
    .yT_4x4_in             (yT_in_min_tus_out)      , 
    .x0_tu_end_in_min_tus_in(x0_tu_end_in_min_tus_cache_out),
    .y0_tu_end_in_min_tus_in(y0_tu_end_in_min_tus_cache_out),
`else
    .res_present_in         (y_res_present),
    .bi_pred_block_filter_in(bi_pred_block_cache_in),
    .xT_4x4_in             (xT_in_min_tus)      ,
    .yT_4x4_in             (yT_in_min_tus)      ,
    .x0_tu_end_in_min_tus_in(x0_tu_end_in_min_tus),
    .y0_tu_end_in_min_tus_in(y0_tu_end_in_min_tus),
`endif
    .res_present_out        (y_res_present_reg3),
    .bi_pred_block_filter_out(bi_pred_block_y_filter_out),
    .x0_tu_end_in_min_tus_out(x0_tu_end_in_min_tus_yy_filt_out),
    .y0_tu_end_in_min_tus_out(y0_tu_end_in_min_tus_yy_filt_out),
	
    .ref_l_start_x         (luma_ref_start_x_out)    ,
    .ref_l_start_y         (luma_ref_start_y_out)    ,
    .ref_l_height          (luma_ref_height_y_out)   ,
    .ref_l_width           (luma_ref_width_x_out)    ,
    .block_start_x         (block_x_offset_luma)     ,
    .block_start_y         (block_y_offset_luma)     ,
    .block_end_x           (block_x_end_luma)      ,
    .block_end_y           (block_y_end_luma)      ,   
    .l_frac_x              (ch_frac_x_cache_out[MV_L_FRAC_WIDTH_HIGH-1:0])      ,
    .l_frac_y              (ch_frac_y_cache_out[MV_L_FRAC_WIDTH_HIGH-1:0])      ,
    
    .ref_pix_l_in          (block_121_out)           , 
`ifndef DBF_DISCONNECTED	
    .valid_in              (cache_block_ready)       ,
`else
	.valid_in              (ref_range_valid_out)       ,
`endif
    .pic_width               ({1'd0,pic_width})  ,
    .pic_height              ({1'd0,pic_height})  ,

    .block_ready_out        (luma_filter_ready)   ,
    // .filter_idle_out        (luma_filter_idle)     ,
    .filter_idle_out        (luma_filter_idle)     ,
    .hori_vert_idle_out     (luma_full_filter_idle),
    
    .pred_pix_out_4x4       (luma_filter_out)    ,
    .xT_4x4_out             (xT_in_min_luma)     ,
    .yT_4x4_out             (yT_in_min_luma)
);

`ifdef INTER_PL_TEST    
luma_filter_wrapper luma_filter_wrapper_block_dup(
    .clk                   (clk)      ,
    .reset                 (reset)      ,
`ifndef DBF_DISCONNECTED
    .res_present_in         (y_res_present_reg2),
    .bi_pred_block_filter_in(bi_pred_block_filter_in),
    .xT_4x4_in             (xT_in_min_tus_out)      ,
    .yT_4x4_in             (yT_in_min_tus_out)      , 
    .x0_tu_end_in_min_tus_in(x0_tu_end_in_min_tus_cache_out),
    .y0_tu_end_in_min_tus_in(y0_tu_end_in_min_tus_cache_out),
`else
    .res_present_in         (y_res_present),
    .bi_pred_block_filter_in(bi_pred_block_cache_in),
    .xT_4x4_in             (xT_in_min_tus)      ,
    .yT_4x4_in             (yT_in_min_tus)      ,
    .x0_tu_end_in_min_tus_in(x0_tu_end_in_min_tus),
    .y0_tu_end_in_min_tus_in(y0_tu_end_in_min_tus),
`endif
    // .res_present_out        (y_res_present_reg3),
    // .bi_pred_block_filter_out(bi_pred_block_y_filter_out),
    // .x0_tu_end_in_min_tus_out(x0_tu_end_in_min_tus_yy_filt_out),
    // .y0_tu_end_in_min_tus_out(y0_tu_end_in_min_tus_yy_filt_out),
	
    .ref_l_start_x         (luma_ref_start_x_out)    ,
    .ref_l_start_y         (luma_ref_start_y_out)    ,
    .ref_l_height          (luma_ref_height_y_out)   ,
    .ref_l_width           (luma_ref_width_x_out)    ,
    .block_start_x         (block_x_offset_luma)     ,
    .block_start_y         (block_y_offset_luma)     ,
    .block_end_x           (block_x_end_luma)      ,
    .block_end_y           (block_y_end_luma)      ,   
    .l_frac_x              (ch_frac_x_cache_out[MV_L_FRAC_WIDTH_HIGH-1:0])      ,
    .l_frac_y              (ch_frac_y_cache_out[MV_L_FRAC_WIDTH_HIGH-1:0])      ,
    
    .ref_pix_l_in          (block_121_out)           , 
`ifndef DBF_DISCONNECTED	
    .valid_in              (cache_block_ready)       ,
`else
	.valid_in              (ref_range_valid_out)       ,
`endif
    .pic_width               ({1'd0,pic_width})  ,
    .pic_height              ({1'd0,pic_height})  ,

    .block_ready_out        (luma_filter_ready2)   ,
    .filter_idle_out        (luma_filter_idle2)     ,

    
    .pred_pix_out_4x4       (luma_filter_out2)    
    // .xT_4x4_out             (xT_in_min_luma)     ,
    // .yT_4x4_out             (yT_in_min_luma)
); 
 `endif
   
`ifdef INTER_PL_PIPE   
chma_filter_wrapper_modi cb_filter_wrapper (
`else
chroma_filter_wrapper cb_filter_wrapper (
`endif
    .clk(clk), 
    .reset(reset), 
`ifndef DBF_DISCONNECTED
    .bi_pred_block_filter_in(bi_pred_block_filter_in),
    .xT_4x4_in             (xT_in_min_tus_out)      ,
    .yT_4x4_in             (yT_in_min_tus_out)      , 
    .x0_tu_end_in_min_tus_in(x0_tu_end_in_min_tus_cache_out),
    .y0_tu_end_in_min_tus_in(y0_tu_end_in_min_tus_cache_out),
`else
    .bi_pred_block_filter_in(bi_pred_block_cache_in),
    .xT_4x4_in             (xT_in_min_tus)      ,
    .yT_4x4_in             (yT_in_min_tus)      , 
    .x0_tu_end_in_min_tus_in(x0_tu_end_in_min_tus),
    .y0_tu_end_in_min_tus_in(y0_tu_end_in_min_tus),
`endif
    .bi_pred_block_filter_out(bi_pred_block_cb_filter_out),
    .x0_tu_end_in_min_tus_out(x0_tu_end_in_min_tus_cb_filt_out),
    .y0_tu_end_in_min_tus_out(y0_tu_end_in_min_tus_cb_filt_out), 
	
    .ref_c_start_x      (chma_ref_start_x_out), 
    .ref_c_start_y      (chma_ref_start_y_out), 
    .ref_c_height       (chma_ref_height_y_out), 
    .ref_c_width        (chma_ref_width_x_out), 
    .block_start_x_ch   (block_x_offset_chma), 
    .block_start_y_ch   (block_y_offset_chma), 
    .block_end_x_ch     (block_x_end_chma), 
    .block_end_y_ch     (block_y_end_chma), 
    
    .c_frac_x           (ch_frac_x_cache_out), 
    .c_frac_y           (ch_frac_y_cache_out), 
    .ref_pix_l_in       (block_25cb_out), 
`ifndef DBF_DISCONNECTED	
    .valid_in              (cache_block_ready)       ,
`else
	.valid_in              (ref_range_valid_out)       ,
`endif
    .pic_width           ({1'd0,pic_width})  ,
    .pic_height          ({1'd0,pic_height})  , 
    .block_ready_out    (cb_filter_ready), 
    .filter_idle_out    (cb_filter_idle), 
    .hori_vert_idle_out (cb_full_filter_idle),
    .pred_pix_out_4x4   (cb_filter_out), 
    .xT_4x4_out         (xT_in_min_cb), 
    .yT_4x4_out         (yT_in_min_cb)
    );

`ifdef INTER_PL_TEST  
chroma_filter_wrapper cb_filter_wrapper_dup (
// chma_filter_wrapper_modi cb_filter_wrapper_dup (
    .clk(clk), 
    .reset(reset), 
`ifndef DBF_DISCONNECTED
    .bi_pred_block_filter_in(bi_pred_block_filter_in),
    .xT_4x4_in             (xT_in_min_tus_out)      ,
    .yT_4x4_in             (yT_in_min_tus_out)      , 
    .x0_tu_end_in_min_tus_in(x0_tu_end_in_min_tus_cache_out),
    .y0_tu_end_in_min_tus_in(y0_tu_end_in_min_tus_cache_out),
`else
    .bi_pred_block_filter_in(bi_pred_block_cache_in),
    .xT_4x4_in             (xT_in_min_tus)      ,
    .yT_4x4_in             (yT_in_min_tus)      , 
    .x0_tu_end_in_min_tus_in(x0_tu_end_in_min_tus),
    .y0_tu_end_in_min_tus_in(y0_tu_end_in_min_tus),
`endif
    .bi_pred_block_filter_out(),
    .x0_tu_end_in_min_tus_out(),
    .y0_tu_end_in_min_tus_out(), 
	
    .ref_c_start_x      (chma_ref_start_x_out), 
    .ref_c_start_y      (chma_ref_start_y_out), 
    .ref_c_height       (chma_ref_height_y_out), 
    .ref_c_width        (chma_ref_width_x_out), 
    .block_start_x_ch   (block_x_offset_chma), 
    .block_start_y_ch   (block_y_offset_chma), 
    .block_end_x_ch     (block_x_end_chma), 
    .block_end_y_ch     (block_y_end_chma), 
    
    .c_frac_x           (ch_frac_x_cache_out), 
    .c_frac_y           (ch_frac_y_cache_out), 
    .ref_pix_l_in       (block_25cb_out), 
`ifndef DBF_DISCONNECTED	
    .valid_in              (cache_block_ready)       ,
`else
	.valid_in              (ref_range_valid_out)       ,
`endif
    .pic_width           ({1'd0,pic_width})  ,
    .pic_height          ({1'd0,pic_height})  , 
    .block_ready_out    (), 
    .filter_idle_out    (), 
    .pred_pix_out_4x4   (), 
    .xT_4x4_out         (), 
    .yT_4x4_out         ()
    );
`endif
	
`ifdef INTER_PL_PIPE   
chma_filter_wrapper_modi cr_filter_wrapper (
`else
chroma_filter_wrapper cr_filter_wrapper (
`endif
    .clk(clk), 
    .reset(reset), 
`ifndef DBF_DISCONNECTED
    .bi_pred_block_filter_in(bi_pred_block_filter_in),
    .xT_4x4_in             (xT_in_min_tus_out)      ,
    .yT_4x4_in             (yT_in_min_tus_out)      , 
    .x0_tu_end_in_min_tus_in(x0_tu_end_in_min_tus_cache_out),
    .y0_tu_end_in_min_tus_in(y0_tu_end_in_min_tus_cache_out),
`else
    .bi_pred_block_filter_in(bi_pred_block_cache_in),
    .xT_4x4_in             (xT_in_min_tus)      ,
    .yT_4x4_in             (yT_in_min_tus)      , 
    .x0_tu_end_in_min_tus_in(x0_tu_end_in_min_tus),
    .y0_tu_end_in_min_tus_in(y0_tu_end_in_min_tus),
`endif 
    .bi_pred_block_filter_out(bi_pred_block_cr_filter_out),
    .x0_tu_end_in_min_tus_out(x0_tu_end_in_min_tus_cr_filt_out),
    .y0_tu_end_in_min_tus_out(y0_tu_end_in_min_tus_cr_filt_out),

	
    .ref_c_start_x      (chma_ref_start_x_out), 
    .ref_c_start_y      (chma_ref_start_y_out), 
    .ref_c_height       (chma_ref_height_y_out), 
    .ref_c_width        (chma_ref_width_x_out), 
    .block_start_x_ch   (block_x_offset_chma), 
    .block_start_y_ch   (block_y_offset_chma), 
    .block_end_x_ch     (block_x_end_chma), 
    .block_end_y_ch     (block_y_end_chma), 

    .c_frac_x           (ch_frac_x_cache_out), 
    .c_frac_y           (ch_frac_y_cache_out), 
    .ref_pix_l_in       (block_25cr_out), 
`ifndef DBF_DISCONNECTED	
    .valid_in              (cache_block_ready)       ,
`else
	.valid_in              (ref_range_valid_out)       ,
`endif
    .pic_width           ({1'd0,pic_width})  ,
    .pic_height          ({1'd0,pic_height})  ,
    .block_ready_out    (cr_filter_ready), 
    .filter_idle_out    (cr_filter_idle), 
    .hori_vert_idle_out (cr_full_filter_idle),
    .pred_pix_out_4x4   (cr_filter_out), 
    .xT_4x4_out         (xT_in_min_cr), 
    .yT_4x4_out         (yT_in_min_cr)
    );


weighted_sample_prediction weight_pred_luma_block (
    .clk(clk), 
    .reset(reset), 
    .bi_pred_block_in(bi_pred_block_y_filter_out),
    .x0_tu_end_in_min_tus_in (x0_tu_end_in_min_tus_yy_filt_out),
    .x0_tu_end_in_min_tus_out(x0_tu_end_in_min_tus_yy_wgt_out),
    .y0_tu_end_in_min_tus_in (y0_tu_end_in_min_tus_yy_filt_out),
    .y0_tu_end_in_min_tus_out(y0_tu_end_in_min_tus_yy_wgt_out),

    .res_present_in(y_res_present_reg3),
    .res_present_out(y_res_present_out),
    .xT_in_min_tus(xT_in_min_luma), 
    .yT_in_min_tus(yT_in_min_luma), 
    .xT_out_min_tus(xT_in_min_luma_out), 
    .yT_out_min_tus(yT_in_min_luma_out), 
    .valid_in(luma_filter_ready), 
    .pred_pix_in_4x4(luma_filter_out), 
    .pred_pix_out_4x4(luma_wgt_pred_out), 
    .valid_out(luma_wght_pred_valid)
    );

chroma_weight_pred weight_pred_cb_block (
    .clk(clk), 
    .reset(reset), 
    .bi_pred_block_in(bi_pred_block_cb_filter_out),
    .x0_tu_end_in_min_tus_in (x0_tu_end_in_min_tus_cb_filt_out),
    .x0_tu_end_in_min_tus_out(x0_tu_end_in_min_tus_cb_wgt_out),
    .y0_tu_end_in_min_tus_in (y0_tu_end_in_min_tus_cb_filt_out),
    .y0_tu_end_in_min_tus_out(y0_tu_end_in_min_tus_cb_wgt_out),

    .xT_in_min_tus(xT_in_min_cb), 
    .yT_in_min_tus(yT_in_min_cb), 
    .xT_out_min_tus(xT_in_min_cb_out), 
    .yT_out_min_tus(yT_in_min_cb_out), 
    .valid_in(cb_filter_ready), 
    .pred_pix_in_4x4(cb_filter_out), 
    .pred_pix_out_4x4(cb_wgt_pred_out), 
    .valid_out(cb_wght_pred_valid)
    );

chroma_weight_pred weight_pred_cr_block (
    .clk(clk), 
    .reset(reset), 
    .bi_pred_block_in(bi_pred_block_cr_filter_out),
    .x0_tu_end_in_min_tus_in (x0_tu_end_in_min_tus_cr_filt_out),
    .x0_tu_end_in_min_tus_out(x0_tu_end_in_min_tus_cr_wgt_out),
    .y0_tu_end_in_min_tus_in (y0_tu_end_in_min_tus_cr_filt_out),
    .y0_tu_end_in_min_tus_out(y0_tu_end_in_min_tus_cr_wgt_out),

    .xT_in_min_tus(xT_in_min_cr), 
    .yT_in_min_tus(yT_in_min_cr), 
    .xT_out_min_tus(xT_in_min_cr_out), 
    .yT_out_min_tus(yT_in_min_cr_out), 
    .valid_in(cr_filter_ready), 
    .pred_pix_in_4x4(cr_filter_out), 
    .pred_pix_out_4x4(cr_wgt_pred_out), 
    .valid_out(cr_wght_pred_valid)
    );

   
 

pb_xyhw_gen_modified pb_xyhw_gen_block0 (
    .clk(clk), 
    // .reset(reset), 
    .cb_part_mode(now_cb_part_mode), 
    .pb_part_idx(3'd1), 
    .cb_size(now_cb_size_in_min_tus), 
    .cb_xx(now_cb_xx_in_min_tus), 
    .cb_yy(now_cb_yy_in_min_tus), 
    .xx_pb_start(xx_pb0_strt_in_min_tus), 
    .yy_pb_start(yy_pb0_strt_in_min_tus), 
    .xx_pb_end  (xx_pb0_end__in_min_tus), 
    .yy_pb_end  (yy_pb0_end__in_min_tus)
    );

pb_xyhw_gen_modified pb_xyhw_gen_block1 (
    .clk(clk), 
    // .reset(reset), 
    .cb_part_mode(now_cb_part_mode), 
    .pb_part_idx(3'd2), 
    .cb_size(now_cb_size_in_min_tus), 
    .cb_xx(now_cb_xx_in_min_tus), 
    .cb_yy(now_cb_yy_in_min_tus), 
    .xx_pb_start(xx_pb1_strt_in_min_tus), 
    .yy_pb_start(yy_pb1_strt_in_min_tus), 
    .xx_pb_end  (xx_pb1_end__in_min_tus), 
    .yy_pb_end  (yy_pb1_end__in_min_tus)
    );
    
pb_xyhw_gen_modified pb_xyhw_gen_block2 (
    .clk(clk), 
    // .reset(reset), 
    .cb_part_mode(now_cb_part_mode), 
    .pb_part_idx(3'd3), 
    .cb_size(now_cb_size_in_min_tus), 
    .cb_xx(now_cb_xx_in_min_tus), 
    .cb_yy(now_cb_yy_in_min_tus), 
    .xx_pb_start(xx_pb2_strt_in_min_tus), 
    .yy_pb_start(yy_pb2_strt_in_min_tus), 
    .xx_pb_end  (xx_pb2_end__in_min_tus), 
    .yy_pb_end  (yy_pb2_end__in_min_tus)
    );
    
pb_xyhw_gen_modified pb_xyhw_gen_block3 (
    .clk(clk), 
    // .reset(reset), 
    .cb_part_mode(now_cb_part_mode), 
    .pb_part_idx(3'd4), 
    .cb_size(now_cb_size_in_min_tus), 
    .cb_xx(now_cb_xx_in_min_tus), 
    .cb_yy(now_cb_yy_in_min_tus), 
    .xx_pb_start(xx_pb3_strt_in_min_tus), 
    .yy_pb_start(yy_pb3_strt_in_min_tus), 
    .xx_pb_end  (xx_pb3_end__in_min_tus), 
    .yy_pb_end  (yy_pb3_end__in_min_tus)
    );
	
wire mv_x_l0_l1_same = (cand_mv_field_mv_x_l0 == cand_mv_field_mv_x_l1) ? 1'b1 : 1'b0;
wire mv_y_l0_l1_same = (cand_mv_field_mv_y_l0 == cand_mv_field_mv_y_l1) ? 1'b1 : 1'b0;
wire mv_dpb_idx_l0_l1_same = (cand_dpb_idx_l0 == cand_dpb_idx_l1) ? 1'b1 : 1'b0;
   
wire first_cu_mv_field_valid_in_cu = ( (next_mv_field_valid_in == 1) && (pb_part_idx == 0) )|| (   end_of_cu && ( (inter_pred_sample_state == STATE_INTER_PRED_MV_INT_DECODE) || (inter_pred_sample_state == STATE_INTRA_PRED_SAMPLE_X_4_Y_4_UPDATE1) ) );


always@(posedge clk) begin
    if(next_mv_field_valid_in) begin
        prev_mv_field_pred_flag_l0[pb_part_idx]     <=      next_mv_field_pred_flag_l0_in;
        prev_mv_field_pred_flag_l1[pb_part_idx]     <=      next_mv_field_pred_flag_l1_in;
        // if(mv_x_l0_l1_same & mv_y_l0_l1_same & mv_dpb_idx_l0_l1_same) begin
        //     prev_mv_field_pred_flag_l1[pb_part_idx]     <= 1'b0;
        // end
        // else begin
            
        // end
        // prev_mv_field_ref_idx_l0[pb_part_idx]       <=    next_mv_field_ref_idx_l0_in;
        // prev_mv_field_ref_idx_l1[pb_part_idx]       <=    next_mv_field_ref_idx_l1_in;
        prev_mv_field_mv_x_l0[pb_part_idx]          <=    next_mv_field_mv_x_l0_in;
        prev_mv_field_mv_y_l0[pb_part_idx]          <=    next_mv_field_mv_y_l0_in;
        prev_mv_field_mv_x_l1[pb_part_idx]          <=    next_mv_field_mv_x_l1_in;
        prev_mv_field_mv_y_l1[pb_part_idx]          <=    next_mv_field_mv_y_l1_in;    
        prev_dpb_idx_l0[pb_part_idx]                <=    next_dpb_idx_l0_in;    
        prev_dpb_idx_l1[pb_part_idx]                <=    next_dpb_idx_l1_in;    
        
        // prev_xx_pb[pb_part_idx]                     <=    next_xx_pb_in;
        // prev_hh_pb[pb_part_idx]                     <=    next_hh_pb_in;
        // prev_yy_pb[pb_part_idx]                     <=    next_yy_pb_in;
        // prev_ww_pb[pb_part_idx]                     <=    next_ww_pb_in;  
    end

    if(reset) begin
        
    end
    else begin
        if(first_cu_mv_field_valid_in_cu) begin //&& inter_pred_sample_state == STATE_INTER_PRED_SAMPLE_CONFIG_UPDATE) begin
            now_cb_xx_in_min_tus            <= next_cb_xx_in_min_tus;
            now_cb_yy_in_min_tus            <= next_cb_yy_in_min_tus;
            now_cb_pred_mode                <= next_cb_pred_mode;
            now_cb_part_mode                <= next_cb_part_mode;
            // now_pic_dpb_idx                 <= next_pic_dpb_idx;
            now_ctb_xx_in_min_tus           <= next_ctb_xx_in_min_tus;
            now_ctb_yy_in_min_tus           <= next_ctb_yy_in_min_tus;
            now_cb_xx_end_in_min_tus        <= next_cb_xx_in_min_tus + next_cb_size_in_min_tus;
            now_cb_yy_end_in_min_tus        <= next_cb_yy_in_min_tus + next_cb_size_in_min_tus;
            now_cb_size_in_min_tus          <= next_cb_size_in_min_tus;    
        end
    end
end


assign intra_tu_4by4_valid_to_bs = (inter_pred_sample_state == STATE_INTRA_PRED_SAMPLE_X_4_Y_4_UPDATE1);
always@(posedge clk) begin
    intra_tu_4by4_valid_to_bs_d <= intra_tu_4by4_valid_to_bs;
end

always@(*) begin
    case(pb_part_idx)
        1: begin
            pb_part_idx_decode = 4'd1;
        end
        2: pb_part_idx_decode = 4'b0011;
        3: pb_part_idx_decode = 4'b0111;
        4: pb_part_idx_decode = 4'b1111;
        default: begin
            pb_part_idx_decode = 4'd0;
        end
    endcase
end


always@(posedge clk) begin    : part_idx_logic
    if(reset) begin
        pb_part_idx <= 0;
    end
    else begin
        if(((inter_pred_sample_state == STATE_INTER_PRED_MV_INT_DECODE) || (inter_pred_sample_state == STATE_INTRA_PRED_SAMPLE_X_4_Y_4_UPDATE1)) && (end_of_cu == 1))begin
            pb_part_idx <= 0;
        end
        else if(next_mv_field_valid_in) begin
            pb_part_idx <= pb_part_idx + 1'b1;
        end
    end
end



always@(posedge clk) begin
	if(reset) begin
		bs_fill_range_idx <= 0;
		bs_fill_started_range <= 0; 
		bs_fill_range_case <= STATE_IDLE;
		bs_fill_started_range_d <= 0;
	end
	else begin
	  bs_fill_started_range_d <= bs_fill_started_range;
		case(bs_fill_range_case)
			STATE_IDLE:begin
				if(next_mv_field_valid_in) begin
					bs_fill_range_case <= STATE_ACTIVE;
				end
				if(((inter_pred_sample_state == STATE_INTER_PRED_MV_INT_DECODE) || (inter_pred_sample_state == STATE_INTRA_PRED_SAMPLE_X_4_Y_4_UPDATE1)) && (end_of_cu == 1))begin
					bs_fill_started_range <= 0;
					bs_fill_range_idx <= 0;
				end
			end
			STATE_ACTIVE: begin
				if(bs_top_valid_in | bs_left_valid_in) begin
					bs_fill_range_case <= STATE_IDLE;
					bs_fill_range_idx <= bs_fill_range_idx + 1'b1;
					bs_fill_started_range[bs_fill_range_idx] <=  1'b1;
				end
				if(((inter_pred_sample_state == STATE_INTER_PRED_MV_INT_DECODE) || (inter_pred_sample_state == STATE_INTRA_PRED_SAMPLE_X_4_Y_4_UPDATE1)) && (end_of_cu == 1))begin
					$display("%d cu block end should not come before top left mv send",$time);
					$stop;
				end
			end
		endcase
	end
end

always@(posedge clk) begin
	if(reset) begin
		first_mv_of_cu_ready <= 0;
	end
	case(first_mv_of_cu_ready)
		0: begin
			if(( (next_mv_field_valid_in == 1) && (pb_part_idx == 0) )) begin
				first_mv_of_cu_ready <= 1;
			end
		end
		1: begin
			if(((inter_pred_sample_state == STATE_INTER_PRED_MV_INT_DECODE) || (inter_pred_sample_state == STATE_INTRA_PRED_SAMPLE_X_4_Y_4_UPDATE1)) && (end_of_cu == 1))begin
				first_mv_of_cu_ready <= 0;
			end
		end
	endcase
end

always@(posedge clk) begin : mv_derive_idle_logic
    if(reset)begin
        pred_pixl_gen_idle_out_to_mv_derive <= 1;
    end

    else begin
        if(end_of_cu && ((inter_pred_sample_state == STATE_INTER_PRED_MV_INT_DECODE)|| (inter_pred_sample_state == STATE_INTRA_PRED_SAMPLE_X_4_Y_4_UPDATE1) )) begin
            pred_pixl_gen_idle_out_to_mv_derive <= 1;
        end
        else begin
            case(now_cb_part_mode)
                `PART_2Nx2N: begin
                    if(pb_part_idx == 1) begin
                        pred_pixl_gen_idle_out_to_mv_derive <= 0;
                    end
                    else begin
                        pred_pixl_gen_idle_out_to_mv_derive <= 1;
                    end
                end
                `PART_2NxN,`PART_Nx2N,`PART_nLx2N,`PART_nRx2N,`PART_2NxnD,`PART_2NxnU: begin
                    if(pb_part_idx == 2) begin
                        pred_pixl_gen_idle_out_to_mv_derive <= 0;
                    end
                    else begin
                        pred_pixl_gen_idle_out_to_mv_derive <= 1;
                    end
                end
                `PART_NxN: begin
                    if(pb_part_idx == 4) begin
                        pred_pixl_gen_idle_out_to_mv_derive <= 0;
                    end
                    else begin
                        pred_pixl_gen_idle_out_to_mv_derive <= 1;
                    end
                end
                default: begin
                    pred_pixl_gen_idle_out_to_mv_derive <= 1;
                end
            endcase
        end
    end
end

    
always@(*) begin    : next_state_logic
    inter_pred_sample_next_state = inter_pred_sample_state;
    pred_pixl_gen_idle_out_to_pred_top = 0;
    res_present_pu_range_4by4_valid = 0;
    ref_range_valid_out = 0;
    bi_pred_block_cache_in = 0;
    cand_mv_cache_valid = 0;
    case(inter_pred_sample_state)
        STATE_INTER_PRED_SAMPLE_FILL_TU_DATA: begin
            if(!tu_empty & first_mv_of_cu_ready & (~y_dbf_fifo_is_full_hold) & (~cb_dbf_fifo_is_full_hold) & (~cr_dbf_fifo_is_full_hold) ) begin
                inter_pred_sample_next_state = STATE_INTER_PRED_SAMPLE_X_8_Y_8_UPDATE;
            end
        end
        STATE_INTER_PRED_SAMPLE_X_8_Y_8_UPDATE: begin
            
            pred_pixl_gen_idle_out_to_pred_top = 1;
            if(now_cb_pred_mode == `MODE_INTRA) begin
                inter_pred_sample_next_state = STATE_INTRA_PRED_SAMPLE_X_4_Y_4_UPDATE1;
            end
            else begin
				// if(next_mv_field_valid_in) begin
					//inter_pred_sample_next_state = STATE_INTER_PRED_SAMPLE_MV_VALID_WAIT;
				// end
				// else begin
					 inter_pred_sample_next_state = STATE_INTER_PRED_SAMPLE_PU_SELECT;
				// end
            end
        end
        STATE_INTER_PRED_SAMPLE_PU_SELECT: begin
            if((pu_0_in_range && pb_part_idx_decode[0] && bs_fill_started_range_d[0]) || (pu_1_in_range && pb_part_idx_decode[1] && bs_fill_started_range_d[1]) || (pu_2_in_range && pb_part_idx_decode[2] && bs_fill_started_range_d[2])  || (pu_3_in_range && pb_part_idx_decode[3] && bs_fill_started_range_d[3]) ) begin
                inter_pred_sample_next_state = STATE_INTER_PRED_MV_INT_DECODE;
                res_present_pu_range_4by4_valid = 1;
            end
			// else begin
				// inter_pred_sample_next_state = STATE_INTER_PRED_SAMPLE_MV_VALID_WAIT;
			// end
        end
		// STATE_INTER_PRED_PU_SELECT_WAIT: begin
			// if((pu_0_in_range && pb_part_idx_decode[0]) || (pu_1_in_range && pb_part_idx_decode[1]) || (pu_2_in_range && pb_part_idx_decode[2])  || (pu_3_in_range && pb_part_idx_decode[3]) ) begin
                // if(bs_top_valid_in | bs_left_valid_in) begin
					// inter_pred_sample_next_state = STATE_INTER_PRED_SAMPLE_PU_SELECT;
				// end
            // end
		// end
        STATE_INTER_PRED_MV_INT_DECODE : begin
            inter_pred_sample_next_state = STATE_INTER_PRED_MV_FRAC_DECODE;
            cand_mv_cache_valid = 1;
        end
        STATE_INTER_PRED_MV_FRAC_DECODE: begin
            if(cand_mv_field_pred_flag_l0 & cand_mv_field_pred_flag_l1) begin
                if(mv_x_l0_l1_same & mv_y_l0_l1_same & mv_dpb_idx_l0_l1_same) begin
                    inter_pred_sample_next_state = STATE_INTER_PRED_MV_DECODE_READY;
                end
                else begin
                    inter_pred_sample_next_state = STATE_INTER_PRED_MV_DECODE_READY_BI;    
                end
                
            end
            else begin
                inter_pred_sample_next_state = STATE_INTER_PRED_MV_DECODE_READY;
            end
        end
        STATE_INTER_PRED_MV_DECODE_READY_BI: begin
`ifndef DBF_DISCONNECTED
            // if(cache_idle_in) begin
            if(cache_idle_in & !tu_res_pres_cb_empty & !tu_res_pres_cr_empty) begin
`else
            if(luma_filter_idle & cb_filter_idle & cr_filter_idle & !tu_res_pres_cb_empty & !tu_res_pres_cr_empty ) begin
`endif
                ref_range_valid_out = 1;
                bi_pred_block_cache_in = 1;
                inter_pred_sample_next_state = STATE_INTER_PRED_MV_FRAC_BI_DECODE;
            end
        end
        STATE_INTER_PRED_MV_FRAC_BI_DECODE: begin
            inter_pred_sample_next_state = STATE_INTER_PRED_MV_DECODE_READY;
        end
        STATE_INTER_PRED_MV_DECODE_READY: begin
`ifndef DBF_DISCONNECTED
            // if(cache_idle_in) begin
            if(cache_idle_in & !tu_res_pres_cb_empty & !tu_res_pres_cr_empty) begin
`else
            if(luma_filter_idle & cb_filter_idle & cr_filter_idle & !tu_res_pres_cb_empty & !tu_res_pres_cr_empty ) begin
`endif
                ref_range_valid_out = 1;
                if(end_of_cu) begin
                    inter_pred_sample_next_state = STATE_INTER_PRED_SAMPLE_FILL_TU_DATA;    
                end
                else if(end_of_tu) begin
                    inter_pred_sample_next_state = STATE_INTER_PRED_SAMPLE_FILL_TU_DATA;
                end
                else begin
					// if(next_mv_field_valid_in) begin
						// inter_pred_sample_next_state = STATE_INTER_PRED_SAMPLE_MV_VALID_WAIT;
					// end
					// else begin
						inter_pred_sample_next_state = STATE_INTER_PRED_SAMPLE_PU_SELECT;
					// end
                end
            end
            
        end
		// STATE_INTER_PRED_SAMPLE_MV_VALID_WAIT: begin
			// if(bs_top_valid_in | bs_left_valid_in) begin
				// inter_pred_sample_next_state = STATE_INTER_PRED_SAMPLE_PU_SELECT;
			// end
		// end
        STATE_INTRA_PRED_SAMPLE_X_4_Y_4_UPDATE1: begin
            if(end_of_tu) begin
                inter_pred_sample_next_state = STATE_INTER_PRED_SAMPLE_FILL_TU_DATA;    
            end
            else begin
                inter_pred_sample_next_state = STATE_INTRA_PRED_SAMPLE_X_4_Y_4_UPDATE2; 
            end
        end
        STATE_INTRA_PRED_SAMPLE_X_4_Y_4_UPDATE2: begin
            inter_pred_sample_next_state = STATE_INTRA_PRED_SAMPLE_X_4_Y_4_UPDATE1;
        end
    endcase
end


always @(posedge clk) begin

    if(end_of_tu_cr_rd_en)begin
        tu_res_pres_cr_rd_en <= 1;
        tu_res_pres_cb_rd_en <= 1;
    end
    else begin
        tu_res_pres_cr_rd_en <= 0;
        tu_res_pres_cb_rd_en <= 0;
    end
	
    if(cr_wght_pred_valid)begin
        tu_res_pres_cr_rd_en_stage_2 <= 1;
        tu_res_pres_cb_rd_en_stage_2 <= 1;
    end
    else begin
        tu_res_pres_cr_rd_en_stage_2 <= 0;
        tu_res_pres_cb_rd_en_stage_2 <= 0;
    end
end

always@(posedge clk) begin : inter_idle_to_intra_logic
	if(reset) begin
		inter_idle_to_intra <= 1;
	end
	else begin
		case(inter_pred_sample_state)
			STATE_INTER_PRED_MV_DECODE_READY,STATE_INTER_PRED_MV_DECODE_READY_BI: begin
				// if(cache_idle_in) begin
				if(cache_idle_in  & !tu_res_pres_cb_empty & !tu_res_pres_cr_empty) begin
					if(end_of_cu_d) begin
						inter_idle_to_intra <= 1;
					end
					else begin
						inter_idle_to_intra <= 0;
					end
				end
			end
		endcase
	end
end

always@(posedge clk) begin
    if(reset) begin
        xT_in_min_tus <= 0;
        yT_in_min_tus <= 0;
        y_res_present <= 0;
		end_of_cu_d <= 0;
    end
    case(inter_pred_sample_state) 
        STATE_INTER_PRED_SAMPLE_FILL_TU_DATA: begin
            if(!tu_empty) begin
                // c_idx <= c_idx_wire;
                x0_tu_in_min_tus        <= (x0_tu_in_ctu_wire>>(LOG2_MIN_TU_SIZE-1)) + now_ctb_xx_in_min_tus;
                x0_tu_end_in_min_tus    <= (x0_tu_in_ctu_wire>>(LOG2_MIN_TU_SIZE-1)) + now_ctb_xx_in_min_tus + tb_size_wire_in_min_tus;
                y0_tu_in_min_tus        <= (y0_tu_in_ctu_wire>>(LOG2_MIN_TU_SIZE-1)) + now_ctb_yy_in_min_tus;
                y0_tu_end_in_min_tus    <= (y0_tu_in_ctu_wire>>(LOG2_MIN_TU_SIZE-1)) + now_ctb_yy_in_min_tus + tb_size_wire_in_min_tus;
                x_8x8_loc               <= (x0_tu_in_ctu_wire>>(LOG2_MIN_TU_SIZE-1)) + now_ctb_xx_in_min_tus;
                y_8x8_loc               <= (y0_tu_in_ctu_wire>>(LOG2_MIN_TU_SIZE-1)) + now_ctb_yy_in_min_tus;
                inner_x_8x8_loc <= 0;
                inner_y_8x8_loc <= 0;
                y_res_present <= res_present_wire;
            end

        end
        STATE_INTER_PRED_SAMPLE_X_8_Y_8_UPDATE: begin
            xT_in_min_tus <= next_xT_in_min_tus;
            yT_in_min_tus <= next_yT_in_min_tus;
        end
        STATE_INTER_PRED_SAMPLE_PU_SELECT: begin
			end_of_cu_d <= 0;
            if(pu_0_in_range && pb_part_idx_decode[0]) begin
                cand_mv_field_pred_flag_l0 <= prev_mv_field_pred_flag_l0[0] ;
                cand_mv_field_pred_flag_l1 <= prev_mv_field_pred_flag_l1[0] ;
                // cand_mv_field_ref_idx_l0   <= prev_mv_field_ref_idx_l0[0]   ;
                // cand_mv_field_ref_idx_l1   <= prev_mv_field_ref_idx_l1[0]   ;
                cand_mv_field_mv_x_l0      <= prev_mv_field_mv_x_l0[0]      ;
                cand_mv_field_mv_y_l0      <= prev_mv_field_mv_y_l0[0]      ;
                cand_mv_field_mv_x_l1      <= prev_mv_field_mv_x_l1[0]      ;
                cand_mv_field_mv_y_l1      <= prev_mv_field_mv_y_l1[0]      ;
                cand_dpb_idx_l0            <= prev_dpb_idx_l0[0]            ;
                cand_dpb_idx_l1            <= prev_dpb_idx_l1[0]            ;
 
            end
            if(pu_1_in_range && pb_part_idx_decode[1]) begin
                cand_mv_field_pred_flag_l0 <= prev_mv_field_pred_flag_l0[1] ;
                cand_mv_field_pred_flag_l1 <= prev_mv_field_pred_flag_l1[1] ;
                // cand_mv_field_ref_idx_l0   <= prev_mv_field_ref_idx_l0[1]   ;
                // cand_mv_field_ref_idx_l1   <= prev_mv_field_ref_idx_l1[1]   ;
                cand_mv_field_mv_x_l0      <= prev_mv_field_mv_x_l0[1]      ;
                cand_mv_field_mv_y_l0      <= prev_mv_field_mv_y_l0[1]      ;
                cand_mv_field_mv_x_l1      <= prev_mv_field_mv_x_l1[1]      ;
                cand_mv_field_mv_y_l1      <= prev_mv_field_mv_y_l1[1]      ;
                cand_dpb_idx_l0            <= prev_dpb_idx_l0[1]            ;
                cand_dpb_idx_l1            <= prev_dpb_idx_l1[1]            ;
            end
            if(pu_2_in_range && pb_part_idx_decode[2]) begin
                cand_mv_field_pred_flag_l0 <= prev_mv_field_pred_flag_l0[2] ;
                cand_mv_field_pred_flag_l1 <= prev_mv_field_pred_flag_l1[2] ;
                // cand_mv_field_ref_idx_l0   <= prev_mv_field_ref_idx_l0[2]   ;
                // cand_mv_field_ref_idx_l1   <= prev_mv_field_ref_idx_l1[2]   ;
                cand_mv_field_mv_x_l0      <= prev_mv_field_mv_x_l0[2]      ;
                cand_mv_field_mv_y_l0      <= prev_mv_field_mv_y_l0[2]      ;
                cand_mv_field_mv_x_l1      <= prev_mv_field_mv_x_l1[2]      ;
                cand_mv_field_mv_y_l1      <= prev_mv_field_mv_y_l1[2]      ;
                cand_dpb_idx_l0            <= prev_dpb_idx_l0[2]            ;
                cand_dpb_idx_l1            <= prev_dpb_idx_l1[2]            ;
            end
            if(pu_3_in_range && pb_part_idx_decode[3])begin
                cand_mv_field_pred_flag_l0 <= prev_mv_field_pred_flag_l0[3] ;
                cand_mv_field_pred_flag_l1 <= prev_mv_field_pred_flag_l1[3] ;
                // cand_mv_field_ref_idx_l0   <= prev_mv_field_ref_idx_l0[3]   ;
                // cand_mv_field_ref_idx_l1   <= prev_mv_field_ref_idx_l1[3]   ;
                cand_mv_field_mv_x_l0      <= prev_mv_field_mv_x_l0[3]      ;
                cand_mv_field_mv_y_l0      <= prev_mv_field_mv_y_l0[3]      ;
                cand_mv_field_mv_x_l1      <= prev_mv_field_mv_x_l1[3]      ;
                cand_mv_field_mv_y_l1      <= prev_mv_field_mv_y_l1[3]      ;
                cand_dpb_idx_l0            <= prev_dpb_idx_l0[3]            ;
                cand_dpb_idx_l1            <= prev_dpb_idx_l1[3]            ;
            end
        end
        STATE_INTER_PRED_MV_INT_DECODE : begin
			end_of_cu_d <= end_of_cu;
            inner_x_8x8_loc <= next_inner_x_8x8_loc;
            inner_y_8x8_loc <= next_inner_y_8x8_loc;
            x_8x8_loc <= next_x_8x8_loc;
            y_8x8_loc <= next_y_8x8_loc;

            luma_l0_mid_x <= luma_l0_mid_x_wire;
            luma_l0_mid_y <= luma_l0_mid_y_wire;
            chma_l0_mid_x <= chma_l0_mid_x_wire;
            chma_l0_mid_y <= chma_l0_mid_y_wire;

            luma_l1_mid_x <= luma_l1_mid_x_wire;
            luma_l1_mid_y <= luma_l1_mid_y_wire;
            chma_l1_mid_x <= chma_l1_mid_x_wire;
            chma_l1_mid_y <= chma_l1_mid_y_wire;
            
        end
        STATE_INTER_PRED_MV_FRAC_DECODE: begin
            if(cand_mv_field_pred_flag_l0) begin
                luma_ref_start_x <= luma_l0_ref_start_x_wire;
                luma_ref_start_y <= luma_l0_ref_start_y_wire;
                luma_ref_width_x <= luma_l0_ref_width_x_wire;
                luma_ref_height_y <= luma_l0_ref_height_y_wire;
                chma_ref_start_x <= chma_l0_ref_start_x_wire;
                chma_ref_start_y <= chma_l0_ref_start_y_wire;
                chma_ref_width_x <= chma_l0_ref_width_x_wire;
                chma_ref_height_y <= chma_l0_ref_height_y_wire;   
                ref_dpb_idx       <= cand_dpb_idx_l0;
                ch_frac_x <= cand_mv_field_mv_x_l0[MV_C_FRAC_WIDTH_HIGH-1:0];
                ch_frac_y <= cand_mv_field_mv_y_l0[MV_C_FRAC_WIDTH_HIGH-1:0];

            end
            else if(cand_mv_field_pred_flag_l1) begin
                luma_ref_start_x <= luma_l1_ref_start_x_wire;
                luma_ref_start_y <= luma_l1_ref_start_y_wire;
                luma_ref_width_x <= luma_l1_ref_width_x_wire;
                luma_ref_height_y <= luma_l1_ref_height_y_wire;
                chma_ref_start_x <= chma_l1_ref_start_x_wire;
                chma_ref_start_y <= chma_l1_ref_start_y_wire;
                chma_ref_width_x <= chma_l1_ref_width_x_wire;
                chma_ref_height_y <= chma_l1_ref_height_y_wire;
                ref_dpb_idx       <= cand_dpb_idx_l1;
                ch_frac_x <= cand_mv_field_mv_x_l1[MV_C_FRAC_WIDTH_HIGH-1:0];      
                ch_frac_y <= cand_mv_field_mv_y_l1[MV_C_FRAC_WIDTH_HIGH-1:0];
            end            
        end
        STATE_INTER_PRED_MV_FRAC_BI_DECODE: begin
            luma_ref_start_x <= luma_l1_ref_start_x_wire;
            luma_ref_start_y <= luma_l1_ref_start_y_wire;
            luma_ref_width_x <= luma_l1_ref_width_x_wire;
            luma_ref_height_y <= luma_l1_ref_height_y_wire;
            chma_ref_start_x <= chma_l1_ref_start_x_wire;
            chma_ref_start_y <= chma_l1_ref_start_y_wire;
            chma_ref_width_x <= chma_l1_ref_width_x_wire;
            chma_ref_height_y <= chma_l1_ref_height_y_wire;
            ref_dpb_idx       <= cand_dpb_idx_l1;
            ch_frac_x <= cand_mv_field_mv_x_l1[MV_C_FRAC_WIDTH_HIGH-1:0];      
            ch_frac_y <= cand_mv_field_mv_y_l1[MV_C_FRAC_WIDTH_HIGH-1:0];
        end
        STATE_INTER_PRED_MV_DECODE_READY: begin
`ifndef DBF_DISCONNECTED
            // if(cache_idle_in) begin
            if(cache_idle_in & !tu_res_pres_cb_empty & !tu_res_pres_cr_empty) begin
`else
            if(luma_filter_idle & cb_filter_idle & cr_filter_idle & !tu_res_pres_cb_empty & !tu_res_pres_cr_empty ) begin
`endif
                xT_in_min_tus <= next_xT_in_min_tus;
                yT_in_min_tus <= next_yT_in_min_tus;
            end
        end
        STATE_INTRA_PRED_SAMPLE_X_4_Y_4_UPDATE1: begin
            inner_x_8x8_loc <= next_inner_x_8x8_loc;
            inner_y_8x8_loc <= next_inner_y_8x8_loc;
            x_8x8_loc <= next_x_8x8_loc;
            y_8x8_loc <= next_y_8x8_loc;
        end
        STATE_INTRA_PRED_SAMPLE_X_4_Y_4_UPDATE2: begin
            xT_in_min_tus <= next_xT_in_min_tus;
            yT_in_min_tus <= next_yT_in_min_tus;
        end
    endcase
end

always@(posedge clk) begin
	dbf_fifo_is_full_hold_d <= (~y_dbf_fifo_is_full_hold) & (~cb_dbf_fifo_is_full_hold) & (~cr_dbf_fifo_is_full_hold) ;
end

always@(posedge clk) begin
    if(reset) begin
        inter_pred_sample_state <= STATE_INTER_PRED_SAMPLE_FILL_TU_DATA;
    end
    else begin
        inter_pred_sample_state <= inter_pred_sample_next_state;
    end
end

always@(posedge clk) begin
       case(config_mode_in)
        `INTER_TOP_PARA_0: begin
 `ifdef READ_FILE    
             pic_width <=   (config_data_bus_in[PIC_WIDTH_WIDTH + HEADER_WIDTH -1: HEADER_WIDTH])%(1<<PIC_DIM_WIDTH);
             pic_height  <=   (config_data_bus_in[HEADER_WIDTH + PIC_WIDTH_WIDTH + PIC_HEIGHT_WIDTH- 1: HEADER_WIDTH + PIC_WIDTH_WIDTH])%(1<<PIC_DIM_WIDTH);
 `else
             pic_height <=   (config_data_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH])%(1<<PIC_DIM_WIDTH);
             pic_width  <=   (config_data_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH - PIC_HEIGHT_WIDTH])%(1<<PIC_DIM_WIDTH);
 `endif
        end
        `INTER_TOP_PARA_1: begin
// `ifdef READ_FILE
        // log2_ctb_size <=  config_data_bus_in[ HEADER_WIDTH + LOG2CTBSIZEY_WIDTH -1 : HEADER_WIDTH];
// `else                    
        // log2_ctb_size <=  config_data_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - LOG2CTBSIZEY_WIDTH];
// `endif
        end
        `INTER_TOP_PARA_2: begin 
            
            
        end
        `INTER_TOP_SLICE_1: begin
// `ifdef READ_FILE
            // slice_temporal_mvp_enabled_flag <= config_data_bus_in[   HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH- 1:
                                                                // HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH  ];     
            // slice_type              <=      config_data_bus_in[      HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH + SLICE_TYPE_WIDTH- 1:
                                                                // HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH  ];   
                                                
            // collocated_from_l0_flag <= config_data_bus_in[   HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH + SLICE_TYPE_WIDTH + COLLOCATED_FROM_L0_FLAG_WIDTH - 1:
                                                        // HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH + SLICE_TYPE_WIDTH];
// `else                        
            // slice_temporal_mvp_enabled_flag <=   config_data_bus_in[ INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - 1:
                                                                // INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH];     
            // slice_type              <=      config_data_bus_in[  INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH -1:
                                                            // INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH - SLICE_TYPE_WIDTH];   
            // collocated_from_l0_flag <= config_data_bus_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH - SLICE_TYPE_WIDTH - 1:
                                                        // INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH - SLICE_TYPE_WIDTH - COLLOCATED_FROM_L0_FLAG_WIDTH];
// `endif
        end
        `INTER_TOP_SLICE_2: begin
// `ifdef READ_FILE
            // collocated_ref_idx <= config_data_bus_in[    HEADER_WIDTH + SLICE_CB_QP_OFFSET_WIDTH + SLICE_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH + SLICE_BETA_OFFSET_DIV2_WIDTH + SLICE_TC_OFFSET_DIV2_WIDTH + COLLOCATED_REF_IDX_WIDTH - 2:
                                                    // HEADER_WIDTH + SLICE_CB_QP_OFFSET_WIDTH + SLICE_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH + SLICE_BETA_OFFSET_DIV2_WIDTH + SLICE_TC_OFFSET_DIV2_WIDTH];
// `else
             // collocated_ref_idx <= config_data_bus_in[    INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SLICE_CB_QP_OFFSET_WIDTH - SLICE_CR_QP_OFFSET_WIDTH - DISABLE_DBF_WIDTH - SLICE_BETA_OFFSET_DIV2_WIDTH - SLICE_TC_OFFSET_DIV2_WIDTH - 2:
                                                    // INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SLICE_CB_QP_OFFSET_WIDTH - SLICE_CR_QP_OFFSET_WIDTH - DISABLE_DBF_WIDTH - SLICE_BETA_OFFSET_DIV2_WIDTH - SLICE_TC_OFFSET_DIV2_WIDTH - COLLOCATED_REF_IDX_WIDTH];
// `endif
        end        
        `INTER_TOP_CURR_POC: begin
// `ifdef READ_FILE
            // current_poc <=  config_data_bus_in[POC_WIDTH - 1: 0];
// `else
            // current_poc <=  config_data_bus_in[INTER_TOP_CONFIG_BUS_WIDTH -1 : INTER_TOP_CONFIG_BUS_WIDTH - POC_WIDTH];
// `endif
        end
        `INTER_CURRENT_PIC_DPB_IDX: begin
            // next_pic_dpb_idx <= config_data_bus_in[INTER_TOP_CONFIG_BUS_WIDTH -1: INTER_TOP_CONFIG_BUS_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH];
        end
        `INTER_SLICE_TILE_INFO: begin
            
        end
        `INTER_CTU0_HEADER: begin
            // new_ctu_start_to_col <= 1;
`ifdef READ_FILE
            next_ctb_xx_in_min_tus <= config_data_bus_in[HEADER_WIDTH + X11_ADDR_WDTH   -1: HEADER_WIDTH] >> LOG2_MIN_TU_SIZE;                 // 12-bit input assigned to 11- bit output MSB dropped
            next_ctb_yy_in_min_tus <= config_data_bus_in[HEADER_WIDTH + X11_ADDR_WDTH + X_ADDR_WDTH -1: HEADER_WIDTH + X_ADDR_WDTH] >> LOG2_MIN_TU_SIZE;
`else                   
            next_ctb_xx_in_min_tus <= config_data_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH -1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - X_ADDR_WDTH];                 // 12-bit input assigned to 11- bit output MSB dropped
            next_ctb_yy_in_min_tus <= config_data_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - X11_ADDR_WDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X_ADDR_WDTH];
`endif
        end
        `INTER_CU_HEADER: begin
`ifdef READ_FILE
            next_cb_xx_in_min_tus <= next_ctb_xx_in_min_tus + (config_data_bus_in[   HEADER_WIDTH + X0_WIDTH - 1: 
                                                          HEADER_WIDTH]>> LOG2_MIN_TU_SIZE);
            next_cb_yy_in_min_tus <= next_ctb_yy_in_min_tus + (config_data_bus_in[   HEADER_WIDTH + 2*X0_WIDTH -1:
                                                HEADER_WIDTH + X0_WIDTH]>> LOG2_MIN_TU_SIZE);
            next_cb_pred_mode <=  config_data_bus_in[     HEADER_WIDTH + 2*X0_WIDTH + LOG2_CTB_WIDTH];
            next_cb_part_mode <=  config_data_bus_in[     HEADER_WIDTH + 2*X0_WIDTH + LOG2_CTB_WIDTH + PREDMODE_WIDTH + PARTMODE_WIDTH -1:
                                                HEADER_WIDTH + 2*X0_WIDTH + LOG2_CTB_WIDTH + PREDMODE_WIDTH];
`else
            next_cb_xx_in_min_tus <= next_ctb_xx_in_min_tus + (config_data_bus_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 1: 
                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - X0_WIDTH]>> LOG2_MIN_TU_SIZE);
            next_cb_yy_in_min_tus <= next_ctb_yy_in_min_tus + (config_data_bus_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - X0_WIDTH - 1: 
                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH]>> LOG2_MIN_TU_SIZE);
            next_cb_pred_mode <=  config_data_bus_in[     INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - LOG2_CTB_WIDTH - 1];
            next_cb_part_mode <=  config_data_bus_in[     INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - LOG2_CTB_WIDTH - PREDMODE_WIDTH - 1:
                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - LOG2_CTB_WIDTH - PREDMODE_WIDTH - PARTMODE_WIDTH];
`endif
            // next_cb_size <=  cb_size_wire ;
            next_cb_size_in_min_tus <=  cb_size_wire_in_min_pus ;
            end

    endcase
end


`ifndef DBF_DISCONNECTED  
    `ifdef CACHE_PIPE
	assign  inter_filter_cache_idle_out = cache_idle_in & inter_idle_to_intra & luma_filter_idle & cb_filter_idle & cr_filter_idle & (inter_pred_sample_state == STATE_INTER_PRED_SAMPLE_FILL_TU_DATA); 
    `elsif CACHE_PIPE_PIPE
	assign  inter_filter_cache_idle_out = cache_idle_in & inter_idle_to_intra & luma_full_filter_idle & cb_full_filter_idle & cr_full_filter_idle & (inter_pred_sample_state == STATE_INTER_PRED_SAMPLE_FILL_TU_DATA) & miss_elem_fifo_empty_out; 
    `elsif CACHE_PIPE_PIPE_HIT_FIFO
	assign  inter_filter_cache_idle_out = cache_idle_in & inter_idle_to_intra & luma_full_filter_idle & cb_full_filter_idle & cr_full_filter_idle & (inter_pred_sample_state == STATE_INTER_PRED_SAMPLE_FILL_TU_DATA) & miss_elem_fifo_empty_out & (dbf_fifo_is_full_hold_d) ; 
	`else
    assign  inter_filter_cache_idle_out = cache_idle_in & luma_filter_idle & cb_filter_idle & cr_filter_idle & (!cache_block_ready) & (inter_pred_sample_state == STATE_INTER_PRED_SAMPLE_FILL_TU_DATA); 

	`endif
`else
	assign  inter_filter_cache_idle_out = 1 & luma_filter_idle & cb_filter_idle & cr_filter_idle & (!ref_range_valid_out); 
`endif
	assign cb_res_present_out = tu_res_pres_cb_out_stage_2;
    assign cr_res_present_out = tu_res_pres_cr_out_stage_2;

always@(*) begin
    if((xx_pb0_strt_in_min_tus <= xT_in_min_tus) && (xx_pb0_end__in_min_tus > xT_in_min_tus)  && (yy_pb0_strt_in_min_tus <= yT_in_min_tus) && (yy_pb0_end__in_min_tus > yT_in_min_tus)) begin
        pu_0_in_range = 1;
    end
    else begin
        pu_0_in_range = 0;
    end
    if((xx_pb1_strt_in_min_tus <= xT_in_min_tus) && (xx_pb1_end__in_min_tus > xT_in_min_tus)  && (yy_pb1_strt_in_min_tus <= yT_in_min_tus) && (yy_pb1_end__in_min_tus > yT_in_min_tus)) begin
        pu_1_in_range = 1;
    end
    else begin
        pu_1_in_range = 0;
    end
    if((xx_pb2_strt_in_min_tus <= xT_in_min_tus) && (xx_pb2_end__in_min_tus > xT_in_min_tus)  && (yy_pb2_strt_in_min_tus <= yT_in_min_tus) && (yy_pb2_end__in_min_tus > yT_in_min_tus)) begin
        pu_2_in_range = 1;
    end
    else begin
        pu_2_in_range = 0;
    end
    if((xx_pb3_strt_in_min_tus <= xT_in_min_tus) && (xx_pb3_end__in_min_tus > xT_in_min_tus)  && (yy_pb3_strt_in_min_tus <= yT_in_min_tus) && (yy_pb3_end__in_min_tus > yT_in_min_tus)) begin
        pu_3_in_range = 1;
    end
    else begin
        pu_3_in_range = 0;
    end
end

// assign luma_l0_mid_x_clipped_wire = luma_l0_mid_x[MVD_WIDTH] ? 0 : ((luma_l0_mid_x >= pic_width )? (pic_width -1 ):luma_l0_mid_x[X11_ADDR_WDTH-1:0] )   ; //12 bits
// assign luma_l0_mid_y_clipped_wire = luma_l0_mid_y[MVD_WIDTH] ? 0 : ((luma_l0_mid_y >= pic_height)? (pic_height -1):luma_l0_mid_y[X11_ADDR_WDTH-1:0] )   ;
// assign chma_l0_mid_x_clipped_wire = chma_l0_mid_x[MVD_WIDTH] ? 0 : ((chma_l0_mid_x >= pic_width )? (pic_width -1 ):chma_l0_mid_x[X11_ADDR_WDTH-1:0] )   ;
// assign chma_l0_mid_y_clipped_wire = chma_l0_mid_y[MVD_WIDTH] ? 0 : ((chma_l0_mid_y >= pic_height)? (pic_height -1):chma_l0_mid_y[X11_ADDR_WDTH-1:0] )   ;

// assign luma_l1_mid_x_clipped_wire = luma_l1_mid_x[MVD_WIDTH] ? 0 : ((luma_l1_mid_x >= pic_width )? (pic_width -1 ):luma_l1_mid_x[X11_ADDR_WDTH-1:0] )   ;
// assign luma_l1_mid_y_clipped_wire = luma_l1_mid_y[MVD_WIDTH] ? 0 : ((luma_l1_mid_y >= pic_height)? (pic_height -1):luma_l1_mid_y[X11_ADDR_WDTH-1:0] )   ;
// assign chma_l1_mid_x_clipped_wire = chma_l1_mid_x[MVD_WIDTH] ? 0 : ((chma_l1_mid_x >= pic_width )? (pic_width -1 ):chma_l1_mid_x[X11_ADDR_WDTH-1:0] )   ;
// assign chma_l1_mid_y_clipped_wire = chma_l1_mid_y[MVD_WIDTH] ? 0 : ((chma_l1_mid_y >= pic_height)? (pic_height -1):chma_l1_mid_y[X11_ADDR_WDTH-1:0] )   ;


assign luma_l0_mid_x_wire = cand_mv_field_mv_x_l0[MVD_WIDTH -1: MV_L_INT_WIDTH_LOW]         + (xT_in_min_tus<<2); //15 bits ( 16 + 1 - FRAC_BITS)
assign luma_l0_mid_y_wire = cand_mv_field_mv_y_l0[MVD_WIDTH -1: MV_L_INT_WIDTH_LOW]         + (yT_in_min_tus<<2);
assign chma_l0_mid_x_wire = (cand_mv_field_mv_x_l0[MVD_WIDTH -1: MV_C_INT_WIDTH_LOW]<<1)    + (xT_in_min_tus<<2);
assign chma_l0_mid_y_wire = (cand_mv_field_mv_y_l0[MVD_WIDTH -1: MV_C_INT_WIDTH_LOW]<<1)    + (yT_in_min_tus<<2);

assign luma_l1_mid_x_wire = cand_mv_field_mv_x_l1[MVD_WIDTH -1: MV_L_INT_WIDTH_LOW]         + (xT_in_min_tus<<2);
assign luma_l1_mid_y_wire = cand_mv_field_mv_y_l1[MVD_WIDTH -1: MV_L_INT_WIDTH_LOW]         + (yT_in_min_tus<<2);
assign chma_l1_mid_x_wire = (cand_mv_field_mv_x_l1[MVD_WIDTH -1: MV_C_INT_WIDTH_LOW]<<1)    + (xT_in_min_tus<<2);
assign chma_l1_mid_y_wire = (cand_mv_field_mv_y_l1[MVD_WIDTH -1: MV_C_INT_WIDTH_LOW]<<1)    + (yT_in_min_tus<<2);


always@(*) begin
    case(cand_mv_field_mv_x_l0[MV_L_FRAC_WIDTH_HIGH-1:0])
        2'd0: begin luma_l0_ref_start_x_wire = luma_l0_mid_x        ;luma_l0_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE)       -1; end   //15 -bits, width 4 bits
        2'd1: begin luma_l0_ref_start_x_wire = luma_l0_mid_x - 4'd3 ;luma_l0_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) +3 +3 -1; end
        2'd3: begin luma_l0_ref_start_x_wire = luma_l0_mid_x - 4'd2 ;luma_l0_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) +4 +2 -1; end
        2'd2: begin luma_l0_ref_start_x_wire = luma_l0_mid_x - 4'd3 ;luma_l0_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) +4 +3 -1; end
    endcase
end


always@(*) begin
    case(cand_mv_field_mv_x_l1[MV_L_FRAC_WIDTH_HIGH-1:0])
        2'd0: begin luma_l1_ref_start_x_wire = luma_l1_mid_x        ;luma_l1_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE)       -1; end
        2'd1: begin luma_l1_ref_start_x_wire = luma_l1_mid_x - 4'd3 ;luma_l1_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) +3 +3 -1; end
        2'd3: begin luma_l1_ref_start_x_wire = luma_l1_mid_x - 4'd2 ;luma_l1_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) +4 +2 -1; end
        2'd2: begin luma_l1_ref_start_x_wire = luma_l1_mid_x - 4'd3 ;luma_l1_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) +4 +3 -1; end
    endcase
end

always@(*) begin
    case(cand_mv_field_mv_y_l0[MV_L_FRAC_WIDTH_HIGH-1:0])
        2'd0: begin luma_l0_ref_start_y_wire = luma_l0_mid_y        ;luma_l0_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE)       -1; end
        2'd1: begin luma_l0_ref_start_y_wire = luma_l0_mid_y - 4'd3 ;luma_l0_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) +3 +3 -1; end
        2'd3: begin luma_l0_ref_start_y_wire = luma_l0_mid_y - 4'd2 ;luma_l0_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) +4 +2 -1; end
        2'd2: begin luma_l0_ref_start_y_wire = luma_l0_mid_y - 4'd3 ;luma_l0_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) +4 +3 -1; end
    endcase
end


always@(*) begin
    case(cand_mv_field_mv_y_l1[MV_L_FRAC_WIDTH_HIGH-1:0])
        2'd0: begin luma_l1_ref_start_y_wire = luma_l1_mid_y        ;luma_l1_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE)       -1; end
        2'd1: begin luma_l1_ref_start_y_wire = luma_l1_mid_y - 4'd3 ;luma_l1_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) +3 +3 -1; end
        2'd3: begin luma_l1_ref_start_y_wire = luma_l1_mid_y - 4'd2 ;luma_l1_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) +4 +2 -1; end
        2'd2: begin luma_l1_ref_start_y_wire = luma_l1_mid_y - 4'd3 ;luma_l1_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) +4 +3 -1; end
    endcase
end


always@(*) begin
    case(cand_mv_field_mv_x_l0[MV_C_FRAC_WIDTH_HIGH-1:0])
        3'd0    : begin chma_l0_ref_start_x_wire = chma_l0_mid_x        ;chma_l0_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) -2                 ; end
        default : begin chma_l0_ref_start_x_wire = chma_l0_mid_x -1*2   ;chma_l0_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) +(2*2) + 1*2 - 2   ; end
    endcase
end

always@(*) begin
    case(cand_mv_field_mv_x_l1[MV_C_FRAC_WIDTH_HIGH-1:0])
        3'd0    : begin chma_l1_ref_start_x_wire = chma_l1_mid_x        ;chma_l1_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) -2                 ; end
        default : begin chma_l1_ref_start_x_wire = chma_l1_mid_x -1*2   ;chma_l1_ref_width_x_wire = (1<<LOG2_MIN_TU_SIZE) +(2*2) + 1*2 - 2   ; end
    endcase
end

always@(*) begin
    case(cand_mv_field_mv_y_l0[MV_C_FRAC_WIDTH_HIGH-1:0])
        3'd0    : begin chma_l0_ref_start_y_wire = chma_l0_mid_y        ;chma_l0_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) -2                 ; end
        default : begin chma_l0_ref_start_y_wire = chma_l0_mid_y -1*2   ;chma_l0_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) +(2*2) + 1*2 - 2   ; end
    endcase
end

always@(*) begin
    case(cand_mv_field_mv_y_l1[MV_C_FRAC_WIDTH_HIGH-1:0])
        3'd0    : begin chma_l1_ref_start_y_wire = chma_l1_mid_y        ;chma_l1_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) -2                 ; end
        default : begin chma_l1_ref_start_y_wire = chma_l1_mid_y -1*2   ;chma_l1_ref_height_y_wire = (1<<LOG2_MIN_TU_SIZE) +(2*2) + 1*2 - 2   ; end
    endcase
end


    
    
    
`ifdef READ_FILE

    assign log2_cb_size_wire = config_data_bus_in[      HEADER_WIDTH + 2*X0_WIDTH + LOG2_CTB_WIDTH - 1:
                                                        HEADER_WIDTH + 2*X0_WIDTH];
`else
    assign log2_cb_size_wire = config_data_bus_in[      INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - 1: 
                                                        INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - LOG2_CTB_WIDTH];
`endif

`ifdef READ_FILE
    assign x0_tu_in_ctu_wire = tu_data_in[   HEADER_WIDTH + XT_WIDTH -1:
                                            HEADER_WIDTH]                               ;
`else
    assign x0_tu_in_ctu_wire = tu_data_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 1: 
                                            INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - XT_WIDTH]    ;
`endif

`ifdef READ_FILE
    assign y0_tu_in_ctu_wire = tu_data_in[   HEADER_WIDTH + XT_WIDTH + YT_WIDTH -1:
                                            HEADER_WIDTH + YT_WIDTH]                            ;
`else
    assign y0_tu_in_ctu_wire = tu_data_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - XT_WIDTH- 1: 
                                            INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - XT_WIDTH - YT_WIDTH] ;
`endif

`ifdef READ_FILE
    assign log2_tb_size_wire = tu_data_in[     HEADER_WIDTH + XT_WIDTH + YT_WIDTH + CIDX_WIDTH + LOG2_CB_SIZE_WIDTH -1:
                                            HEADER_WIDTH + XT_WIDTH + YT_WIDTH + CIDX_WIDTH];
`else
    assign log2_tb_size_wire = tu_data_in[     INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - XT_WIDTH - YT_WIDTH - CIDX_WIDTH - 1: 
                                            INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - XT_WIDTH - YT_WIDTH - CIDX_WIDTH - LOG2_CB_SIZE_WIDTH];
`endif

`ifdef READ_FILE
    assign c_idx_wire = tu_data_in[    HEADER_WIDTH + XT_WIDTH + YT_WIDTH + CIDX_WIDTH-1:
                                    HEADER_WIDTH + XT_WIDTH + YT_WIDTH];
`else
    assign c_idx_wire = tu_data_in[     INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - XT_WIDTH - YT_WIDTH - 1: 
                                        INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - XT_WIDTH - YT_WIDTH - CIDX_WIDTH];
`endif


`ifdef READ_FILE
    assign res_present_wire = tu_data_in[       HEADER_WIDTH + XT_WIDTH + YT_WIDTH + CIDX_WIDTH + LOG2_CB_SIZE_WIDTH + INTRA_MODE_WIDTH + RES_PRESENT_WIDTH -1:
                                                HEADER_WIDTH + XT_WIDTH + YT_WIDTH + CIDX_WIDTH + LOG2_CB_SIZE_WIDTH + INTRA_MODE_WIDTH];
`else
    assign res_present_wire = tu_data_in[       INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - XT_WIDTH - YT_WIDTH - CIDX_WIDTH - LOG2_CB_SIZE_WIDTH - INTRA_MODE_WIDTH - 1: 
                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - XT_WIDTH - YT_WIDTH - CIDX_WIDTH - LOG2_CB_SIZE_WIDTH - INTRA_MODE_WIDTH - RES_PRESENT_WIDTH];
`endif





    always@(*) begin
        case(log2_cb_size_wire)
            3: begin
                cb_size_wire_in_min_pus = 2;
            end
            4: begin
                cb_size_wire_in_min_pus = 4;  
            end
            5: begin
                cb_size_wire_in_min_pus = 8;
            end
            6: begin
                cb_size_wire_in_min_pus = 16;
            end
            default: begin
                cb_size_wire_in_min_pus = 16;
            end
        endcase
    end
    
    always@(*) begin
        case(log2_tb_size_wire)
            2: begin
                tb_size_wire_in_min_tus = 1;
            end
            3: begin
                tb_size_wire_in_min_tus = 2;
            end
            4: begin
                tb_size_wire_in_min_tus = 4;  
            end
            5: begin
                tb_size_wire_in_min_tus = 8;
            end
            6: begin
                tb_size_wire_in_min_tus = 16;
            end
			default: begin
				tb_size_wire_in_min_tus = 0;
			end
        endcase
    end


always@(*) begin
    if(((xT_in_min_tus + 1) == now_cb_xx_end_in_min_tus ) && ((yT_in_min_tus + 1) == now_cb_yy_end_in_min_tus)) begin
        end_of_cu = 1;
    end
    else begin
        end_of_cu = 0;
    end
end



always@(*) begin
    if(((xT_in_min_tus + 1) == x0_tu_end_in_min_tus ) && ((yT_in_min_tus + 1) == y0_tu_end_in_min_tus)) begin
        end_of_tu = 1;
    end
    else begin
        end_of_tu = 0;
    end
end


always@(*) begin
    if(((xT_in_min_tus + 1) == x0_tu_end_in_min_tus ) && ((yT_in_min_tus + 1) == y0_tu_end_in_min_tus) && bi_pred_block_cache_in == 0 && ref_range_valid_out && xT_in_min_tus[0]==1 & yT_in_min_tus[0]==1) begin
        end_of_tu_cr_rd_en = 1;
    end
    else begin
        end_of_tu_cr_rd_en = 0;
    end
end

always@(*) begin
    if(((xT_in_min_cr_out + 1) ==  x0_tu_end_in_min_tus_cr_wgt_out) && ((yT_in_min_cr_out + 1) == y0_tu_end_in_min_tus_cr_wgt_out) && (cr_wght_pred_valid)) begin
        end_of_tu_cr_rd_en_stage_2 = 1;
    end
    else begin
        end_of_tu_cr_rd_en_stage_2 = 0;
    end
end


// always@(*) begin
    // if(((xT_in_min_cb_out + 1) ==  x0_tu_end_in_min_tus_cb_wgt_out) && ((yT_in_min_cb_out + 1) == y0_tu_end_in_min_tus_cb_wgt_out) && (cb_wght_pred_valid)) begin
        // end_of_tu_cb_rd_en_stage_2 = 1;
    // end
    // else begin
        // end_of_tu_cb_rd_en_stage_2 = 0;
    // end
// end      


always@(*) begin
    if(((xT_in_min_cr_out + 1) ==  x0_tu_end_in_min_tus_cr_wgt_out)) begin
        inter_cr_predsample_4by4_last_col = 1;
    end
    else begin
        inter_cr_predsample_4by4_last_col = 0;
    end
end

always@(*) begin
    if(((yT_in_min_cr_out + 1) ==  y0_tu_end_in_min_tus_cr_wgt_out)) begin
        inter_cr_predsample_4by4_last_row = 1;
    end
    else begin
        inter_cr_predsample_4by4_last_row = 0;
    end
end


always@(*) begin
    if(((xT_in_min_cb_out + 1) ==  x0_tu_end_in_min_tus_cb_wgt_out)) begin
        inter_cb_predsample_4by4_last_col = 1;
    end
    else begin
        inter_cb_predsample_4by4_last_col = 0;
    end
end

always@(*) begin
    if(((yT_in_min_cb_out + 1) ==  y0_tu_end_in_min_tus_cb_wgt_out)) begin
        inter_cb_predsample_4by4_last_row = 1;
    end
    else begin
        inter_cb_predsample_4by4_last_row = 0;
    end
end

always@(*) begin
    if(((xT_in_min_luma_out + 1) ==  x0_tu_end_in_min_tus_yy_wgt_out)) begin
        inter_y_predsample_4by4_last_col = 1;
    end
    else begin
        inter_y_predsample_4by4_last_col = 0;
    end
end

always@(*) begin
    if(((yT_in_min_luma_out + 1) ==  y0_tu_end_in_min_tus_yy_wgt_out)) begin
        inter_y_predsample_4by4_last_row = 1;
    end
    else begin
        inter_y_predsample_4by4_last_row = 0;
    end
end


always@(*) begin
    // if(((xT_in_min_tus + 1) == x0_tu_end_in_min_tus ) && (xT_in_min_tus[0] & yT_in_min_tus[0])) begin
        // next_xT_in_min_tus = x0_tu_in_min_tus;       
        // next_yT_in_min_tus = yT_in_min_tus + 1;
    // end
    // else begin
        next_xT_in_min_tus = x_8x8_loc + inner_x_8x8_loc;
        next_yT_in_min_tus = y_8x8_loc + inner_y_8x8_loc;    
    // end
end

always@(*) begin
    if(((x_8x8_loc + 2) == x0_tu_end_in_min_tus ) && (inner_x_8x8_loc & inner_y_8x8_loc)) begin // if both xT_in_min_tus and yT_in_min_tus are odd numbers that is  the end point of a 8x8 block
        next_y_8x8_loc = y_8x8_loc + 2;
        next_x_8x8_loc = x0_tu_in_min_tus;        
    end
    else begin
        if(inner_x_8x8_loc & inner_y_8x8_loc) begin
            next_y_8x8_loc = y_8x8_loc;
            next_x_8x8_loc = x_8x8_loc + 2;
        end
        else begin
            next_x_8x8_loc = x_8x8_loc;
            next_y_8x8_loc = y_8x8_loc;
        end
    end
end

always@(*) begin
    next_inner_x_8x8_loc = !inner_x_8x8_loc;            // inner locs and x_8x8_loc y_8x8_loc operate one clock cycle ahead
    if(inner_x_8x8_loc) begin
        next_inner_y_8x8_loc = !inner_y_8x8_loc;   
    end
    else begin
        next_inner_y_8x8_loc = inner_y_8x8_loc;   
    end
end


// synthesis translate_off
    always@(*) begin
        if(inter_pred_sample_state == STATE_INTER_PRED_SAMPLE_FILL_TU_DATA && !tu_empty) begin
            if(log2_tb_size_wire == 6 || log2_tb_size_wire == 7) begin
                $display(" wrong log2_ctb_size");
                $stop();
            end 
        end
    end

// synthesis translate_on

endmodule 