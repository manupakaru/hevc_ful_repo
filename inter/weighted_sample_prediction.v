`timescale 1ns / 1ps
module weighted_sample_prediction(
    clk,
    reset,
    bi_pred_block_in,
    res_present_in,
    x0_tu_end_in_min_tus_in,
    x0_tu_end_in_min_tus_out,
    y0_tu_end_in_min_tus_in,
    y0_tu_end_in_min_tus_out,

    res_present_out,
    xT_in_min_tus,
    yT_in_min_tus,
    xT_out_min_tus,
    yT_out_min_tus,
    valid_in,
    pred_pix_in_4x4,
    pred_pix_out_4x4,
    valid_out
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter   BLOCK_WIDTH_4x4 = 3'd4;
    parameter   SHIFT1 = 4'd14 - PIXEL_WIDTH;
    parameter   SHIFT2 = 4'd15 - PIXEL_WIDTH;

	parameter OFFSET1 = SHIFT1 > 1'b0 ? 1<<(SHIFT1-1'b1) : 1'b0;
	parameter OFFSET2 = 1<< (SHIFT2-1'b1);

    parameter PIXEL_MAX = 255;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
    input bi_pred_block_in;
    input res_present_in;
    output reg res_present_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      x0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      y0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_out_min_tus;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_out_min_tus;
    input valid_in;
    input signed [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 * FILTER_PIXEL_WIDTH -1:0]         pred_pix_in_4x4;    
    output reg [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 * PIXEL_WIDTH -1:0]                  pred_pix_out_4x4;    
    output reg valid_out;

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus_d;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus_d;
    
    
    integer i,j,k;
    
    reg signed [FILTER_PIXEL_WIDTH - 1:0] in_store_wire   [BLOCK_WIDTH_4x4 - 1: 0][BLOCK_WIDTH_4x4 -1: 0];
    reg signed [FILTER_PIXEL_WIDTH    :0] in_store_d      [BLOCK_WIDTH_4x4 - 1: 0][BLOCK_WIDTH_4x4 -1: 0];
    reg signed [FILTER_PIXEL_WIDTH    :0] in_store_2d     [BLOCK_WIDTH_4x4 - 1: 0][BLOCK_WIDTH_4x4 -1: 0];
    
    reg [PIXEL_WIDTH - 1:0] out_store[BLOCK_WIDTH_4x4 - 1: 0][BLOCK_WIDTH_4x4 -1: 0];

    reg is_prev_4x4_bi_pred;

    reg res_present_d;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_d;

    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_d;

    integer weight_state;
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module

always@(*) begin
    for(j=0;j <BLOCK_WIDTH_4x4 ; j=j+1 ) begin
        for(i=0 ; i < BLOCK_WIDTH_4x4 ; i = i+1) begin
            for(k=0; k< FILTER_PIXEL_WIDTH ; k = k+1) begin
                in_store_wire[j][i][k] = pred_pix_in_4x4[(j*BLOCK_WIDTH_4x4 + i)*FILTER_PIXEL_WIDTH + k];
            end
        end
    end
end

always@(*) begin
    for(j=0;j <BLOCK_WIDTH_4x4 ; j=j+1 ) begin
        for(i=0 ; i < BLOCK_WIDTH_4x4 ; i = i+1) begin
            for(k=0; k< PIXEL_WIDTH ; k = k+1) begin
                pred_pix_out_4x4[(j*BLOCK_WIDTH_4x4 + i)*PIXEL_WIDTH + k] = out_store[j][i][k];
            end
        end
    end
end


always@(*) begin
    if(is_prev_4x4_bi_pred) begin
        for(j=0;j <BLOCK_WIDTH_4x4 ; j=j+1 ) begin
            for(i=0 ; i < BLOCK_WIDTH_4x4 ; i = i+1) begin
                in_store_2d[j][i] = $signed($signed(in_store_d[j][i]) + $signed(OFFSET2))>>> SHIFT2;
            end
        end    
    end
    else begin
        for(j=0;j <BLOCK_WIDTH_4x4 ; j=j+1 ) begin
            for(i=0 ; i < BLOCK_WIDTH_4x4 ; i = i+1) begin
                in_store_2d[j][i] = $signed($signed(in_store_d[j][i]) + $signed(OFFSET1))>>> SHIFT1;
            end
        end    
    end
end



always@(posedge clk) begin
    if(reset) begin
        is_prev_4x4_bi_pred <= 0;
        valid_out <= 0;
        weight_state <=0;
        xT_in_min_tus_d <= -1;
        yT_in_min_tus_d <= -1;
    end
    else begin
        case(weight_state)
            0: begin
                valid_out <= 0;
                if(valid_in) begin
                    res_present_d <= res_present_in;
                    xT_in_min_tus_d <= xT_in_min_tus;
                    yT_in_min_tus_d <= yT_in_min_tus;
                    x0_tu_end_in_min_tus_d <= x0_tu_end_in_min_tus_in;
                    y0_tu_end_in_min_tus_d <= y0_tu_end_in_min_tus_in;
                    for(j=0;j <BLOCK_WIDTH_4x4 ; j=j+1 ) begin
                        for(i=0 ; i < BLOCK_WIDTH_4x4 ; i = i+1) begin
                            in_store_d[j][i] <= {in_store_wire[j][i][FILTER_PIXEL_WIDTH-1], in_store_wire[j][i]};
                        end
                    end
                    if (bi_pred_block_in) begin
                        weight_state <= 2;
                    end
                    else begin
                        weight_state <= 1;
                    end
                end
            end
            1: begin
                weight_state <= 0;
                valid_out <= 1;
                is_prev_4x4_bi_pred <= 0;
                for(j=0;j <BLOCK_WIDTH_4x4 ; j=j+1 ) begin
                    for(i=0 ; i < BLOCK_WIDTH_4x4 ; i = i+1) begin
                        if($signed(in_store_2d[j][i]) > PIXEL_MAX) begin
                            out_store[j][i] <= {PIXEL_WIDTH{1'b1}};
                        end
                        else if(in_store_2d[j][i][FILTER_PIXEL_WIDTH]) begin
                            out_store[j][i] <= {PIXEL_WIDTH{1'b0}};
                        end
                        else begin
                            out_store[j][i] <= in_store_2d[j][i];   //warning dunnata awulak naha
                        end
                    end
                end
                xT_out_min_tus <= xT_in_min_tus_d;
                yT_out_min_tus <= yT_in_min_tus_d;
                res_present_out <= res_present_d;    
                x0_tu_end_in_min_tus_out <= x0_tu_end_in_min_tus_d;
                y0_tu_end_in_min_tus_out <= y0_tu_end_in_min_tus_d;
            end
            2:begin
                if (valid_in) begin
                    // synthesis translate_off
                    if((xT_in_min_tus != xT_in_min_tus_d) || (yT_in_min_tus != yT_in_min_tus_d) || (res_present_in != res_present_d)) begin
                        $display("%d bi pred signal wrong!!",$time);
                        $stop;
                    end
                    // synthesis translate_on
                    weight_state <= 1;
                    is_prev_4x4_bi_pred <= 1;
                    for(j=0;j <BLOCK_WIDTH_4x4 ; j=j+1 ) begin
                        for(i=0 ; i < BLOCK_WIDTH_4x4 ; i = i+1) begin
                            in_store_d[j][i] <= {in_store_wire[j][i][FILTER_PIXEL_WIDTH-1], in_store_wire[j][i]} + in_store_d[j][i];
                        end
                    end
                end
            end
        endcase
    end
end


endmodule 