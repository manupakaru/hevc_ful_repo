
// ************************************************************************************************
//
// PROJECT      :   <project name where applicable> 
// PRODUCT      :   <product name where applicable> 
// FILE         :   <File Name/Module Name>
// AUTHOR       :   <Author's name>
// DESCRIPTION  :   <at least a short description about the module/file>
//                  Where does this file get inputs and send outputs?
//                  What does the guts of this file accomplish, and how does it do it?
//                  What module(s) does this file instantiate?
//
// ************************************************************************************************
//
// REVISIONS:
//
//	Date			Developer	Description
//	----			---------	-----------
//  29 Jan 2012		nuwang		Creation
//  29 Feb 2012		user123     bug fix - <jira issue id>
//  31 Oct 2007		userxyz		added xyz functionality - <jira issue id>
//  ...              ...         ...
//  ...              ...         ...
//
//**************************************************************************************************

`timescale 1ns / 1ps

module residual_adder
    (
      clk,
      reset,

      res_present_in,
      predsamples_4by4_valid_in,
      predsamples_4by4_in,

      residual_4by4_in,
      residual_fifo_read_en_out,
      residual_fifo_is_empty_in,

      ans_4by4_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input 																clk;
    input 																reset;

    input 	[PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] 	predsamples_4by4_in;

    input 																res_present_in;

    input   [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0] residual_4by4_in;
    input                                                               residual_fifo_is_empty_in;
    output reg                                                          residual_fifo_read_en_out;

    output  [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] 	ans_4by4_out;

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
	wire    [PIXEL_WIDTH - 1:0]											predsamples_4by4_int_arr [OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
	wire    [RESIDUAL_WIDTH - 1:0]										residual_4by4_int_arr    [OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
	reg     [RESIDUAL_WIDTH + 1 - 1:0]									res_added_10b			 [OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0]; 

	reg     [PIXEL_WIDTH - 1:0]											temp_pre_ans_reg         [OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];

//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    
    always @(posedge clk) begin : template_sync_reset
    	integer i;
    	integer j;
    	if(reset) begin
    		
    	end
    	else begin
            if((predsamples_4by4_valid_in == 1'b1)&&(residual_fifo_is_empty_in == 1'b0)) begin

        		if(res_present_in == 1'b1) begin

        			for ( i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin
        				for ( j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin
        					res_added_10b[i][j] = {2'd0,predsamples_4by4_int_arr[i][j]} + {residual_4by4_int_arr[i][j][RESIDUAL_WIDTH - 1],residual_4by4_int_arr[i][j]};
        				end
        			end

                    residual_fifo_read_en_out <= 1'b1;
        			
        		end
        		else begin

                    for ( i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin
                        for ( j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin
                            res_added_10b[i][j] = {2'd0,predsamples_4by4_int_arr[i][j]};
                        end
                    end
        			
        		end

            end            
    	end
    end

    always @(*) begin : clipping
        integer i;
        integer j;
		for ( i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin
			for ( j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin
				case(res_added_10b[i][j][RESIDUAL_WIDTH + 1 - 1 : RESIDUAL_WIDTH + 1 - 2])
					2'b00 : begin
						temp_pre_ans_reg[i][j] = res_added_10b[i][j][PIXEL_WIDTH - 1:0];
					end
					2'b01 : begin
						temp_pre_ans_reg[i][j] = (1'b1 << PIXEL_WIDTH) - 1'b1;
					end
					2'b10 : begin
						temp_pre_ans_reg[i][j] = {PIXEL_WIDTH{1'b0}};
					end
					2'b11 : begin
						temp_pre_ans_reg[i][j] = {PIXEL_WIDTH{1'b0}};
					end		
				endcase
			end
		end	
    end

    generate
	    genvar i;
	    genvar j;
        
        for (i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin : row_iteration
            for (j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin : column_iteration
                assign predsamples_4by4_int_arr[i][j] = predsamples_4by4_in[i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE +(j+1)*PIXEL_WIDTH - 1:j*PIXEL_WIDTH + i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE];
                assign residual_4by4_int_arr[i][j]    = residual_4by4_in   [i*RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE +(j+1)*RESIDUAL_WIDTH - 1:j*RESIDUAL_WIDTH + i*RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE];

                assign ans_4by4_out[i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE +(j+1)*PIXEL_WIDTH - 1:j*PIXEL_WIDTH + i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE] = temp_pre_ans_reg[i][j];
            end
        end
    endgenerate
    
     

    
   
    
    
endmodule