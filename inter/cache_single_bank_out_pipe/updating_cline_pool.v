`timescale 1ns / 1ps
module updating_cline_pool
(
    clk,
	reset,
    new_wr_en,
	new_cache_addr_in,
	cache_wr_en,
	cache_addr_in,
	valid_mask_out,
	
	pool_addr_out1,
	pool_addr_out2,
	pool_addr_out3,
	pool_addr_out4,
	pool_addr_out5,
	pool_addr_out6,
	pool_addr_out7,
	pool_addr_out8,

);

    `include "../sim/cache_configs_def.v"
    
    //---------------------------------------------------------------------------------------------------------------------
    // parameter definitions
    //---------------------------------------------------------------------------------------------------------------------
	parameter												MAX_IN_POOL = 8 ;
	parameter												TAG_BANK_LEN = SET_ADDR_WDTH + C_N_WAY ;
	
    //---------------------------------------------------------------------------------------------------------------------
    // localparam definitions
    //---------------------------------------------------------------------------------------------------------------------
    
    localparam                          STATE_CLEAR				        = 0;    // the state that is entered upon reset
    localparam                          STATE_NORMAL	                = 1;
   	
    //---------------------------------------------------------------------------------------------------------------------
    // I/O signals
    //---------------------------------------------------------------------------------------------------------------------


    input                           						clk;
	input													reset;
	input													new_wr_en;
	input													cache_wr_en;
	input	[TAG_BANK_LEN-1:0]								new_cache_addr_in;			// write 1 tag at once 3 LSBs for set_idx
	input	[TAG_BANK_LEN-1:0]								cache_addr_in;			// write 1 tag at once 3 LSBs for set_idx
	output reg	[MAX_IN_POOL-1:0]      						valid_mask_out;

	output  [TAG_BANK_LEN-1:0] pool_addr_out1;
	output  [TAG_BANK_LEN-1:0] pool_addr_out2;
	output  [TAG_BANK_LEN-1:0] pool_addr_out3;
	output  [TAG_BANK_LEN-1:0] pool_addr_out4;
	output  [TAG_BANK_LEN-1:0] pool_addr_out5;
	output  [TAG_BANK_LEN-1:0] pool_addr_out6;
	output  [TAG_BANK_LEN-1:0] pool_addr_out7;
	output  [TAG_BANK_LEN-1:0] pool_addr_out8;

	

	
	//---------------------------------------------------------------------------------------------------------------------
    // Internal wires and registers
    //---------------------------------------------------------------------------------------------------------------------
  
	integer i;
	integer ii;
	reg  [TAG_BANK_LEN-1:0] pool_addr_out[0:MAX_IN_POOL-1];
	
	assign pool_addr_out1 = pool_addr_out[0];
	assign pool_addr_out2 = pool_addr_out[1];
	assign pool_addr_out3 = pool_addr_out[2];
	assign pool_addr_out4 = pool_addr_out[3];
	assign pool_addr_out5 = pool_addr_out[4];
	assign pool_addr_out6 = pool_addr_out[5];
	assign pool_addr_out7 = pool_addr_out[6];
	assign pool_addr_out8 = pool_addr_out[7];

    //---------------------------------------------------------------------------------------------------------------------
    // Implmentation
    //---------------------------------------------------------------------------------------------------------------------
    
 	always@(posedge clk) begin
		if(new_wr_en) begin
			if(valid_mask_out[0] == 0) begin
				valid_mask_out[0] <= 1; pool_addr_out[0] <= new_cache_addr_in ;
			end
			else if(cache_wr_en && cache_addr_in == pool_addr_out1) begin
				valid_mask_out[0] <= 0;
			end
		end
		else begin
			if(cache_wr_en && cache_addr_in == pool_addr_out1) begin
				valid_mask_out[0] <= 0;
			end
		end

		for (i=1; i< MAX_IN_POOL; i=i+1) begin
			if(new_wr_en) begin
				if(valid_mask_out[i] == 0 && ((&valid_mask_out[i-1:0])) ) begin
					valid_mask_out[i] <= 1; pool_addr_out[i] <= new_cache_addr_in ;
				end
				else if(cache_wr_en && cache_addr_in == pool_addr_out[i]) begin
					valid_mask_out[i] <= 0;
				end
			end
			else begin
				if(cache_wr_en && cache_addr_in == pool_addr_out[i]) begin
					valid_mask_out[i] <= 0;
				end
			end		
		end

	end
  
	
endmodule