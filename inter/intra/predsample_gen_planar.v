`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:53:23 11/11/2013 
// Design Name: 
// Module Name:    predsample_gen_planar 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module predsample_gen_planar
    (
        clk,
        reset,
        
        intrapredangle_low5b_in,
        mode_in,
        planar_en_in,
        
        data_0_in,
        data_1_in,
        data_2_in,
        data_3_in,
        data_4_in,
        
        predsample_4by4_out,
        predsample_4by4_valid_out   
    
    
    );


endmodule
