`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:59:52 10/16/2013 
// Design Name: 
// Module Name:    intra_angular_top_prediction_aux_left_data_supplier 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module intra_angular_prediction_aux_data_supplier(
        clk,
        reset,
        enable,
        
        //ntbs_in,
        abs_intrapredangle_in,
        
        read_from_aux_flags_in,
        valid_in,
        mode_in,
        low_6bits_of_read_addr_0_in,
        aux_axis_tb_corner_addr_in,
        inner_aux_in,
        filter_done_in,
        filter_flag_in,
        
        max_valid_aux_range_valid_in,
        max_valid_aux_range_in,
        
        aux_read_addr_out,
        aux_read_en_out,
        aux_read_data_in,
        
        corner_pixel_data_in,
        corner_pixel_filtered_data_in,

        aux_data0_out,
        aux_data1_out,
        aux_data2_out,
        aux_data3_out,
        aux_data4_out
    );
    
//----------------------------------------------------
//LOCALPARAMS
//----------------------------------------------------
    localparam                      INTRA_PRED_ANGLE_WIDTH  = 6;
    localparam                      MAX_NTBS_SIZE           = 6;
    localparam                      NUM_OF_DATA_REGS        = 4;
    localparam                      PIXEL_WIDTH             = 8;
    localparam                      LOG2_FRAME_SIZE         = 12;
    localparam                      MODE_WIDTH              = 2;
    
    localparam                      STATE_WAIT              = 0;
    localparam                      STATE_READ              = 1;
    
    localparam                      TOP_PRED_MODE_WIDTH         = 2;
    localparam                      TOP_PRED_MODE_DC            = 0;
    localparam                      TOP_PRED_MODE_PLANAR        = 1;
    localparam                      TOP_PRED_MODE_ANGULAR_TOP   = 2;
    localparam                      TOP_PRED_MODE_ANGULAR_LEFT  = 3;
    
//----------------------------------------------------
// I/O Signals
//----------------------------------------------------

    input                                       clk;
    input                                       reset;
    input                                       enable;
    
    input       [INTRA_PRED_ANGLE_WIDTH - 1:0]  abs_intrapredangle_in;
    input                                       read_from_aux_flags_in;
    input                                       valid_in;
    input       [TOP_PRED_MODE_WIDTH - 1:0]     mode_in;
    input       [MAX_NTBS_SIZE - 1:0]           low_6bits_of_read_addr_0_in;
    input       [LOG2_FRAME_SIZE - 1:0]         aux_axis_tb_corner_addr_in;
    input       [MAX_NTBS_SIZE - 1:0]           inner_aux_in;
    input                                       filter_done_in;
    input                                       filter_flag_in;
    
    input                                       max_valid_aux_range_valid_in;
    input       [LOG2_FRAME_SIZE - 1:0]         max_valid_aux_range_in;
    
    output reg  [LOG2_FRAME_SIZE - 1:0]         aux_read_addr_out;
    output reg                                  aux_read_en_out;
    input       [PIXEL_WIDTH - 1:0]             aux_read_data_in;
    
    input       [PIXEL_WIDTH - 1:0]             corner_pixel_data_in;
    input       [PIXEL_WIDTH - 1:0]             corner_pixel_filtered_data_in;
    
    output reg  [PIXEL_WIDTH - 1:0]             aux_data0_out;
    output      [PIXEL_WIDTH - 1:0]             aux_data1_out;
    output      [PIXEL_WIDTH - 1:0]             aux_data2_out;
    output      [PIXEL_WIDTH - 1:0]             aux_data3_out;
    output      [PIXEL_WIDTH - 1:0]             aux_data4_out;
    
//----------------------------------------------------
//  Internal Regs and Wires 
//----------------------------------------------------    
    
    reg     [PIXEL_WIDTH - 1:0]                 aux_data    [NUM_OF_DATA_REGS - 1:0];
    reg     [LOG2_FRAME_SIZE - 1:0]             aux_read_addr_prefilt;
    wire    [MAX_NTBS_SIZE - 1:0]               aux_read_addr_offset;
    reg     [LOG2_FRAME_SIZE - 1:0]             max_valid_aux_range_reg;
    integer                                     state;
    
    intra_angular_aux_addr_gen intra_angular_aux_addr_gen_block(
        
        //.ntbs_in                    (ntbs_in),
        .abs_intrapredangle_in      (abs_intrapredangle_in),
        
        .read_neg_addr_index_in     (low_6bits_of_read_addr_0_in),
        .aux_read_addr_offset_out   (aux_read_addr_offset)
    );
    
    always @(*) begin
        if(filter_flag_in & filter_done_in) begin
            if(mode_in == TOP_PRED_MODE_PLANAR) begin
                aux_read_addr_prefilt = aux_read_addr_offset;
            end
            else begin
                aux_read_addr_prefilt = (1'b1 + inner_aux_in);
            end
        end
        else begin
            if(mode_in == TOP_PRED_MODE_PLANAR) begin
                aux_read_addr_prefilt = aux_axis_tb_corner_addr_in + aux_read_addr_offset;
            end
            else begin
                aux_read_addr_prefilt = aux_axis_tb_corner_addr_in + (1'b1 + inner_aux_in);
            end
        end
    end
    
    
//    assign aux_read_addr_prefilt = (mode_in == TOP_PRED_MODE_PLANAR) ?  aux_axis_tb_corner_addr_in + aux_read_addr_offset : aux_axis_tb_corner_addr_in + (1'b1 + inner_aux_in);
    
    always @(*) begin
        if(aux_read_addr_prefilt == {LOG2_FRAME_SIZE{1'b0}}) begin
            aux_read_addr_out = {LOG2_FRAME_SIZE{1'b0}};
        end
        else if(aux_read_addr_prefilt > max_valid_aux_range_reg) begin
            aux_read_addr_out = max_valid_aux_range_in;
        end
        else begin
            aux_read_addr_out = aux_read_addr_prefilt - 1'b1;
        end
    end
    
    always @(posedge clk) begin
        if(max_valid_aux_range_valid_in == 1'b1) begin
            max_valid_aux_range_reg <= max_valid_aux_range_in;
        end
    end
    
    always @(*) begin
        aux_read_en_out = read_from_aux_flags_in & valid_in;
    end

    always @(posedge clk) begin
        if(reset) begin
            state <= STATE_WAIT;
        end
        else if (enable) begin
            case(state) 
                STATE_WAIT : begin
                    if(read_from_aux_flags_in & valid_in) begin
                        state <= STATE_READ;
                    end 
                end
                STATE_READ : begin
                
                    // if(aux_read_addr_offset == {MAX_NTBS_SIZE{1'b0}} ) begin
                    //     if(filter_flag_in & filter_done_in) begin
                    //         aux_data[0] <= corner_pixel_filtered_data_in;
                    //     end
                    //     else begin
                    //         aux_data[0] <= corner_pixel_data_in;
                    //     end
                    // end
                    // else begin
                    //     aux_data[0] <= aux_read_data_in;
                    // end
                    
                    aux_data[0] <= aux_data0_out;
                    aux_data[1] <= aux_data[0];
                    aux_data[2] <= aux_data[1];
                    aux_data[3] <= aux_data[2];
//                  aux_data[4] <= aux_data[3];
                
                    if(read_from_aux_flags_in & valid_in) begin
                        state <= STATE_READ;
                    end
                    else begin
                        state <= STATE_WAIT;
                    end
                end
            endcase
        end
    end

    always @(*) begin

        if(aux_read_addr_offset == {MAX_NTBS_SIZE{1'b0}} ) begin
            if(filter_flag_in & filter_done_in) begin
                aux_data0_out = corner_pixel_filtered_data_in;
            end
            else begin
                aux_data0_out = corner_pixel_data_in;
            end
        end
        else begin
            aux_data0_out = aux_read_data_in;
        end
            
    end
    
//    assign aux_data0_out = aux_read_data_in;
    assign aux_data1_out = aux_data[0];
    assign aux_data2_out = aux_data[1];
    assign aux_data3_out = aux_data[2];
    assign aux_data4_out = aux_data[3];
    
endmodule
