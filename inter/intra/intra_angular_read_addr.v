module intra_angular_read_addr 
    (
        clk,
        reset,
        enable,
        
        abs_intrapredangle_in,
        sgn_intrapredangle_in,
        inner_mainaxis_shifted_2_in,
        inner_auxaxis_in,
        main_t_in,
        aux_t_in,
        // valid_in,
        mode_in,
        dc_read_addr_offset_in,
        
        filter_flag_in,
        filter_done_in,
        filter_addr_offset_in,
        
        max_valid_range_in,
        max_valid_range_valid_in,
     
        read_from_aux_flag,
        read_addr_out,
        // valid_out,
        
        intrapredacc_lower5b
        // intrapredacc_upper7b
    );
    
//----------------------------------------------------
//LOCALPARAMS
//----------------------------------------------------
    localparam                      INTRA_PRED_ANGLE_WIDTH      = 6;
    localparam                      MAX_NTBS_SIZE               = 6;
    localparam                      NUM_OF_DATA_REGS            = 5;
    localparam                      PIXEL_WIDTH                 = 8;
    localparam                      LOG2_FRAME_SIZE             = 12;
    localparam                      VALID_RANGE_SHIFTER_LENGTH  = 2;
    localparam                      MODE_WIDTH                  = 2;
    
    localparam                      TOP_PRED_MODE_WIDTH         = 2;
    localparam                      TOP_PRED_MODE_DC            = 0;
    localparam                      TOP_PRED_MODE_PLANAR        = 1;
    localparam                      TOP_PRED_MODE_ANGULAR_TOP   = 2;
    localparam                      TOP_PRED_MODE_ANGULAR_LEFT  = 3;
    
    
//----------------------------------------------------
//PARAMETERS
//----------------------------------------------------
    
//----------------------------------------------------
//I/O Signals
//----------------------------------------------------

    
    input               clk;
    input               reset;
    input               enable;
    
    input               [INTRA_PRED_ANGLE_WIDTH - 1:0]  abs_intrapredangle_in;
    input                                               sgn_intrapredangle_in;
    input               [MAX_NTBS_SIZE - 1:0]           inner_mainaxis_shifted_2_in;
    input               [MAX_NTBS_SIZE - 1:0]           inner_auxaxis_in;
    input               [LOG2_FRAME_SIZE - 1:0]         main_t_in;
    input               [LOG2_FRAME_SIZE - 1:0]         aux_t_in;
    // input                                               valid_in;
    input               [TOP_PRED_MODE_WIDTH - 1:0]     mode_in;
    input               [MAX_NTBS_SIZE - 1:0]           dc_read_addr_offset_in;
    
    input                                               filter_flag_in;
    input                                               filter_done_in;
    input               [MAX_NTBS_SIZE - 1:0]           filter_addr_offset_in;
    
    input               [LOG2_FRAME_SIZE - 1:0]         max_valid_range_in;
    input                                               max_valid_range_valid_in;
    
    output reg                                          read_from_aux_flag;
    output reg          [LOG2_FRAME_SIZE - 1:0]         read_addr_out;
    // output reg                                          valid_out;
    
    output              [4:0]                           intrapredacc_lower5b;
    // output              [6:0]                           intrapredacc_upper7b;
    
//----------------------------------------------------
//  Internal Regs and Wires 
//----------------------------------------------------      
        
    reg                                 sgn_intrapredangle_reg;
    reg         [MAX_NTBS_SIZE - 1:0]   inner_mainaxis_shifted_2_reg;  
    reg         [MAX_NTBS_SIZE - 1:0]   inner_auxaxis_reg;
    
    reg         [LOG2_FRAME_SIZE - 1:0] intrapredangle_acc;
    reg         [LOG2_FRAME_SIZE - 1:0] read_addr;     
    // reg                                 valid_reg;
    
    reg         [LOG2_FRAME_SIZE - 1:0] max_valid_range_reg [VALID_RANGE_SHIFTER_LENGTH - 1:0];
    
//----------------------------------------------------
//  Implementation
//---------------------------------------------------- 
    
    always @(posedge clk /*or posedge reset*/) begin : main
       integer i;
       if (reset) begin
       
       end
       else if(enable) begin
            // valid_reg                        <=   valid_in;
            // valid_out                        <=   valid_reg;

            if(max_valid_range_valid_in == 1'b1) begin
                max_valid_range_reg[0] <= max_valid_range_in;
                    for( i = 0 ; i < VALID_RANGE_SHIFTER_LENGTH - 1 ; i = i + 1 ) begin 
                        max_valid_range_reg[i+1] <= max_valid_range_reg[i];
                    end
            end
           
            // if(valid_in) begin
               intrapredangle_acc           <=   abs_intrapredangle_in*inner_auxaxis_in;
               inner_mainaxis_shifted_2_reg <=   inner_mainaxis_shifted_2_in;
               sgn_intrapredangle_reg       <=   sgn_intrapredangle_in;
            // end
       
            
                if(filter_flag_in == 1'b0) begin
                    case(mode_in)
                        TOP_PRED_MODE_ANGULAR_LEFT,TOP_PRED_MODE_ANGULAR_TOP : begin
                            if(sgn_intrapredangle_reg == 1'b0) begin
                                read_addr           <= main_t_in + inner_mainaxis_shifted_2_reg + intrapredangle_acc[11:5];
                                read_from_aux_flag  <= 1'b0;
                            end
                            else begin
                                read_addr           <= {{(LOG2_FRAME_SIZE-5){1'b0}},intrapredangle_acc[11:5]};  
                                read_from_aux_flag  <= 1'b1;          
                            end
                        end
                        TOP_PRED_MODE_PLANAR : begin
                            read_addr           <= main_t_in + inner_mainaxis_shifted_2_reg;  
                            read_from_aux_flag  <= 1'b1;   
                        end
                        TOP_PRED_MODE_DC : begin
                            read_addr           <= main_t_in + dc_read_addr_offset_in;
                            read_from_aux_flag  <= 1'b0; 
                        end
                    endcase
                end
                else begin
                    if(filter_done_in == 1'b1) begin
                        case(mode_in)
                            TOP_PRED_MODE_ANGULAR_LEFT,TOP_PRED_MODE_ANGULAR_TOP : begin
                                if(sgn_intrapredangle_reg == 1'b0) begin
                                    read_addr           <= inner_mainaxis_shifted_2_reg + intrapredangle_acc[11:5];
                                    read_from_aux_flag  <= 1'b0;
                                end
                                else begin
                                    read_addr           <= {{(LOG2_FRAME_SIZE-5){1'b0}},intrapredangle_acc[11:5]};  
                                    read_from_aux_flag  <= 1'b1;          
                                end
                            end
                            TOP_PRED_MODE_PLANAR : begin
                                read_addr           <= inner_mainaxis_shifted_2_reg;  
                                read_from_aux_flag  <= 1'b1;   
                            end
                            TOP_PRED_MODE_DC : begin
                                read_addr           <= dc_read_addr_offset_in;
                                read_from_aux_flag  <= 1'b0; 
                            end
                        endcase
                    end
                    else begin
                        read_addr           <= main_t_in + filter_addr_offset_in;
                        read_from_aux_flag  <= 1'b0; 
                    end
                end
            
       

    end
end

    always @(*) begin
        if(read_addr > max_valid_range_in) begin
            read_addr_out = max_valid_range_in;
        end
        else begin
            read_addr_out = read_addr;
        end 
    end

assign intrapredacc_lower5b = intrapredangle_acc[4:0];
// assign intrapredacc_upper7b = intrapredangle_acc[11:5];
    
//    module intra_angular_refpixel_selection
//    (
//        .clk(clk),
//        .reset(reset),
//        .enable(1'b1),
//    
//        .intra_pred_angle(abs_intrapredAngle_in),
//    
//        .ref_pixel_minus_addr_in(read_addr[0]),
//        .ref_pixel_other_addr_out(read_other_addr_int),
//    
//    );
    
//    always @(*) begin
//        if(max_valid_leftrange_in < read_other_addr_int) begin
//            read_other_addr_out = max_valid_leftrange_in;
//        end
//        else begin
//            read_other_addr_out = read_other_addr_int;
//        end
//    end
    
//    assign read_addr_0_out = read_addr_reg[0];
//    assign read_addr_1_out = read_addr_reg[1];
//    assign read_addr_2_out = read_addr_reg[2];
//    assign read_addr_3_out = read_addr_reg[3];
//    assign read_addr_4_out = read_addr_reg[4];
    

endmodule
    