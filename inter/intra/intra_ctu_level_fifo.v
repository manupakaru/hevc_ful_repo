`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:33:54 11/13/2013 
// Design Name: 
// Module Name:    intra_ctu_level_fifo 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module intra_ctu_level_fifo
    (
        clk,
        reset,
        
        read_data_out,
        read_en_in,
        read_almost_empty_out,
        read_empty_out,
        
        write_data_in,
        write_en_in,
        write_almost_full_out,
        write_full_out
    );
    
//----------------------------------------------------
//LOCALPARAMS
//----------------------------------------------------
    localparam       LOG2_MAX_CTU_SIZE          = 6;
    localparam       MAX_CTU_SIZE               = 1 << LOG2_MAX_CTU_SIZE;
    localparam       MAX_FRAME_WIDTH            = 1920;
    localparam       MAX_FRAME_HEIGHT           = 1080;
    localparam       LOG2_FRAME_SIZE            = 12;
    localparam       PIXEL_WIDTH                = 8;
    localparam       NUMBER_OF_CTUS_PER_WIDTH   = (1920 + ((1 << LOG2_MAX_CTU_SIZE) - 1)) >> LOG2_MAX_CTU_SIZE;      
    
//----------------------------------------------------
// I/O Signals
//----------------------------------------------------
    
    input                                       clk;
    input                                       reset;
    
    output reg [PIXEL_WIDTH - 1:0]              read_data_out;
    input                                       read_en_in;
    output                                      read_almost_empty_out;
    output                                      read_empty_out;
    
    input   [PIXEL_WIDTH - 1:0]                 write_data_in;
    input                                       write_en_in;
    output                                      write_almost_full_out;
    output                                      write_full_out;
    
//----------------------------------------------------
// Internal Regs and Wires
//----------------------------------------------------

    reg     [LOG2_FRAME_SIZE - LOG2_MAX_CTU_SIZE - 1:0] write_pointer;
    reg     [LOG2_FRAME_SIZE - LOG2_MAX_CTU_SIZE - 1:0] read_pointer;
    
    reg     [LOG2_FRAME_SIZE - LOG2_MAX_CTU_SIZE - 1:0] data_inside;
    
    reg     [PIXEL_WIDTH - 1:0]                         fifo_mem[NUMBER_OF_CTUS_PER_WIDTH - 1:0];

    
//----------------------------------------------------
// Implementation
//----------------------------------------------------    
    
//    always @(*) begin
//        if(read_pointer == {(LOG2_FRAME_SIZE - LOG2_MAX_CTU_SIZE){1'b0}}) begin
//            if(old_write_pointer == NUMBER_OF_CTUS_PER_WIDTH) begin
//                write_full = 1'b1;
//            end
//            else begin
//                write_full = 1'b0;
//            end
//        end
//        else begin
//            if(old_write_pointer == read_pointer - 1'b1) begin
//                write_full = 1'b1;
//            end
//            else begin
//                write_full = 1'b0;
//            end
//        end
//    end
    
    always @(posedge clk or posedge reset) begin : data_inside_modifier
        if(reset) begin
            data_inside <= {(LOG2_FRAME_SIZE - LOG2_MAX_CTU_SIZE){1'b0}};
        end
        else begin
            if(data_inside == 0) begin
                if(write_en_in) begin
                    data_inside <= data_inside + 1'b1;
                end
            end
            else if(data_inside == NUMBER_OF_CTUS_PER_WIDTH) begin
                if(read_en_in) begin
                    data_inside <= data_inside - 1'b1;
                end
            end
            else begin
                if(write_en_in & read_en_in) begin
                    data_inside <= data_inside;
                end
                else if(write_en_in) begin
                    data_inside <= data_inside + 1'b1;
                end
                else if(read_en_in) begin
                    data_inside <= data_inside - 1'b1;
                end
            end
        end
    end
    
    always @(posedge clk) begin : write
        if(reset) begin
            write_pointer   <= {(LOG2_FRAME_SIZE - LOG2_MAX_CTU_SIZE){1'b0}};
        end
        else if(write_en_in) begin
            if(data_inside != NUMBER_OF_CTUS_PER_WIDTH) begin
                fifo_mem[write_pointer] <= write_data_in;
                write_pointer <= write_pointer + 1'b1;
            end
        end
    end
    
    always @(posedge clk) begin : read
        if(reset) begin
            read_pointer    <= {(LOG2_FRAME_SIZE - LOG2_MAX_CTU_SIZE){1'b0}};
        end
        else if(read_en_in) begin
            if(data_inside != {(LOG2_FRAME_SIZE - LOG2_MAX_CTU_SIZE){1'b0}}) begin
                read_data_out <= fifo_mem[read_pointer];
                read_pointer  <= read_pointer + 1'b1;
            end
        end
    end
    
    assign write_full_out           = (data_inside == NUMBER_OF_CTUS_PER_WIDTH);
    assign write_almost_full_out    = (data_inside == (NUMBER_OF_CTUS_PER_WIDTH - 1'b1));
    assign read_empty_out           = (data_inside == {(LOG2_FRAME_SIZE - LOG2_MAX_CTU_SIZE){1'b0}} );
    assign read_almost_empty_out    = (data_inside == {{(LOG2_FRAME_SIZE - LOG2_MAX_CTU_SIZE - 1){1'b0}},1'b1});
    
endmodule
