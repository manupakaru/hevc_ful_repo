`timescale 1ns / 1ps
module tag_memory_write_first
(
    clk,
	reset,
    r_addr_in,
	w_addr_in,
	r_data_out,
	w_data_in,
	w_en_in,
	//r_en_in,
	valid_bits_out

);

    `include "../sim/cache_configs_def.v"
    
    //---------------------------------------------------------------------------------------------------------------------
    // parameter definitions
    //---------------------------------------------------------------------------------------------------------------------
	parameter												TAG_BANK_LEN = SET_ADDR_WDTH + C_N_WAY ;
    
	
    //---------------------------------------------------------------------------------------------------------------------
    // localparam definitions
    //---------------------------------------------------------------------------------------------------------------------
    
    localparam                          STATE_CLEAR				        = 0;    // the state that is entered upon reset
    localparam                          STATE_NORMAL	                = 1;
   	
    //---------------------------------------------------------------------------------------------------------------------
    // I/O signals
    //---------------------------------------------------------------------------------------------------------------------
    

    input                           						clk;
	input													reset;
	input	[TAG_ADDR_WDTH-1:0]      						w_data_in;
	input	[SET_ADDR_WDTH -1:0]							r_addr_in;			// read 8 tags at once 
	input	[TAG_BANK_LEN-1:0]								w_addr_in;			// write 1 tag at once 3 LSBs for set_idx
	input													w_en_in;
    //input                                                   r_en_in;
	output  reg [(1<<C_N_WAY)*TAG_ADDR_WDTH-1:0]    	      	r_data_out;
	output 	reg [(1<<C_N_WAY)-1:0]						    valid_bits_out;
	
	
	//---------------------------------------------------------------------------------------------------------------------
    // Internal wires and registers
    //---------------------------------------------------------------------------------------------------------------------
  

    reg         [TAG_ADDR_WDTH-1:0]      				mem [(1<<TAG_BANK_LEN)-1:0];
	reg			[(1<<TAG_BANK_LEN)-1:0] 			    valid_bits;
	//reg			[SET_ADDR_WDTH - Y_BANK_BITS-1:0]		out_address;
//	reg         [TAG_BANK_LEN-1:0]                      count;
//    reg                                                 all_clear;
//	integer state;
	wire [(1<<C_N_WAY)-1:0]valid_bit_sets[0 : (1<<SET_ADDR_WDTH)-1];
  
	    generate
			genvar ii;
			for(ii=0;ii<(1<<SET_ADDR_WDTH);ii=ii+1) begin
				assign valid_bit_sets[ii] = valid_bits[(ii+1)*(1<<C_N_WAY) -1: (ii)*(1<<C_N_WAY)];
			end
		endgenerate
    //---------------------------------------------------------------------------------------------------------------------
    // Implmentation
    //---------------------------------------------------------------------------------------------------------------------
    
    
	// always @(posedge clk) begin
		// if(reset) begin
			// count <= 0;
			// state <= STATE_CLEAR;
		// end
		// else begin
			// case(state) 
				// STATE_CLEAR: begin
					// if(count == TAG_BANK_LEN) begin
						// state <= STATE_NORMAL;
					// end
					// else begin
						// valid_bits[count] <= 0;
                        // count <= count + 1;
					// end
				// end
				// STATE_NORMAL: begin
					// if(w_en_in) begin
						// mem[w_addr_in] <= w_data_in;
						// valid_bits[w_addr_in]	<= 1;
					// end
				// end
			// endcase
		// end
	// end	
	
	// assign 	r_data_out = {	mem[{r_addr_in,3'b000}],
							// mem[{r_addr_in,3'b001}],
							// mem[{r_addr_in,3'b010}],
							// mem[{r_addr_in,3'b011}],
							// mem[{r_addr_in,3'b100}],
							// mem[{r_addr_in,3'b101}],
							// mem[{r_addr_in,3'b110}],
							// mem[{r_addr_in,3'b111}]};
						
	// assign 	valid_bits_out = {	valid_bits[{r_addr_in,3'b000}],
								// valid_bits[{r_addr_in,3'b001}],
								// valid_bits[{r_addr_in,3'b010}],
								// valid_bits[{r_addr_in,3'b011}],
								// valid_bits[{r_addr_in,3'b100}],
								// valid_bits[{r_addr_in,3'b101}],
								// valid_bits[{r_addr_in,3'b110}],
								// valid_bits[{r_addr_in,3'b111}]};    
    
	always @(posedge clk) begin
		if(reset) begin
			//count <= 0;
			//state <= STATE_NORMAL;
            //all_clear <=0;
            valid_bits <= {(1<<TAG_BANK_LEN){1'b0}};
		end
		else begin

			if(w_en_in) begin
				mem[w_addr_in] <= w_data_in;
				valid_bits[w_addr_in]	<= 1'b1;
			end
			if(r_addr_in == w_addr_in[SET_ADDR_WDTH + C_N_WAY-1:C_N_WAY] && w_en_in) begin
				case(w_addr_in[C_N_WAY-1:0])
					3'd0: begin r_data_out <=  {	mem[{r_addr_in,3'b111}]	,mem[{r_addr_in,3'b110}],mem[{r_addr_in,3'b101}],mem[{r_addr_in,3'b100}],mem[{r_addr_in,3'b011}],mem[{r_addr_in,3'b010}],mem[{r_addr_in,3'b001}],w_data_in				};		end					
					3'd1: begin r_data_out <=  {	mem[{r_addr_in,3'b111}]	,mem[{r_addr_in,3'b110}],mem[{r_addr_in,3'b101}],mem[{r_addr_in,3'b100}],mem[{r_addr_in,3'b011}],mem[{r_addr_in,3'b010}],w_data_in				,mem[{r_addr_in,3'b000}]};		end					
					3'd2: begin r_data_out <=  {	mem[{r_addr_in,3'b111}]	,mem[{r_addr_in,3'b110}],mem[{r_addr_in,3'b101}],mem[{r_addr_in,3'b100}],mem[{r_addr_in,3'b011}],w_data_in				,mem[{r_addr_in,3'b001}],mem[{r_addr_in,3'b000}]};		end					
					3'd3: begin r_data_out <=  {	mem[{r_addr_in,3'b111}]	,mem[{r_addr_in,3'b110}],mem[{r_addr_in,3'b101}],mem[{r_addr_in,3'b100}],w_data_in				,mem[{r_addr_in,3'b010}],mem[{r_addr_in,3'b001}],mem[{r_addr_in,3'b000}]};		end					
					3'd4: begin r_data_out <=  {	mem[{r_addr_in,3'b111}]	,mem[{r_addr_in,3'b110}],mem[{r_addr_in,3'b101}],w_data_in				,mem[{r_addr_in,3'b011}],mem[{r_addr_in,3'b010}],mem[{r_addr_in,3'b001}],mem[{r_addr_in,3'b000}]};		end					
					3'd5: begin r_data_out <=  {	mem[{r_addr_in,3'b111}]	,mem[{r_addr_in,3'b110}],w_data_in				,mem[{r_addr_in,3'b100}],mem[{r_addr_in,3'b011}],mem[{r_addr_in,3'b010}],mem[{r_addr_in,3'b001}],mem[{r_addr_in,3'b000}]};		end					
					3'd6: begin r_data_out <=  {	mem[{r_addr_in,3'b111}]	,w_data_in				,mem[{r_addr_in,3'b101}],mem[{r_addr_in,3'b100}],mem[{r_addr_in,3'b011}],mem[{r_addr_in,3'b010}],mem[{r_addr_in,3'b001}],mem[{r_addr_in,3'b000}]};		end					
					3'd7: begin r_data_out <=  {	w_data_in				,mem[{r_addr_in,3'b110}],mem[{r_addr_in,3'b101}],mem[{r_addr_in,3'b100}],mem[{r_addr_in,3'b011}],mem[{r_addr_in,3'b010}],mem[{r_addr_in,3'b001}],mem[{r_addr_in,3'b000}]};		end					
				endcase
				case(w_addr_in[C_N_WAY-1:0])
					3'd0: begin valid_bits_out <=  {	valid_bits[{r_addr_in,3'b111}]	,valid_bits[{r_addr_in,3'b110}]	,valid_bits[{r_addr_in,3'b101}]	,valid_bits[{r_addr_in,3'b100}]	,valid_bits[{r_addr_in,3'b011}]	,valid_bits[{r_addr_in,3'b010}]	,valid_bits[{r_addr_in,3'b001}]	,1'b1          					};		end					
					3'd1: begin valid_bits_out <=  {	valid_bits[{r_addr_in,3'b111}]	,valid_bits[{r_addr_in,3'b110}]	,valid_bits[{r_addr_in,3'b101}]	,valid_bits[{r_addr_in,3'b100}]	,valid_bits[{r_addr_in,3'b011}]	,valid_bits[{r_addr_in,3'b010}]	,1'b1          					,valid_bits[{r_addr_in,3'b000}]};		end					
					3'd2: begin valid_bits_out <=  {	valid_bits[{r_addr_in,3'b111}]	,valid_bits[{r_addr_in,3'b110}]	,valid_bits[{r_addr_in,3'b101}]	,valid_bits[{r_addr_in,3'b100}]	,valid_bits[{r_addr_in,3'b011}]	,1'b1          					,valid_bits[{r_addr_in,3'b001}]	,valid_bits[{r_addr_in,3'b000}]};		end					
					3'd3: begin valid_bits_out <=  {	valid_bits[{r_addr_in,3'b111}]	,valid_bits[{r_addr_in,3'b110}]	,valid_bits[{r_addr_in,3'b101}]	,valid_bits[{r_addr_in,3'b100}]	,1'b1          					,valid_bits[{r_addr_in,3'b010}]	,valid_bits[{r_addr_in,3'b001}]	,valid_bits[{r_addr_in,3'b000}]};		end					
					3'd4: begin valid_bits_out <=  {	valid_bits[{r_addr_in,3'b111}]	,valid_bits[{r_addr_in,3'b110}]	,valid_bits[{r_addr_in,3'b101}]	,1'b1          					,valid_bits[{r_addr_in,3'b011}]	,valid_bits[{r_addr_in,3'b010}]	,valid_bits[{r_addr_in,3'b001}]	,valid_bits[{r_addr_in,3'b000}]};		end					
					3'd5: begin valid_bits_out <=  {	valid_bits[{r_addr_in,3'b111}]	,valid_bits[{r_addr_in,3'b110}]	,1'b1          					,valid_bits[{r_addr_in,3'b100}]	,valid_bits[{r_addr_in,3'b011}]	,valid_bits[{r_addr_in,3'b010}]	,valid_bits[{r_addr_in,3'b001}]	,valid_bits[{r_addr_in,3'b000}]};		end					
					3'd6: begin valid_bits_out <=  {	valid_bits[{r_addr_in,3'b111}]	,1'b1          					,valid_bits[{r_addr_in,3'b101}]	,valid_bits[{r_addr_in,3'b100}]	,valid_bits[{r_addr_in,3'b011}]	,valid_bits[{r_addr_in,3'b010}]	,valid_bits[{r_addr_in,3'b001}]	,valid_bits[{r_addr_in,3'b000}]};		end					
					3'd7: begin valid_bits_out <=  {	1'b1          					,valid_bits[{r_addr_in,3'b110}]	,valid_bits[{r_addr_in,3'b101}]	,valid_bits[{r_addr_in,3'b100}]	,valid_bits[{r_addr_in,3'b011}]	,valid_bits[{r_addr_in,3'b010}]	,valid_bits[{r_addr_in,3'b001}]	,valid_bits[{r_addr_in,3'b000}]};		end					
				endcase
			end
			else begin
				r_data_out <=  {	mem[{r_addr_in,3'b111}],
									mem[{r_addr_in,3'b110}],
									mem[{r_addr_in,3'b101}],
									mem[{r_addr_in,3'b100}],
									mem[{r_addr_in,3'b011}],
									mem[{r_addr_in,3'b010}],
									mem[{r_addr_in,3'b001}],
									mem[{r_addr_in,3'b000}]};	
				valid_bits_out <=  {	valid_bit_sets[{r_addr_in}]};	
			
			end

		end
	end	
	

    // assign r_data_out = {	mem[{r_addr_in,3'b111}],
                            // mem[{r_addr_in,3'b110}],
                            // mem[{r_addr_in,3'b101}],
                            // mem[{r_addr_in,3'b100}],
                            // mem[{r_addr_in,3'b011}],
                            // mem[{r_addr_in,3'b010}],
                            // mem[{r_addr_in,3'b001}],
                            // mem[{r_addr_in,3'b000}]};
                            
    // always@(posedge clk) begin
    	// case(r_addr_in)
    		// 4'd0 : begin valid_bits_out <= valid_bits[{4'd0 ,3'b111}:{4'd0 ,3'b000}];  end 
    		// 4'd1 : begin valid_bits_out <= valid_bits[{4'd1 ,3'b111}:{4'd1 ,3'b000}];  end
    		// 4'd2 : begin valid_bits_out <= valid_bits[{4'd2 ,3'b111}:{4'd2 ,3'b000}];  end
    		// 4'd3 : begin valid_bits_out <= valid_bits[{4'd3 ,3'b111}:{4'd3 ,3'b000}];  end
    		// 4'd4 : begin valid_bits_out <= valid_bits[{4'd4 ,3'b111}:{4'd4 ,3'b000}];  end
    		// 4'd5 : begin valid_bits_out <= valid_bits[{4'd5 ,3'b111}:{4'd5 ,3'b000}];  end
    		// 4'd6 : begin valid_bits_out <= valid_bits[{4'd6 ,3'b111}:{4'd6 ,3'b000}];  end
    		// 4'd7 : begin valid_bits_out <= valid_bits[{4'd7 ,3'b111}:{4'd7 ,3'b000}];  end
    		// 4'd8 : begin valid_bits_out <= valid_bits[{4'd8 ,3'b111}:{4'd8 ,3'b000}];  end
    		// 4'd9 : begin valid_bits_out <= valid_bits[{4'd9 ,3'b111}:{4'd9 ,3'b000}];  end
    		// 4'd10: begin valid_bits_out <= valid_bits[{4'd10,3'b111}:{4'd10,3'b000}];  end
    		// 4'd11: begin valid_bits_out <= valid_bits[{4'd11,3'b111}:{4'd11,3'b000}];  end
    		// 4'd12: begin valid_bits_out <= valid_bits[{4'd12,3'b111}:{4'd12,3'b000}];  end
    		// 4'd13: begin valid_bits_out <= valid_bits[{4'd13,3'b111}:{4'd13,3'b000}];  end
    		// 4'd14: begin valid_bits_out <= valid_bits[{4'd14,3'b111}:{4'd14,3'b000}];  end
    		// 4'd15: begin valid_bits_out <= valid_bits[{4'd15,3'b111}:{4'd15,3'b000}];  end
    	// endcase
        
    // end
						
endmodule