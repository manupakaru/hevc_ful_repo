`timescale 1ns / 1ps
module inter_cache_pipe_hit_pipe
(
    clk,
    reset,
	
	output_fifo_full, 
	output_fifo_almost_full, 
	output_fifo_almost_full_only,
	output_fifo_program_full,
	block_ready_internal,
	dest_fill_x_loc_luma_output,
	dest_fill_y_loc_luma_output,
	dest_fill_x_mask_luma_output,
	dest_fill_y_mask_luma_output,
	
	dest_fill_x_loc_chma_output,
	dest_fill_y_loc_chma_output,
	dest_fill_x_mask_chma_output,
	dest_fill_y_mask_chma_output,
	cache_luma_out,
	cache_cb_out,
	cache_cr_out,
	
    luma_ref_start_x_in 	,
	luma_ref_start_y_in    ,
    chma_ref_start_x_in 	,
    chma_ref_start_y_in 	,
    
    res_present_in,
    res_present_out,

	
    bi_pred_block_cache_in,
    bi_pred_block_cache_out,
    x0_tu_end_in_min_tus_in,
    x0_tu_end_in_min_tus_out,
    y0_tu_end_in_min_tus_in,
    y0_tu_end_in_min_tus_out,

    luma_ref_width_x_in   ,
    chma_ref_width_x_in   ,
    luma_ref_height_y_in  ,
    chma_ref_height_y_in  ,

    luma_ref_start_x_out     , 
    luma_ref_start_y_out    ,
    chma_ref_start_x_out     ,
    chma_ref_start_y_out     ,
    
    luma_ref_width_x_out   ,
    chma_ref_width_x_out   ,
    luma_ref_height_y_out  ,
    chma_ref_height_y_out  ,

    cache_idle_out,
	
    block_x_offset_luma ,
    block_y_offset_luma ,
    block_x_offset_chma ,
    block_y_offset_chma ,
    block_x_end_luma    ,
    block_y_end_luma    ,
    block_x_end_chma    ,
    block_y_end_chma    ,




    pic_width,
    pic_height,

    ch_frac_x   ,
    ch_frac_y   ,
    ch_frac_x_out,
    ch_frac_y_out,

    xT_in_min_tus_in,
    yT_in_min_tus_in,
    xT_in_min_tus_out,
    yT_in_min_tus_out,

    filer_idle_in,
	ref_idx_in_in,
    valid_in,
    // block_121_out,
    // block_25cb_out,
    // block_25cr_out,
    block_ready,
    
    ref_pix_axi_ar_addr,
    ref_pix_axi_ar_len,
    ref_pix_axi_ar_size,
    ref_pix_axi_ar_burst,
    ref_pix_axi_ar_prot,
    ref_pix_axi_ar_valid,
    ref_pix_axi_ar_ready,
    ref_pix_axi_r_data,
    ref_pix_axi_r_resp,
    ref_pix_axi_r_last,
    ref_pix_axi_r_valid,
    ref_pix_axi_r_ready
	
	,miss_elem_fifo_empty_out

	,test_cache_addr
	,test_cache_luma_data
	,test_cache_en
	,test_ref_block_en
	,test_yT_in_min_luma_cache
	,test_xT_in_min_luma_cache
	,test_ref_luma_data_4x4
	,rf_blk_hgt_in
	,rf_blk_wdt_in
	,rf_blk_hgt_ch
	,rf_blk_wdt_ch	
		
	,start_great_x_in	
	,start_great_y_in
	,rf_blk_great_hgt_in
	,rf_blk_great_wdt_in   
	
	,start_x_in
	,start_y_in
	,start_x_ch
	,start_y_ch
	
	
	

);

    `include "../sim/cache_configs_def.v"
    `include "../sim/pred_def.v"
    `include "../sim/inter_axi_def.v" // always define below pred_def

    //---------------------------------------------------------------------------------------------------------------------
    // parameter definitions
    //---------------------------------------------------------------------------------------------------------------------
    
    parameter                           LUMA_DIM_WDTH		    	= 4;        // out block dimension  max 11
    parameter                           LUMA_DIM_ADDR_WDTH          = 7;        //max 121
    parameter                           CHMA_DIM_WDTH               = 3;        // max 5 
    parameter                           CHMA_DIM_ADDR_WDTH          = 5;        // max 25 
    parameter                           LUMA_REF_BLOCK_WIDTH        = 4'd11;
    parameter                           CHMA_REF_BLOCK_WIDTH        = 3'd5;
	
	parameter 							BLOCK_NUMBER_WIDTH 			= 6; // Since 32 elements can be occupied in hit fifo, if all of them come from single cache line block 6 bits needed to uniquely identify a block

    parameter                           CACHE_LINE_LUMA_OFFSET      = 0;
    parameter                           CACHE_LINE_CB_OFFSET      = CACHE_LINE_WDTH * LUMA_BITS;
    parameter                           CACHE_LINE_CR_OFFSET      = CACHE_LINE_CB_OFFSET + ((CACHE_LINE_WDTH * LUMA_BITS)>>2);

    parameter                           REF_PIX_AXI_AX_SIZE  = `AX_SIZE_64;
    parameter                           REF_PIX_AXI_AX_LEN   = `AX_LEN_1;

    
    //---------------------------------------------------------------------------------------------------------------------
    // localparam definitions
    //---------------------------------------------------------------------------------------------------------------------
    
    // localparam                          STATE_SET_INPUTS                = 0;    // the state that is entered upon reset
	// localparam                          STATE_TAG_READ                  = 1;
    // localparam                          STATE_TAG_COMPARE               = 2;
    // localparam                          STATE_C_LINE_HIT_FETCH          = 3;
    // localparam                          STATE_C_LINE_MISS_FETCH         = 4;
    // localparam                          STATE_DEST_FILL_HIT             = 5;
    // localparam                          STATE_CACHE_READY               = 6;
    // localparam                          STATE_MISS_AR_REDY_WAIT         = 7;
    // localparam                          STATE_DEST_FILL_MIS		        = 8;

		localparam							STATE_IDLE	 			= 0;
		localparam 							STATE_ACTIVE 			= 1;
		localparam							STATE_PASS_ONLY	 		= 2;
		localparam							STATE_READY_WAIT	 	= 3;
		localparam							STATE_READY_WAIT_ONLY	= 4;
		localparam							STATE_MISS_FULL_WAIT  = 5;
		localparam							STATE_READY_AND_FUL_WAIT  = 6;

    //---------------------------------------------------------------------------------------------------------------------
    // I/O signals
    //---------------------------------------------------------------------------------------------------------------------
    
    
    input                                           clk;
    input                                           reset;
                
    // inter prediction filter interface            
    input                                           valid_in;
    // output	reg							            block_ready;   // assuming cache_block_ready is single cylce
    output								            block_ready;   // assuming cache_block_ready is single cylce
	// output	reg										cache_idle_out;
	output										cache_idle_out;
    input                                           filer_idle_in;

    input res_present_in;
    output reg res_present_out;
	


	

    input bi_pred_block_cache_in;
    output reg bi_pred_block_cache_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      x0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      y0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus_in;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus_out;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus_out;

	
	input  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] 	luma_ref_start_x_in;	
	input  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] 	luma_ref_start_y_in;
	input  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] 	chma_ref_start_x_in;	
	input  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] 	chma_ref_start_y_in;	
	
	input  [LUMA_DIM_WDTH - 1:0]   			chma_ref_width_x_in            ;	
    input  [LUMA_DIM_WDTH - 1:0]            chma_ref_height_y_in           ;   
	input  [LUMA_DIM_WDTH - 1:0]   			luma_ref_width_x_in            ;	
    input  [LUMA_DIM_WDTH - 1:0]            luma_ref_height_y_in           ;   


    output reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  luma_ref_start_x_out   ;
    output reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  luma_ref_start_y_out   ;
    output reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  chma_ref_start_x_out   ;
    output reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  chma_ref_start_y_out   ;
                    
    output reg  [LUMA_DIM_WDTH - 1:0]                   chma_ref_width_x_out   ;
    output reg  [LUMA_DIM_WDTH - 1:0]                   chma_ref_height_y_out  ;
    output reg  [LUMA_DIM_WDTH - 1:0]                   luma_ref_width_x_out   ;
    output reg  [LUMA_DIM_WDTH - 1:0]                   luma_ref_height_y_out  ;


    input  [MV_C_FRAC_WIDTH_HIGH -1:0]      ch_frac_x;
    input  [MV_C_FRAC_WIDTH_HIGH -1:0]      ch_frac_y;
    output  reg [MV_C_FRAC_WIDTH_HIGH -1:0]      ch_frac_x_out;
    output  reg [MV_C_FRAC_WIDTH_HIGH -1:0]      ch_frac_y_out;

	input			[REF_ADDR_WDTH-1:0]		            ref_idx_in_in;
	
    input  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   pic_width;   
    input  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   pic_height;   
	
	
    // output [LUMA_BITS* LUMA_REF_BLOCK_WIDTH* LUMA_REF_BLOCK_WIDTH -1:0]     block_121_out;
    // output [CHMA_BITS* CHMA_REF_BLOCK_WIDTH* CHMA_REF_BLOCK_WIDTH -1:0]     block_25cb_out;
    // output [CHMA_BITS* CHMA_REF_BLOCK_WIDTH* CHMA_REF_BLOCK_WIDTH -1:0]     block_25cr_out;



               
    // axi master interface         
    output	 	[AXI_ADDR_WDTH-1:0]		            ref_pix_axi_ar_addr;
    reg 		[AXI_ADDR_WDTH-1:0]		            ref_pix_axi_ar_addr_fifo_in;
    output wire [7:0]					            ref_pix_axi_ar_len;
    output wire	[2:0]					            ref_pix_axi_ar_size;
    output wire [1:0]					            ref_pix_axi_ar_burst;
    output wire [2:0]					            ref_pix_axi_ar_prot;
    output 							            	ref_pix_axi_ar_valid;
    reg							            		ref_pix_axi_ar_valid_fifo_in;
    wire							            	ref_pix_axi_ar_fifo_empty;
    wire							            	ref_pix_axi_ar_fifo_full;
    wire							            	ref_pix_axi_ar_fifo_rd_en;
    input 								            ref_pix_axi_ar_ready;
                
    input		[AXI_CACHE_DATA_WDTH-1:0]		    ref_pix_axi_r_data;

    input		[1:0]					            ref_pix_axi_r_resp;
    input 								            ref_pix_axi_r_last;
    input								            ref_pix_axi_r_valid;
    output reg 							            ref_pix_axi_r_ready;
		
    output reg        [LUMA_DIM_WDTH-1:0]                 block_x_offset_luma;
    output reg        [LUMA_DIM_WDTH-1:0]                 block_y_offset_luma;
    output reg        [CHMA_DIM_WDTH-1:0]                 block_x_offset_chma;
    output reg        [CHMA_DIM_WDTH-1:0]                 block_y_offset_chma; 

    output reg         [LUMA_DIM_WDTH-1:0]                 block_x_end_luma;
    output reg         [LUMA_DIM_WDTH-1:0]                 block_y_end_luma;
    output reg         [CHMA_DIM_WDTH-1:0]                 block_x_end_chma;
    output reg         [CHMA_DIM_WDTH-1:0]                 block_y_end_chma; 
	
	output miss_elem_fifo_empty_out;
	output [7-1:0] test_cache_addr;
	output  test_cache_en;
	output [512-1:0] test_cache_luma_data;	
	output  test_ref_block_en;	
	output [9-1:0] test_yT_in_min_luma_cache;	
	output [9-1:0] test_xT_in_min_luma_cache;	
	output [128-1:0] test_ref_luma_data_4x4;	
	wire [224-1:0] test_ref_luma_data_11x4;	
	wire [224-1:0] test_ref_luma_data_11x8;	
	wire [224-1:0] test_ref_luma_data_11x11;	
	wire [224-1:0] test_ref_luma_data_4x11;	
	
    integer next_state, state;
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   luma_ref_end_x = luma_ref_start_x_in + luma_ref_width_x_in  ;   
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   luma_ref_end_y = luma_ref_start_y_in + luma_ref_height_y_in ;
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   chma_ref_end_x = chma_ref_start_x_in + chma_ref_width_x_in;   
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   chma_ref_end_y = chma_ref_start_y_in + chma_ref_height_y_in ;		
		
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   luma_start_x_rel_pic_width =  pic_width  - 1'b1 -      luma_ref_start_x_in;   
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   luma_start_y_rel_pic_height = pic_height - 1'b1 -      luma_ref_start_y_in;
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   chma_start_x_rel_pic_width =  pic_width  - 1'b1 -     chma_ref_start_x_in;  
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   chma_start_y_rel_pic_height = pic_height - 1'b1 -     chma_ref_start_y_in;      

    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   luma_start_x_rel_0 =  0  -        luma_ref_start_x_in;   
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   luma_start_y_rel_0 = 0   -        luma_ref_start_y_in;
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   chma_start_x_rel_0 =  0  -       chma_ref_start_x_in;  
    wire  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]   chma_start_y_rel_0 = 0   -       chma_ref_start_y_in;      
  

    reg         [REF_ADDR_WDTH-1:0]                 ref_idx_in;
     


    wire    [1:0]                                   delta_x;    // possible 0,1,2
    wire    [1:0]                                   delta_y;    //possible 0,1,2,3
	

    reg    [1:0]                                    delta_x_2;    // possible 0,1,2
    reg    [1:0]                                    delta_y_2;    //possible 0,1,2,3
	
    reg    [1:0]                                    delta_x_3;    // possible 0,1,2
    reg    [1:0]                                    delta_y_3;    //possible 0,1,2,3

    reg    [1:0]                                    delta_x_4;    // possible 0,1,2
    reg    [1:0]                                    delta_y_4;    //possible 0,1,2,3	
    wire    [1:0]                                   delta_x_luma;    // possible 0,1,2
    wire    [1:0]                                   delta_y_luma;    //possible 0,1,2,3   
    wire                                            delta_x_chma; // possible 0,1,2
    wire    [1:0]                                   delta_y_chma; //possible 0,1,2,3

    input         [LUMA_DIM_WDTH-1:0]                 rf_blk_hgt_in;
    input         [LUMA_DIM_WDTH-1:0]                 rf_blk_wdt_in;
    input         [CHMA_DIM_WDTH-1:0]                 rf_blk_hgt_ch;
    input         [CHMA_DIM_WDTH-1:0]                 rf_blk_wdt_ch;  
	
    input       [X_ADDR_WDTH-1:0]                     start_great_x_in;
    input       [Y_ADDR_WDTH-1:0]                     start_great_y_in;
    input       [LUMA_DIM_WDTH-1:0]                   rf_blk_great_hgt_in;
    input       [LUMA_DIM_WDTH-1:0]                   rf_blk_great_wdt_in;    
    
    input       [X_ADDR_WDTH-1:0]                     start_x_in;
    input       [Y_ADDR_WDTH-1:0]                     start_y_in;
    input       [X_ADDR_WDTH-2:0]                     start_x_ch; //% value after division by two
    input       [Y_ADDR_WDTH-2:0]                     start_y_ch;

    reg     [1:0]                                   curr_x; // possible 0,1,2
    reg     [1:0]                                   curr_y; // possible 0,1,2,3

    reg     [1:0]                                   curr_x_d; // possible 0,1,2
    reg     [1:0]                                   curr_y_d; // possible 0,1,2,3
	
    reg     [1:0]                                   curr_x_2d; // possible 0,1,2
    reg     [1:0]                                   curr_y_2d; // possible 0,1,2,3
	
    reg     [1:0]                                   curr_x_3d; // possible 0,1,2
    reg     [1:0]                                   curr_y_3d; // possible 0,1,2,3
	
	wire		[1:0] 		 curr_x_read;
	wire		[1:0] 		 curr_y_read;
	wire		[1:0] 		delta_x_read;
	wire		[1:0] 		delta_y_read;

	wire		[1:0] 		 curr_x_hit;
	wire		[1:0] 		 curr_y_hit;
	wire		[1:0] 		delta_x_hit;
	wire		[1:0] 		delta_y_hit;
	
    reg     [1:0]                                   curr_x_luma; // possible 0,1,2
    reg     [1:0]                                   curr_y_luma; // possible 0,1,2,3

    reg     [1:0]                                   curr_x_chma; // possible 0,1,2
    reg     [1:0]                                   curr_y_chma; // possible 0,1,2,3

    reg     [SET_ADDR_WDTH -1:0]                     set_addr;
    reg     [SET_ADDR_WDTH -1:0]                     set_addr_d;
    reg     [SET_ADDR_WDTH -1:0]                     set_addr_2d;
    reg     [SET_ADDR_WDTH -1:0]                     set_addr_3d;
    wire    [SET_ADDR_WDTH -1:0]                     set_addr_read;
    wire    [SET_ADDR_WDTH -1:0]                     set_addr_hit;



    wire    [X_ADDR_WDTH - C_L_H_SIZE -1: 0]          curr_x_addr;
    wire    [Y_ADDR_WDTH - C_L_V_SIZE -1: 0]          curr_y_addr;

    reg    [X_ADDR_WDTH - C_L_H_SIZE -1: 0]          curr_x_addr_reg;
    reg    [Y_ADDR_WDTH - C_L_V_SIZE -1: 0]          curr_y_addr_reg;
    reg    [X_ADDR_WDTH - C_L_H_SIZE -1: 0]          curr_x_addr_reg_1;
    reg    [Y_ADDR_WDTH - C_L_V_SIZE -1: 0]          curr_y_addr_reg_1;
    reg     [TAG_ADDR_WDTH-1:0]                     tag_addr;
    reg     [TAG_ADDR_WDTH-1:0]                     tag_addr_d;
    reg     [TAG_ADDR_WDTH-1:0]                     tag_addr_2d;
	reg 											tag_mem_wr_en;

    wire    [(1<<C_N_WAY)-1:0]                      set_vld_bits;
    wire    [(1<<C_N_WAY)*TAG_ADDR_WDTH-1:0]        tag_rdata_set;
    wire                                            is_hit;
    reg                                             is_hit_d;
    wire    [C_N_WAY-1:0]                           set_idx;
    reg     [C_N_WAY-1:0]                           set_idx_d;
    reg     [C_N_WAY-1:0]                           set_idx_2d;
    wire    [C_N_WAY-1:0]                           set_idx_miss;
    wire    [C_N_WAY-1:0]                           set_idx_miss_read;
    wire    [C_N_WAY-1:0]                           set_idx_hit;
	

    reg     [SET_ADDR_WDTH+C_N_WAY-1:0]             cache_w_addr;
    reg     [SET_ADDR_WDTH+C_N_WAY-1:0]             cache_r_addr;
    reg     [SET_ADDR_WDTH+C_N_WAY-1:0]             tag_write_addr;
    wire    [PIXEL_BITS*CACHE_LINE_WDTH-1:0]        cache_rdata;
    reg                                             cache_wr_en;

    wire    [(1<<C_N_WAY)*C_N_WAY-1:0]              age_val_set;
    wire    [(1<<C_N_WAY)*C_N_WAY-1:0]              new_age_set;

    reg      luma_dest_enable_wire;
    reg      luma_dest_enable_wire_next;
    reg      luma_dest_enable_reg;
    reg      luma_dest_enable_reg_d;
    wire     luma_dest_enable_reg_read;
    wire     luma_dest_enable_reg_hit;
    reg      chma_dest_enable_wire;
    reg      chma_dest_enable_wire_next;
    reg      chma_dest_enable_reg;
    reg      chma_dest_enable_reg_d;
    wire     chma_dest_enable_reg_read;
    wire     chma_dest_enable_reg_hit;

    reg     [1:0]                                   next_curr_x_luma; // possible 0,1,2
    reg     [1:0]                                   next_curr_y_luma; // possible 0,1,2,3

    reg     [1:0]                                   next_curr_x_chma; // possible 0,1,2
    reg     [1:0]                                   next_curr_y_chma; // possible 0,1,2,3

    reg     [1:0]                                   next_curr_x; // possible 0,1,2
    reg     [1:0]                                   next_curr_y; // possible 0,1,2,3
	
    reg     [1:0]                                   next_curr_x_3d; // possible 0,1,2
    reg     [1:0]                                   next_curr_y_3d; // possible 0,1,2,3
	
    integer i,j;          
    
    wire    [C_L_H_SIZE-1:0]                        cl_strt_x_luma;
    reg     [C_L_H_SIZE-1:0]                        cl_strt_x_luma_d;
    wire    [C_L_H_SIZE_C-1:0]                      cl_strt_x_chma;
    reg     [C_L_H_SIZE_C-1:0]                      cl_strt_x_chma_d;
    wire    [C_L_V_SIZE-1:0]                        cl_strt_y_luma;
    reg     [C_L_V_SIZE-1:0]                        cl_strt_y_luma_d;
    wire    [C_L_V_SIZE_C-1:0]                      cl_strt_y_chma;
    reg     [C_L_V_SIZE_C-1:0]                      cl_strt_y_chma_d;
    
    //wire    [C_L_H_SIZE-1:0]                        cl_end__x_luma;    
    //reg     [C_L_H_SIZE-1:0]                        cl_end__x_luma_d;    
    //wire    [C_L_H_SIZE_C-1:0]                      cl_end__x_chma;
    //reg     [C_L_H_SIZE_C-1:0]                      cl_end__x_chma_d;

    //wire    [C_L_H_SIZE-1:0]                        cl_end__y_luma;    
    //reg     [C_L_H_SIZE-1:0]                        cl_end__y_luma_d;    
    //wire    [C_L_H_SIZE_C-1:0]                      cl_end__y_chma;
    //reg     [C_L_H_SIZE_C-1:0]                      cl_end__y_chma_d;

    reg     [LUMA_DIM_WDTH-1:0] next_dst_strt_x_luma;
    reg     [LUMA_DIM_WDTH-1:0] next_dest_end_x_luma;
    reg     [LUMA_DIM_WDTH-1:0] next_dst_strt_y_luma;
    reg     [LUMA_DIM_WDTH-1:0] next_dest_end_y_luma;


    reg     [LUMA_DIM_WDTH-1:0] dst_strt_x_luma;
    reg     [LUMA_DIM_WDTH-1:0] dest_end_x_luma;
    reg     [LUMA_DIM_WDTH-1:0] dst_strt_y_luma;
    reg     [LUMA_DIM_WDTH-1:0] dest_end_y_luma;


    reg     [CHMA_DIM_WDTH-1:0] next_dst_strt_x_chma;
    reg     [CHMA_DIM_WDTH-1:0] next_dest_end_x_chma;
    reg     [CHMA_DIM_WDTH-1:0] next_dst_strt_y_chma;
    reg     [CHMA_DIM_WDTH-1:0] next_dest_end_y_chma;


    reg     [CHMA_DIM_WDTH-1:0] dst_strt_x_chma;
    reg     [CHMA_DIM_WDTH-1:0] dest_end_x_chma;
    reg     [CHMA_DIM_WDTH-1:0] dst_strt_y_chma;
    reg     [CHMA_DIM_WDTH-1:0] dest_end_y_chma;

    reg   [C_L_H_SIZE -1:0] 						dest_fill_x_loc_luma[LUMA_REF_BLOCK_WIDTH -1:0];
    // reg   [C_L_H_SIZE -1:0] 						dest_fill_x_loc_luma_d[LUMA_REF_BLOCK_WIDTH -1:0];
    // wire  [C_L_H_SIZE -1:0] 						dest_fill_x_loc_luma_read_arry[LUMA_REF_BLOCK_WIDTH -1:0];
    // wire  [C_L_H_SIZE -1:0] 						dest_fill_x_loc_luma_hit_arry[LUMA_REF_BLOCK_WIDTH -1:0];
    wire  [C_L_H_SIZE *LUMA_REF_BLOCK_WIDTH -1:0] 	dest_fill_x_loc_luma_read;
    wire  [C_L_H_SIZE *LUMA_REF_BLOCK_WIDTH -1:0] 	dest_fill_x_loc_luma_hit;
    reg  [C_L_H_SIZE *LUMA_REF_BLOCK_WIDTH -1:0] 	dest_fill_x_loc_luma_hit_d;
    output reg  [C_L_H_SIZE *LUMA_REF_BLOCK_WIDTH -1:0] 	dest_fill_x_loc_luma_output;
    wire  [C_L_H_SIZE *LUMA_REF_BLOCK_WIDTH -1:0] 	dest_fill_x_loc_luma_write;
	
	
    reg   [C_L_V_SIZE -1:0] 						dest_fill_y_loc_luma[LUMA_REF_BLOCK_WIDTH -1:0];
    // reg   [C_L_V_SIZE -1:0] 						dest_fill_y_loc_luma_d[LUMA_REF_BLOCK_WIDTH -1:0];
    // wire  [C_L_V_SIZE -1:0] 						dest_fill_y_loc_luma_read_arry[LUMA_REF_BLOCK_WIDTH -1:0];
    // wire  [C_L_V_SIZE -1:0] 						dest_fill_y_loc_luma_hit_arry[LUMA_REF_BLOCK_WIDTH -1:0];
    wire  [C_L_V_SIZE *LUMA_REF_BLOCK_WIDTH -1:0] 	dest_fill_y_loc_luma_read;
    wire  [C_L_V_SIZE *LUMA_REF_BLOCK_WIDTH -1:0] 	dest_fill_y_loc_luma_hit;
    reg  [C_L_V_SIZE *LUMA_REF_BLOCK_WIDTH -1:0] 	dest_fill_y_loc_luma_hit_d;
    output  reg [C_L_V_SIZE *LUMA_REF_BLOCK_WIDTH -1:0] 	dest_fill_y_loc_luma_output;
    wire  [C_L_V_SIZE *LUMA_REF_BLOCK_WIDTH -1:0] 	dest_fill_y_loc_luma_write;
    reg   [C_L_V_SIZE + C_L_H_SIZE-1:0] 			dest_fill_xy_loc_luma_d[0:LUMA_REF_BLOCK_WIDTH -1][0:LUMA_REF_BLOCK_WIDTH -1];


	
    reg   [LUMA_REF_BLOCK_WIDTH -1:0]                  dest_fill_x_mask_luma;
    reg   [LUMA_REF_BLOCK_WIDTH -1:0]                  dest_fill_x_mask_luma_d;
    output reg   [LUMA_REF_BLOCK_WIDTH -1:0]                  dest_fill_x_mask_luma_output;
    wire  [LUMA_REF_BLOCK_WIDTH -1:0]                  dest_fill_x_mask_luma_read;
    wire  [LUMA_REF_BLOCK_WIDTH -1:0]                  dest_fill_x_mask_luma_hit;
    reg   [LUMA_REF_BLOCK_WIDTH -1:0]                  dest_fill_y_mask_luma;
    reg   [LUMA_REF_BLOCK_WIDTH -1:0]                  dest_fill_y_mask_luma_d;
    output reg   [LUMA_REF_BLOCK_WIDTH -1:0]                  dest_fill_y_mask_luma_output;
    wire  [LUMA_REF_BLOCK_WIDTH -1:0]                  dest_fill_y_mask_luma_read;
    wire  [LUMA_REF_BLOCK_WIDTH -1:0]                  dest_fill_y_mask_luma_hit;

    reg   [C_L_H_SIZE_C -1:0] 								dest_fill_x_loc_chma[CHMA_REF_BLOCK_WIDTH -1:0];
    // reg   [C_L_H_SIZE_C -1:0] 								dest_fill_x_loc_chma_d[CHMA_REF_BLOCK_WIDTH -1:0];
    wire  [C_L_H_SIZE_C * CHMA_REF_BLOCK_WIDTH -1:0] 		dest_fill_x_loc_chma_write;
    wire  [C_L_H_SIZE_C * CHMA_REF_BLOCK_WIDTH -1:0] 		dest_fill_x_loc_chma_read;
    wire  [C_L_H_SIZE_C * CHMA_REF_BLOCK_WIDTH -1:0] 		dest_fill_x_loc_chma_hit;
    reg  [C_L_H_SIZE_C * CHMA_REF_BLOCK_WIDTH -1:0] 		dest_fill_x_loc_chma_hit_d;
    output reg  [C_L_H_SIZE_C * CHMA_REF_BLOCK_WIDTH -1:0] 		dest_fill_x_loc_chma_output;
    // wire  [C_L_H_SIZE_C -1:0] 								dest_fill_x_loc_chma_read_arry[CHMA_REF_BLOCK_WIDTH -1:0];
    // wire  [C_L_H_SIZE_C -1:0] 								dest_fill_x_loc_chma_hit_arry[CHMA_REF_BLOCK_WIDTH -1:0];
	
	
    reg   [C_L_V_SIZE_C -1:0] 								dest_fill_y_loc_chma[CHMA_REF_BLOCK_WIDTH -1:0];
    reg   [C_L_V_SIZE_C -1:0] 								dest_fill_y_loc_chma_d[CHMA_REF_BLOCK_WIDTH -1:0];
    wire  [C_L_V_SIZE_C -1:0] 								dest_fill_y_loc_chma_read_arry[CHMA_REF_BLOCK_WIDTH -1:0];
    wire  [C_L_V_SIZE_C -1:0] 								dest_fill_y_loc_chma_hit_arry[CHMA_REF_BLOCK_WIDTH -1:0];
    wire  [C_L_V_SIZE_C *CHMA_REF_BLOCK_WIDTH -1:0] 		dest_fill_y_loc_chma_read;
    wire  [C_L_V_SIZE_C *CHMA_REF_BLOCK_WIDTH -1:0] 		dest_fill_y_loc_chma_hit;
    reg  [C_L_V_SIZE_C *CHMA_REF_BLOCK_WIDTH -1:0] 		dest_fill_y_loc_chma_hit_d;
    output reg  [C_L_V_SIZE_C *CHMA_REF_BLOCK_WIDTH -1:0] 		dest_fill_y_loc_chma_output;
    wire  [C_L_V_SIZE_C *CHMA_REF_BLOCK_WIDTH -1:0] 		dest_fill_y_loc_chma_write;
    reg   [C_L_V_SIZE_C + C_L_H_SIZE_C-1:0] 					dest_fill_xy_loc_chma_d[0:CHMA_REF_BLOCK_WIDTH -1][0:CHMA_REF_BLOCK_WIDTH -1];
	

    wire     [CHMA_REF_BLOCK_WIDTH -1:0]                 	dest_fill_x_mask_chma_read	;
    wire     [CHMA_REF_BLOCK_WIDTH -1:0]                 	dest_fill_x_mask_chma_hit	;
    reg      [CHMA_REF_BLOCK_WIDTH -1:0]                 	dest_fill_x_mask_chma		;
    reg      [CHMA_REF_BLOCK_WIDTH -1:0]                 	dest_fill_x_mask_chma_d		;
    output reg      [CHMA_REF_BLOCK_WIDTH -1:0]                 	dest_fill_x_mask_chma_output		;
    wire     [CHMA_REF_BLOCK_WIDTH -1:0]                 	dest_fill_y_mask_chma_read	;
    wire     [CHMA_REF_BLOCK_WIDTH -1:0]                 	dest_fill_y_mask_chma_hit	;
    reg      [CHMA_REF_BLOCK_WIDTH -1:0]                 	dest_fill_y_mask_chma		;
    reg      [CHMA_REF_BLOCK_WIDTH -1:0]                 	dest_fill_y_mask_chma_d		;
    output reg      [CHMA_REF_BLOCK_WIDTH -1:0]                 	dest_fill_y_mask_chma_output		;

    // reg  [LUMA_BITS-1:0]     block_11x11 [0:LUMA_REF_BLOCK_WIDTH -1][0:LUMA_REF_BLOCK_WIDTH -1];
    
    // reg  [CHMA_BITS-1:0]     block_5x5_cb[0:CHMA_REF_BLOCK_WIDTH -1][0:CHMA_REF_BLOCK_WIDTH -1];
    // reg  [CHMA_BITS-1:0]     block_5x5_cr[0:CHMA_REF_BLOCK_WIDTH -1][0:CHMA_REF_BLOCK_WIDTH -1];

    reg age_wr_en;


    wire [CTB_SIZE_WIDTH - C_L_V_SIZE-1:0] 		bu_idx 		;
    wire [CTB_SIZE_WIDTH - C_L_H_SIZE-1:0] 		bu_row_idx 	;
    wire [X_ADDR_WDTH -CTB_SIZE_WIDTH-1:0]  	iu_idx 		= {curr_x_addr_reg[X_ADDR_WDTH - C_L_H_SIZE -1:CTB_SIZE_WIDTH - C_L_H_SIZE]};
    wire [Y_ADDR_WDTH -CTB_SIZE_WIDTH-1:0]  	iu_row_idx 	= {curr_y_addr_reg[Y_ADDR_WDTH - C_L_V_SIZE -1:CTB_SIZE_WIDTH - C_L_V_SIZE]};

    reg [15: 0]  bu_idx_val;
    reg [20: 0]  iu_idx_val;
    reg [25: 0]  iu_idx_row_val;
    reg [31: 0]  ref_idx_val;


    
    reg luma_dest_enable_x,luma_dest_enable_y;
    reg luma_dest_enable_x_next,luma_dest_enable_y_next;
    reg chma_dest_enable_x_next,chma_dest_enable_y_next;   
    reg chma_dest_enable_x,chma_dest_enable_y;   

    reg cur_xy_changed_luma, cur_xy_changed_chma;
    
    // wire [LUMA_BITS-1:0] cache_w_data_arr [(PIXEL_BITS*CACHE_LINE_WDTH/LUMA_BITS)-1:0];
    // wire [LUMA_BITS-1:0] cache_w_data_arr_luma [CACHE_LINE_WDTH-1:0];
    // wire [LUMA_BITS-1:0] cache_w_data_arr_cb [CACHE_LINE_WDTH/4-1:0];
    // wire [LUMA_BITS-1:0] cache_w_data_arr_cr [CACHE_LINE_WDTH/4-1:0];
	
    // wire [LUMA_BITS-1:0] cache_rdata_arr    [(PIXEL_BITS*CACHE_LINE_WDTH/LUMA_BITS)-1:0];
    // wire [LUMA_BITS-1:0] cache_rdata_arr_luma [CACHE_LINE_WDTH-1:0];
    output reg [CACHE_LINE_WDTH*LUMA_BITS-1:0] cache_luma_out;
    wire [CACHE_LINE_WDTH*LUMA_BITS-1:0] cache_rdata_luma;
    wire [CACHE_LINE_WDTH*LUMA_BITS-1:0] cache_wdata_luma;
    // wire [LUMA_BITS-1:0] cache_rdata_arr_cb [CACHE_LINE_WDTH/4-1:0];
    output reg [CACHE_LINE_WDTH*LUMA_BITS-1:0]  cache_cb_out;
	wire [CACHE_LINE_WDTH*LUMA_BITS-1:0] cache_wdata_cb;
    wire [CACHE_LINE_WDTH*LUMA_BITS-1:0] cache_rdata_cb;
    // wire [LUMA_BITS-1:0] cache_rdata_arr_cr [CACHE_LINE_WDTH/4-1:0];
	output reg [CACHE_LINE_WDTH*LUMA_BITS-1:0]  cache_cr_out;
    wire [CACHE_LINE_WDTH*LUMA_BITS-1:0] cache_rdata_cr;
    wire [CACHE_LINE_WDTH*LUMA_BITS-1:0] cache_wdata_cr;
	
	wire		[CACHE_LINE_WDTH*PIXEL_BITS -1:0]		    cache_w_port;
	reg 		[CACHE_LINE_WDTH*PIXEL_BITS/2 -1:0]		    cache_w_port_old_reg;

	wire miss_elem_fifo_empty;
	wire  hit_elem_fifo_empty;
	// wire d_miss_elem_fifo_empty;
	// wire  d_hit_elem_fifo_empty;
	wire miss_elem_fifo_full;
	wire  hit_elem_fifo_full;
	reg  miss_elem_fifo_wr_en;
	reg   hit_elem_fifo_wr_en;
	reg hit_elem_fifo_wr_en_d;
	
	reg set_input_stage_valid;
	reg [1:0] state_set_input;
	reg set_input_stage_ready;
	
	reg dest_enable_wire_valid;
	reg [1:0] state_tag_read;
	reg [1:0] state_tag_read_d;
	reg tag_read_stage_ready;
	reg tag_compare_stage_valid;
	

	reg [3:0] state_tag_compare;
	reg [3:0] state_tag_compare_d;
	reg tag_compare_stage_ready;

	// reg  miss_idx_fifo_wr_en;
	// wire miss_idx_fifo_empty;
	// wire miss_idx_fifo_full;	
	reg miss_elem_fifo_rd_en;	
	reg  hit_elem_fifo_rd_en;	
	
	reg data_read_stage_valid;
	// reg data_read_stage_ready;
	reg [2:0] state_data_read;
	// reg [2:0] state_data_read_d;
	

	reg last_block_valid_0d;
	reg last_block_valid_1d;
	reg last_block_valid_2d;
	reg last_block_valid_3d;
	wire last_block_valid_2d_read;
	wire last_block_valid_2d_hit;
	
	reg							            block_ready_reg;   // assuming cache_block_ready is single cylce
    
    reg         [LUMA_DIM_WDTH-1:0]                 d_block_x_offset_luma;
    reg         [LUMA_DIM_WDTH-1:0]                 d_block_y_offset_luma;
    reg         [CHMA_DIM_WDTH-1:0]                 d_block_x_offset_chma;
    reg         [CHMA_DIM_WDTH-1:0]                 d_block_y_offset_chma; 

    reg         [LUMA_DIM_WDTH-1:0]                 d_block_x_end_luma;
    reg         [LUMA_DIM_WDTH-1:0]                 d_block_y_end_luma;
    reg         [CHMA_DIM_WDTH-1:0]                 d_block_x_end_chma;
    reg         [CHMA_DIM_WDTH-1:0]                 d_block_y_end_chma; 
	

    wire         [LUMA_DIM_WDTH-1:0]                 d_block_x_offset_luma_read;
    wire         [LUMA_DIM_WDTH-1:0]                 d_block_x_offset_luma_hit;
    wire         [LUMA_DIM_WDTH-1:0]                 d_block_y_offset_luma_read;
    wire         [LUMA_DIM_WDTH-1:0]                 d_block_y_offset_luma_hit;
    wire         [CHMA_DIM_WDTH-1:0]                 d_block_x_offset_chma_read;
    wire         [CHMA_DIM_WDTH-1:0]                 d_block_x_offset_chma_hit;
    wire         [CHMA_DIM_WDTH-1:0]                 d_block_y_offset_chma_read; 
    wire         [CHMA_DIM_WDTH-1:0]                 d_block_y_offset_chma_hit; 
	
    wire         [LUMA_DIM_WDTH-1:0]                 d_block_x_end_luma_read;
    wire         [LUMA_DIM_WDTH-1:0]                 d_block_x_end_luma_hit;
    wire         [LUMA_DIM_WDTH-1:0]                 d_block_y_end_luma_read;
    wire         [LUMA_DIM_WDTH-1:0]                 d_block_y_end_luma_hit;
    wire         [CHMA_DIM_WDTH-1:0]                 d_block_x_end_chma_read;
    wire         [CHMA_DIM_WDTH-1:0]                 d_block_x_end_chma_hit;
    wire         [CHMA_DIM_WDTH-1:0]                 d_block_y_end_chma_read; 
    wire         [CHMA_DIM_WDTH-1:0]                 d_block_y_end_chma_hit; 

	
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  luma_ref_start_x  ;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  luma_ref_start_y  ;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  chma_ref_start_x  ;
    reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  chma_ref_start_y  ;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] luma_ref_start_x_read  ;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] luma_ref_start_x_hit  ;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] luma_ref_start_y_read  ;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] luma_ref_start_y_hit  ;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] chma_ref_start_x_read  ;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] chma_ref_start_x_hit  ;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] chma_ref_start_y_read  ;
    wire [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] chma_ref_start_y_hit  ;
             
    reg  [LUMA_DIM_WDTH - 1:0]                   chma_ref_width_x;
    reg  [LUMA_DIM_WDTH - 1:0]                   chma_ref_height_y;
    reg  [LUMA_DIM_WDTH - 1:0]                   luma_ref_width_x;
    reg  [LUMA_DIM_WDTH - 1:0]                   luma_ref_height_y;
    wire  [LUMA_DIM_WDTH - 1:0]                  chma_ref_width_x_read;
    wire  [LUMA_DIM_WDTH - 1:0]                  chma_ref_width_x_hit;
    wire  [LUMA_DIM_WDTH - 1:0]                  chma_ref_height_y_read;
    wire  [LUMA_DIM_WDTH - 1:0]                  chma_ref_height_y_hit;
    wire  [LUMA_DIM_WDTH - 1:0]                  luma_ref_width_x_read;
    wire  [LUMA_DIM_WDTH - 1:0]                  luma_ref_width_x_hit;
    wire  [LUMA_DIM_WDTH - 1:0]                  luma_ref_height_y_read;
    wire  [LUMA_DIM_WDTH - 1:0]                  luma_ref_height_y_hit;
	
    reg res_present;
    wire res_present_read;
    wire res_present_hit;
    reg bi_pred_block_cache;
    wire bi_pred_block_cache_read;
    wire bi_pred_block_cache_hit;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_read;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_hit;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_read;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_hit;

    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus;
	
	
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus_read;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus_hit;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus_read;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus_hit;
	
    reg  [MV_C_FRAC_WIDTH_HIGH -1:0]      d_frac_x_out;
    reg  [MV_C_FRAC_WIDTH_HIGH -1:0]      d_frac_y_out;	
	
    wire  [MV_C_FRAC_WIDTH_HIGH -1:0]     d_frac_x_out_read;
    wire  [MV_C_FRAC_WIDTH_HIGH -1:0]     d_frac_x_out_hit;
    wire  [MV_C_FRAC_WIDTH_HIGH -1:0]     d_frac_y_out_read;
    wire  [MV_C_FRAC_WIDTH_HIGH -1:0]     d_frac_y_out_hit;
	
	
	reg [1:0] block_ready_state;
    
	
	reg res_present_fifo_in;
	reg bi_pred_block_cache_fifo_in;
	reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_fifo_in;
	reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_fifo_in;
	reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus_fifo_in;
	reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus_fifo_in;
	reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  luma_ref_start_x_fifo_in   ;
	reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  luma_ref_start_y_fifo_in   ;
	reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  chma_ref_start_x_fifo_in   ;
	reg [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  chma_ref_start_y_fifo_in   ;
	reg  [LUMA_DIM_WDTH - 1:0]                   chma_ref_width_x_fifo_in   ;
	reg  [LUMA_DIM_WDTH - 1:0]                   chma_ref_height_y_fifo_in  ;
	reg  [LUMA_DIM_WDTH - 1:0]                   luma_ref_width_x_fifo_in   ;
	reg  [LUMA_DIM_WDTH - 1:0]                   luma_ref_height_y_fifo_in  ;
	reg  [MV_C_FRAC_WIDTH_HIGH -1:0]      ch_frac_x_fifo_in;
	reg  [MV_C_FRAC_WIDTH_HIGH -1:0]      ch_frac_y_fifo_in;
	wire [LUMA_BITS* LUMA_REF_BLOCK_WIDTH* LUMA_REF_BLOCK_WIDTH -1:0]     block_121_fifo_in;
	wire [CHMA_BITS* CHMA_REF_BLOCK_WIDTH* CHMA_REF_BLOCK_WIDTH -1:0]     block_25cb_fifo_in;
	wire [CHMA_BITS* CHMA_REF_BLOCK_WIDTH* CHMA_REF_BLOCK_WIDTH -1:0]     block_25cr_fifo_in;
	reg         [LUMA_DIM_WDTH-1:0]                 block_x_offset_luma_fifo_in;
	reg         [LUMA_DIM_WDTH-1:0]                 block_y_offset_luma_fifo_in;
	reg         [CHMA_DIM_WDTH-1:0]                 block_x_offset_chma_fifo_in;
	reg         [CHMA_DIM_WDTH-1:0]                 block_y_offset_chma_fifo_in; 
	reg         [LUMA_DIM_WDTH-1:0]                 block_x_end_luma_fifo_in;
	reg         [LUMA_DIM_WDTH-1:0]                 block_y_end_luma_fifo_in;
	reg         [CHMA_DIM_WDTH-1:0]                 block_x_end_chma_fifo_in;
	reg         [CHMA_DIM_WDTH-1:0]                 block_y_end_chma_fifo_in;	
	
	wire output_fifo_empty;
	input output_fifo_full;
	input output_fifo_almost_full;
	input output_fifo_almost_full_only;
	input output_fifo_program_full;
	output block_ready_internal;
	// assign block_ready = block_ready_reg & filer_idle_in & miss_elem_fifo_empty;
	
	
	reg         [LUMA_DIM_WDTH-1:0]                 		d_block_x_offset_luma_2;
	reg         [LUMA_DIM_WDTH-1:0]                 		d_block_y_offset_luma_2;
	reg         [CHMA_DIM_WDTH-1:0]                 		d_block_x_offset_chma_2;
	reg         [CHMA_DIM_WDTH-1:0]                 		d_block_y_offset_chma_2; 
	reg         [LUMA_DIM_WDTH-1:0]                 		d_block_x_end_luma_2;
	reg         [LUMA_DIM_WDTH-1:0]                 		d_block_y_end_luma_2;
	reg         [CHMA_DIM_WDTH-1:0]                 		d_block_x_end_chma_2;
	reg         [CHMA_DIM_WDTH-1:0]                 		d_block_y_end_chma_2; 
	reg 		[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  	luma_ref_start_x_2  ;
	reg 		[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  	luma_ref_start_y_2  ;
	reg 		[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  	chma_ref_start_x_2  ;
	reg 		[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  	chma_ref_start_y_2  ;
	reg  		[LUMA_DIM_WDTH - 1:0]                  	 	chma_ref_width_x_2;
	reg  		[LUMA_DIM_WDTH - 1:0]                   	chma_ref_height_y_2;
	reg  		[LUMA_DIM_WDTH - 1:0]                   	luma_ref_width_x_2;
	reg  		[LUMA_DIM_WDTH - 1:0]                   	luma_ref_height_y_2;
	reg 													res_present_2;
	reg 													bi_pred_block_cache_2;
	reg 		[X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] 	x0_tu_end_in_min_tus_2;
	reg 		[X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] 	y0_tu_end_in_min_tus_2;
	reg 		[X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] 	xT_in_min_tus_2;
	reg 		[X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] 	yT_in_min_tus_2;
	reg  		[MV_C_FRAC_WIDTH_HIGH -1:0]      			d_frac_x_out_2;
	reg  		[MV_C_FRAC_WIDTH_HIGH -1:0]      			d_frac_y_out_2;	
	
	
	reg         [LUMA_DIM_WDTH-1:0]                 		d_block_x_offset_luma_3;
	reg         [LUMA_DIM_WDTH-1:0]                 		d_block_y_offset_luma_3;
	reg         [CHMA_DIM_WDTH-1:0]                 		d_block_x_offset_chma_3;
	reg         [CHMA_DIM_WDTH-1:0]                 		d_block_y_offset_chma_3; 
	reg         [LUMA_DIM_WDTH-1:0]                 		d_block_x_end_luma_3;
	reg         [LUMA_DIM_WDTH-1:0]                 		d_block_y_end_luma_3;
	reg         [CHMA_DIM_WDTH-1:0]                 		d_block_x_end_chma_3;
	reg         [CHMA_DIM_WDTH-1:0]                 		d_block_y_end_chma_3; 
	reg 		[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  	luma_ref_start_x_3  ;
	reg 		[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  	luma_ref_start_y_3  ;
	reg 		[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  	chma_ref_start_x_3  ;
	reg 		[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  	chma_ref_start_y_3  ;
	reg  		[LUMA_DIM_WDTH - 1:0]                  	 	chma_ref_width_x_3;
	reg  		[LUMA_DIM_WDTH - 1:0]                   	chma_ref_height_y_3;
	reg  		[LUMA_DIM_WDTH - 1:0]                   	luma_ref_width_x_3;
	reg  		[LUMA_DIM_WDTH - 1:0]                   	luma_ref_height_y_3;
	reg 													res_present_3;
	reg 													bi_pred_block_cache_3;
	reg 		[X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] 	x0_tu_end_in_min_tus_3;
	reg 		[X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] 	y0_tu_end_in_min_tus_3;
	reg 		[X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] 	xT_in_min_tus_3;
	reg 		[X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] 	yT_in_min_tus_3;
	reg  		[MV_C_FRAC_WIDTH_HIGH -1:0]      			d_frac_x_out_3;
	reg  		[MV_C_FRAC_WIDTH_HIGH -1:0]      			d_frac_y_out_3;	
	
	reg         [LUMA_DIM_WDTH-1:0]                 		d_block_x_offset_luma_4;
	reg         [LUMA_DIM_WDTH-1:0]                 		d_block_y_offset_luma_4;
	reg         [CHMA_DIM_WDTH-1:0]                 		d_block_x_offset_chma_4;
	reg         [CHMA_DIM_WDTH-1:0]                 		d_block_y_offset_chma_4; 
	reg         [LUMA_DIM_WDTH-1:0]                 		d_block_x_end_luma_4;
	reg         [LUMA_DIM_WDTH-1:0]                 		d_block_y_end_luma_4;
	reg         [CHMA_DIM_WDTH-1:0]                 		d_block_x_end_chma_4;
	reg         [CHMA_DIM_WDTH-1:0]                 		d_block_y_end_chma_4; 
	reg 		[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  	luma_ref_start_x_4  ;
	reg 		[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  	luma_ref_start_y_4  ;
	reg 		[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  	chma_ref_start_x_4  ;
	reg 		[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  	chma_ref_start_y_4  ;
	reg  		[LUMA_DIM_WDTH - 1:0]                  	 	chma_ref_width_x_4;
	reg  		[LUMA_DIM_WDTH - 1:0]                   	chma_ref_height_y_4;
	reg  		[LUMA_DIM_WDTH - 1:0]                   	luma_ref_width_x_4;
	reg  		[LUMA_DIM_WDTH - 1:0]                   	luma_ref_height_y_4;
	reg 													res_present_4;
	reg 													bi_pred_block_cache_4;
	reg 		[X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] 	x0_tu_end_in_min_tus_4;
	reg 		[X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] 	y0_tu_end_in_min_tus_4;
	reg 		[X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] 	xT_in_min_tus_4;
	reg 		[X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] 	yT_in_min_tus_4;
	reg  		[MV_C_FRAC_WIDTH_HIGH -1:0]      			d_frac_x_out_4;
	reg  		[MV_C_FRAC_WIDTH_HIGH -1:0]      			d_frac_y_out_4;	
	
	reg [BLOCK_NUMBER_WIDTH-1:0] block_number_3;
	wire [BLOCK_NUMBER_WIDTH-1:0] block_number_3_hit;
	wire [BLOCK_NUMBER_WIDTH-1:0] block_number_3_read;
	reg [BLOCK_NUMBER_WIDTH-1:0] block_number_4;
		







		
	assign ref_pix_axi_ar_fifo_rd_en = ref_pix_axi_ar_ready & ref_pix_axi_ar_valid;
	assign ref_pix_axi_ar_valid = !ref_pix_axi_ar_fifo_empty;
	assign block_ready = !output_fifo_empty & filer_idle_in;
	assign block_ready_internal = block_ready_reg & !output_fifo_full;
	generate  
		if(C_SIZE == 13) begin
			assign cache_w_port = {
			            ref_pix_axi_r_data	[CACHE_LINE_WDTH*PIXEL_BITS/2-1:CACHE_LINE_CR_OFFSET/2],
									cache_w_port_old_reg[CACHE_LINE_WDTH*PIXEL_BITS/2-1:CACHE_LINE_CR_OFFSET/2],			            
			            ref_pix_axi_r_data	[CACHE_LINE_CR_OFFSET/2-1:CACHE_LINE_CB_OFFSET/2],
									cache_w_port_old_reg[CACHE_LINE_CR_OFFSET/2-1:CACHE_LINE_CB_OFFSET/2],
									ref_pix_axi_r_data[CACHE_LINE_CB_OFFSET/2 -1:0],cache_w_port_old_reg[CACHE_LINE_CB_OFFSET/2-1:0]};
			// assign cache_w_port = {ref_pix_axi_r_data[CACHE_LINE_WDTH*PIXEL_BITS -1:AXI_CACHE_DATA_WDTH/2],ref_pix_axi_r_data[CACHE_LINE_WDTH*PIXEL_BITS/2 -1:0]};
			assign                  ref_pix_axi_ar_len    = `AX_LEN_2;
			assign                  ref_pix_axi_ar_size   = `AX_SIZE_64;
			assign 					bu_idx 		= {curr_x_addr_reg[2:0]};
			assign 					bu_row_idx 	= {curr_y_addr_reg[2:0]};
		end
		else begin
			assign cache_w_port = ref_pix_axi_r_data[CACHE_LINE_WDTH*PIXEL_BITS -1:0];
			assign                  ref_pix_axi_ar_len    = `AX_LEN_1;
			assign                  ref_pix_axi_ar_size   = `AX_SIZE_64;
			assign 					bu_idx 		= {curr_x_addr_reg[2:0],curr_y_addr_reg[0]};
			assign 					bu_row_idx 	= {curr_y_addr_reg[3:1]};
		end
	endgenerate
		

// synthesis translate_off
cache_print cache_print_block(
    .clk(clk),
    .reset(reset),
	.bi_pred_block_cache_in(bi_pred_block_cache_in),
	.cache_valid_in	(valid_in		),
	.rready			(ref_pix_axi_r_ready),
	.xT_in_min_tus	(xT_in_min_tus	),
	.yT_in_min_tus	(yT_in_min_tus	),
	.delta_x(delta_x),
	.delta_y(delta_y),
    .valid_in(ref_pix_axi_ar_valid),
    .ready_in(ref_pix_axi_ar_ready),
    .iu_idx_val(iu_idx_val),
    .iu_idx_row_val(iu_idx_row_val),
    .ref_idx_val(ref_idx_val),
    .bu_idx_val(bu_idx_val)
);

// always@(posedge clk) begin
	// if(data_read_stage_valid & ref_pix_axi_r_valid) begin
		// if(ref_pix_axi_r_last) begin
			// $display("contention for write");
			// $stop;
		// end
	// end
// end
// synthesis translate_on


    num_val_clines_generator num_val_clines_block_luma (
    .start_x_in(start_x_in[C_L_H_SIZE+LUMA_DIM_WDTH-1:0]), 
    .start_y_in(start_y_in[C_L_V_SIZE+LUMA_DIM_WDTH-1:0]), 
    .rf_blk_wdt_in(rf_blk_wdt_in), 
    .rf_blk_hgt_in(rf_blk_hgt_in), 
    .delta_x_out(delta_x_luma), 
    .delta_y_out(delta_y_luma)
    );
    
    
    num_val_clines_generator_ch num_val_clines_block_chma (
    .start_x_in(start_x_ch[C_L_H_SIZE_C+CHMA_DIM_WDTH-1:0]), 
    .start_y_in(start_y_ch[C_L_V_SIZE_C+CHMA_DIM_WDTH-1:0]), 
    .rf_blk_wdt_in(rf_blk_wdt_ch), 
    .rf_blk_hgt_in(rf_blk_hgt_ch), 
    .delta_x_out(delta_x_chma), 
    .delta_y_out(delta_y_chma)
    );

    num_val_clines_generator num_val_clines_block_great (
    .start_x_in(start_great_x_in[C_L_H_SIZE+LUMA_DIM_WDTH-1:0]), 
    .start_y_in(start_great_y_in[C_L_V_SIZE+LUMA_DIM_WDTH-1:0]), 
    .rf_blk_wdt_in(rf_blk_great_wdt_in), 
    .rf_blk_hgt_in(rf_blk_great_hgt_in), 
    .delta_x_out(delta_x), 
    .delta_y_out(delta_y)
    );


	tag_memory_write_first tag_block (
    .clk(clk), 
    .reset(reset), 
    .r_addr_in(set_addr), 
    .w_addr_in(tag_write_addr), 
    .r_data_out(tag_rdata_set), 
    .w_data_in(tag_addr_2d), 
    .w_en_in(tag_mem_wr_en), 
    .valid_bits_out(set_vld_bits)
    );
	

	age_memory age_block (
    .clk(clk), 
    .reset(reset), 
    .r_addr_in(set_addr_d), 
    .w_addr_in(set_addr_2d), 
    .r_data_out(age_val_set), 
    .w_data_in(new_age_set),
`ifdef CACHE_HARD_DEBUG	
	.state_out(state_out),
`endif
    .w_en_in(age_wr_en)
    );


    compare_tags_new tag_compare_block (
    .tags_set_in(tag_rdata_set), 
    .the_tag_in(tag_addr), 
    .ishit(is_hit), 
    .set_idx(set_idx),
`ifdef CACHE_HARD_DEBUG
    .ishit_out(ishit_out),
`endif
    .valid_bits_in(set_vld_bits)
    );
    
    new_age_converter age_conv_block (
    .ishit_in(is_hit_d), 
    .set_idx_in(set_idx_d), 
    .age_vals_in(age_val_set), 
    .new_age_vals_out(new_age_set), 
    .set_idx_miss_bnk_out(set_idx_miss)
    );    

    cache_data_mem_dual cache_mem_block(
    .clk(clk), 
    .w_addr_in(cache_w_addr), 
    .r_addr_in(cache_r_addr), 
    .r_data_out(cache_rdata), 
    .w_data_in(cache_w_port[CACHE_LINE_WDTH*PIXEL_BITS -1:0]), 
    .w_en_in(cache_wr_en)
    );

       geet_fifo_almost_full #(
		.LOG2_FIFO_DEPTH(3),
        .FIFO_DATA_WIDTH(
			AXI_ADDR_WDTH
		)
    ) miss_addr_fifo (
        .clk(clk), 
        .reset(reset), 
        .wr_en(ref_pix_axi_ar_valid_fifo_in), 
        .rd_en(ref_pix_axi_ar_fifo_rd_en), 
        .d_in({
			ref_pix_axi_ar_addr_fifo_in
		}), 
        .d_out({	
			ref_pix_axi_ar_addr
		}), 
		// .d_empty(d_miss_elem_fifo_empty),
        .empty(ref_pix_axi_ar_fifo_empty), 
        .program_full(ref_pix_axi_ar_fifo_full)
        );	
		
		
`ifdef CACHE_FIFOS
`ifdef HEVC_4K
      miss_elem_fifo_4k miss_elem_fifo(
`else
      miss_elem_fifo miss_elem_fifo(
`endif
        .clk(clk), 
        .srst(reset), 
        .wr_en(miss_elem_fifo_wr_en), 
        .rd_en(miss_elem_fifo_rd_en), 
        .din({
			block_number_3,
			curr_x_2d,
			curr_y_2d,
			delta_x_3,
			delta_y_3,
			set_idx_miss,
			last_block_valid_2d,
			luma_dest_enable_reg,
			chma_dest_enable_reg,
			dest_fill_x_mask_luma,
			dest_fill_y_mask_luma,
			dest_fill_x_loc_luma_write,
			dest_fill_y_loc_luma_write,
			dest_fill_x_mask_chma,
			dest_fill_y_mask_chma,
			dest_fill_x_loc_chma_write,
			dest_fill_y_loc_chma_write,
			set_addr_2d,
			xT_in_min_tus_3,
			yT_in_min_tus_3,
			bi_pred_block_cache_3
			,d_block_x_offset_luma_3
			,d_block_y_offset_luma_3
			,d_block_x_offset_chma_3
			,d_block_y_offset_chma_3
			,d_block_x_end_luma_3
			,d_block_y_end_luma_3
			,d_block_x_end_chma_3
			,d_block_y_end_chma_3 
			,luma_ref_start_x_3  
			,luma_ref_start_y_3  
			,chma_ref_start_x_3  
			,chma_ref_start_y_3  
			,chma_ref_width_x_3
			,chma_ref_height_y_3
			,luma_ref_width_x_3
			,luma_ref_height_y_3	
			,res_present_3	
			,x0_tu_end_in_min_tus_3
			,y0_tu_end_in_min_tus_3
			,d_frac_x_out_3
			,d_frac_y_out_3
		
		}), 
        .dout({
			block_number_3_read,
			curr_x_read,
			curr_y_read,
			delta_x_read,
			delta_y_read,
			set_idx_miss_read,
			last_block_valid_2d_read,
			luma_dest_enable_reg_read,
			chma_dest_enable_reg_read,
			dest_fill_x_mask_luma_read,
			dest_fill_y_mask_luma_read,
			dest_fill_x_loc_luma_read,
			dest_fill_y_loc_luma_read,
			dest_fill_x_mask_chma_read,
			dest_fill_y_mask_chma_read,
			dest_fill_x_loc_chma_read,
			dest_fill_y_loc_chma_read,
			set_addr_read,
			xT_in_min_tus_read,
			yT_in_min_tus_read,
			bi_pred_block_cache_read
			,d_block_x_offset_luma_read
			,d_block_y_offset_luma_read
			,d_block_x_offset_chma_read
			,d_block_y_offset_chma_read
			,d_block_x_end_luma_read
			,d_block_y_end_luma_read
			,d_block_x_end_chma_read
			,d_block_y_end_chma_read 
			,luma_ref_start_x_read  
			,luma_ref_start_y_read  
			,chma_ref_start_x_read  
			,chma_ref_start_y_read  
			,chma_ref_width_x_read
			,chma_ref_height_y_read
			,luma_ref_width_x_read
			,luma_ref_height_y_read	
			,res_present_read	
			,x0_tu_end_in_min_tus_read
			,y0_tu_end_in_min_tus_read
			,d_frac_x_out_read
			,d_frac_y_out_read			
			
		}), 
		// .d_empty(d_miss_elem_fifo_empty),
        .empty(miss_elem_fifo_empty), 
        .prog_full(miss_elem_fifo_full),
        .full()
        );
		  
`else		
       geet_fifo_almost_full #(
		.LOG2_FIFO_DEPTH(4),
        .FIFO_DATA_WIDTH(BLOCK_NUMBER_WIDTH + 2*2 + 2*2 +1+1+1+ (C_L_H_SIZE+C_L_V_SIZE+2)*LUMA_REF_BLOCK_WIDTH+ (C_L_H_SIZE_C+C_L_V_SIZE_C+2)*CHMA_REF_BLOCK_WIDTH + SET_ADDR_WDTH + C_N_WAY + ((X11_ADDR_WDTH - LOG2_MIN_TU_SIZE)*2) + 1
						+MV_C_FRAC_WIDTH_HIGH*2 + (X11_ADDR_WDTH - LOG2_MIN_TU_SIZE)*2 + 1 + LUMA_DIM_WDTH*4 + (MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH)*4 + LUMA_DIM_WDTH*4 + CHMA_DIM_WDTH*4
		)
    ) miss_elem_fifo (
        .clk(clk), 
        .reset(reset), 
        .wr_en(miss_elem_fifo_wr_en), 
        .rd_en(miss_elem_fifo_rd_en), 
        .d_in({
			block_number_3,
			curr_x_2d,
			curr_y_2d,
			delta_x_3,
			delta_y_3,
			set_idx_miss,
			last_block_valid_2d,
			luma_dest_enable_reg,
			chma_dest_enable_reg,
			dest_fill_x_mask_luma,
			dest_fill_y_mask_luma,
			dest_fill_x_loc_luma_write,
			dest_fill_y_loc_luma_write,
			dest_fill_x_mask_chma,
			dest_fill_y_mask_chma,
			dest_fill_x_loc_chma_write,
			dest_fill_y_loc_chma_write,
			set_addr_2d,
			xT_in_min_tus_3,
			yT_in_min_tus_3,
			bi_pred_block_cache_3
			,d_block_x_offset_luma_3
			,d_block_y_offset_luma_3
			,d_block_x_offset_chma_3
			,d_block_y_offset_chma_3
			,d_block_x_end_luma_3
			,d_block_y_end_luma_3
			,d_block_x_end_chma_3
			,d_block_y_end_chma_3 
			,luma_ref_start_x_3  
			,luma_ref_start_y_3  
			,chma_ref_start_x_3  
			,chma_ref_start_y_3  
			,chma_ref_width_x_3
			,chma_ref_height_y_3
			,luma_ref_width_x_3
			,luma_ref_height_y_3	
			,res_present_3	
			,x0_tu_end_in_min_tus_3
			,y0_tu_end_in_min_tus_3
			,d_frac_x_out_3
			,d_frac_y_out_3
		
		}), 
        .d_out({
			block_number_3_read,
			curr_x_read,
			curr_y_read,
			delta_x_read,
			delta_y_read,
			set_idx_miss_read,
			last_block_valid_2d_read,
			luma_dest_enable_reg_read,
			chma_dest_enable_reg_read,
			dest_fill_x_mask_luma_read,
			dest_fill_y_mask_luma_read,
			dest_fill_x_loc_luma_read,
			dest_fill_y_loc_luma_read,
			dest_fill_x_mask_chma_read,
			dest_fill_y_mask_chma_read,
			dest_fill_x_loc_chma_read,
			dest_fill_y_loc_chma_read,
			set_addr_read,
			xT_in_min_tus_read,
			yT_in_min_tus_read,
			bi_pred_block_cache_read
			,d_block_x_offset_luma_read
			,d_block_y_offset_luma_read
			,d_block_x_offset_chma_read
			,d_block_y_offset_chma_read
			,d_block_x_end_luma_read
			,d_block_y_end_luma_read
			,d_block_x_end_chma_read
			,d_block_y_end_chma_read 
			,luma_ref_start_x_read  
			,luma_ref_start_y_read  
			,chma_ref_start_x_read  
			,chma_ref_start_y_read  
			,chma_ref_width_x_read
			,chma_ref_height_y_read
			,luma_ref_width_x_read
			,luma_ref_height_y_read	
			,res_present_read	
			,x0_tu_end_in_min_tus_read
			,y0_tu_end_in_min_tus_read
			,d_frac_x_out_read
			,d_frac_y_out_read			
			
		}), 
		// .d_empty(d_miss_elem_fifo_empty),
        .empty(miss_elem_fifo_empty), 
        .program_full(miss_elem_fifo_full),
		.almost_full(),
        .full()
        );
`endif
`ifdef CACHE_FIFOS
`ifdef HEVC_4K
hit_elem_fifo_4k hit_elem_fifo (
`else
hit_elem_fifo hit_elem_fifo (
`endif
  .clk(clk), // input clk
  .srst(reset), // input rst
  .din({
			block_number_3,
			curr_x_2d,
			curr_y_2d,
			delta_x_3,
			delta_y_3,
			set_idx_d,
			last_block_valid_2d,
			luma_dest_enable_reg,
			chma_dest_enable_reg,
			dest_fill_x_mask_luma,
			dest_fill_y_mask_luma,
			dest_fill_x_loc_luma_write,
			dest_fill_y_loc_luma_write,
			dest_fill_x_mask_chma,
			dest_fill_y_mask_chma,
			dest_fill_x_loc_chma_write,
			dest_fill_y_loc_chma_write,
			set_addr_2d,
			xT_in_min_tus_3,
			yT_in_min_tus_3,
			bi_pred_block_cache_3
			,d_block_x_offset_luma_3
			,d_block_y_offset_luma_3
			,d_block_x_offset_chma_3
			,d_block_y_offset_chma_3
			,d_block_x_end_luma_3
			,d_block_y_end_luma_3
			,d_block_x_end_chma_3
			,d_block_y_end_chma_3 
			,luma_ref_start_x_3  
			,luma_ref_start_y_3  
			,chma_ref_start_x_3  
			,chma_ref_start_y_3  
			,chma_ref_width_x_3
			,chma_ref_height_y_3
			,luma_ref_width_x_3
			,luma_ref_height_y_3	
			,res_present_3	
			,x0_tu_end_in_min_tus_3
			,y0_tu_end_in_min_tus_3
			,d_frac_x_out_3
			,d_frac_y_out_3
		
		}), // input [285 : 0] din
  .wr_en(hit_elem_fifo_wr_en), // input wr_en
  .rd_en(hit_elem_fifo_rd_en), // input rd_en
  .dout({
			block_number_3_hit,
			curr_x_hit,
			curr_y_hit,
			delta_x_hit,
			delta_y_hit,
			set_idx_hit,
			last_block_valid_2d_hit,
			luma_dest_enable_reg_hit,
			chma_dest_enable_reg_hit,
			dest_fill_x_mask_luma_hit,
			dest_fill_y_mask_luma_hit,
			dest_fill_x_loc_luma_hit,
			dest_fill_y_loc_luma_hit,
			dest_fill_x_mask_chma_hit,
			dest_fill_y_mask_chma_hit,
			dest_fill_x_loc_chma_hit,
			dest_fill_y_loc_chma_hit,
			set_addr_hit,
			xT_in_min_tus_hit,
			yT_in_min_tus_hit,
			bi_pred_block_cache_hit
			,d_block_x_offset_luma_hit
			,d_block_y_offset_luma_hit
			,d_block_x_offset_chma_hit
			,d_block_y_offset_chma_hit
			,d_block_x_end_luma_hit
			,d_block_y_end_luma_hit
			,d_block_x_end_chma_hit
			,d_block_y_end_chma_hit 
			,luma_ref_start_x_hit  
			,luma_ref_start_y_hit  
			,chma_ref_start_x_hit  
			,chma_ref_start_y_hit  
			,chma_ref_width_x_hit
			,chma_ref_height_y_hit
			,luma_ref_width_x_hit
			,luma_ref_height_y_hit	
			,res_present_hit	
			,x0_tu_end_in_min_tus_hit
			,y0_tu_end_in_min_tus_hit
			,d_frac_x_out_hit
			,d_frac_y_out_hit			
			
		}), // output [285 : 0] dout
  .full(), // output full
  .empty(hit_elem_fifo_empty), 
  .prog_full(hit_elem_fifo_full)
);    
`else    
       geet_fifo_almost_full #(
		.LOG2_FIFO_DEPTH(5),
        .FIFO_DATA_WIDTH(BLOCK_NUMBER_WIDTH + 2*2 + 2*2 +1+1+1+ (C_L_H_SIZE+C_L_V_SIZE+2)*LUMA_REF_BLOCK_WIDTH+ (C_L_H_SIZE_C+C_L_V_SIZE_C+2)*CHMA_REF_BLOCK_WIDTH + SET_ADDR_WDTH + C_N_WAY + ((X11_ADDR_WDTH - LOG2_MIN_TU_SIZE)*2) + 1
						+MV_C_FRAC_WIDTH_HIGH*2 + (X11_ADDR_WDTH - LOG2_MIN_TU_SIZE)*2 + 1 + LUMA_DIM_WDTH*4 + (MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH)*4 + LUMA_DIM_WDTH*4 + CHMA_DIM_WDTH*4
		)
    ) hit_elem_fifo (
        .clk(clk), 
        .reset(reset), 
        .wr_en(hit_elem_fifo_wr_en), 
        .rd_en(hit_elem_fifo_rd_en), 
        .d_in({
			block_number_3,
			curr_x_2d,
			curr_y_2d,
			delta_x_3,
			delta_y_3,
			set_idx_d,
			last_block_valid_2d,
			luma_dest_enable_reg,
			chma_dest_enable_reg,
			dest_fill_x_mask_luma,
			dest_fill_y_mask_luma,
			dest_fill_x_loc_luma_write,
			dest_fill_y_loc_luma_write,
			dest_fill_x_mask_chma,
			dest_fill_y_mask_chma,
			dest_fill_x_loc_chma_write,
			dest_fill_y_loc_chma_write,
			set_addr_2d,
			xT_in_min_tus_3,
			yT_in_min_tus_3,
			bi_pred_block_cache_3
			,d_block_x_offset_luma_3
			,d_block_y_offset_luma_3
			,d_block_x_offset_chma_3
			,d_block_y_offset_chma_3
			,d_block_x_end_luma_3
			,d_block_y_end_luma_3
			,d_block_x_end_chma_3
			,d_block_y_end_chma_3 
			,luma_ref_start_x_3  
			,luma_ref_start_y_3  
			,chma_ref_start_x_3  
			,chma_ref_start_y_3  
			,chma_ref_width_x_3
			,chma_ref_height_y_3
			,luma_ref_width_x_3
			,luma_ref_height_y_3	
			,res_present_3	
			,x0_tu_end_in_min_tus_3
			,y0_tu_end_in_min_tus_3
			,d_frac_x_out_3
			,d_frac_y_out_3
		
		}), 
        .d_out({
			block_number_3_hit,
			curr_x_hit,
			curr_y_hit,
			delta_x_hit,
			delta_y_hit,
			set_idx_hit,
			last_block_valid_2d_hit,
			luma_dest_enable_reg_hit,
			chma_dest_enable_reg_hit,
			dest_fill_x_mask_luma_hit,
			dest_fill_y_mask_luma_hit,
			dest_fill_x_loc_luma_hit,
			dest_fill_y_loc_luma_hit,
			dest_fill_x_mask_chma_hit,
			dest_fill_y_mask_chma_hit,
			dest_fill_x_loc_chma_hit,
			dest_fill_y_loc_chma_hit,
			set_addr_hit,
			xT_in_min_tus_hit,
			yT_in_min_tus_hit,
			bi_pred_block_cache_hit
			,d_block_x_offset_luma_hit
			,d_block_y_offset_luma_hit
			,d_block_x_offset_chma_hit
			,d_block_y_offset_chma_hit
			,d_block_x_end_luma_hit
			,d_block_y_end_luma_hit
			,d_block_x_end_chma_hit
			,d_block_y_end_chma_hit 
			,luma_ref_start_x_hit  
			,luma_ref_start_y_hit  
			,chma_ref_start_x_hit  
			,chma_ref_start_y_hit  
			,chma_ref_width_x_hit
			,chma_ref_height_y_hit
			,luma_ref_width_x_hit
			,luma_ref_height_y_hit	
			,res_present_hit	
			,x0_tu_end_in_min_tus_hit
			,y0_tu_end_in_min_tus_hit
			,d_frac_x_out_hit
			,d_frac_y_out_hit			
			
		}), 
		// .d_empty(d_hit_elem_fifo_empty),
        .empty(hit_elem_fifo_empty), 
        .program_full(hit_elem_fifo_full),
        .almost_full(),
        .full()
        );            						
`endif			 


	assign miss_elem_fifo_empty_out = hit_elem_fifo_empty & miss_elem_fifo_empty & output_fifo_empty & !data_read_stage_valid & !hit_elem_fifo_wr_en_d;

    assign                  ref_pix_axi_ar_burst  = `AX_BURST_INC;
//    assign                  mv_pref_axi_arlock   = `AX_LOCK_DEFAULT;
//    assign                  mv_pref_axi_arcache  = `AX_CACHE_DEFAULT;
    assign                  ref_pix_axi_ar_prot   = `AX_PROT_DATA;    
    // assign                  ref_pix_axi_r_ready = 1;
    always@(posedge clk) begin
      hit_elem_fifo_wr_en_d <= hit_elem_fifo_wr_en;
    end
    
    always@(*) begin
		if(block_ready_reg & output_fifo_full) begin
			ref_pix_axi_r_ready = 0;
		end
		else if(state_data_read == STATE_IDLE || state_data_read == STATE_PASS_ONLY)begin
			ref_pix_axi_r_ready = 0;
		end
		else begin
			if(!hit_elem_fifo_empty) begin
				if( (curr_x_3d == curr_x_hit) && ((curr_y_3d == curr_y_hit)) 
					&& (xT_in_min_tus_4 == xT_in_min_tus_hit) && (yT_in_min_tus_4 == yT_in_min_tus_hit) && (bi_pred_block_cache_4 == bi_pred_block_cache_hit)) begin // check if next curr_xy need to be added here 
					ref_pix_axi_r_ready = 0;
				end
				else begin
					ref_pix_axi_r_ready = 1;
				end
			end else begin
				ref_pix_axi_r_ready = 1;
			end
		end

	end

assign curr_x_addr = curr_x + (start_great_x_in >> C_L_H_SIZE); 
assign curr_y_addr = curr_y + (start_great_y_in >> C_L_V_SIZE); 
		
assign cl_strt_x_luma    = (curr_x_luma == 2'b00)                ? start_x_in[C_L_H_SIZE-1:0]                                : {C_L_H_SIZE{1'b0}};
//assign cl_end__x_luma    = (curr_x_luma == delta_x_luma)         ? start_x_in[C_L_H_SIZE-1:0] + rf_blk_wdt_in[C_L_H_SIZE-1:0]: {C_L_H_SIZE{1'b1}};

assign cl_strt_y_luma    = (curr_y_luma == 2'b00)                ? start_y_in[C_L_V_SIZE-1:0]                                : {C_L_V_SIZE{1'b0}};
//assign cl_end__y_luma    = (curr_y_luma == delta_y_luma)         ? start_y_in[C_L_H_SIZE-1:0] + rf_blk_hgt_in[C_L_H_SIZE-1:0]: {C_L_H_SIZE{1'b1}};

assign cl_strt_x_chma    = (curr_x_chma == 0)                   ? start_x_ch[C_L_H_SIZE_C-1:0]                                  : {C_L_H_SIZE_C{1'b0}};
//assign cl_end__x_chma    = (curr_x_chma == delta_x_chma)        ? start_x_ch[C_L_H_SIZE_C-1:0] + rf_blk_wdt_ch[C_L_H_SIZE_C-1:0]: {C_L_H_SIZE_C{1'b1}};

assign cl_strt_y_chma    = (curr_y_chma == 0)                   ? start_y_ch[C_L_V_SIZE_C-1:0]                                  : {C_L_V_SIZE_C{1'b0}};
//assign cl_end__y_chma    = (curr_y_chma == delta_y_chma)        ? start_y_ch[C_L_H_SIZE_C-1:0] + rf_blk_hgt_ch[C_L_H_SIZE_C-1:0]: {C_L_H_SIZE_C{1'b1}};
 		

        
        
    generate
        genvar ii;
        genvar jj;
        
        for(jj=0;jj <LUMA_REF_BLOCK_WIDTH ; jj=jj+1 ) begin : row_iteration
            for(ii=0 ; ii < LUMA_REF_BLOCK_WIDTH ; ii = ii+1) begin : column_iteration
                // assign  block_121_out[(jj*LUMA_REF_BLOCK_WIDTH + ii +1)*(LUMA_BITS)-1: (jj*LUMA_REF_BLOCK_WIDTH + ii)*LUMA_BITS ] =  block_11x11[jj][ii];
            end
        end
        
        for(jj=0;jj <CHMA_REF_BLOCK_WIDTH ; jj=jj+1 ) begin
            for(ii=0 ; ii < CHMA_REF_BLOCK_WIDTH ; ii = ii+1) begin
                // assign    block_25cb_out[(jj*CHMA_REF_BLOCK_WIDTH + ii +1)*(CHMA_BITS)-1: (jj*CHMA_REF_BLOCK_WIDTH + ii)*CHMA_BITS] = block_5x5_cb[jj][ii];
            end
        end
        
        for(jj=0;jj <CHMA_REF_BLOCK_WIDTH ; jj=jj+1 ) begin
            for(ii=0 ; ii < CHMA_REF_BLOCK_WIDTH ; ii = ii+1) begin
                // assign    block_25cr_out[(jj*CHMA_REF_BLOCK_WIDTH + ii +1)*(CHMA_BITS)-1: (jj*CHMA_REF_BLOCK_WIDTH + ii)*CHMA_BITS] = block_5x5_cr[jj][ii];
            end
        end
        
        for(ii=0;ii < ((PIXEL_BITS*CACHE_LINE_WDTH/LUMA_BITS)); ii=ii+1) begin
            // assign    cache_w_data_arr[ii] = cache_w_port[LUMA_BITS*(ii+1)-1:LUMA_BITS*ii];
        end
		for(ii=0;ii < ((CACHE_LINE_WDTH)); ii=ii+1) begin
            // assign    cache_w_data_arr_luma[ii] = cache_w_port[(CACHE_LINE_LUMA_OFFSET)+LUMA_BITS*(ii+1)-1:(CACHE_LINE_LUMA_OFFSET)+LUMA_BITS*ii];
        end
		for(ii=0;ii < ((CACHE_LINE_WDTH/4)); ii=ii+1) begin
            // assign    cache_w_data_arr_cb[ii] = cache_w_port[(CACHE_LINE_CB_OFFSET)+LUMA_BITS*(ii+1)-1:(CACHE_LINE_CB_OFFSET)+LUMA_BITS*ii];
            // assign    cache_w_data_arr_cr[ii] = cache_w_port[(CACHE_LINE_CR_OFFSET)+LUMA_BITS*(ii+1)-1:(CACHE_LINE_CR_OFFSET)+LUMA_BITS*ii];
        end
		
        for(ii=0; ii<(PIXEL_BITS*CACHE_LINE_WDTH/LUMA_BITS) ; ii=ii+1) begin
            // assign    cache_rdata_arr[ii] = cache_rdata[LUMA_BITS*(ii+1)-1:LUMA_BITS*ii];
        end
		for(ii=0;ii < ((CACHE_LINE_WDTH)); ii=ii+1) begin
            // assign    cache_rdata_arr_luma[ii] = cache_rdata[(CACHE_LINE_LUMA_OFFSET)+LUMA_BITS*(ii+1)-1:(CACHE_LINE_LUMA_OFFSET)+LUMA_BITS*ii];
        end
		assign cache_rdata_luma = cache_rdata[(CACHE_LINE_LUMA_OFFSET)+LUMA_BITS*(CACHE_LINE_WDTH)-1:(CACHE_LINE_LUMA_OFFSET)];
		assign cache_wdata_luma = cache_w_port[(CACHE_LINE_LUMA_OFFSET)+LUMA_BITS*(CACHE_LINE_WDTH)-1:(CACHE_LINE_LUMA_OFFSET)];
		for(ii=0;ii < ((CACHE_LINE_WDTH/4)); ii=ii+1) begin
            // assign    cache_rdata_arr_cb[ii] = cache_rdata[(CACHE_LINE_CB_OFFSET)+LUMA_BITS*(ii+1)-1:(CACHE_LINE_CB_OFFSET)+LUMA_BITS*ii];
            // assign    cache_rdata_arr_cr[ii] = cache_rdata[(CACHE_LINE_CR_OFFSET)+LUMA_BITS*(ii+1)-1:(CACHE_LINE_CR_OFFSET)+LUMA_BITS*ii];
        end
		assign    cache_rdata_cb = cache_rdata[(CACHE_LINE_CB_OFFSET)+LUMA_BITS*(CACHE_LINE_WDTH/4)-1:(CACHE_LINE_CB_OFFSET)];
		assign    cache_wdata_cb = cache_w_port[(CACHE_LINE_CB_OFFSET)+LUMA_BITS*(CACHE_LINE_WDTH/4)-1:(CACHE_LINE_CB_OFFSET)];
		assign    cache_rdata_cr = cache_rdata[(CACHE_LINE_CR_OFFSET)+LUMA_BITS*(CACHE_LINE_WDTH/4)-1:(CACHE_LINE_CR_OFFSET)];
		assign    cache_wdata_cr = cache_w_port[(CACHE_LINE_CR_OFFSET)+LUMA_BITS*(CACHE_LINE_WDTH/4)-1:(CACHE_LINE_CR_OFFSET)];
            
		for(jj=0;jj <LUMA_REF_BLOCK_WIDTH ; jj=jj+1 ) begin : iteration1
			assign  dest_fill_x_loc_luma_write[((jj+1)*C_L_H_SIZE)-1: (jj)*C_L_H_SIZE] 	= dest_fill_x_loc_luma[jj];
			// assign  dest_fill_x_loc_luma_read_arry[jj] 									= dest_fill_x_loc_luma_read[((jj+1)*C_L_H_SIZE)-1: (jj)*C_L_H_SIZE] ;
			// assign  dest_fill_x_loc_luma_hit_arry[jj] 									= dest_fill_x_loc_luma_hit[((jj+1)*C_L_H_SIZE)-1: (jj)*C_L_H_SIZE] ;
        end
        for(jj=0;jj <LUMA_REF_BLOCK_WIDTH ; jj=jj+1 ) begin : iteration2
			assign  dest_fill_y_loc_luma_write[((jj+1)*C_L_V_SIZE)-1: (jj)*C_L_V_SIZE] 	= dest_fill_y_loc_luma[jj];
			// assign  dest_fill_y_loc_luma_read_arry[jj] 									= dest_fill_y_loc_luma_read[((jj+1)*C_L_V_SIZE)-1: (jj)*C_L_V_SIZE] ;
			// assign  dest_fill_y_loc_luma_hit_arry[jj] 									= dest_fill_y_loc_luma_hit[((jj+1)*C_L_V_SIZE)-1: (jj)*C_L_V_SIZE] ;
        end
        for(jj=0;jj <CHMA_REF_BLOCK_WIDTH ; jj=jj+1 ) begin : iteration3
			assign  dest_fill_y_loc_chma_write[((jj+1)*C_L_V_SIZE_C)-1: (jj)*C_L_V_SIZE_C] 	= dest_fill_y_loc_chma[jj];
			// assign  dest_fill_y_loc_chma_read_arry[jj] 										= dest_fill_y_loc_chma_read[((jj+1)*C_L_V_SIZE_C)-1: (jj)*C_L_V_SIZE_C] ;
			// assign  dest_fill_y_loc_chma_hit_arry[jj] 										= dest_fill_y_loc_chma_hit[((jj+1)*C_L_V_SIZE_C)-1: (jj)*C_L_V_SIZE_C] ;
        end   
		for(jj=0;jj <CHMA_REF_BLOCK_WIDTH ; jj=jj+1 ) begin : iteration4
			assign  dest_fill_x_loc_chma_write[((jj+1)*C_L_H_SIZE_C)-1: (jj)*C_L_H_SIZE_C] 	= dest_fill_x_loc_chma[jj];
			// assign  dest_fill_x_loc_chma_read_arry[jj] 										= dest_fill_x_loc_chma_read[((jj+1)*C_L_H_SIZE_C)-1: (jj)*C_L_H_SIZE_C] ;
			// assign  dest_fill_x_loc_chma_hit_arry[jj] 										= dest_fill_x_loc_chma_hit[((jj+1)*C_L_H_SIZE_C)-1: (jj)*C_L_H_SIZE_C] ;
        end  
		
    endgenerate


                        
	// assign cache_idle_out =  set_input_stage_ready & tag_read_stage_ready & tag_compare_stage_ready  & miss_elem_fifo_empty & data_read_stage_ready & (!(block_ready_reg & !block_ready))
							// &(!set_input_stage_valid		)&(!dest_enable_wire_valid    )&(!tag_compare_stage_valid   )&(!miss_elem_fifo_rd_en) & (!miss_elem_fifo_wr_en) & (!data_read_stage_valid);
	// assign cache_idle_out =  set_input_stage_ready & tag_read_stage_ready & tag_compare_stage_ready & 
	assign cache_idle_out =  set_input_stage_ready & tag_read_stage_ready & tag_compare_stage_ready & 
							 // (!set_input_stage_valid		)&(!dest_enable_wire_valid    )&(!tag_compare_stage_valid   );
							 (!set_input_stage_valid		)&(!dest_enable_wire_valid    );

always@(*) begin : next_dest_start_end
    if(curr_x_chma == 0) begin
        next_dst_strt_x_chma = d_block_x_offset_chma;
        if(curr_x_chma == delta_x_chma) begin
            next_dest_end_x_chma = d_block_x_offset_chma + rf_blk_wdt_ch;
        end
        else begin
            next_dest_end_x_chma = {1'b0,({C_L_H_SIZE_C{1'b1}} - start_x_ch[C_L_H_SIZE_C-1:0])} + d_block_x_offset_chma;
        end
    end
    else begin
        next_dst_strt_x_chma = dest_end_x_chma + 1'b1;   // max 4
        if(curr_x_chma == delta_x_chma) begin
            next_dest_end_x_chma = d_block_x_offset_chma + rf_blk_wdt_ch;
        end
        else begin
            next_dest_end_x_chma = dest_end_x_chma + {1'b1,{C_L_H_SIZE_C{1'b0}}};
        end
    end
    if(curr_x_luma == 0) begin
        next_dst_strt_x_luma = d_block_x_offset_luma;
        if(curr_x_luma == delta_x_luma) begin
            next_dest_end_x_luma = d_block_x_offset_luma + rf_blk_wdt_in;
        end
        else begin
            next_dest_end_x_luma = {1'b0,({C_L_H_SIZE{1'b1}} - start_x_in[C_L_H_SIZE-1:0])} + d_block_x_offset_luma;
        end
    end
    else begin
        next_dst_strt_x_luma = dest_end_x_luma + 1'b1;
        if(curr_x_luma == delta_x_luma) begin
            next_dest_end_x_luma = d_block_x_offset_luma + rf_blk_wdt_in;
        end
        else begin
            next_dest_end_x_luma = dest_end_x_luma + {1'b1,{C_L_H_SIZE{1'b0}}}; 
        end
    end
    if(curr_y_chma == 0) begin
        next_dst_strt_y_chma = d_block_y_offset_chma;
        if(curr_y_chma == delta_y_chma) begin
            next_dest_end_y_chma = d_block_y_offset_chma + rf_blk_hgt_ch;
        end
        else begin
            next_dest_end_y_chma = {1'b0,({C_L_V_SIZE_C{1'b1}} - start_y_ch[C_L_V_SIZE_C-1:0])} + d_block_y_offset_chma;
        end
    end
    else if(curr_x_chma == 0)begin
        next_dst_strt_y_chma = dest_end_y_chma + 1'b1;  
        if(curr_y_chma == delta_y_chma) begin
            next_dest_end_y_chma = d_block_y_offset_chma + rf_blk_hgt_ch;
        end
        else begin 
            next_dest_end_y_chma = dest_end_y_chma + {1'b1,{C_L_V_SIZE_C{1'b0}}};
        end
    end
    else begin
        next_dst_strt_y_chma = dst_strt_y_chma;
        next_dest_end_y_chma = dest_end_y_chma;        
    end
    if(curr_y_luma == 0) begin
        next_dst_strt_y_luma = d_block_y_offset_luma;
        if(curr_y_luma == delta_y_luma) begin
            next_dest_end_y_luma = d_block_y_offset_luma + rf_blk_hgt_in;
        end
        else begin
            next_dest_end_y_luma = {1'b0,({C_L_V_SIZE{1'b1}} - start_y_in[C_L_V_SIZE-1:0])} + d_block_y_offset_luma;
        end
    end
    else if(curr_x_luma == 0)begin
        next_dst_strt_y_luma = dest_end_y_luma + 1'b1;
        if(curr_y_luma == delta_y_luma) begin
            next_dest_end_y_luma = d_block_y_offset_luma + rf_blk_hgt_in;
        end
        else begin
            next_dest_end_y_luma = dest_end_y_luma + {1'b1,{C_L_V_SIZE{1'b0}}}; 
        end
    end
    else begin
        next_dst_strt_y_luma = dst_strt_y_luma;
        next_dest_end_y_luma = dest_end_y_luma;
    end
end

always @(* ) begin : luma_dest_enable
    if ( ((curr_x_addr_reg << C_L_H_SIZE) <= (start_x_in + rf_blk_wdt_in) ) && (( (curr_x_addr_reg_1) << C_L_H_SIZE) > start_x_in )) begin
        luma_dest_enable_x = 1;
    end
    else begin
        luma_dest_enable_x = 0;
    end
    if ( ((curr_y_addr_reg << C_L_V_SIZE) <= (start_y_in  + rf_blk_hgt_in) ) && (( (curr_y_addr_reg_1) << C_L_V_SIZE) > start_y_in )) begin
        luma_dest_enable_y = 1;
    end
    else begin
        luma_dest_enable_y = 0;
    end
	
    if ( ((curr_x_addr << C_L_H_SIZE) <= (start_x_in + rf_blk_wdt_in) ) && (( (curr_x_addr + 1'b1) << C_L_H_SIZE) > start_x_in )) begin
        luma_dest_enable_x_next = 1;
    end
    else begin
        luma_dest_enable_x_next = 0;
    end
    if ( ((curr_y_addr << C_L_V_SIZE) <= (start_y_in  + rf_blk_hgt_in) ) && (( (curr_y_addr +1'b1) << C_L_V_SIZE) > start_y_in )) begin
        luma_dest_enable_y_next = 1;
    end
    else begin
        luma_dest_enable_y_next = 0;
    end
	
    if(luma_dest_enable_x & luma_dest_enable_y ) begin
        luma_dest_enable_wire = 1;
    end
    else begin
        luma_dest_enable_wire = 0;
    end
	
    if(luma_dest_enable_x_next & luma_dest_enable_y_next ) begin
        luma_dest_enable_wire_next = 1;
    end
    else begin
        luma_dest_enable_wire_next = 0;
    end	
end

always @(*) begin : chma_dest_enable
    if ( ((curr_x_addr_reg << C_L_H_SIZE_C) <= (start_x_ch + rf_blk_wdt_ch) ) && (( (curr_x_addr_reg_1) << C_L_H_SIZE_C) > start_x_ch )) begin
        chma_dest_enable_x = 1;
    end
    else begin
        chma_dest_enable_x = 0;
    end
    if ( ((curr_y_addr_reg << C_L_V_SIZE_C) <= (start_y_ch  + rf_blk_hgt_ch) ) && (( (curr_y_addr_reg_1) << C_L_V_SIZE_C) > start_y_ch )) begin
        chma_dest_enable_y = 1;
    end
    else begin
        chma_dest_enable_y = 0;
    end
	
    if ( ((curr_x_addr << C_L_H_SIZE_C) <= (start_x_ch + rf_blk_wdt_ch) ) && (( (curr_x_addr +1'b1) << C_L_H_SIZE_C) > start_x_ch )) begin
        chma_dest_enable_x_next = 1;
    end
    else begin
        chma_dest_enable_x_next = 0;
    end
    if ( ((curr_y_addr << C_L_V_SIZE_C) <= (start_y_ch  + rf_blk_hgt_ch) ) && (( (curr_y_addr + 1'b1) << C_L_V_SIZE_C) > start_y_ch )) begin
        chma_dest_enable_y_next = 1;
    end
    else begin
        chma_dest_enable_y_next = 0;
    end
	
    if( chma_dest_enable_x && chma_dest_enable_y) begin
        chma_dest_enable_wire = 1;
    end
    else begin
        chma_dest_enable_wire = 0;
    end
	
    if( chma_dest_enable_x_next && chma_dest_enable_y_next) begin
        chma_dest_enable_wire_next = 1;
    end
    else begin
        chma_dest_enable_wire_next = 0;
    end
end


always@(*) begin :next_curr_x_y
    if(curr_x == delta_x) begin
        next_curr_x = 0;
        next_curr_y = curr_y + 1'b1;
    end
    else begin
        next_curr_x = curr_x + 1'b1;
        next_curr_y = curr_y;
    end
    if(curr_x_luma == delta_x_luma) begin
        next_curr_x_luma = 0;
        next_curr_y_luma = curr_y_luma + 1'b1;
    end
    else begin
        next_curr_x_luma = curr_x_luma + 1'b1;
        next_curr_y_luma = curr_y_luma;
    end	
    if(curr_x_chma == delta_x_chma) begin
        next_curr_x_chma = 0;
        next_curr_y_chma = curr_y_chma + 1'b1;
    end
    else begin
        next_curr_x_chma = curr_x_chma + 1'b1;
        next_curr_y_chma = curr_y_chma;
    end	
end



	
always@(posedge clk) begin : SET_INPUT_STAGE
	if(reset) begin
		set_input_stage_valid <= 0;
		state_set_input <= STATE_IDLE;
		curr_x <= 0;
        curr_y <= 0;
		ref_idx_in <= 0;
		
	end
	else begin
			case(state_set_input)
				STATE_IDLE: begin
					// if(!tag_read_stage_ready) begin
						// if(valid_in) begin
							// $disaply("valid in cant come when tag_read_stage not ready!");
							// $stop;
						// end
					// end
					// else 
					if(valid_in) begin
						state_set_input <= STATE_ACTIVE;
						set_input_stage_valid <= 1;
						
						xT_in_min_tus <= xT_in_min_tus_in;
						yT_in_min_tus <= yT_in_min_tus_in;
						
						
						res_present <= res_present_in;
						bi_pred_block_cache <= bi_pred_block_cache_in;
						x0_tu_end_in_min_tus <= x0_tu_end_in_min_tus_in;
						y0_tu_end_in_min_tus <= y0_tu_end_in_min_tus_in;

						luma_ref_start_x    <=  luma_ref_start_x_in  ;
						luma_ref_start_y    <=  luma_ref_start_y_in  ;   
						chma_ref_start_x    <=  chma_ref_start_x_in >>>1 ;       
						chma_ref_start_y    <=  chma_ref_start_y_in >>>1 ;       
						chma_ref_width_x    <=  chma_ref_width_x_in >>1 ;   
						chma_ref_height_y   <=  chma_ref_height_y_in >>1;       
						luma_ref_width_x    <=  luma_ref_width_x_in  ;       
						luma_ref_height_y   <=  luma_ref_height_y_in ;   
						d_frac_x_out <= ch_frac_x;
						d_frac_y_out <= ch_frac_y;    
								
											
						

						ref_idx_in <= ref_idx_in_in;
						curr_x <= 0;
						curr_y <= 0;

						curr_x_luma <= 0;
						curr_y_luma <= 0;
						curr_x_chma <= 0;
						curr_y_chma <= 0;
						cur_xy_changed_luma <= 1;
						cur_xy_changed_chma <= 1;
						
					end
					else begin
						set_input_stage_valid <= 0;
					end
				end
				STATE_ACTIVE: begin
					set_input_stage_valid <= 0;
					if(curr_x == delta_x && curr_y == delta_y) begin
						state_set_input <= STATE_IDLE;
					end	
					else if(tag_read_stage_ready) begin
						set_input_stage_valid <= 1; 
						curr_x <= next_curr_x;
						curr_y <= next_curr_y;
						if(luma_dest_enable_wire_next) begin
							curr_x_luma <= next_curr_x_luma;
							curr_y_luma <= next_curr_y_luma;
							cur_xy_changed_luma <= 1;
						end
						else begin
							cur_xy_changed_luma <= 0;
						end
						if(chma_dest_enable_wire_next) begin
							curr_x_chma <= next_curr_x_chma;
							curr_y_chma <= next_curr_y_chma;
							cur_xy_changed_chma <= 1;  
						end
						else begin
							cur_xy_changed_chma <= 0;
						end		
						
								
						if(next_curr_x == delta_x && next_curr_y == delta_y) begin
							state_set_input <= STATE_IDLE;
						end
												
					end

				end

			endcase
	end
end

always@(*) begin
	set_input_stage_ready = 0;
	case(state_set_input)
		STATE_IDLE: begin
			set_input_stage_ready = 1;
		end
	endcase
end


	
always@(posedge clk) begin : TAG_READ_STATE
	if(reset) begin
		dest_enable_wire_valid <= 0;
		curr_x_addr_reg     <= 0;		// reseting to set valid value for araddr after reset
		curr_y_addr_reg     <= 0;
		state_tag_read <= STATE_IDLE;
		last_block_valid_1d <= 0;
		curr_x_d <= 0;
		curr_y_d <= 0;
	end
	else begin
		case(state_tag_read)
			STATE_IDLE: begin
				if(set_input_stage_valid) begin 
					curr_x_d <= curr_x;
					curr_y_d <= curr_y;
					dest_enable_wire_valid <= 1;
					if(curr_x == delta_x && curr_y == delta_y) begin
						last_block_valid_1d <= 1;
					end
					else begin
						last_block_valid_1d <= 0;
					end
					if(curr_x == 0 && curr_y == 0) begin
						
						delta_x_2 <= delta_x;
						delta_y_2 <= delta_y;
						xT_in_min_tus_2 <= xT_in_min_tus;
						yT_in_min_tus_2 <= yT_in_min_tus;
						bi_pred_block_cache_2 <= bi_pred_block_cache;
						
						
						d_block_x_offset_luma_2 <= d_block_x_offset_luma;
						d_block_y_offset_luma_2 <= d_block_y_offset_luma;
						d_block_x_offset_chma_2 <= d_block_x_offset_chma;
						d_block_y_offset_chma_2 <= d_block_y_offset_chma; 
						
						d_block_x_end_luma_2 <= d_block_x_end_luma;
						d_block_y_end_luma_2 <= d_block_y_end_luma;
						d_block_x_end_chma_2 <= d_block_x_end_chma;
						d_block_y_end_chma_2 <= d_block_y_end_chma; 
					
						luma_ref_start_x_2 <= luma_ref_start_x;
						luma_ref_start_y_2 <= luma_ref_start_y;
						chma_ref_start_x_2 <= chma_ref_start_x;
						chma_ref_start_y_2 <= chma_ref_start_y;
							 
						chma_ref_width_x_2  <= chma_ref_width_x;
						chma_ref_height_y_2 <= chma_ref_height_y;
						luma_ref_width_x_2  <= luma_ref_width_x;
						luma_ref_height_y_2 <= luma_ref_height_y;

						res_present_2 <= res_present;
						x0_tu_end_in_min_tus_2 <= x0_tu_end_in_min_tus;
						y0_tu_end_in_min_tus_2 <= y0_tu_end_in_min_tus;
						
						d_frac_y_out_2 <= d_frac_y_out;
						d_frac_x_out_2 <= d_frac_x_out;					
					end
	
					curr_x_addr_reg     <= curr_x_addr;
					curr_y_addr_reg     <= curr_y_addr;
					tag_addr <= {ref_idx_in,curr_y_addr[Y_ADDR_WDTH - C_L_V_SIZE -1:2],curr_x_addr[X_ADDR_WDTH-C_L_H_SIZE-1:2]};    // the 2 in the expression indicates x/y bits in the set address
					curr_y_addr_reg_1     <= curr_y_addr +1; 
					curr_x_addr_reg_1     <= curr_x_addr +1;          
					set_addr_d <= set_addr;
					if(!tag_compare_stage_ready) begin
						state_tag_read <= STATE_ACTIVE;
					end

					if(cur_xy_changed_luma) begin
						cl_strt_x_luma_d <= cl_strt_x_luma;
						//cl_end__x_luma_d <= cl_end__x_luma;
						cl_strt_y_luma_d <= cl_strt_y_luma;
						//cl_end__y_luma_d <= cl_end__y_luma;

						dest_end_x_luma <= next_dest_end_x_luma;
						dst_strt_x_luma <= next_dst_strt_x_luma;
						dest_end_y_luma <= next_dest_end_y_luma;
						dst_strt_y_luma <= next_dst_strt_y_luma;
					end
					if(cur_xy_changed_chma) begin
						cl_strt_x_chma_d <= cl_strt_x_chma;
						//cl_end__x_chma_d <= cl_end__x_chma;
						cl_strt_y_chma_d <= cl_strt_y_chma;
						//cl_end__y_chma_d <= cl_end__y_chma;

						dest_end_x_chma <= next_dest_end_x_chma;
						dst_strt_x_chma <= next_dst_strt_x_chma;
						dest_end_y_chma <= next_dest_end_y_chma;
						dst_strt_y_chma <= next_dst_strt_y_chma;
					end					
				end
				else begin
					dest_enable_wire_valid <= 0;
				end
			end
			STATE_ACTIVE: begin
				if(tag_compare_stage_ready) begin
					state_tag_read <= STATE_IDLE;
					dest_enable_wire_valid <= 0;			
					
				end
			end
		endcase
	end
end


always@(*) begin
	set_addr = {curr_y_addr[1:0],curr_x_addr[1:0]};
end

always@(*) begin
	tag_read_stage_ready = 0;
	case(state_tag_read)
		STATE_IDLE: begin
			tag_read_stage_ready = 1;
			if(state_tag_read_d != STATE_IDLE) begin
			end
			else begin
				if(set_input_stage_valid) begin
					if(!tag_compare_stage_ready) begin
						tag_read_stage_ready = 0;
					end
				end
			end
		end

	endcase 
end

always@(posedge clk) begin
	state_tag_read_d <= state_tag_read;
end
	

always@(posedge clk) begin : TAG_COMPARE_STAGE
	if(reset) begin
		ref_pix_axi_ar_valid_fifo_in <= 0;
		iu_idx_val <= 0;
		iu_idx_row_val <= 0;
		ref_idx_val <= 0;
		bu_idx_val <= 0;
		set_addr_2d <= 0;
		state_tag_compare <= STATE_IDLE;
		miss_elem_fifo_wr_en <= 0;
		hit_elem_fifo_wr_en <= 0;
		last_block_valid_2d <= 0;
		block_number_3  <= -1;
		tag_compare_stage_valid <= 0;
	end
	else begin
		tag_compare_stage_valid <= 0;
		case(state_tag_compare) 
			STATE_IDLE: begin
				miss_elem_fifo_wr_en <= 0;	
				hit_elem_fifo_wr_en  <= 0;
				ref_pix_axi_ar_valid_fifo_in <= 0;	
				if(dest_enable_wire_valid) begin
					curr_x_2d <= curr_x_d;
					curr_y_2d <= curr_y_d;				
					if(curr_x_d == 0 && curr_y_d == 0) begin
						block_number_3 <= block_number_3 + 1'b1;
						delta_x_3 <= delta_x_2;
						delta_y_3 <= delta_y_2;						
						xT_in_min_tus_3 <= xT_in_min_tus_2;
						yT_in_min_tus_3 <= yT_in_min_tus_2;
						bi_pred_block_cache_3 <= bi_pred_block_cache_2;
						
						
						d_block_x_offset_luma_3 <= d_block_x_offset_luma_2;
						d_block_y_offset_luma_3 <= d_block_y_offset_luma_2;
						d_block_x_offset_chma_3 <= d_block_x_offset_chma_2;
						d_block_y_offset_chma_3 <= d_block_y_offset_chma_2; 
						
						d_block_x_end_luma_3 <= d_block_x_end_luma_2;
						d_block_y_end_luma_3 <= d_block_y_end_luma_2;
						d_block_x_end_chma_3 <= d_block_x_end_chma_2;
						d_block_y_end_chma_3 <= d_block_y_end_chma_2; 
					
						luma_ref_start_x_3 <= luma_ref_start_x_2;
						luma_ref_start_y_3 <= luma_ref_start_y_2;
						chma_ref_start_x_3 <= chma_ref_start_x_2;
						chma_ref_start_y_3 <= chma_ref_start_y_2;
							 
						chma_ref_width_x_3  <= chma_ref_width_x_2;
						chma_ref_height_y_3 <= chma_ref_height_y_2;
						luma_ref_width_x_3  <= luma_ref_width_x_2;
						luma_ref_height_y_3 <= luma_ref_height_y_2;

						res_present_3 <= res_present_2;
						x0_tu_end_in_min_tus_3 <= x0_tu_end_in_min_tus_2;
						y0_tu_end_in_min_tus_3 <= y0_tu_end_in_min_tus_2;
						
						d_frac_y_out_3 <= d_frac_y_out_2;
						d_frac_x_out_3 <= d_frac_x_out_2;					
					end				
					last_block_valid_2d  <= last_block_valid_1d;
					is_hit_d <= is_hit;
					set_idx_d <= set_idx;
					set_addr_2d <= set_addr_d;
					tag_addr_d <= tag_addr;

					if(!is_hit) begin
						tag_compare_stage_valid <= 1;
						ref_pix_axi_ar_valid_fifo_in <= 1;
						miss_elem_fifo_wr_en <= 1;
						if(miss_elem_fifo_full || ref_pix_axi_ar_fifo_full) begin
							state_tag_compare <= STATE_READY_WAIT;
						end
						// 
						iu_idx_val <= iu_idx * `REF_PIX_IU_OFFSET;
						iu_idx_row_val <= iu_row_idx * `REF_PIX_IU_ROW_OFFSET;
						ref_idx_val <= ref_idx_in * `REF_PIX_FRAME_OFFSET;
						if(C_SIZE == 13) begin
							bu_idx_val <= bu_idx * `REF_PIX_BU_OFFSET + bu_row_idx * `REF_PIX_BU_ROW_OFFSET;
						end
						else begin
							bu_idx_val <= bu_idx * `REF_PIX_BU_OFFSET_OLD + bu_row_idx * `REF_PIX_BU_ROW_OFFSET;
						end	
						
					end
					else begin
						hit_elem_fifo_wr_en <= 1;
						ref_pix_axi_ar_valid_fifo_in <= 0;	
						if(hit_elem_fifo_full ) begin
							state_tag_compare <= STATE_PASS_ONLY;	
						end
					end
					luma_dest_enable_reg <= luma_dest_enable_wire;
					chma_dest_enable_reg <= chma_dest_enable_wire;			
					//if(luma_dest_enable_reg == 1) begin
						for(j=0;j < LUMA_REF_BLOCK_WIDTH; j = j+1) begin // horizontal span
							if((dst_strt_x_luma <= j[LUMA_DIM_WDTH -1:0]) && (j[LUMA_DIM_WDTH -1:0] <= dest_end_x_luma) ) begin
								dest_fill_x_mask_luma[j] <= 1;
								dest_fill_x_loc_luma[j] <= (j[LUMA_DIM_WDTH -1:0] - dst_strt_x_luma) + cl_strt_x_luma_d;
							end
							else begin
								dest_fill_x_mask_luma[j] <= 0;
							end
						end
						for(j=0;j < LUMA_REF_BLOCK_WIDTH; j = j+1) begin // vertical span
							if((dst_strt_y_luma <= j[LUMA_DIM_WDTH -1:0]) && (j[LUMA_DIM_WDTH -1:0] <= dest_end_y_luma)) begin
								dest_fill_y_mask_luma[j] <= 1;
								dest_fill_y_loc_luma[j] <= (j[LUMA_DIM_WDTH -1:0] - dst_strt_y_luma) + cl_strt_y_luma_d;
							end
							else begin
								dest_fill_y_mask_luma[j] <= 0;
							end
						end
					//end

					//if(chma_dest_enable_reg == 1) begin
						for(j=0;j < CHMA_REF_BLOCK_WIDTH; j = j+1) begin // horizontal span
							if((dst_strt_x_chma <= j[CHMA_DIM_WDTH -1:0]) && (j[CHMA_DIM_WDTH -1:0] <= dest_end_x_chma) ) begin
								dest_fill_x_mask_chma[j] <= 1;
								dest_fill_x_loc_chma[j] <= (j[CHMA_DIM_WDTH -1:0] - dst_strt_x_chma) + cl_strt_x_chma_d;
							end
							else begin
								dest_fill_x_mask_chma[j] <= 0;
							end
						end
						for(j=0;j < CHMA_REF_BLOCK_WIDTH; j = j+1) begin // vertical span
							if((dst_strt_y_chma <= j[CHMA_DIM_WDTH -1:0]) && (j[CHMA_DIM_WDTH -1:0] <= dest_end_y_chma)) begin
								dest_fill_y_mask_chma[j] <= 1;
								dest_fill_y_loc_chma[j] <= (j[CHMA_DIM_WDTH -1:0] - dst_strt_y_chma) + cl_strt_y_chma_d;
							end
							else begin
								dest_fill_y_mask_chma[j] <= 0;
							end
						end
					//end				
				end
			end
			STATE_READY_WAIT: begin
				miss_elem_fifo_wr_en <= 0;
				ref_pix_axi_ar_valid_fifo_in <= 0;
				if(!ref_pix_axi_ar_fifo_full & !miss_elem_fifo_full) begin
					
					state_tag_compare <= STATE_IDLE;
				end
			end
			STATE_MISS_FULL_WAIT: begin
				ref_pix_axi_ar_valid_fifo_in <= 0;
				if(!miss_elem_fifo_full) begin
					state_tag_compare <= STATE_IDLE;
					miss_elem_fifo_wr_en <= 1;
				end
			end
			STATE_READY_AND_FUL_WAIT: begin
				case({!miss_elem_fifo_full,!ref_pix_axi_ar_fifo_full})
					2'b00: begin
					
					end
					2'b10: begin
						miss_elem_fifo_wr_en <= 1;
						state_tag_compare <= STATE_READY_WAIT;
					end
					2'b01: begin
						ref_pix_axi_ar_valid_fifo_in <= 1;
						state_tag_compare <= STATE_MISS_FULL_WAIT;
					end
					2'b11: begin
						ref_pix_axi_ar_valid_fifo_in <= 1;
						miss_elem_fifo_wr_en <= 1;
						state_tag_compare <= STATE_IDLE;
					end
				endcase
			end
			STATE_PASS_ONLY: begin
				hit_elem_fifo_wr_en <= 0;
				if(!hit_elem_fifo_full) begin
					state_tag_compare <= STATE_IDLE;

				end
			end

		endcase

	end
end

always@(*) begin
	ref_pix_axi_ar_addr_fifo_in = bu_idx_val  + iu_idx_val + iu_idx_row_val + ref_idx_val;
end
always@(*) begin
	tag_compare_stage_ready = 0;
	case(state_tag_compare)
		STATE_IDLE: begin
			tag_compare_stage_ready = 1;
			if(state_tag_compare_d != STATE_IDLE) begin// || state_tag_compare_d == STATE_FIFO_CLEAR_WAIT) begin
				tag_compare_stage_ready = 1;
			end
			else begin
				if(dest_enable_wire_valid) begin
					if(!is_hit) begin
						if(miss_elem_fifo_full || ref_pix_axi_ar_fifo_full) begin
							tag_compare_stage_ready = 0;
						end
					end
					else begin
						if(hit_elem_fifo_full ) begin
							tag_compare_stage_ready = 0;	
						end
					end
				end						
			
			end
		end
	endcase
end

always@(posedge clk) begin
	state_tag_compare_d <= state_tag_compare;
end

always@(posedge clk) begin
	if(reset) begin
		block_ready_reg <= 0;  // block ready can now assert in consecutive clock cycles
		block_ready_state <= STATE_IDLE;
	end
	else begin
		case(block_ready_state)
			STATE_IDLE: begin
				block_ready_reg <= 0;
				if(miss_elem_fifo_rd_en & last_block_valid_2d_read) begin
					block_ready_reg <= 1;
					if(output_fifo_almost_full ) begin    // data valid working with one cycle latency, dont get almost_full here
						block_ready_state <= STATE_PASS_ONLY;
						block_ready_reg <= 0;
					end
				end
				if ( data_read_stage_valid & last_block_valid_3d)begin
					block_ready_reg <= 1;
					if(output_fifo_almost_full ) begin   
						block_ready_state <= STATE_PASS_ONLY;
						block_ready_reg <= 0;
					end
				end
				
			end
			STATE_PASS_ONLY: begin
				if(!output_fifo_almost_full ) begin    // need not to wait til miss element fifo is empty for the block to declared ready, cache lines for the next block could wait in the pipeline
					block_ready_reg <= 1;
					block_ready_state <= STATE_IDLE;
				end				
			end
		endcase

	end
end
	
// always@(posedge clk) begin
	// state_data_read_d <= state_data_read;
// end

always@(posedge clk) begin
	tag_mem_wr_en <= 0;
	if(tag_compare_stage_valid) begin // tag address update not effected by r_last
		tag_addr_2d <= tag_addr_d;
		tag_mem_wr_en <= 1;
		tag_write_addr <= {set_addr_2d,set_idx_miss};								
	end
end

// always@(posedge clk) begin


			// if(ref_pix_axi_r_valid & ref_pix_axi_r_ready) begin
				// if(ref_pix_axi_r_last) begin
					////synthesis translate_off
						// if(miss_elem_fifo_empty ) begin
							// $display("%d miss fifo cant be empty at this stage",$time);
							// $stop;
						// end
					////synthesis translate_on
					// if(luma_dest_enable_reg_read) begin
						// for(i=0;i<LUMA_REF_BLOCK_WIDTH; i=i+1) begin
							// for(j=0;j<LUMA_REF_BLOCK_WIDTH; j=j+1) begin
								// if(dest_fill_x_mask_luma_read[j] & dest_fill_y_mask_luma_read[i]) begin
									// block_11x11[i][j] <= cache_w_data_arr[(CACHE_LINE_LUMA_OFFSET/LUMA_BITS) + (dest_fill_y_loc_luma_read_arry[i] * (1<<C_L_H_SIZE) + dest_fill_x_loc_luma_read_arry[j])];
								// end
							// end
						// end
					// end
					// if(chma_dest_enable_reg_read) begin
						// for(i=0;i<CHMA_REF_BLOCK_WIDTH; i=i+1) begin
							// for(j=0;j<CHMA_REF_BLOCK_WIDTH; j=j+1) begin
								// if(dest_fill_x_mask_chma_read[j] & dest_fill_y_mask_chma_read[i]) begin
									// block_5x5_cb[i][j] <= cache_w_data_arr[(CACHE_LINE_CB_OFFSET/LUMA_BITS) + (dest_fill_y_loc_chma_read_arry[i] * (1<<C_L_H_SIZE_C) + dest_fill_x_loc_chma_read_arry[j])];
									// block_5x5_cr[i][j] <= cache_w_data_arr[(CACHE_LINE_CR_OFFSET/LUMA_BITS) + (dest_fill_y_loc_chma_read_arry[i] * (1<<C_L_H_SIZE_C) + dest_fill_x_loc_chma_read_arry[j])];
								// end
							// end
						// end
					// end
				// end
				// else begin
					// if(data_read_stage_valid) begin
						// if(luma_dest_enable_reg_d) begin
							// for(i=0;i<LUMA_REF_BLOCK_WIDTH; i=i+1) begin
								// for(j=0;j<LUMA_REF_BLOCK_WIDTH; j=j+1) begin
									// if(dest_fill_x_mask_luma_d[j] & dest_fill_y_mask_luma_d[i]) begin
										// block_11x11[i][j] <= cache_rdata_arr[(CACHE_LINE_LUMA_OFFSET/LUMA_BITS) + (dest_fill_y_loc_luma_d[i] * (1<<C_L_H_SIZE) + dest_fill_x_loc_luma_d[j])];
									// end
								// end
							// end
						// end
						// if(chma_dest_enable_reg_d) begin
							// for(i=0;i<CHMA_REF_BLOCK_WIDTH; i=i+1) begin
								// for(j=0;j<CHMA_REF_BLOCK_WIDTH; j=j+1) begin
									// if(dest_fill_x_mask_chma_d[j] & dest_fill_y_mask_chma_d[i]) begin
										// block_5x5_cb[i][j] <= cache_rdata_arr[(CACHE_LINE_CB_OFFSET/LUMA_BITS) + (dest_fill_y_loc_chma_d[i] * (1<<C_L_H_SIZE_C) + dest_fill_x_loc_chma_d[j])];
										// block_5x5_cr[i][j] <= cache_rdata_arr[(CACHE_LINE_CR_OFFSET/LUMA_BITS) + (dest_fill_y_loc_chma_d[i] * (1<<C_L_H_SIZE_C) + dest_fill_x_loc_chma_d[j])];
									// end
								// end
							// end
						// end
					// end
				// end
			// end
			// else begin
				// if(data_read_stage_valid) begin
					// if(luma_dest_enable_reg_d) begin
						// for(i=0;i<LUMA_REF_BLOCK_WIDTH; i=i+1) begin
							// for(j=0;j<LUMA_REF_BLOCK_WIDTH; j=j+1) begin
								// if(dest_fill_x_mask_luma_d[j] & dest_fill_y_mask_luma_d[i]) begin
									// block_11x11[i][j] <= cache_rdata_arr[(CACHE_LINE_LUMA_OFFSET/LUMA_BITS) + (dest_fill_y_loc_luma_d[i] * (1<<C_L_H_SIZE) + dest_fill_x_loc_luma_d[j])];
								// end
							// end
						// end
					// end
					// if(chma_dest_enable_reg_d) begin
						// for(i=0;i<CHMA_REF_BLOCK_WIDTH; i=i+1) begin
							// for(j=0;j<CHMA_REF_BLOCK_WIDTH; j=j+1) begin
								// if(dest_fill_x_mask_chma_d[j] & dest_fill_y_mask_chma_d[i]) begin
									// block_5x5_cb[i][j] <= cache_rdata_arr[(CACHE_LINE_CB_OFFSET/LUMA_BITS) + (dest_fill_y_loc_chma_d[i] * (1<<C_L_H_SIZE_C) + dest_fill_x_loc_chma_d[j])];
									// block_5x5_cr[i][j] <= cache_rdata_arr[(CACHE_LINE_CR_OFFSET/LUMA_BITS) + (dest_fill_y_loc_chma_d[i] * (1<<C_L_H_SIZE_C) + dest_fill_x_loc_chma_d[j])];
								// end
							// end
						// end
					// end
				// end
			// end
						
// end

// always@(*) begin
	// for(i=0;i<LUMA_REF_BLOCK_WIDTH; i=i+1) begin
		// for(j=0;j<LUMA_REF_BLOCK_WIDTH; j=j+1) begin
			// dest_fill_xy_loc_luma_d[i][j] <= (dest_fill_y_loc_luma_d[i] * (1<<C_L_H_SIZE) + dest_fill_x_loc_luma_d[j]);
		// end
	// end
// end
always@(posedge clk) begin

		if(data_read_stage_valid) begin
			if(luma_dest_enable_reg_d) begin
				dest_fill_x_loc_luma_output <= dest_fill_x_loc_luma_hit_d;
				dest_fill_y_loc_luma_output <= dest_fill_y_loc_luma_hit_d;
				dest_fill_x_mask_luma_output <= dest_fill_x_mask_luma_d;
				dest_fill_y_mask_luma_output <= dest_fill_y_mask_luma_d;
				cache_luma_out <= cache_rdata_luma;
				
			end
		end
		else if(ref_pix_axi_r_valid & ref_pix_axi_r_ready) begin
			if(ref_pix_axi_r_last) begin
				// synthesis translate_off
					if(miss_elem_fifo_empty ) begin
						$display("%d miss fifo cant be empty at this stage",$time);
						$stop;
					end
				// synthesis translate_on
				if(luma_dest_enable_reg_read) begin
					dest_fill_x_mask_luma_output <= dest_fill_x_mask_luma_read;
					dest_fill_y_mask_luma_output <= dest_fill_y_mask_luma_read;
					dest_fill_y_loc_luma_output <= dest_fill_y_loc_luma_read;
					dest_fill_x_loc_luma_output <= dest_fill_x_loc_luma_read;
					cache_luma_out <= cache_wdata_luma;
					
				end
				// else if(data_read_stage_valid) begin
					// if(luma_dest_enable_reg_d) begin
						// if(dest_fill_x_mask_luma_d[j] & dest_fill_y_mask_luma_d[i]) begin
							// block_11x11[i][j] <= cache_rdata_arr[(CACHE_LINE_LUMA_OFFSET/LUMA_BITS) + (dest_fill_y_loc_luma_d[i] * (1<<C_L_H_SIZE) + dest_fill_x_loc_luma_d[j])];
						// end
					// end
				// end
			end
		end
	
		if(data_read_stage_valid) begin
			if(chma_dest_enable_reg_d) begin
					dest_fill_x_loc_chma_output <= dest_fill_x_loc_chma_hit_d;
					dest_fill_y_loc_chma_output <= dest_fill_y_loc_chma_hit_d;
					dest_fill_x_mask_chma_output <= dest_fill_x_mask_chma_d;
					dest_fill_y_mask_chma_output <= dest_fill_y_mask_chma_d;
					cache_cb_out <= cache_rdata_cb;
					cache_cr_out <= cache_rdata_cr;
				// if(dest_fill_x_mask_chma_d[j] & dest_fill_y_mask_chma_d[i]) begin
					// block_5x5_cb[i][j] <= cache_rdata_arr_cb[(dest_fill_y_loc_chma_d[i] * (1<<C_L_H_SIZE_C) + dest_fill_x_loc_chma_d[j])];
					// block_5x5_cr[i][j] <= cache_rdata_arr_cr[(dest_fill_y_loc_chma_d[i] * (1<<C_L_H_SIZE_C) + dest_fill_x_loc_chma_d[j])];
				// end
			end
		end

		else if(ref_pix_axi_r_valid & ref_pix_axi_r_ready) begin
			if(ref_pix_axi_r_last) begin
				if(chma_dest_enable_reg_read ) begin
					dest_fill_x_mask_chma_output <= dest_fill_x_mask_chma_read;
					dest_fill_y_mask_chma_output <= dest_fill_y_mask_chma_read;
					dest_fill_x_loc_chma_output <= dest_fill_x_loc_chma_read;
					dest_fill_y_loc_chma_output <= dest_fill_y_loc_chma_read;
					cache_cb_out <= cache_wdata_cb;
					cache_cr_out <= cache_wdata_cr;
								
				end
				
			end

		end
end




always@(posedge clk) begin
	if(reset) begin
		state_data_read <= STATE_IDLE;
		data_read_stage_valid <= 0;
		block_number_4 <= 0;
	end
	else begin
		case(state_data_read) 
			STATE_IDLE: begin // we know first one is definitely a miss so update parameters from that
				curr_x_3d <= 0;
				curr_y_3d <= 0;
				data_read_stage_valid <= 0;
				if(!hit_elem_fifo_empty) begin
					if(block_number_4 == block_number_3_hit && (curr_x_hit ==0 && curr_y_hit ==0)) begin
						block_number_4 <= block_number_4 + 1'b1;
						state_data_read <= STATE_ACTIVE;
						delta_x_4 <= delta_x_hit;
						delta_y_4 <= delta_y_hit;						
						xT_in_min_tus_4 <= xT_in_min_tus_hit;
						yT_in_min_tus_4 <= yT_in_min_tus_hit;
						bi_pred_block_cache_4 <= bi_pred_block_cache_hit;
						data_read_stage_valid <= 1;
						if(delta_x_hit == 0 && delta_y_hit ==0) begin
							if(output_fifo_program_full ) begin
								state_data_read <= STATE_PASS_ONLY;
							end
							else begin
								state_data_read <= STATE_IDLE;
							end
						end
						else if(delta_x_hit ==0) begin
							curr_x_3d <= 0;
							curr_y_3d <= 1;							
						end
						else begin
							curr_x_3d <= 1;
							curr_y_3d <= 0;							
						end
						
						last_block_valid_3d <= last_block_valid_2d_hit;
						luma_dest_enable_reg_d <= luma_dest_enable_reg_hit;
						chma_dest_enable_reg_d <= chma_dest_enable_reg_hit;
						dest_fill_y_mask_chma_d <= dest_fill_y_mask_chma_hit;
						dest_fill_x_mask_chma_d <= dest_fill_x_mask_chma_hit;
						// for(j=0;j < CHMA_REF_BLOCK_WIDTH; j = j+1) begin // vertical span
							// dest_fill_y_loc_chma_d[j] <= dest_fill_y_loc_chma_hit_arry[j];
							// dest_fill_x_loc_chma_d[j] <= dest_fill_x_loc_chma_hit_arry[j];
						// end
						dest_fill_y_mask_luma_d <= dest_fill_y_mask_luma_hit;
						dest_fill_x_mask_luma_d <= dest_fill_x_mask_luma_hit;
						dest_fill_x_loc_luma_hit_d <= dest_fill_x_loc_luma_hit;
						dest_fill_x_loc_chma_hit_d <= dest_fill_x_loc_chma_hit;
						dest_fill_y_loc_luma_hit_d <= dest_fill_y_loc_luma_hit;
						dest_fill_y_loc_chma_hit_d <= dest_fill_y_loc_chma_hit;
						
						// for(j=0;j < LUMA_REF_BLOCK_WIDTH; j = j+1) begin // vertical span
							// dest_fill_y_loc_luma_d[j] <= dest_fill_y_loc_luma_hit_arry[j];
							// dest_fill_x_loc_luma_d[j] <= dest_fill_x_loc_luma_hit_arry[j];
						// end
					end
				
					else if(!miss_elem_fifo_empty) begin
						if(block_number_4 == block_number_3_read && (curr_x_read ==0 && curr_y_read ==0)) begin
						// synthesis translate_off
							if(ref_pix_axi_r_valid & ref_pix_axi_r_ready & ref_pix_axi_r_last ) begin
								$display("%d not expecting ddr data to come at this stage ",$time);
								$stop;
							end
						// synthesis translate_on
							state_data_read <= STATE_ACTIVE;
							block_number_4 <= block_number_4 + 1'b1;
							delta_x_4 <= delta_x_read;
							delta_y_4 <= delta_y_read;						
							xT_in_min_tus_4 <= xT_in_min_tus_read;
							yT_in_min_tus_4 <= yT_in_min_tus_read;
							bi_pred_block_cache_4 <= bi_pred_block_cache_read;		
						end
					end
				end
				else if(!miss_elem_fifo_empty) begin
					if(block_number_4 == block_number_3_read && (curr_x_read ==0 && curr_y_read ==0)) begin
					// synthesis translate_off
						if(ref_pix_axi_r_valid & ref_pix_axi_r_ready & ref_pix_axi_r_last ) begin
							$display("%d not expecting ddr data to come at this stage ",$time);
							$stop;
						end
					// synthesis translate_on
						state_data_read <= STATE_ACTIVE;
						block_number_4 <= block_number_4 + 1'b1;
						delta_x_4 <= delta_x_read;
						delta_y_4 <= delta_y_read;						
						xT_in_min_tus_4 <= xT_in_min_tus_read;
						yT_in_min_tus_4 <= yT_in_min_tus_read;
						bi_pred_block_cache_4 <= bi_pred_block_cache_read;		
					end
				end
			end
			STATE_ACTIVE: begin
				data_read_stage_valid <= 0;
				if(!hit_elem_fifo_empty) begin
					if( (curr_x_3d == curr_x_hit) && ((curr_y_3d == curr_y_hit)) 
						&& (xT_in_min_tus_4 == xT_in_min_tus_hit) && (yT_in_min_tus_4 == yT_in_min_tus_hit) && (bi_pred_block_cache_4 == bi_pred_block_cache_hit)) begin // check if next curr_xy need to be added here 
							curr_x_3d <= next_curr_x_3d;
							curr_y_3d <= next_curr_y_3d;	
							data_read_stage_valid <= 1;	
							if(curr_x_3d == delta_x_4 && curr_y_3d == delta_y_4) begin
								if(output_fifo_program_full ) begin
									state_data_read <= STATE_PASS_ONLY;
								end
								else begin
									state_data_read <= STATE_IDLE;
								end
							end		
					
							last_block_valid_3d <= last_block_valid_2d_hit;
							luma_dest_enable_reg_d <= luma_dest_enable_reg_hit;
							chma_dest_enable_reg_d <= chma_dest_enable_reg_hit;
							dest_fill_y_mask_chma_d <= dest_fill_y_mask_chma_hit;
							dest_fill_x_mask_chma_d <= dest_fill_x_mask_chma_hit;
							// for(j=0;j < CHMA_REF_BLOCK_WIDTH; j = j+1) begin // vertical span
								// dest_fill_y_loc_chma_d[j] <= dest_fill_y_loc_chma_hit_arry[j];
								// dest_fill_x_loc_chma_d[j] <= dest_fill_x_loc_chma_hit_arry[j];
							// end
							dest_fill_y_mask_luma_d <= dest_fill_y_mask_luma_hit;
							dest_fill_y_mask_chma_d <= dest_fill_y_mask_chma_hit;
							dest_fill_x_loc_luma_hit_d <= dest_fill_x_loc_luma_hit;
							dest_fill_x_loc_chma_hit_d <= dest_fill_x_loc_chma_hit;
							dest_fill_y_loc_luma_hit_d <= dest_fill_y_loc_luma_hit;
							dest_fill_y_loc_chma_hit_d <= dest_fill_y_loc_chma_hit;
							
							// for(j=0;j < LUMA_REF_BLOCK_WIDTH; j = j+1) begin // vertical span
								// dest_fill_y_loc_luma_d[j] <= dest_fill_y_loc_luma_hit_arry[j];
								// dest_fill_x_loc_luma_d[j] <= dest_fill_x_loc_luma_hit_arry[j];
							// end
				
					end
					else if(!miss_elem_fifo_empty) begin
						if( (curr_x_3d == curr_x_read) && ((curr_y_3d == curr_y_read)) 
							&& (xT_in_min_tus_4 == xT_in_min_tus_read) && (yT_in_min_tus_4 == yT_in_min_tus_read) && (bi_pred_block_cache_4 == bi_pred_block_cache_read)) begin // check if next curr_xy need to be added here 
							if(ref_pix_axi_r_valid & ref_pix_axi_r_ready) begin
								if(ref_pix_axi_r_last) begin
									curr_x_3d <= next_curr_x_3d;
									curr_y_3d <= next_curr_y_3d;
									if(curr_x_3d == delta_x_4 && curr_y_3d == delta_y_4) begin
										if(output_fifo_program_full ) begin
											state_data_read <= STATE_PASS_ONLY;
										end
										else begin
											state_data_read <= STATE_IDLE;
										end								
									end
								end
							end
						end
					end
				end
				else if(!miss_elem_fifo_empty) begin
					if( (curr_x_3d == curr_x_read) && ((curr_y_3d == curr_y_read)) 
						&& (xT_in_min_tus_4 == xT_in_min_tus_read) && (yT_in_min_tus_4 == yT_in_min_tus_read) && (bi_pred_block_cache_4 == bi_pred_block_cache_read)) begin // check if next curr_xy need to be added here 
						if(ref_pix_axi_r_valid & ref_pix_axi_r_ready) begin
							if(ref_pix_axi_r_last) begin
								curr_x_3d <= next_curr_x_3d;
								curr_y_3d <= next_curr_y_3d;
								if(curr_x_3d == delta_x_4 && curr_y_3d == delta_y_4) begin
									if(output_fifo_program_full ) begin
										state_data_read <= STATE_PASS_ONLY;
									end
									else begin
										state_data_read <= STATE_IDLE;
									end								
								end
							end
						end
					end
				end
				
			end
			STATE_PASS_ONLY: begin
				data_read_stage_valid <= 0;	
				if(!output_fifo_almost_full ) begin
					state_data_read <= STATE_IDLE;
				end
			end
		endcase
	end
end

always@(*) begin
    if(curr_x_3d == delta_x_4) begin
        next_curr_x_3d = 0;
        next_curr_y_3d = curr_y_3d + 1'b1;
    end
    else begin
        next_curr_x_3d = curr_x_3d + 1'b1;
        next_curr_y_3d = curr_y_3d;
    end
end



always@(*) begin
    cache_wr_en = 0;
    cache_w_addr = {(SET_ADDR_WDTH+C_N_WAY) {1'bx}};	
	if(ref_pix_axi_r_valid & ref_pix_axi_r_ready) begin
		if(ref_pix_axi_r_last) begin
			cache_wr_en = 1;
			cache_w_addr = {set_addr_read,set_idx_miss_read};	
		end	
	end	

end
always@(*) begin
	cache_r_addr = {(SET_ADDR_WDTH+C_N_WAY) {1'bx}};
	hit_elem_fifo_rd_en = 0;
	case(state_data_read)
		STATE_IDLE: begin
			if(!hit_elem_fifo_empty) begin
				if(block_number_4 == block_number_3_hit && (curr_x_hit ==0 && curr_y_hit ==0)) begin
					cache_r_addr = {set_addr_hit,set_idx_hit};
					hit_elem_fifo_rd_en = 1;
				end
			end
		end
		STATE_ACTIVE: begin
			if(!hit_elem_fifo_empty) begin
				if( (curr_x_3d == curr_x_hit) && ((curr_y_3d == curr_y_hit)) 
					&& (xT_in_min_tus_4 == xT_in_min_tus_hit) && (yT_in_min_tus_4 == yT_in_min_tus_hit) && (bi_pred_block_cache_4 == bi_pred_block_cache_hit)) begin
					cache_r_addr = {set_addr_hit,set_idx_hit};
					hit_elem_fifo_rd_en = 1;
				end
			end
		end
	endcase
end	
	
always@(posedge clk) begin
	if(reset) begin
		age_wr_en <= 0;
	end
	else begin
		case(state_tag_compare) 
			STATE_IDLE: begin
				if(dest_enable_wire_valid) begin
					age_wr_en <= 1;
				end
				else begin
					age_wr_en <= 0;
				end
			end
			default: begin
				age_wr_en <= 0;
			end
		endcase
	end
end

// always@(*) begin
	// age_wr_en = hit_elem_fifo_wr_en | miss_elem_fifo_wr_en;
// end

always@(*) begin
	miss_elem_fifo_rd_en = 0;
	if(ref_pix_axi_r_valid & ref_pix_axi_r_ready) begin
		if(ref_pix_axi_r_last) begin
			miss_elem_fifo_rd_en = 1;
		end
		else begin
			miss_elem_fifo_rd_en = 0;
		end
	end
end

always@(posedge clk) begin
	if(ref_pix_axi_r_valid & ref_pix_axi_r_ready) begin
		if(!ref_pix_axi_r_last) begin
			if(C_SIZE ==13) begin
				cache_w_port_old_reg[CACHE_LINE_WDTH*PIXEL_BITS/2 -1:0]	<= ref_pix_axi_r_data[CACHE_LINE_WDTH*PIXEL_BITS/2 -1:0]	    ;
			end
		end
	end
end

always@(posedge clk) begin
	d_block_x_offset_luma_4 <= d_block_x_offset_luma_hit;
	d_block_y_offset_luma_4 <= d_block_y_offset_luma_hit;
	d_block_x_offset_chma_4 <= d_block_x_offset_chma_hit;
	d_block_y_offset_chma_4 <= d_block_y_offset_chma_hit; 
	
	d_block_x_end_luma_4 <= d_block_x_end_luma_hit;
	d_block_y_end_luma_4 <= d_block_y_end_luma_hit;
	d_block_x_end_chma_4 <= d_block_x_end_chma_hit;
	d_block_y_end_chma_4 <= d_block_y_end_chma_hit; 

	luma_ref_start_x_4 <= luma_ref_start_x_hit;
	luma_ref_start_y_4 <= luma_ref_start_y_hit;
	chma_ref_start_x_4 <= chma_ref_start_x_hit;
	chma_ref_start_y_4 <= chma_ref_start_y_hit;
		 
	chma_ref_width_x_4  <= chma_ref_width_x_hit;
	chma_ref_height_y_4 <= chma_ref_height_y_hit;
	luma_ref_width_x_4  <= luma_ref_width_x_hit;
	luma_ref_height_y_4 <= luma_ref_height_y_hit;

	res_present_4 <= res_present_hit;
	x0_tu_end_in_min_tus_4 <= x0_tu_end_in_min_tus_hit;
	y0_tu_end_in_min_tus_4 <= y0_tu_end_in_min_tus_hit;
	
	d_frac_y_out_4 <= d_frac_y_out_hit;
	d_frac_x_out_4 <= d_frac_x_out_hit;
end
always@(posedge clk) begin
		if ( data_read_stage_valid & last_block_valid_3d)begin
			block_x_offset_luma <= d_block_x_offset_luma_4;
			block_y_offset_luma <= d_block_y_offset_luma_4;
			block_x_offset_chma <= d_block_x_offset_chma_4;
			block_y_offset_chma <= d_block_y_offset_chma_4; 
			
			block_x_end_luma <= d_block_x_end_luma_4;
			block_y_end_luma <= d_block_y_end_luma_4;
			block_x_end_chma <= d_block_x_end_chma_4;
			block_y_end_chma <= d_block_y_end_chma_4; 
		
			luma_ref_start_x_out <= luma_ref_start_x_4  ;
			luma_ref_start_y_out <= luma_ref_start_y_4  ;
			chma_ref_start_x_out <= chma_ref_start_x_4  ;
			chma_ref_start_y_out <= chma_ref_start_y_4  ;
				 
			chma_ref_width_x_out  <= chma_ref_width_x_4;
			chma_ref_height_y_out <= chma_ref_height_y_4;
			luma_ref_width_x_out  <= luma_ref_width_x_4;
			luma_ref_height_y_out <= luma_ref_height_y_4;

			res_present_out <= res_present_4;
			x0_tu_end_in_min_tus_out <= x0_tu_end_in_min_tus_4;
			y0_tu_end_in_min_tus_out <= y0_tu_end_in_min_tus_4;
			
			ch_frac_y_out <= d_frac_y_out_4;
			ch_frac_x_out <= d_frac_x_out_4;
			
			bi_pred_block_cache_out  <= bi_pred_block_cache_4;
			xT_in_min_tus_out <= xT_in_min_tus_4;
			yT_in_min_tus_out <= yT_in_min_tus_4;
		end
		else if((miss_elem_fifo_rd_en & last_block_valid_2d_read))begin
			block_x_offset_luma <= d_block_x_offset_luma_read;
			block_y_offset_luma <= d_block_y_offset_luma_read;
			block_x_offset_chma <= d_block_x_offset_chma_read;
			block_y_offset_chma <= d_block_y_offset_chma_read; 
			
			block_x_end_luma <= d_block_x_end_luma_read;
			block_y_end_luma <= d_block_y_end_luma_read;
			block_x_end_chma <= d_block_x_end_chma_read;
			block_y_end_chma <= d_block_y_end_chma_read; 
		
			luma_ref_start_x_out <= luma_ref_start_x_read  ;
			luma_ref_start_y_out <= luma_ref_start_y_read  ;
			chma_ref_start_x_out <= chma_ref_start_x_read  ;
			chma_ref_start_y_out <= chma_ref_start_y_read  ;
				 
			chma_ref_width_x_out  <= chma_ref_width_x_read;
			chma_ref_height_y_out <= chma_ref_height_y_read;
			luma_ref_width_x_out  <= luma_ref_width_x_read;
			luma_ref_height_y_out <= luma_ref_height_y_read;

			res_present_out <= res_present_read;
			x0_tu_end_in_min_tus_out <= x0_tu_end_in_min_tus_read;
			y0_tu_end_in_min_tus_out <= y0_tu_end_in_min_tus_read;

			ch_frac_y_out <= d_frac_y_out_read;
			ch_frac_x_out <= d_frac_x_out_read;	

			bi_pred_block_cache_out  <= bi_pred_block_cache_read;
			xT_in_min_tus_out <= xT_in_min_tus_read;
			yT_in_min_tus_out <= yT_in_min_tus_read;			
		end

end

always@(posedge clk) begin
		if ( data_read_stage_valid )begin
			
		end
		else if((miss_elem_fifo_rd_en ))begin
						
		end

end

endmodule
