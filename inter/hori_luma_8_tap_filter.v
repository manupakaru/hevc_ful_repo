`timescale 1ns / 1ps
module hori_luma_8_tap_filter(
    clk,
    filt_type,
    a_ref_pixel_in ,
    b_ref_pixel_in ,
    c_ref_pixel_in ,
    d_ref_pixel_in ,
    e_ref_pixel_in ,
    f_ref_pixel_in ,
    g_ref_pixel_in ,
    h_ref_pixel_in ,
    pred_pixel_out
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"
    
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
	parameter IN_PIXEL_WIDTH = 8;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam SHIFT2 = 3'd6;
	parameter   SHIFT4 = 4'd8;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input [1:0] filt_type;
    input [IN_PIXEL_WIDTH -1: 0] a_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] b_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] c_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] d_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] e_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] f_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] g_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] h_ref_pixel_in;
    output reg [FILTER_PIXEL_WIDTH -1:0]pred_pixel_out;

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    reg [IN_PIXEL_WIDTH -1: 0] a_ref_pixel_d;
	
    reg [IN_PIXEL_WIDTH -1: 0] a_ref_pixel;
    reg [IN_PIXEL_WIDTH -1: 0] b_ref_pixel;
    reg [IN_PIXEL_WIDTH -1: 0] c_ref_pixel;
    reg [IN_PIXEL_WIDTH -1: 0] d_ref_pixel;
    reg [IN_PIXEL_WIDTH -1: 0] e_ref_pixel;
    reg [IN_PIXEL_WIDTH -1: 0] f_ref_pixel;
    reg [IN_PIXEL_WIDTH -1: 0] g_ref_pixel;
    reg [IN_PIXEL_WIDTH -1: 0] h_ref_pixel;
	
	
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module

reg signed [IN_PIXEL_WIDTH + 7 -1: 0] com_1_level1_adder;
reg signed [IN_PIXEL_WIDTH + 7 -1: 0] com_2_level1_adder;
reg signed [IN_PIXEL_WIDTH + 7 -1: 0] com_3_level1_adder;
reg signed [IN_PIXEL_WIDTH + 7 -1: 0] com_1_level2_adder;
reg signed [IN_PIXEL_WIDTH + 7 -1: 0] com_2_level2_adder;
reg signed [IN_PIXEL_WIDTH + 7 -1: 0] com_1_level3_adder;

reg signed [IN_PIXEL_WIDTH +7 -1: 0] type1_1_level1_adder;
reg signed [IN_PIXEL_WIDTH +7 -1: 0] type1_2_level1_adder;
reg signed [IN_PIXEL_WIDTH +7 -1: 0] type1_3_level1_adder;
reg signed [IN_PIXEL_WIDTH +7 -1: 0] type1_1_level2_adder;
reg signed [IN_PIXEL_WIDTH +7 -1: 0] type1_1_level3_adder;


reg signed [IN_PIXEL_WIDTH +7 -1: 0] type2_1_level1_adder;
reg signed [IN_PIXEL_WIDTH +7 -1: 0] type2_2_level1_adder;
reg signed [IN_PIXEL_WIDTH +7 -1: 0] type2_3_level1_adder;
reg signed [IN_PIXEL_WIDTH +7 -1: 0] type2_1_level2_adder;
reg signed [IN_PIXEL_WIDTH +7 -1: 0] type2_2_level2_adder;
reg signed [IN_PIXEL_WIDTH +7 -1: 0] type2_1_level3_adder;

reg [1:0] filt_type_d;


always@(*) begin
	if(filt_type != 2'b11) begin
		a_ref_pixel = a_ref_pixel_in;
		b_ref_pixel = b_ref_pixel_in;
		c_ref_pixel = c_ref_pixel_in;
		d_ref_pixel = d_ref_pixel_in;
		e_ref_pixel = e_ref_pixel_in;
		f_ref_pixel = f_ref_pixel_in;
		g_ref_pixel = g_ref_pixel_in;
		h_ref_pixel = h_ref_pixel_in;
	end
	else begin
		a_ref_pixel = h_ref_pixel_in;
		b_ref_pixel = g_ref_pixel_in;
		c_ref_pixel = f_ref_pixel_in;
		d_ref_pixel = e_ref_pixel_in;
		e_ref_pixel = d_ref_pixel_in;
		f_ref_pixel = c_ref_pixel_in;
		g_ref_pixel = b_ref_pixel_in;
		h_ref_pixel = a_ref_pixel_in;	
	end
end
always@(posedge clk) begin
    filt_type_d <= filt_type;
	a_ref_pixel_d <= a_ref_pixel_in;
end


always@(*) begin
    type2_1_level1_adder = ((-(f_ref_pixel))<<1) + ((g_ref_pixel) << 2); 
    type2_2_level1_adder = (((c_ref_pixel)) + ((h_ref_pixel)));   //+1
    type2_3_level1_adder = (e_ref_pixel) + (-(f_ref_pixel));  //+1
    
    
end


always@(posedge clk) begin
    type2_1_level2_adder <= (-type2_2_level1_adder) + ((e_ref_pixel)<<5);  
    type2_2_level2_adder <= type2_1_level1_adder + (type2_3_level1_adder<<3);
end

always@(*) begin
    type2_1_level3_adder = type2_1_level2_adder + type2_2_level2_adder;
end


always@(*) begin
    
    type1_2_level1_adder = (e_ref_pixel) + ((d_ref_pixel) << 1);
    type1_3_level1_adder = ((-(f_ref_pixel)) << 2) + (g_ref_pixel);

    
end

always@(posedge clk) begin
    type1_1_level2_adder <= (type1_2_level1_adder) + (type1_3_level1_adder);
    type1_1_level1_adder <= (d_ref_pixel ) + (e_ref_pixel);
end

always@(*) begin
    type1_1_level3_adder = (type1_1_level2_adder) + (type1_1_level1_adder << 4);
end



always@(*) begin    
    com_1_level1_adder = (a_ref_pixel) + (f_ref_pixel);
    com_2_level1_adder = (-(c_ref_pixel)) + (d_ref_pixel);
    com_3_level1_adder =  ((-(c_ref_pixel))<<1) + ((b_ref_pixel)<<2);
    
    // com_1_level2_adder = (-com_1_level1_adder) + (com_2_level1_adder<<3);
    // com_2_level2_adder = (d_ref_pixel<<5) + (com_3_level1_adder);
    
    
end

always@(posedge clk) begin
    com_1_level2_adder <= (-com_1_level1_adder) + (com_2_level1_adder<<3);
    com_2_level2_adder <= ((d_ref_pixel)<<5) + (com_3_level1_adder);
end


always@(*) begin
    com_1_level3_adder = com_1_level2_adder + com_2_level2_adder;
end


always@(posedge clk) begin
	if(filt_type_d == 2'b00) begin
		pred_pixel_out <= {{SHIFT4{1'b0}},a_ref_pixel_d};
	end
    else if(filt_type_d == 2'b10) begin
        pred_pixel_out <= (com_1_level3_adder + type2_1_level3_adder);
    end
    else begin
        pred_pixel_out <= com_1_level3_adder + type1_1_level3_adder;
    end
    
end

endmodule 