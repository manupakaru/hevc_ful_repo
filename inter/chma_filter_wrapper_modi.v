`timescale 1ns / 1ps
module chma_filter_wrapper_modi(
    clk,
    reset,
    bi_pred_block_filter_in,
    bi_pred_block_filter_out,
    x0_tu_end_in_min_tus_in,
    x0_tu_end_in_min_tus_out,
    y0_tu_end_in_min_tus_in,
    y0_tu_end_in_min_tus_out,

    ref_c_start_x,
    ref_c_start_y,
    ref_c_height,
    ref_c_width,
    block_start_x_ch,
    block_start_y_ch,
    block_end_x_ch,
    block_end_y_ch,
    xT_4x4_in,
    yT_4x4_in,     
    c_frac_x,
    c_frac_y,
    
    ref_pix_l_in,    
    valid_in,
    
    pic_width,
    pic_height,
    
    block_ready_out,
    filter_idle_out,
    hori_vert_idle_out,
    
    pred_pix_out_4x4,
    xT_4x4_out,
    yT_4x4_out
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter   REF_BLOCK_WIDTH = 4'd5;
    parameter   BLOCK_WIDTH_2x2 = 2'd2;
    parameter   FRAC_WIDTH = 2'd3;
    
    parameter   SHIFT4 = 4'd8;
    parameter   SHIFT3 = 3'd6;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

    
    parameter                           LUMA_DIM_WDTH		    	= 4;        // out block dimension  max 11
	parameter                           CHMA_DIM_WDTH               = 3;        // max 5 
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
    input 	bi_pred_block_filter_in;
    output  bi_pred_block_filter_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      x0_tu_end_in_min_tus_in;
    output  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] 	x0_tu_end_in_min_tus_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      y0_tu_end_in_min_tus_in;
    output [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] 	y0_tu_end_in_min_tus_out;

    input signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0]   ref_c_start_x;
    input signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0]   ref_c_start_y;
    input [LUMA_DIM_WDTH -1: 0]                 ref_c_height;
    input [LUMA_DIM_WDTH -1: 0]                 ref_c_width;
    input [CHMA_DIM_WDTH -1: 0]                 block_start_x_ch;
    input [CHMA_DIM_WDTH -1: 0]                 block_start_y_ch;
    input [CHMA_DIM_WDTH -1: 0]                 block_end_x_ch;
    input [CHMA_DIM_WDTH -1: 0]                 block_end_y_ch;
    input                           valid_in;
    
    input [PIC_WIDTH_WIDTH -1:0]          pic_width;
    input [PIC_WIDTH_WIDTH -1:0]          pic_height;
    
    input [REF_BLOCK_WIDTH* REF_BLOCK_WIDTH * PIXEL_WIDTH -1:0] ref_pix_l_in;
    output  block_ready_out;
    output  filter_idle_out;
    output hori_vert_idle_out;
    
    input [FRAC_WIDTH -1: 0] c_frac_x;
    input [FRAC_WIDTH -1: 0] c_frac_y;
    
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4_in;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4_in;
    
    output  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4_out;
    output  [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4_out;
    
    output  [BLOCK_WIDTH_2x2 *  BLOCK_WIDTH_2x2 * FILTER_PIXEL_WIDTH -1:0]  pred_pix_out_4x4;      
    

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module

	wire hori_valid;
	wire hori_res_present;
	wire hori_bi_pred_block_filter;
	wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] hori_x0_tu_end_in_min_tus;
	wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] hori_y0_tu_end_in_min_tus;
	wire [FRAC_WIDTH -1 :0] hori_frac_y;
	wire [FRAC_WIDTH -1 :0] hori_frac_x;
	wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] hori_xT_4x4;
    wire [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] hori_yT_4x4;
	
    wire [FILTER_PIXEL_WIDTH-1:0] hori_filter1_in_1;
    wire [FILTER_PIXEL_WIDTH-1:0] hori_filter1_in_2;
    wire [FILTER_PIXEL_WIDTH-1:0] hori_filter1_in_3;
    wire [FILTER_PIXEL_WIDTH-1:0] hori_filter1_in_4;
    wire [FILTER_PIXEL_WIDTH-1:0] hori_filter1_in_5;

	wire vert_idle_out;
	wire hori_idle_out;
	wire vert_full_idle;
	
	assign filter_idle_out =  hori_idle_out;
	assign hori_vert_idle_out = hori_idle_out & vert_full_idle;

// Instantiate the module
chroma_hori_5_filters hori_filter_module (						
    .clk		(clk),                                         
    .reset		(reset),                                       
    .valid_in	(valid_in),                                    
    .valid_out(hori_valid),                                    
    .ready_out(hori_idle_out),                                 
    .c_frac_x(c_frac_x),                                       
    .c_frac_y(c_frac_y),                                       
    .ref_c_start_x(ref_c_start_x),                             
    .ref_c_start_y(ref_c_start_y),                             
    .ref_c_height(ref_c_height),                               
    .ref_c_width(ref_c_width),                                 
    .block_start_x_ch(block_start_x_ch),                       
    .block_start_y_ch(block_start_y_ch),                       
    .block_end_x_ch(block_end_x_ch),                           
    .block_end_y_ch(block_end_y_ch),                        
    .bi_pred_block_filter_in(bi_pred_block_filter_in),         
    .bi_pred_block_filter_out(hori_bi_pred_block_filter),      
    .x0_tu_end_in_min_tus_in(x0_tu_end_in_min_tus_in),         
    .x0_tu_end_in_min_tus_out(hori_x0_tu_end_in_min_tus),      
    .y0_tu_end_in_min_tus_in(y0_tu_end_in_min_tus_in),         
    .y0_tu_end_in_min_tus_out(hori_y0_tu_end_in_min_tus),      
    .ref_pix_l_in(ref_pix_l_in),                               
    .frac_y_out(hori_frac_y),                                  
    .frac_x_out(hori_frac_x),                                  
    .pic_width(pic_width),                                     
    .pic_height(pic_height),      
	.vert_idle_in(vert_idle_out),
    .filter_1_out(hori_filter1_in_1),                       	
    .filter_2_out(hori_filter1_in_2),                       	
    .filter_3_out(hori_filter1_in_3),                       	
    .filter_4_out(hori_filter1_in_4),                       	
    .filter_5_out(hori_filter1_in_5),                                	
    .xT_4x4_in(xT_4x4_in),                                     
    .yT_4x4_in(yT_4x4_in),                                     
    .xT_4x4_out(hori_xT_4x4),                                  
    .yT_4x4_out(hori_yT_4x4)                                   
    );                                                         

// Instantiate the module
chroma_vert_2_filter_module vert_filter_module (				
    .clk(clk),                                                  
    .reset(reset),                                              
    .bi_pred_block_filter_in(hori_bi_pred_block_filter),        
    .bi_pred_block_filter_out(bi_pred_block_filter_out),        
    .x0_tu_end_in_min_tus_in(hori_x0_tu_end_in_min_tus),        
    .x0_tu_end_in_min_tus_out(x0_tu_end_in_min_tus_out),        
    .y0_tu_end_in_min_tus_in(hori_y0_tu_end_in_min_tus),        
    .y0_tu_end_in_min_tus_out(y0_tu_end_in_min_tus_out),       
    .xT_4x4_in(hori_xT_4x4),                                    
    .yT_4x4_in(hori_yT_4x4),                                    
    .c_frac_y(hori_frac_y),                                     
    .c_frac_x(hori_frac_x),                                     
    .valid_in(hori_valid),                                      
    .filter_in_0(hori_filter1_in_1),                            
    .filter_in_1(hori_filter1_in_2),                            
    .filter_in_2(hori_filter1_in_3),                            
    .filter_in_3(hori_filter1_in_4),                            
    .filter_in_4(hori_filter1_in_5),                          
    .valid_out(block_ready_out),                                
    .vert_idle_out(vert_idle_out),
    .vert_full_idle_out(vert_full_idle),                              
    .pred_pix_out_4x4(pred_pix_out_4x4), 
    .xT_4x4_out(xT_4x4_out), 
    .yT_4x4_out(yT_4x4_out)
    );

endmodule 