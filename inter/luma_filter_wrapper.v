`timescale 1ns / 1ps
module luma_filter_wrapper(
    clk,
    reset,
    bi_pred_block_filter_in,
    bi_pred_block_filter_out,
    x0_tu_end_in_min_tus_in,
    x0_tu_end_in_min_tus_out,
    y0_tu_end_in_min_tus_in,
    y0_tu_end_in_min_tus_out,

    res_present_in,
    res_present_out,
    ref_l_start_x ,
    ref_l_start_y,
    ref_l_height,
    ref_l_width,
    block_start_x,
    block_start_y,
    block_end_x,
    block_end_y,
    xT_4x4_in,
    yT_4x4_in,    
    l_frac_x,
    l_frac_y,
    
    ref_pix_l_in,    
    valid_in,
    
    pic_width,
    pic_height,
    
    block_ready_out,
    filter_idle_out,

    
    pred_pix_out_4x4,
    xT_4x4_out,
    yT_4x4_out
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter   REF_BLOCK_WIDTH = 4'd11;
    parameter   BLOCK_WIDTH_4x4 = 3'd4;
    parameter   FRAC_WIDTH = 2'd2;
    
    parameter   SHIFT4 = 4'd8;
    parameter   SHIFT3 = 3'd6;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam STATE_IDLE = 0;
    localparam STATE_FIL_START = 1;
    localparam STATE_CORNER_FILL = 2;
    localparam STATE_HOR_FILT1 = 3;
    localparam STATE_HOR_FILT2 = 4;
    localparam STATE_HOR_FILT3 = 5;
    localparam STATE_HOR_FILT4 = 6;
    localparam STATE_HOR_FILT5 = 7;
    localparam STATE_HOR_FILT6 = 8;
    localparam STATE_VER_FILT1 = 9;
    localparam STATE_VER_FILT2 = 10;
    localparam STATE_VER_FILT3 = 11;
    localparam STATE_VER_FILT4 = 12;
    localparam STATE_VER_FILT5 = 13;
    localparam STATE_VER_FILT6 = 14;
    
    parameter                           LUMA_DIM_WDTH		    	= 4;        // out block dimension  max 11

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
    input res_present_in;
    output reg res_present_out;
    input bi_pred_block_filter_in;
    output reg bi_pred_block_filter_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      x0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      y0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_out;

    input signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0]   ref_l_start_x;
    input signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0]  ref_l_start_y;
    input [LUMA_DIM_WDTH -1: 0]                 ref_l_height;
    input [LUMA_DIM_WDTH -1: 0]                 ref_l_width;
    input [LUMA_DIM_WDTH -1: 0]                 block_start_x;
    input [LUMA_DIM_WDTH -1: 0]                 block_start_y;
    input [LUMA_DIM_WDTH -1: 0]                 block_end_x;
    input [LUMA_DIM_WDTH -1: 0]                 block_end_y;
    input                           valid_in;
    
    input [PIC_WIDTH_WIDTH -1:0]          pic_width;
    input [PIC_WIDTH_WIDTH -1:0]          pic_height;
    
    input [REF_BLOCK_WIDTH* REF_BLOCK_WIDTH * PIXEL_WIDTH -1:0] ref_pix_l_in;
    output reg block_ready_out;
    output reg filter_idle_out;
    
    input [FRAC_WIDTH -1: 0] l_frac_x;
    input [FRAC_WIDTH -1: 0] l_frac_y;
    
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4_in;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4_in;
    
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4_out;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4_out;
    
    output  [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 * FILTER_PIXEL_WIDTH -1:0]  pred_pix_out_4x4;      
    

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    integer filter_state;
    integer next_state;
    
    wire [PIXEL_WIDTH - 1:0] in_store_wire[REF_BLOCK_WIDTH - 1: 0][REF_BLOCK_WIDTH -1: 0];
    
    reg [PIXEL_WIDTH - 1:0] in_store[REF_BLOCK_WIDTH - 1: 0][REF_BLOCK_WIDTH -1: 0];
    reg [FILTER_PIXEL_WIDTH - 1:0] out_store[BLOCK_WIDTH_4x4 - 1: 0][BLOCK_WIDTH_4x4 -1: 0];
    reg [FILTER_PIXEL_WIDTH - 1:0] out_store_alt[REF_BLOCK_WIDTH - 1: BLOCK_WIDTH_4x4][BLOCK_WIDTH_4x4 -1: 0];
    
    reg [FILTER_PIXEL_WIDTH -1:0] filter1_in_1 [REF_BLOCK_WIDTH -1: 0];
    reg [FILTER_PIXEL_WIDTH -1:0] filter1_in_2 [REF_BLOCK_WIDTH -1: 0];
    reg [FILTER_PIXEL_WIDTH -1:0] filter1_in_3 [REF_BLOCK_WIDTH -1: 0];
    reg [FILTER_PIXEL_WIDTH -1:0] filter1_in_4 [REF_BLOCK_WIDTH -1: 0];
    
    reg [FILTER_PIXEL_WIDTH -1:0] filter2_in_1 [REF_BLOCK_WIDTH -1: 0];
    reg [FILTER_PIXEL_WIDTH -1:0] filter2_in_2 [REF_BLOCK_WIDTH -1: 0];
    reg [FILTER_PIXEL_WIDTH -1:0] filter2_in_3 [REF_BLOCK_WIDTH -1: 0];
    reg [FILTER_PIXEL_WIDTH -1:0] filter2_in_4 [REF_BLOCK_WIDTH -1: 0];
    
    reg [FILTER_PIXEL_WIDTH -1:0] filter3_in_1 [REF_BLOCK_WIDTH -1: 0];
    reg [FILTER_PIXEL_WIDTH -1:0] filter3_in_2 [REF_BLOCK_WIDTH -1: 0];
    reg [FILTER_PIXEL_WIDTH -1:0] filter3_in_3 [REF_BLOCK_WIDTH -1: 0];

    

    
    reg [FRAC_WIDTH -1:0] l_frac_x_d ;//= l_frac_x[FRAC_WIDTH -2:0];
    reg [FRAC_WIDTH -1:0] l_frac_y_d ;//= l_frac_y[FRAC_WIDTH -2:0];
    
    reg [4 -1: 0]                 corner_x_start;
    reg [4 -1: 0]                 corner_x_end;
    reg [4 -1: 0]                 corner_y_start;
    reg [4 -1: 0]                 corner_y_end;
    
    reg [4 -1: 0]                 corner_idx_x;
    reg [4 -1: 0]                 corner_idx_y;
    
    wire [FILTER_PIXEL_WIDTH -1:0] filter1_out_1;
    wire [FILTER_PIXEL_WIDTH -1:0] filter1_out_2;
    wire [FILTER_PIXEL_WIDTH -1:0] filter1_out_3;
    wire [FILTER_PIXEL_WIDTH -1:0] filter1_out_4;
    wire [FILTER_PIXEL_WIDTH -1:0] filter2_out_1;
    wire [FILTER_PIXEL_WIDTH -1:0] filter2_out_2;
    wire [FILTER_PIXEL_WIDTH -1:0] filter2_out_3;
    wire [FILTER_PIXEL_WIDTH -1:0] filter2_out_4;
    wire [FILTER_PIXEL_WIDTH -1:0] filter3_out_1;
    wire [FILTER_PIXEL_WIDTH -1:0] filter3_out_2;
    wire [FILTER_PIXEL_WIDTH -1:0] filter3_out_3;
    
    reg filt_type;
    reg level;
    
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module
luma_8_tap_filter filter_1_1 (
    .clk(clk), 
    .filt_type(filt_type), 
    .level_in(level),
    .a_ref_pixel_in(filter1_in_1[0]), 
    .b_ref_pixel_in(filter1_in_1[1]), 
    .c_ref_pixel_in(filter1_in_1[2]), 
    .d_ref_pixel_in(filter1_in_1[3]), 
    .e_ref_pixel_in(filter1_in_1[4]), 
    .f_ref_pixel_in(filter1_in_1[5]), 
    .g_ref_pixel_in(filter1_in_1[6]), 
    .h_ref_pixel_in(filter1_in_1[7]), 
    .pred_pixel_out(filter1_out_1)
    );
    
luma_8_tap_filter filter_1_2 (
    .clk(clk), 
    .filt_type(filt_type), 
    .level_in(level),
    .a_ref_pixel_in(filter1_in_2[0]), 
    .b_ref_pixel_in(filter1_in_2[1]), 
    .c_ref_pixel_in(filter1_in_2[2]), 
    .d_ref_pixel_in(filter1_in_2[3]), 
    .e_ref_pixel_in(filter1_in_2[4]), 
    .f_ref_pixel_in(filter1_in_2[5]), 
    .g_ref_pixel_in(filter1_in_2[6]), 
    .h_ref_pixel_in(filter1_in_2[7]), 
    .pred_pixel_out(filter1_out_2)
    );
    
luma_8_tap_filter filter_1_3 (
    .clk(clk), 
    .filt_type(filt_type), 
    .level_in(level),
    .a_ref_pixel_in(filter1_in_3[0]), 
    .b_ref_pixel_in(filter1_in_3[1]), 
    .c_ref_pixel_in(filter1_in_3[2]), 
    .d_ref_pixel_in(filter1_in_3[3]), 
    .e_ref_pixel_in(filter1_in_3[4]), 
    .f_ref_pixel_in(filter1_in_3[5]), 
    .g_ref_pixel_in(filter1_in_3[6]), 
    .h_ref_pixel_in(filter1_in_3[7]), 
    .pred_pixel_out(filter1_out_3)
    );
    
luma_8_tap_filter filter_1_4 (
    .clk(clk), 
    .filt_type(filt_type), 
    .level_in(level),
    .a_ref_pixel_in(filter1_in_4[0]), 
    .b_ref_pixel_in(filter1_in_4[1]), 
    .c_ref_pixel_in(filter1_in_4[2]), 
    .d_ref_pixel_in(filter1_in_4[3]), 
    .e_ref_pixel_in(filter1_in_4[4]), 
    .f_ref_pixel_in(filter1_in_4[5]), 
    .g_ref_pixel_in(filter1_in_4[6]), 
    .h_ref_pixel_in(filter1_in_4[7]), 
    .pred_pixel_out(filter1_out_4)
    );
    
luma_8_tap_filter filter_2_1 (
    .clk(clk), 
    .filt_type(filt_type), 
    .level_in(level),
    .a_ref_pixel_in(filter2_in_1[0]), 
    .b_ref_pixel_in(filter2_in_1[1]), 
    .c_ref_pixel_in(filter2_in_1[2]), 
    .d_ref_pixel_in(filter2_in_1[3]), 
    .e_ref_pixel_in(filter2_in_1[4]), 
    .f_ref_pixel_in(filter2_in_1[5]), 
    .g_ref_pixel_in(filter2_in_1[6]), 
    .h_ref_pixel_in(filter2_in_1[7]), 
    .pred_pixel_out(filter2_out_1)
    );
    
luma_8_tap_filter filter_2_2 (
    .clk(clk), 
    .filt_type(filt_type), 
    .level_in(level),
    .a_ref_pixel_in(filter2_in_2[0]), 
    .b_ref_pixel_in(filter2_in_2[1]), 
    .c_ref_pixel_in(filter2_in_2[2]), 
    .d_ref_pixel_in(filter2_in_2[3]), 
    .e_ref_pixel_in(filter2_in_2[4]), 
    .f_ref_pixel_in(filter2_in_2[5]), 
    .g_ref_pixel_in(filter2_in_2[6]), 
    .h_ref_pixel_in(filter2_in_2[7]), 
    .pred_pixel_out(filter2_out_2)
    );
    
luma_8_tap_filter filter_2_3 (
    .clk(clk), 
    .filt_type(filt_type), 
    .level_in(level),
    .a_ref_pixel_in(filter2_in_3[0]), 
    .b_ref_pixel_in(filter2_in_3[1]), 
    .c_ref_pixel_in(filter2_in_3[2]), 
    .d_ref_pixel_in(filter2_in_3[3]), 
    .e_ref_pixel_in(filter2_in_3[4]), 
    .f_ref_pixel_in(filter2_in_3[5]), 
    .g_ref_pixel_in(filter2_in_3[6]), 
    .h_ref_pixel_in(filter2_in_3[7]), 
    .pred_pixel_out(filter2_out_3)
    );
    
luma_8_tap_filter filter_2_4 (
    .clk(clk), 
    .filt_type(filt_type), 
    .level_in(level),
    .a_ref_pixel_in(filter2_in_4[0]), 
    .b_ref_pixel_in(filter2_in_4[1]), 
    .c_ref_pixel_in(filter2_in_4[2]), 
    .d_ref_pixel_in(filter2_in_4[3]), 
    .e_ref_pixel_in(filter2_in_4[4]), 
    .f_ref_pixel_in(filter2_in_4[5]), 
    .g_ref_pixel_in(filter2_in_4[6]), 
    .h_ref_pixel_in(filter2_in_4[7]), 
    .pred_pixel_out(filter2_out_4)
    );
    
luma_8_tap_filter filter_3_1 (
    .clk(clk), 
    .filt_type(filt_type), 
    .level_in(level),
    .a_ref_pixel_in(filter3_in_1[0]), 
    .b_ref_pixel_in(filter3_in_1[1]), 
    .c_ref_pixel_in(filter3_in_1[2]), 
    .d_ref_pixel_in(filter3_in_1[3]), 
    .e_ref_pixel_in(filter3_in_1[4]), 
    .f_ref_pixel_in(filter3_in_1[5]), 
    .g_ref_pixel_in(filter3_in_1[6]), 
    .h_ref_pixel_in(filter3_in_1[7]), 
    .pred_pixel_out(filter3_out_1)
    );
    
luma_8_tap_filter filter_3_2 (
    .clk(clk), 
    .filt_type(filt_type), 
    .level_in(level),
    .a_ref_pixel_in(filter3_in_2[0]), 
    .b_ref_pixel_in(filter3_in_2[1]), 
    .c_ref_pixel_in(filter3_in_2[2]), 
    .d_ref_pixel_in(filter3_in_2[3]), 
    .e_ref_pixel_in(filter3_in_2[4]), 
    .f_ref_pixel_in(filter3_in_2[5]), 
    .g_ref_pixel_in(filter3_in_2[6]), 
    .h_ref_pixel_in(filter3_in_2[7]), 
    .pred_pixel_out(filter3_out_2)
    );

luma_8_tap_filter filter_3_3 (
    .clk(clk), 
    .filt_type(filt_type), 
    .level_in(level),
    .a_ref_pixel_in(filter3_in_3[0]), 
    .b_ref_pixel_in(filter3_in_3[1]), 
    .c_ref_pixel_in(filter3_in_3[2]), 
    .d_ref_pixel_in(filter3_in_3[3]), 
    .e_ref_pixel_in(filter3_in_3[4]), 
    .f_ref_pixel_in(filter3_in_3[5]), 
    .g_ref_pixel_in(filter3_in_3[6]), 
    .h_ref_pixel_in(filter3_in_3[7]), 
    .pred_pixel_out(filter3_out_3)
    );
        
    
integer i,j,k;
    generate
        genvar ii;
        genvar jj;
		for(jj=0;jj <REF_BLOCK_WIDTH ; jj=jj+1 ) begin : row_iteration1
			for(ii=0 ; ii < REF_BLOCK_WIDTH ; ii = ii+1) begin : column_iteration1
				assign in_store_wire[jj][ii] = ref_pix_l_in[(jj*REF_BLOCK_WIDTH + ii + 1)*PIXEL_WIDTH-1:(jj*REF_BLOCK_WIDTH + ii)*PIXEL_WIDTH];
			end
		end
		
		for(jj=0;jj <BLOCK_WIDTH_4x4 ; jj=jj+1 ) begin : row_iteration2
			for(ii=0 ; ii < BLOCK_WIDTH_4x4 ; ii = ii+1) begin : column_iteration2
				assign pred_pix_out_4x4[(jj*BLOCK_WIDTH_4x4 + ii + 1)*FILTER_PIXEL_WIDTH -1:(jj*BLOCK_WIDTH_4x4 + ii)*FILTER_PIXEL_WIDTH] = out_store[jj][ii];
			end
		end
	endgenerate



always@(*) begin    
    next_state = filter_state;
    filter_idle_out = 0;
    case(filter_state)
        STATE_IDLE: begin
            filter_idle_out = 1;
            if(valid_in) begin
                if ((ref_l_start_x[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1]) || (ref_l_start_y[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1]) || ((ref_l_start_x + ref_l_width) >= pic_width ) || ((ref_l_start_y + ref_l_height) >= pic_height )) begin
                    next_state = STATE_CORNER_FILL;
                end
                else begin
                    next_state = STATE_FIL_START;
                end
            end
        end
        STATE_FIL_START: begin
            if(l_frac_x_d == {FRAC_WIDTH{1'b0}} && l_frac_y_d == {FRAC_WIDTH{1'b0}}) begin
                next_state = STATE_IDLE;
            end
            else if(l_frac_x_d != {FRAC_WIDTH{1'b0}}) begin
                next_state = STATE_HOR_FILT1;
            end
            else begin // (l_frac_y != 0)
                next_state = STATE_VER_FILT1;
            end
        end
        STATE_CORNER_FILL: begin
            next_state = STATE_FIL_START;
        end
        STATE_HOR_FILT1: begin
            next_state = STATE_HOR_FILT2;
        end
        STATE_HOR_FILT2: begin
            next_state = STATE_HOR_FILT3;
        end
        STATE_HOR_FILT3: begin
           next_state = STATE_HOR_FILT4; 
        end
        STATE_HOR_FILT4: begin
            next_state = STATE_HOR_FILT5;
        end
        STATE_HOR_FILT5: begin
            next_state = STATE_HOR_FILT6;
        end
        STATE_HOR_FILT6: begin
            if(l_frac_y_d == {FRAC_WIDTH{1'b0}}) begin
                next_state = STATE_IDLE;
            end
            else begin
                next_state = STATE_VER_FILT1;
            end
        end
        STATE_VER_FILT1: begin
            next_state = STATE_VER_FILT2;
        end
        STATE_VER_FILT2: begin
            next_state = STATE_VER_FILT3;
        end
        STATE_VER_FILT3: begin
            next_state = STATE_VER_FILT4;
        end
        STATE_VER_FILT4: begin
            next_state = STATE_VER_FILT5;
        end
        STATE_VER_FILT5: begin
            next_state = STATE_VER_FILT6;
        end
        STATE_VER_FILT6: begin
            next_state = STATE_IDLE;
        end
    endcase
end


always@(posedge clk) begin
    case(filter_state)
        STATE_IDLE: begin
            block_ready_out <= 0;
            if(valid_in) begin
                xT_4x4_out <= xT_4x4_in;
                yT_4x4_out <= yT_4x4_in;
                res_present_out <= res_present_in;
                bi_pred_block_filter_out <= bi_pred_block_filter_in;
                x0_tu_end_in_min_tus_out <= x0_tu_end_in_min_tus_in;
                y0_tu_end_in_min_tus_out <= y0_tu_end_in_min_tus_in;
                l_frac_x_d <= l_frac_x;
                l_frac_y_d <= l_frac_y;
                for(j=0;j <REF_BLOCK_WIDTH ; j=j+1 ) begin
                    for(i=0 ; i < REF_BLOCK_WIDTH ; i = i+1) begin
                        // for(k=0; k< PIXEL_WIDTH ; k = k+1) begin
                            in_store[j][i] <= in_store_wire[j][i];
                        // end
                    end
                end
                if((ref_l_start_x[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1])) begin
                    corner_x_start <= 0;
                    corner_x_end <= block_start_x;
                    corner_idx_x <= block_start_x;
                end
                else if((ref_l_start_x + ref_l_width) >= pic_width ) begin
                    corner_x_start <= block_end_x;
                    corner_x_end <= REF_BLOCK_WIDTH;
                    corner_idx_x <= block_end_x;
                end
                else begin
                    corner_x_start <= REF_BLOCK_WIDTH;
                    corner_x_end <= 0;
                end
                if((ref_l_start_y[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1])) begin
                    corner_y_start <= 0;
                    corner_y_end <= block_start_y;
                    corner_idx_y <= block_start_y;
                end
                else if((ref_l_start_y + ref_l_height) >= pic_height ) begin
                    corner_y_start <= block_end_y;
                    corner_y_end <= REF_BLOCK_WIDTH;
                    corner_idx_y <= block_end_y;
                end
                else begin
                    corner_y_start <= REF_BLOCK_WIDTH;
                    corner_y_end <= 0;
                end
            end
        end
        STATE_CORNER_FILL: begin
            for(j=0;j <REF_BLOCK_WIDTH ; j=j+1 ) begin
                for(i=0 ; i < REF_BLOCK_WIDTH ; i = i+1) begin
                    if( (corner_x_start <= i) && (corner_x_end > i) && (corner_y_start <= j) && (corner_y_end > j)) begin
                        // for(k=0; k< PIXEL_WIDTH ; k = k+1) begin
                            in_store[j][i] <= in_store[corner_idx_y][corner_idx_x];
                        // end
                    end
                    else if( (corner_x_start <= i) && (corner_x_end > i)) begin
                        // for(k=0; k< PIXEL_WIDTH ; k = k+1) begin
                            in_store[j][i] <= in_store[j][corner_idx_x];
                        // end
                    end
                    else if( (corner_y_start <= j) && (corner_y_end > j)) begin
                        // for(k=0; k< PIXEL_WIDTH ; k = k+1) begin
                            in_store[j][i] <= in_store[corner_idx_y][i];
                        // end
                    end
                end
            end
        end
        STATE_FIL_START: begin
            level <= 0;
            case({l_frac_x_d,l_frac_y_d})
                4'b00_00: begin
                    block_ready_out <= 1;
                    for(j = 0; j < BLOCK_WIDTH_4x4; j=j+1) begin
                        for(i=0; i< BLOCK_WIDTH_4x4 ; i = i+1) begin
                            // for(k=0;k< PIXEL_WIDTH ; k= k+1) begin
                                out_store[j][i] <= in_store[j][i] << SHIFT3;
                            // end
                        end
                    end
                end
                4'b01_00, 4'b01_01, 4'b01_10, 4'b01_11, 4'b10_00, 4'b10_01, 4'b10_10, 4'b10_11: begin
                    filt_type <= !l_frac_x_d[0];
                    for(j=0; j< REF_BLOCK_WIDTH ; j= j+1) begin
                        filter1_in_1[j] <= {{SHIFT4{1'b0}},in_store[0][j]};
                        filter1_in_2[j] <= {{SHIFT4{1'b0}},in_store[1][j]};
                        filter1_in_3[j] <= {{SHIFT4{1'b0}},in_store[2][j]};
                        filter1_in_4[j] <= {{SHIFT4{1'b0}},in_store[3][j]};
                        
                        filter2_in_1[j] <= {{SHIFT4{1'b0}},in_store[4][j]};
                        filter2_in_2[j] <= {{SHIFT4{1'b0}},in_store[5][j]};
                        filter2_in_3[j] <= {{SHIFT4{1'b0}},in_store[6][j]};
                        filter2_in_4[j] <= {{SHIFT4{1'b0}},in_store[7][j]};
                        
                        filter3_in_1[j] <= {{SHIFT4{1'b0}},in_store[8][j]};
                        filter3_in_2[j] <= {{SHIFT4{1'b0}},in_store[9][j]};
                        filter3_in_3[j] <= {{SHIFT4{1'b0}},in_store[10][j]};
                    end
                end
                4'b11_00, 4'b11_01, 4'b11_10, 4'b11_11: begin
                    filt_type <= !l_frac_x_d[0];
                    for(j=0; j< REF_BLOCK_WIDTH-1 ; j= j+1) begin
                        filter1_in_1[j] <= {{SHIFT4{1'b0}},in_store[0][9 -j]};
                        filter1_in_2[j] <= {{SHIFT4{1'b0}},in_store[1][9 -j]};
                        filter1_in_3[j] <= {{SHIFT4{1'b0}},in_store[2][9 -j]};
                        filter1_in_4[j] <= {{SHIFT4{1'b0}},in_store[3][9 -j]};
                        
                        filter2_in_1[j] <= {{SHIFT4{1'b0}},in_store[4][9 -j]};
                        filter2_in_2[j] <= {{SHIFT4{1'b0}},in_store[5][9 -j]};
                        filter2_in_3[j] <= {{SHIFT4{1'b0}},in_store[6][9 -j]};
                        filter2_in_4[j] <= {{SHIFT4{1'b0}},in_store[7][9 -j]};
                        
                        filter3_in_1[j] <= { {SHIFT4{1'b0}},in_store[8][9 -j]};
                        filter3_in_2[j] <= { {SHIFT4{1'b0}},in_store[9][9 -j]};
                        filter3_in_3[j] <= {{SHIFT4{1'b0}},in_store[10][9 -j]};
                    end
                end
                4'b00_01, 4'b00_10: begin
                    filt_type <= !l_frac_y_d[0];
                    for(j=0; j< REF_BLOCK_WIDTH ; j= j+1) begin
                        filter1_in_1[j] <= {{SHIFT4{1'b0}},in_store[j][0]};
                        filter1_in_2[j] <= {{SHIFT4{1'b0}},in_store[j][1]};
                        filter1_in_3[j] <= {{SHIFT4{1'b0}},in_store[j][2]};
                        filter1_in_4[j] <= {{SHIFT4{1'b0}},in_store[j][3]};
                    end
                end
                4'b00_11: begin
                    filt_type <= !l_frac_y_d[0];
                    for(j=0; j< REF_BLOCK_WIDTH-1 ; j= j+1) begin
                        filter1_in_1[j] <= {{SHIFT4{1'b0}},in_store[9-j][0]};
                        filter1_in_2[j] <= {{SHIFT4{1'b0}},in_store[9-j][1]};
                        filter1_in_3[j] <= {{SHIFT4{1'b0}},in_store[9-j][2]};
                        filter1_in_4[j] <= {{SHIFT4{1'b0}},in_store[9-j][3]};
                    end
                end
            endcase
        end
        STATE_HOR_FILT1: begin
            for(j=0; j< REF_BLOCK_WIDTH -1 ; j= j+1) begin
                filter1_in_1[j] <= filter1_in_1[j+1]; // left shift
                filter1_in_2[j] <= filter1_in_2[j+1];
                filter1_in_3[j] <= filter1_in_3[j+1];
                filter1_in_4[j] <= filter1_in_4[j+1];

                filter2_in_1[j] <= filter2_in_1[j+1];
                filter2_in_2[j] <= filter2_in_2[j+1];
                filter2_in_3[j] <= filter2_in_3[j+1];
                filter2_in_4[j] <= filter2_in_4[j+1];

                filter3_in_1[j] <= filter3_in_1[j+1];
                filter3_in_2[j] <= filter3_in_2[j+1];
                filter3_in_3[j] <= filter3_in_3[j+1];
            end
        end
        STATE_HOR_FILT2: begin
            for(j=0; j< REF_BLOCK_WIDTH -1 ; j= j+1) begin
                filter1_in_1[j] <= filter1_in_1[j+1]; // left shift
                filter1_in_2[j] <= filter1_in_2[j+1];
                filter1_in_3[j] <= filter1_in_3[j+1];
                filter1_in_4[j] <= filter1_in_4[j+1];

                filter2_in_1[j] <= filter2_in_1[j+1];
                filter2_in_2[j] <= filter2_in_2[j+1];
                filter2_in_3[j] <= filter2_in_3[j+1];
                filter2_in_4[j] <= filter2_in_4[j+1];

                filter3_in_1[j] <= filter3_in_1[j+1];
                filter3_in_2[j] <= filter3_in_2[j+1];
                filter3_in_3[j] <= filter3_in_3[j+1];
            end
        end
        STATE_HOR_FILT3: begin
            for(j=0; j< REF_BLOCK_WIDTH -1 ; j= j+1) begin
                filter1_in_1[j] <= filter1_in_1[j+1]; // left shift
                filter1_in_2[j] <= filter1_in_2[j+1];
                filter1_in_3[j] <= filter1_in_3[j+1];
                filter1_in_4[j] <= filter1_in_4[j+1];

                filter2_in_1[j] <= filter2_in_1[j+1];
                filter2_in_2[j] <= filter2_in_2[j+1];
                filter2_in_3[j] <= filter2_in_3[j+1];
                filter2_in_4[j] <= filter2_in_4[j+1];

                filter3_in_1[j] <= filter3_in_1[j+1];
                filter3_in_2[j] <= filter3_in_2[j+1];
                filter3_in_3[j] <= filter3_in_3[j+1];
            end
            if(&l_frac_x_d ==0) begin
                out_store[ 0][0] <= filter1_out_1 ;
                out_store[ 1][0] <= filter1_out_2 ;
                out_store[ 2][0] <= filter1_out_3 ;
                out_store[ 3][0] <= filter1_out_4 ;
                out_store_alt[ 4][0] <= filter2_out_1 ;
                out_store_alt[ 5][0] <= filter2_out_2 ;
                out_store_alt[ 6][0] <= filter2_out_3 ;
                out_store_alt[ 7][0] <= filter2_out_4 ;
                out_store_alt[ 8][0] <= filter3_out_1 ;
                out_store_alt[ 9][0] <= filter3_out_2 ;
                out_store_alt[10][0] <= filter3_out_3 ;
            end
            else begin
                out_store[ 0][3] <= filter1_out_1 ;
                out_store[ 1][3] <= filter1_out_2 ;
                out_store[ 2][3] <= filter1_out_3 ;
                out_store[ 3][3] <= filter1_out_4 ;
                out_store_alt[ 4][3] <= filter2_out_1 ;
                out_store_alt[ 5][3] <= filter2_out_2 ;
                out_store_alt[ 6][3] <= filter2_out_3 ;
                out_store_alt[ 7][3] <= filter2_out_4 ;
                out_store_alt[ 8][3] <= filter3_out_1 ;
                out_store_alt[ 9][3] <= filter3_out_2 ;
                out_store_alt[10][3] <= filter3_out_3 ;
            end
        end
        STATE_HOR_FILT4: begin
            if(&l_frac_x_d ==0) begin
                out_store[ 0][1] <= filter1_out_1 ;
                out_store[ 1][1] <= filter1_out_2 ;
                out_store[ 2][1] <= filter1_out_3 ;
                out_store[ 3][1] <= filter1_out_4 ;
                out_store_alt[ 4][1] <= filter2_out_1 ;
                out_store_alt[ 5][1] <= filter2_out_2 ;
                out_store_alt[ 6][1] <= filter2_out_3 ;
                out_store_alt[ 7][1] <= filter2_out_4 ;
                out_store_alt[ 8][1] <= filter3_out_1 ;
                out_store_alt[ 9][1] <= filter3_out_2 ;
                out_store_alt[10][1] <= filter3_out_3 ;
            end
            else begin
                out_store[ 0][2] <= filter1_out_1 ;
                out_store[ 1][2] <= filter1_out_2 ;
                out_store[ 2][2] <= filter1_out_3 ;
                out_store[ 3][2] <= filter1_out_4 ;
                out_store_alt[ 4][2] <= filter2_out_1 ;
                out_store_alt[ 5][2] <= filter2_out_2 ;
                out_store_alt[ 6][2] <= filter2_out_3 ;
                out_store_alt[ 7][2] <= filter2_out_4 ;
                out_store_alt[ 8][2] <= filter3_out_1 ;
                out_store_alt[ 9][2] <= filter3_out_2 ;
                out_store_alt[10][2] <= filter3_out_3 ;                
            end    
        end
        STATE_HOR_FILT5: begin
            if(&l_frac_x_d==0)begin
                out_store[ 0][2] <= filter1_out_1 ;
                out_store[ 1][2] <= filter1_out_2 ;
                out_store[ 2][2] <= filter1_out_3 ;
                out_store[ 3][2] <= filter1_out_4 ;
                out_store_alt[ 4][2] <= filter2_out_1 ;
                out_store_alt[ 5][2] <= filter2_out_2 ;
                out_store_alt[ 6][2] <= filter2_out_3 ;
                out_store_alt[ 7][2] <= filter2_out_4 ;
                out_store_alt[ 8][2] <= filter3_out_1 ;
                out_store_alt[ 9][2] <= filter3_out_2 ;
                out_store_alt[10][2] <= filter3_out_3 ;
            end
            else begin
                out_store[ 0][1] <= filter1_out_1 ;
                out_store[ 1][1] <= filter1_out_2 ;
                out_store[ 2][1] <= filter1_out_3 ;
                out_store[ 3][1] <= filter1_out_4 ;
                out_store_alt[ 4][1] <= filter2_out_1 ;
                out_store_alt[ 5][1] <= filter2_out_2 ;
                out_store_alt[ 6][1] <= filter2_out_3 ;
                out_store_alt[ 7][1] <= filter2_out_4 ;
                out_store_alt[ 8][1] <= filter3_out_1 ;
                out_store_alt[ 9][1] <= filter3_out_2 ;
                out_store_alt[10][1] <= filter3_out_3 ;                
            end
        end
        STATE_HOR_FILT6: begin
            if(l_frac_y_d == 0) begin
                block_ready_out <= 1;
            end
            if (&l_frac_x_d ==0) begin
                out_store[ 0][3] <= filter1_out_1 ;
                out_store[ 1][3] <= filter1_out_2 ;
                out_store[ 2][3] <= filter1_out_3 ;
                out_store[ 3][3] <= filter1_out_4 ;
            end
            else begin
                out_store[ 0][0] <= filter1_out_1 ;
                out_store[ 1][0] <= filter1_out_2 ;
                out_store[ 2][0] <= filter1_out_3 ;
                out_store[ 3][0] <= filter1_out_4 ;                
            end
            // out_store_alt[ 4][3] <= filter2_out_1 ;
            // out_store_alt[ 5][3] <= filter2_out_2 ;
            // out_store_alt[ 6][3] <= filter2_out_3 ;
            // out_store_alt[ 7][3] <= filter2_out_4 ;
            // out_store_alt[ 8][3] <= filter3_out_1 ;
            // out_store_alt[ 9][3] <= filter3_out_2 ;
            // out_store_alt[10][3] <= filter3_out_3 ;
            level <= 1;
            if(&l_frac_y_d == 0) begin
                filt_type <= !l_frac_y_d[0];
                if(&l_frac_x_d==0)begin
                    for(j=0; j< BLOCK_WIDTH_4x4 ; j= j+1) begin
                        filter1_in_1[j] <= {out_store[j][0]};
                        filter1_in_2[j] <= {out_store[j][1]};
                        filter1_in_3[j] <= {out_store[j][2]};
                    end
                    for(j=BLOCK_WIDTH_4x4; j< REF_BLOCK_WIDTH ; j= j+1) begin
                        filter1_in_1[j] <= {out_store_alt[j][0]};
                        filter1_in_2[j] <= {out_store_alt[j][1]};
                        filter1_in_3[j] <= {out_store_alt[j][2]};
                    end
                    filter1_in_4[ 0] <= filter1_out_1 ;
                    filter1_in_4[ 1] <= filter1_out_2 ;
                    filter1_in_4[ 2] <= filter1_out_3 ;
                    filter1_in_4[ 3] <= filter1_out_4 ;
                    filter1_in_4[ 4] <= filter2_out_1 ;
                    filter1_in_4[ 5] <= filter2_out_2 ;
                    filter1_in_4[ 6] <= filter2_out_3 ;
                    filter1_in_4[ 7] <= filter2_out_4 ;
                    filter1_in_4[ 8] <= filter3_out_1 ;
                    filter1_in_4[ 9] <= filter3_out_2 ;
                    filter1_in_4[10] <= filter3_out_3 ;
                end
                else begin
                    for(j=0; j< BLOCK_WIDTH_4x4 ; j= j+1) begin
                        filter1_in_2[j] <= {out_store[j][1]};
                        filter1_in_3[j] <= {out_store[j][2]};
                        filter1_in_4[j] <= {out_store[j][3]};
                    end
                    for(j=BLOCK_WIDTH_4x4; j< REF_BLOCK_WIDTH ; j= j+1) begin
                        filter1_in_2[j] <= {out_store_alt[j][1]};
                        filter1_in_3[j] <= {out_store_alt[j][2]};
                        filter1_in_4[j] <= {out_store_alt[j][3]};
                    end
                    filter1_in_1[ 0] <= filter1_out_1 ;
                    filter1_in_1[ 1] <= filter1_out_2 ;
                    filter1_in_1[ 2] <= filter1_out_3 ;
                    filter1_in_1[ 3] <= filter1_out_4 ;
                    filter1_in_1[ 4] <= filter2_out_1 ;
                    filter1_in_1[ 5] <= filter2_out_2 ;
                    filter1_in_1[ 6] <= filter2_out_3 ;
                    filter1_in_1[ 7] <= filter2_out_4 ;
                    filter1_in_1[ 8] <= filter3_out_1 ;
                    filter1_in_1[ 9] <= filter3_out_2 ;
                    filter1_in_1[10] <= filter3_out_3 ;                    
                end  
            end
            else begin// if(l_frac_y_d[0] == 2'b11) begin
                filt_type <= !l_frac_y_d[0];
                if(&l_frac_x_d ==0) begin
                    for(j=0; j< REF_BLOCK_WIDTH - BLOCK_WIDTH_4x4-1 ; j= j+1) begin
                        filter1_in_1[j] <= {out_store_alt[9 -j][0]};
                        filter1_in_2[j] <= {out_store_alt[9 -j][1]};
                        filter1_in_3[j] <= {out_store_alt[9 -j][2]};
                    end
                    for(j=REF_BLOCK_WIDTH - BLOCK_WIDTH_4x4-1 ; j< REF_BLOCK_WIDTH-1 ; j= j+1) begin
                        filter1_in_1[j] <= {out_store[9 -j][0]};
                        filter1_in_2[j] <= {out_store[9 -j][1]};
                        filter1_in_3[j] <= {out_store[9 -j][2]};
                    end

                    filter1_in_4[10-1] <= filter1_out_1 ;
                    filter1_in_4[ 9-1] <= filter1_out_2 ;
                    filter1_in_4[ 8-1] <= filter1_out_3 ;
                    filter1_in_4[ 7-1] <= filter1_out_4 ;
                    filter1_in_4[ 6-1] <= filter2_out_1 ;
                    filter1_in_4[ 5-1] <= filter2_out_2 ;
                    filter1_in_4[ 4-1] <= filter2_out_3 ;
                    filter1_in_4[ 3-1] <= filter2_out_4 ;
                    filter1_in_4[ 2-1] <= filter3_out_1 ;
                    filter1_in_4[ 1-1] <= filter3_out_2 ;
                end
                else begin
                    for(j=0; j< REF_BLOCK_WIDTH - BLOCK_WIDTH_4x4-1 ; j= j+1) begin
                        filter1_in_2[j] <= {out_store_alt[9 -j][1]};
                        filter1_in_3[j] <= {out_store_alt[9 -j][2]};
                        filter1_in_4[j] <= {out_store_alt[9 -j][3]};
                    end
                    for(j=REF_BLOCK_WIDTH - BLOCK_WIDTH_4x4-1 ; j< REF_BLOCK_WIDTH-1 ; j= j+1) begin
                        filter1_in_2[j] <= {out_store[9 -j][1]};
                        filter1_in_3[j] <= {out_store[9 -j][2]};
                        filter1_in_4[j] <= {out_store[9 -j][3]};
                    end

                    filter1_in_1[10-1] <= filter1_out_1 ;
                    filter1_in_1[ 9-1] <= filter1_out_2 ;
                    filter1_in_1[ 8-1] <= filter1_out_3 ;
                    filter1_in_1[ 7-1] <= filter1_out_4 ;
                    filter1_in_1[ 6-1] <= filter2_out_1 ;
                    filter1_in_1[ 5-1] <= filter2_out_2 ;
                    filter1_in_1[ 4-1] <= filter2_out_3 ;
                    filter1_in_1[ 3-1] <= filter2_out_4 ;
                    filter1_in_1[ 2-1] <= filter3_out_1 ;
                    filter1_in_1[ 1-1] <= filter3_out_2 ;                    
                end                       
            end
        end
        STATE_VER_FILT1: begin
            for(j=0; j< REF_BLOCK_WIDTH -1 ; j= j+1) begin
                filter1_in_1[j] <= filter1_in_1[j+1]; // left shift
                filter1_in_2[j] <= filter1_in_2[j+1];
                filter1_in_3[j] <= filter1_in_3[j+1];
                filter1_in_4[j] <= filter1_in_4[j+1];

                // filter2_in_1[j] <= filter2_in_1[j+1];
                // filter2_in_2[j] <= filter2_in_2[j+1];
                // filter2_in_3[j] <= filter2_in_3[j+1];
                // filter2_in_4[j] <= filter2_in_4[j+1];

                // filter3_in_1[j] <= filter3_in_1[j+1];
                // filter3_in_2[j] <= filter3_in_2[j+1];
                // filter3_in_3[j] <= filter3_in_3[j+1];
            end
        end
        STATE_VER_FILT2: begin
            for(j=0; j< REF_BLOCK_WIDTH -1 ; j= j+1) begin
                filter1_in_1[j] <= filter1_in_1[j+1]; // left shift
                filter1_in_2[j] <= filter1_in_2[j+1];
                filter1_in_3[j] <= filter1_in_3[j+1];
                filter1_in_4[j] <= filter1_in_4[j+1];

                // filter2_in_1[j] <= filter2_in_1[j+1];
                // filter2_in_2[j] <= filter2_in_2[j+1];
                // filter2_in_3[j] <= filter2_in_3[j+1];
                // filter2_in_4[j] <= filter2_in_4[j+1];

                // filter3_in_1[j] <= filter3_in_1[j+1];
                // filter3_in_2[j] <= filter3_in_2[j+1];
                // filter3_in_3[j] <= filter3_in_3[j+1];
            end
        end
        STATE_VER_FILT3: begin
            for(j=0; j< REF_BLOCK_WIDTH -1 ; j= j+1) begin
                filter1_in_1[j] <= filter1_in_1[j+1]; // left shift
                filter1_in_2[j] <= filter1_in_2[j+1];
                filter1_in_3[j] <= filter1_in_3[j+1];
                filter1_in_4[j] <= filter1_in_4[j+1];

                // filter2_in_1[j] <= filter2_in_1[j+1];
                // filter2_in_2[j] <= filter2_in_2[j+1];
                // filter2_in_3[j] <= filter2_in_3[j+1];
                // filter2_in_4[j] <= filter2_in_4[j+1];

                // filter3_in_1[j] <= filter3_in_1[j+1];
                // filter3_in_2[j] <= filter3_in_2[j+1];
                // filter3_in_3[j] <= filter3_in_3[j+1];
            end
            if(&l_frac_y_d ==0) begin
                out_store[ 0][0] <= filter1_out_1 ;
                out_store[ 0][1] <= filter1_out_2 ;
                out_store[ 0][2] <= filter1_out_3 ;
                out_store[ 0][3] <= filter1_out_4 ;
            end
            else begin
                out_store[3][0] <= filter1_out_1 ;
                out_store[3][1] <= filter1_out_2 ;
                out_store[3][2] <= filter1_out_3 ;
                out_store[3][3] <= filter1_out_4 ;
            end
            // out_store[ 4][0] <= filter2_out_1 ;
            // out_store[ 5][0] <= filter2_out_2 ;
            // out_store[ 6][0] <= filter2_out_3 ;
            // out_store[ 7][0] <= filter2_out_4 ;
            // out_store[ 8][0] <= filter3_out_1 ;
            // out_store[ 9][0] <= filter3_out_2 ;
            // out_store[10][0] <= filter3_out_3 ;
        end
        STATE_VER_FILT4: begin
            if(&l_frac_y_d == 0)begin
                out_store[ 1][0] <= filter1_out_1 ;
                out_store[ 1][1] <= filter1_out_2 ;
                out_store[ 1][2] <= filter1_out_3 ;
                out_store[ 1][3] <= filter1_out_4 ;
            end
            else begin
                out_store[ 2][0] <= filter1_out_1 ;
                out_store[ 2][1] <= filter1_out_2 ;
                out_store[ 2][2] <= filter1_out_3 ;
                out_store[ 2][3] <= filter1_out_4 ;                
            end
            // out_store[ 4][1] <= filter2_out_1 ;
            // out_store[ 5][1] <= filter2_out_2 ;
            // out_store[ 6][1] <= filter2_out_3 ;
            // out_store[ 7][1] <= filter2_out_4 ;
            // out_store[ 8][1] <= filter3_out_1 ;
            // out_store[ 9][1] <= filter3_out_2 ;
            // out_store[10][1] <= filter3_out_3 ;
        end
        STATE_VER_FILT5: begin
            if(&l_frac_y_d ==0)begin
                out_store[ 2][0] <= filter1_out_1 ;
                out_store[ 2][1] <= filter1_out_2 ;
                out_store[ 2][2] <= filter1_out_3 ;
                out_store[ 2][3] <= filter1_out_4 ;
            end
            else begin
                out_store[ 1][0] <= filter1_out_1 ;
                out_store[ 1][1] <= filter1_out_2 ;
                out_store[ 1][2] <= filter1_out_3 ;
                out_store[ 1][3] <= filter1_out_4 ;
            end
            // out_store[ 4][2] <= filter2_out_1 ;
            // out_store[ 5][2] <= filter2_out_2 ;
            // out_store[ 6][2] <= filter2_out_3 ;
            // out_store[ 7][2] <= filter2_out_4 ;
            // out_store[ 8][2] <= filter3_out_1 ;
            // out_store[ 9][2] <= filter3_out_2 ;
            // out_store[10][2] <= filter3_out_3 ;
        end
        STATE_VER_FILT6: begin
            block_ready_out <= 1;
            if(&l_frac_y_d ==0)begin
                out_store[ 3][0] <= filter1_out_1 ;
                out_store[ 3][1] <= filter1_out_2 ;
                out_store[ 3][2] <= filter1_out_3 ;
                out_store[ 3][3] <= filter1_out_4 ;
            end
            else begin
                out_store[ 0][0] <= filter1_out_1 ;
                out_store[ 0][1] <= filter1_out_2 ;
                out_store[ 0][2] <= filter1_out_3 ;
                out_store[ 0][3] <= filter1_out_4 ;               
            end
            // out_store[ 4][3] <= filter2_out_1 ;
            // out_store[ 5][3] <= filter2_out_2 ;
            // out_store[ 6][3] <= filter2_out_3 ;
            // out_store[ 7][3] <= filter2_out_4 ;
            // out_store[ 8][3] <= filter3_out_1 ;
            // out_store[ 9][3] <= filter3_out_2 ;
            // out_store[10][3] <= filter3_out_3 ;            
        end
    endcase
end



always@(posedge clk) begin    
    if(reset) begin
        filter_state <= STATE_IDLE;
    end
    else begin
        filter_state <= next_state;
    end
end


endmodule 