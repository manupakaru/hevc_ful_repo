`timescale 1ns / 1ps
module hori_11_filter_module(
    clk,
    reset,
	valid_in,
	valid_out,
	ready_out,
	
	l_frac_x,
	l_frac_y,
	
    ref_l_start_x ,
    ref_l_start_y,
    ref_l_height,
    ref_l_width,
    block_start_x,
    block_start_y,
    block_end_x,
    block_end_y,	
	
    res_present_in,
    res_present_out,	
    bi_pred_block_filter_in,
    bi_pred_block_filter_out,
    x0_tu_end_in_min_tus_in,
    x0_tu_end_in_min_tus_out,
    y0_tu_end_in_min_tus_in,
    y0_tu_end_in_min_tus_out,
	
    ref_pix_l_in,  
	
	frac_y_out,
	frac_x_out,
	pic_width,
	pic_height,
	
    filter1_in_1_out ,
    filter1_in_2_out ,
    filter1_in_3_out ,
    filter1_in_4_out ,

    filter2_in_1_out ,
    filter2_in_2_out ,
    filter2_in_3_out ,
    filter2_in_4_out ,
	
    filter3_in_1_out ,
    filter3_in_2_out ,
    filter3_in_3_out ,
	
    xT_4x4_in,
    yT_4x4_in,	
	xT_4x4_out,
    yT_4x4_out
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter   REF_BLOCK_WIDTH = 4'd11;
    parameter   BLOCK_WIDTH_4x4 = 3'd4;
    parameter   FRAC_WIDTH = 2'd2;
    
    parameter   SHIFT4 = 4'd8;
    parameter   SHIFT3 = 3'd6;
	
	parameter FILTER_LENGTH = 8;
    parameter                           LUMA_DIM_WDTH		    	= 4;        // out block dimension  max 11
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
	localparam STATE_IDLE = 0;
	localparam STATE_CORNER_FILL = 1;
	localparam STATE_FIL_START1 = 2;
	localparam STATE_FIL_START2 = 3;
	localparam STATE_FIL_START3 = 4;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
	input valid_in;  // downstream module should know its gonnabe valid for 4 consecutive cycles
	output reg valid_out;  // valid out for 4 consecutive cycles
	output reg ready_out;
	
    input res_present_in;
    output reg res_present_out;
    input bi_pred_block_filter_in;
    output reg bi_pred_block_filter_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      x0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      y0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_out;	
	
	input [FRAC_WIDTH -1:0] l_frac_x;
	input [FRAC_WIDTH -1:0] l_frac_y;
	
    input signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0]   ref_l_start_x;
    input signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0]  ref_l_start_y;
    input [LUMA_DIM_WDTH -1: 0]                 ref_l_height;
    input [LUMA_DIM_WDTH -1: 0]                 ref_l_width;
    input [LUMA_DIM_WDTH -1: 0]                 block_start_x;
    input [LUMA_DIM_WDTH -1: 0]                 block_start_y;
    input [LUMA_DIM_WDTH -1: 0]                 block_end_x;
    input [LUMA_DIM_WDTH -1: 0]                 block_end_y;

    input [PIC_WIDTH_WIDTH -1:0]          pic_width;
    input [PIC_WIDTH_WIDTH -1:0]          pic_height;
	
    input [REF_BLOCK_WIDTH* REF_BLOCK_WIDTH * PIXEL_WIDTH -1:0] ref_pix_l_in;
	
	output [FRAC_WIDTH -1:0] frac_y_out;
	output [FRAC_WIDTH -1:0] frac_x_out;
	
	
    output [FILTER_PIXEL_WIDTH-1:0] filter1_in_1_out ;
    output [FILTER_PIXEL_WIDTH-1:0] filter1_in_2_out ;
    output [FILTER_PIXEL_WIDTH-1:0] filter1_in_3_out ;
    output [FILTER_PIXEL_WIDTH-1:0] filter1_in_4_out ;
    output [FILTER_PIXEL_WIDTH-1:0] filter2_in_1_out ;
    output [FILTER_PIXEL_WIDTH-1:0] filter2_in_2_out ;
    output [FILTER_PIXEL_WIDTH-1:0] filter2_in_3_out ;
    output [FILTER_PIXEL_WIDTH-1:0] filter2_in_4_out ;
    output [FILTER_PIXEL_WIDTH-1:0] filter3_in_1_out ;
    output [FILTER_PIXEL_WIDTH-1:0] filter3_in_2_out ;
    output [FILTER_PIXEL_WIDTH-1:0] filter3_in_3_out ;

	// pipe line interface
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4_in;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4_in;
    
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4_out;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4_out;
	
	
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    wire [PIXEL_WIDTH - 1:0] in_store_wire[REF_BLOCK_WIDTH - 1: 0][REF_BLOCK_WIDTH -1: 0];
    
    reg [PIXEL_WIDTH - 1:0] in_store[REF_BLOCK_WIDTH - 1: 0][REF_BLOCK_WIDTH -1: 0];
    reg [PIXEL_WIDTH - 1:0] corner_pixel_x[REF_BLOCK_WIDTH - 1: 0];
    reg [PIXEL_WIDTH - 1:0] corner_pixel_y[REF_BLOCK_WIDTH - 1: 0];
    reg [PIXEL_WIDTH - 1:0] corner_pixel;
	
	(* keep = "true", max_fanout = "200" *) integer hori_state;
	integer hori_next_state;
	
    reg [FRAC_WIDTH -1:0] l_frac_x_d ;//= l_frac_x[FRAC_WIDTH -2:0];
    reg [FRAC_WIDTH -1:0] l_frac_y_d ;//= l_frac_y[FRAC_WIDTH -2:0];
    
    reg [4 -1: 0]                 corner_x_start;
    reg [4 -1: 0]                 corner_x_end;
    reg [4 -1: 0]                 corner_y_start;
    reg [4 -1: 0]                 corner_y_end;
    
    reg [4 -1: 0]                 corner_idx_x;
    reg [4 -1: 0]                 corner_idx_y;
	
	assign frac_y_out = l_frac_y_d;
	assign frac_x_out = l_frac_x_d;
	integer i,j;
	
	reg valid_reg;
	reg valid_reg_d;
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------


// Instantiate the module
hori_luma_8_tap_filter filter_1_1 (
    .clk(clk), 
    .filt_type(l_frac_x_d), 
    .a_ref_pixel_in(in_store[0][0]), 
    .b_ref_pixel_in(in_store[0][1]), 
    .c_ref_pixel_in(in_store[0][2]), 
    .d_ref_pixel_in(in_store[0][3]), 
    .e_ref_pixel_in(in_store[0][4]), 
    .f_ref_pixel_in(in_store[0][5]), 
    .g_ref_pixel_in(in_store[0][6]), 
    .h_ref_pixel_in(in_store[0][7]), 
    .pred_pixel_out(filter1_in_1_out)
    );

hori_luma_8_tap_filter filter_1_2 (
    .clk(clk), 
    .filt_type(l_frac_x_d), 
    .a_ref_pixel_in(in_store[1][0]), 
    .b_ref_pixel_in(in_store[1][1]), 
    .c_ref_pixel_in(in_store[1][2]), 
    .d_ref_pixel_in(in_store[1][3]), 
    .e_ref_pixel_in(in_store[1][4]), 
    .f_ref_pixel_in(in_store[1][5]), 
    .g_ref_pixel_in(in_store[1][6]), 
    .h_ref_pixel_in(in_store[1][7]), 
    .pred_pixel_out(filter1_in_2_out)
    );

hori_luma_8_tap_filter filter_1_3 (
    .clk(clk), 
    .filt_type(l_frac_x_d), 
    .a_ref_pixel_in(in_store[2][0]), 
    .b_ref_pixel_in(in_store[2][1]), 
    .c_ref_pixel_in(in_store[2][2]), 
    .d_ref_pixel_in(in_store[2][3]), 
    .e_ref_pixel_in(in_store[2][4]), 
    .f_ref_pixel_in(in_store[2][5]), 
    .g_ref_pixel_in(in_store[2][6]), 
    .h_ref_pixel_in(in_store[2][7]), 
    .pred_pixel_out(filter1_in_3_out)
    );

 hori_luma_8_tap_filter filter_1_4 (
    .clk(clk), 
    .filt_type(l_frac_x_d), 
    .a_ref_pixel_in(in_store[3][0]), 
    .b_ref_pixel_in(in_store[3][1]), 
    .c_ref_pixel_in(in_store[3][2]), 
    .d_ref_pixel_in(in_store[3][3]), 
    .e_ref_pixel_in(in_store[3][4]), 
    .f_ref_pixel_in(in_store[3][5]), 
    .g_ref_pixel_in(in_store[3][6]), 
    .h_ref_pixel_in(in_store[3][7]), 
    .pred_pixel_out(filter1_in_4_out)
    );

hori_luma_8_tap_filter filter_2_1 (
    .clk(clk), 
    .filt_type(l_frac_x_d), 
    .a_ref_pixel_in(in_store[4][0]), 
    .b_ref_pixel_in(in_store[4][1]), 
    .c_ref_pixel_in(in_store[4][2]), 
    .d_ref_pixel_in(in_store[4][3]), 
    .e_ref_pixel_in(in_store[4][4]), 
    .f_ref_pixel_in(in_store[4][5]), 
    .g_ref_pixel_in(in_store[4][6]), 
    .h_ref_pixel_in(in_store[4][7]), 
    .pred_pixel_out(filter2_in_1_out)
    );  
	
hori_luma_8_tap_filter filter_2_2 (
    .clk(clk), 
    .filt_type(l_frac_x_d), 
    .a_ref_pixel_in(in_store[5][0]), 
    .b_ref_pixel_in(in_store[5][1]), 
    .c_ref_pixel_in(in_store[5][2]), 
    .d_ref_pixel_in(in_store[5][3]), 
    .e_ref_pixel_in(in_store[5][4]), 
    .f_ref_pixel_in(in_store[5][5]), 
    .g_ref_pixel_in(in_store[5][6]), 
    .h_ref_pixel_in(in_store[5][7]), 
    .pred_pixel_out(filter2_in_2_out)
    );  
	
hori_luma_8_tap_filter filter_2_3 (
    .clk(clk), 
    .filt_type(l_frac_x_d), 
    .a_ref_pixel_in(in_store[6][0]), 
    .b_ref_pixel_in(in_store[6][1]), 
    .c_ref_pixel_in(in_store[6][2]), 
    .d_ref_pixel_in(in_store[6][3]), 
    .e_ref_pixel_in(in_store[6][4]), 
    .f_ref_pixel_in(in_store[6][5]), 
    .g_ref_pixel_in(in_store[6][6]), 
    .h_ref_pixel_in(in_store[6][7]), 
    .pred_pixel_out(filter2_in_3_out)
    );  

hori_luma_8_tap_filter filter_2_4 (
    .clk(clk), 
    .filt_type(l_frac_x_d), 
    .a_ref_pixel_in(in_store[7][0]), 
    .b_ref_pixel_in(in_store[7][1]), 
    .c_ref_pixel_in(in_store[7][2]), 
    .d_ref_pixel_in(in_store[7][3]), 
    .e_ref_pixel_in(in_store[7][4]), 
    .f_ref_pixel_in(in_store[7][5]), 
    .g_ref_pixel_in(in_store[7][6]), 
    .h_ref_pixel_in(in_store[7][7]), 
    .pred_pixel_out(filter2_in_4_out)
    );  

hori_luma_8_tap_filter filter_3_1 (
    .clk(clk), 
    .filt_type(l_frac_x_d), 
    .a_ref_pixel_in(in_store[8][0]), 
    .b_ref_pixel_in(in_store[8][1]), 
    .c_ref_pixel_in(in_store[8][2]), 
    .d_ref_pixel_in(in_store[8][3]), 
    .e_ref_pixel_in(in_store[8][4]), 
    .f_ref_pixel_in(in_store[8][5]), 
    .g_ref_pixel_in(in_store[8][6]), 
    .h_ref_pixel_in(in_store[8][7]), 
    .pred_pixel_out(filter3_in_1_out)
    );  

hori_luma_8_tap_filter filter_3_2 (
    .clk(clk), 
    .filt_type(l_frac_x_d), 
    .a_ref_pixel_in(in_store[9][0]), 
    .b_ref_pixel_in(in_store[9][1]), 
    .c_ref_pixel_in(in_store[9][2]), 
    .d_ref_pixel_in(in_store[9][3]), 
    .e_ref_pixel_in(in_store[9][4]), 
    .f_ref_pixel_in(in_store[9][5]), 
    .g_ref_pixel_in(in_store[9][6]), 
    .h_ref_pixel_in(in_store[9][7]), 
    .pred_pixel_out(filter3_in_2_out)
    );  

hori_luma_8_tap_filter filter_3_3 (
    .clk(clk), 
    .filt_type(l_frac_x_d), 
    .a_ref_pixel_in(in_store[10][0]), 
    .b_ref_pixel_in(in_store[10][1]), 
    .c_ref_pixel_in(in_store[10][2]), 
    .d_ref_pixel_in(in_store[10][3]), 
    .e_ref_pixel_in(in_store[10][4]), 
    .f_ref_pixel_in(in_store[10][5]), 
    .g_ref_pixel_in(in_store[10][6]), 
    .h_ref_pixel_in(in_store[10][7]), 
    .pred_pixel_out(filter3_in_3_out)
    );  
	
	
    generate
        genvar ii;
        genvar jj;
		for(jj=0;jj <REF_BLOCK_WIDTH ; jj=jj+1 ) begin : row_iteration1
			for(ii=0 ; ii < REF_BLOCK_WIDTH ; ii = ii+1) begin : column_iteration1
				assign in_store_wire[jj][ii] = ref_pix_l_in[(jj*REF_BLOCK_WIDTH + ii + 1)*PIXEL_WIDTH-1:(jj*REF_BLOCK_WIDTH + ii)*PIXEL_WIDTH];
			end
		end

	endgenerate

always@(posedge clk) begin    
	if(reset) begin
		hori_state <= STATE_IDLE;
	end
	else begin
		hori_state <= hori_next_state;
	end
end

always@(*) begin
	hori_next_state = hori_state;
	ready_out = 0;
	case(hori_state)
		STATE_IDLE: begin
			ready_out = 1;
			if(valid_in) begin
				if ((ref_l_start_x[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1]) || (ref_l_start_y[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1]) || ((ref_l_start_x + ref_l_width) >= pic_width ) || ((ref_l_start_y + ref_l_height) >= pic_height )) begin
                    hori_next_state = STATE_CORNER_FILL;
                end
                else begin
                    hori_next_state = STATE_FIL_START1;
                end
			end
		end
		STATE_CORNER_FILL: begin
            hori_next_state = STATE_FIL_START1;
        end
		STATE_FIL_START1: begin
			hori_next_state = STATE_FIL_START2;
		end
		STATE_FIL_START2: begin
			hori_next_state = STATE_FIL_START3;
		end
		STATE_FIL_START3: begin
			hori_next_state = STATE_IDLE;
		end
	endcase
end

always@(posedge clk) begin
	valid_reg <= 0;
    case(hori_state)
        STATE_IDLE: begin
            if(valid_in) begin	
                l_frac_x_d <= l_frac_x;
                l_frac_y_d <= l_frac_y;
				if ((ref_l_start_x[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1]) || (ref_l_start_y[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1]) || ((ref_l_start_x + ref_l_width) >= pic_width ) || ((ref_l_start_y + ref_l_height) >= pic_height )) begin	
					valid_reg <= 0;
				end else begin
					valid_reg <= 1;
				end

					// for(k=0; k< PIXEL_WIDTH ; k = k+1) begin
					if ((ref_l_start_x[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1]) || (ref_l_start_y[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1]) || ((ref_l_start_x + ref_l_width) >= pic_width ) || ((ref_l_start_y + ref_l_height) >= pic_height )) begin	
						for(j=0;j <REF_BLOCK_WIDTH ; j=j+1 ) begin
							for(i=0 ; i < REF_BLOCK_WIDTH ; i = i+1) begin 
								in_store[j][i] <= in_store_wire[j][i];
							end
						end
					end
					else begin
						case({l_frac_y == 2'b11,l_frac_x == 2'b11})
							2'b00: begin
								for(j=0;j <REF_BLOCK_WIDTH ; j=j+1 ) begin
									for(i=0 ; i < REF_BLOCK_WIDTH ; i = i+1) begin 
										in_store[j][i] <= in_store_wire[j][i];
									end
								end
							end
							2'b01: begin
								for(j=0;j <REF_BLOCK_WIDTH ; j=j+1 ) begin
										for(i=0 ; i < REF_BLOCK_WIDTH-1 ; i = i+1) begin 
											in_store[j][i+1] <= in_store_wire[j][i];
										end
								end
							end
							2'b10: begin
								for(j=0;j <REF_BLOCK_WIDTH-1 ; j=j+1 ) begin
										for(i=0 ; i < REF_BLOCK_WIDTH ; i = i+1) begin 
											in_store[j+1][i] <= in_store_wire[j][i];
										end
									end
								end
							2'b11: begin
								for(j=0;j <REF_BLOCK_WIDTH-1 ; j=j+1 ) begin
										for(i=0 ; i < REF_BLOCK_WIDTH-1 ; i = i+1) begin 
											in_store[j+1][i+1] <= in_store_wire[j][i];
										end
								end						
							end
						endcase
					end
					// end

                if((ref_l_start_x[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1])) begin
                    corner_x_start <= 0;
                    corner_x_end <= block_start_x;
                    corner_idx_x <= block_start_x;
                end
                else if((ref_l_start_x + ref_l_width) >= pic_width ) begin
                    corner_x_start <= block_end_x;
                    corner_x_end <= REF_BLOCK_WIDTH;
                    corner_idx_x <= block_end_x;
                end
                else begin
                    corner_x_start <= REF_BLOCK_WIDTH;
                    corner_x_end <= 0;
                end
                if((ref_l_start_y[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1])) begin
                    corner_y_start <= 0;
                    corner_y_end <= block_start_y;
                    corner_idx_y <= block_start_y;
                end
                else if((ref_l_start_y + ref_l_height) >= pic_height ) begin
                    corner_y_start <= block_end_y;
                    corner_y_end <= REF_BLOCK_WIDTH;
                    corner_idx_y <= block_end_y;
                end
                else begin
                    corner_y_start <= REF_BLOCK_WIDTH;
                    corner_y_end <= 0;
                end
                case({ref_l_start_x[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1],ref_l_start_y[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1]})
                  2'b00: begin
                     corner_pixel <= in_store_wire[block_end_y][block_end_x];
                     corner_pixel_x[0 ] <= in_store_wire[0 ][block_end_x];
                     corner_pixel_x[1 ] <= in_store_wire[1 ][block_end_x];
                     corner_pixel_x[2 ] <= in_store_wire[2 ][block_end_x];
                     corner_pixel_x[3 ] <= in_store_wire[3 ][block_end_x];
                     corner_pixel_x[4 ] <= in_store_wire[4 ][block_end_x];
                     corner_pixel_x[5 ] <= in_store_wire[5 ][block_end_x];
                     corner_pixel_x[6 ] <= in_store_wire[6 ][block_end_x];
                     corner_pixel_x[7 ] <= in_store_wire[7 ][block_end_x];
                     corner_pixel_x[8 ] <= in_store_wire[8 ][block_end_x];
                     corner_pixel_x[9 ] <= in_store_wire[9 ][block_end_x];
                     corner_pixel_x[10] <= in_store_wire[10][block_end_x];
                     
                     corner_pixel_y[0 ] <= in_store_wire[block_end_y][0 ];
                     corner_pixel_y[1 ] <= in_store_wire[block_end_y][1 ];
                     corner_pixel_y[2 ] <= in_store_wire[block_end_y][2 ];
                     corner_pixel_y[3 ] <= in_store_wire[block_end_y][3 ];
                     corner_pixel_y[4 ] <= in_store_wire[block_end_y][4 ];
                     corner_pixel_y[5 ] <= in_store_wire[block_end_y][5 ];
                     corner_pixel_y[6 ] <= in_store_wire[block_end_y][6 ];
                     corner_pixel_y[7 ] <= in_store_wire[block_end_y][7 ];
                     corner_pixel_y[8 ] <= in_store_wire[block_end_y][8 ];
                     corner_pixel_y[9 ] <= in_store_wire[block_end_y][9 ];
                     corner_pixel_y[10] <= in_store_wire[block_end_y][10];
                  end
                  2'b01: begin
                     corner_pixel <= in_store_wire[block_start_y][block_end_x];
                     corner_pixel_x[0 ] <= in_store_wire[0 ][block_end_x];
                     corner_pixel_x[1 ] <= in_store_wire[1 ][block_end_x];
                     corner_pixel_x[2 ] <= in_store_wire[2 ][block_end_x];
                     corner_pixel_x[3 ] <= in_store_wire[3 ][block_end_x];
                     corner_pixel_x[4 ] <= in_store_wire[4 ][block_end_x];
                     corner_pixel_x[5 ] <= in_store_wire[5 ][block_end_x];
                     corner_pixel_x[6 ] <= in_store_wire[6 ][block_end_x];
                     corner_pixel_x[7 ] <= in_store_wire[7 ][block_end_x];
                     corner_pixel_x[8 ] <= in_store_wire[8 ][block_end_x];
                     corner_pixel_x[9 ] <= in_store_wire[9 ][block_end_x];
                     corner_pixel_x[10] <= in_store_wire[10][block_end_x];
                     
                     corner_pixel_y[0 ] <= in_store_wire[block_start_y][0 ];
                     corner_pixel_y[1 ] <= in_store_wire[block_start_y][1 ];
                     corner_pixel_y[2 ] <= in_store_wire[block_start_y][2 ];
                     corner_pixel_y[3 ] <= in_store_wire[block_start_y][3 ];
                     corner_pixel_y[4 ] <= in_store_wire[block_start_y][4 ];
                     corner_pixel_y[5 ] <= in_store_wire[block_start_y][5 ];
                     corner_pixel_y[6 ] <= in_store_wire[block_start_y][6 ];
                     corner_pixel_y[7 ] <= in_store_wire[block_start_y][7 ];
                     corner_pixel_y[8 ] <= in_store_wire[block_start_y][8 ];
                     corner_pixel_y[9 ] <= in_store_wire[block_start_y][9 ];
                     corner_pixel_y[10] <= in_store_wire[block_start_y][10];                  
                  end
                  2'b10: begin
                     corner_pixel <= in_store_wire[block_end_y][block_start_x];
                     corner_pixel_x[0 ] <= in_store_wire[0 ][block_start_x];
                     corner_pixel_x[1 ] <= in_store_wire[1 ][block_start_x];
                     corner_pixel_x[2 ] <= in_store_wire[2 ][block_start_x];
                     corner_pixel_x[3 ] <= in_store_wire[3 ][block_start_x];
                     corner_pixel_x[4 ] <= in_store_wire[4 ][block_start_x];
                     corner_pixel_x[5 ] <= in_store_wire[5 ][block_start_x];
                     corner_pixel_x[6 ] <= in_store_wire[6 ][block_start_x];
                     corner_pixel_x[7 ] <= in_store_wire[7 ][block_start_x];
                     corner_pixel_x[8 ] <= in_store_wire[8 ][block_start_x];
                     corner_pixel_x[9 ] <= in_store_wire[9 ][block_start_x];
                     corner_pixel_x[10] <= in_store_wire[10][block_start_x];
                     
                     corner_pixel_y[0 ] <= in_store_wire[block_end_y][0 ];
                     corner_pixel_y[1 ] <= in_store_wire[block_end_y][1 ];
                     corner_pixel_y[2 ] <= in_store_wire[block_end_y][2 ];
                     corner_pixel_y[3 ] <= in_store_wire[block_end_y][3 ];
                     corner_pixel_y[4 ] <= in_store_wire[block_end_y][4 ];
                     corner_pixel_y[5 ] <= in_store_wire[block_end_y][5 ];
                     corner_pixel_y[6 ] <= in_store_wire[block_end_y][6 ];
                     corner_pixel_y[7 ] <= in_store_wire[block_end_y][7 ];
                     corner_pixel_y[8 ] <= in_store_wire[block_end_y][8 ];
                     corner_pixel_y[9 ] <= in_store_wire[block_end_y][9 ];
                     corner_pixel_y[10] <= in_store_wire[block_end_y][10];                   
                  end
                  2'b11: begin
                     corner_pixel <= in_store_wire[block_start_y][block_start_x];
                     corner_pixel_x[0 ] <= in_store_wire[0 ][block_start_x];
                     corner_pixel_x[1 ] <= in_store_wire[1 ][block_start_x];
                     corner_pixel_x[2 ] <= in_store_wire[2 ][block_start_x];
                     corner_pixel_x[3 ] <= in_store_wire[3 ][block_start_x];
                     corner_pixel_x[4 ] <= in_store_wire[4 ][block_start_x];
                     corner_pixel_x[5 ] <= in_store_wire[5 ][block_start_x];
                     corner_pixel_x[6 ] <= in_store_wire[6 ][block_start_x];
                     corner_pixel_x[7 ] <= in_store_wire[7 ][block_start_x];
                     corner_pixel_x[8 ] <= in_store_wire[8 ][block_start_x];
                     corner_pixel_x[9 ] <= in_store_wire[9 ][block_start_x];
                     corner_pixel_x[10] <= in_store_wire[10][block_start_x];
                     
                     corner_pixel_y[0 ] <= in_store_wire[block_start_y][0 ];
                     corner_pixel_y[1 ] <= in_store_wire[block_start_y][1 ];
                     corner_pixel_y[2 ] <= in_store_wire[block_start_y][2 ];
                     corner_pixel_y[3 ] <= in_store_wire[block_start_y][3 ];
                     corner_pixel_y[4 ] <= in_store_wire[block_start_y][4 ];
                     corner_pixel_y[5 ] <= in_store_wire[block_start_y][5 ];
                     corner_pixel_y[6 ] <= in_store_wire[block_start_y][6 ];
                     corner_pixel_y[7 ] <= in_store_wire[block_start_y][7 ];
                     corner_pixel_y[8 ] <= in_store_wire[block_start_y][8 ];
                     corner_pixel_y[9 ] <= in_store_wire[block_start_y][9 ];
                     corner_pixel_y[10] <= in_store_wire[block_start_y][10];                  
                  end
                endcase
            end
        end
        STATE_CORNER_FILL: begin
			valid_reg <= 1;
			case({l_frac_y_d == 2'b11,l_frac_x_d == 2'b11})
				2'b00: begin
					for(j=0;j <REF_BLOCK_WIDTH ; j=j+1 ) begin
						for(i=0 ; i < REF_BLOCK_WIDTH ; i = i+1) begin
							if( (corner_x_start <= i) && (corner_x_end > i) && (corner_y_start <= j) && (corner_y_end > j)) begin
								in_store[j][i] <= corner_pixel;
							end
							else if( (corner_x_start <= i) && (corner_x_end > i)) begin
								in_store[j][i] <= corner_pixel_x[j];
							end
							else if( (corner_y_start <= j) && (corner_y_end > j)) begin
								in_store[j][i] <= corner_pixel_y[i];
							end
						end
					end			
								
				end
				2'b01: begin
					for(j=0;j <REF_BLOCK_WIDTH ; j=j+1 ) begin
						for(i=0 ; i < REF_BLOCK_WIDTH-1 ; i = i+1) begin
							if( (corner_x_start <= i) && (corner_x_end > i) && (corner_y_start <= j) && (corner_y_end > j)) begin
								in_store[j][i+1] <= corner_pixel;
							end
							else if( (corner_x_start <= i) && (corner_x_end > i)) begin
								in_store[j][i+1] <= corner_pixel_x[j];
							end
							else if( (corner_y_start <= j) && (corner_y_end > j)) begin
								in_store[j][i+1] <= corner_pixel_y[i];
							end
							else begin
								in_store[j][i+1] <= in_store[j][i];
							end
						end
					end							
				end
				2'b10: begin
					for(j=0;j <REF_BLOCK_WIDTH-1 ; j=j+1 ) begin
						for(i=0 ; i < REF_BLOCK_WIDTH ; i = i+1) begin
							if( (corner_x_start <= i) && (corner_x_end > i) && (corner_y_start <= j) && (corner_y_end > j)) begin
								in_store[j+1][i] <= corner_pixel;
							end
							else if( (corner_x_start <= i) && (corner_x_end > i)) begin
								in_store[j+1][i] <= corner_pixel_x[j];
							end
							else if( (corner_y_start <= j) && (corner_y_end > j)) begin
								in_store[j+1][i] <= corner_pixel_y[i];
							end
							else begin
								in_store[j+1][i] <= in_store[j][i];
							end
						end
					end
							
				end
				2'b11: begin
					for(j=0;j <REF_BLOCK_WIDTH-1 ; j=j+1 ) begin
						for(i=0 ; i < REF_BLOCK_WIDTH-1 ; i = i+1) begin
							if( (corner_x_start <= i) && (corner_x_end > i) && (corner_y_start <= j) && (corner_y_end > j)) begin
								in_store[j+1][i+1] <= corner_pixel;
							end
							else if( (corner_x_start <= i) && (corner_x_end > i)) begin
								in_store[j+1][i+1] <= corner_pixel_x[j];
							end
							else if( (corner_y_start <= j) && (corner_y_end > j)) begin
								in_store[j+1][i+1] <= corner_pixel_y[i];
							end
							else begin
								in_store[j+1][i+1] <= in_store[j][i];
							end
						end
					end				
				end
			endcase
        
        end
		STATE_FIL_START1,STATE_FIL_START2,STATE_FIL_START3: begin
			valid_reg <= 1;
			for(j=0; j< REF_BLOCK_WIDTH -1 ; j= j+1) begin
                in_store[0][j] <= in_store[0][j+1]; // left shift
                in_store[1][j] <= in_store[1][j+1];
                in_store[2][j] <= in_store[2][j+1];
                in_store[3][j] <= in_store[3][j+1];
                        
                in_store[4][j] <= in_store[4][j+1];
                in_store[5][j] <= in_store[5][j+1];
                in_store[6][j] <= in_store[6][j+1];
                in_store[7][j] <= in_store[7][j+1];
                        
                in_store[8][j] <= in_store[8][j+1];
                in_store[9][j] <= in_store[9][j+1];
                in_store[10][j] <= in_store[10][j+1];
            end
		end
	endcase
end
		


always@(posedge clk) begin
		if(valid_in) begin  // need not pipe line these inputs because first valid_out comes before going to idle
			xT_4x4_out <= xT_4x4_in;
			yT_4x4_out <= yT_4x4_in;
			res_present_out <= res_present_in;
			bi_pred_block_filter_out <= bi_pred_block_filter_in;
			x0_tu_end_in_min_tus_out <= x0_tu_end_in_min_tus_in;
			y0_tu_end_in_min_tus_out <= y0_tu_end_in_min_tus_in;
		end
		valid_reg_d <= valid_reg;
		valid_out <= valid_reg_d;
end


endmodule