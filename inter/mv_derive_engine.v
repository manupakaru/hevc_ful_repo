`timescale 1ns / 1ps
module mv_derive_engine(
    clk,
    reset,
    enable,
    config_mode_in,
    config_bus_in,
    
    from_top_ref_pic_list_poc_data_in,
    from_top_ref_pic_list0_poc_wr_en,
    from_top_ref_pic_list1_poc_wr_en,
    from_top_ref_pic_list0_poc_addr,
    from_top_ref_pic_list1_poc_addr,

    from_top_ref_pic_list_idx_data_in,
    from_top_ref_pic_list_idx_wr_en,
    
    mv_done_out,
    pref_ctu_col_writ_ctu_done_out,
    
    mv_col_axi_awid,     
    mv_col_axi_awlen,    
    mv_col_axi_awsize,   
    mv_col_axi_awburst,  
    mv_col_axi_awlock,   
    mv_col_axi_awcache,  
    mv_col_axi_awprot,   
    mv_col_axi_awvalid,
    mv_col_axi_awaddr,
    mv_col_axi_awready,
    mv_col_axi_wstrb,
    mv_col_axi_wlast,
    mv_col_axi_wvalid,
    mv_col_axi_wdata,
    mv_col_axi_wready,
    // mv_col_axi_bid,
    mv_col_axi_bresp,
    mv_col_axi_bvalid,
    mv_col_axi_bready,
    
    top_mvs_to_bs_out,
    left_mvs_to_bs_out,
    x_rel_top_out,
    y_rel_left_out,
    bs_left_valid_out,
    bs_top_valid_out,
    
    mv_pref_axi_araddr              ,
    mv_pref_axi_arlen               ,
    mv_pref_axi_arsize              ,
    mv_pref_axi_arburst             ,
    mv_pref_axi_arprot              ,
    mv_pref_axi_arvalid             ,
    mv_pref_axi_arready             ,

    mv_pref_axi_rdata               ,
    mv_pref_axi_rresp               ,
    mv_pref_axi_rlast               ,
    mv_pref_axi_rvalid              ,
    mv_pref_axi_rready              ,

    mv_pref_axi_arlock              ,
    mv_pref_axi_arid                ,
    mv_pref_axi_arcache             ,
    
    current_mv_field_pred_flag_l0   ,
    current_mv_field_pred_flag_l1   ,
    current_mv_field_ref_idx_l0     ,
    current_mv_field_ref_idx_l1     ,
    current_mv_field_mv_x_l0        ,
    current_mv_field_mv_y_l0        ,
    current_mv_field_mv_x_l1        ,
    current_mv_field_mv_y_l1        ,
    current_dpb_idx_l0              ,
    current_dpb_idx_l1              ,
    current_mv_field_valid          ,
    
    xx_pb,
    hh_pb,
    yy_pb,
    ww_pb,
    
    pred_sample_gen_idle_in
    ,mv_state_8bit_out
    ,col_state_axi_write
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"
    `include "../sim/inter_axi_def.v" // always define below pred_def

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                          STATE_MV_DERIVE_CONFIG_UPDATE = 0;
    localparam                          STATE_PREFETCH_COL_WRITE_WAIT = 1;
    localparam                          STATE_MV_DERIVE_PU_OVERWRIT_3 = 2;
    localparam                          STATE_MV_DERIVE_AVAILABLE_CHECK4 = 3;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_9 = 4;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_10 = 5;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_11 = 6;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_12 = 7;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_13 = 8;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_MERGE_14     = 9;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_MERGE_15     = 10;
    // localparam                          STATE_MV_DERIVE_WAIT_BEFORE_ZERO_AMVP     = 11;
    localparam                          STATE_MV_DERIVE_COMPARE_MVS       = 12;
    localparam                          STATE_MV_DERIVE_ZERO_MERGE            = 13;
    localparam                          STATE_MV_DERIVE_ADD_ZEROS_AMVP           = 14;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_9 = 16;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_10 = 17;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_11 = 18;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_12 = 19;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_13 = 20;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP14   = 21;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP15   = 22;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP16   = 23;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP17   = 24;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP18   = 25;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP19   = 26;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP20   = 27;
    localparam                          STATE_MV_DERIVE_COL_MV_WAIT         = 28;
    localparam                          STATE_MV_DERIVE_MVD_ADD_AMVP       = 29;
    localparam                          STATE_MV_DERIVE_SET_DUMMY_INTRA_MV = 31;
    localparam                          STATE_MV_DERIVE_SET_PRE_INTRA_MV    = 32;    
    localparam                          STATE_MV_DERIVE_DONE    = 33;
    localparam                          STATE_MV_DERIVE_CTU_DONE    = 34;

    localparam                          STATE_MV_DERIVE_SET_INTER_MV    = 35;
    
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS1       = 36;
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS2       = 37;
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS3       = 38;
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS4       = 39;
    localparam                          STATE_MV_DERIVE_WAIT_BEFOR_CONFIG_UPDATE       = 40;
    localparam                          STATE_MV_DERIVE_CTU_DONE_WAIT    = 41;    
    localparam                          CLIPPED_WIDTH                       = 8;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
    input enable;
    
    input [INTER_TOP_CONFIG_BUS_MODE_WIDTH -1 :0] config_mode_in;

    input [INTER_TOP_CONFIG_BUS_WIDTH -1:0] config_bus_in;

    input [REF_PIC_LIST_POC_DATA_WIDTH-1:0]     from_top_ref_pic_list_poc_data_in;
    input                                       from_top_ref_pic_list0_poc_wr_en  ;
    input                                       from_top_ref_pic_list1_poc_wr_en  ;
    input [NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]     from_top_ref_pic_list0_poc_addr  ;
    input [NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]     from_top_ref_pic_list1_poc_addr  ;
    input [DPB_FRAME_OFFSET_WIDTH -1:0]         from_top_ref_pic_list_idx_data_in;
    input                                       from_top_ref_pic_list_idx_wr_en;
    
    
    output                                              pref_ctu_col_writ_ctu_done_out;    
    output reg                                          mv_done_out;
    
    
	 // write address channel
	 output                                     mv_col_axi_awid    ;// = 0;
	 output      [7:0]                          mv_col_axi_awlen   ;// = MV_COL_AXI_AX_LEN;
	 output      [2:0]                          mv_col_axi_awsize  ;// = MV_COL_AXI_AX_SIZE;
	 output      [1:0]                          mv_col_axi_awburst ;// = `AX_BURST_INC;
	 output                        	            mv_col_axi_awlock  ;// = `AX_LOCK_DEFAULT;
	 output      [3:0]                          mv_col_axi_awcache ;// = `AX_CACHE_DEFAULT;
	 output      [2:0]                          mv_col_axi_awprot  ;// = `AX_PROT_DATA;
	 output                                     mv_col_axi_awvalid;
     output      [31:0]                         mv_col_axi_awaddr;

	 input                       	            mv_col_axi_awready;
	 
	 // write data channel
	 output      [64-1:0]                       mv_col_axi_wstrb;// = 64'hFFFFF_FFFFF_FFFFF_FFFFF_FFFFF;       // 40 bytes
	 output                                     mv_col_axi_wlast;
	 output                                     mv_col_axi_wvalid;
	 output      [MV_COL_AXI_DATA_WIDTH -1:0]   mv_col_axi_wdata;
     
	 input	                                    mv_col_axi_wready;
	 
	 //write response channel
	 // input                       	            mv_col_axi_bid;
	 input       [1:0]                          mv_col_axi_bresp;
	 input                       	            mv_col_axi_bvalid;
	 output                                     mv_col_axi_bready;  

    output	    [AXI_ADDR_WDTH-1:0]		            mv_pref_axi_araddr  ;
    output wire [7:0]					            mv_pref_axi_arlen   ;
    output wire	[2:0]					            mv_pref_axi_arsize  ;
    output wire [1:0]					            mv_pref_axi_arburst ;
    output wire [2:0]					            mv_pref_axi_arprot  ;
    output 	   						                mv_pref_axi_arvalid ;
    input 								            mv_pref_axi_arready ;
                
    input		[MV_COL_AXI_DATA_WIDTH-1:0]		    mv_pref_axi_rdata   ;
    input		[1:0]					            mv_pref_axi_rresp   ;
    input 								            mv_pref_axi_rlast   ;
    input								            mv_pref_axi_rvalid  ;
    output 						                    mv_pref_axi_rready  ;
    
    output                        	                mv_pref_axi_arlock  ;
	output                                          mv_pref_axi_arid    ;    
    output      [3:0]                               mv_pref_axi_arcache ;             
     
    output [MV_FIELD_DATA_WIDTH -1:0]   top_mvs_to_bs_out;
    output [MV_FIELD_DATA_WIDTH -1:0]   left_mvs_to_bs_out;
    output     [CTB_SIZE_WIDTH -1:0]    x_rel_top_out;
    output     [CTB_SIZE_WIDTH -1:0]    y_rel_left_out;
    output      bs_left_valid_out;
    output      bs_top_valid_out;

    output reg  [X11_ADDR_WDTH - 1:0]       xx_pb;
    output reg  [CB_SIZE_WIDTH -1:0 ]       hh_pb;
    output reg  [Y11_ADDR_WDTH - 1:0]       yy_pb;
    output reg  [CB_SIZE_WIDTH -1:0 ]       ww_pb;  

    output reg                                  current_mv_field_pred_flag_l0;
    output reg                                  current_mv_field_pred_flag_l1;
    output reg [REF_IDX_LX_WIDTH -1:0]          current_mv_field_ref_idx_l0;
    output reg [REF_IDX_LX_WIDTH -1:0]          current_mv_field_ref_idx_l1;
    output reg signed [MVD_WIDTH -1:0]          current_mv_field_mv_x_l0;
    output reg signed [MVD_WIDTH -1:0]          current_mv_field_mv_y_l0;
    output reg signed [MVD_WIDTH -1:0]          current_mv_field_mv_x_l1;
    output reg signed [MVD_WIDTH -1:0]          current_mv_field_mv_y_l1;    
    output reg [DPB_ADDR_WIDTH -1:0]            current_dpb_idx_l0;    
    output reg [DPB_ADDR_WIDTH -1:0]            current_dpb_idx_l1;    
    output reg                                  current_mv_field_valid; 
    
    input                                       pred_sample_gen_idle_in;
	output [7:0] mv_state_8bit_out;
	output [7:0] col_state_axi_write;
	
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    
	`ifdef SYNTHESIZABLE
		integer                                         mv_derive_state;                      // the internal mv_derive_state of the module
		integer                                         mv_derive_next_state;
	`else
    (* keep = "true" *) integer                                         mv_derive_state;                      // the internal mv_derive_state of the module
	(* keep = "true" *) integer                                         mv_derive_next_state;                      // the internal mv_derive_state of the module
	`endif

    
    integer i,j;
                
    reg         [PIC_DIM_WIDTH-1:0]                 pic_width;
    reg         [PIC_DIM_WIDTH-1:0]                 pic_height;
    reg         [LOG2_CTB_WIDTH - 1: 0]             log2_ctb_size;
    reg         [CTB_SIZE_WIDTH    : 0]             ctb_size;
    

    reg         [PARALLEL_MERGE_LEVEL_WIDTH - 1: 0] log2_parallel_merge_level;      // rarely used
    reg                                             log2_parallel_is_great_2;     
          

    reg         [MAX_MERGE_CAND_WIDTH -1:0]             inter_maxnummergecand;
    
    //reg         [MERGE_CAND_TYPE_WIDTH -1:0]            merge_cand_list[0:MAX_NUM_MERGE_CAND_CONST -1];    //to do put labels in location to identify the candidate
    reg         [MERGE_CAND_TYPE_WIDTH -1:0]            merge_cand_list[0:5 -1];    //to do put labels in location to identify the candidate
    reg         [MERGE_CAND_TYPE_WIDTH -1:0]            mvpl0_cand_list[0:AMVP_NUM_CAND_CONST -1];    
    reg         [MERGE_CAND_TYPE_WIDTH -1:0]            mvpl1_cand_list[0:AMVP_NUM_CAND_CONST -1];    
    reg         [MAX_MERGE_CAND_WIDTH -1:0]             merge_array_idx;
    reg         [MAX_MERGE_CAND_WIDTH -1:0]             mvp_l0_idx;
    reg         [MAX_MERGE_CAND_WIDTH -1:0]             mvp_l1_idx;
    reg                                                 is_merge_array_idx_now;
    reg                                                 is_merge_array_idx_now_d;
    reg         [SLICE_TYPE_WIDTH -1:0]                 slice_type;
    reg                                                 slice_temporal_mvp_enabled_flag;
    reg         [NUM_REF_IDX_L0_MINUS1_WIDTH - 1:0]     num_ref_idx_l0_active;
    reg         [NUM_REF_IDX_L1_MINUS1_WIDTH - 1:0]     num_ref_idx_l1_active;
    reg                                                 collocated_from_l0_flag;
    
    reg         [COLLOCATED_REF_IDX_WIDTH - 2: 0]        collocated_ref_idx;
    
    
    reg  signed [POC_WIDTH -1: 0]                       current_poc;    
    reg  signed [POC_WIDTH -1: 0]                       ref_poc_l0;    
    reg  signed [POC_WIDTH -1: 0]                       ref_poc_cand_l0;    
    reg  signed [POC_WIDTH -1: 0]                       ref_poc_l1;    
    reg  signed [POC_WIDTH -1: 0]                       ref_poc_cand_l1;    
    reg  signed [POC_WIDTH -1: 0]                       tbl0;    
    reg  signed [CLIPPED_WIDTH -1: 0]                               tbl0_clipped;    
    reg  signed [CLIPPED_WIDTH -1: 0]                               tdl0_clipped;    
    reg  signed [POC_WIDTH -1: 0]                       tbl1;    
    reg  signed [CLIPPED_WIDTH -1: 0]                               tbl1_clipped; 
    reg  signed [CLIPPED_WIDTH -1: 0]                               tdl1_clipped; 
    reg                                                 is_diff_poc_curr_l0_zero;
    reg                                                 is_diff_poc_curr_l1_zero;
    
    

    reg  signed [REF_PIC_LIST_POC_DATA_WIDTH -1: 0]         ref_pic_list0_poc_data_in;
    reg  signed [REF_PIC_LIST_POC_DATA_WIDTH -1: 0]         ref_pic_list1_poc_data_in;
    wire signed [REF_PIC_LIST_POC_DATA_WIDTH -1: 0]         ref_pic_list0_poc_data_out;
    wire signed [REF_PIC_LIST_POC_DATA_WIDTH -1: 0]         ref_pic_list1_poc_data_out;
    reg         [NUM_REF_IDX_L0_MINUS1_WIDTH -1: 0]         ref_pic_list0_poc_addr;
    reg         [NUM_REF_IDX_L0_MINUS1_WIDTH -1: 0]         ref_pic_list0_idx_addr;
    reg         [NUM_REF_IDX_L0_MINUS1_WIDTH -1: 0]         ref_pic_list1_poc_addr;
    reg         [NUM_REF_IDX_L0_MINUS1_WIDTH -1: 0]         ref_pic_list1_idx_addr;
    
    reg                                                     ref_pic_list_idx_wr_en;
    reg         [DPB_FRAME_OFFSET_WIDTH -1: 0]              ref_pic_list0_idx_data_in;
    wire        [DPB_FRAME_OFFSET_WIDTH -1: 0]              ref_pic_list0_idx_data_out;
    reg         [DPB_FRAME_OFFSET_WIDTH -1: 0]              ref_pic_list1_idx_data_in;
    wire        [DPB_FRAME_OFFSET_WIDTH -1: 0]              ref_pic_list1_idx_data_out;
    
        
    reg                                                     ref_pic_list0_poc_wr_en;
    reg                                                     ref_pic_list1_poc_wr_en;
    
 
    reg         [DPB_ADDR_WIDTH -1: 0]                      current_pic_dpb_idx;
        
    wire                                                    avaialble_flag_out;
    reg         [AVAILABLE_CONFIG_BUS_WIDTH -1:0]           available_config_bus_mode;
    reg         [X11_ADDR_WDTH + X_ADDR_WDTH-1 :0]          available_config_bus;
    
    wire        [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   xN_pu_out;
    wire        [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   yN_pu_out;
    
    reg  [X11_ADDR_WDTH - 1:0]     cb_xx;
    reg  [X11_ADDR_WDTH - 1:0]     cb_xx_end;
    reg  [X11_ADDR_WDTH - 1:0]     cb_xx_plus_half_cb;
    reg  [X11_ADDR_WDTH - 1:0]     ctb_xx;
    reg  [X11_ADDR_WDTH - 1:0]     ctb_xx_end;
    wire [X11_ADDR_WDTH - 1:0]     xx_pb_temp;
    reg  [X11_ADDR_WDTH - 1:0]     xx_pb_end;
    reg  [Y11_ADDR_WDTH - 1:0]     ctb_yy;
    reg  [Y11_ADDR_WDTH - 1:0]     ctb_yy_end;
    reg  [Y11_ADDR_WDTH - 1:0]     cb_yy;
    reg  [Y11_ADDR_WDTH - 1:0]     cb_yy_end;
    reg  [Y11_ADDR_WDTH - 1:0]     cb_yy_plus_half_cb;
    wire [Y11_ADDR_WDTH - 1:0]     yy_pb_temp;
    reg  [Y11_ADDR_WDTH - 1:0]     yy_pb_end;
    reg                             mv_read_from_top;
    reg                             mv_read_from_top_1d;
    reg                             mv_read_from_top_2d;
    reg                             mv_read_from_top_3d;
    
    reg  [CB_SIZE_WIDTH -1:0 ]  cb_size;
    reg                         cb_size_is_8;
    reg  [CB_SIZE_WIDTH -1:0 ]  cb_size_wire;
    wire [LOG2_CB_SIZE_WIDTH -1:0 ]  log2_cb_size_wire;
    wire [CB_SIZE_WIDTH -1:0 ]  hh_pb_temp;
    reg  [CB_SIZE_WIDTH -1:0 ]  nOrigPbW;
    wire [CB_SIZE_WIDTH -1:0 ]  ww_pb_temp;
    reg  [CB_SIZE_WIDTH -1:0 ]  nOrigPbH;
    reg                         cb_pred_mode;    
    reg  [PARTMODE_WIDTH -1:0]  cb_part_mode;
    
    reg  [PART_IDX_WIDTH -1:0]  pb_part_idx;
    
    reg [PRED_IDC_WIDTH -1:0]   pred_idc;
    reg                         merge_flag;
    reg [MERGE_IDX_WIDTH-1:0]   merge_idx;
    reg                         mvp_l0_flag;
    reg                         mvp_l1_flag;
    reg [REF_IDX_LX_WIDTH-1:0]  ref_idx_l0;
    reg [REF_IDX_LX_WIDTH-1:0]  ref_idx_l1;
    
    reg         [MAX_MERGE_CAND_WIDTH -1:0] current_cand_num;
    reg         [MAX_MERGE_CAND_WIDTH -1:0] cand_num_id;
    reg signed [MVD_WIDTH -1:0]        mvd_l0_x;
    reg signed [MVD_WIDTH -1:0]        mvd_l0_y;
    reg signed [MVD_WIDTH -1:0]        mvd_l1_x;
    reg signed [MVD_WIDTH -1:0]        mvd_l1_y;
    
    reg                         part_mode_check_merge_4;
    
    reg  signed [X_CAND_WDTH -1:0]                        xN_in;
    reg  signed [X_CAND_WDTH -1:0]                        yN_in;
    
    reg                             is_smallest_pu;            
    reg  [CB_SIZE_WIDTH     :0 ]    pu_dim_sum;    
    reg                             part_mode_left_right;
    reg                             part_mode_2n_n;
    reg                             part_mode_n_2n;
    reg                             part_mode_up_down;
    reg                             part_mode_n_n;
    
    
    
    reg                             cand_x_down_range_check;
    reg                             cand_y_range_check;
    reg                             cand_y_down_range_check;
    reg                             cand_x_up_range_check;
    reg                             cand_x_range_check;
    reg                             cand_y_up_range_check;
    reg                             is_cand_in_prev_pu;
    
    reg  [X11_ADDR_WDTH - 1:0]    xx_pb_prev;
    reg  [Y11_ADDR_WDTH - 1:0]    yy_pb_prev;
    reg  [CB_SIZE_WIDTH -1:0 ]  hh_pb_prev;    
    reg  [CB_SIZE_WIDTH -1:0 ]  ww_pb_prev;
    
    reg                         is_diff_mer_x;
    reg                         is_diff_mer_y;
    reg                         is_part_idx_equal_1;
    reg                         part_idx_mode_logic_merge_5;
    reg                         part_merge_logic_merge_6;
    
    reg                         available_flag_to_mv_buffer;
    reg     [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   xN_pu_mv_buffer_in;
    reg     [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   yN_pu_mv_buffer_in;    
      

    
    wire                                            same_cb_check;
    reg                                             nbr_x_3_qdrant,nbr_y_3_qdrant;
    reg                                             qudrant3_exception;
    reg                                             is_mvd_l0_filled, is_mvd_l1_filled;
    wire [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE - 1:0]     xPpu_in = xx_pb>>LOG2_MIN_PU_SIZE;
    wire [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE - 1:0]     yPpu_in = yy_pb>>LOG2_MIN_PU_SIZE;
    
    wire                                    cand_mv_field_pred_flag_l0      ;
    wire                                    cand_mv_field_pred_flag_l1      ;
    wire [REF_IDX_LX_WIDTH -1:0]            cand_mv_field_ref_idx_l0        ;
    reg  [REF_IDX_LX_WIDTH -1:0]            cand_mv_field_ref_idx_l0_d      ;
    wire [REF_IDX_LX_WIDTH -1:0]            cand_mv_field_ref_idx_l1        ;
    reg  [REF_IDX_LX_WIDTH -1:0]            cand_mv_field_ref_idx_l1_d      ;
    wire signed [MVD_WIDTH -1:0]            cand_mv_field_mv_x_l0           ;
    wire signed [MVD_WIDTH -1:0]            cand_mv_field_mv_y_l0           ;
    wire signed [MVD_WIDTH -1:0]            cand_mv_field_mv_x_l1           ;
    wire signed [MVD_WIDTH -1:0]            cand_mv_field_mv_y_l1           ;

    reg                                     a0_mv_field_pred_flag_l0         ;
    reg                                     a0_mv_field_pred_flag_l1         ;
    reg [REF_IDX_LX_WIDTH -1:0]             a0_mv_field_ref_idx_l0           ;
    reg [REF_IDX_LX_WIDTH -1:0]             a0_mv_field_ref_idx_l1           ;
    reg signed [MVD_WIDTH -1:0]             a0_mv_field_mv_x_l0              ;
    reg signed [MVD_WIDTH -1:0]             a0_mv_field_mv_y_l0              ;
    reg signed [MVD_WIDTH -1:0]             a0_mv_field_mv_x_l1              ;
    reg signed [MVD_WIDTH -1:0]             a0_mv_field_mv_y_l1              ;

    reg                                     a1_mv_field_pred_flag_l0         ;
    reg                                     a1_mv_field_pred_flag_l1         ;
    reg [REF_IDX_LX_WIDTH -1:0]             a1_mv_field_ref_idx_l0           ;
    reg [REF_IDX_LX_WIDTH -1:0]             a1_mv_field_ref_idx_l1           ;
    reg signed [MVD_WIDTH -1:0]             a1_mv_field_mv_x_l0              ;
    reg signed [MVD_WIDTH -1:0]             a1_mv_field_mv_y_l0              ;
    reg signed [MVD_WIDTH -1:0]             a1_mv_field_mv_x_l1              ;
    reg signed [MVD_WIDTH -1:0]             a1_mv_field_mv_y_l1              ;
    
    reg                                     b0_mv_field_pred_flag_l0         ;
    reg                                     b0_mv_field_pred_flag_l1         ;
    reg [REF_IDX_LX_WIDTH -1:0]             b0_mv_field_ref_idx_l0           ;
    reg [REF_IDX_LX_WIDTH -1:0]             b0_mv_field_ref_idx_l1           ;
    reg signed [MVD_WIDTH -1:0]             b0_mv_field_mv_x_l0              ;
    reg signed [MVD_WIDTH -1:0]             b0_mv_field_mv_y_l0              ;
    reg signed [MVD_WIDTH -1:0]             b0_mv_field_mv_x_l1              ;
    reg signed [MVD_WIDTH -1:0]             b0_mv_field_mv_y_l1              ;

    reg                                     b1_mv_field_pred_flag_l0         ;
    reg                                     b1_mv_field_pred_flag_l1         ;
    reg [REF_IDX_LX_WIDTH -1:0]             b1_mv_field_ref_idx_l0           ;
    reg [REF_IDX_LX_WIDTH -1:0]             b1_mv_field_ref_idx_l1           ;
    reg signed [MVD_WIDTH -1:0]             b1_mv_field_mv_x_l0              ;
    reg signed [MVD_WIDTH -1:0]             b1_mv_field_mv_y_l0              ;
    reg signed [MVD_WIDTH -1:0]             b1_mv_field_mv_x_l1              ;
    reg signed [MVD_WIDTH -1:0]             b1_mv_field_mv_y_l1              ;

    reg                                     b2_mv_field_pred_flag_l0         ;
    reg                                     b2_mv_field_pred_flag_l1         ;
    reg [REF_IDX_LX_WIDTH -1:0]             b2_mv_field_ref_idx_l0           ;
    reg [REF_IDX_LX_WIDTH -1:0]             b2_mv_field_ref_idx_l1           ;
    reg signed [MVD_WIDTH -1:0]             b2_mv_field_mv_x_l0              ;
    reg signed [MVD_WIDTH -1:0]             b2_mv_field_mv_y_l0              ;
    reg signed [MVD_WIDTH -1:0]             b2_mv_field_mv_x_l1              ;
    reg signed [MVD_WIDTH -1:0]             b2_mv_field_mv_y_l1              ;    

    
    wire                            pu_mv_buffer_done;
    wire                            ctu_write_done_out;
    
    reg                             new_ctu_start_in;
    

	`ifdef SYNTHESIZABLE
		    reg                             is_pb_ctu_last;
			reg                             is_pb_x_ctu_last;
			reg                             is_pb_y_ctu_last;
	`else
    (* keep = "true" *)     reg                             is_pb_ctu_last;
    (* keep = "true" *)     reg                             is_pb_x_ctu_last;
    (* keep = "true" *)     reg                             is_pb_y_ctu_last;
	`endif    
    reg                             is_available_a0;
    reg                             is_available_a1;
    reg                             is_available_b0;
    reg                             is_available_b1;
    reg                             is_available_b2;
    
    reg                             merge_cand_found;
    reg                             merge_cand_found_d;
    
    wire                             compare_a1b1_curr_ref_idx;
    wire                             compare_a1b1_curr_mv;
 
    wire                             compare_b1b0_curr_ref_idx;
    wire                             compare_b1b0_curr_mv;

    wire                             compare_a0a1_curr_ref_idx;
    wire                             compare_a0a1_curr_mv;
    
    wire                             compare_b2b1_curr_ref_idx;
    wire                             compare_b2b1_curr_mv;

    wire                             compare_b2a1_curr_ref_idx;
    wire                             compare_b2a1_curr_mv;    
    
    reg signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_a0_l0;
    reg signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_a0_l1;
    reg signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_a1_l0;
    reg signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_a1_l1;
    reg signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_b0_l0;
    reg signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_b0_l1;
    reg signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_b1_l0;
    reg signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_b1_l1;
    reg signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_b2_l0;
    reg signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_b2_l1;

    reg [DPB_ADDR_WIDTH -1:0]  ref_dpb_idx_a0_l0;
    reg [DPB_ADDR_WIDTH -1:0]  ref_dpb_idx_a0_l1;
    reg [DPB_ADDR_WIDTH -1:0]  ref_dpb_idx_a1_l0;
    reg [DPB_ADDR_WIDTH -1:0]  ref_dpb_idx_a1_l1;
    reg [DPB_ADDR_WIDTH -1:0]  ref_dpb_idx_b0_l0;
    reg [DPB_ADDR_WIDTH -1:0]  ref_dpb_idx_b0_l1;
    reg [DPB_ADDR_WIDTH -1:0]  ref_dpb_idx_b1_l0;
    reg [DPB_ADDR_WIDTH -1:0]  ref_dpb_idx_b1_l1;
    reg [DPB_ADDR_WIDTH -1:0]  ref_dpb_idx_b2_l0;
    reg [DPB_ADDR_WIDTH -1:0]  ref_dpb_idx_b2_l1;
    reg [DPB_ADDR_WIDTH -1:0]  ref_dpb_idx_l0; 
    reg [DPB_ADDR_WIDTH -1:0]  ref_dpb_idx_l1; 
    
    reg clear_bi_pred_list;
    wire    [NUM_BI_PRED_CANDS -1: 0]   bi_pred_cand_valid;
    reg    [NUM_BI_PRED_CANDS -1: 0]   bi_pred_cand_valid_d;
    reg    [DPB_ADDR_WIDTH -1: 0]   bi_pred_cand_idx;
    reg    [DPB_ADDR_WIDTH -1: 0]   bi_pred_cand_idx_chosen;
    reg    [DPB_ADDR_WIDTH -1: 0]   bi_pred_cand_idx_chosen_d;
    reg    [DPB_ADDR_WIDTH -1: 0]   bi_pred_cand_idx_chosen_2d;
    
    reg                             avaialble_flag_a0;
    reg                             avaialble_flag_a1;
    reg                             avaialble_flag_b0;
    reg                             avaialble_flag_b1;
    reg                             avaialble_flag_b2;
    reg                             avaialble_flag_a0_wire;
    reg                             avaialble_flag_4;
    
    reg         [MERGE_CAND_TYPE_WIDTH*MAX_NUM_MERGE_CAND_CONST -1:0]         merge_cand_list_in;
    
    wire                            col_preftch_done;
    
    
    reg is_scaled_flag;
    
    reg available_flag_a_l0;
    reg available_flag_a_l1;
    reg available_flag_b_l0;
    reg available_flag_b_l1;
    
    reg signed [MVD_WIDTH -1:0]   mv_l0_a_x;
    reg signed [MVD_WIDTH -1:0]   mv_l0_a_y;
    reg signed [MVD_WIDTH -1:0]   mv_l1_a_x;
    reg signed [MVD_WIDTH -1:0]   mv_l1_a_y;
    
    reg signed [MVD_WIDTH -1:0]   mv_l0_b_x;
    reg signed [MVD_WIDTH -1:0]   mv_l0_b_y;
    reg signed [MVD_WIDTH -1:0]   mv_l1_b_x;
    reg signed [MVD_WIDTH -1:0]   mv_l1_b_y;    
    
    
    reg                     is_mvp_l0_idx_now;
    reg                     is_mvp_l0_idx_now_d;
    reg                     is_mvp_l1_idx_now;
    reg                     is_mvp_l1_idx_now_d;
    reg                     mvp_l0_cand_found;
    reg                     mvp_l1_cand_found;
    reg                     mvp_both_cand_found;
    
    wire  signed  [TX_WIDTH -1:0]             tx_l0;
    reg   signed  [TX_WIDTH + CLIPPED_WIDTH -1:0]             distscale_factor_l0;
    wire  signed  [TX_WIDTH -1:0]             tx_l1;
    reg   signed  [TX_WIDTH + CLIPPED_WIDTH -1:0]             distscale_factor_l1;
    reg   signed  [DIST_SCALE_WIDTH -1:0]     distscale_factor_l0_clipped;
    reg   signed  [DIST_SCALE_WIDTH -1:0]     distscale_factor_l1_clipped;
    wire                            tx_enable = !merge_flag;
    
    reg                     is_available_cand;
    reg                     is_available_cand_1d;
    reg                     is_available_cand_2d;
    reg                     is_available_cand_3d;
    reg                     is_available_cand_4d;
    reg                     is_available_cand_5d;
    reg                     is_available_cand_6d;
    reg                     is_available_cand_7d;
    reg                     is_available_cand_8d;
    
    
    reg  signed [POC_WIDTH -1:0]   diff_curr_ref_cand_l0;// = current_poc - ref_poc_cand_l0;
    reg  signed [POC_WIDTH -1:0]   diff_curr_ref_cand_l1;// = current_poc - ref_poc_cand_l1;
    reg                             zero_diff_refl0_cand_l0;// = current_poc - ref_poc_cand_l0;
    reg                             zero_diff_refl1_cand_l0;// = current_poc - ref_poc_cand_l0;
    reg                             zero_diff_refl0_cand_l1;// = current_poc - ref_poc_cand_l0;
    reg                             zero_diff_refl1_cand_l1;// = current_poc - ref_poc_cand_l0;
    reg                             mv_a_l0_updated_later;
    reg                             mv_a_l1_updated_later;
    reg                             mv_b_l0_updated_later;
    reg                             mv_b_l1_updated_later;
    
    reg   [1:0]                     mv_a_l0_filled_type;
    reg   [1:0]                     mv_a_l1_filled_type;
    reg   [1:0]                     mv_b_l0_filled_type;    
    reg   [1:0]                     mv_b_l1_filled_type;    
  
    reg                             cand_l0_pred_flag;
    reg                             cand_l0_pred_flag_d;
    reg                             cand_l0_pred_flag_2d;
    reg                             cand_l1_pred_flag;
    reg                             cand_l1_pred_flag_d;
    reg                             cand_l1_pred_flag_2d;

    reg signed   [29 -1:0]        dist_scale_into_mv_l0_x;
    reg signed   [29 -1:0]        dist_scale_into_mv_l0_y;
    reg signed   [29 -1:0]        dist_scale_into_mv_l1_x;
    reg signed   [29 -1:0]        dist_scale_into_mv_l1_y;

    reg signed   [29 -1:0]        dist_scale_into_mv_l0_x_d;
    reg signed   [29 -1:0]        dist_scale_into_mv_l0_y_d;
    reg signed   [29 -1:0]        dist_scale_into_mv_l1_x_d;
    reg signed   [29 -1:0]        dist_scale_into_mv_l1_y_d;
    
    wire                        sign_dist_scale_into_mv_l0_x = (dist_scale_into_mv_l0_x_d[29-1]==0) ? 1: 0;//(dist_scale_into_mv_l0_x_d>0) ? 1: 0;      // todo : hope checking one the MSB for sign of the value is ok
    wire                        sign_dist_scale_into_mv_l0_y = (dist_scale_into_mv_l0_y_d[29-1]==0) ? 1: 0;//(dist_scale_into_mv_l0_y_d>0) ? 1: 0;
    wire                        sign_dist_scale_into_mv_l1_x = (dist_scale_into_mv_l1_x_d[29-1]==0) ? 1: 0;//(dist_scale_into_mv_l1_x_d>0) ? 1: 0;
    wire                        sign_dist_scale_into_mv_l1_y = (dist_scale_into_mv_l1_y_d[29-1]==0) ? 1: 0;//(dist_scale_into_mv_l1_y_d>0) ? 1: 0;

    wire signed   [30 -1:0]        abs_dist_scale_into_mv_l0_x_plus_127 = sign_dist_scale_into_mv_l0_x ? (dist_scale_into_mv_l0_x_d + 127) : (~(dist_scale_into_mv_l0_x_d) + 128);
    wire signed   [30 -1:0]        abs_dist_scale_into_mv_l0_y_plus_127 = sign_dist_scale_into_mv_l0_y ? (dist_scale_into_mv_l0_y_d + 127) : (~(dist_scale_into_mv_l0_y_d) + 128);
    wire signed   [30 -1:0]        abs_dist_scale_into_mv_l1_x_plus_127 = sign_dist_scale_into_mv_l1_x ? (dist_scale_into_mv_l1_x_d + 127) : (~(dist_scale_into_mv_l1_x_d) + 128);
    wire signed   [30 -1:0]        abs_dist_scale_into_mv_l1_y_plus_127 = sign_dist_scale_into_mv_l1_y ? (dist_scale_into_mv_l1_y_d + 127) : (~(dist_scale_into_mv_l1_y_d) + 128);
    
    
    // wire signed   [30 -1:0]        abs_dist_scale_into_mv_l0_x_plus_127 = abs_dist_scale_into_mv_l0_x + 127;
    // wire signed   [30 -1:0]        abs_dist_scale_into_mv_l0_y_plus_127 = abs_dist_scale_into_mv_l0_y + 127;
    // wire signed   [30 -1:0]        abs_dist_scale_into_mv_l1_x_plus_127 = abs_dist_scale_into_mv_l1_x + 127;
    // wire signed   [30 -1:0]        abs_dist_scale_into_mv_l1_y_plus_127 = abs_dist_scale_into_mv_l1_y + 127;    
    
    
    reg signed [21 - 1:0]         new_mv_l0_x;// = (sign_dist_scale_into_mv_l0_x)? (abs_dist_scale_into_mv_l0_x_plus_127>>8): -1*(abs_dist_scale_into_mv_l0_x_plus_127>>8);
    reg signed [21 - 1:0]         new_mv_l0_y;// = (sign_dist_scale_into_mv_l0_y)? (abs_dist_scale_into_mv_l0_y_plus_127>>8): -1*(abs_dist_scale_into_mv_l0_y_plus_127>>8);
    reg signed [21 - 1:0]         new_mv_l1_x;// = (sign_dist_scale_into_mv_l1_x)? (abs_dist_scale_into_mv_l1_x_plus_127>>8): -1*(abs_dist_scale_into_mv_l1_x_plus_127>>8);
    reg signed [21 - 1:0]         new_mv_l1_y;// = (sign_dist_scale_into_mv_l1_y)? (abs_dist_scale_into_mv_l1_y_plus_127>>8): -1*(abs_dist_scale_into_mv_l1_y_plus_127>>8);
    
    reg signed [MVD_WIDTH - 1:0]         pipe_line_mv_l0_x;
    reg signed [MVD_WIDTH - 1:0]         pipe_line_mv_l0_y;
    reg signed [MVD_WIDTH - 1:0]         pipe_line_mv_l1_x;
    reg signed [MVD_WIDTH - 1:0]         pipe_line_mv_l1_y;
    
    //todo change this clip operation similar to temporal engine
    wire signed [MVD_WIDTH -1:0] new_mv_l0_x_clipped = (new_mv_l0_x > 32767) ?32767: ((new_mv_l0_x < -32768)? -32768 : new_mv_l0_x[MVD_WIDTH-1:0]);
    wire signed [MVD_WIDTH -1:0] new_mv_l0_y_clipped = (new_mv_l0_y > 32767) ?32767: ((new_mv_l0_y < -32768)? -32768 : new_mv_l0_y[MVD_WIDTH-1:0]);
    wire signed [MVD_WIDTH -1:0] new_mv_l1_x_clipped = (new_mv_l1_x > 32767) ?32767: ((new_mv_l1_x < -32768)? -32768 : new_mv_l1_x[MVD_WIDTH-1:0]);
    wire signed [MVD_WIDTH -1:0] new_mv_l1_y_clipped = (new_mv_l1_y > 32767) ?32767: ((new_mv_l1_y < -32768)? -32768 : new_mv_l1_y[MVD_WIDTH-1:0]);
    
    reg     poc_diff_zero_type_b_done;
    reg     mv_a_filled_last_cycle_l0;
    reg     mv_a_filled_last_cycle_l1;
    
    reg     [X11_ADDR_WDTH  -1:0]                                   col_xbr_addr;
    reg     [CTB_SIZE_WIDTH - LOG2_MIN_COLLOCATE_SIZE -1 + 1:0]         col_xctr_addr;
    reg     [X11_ADDR_WDTH  -1:0]                                   col_ybr_addr;
    reg     [CTB_SIZE_WIDTH - LOG2_MIN_COLLOCATE_SIZE -1:0]         col_yctr_addr;
    // reg                                                             new_ctu_start_to_col;    
    
    wire    [X11_ADDR_WDTH - 1:0] xx_pb_ww_add = xx_pb + ww_pb;
    wire    [CTB_SIZE_WIDTH - 1:0] xx_pb_ww2_add = xx_pb + (ww_pb>>1);
    wire    [X11_ADDR_WDTH - 1:0] yy_pb_hh_add = yy_pb + hh_pb;
    wire    [CTB_SIZE_WIDTH - 1:0] yy_pb_hh2_add = yy_pb + (hh_pb>>1);
    
    reg     col_addr_valid_in;
    reg     [INTER_TOP_CONFIG_BUS_MODE_WIDTH -1 :0]     config_mode_in_d;
    reg     [DPB_ADDR_WIDTH -1: 0]                    collocated_dpb_idx;
    // reg   [DPB_ADDR_WIDTH -1: 0]                    collocated_dpb_idx_d;
    reg   [POC_WIDTH -1: 0]                         col_poc;
    reg   [POC_WIDTH -1: 0]                         col_poc_d;
    reg                                             is_new_pic_col_ref_idx;
    
    reg                                             col_br_boundary_ok;
    wire                                            col_mv_ready;
    wire                                            available_col_l0_flag;
    wire                                            available_col_l1_flag;
    wire [MVD_WIDTH -1:0]                           col_mv_field_mv_x_l0_to_inter;
    wire [MVD_WIDTH -1:0]                           col_mv_field_mv_y_l0_to_inter;
    wire [MVD_WIDTH -1:0]                           col_mv_field_mv_x_l1_to_inter;
    wire [MVD_WIDTH -1:0]                           col_mv_field_mv_y_l1_to_inter;
    reg                                             from_top_ref_pic_list_valid;
    reg                                             mvp_l0_mvs_not_same;
    reg                                             mvp_l1_mvs_not_same;
    
    wire mv_l0_b_x_same_mv_l0_a_x =    ( mv_l0_b_x == mv_l0_a_x)? 1'b1 : 1'b0;
    wire mv_l0_b_y_same_mv_l0_a_y =    ( mv_l0_b_y == mv_l0_a_y)? 1'b1 : 1'b0;
    wire mv_l1_b_x_same_mv_l1_a_x =    ( mv_l1_b_x == mv_l1_a_x)? 1'b1 : 1'b0;
    wire mv_l1_b_y_same_mv_l0_a_y =    ( mv_l1_b_y == mv_l1_a_y)? 1'b1 : 1'b0;
    
    wire [MERGE_CAND_TYPE_WIDTH -1:0 ]    bi_cand0_type;
    wire [MERGE_CAND_TYPE_WIDTH -1:0 ]    bi_cand1_type;
    wire [MERGE_CAND_TYPE_WIDTH -1:0 ]    bi_cand2_type;
    wire [MERGE_CAND_TYPE_WIDTH -1:0 ]    bi_cand3_type;
    
    reg                                     bi_mv_field_pred_flag_l0         ;
    reg                                     bi_mv_field_pred_flag_l1         ;
    reg [REF_IDX_LX_WIDTH -1:0]             bi_mv_field_ref_idx_l0           ;
    reg [REF_IDX_LX_WIDTH -1:0]             bi_mv_field_ref_idx_l1           ;
    reg signed [MVD_WIDTH -1:0]             bi_mv_field_mv_x_l0              ;
    reg signed [MVD_WIDTH -1:0]             bi_mv_field_mv_y_l0              ;
    reg signed [MVD_WIDTH -1:0]             bi_mv_field_mv_x_l1              ;
    reg signed [MVD_WIDTH -1:0]             bi_mv_field_mv_y_l1              ; 
    reg [DPB_ADDR_WIDTH -1:0]               ref_dpb_idx_bi_l0                ; 
    reg [DPB_ADDR_WIDTH -1:0]               ref_dpb_idx_bi_l1                ; 
    
    reg [REF_IDX_LX_WIDTH -1:0] zero_ref_idx;
    reg [REF_IDX_LX_WIDTH -1:0] num_ref_idx;
    
    reg cand_x_prev_top_left;
    reg cand_x_prev_top_left_d;
    reg cand_x_prev_top_left_2d;
    
    wire bs_top_valid;
    wire bs_left_valid;
    


    wire bs_top_valid_feed;
    wire bs_left_valid_feed;

	reg [AXI_ADDR_WDTH-1:0] axi_col_mv_frame_offet;
	reg current_mv_valid_now;
	wire col_write_done_out;
 
    // design notes-----  no need. internally handled - when a cu is intra, send an artificial pu packet to inter with pred mode set to intra
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
`ifdef CHIPSCOPE_DEBUG
  assign mv_state_8bit_out = mv_derive_state[7:0];
`endif
   
// Instantiate the module
    assign pref_ctu_col_writ_ctu_done_out = ctu_write_done_out;//col_preftch_done & ctu_write_done_out;
    assign bs_left_valid_feed = (cb_xx == xx_pb);
    assign bs_top_valid_feed =  (cb_yy == yy_pb);

    assign bs_top_valid_out = bs_top_valid ;
    assign bs_left_valid_out = bs_left_valid ;

s_sync_ram  #(
    .DATA_WIDTH(DPB_FRAME_OFFSET_WIDTH),
    .ADDR_WIDTH(NUM_REF_IDX_L0_MINUS1_WIDTH),
    .DATA_DEPTH((1<<NUM_REF_IDX_L0_MINUS1_WIDTH))
) ref_pic_list0_dpb_idx
(
    .clk(clk), 
    .addr_in(ref_pic_list0_idx_addr), 
    .r_data_out(ref_pic_list0_idx_data_out), 
    .w_data_in(ref_pic_list0_idx_data_in), 
    .w_en_in(ref_pic_list_idx_wr_en), 
    .enable(enable)
);

s_sync_ram  #(
    .DATA_WIDTH(DPB_FRAME_OFFSET_WIDTH),
    .ADDR_WIDTH(NUM_REF_IDX_L0_MINUS1_WIDTH),
    .DATA_DEPTH((1<<NUM_REF_IDX_L0_MINUS1_WIDTH))
) ref_pic_list1_dpb_idx
(
    .clk(clk), 
    .addr_in(ref_pic_list1_idx_addr), 
    .r_data_out(ref_pic_list1_idx_data_out), 
    .w_data_in(ref_pic_list1_idx_data_in), 
    .w_en_in(ref_pic_list_idx_wr_en), 
    .enable(enable)
);


s_sync_ram  #(
    .DATA_WIDTH(REF_PIC_LIST_POC_DATA_WIDTH),
    .ADDR_WIDTH(NUM_REF_IDX_L0_MINUS1_WIDTH),
    .DATA_DEPTH((1<<NUM_REF_IDX_L0_MINUS1_WIDTH))
) ref_pic_list_0
(
    .clk(clk), 
    .addr_in(ref_pic_list0_poc_addr), 
    .r_data_out(ref_pic_list0_poc_data_out), 
    .w_data_in(ref_pic_list0_poc_data_in), 
    .w_en_in(ref_pic_list0_poc_wr_en), 
    .enable(enable)
);

s_sync_ram  #(
    .DATA_WIDTH(REF_PIC_LIST_POC_DATA_WIDTH),
    .ADDR_WIDTH(NUM_REF_IDX_L0_MINUS1_WIDTH),
    .DATA_DEPTH((1<<NUM_REF_IDX_L0_MINUS1_WIDTH))
) ref_pic_list_1
(
    .clk(clk), 
    .addr_in(ref_pic_list1_poc_addr), 
    .r_data_out(ref_pic_list1_poc_data_out), 
    .w_data_in(ref_pic_list1_poc_data_in), 
    .w_en_in(ref_pic_list1_poc_wr_en), 
    .enable(enable)
);

pb_xyhw_gen pb_xyhw_gen_block (
    .clk(clk), 
    // .reset(reset), 
    .cb_part_mode(cb_part_mode), 
    .pb_part_idx(pb_part_idx), 
    .cb_size(cb_size), 
    .cb_xx(cb_xx), 
    .cb_yy(cb_yy), 
    .xx_pb(xx_pb_temp), 
    .yy_pb(yy_pb_temp), 
    .hh_pb(hh_pb_temp), 
    .ww_pb(ww_pb_temp)
    );

compare_mv_ref_idx a1_b1_compare_block (
    .clk(clk),
    .in1_pred_flag_l0       (a1_mv_field_pred_flag_l0  ), 
    .in1_pred_flag_l1       (a1_mv_field_pred_flag_l1  ), 
    .in1_mv_ref_idx_l0      (a1_mv_field_ref_idx_l0    ), 
    .in1_mv_ref_idx_l1      (a1_mv_field_ref_idx_l1    ), 
    .in1_mv_x_l0            (a1_mv_field_mv_x_l0       ), 
    .in1_mv_y_l0            (a1_mv_field_mv_y_l0       ), 
    .in1_mv_x_l1            (a1_mv_field_mv_x_l1       ), 
    .in1_mv_y_l1            (a1_mv_field_mv_y_l1       ), 
    .in2_pred_flag_l0       (b1_mv_field_pred_flag_l0  ), 
    .in2_pred_flag_l1       (b1_mv_field_pred_flag_l1  ),
    .in2_mv_ref_idx_l0      (b1_mv_field_ref_idx_l0    ), 
    .in2_mv_ref_idx_l1      (b1_mv_field_ref_idx_l1    ), 
    .in2_mv_x_l0            (b1_mv_field_mv_x_l0       ), 
    .in2_mv_y_l0            (b1_mv_field_mv_y_l0       ), 
    .in2_mv_x_l1            (b1_mv_field_mv_x_l1       ), 
    .in2_mv_y_l1            (b1_mv_field_mv_y_l1       ), 
    .compare_in1in2_ref_idx (compare_a1b1_curr_ref_idx     ), 
    .compare_in1in2_mv      (compare_a1b1_curr_mv)
    );

compare_mv_ref_idx b0_b1_compare_block (
    .clk(clk),
    .in1_pred_flag_l0       (b0_mv_field_pred_flag_l0  ), 
    .in1_pred_flag_l1       (b0_mv_field_pred_flag_l1  ), 
    .in1_mv_ref_idx_l0      (b0_mv_field_ref_idx_l0    ), 
    .in1_mv_ref_idx_l1      (b0_mv_field_ref_idx_l1    ), 
    .in1_mv_x_l0            (b0_mv_field_mv_x_l0       ), 
    .in1_mv_y_l0            (b0_mv_field_mv_y_l0       ), 
    .in1_mv_x_l1            (b0_mv_field_mv_x_l1       ), 
    .in1_mv_y_l1            (b0_mv_field_mv_y_l1       ), 
    .in2_pred_flag_l0       (b1_mv_field_pred_flag_l0  ), 
    .in2_pred_flag_l1       (b1_mv_field_pred_flag_l1  ),
    .in2_mv_ref_idx_l0      (b1_mv_field_ref_idx_l0    ), 
    .in2_mv_ref_idx_l1      (b1_mv_field_ref_idx_l1    ), 
    .in2_mv_x_l0            (b1_mv_field_mv_x_l0       ), 
    .in2_mv_y_l0            (b1_mv_field_mv_y_l0       ), 
    .in2_mv_x_l1            (b1_mv_field_mv_x_l1       ), 
    .in2_mv_y_l1            (b1_mv_field_mv_y_l1       ), 
    .compare_in1in2_ref_idx (compare_b1b0_curr_ref_idx     ), 
    .compare_in1in2_mv      (compare_b1b0_curr_mv)
    );

compare_mv_ref_idx a0_a1_compare_block (
    .clk(clk),
    .in1_pred_flag_l0       (a0_mv_field_pred_flag_l0  ), 
    .in1_pred_flag_l1       (a0_mv_field_pred_flag_l1  ), 
    .in1_mv_ref_idx_l0      (a0_mv_field_ref_idx_l0    ), 
    .in1_mv_ref_idx_l1      (a0_mv_field_ref_idx_l1    ), 
    .in1_mv_x_l0            (a0_mv_field_mv_x_l0       ), 
    .in1_mv_y_l0            (a0_mv_field_mv_y_l0       ), 
    .in1_mv_x_l1            (a0_mv_field_mv_x_l1       ), 
    .in1_mv_y_l1            (a0_mv_field_mv_y_l1       ), 
    .in2_pred_flag_l0       (a1_mv_field_pred_flag_l0  ), 
    .in2_pred_flag_l1       (a1_mv_field_pred_flag_l1  ),
    .in2_mv_ref_idx_l0      (a1_mv_field_ref_idx_l0    ), 
    .in2_mv_ref_idx_l1      (a1_mv_field_ref_idx_l1    ), 
    .in2_mv_x_l0            (a1_mv_field_mv_x_l0       ), 
    .in2_mv_y_l0            (a1_mv_field_mv_y_l0       ), 
    .in2_mv_x_l1            (a1_mv_field_mv_x_l1       ), 
    .in2_mv_y_l1            (a1_mv_field_mv_y_l1       ), 
    .compare_in1in2_ref_idx (compare_a0a1_curr_ref_idx     ), 
    .compare_in1in2_mv      (compare_a0a1_curr_mv)
    );

    
    
compare_mv_ref_idx b2a1_compare_block (
    .clk(clk),
    .in1_pred_flag_l0       (b2_mv_field_pred_flag_l0  ), 
    .in1_pred_flag_l1       (b2_mv_field_pred_flag_l1  ), 
    .in1_mv_ref_idx_l0      (b2_mv_field_ref_idx_l0    ), 
    .in1_mv_ref_idx_l1      (b2_mv_field_ref_idx_l1    ), 
    .in1_mv_x_l0            (b2_mv_field_mv_x_l0       ), 
    .in1_mv_y_l0            (b2_mv_field_mv_y_l0       ), 
    .in1_mv_x_l1            (b2_mv_field_mv_x_l1       ), 
    .in1_mv_y_l1            (b2_mv_field_mv_y_l1       ), 
    .in2_pred_flag_l0       (a1_mv_field_pred_flag_l0  ), 
    .in2_pred_flag_l1       (a1_mv_field_pred_flag_l1  ),
    .in2_mv_ref_idx_l0      (a1_mv_field_ref_idx_l0    ), 
    .in2_mv_ref_idx_l1      (a1_mv_field_ref_idx_l1    ), 
    .in2_mv_x_l0            (a1_mv_field_mv_x_l0       ), 
    .in2_mv_y_l0            (a1_mv_field_mv_y_l0       ), 
    .in2_mv_x_l1            (a1_mv_field_mv_x_l1       ), 
    .in2_mv_y_l1            (a1_mv_field_mv_y_l1       ), 
    .compare_in1in2_ref_idx (compare_b2a1_curr_ref_idx     ), 
    .compare_in1in2_mv      (compare_b2a1_curr_mv)
    );

compare_mv_ref_idx b2_b1_compare_block (
    .clk(clk),
    .in1_pred_flag_l0       (b2_mv_field_pred_flag_l0  ), 
    .in1_pred_flag_l1       (b2_mv_field_pred_flag_l1  ), 
    .in1_mv_ref_idx_l0      (b2_mv_field_ref_idx_l0    ), 
    .in1_mv_ref_idx_l1      (b2_mv_field_ref_idx_l1    ), 
    .in1_mv_x_l0            (b2_mv_field_mv_x_l0       ), 
    .in1_mv_y_l0            (b2_mv_field_mv_y_l0       ), 
    .in1_mv_x_l1            (b2_mv_field_mv_x_l1       ), 
    .in1_mv_y_l1            (b2_mv_field_mv_y_l1       ), 
    .in2_pred_flag_l0       (b1_mv_field_pred_flag_l0  ), 
    .in2_pred_flag_l1       (b1_mv_field_pred_flag_l1  ),
    .in2_mv_ref_idx_l0      (b1_mv_field_ref_idx_l0    ), 
    .in2_mv_ref_idx_l1      (b1_mv_field_ref_idx_l1    ), 
    .in2_mv_x_l0            (b1_mv_field_mv_x_l0       ), 
    .in2_mv_y_l0            (b1_mv_field_mv_y_l0       ), 
    .in2_mv_x_l1            (b1_mv_field_mv_x_l1       ), 
    .in2_mv_y_l1            (b1_mv_field_mv_y_l1       ), 
    .compare_in1in2_ref_idx (compare_b2b1_curr_ref_idx     ), 
    .compare_in1in2_mv      (compare_b2b1_curr_mv)
    );
    
check_prediction_block_available available_block (
    .clk(clk), 
    .config_bus(available_config_bus), 
    .config_bus_mode(available_config_bus_mode), 
    .xPpu_in(xPpu_in), 
    .yPpu_in(yPpu_in), 
    .xN_in(xN_in), 
    .yN_in(yN_in), 
    .xCb_start_in(cb_xx),
    .xCb_end_in(cb_xx_end),  
    .yCb_start_in(cb_yy),
    .yCb_end_in(cb_yy_end),  
    .xN_pu_out(xN_pu_out), 
    .yN_pu_out(yN_pu_out), 
    .avaialble_flag_out(avaialble_flag_out),
    .same_cb_check(same_cb_check)
    );

    
mv_buffer mv_buffer_block (
    .clk(clk), 
    .reset(reset), 
    .enable(enable), 
    //.temporal_enable(slice_temporal_mvp_enabled_flag),
    .current_pic_dpb_idx(current_pic_dpb_idx), 
    
    .x_N_pu_in(xN_pu_mv_buffer_in), 
    .y_N_pu_in(yN_pu_mv_buffer_in), 
    .available_flag_in(available_flag_to_mv_buffer), 
    .mv_read_from_top(mv_read_from_top_3d), 
    .is_cand_in_prev_pu_in(is_cand_in_prev_pu), 
    .cand_x_prev_top_left_in(cand_x_prev_top_left_2d),

    .bs_left_valid_feed_in(bs_left_valid_feed),
    .bs_top_valid_feed_in(bs_top_valid_feed),
    
    .mv_field_pred_flag_l0_out(cand_mv_field_pred_flag_l0), 
    .mv_field_pred_flag_l1_out(cand_mv_field_pred_flag_l1), 
    .mv_field_ref_idx_l0_out(cand_mv_field_ref_idx_l0), 
    .mv_field_ref_idx_l1_out(cand_mv_field_ref_idx_l1), 
    .mv_field_mv_x_l0_out(cand_mv_field_mv_x_l0), 
    .mv_field_mv_y_l0_out(cand_mv_field_mv_y_l0), 
    .mv_field_mv_x_l1_out(cand_mv_field_mv_x_l1), 
    .mv_field_mv_y_l1_out(cand_mv_field_mv_y_l1), 
    
    .x_P_pu_start_in(xx_pb  [X11_ADDR_WDTH - 1:LOG2_MIN_PU_SIZE]), 
    .x_P_pu_end_in(xx_pb_end[X11_ADDR_WDTH - 1:LOG2_MIN_PU_SIZE]), 
    .y_P_pu_start_in(yy_pb  [X11_ADDR_WDTH - 1:LOG2_MIN_PU_SIZE]), 
    .y_P_pu_end_in(yy_pb_end[X11_ADDR_WDTH - 1:LOG2_MIN_PU_SIZE]), 
    .pu_mv_buffer_done_out(pu_mv_buffer_done), 
    
    .prev_mv_field_pred_flag_l0_in(current_mv_field_pred_flag_l0), 
    .prev_mv_field_pred_flag_l1_in(current_mv_field_pred_flag_l1), 
    .prev_mv_field_ref_idx_l0_in(current_mv_field_ref_idx_l0), 
    .prev_mv_field_ref_idx_l1_in(current_mv_field_ref_idx_l1), 
    .prev_mv_field_mv_x_l0_in(current_mv_field_mv_x_l0), 
    .prev_mv_field_mv_x_l1_in(current_mv_field_mv_x_l1), 
    .prev_mv_field_mv_y_l0_in(current_mv_field_mv_y_l0), 
    .prev_mv_field_mv_y_l1_in(current_mv_field_mv_y_l1), 
    .prev_mv_field_valid_in(current_mv_field_valid), 
	.prev_mv_field_valid_now(current_mv_valid_now),
    .prev_mv_field_dpb_idx_l0_in(current_dpb_idx_l0),
    .prev_mv_field_dpb_idx_l1_in(current_dpb_idx_l1),
	
    .mv_col_axi_awid        (mv_col_axi_awid), 
    .mv_col_axi_awlen       (mv_col_axi_awlen), 
    .mv_col_axi_awsize      (mv_col_axi_awsize), 
    .mv_col_axi_awburst     (mv_col_axi_awburst), 
    .mv_col_axi_awlock      (mv_col_axi_awlock), 
    .mv_col_axi_awcache     (mv_col_axi_awcache), 
    .mv_col_axi_awprot      (mv_col_axi_awprot), 
    .mv_col_axi_awvalid     (mv_col_axi_awvalid), 
    .mv_col_axi_awaddr      (mv_col_axi_awaddr), 
    .mv_col_axi_awready     (mv_col_axi_awready), 
    .mv_col_axi_wstrb       (mv_col_axi_wstrb), 
    .mv_col_axi_wlast       (mv_col_axi_wlast), 
    .mv_col_axi_wvalid      (mv_col_axi_wvalid), 
    .mv_col_axi_wdata       (mv_col_axi_wdata), 
    .mv_col_axi_wready      (mv_col_axi_wready), 
    // .mv_col_axi_bid         (mv_col_axi_bid), 
    .mv_col_axi_bresp       (mv_col_axi_bresp), 
    .mv_col_axi_bvalid      (mv_col_axi_bvalid), 
    .mv_col_axi_bready      (mv_col_axi_bready), 
    
    .last_pb_of_ctu_in      (is_pb_ctu_last), 
    .new_ctu_start_in       (new_ctu_start_in), 
    .ctu_write_done_out     (ctu_write_done_out), 
    .col_write_done_out		(col_write_done_out),
	
    .top_mvs_to_bs_out      (top_mvs_to_bs_out), 
    .left_mvs_to_bs_out     (left_mvs_to_bs_out), 
    .x_rel_top_out          (x_rel_top_out), 
    .y_rel_left_out         (y_rel_left_out), 
    .bs_left_valid_out          (bs_left_valid), 
    .bs_top_valid_out           (bs_top_valid),
    .pic_width_in_pu        (pic_width[X11_ADDR_WDTH - 1:LOG2_MIN_PU_SIZE]),
    .log2_ctb_size_in       (log2_ctb_size)
	,.col_state_axi_write(	col_state_axi_write)
    );
    
bi_pred_gen bi_pred_gen_block (
    .clk(clk), 
    .clear_list(clear_bi_pred_list),
    .merge_array_idx(merge_array_idx), 
    .merge_cand_list_in(merge_cand_list_in), 
    .a0_mv_field_pred_flag_l0(a0_mv_field_pred_flag_l0), 
    .a0_mv_field_pred_flag_l1(a0_mv_field_pred_flag_l1), 
    .a0_mv_field_mv_x_l0(a0_mv_field_mv_x_l0), 
    .a0_mv_field_mv_y_l0(a0_mv_field_mv_y_l0), 
    .a0_mv_field_mv_x_l1(a0_mv_field_mv_x_l1), 
    .a0_mv_field_mv_y_l1(a0_mv_field_mv_y_l1), 
    
    .a1_mv_field_pred_flag_l0(a1_mv_field_pred_flag_l0), 
    .a1_mv_field_pred_flag_l1(a1_mv_field_pred_flag_l1), 
    .a1_mv_field_mv_x_l0(a1_mv_field_mv_x_l0), 
    .a1_mv_field_mv_y_l0(a1_mv_field_mv_y_l0), 
    .a1_mv_field_mv_x_l1(a1_mv_field_mv_x_l1), 
    .a1_mv_field_mv_y_l1(a1_mv_field_mv_y_l1), 
    
    .b0_mv_field_pred_flag_l0(b0_mv_field_pred_flag_l0), 
    .b0_mv_field_pred_flag_l1(b0_mv_field_pred_flag_l1), 
    .b0_mv_field_mv_x_l0(b0_mv_field_mv_x_l0), 
    .b0_mv_field_mv_y_l0(b0_mv_field_mv_y_l0), 
    .b0_mv_field_mv_x_l1(b0_mv_field_mv_x_l1), 
    .b0_mv_field_mv_y_l1(b0_mv_field_mv_y_l1), 
    
    .b1_mv_field_pred_flag_l0(b1_mv_field_pred_flag_l0), 
    .b1_mv_field_pred_flag_l1(b1_mv_field_pred_flag_l1), 
    .b1_mv_field_mv_x_l0(b1_mv_field_mv_x_l0), 
    .b1_mv_field_mv_y_l0(b1_mv_field_mv_y_l0), 
    .b1_mv_field_mv_x_l1(b1_mv_field_mv_x_l1), 
    .b1_mv_field_mv_y_l1(b1_mv_field_mv_y_l1), 
    
    .b2_mv_field_pred_flag_l0(b2_mv_field_pred_flag_l0), 
    .b2_mv_field_pred_flag_l1(b2_mv_field_pred_flag_l1), 
    .b2_mv_field_mv_x_l0(b2_mv_field_mv_x_l0), 
    .b2_mv_field_mv_y_l0(b2_mv_field_mv_y_l0), 
    .b2_mv_field_mv_x_l1(b2_mv_field_mv_x_l1), 
    .b2_mv_field_mv_y_l1(b2_mv_field_mv_y_l1), 

    .col_mv_field_pred_flag_l0   (available_col_l0_flag),
    .col_mv_field_pred_flag_l1   (available_col_l1_flag),
    .col_mv_field_mv_x_l0        (col_mv_field_mv_x_l0_to_inter ),
    .col_mv_field_mv_y_l0        (col_mv_field_mv_y_l0_to_inter ),
    .col_mv_field_mv_x_l1        (col_mv_field_mv_x_l1_to_inter ),
    .col_mv_field_mv_y_l1        (col_mv_field_mv_y_l1_to_inter ),
    .ref_poc_col_l0(ref_poc_l0),
    .ref_poc_col_l1(ref_poc_l1), 


    .ref_poc_a0_l0(ref_poc_a0_l0), 
    .ref_poc_a0_l1(ref_poc_a0_l1), 
    .ref_poc_a1_l0(ref_poc_a1_l0), 
    .ref_poc_a1_l1(ref_poc_a1_l1), 
    .ref_poc_b0_l0(ref_poc_b0_l0), 
    .ref_poc_b0_l1(ref_poc_b0_l1), 
    .ref_poc_b1_l0(ref_poc_b1_l0), 
    .ref_poc_b1_l1(ref_poc_b1_l1), 
    .ref_poc_b2_l0(ref_poc_b2_l0), 
    .ref_poc_b2_l1(ref_poc_b2_l1), 
    
    .bi_pred_cand_valid(bi_pred_cand_valid),
    .bi_cand_type_out({bi_cand3_type,bi_cand2_type,bi_cand1_type,bi_cand0_type})
    );

tx_division_engine tx_block_l0 (
    .clk(clk), 
    .enable(tx_enable), 
    .divisor(tdl0_clipped), 
    .quotient(tx_l0)
    );

tx_division_engine tx_block_l1 (
    .clk(clk), 
    .enable(tx_enable), 
    .divisor(tdl1_clipped), 
    .quotient(tx_l1)
    );


temporal_pred_engine temporal_pred_block (
    .clk(clk), 
    .reset(reset), 
    .enable(enable), 
    .is_new_pic_col_ref_idx(is_new_pic_col_ref_idx), 
    .current_poc(current_poc), 
    .col_pic(col_poc), 
	.collocated_dpb_idx(collocated_dpb_idx),
    .axi_col_mv_frame_offet_in(axi_col_mv_frame_offet), 
    .current_dpb_idx(current_pic_dpb_idx), 
    
    .col_x_addr(col_xctr_addr), 
    .col_y_addr(col_yctr_addr), 
    .col_addr_valid(col_addr_valid_in), 
    .col_br_boundary_ok(col_br_boundary_ok), 
    
    .collocated_from_l0_flag(collocated_from_l0_flag), 
    .cur_poc_l0_diff(tbl0), 
    .cur_poc_l1_diff(tbl1), 
    .cur_poc_l0_diff_clipped(tbl0_clipped), 
    .cur_poc_l1_diff_clipped(tbl1_clipped), 
    
    .axi_ctu_read_done_out(col_preftch_done), 
    .new_ctu_start_in(new_ctu_start_in), 
    .ctb_xx(ctb_xx[X11_ADDR_WDTH-1:CTB_SIZE_WIDTH]), 
    .ctb_yy(ctb_yy[X11_ADDR_WDTH-1:CTB_SIZE_WIDTH]), 
    
    .slice_type(slice_type), 
    .slice_temporal_mvp_enabled_flag(slice_temporal_mvp_enabled_flag), 
    .col_mv_ready(col_mv_ready), 
    .available_col_l0_flag(available_col_l0_flag), 
    .available_col_l1_flag(available_col_l1_flag), 
    .col_mv_field_mv_x_l0_to_inter(col_mv_field_mv_x_l0_to_inter), 
    .col_mv_field_mv_y_l0_to_inter(col_mv_field_mv_y_l0_to_inter), 
    .col_mv_field_mv_x_l1_to_inter(col_mv_field_mv_x_l1_to_inter), 
    .col_mv_field_mv_y_l1_to_inter(col_mv_field_mv_y_l1_to_inter), 
    
    .from_top_ref_pic_list_poc_data_in(from_top_ref_pic_list_poc_data_in), 
    .from_top_ref_pic_list0_poc_wr_en(from_top_ref_pic_list0_poc_wr_en), 
    .from_top_ref_pic_list1_poc_wr_en(from_top_ref_pic_list1_poc_wr_en), 
    .from_top_ref_pic_list0_poc_addr(from_top_ref_pic_list0_poc_addr), 
    .from_top_ref_pic_list1_poc_addr(from_top_ref_pic_list1_poc_addr), 
    .from_top_ref_pic_list_valid(from_top_ref_pic_list_valid), 
    
    .mv_pref_axi_araddr     (mv_pref_axi_araddr), 
    .mv_pref_axi_arlen      (mv_pref_axi_arlen), 
    .mv_pref_axi_arsize     (mv_pref_axi_arsize), 
    .mv_pref_axi_arburst    (mv_pref_axi_arburst), 
    .mv_pref_axi_arprot     (mv_pref_axi_arprot), 
    .mv_pref_axi_arvalid    (mv_pref_axi_arvalid), 
    .mv_pref_axi_arready    (mv_pref_axi_arready), 
    .mv_pref_axi_rdata      (mv_pref_axi_rdata), 
    .mv_pref_axi_rresp      (mv_pref_axi_rresp), 
    .mv_pref_axi_rlast      (mv_pref_axi_rlast), 
    .mv_pref_axi_rvalid     (mv_pref_axi_rvalid), 
    .mv_pref_axi_rready     (mv_pref_axi_rready), 
    .mv_pref_axi_arlock     (mv_pref_axi_arlock), 
    .mv_pref_axi_arid       (mv_pref_axi_arid), 
    .mv_pref_axi_arcache    (mv_pref_axi_arcache)
    );
    
    
    
always@(*) begin
    mv_derive_next_state = mv_derive_state;
    avaialble_flag_a0_wire = 0;
    ref_pic_list0_poc_wr_en = 0;
    ref_pic_list1_poc_wr_en = 0;
    ref_pic_list_idx_wr_en = 0;
    ref_pic_list0_idx_addr = {NUM_REF_IDX_L0_MINUS1_WIDTH{1'bx}};
    ref_pic_list1_idx_addr = {NUM_REF_IDX_L0_MINUS1_WIDTH{1'bx}};
    ref_pic_list0_poc_addr = {NUM_REF_IDX_L0_MINUS1_WIDTH{1'bx}};
    ref_pic_list0_poc_data_in = {REF_PIC_LIST_POC_DATA_WIDTH{1'bx}};
    ref_pic_list1_poc_addr = {NUM_REF_IDX_L0_MINUS1_WIDTH{1'bx}};
    ref_pic_list1_poc_data_in = {REF_PIC_LIST_POC_DATA_WIDTH{1'bx}};
    ref_pic_list0_idx_data_in = {DPB_FRAME_OFFSET_WIDTH{1'bx}};
    ref_pic_list1_idx_data_in = {DPB_FRAME_OFFSET_WIDTH{1'bx}};
    from_top_ref_pic_list_valid = 0;
	current_mv_valid_now = 0;
    case(mv_derive_state) 
        STATE_MV_DERIVE_CONFIG_UPDATE: begin
                if(config_mode_in ==`INTER_REF_PIC_TRANSFER) begin // reef pic list comes whenever there is a new picture. ref pic list is sent just after sending the poc
                    ref_pic_list0_poc_addr = from_top_ref_pic_list0_poc_addr;
                    ref_pic_list1_poc_addr = from_top_ref_pic_list1_poc_addr;
                    ref_pic_list0_poc_wr_en = from_top_ref_pic_list0_poc_wr_en;
                    ref_pic_list1_poc_wr_en = from_top_ref_pic_list1_poc_wr_en;
                    ref_pic_list0_poc_data_in = from_top_ref_pic_list_poc_data_in;
                    ref_pic_list1_poc_data_in = from_top_ref_pic_list_poc_data_in;

                    ref_pic_list0_idx_addr = from_top_ref_pic_list0_poc_addr;
                    ref_pic_list1_idx_addr = from_top_ref_pic_list1_poc_addr;
                    ref_pic_list0_idx_data_in = from_top_ref_pic_list_idx_data_in;
                    ref_pic_list1_idx_data_in = from_top_ref_pic_list_idx_data_in;
                    ref_pic_list_idx_wr_en = from_top_ref_pic_list_idx_wr_en;
                    from_top_ref_pic_list_valid = 1;
                end
                else if(config_mode_in == `INTER_TOP_SLICE_2 ) begin    // dont expect col_mv_ready to be set along with this for temporal engine state machine to work
`ifdef READ_FILE
                    ref_pic_list0_poc_addr =      config_bus_in[    HEADER_WIDTH + SLICE_CB_QP_OFFSET_WIDTH + SLICE_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH + SLICE_BETA_OFFSET_DIV2_WIDTH + SLICE_TC_OFFSET_DIV2_WIDTH + COLLOCATED_REF_IDX_WIDTH - 2:
                                                                    HEADER_WIDTH + SLICE_CB_QP_OFFSET_WIDTH + SLICE_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH + SLICE_BETA_OFFSET_DIV2_WIDTH + SLICE_TC_OFFSET_DIV2_WIDTH];
                    ref_pic_list1_poc_addr =      config_bus_in[    HEADER_WIDTH + SLICE_CB_QP_OFFSET_WIDTH + SLICE_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH + SLICE_BETA_OFFSET_DIV2_WIDTH + SLICE_TC_OFFSET_DIV2_WIDTH + COLLOCATED_REF_IDX_WIDTH - 2:
                                                                    HEADER_WIDTH + SLICE_CB_QP_OFFSET_WIDTH + SLICE_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH + SLICE_BETA_OFFSET_DIV2_WIDTH + SLICE_TC_OFFSET_DIV2_WIDTH];
                    ref_pic_list0_idx_addr =      config_bus_in[    HEADER_WIDTH + SLICE_CB_QP_OFFSET_WIDTH + SLICE_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH + SLICE_BETA_OFFSET_DIV2_WIDTH + SLICE_TC_OFFSET_DIV2_WIDTH + COLLOCATED_REF_IDX_WIDTH - 2:
                                                                    HEADER_WIDTH + SLICE_CB_QP_OFFSET_WIDTH + SLICE_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH + SLICE_BETA_OFFSET_DIV2_WIDTH + SLICE_TC_OFFSET_DIV2_WIDTH];
                    ref_pic_list1_idx_addr =      config_bus_in[    HEADER_WIDTH + SLICE_CB_QP_OFFSET_WIDTH + SLICE_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH + SLICE_BETA_OFFSET_DIV2_WIDTH + SLICE_TC_OFFSET_DIV2_WIDTH + COLLOCATED_REF_IDX_WIDTH - 2:
                                                                    HEADER_WIDTH + SLICE_CB_QP_OFFSET_WIDTH + SLICE_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH + SLICE_BETA_OFFSET_DIV2_WIDTH + SLICE_TC_OFFSET_DIV2_WIDTH];
`else                                                                    
                    ref_pic_list0_poc_addr = config_bus_in[     INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SLICE_CB_QP_OFFSET_WIDTH - SLICE_CR_QP_OFFSET_WIDTH - DISABLE_DBF_WIDTH - SLICE_BETA_OFFSET_DIV2_WIDTH - SLICE_TC_OFFSET_DIV2_WIDTH - 2:
                                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SLICE_CB_QP_OFFSET_WIDTH - SLICE_CR_QP_OFFSET_WIDTH - DISABLE_DBF_WIDTH - SLICE_BETA_OFFSET_DIV2_WIDTH - SLICE_TC_OFFSET_DIV2_WIDTH - COLLOCATED_REF_IDX_WIDTH];
                    ref_pic_list1_poc_addr = config_bus_in[     INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SLICE_CB_QP_OFFSET_WIDTH - SLICE_CR_QP_OFFSET_WIDTH - DISABLE_DBF_WIDTH - SLICE_BETA_OFFSET_DIV2_WIDTH - SLICE_TC_OFFSET_DIV2_WIDTH - 2:
                                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SLICE_CB_QP_OFFSET_WIDTH - SLICE_CR_QP_OFFSET_WIDTH - DISABLE_DBF_WIDTH - SLICE_BETA_OFFSET_DIV2_WIDTH - SLICE_TC_OFFSET_DIV2_WIDTH - COLLOCATED_REF_IDX_WIDTH];
                    ref_pic_list0_idx_addr = config_bus_in[     INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SLICE_CB_QP_OFFSET_WIDTH - SLICE_CR_QP_OFFSET_WIDTH - DISABLE_DBF_WIDTH - SLICE_BETA_OFFSET_DIV2_WIDTH - SLICE_TC_OFFSET_DIV2_WIDTH - 2:
                                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SLICE_CB_QP_OFFSET_WIDTH - SLICE_CR_QP_OFFSET_WIDTH - DISABLE_DBF_WIDTH - SLICE_BETA_OFFSET_DIV2_WIDTH - SLICE_TC_OFFSET_DIV2_WIDTH - COLLOCATED_REF_IDX_WIDTH];
                    ref_pic_list1_idx_addr = config_bus_in[     INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SLICE_CB_QP_OFFSET_WIDTH - SLICE_CR_QP_OFFSET_WIDTH - DISABLE_DBF_WIDTH - SLICE_BETA_OFFSET_DIV2_WIDTH - SLICE_TC_OFFSET_DIV2_WIDTH - 2:
                                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SLICE_CB_QP_OFFSET_WIDTH - SLICE_CR_QP_OFFSET_WIDTH - DISABLE_DBF_WIDTH - SLICE_BETA_OFFSET_DIV2_WIDTH - SLICE_TC_OFFSET_DIV2_WIDTH - COLLOCATED_REF_IDX_WIDTH];

`endif
                end
                else if(config_mode_in == `INTER_SLICE_TILE_INFO) begin // assume slice tile info to always come after a new poc
					ref_pic_list0_poc_addr = collocated_ref_idx;
					ref_pic_list1_poc_addr = collocated_ref_idx;
                    ref_pic_list0_idx_addr = collocated_ref_idx;
                    ref_pic_list1_idx_addr = collocated_ref_idx;	
                end
				else if(`INTER_END_REF_PIC_TRANSFER) begin
					ref_pic_list0_poc_addr = collocated_ref_idx;
					ref_pic_list1_poc_addr = collocated_ref_idx;
                    ref_pic_list0_idx_addr = collocated_ref_idx;
                    ref_pic_list1_idx_addr = collocated_ref_idx;				
				end
            case(config_mode_in)
                `INTER_CTU0_HEADER: begin
					mv_derive_next_state = STATE_PREFETCH_COL_WRITE_WAIT;
                end
                `INTER_CU_HEADER: begin
`ifdef  READ_FILE
                    if(config_bus_in[     HEADER_WIDTH + 2*X0_WIDTH + LOG2_CTB_WIDTH] == `MODE_INTRA) begin
`else
                    if(config_bus_in[     INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - LOG2_CTB_WIDTH - 1] == `MODE_INTRA) begin
`endif
                        mv_derive_next_state = STATE_MV_DERIVE_SET_DUMMY_INTRA_MV;
                    end
                end
                `INTER_PU_HEADER: begin
                    mv_derive_next_state = STATE_MV_DERIVE_PU_OVERWRIT_3;
                end
                
            endcase
        end
        STATE_PREFETCH_COL_WRITE_WAIT: begin
        // temp comment ********************************
            
            //******************************
                mv_derive_next_state = STATE_MV_DERIVE_CONFIG_UPDATE;
            
        end
        STATE_MV_DERIVE_PU_OVERWRIT_3: begin
			if(col_preftch_done == 1) begin
				mv_derive_next_state = STATE_MV_DERIVE_AVAILABLE_CHECK4;
			end
        end
        STATE_MV_DERIVE_AVAILABLE_CHECK4: begin
            if(merge_flag == 1) begin
                ref_pic_list0_poc_addr = 0;
                ref_pic_list0_idx_addr = 0;
                ref_pic_list1_poc_addr = 0;            
                ref_pic_list1_idx_addr = 0;            
            end
            else begin
                ref_pic_list0_poc_addr = current_mv_field_ref_idx_l0;
                ref_pic_list0_idx_addr = current_mv_field_ref_idx_l0;
                ref_pic_list1_poc_addr = current_mv_field_ref_idx_l1;            
                ref_pic_list1_idx_addr = current_mv_field_ref_idx_l1;            
            end
            if(current_cand_num == 4) begin
                if(merge_flag == 1) begin
                    mv_derive_next_state = STATE_MV_DERIVE_MV_AVAILABLE_MERGE_9;
                end
                else begin
                    mv_derive_next_state = STATE_MV_DERIVE_MV_AVAILABLE_AMVP_9;
                end
            end
        end
        STATE_MV_DERIVE_MV_AVAILABLE_AMVP_9: begin
            mv_derive_next_state = STATE_MV_DERIVE_MV_AVAILABLE_AMVP_10;
        end
        STATE_MV_DERIVE_MV_AVAILABLE_AMVP_10: begin
            mv_derive_next_state = STATE_MV_DERIVE_MV_AVAILABLE_AMVP_11;
            ref_pic_list0_poc_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list0_idx_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list1_poc_addr = cand_mv_field_ref_idx_l1_d;    // find poc of A0
            ref_pic_list1_idx_addr = cand_mv_field_ref_idx_l1_d;    // find poc of A0
        end
        STATE_MV_DERIVE_MV_AVAILABLE_AMVP_11: begin
            mv_derive_next_state = STATE_MV_DERIVE_MV_AVAILABLE_AMVP_12;  
            ref_pic_list0_poc_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list0_idx_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list1_poc_addr = cand_mv_field_ref_idx_l1_d;    // find poc of A1  
            ref_pic_list1_idx_addr = cand_mv_field_ref_idx_l1_d;    // find poc of A1  
        end
        STATE_MV_DERIVE_MV_AVAILABLE_AMVP_12: begin
            mv_derive_next_state = STATE_MV_DERIVE_MV_AVAILABLE_AMVP_13;
            ref_pic_list0_poc_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list0_idx_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list1_poc_addr = cand_mv_field_ref_idx_l1_d;    // find poc of B0            
            ref_pic_list1_idx_addr = cand_mv_field_ref_idx_l1_d;    // find poc of B0            
        end
        STATE_MV_DERIVE_MV_AVAILABLE_AMVP_13: begin
            mv_derive_next_state = STATE_MV_DERIVE_MV_PROCESS_AMVP14;
            ref_pic_list0_poc_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list0_idx_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list1_poc_addr = cand_mv_field_ref_idx_l1_d;    // find poc of B1            
            ref_pic_list1_idx_addr = cand_mv_field_ref_idx_l1_d;    // find poc of B1            
        end
        STATE_MV_DERIVE_MV_PROCESS_AMVP14: begin
            ref_pic_list0_poc_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list0_idx_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list1_poc_addr = cand_mv_field_ref_idx_l1_d;    // find poc of B2        
            ref_pic_list1_idx_addr = cand_mv_field_ref_idx_l1_d;    // find poc of B2        
            if(mvp_both_cand_found) begin
                mv_derive_next_state = STATE_MV_DERIVE_MVD_ADD_AMVP;
            end
            else begin
                mv_derive_next_state = STATE_MV_DERIVE_MV_PROCESS_AMVP15;
            end        
        end
        STATE_MV_DERIVE_MV_PROCESS_AMVP15: begin
            if(mvp_both_cand_found) begin
                mv_derive_next_state = STATE_MV_DERIVE_MVD_ADD_AMVP;
            end
            else begin
                mv_derive_next_state = STATE_MV_DERIVE_MV_PROCESS_AMVP16;
            end         
        end
        STATE_MV_DERIVE_MV_PROCESS_AMVP16: begin
            if(mvp_both_cand_found) begin
                mv_derive_next_state = STATE_MV_DERIVE_MVD_ADD_AMVP;
            end
            else begin
                mv_derive_next_state = STATE_MV_DERIVE_MV_PROCESS_AMVP17;
            end         
        end
        STATE_MV_DERIVE_MV_PROCESS_AMVP17: begin
            if(mvp_both_cand_found) begin
                mv_derive_next_state = STATE_MV_DERIVE_MVD_ADD_AMVP;
            end
            else begin
                mv_derive_next_state = STATE_MV_DERIVE_MV_PROCESS_AMVP18;
            end         
        end
        STATE_MV_DERIVE_MV_PROCESS_AMVP18: begin
            if(mvp_both_cand_found) begin
                mv_derive_next_state = STATE_MV_DERIVE_MVD_ADD_AMVP;
            end
            else begin
                mv_derive_next_state = STATE_MV_DERIVE_MV_PROCESS_AMVP19;
            end         
        end
        STATE_MV_DERIVE_MV_PROCESS_AMVP19: begin
            if(mvp_both_cand_found) begin
                mv_derive_next_state = STATE_MV_DERIVE_MVD_ADD_AMVP;
            end
            else begin
                mv_derive_next_state = STATE_MV_DERIVE_MV_PROCESS_AMVP20;
            end
        end
        STATE_MV_DERIVE_MV_PROCESS_AMVP20: begin
            if(mvp_both_cand_found) begin
                mv_derive_next_state = STATE_MV_DERIVE_MVD_ADD_AMVP;
            end
            else if(cand_num_id == 5) begin     // wait one cycle to transfer mvp_l0_mvs_not_same fiiled by B2 in the last cycle
                mv_derive_next_state = STATE_MV_DERIVE_COMPARE_MVS;
            end
        end
        STATE_MV_DERIVE_COMPARE_MVS: begin
            mv_derive_next_state = STATE_MV_DERIVE_COL_MV_WAIT;
        end
        STATE_MV_DERIVE_MVD_ADD_AMVP: begin
            if((!current_mv_field_pred_flag_l0 | is_mvd_l0_filled) & (!current_mv_field_pred_flag_l1 | is_mvd_l1_filled)) begin
                mv_derive_next_state = STATE_MV_DERIVE_SET_INTER_MV;
            end
			
			// synthesis translate_off
			else begin
				//$display("should not reach this !");
				//$stop;
			end
			// synthesis translate_on
        end
        STATE_MV_DERIVE_MV_AVAILABLE_MERGE_9: begin
            mv_derive_next_state = STATE_MV_DERIVE_MV_AVAILABLE_MERGE_10;
        end
        STATE_MV_DERIVE_MV_AVAILABLE_MERGE_10: begin
            ref_pic_list0_poc_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list0_idx_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list1_poc_addr = cand_mv_field_ref_idx_l1_d;    // find poc of A1        
            ref_pic_list1_idx_addr = cand_mv_field_ref_idx_l1_d;    // find poc of A1        
            // if(merge_cand_found == 1) begin
                // mv_derive_next_state = STATE_MV_DERIVE_SET_INTER_MV;
            // end
            // else begin
                mv_derive_next_state = STATE_MV_DERIVE_MV_AVAILABLE_MERGE_11;
            // end
        end
        STATE_MV_DERIVE_MV_AVAILABLE_MERGE_11: begin  
            ref_pic_list0_poc_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list0_idx_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list1_poc_addr = cand_mv_field_ref_idx_l1_d;    // find poc of B1      
            ref_pic_list1_idx_addr = cand_mv_field_ref_idx_l1_d;    // find poc of B1      
            if(merge_cand_found == 1) begin
                mv_derive_next_state = STATE_MV_DERIVE_SET_INTER_MV;
            end
            else begin
                mv_derive_next_state = STATE_MV_DERIVE_MV_AVAILABLE_MERGE_12;
            end        
        end
        STATE_MV_DERIVE_MV_AVAILABLE_MERGE_12: begin
            ref_pic_list0_poc_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list0_idx_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list1_poc_addr = cand_mv_field_ref_idx_l1_d;    // find poc of B0
            ref_pic_list1_idx_addr = cand_mv_field_ref_idx_l1_d;    // find poc of B0
            if(merge_cand_found == 1) begin
                mv_derive_next_state = STATE_MV_DERIVE_SET_INTER_MV;
            end
            else begin
                mv_derive_next_state = STATE_MV_DERIVE_MV_AVAILABLE_MERGE_13;
            end            
        end
        STATE_MV_DERIVE_MV_AVAILABLE_MERGE_13: begin
            ref_pic_list0_poc_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list0_idx_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list1_poc_addr = cand_mv_field_ref_idx_l1_d;    // find poc of A0
            ref_pic_list1_idx_addr = cand_mv_field_ref_idx_l1_d;    // find poc of A0
            if(merge_cand_found == 1) begin
                mv_derive_next_state = STATE_MV_DERIVE_SET_INTER_MV;
            end
            else begin
                mv_derive_next_state = STATE_MV_DERIVE_MV_PROCESS_MERGE_14;
            end             
        end     
        STATE_MV_DERIVE_MV_PROCESS_MERGE_14: begin
            ref_pic_list0_poc_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list0_idx_addr = cand_mv_field_ref_idx_l0_d;
            ref_pic_list1_poc_addr = cand_mv_field_ref_idx_l1_d;    // find poc of B2
            ref_pic_list1_idx_addr = cand_mv_field_ref_idx_l1_d;    // find poc of B2
            
            if(is_available_a0 == 1) begin
                if(is_available_a1) begin
                    if(compare_a0a1_curr_ref_idx & compare_a0a1_curr_mv) begin
                        avaialble_flag_a0_wire = 0;                               
                    end
                    else begin
                        avaialble_flag_a0_wire = 1;
                    end
                end
                else begin                       
                    avaialble_flag_a0_wire = 1;
                end
            end 
            else begin
                avaialble_flag_a0_wire = 0;
            end
            if(merge_cand_found == 1) begin
                mv_derive_next_state = STATE_MV_DERIVE_SET_INTER_MV;
            end
            else begin
                mv_derive_next_state = STATE_MV_DERIVE_MV_PROCESS_MERGE_15;
            end 
        end
        STATE_MV_DERIVE_MV_PROCESS_MERGE_15: begin
            if(merge_cand_found == 1) begin
                mv_derive_next_state = STATE_MV_DERIVE_SET_INTER_MV;
            end
            else begin
                mv_derive_next_state = STATE_MV_DERIVE_COL_MV_WAIT;
            end         
        end
        STATE_MV_DERIVE_COL_MV_WAIT: begin
            if(merge_flag) begin
                if(merge_cand_found == 1) begin
                    mv_derive_next_state = STATE_MV_DERIVE_SET_INTER_MV;
                end
                else if(col_mv_ready ) begin
                    if(slice_type == `SLICE_B) begin
                        mv_derive_next_state = STATE_MV_DERIVE_BI_PRED_CANDS1;
                    end
                    else begin
                        mv_derive_next_state = STATE_MV_DERIVE_ZERO_MERGE;
                    end
                end
            end
            else begin
                if(mvp_both_cand_found) begin
                    mv_derive_next_state = STATE_MV_DERIVE_MVD_ADD_AMVP;
                end
                else if(col_mv_ready) begin
                    mv_derive_next_state = STATE_MV_DERIVE_ADD_ZEROS_AMVP;
                end                
            end
        end
        // STATE_MV_DERIVE_WAIT_BEFORE_ZERO_AMVP: begin
            // mv_derive_next_state = STATE_MV_DERIVE_ADD_ZEROS_AMVP;
        // end
        STATE_MV_DERIVE_BI_PRED_CANDS1: begin
            if(merge_cand_found == 1) begin
                mv_derive_next_state = STATE_MV_DERIVE_SET_INTER_MV;
            end
            else begin
                mv_derive_next_state = STATE_MV_DERIVE_BI_PRED_CANDS2;
            end
            
        end
        STATE_MV_DERIVE_BI_PRED_CANDS2: begin
			if(merge_cand_found == 1) begin
                mv_derive_next_state = STATE_MV_DERIVE_SET_INTER_MV;// finding temporal candidate here
            end
            else begin
                mv_derive_next_state = STATE_MV_DERIVE_BI_PRED_CANDS3;  
            end
        end
        STATE_MV_DERIVE_BI_PRED_CANDS3: begin
            mv_derive_next_state = STATE_MV_DERIVE_BI_PRED_CANDS4;
        end
        STATE_MV_DERIVE_BI_PRED_CANDS4: begin
            if(merge_cand_found == 1) begin
                mv_derive_next_state = STATE_MV_DERIVE_SET_INTER_MV;
            end
            else if(bi_pred_cand_idx == 11) begin
                mv_derive_next_state = STATE_MV_DERIVE_ZERO_MERGE;
            end
            ref_pic_list0_idx_addr = 0;
            ref_pic_list1_idx_addr = 0;  
        end
        STATE_MV_DERIVE_ZERO_MERGE: begin
            if(merge_cand_found == 1) begin
                mv_derive_next_state = STATE_MV_DERIVE_SET_INTER_MV;
                ref_pic_list0_idx_addr = zero_ref_idx ;
                ref_pic_list1_idx_addr = zero_ref_idx ;  
            end
            else if(zero_ref_idx == inter_maxnummergecand || merge_array_idx == merge_idx) begin
                mv_derive_next_state = STATE_MV_DERIVE_SET_INTER_MV;
                ref_pic_list0_idx_addr = zero_ref_idx;
                ref_pic_list1_idx_addr = zero_ref_idx;  
            end
            else begin
                ref_pic_list0_idx_addr = zero_ref_idx +1;
                ref_pic_list1_idx_addr = zero_ref_idx +1;  
            end
        end
        STATE_MV_DERIVE_ADD_ZEROS_AMVP: begin
            mv_derive_next_state = STATE_MV_DERIVE_MVD_ADD_AMVP;
        end
        STATE_MV_DERIVE_SET_DUMMY_INTRA_MV: begin
            mv_derive_next_state  = STATE_MV_DERIVE_SET_PRE_INTRA_MV;
        end
        STATE_MV_DERIVE_SET_PRE_INTRA_MV: begin
            if(pu_mv_buffer_done & pred_sample_gen_idle_in) begin
                mv_derive_next_state  = STATE_MV_DERIVE_DONE;
            end
        end        
        STATE_MV_DERIVE_SET_INTER_MV: begin
            ref_pic_list0_idx_addr = zero_ref_idx ; // in order to hold ref_poc_list_idx data out for current mv
            ref_pic_list1_idx_addr = zero_ref_idx ;  
            if(pu_mv_buffer_done & pred_sample_gen_idle_in) begin
                mv_derive_next_state  = STATE_MV_DERIVE_DONE;
            end
        end
        STATE_MV_DERIVE_DONE: begin
        // temp comment ********************************
            current_mv_valid_now = 1;
            if(is_pb_x_ctu_last & is_pb_y_ctu_last) begin
                mv_derive_next_state = STATE_MV_DERIVE_CTU_DONE_WAIT;
            end
            else begin
    //  ********************************
                mv_derive_next_state = STATE_MV_DERIVE_WAIT_BEFOR_CONFIG_UPDATE;
    // temp comment ********************************
            end
        // ***********************
            ref_pic_list0_idx_addr = zero_ref_idx ; // in order to hold ref_poc_list_idx data out for current mv
            ref_pic_list1_idx_addr = zero_ref_idx ;  
        end
		STATE_MV_DERIVE_CTU_DONE_WAIT: begin
			if(col_write_done_out) begin
				mv_derive_next_state = STATE_MV_DERIVE_CTU_DONE;
			end
		end
        STATE_MV_DERIVE_CTU_DONE: begin
            if(pref_ctu_col_writ_ctu_done_out ) begin
                mv_derive_next_state = STATE_MV_DERIVE_CONFIG_UPDATE;
            end
        end
        STATE_MV_DERIVE_WAIT_BEFOR_CONFIG_UPDATE: begin
                mv_derive_next_state = STATE_MV_DERIVE_CONFIG_UPDATE;
        end
    endcase
end


always@(posedge clk) begin
    case(bi_pred_cand_idx_chosen_d)
        0: begin
            case(bi_cand0_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l0    <= a0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a0_mv_field_mv_y_l0;
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a0_l0;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l0    <= a1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a1_mv_field_mv_y_l0;
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a1_l0;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l0    <= b0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b0_mv_field_mv_y_l0;
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b0_l0;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l0    <= b1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b1_mv_field_mv_y_l0;   
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b1_l0;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l0    <= b2_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b2_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b2_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b2_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b2_l0;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l0    <= available_col_l0_flag;
                    bi_mv_field_ref_idx_l0      <= 0;
                    bi_mv_field_mv_x_l0         <= col_mv_field_mv_x_l0_to_inter;
                    bi_mv_field_mv_y_l0         <= col_mv_field_mv_y_l0_to_inter;  
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_l0;
                end
            endcase
            case(bi_cand1_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l1    <= a0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a0_mv_field_mv_y_l1;
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a0_l1;
                    
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l1    <= a1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a1_mv_field_mv_y_l1;  
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a1_l1;                    
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l1    <= b0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b0_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b0_l1;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l1    <= b1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b1_l1;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l1    <= b2_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b2_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b2_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b2_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b2_l1;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l1    <= available_col_l1_flag;
                    bi_mv_field_ref_idx_l1      <= 0;
                    bi_mv_field_mv_x_l1         <= col_mv_field_mv_x_l1_to_inter;
                    bi_mv_field_mv_y_l1         <= col_mv_field_mv_y_l1_to_inter;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_l1;
                end
            endcase            
        end
        1: begin
            case(bi_cand1_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l0    <= a0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a0_mv_field_mv_y_l0;
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a0_l0;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l0    <= a1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a1_l0;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l0    <= b0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b0_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b0_l0;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l0    <= b1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b1_l0;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l0    <= b2_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b2_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b2_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b2_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b2_l0;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l0    <= available_col_l0_flag;
                    bi_mv_field_ref_idx_l0      <= 0;
                    bi_mv_field_mv_x_l0         <= col_mv_field_mv_x_l0_to_inter;
                    bi_mv_field_mv_y_l0         <= col_mv_field_mv_y_l0_to_inter;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_l0;
                end
            endcase
            case(bi_cand0_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l1    <= a0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a0_mv_field_mv_y_l1;
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a0_l1;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l1    <= a1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a1_l1;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l1    <= b0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b0_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b0_l1;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l1    <= b1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b1_l1;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l1    <= b2_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b2_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b2_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b2_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b2_l1;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l1    <= available_col_l1_flag;
                    bi_mv_field_ref_idx_l1      <= 0;
                    bi_mv_field_mv_x_l1         <= col_mv_field_mv_x_l1_to_inter;
                    bi_mv_field_mv_y_l1         <= col_mv_field_mv_y_l1_to_inter;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_l1;
                end
            endcase 
        end
        2: begin
            case(bi_cand0_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l0    <= a0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a0_mv_field_mv_y_l0;
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a0_l0;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l0    <= a1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a1_l0;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l0    <= b0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b0_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b0_l0;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l0    <= b1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b1_l0;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l0    <= b2_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b2_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b2_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b2_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b2_l0;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l0    <= available_col_l0_flag;
                    bi_mv_field_ref_idx_l0      <= 0;
                    bi_mv_field_mv_x_l0         <= col_mv_field_mv_x_l0_to_inter;
                    bi_mv_field_mv_y_l0         <= col_mv_field_mv_y_l0_to_inter;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_l0;
                end
            endcase
            case(bi_cand2_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l1    <= a0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a0_mv_field_mv_y_l1;
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a0_l1;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l1    <= a1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a1_l1;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l1    <= b0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b0_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b0_l1;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l1    <= b1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b1_l1;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l1    <= b2_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b2_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b2_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b2_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b2_l1;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l1    <= available_col_l1_flag;
                    bi_mv_field_ref_idx_l1      <= 0;
                    bi_mv_field_mv_x_l1         <= col_mv_field_mv_x_l1_to_inter;
                    bi_mv_field_mv_y_l1         <= col_mv_field_mv_y_l1_to_inter;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_l1;
                end
            endcase
        end
        3: begin
            case(bi_cand2_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l0    <= a0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a0_mv_field_mv_y_l0;
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a0_l0;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l0    <= a1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a1_l0;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l0    <= b0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b0_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b0_l0;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l0    <= b1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b1_l0;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l0    <= b2_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b2_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b2_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b2_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b2_l0;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l0    <= available_col_l0_flag;
                    bi_mv_field_ref_idx_l0      <= 0;
                    bi_mv_field_mv_x_l0         <= col_mv_field_mv_x_l0_to_inter;
                    bi_mv_field_mv_y_l0         <= col_mv_field_mv_y_l0_to_inter;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_l0;
                end
            endcase
            case(bi_cand0_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l1    <= a0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a0_mv_field_mv_y_l1;
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a0_l1;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l1    <= a1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a1_l1;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l1    <= b0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b0_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b0_l1;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l1    <= b1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b1_l1;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l1    <= b2_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b2_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b2_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b2_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b2_l1;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l1    <= available_col_l1_flag;
                    bi_mv_field_ref_idx_l1      <= 0;
                    bi_mv_field_mv_x_l1         <= col_mv_field_mv_x_l1_to_inter;
                    bi_mv_field_mv_y_l1         <= col_mv_field_mv_y_l1_to_inter;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_l1;
                end
            endcase        
        end
        4: begin
            case(bi_cand1_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l0    <= a0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a0_mv_field_mv_y_l0;
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a0_l0;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l0    <= a1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a1_l0;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l0    <= b0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b0_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b0_l0;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l0    <= b1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b1_l0;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l0    <= b2_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b2_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b2_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b2_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b2_l0;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l0    <= available_col_l0_flag;
                    bi_mv_field_ref_idx_l0      <= 0;
                    bi_mv_field_mv_x_l0         <= col_mv_field_mv_x_l0_to_inter;
                    bi_mv_field_mv_y_l0         <= col_mv_field_mv_y_l0_to_inter;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_l0;
                end
            endcase
            case(bi_cand2_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l1    <= a0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a0_mv_field_mv_y_l1;
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a0_l1;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l1    <= a1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a1_l1;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l1    <= b0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b0_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b0_l1;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l1    <= b1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b1_l1;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l1    <= b2_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b2_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b2_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b2_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b2_l1;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l1    <= available_col_l1_flag;
                    bi_mv_field_ref_idx_l1      <= 0;
                    bi_mv_field_mv_x_l1         <= col_mv_field_mv_x_l1_to_inter;
                    bi_mv_field_mv_y_l1         <= col_mv_field_mv_y_l1_to_inter;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_l1;
                end
            endcase        
        end
        5: begin
            case(bi_cand2_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l0    <= a0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a0_mv_field_mv_y_l0;
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a0_l0;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l0    <= a1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a1_l0;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l0    <= b0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b0_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b0_l0;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l0    <= b1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b1_l0;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l0    <= b2_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b2_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b2_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b2_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b2_l0;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l0    <= available_col_l0_flag;
                    bi_mv_field_ref_idx_l0      <= 0;
                    bi_mv_field_mv_x_l0         <= col_mv_field_mv_x_l0_to_inter;
                    bi_mv_field_mv_y_l0         <= col_mv_field_mv_y_l0_to_inter;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_l0;
                end
            endcase
            case(bi_cand1_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l1    <= a0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a0_mv_field_mv_y_l1;
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a0_l1;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l1    <= a1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a1_l1;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l1    <= b0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b0_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b0_l1;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l1    <= b1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b1_l1;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l1    <= b2_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b2_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b2_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b2_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b2_l1;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l1    <= available_col_l1_flag;
                    bi_mv_field_ref_idx_l1      <= 0;
                    bi_mv_field_mv_x_l1         <= col_mv_field_mv_x_l1_to_inter;
                    bi_mv_field_mv_y_l1         <= col_mv_field_mv_y_l1_to_inter;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_l1;
                end
            endcase        
        end
        6: begin
            case(bi_cand0_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l0    <= a0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a0_mv_field_mv_y_l0;
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a0_l0;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l0    <= a1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a1_l0;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l0    <= b0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b0_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b0_l0;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l0    <= b1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b1_l0;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l0    <= b2_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b2_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b2_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b2_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b2_l0;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l0    <= available_col_l0_flag;
                    bi_mv_field_ref_idx_l0      <= 0;
                    bi_mv_field_mv_x_l0         <= col_mv_field_mv_x_l0_to_inter;
                    bi_mv_field_mv_y_l0         <= col_mv_field_mv_y_l0_to_inter;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_l0;
                end
            endcase
            case(bi_cand3_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l1    <= a0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a0_mv_field_mv_y_l1;
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a0_l1;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l1    <= a1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a1_l1;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l1    <= b0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b0_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b0_l1;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l1    <= b1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b1_l1;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l1    <= b2_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b2_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b2_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b2_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b2_l1;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l1    <= available_col_l1_flag;
                    bi_mv_field_ref_idx_l1      <= 0;
                    bi_mv_field_mv_x_l1         <= col_mv_field_mv_x_l1_to_inter;
                    bi_mv_field_mv_y_l1         <= col_mv_field_mv_y_l1_to_inter;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_l1;
                end
            endcase        
        end
        7: begin
            case(bi_cand3_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l0    <= a0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a0_mv_field_mv_y_l0;
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a0_l0;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l0    <= a1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a1_l0;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l0    <= b0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b0_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b0_l0;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l0    <= b1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b1_l0;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l0    <= b2_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b2_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b2_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b2_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b2_l0;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l0    <= available_col_l0_flag;
                    bi_mv_field_ref_idx_l0      <= 0;
                    bi_mv_field_mv_x_l0         <= col_mv_field_mv_x_l0_to_inter;
                    bi_mv_field_mv_y_l0         <= col_mv_field_mv_y_l0_to_inter;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_l0;
                end
            endcase
            case(bi_cand0_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l1    <= a0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a0_mv_field_mv_y_l1;
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a0_l1;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l1    <= a1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a1_l1;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l1    <= b0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b0_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b0_l1;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l1    <= b1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b1_l1;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l1    <= b2_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b2_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b2_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b2_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b2_l1;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l1    <= available_col_l1_flag;
                    bi_mv_field_ref_idx_l1      <= 0;
                    bi_mv_field_mv_x_l1         <= col_mv_field_mv_x_l1_to_inter;
                    bi_mv_field_mv_y_l1         <= col_mv_field_mv_y_l1_to_inter;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_l1;
                end
            endcase        
        end
        8: begin
            case(bi_cand1_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l0    <= a0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a0_mv_field_mv_y_l0;
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a0_l0;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l0    <= a1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a1_l0;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l0    <= b0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b0_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b0_l0;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l0    <= b1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b1_l0;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l0    <= b2_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b2_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b2_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b2_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b2_l0;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l0    <= available_col_l0_flag;
                    bi_mv_field_ref_idx_l0      <= 0;
                    bi_mv_field_mv_x_l0         <= col_mv_field_mv_x_l0_to_inter;
                    bi_mv_field_mv_y_l0         <= col_mv_field_mv_y_l0_to_inter;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_l0;
                end
            endcase
            case(bi_cand3_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l1    <= a0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a0_mv_field_mv_y_l1;
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a0_l1;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l1    <= a1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a1_l1;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l1    <= b0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b0_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b0_l1;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l1    <= b1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b1_l1;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l1    <= b2_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b2_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b2_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b2_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b2_l1;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l1    <= available_col_l1_flag;
                    bi_mv_field_ref_idx_l1      <= 0;
                    bi_mv_field_mv_x_l1         <= col_mv_field_mv_x_l1_to_inter;
                    bi_mv_field_mv_y_l1         <= col_mv_field_mv_y_l1_to_inter;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_l1;
                end
            endcase        
        end
        9: begin
            case(bi_cand3_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l0    <= a0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a0_mv_field_mv_y_l0;
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a0_l0;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l0    <= a1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a1_l0;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l0    <= b0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b0_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b0_l0;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l0    <= b1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b1_l0;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l0    <= b2_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b2_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b2_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b2_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b2_l0;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l0    <= available_col_l0_flag;
                    bi_mv_field_ref_idx_l0      <= 0;
                    bi_mv_field_mv_x_l0         <= col_mv_field_mv_x_l0_to_inter;
                    bi_mv_field_mv_y_l0         <= col_mv_field_mv_y_l0_to_inter;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_l0;
                end
            endcase
            case(bi_cand1_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l1    <= a0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a0_mv_field_mv_y_l1;
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a0_l1;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l1    <= a1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a1_l1;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l1    <= b0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b0_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b0_l1;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l1    <= b1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b1_l1;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l1    <= b2_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b2_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b2_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b2_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b2_l1;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l1    <= available_col_l1_flag;
                    bi_mv_field_ref_idx_l1      <= 0;
                    bi_mv_field_mv_x_l1         <= col_mv_field_mv_x_l1_to_inter;
                    bi_mv_field_mv_y_l1         <= col_mv_field_mv_y_l1_to_inter;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_l1;
                end
            endcase        
        end
        10: begin
            case(bi_cand2_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l0    <= a0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a0_mv_field_mv_y_l0;
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a0_l0;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l0    <= a1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a1_l0;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l0    <= b0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b0_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b0_l0;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l0    <= b1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b1_l0;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l0    <= b2_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b2_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b2_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b2_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b2_l0;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l0    <= available_col_l0_flag;
                    bi_mv_field_ref_idx_l0      <= 0;
                    bi_mv_field_mv_x_l0         <= col_mv_field_mv_x_l0_to_inter;
                    bi_mv_field_mv_y_l0         <= col_mv_field_mv_y_l0_to_inter;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_l0;
                end
            endcase
            case(bi_cand3_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l1    <= a0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a0_mv_field_mv_y_l1;
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a0_l1;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l1    <= a1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a1_l1;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l1    <= b0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b0_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b0_l1;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l1    <= b1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b1_l1;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l1    <= b2_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b2_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b2_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b2_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b2_l1;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l1    <= available_col_l1_flag;
                    bi_mv_field_ref_idx_l1      <= 0;
                    bi_mv_field_mv_x_l1         <= col_mv_field_mv_x_l1_to_inter;
                    bi_mv_field_mv_y_l1         <= col_mv_field_mv_y_l1_to_inter;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_l1;
                end
            endcase        
        end
        11: begin
            case(bi_cand3_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l0    <= a0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a0_mv_field_mv_y_l0;
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a0_l0;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l0    <= a1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= a1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= a1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= a1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_a1_l0;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l0    <= b0_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b0_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b0_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b0_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b0_l0;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l0    <= b1_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b1_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b1_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b1_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b1_l0;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l0    <= b2_mv_field_pred_flag_l0;
                    bi_mv_field_ref_idx_l0      <= b2_mv_field_ref_idx_l0;
                    bi_mv_field_mv_x_l0         <= b2_mv_field_mv_x_l0;
                    bi_mv_field_mv_y_l0         <= b2_mv_field_mv_y_l0;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_b2_l0;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l0    <= available_col_l0_flag;
                    bi_mv_field_ref_idx_l0      <= 0;
                    bi_mv_field_mv_x_l0         <= col_mv_field_mv_x_l0_to_inter;
                    bi_mv_field_mv_y_l0         <= col_mv_field_mv_y_l0_to_inter;                
                    ref_dpb_idx_bi_l0           <= ref_dpb_idx_l0;
                end
            endcase
            case(bi_cand2_type)
                `MV_MERGE_CAND_A0: begin
                    bi_mv_field_pred_flag_l1    <= a0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a0_mv_field_mv_y_l1;
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a0_l1;
                end
                `MV_MERGE_CAND_A1: begin
                    bi_mv_field_pred_flag_l1    <= a1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= a1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= a1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= a1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_a1_l1;
                end
                `MV_MERGE_CAND_B0: begin
                    bi_mv_field_pred_flag_l1    <= b0_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b0_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b0_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b0_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b0_l1;
                end
                `MV_MERGE_CAND_B1: begin
                    bi_mv_field_pred_flag_l1    <= b1_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b1_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b1_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b1_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b1_l1;
                end
                `MV_MERGE_CAND_B2: begin
                    bi_mv_field_pred_flag_l1    <= b2_mv_field_pred_flag_l1;
                    bi_mv_field_ref_idx_l1      <= b2_mv_field_ref_idx_l1;
                    bi_mv_field_mv_x_l1         <= b2_mv_field_mv_x_l1;
                    bi_mv_field_mv_y_l1         <= b2_mv_field_mv_y_l1;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_b2_l1;
                end
                `MV_MERGE_CAND_COL: begin
                    bi_mv_field_pred_flag_l1    <= available_col_l1_flag;
                    bi_mv_field_ref_idx_l1      <= 0;
                    bi_mv_field_mv_x_l1         <= col_mv_field_mv_x_l1_to_inter;
                    bi_mv_field_mv_y_l1         <= col_mv_field_mv_y_l1_to_inter;                
                    ref_dpb_idx_bi_l1           <= ref_dpb_idx_l1;
                end
            endcase        
        end
    endcase
end


    always@* begin
        for ( i= 0; i<MAX_NUM_MERGE_CAND_CONST; i=i+1) begin
              for ( j= 0; j<MERGE_CAND_TYPE_WIDTH; j=j+1) begin
                    merge_cand_list_in[i*MERGE_CAND_TYPE_WIDTH + j] = merge_cand_list[i][j];
              end
        end
    end
	
	always@(*) begin
		is_pb_ctu_last = 0;
		case(mv_derive_state) 
			STATE_MV_DERIVE_SET_PRE_INTRA_MV: begin
				
			end
			STATE_MV_DERIVE_SET_INTER_MV: begin
			
			end
			STATE_MV_DERIVE_CTU_DONE_WAIT: begin
				if(col_write_done_out) begin
					is_pb_ctu_last = is_pb_x_ctu_last & is_pb_y_ctu_last ;
				end
			end
		endcase
	end
 
    always@(posedge clk) begin: temporal_inital 
        if(reset) begin
            col_poc <= {POC_WIDTH{1'b1}};
            axi_col_mv_frame_offet <= 0;
			collocated_dpb_idx <= 0;
            // collocated_dpb_idx_d <= 0;
        end
        else begin
            if(config_mode_in_d == `INTER_END_REF_PIC_TRANSFER || config_mode_in_d == `INTER_SLICE_TILE_INFO || config_mode_in_d == `INTER_TOP_SLICE_2) begin  // col index can change after a slice half way in the picture
                if(slice_type == `SLICE_B && collocated_from_l0_flag == 0) begin
                    axi_col_mv_frame_offet <= `COL_MV_ADDR_OFFSET + ((`COL_MV_FRAME_OFFSET_VAL*ref_pic_list1_idx_data_out)<<`COL_MV_FRAME_OFFSET_SHIFT);//%(1<<31));;
					collocated_dpb_idx <= ref_pic_list1_idx_data_out;
                end
                else if((slice_type == `SLICE_B && collocated_from_l0_flag == 1) || slice_type == `SLICE_P) begin
                    axi_col_mv_frame_offet <= `COL_MV_ADDR_OFFSET + ((`COL_MV_FRAME_OFFSET_VAL*ref_pic_list0_idx_data_out)<<`COL_MV_FRAME_OFFSET_SHIFT);//%(1<<31));;
					collocated_dpb_idx <= ref_pic_list0_idx_data_out;
                end
                if(slice_type == `SLICE_B && collocated_from_l0_flag == 0) begin
                    col_poc <= ref_pic_list1_poc_data_out;
                end
                else if((slice_type == `SLICE_B && collocated_from_l0_flag == 1) || slice_type == `SLICE_P) begin
                    col_poc <= ref_pic_list0_poc_data_out;
                end
            end
            if(col_poc != col_poc_d) begin      // no need to check if collocated_dpb_idx index has changed col_poc check should suffice
                is_new_pic_col_ref_idx <= 1;
            end
            else begin
                is_new_pic_col_ref_idx <= 0;
            end
        end
    end
    
    always@(posedge clk) begin
        if(cb_pred_mode == `MODE_INTRA) begin
                current_mv_field_pred_flag_l0    <= 0                               ;
                current_mv_field_pred_flag_l1    <= 0                               ;
                current_mv_field_ref_idx_l0      <= {(REF_IDX_LX_WIDTH){1'bx}}   ;
                current_mv_field_ref_idx_l1      <= {(REF_IDX_LX_WIDTH){1'bx}}   ;
                current_mv_field_mv_x_l0         <= {(MVD_WIDTH){1'bx}}          ;
                current_mv_field_mv_y_l0         <= {(MVD_WIDTH){1'bx}}          ;
                current_mv_field_mv_x_l1         <= {(MVD_WIDTH){1'bx}}          ;
                current_mv_field_mv_y_l1         <= {(MVD_WIDTH){1'bx}}          ;  
                current_dpb_idx_l0               <= {(DPB_ADDR_WIDTH){1'bx}};
                current_dpb_idx_l1               <= {(DPB_ADDR_WIDTH){1'bx}};
        end
        else begin
            if(merge_flag) begin
                case(merge_cand_list[merge_idx])
                    `MV_MERGE_CAND_A0: begin
                        current_mv_field_pred_flag_l0    <= a0_mv_field_pred_flag_l0        ;
                        if(is_smallest_pu & a0_mv_field_pred_flag_l0 & a0_mv_field_pred_flag_l1) begin
                            current_mv_field_pred_flag_l1 <= 0;
                        end 
                        else begin
                            current_mv_field_pred_flag_l1    <= a0_mv_field_pred_flag_l1        ;
                        end
                        current_mv_field_ref_idx_l0      <= a0_mv_field_ref_idx_l0          ;
                        current_mv_field_ref_idx_l1      <= a0_mv_field_ref_idx_l1          ;
                        current_mv_field_mv_x_l0         <= a0_mv_field_mv_x_l0             ;
                        current_mv_field_mv_y_l0         <= a0_mv_field_mv_y_l0             ;
                        current_mv_field_mv_x_l1         <= a0_mv_field_mv_x_l1             ;
                        current_mv_field_mv_y_l1         <= a0_mv_field_mv_y_l1             ;    
                        current_dpb_idx_l0               <= ref_dpb_idx_a0_l0               ;           
                        current_dpb_idx_l1               <= ref_dpb_idx_a0_l1               ;           
                    end
                    `MV_MERGE_CAND_A1: begin
                        current_mv_field_pred_flag_l0    <=a1_mv_field_pred_flag_l0        ;
                        if(is_smallest_pu & a1_mv_field_pred_flag_l0 & a1_mv_field_pred_flag_l1) begin
                            current_mv_field_pred_flag_l1 <= 0;
                        end 
                        else begin
                            current_mv_field_pred_flag_l1    <= a1_mv_field_pred_flag_l1        ;
                        end
                        current_mv_field_ref_idx_l0      <=a1_mv_field_ref_idx_l0          ;
                        current_mv_field_ref_idx_l1      <=a1_mv_field_ref_idx_l1          ;
                        current_mv_field_mv_x_l0         <=a1_mv_field_mv_x_l0             ;
                        current_mv_field_mv_y_l0         <=a1_mv_field_mv_y_l0             ;
                        current_mv_field_mv_x_l1         <=a1_mv_field_mv_x_l1             ;
                        current_mv_field_mv_y_l1         <=a1_mv_field_mv_y_l1             ; 
                        current_dpb_idx_l0               <= ref_dpb_idx_a1_l0               ;           
                        current_dpb_idx_l1               <= ref_dpb_idx_a1_l1               ;
                    end
                    `MV_MERGE_CAND_B0: begin
                        current_mv_field_pred_flag_l0    <= b0_mv_field_pred_flag_l0        ;
                        if(is_smallest_pu & b0_mv_field_pred_flag_l0 & b0_mv_field_pred_flag_l1) begin
                            current_mv_field_pred_flag_l1 <= 0;
                        end 
                        else begin
                            current_mv_field_pred_flag_l1    <= b0_mv_field_pred_flag_l1        ;
                        end
                        current_mv_field_ref_idx_l0      <= b0_mv_field_ref_idx_l0          ;
                        current_mv_field_ref_idx_l1      <= b0_mv_field_ref_idx_l1          ;
                        current_mv_field_mv_x_l0         <= b0_mv_field_mv_x_l0             ;
                        current_mv_field_mv_y_l0         <= b0_mv_field_mv_y_l0             ;
                        current_mv_field_mv_x_l1         <= b0_mv_field_mv_x_l1             ;
                        current_mv_field_mv_y_l1         <= b0_mv_field_mv_y_l1             ;  
                        current_dpb_idx_l0               <= ref_dpb_idx_b0_l0               ;           
                        current_dpb_idx_l1               <= ref_dpb_idx_b0_l1               ;
                    end
                    `MV_MERGE_CAND_B1: begin
                        current_mv_field_pred_flag_l0    <= b1_mv_field_pred_flag_l0        ;
                        if(is_smallest_pu & b1_mv_field_pred_flag_l0 & b1_mv_field_pred_flag_l1) begin
                            current_mv_field_pred_flag_l1 <= 0;
                        end 
                        else begin
                            current_mv_field_pred_flag_l1    <= b1_mv_field_pred_flag_l1        ;
                        end
                        current_mv_field_ref_idx_l0      <= b1_mv_field_ref_idx_l0          ;
                        current_mv_field_ref_idx_l1      <= b1_mv_field_ref_idx_l1          ;
                        current_mv_field_mv_x_l0         <= b1_mv_field_mv_x_l0             ;
                        current_mv_field_mv_y_l0         <= b1_mv_field_mv_y_l0             ;
                        current_mv_field_mv_x_l1         <= b1_mv_field_mv_x_l1             ;
                        current_mv_field_mv_y_l1         <= b1_mv_field_mv_y_l1             ;   
                        current_dpb_idx_l0               <= ref_dpb_idx_b1_l0               ;           
                        current_dpb_idx_l1               <= ref_dpb_idx_b1_l1               ;
                    end
                    `MV_MERGE_CAND_B2: begin
                        current_mv_field_pred_flag_l0    <= b2_mv_field_pred_flag_l0        ;
                        if(is_smallest_pu & b2_mv_field_pred_flag_l0 & b2_mv_field_pred_flag_l1) begin
                            current_mv_field_pred_flag_l1 <= 0;
                        end 
                        else begin
                            current_mv_field_pred_flag_l1    <= b2_mv_field_pred_flag_l1        ;
                        end
                        current_mv_field_ref_idx_l0      <= b2_mv_field_ref_idx_l0          ;
                        current_mv_field_ref_idx_l1      <= b2_mv_field_ref_idx_l1          ;
                        current_mv_field_mv_x_l0         <= b2_mv_field_mv_x_l0             ;
                        current_mv_field_mv_y_l0         <= b2_mv_field_mv_y_l0             ;
                        current_mv_field_mv_x_l1         <= b2_mv_field_mv_x_l1             ;
                        current_mv_field_mv_y_l1         <= b2_mv_field_mv_y_l1             ; 
                        current_dpb_idx_l0               <= ref_dpb_idx_b2_l0               ;           
                        current_dpb_idx_l1               <= ref_dpb_idx_b2_l1               ;
                    end
                    `MV_MERGE_CAND_COL: begin
                        current_mv_field_pred_flag_l0    <= available_col_l0_flag        ;
                        if(is_smallest_pu & available_col_l0_flag & available_col_l1_flag) begin
                            current_mv_field_pred_flag_l1 <= 0;
                        end 
                        else begin
                            current_mv_field_pred_flag_l1    <= available_col_l1_flag        ;
                        end
                        current_mv_field_ref_idx_l0      <= 0          ;
                        current_mv_field_ref_idx_l1      <= 0          ;
                        current_mv_field_mv_x_l0         <= col_mv_field_mv_x_l0_to_inter             ;
                        current_mv_field_mv_y_l0         <= col_mv_field_mv_y_l0_to_inter             ;
                        current_mv_field_mv_x_l1         <= col_mv_field_mv_x_l1_to_inter             ;
                        current_mv_field_mv_y_l1         <= col_mv_field_mv_y_l1_to_inter             ;
                        current_dpb_idx_l0               <= ref_dpb_idx_l0               ;           
                        current_dpb_idx_l1               <= ref_dpb_idx_l1               ;
                    end
                    `MV_MERGE_CAND_BI: begin
                        if(merge_cand_found_d) begin
                            current_mv_field_pred_flag_l0 <= bi_mv_field_pred_flag_l0;
                            if(is_smallest_pu & bi_mv_field_pred_flag_l1 & bi_mv_field_pred_flag_l0) begin
                                current_mv_field_pred_flag_l1 <= 0;
                            end 
                            else begin
                                current_mv_field_pred_flag_l1    <= bi_mv_field_pred_flag_l1        ;
                            end
                            current_mv_field_pred_flag_l1       <= bi_mv_field_pred_flag_l1;
                            current_mv_field_ref_idx_l0         <= bi_mv_field_ref_idx_l0 ;
                            current_mv_field_ref_idx_l1         <= bi_mv_field_ref_idx_l1 ;
                            current_mv_field_mv_x_l0            <= bi_mv_field_mv_x_l0    ;
                            current_mv_field_mv_y_l0            <= bi_mv_field_mv_y_l0    ;
                            current_mv_field_mv_x_l1            <= bi_mv_field_mv_x_l1    ;
                            current_mv_field_mv_y_l1            <= bi_mv_field_mv_y_l1    ;
                            current_dpb_idx_l0                  <= ref_dpb_idx_bi_l0               ;           
                            current_dpb_idx_l1                  <= ref_dpb_idx_bi_l1               ;
                        end
                    end
                    `MV_MERGE_CAND_ZERO: begin
                        if(merge_cand_found) begin
                            current_mv_field_pred_flag_l0    <= (slice_type == `SLICE_B || slice_type == `SLICE_P)      ;
                            current_mv_field_pred_flag_l1    <= (slice_type == `SLICE_B) && (!is_smallest_pu)       ;  // for current pred flag l1 to be high both slice type == B and not smallest pu should be set
                            current_mv_field_ref_idx_l0      <= (zero_ref_idx >= num_ref_idx) ? 0: zero_ref_idx;
                            current_mv_field_ref_idx_l1      <= (zero_ref_idx >= num_ref_idx) ? 0: zero_ref_idx;
                            current_mv_field_mv_x_l0         <= 0          ;
                            current_mv_field_mv_y_l0         <= 0          ;
                            current_mv_field_mv_x_l1         <= 0          ;
                            current_mv_field_mv_y_l1         <= 0          ;  
                            current_dpb_idx_l0               <= (zero_ref_idx >= num_ref_idx) ? ref_dpb_idx_l0:  ref_pic_list0_idx_data_out               ;           
                            current_dpb_idx_l1               <= (zero_ref_idx >= num_ref_idx) ? ref_dpb_idx_l1:  ref_pic_list1_idx_data_out               ;                        
                        end
                    end
                endcase
            end
            else begin  // AMVP mode
                if(pred_idc != `PRED_L1) begin
                    current_mv_field_pred_flag_l0 <= 1;
                    if(num_ref_idx_l0_active > 0 ) begin
                        current_mv_field_ref_idx_l0 <= ref_idx_l0;
                    end
                    else begin
                        current_mv_field_ref_idx_l0 <= 0;
                    end
                end
                else begin
                    current_mv_field_pred_flag_l0 <= 0;
                    current_mv_field_ref_idx_l0 <= 0;
                end
                if(pred_idc != `PRED_L0) begin
                    current_mv_field_pred_flag_l1 <= 1;
                    if(num_ref_idx_l1_active > 0 ) begin
                        current_mv_field_ref_idx_l1 <= ref_idx_l1;
                    end
                    else begin
                        current_mv_field_ref_idx_l1 <= 0;
                    end                        
                end
                else begin
                    current_mv_field_pred_flag_l1 <= 0;
                    current_mv_field_ref_idx_l1 <= 0;
                end
                current_dpb_idx_l0               <= ref_dpb_idx_l0               ;           
                current_dpb_idx_l1               <= ref_dpb_idx_l1               ;
                if(current_mv_field_pred_flag_l0) begin
                    case(mvpl0_cand_list[mvp_l0_flag])
                        `MV_AMVP_CAND_A: begin
                            current_mv_field_mv_x_l0 <= (mv_l0_a_x + mvd_l0_x) %(1<<MVD_WIDTH); //to do possibilty of reducing 16 bit adders
                            current_mv_field_mv_y_l0 <= (mv_l0_a_y + mvd_l0_y) %(1<<MVD_WIDTH);
                        end
                        `MV_AMVP_CAND_B: begin
                            current_mv_field_mv_x_l0 <= (mv_l0_b_x + mvd_l0_x) %(1<<MVD_WIDTH);
                            current_mv_field_mv_y_l0 <= (mv_l0_b_y + mvd_l0_y) %(1<<MVD_WIDTH);                           
                        end
                        `MV_AMVP_CAND_COL: begin
                            current_mv_field_mv_x_l0 <= (col_mv_field_mv_x_l0_to_inter + mvd_l0_x) %(1<<MVD_WIDTH);
                            current_mv_field_mv_y_l0 <= (col_mv_field_mv_y_l0_to_inter + mvd_l0_y) %(1<<MVD_WIDTH);   
                        end
                        `MV_AMVP_CAND_ZERO: begin
                            current_mv_field_mv_x_l0 <= ({(MVD_WIDTH){1'b0}} + mvd_l0_x)%(1<<MVD_WIDTH);
                            current_mv_field_mv_y_l0 <= ({(MVD_WIDTH){1'b0}} + mvd_l0_y)%(1<<MVD_WIDTH);                   
                        end
                    endcase
                end
                else begin
                    current_mv_field_mv_x_l0         <= 0          ;
                    current_mv_field_mv_y_l0         <= 0          ;                
                end

                if(current_mv_field_pred_flag_l1) begin
                    case(mvpl1_cand_list[mvp_l1_flag])
                        `MV_AMVP_CAND_A: begin
                            current_mv_field_mv_x_l1 <= (mv_l1_a_x + mvd_l1_x) %(1<<MVD_WIDTH); 
                            current_mv_field_mv_y_l1 <= (mv_l1_a_y + mvd_l1_y) %(1<<MVD_WIDTH);
                        end                                              
                        `MV_AMVP_CAND_B: begin                           
                            current_mv_field_mv_x_l1 <= (mv_l1_b_x + mvd_l1_x) %(1<<MVD_WIDTH);
                            current_mv_field_mv_y_l1 <= (mv_l1_b_y + mvd_l1_y) %(1<<MVD_WIDTH); 
                        end
                        `MV_AMVP_CAND_COL: begin
                            current_mv_field_mv_x_l1 <= (col_mv_field_mv_x_l1_to_inter + mvd_l1_x) %(1<<MVD_WIDTH);
                            current_mv_field_mv_y_l1 <= (col_mv_field_mv_y_l1_to_inter + mvd_l1_y) %(1<<MVD_WIDTH);   
                        end                        
                        `MV_AMVP_CAND_ZERO: begin
                            current_mv_field_mv_x_l1 <= ({(MVD_WIDTH){1'b0}} + mvd_l1_x)%(1<<MVD_WIDTH);
                            current_mv_field_mv_y_l1 <= ({(MVD_WIDTH){1'b0}} + mvd_l1_y)%(1<<MVD_WIDTH);                
                        end
                    endcase
                end
                else begin
                    current_mv_field_mv_x_l1         <= 0          ;
                    current_mv_field_mv_y_l1         <= 0          ;                
                end   
            end
        end
    
    end

    always@(posedge clk) begin
        if(slice_type == `SLICE_B) begin
            if(num_ref_idx_l0_active > num_ref_idx_l1_active) begin
                num_ref_idx <= num_ref_idx_l1_active;
            end
            else begin
                num_ref_idx <= num_ref_idx_l0_active;
            end       
        end
        else begin
            num_ref_idx <= num_ref_idx_l0_active;
        end

    end
    
    always@(posedge clk) begin
        bi_pred_cand_idx_chosen_d <= bi_pred_cand_idx_chosen;
        bi_pred_cand_idx_chosen_2d <= bi_pred_cand_idx_chosen_d;
        cb_xx_end <= (cb_xx + cb_size)%(1<<X11_ADDR_WDTH);
        cb_yy_end <= (cb_yy + cb_size)%(1<<X11_ADDR_WDTH);
        cb_xx_plus_half_cb <= (cb_xx + (cb_size>>1))%(1<<X11_ADDR_WDTH);
        cb_yy_plus_half_cb <= (cb_yy + (cb_size>>1))%(1<<X11_ADDR_WDTH);
        
        
        merge_cand_found_d <= merge_cand_found;
        ctb_size <= (1'b1<<log2_ctb_size);//%(1<<CTB_SIZE_WIDTH+1);
        ctb_xx_end <= (ctb_xx + ctb_size)%(1<<X11_ADDR_WDTH);
        ctb_yy_end <= (ctb_yy + ctb_size)%(1<<X11_ADDR_WDTH);
        is_merge_array_idx_now <= (merge_array_idx == merge_idx) ? 1'b1 : 1'b0;
        
        if(mvp_l0_flag) begin
            if(mvp_l0_idx == 1) begin
                is_mvp_l0_idx_now <= 1;
            end
            else begin
                is_mvp_l0_idx_now <= 0;
            end
        end 
        else begin
            if(mvp_l0_idx == 0) begin
                is_mvp_l0_idx_now <= 1;
            end
            else begin
                is_mvp_l0_idx_now <= 0;
            end
        end
        if(mvp_l1_flag) begin
            if(mvp_l1_idx == 1) begin
                is_mvp_l1_idx_now <= 1;
            end
            else begin
                is_mvp_l1_idx_now <= 0;
            end
        end 
        else begin
            if(mvp_l1_idx == 0) begin
                is_mvp_l1_idx_now <= 1;
            end
            else begin
                is_mvp_l1_idx_now <= 0;
            end
        end

        is_mvp_l0_idx_now_d <= is_mvp_l0_idx_now;
        is_mvp_l1_idx_now_d <= is_mvp_l1_idx_now;
        is_merge_array_idx_now_d <= is_merge_array_idx_now;
        is_available_cand_1d <= is_available_cand;
        is_available_cand_2d <= is_available_cand_1d;
        is_available_cand_3d <= is_available_cand_2d;
        is_available_cand_4d <= is_available_cand_3d;
        is_available_cand_5d <= is_available_cand_4d;
        is_available_cand_6d <= is_available_cand_5d;
        is_available_cand_7d <= is_available_cand_6d;
        is_available_cand_8d <= is_available_cand_7d;
        
        config_mode_in_d <= config_mode_in;
        col_poc_d <= col_poc;
        cand_mv_field_ref_idx_l0_d <= cand_mv_field_ref_idx_l0;
        cand_mv_field_ref_idx_l1_d <= cand_mv_field_ref_idx_l1;
        avaialble_flag_4 <= avaialble_flag_a0_wire & ((avaialble_flag_a1 & avaialble_flag_b0) & avaialble_flag_b1);     // use of paranthesis to reduce the aggregate delay comming from avaialble_flag_a0_wire
    end
    
    always@(*) begin
        if(is_merge_array_idx_now == 0 && is_merge_array_idx_now_d ==1) begin   // when is_merge_array_idx_now changes from 1 to 0, it means you have hit the right candidate!
            merge_cand_found = 1;
        end
        else begin
            merge_cand_found = 0;
        end
        if(is_mvp_l0_idx_now == 0 && is_mvp_l0_idx_now_d == 1) begin
            mvp_l0_cand_found = 1;
        end
        else begin
            mvp_l0_cand_found = 0;
        end
        if(is_mvp_l1_idx_now == 0 && is_mvp_l1_idx_now_d == 1) begin
            mvp_l1_cand_found = 1;
        end
        else begin
            mvp_l1_cand_found = 0;
        end
        mvp_both_cand_found = (mvp_l0_cand_found | !current_mv_field_pred_flag_l0) & (mvp_l1_cand_found | !current_mv_field_pred_flag_l1);
    end
       
    
    always@(posedge clk) begin
        if(xx_pb_end == ctb_xx_end || xx_pb_end == pic_width) begin
            is_pb_x_ctu_last <= 1;
        end
        else begin
            is_pb_x_ctu_last <= 0;
        end
        if (yy_pb_end == ctb_yy_end || yy_pb_end == pic_height) begin
            is_pb_y_ctu_last <= 1;
        end
        else begin
            is_pb_y_ctu_last <= 0;
        end
    end

`ifdef READ_FILE

    assign log2_cb_size_wire = config_bus_in[   HEADER_WIDTH + 2*X0_WIDTH + LOG2_CTB_WIDTH - 1:
                                                HEADER_WIDTH + 2*X0_WIDTH];
`else
    assign log2_cb_size_wire = config_bus_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - 1: 
                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - LOG2_CTB_WIDTH];
`endif
    always@(posedge clk) begin
        
        if(!(mv_l0_b_x_same_mv_l0_a_x  & mv_l0_b_y_same_mv_l0_a_y)) begin
            mvp_l0_mvs_not_same <= 1;
        end
        else begin
            mvp_l0_mvs_not_same <= 0;
        end
        if(!(mv_l1_b_x_same_mv_l1_a_x  && mv_l1_b_y_same_mv_l0_a_y)) begin
            mvp_l1_mvs_not_same <= 1;
        end
        else begin
            mvp_l1_mvs_not_same <= 0;
        end
    end


    
    always@(*) begin
        case(log2_cb_size_wire)
            3: begin
                cb_size_wire = 8;
            end
            4: begin
                cb_size_wire = 16;  
            end
            5: begin
                cb_size_wire = 32;
            end
            6: begin
                cb_size_wire = 64;
            end
            default: begin
                cb_size_wire = 0;
            end
        endcase
    end
    
always@(posedge clk) begin
    if(config_mode_in == `INTER_CTU0_HEADER) begin
        new_ctu_start_in <= 1;
    end
    else begin
        new_ctu_start_in <= 0;
    end
end

// synthesis translate_off
always@(posedge clk) begin
	if(mv_derive_state != 0) begin
		if(config_mode_in != `INTER_MVD_0_INFO && config_mode_in != `INTER_MVD_1_INFO && config_mode_in != `INTER_TOP_CONFIG_IDLE) begin
			$display("%d mv derive engine not ready to accept packets %d state %d mode !",$time,mv_derive_state,config_mode_in);
			$stop;
		end
	end
end
// synthesis translate_on

always@(posedge clk) begin
    if(reset) begin
        is_mvd_l0_filled <= 0;
        is_mvd_l1_filled <= 0;
    end
    else begin
        case(config_mode_in)
            `INTER_PU_HEADER: begin
                is_mvd_l0_filled <= 0;
                is_mvd_l1_filled <= 0;
            end
            `INTER_MVD_0_INFO: begin
                is_mvd_l0_filled <= 1;
`ifdef READ_FILE
                mvd_l0_x <=     config_bus_in[  MVD_WIDTH -1  :0* MVD_WIDTH];
                mvd_l0_y <=     config_bus_in[  2*MVD_WIDTH -1:1* MVD_WIDTH];     
`else
                mvd_l0_x <=     config_bus_in[  INTER_TOP_CONFIG_BUS_WIDTH - 1:
                                                INTER_TOP_CONFIG_BUS_WIDTH - MVD_WIDTH];
                mvd_l0_y <=     config_bus_in[  INTER_TOP_CONFIG_BUS_WIDTH - 1:
                                                INTER_TOP_CONFIG_BUS_WIDTH - MVD_WIDTH];     
`endif
            end
            `INTER_MVD_1_INFO: begin
                is_mvd_l1_filled <= 1;
`ifdef READ_FILE
                mvd_l1_x <=     config_bus_in[  MVD_WIDTH -1  :0* MVD_WIDTH];
                mvd_l1_y <=     config_bus_in[  2*MVD_WIDTH -1:1* MVD_WIDTH];     
`else
                mvd_l1_x <=     config_bus_in[  INTER_TOP_CONFIG_BUS_WIDTH - 1:
                                                INTER_TOP_CONFIG_BUS_WIDTH - MVD_WIDTH];
                mvd_l1_y <=     config_bus_in[  INTER_TOP_CONFIG_BUS_WIDTH - 1:
                                                INTER_TOP_CONFIG_BUS_WIDTH - MVD_WIDTH];     
`endif  
            end
        endcase   
    
    end

end


always@(posedge clk) begin  :cycle1_2
    cb_size_is_8 <= (cb_size == 8)?1'b1 : 1'b0;
    log2_parallel_is_great_2 <= (log2_parallel_merge_level >2 )? 1'b1:1'b0;
    
end

always@(posedge clk) begin : cycle2_3

    if (cb_part_mode == `PART_2NxnD || cb_part_mode == `PART_2NxnU) begin
        part_mode_up_down <= 1;
    end
    else begin
        part_mode_up_down <= 0;
    end
    
    if (cb_part_mode == `PART_nLx2N || cb_part_mode == `PART_nRx2N) begin
        part_mode_left_right <= 1;
    end
    else begin
        part_mode_left_right <= 0;
    end
    part_mode_n_2n <= (cb_part_mode == `PART_Nx2N)?1'b1:1'b0;
    part_mode_2n_n <= (cb_part_mode == `PART_2NxN)?1'b1:1'b0;
    part_mode_n_n <= (cb_part_mode == `PART_NxN)?1'b1:1'b0;
end

always@(posedge clk) begin  : cycle3_4

    col_xbr_addr <= xx_pb_ww_add;
    col_ybr_addr <= yy_pb_hh_add;


    case(current_cand_num)
        0: begin
            mv_read_from_top <= 0;
            part_mode_check_merge_4 <= (part_mode_n_2n | part_mode_left_right)?1'b1:1'b0;
            is_part_idx_equal_1  <= (pb_part_idx == 1)? 1'b1: 1'b0;
            if (merge_flag) begin   //A1
                xN_in <= (xx_pb - 1)%(1<<(X11_ADDR_WDTH+1));
                yN_in <= (yy_pb + hh_pb -1)%(1<<(X11_ADDR_WDTH+1));
            end
            else begin              //A0
                xN_in <= (xx_pb - 1)%(1<<(X11_ADDR_WDTH+1));
                yN_in <= (yy_pb + hh_pb)%(1<<(X11_ADDR_WDTH+1));   

            end
        end
        1 : begin
            part_mode_check_merge_4 <= (part_mode_2n_n | part_mode_up_down)?1'b1:1'b0;
            is_part_idx_equal_1  <= (pb_part_idx == 1)? 1'b1: 1'b0;
            if (merge_flag) begin   //B1
                xN_in <= (xx_pb + ww_pb -1)%(1<<(X11_ADDR_WDTH+1));
                yN_in <= (yy_pb -1)%(1<<(X11_ADDR_WDTH+1));
                mv_read_from_top <= 1;
            end
            else begin              //A1
                xN_in <= (xx_pb - 1)%(1<<(X11_ADDR_WDTH+1));
                yN_in <= (yy_pb + hh_pb -1)%(1<<(X11_ADDR_WDTH+1));     
                mv_read_from_top <= 0;
            end
        end
        2: begin
            //B0
            is_part_idx_equal_1 <= 0;
            part_mode_check_merge_4 <= 0;
            xN_in <= (xx_pb + ww_pb)%(1<<(X11_ADDR_WDTH+1));
            yN_in <= (yy_pb - 1)%(1<<(X11_ADDR_WDTH+1));
            mv_read_from_top <= 1;

        end
        3: begin
            is_part_idx_equal_1 <= 0;
            part_mode_check_merge_4 <= 0;
            if (merge_flag) begin   //A0
                xN_in <= (xx_pb - 1)%(1<<(X11_ADDR_WDTH+1));
                yN_in <= (yy_pb + hh_pb)%(1<<(X11_ADDR_WDTH+1));  
                mv_read_from_top <= 0;
            end
            else begin              //B1
                xN_in <= (xx_pb + ww_pb -1)%(1<<(X11_ADDR_WDTH+1));
                yN_in <= (yy_pb -1)%(1<<(X11_ADDR_WDTH+1));    
                mv_read_from_top <= 1;
            end
        end
        4: begin
            //B2
            is_part_idx_equal_1 <= 0;
            part_mode_check_merge_4 <= 0;
            mv_read_from_top <= 1;
            xN_in <= (xx_pb - 1)%(1<<(X11_ADDR_WDTH+1));
            yN_in <= (yy_pb - 1)%(1<<(X11_ADDR_WDTH+1));  
        end
    endcase
    pu_dim_sum <= nOrigPbH + nOrigPbW;
end


always@(posedge clk) begin :cycle_4_5

    
    mv_read_from_top_1d <= mv_read_from_top;
    nbr_y_3_qdrant <= (cb_yy_plus_half_cb <= yN_in) ? 1'b1:1'b0;
    nbr_x_3_qdrant <= (cb_xx_plus_half_cb > xN_in) ? 1'b1:1'b0;
    
    part_idx_mode_logic_merge_5 <= (part_mode_check_merge_4 & is_part_idx_equal_1)? 1'b1:1'b0;

    if(pu_dim_sum == 12) begin
        is_smallest_pu <= 1;
    end
    else begin
        is_smallest_pu <= 0;
    end
    
    if ((xN_in >> log2_parallel_merge_level) == (xx_pb >> log2_parallel_merge_level)) begin
		is_diff_mer_x <= 1;
	end
    else begin
        is_diff_mer_x <= 0;
    end
    
    if ((yN_in >> log2_parallel_merge_level) == (yy_pb >> log2_parallel_merge_level)) begin
        is_diff_mer_y <= 1;
    end
    else begin
        is_diff_mer_y <= 0;
    end
    
    if (xx_pb_prev <= xN_in) begin
        cand_x_down_range_check <= 1;
    end
   
    else begin
        cand_x_down_range_check <= 0;
    end
    if((xx_pb_prev -1 == xN_in) ) begin// ************* xx_pb_prev-1 has to be put here to accomodate the case that available N request xx_pp-1 address of prev mv, which is being currently updated.. so , it should take from left buffer for safety
        cand_x_prev_top_left <= 1;
    end
    else begin
        cand_x_prev_top_left <= 0;
    end
    if (xx_pb_prev + ww_pb_prev > xN_in) begin
        cand_x_up_range_check <= 1;
    end
    else begin
        cand_x_up_range_check <= 0;
    end
    
    if (yy_pb_prev <= yN_in) begin
        cand_y_down_range_check <= 1;
    end
    else begin
        cand_y_down_range_check <= 0;
    end
    
    if (yy_pb_prev + hh_pb_prev > yN_in) begin
        cand_y_up_range_check <= 1;
    end
    else begin
        cand_y_up_range_check <= 0;
    end
end

always@(posedge clk) begin  :cycle_5_6
    if(merge_flag == 0 || col_mv_ready == 0) begin
        tbl0 <= current_poc - ref_poc_l0;
        tbl1 <= current_poc - ref_poc_l1;
    end
    mv_read_from_top_2d <= mv_read_from_top_1d;
    part_merge_logic_merge_6 <= (part_idx_mode_logic_merge_5 | (is_diff_mer_x & is_diff_mer_y)) ? 1'b1:1'b0;
    if(nbr_x_3_qdrant & nbr_y_3_qdrant & part_mode_n_n & is_part_idx_equal_1) begin
        qudrant3_exception <= 1;
    end
    else begin
        qudrant3_exception <= 0;
    end
    if (cand_y_up_range_check & cand_y_down_range_check) begin
        cand_y_range_check <= 1;
    end
    else begin
        cand_y_range_check <= 0;
    end
    
    if (cand_x_up_range_check & cand_x_down_range_check) begin
        cand_x_range_check <= 1;
    end
    else begin
        cand_x_range_check <= 0;
    end
    cand_x_prev_top_left_d <= cand_x_prev_top_left;
end

always@(posedge clk) begin : cycle_6_7
    mv_read_from_top_3d <= mv_read_from_top_2d;
    if (cand_x_range_check & cand_y_range_check) begin
        is_cand_in_prev_pu <= 1;
    end
    else begin
        is_cand_in_prev_pu <= 0;
    end
    
    cand_x_prev_top_left_2d <= cand_x_prev_top_left_d & cand_y_range_check;
    xN_pu_mv_buffer_in <= xN_pu_out;
    yN_pu_mv_buffer_in <= yN_pu_out;

    if(mv_derive_state == STATE_MV_DERIVE_AVAILABLE_CHECK4 || mv_derive_state == STATE_MV_DERIVE_MV_AVAILABLE_MERGE_9 || mv_derive_state == STATE_MV_DERIVE_MV_AVAILABLE_MERGE_10 || mv_derive_state == STATE_MV_DERIVE_MV_AVAILABLE_MERGE_11 ||
                                                              mv_derive_state == STATE_MV_DERIVE_MV_AVAILABLE_AMVP_9 || mv_derive_state == STATE_MV_DERIVE_MV_AVAILABLE_AMVP_10 || mv_derive_state == STATE_MV_DERIVE_MV_AVAILABLE_AMVP_11) begin
        if(same_cb_check == 1) begin
            if(merge_flag == 1) begin
                available_flag_to_mv_buffer <= !part_merge_logic_merge_6 & !qudrant3_exception;
            end
            else begin
                available_flag_to_mv_buffer <= !qudrant3_exception;
            end
        end
        else begin
            if(merge_flag == 1) begin
                available_flag_to_mv_buffer <= avaialble_flag_out & !part_merge_logic_merge_6;
            end
            else begin
                available_flag_to_mv_buffer <= avaialble_flag_out;
            end
        end
    end
    else begin
        available_flag_to_mv_buffer <= 0;
    end
    
    if(tbl0 > 127) begin    // merge flag check removed expecting reduction in combination delay
        tbl0_clipped <= 127;
    end
    else if(tbl0 < -128) begin
        tbl0_clipped <= -128;
    end
    else begin
        tbl0_clipped <= tbl0[CLIPPED_WIDTH -1:0];
    end
    if(tbl1 > 127) begin    
        tbl1_clipped <= 127;
    end
    else if(tbl1 < -128) begin
        tbl1_clipped <= -128;
    end
    else begin
        tbl1_clipped <= tbl1[CLIPPED_WIDTH -1:0];
    end    
    
end
    
always@(posedge clk) begin : cycle_8_9

end


always@(*) begin 
    case(current_cand_num)
        0: begin
            is_available_cand = is_available_a0;
            cand_l0_pred_flag = a0_mv_field_pred_flag_l0;
            cand_l1_pred_flag = a0_mv_field_pred_flag_l1;
        end
        1: begin
            is_available_cand = is_available_a1;
            cand_l0_pred_flag = a1_mv_field_pred_flag_l0;
            cand_l1_pred_flag = a1_mv_field_pred_flag_l1;
        end
        2: begin
            is_available_cand = is_available_b0;
            cand_l0_pred_flag = b0_mv_field_pred_flag_l0;
            cand_l1_pred_flag = b0_mv_field_pred_flag_l1;            
        end
        3: begin
            is_available_cand = is_available_b1;
            cand_l0_pred_flag = b1_mv_field_pred_flag_l0;
            cand_l1_pred_flag = b1_mv_field_pred_flag_l1;                        
        end
        4: begin
            is_available_cand = is_available_b2;
            cand_l0_pred_flag = b2_mv_field_pred_flag_l0;
            cand_l1_pred_flag = b2_mv_field_pred_flag_l1;                        
        end
        default: begin
            is_available_cand = 1'b0;
            cand_l0_pred_flag = 0;
            cand_l1_pred_flag = 0;
        end
    endcase
end
    

always@(posedge clk) begin 

    if(is_available_cand) begin :cycle_10_11
        ref_poc_cand_l0 <= ref_pic_list0_poc_data_out;
        ref_poc_cand_l1 <= ref_pic_list1_poc_data_out;
        cand_l0_pred_flag_d <= cand_l0_pred_flag;
        cand_l1_pred_flag_d <= cand_l1_pred_flag;
    end
    
    if(is_available_cand_1d) begin : cycle_11_12
        cand_l0_pred_flag_2d <= cand_l0_pred_flag_d;
        cand_l1_pred_flag_2d <= cand_l1_pred_flag_d;
        zero_diff_refl0_cand_l0 <= (ref_poc_l0 - ref_poc_cand_l0) == 0 ? 1'b1 : 1'b0;
        zero_diff_refl0_cand_l1 <= (ref_poc_l0 - ref_poc_cand_l1) == 0 ? 1'b1 : 1'b0;
        zero_diff_refl1_cand_l0 <= (ref_poc_l1 - ref_poc_cand_l0) == 0 ? 1'b1 : 1'b0;     
        zero_diff_refl1_cand_l1 <= (ref_poc_l1 - ref_poc_cand_l1) == 0 ? 1'b1 : 1'b0;
        
        diff_curr_ref_cand_l1 <= current_poc - ref_poc_cand_l1;
        diff_curr_ref_cand_l0 <= current_poc - ref_poc_cand_l0;
    end
   
    if(is_available_cand_2d) begin : cycle_12_13
        if(cand_l0_pred_flag_2d == 1) begin
            if(diff_curr_ref_cand_l0 > 127) begin
                tdl0_clipped <= 127;
            end
            else if(diff_curr_ref_cand_l0 < -128) begin
                tdl0_clipped <= -128;
            end
            else begin
                tdl0_clipped <= diff_curr_ref_cand_l0[7:0];
            end        
        end
        else if(cand_l1_pred_flag_2d == 1) begin
            if(diff_curr_ref_cand_l1 > 127) begin
                tdl0_clipped <= 127;
            end
            else if(diff_curr_ref_cand_l1 < -128) begin
                tdl0_clipped <= -128;
            end
            else begin
                tdl0_clipped <= diff_curr_ref_cand_l1[7:0];
            end        
        end
        
        if(cand_l1_pred_flag_2d == 1) begin
            if(diff_curr_ref_cand_l1 > 127) begin
                tdl1_clipped <= 127;
            end
            else if(diff_curr_ref_cand_l1 < -128) begin
                tdl1_clipped <= -128;
            end
            else begin
                tdl1_clipped <= diff_curr_ref_cand_l1[7:0];
            end
        end
        else if(cand_l0_pred_flag_2d) begin
            if(diff_curr_ref_cand_l0 > 127) begin
                tdl1_clipped <= 127;
            end
            else if(diff_curr_ref_cand_l0 < -128) begin
                tdl1_clipped <= -128;
            end
            else begin
                tdl1_clipped <= diff_curr_ref_cand_l0[7:0];
            end       
        end
                
    end
    
    if(is_available_cand_3d) begin : cycle_13_14
        // calculation of tx
    end
    if(is_available_cand_4d) begin : cycle_14_15
        distscale_factor_l0 <= ((tx_l0 * tbl0_clipped) + 32)>>6;
        distscale_factor_l1 <= ((tx_l1 * tbl1_clipped) + 32)>>6;
    end
    if(is_available_cand_5d) begin : cycle_15_16
        if(|(distscale_factor_l0[TX_WIDTH + CLIPPED_WIDTH -1:12]) & distscale_factor_l0[TX_WIDTH + CLIPPED_WIDTH -1] == 0)begin //if(distscale_factor_l0 > 4095) begin
            distscale_factor_l0_clipped <= 4095;
        end
        else if(!(&(distscale_factor_l0[TX_WIDTH + CLIPPED_WIDTH -1:12])) & distscale_factor_l0[TX_WIDTH + CLIPPED_WIDTH -1] == 1) begin//else if(distscale_factor_l0 < -4096) begin
            distscale_factor_l0_clipped <= -4096;
        end
        else begin
            distscale_factor_l0_clipped <= distscale_factor_l0[DIST_SCALE_WIDTH-1:0] ;
        end
        if(|(distscale_factor_l1[TX_WIDTH + CLIPPED_WIDTH -1:12]) & distscale_factor_l1[TX_WIDTH + CLIPPED_WIDTH -1] == 0) begin//if(distscale_factor_l1 > 4095) begin
            distscale_factor_l1_clipped <= 4095;
        end
        else if(!(&(distscale_factor_l1[TX_WIDTH + CLIPPED_WIDTH -1:12])) & distscale_factor_l1[TX_WIDTH + CLIPPED_WIDTH -1] == 1) begin//else if(distscale_factor_l1 < -4096) begin
            distscale_factor_l1_clipped <= -4096;
        end
        else begin
            distscale_factor_l1_clipped <= distscale_factor_l1[DIST_SCALE_WIDTH-1:0];
        end
        
        if(current_cand_num == 5 || current_cand_num == 6) begin
            pipe_line_mv_l0_x <= mv_l0_a_x;
            pipe_line_mv_l0_y <= mv_l0_a_y;
            pipe_line_mv_l1_x <= mv_l1_a_x;
            pipe_line_mv_l1_y <= mv_l1_a_y;
        end
        else begin
            pipe_line_mv_l0_x <= mv_l0_b_x;
            pipe_line_mv_l0_y <= mv_l0_b_y;
            pipe_line_mv_l1_x <= mv_l1_b_x;
            pipe_line_mv_l1_y <= mv_l1_b_y;
        end
    end
    if(is_available_cand_6d) begin  : cycle_16_17
            dist_scale_into_mv_l0_x <= (distscale_factor_l0_clipped * pipe_line_mv_l0_x);
            dist_scale_into_mv_l0_y <= (distscale_factor_l0_clipped * pipe_line_mv_l0_y);
            dist_scale_into_mv_l1_x <= (distscale_factor_l1_clipped * pipe_line_mv_l1_x);
            dist_scale_into_mv_l1_y <= (distscale_factor_l1_clipped * pipe_line_mv_l1_y);
    end
    if(is_available_cand_7d ) begin : cycle_17_18
            dist_scale_into_mv_l0_x_d <= dist_scale_into_mv_l0_x;
            dist_scale_into_mv_l0_y_d <= dist_scale_into_mv_l0_y;
            dist_scale_into_mv_l1_x_d <= dist_scale_into_mv_l1_x;
            dist_scale_into_mv_l1_y_d <= dist_scale_into_mv_l1_y;    
    end
    if(is_available_cand_8d) begin : cycle_18_19
        new_mv_l0_x <= (sign_dist_scale_into_mv_l0_x)? (abs_dist_scale_into_mv_l0_x_plus_127 >>>8): -1*(abs_dist_scale_into_mv_l0_x_plus_127>>>8);
        new_mv_l0_y <= (sign_dist_scale_into_mv_l0_y)? (abs_dist_scale_into_mv_l0_y_plus_127 >>>8): -1*(abs_dist_scale_into_mv_l0_y_plus_127>>>8);
        new_mv_l1_x <= (sign_dist_scale_into_mv_l1_x)? (abs_dist_scale_into_mv_l1_x_plus_127 >>>8): -1*(abs_dist_scale_into_mv_l1_x_plus_127>>>8);
        new_mv_l1_y <= (sign_dist_scale_into_mv_l1_y)? (abs_dist_scale_into_mv_l1_y_plus_127 >>>8): -1*(abs_dist_scale_into_mv_l1_y_plus_127>>>8);
        
    end
    
end



    always@(posedge clk) begin
        if(reset) begin
            available_config_bus_mode <= `AVAILABLE_CONFIG_IDLE;
            current_mv_field_valid  <= 0;
            mv_done_out <= 1;
            current_cand_num <= 0;
            xx_pb_prev <= {X11_ADDR_WDTH{1'b1}};
            yy_pb_prev <= {Y11_ADDR_WDTH{1'b1}};
            hh_pb_prev <= 0;
            ww_pb_prev <= 0;
            merge_array_idx <= 0;
            mvp_l0_idx <= 0;
            mvp_l1_idx <= 0;
            clear_bi_pred_list <= 0;
            col_addr_valid_in <= 0;
            xx_pb_end <= {X11_ADDR_WDTH{1'b0}};
            yy_pb_end <= {X11_ADDR_WDTH{1'b0}};
        end
        else if(enable) begin
            available_config_bus_mode <= `AVAILABLE_CONFIG_IDLE;
            
            mv_done_out <= 0;//  temp change***********
            //        mv_done_out <= 1;
            case(mv_derive_state) 
                STATE_MV_DERIVE_CONFIG_UPDATE: begin
                    current_mv_field_valid <= 0;
                    mv_done_out <= 1;
                    case(config_mode_in)
                        `INTER_TOP_PARA_0: begin
`ifdef READ_FILE    
                            pic_width <=   (config_bus_in[PIC_WIDTH_WIDTH + HEADER_WIDTH -1: HEADER_WIDTH])%(1<<PIC_DIM_WIDTH);
                            pic_height  <=   (config_bus_in[HEADER_WIDTH + PIC_WIDTH_WIDTH + PIC_HEIGHT_WIDTH- 1: HEADER_WIDTH + PIC_WIDTH_WIDTH])%(1<<PIC_DIM_WIDTH);
                            available_config_bus <= {(config_bus_in[HEADER_WIDTH + PIC_WIDTH_WIDTH - 1: HEADER_WIDTH]),
                                                     (config_bus_in[HEADER_WIDTH + PIC_WIDTH_WIDTH + PIC_HEIGHT_WIDTH -1: HEADER_WIDTH + PIC_WIDTH_WIDTH])};

`else
                            pic_height <=   (config_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH])%(1<<PIC_DIM_WIDTH);
                            pic_width  <=   (config_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH - PIC_HEIGHT_WIDTH])%(1<<PIC_DIM_WIDTH);
                            available_config_bus <= {(config_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH]),
                                                     (config_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH - PIC_HEIGHT_WIDTH])};

`endif
                            available_config_bus_mode <=  `AVAILABLE_CONFIG_PIC_DIM; 
                        end
                        `INTER_TOP_PARA_1: begin
`ifdef READ_FILE
                        log2_ctb_size <=  config_bus_in[ HEADER_WIDTH + LOG2CTBSIZEY_WIDTH -1 : HEADER_WIDTH];
`else                    
                        log2_ctb_size <=  config_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - LOG2CTBSIZEY_WIDTH];
`endif
                        end
                        `INTER_TOP_PARA_2: begin 
`ifdef READ_FILE
                        log2_parallel_merge_level   <= (config_bus_in[  HEADER_WIDTH + NUM_SHORT_TERM_REF_WIDTH + WEIGHTED_PRED_WIDTH + WEIGHTED_BIPRED_WIDTH + PARALLEL_MERGE_LEVEL_WIDTH - 1:
                                                                        HEADER_WIDTH + NUM_SHORT_TERM_REF_WIDTH + WEIGHTED_PRED_WIDTH + WEIGHTED_BIPRED_WIDTH ] + 2)%(1<<LOG2_CTB_WIDTH);                                                                    
`else                    
                        log2_parallel_merge_level   <= (config_bus_in[  INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - NUM_SHORT_TERM_REF_WIDTH - WEIGHTED_PRED_WIDTH - WEIGHTED_BIPRED_WIDTH -1:
                                                                            INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - NUM_SHORT_TERM_REF_WIDTH - WEIGHTED_PRED_WIDTH - WEIGHTED_BIPRED_WIDTH - PARALLEL_MERGE_LEVEL_WIDTH] + 2)%(1<<LOG2_CTB_WIDTH);                                                                    
`endif
                            
                            
                        end
                        `INTER_TOP_SLICE_1: begin
`ifdef READ_FILE
                            slice_temporal_mvp_enabled_flag <= config_bus_in[   HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH- 1:
                                                                                HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH  ];     
                            slice_type              <=      config_bus_in[      HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH + SLICE_TYPE_WIDTH- 1:
                                                                                HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH  ];   
                            num_ref_idx_l0_active  <= config_bus_in[            HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH- 1:
                                                                                HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH  ] + 1'b1;
                                                                
                            num_ref_idx_l1_active   <= config_bus_in[           HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH- 1:
                                                                                HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH  ] + 1'b1;
                            inter_maxnummergecand   <= (5 - config_bus_in[  HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH -1:
                                                                            HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH]) % (1<< MAX_MERGE_CAND_WIDTH);
                            collocated_from_l0_flag <= config_bus_in[   HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH + SLICE_TYPE_WIDTH + COLLOCATED_FROM_L0_FLAG_WIDTH - 1:
                                                                        HEADER_WIDTH + SHORT_TERM_REF_PIC_SPS_WIDTH + SHORT_TERM_REF_PIC_IDX_WIDTH + TEMPORAL_MVP_ENABLED_WIDTH + SAO_LUMA_WIDTH + SAO_CHROMA_WIDTH + NUM_REF_IDX_L0_MINUS1_WIDTH + NUM_REF_IDX_L1_MINUS1_WIDTH + MAX_MERGE_CAND_WIDTH + SLICE_TYPE_WIDTH];
`else                        
                            slice_temporal_mvp_enabled_flag <=   config_bus_in[ INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - 1:
                                                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH];     
                            num_ref_idx_l0_active  <= config_bus_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - 1:
                                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH] + 1'b1;
                                                                
                            num_ref_idx_l1_active   <= config_bus_in[  INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - 1:
                                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH] + 1'b1;
                            inter_maxnummergecand   <= (5 - config_bus_in[  INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - 1:
                                                                            INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH]) % (1<< MAX_MERGE_CAND_WIDTH);
                            slice_type              <=      config_bus_in[  INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH -1:
                                                                            INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH - SLICE_TYPE_WIDTH];   
                            collocated_from_l0_flag <= config_bus_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH - SLICE_TYPE_WIDTH - 1:
                                                                        INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SHORT_TERM_REF_PIC_SPS_WIDTH - SHORT_TERM_REF_PIC_IDX_WIDTH - TEMPORAL_MVP_ENABLED_WIDTH - SAO_LUMA_WIDTH - SAO_CHROMA_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH - NUM_REF_IDX_L1_MINUS1_WIDTH - MAX_MERGE_CAND_WIDTH - SLICE_TYPE_WIDTH - COLLOCATED_FROM_L0_FLAG_WIDTH];
`endif
                        end
                        `INTER_TOP_SLICE_2: begin
`ifdef READ_FILE
                            collocated_ref_idx <= config_bus_in[    HEADER_WIDTH + SLICE_CB_QP_OFFSET_WIDTH + SLICE_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH + SLICE_BETA_OFFSET_DIV2_WIDTH + SLICE_TC_OFFSET_DIV2_WIDTH + COLLOCATED_REF_IDX_WIDTH - 2:
                                                                    HEADER_WIDTH + SLICE_CB_QP_OFFSET_WIDTH + SLICE_CR_QP_OFFSET_WIDTH + DISABLE_DBF_WIDTH + SLICE_BETA_OFFSET_DIV2_WIDTH + SLICE_TC_OFFSET_DIV2_WIDTH];
`else
                             collocated_ref_idx <= config_bus_in[    INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SLICE_CB_QP_OFFSET_WIDTH - SLICE_CR_QP_OFFSET_WIDTH - DISABLE_DBF_WIDTH - SLICE_BETA_OFFSET_DIV2_WIDTH - SLICE_TC_OFFSET_DIV2_WIDTH - 2:
                                                                    INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - SLICE_CB_QP_OFFSET_WIDTH - SLICE_CR_QP_OFFSET_WIDTH - DISABLE_DBF_WIDTH - SLICE_BETA_OFFSET_DIV2_WIDTH - SLICE_TC_OFFSET_DIV2_WIDTH - COLLOCATED_REF_IDX_WIDTH];
`endif
                        end        
                        `INTER_TOP_CURR_POC: begin
`ifdef READ_FILE
                            current_poc <=  config_bus_in[POC_WIDTH - 1: 0];
`else
                            current_poc <=  config_bus_in[INTER_TOP_CONFIG_BUS_WIDTH -1 : INTER_TOP_CONFIG_BUS_WIDTH - POC_WIDTH];
`endif
                        end
                        `INTER_CURRENT_PIC_DPB_IDX: begin
                            current_pic_dpb_idx <= config_bus_in[INTER_TOP_CONFIG_BUS_WIDTH -1: INTER_TOP_CONFIG_BUS_WIDTH - NUM_REF_IDX_L0_MINUS1_WIDTH];
                        end
                        `INTER_SLICE_TILE_INFO: begin
`ifdef READ_FILE                            
                            available_config_bus <= {(config_bus_in[ HEADER_WIDTH + PIC_WIDTH_WIDTH                     - 1: HEADER_WIDTH]),
                                                     (config_bus_in[ HEADER_WIDTH + PIC_WIDTH_WIDTH + PIC_HEIGHT_WIDTH  - 1: HEADER_WIDTH + PIC_WIDTH_WIDTH])};
                            if(config_bus_in[HEADER_WIDTH - 1: 0] == `HEADER_N_SLICE) begin

`else
                            available_config_bus <= {(config_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH]),
                                                     (config_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PIC_WIDTH_WIDTH - PIC_HEIGHT_WIDTH])};
                            if(config_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH] == `HEADER_N_SLICE) begin
`endif
                                available_config_bus_mode <=  `AVAILABLE_CONFIG_PIC_SLICE;                             
                            end
                            else begin
                                available_config_bus_mode <= `AVAILABLE_CONFIG_PIC_TILE;
                            end
                            
                        end
                        `INTER_CTU0_HEADER: begin
                            // new_ctu_start_to_col <= 1;
`ifdef READ_FILE
                            ctb_xx <= config_bus_in[HEADER_WIDTH + X11_ADDR_WDTH   -1: HEADER_WIDTH];                 // 12-bit input assigned to 11- bit output MSB dropped
                            ctb_yy <= config_bus_in[HEADER_WIDTH + X11_ADDR_WDTH + X_ADDR_WDTH -1: HEADER_WIDTH + X_ADDR_WDTH];
`else
                            ctb_xx <= config_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH -1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - X_ADDR_WDTH];                 // 12-bit input assigned to 11- bit output MSB dropped
                            ctb_yy <= config_bus_in[INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - X11_ADDR_WDTH - 1: INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X_ADDR_WDTH];
`endif
                        end
                        `INTER_CU_HEADER: begin
`ifdef READ_FILE
                            cb_xx <= (ctb_xx + config_bus_in[   HEADER_WIDTH + X0_WIDTH - 1: 
                                                                HEADER_WIDTH])%(1<<X11_ADDR_WDTH);
                            cb_yy <= (ctb_yy + config_bus_in[   HEADER_WIDTH + 2*X0_WIDTH -1:
                                                                HEADER_WIDTH + X0_WIDTH])%(1<<X11_ADDR_WDTH);
                            cb_pred_mode <=  config_bus_in[     HEADER_WIDTH + 2*X0_WIDTH + LOG2_CTB_WIDTH];
                            cb_part_mode <=  config_bus_in[     HEADER_WIDTH + 2*X0_WIDTH + LOG2_CTB_WIDTH + PREDMODE_WIDTH + PARTMODE_WIDTH -1:
                                                                HEADER_WIDTH + 2*X0_WIDTH + LOG2_CTB_WIDTH + PREDMODE_WIDTH];
`else
                            cb_xx <= (ctb_xx + config_bus_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 1: 
                                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - X0_WIDTH])%(1<<X11_ADDR_WDTH);
                            cb_yy <= (ctb_yy + config_bus_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - X0_WIDTH - 1: 
                                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH])%(1<<X11_ADDR_WDTH);
                            cb_pred_mode <=  config_bus_in[     INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - LOG2_CTB_WIDTH - 1];
                            cb_part_mode <=  config_bus_in[     INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - LOG2_CTB_WIDTH - PREDMODE_WIDTH - 1:
                                                                INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - 2*X0_WIDTH - LOG2_CTB_WIDTH - PREDMODE_WIDTH - PARTMODE_WIDTH];
`endif
                            pb_part_idx <= 0;   // TODO increment this when motion vector is output and before reading next pu - done
                            cb_size <=  cb_size_wire;
                            end
                        `INTER_PU_HEADER: begin
                            for ( i= 0; i< MAX_NUM_MERGE_CAND_CONST; i=i+1) begin
                                  merge_cand_list[i] <= `MV_MERGE_CAND_NONE;
                            end
                            for ( i= 0; i< AMVP_NUM_CAND_CONST; i=i+1) begin
                                  mvpl0_cand_list[i] <= `MV_MERGE_CAND_NONE;
                                  mvpl1_cand_list[i] <= `MV_MERGE_CAND_NONE;
                            end                      
                            mv_a_l0_updated_later <= 0;
                            mv_a_l1_updated_later <= 0;
                            mv_b_l0_updated_later <= 0;
                            mv_b_l1_updated_later <= 0;
                            clear_bi_pred_list <= 1;
                            merge_array_idx <= 0;
                            mvp_l0_idx <= 0;
                            mvp_l1_idx <= 0;
                            available_flag_a_l0 <= 0;
                            available_flag_a_l1 <= 0;
                            available_flag_b_l0 <= 0;
                            available_flag_b_l1 <= 0;
`ifdef READ_FILE
                            pred_idc    <= config_bus_in[   HEADER_WIDTH + PART_IDX_WIDTH + PRED_IDC_WIDTH -1:
                                                            HEADER_WIDTH + PART_IDX_WIDTH ];
                            merge_flag  <=  config_bus_in[  HEADER_WIDTH + PART_IDX_WIDTH + PRED_IDC_WIDTH ];       
                            merge_idx   <=  config_bus_in[  HEADER_WIDTH + PART_IDX_WIDTH + PRED_IDC_WIDTH + MERGE_FLAG_WIDTH + MERGE_IDX_WIDTH -1:
                                                            HEADER_WIDTH + PART_IDX_WIDTH + PRED_IDC_WIDTH + MERGE_FLAG_WIDTH];   
                            mvp_l0_flag <=  config_bus_in[  HEADER_WIDTH + PART_IDX_WIDTH + PRED_IDC_WIDTH + MERGE_FLAG_WIDTH + MERGE_IDX_WIDTH];
                            mvp_l1_flag <=  config_bus_in[  HEADER_WIDTH + PART_IDX_WIDTH + PRED_IDC_WIDTH + MERGE_FLAG_WIDTH + MERGE_IDX_WIDTH + MVP_L0_FLAG_WIDTH];
                            ref_idx_l0  <=  config_bus_in[  HEADER_WIDTH + PART_IDX_WIDTH + PRED_IDC_WIDTH + MERGE_FLAG_WIDTH + MERGE_IDX_WIDTH + MVP_L0_FLAG_WIDTH + MVP_L1_FLAG_WIDTH + REF_IDX_LX_WIDTH -1:
                                                            HEADER_WIDTH + PART_IDX_WIDTH + PRED_IDC_WIDTH + MERGE_FLAG_WIDTH + MERGE_IDX_WIDTH + MVP_L0_FLAG_WIDTH + MVP_L1_FLAG_WIDTH];
                            ref_idx_l1  <=  config_bus_in[  HEADER_WIDTH + PART_IDX_WIDTH + PRED_IDC_WIDTH + MERGE_FLAG_WIDTH + MERGE_IDX_WIDTH + MVP_L0_FLAG_WIDTH + MVP_L1_FLAG_WIDTH + REF_IDX_LX_WIDTH + REF_IDX_LX_WIDTH -1:
                                                            HEADER_WIDTH + PART_IDX_WIDTH + PRED_IDC_WIDTH + MERGE_FLAG_WIDTH + MERGE_IDX_WIDTH + MVP_L0_FLAG_WIDTH + MVP_L1_FLAG_WIDTH + REF_IDX_LX_WIDTH];
`else
                            pred_idc    <= config_bus_in[   INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PART_IDX_WIDTH - 1:
                                                            INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PART_IDX_WIDTH - PRED_IDC_WIDTH];
                            merge_flag  <=  config_bus_in[  INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PART_IDX_WIDTH - PRED_IDC_WIDTH - 1];       
                            merge_idx   <=  config_bus_in[  INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PART_IDX_WIDTH - PRED_IDC_WIDTH - MERGE_FLAG_WIDTH - 1:
                                                            INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PART_IDX_WIDTH - PRED_IDC_WIDTH - MERGE_FLAG_WIDTH - MERGE_IDX_WIDTH];   
                            mvp_l0_flag <=  config_bus_in[  INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PART_IDX_WIDTH - PRED_IDC_WIDTH - MERGE_FLAG_WIDTH - MERGE_IDX_WIDTH -1];
                            mvp_l1_flag <=  config_bus_in[  INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PART_IDX_WIDTH - PRED_IDC_WIDTH - MERGE_FLAG_WIDTH - MERGE_IDX_WIDTH - MVP_L0_FLAG_WIDTH - 1];
                            ref_idx_l0  <=  config_bus_in[  INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PART_IDX_WIDTH - PRED_IDC_WIDTH - MERGE_FLAG_WIDTH - MERGE_IDX_WIDTH - MVP_L0_FLAG_WIDTH - MVP_L1_FLAG_WIDTH - 1:
                                                            INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PART_IDX_WIDTH - PRED_IDC_WIDTH - MERGE_FLAG_WIDTH - MERGE_IDX_WIDTH - MVP_L0_FLAG_WIDTH - MVP_L1_FLAG_WIDTH - REF_IDX_LX_WIDTH];
                            ref_idx_l1  <=  config_bus_in[  INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PART_IDX_WIDTH - PRED_IDC_WIDTH - MERGE_FLAG_WIDTH - MERGE_IDX_WIDTH - MVP_L0_FLAG_WIDTH - MVP_L1_FLAG_WIDTH - REF_IDX_LX_WIDTH - 1:
                                                            INTER_TOP_CONFIG_BUS_WIDTH - HEADER_WIDTH - PART_IDX_WIDTH - PRED_IDC_WIDTH - MERGE_FLAG_WIDTH - MERGE_IDX_WIDTH - MVP_L0_FLAG_WIDTH - MVP_L1_FLAG_WIDTH - REF_IDX_LX_WIDTH - REF_IDX_LX_WIDTH];
`endif
                        end
                    endcase     
                end
                STATE_MV_DERIVE_PU_OVERWRIT_3: begin
                    clear_bi_pred_list <= 0;
                    avaialble_flag_a0 <= 0;
                    avaialble_flag_a1 <= 0;
                    avaialble_flag_b0 <= 0;
                    avaialble_flag_b1 <= 0;
                    avaialble_flag_b2 <= 0;
                    poc_diff_zero_type_b_done <= 0;
                    if(log2_parallel_is_great_2 & cb_size_is_8 & merge_flag) begin
                        pb_part_idx <= 0;
                        xx_pb <= cb_xx;
                        yy_pb <= cb_yy;
                        hh_pb <= cb_size;
                        ww_pb <= cb_size;                        
                    end
                    else begin
                        xx_pb <= xx_pb_temp;
                        yy_pb <= yy_pb_temp;
                        hh_pb <= hh_pb_temp;
                        ww_pb <= ww_pb_temp; 
                    end
                    nOrigPbH <= hh_pb_temp;
                    nOrigPbW <= ww_pb_temp;                   
                    current_cand_num <= 0; 
                end
                STATE_MV_DERIVE_AVAILABLE_CHECK4: begin
                    current_cand_num <= (current_cand_num +1)%(1<<(MAX_MERGE_CAND_WIDTH));
                    xx_pb_end <= (xx_pb + ww_pb)%(1<<(X11_ADDR_WDTH));
                    yy_pb_end <= (yy_pb + hh_pb)%(1<<(X11_ADDR_WDTH)); 
                    if(col_xbr_addr< pic_width && col_ybr_addr < pic_height && (col_ybr_addr[Y11_ADDR_WDTH-1:CTB_SIZE_WIDTH] == ctb_yy[Y11_ADDR_WDTH-1:CTB_SIZE_WIDTH]) ) begin

                        col_br_boundary_ok <= 1;
                    end
                    else begin
                        col_br_boundary_ok <= 0;
                    end      
                    
                    if(current_cand_num == 1) begin
                        ref_poc_l0 <= ref_pic_list0_poc_data_out;
                        ref_dpb_idx_l0 <= ref_pic_list0_idx_data_out;
                        ref_poc_l1 <= ref_pic_list1_poc_data_out;                        
                        ref_dpb_idx_l1 <= ref_pic_list1_idx_data_out;                        
                    end
                    if(current_cand_num == 0) begin
                        col_addr_valid_in <= 1;
                        col_xctr_addr <= xx_pb_ww2_add[CTB_SIZE_WIDTH-1:LOG2_MIN_COLLOCATE_SIZE];
                        col_yctr_addr <= yy_pb_hh2_add[CTB_SIZE_WIDTH-1:LOG2_MIN_COLLOCATE_SIZE];
                    end
                    else begin
                        col_addr_valid_in <= 0;
                        col_yctr_addr <= col_ybr_addr[CTB_SIZE_WIDTH  -1:LOG2_MIN_COLLOCATE_SIZE];
						if(col_xbr_addr[X11_ADDR_WDTH-1:CTB_SIZE_WIDTH] == ctb_xx[X11_ADDR_WDTH-1:CTB_SIZE_WIDTH]) begin
							col_xctr_addr <= col_xbr_addr[CTB_SIZE_WIDTH  -1:LOG2_MIN_COLLOCATE_SIZE];  
						end
						else begin
							col_xctr_addr <= col_xbr_addr[CTB_SIZE_WIDTH  -1:LOG2_MIN_COLLOCATE_SIZE] + 4'd4;  
						end
                                              
                    end
                    if(current_cand_num == 4 & merge_flag == 1) begin
                        is_available_a1 <= available_flag_to_mv_buffer;
                    end
                    else if(current_cand_num == 4 & merge_flag == 0) begin
                        is_available_a0 <= available_flag_to_mv_buffer;
                    end
                    
                end
                STATE_MV_DERIVE_MV_AVAILABLE_AMVP_9: begin
                    mv_a_l0_filled_type <= `MVP_FILLE_MV_NO;
                    mv_a_l1_filled_type <= `MVP_FILLE_MV_NO;
                    mv_b_l0_filled_type <= `MVP_FILLE_MV_NO;
                    mv_b_l1_filled_type <= `MVP_FILLE_MV_NO;
                    is_available_a1 <= available_flag_to_mv_buffer;         
                    if(is_available_a0) begin
                        is_available_a0 <= (cand_mv_field_pred_flag_l0 | cand_mv_field_pred_flag_l1);
                    end
                    a0_mv_field_pred_flag_l0    <= cand_mv_field_pred_flag_l0;
                    a0_mv_field_pred_flag_l1    <= cand_mv_field_pred_flag_l1;
                    a0_mv_field_ref_idx_l0      <= cand_mv_field_ref_idx_l0;
                    a0_mv_field_ref_idx_l1      <= cand_mv_field_ref_idx_l1;
                    a0_mv_field_mv_x_l0         <= cand_mv_field_mv_x_l0;
                    a0_mv_field_mv_y_l0         <= cand_mv_field_mv_y_l0;
                    a0_mv_field_mv_x_l1         <= cand_mv_field_mv_x_l1;
                    a0_mv_field_mv_y_l1         <= cand_mv_field_mv_y_l1;                    
                end
                STATE_MV_DERIVE_MV_AVAILABLE_AMVP_10: begin
                    is_available_b0 <= available_flag_to_mv_buffer;   
                    if(is_available_a1) begin
                        is_available_a1 <= cand_mv_field_pred_flag_l0 | cand_mv_field_pred_flag_l1;
                    end
                    a1_mv_field_pred_flag_l0    <= cand_mv_field_pred_flag_l0;
                    a1_mv_field_pred_flag_l1    <= cand_mv_field_pred_flag_l1;
                    a1_mv_field_ref_idx_l0      <= cand_mv_field_ref_idx_l0;
                    a1_mv_field_ref_idx_l1      <= cand_mv_field_ref_idx_l1;
                    a1_mv_field_mv_x_l0         <= cand_mv_field_mv_x_l0;
                    a1_mv_field_mv_y_l0         <= cand_mv_field_mv_y_l0;
                    a1_mv_field_mv_x_l1         <= cand_mv_field_mv_x_l1;
                    a1_mv_field_mv_y_l1         <= cand_mv_field_mv_y_l1;
                    current_cand_num <= 0;
                end
                STATE_MV_DERIVE_MV_AVAILABLE_AMVP_11: begin
                    current_cand_num <= (current_cand_num +1)%(1<<(MAX_MERGE_CAND_WIDTH));
                    is_available_b1 <= available_flag_to_mv_buffer; 
                    is_scaled_flag <= is_available_a0 | is_available_a1;
                    if(is_available_b0) begin
                        is_available_b0 <= cand_mv_field_pred_flag_l0 | cand_mv_field_pred_flag_l1;
                    end
                    b0_mv_field_pred_flag_l0    <= cand_mv_field_pred_flag_l0;
                    b0_mv_field_pred_flag_l1    <= cand_mv_field_pred_flag_l1;
                    b0_mv_field_ref_idx_l0      <= cand_mv_field_ref_idx_l0;
                    b0_mv_field_ref_idx_l1      <= cand_mv_field_ref_idx_l1;
                    b0_mv_field_mv_x_l0         <= cand_mv_field_mv_x_l0;
                    b0_mv_field_mv_y_l0         <= cand_mv_field_mv_y_l0;
                    b0_mv_field_mv_x_l1         <= cand_mv_field_mv_x_l1;
                    b0_mv_field_mv_y_l1         <= cand_mv_field_mv_y_l1;
                end
                STATE_MV_DERIVE_MV_AVAILABLE_AMVP_12: begin
                    current_cand_num <= (current_cand_num +1)%(1<<(MAX_MERGE_CAND_WIDTH)); 
                    is_available_b2 <= available_flag_to_mv_buffer; 
                    if(is_available_b1) begin
                        is_available_b1 <= cand_mv_field_pred_flag_l0 | cand_mv_field_pred_flag_l1;
                    end
                    b1_mv_field_pred_flag_l0    <= cand_mv_field_pred_flag_l0;
                    b1_mv_field_pred_flag_l1    <= cand_mv_field_pred_flag_l1;
                    b1_mv_field_ref_idx_l0      <= cand_mv_field_ref_idx_l0;
                    b1_mv_field_ref_idx_l1      <= cand_mv_field_ref_idx_l1;
                    b1_mv_field_mv_x_l0         <= cand_mv_field_mv_x_l0;
                    b1_mv_field_mv_y_l0         <= cand_mv_field_mv_y_l0;
                    b1_mv_field_mv_x_l1         <= cand_mv_field_mv_x_l1;
                    b1_mv_field_mv_y_l1         <= cand_mv_field_mv_y_l1;
                    
                end
                STATE_MV_DERIVE_MV_AVAILABLE_AMVP_13: begin
                    current_cand_num <= (current_cand_num +1)%(1<<(MAX_MERGE_CAND_WIDTH));
                    if(is_available_b2) begin
                        is_available_b2 <= cand_mv_field_pred_flag_l0 | cand_mv_field_pred_flag_l1;
                    end
                    if(is_available_a0 & current_mv_field_pred_flag_l0) begin
                        if(zero_diff_refl0_cand_l0 & a0_mv_field_pred_flag_l0) begin
                            available_flag_a_l0 <= 1;
                            mvp_l0_idx <= 1;                        
                            mv_l0_a_x <= a0_mv_field_mv_x_l0;
                            mv_l0_a_y <= a0_mv_field_mv_y_l0;   
                            mvpl0_cand_list[0] <= `MV_AMVP_CAND_A;
                            mv_a_l0_filled_type <= `MVP_FILLE_MV_A0;
                            mv_a_l0_updated_later <= 0;
                        end
                        else if(zero_diff_refl0_cand_l1 & a0_mv_field_pred_flag_l1) begin
                            available_flag_a_l0 <= 1;
                            mvp_l0_idx <= 1;                        
                            mv_l0_a_x <= a0_mv_field_mv_x_l1;
                            mv_l0_a_y <= a0_mv_field_mv_y_l1;
                            mv_a_l0_updated_later <= 0;                            
                            mvpl0_cand_list[0] <= `MV_AMVP_CAND_A;
                            mv_a_l0_filled_type <= `MVP_FILLE_MV_A0;
                        end
                        else if(a0_mv_field_pred_flag_l0 && mv_a_l0_updated_later == 0) begin
                            mv_l0_a_x <= a0_mv_field_mv_x_l0;
                            mv_l0_a_y <= a0_mv_field_mv_y_l0; 
                            mv_a_l0_updated_later <= 1;
                            mvpl0_cand_list[0] <= `MV_AMVP_CAND_A;
                            mv_a_l0_filled_type <= `MVP_FILLE_MV_A0;
                        end
                        else if(a0_mv_field_pred_flag_l1 && mv_a_l0_updated_later == 0) begin
                            mv_l0_a_x <= a0_mv_field_mv_x_l1;
                            mv_l0_a_y <= a0_mv_field_mv_y_l1;
                            mv_a_l0_updated_later <= 1;                            
                            mvpl0_cand_list[0] <= `MV_AMVP_CAND_A;
                            mv_a_l0_filled_type <= `MVP_FILLE_MV_A0;                        
                        end
                    end
                    if(is_available_a0 & current_mv_field_pred_flag_l1) begin   //// l1 gets priority from pred_flag_list1
                        if(zero_diff_refl1_cand_l1 & a0_mv_field_pred_flag_l1) begin
                            available_flag_a_l1 <= 1;
                            mvp_l1_idx <= 1;                        
                            mv_l1_a_x <= a0_mv_field_mv_x_l1;
                            mv_l1_a_y <= a0_mv_field_mv_y_l1;   
                            mvpl1_cand_list[0] <= `MV_AMVP_CAND_A;
                            mv_a_l1_filled_type <= `MVP_FILLE_MV_A0;
                            mv_a_l1_updated_later <= 0;
                        end
                        else if(zero_diff_refl1_cand_l0 & a0_mv_field_pred_flag_l0) begin
                            available_flag_a_l1 <= 1;
                            mvp_l1_idx <= 1;                        
                            mv_l1_a_x <= a0_mv_field_mv_x_l0;
                            mv_l1_a_y <= a0_mv_field_mv_y_l0;
                            mv_a_l1_updated_later <= 0;                            
                            mvpl1_cand_list[0] <= `MV_AMVP_CAND_A;
                            mv_a_l1_filled_type <= `MVP_FILLE_MV_A0;
                        end
                        else if(a0_mv_field_pred_flag_l1 && mv_a_l1_updated_later == 0) begin
                            mv_l1_a_x <= a0_mv_field_mv_x_l1;
                            mv_l1_a_y <= a0_mv_field_mv_y_l1; 
                            mv_a_l1_updated_later <= 1;
                            mvpl1_cand_list[0] <= `MV_AMVP_CAND_A;
                            mv_a_l1_filled_type <= `MVP_FILLE_MV_A0;
                        end
                        else if(a0_mv_field_pred_flag_l0 && mv_a_l1_updated_later == 0) begin
                            mv_l1_a_x <= a0_mv_field_mv_x_l0;
                            mv_l1_a_y <= a0_mv_field_mv_y_l0;
                            mv_a_l1_updated_later <= 1;                            
                            mvpl1_cand_list[0] <= `MV_AMVP_CAND_A;
                            mv_a_l1_filled_type <= `MVP_FILLE_MV_A0;                        
                        end                   
                    end             
                    
                    b2_mv_field_pred_flag_l0    <= cand_mv_field_pred_flag_l0;
                    b2_mv_field_pred_flag_l1    <= cand_mv_field_pred_flag_l1;
                    b2_mv_field_ref_idx_l0      <= cand_mv_field_ref_idx_l0;
                    b2_mv_field_ref_idx_l1      <= cand_mv_field_ref_idx_l1;
                    b2_mv_field_mv_x_l0         <= cand_mv_field_mv_x_l0;
                    b2_mv_field_mv_y_l0         <= cand_mv_field_mv_y_l0;
                    b2_mv_field_mv_x_l1         <= cand_mv_field_mv_x_l1;
                    b2_mv_field_mv_y_l1         <= cand_mv_field_mv_y_l1;                
                end
                STATE_MV_DERIVE_MV_PROCESS_AMVP14: begin
                    current_cand_num <= (current_cand_num +1)%(1<<(MAX_MERGE_CAND_WIDTH));
                    if(available_flag_a_l0 == 0) begin
                        if(is_available_a1 & current_mv_field_pred_flag_l0) begin
                            if(zero_diff_refl0_cand_l0 & a1_mv_field_pred_flag_l0) begin
                                available_flag_a_l0 <= 1;
                                mvp_l0_idx <= 1;                        
                                mv_l0_a_x <= a1_mv_field_mv_x_l0;
                                mv_l0_a_y <= a1_mv_field_mv_y_l0;   
                                mvpl0_cand_list[0] <= `MV_AMVP_CAND_A;
                                mv_a_l0_filled_type <= `MVP_FILLE_MV_A1;
                                mv_a_l0_updated_later <= 0;
                            end
                            else if(zero_diff_refl0_cand_l1 & a1_mv_field_pred_flag_l1) begin
                                available_flag_a_l0 <= 1;
                                mvp_l0_idx <= 1;                        
                                mv_l0_a_x <= a1_mv_field_mv_x_l1;
                                mv_l0_a_y <= a1_mv_field_mv_y_l1;
                                mv_a_l0_updated_later <= 0;                            
                                mvpl0_cand_list[0] <= `MV_AMVP_CAND_A;
                                mv_a_l0_filled_type <= `MVP_FILLE_MV_A1;
                            end
                            else if(a1_mv_field_pred_flag_l0 && mv_a_l0_updated_later == 0) begin
                                mv_l0_a_x <= a1_mv_field_mv_x_l0;
                                mv_l0_a_y <= a1_mv_field_mv_y_l0; 
                                mv_a_l0_updated_later <= 1;
                                mvpl0_cand_list[0] <= `MV_AMVP_CAND_A;
                                mv_a_l0_filled_type <= `MVP_FILLE_MV_A1;
                            end
                            else if(a1_mv_field_pred_flag_l1 && mv_a_l0_updated_later == 0) begin
                                mv_l0_a_x <= a1_mv_field_mv_x_l1;
                                mv_l0_a_y <= a1_mv_field_mv_y_l1;
                                mv_a_l0_updated_later <= 1;                            
                                mvpl0_cand_list[0] <= `MV_AMVP_CAND_A;
                                mv_a_l0_filled_type <= `MVP_FILLE_MV_A1;                        
                            end
                        end           
                    end
                    if(available_flag_a_l1 == 0) begin
                        if(is_available_a1 & current_mv_field_pred_flag_l1) begin   //// l1 gets priority from pred_flag_list1
                            if(zero_diff_refl1_cand_l1 & a1_mv_field_pred_flag_l1) begin
                                available_flag_a_l1 <= 1;
                                mvp_l1_idx <= 1;                        
                                mv_l1_a_x <= a1_mv_field_mv_x_l1;
                                mv_l1_a_y <= a1_mv_field_mv_y_l1;   
                                mvpl1_cand_list[0] <= `MV_AMVP_CAND_A;
                                mv_a_l1_filled_type <= `MVP_FILLE_MV_A1;
                                mv_a_l1_updated_later <= 0;
                            end
                            else if(zero_diff_refl1_cand_l0 & a1_mv_field_pred_flag_l0) begin
                                available_flag_a_l1 <= 1;
                                mvp_l1_idx <= 1;                        
                                mv_l1_a_x <= a1_mv_field_mv_x_l0;
                                mv_l1_a_y <= a1_mv_field_mv_y_l0;
                                mv_a_l1_updated_later <= 0;                            
                                mvpl1_cand_list[0] <= `MV_AMVP_CAND_A;
                                mv_a_l1_filled_type <= `MVP_FILLE_MV_A1;
                            end
                            else if(a1_mv_field_pred_flag_l1 && mv_a_l1_updated_later == 0) begin 
                                mv_l1_a_x <= a1_mv_field_mv_x_l1;
                                mv_l1_a_y <= a1_mv_field_mv_y_l1; 
                                mv_a_l1_updated_later <= 1;
                                mvpl1_cand_list[0] <= `MV_AMVP_CAND_A;
                                mv_a_l1_filled_type <= `MVP_FILLE_MV_A1;
                            end
                            else if(a1_mv_field_pred_flag_l0 && mv_a_l1_updated_later == 0) begin
                                mv_l1_a_x <= a1_mv_field_mv_x_l0;
                                mv_l1_a_y <= a1_mv_field_mv_y_l0;
                                mv_a_l1_updated_later <= 1;                            
                                mvpl1_cand_list[0] <= `MV_AMVP_CAND_A;
                                mv_a_l1_filled_type <= `MVP_FILLE_MV_A1;                        
                            end                   
                        end              
                    end
                end
                STATE_MV_DERIVE_MV_PROCESS_AMVP15: begin
                    current_cand_num <= (current_cand_num +1)%(1<<(MAX_MERGE_CAND_WIDTH));
                    if(is_scaled_flag) begin
                        if(available_flag_b_l0 == 0) begin
                            if(is_available_b0 & current_mv_field_pred_flag_l0) begin
                                if(zero_diff_refl0_cand_l0 & b0_mv_field_pred_flag_l0) begin
                                    available_flag_b_l0 <= 1;                    
                                    mv_l0_b_x <= b0_mv_field_mv_x_l0;
                                    mv_l0_b_y <= b0_mv_field_mv_y_l0;   
                                    if(mv_a_l0_updated_later == 0) begin
                                        if(mvp_l0_cand_found == 0 && mvp_l0_idx == 0)
                                            mvp_l0_idx <= 1;
                                            // mvp_l0_idx <= (mvp_l0_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);       // adds B type mv if type A has not kept anything to update later
                                                                               
                                    end
                                    //mvpl0_cand_list[mvp_l0_idx] <= `MV_AMVP_CAND_B;    
                                    mv_b_l0_filled_type <= `MVP_FILLE_MV_B0;
                                    mv_b_l0_updated_later <= 0;
                                end
                                else if(zero_diff_refl0_cand_l1 & b0_mv_field_pred_flag_l1) begin
                                    available_flag_b_l0 <= 1;
                                    if(mv_a_l0_updated_later == 0) begin
                                        if(mvp_l0_cand_found == 0 && mvp_l0_idx == 0)
                                            mvp_l0_idx <= 1;
                                            // mvp_l0_idx <= (mvp_l0_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);       // adds B type mv if type A has not kept anything to update later
                                                                               
                                    end                       
                                    mv_l0_b_x <= b0_mv_field_mv_x_l1;
                                    mv_l0_b_y <= b0_mv_field_mv_y_l1;
                                    mv_b_l0_updated_later <= 0;                            
                                    //mvpl0_cand_list[mvp_l0_idx] <= `MV_AMVP_CAND_B;
                                    mv_b_l0_filled_type <= `MVP_FILLE_MV_B0;
                                end
                                // else if(b0_mv_field_pred_flag_l0 && mv_b_l0_updated_later == 0) begin
                                    // mv_l0_b_x <= b0_mv_field_mv_x_l0;
                                    // mv_l0_b_y <= b0_mv_field_mv_y_l0; 
                                    // mv_b_l0_updated_later <= 1;
                                    // //mvpl0_cand_list[mvp_l0_idx] <= `MV_AMVP_CAND_B;
                                    // mv_b_l0_filled_type <= `MVP_FILLE_MV_B0;
                                // end
                                // else if(b0_mv_field_pred_flag_l1 && mv_b_l0_updated_later == 0) begin
                                    // mv_l0_b_x <= b0_mv_field_mv_x_l1;
                                    // mv_l0_b_y <= b0_mv_field_mv_y_l1;
                                    // mv_b_l0_updated_later <= 1;                            
                                    // //mvpl0_cand_list[mvp_l0_idx] <= `MV_AMVP_CAND_B;
                                    // mv_b_l0_filled_type <= `MVP_FILLE_MV_B0;                        
                                // end
                            end 
                        end
                        if(available_flag_b_l1 == 0) begin
                            if(is_available_b0 & current_mv_field_pred_flag_l1) begin
                                if(zero_diff_refl1_cand_l1 & b0_mv_field_pred_flag_l1) begin
                                    available_flag_b_l1 <= 1;                    
                                    mv_l1_b_x <= b0_mv_field_mv_x_l1;
                                    mv_l1_b_y <= b0_mv_field_mv_y_l1;   
                                    if(mv_a_l1_updated_later == 0) begin
                                        if(mvp_l1_cand_found == 0 && mvp_l1_idx == 0) begin
                                            mvp_l1_idx <= 1;                       
                                        end       // adds B type mv if type A has not kept anything to update later
                                                                                
                                    end
                                    mvpl1_cand_list[mvp_l1_idx] <= `MV_AMVP_CAND_B;   
                                    mv_b_l1_filled_type <= `MVP_FILLE_MV_B0;
                                    mv_b_l1_updated_later <= 0;
                                end
                                else if(zero_diff_refl1_cand_l0 & b0_mv_field_pred_flag_l0) begin
                                    available_flag_b_l1 <= 1;
                                    if(mv_a_l1_updated_later == 0) begin
                                        if(mvp_l1_cand_found == 0 && mvp_l1_idx == 0) begin
                                            mvp_l1_idx <= 1;                       
                                        end       // adds B type mv if type A has not kept anything to update later                                         
                                    end                  
                                    mv_l1_b_x <= b0_mv_field_mv_x_l0;
                                    mv_l1_b_y <= b0_mv_field_mv_y_l0;
                                    mv_b_l1_updated_later <= 0;                            
                                    mvpl1_cand_list[mvp_l1_idx] <= `MV_AMVP_CAND_B;
                                    mv_b_l1_filled_type <= `MVP_FILLE_MV_B0;
                                end
                                // else if(b0_mv_field_pred_flag_l1 && mv_b_l1_updated_later == 0) begin
                                    // mv_l1_b_x <= b0_mv_field_mv_x_l1;
                                    // mv_l1_b_y <= b0_mv_field_mv_y_l1; 
                                    // mv_b_l1_updated_later <= 1;
                                    // mvpl1_cand_list[mvp_l1_idx] <= `MV_AMVP_CAND_B;
                                    // mv_b_l1_filled_type <= `MVP_FILLE_MV_B0;
                                // end
                                // else if(b0_mv_field_pred_flag_l0 && mv_b_l1_updated_later == 0) begin
                                    // mv_l1_b_x <= b0_mv_field_mv_x_l0;
                                    // mv_l1_b_y <= b0_mv_field_mv_y_l0;
                                    // mv_b_l1_updated_later <= 1;                            
                                    // mvpl1_cand_list[mvp_l1_idx] <= `MV_AMVP_CAND_B;
                                    // mv_b_l1_filled_type <= `MVP_FILLE_MV_B0;                        
                                // end
                            end 
                        end
                    end
                    else begin  //if(is_scaled_flag == 0)
                        if(is_available_b0 & current_mv_field_pred_flag_l0) begin
                            if(available_flag_a_l0 == 0) begin
                                if(zero_diff_refl0_cand_l0 & b0_mv_field_pred_flag_l0) begin
                                    available_flag_a_l0 <= 1;
                                    mvp_l0_idx <= 1;                        
                                    mv_l0_a_x <= b0_mv_field_mv_x_l0;
                                    mv_l0_a_y <= b0_mv_field_mv_y_l0;   
                                    mvpl0_cand_list[0] <= `MV_AMVP_CAND_A;
                                    mv_a_l0_filled_type <= `MVP_FILLE_MV_B0;
                                end
                                else if(zero_diff_refl0_cand_l1 & b0_mv_field_pred_flag_l1) begin
                                    available_flag_a_l0 <= 1;
                                    mvp_l0_idx <= 1;                        
                                    mv_l0_a_x <= b0_mv_field_mv_x_l1;
                                    mv_l0_a_y <= b0_mv_field_mv_y_l1;                
                                    mvpl0_cand_list[0] <= `MV_AMVP_CAND_A;
                                    mv_a_l0_filled_type <= `MVP_FILLE_MV_B0;
                                end
                            end
                            if(mv_b_l0_updated_later ==0) begin
                                if(b0_mv_field_pred_flag_l0 ) begin     //&& zero_diff_refl0_cand_l0 ==0 
                                    mv_l0_b_x <= b0_mv_field_mv_x_l0;
                                    mv_l0_b_y <= b0_mv_field_mv_y_l0; 
                                    mv_b_l0_updated_later <= 1;
                                    mv_b_l0_filled_type <= `MVP_FILLE_MV_B0;
                                end
                                else if(b0_mv_field_pred_flag_l1 ) begin        //zero_diff_refl0_cand_l1== 0
                                    mv_l0_b_x <= b0_mv_field_mv_x_l1;
                                    mv_l0_b_y <= b0_mv_field_mv_y_l1;
                                    mv_b_l0_updated_later <= 1;                    
                                    mv_b_l0_filled_type <= `MVP_FILLE_MV_B0;                        
                                end
                            end
                        end
                        
                        if(is_available_b0 & current_mv_field_pred_flag_l1) begin
                            if(available_flag_a_l1 == 0) begin
                                if(zero_diff_refl1_cand_l1 & b0_mv_field_pred_flag_l1) begin
                                    available_flag_a_l1 <= 1;
                                    mvp_l1_idx <= 1;                        
                                    mv_l1_a_x <= b0_mv_field_mv_x_l1;
                                    mv_l1_a_y <= b0_mv_field_mv_y_l1;   
                                    mvpl1_cand_list[0] <= `MV_AMVP_CAND_A;
                                    mv_a_l1_filled_type <= `MVP_FILLE_MV_B0;
                                end
                                else if(zero_diff_refl1_cand_l0 & b0_mv_field_pred_flag_l0) begin
                                    available_flag_a_l1 <= 1;
                                    mvp_l1_idx <= 1;                        
                                    mv_l1_a_x <= b0_mv_field_mv_x_l0;
                                    mv_l1_a_y <= b0_mv_field_mv_y_l0;                
                                    mvpl1_cand_list[0] <= `MV_AMVP_CAND_A;
                                    mv_a_l1_filled_type <= `MVP_FILLE_MV_B0;
                                end
                            end
                            if(mv_b_l1_updated_later ==0) begin
                                if(b0_mv_field_pred_flag_l1  ) begin        //&& zero_diff_refl1_cand_l1 ==0
                                    mv_l1_b_x <= b0_mv_field_mv_x_l1;
                                    mv_l1_b_y <= b0_mv_field_mv_y_l1; 
                                    mv_b_l1_updated_later <= 1;
                                    mv_b_l1_filled_type <= `MVP_FILLE_MV_B0;
                                end
                                else if(b0_mv_field_pred_flag_l0 ) begin        //&& zero_diff_refl1_cand_l0== 0
                                    mv_l1_b_x <= b0_mv_field_mv_x_l0;
                                    mv_l1_b_y <= b0_mv_field_mv_y_l0;
                                    mv_b_l1_updated_later <= 1;                    
                                    mv_b_l1_filled_type <= `MVP_FILLE_MV_B0;                        
                                end
                            end
                        end
                    end                    
                end
                STATE_MV_DERIVE_MV_PROCESS_AMVP16: begin
                    current_cand_num <= (current_cand_num +1)%(1<<(MAX_MERGE_CAND_WIDTH));
                    if(is_scaled_flag) begin
                        if(available_flag_b_l0 == 0) begin
                            if(is_available_b1 & current_mv_field_pred_flag_l0) begin
                                if(zero_diff_refl0_cand_l0 & b1_mv_field_pred_flag_l0) begin
                                    available_flag_b_l0 <= 1;                    
                                    mv_l0_b_x <= b1_mv_field_mv_x_l0;
                                    mv_l0_b_y <= b1_mv_field_mv_y_l0;   
                                    if(mv_a_l0_updated_later == 0) begin
                                        if(mvp_l0_cand_found == 0 && mvp_l0_idx == 0)
                                            mvp_l0_idx <= 1;
                                            // mvp_l0_idx <= (mvp_l0_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);       // adds B type mv if type A has not kept anything to update later
                                                                                   
                                    end
                                    //mvpl0_cand_list[mvp_l0_idx] <= `MV_AMVP_CAND_B;
                                    mv_b_l0_filled_type <= `MVP_FILLE_MV_B1;
                                    mv_b_l0_updated_later <= 0;
                                end
                                else if(zero_diff_refl0_cand_l1 & b1_mv_field_pred_flag_l1) begin
                                    available_flag_b_l0 <= 1;
                                    if(mv_a_l0_updated_later == 0) begin
                                        if(mvp_l0_cand_found == 0 && mvp_l0_idx == 0)
                                            mvp_l0_idx <= 1;
                                            // mvp_l0_idx <= (mvp_l0_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);       // adds B type mv if type A has not kept anything to update later
                                                                                   
                                    end                    
                                    mv_l0_b_x <= b1_mv_field_mv_x_l1;
                                    mv_l0_b_y <= b1_mv_field_mv_y_l1;
                                    mv_b_l0_updated_later <= 0;                            
                                    //mvpl0_cand_list[mvp_l0_idx] <= `MV_AMVP_CAND_B;
                                    mv_b_l0_filled_type <= `MVP_FILLE_MV_B1;
                                end
                                // else if(b1_mv_field_pred_flag_l0 && mv_b_l0_updated_later == 0) begin
                                    // mv_l0_b_x <= b1_mv_field_mv_x_l0;
                                    // mv_l0_b_y <= b1_mv_field_mv_y_l0; 
                                    // mv_b_l0_updated_later <= 1;
                                    // //mvpl0_cand_list[mvp_l0_idx] <= `MV_AMVP_CAND_B;
                                    // mv_b_l0_filled_type <= `MVP_FILLE_MV_B1;
                                // end
                                // else if(b1_mv_field_pred_flag_l1 && mv_b_l0_updated_later == 0) begin
                                    // mv_l0_b_x <= b1_mv_field_mv_x_l1;
                                    // mv_l0_b_y <= b1_mv_field_mv_y_l1;
                                    // mv_b_l0_updated_later <= 1;                            
                                    // //mvpl0_cand_list[mvp_l0_idx] <= `MV_AMVP_CAND_B;
                                    // mv_b_l0_filled_type <= `MVP_FILLE_MV_B1;                        
                                // end
                            end 
                        end
                        if(available_flag_b_l1 == 0) begin
                            if(is_available_b1 & current_mv_field_pred_flag_l1) begin
                                if(zero_diff_refl1_cand_l1 & b1_mv_field_pred_flag_l1) begin
                                    available_flag_b_l1 <= 1;                    
                                    mv_l1_b_x <= b1_mv_field_mv_x_l1;
                                    mv_l1_b_y <= b1_mv_field_mv_y_l1;   
                                    if(mv_a_l1_updated_later == 0) begin
                                        if(mvp_l1_cand_found == 0 && mvp_l1_idx == 0) begin
                                            mvp_l1_idx <= 1;                       
                                        end      // adds B type mv if type A has not kept anything to update later
                                                                             
                                    end
                                    mvpl1_cand_list[mvp_l1_idx] <= `MV_AMVP_CAND_B;      
                                    mv_b_l1_filled_type <= `MVP_FILLE_MV_B1;
                                    mv_b_l1_updated_later <= 0;
                                end
                                else if(zero_diff_refl1_cand_l0 & b1_mv_field_pred_flag_l0) begin
                                    available_flag_b_l1 <= 1;
                                    if(mv_a_l1_updated_later == 0) begin
                                        if(mvp_l1_cand_found == 0 && mvp_l1_idx == 0) begin
                                            mvp_l1_idx <= 1;                       
                                        end      // adds B type mv if type A has not kept anything to update later
                                                                             
                                    end                      
                                    mv_l1_b_x <= b1_mv_field_mv_x_l0;
                                    mv_l1_b_y <= b1_mv_field_mv_y_l0;
                                    mv_b_l1_updated_later <= 0;                            
                                    mvpl1_cand_list[mvp_l1_idx] <= `MV_AMVP_CAND_B;
                                    mv_b_l1_filled_type <= `MVP_FILLE_MV_B1;
                                end
                                // else if(b1_mv_field_pred_flag_l1 && mv_b_l1_updated_later == 0) begin
                                    // mv_l1_b_x <= b1_mv_field_mv_x_l1;
                                    // mv_l1_b_y <= b1_mv_field_mv_y_l1; 
                                    // mv_b_l1_updated_later <= 1;
                                    // mvpl1_cand_list[mvp_l1_idx] <= `MV_AMVP_CAND_B;
                                    // mv_b_l1_filled_type <= `MVP_FILLE_MV_B1;
                                // end
                                // else if(b1_mv_field_pred_flag_l0 && mv_b_l1_updated_later == 0) begin
                                    // mv_l1_b_x <= b1_mv_field_mv_x_l0;
                                    // mv_l1_b_y <= b1_mv_field_mv_y_l0;
                                    // mv_b_l1_updated_later <= 1;                            
                                    // mvpl1_cand_list[mvp_l1_idx] <= `MV_AMVP_CAND_B;
                                    // mv_b_l1_filled_type <= `MVP_FILLE_MV_B1;                        
                                // end
                            end 
                        end
                    end
                    else begin  //if(is_scaled_flag == 0)
                        if(is_available_b1 & current_mv_field_pred_flag_l0) begin
                            if(available_flag_a_l0 == 0) begin
                                if(zero_diff_refl0_cand_l0 & b1_mv_field_pred_flag_l0) begin
                                    available_flag_a_l0 <= 1;
                                    mvp_l0_idx <= 1;                        
                                    mv_l0_a_x <= b1_mv_field_mv_x_l0;
                                    mv_l0_a_y <= b1_mv_field_mv_y_l0;   
                                    mvpl0_cand_list[0] <= `MV_AMVP_CAND_A;
                                    mv_a_l0_filled_type <= `MVP_FILLE_MV_B1;
                                end
                                else if(zero_diff_refl0_cand_l1 & b1_mv_field_pred_flag_l1) begin
                                    available_flag_a_l0 <= 1;
                                    mvp_l0_idx <= 1;                        
                                    mv_l0_a_x <= b1_mv_field_mv_x_l1;
                                    mv_l0_a_y <= b1_mv_field_mv_y_l1;                
                                    mvpl0_cand_list[0] <= `MV_AMVP_CAND_A;
                                    mv_a_l0_filled_type <= `MVP_FILLE_MV_B1;
                                end
                            end
                            if(mv_b_l0_updated_later ==0) begin
                                if(b1_mv_field_pred_flag_l0 ) begin //&& zero_diff_refl0_cand_l0 ==0 
                                    mv_l0_b_x <= b1_mv_field_mv_x_l0;
                                    mv_l0_b_y <= b1_mv_field_mv_y_l0; 
                                    mv_b_l0_updated_later <= 1;
                                    mv_b_l0_filled_type <= `MVP_FILLE_MV_B1;
                                end
                                else if(b1_mv_field_pred_flag_l1) begin     //zero_diff_refl0_cand_l1== 0
                                    mv_l0_b_x <= b1_mv_field_mv_x_l1;
                                    mv_l0_b_y <= b1_mv_field_mv_y_l1;
                                    mv_b_l0_updated_later <= 1;                    
                                    mv_b_l0_filled_type <= `MVP_FILLE_MV_B1;                        
                                end
                            end
                        end
                        
                        if(is_available_b1 & current_mv_field_pred_flag_l1) begin
                            if(available_flag_a_l1 == 0) begin
                                if(zero_diff_refl1_cand_l1 & b1_mv_field_pred_flag_l1) begin
                                    available_flag_a_l1 <= 1;
                                    mvp_l1_idx <= 1;                        
                                    mv_l1_a_x <= b1_mv_field_mv_x_l1;
                                    mv_l1_a_y <= b1_mv_field_mv_y_l1;   
                                    mvpl1_cand_list[0] <= `MV_AMVP_CAND_A;
                                    mv_a_l1_filled_type <= `MVP_FILLE_MV_B1;
                                end
                                else if(zero_diff_refl1_cand_l0 & b1_mv_field_pred_flag_l0) begin
                                    available_flag_a_l1 <= 1;
                                    mvp_l1_idx <= 1;                        
                                    mv_l1_a_x <= b1_mv_field_mv_x_l0;
                                    mv_l1_a_y <= b1_mv_field_mv_y_l0;                
                                    mvpl1_cand_list[0] <= `MV_AMVP_CAND_A;
                                    mv_a_l1_filled_type <= `MVP_FILLE_MV_B1;
                                end
                            end
                            if(mv_b_l1_updated_later ==0) begin
                                if(b1_mv_field_pred_flag_l1  ) begin        //&& zero_diff_refl1_cand_l1 ==0
                                    mv_l1_b_x <= b1_mv_field_mv_x_l1;
                                    mv_l1_b_y <= b1_mv_field_mv_y_l1; 
                                    mv_b_l1_updated_later <= 1;
                                    mv_b_l1_filled_type <= `MVP_FILLE_MV_B1;
                                end
                                else if(b1_mv_field_pred_flag_l0 ) begin    //&& zero_diff_refl1_cand_l0== 0
                                    mv_l1_b_x <= b1_mv_field_mv_x_l0;
                                    mv_l1_b_y <= b1_mv_field_mv_y_l0;
                                    mv_b_l1_updated_later <= 1;                    
                                    mv_b_l1_filled_type <= `MVP_FILLE_MV_B1;                        
                                end
                            end
                        end
                    end                
                end
                STATE_MV_DERIVE_MV_PROCESS_AMVP17: begin
                    current_cand_num <= (current_cand_num +1)%(1<<(MAX_MERGE_CAND_WIDTH));
                    if(is_scaled_flag) begin
                        if(available_flag_b_l0 == 0) begin
                            if(is_available_b2 & current_mv_field_pred_flag_l0) begin
                                if(zero_diff_refl0_cand_l0 & b2_mv_field_pred_flag_l0) begin
                                    available_flag_b_l0 <= 1;                    
                                    mv_l0_b_x <= b2_mv_field_mv_x_l0;
                                    mv_l0_b_y <= b2_mv_field_mv_y_l0;   
                                    if(mv_a_l0_updated_later == 0) begin
                                        if(mvp_l0_cand_found == 0 && mvp_l0_idx == 0) begin
                                            mvp_l0_idx <= 1;                       
                                        end                  // mvp_l0_idx <= (mvp_l0_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);       // adds B type mv if type A has not kept anything to update later                            
                                    end
                                    //mvpl0_cand_list[mvp_l0_idx] <= `MV_AMVP_CAND_B;
                                    mv_b_l0_filled_type <= `MVP_FILLE_MV_B2;
                                    mv_b_l0_updated_later <= 0;
                                end
                                else if(zero_diff_refl0_cand_l1 & b2_mv_field_pred_flag_l1) begin
                                    available_flag_b_l0 <= 1;
                                    if(mv_a_l0_updated_later == 0) begin
                                        if(mvp_l0_cand_found == 0 && mvp_l0_idx == 0) begin
                                            mvp_l0_idx <= 1;                       
                                        end
                                    end
                                    mv_l0_b_x <= b2_mv_field_mv_x_l1;
                                    mv_l0_b_y <= b2_mv_field_mv_y_l1;
                                    mv_b_l0_updated_later <= 0;                            
                                    //mvpl0_cand_list[mvp_l0_idx] <= `MV_AMVP_CAND_B;
                                    mv_b_l0_filled_type <= `MVP_FILLE_MV_B2;
                                end
                                // else if(b2_mv_field_pred_flag_l0 && mv_b_l0_updated_later == 0) begin
                                    // mv_l0_b_x <= b2_mv_field_mv_x_l0;
                                    // mv_l0_b_y <= b2_mv_field_mv_y_l0; 
                                    // mv_b_l0_updated_later <= 1;
                                    // //mvpl0_cand_list[mvp_l0_idx] <= `MV_AMVP_CAND_B;
                                    // mv_b_l0_filled_type <= `MVP_FILLE_MV_B2;
                                // end
                                // else if(b2_mv_field_pred_flag_l1 && mv_b_l0_updated_later == 0) begin
                                    // mv_l0_b_x <= b2_mv_field_mv_x_l1;
                                    // mv_l0_b_y <= b2_mv_field_mv_y_l1;
                                    // mv_b_l0_updated_later <= 1;                            
                                    // //mvpl0_cand_list[mvp_l0_idx] <= `MV_AMVP_CAND_B;
                                    // mv_b_l0_filled_type <= `MVP_FILLE_MV_B2;                        
                                // end
                            end 
                        end
                        if(available_flag_b_l1 == 0) begin
                            if(is_available_b2 & current_mv_field_pred_flag_l1) begin
                                if(zero_diff_refl1_cand_l1 & b2_mv_field_pred_flag_l1) begin
                                    available_flag_b_l1 <= 1;                    
                                    mv_l1_b_x <= b2_mv_field_mv_x_l1;
                                    mv_l1_b_y <= b2_mv_field_mv_y_l1;   
                                    if(mv_a_l1_updated_later == 0) begin
                                        if(mvp_l1_cand_found == 0 && mvp_l1_idx == 0) begin
                                            mvp_l1_idx <= 1;                                    // adds B type mv if type A has not kept anything to update later
                                        end
                                    end
                                    mvpl1_cand_list[mvp_l1_idx] <= `MV_AMVP_CAND_B;
                                    mv_b_l1_filled_type <= `MVP_FILLE_MV_B2;
                                    mv_b_l1_updated_later <= 0;
                                end
                                else if(zero_diff_refl1_cand_l0 & b2_mv_field_pred_flag_l0) begin
                                    available_flag_b_l1 <= 1;
                                    if(mv_a_l1_updated_later == 0) begin
                                        if(mvp_l1_cand_found == 0 && mvp_l1_idx == 0) begin
                                            mvp_l1_idx <= 1;                                    // adds B type mv if type A has not kept anything to update later
                                        end
                                    end
                                    
                                    mv_l1_b_x <= b2_mv_field_mv_x_l0;
                                    mv_l1_b_y <= b2_mv_field_mv_y_l0;
                                    mv_b_l1_updated_later <= 0;                            
                                    mvpl1_cand_list[mvp_l1_idx] <= `MV_AMVP_CAND_B;
                                    mv_b_l1_filled_type <= `MVP_FILLE_MV_B2;
                                end
                                // else if(b2_mv_field_pred_flag_l1 && mv_b_l1_updated_later == 0) begin
                                    // mv_l1_b_x <= b2_mv_field_mv_x_l1;
                                    // mv_l1_b_y <= b2_mv_field_mv_y_l1; 
                                    // mv_b_l1_updated_later <= 1;
                                    // mvpl1_cand_list[mvp_l1_idx] <= `MV_AMVP_CAND_B;
                                    // mv_b_l1_filled_type <= `MVP_FILLE_MV_B2;
                                // end
                                // else if(b2_mv_field_pred_flag_l0 && mv_b_l1_updated_later == 0) begin
                                    // mv_l1_b_x <= b2_mv_field_mv_x_l0;
                                    // mv_l1_b_y <= b2_mv_field_mv_y_l0;
                                    // mv_b_l1_updated_later <= 1;                            
                                    // mvpl1_cand_list[mvp_l1_idx] <= `MV_AMVP_CAND_B;
                                    // mv_b_l1_filled_type <= `MVP_FILLE_MV_B2;                        
                                // end
                            end 
                        end
                    end
                    else begin  //if(is_scaled_flag == 0)
                        if(is_available_b2 & current_mv_field_pred_flag_l0) begin
                            if(available_flag_a_l0 == 0) begin
                                if(zero_diff_refl0_cand_l0 & b2_mv_field_pred_flag_l0) begin
                                    available_flag_a_l0 <= 1;
                                    mvp_l0_idx <= 1;                        
                                    mv_l0_a_x <= b2_mv_field_mv_x_l0;
                                    mv_l0_a_y <= b2_mv_field_mv_y_l0;   
                                    mvpl0_cand_list[0] <= `MV_AMVP_CAND_A;
                                    mv_a_l0_filled_type <= `MVP_FILLE_MV_B2;
                                end
                                else if(zero_diff_refl0_cand_l1 & b2_mv_field_pred_flag_l1) begin
                                    available_flag_a_l0 <= 1;
                                    mvp_l0_idx <= 1;                        
                                    mv_l0_a_x <= b2_mv_field_mv_x_l1;
                                    mv_l0_a_y <= b2_mv_field_mv_y_l1;                
                                    mvpl0_cand_list[0] <= `MV_AMVP_CAND_A;
                                    mv_a_l0_filled_type <= `MVP_FILLE_MV_B2;
                                end
                            end
                            if(mv_b_l0_updated_later ==0) begin
                                if(b2_mv_field_pred_flag_l0 ) begin     //&& zero_diff_refl0_cand_l0 ==0 
                                    mv_l0_b_x <= b2_mv_field_mv_x_l0;
                                    mv_l0_b_y <= b2_mv_field_mv_y_l0; 
                                    mv_b_l0_updated_later <= 1;
                                    mv_b_l0_filled_type <= `MVP_FILLE_MV_B2;
                                end
                                else if(b2_mv_field_pred_flag_l1 ) begin    //&& zero_diff_refl0_cand_l1== 0
                                    mv_l0_b_x <= b2_mv_field_mv_x_l1;
                                    mv_l0_b_y <= b2_mv_field_mv_y_l1;
                                    mv_b_l0_updated_later <= 1;                    
                                    mv_b_l0_filled_type <= `MVP_FILLE_MV_B2;                        
                                end
                            end
                        end
                        
                        if(is_available_b2 & current_mv_field_pred_flag_l1) begin
                            if(available_flag_a_l1 == 0) begin
                                if(zero_diff_refl1_cand_l1 & b2_mv_field_pred_flag_l1) begin
                                    available_flag_a_l1 <= 1;
                                    mvp_l1_idx <= 1;                        
                                    mv_l1_a_x <= b2_mv_field_mv_x_l1;
                                    mv_l1_a_y <= b2_mv_field_mv_y_l1;   
                                    mvpl1_cand_list[0] <= `MV_AMVP_CAND_A;
                                    mv_a_l1_filled_type <= `MVP_FILLE_MV_B2;
                                end
                                else if(zero_diff_refl1_cand_l0 & b2_mv_field_pred_flag_l0) begin
                                    available_flag_a_l1 <= 1;
                                    mvp_l1_idx <= 1;                        
                                    mv_l1_a_x <= b2_mv_field_mv_x_l0;
                                    mv_l1_a_y <= b2_mv_field_mv_y_l0;                
                                    mvpl1_cand_list[0] <= `MV_AMVP_CAND_A;
                                    mv_a_l1_filled_type <= `MVP_FILLE_MV_B2;
                                end
                            end
                            if(mv_b_l1_updated_later ==0) begin
                                if(b2_mv_field_pred_flag_l1 ) begin //&& zero_diff_refl1_cand_l1 ==0 
                                    mv_l1_b_x <= b2_mv_field_mv_x_l1;
                                    mv_l1_b_y <= b2_mv_field_mv_y_l1; 
                                    mv_b_l1_updated_later <= 1;
                                    mv_b_l1_filled_type <= `MVP_FILLE_MV_B2;
                                end
                                else if(b2_mv_field_pred_flag_l0 ) begin    //&& zero_diff_refl1_cand_l0== 0
                                    mv_l1_b_x <= b2_mv_field_mv_x_l0;
                                    mv_l1_b_y <= b2_mv_field_mv_y_l0;
                                    mv_b_l1_updated_later <= 1;                    
                                    mv_b_l1_filled_type <= `MVP_FILLE_MV_B2;                        
                                end
                            end
                        end
                    end
                end
                STATE_MV_DERIVE_MV_PROCESS_AMVP19: begin
                    cand_num_id <= 0;
                    mv_a_filled_last_cycle_l0 <= 0;
                    mv_a_filled_last_cycle_l1 <= 0;
                end      
                STATE_MV_DERIVE_MV_PROCESS_AMVP20: begin
                    cand_num_id <= (cand_num_id +1)%(1<<(MAX_MERGE_CAND_WIDTH));
                    case(cand_num_id)
                        0: begin
                            if(mv_a_l0_filled_type == `MVP_FILLE_MV_A0 & mv_a_l0_updated_later == 1) begin
                                mv_l0_a_x <= new_mv_l0_x_clipped;
                                mv_l0_a_y <= new_mv_l0_y_clipped;
                                mvp_l0_idx <= 1;       
                                mvpl0_cand_list[0] <= `MV_AMVP_CAND_A;
                                mv_a_filled_last_cycle_l0 <= 1;
                                available_flag_a_l0 <= 1;
                            end
                            if(mv_a_l1_filled_type == `MVP_FILLE_MV_A0 & mv_a_l1_updated_later == 1) begin
                                mv_l1_a_x <= new_mv_l1_x_clipped;
                                mv_l1_a_y <= new_mv_l1_y_clipped;
                                mvp_l1_idx <= 1;       
                                mvpl1_cand_list[0] <= `MV_AMVP_CAND_A;
                                mv_a_filled_last_cycle_l1 <= 1;
                                available_flag_a_l1 <= 1;
                            end                         
                        end
                        1: begin
                            if(mv_a_l0_filled_type == `MVP_FILLE_MV_A1 & mv_a_l0_updated_later == 1) begin
                                mv_l0_a_x <= new_mv_l0_x_clipped;
                                mv_l0_a_y <= new_mv_l0_y_clipped;
                                mvp_l0_idx <= 1;       
                                mvpl0_cand_list[0] <= `MV_AMVP_CAND_A;
                                mv_a_filled_last_cycle_l0 <= 1;
                                available_flag_a_l0 <= 1;
                            end
                            // else if(mv_a_filled_last_cycle_l0 & available_flag_b_l0 & !mv_b_l0_updated_later) begin // increment list if cand a was holding cand b with diff_poc = 0 being entered to the list
                                // if(mvp_l0_cand_found == 0 && mvp_l0_idx == 0) begin
                                    // mvp_l0_idx <= 1;                       
                                // end

                            // end
                            if(mv_a_l1_filled_type == `MVP_FILLE_MV_A1 & mv_a_l1_updated_later == 1) begin
                                mv_l1_a_x <= new_mv_l1_x_clipped;
                                mv_l1_a_y <= new_mv_l1_y_clipped;
                                mvp_l1_idx <= 1;       
                                mvpl1_cand_list[0] <= `MV_AMVP_CAND_A;
                                mv_a_filled_last_cycle_l1 <= 1;
                                available_flag_a_l1 <= 1;
                            end   
                            // else if(mv_a_filled_last_cycle_l1 & available_flag_b_l1 & !mv_b_l1_updated_later) begin // increment list if cand a was holding cand b with diff_poc = 0 being entered to the list
                                // if(mvp_l1_cand_found == 0 && mvp_l1_idx == 0) begin
                                    // mvp_l1_idx <= 1;                       
                                // end
                            // end
                        end
                        2: begin
                            if(mv_b_l0_filled_type == `MVP_FILLE_MV_B0 & mv_b_l0_updated_later == 1) begin
                                mv_l0_b_x <= new_mv_l0_x_clipped;
                                mv_l0_b_y <= new_mv_l0_y_clipped;
                                if(mvp_l0_cand_found == 0 && mvp_l0_idx == 0)  begin
                                            mvp_l0_idx <= 1;
                                end
                                    // mvp_l0_idx <= (mvp_l0_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH); 
                                mvpl0_cand_list[mvp_l0_idx] <= `MV_AMVP_CAND_B;
                                available_flag_b_l0 <= 1;
                            end
                            // else if(mv_a_filled_last_cycle_l0 & available_flag_b_l0 & !mv_b_l0_updated_later) begin // increment list if cand a was holding cand b with diff_poc = 0 being entered to the list
                                // if(mvp_l0_cand_found ==0 )
                                    // mvp_l0_idx <= (mvp_l0_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH); 
                            // end
                            if(mv_b_l1_filled_type == `MVP_FILLE_MV_B0 & mv_b_l1_updated_later == 1) begin
                                mv_l1_b_x <= new_mv_l1_x_clipped;
                                mv_l1_b_y <= new_mv_l1_y_clipped;
                                if(mvp_l1_cand_found == 0 && mvp_l1_idx == 0) begin
                                    mvp_l1_idx <= 1;                       
                                end
                                mvpl1_cand_list[mvp_l1_idx] <= `MV_AMVP_CAND_B;
                                available_flag_b_l1 <= 1;
                            end  
                            // else if(mv_a_filled_last_cycle_l1 & available_flag_b_l1 & !mv_b_l1_updated_later) begin // increment list if cand a was holding cand b with diff_poc = 0 being entered to the list
                                // if(mvp_l1_cand_found == 0 && mvp_l1_idx == 0) begin
                                    // mvp_l1_idx <= 1;                       
                                // end                      // need not give nother chance in B1 because this line is valid only when cand b had not kept it to update later
                            // end
                        end
                        3: begin
                            if(mv_b_l0_filled_type == `MVP_FILLE_MV_B1 & mv_b_l0_updated_later == 1) begin
                                mv_l0_b_x <= new_mv_l0_x_clipped;
                                mv_l0_b_y <= new_mv_l0_y_clipped;
                                if(mvp_l0_cand_found == 0 && mvp_l0_idx == 0) begin
                                            mvp_l0_idx <= 1;
                                    // mvp_l0_idx <= (mvp_l0_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);   
                                end
                                available_flag_b_l0 <= 1;
                                mvpl0_cand_list[mvp_l0_idx] <= `MV_AMVP_CAND_B;
                            end
                            if(mv_b_l1_filled_type == `MVP_FILLE_MV_B1 & mv_b_l1_updated_later == 1) begin
                                mv_l1_b_x <= new_mv_l1_x_clipped;
                                mv_l1_b_y <= new_mv_l1_y_clipped;
                                if(mvp_l1_cand_found == 0 && mvp_l1_idx == 0) begin
                                    mvp_l1_idx <= 1;                       
                                end      
                                mvpl1_cand_list[mvp_l1_idx] <= `MV_AMVP_CAND_B;
                                available_flag_b_l1 <= 1;
                            end                        
                        end
                        4: begin
                            if(mv_b_l0_filled_type == `MVP_FILLE_MV_B2 & mv_b_l0_updated_later == 1) begin
                                mv_l0_b_x <= new_mv_l0_x_clipped;
                                mv_l0_b_y <= new_mv_l0_y_clipped;
                                if(mvp_l0_cand_found == 0 && mvp_l0_idx == 0) begin
                                            mvp_l0_idx <= 1;
                                    // mvp_l0_idx <= (mvp_l0_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);       
                                end
                                mvpl0_cand_list[mvp_l0_idx] <= `MV_AMVP_CAND_B;
                                available_flag_b_l0 <= 1;
                            end
                            if(mv_b_l1_filled_type == `MVP_FILLE_MV_B2 & mv_b_l1_updated_later == 1) begin
                                mv_l1_b_x <= new_mv_l1_x_clipped;
                                mv_l1_b_y <= new_mv_l1_y_clipped;
                                if(mvp_l1_cand_found == 0 && mvp_l1_idx == 0) begin
                                    mvp_l1_idx <= 1;                       
                                end       
                                mvpl1_cand_list[mvp_l1_idx] <= `MV_AMVP_CAND_B;
                                available_flag_b_l1 <= 1;
                            end                        
                        end
                    endcase
               
                end
                STATE_MV_DERIVE_COMPARE_MVS: begin
                    if(mv_b_l0_filled_type != `MVP_FILLE_MV_NO && available_flag_b_l0 == 1) begin
                        // assertion ( mvp_l0_idx == 1 && )
                        if(available_flag_a_l0 == 1) begin
                            if(mvp_l0_mvs_not_same && mvp_l0_cand_found == 0 ) begin
                                mvp_l0_idx <= 2;       
                                mvpl0_cand_list[1] <= `MV_AMVP_CAND_B;
                            end
                        end
                        else begin
                            //if(mvp_l0_cand_found == 0 ) begin
                                mvp_l0_idx <= 1;       
                                mvpl0_cand_list[0] <= `MV_AMVP_CAND_B;
                            //end
                        end
                    end
                    if(mv_b_l1_filled_type != `MVP_FILLE_MV_NO && available_flag_b_l1 == 1) begin
                        // assertion ( mvp_l1_idx == 1 && )
                        if(available_flag_a_l1 == 1) begin
                            if(mvp_l1_mvs_not_same && mvp_l1_cand_found == 0 ) begin
                                mvp_l1_idx <= 2; 
                                mvpl1_cand_list[1] <= `MV_AMVP_CAND_B;
                            end
                        end
                        else begin
                            mvp_l1_idx <= 1;       
                            mvpl1_cand_list[0] <= `MV_AMVP_CAND_B;
                        end
                    end
                end
                STATE_MV_DERIVE_MV_AVAILABLE_MERGE_9: begin
                    is_available_b1 <= available_flag_to_mv_buffer;
                    if(is_available_a1) begin
                        is_available_a1 <= cand_mv_field_pred_flag_l0 | cand_mv_field_pred_flag_l1;
                    end
                    a1_mv_field_pred_flag_l0    <= cand_mv_field_pred_flag_l0;
                    a1_mv_field_pred_flag_l1    <= cand_mv_field_pred_flag_l1;
                    a1_mv_field_ref_idx_l0      <= cand_mv_field_ref_idx_l0;
                    a1_mv_field_ref_idx_l1      <= cand_mv_field_ref_idx_l1;
                    a1_mv_field_mv_x_l0         <= cand_mv_field_mv_x_l0;
                    a1_mv_field_mv_y_l0         <= cand_mv_field_mv_y_l0;
                    a1_mv_field_mv_x_l1         <= cand_mv_field_mv_x_l1;
                    a1_mv_field_mv_y_l1         <= cand_mv_field_mv_y_l1;
                end
                STATE_MV_DERIVE_MV_AVAILABLE_MERGE_10: begin
                    is_available_b0 <= available_flag_to_mv_buffer; 
                    if(is_available_b1) begin
                        is_available_b1 <= cand_mv_field_pred_flag_l0 | cand_mv_field_pred_flag_l1;
                    end
                    if(is_available_a1 == 1) begin
                        merge_array_idx <= (merge_array_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);
                        merge_cand_list[merge_array_idx] <= `MV_MERGE_CAND_A1;
                        avaialble_flag_a1 <= 1;
                    end                
                    else begin
                        a1_mv_field_pred_flag_l0 <= 0;
                        a1_mv_field_pred_flag_l1 <= 0;
                    end
                    b1_mv_field_pred_flag_l0    <= cand_mv_field_pred_flag_l0;
                    b1_mv_field_pred_flag_l1    <= cand_mv_field_pred_flag_l1;
                    b1_mv_field_ref_idx_l0      <= cand_mv_field_ref_idx_l0;
                    b1_mv_field_ref_idx_l1      <= cand_mv_field_ref_idx_l1;
                    b1_mv_field_mv_x_l0         <= cand_mv_field_mv_x_l0;
                    b1_mv_field_mv_y_l0         <= cand_mv_field_mv_y_l0;
                    b1_mv_field_mv_x_l1         <= cand_mv_field_mv_x_l1;
                    b1_mv_field_mv_y_l1         <= cand_mv_field_mv_y_l1;
                    
                end
                STATE_MV_DERIVE_MV_AVAILABLE_MERGE_11: begin
                    is_available_a0 <= available_flag_to_mv_buffer;   
                    ref_poc_a1_l0 <= ref_pic_list0_poc_data_out;
                    ref_poc_a1_l1 <= ref_pic_list1_poc_data_out;
                    ref_dpb_idx_a1_l0   <= ref_pic_list0_idx_data_out;
                    ref_dpb_idx_a1_l1   <= ref_pic_list1_idx_data_out;
                    if(is_available_b0) begin
                        is_available_b0 <= cand_mv_field_pred_flag_l0 | cand_mv_field_pred_flag_l1;
                    end                  
                    b0_mv_field_pred_flag_l0    <= cand_mv_field_pred_flag_l0;
                    b0_mv_field_pred_flag_l1    <= cand_mv_field_pred_flag_l1;
                    b0_mv_field_ref_idx_l0      <= cand_mv_field_ref_idx_l0;
                    b0_mv_field_ref_idx_l1      <= cand_mv_field_ref_idx_l1;
                    b0_mv_field_mv_x_l0         <= cand_mv_field_mv_x_l0;
                    b0_mv_field_mv_y_l0         <= cand_mv_field_mv_y_l0;
                    b0_mv_field_mv_x_l1         <= cand_mv_field_mv_x_l1;
                    b0_mv_field_mv_y_l1         <= cand_mv_field_mv_y_l1;
                end
                STATE_MV_DERIVE_MV_AVAILABLE_MERGE_12: begin
                    is_available_b2 <= available_flag_to_mv_buffer;
                    ref_poc_b1_l0 <= ref_pic_list0_poc_data_out;
                    ref_poc_b1_l1 <= ref_pic_list1_poc_data_out;
                    ref_dpb_idx_b1_l0   <= ref_pic_list0_idx_data_out;
                    ref_dpb_idx_b1_l1   <= ref_pic_list1_idx_data_out;
                    if(is_available_a0) begin
                        is_available_a0 <= cand_mv_field_pred_flag_l0 | cand_mv_field_pred_flag_l1;
                    end
                    if(is_available_b1 == 1) begin
                        if(is_available_a1) begin
                            if(compare_a1b1_curr_ref_idx & compare_a1b1_curr_mv) begin
                                b1_mv_field_pred_flag_l0    <= 0;
                                b1_mv_field_pred_flag_l1    <= 0;

                            end
                            else begin
                                merge_array_idx                     <= (merge_array_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);
                                merge_cand_list[merge_array_idx]    <= `MV_MERGE_CAND_B1;    
                                avaialble_flag_b1                   <= 1;
                            end
                        end
                        else begin
                            merge_array_idx <= (merge_array_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);
                            merge_cand_list[merge_array_idx] <= `MV_MERGE_CAND_B1;                     
                            avaialble_flag_b1                   <= 1;
                        end
                    end
                    else begin
                        b1_mv_field_pred_flag_l0    <= 0;
                        b1_mv_field_pred_flag_l1    <= 0;                        
                    end
                    a0_mv_field_pred_flag_l0    <= cand_mv_field_pred_flag_l0;
                    a0_mv_field_pred_flag_l1    <= cand_mv_field_pred_flag_l1;
                    a0_mv_field_ref_idx_l0      <= cand_mv_field_ref_idx_l0;
                    a0_mv_field_ref_idx_l1      <= cand_mv_field_ref_idx_l1;
                    a0_mv_field_mv_x_l0         <= cand_mv_field_mv_x_l0;
                    a0_mv_field_mv_y_l0         <= cand_mv_field_mv_y_l0;
                    a0_mv_field_mv_x_l1         <= cand_mv_field_mv_x_l1;
                    a0_mv_field_mv_y_l1         <= cand_mv_field_mv_y_l1;
                    
                end
                STATE_MV_DERIVE_MV_AVAILABLE_MERGE_13: begin
                    ref_poc_b0_l0 <= ref_pic_list0_poc_data_out;
                    ref_poc_b0_l1 <= ref_pic_list1_poc_data_out;
                    ref_dpb_idx_b0_l0   <= ref_pic_list0_idx_data_out;
                    ref_dpb_idx_b0_l1   <= ref_pic_list1_idx_data_out;                    
                    if(is_available_b0 == 1) begin
                        if(is_available_b1) begin
                            if(compare_b1b0_curr_ref_idx & compare_b1b0_curr_mv) begin
                                b0_mv_field_pred_flag_l0    <= 0;
                                b0_mv_field_pred_flag_l1    <= 0;                                
                            end
                            else begin
                                merge_array_idx <= (merge_array_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);
                                merge_cand_list[merge_array_idx] <= `MV_MERGE_CAND_B0; 
                                avaialble_flag_b0 <= 1;
                            end
                        end
                        else begin
                            merge_array_idx <= (merge_array_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);
                            merge_cand_list[merge_array_idx] <= `MV_MERGE_CAND_B0;                          
                            avaialble_flag_b0 <= 1;
                        end
                    end
                    else begin
                        b0_mv_field_pred_flag_l0    <= 0;
                        b0_mv_field_pred_flag_l1    <= 0;                      
                    end
                    if(is_available_b2) begin
                        is_available_b2 <= cand_mv_field_pred_flag_l0 | cand_mv_field_pred_flag_l1;
                    end
                    b2_mv_field_pred_flag_l0    <= cand_mv_field_pred_flag_l0;
                    b2_mv_field_pred_flag_l1    <= cand_mv_field_pred_flag_l1;
                    b2_mv_field_ref_idx_l0      <= cand_mv_field_ref_idx_l0;
                    b2_mv_field_ref_idx_l1      <= cand_mv_field_ref_idx_l1;
                    b2_mv_field_mv_x_l0         <= cand_mv_field_mv_x_l0;
                    b2_mv_field_mv_y_l0         <= cand_mv_field_mv_y_l0;
                    b2_mv_field_mv_x_l1         <= cand_mv_field_mv_x_l1;
                    b2_mv_field_mv_y_l1         <= cand_mv_field_mv_y_l1;                    
                end   
                STATE_MV_DERIVE_MV_PROCESS_MERGE_14: begin
                    
                    ref_poc_a0_l0       <= ref_pic_list0_poc_data_out;
                    ref_poc_a0_l1       <= ref_pic_list1_poc_data_out;
                    ref_dpb_idx_a0_l0   <= ref_pic_list0_idx_data_out;
                    ref_dpb_idx_a0_l1   <= ref_pic_list1_idx_data_out;
                    if(is_available_a0 == 1) begin
                        if(is_available_a1) begin
                            if(compare_a0a1_curr_ref_idx & compare_a0a1_curr_mv) begin
                                a0_mv_field_pred_flag_l0    <= 0;
                                a0_mv_field_pred_flag_l1    <= 0;                                
                            end
                            else begin
                                merge_array_idx <= (merge_array_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);
                                merge_cand_list[merge_array_idx] <= `MV_MERGE_CAND_A0;    
                                avaialble_flag_a0 <= 1;
                            end
                        end
                        else begin
                            merge_array_idx <= (merge_array_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);
                            merge_cand_list[merge_array_idx] <= `MV_MERGE_CAND_A0;                          
                            avaialble_flag_a0 <= 1;
                        end
                    end 
                    else begin
                        a0_mv_field_pred_flag_l0    <= 0;
                        a0_mv_field_pred_flag_l1    <= 0;  
                    end
                end
                STATE_MV_DERIVE_MV_PROCESS_MERGE_15: begin
                    bi_pred_cand_idx <= 0;
                    ref_poc_b2_l0 <= ref_pic_list0_poc_data_out;
                    ref_poc_b2_l1 <= ref_pic_list1_poc_data_out;   
                    ref_dpb_idx_b2_l0   <= ref_pic_list0_idx_data_out;
                    ref_dpb_idx_b2_l1   <= ref_pic_list1_idx_data_out;
                    if(is_available_b2) begin
                        case({is_available_a1,is_available_b1})
                            2'b00: begin
                                if(avaialble_flag_4) begin
                                    b2_mv_field_pred_flag_l0    <= 0;
                                    b2_mv_field_pred_flag_l1    <= 0;                                  
                                end
                                else begin
                                    merge_array_idx <= (merge_array_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);
                                    merge_cand_list[merge_array_idx] <= `MV_MERGE_CAND_B2;    
                                    avaialble_flag_b2 <= 1;                                    
                                end
                            end
                            2'b10: begin
                                if((compare_b2a1_curr_ref_idx & compare_b2a1_curr_mv) | avaialble_flag_4) begin
                                    b2_mv_field_pred_flag_l0    <= 0;
                                    b2_mv_field_pred_flag_l1    <= 0;                                
                                end
                                
                                else begin
                                    merge_array_idx <= (merge_array_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);
                                    merge_cand_list[merge_array_idx] <= `MV_MERGE_CAND_B2;    
                                    avaialble_flag_b2 <= 1;
                                end                                
                            end
                            2'b01: begin
                                if((compare_b2b1_curr_ref_idx & compare_b2b1_curr_mv) | avaialble_flag_4) begin
                                    b2_mv_field_pred_flag_l0    <= 0;
                                    b2_mv_field_pred_flag_l1    <= 0;                                
                                end
                                else begin
                                    merge_array_idx <= (merge_array_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);
                                    merge_cand_list[merge_array_idx] <= `MV_MERGE_CAND_B2;    
                                    avaialble_flag_b2 <= 1;
                                end                             
                            end
                            2'b11: begin
                                if((compare_b2a1_curr_ref_idx & compare_b2a1_curr_mv) | (compare_b2b1_curr_ref_idx & compare_b2b1_curr_mv) | avaialble_flag_4) begin
                                    b2_mv_field_pred_flag_l0    <= 0;
                                    b2_mv_field_pred_flag_l1    <= 0;                                
                                end
                                else begin
                                    merge_array_idx <= (merge_array_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);
                                    merge_cand_list[merge_array_idx] <= `MV_MERGE_CAND_B2;    
                                    avaialble_flag_b2 <= 1;
                                end       
                            end
                        endcase        
                    end
                    else begin
                        b2_mv_field_pred_flag_l0    <= 0;
                        b2_mv_field_pred_flag_l1    <= 0;                        
                    end
                end
                STATE_MV_DERIVE_COL_MV_WAIT: begin
                    zero_ref_idx <= 0;
                    if(!merge_flag & col_mv_ready) begin
                        if(available_col_l0_flag && mvp_l0_cand_found == 0 ) begin
                            mvp_l0_idx <= (mvp_l0_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);     
                            mvpl0_cand_list[mvp_l0_idx] <= `MV_AMVP_CAND_COL;
                        end
                        if(slice_type == `SLICE_B) begin
                            if(available_col_l1_flag && mvp_l1_cand_found == 0 ) begin
                                mvp_l1_idx <= (mvp_l1_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH); 
                                mvpl1_cand_list[mvp_l1_idx] <= `MV_AMVP_CAND_COL;                                
                            end
                        end
                    end
                    else if(merge_flag & col_mv_ready) begin
                        if(available_col_l0_flag | available_col_l1_flag && merge_cand_found == 0) begin
                            merge_array_idx <= (merge_array_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);
                            merge_cand_list[merge_array_idx] <= `MV_MERGE_CAND_COL;    
                        end
                    end
                end
                STATE_MV_DERIVE_BI_PRED_CANDS3: begin
                    bi_pred_cand_valid_d <= bi_pred_cand_valid;
                end
                STATE_MV_DERIVE_BI_PRED_CANDS4: begin
                    if(bi_pred_cand_valid_d[bi_pred_cand_idx] == 1) begin
                        merge_array_idx <= (merge_array_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);
                        merge_cand_list[merge_array_idx] <= `MV_MERGE_CAND_BI; 
                        bi_pred_cand_idx_chosen <= bi_pred_cand_idx;
                    end
                    bi_pred_cand_idx <= (bi_pred_cand_idx + 1)%(1<<DPB_ADDR_WIDTH);
                end
                STATE_MV_DERIVE_ZERO_MERGE: begin
                    if(!(zero_ref_idx == inter_maxnummergecand || merge_array_idx == merge_idx)) begin
                        zero_ref_idx <= (zero_ref_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH); 
                    end        
                    merge_array_idx <= (merge_array_idx + 1)%(1<<MAX_MERGE_CAND_WIDTH);
                    merge_cand_list[merge_array_idx] <= `MV_MERGE_CAND_ZERO;    
                end
                STATE_MV_DERIVE_ADD_ZEROS_AMVP: begin
                    if(mvp_l0_cand_found == 0) begin
                        mvp_l0_idx <= mvp_l0_idx + 1; 
                        mvpl0_cand_list[mvp_l0_idx] <= `MV_AMVP_CAND_ZERO;   
                    end
                    if(mvp_l1_cand_found == 0) begin
                        mvp_l1_idx <= mvp_l1_idx + 1; 
                        mvpl1_cand_list[mvp_l1_idx] <= `MV_AMVP_CAND_ZERO; 
                    end               
                
                end
                STATE_MV_DERIVE_SET_DUMMY_INTRA_MV: begin
                    xx_pb <= cb_xx;
                    yy_pb <= cb_yy; 
                    hh_pb <= hh_pb_temp;
                    ww_pb <= ww_pb_temp;  
                    xx_pb_end <= (cb_xx + cb_size)%(1<<(X11_ADDR_WDTH));
                    yy_pb_end <= (cb_yy + cb_size)%(1<<(X11_ADDR_WDTH));  
                    current_mv_field_valid <= 0;
                end
                STATE_MV_DERIVE_MVD_ADD_AMVP: begin
                    
                end
                STATE_MV_DERIVE_SET_INTER_MV: begin
                    current_mv_field_valid <= 0;
                end
                STATE_MV_DERIVE_SET_PRE_INTRA_MV: begin
                    current_mv_field_valid <= 0;                    
                end                
                STATE_MV_DERIVE_DONE: begin
                    current_mv_field_valid <= 1;
                    xx_pb_prev <= xx_pb;
                    yy_pb_prev <= yy_pb;
                    hh_pb_prev <= hh_pb;
                    ww_pb_prev <= ww_pb;     
                    if(pu_mv_buffer_done) begin
                        pb_part_idx <= (pb_part_idx + 1)%(1<<PART_IDX_WIDTH);
                    end
                end
                STATE_MV_DERIVE_CTU_DONE_WAIT: begin
                    current_mv_field_valid <= 0;
                end
                STATE_MV_DERIVE_WAIT_BEFOR_CONFIG_UPDATE: begin
                    current_mv_field_valid <= 0;
                end
            endcase
        end
    end


always@(posedge clk) begin
    if(reset) begin
        mv_derive_state <= STATE_MV_DERIVE_CONFIG_UPDATE;
    end
    else begin
        mv_derive_state <= mv_derive_next_state;
    end
end    
    
endmodule    