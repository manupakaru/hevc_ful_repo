`timescale 1ns / 1ps
module luma_8_tap_filter(
    clk,
    filt_type,
    level_in,
    a_ref_pixel_in ,
    b_ref_pixel_in ,
    c_ref_pixel_in ,
    d_ref_pixel_in ,
    e_ref_pixel_in ,
    f_ref_pixel_in ,
    g_ref_pixel_in ,
    h_ref_pixel_in ,
    pred_pixel_out
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"
    
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
	parameter IN_PIXEL_WIDTH = FILTER_PIXEL_WIDTH;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam SHIFT2 = 3'd6;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input filt_type;
    input level_in;
    input [IN_PIXEL_WIDTH -1: 0] a_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] b_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] c_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] d_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] e_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] f_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] g_ref_pixel_in;
    input [IN_PIXEL_WIDTH -1: 0] h_ref_pixel_in;
    output reg [FILTER_PIXEL_WIDTH -1:0]pred_pixel_out;

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module

reg signed [IN_PIXEL_WIDTH + 6 -1: 0] com_1_level1_adder;
reg signed [IN_PIXEL_WIDTH + 6 -1: 0] com_2_level1_adder;
reg signed [IN_PIXEL_WIDTH + 6 -1: 0] com_3_level1_adder;
reg signed [IN_PIXEL_WIDTH + 6 -1: 0] com_1_level2_adder;
reg signed [IN_PIXEL_WIDTH + 6 -1: 0] com_2_level2_adder;
reg signed [IN_PIXEL_WIDTH + 6 -1: 0] com_1_level3_adder;

reg signed [IN_PIXEL_WIDTH +6 -1: 0] type1_1_level1_adder;
reg signed [IN_PIXEL_WIDTH +6 -1: 0] type1_2_level1_adder;
reg signed [IN_PIXEL_WIDTH +6 -1: 0] type1_3_level1_adder;
reg signed [IN_PIXEL_WIDTH +6 -1: 0] type1_1_level2_adder;
reg signed [IN_PIXEL_WIDTH +6 -1: 0] type1_1_level3_adder;


reg signed [IN_PIXEL_WIDTH +6 -1: 0] type2_1_level1_adder;
reg signed [IN_PIXEL_WIDTH +6 -1: 0] type2_2_level1_adder;
reg signed [IN_PIXEL_WIDTH +6 -1: 0] type2_3_level1_adder;
reg signed [IN_PIXEL_WIDTH +6 -1: 0] type2_1_level2_adder;
reg signed [IN_PIXEL_WIDTH +6 -1: 0] type2_2_level2_adder;
reg signed [IN_PIXEL_WIDTH +6 -1: 0] type2_1_level3_adder;

reg filt_type_d;
reg level_in_d;

always@(posedge clk) begin
    filt_type_d <= filt_type;
    level_in_d <= level_in;
end


always@(*) begin
    type2_1_level1_adder = ((-$signed(f_ref_pixel_in))<<1) + ($signed(g_ref_pixel_in) << 2); 
    type2_2_level1_adder = (($signed(c_ref_pixel_in)) + ($signed(h_ref_pixel_in)));   //+1
    type2_3_level1_adder = $signed(e_ref_pixel_in) + (-$signed(f_ref_pixel_in));  //+1
    
    
end


always@(posedge clk) begin
    type2_1_level2_adder <= (-type2_2_level1_adder) + ($signed(e_ref_pixel_in)<<5);  
    type2_2_level2_adder <= type2_1_level1_adder + (type2_3_level1_adder<<3);
end

always@(*) begin
    type2_1_level3_adder = type2_1_level2_adder + type2_2_level2_adder;
end


always@(*) begin
    
    type1_2_level1_adder = $signed(e_ref_pixel_in) + ($signed(d_ref_pixel_in) << 1);
    type1_3_level1_adder = ((-$signed(f_ref_pixel_in)) << 2) + $signed(g_ref_pixel_in);

    
end

always@(posedge clk) begin
    type1_1_level2_adder <= (type1_2_level1_adder) + (type1_3_level1_adder);
    type1_1_level1_adder <= $signed(d_ref_pixel_in ) + $signed(e_ref_pixel_in);
end

always@(*) begin
    type1_1_level3_adder = (type1_1_level2_adder) + (type1_1_level1_adder << 4);
end



always@(*) begin    
    com_1_level1_adder = $signed(a_ref_pixel_in) + $signed(f_ref_pixel_in);
    com_2_level1_adder = (-$signed(c_ref_pixel_in)) + $signed(d_ref_pixel_in);
    com_3_level1_adder =  ((-$signed(c_ref_pixel_in))<<1) + ($signed(b_ref_pixel_in)<<2);
    
    // com_1_level2_adder = (-com_1_level1_adder) + (com_2_level1_adder<<3);
    // com_2_level2_adder = (d_ref_pixel_in<<5) + (com_3_level1_adder);
    
    
end

always@(posedge clk) begin
    com_1_level2_adder <= (-com_1_level1_adder) + (com_2_level1_adder<<3);
    com_2_level2_adder <= ($signed(d_ref_pixel_in)<<5) + (com_3_level1_adder);
end


always@(*) begin
    com_1_level3_adder = com_1_level2_adder + com_2_level2_adder;
end


always@(posedge clk) begin
    if(filt_type_d) begin
        if(level_in_d) begin
            pred_pixel_out <= (com_1_level3_adder + type2_1_level3_adder)>>SHIFT2; 
        end
        else begin
            pred_pixel_out <= (com_1_level3_adder + type2_1_level3_adder);
        end
    end
    else begin
        if (level_in_d) begin
            pred_pixel_out <= (com_1_level3_adder + type1_1_level3_adder)>>SHIFT2;
        end
        else begin
            pred_pixel_out <= com_1_level3_adder + type1_1_level3_adder;
        end
    end
    
end

endmodule 