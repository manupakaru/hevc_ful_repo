`timescale 1ns / 1ps
module temporal_pred_engine(
    clk,
    reset,
    enable,
    is_new_pic_col_ref_idx,
    current_poc,
    col_pic,
	collocated_dpb_idx,
    axi_col_mv_frame_offet_in,
    current_dpb_idx,    
    col_x_addr,
    col_y_addr,    
    col_addr_valid,
    col_br_boundary_ok,
    collocated_from_l0_flag,  
    // ref_idx_l0_col,
    // ref_idx_l1_col,
    cur_poc_l0_diff,    
    cur_poc_l1_diff,    
    cur_poc_l0_diff_clipped,    
    cur_poc_l1_diff_clipped,    
    
    axi_ctu_read_done_out,
    new_ctu_start_in,
    ctb_xx,
    ctb_yy,    
    
    slice_type,
    slice_temporal_mvp_enabled_flag,
    // num_ref_idx_l0_active,
    // num_ref_idx_l1_active,
    
    
    col_mv_ready,
    available_col_l0_flag                  ,
    available_col_l1_flag                  ,
    col_mv_field_mv_x_l0_to_inter          ,    
    col_mv_field_mv_y_l0_to_inter          ,    
    col_mv_field_mv_x_l1_to_inter          ,    
    col_mv_field_mv_y_l1_to_inter          ,        
    
    
    from_top_ref_pic_list_poc_data_in,
    from_top_ref_pic_list0_poc_wr_en  ,
    from_top_ref_pic_list1_poc_wr_en  ,
    from_top_ref_pic_list0_poc_addr  ,
    from_top_ref_pic_list1_poc_addr  ,    
    from_top_ref_pic_list_valid     ,

    mv_pref_axi_araddr              ,
    mv_pref_axi_arlen               ,
    mv_pref_axi_arsize              ,
    mv_pref_axi_arburst             ,
    mv_pref_axi_arprot              ,
    mv_pref_axi_arvalid             ,
    mv_pref_axi_arready             ,

    mv_pref_axi_rdata               ,
    mv_pref_axi_rresp               ,
    mv_pref_axi_rlast               ,
    mv_pref_axi_rvalid              ,
    mv_pref_axi_rready              ,

    mv_pref_axi_arlock              ,
    mv_pref_axi_arid                ,
    mv_pref_axi_arcache             
    
);

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"
    `include "../sim/inter_axi_def.v" // always define below pred_def

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    `define L0 0
    `define L1 1
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam STATE_TEMPO_IDLE                 = 0;
    localparam STATE_REF_PIC_COUNT              = 1;
    localparam STATE_TEMPO_GET_CTR_MV_WAIT      = 2;
    localparam STATE_TEMPO_GET_CTR_MV           = 3;
    localparam STATE_TEMPO_GET_BR_MV            = 4;
    localparam STATE_TEMPO_GET_BR_MV_CHECK      = 5;
    localparam STATE_TEMP0R_DIFF_PIC_COUNT      = 6;
    localparam STATE_TEMPOR_POC_DIF_COMPAR      = 7;
    localparam STATE_TEMPOR_TX_WAIT             = 8;
    localparam STATE_TEMP0R_DIST_SCALE          = 9;
    localparam STATE_TEMP0R_DIST_SCALE_CLIP     = 10;
    localparam STATE_TEMP0R_DIST_INTO_MV        = 11;
    localparam STATE_TEMP0R_MV_CLIP             = 12;
    localparam STATE_TEMP0R_DIST_SCALE_DELAY    = 13;
    localparam STATE_TEMP0R_DIST_SCALE_WAIT    = 14;
    
    
    localparam                          CLIPPED_WIDTH                       = 8;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                                           clk;
    input                                                                           reset;
    input                                                                           enable;
    input                                                                           is_new_pic_col_ref_idx;
    input   signed [POC_WIDTH -1:0]                                                 current_poc;
    input   signed [POC_WIDTH -1:0]                                                 col_pic;
    input   [DPB_ADDR_WIDTH -1: 0]                                                  current_dpb_idx;
    input   [DPB_ADDR_WIDTH -1: 0]                  								collocated_dpb_idx;
    input   [AXI_ADDR_WDTH -1: 0]                                                   axi_col_mv_frame_offet_in;    
    input       [(CTB_SIZE_WIDTH - LOG2_MIN_COLLOCATE_SIZE) -1 + 1:0]                   col_x_addr;
    input       [(CTB_SIZE_WIDTH - LOG2_MIN_COLLOCATE_SIZE) -1	  :0]                   col_y_addr;    
    input                                                                           col_addr_valid;
    input                                                                           col_br_boundary_ok;
    input                                                                           collocated_from_l0_flag;  
    // input           [DPB_ADDR_WIDTH -1: 0]                                          ref_idx_l0_col;
    // input           [DPB_ADDR_WIDTH -1: 0]                                          ref_idx_l1_col;
    input signed    [POC_WIDTH -1:0]                                                cur_poc_l0_diff;    
    input signed    [POC_WIDTH -1:0]                                                cur_poc_l1_diff;    
    input signed    [CLIPPED_WIDTH -1:0]                                                        cur_poc_l0_diff_clipped;    
    input signed    [CLIPPED_WIDTH -1:0]                                                        cur_poc_l1_diff_clipped;    
    
    output                                                                          axi_ctu_read_done_out;
    input                                                                           new_ctu_start_in;
    input   [X11_ADDR_WDTH - CTB_SIZE_WIDTH -1:0]                                   ctb_xx;
    input   [Y11_ADDR_WDTH - CTB_SIZE_WIDTH -1:0]                                   ctb_yy;    
    
    input         [SLICE_TYPE_WIDTH -1:0]                                           slice_type;
    input                                                                           slice_temporal_mvp_enabled_flag;
    // input         [NUM_REF_IDX_L0_MINUS1_WIDTH - 1:0]                               num_ref_idx_l0_active;
    // input         [NUM_REF_IDX_L1_MINUS1_WIDTH - 1:0]                               num_ref_idx_l1_active;


    output reg                                                                      col_mv_ready;
    output reg                                                                      available_col_l0_flag                  ;
    output reg                                                                      available_col_l1_flag                  ;
    output reg signed [MVD_WIDTH -1:0]                                              col_mv_field_mv_x_l0_to_inter          ;    
    output reg signed [MVD_WIDTH -1:0]                                              col_mv_field_mv_y_l0_to_inter          ;    
    output reg signed [MVD_WIDTH -1:0]                                              col_mv_field_mv_x_l1_to_inter          ;    
    output reg signed [MVD_WIDTH -1:0]                                              col_mv_field_mv_y_l1_to_inter          ;            
    
    
    input signed [REF_PIC_LIST_POC_DATA_WIDTH-1:0]                                  from_top_ref_pic_list_poc_data_in;
    input                                                                           from_top_ref_pic_list0_poc_wr_en  ;
    input                                                                           from_top_ref_pic_list1_poc_wr_en  ;
    input [NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]                                         from_top_ref_pic_list0_poc_addr  ;
    input [NUM_REF_IDX_L0_MINUS1_WIDTH-1:0]                                         from_top_ref_pic_list1_poc_addr  ;    
    input                                                                           from_top_ref_pic_list_valid;
       
    
    output	    [AXI_ADDR_WDTH-1:0]		            mv_pref_axi_araddr  ;
    output wire [7:0]					            mv_pref_axi_arlen   ;
    output wire	[2:0]					            mv_pref_axi_arsize  ;
    output wire [1:0]					            mv_pref_axi_arburst ;
    output wire [2:0]					            mv_pref_axi_arprot  ;
    output 	   						                mv_pref_axi_arvalid ;
    input 								            mv_pref_axi_arready ;
                
    input		[MV_COL_AXI_DATA_WIDTH-1:0]		    mv_pref_axi_rdata   ;
    input		[1:0]					            mv_pref_axi_rresp   ;
    input 								            mv_pref_axi_rlast   ;
    input								            mv_pref_axi_rvalid  ;
    output 						                    mv_pref_axi_rready  ;
    
    output                        	                mv_pref_axi_arlock  ;
	output                                          mv_pref_axi_arid    ;    
    output      [3:0]                               mv_pref_axi_arcache ;        
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    integer state_temporal;

    
    
    wire                           col_mv_field_pred_flag_l0_out;    
    wire                           col_mv_field_pred_flag_l1_out;    
    wire [REF_IDX_LX_WIDTH -1:0]   col_mv_field_ref_idx_l0_out;
    wire [REF_IDX_LX_WIDTH -1:0]   col_mv_field_ref_idx_l1_out;    
    wire signed [MVD_WIDTH -1:0]          col_mv_field_mv_x_l0_out;    
    wire signed [MVD_WIDTH -1:0]          col_mv_field_mv_y_l0_out;    
    wire signed [MVD_WIDTH -1:0]          col_mv_field_mv_x_l1_out;    
    wire signed [MVD_WIDTH -1:0]          col_mv_field_mv_y_l1_out;    
        
    // reg                           col_br_mv_field_pred_flag_l0;
    // reg                           col_br_mv_field_pred_flag_l1;
    // reg [REF_IDX_LX_WIDTH -1:0]   col_br_mv_field_ref_idx_l0;
    // reg [REF_IDX_LX_WIDTH -1:0]   col_br_mv_field_ref_idx_l1;
    // reg [MVD_WIDTH -1:0]          col_br_mv_field_mv_x_l0;
    // reg [MVD_WIDTH -1:0]          col_br_mv_field_mv_y_l0;
    // reg [MVD_WIDTH -1:0]          col_br_mv_field_mv_x_l1;
    // reg [MVD_WIDTH -1:0]          col_br_mv_field_mv_y_l1;

    reg                           col_ctr_mv_field_pred_flag_l0;
    reg                           col_ctr_mv_field_pred_flag_l1;
    reg [REF_IDX_LX_WIDTH -1:0]   col_ctr_mv_field_ref_idx_l0;
    reg [REF_IDX_LX_WIDTH -1:0]   col_ctr_mv_field_ref_idx_l1;
    reg signed [MVD_WIDTH -1:0]          col_ctr_mv_field_mv_x_l0;
    reg signed [MVD_WIDTH -1:0]          col_ctr_mv_field_mv_y_l0;
    reg signed [MVD_WIDTH -1:0]          col_ctr_mv_field_mv_x_l1;
    reg signed [MVD_WIDTH -1:0]          col_ctr_mv_field_mv_y_l1;    
    
    // reg list_l0_col;
    // reg list_l1_col;
    
    reg [REF_PIC_LIST_POC_DATA_WIDTH-1:0]     from_top_ref_pic_list_poc_d;
    reg                                       from_top_ref_pic_list_poc_wr_en_d;  
    reg                                       from_top_ref_pic_list_valid_d;    
    reg signed  [POC_WIDTH -1:0]                                                current_poc_d;
    
    reg                                         is_refpics_all_old;
    
    
    reg                           col_mv_field_pred_flag_l0_to_inter     ;    
    reg                           col_mv_field_pred_flag_l1_to_inter     ;    
    reg [REF_IDX_LX_WIDTH -1:0]   col_mv_field_ref_idx_l0_to_inter       ;
    reg [REF_IDX_LX_WIDTH -1:0]   col_mv_field_ref_idx_l1_to_inter       ;    
    
    wire signed [POC_WIDTH -1:0]   ref_idx_l0_col_poc_out;
    wire signed [POC_WIDTH -1:0]   ref_idx_l1_col_poc_out;
    
    reg  signed [POC_WIDTH -1:0]    col_l0_poc_diff;
    wire signed [POC_WIDTH -1:0]    col_l0_poc_diff_wire = col_pic - ref_idx_l0_col_poc_out;
    reg  signed [POC_WIDTH -1:0]    col_l1_poc_diff;
    wire signed [POC_WIDTH -1:0]    col_l1_poc_diff_wire = col_pic - ref_idx_l1_col_poc_out;
    
     
    // reg signed      [POC_WIDTH -1:0]                                            cur_poc_l0_diff_wire;   
    // reg signed      [POC_WIDTH -1:0]                                            cur_poc_l1_diff_wire;   

    wire  signed  [TX_WIDTH -1:0]                               tx_l0;
    reg   signed  [TX_WIDTH + CLIPPED_WIDTH -1:0]               distscale_factor_l0;
    wire  signed  [TX_WIDTH -1:0]                               tx_l1;
    reg   signed  [TX_WIDTH + CLIPPED_WIDTH -1:0]               distscale_factor_l1;
    reg   signed  [DIST_SCALE_WIDTH -1:0]                       distscale_factor_l0_clipped;
    reg   signed  [DIST_SCALE_WIDTH -1:0]                       distscale_factor_l1_clipped;
    wire                                                        tx_enable = slice_temporal_mvp_enabled_flag;  

    reg  signed [POC_WIDTH -1: 0]                                   tbl0;    
    reg  signed [CLIPPED_WIDTH -1: 0]                               tbl0_clipped;    
    reg  signed [CLIPPED_WIDTH -1: 0]                               tdl0_clipped;    
    reg  signed [POC_WIDTH -1: 0]                                   tbl1;    
    reg  signed [CLIPPED_WIDTH -1: 0]                               tbl1_clipped; 
    reg  signed [CLIPPED_WIDTH -1: 0]                               tdl1_clipped;  
        
    reg col_br_boundary_ok_d;
    reg signed   [29 -1:0]        dist_scale_into_mv_l0_x;
    reg signed   [29 -1:0]        dist_scale_into_mv_l0_y;
    reg signed   [29 -1:0]        dist_scale_into_mv_l1_x;
    reg signed   [29 -1:0]        dist_scale_into_mv_l1_y;
    
    reg signed   [29 -1:0]        dist_scale_into_mv_l0_x_d;
    reg signed   [29 -1:0]        dist_scale_into_mv_l0_y_d;
    reg signed   [29 -1:0]        dist_scale_into_mv_l1_x_d;
    reg signed   [29 -1:0]        dist_scale_into_mv_l1_y_d;  

	reg signed [21 - 1:0]         new_mv_l0_x;// = (sign_dist_scale_into_mv_l0_x)? (abs_dist_scale_into_mv_l0_x_plus_127>>8): -1*(abs_dist_scale_into_mv_l0_x_plus_127>>8);
    reg signed [21 - 1:0]         new_mv_l0_y;// = (sign_dist_scale_into_mv_l0_y)? (abs_dist_scale_into_mv_l0_y_plus_127>>8): -1*(abs_dist_scale_into_mv_l0_y_plus_127>>8);
    reg signed [21 - 1:0]         new_mv_l1_x;// = (sign_dist_scale_into_mv_l1_x)? (abs_dist_scale_into_mv_l1_x_plus_127>>8): -1*(abs_dist_scale_into_mv_l1_x_plus_127>>8);
    reg signed [21 - 1:0]         new_mv_l1_y;// = (sign_dist_scale_into_mv_l1_y)? (abs_dist_scale_into_mv_l1_y_plus_127>>8): -1*(abs_dist_scale_into_mv_l1_y_plus_127>>8);
   	
    //todo change this clip operation similar to temporal engine
    wire signed [MVD_WIDTH -1:0] new_mv_l0_x_clipped = (new_mv_l0_x > 32767) ?32767: ((new_mv_l0_x < -32768)? -32768 : new_mv_l0_x[MVD_WIDTH-1:0]);
    wire signed [MVD_WIDTH -1:0] new_mv_l0_y_clipped = (new_mv_l0_y > 32767) ?32767: ((new_mv_l0_y < -32768)? -32768 : new_mv_l0_y[MVD_WIDTH-1:0]);
    wire signed [MVD_WIDTH -1:0] new_mv_l1_x_clipped = (new_mv_l1_x > 32767) ?32767: ((new_mv_l1_x < -32768)? -32768 : new_mv_l1_x[MVD_WIDTH-1:0]);
    wire signed [MVD_WIDTH -1:0] new_mv_l1_y_clipped = (new_mv_l1_y > 32767) ?32767: ((new_mv_l1_y < -32768)? -32768 : new_mv_l1_y[MVD_WIDTH-1:0]);
        
    wire                        sign_dist_scale_into_mv_l0_x = (dist_scale_into_mv_l0_x_d[29-1]==0) ? 1: 0;//(dist_scale_into_mv_l0_x_d>0) ? 1: 0;      // todo : hope checking one the MSB for sign of the value is ok
    wire                        sign_dist_scale_into_mv_l0_y = (dist_scale_into_mv_l0_y_d[29-1]==0) ? 1: 0;//(dist_scale_into_mv_l0_y_d>0) ? 1: 0;
    wire                        sign_dist_scale_into_mv_l1_x = (dist_scale_into_mv_l1_x_d[29-1]==0) ? 1: 0;//(dist_scale_into_mv_l1_x_d>0) ? 1: 0;
    wire                        sign_dist_scale_into_mv_l1_y = (dist_scale_into_mv_l1_y_d[29-1]==0) ? 1: 0;//(dist_scale_into_mv_l1_y_d>0) ? 1: 0;

    wire signed   [30 -1:0]        abs_dist_scale_into_mv_l0_x_plus_127 = sign_dist_scale_into_mv_l0_x ? (dist_scale_into_mv_l0_x_d + 127): ((~dist_scale_into_mv_l0_x_d) + 128);
    wire signed   [30 -1:0]        abs_dist_scale_into_mv_l0_y_plus_127 = sign_dist_scale_into_mv_l0_y ? (dist_scale_into_mv_l0_y_d + 127): ((~dist_scale_into_mv_l0_y_d) + 128);
    wire signed   [30 -1:0]        abs_dist_scale_into_mv_l1_x_plus_127 = sign_dist_scale_into_mv_l1_x ? (dist_scale_into_mv_l1_x_d + 127): ((~dist_scale_into_mv_l1_x_d) + 128);
    wire signed   [30 -1:0]        abs_dist_scale_into_mv_l1_y_plus_127 = sign_dist_scale_into_mv_l1_y ? (dist_scale_into_mv_l1_y_d + 127): ((~dist_scale_into_mv_l1_y_d) + 128);
    
                                        // in 2s complement negative value is equal to bit flip + 1
    // wire signed   [30 -1:0]        abs_dist_scale_into_mv_l0_x_plus_127 = abs_dist_scale_into_mv_l0_x + 127;
    // wire signed   [30 -1:0]        abs_dist_scale_into_mv_l0_y_plus_127 = abs_dist_scale_into_mv_l0_y + 127;
    // wire signed   [30 -1:0]        abs_dist_scale_into_mv_l1_x_plus_127 = abs_dist_scale_into_mv_l1_x + 127;
    // wire signed   [30 -1:0]        abs_dist_scale_into_mv_l1_y_plus_127 = abs_dist_scale_into_mv_l1_y + 127;    
    
    // wire signed   [22 -1:0]        abs_dist_scale_into_mv_l0_x_plus_127_shift8 = sign_dist_scale_into_mv_l0_x ? (abs_dist_scale_into_mv_l0_x_plus_127>>8) : (!(abs_dist_scale_into_mv_l0_x_plus_127>>8) + 1) ;
    // wire signed   [22 -1:0]        abs_dist_scale_into_mv_l0_y_plus_127_shift8 = sign_dist_scale_into_mv_l0_y ? (abs_dist_scale_into_mv_l0_y_plus_127>>8) : (!(abs_dist_scale_into_mv_l0_y_plus_127>>8) + 1) ;
    // wire signed   [22 -1:0]        abs_dist_scale_into_mv_l1_x_plus_127_shift8 = sign_dist_scale_into_mv_l1_x ? (abs_dist_scale_into_mv_l1_x_plus_127>>8) : (!(abs_dist_scale_into_mv_l1_x_plus_127>>8) + 1) ;
    // wire signed   [22 -1:0]        abs_dist_scale_into_mv_l1_y_plus_127_shift8 = sign_dist_scale_into_mv_l1_y ? (abs_dist_scale_into_mv_l1_y_plus_127>>8) : (!(abs_dist_scale_into_mv_l1_y_plus_127>>8) + 1) ;
    
    reg switched_to_ctr;
    wire from_top_ref_pic_list_poc_wr_en;
    

    
    
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
  assign from_top_ref_pic_list_poc_wr_en = (from_top_ref_pic_list0_poc_wr_en | from_top_ref_pic_list1_poc_wr_en);
always@(posedge clk) begin
    current_poc_d <= current_poc;
    col_br_boundary_ok_d <= col_br_boundary_ok;
    from_top_ref_pic_list_valid_d <= from_top_ref_pic_list_valid;
    from_top_ref_pic_list_poc_wr_en_d <= from_top_ref_pic_list_poc_wr_en;
    from_top_ref_pic_list_poc_d <= from_top_ref_pic_list_poc_data_in;
end

tx_division_engine tx_block_l0 (
    .clk(clk), 
    .enable(tx_enable), 
    .divisor(tdl0_clipped), 
    .quotient(tx_l0)
    );

tx_division_engine tx_block_l1 (
    .clk(clk), 
    .enable(tx_enable), 
    .divisor(tdl1_clipped), 
    .quotient(tx_l1)
    );



// Instantiate the module
prefetch_buffer mv_pref_buffer_block (
    .clk(clk), 
    .reset(reset), 
    .enable(enable & slice_temporal_mvp_enabled_flag),
	.collocated_dpb_idx(collocated_dpb_idx),	
    .axi_col_mv_frame_offet_in(axi_col_mv_frame_offet_in), 
    .is_new_pic_col_ref_idx(is_new_pic_col_ref_idx), 
    .current_dpb_idx(current_dpb_idx), 
    .from_top_ref_pic_list_poc_data_in(from_top_ref_pic_list_poc_data_in), 
    .from_top_ref_pic_list_poc_wr_en(from_top_ref_pic_list_poc_wr_en), 
    .from_top_ref_pic_list0_poc_addr(from_top_ref_pic_list0_poc_addr), 
    .from_top_ref_pic_list1_poc_addr(from_top_ref_pic_list1_poc_addr), 
    .ref_idx_l0_col(col_mv_field_ref_idx_l0_to_inter), 
    .ref_idx_l0_col_poc_out(ref_idx_l0_col_poc_out), 
    .ref_idx_l1_col(col_mv_field_ref_idx_l1_to_inter), 
    .ref_idx_l1_col_poc_out(ref_idx_l1_col_poc_out), 
    .col_x_addr(col_x_addr), 
    .col_y_addr(col_y_addr), 
    .col_mv_field_pred_flag_l0_out(col_mv_field_pred_flag_l0_out), 
    .col_mv_field_pred_flag_l1_out(col_mv_field_pred_flag_l1_out), 
    .col_mv_field_ref_idx_l0_out(col_mv_field_ref_idx_l0_out), 
    .col_mv_field_ref_idx_l1_out(col_mv_field_ref_idx_l1_out), 
    .col_mv_field_mv_x_l0_out(col_mv_field_mv_x_l0_out), 
    .col_mv_field_mv_x_l1_out(col_mv_field_mv_x_l1_out), 
    .col_mv_field_mv_y_l0_out(col_mv_field_mv_y_l0_out), 
    .col_mv_field_mv_y_l1_out(col_mv_field_mv_y_l1_out), 
    .mv_pref_axi_araddr(mv_pref_axi_araddr), 
    .mv_pref_axi_arlen(mv_pref_axi_arlen), 
    .mv_pref_axi_arsize(mv_pref_axi_arsize), 
    .mv_pref_axi_arburst(mv_pref_axi_arburst), 
    .mv_pref_axi_arprot(mv_pref_axi_arprot), 
    .mv_pref_axi_arvalid(mv_pref_axi_arvalid), 
    .mv_pref_axi_arready(mv_pref_axi_arready), 
    .mv_pref_axi_rdata(mv_pref_axi_rdata), 
    .mv_pref_axi_rresp(mv_pref_axi_rresp), 
    .mv_pref_axi_rlast(mv_pref_axi_rlast), 
    .mv_pref_axi_rvalid(mv_pref_axi_rvalid), 
    .mv_pref_axi_rready(mv_pref_axi_rready), 
    .mv_pref_axi_arlock(mv_pref_axi_arlock), 
    .mv_pref_axi_arid(mv_pref_axi_arid), 
    .mv_pref_axi_arcache(mv_pref_axi_arcache), 
    .x_ctb_addr(ctb_xx), 
    .y_ctb_addr(ctb_yy), 
    .new_ctu_start_in(new_ctu_start_in), 
    .axi_ctu_read_done_out(axi_ctu_read_done_out)
    );


    // always@(*) begin
        // case({list_l0_col,list_l1_col})
            // 2'b00: begin
                // cur_poc_l0_diff_wire = cur_poc_l0_diff;
                // cur_poc_l1_diff_wire = cur_poc_l0_diff;
            // end
            // 2'b11: begin
                // cur_poc_l0_diff_wire = cur_poc_l1_diff;    
                // cur_poc_l1_diff_wire = cur_poc_l1_diff;    
            // end
            // 2'b01: begin
                // cur_poc_l0_diff_wire = cur_poc_l0_diff;    
                // cur_poc_l1_diff_wire = cur_poc_l1_diff;                
            // end
            // default: begin
                // cur_poc_l0_diff_wire = {POC_WIDTH{1'bx}};    
                // cur_poc_l1_diff_wire = {POC_WIDTH{1'bx}};                       
            // end
        // endcase
    // end
    always@(*) begin
		dist_scale_into_mv_l0_x_d = dist_scale_into_mv_l0_x;  
		dist_scale_into_mv_l0_y_d = dist_scale_into_mv_l0_y; 				
		dist_scale_into_mv_l1_x_d = dist_scale_into_mv_l1_x;  
		dist_scale_into_mv_l1_y_d = dist_scale_into_mv_l1_y; 
	end

    always@(posedge clk) begin
        if (reset) begin
            state_temporal <= STATE_TEMPO_IDLE;
            switched_to_ctr <= 0;
            available_col_l0_flag <= 0;
            col_mv_ready <= 1;
            is_refpics_all_old <= 0;
            available_col_l0_flag <= 0;
            available_col_l1_flag <= 0;            
        end
        else begin
            case(state_temporal)
                STATE_TEMPO_IDLE: begin
                    if(col_addr_valid ) begin
                        if(slice_temporal_mvp_enabled_flag) begin
                            state_temporal <= STATE_TEMPO_GET_CTR_MV;
                            switched_to_ctr <= 0;
                            col_mv_ready <= 0;
                            available_col_l0_flag <= 0;
                            available_col_l1_flag <= 0;
                        end
                        else begin
                            available_col_l0_flag <= 0;
                            available_col_l1_flag <= 0;
                        end
                    end
                    else if(from_top_ref_pic_list_valid ==1) begin
                        state_temporal <= STATE_REF_PIC_COUNT;
                        is_refpics_all_old <= 1;
                        col_mv_ready <= 1;
                    end
                    else begin
                        col_mv_ready <= 1;
                    end
                end
                STATE_REF_PIC_COUNT: begin
                    if(from_top_ref_pic_list_valid_d) begin
                        if(from_top_ref_pic_list_poc_wr_en_d == 1) begin
                            if(current_poc_d < from_top_ref_pic_list_poc_d) begin       // assume that ref pic list0 and ref pic list1 consist of only shorterm pictures and that same set of pic present in both lists in different order
                                is_refpics_all_old <= 0;
                            end
                        end
                    end
                    else begin
                        state_temporal <= STATE_TEMPO_IDLE;
                    end
                end
                // STATE_TEMPO_GET_CTR_MV_WAIT: begin
                    // state_temporal <= STATE_TEMPO_GET_CTR_MV;
                // end
                STATE_TEMPO_GET_CTR_MV: begin
                     col_ctr_mv_field_pred_flag_l0  <= col_mv_field_pred_flag_l0_out;    
                     col_ctr_mv_field_pred_flag_l1  <= col_mv_field_pred_flag_l1_out;    
                     col_ctr_mv_field_ref_idx_l0    <= col_mv_field_ref_idx_l0_out;
                     col_ctr_mv_field_ref_idx_l1    <= col_mv_field_ref_idx_l1_out;    
                     col_ctr_mv_field_mv_x_l0       <= col_mv_field_mv_x_l0_out;    
                     col_ctr_mv_field_mv_y_l0       <= col_mv_field_mv_y_l0_out;    
                     col_ctr_mv_field_mv_x_l1       <= col_mv_field_mv_x_l1_out;    
                     col_ctr_mv_field_mv_y_l1       <= col_mv_field_mv_y_l1_out;    
                    state_temporal <= STATE_TEMPO_GET_BR_MV;
                end
                STATE_TEMPO_GET_BR_MV: begin
                    if(col_br_boundary_ok_d) begin
                        col_mv_field_pred_flag_l0_to_inter  <= col_mv_field_pred_flag_l0_out;    
                        col_mv_field_pred_flag_l1_to_inter  <= col_mv_field_pred_flag_l1_out;    
                        col_mv_field_mv_x_l0_to_inter       <= col_mv_field_mv_x_l0_out;    
                        col_mv_field_mv_y_l0_to_inter       <= col_mv_field_mv_y_l0_out;    
                        col_mv_field_mv_x_l1_to_inter       <= col_mv_field_mv_x_l1_out;    
                        col_mv_field_mv_y_l1_to_inter       <= col_mv_field_mv_y_l1_out;    
                    end
                    else begin
                        col_mv_field_pred_flag_l0_to_inter  <= col_ctr_mv_field_pred_flag_l0  ; 
                        col_mv_field_pred_flag_l1_to_inter  <= col_ctr_mv_field_pred_flag_l1  ; 
                        col_mv_field_mv_x_l0_to_inter       <= col_ctr_mv_field_mv_x_l0       ;
                        col_mv_field_mv_y_l0_to_inter       <= col_ctr_mv_field_mv_y_l0       ;
                        col_mv_field_mv_x_l1_to_inter       <= col_ctr_mv_field_mv_x_l1       ;
                        col_mv_field_mv_y_l1_to_inter       <= col_ctr_mv_field_mv_y_l1       ; 
                    end
                    state_temporal <= STATE_TEMPO_GET_BR_MV_CHECK;                
                end
                STATE_TEMPO_GET_BR_MV_CHECK: begin
                    case({col_mv_field_pred_flag_l0_to_inter,col_mv_field_pred_flag_l1_to_inter})
                        2'd0: begin
                            if(switched_to_ctr == 0) begin
                                switched_to_ctr <= 1;
                                state_temporal <= STATE_TEMPO_GET_BR_MV_CHECK;
                                
                            end
                            else begin
                                state_temporal <= STATE_TEMPO_IDLE;
                            end
                            col_mv_field_pred_flag_l0_to_inter <= col_ctr_mv_field_pred_flag_l0  ;
                            col_mv_field_pred_flag_l1_to_inter <= col_ctr_mv_field_pred_flag_l1  ;
                            col_mv_field_mv_x_l0_to_inter      <= col_ctr_mv_field_mv_x_l0       ;
                            col_mv_field_mv_y_l0_to_inter      <= col_ctr_mv_field_mv_y_l0       ;
                            col_mv_field_mv_x_l1_to_inter      <= col_ctr_mv_field_mv_x_l1       ;
                            col_mv_field_mv_y_l1_to_inter      <= col_ctr_mv_field_mv_y_l1       ;
                            available_col_l0_flag <= 0;
                            available_col_l1_flag <= 0;
                        end
                        2'b10: begin
                            // list_l0_col <= `L0;
                            // list_l1_col <= `L0;
							col_l0_poc_diff <= col_l0_poc_diff_wire;
							col_l1_poc_diff <= col_l0_poc_diff_wire;
                            col_mv_field_mv_x_l1_to_inter       <= col_mv_field_mv_x_l0_to_inter ;
                            col_mv_field_mv_y_l1_to_inter       <= col_mv_field_mv_y_l0_to_inter ;     
                            // available_col_l0_flag <= 1;
                            // available_col_l1_flag <= 1;                            
                            state_temporal <= STATE_TEMPOR_POC_DIF_COMPAR;
                        end
                        2'b01: begin
                            // list_l0_col <= `L1;
                            // list_l1_col <= `L1;  
							col_l0_poc_diff <= col_l1_poc_diff_wire;
							col_l1_poc_diff <= col_l1_poc_diff_wire;
                            col_mv_field_mv_x_l0_to_inter       <= col_mv_field_mv_x_l1_to_inter ;
                            col_mv_field_mv_y_l0_to_inter       <= col_mv_field_mv_y_l1_to_inter ; 
                            // available_col_l0_flag <= 1;
                            // available_col_l1_flag <= 1;                              
                            state_temporal <= STATE_TEMPOR_POC_DIF_COMPAR;
                        end
                        2'b11: begin
                            // available_col_l0_flag <= 1;
                            // available_col_l1_flag <= 1;                          
                            if(is_refpics_all_old ==1) begin// nothing to be done mv fileds and ref idx are already in place
                                // list_l0_col <= `L0;
                                // list_l1_col <= `L1;
								col_l0_poc_diff <= col_l0_poc_diff_wire;
								col_l1_poc_diff <= col_l1_poc_diff_wire; 								
                            end
                            else begin
                                if(collocated_from_l0_flag == 0) begin // Geethan 5/1/2014 why check collocated_from_l0_flag at this point?
                                    // list_l0_col <= `L0;
                                    // list_l1_col <= `L0;   
									col_l0_poc_diff <= col_l0_poc_diff_wire;
									col_l1_poc_diff <= col_l0_poc_diff_wire;
                                    col_mv_field_mv_x_l1_to_inter       <= col_mv_field_mv_x_l0_to_inter ;
                                    col_mv_field_mv_y_l1_to_inter       <= col_mv_field_mv_y_l0_to_inter ;                                       
                                end
                                else begin
                                    // list_l0_col <= `L1;
                                    // list_l1_col <= `L1;   
									col_l0_poc_diff <= col_l1_poc_diff_wire;
									col_l1_poc_diff <= col_l1_poc_diff_wire;									
                                    col_mv_field_mv_x_l0_to_inter       <= col_mv_field_mv_x_l1_to_inter ;
                                    col_mv_field_mv_y_l0_to_inter       <= col_mv_field_mv_y_l1_to_inter ;                                 
                                end
                            end
                            state_temporal <= STATE_TEMPOR_POC_DIF_COMPAR;
                        end
                    endcase
                end
                
                STATE_TEMPOR_POC_DIF_COMPAR: begin
                    // long term check
					
					// at this point use available_col_l0_flag as a flag to indicate whether distscaling is used
                    case({cur_poc_l0_diff == col_l0_poc_diff,cur_poc_l1_diff == col_l1_poc_diff})
                        2'b00: begin
                            state_temporal <= STATE_TEMPOR_TX_WAIT;
                            tbl1_clipped <= cur_poc_l1_diff_clipped;
                            tbl0_clipped <= cur_poc_l0_diff_clipped;
                            if((col_l0_poc_diff > 127)) begin               // there is at least one '1' in bits higher than 7th bit and the msb is zero//if(col_l0_poc_diff > 127) begin
                                tdl0_clipped <= 127;
                            end
                            else if( col_l0_poc_diff < -128) begin
                                tdl0_clipped <= -128;
                            end
                            else begin
                                tdl0_clipped <= col_l0_poc_diff[7:0];
                            end   
							if(slice_type == `SLICE_B) begin
								if(col_l1_poc_diff > 127) begin               // there is at least one '1' in bits higher than 7th bit and the msb is zero//if(col_l1_poc_diff > 127) begin
									tdl1_clipped <= 127;
								end
								else if(  col_l1_poc_diff < -128) begin
									tdl1_clipped <= -128;
								end
								else begin
									tdl1_clipped <= col_l1_poc_diff[7:0];
								end                             							
							end

                        end
                        2'b10: begin
							available_col_l0_flag <= 1;
                            if(slice_type == `SLICE_P) begin    // checking slice P gives the advantage of return early
                                col_mv_ready <= 1;
                                state_temporal <= STATE_TEMPO_IDLE;
                            end  
                            else begin
                                state_temporal <= STATE_TEMPOR_TX_WAIT;
                                tbl1_clipped <= cur_poc_l1_diff_clipped;
                                if(col_l1_poc_diff > 127) begin               // there is at least one '1' in bits higher than 7th bit and the msb is zero//if(col_l1_poc_diff > 127) begin
                                    tdl1_clipped <= 127;
                                end
                                else if(  col_l1_poc_diff < -128) begin
                                    tdl1_clipped <= -128;
                                end
                                else begin
                                    tdl1_clipped <= col_l1_poc_diff[7:0];
                                end                                    
                            end
                        end
                        2'b01: begin
                            available_col_l1_flag <= 1;  
                            state_temporal <= STATE_TEMPOR_TX_WAIT;
                            tbl0_clipped <= cur_poc_l0_diff_clipped;
                            if((col_l0_poc_diff > 127)) begin               // there is at least one '1' in bits higher than 7th bit and the msb is zero//if(col_l0_poc_diff > 127) begin
                                tdl0_clipped <= 127;
                            end
                            else if( col_l0_poc_diff < -128) begin
                                tdl0_clipped <= -128;
                            end
                            else begin
                                tdl0_clipped <= col_l0_poc_diff[7:0];
                            end   
                        end
                        2'b11: begin
							if(slice_type == `SLICE_B) begin
								available_col_l1_flag <= 1;
							end
							available_col_l0_flag <= 1;                             
                            col_mv_ready <= 1;
                            state_temporal <= STATE_TEMPO_IDLE;
                        end
						default: begin
							// synthesis translate_off
								$display("should not reach default state ");
								$display("cur_poc_l0_diff %d col_l0_poc_diff %d cur_poc_l1_diff %d col_l1_poc_diff %d",
								cur_poc_l0_diff, col_l0_poc_diff,cur_poc_l1_diff, col_l1_poc_diff);
								$stop;
							// synthesis translate_on 
						end
                    endcase
                   
                end
                STATE_TEMPOR_TX_WAIT: begin
                    state_temporal <= STATE_TEMP0R_DIST_SCALE;
                end
                STATE_TEMP0R_DIST_SCALE: begin
                    state_temporal <= STATE_TEMP0R_DIST_SCALE_CLIP;
					if(available_col_l0_flag ==0) begin
						distscale_factor_l0 <= ((tx_l0 * tbl0_clipped) + 32)>>6;
					end
					if(slice_type == `SLICE_B) begin
						if(available_col_l1_flag ==0) begin
							distscale_factor_l1 <= ((tx_l1 * tbl1_clipped) + 32)>>6;
						end
					end
				end 
                STATE_TEMP0R_DIST_SCALE_CLIP: begin
                    state_temporal <= STATE_TEMP0R_DIST_INTO_MV;
					if(available_col_l0_flag ==0) begin
						if(|(distscale_factor_l0[TX_WIDTH + CLIPPED_WIDTH -1:12]) & distscale_factor_l0[TX_WIDTH + CLIPPED_WIDTH -1] == 0) begin//if(distscale_factor_l0 > 4095) begin
							distscale_factor_l0_clipped <= 4095;
						end
						else if(!(&(distscale_factor_l0[TX_WIDTH + CLIPPED_WIDTH -1:12])) & distscale_factor_l0[TX_WIDTH + CLIPPED_WIDTH -1] == 1) begin //else if(distscale_factor_l0 < -4096) begin
							distscale_factor_l0_clipped <= -4096;
						end
						else begin
							distscale_factor_l0_clipped <= distscale_factor_l0[DIST_SCALE_WIDTH-1:0] ;
						end					
					end
					if(slice_type == `SLICE_B) begin
						if(available_col_l1_flag ==0) begin
							if(|(distscale_factor_l1[TX_WIDTH + CLIPPED_WIDTH -1:12]) & distscale_factor_l1[TX_WIDTH + CLIPPED_WIDTH -1] == 0) begin//if(distscale_factor_l1 > 4095) begin
								distscale_factor_l1_clipped <= 4095;
							end
							else if(!(&(distscale_factor_l1[TX_WIDTH + CLIPPED_WIDTH -1:12])) & distscale_factor_l1[TX_WIDTH + CLIPPED_WIDTH -1] == 1) begin//else if(distscale_factor_l1 < -4096) begin
								distscale_factor_l1_clipped <= -4096;
							end
							else begin
								distscale_factor_l1_clipped <= distscale_factor_l1[DIST_SCALE_WIDTH-1:0];
							end            					
						end
					end
				end
                STATE_TEMP0R_DIST_INTO_MV: begin
					if(available_col_l0_flag ==0) begin
						dist_scale_into_mv_l0_x <= (distscale_factor_l0_clipped * col_mv_field_mv_x_l0_to_inter);  
						dist_scale_into_mv_l0_y <= (distscale_factor_l0_clipped * col_mv_field_mv_y_l0_to_inter); 				
					end
					if(slice_type == `SLICE_B) begin
						if(available_col_l1_flag ==0) begin
							dist_scale_into_mv_l1_x <= (distscale_factor_l1_clipped * col_mv_field_mv_x_l1_to_inter);  
							dist_scale_into_mv_l1_y <= (distscale_factor_l1_clipped * col_mv_field_mv_y_l1_to_inter);           					
						end		
					end
					state_temporal <= STATE_TEMP0R_DIST_SCALE_DELAY;
                end
				// STATE_TEMP0R_DIST_SCALE_WAIT: begin
					// state_temporal <= STATE_TEMP0R_DIST_SCALE_DELAY;
				// end
                STATE_TEMP0R_DIST_SCALE_DELAY: begin
                    state_temporal <= STATE_TEMP0R_MV_CLIP;
					if(available_col_l0_flag ==0) begin
						new_mv_l0_x <= (sign_dist_scale_into_mv_l0_x)? (abs_dist_scale_into_mv_l0_x_plus_127 >>>8): -1*(abs_dist_scale_into_mv_l0_x_plus_127>>>8);
						new_mv_l0_y <= (sign_dist_scale_into_mv_l0_y)? (abs_dist_scale_into_mv_l0_y_plus_127 >>>8): -1*(abs_dist_scale_into_mv_l0_y_plus_127>>>8);		
					end
					if(slice_type == `SLICE_B) begin
						if(available_col_l1_flag ==0) begin
							new_mv_l1_x <= (sign_dist_scale_into_mv_l1_x)? (abs_dist_scale_into_mv_l1_x_plus_127 >>>8): -1*(abs_dist_scale_into_mv_l1_x_plus_127>>>8);
							new_mv_l1_y <= (sign_dist_scale_into_mv_l1_y)? (abs_dist_scale_into_mv_l1_y_plus_127 >>>8): -1*(abs_dist_scale_into_mv_l1_y_plus_127>>>8);
						end	  
					end
                end                
                STATE_TEMP0R_MV_CLIP: begin
					if(available_col_l0_flag ==0) begin
						col_mv_field_mv_x_l0_to_inter <= new_mv_l0_x_clipped;
						col_mv_field_mv_y_l0_to_inter <= new_mv_l0_y_clipped;
					end
					available_col_l0_flag <= 1; 
					if(slice_type == `SLICE_B) begin
						if(available_col_l1_flag ==0) begin
							col_mv_field_mv_x_l1_to_inter <= new_mv_l1_x_clipped;
							col_mv_field_mv_y_l1_to_inter <= new_mv_l1_y_clipped;         					
						end	
						available_col_l1_flag <= 1;
                    end
                           
                    state_temporal <= STATE_TEMPO_IDLE;
					col_mv_ready <= 1;
                end
            endcase
        end
    end
    

	
    always@(*) begin : combinational_logic
		col_mv_field_ref_idx_l0_to_inter = {REF_IDX_LX_WIDTH{1'bx}};
		col_mv_field_ref_idx_l1_to_inter = {REF_IDX_LX_WIDTH{1'bx}};
            case(state_temporal)
                STATE_TEMPO_IDLE: begin
                    
                end
                STATE_REF_PIC_COUNT: begin
                    
                end
                STATE_TEMPO_GET_CTR_MV: begin
                     
                end
                STATE_TEMPO_GET_BR_MV: begin
                    if(col_br_boundary_ok_d) begin   
                        col_mv_field_ref_idx_l0_to_inter    = col_mv_field_ref_idx_l0_out;
                        col_mv_field_ref_idx_l1_to_inter    = col_mv_field_ref_idx_l1_out;    
                    end
                    else begin
                        col_mv_field_ref_idx_l0_to_inter    = col_ctr_mv_field_ref_idx_l0    ;
                        col_mv_field_ref_idx_l1_to_inter    = col_ctr_mv_field_ref_idx_l1    ;
                    end            
                end

                STATE_TEMPO_GET_BR_MV_CHECK: begin
                    case({col_mv_field_pred_flag_l0_to_inter,col_mv_field_pred_flag_l1_to_inter})
                        2'd0: begin
							col_mv_field_ref_idx_l0_to_inter    = col_ctr_mv_field_ref_idx_l0    ;
							col_mv_field_ref_idx_l1_to_inter    = col_ctr_mv_field_ref_idx_l1    ;
                        end	
					endcase
				end
                STATE_TEMP0R_DIFF_PIC_COUNT: begin
                   
                end
                STATE_TEMPOR_POC_DIF_COMPAR: begin
                end
                STATE_TEMPOR_TX_WAIT: begin
                end
                STATE_TEMP0R_DIST_SCALE: begin
                end 
                STATE_TEMP0R_DIST_SCALE_CLIP: begin
                end
                STATE_TEMP0R_DIST_INTO_MV: begin
                end
                STATE_TEMP0R_DIST_SCALE_DELAY: begin
                end                
                STATE_TEMP0R_MV_CLIP: begin
                end
            endcase
    end
    

endmodule   