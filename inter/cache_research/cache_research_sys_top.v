`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:   17:23:09 05/29/2014
// Design Name:   pred_top_wrapper
// Module Name:   C:/Users/IC_2/Desktop/pred_top_total_repo_/sim/intra_pred_tb.v
// Project Name:  pred_ise_project
// Target Device:
// Tool versions:
// Description:
//
// Verilog Test Fixture created by ISE for module: pred_top_wrapper
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
////////////////////////////////////////////////////////////////////////////////

module cache_research_sys_top(

	ddr3_dq,
	ddr3_dqs_n,
	ddr3_dqs_p,


	ddr3_addr,
	ddr3_ba,
	ddr3_ras_n,
	ddr3_cas_n,
	ddr3_we_n,
	ddr3_reset_n,
	ddr3_ck_p,
	ddr3_ck_n,
	ddr3_cke,
	ddr3_cs_n,
	ddr3_dm,
	ddr3_odt,


	clk_in_p,
	clk_in_n,

	sys_rst,
	design_clk,
	reset,
	cache_idle_out,
	valid_in					,
	res_present_in				,	
	bi_pred_block_cache_in		,	
	x0_tu_end_in_min_tus_in		,	
	y0_tu_end_in_min_tus_in		,	
	xT_in_min_tus_in			,	
	yT_in_min_tus_in     		,	
	luma_ref_start_x_in  		,		
	luma_ref_start_y_in  		,	
	chma_ref_start_x_in  		,	
	chma_ref_start_y_in  		,	
	chma_ref_width_x_in  		,	
	chma_ref_height_y_in 		,   
	luma_ref_width_x_in  		,	
	luma_ref_height_y_in 		,
	ch_frac_x					,
	ch_frac_y					,
	ref_idx_in_in				,
	pic_width					,   
	pic_height					 	
	
	
);

    `include "../sim/cache_configs_def.v"
    `include "../sim/pred_def.v"
    `include "../sim/inter_axi_def.v"


//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                           LUMA_DIM_WDTH		    	= 4;        // out block dimension  max 11
    parameter                           LUMA_DIM_ADDR_WDTH          = 7;        //max 121
    parameter                           CHMA_DIM_WDTH               = 3;        // max 5 
    parameter                           CHMA_DIM_ADDR_WDTH          = 5;        // max 25 
    parameter                           LUMA_REF_BLOCK_WIDTH        = 4'd11;
    parameter                           CHMA_REF_BLOCK_WIDTH        = 3'd5;
	
	parameter 							BLOCK_NUMBER_WIDTH 			= 6; // Since 32 elements can be occupied in hit fifo, if all of them come from single cache line block 6 bits needed to uniquely identify a block

    parameter                           CACHE_LINE_LUMA_OFFSET      = 0;
    parameter                           CACHE_LINE_CB_OFFSET      = CACHE_LINE_WDTH * LUMA_BITS;
    parameter                           CACHE_LINE_CR_OFFSET      = CACHE_LINE_CB_OFFSET + ((CACHE_LINE_WDTH * LUMA_BITS)>>2);

    parameter                           REF_PIX_AXI_AX_SIZE  = `AX_SIZE_64;
    parameter                           REF_PIX_AXI_AX_LEN   = `AX_LEN_1;
//----------------------------------------------------mig parameters
   //***************************************************************************
   // Traffic Gen related parameters
   //***************************************************************************
   parameter BL_WIDTH              = 10;
   parameter PORT_MODE             = "BI_MODE";
   parameter DATA_MODE             = 4'b0010;
   parameter ADDR_MODE             = 4'b0011;
   parameter TST_MEM_INSTR_MODE    = "R_W_INSTR_MODE";
   parameter EYE_TEST              = "FALSE";
                                     // set EYE_TEST = "TRUE" to probe memory
                                     // signals. Traffic Generator will only
                                     // write to one single location and no
                                     // read transactions will be generated.
   parameter DATA_PATTERN          = "DGEN_ALL";
                                      // For small devices; choose one only.
                                      // For large device; choose "DGEN_ALL"
                                      // "DGEN_HAMMER"; "DGEN_WALKING1";
                                      // "DGEN_WALKING0";"DGEN_ADDR";"
                                      // "DGEN_NEIGHBOR";"DGEN_PRBS";"DGEN_ALL"
   parameter CMD_PATTERN           = "CGEN_ALL";
                                      // "CGEN_PRBS";"CGEN_FIXED";"CGEN_BRAM";
                                      // "CGEN_SEQUENTIAL"; "CGEN_ALL"
   parameter BEGIN_ADDRESS         = 32'h00000000;
   parameter END_ADDRESS           = 32'h00ffffff;
   parameter MEM_ADDR_ORDER
     = "TG_TEST";
   parameter PRBS_EADDR_MASK_POS   = 32'hff000000;
   parameter CMD_WDT               = 'h3FF;
   parameter WR_WDT                = 'h1FFF;
   parameter RD_WDT                = 'h3FF;
   parameter SEL_VICTIM_LINE       = 0;
   parameter ENFORCE_RD_WR         = 0;
   parameter ENFORCE_RD_WR_CMD     = 8'h11;
   parameter ENFORCE_RD_WR_PATTERN = 3'b000;
   parameter C_EN_WRAP_TRANS       = 0;
   parameter C_AXI_NBURST_TEST     = 0;

   //***************************************************************************
   // The following parameters refer to width of various ports
   //***************************************************************************
   parameter BANK_WIDTH            = 3;
                                     // # of memory Bank Address bits.
   parameter CK_WIDTH              = 1;
                                     // # of CK/CK# outputs to memory.
   parameter COL_WIDTH             = 10;
                                     // # of memory Column Address bits.
   parameter CS_WIDTH              = 1;
                                     // # of unique CS outputs to memory.
   parameter nCS_PER_RANK          = 1;
                                     // # of unique CS outputs per rank for phy
   parameter CKE_WIDTH             = 1;
                                     // # of CKE outputs to memory.
   parameter DATA_BUF_ADDR_WIDTH   = 5;
   parameter DQ_CNT_WIDTH          = 6;
                                     // = ceil(log2(DQ_WIDTH))
   parameter DQ_PER_DM             = 8;
   parameter DM_WIDTH              = 8;
                                     // # of DM (data mask)
   parameter DQ_WIDTH              = 64;
                                     // # of DQ (data)
   parameter DQS_WIDTH             = 8;
   parameter DQS_CNT_WIDTH         = 3;
                                     // = ceil(log2(DQS_WIDTH))
   parameter DRAM_WIDTH            = 8;
                                     // # of DQ per DQS
   parameter ECC                   = "OFF";
   parameter nBANK_MACHS           = 4;
   parameter RANKS                 = 1;
                                     // # of Ranks.
   parameter ODT_WIDTH             = 1;
                                     // # of ODT outputs to memory.
   parameter ROW_WIDTH             = 14;
                                     // # of memory Row Address bits.
   parameter ADDR_WIDTH            = 28;
                                     // # = RANK_WIDTH + BANK_WIDTH
                                     //     + ROW_WIDTH + COL_WIDTH;
                                     // Chip Select is always tied to low for
                                     // single rank devices
   parameter USE_CS_PORT          = 1;
                                     // # = 1; When Chip Select (CS#) output is enabled
                                     //   = 0; When Chip Select (CS#) output is disabled
                                     // If CS_N disabled; user must connect
                                     // DRAM CS_N input(s) to ground
   parameter USE_DM_PORT           = 1;
                                     // # = 1; When Data Mask option is enabled
                                     //   = 0; When Data Mask option is disbaled
                                     // When Data Mask option is disabled in
                                     // MIG Controller Options page; the logic
                                     // related to Data Mask should not get
                                     // synthesized
   parameter USE_ODT_PORT          = 1;
                                     // # = 1; When ODT output is enabled
                                     //   = 0; When ODT output is disabled
                                     // Parameter configuration for Dynamic ODT support:
                                     // USE_ODT_PORT = 0; RTT_NOM = "DISABLED"; RTT_WR = "60/120".
                                     // This configuration allows to save ODT pin mapping from FPGA.
                                     // The user can tie the ODT input of DRAM to HIGH.
   parameter PHY_CONTROL_MASTER_BANK = 1;
                                     // The bank index where master PHY_CONTROL resides;
                                     // equal to the PLL residing bank
   parameter MEM_DENSITY           = "1Gb";
                                     // Indicates the density of the Memory part
                                     // Added for the sake of Vivado simulations
   parameter MEM_SPEEDGRADE        = "125";
                                     // Indicates the Speed grade of Memory Part
                                     // Added for the sake of Vivado simulations
   parameter MEM_DEVICE_WIDTH      = 8;
                                     // Indicates the device width of the Memory Part
                                     // Added for the sake of Vivado simulations

   //***************************************************************************
   // The following parameters are mode register settings
   //***************************************************************************
   parameter AL                    = "0";
                                     // DDR3 SDRAM:
                                     // Additive Latency (Mode Register 1).
                                     // # = "0"; "CL-1"; "CL-2".
                                     // DDR2 SDRAM:
                                     // Additive Latency (Extended Mode Register).
   parameter nAL                   = 0;
                                     // # Additive Latency in number of clock
                                     // cycles.
   parameter BURST_MODE            = "8";
                                     // DDR3 SDRAM:
                                     // Burst Length (Mode Register 0).
                                     // # = "8"; "4"; "OTF".
                                     // DDR2 SDRAM:
                                     // Burst Length (Mode Register).
                                     // # = "8"; "4".
   parameter BURST_TYPE            = "SEQ";
                                     // DDR3 SDRAM: Burst Type (Mode Register 0).
                                     // DDR2 SDRAM: Burst Type (Mode Register).
                                     // # = "SEQ" - (Sequential);
                                     //   = "INT" - (Interleaved).
   parameter CL                    = 9;
                                     // in number of clock cycles
                                     // DDR3 SDRAM: CAS Latency (Mode Register 0).
                                     // DDR2 SDRAM: CAS Latency (Mode Register).
   parameter CWL                   = 7;
                                     // in number of clock cycles
                                     // DDR3 SDRAM: CAS Write Latency (Mode Register 2).
                                     // DDR2 SDRAM: Can be ignored
   parameter OUTPUT_DRV            = "HIGH";
                                     // Output Driver Impedance Control (Mode Register 1).
                                     // # = "HIGH" - RZQ/7;
                                     //   = "LOW" - RZQ/6.
   parameter RTT_NOM               = "40";
                                     // RTT_NOM (ODT) (Mode Register 1).
                                     //   = "120" - RZQ/2;
                                     //   = "60"  - RZQ/4;
                                     //   = "40"  - RZQ/6.
   parameter RTT_WR                = "OFF";
                                     // RTT_WR (ODT) (Mode Register 2).
                                     // # = "OFF" - Dynamic ODT off;
                                     //   = "120" - RZQ/2;
                                     //   = "60"  - RZQ/4;
   parameter ADDR_CMD_MODE         = "1T" ;
                                     // # = "1T"; "2T".
   parameter REG_CTRL              = "OFF";
                                     // # = "ON" - RDIMMs;
                                     //   = "OFF" - Components; SODIMMs; UDIMMs.
   parameter CA_MIRROR             = "OFF";
                                     // C/A mirror opt for DDR3 dual rank

   //***************************************************************************
   // The following parameters are multiplier and divisor factors for PLLE2.
   // Based on the selected design frequency these parameters vary.
   //***************************************************************************
   parameter CLKIN_PERIOD          = 4999;
                                     // Input Clock Period
   parameter CLKFBOUT_MULT         = 11;
                                     // write PLL VCO multiplier
   parameter DIVCLK_DIVIDE         = 2;
                                     // write PLL VCO divisor
   parameter CLKOUT0_PHASE         = 337.5;
                                     // Phase for PLL output clock (CLKOUT0)
   parameter CLKOUT0_DIVIDE        = 2;
                                     // VCO output divisor for PLL output clock (CLKOUT0)
   parameter CLKOUT1_DIVIDE        = 2;
                                     // VCO output divisor for PLL output clock (CLKOUT1)
   parameter CLKOUT2_DIVIDE        = 32;
                                     // VCO output divisor for PLL output clock (CLKOUT2)
   parameter CLKOUT3_DIVIDE        = 8;
                                     // VCO output divisor for PLL output clock (CLKOUT3)

   //***************************************************************************
   // Memory Timing Parameters. These parameters varies based on the selected
   // memory part.
   //***************************************************************************
   parameter tCKE                  = 5000;
                                     // memory tCKE paramter in pS
   parameter tFAW                  = 30000;
                                     // memory tRAW paramter in pS.
   parameter tRAS                  = 35000;
                                     // memory tRAS paramter in pS.
   parameter tRCD                  = 13125;
                                     // memory tRCD paramter in pS.
   parameter tREFI                 = 7800000;
                                     // memory tREFI paramter in pS.
   parameter tRFC                  = 110000;
                                     // memory tRFC paramter in pS.
   parameter tRP                   = 13125;
                                     // memory tRP paramter in pS.
   parameter tRRD                  = 6000;
                                     // memory tRRD paramter in pS.
   parameter tRTP                  = 7500;
                                     // memory tRTP paramter in pS.
   parameter tWTR                  = 7500;
                                     // memory tWTR paramter in pS.
   parameter tZQI                  = 128_000_000;
                                     // memory tZQI paramter in nS.
   parameter tZQCS                 = 64;
                                     // memory tZQCS paramter in clock cycles.

   //***************************************************************************
   // Simulation parameters
   //***************************************************************************
   parameter SIM_BYPASS_INIT_CAL   = "OFF";
                                     // # = "OFF" -  Complete memory init &
                                     //              calibration sequence
                                     // # = "SKIP" - Not supported
                                     // # = "FAST" - Complete memory init & use
                                     //              abbreviated calib sequence

   parameter SIMULATION            = "FALSE";
                                     // Should be TRUE during design simulations and
                                     // FALSE during implementations

   //***************************************************************************
   // The following parameters varies based on the pin out entered in MIG GUI.
   // Do not change any of these parameters directly by editing the RTL.
   // Any changes required should be done through GUI and the design regenerated.
   //***************************************************************************
   parameter BYTE_LANES_B0         = 4'b1111;
                                     // Byte lanes used in an IO column.
   parameter BYTE_LANES_B1         = 4'b1110;
                                     // Byte lanes used in an IO column.
   parameter BYTE_LANES_B2         = 4'b1111;
                                     // Byte lanes used in an IO column.
   parameter BYTE_LANES_B3         = 4'b0000;
                                     // Byte lanes used in an IO column.
   parameter BYTE_LANES_B4         = 4'b0000;
                                     // Byte lanes used in an IO column.
   parameter DATA_CTL_B0           = 4'b1111;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
   parameter DATA_CTL_B1           = 4'b0000;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
   parameter DATA_CTL_B2           = 4'b1111;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
   parameter DATA_CTL_B3           = 4'b0000;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
   parameter DATA_CTL_B4           = 4'b0000;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
   parameter PHY_0_BITLANES        = 48'h3FE_1FF_1FF_2FF;
   parameter PHY_1_BITLANES        = 48'hFFE_F30_CB4_000;
   parameter PHY_2_BITLANES        = 48'h3FE_3FE_3BF_2FF;

   // control/address/data pin mapping parameters
   parameter CK_BYTE_MAP
     = 144'h00_00_00_00_00_00_00_00_00_00_00_00_00_00_00_00_00_11;
   parameter ADDR_MAP
     = 192'h000_000_132_136_135_133_139_124_131_129_137_134_13A_128_138_13B;
   parameter BANK_MAP   = 36'h125_12A_12B;
   parameter CAS_MAP    = 12'h115;
   parameter CKE_ODT_BYTE_MAP = 8'h00;
   parameter CKE_MAP    = 96'h000_000_000_000_000_000_000_117;
   parameter ODT_MAP    = 96'h000_000_000_000_000_000_000_112;
   parameter CS_MAP     = 120'h000_000_000_000_000_000_000_000_000_114;
   parameter PARITY_MAP = 12'h000;
   parameter RAS_MAP    = 12'h11A;
   parameter WE_MAP     = 12'h11B;
   parameter DQS_BYTE_MAP
     = 144'h00_00_00_00_00_00_00_00_00_00_20_21_22_23_03_02_01_00;
   parameter DATA0_MAP  = 96'h009_000_003_001_007_006_005_002;
   parameter DATA1_MAP  = 96'h014_018_010_011_017_016_012_013;
   parameter DATA2_MAP  = 96'h021_022_025_020_027_023_026_028;
   parameter DATA3_MAP  = 96'h033_039_031_035_032_038_034_037;
   parameter DATA4_MAP  = 96'h231_238_237_236_233_232_234_239;
   parameter DATA5_MAP  = 96'h226_227_225_229_221_222_224_228;
   parameter DATA6_MAP  = 96'h214_215_210_218_217_213_219_212;
   parameter DATA7_MAP  = 96'h207_203_204_206_202_201_205_209;
   parameter DATA8_MAP  = 96'h000_000_000_000_000_000_000_000;
   parameter DATA9_MAP  = 96'h000_000_000_000_000_000_000_000;
   parameter DATA10_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA11_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA12_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA13_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA14_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA15_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA16_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA17_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter MASK0_MAP  = 108'h000_200_211_223_235_036_024_015_004;
   parameter MASK1_MAP  = 108'h000_000_000_000_000_000_000_000_000;

   parameter SLOT_0_CONFIG         = 8'b0000_0001;
                                     // Mapping of Ranks.
   parameter SLOT_1_CONFIG         = 8'b0000_0000;
                                     // Mapping of Ranks.

   //***************************************************************************
   // IODELAY and PHY related parameters
   //***************************************************************************
   parameter IODELAY_HP_MODE       = "ON";
                                     // to phy_top
   parameter IBUF_LPWR_MODE        = "OFF";
                                     // to phy_top
   parameter DATA_IO_IDLE_PWRDWN   = "OFF";
                                     // # = "ON"; "OFF"
   parameter BANK_TYPE             = "HP_IO";
                                     // # = "HP_IO"; "HPL_IO"; "HR_IO"; "HRL_IO"
   parameter DATA_IO_PRIM_TYPE     = "DEFAULT";
                                     // # = "HP_LP"; "HR_LP"; "DEFAULT"
   parameter CKE_ODT_AUX           = "FALSE";
   parameter USER_REFRESH          = "OFF";
   parameter WRLVL                 = "ON";
                                     // # = "ON" - DDR3 SDRAM
                                     //   = "OFF" - DDR2 SDRAM.
   parameter ORDERING              = "NORM";
                                     // # = "NORM"; "STRICT"; "RELAXED".
   parameter CALIB_ROW_ADD         = 16'h0000;
                                     // Calibration row address will be used for
                                     // calibration read and write operations
   parameter CALIB_COL_ADD         = 12'h000;
                                     // Calibration column address will be used for
                                     // calibration read and write operations
   parameter CALIB_BA_ADD          = 3'h0;
                                     // Calibration bank address will be used for
                                     // calibration read and write operations
   parameter TCQ                   = 100;
   parameter IODELAY_GRP           = "IODELAY_MIG";
                                     // It is associated to a set of IODELAYs with
                                     // an IDELAYCTRL that have same IODELAY CONTROLLER
                                     // clock frequency.
   parameter SYSCLK_TYPE           = "DIFFERENTIAL";
                                     // System clock type DIFFERENTIAL; SINGLE_ENDED;
                                     // NO_BUFFER
   parameter REFCLK_TYPE           = "USE_SYSTEM_CLOCK";
                                     // Reference clock type DIFFERENTIAL; SINGLE_ENDED;
                                     // NO_BUFFER; USE_SYSTEM_CLOCK
   parameter SYS_RST_PORT          = "FALSE";
                                     // "TRUE" - if pin is selected for sys_rst
                                     //          and IBUF will be instantiated.
                                     // "FALSE" - if pin is not selected for sys_rst

   parameter DRAM_TYPE             = "DDR3";
   parameter CAL_WIDTH             = "HALF";
   parameter STARVE_LIMIT          = 2;
                                     // # = 2;3;4.

   //***************************************************************************
   // Referece clock frequency parameters
   //***************************************************************************
   parameter REFCLK_FREQ           = 200.0;
                                     // IODELAYCTRL reference clock frequency
   parameter DIFF_TERM_REFCLK      = "TRUE";
                                     // Differential Termination for idelay
                                     // reference clock input pins
   //***************************************************************************
   // System clock frequency parameters
   //***************************************************************************
   parameter tCK                   = 1818;
                                     // memory tCK paramter.
                                     // # = Clock Period in pS.
   parameter nCK_PER_CLK           = 4;
                                     // # of memory CKs per fabric CLK
   parameter DIFF_TERM_SYSCLK      = "FALSE";
                                     // Differential Termination for System
                                     // clock input pins


   //***************************************************************************
   // AXI4 Shim parameters
   //***************************************************************************

   parameter UI_EXTRA_CLOCKS = "FALSE";
                                     // Generates extra clocks as
                                     // 1/2; 1/4 and 1/8 of fabrick clock.
                                     // Valid for DDR2/DDR3 AXI interfaces
                                     // based on GUI selection
   parameter C_S_AXI_ID_WIDTH              = 4;
                                             // Width of all master and slave ID signals.
                                             // # = >= 1.
   parameter C_S_AXI_MEM_SIZE              = "1073741824";
                                     // Address Space required for this component
   parameter C_S_AXI_ADDR_WIDTH            = 32;
                                             // Width of S_AXI_AWADDR; S_AXI_ARADDR; M_AXI_AWADDR and
                                             // M_AXI_ARADDR for all SI/MI slots.
                                             // # = 32.
   parameter C_S_AXI_DATA_WIDTH            = AXI_MIG_DATA_WIDTH;
                                             // Width of WDATA and RDATA on SI slot.
                                             // Must be <= APP_DATA_WIDTH.
                                             // # = 32; 64; 128; 256.
   parameter C_MC_nCK_PER_CLK              = 4;
                                             // Indicates whether to instatiate upsizer
                                             // Range: 0; 1
   parameter C_S_AXI_SUPPORTS_NARROW_BURST = 1;
                                             // Indicates whether to instatiate upsizer
                                             // Range: 0; 1
   parameter C_RD_WR_ARB_ALGORITHM          = "RD_PRI_REG";
                                             // Indicates the Arbitration
                                             // Allowed values - "TDM"; "ROUND_ROBIN";
                                             // "RD_PRI_REG"; "RD_PRI_REG_STARVE_LIMIT"
                                             // "WRITE_PRIORITY"; "WRITE_PRIORITY_REG"
   parameter C_S_AXI_REG_EN0               = 20'h00000;
                                             // C_S_AXI_REG_EN0[00] = Reserved
                                             // C_S_AXI_REG_EN0[04] = AW CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN0[05] =  W CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN0[06] =  B CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN0[07] =  R CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN0[08] = AW CHANNEL UPSIZER REGISTER SLICE
                                             // C_S_AXI_REG_EN0[09] =  W CHANNEL UPSIZER REGISTER SLICE
                                             // C_S_AXI_REG_EN0[10] = AR CHANNEL UPSIZER REGISTER SLICE
                                             // C_S_AXI_REG_EN0[11] =  R CHANNEL UPSIZER REGISTER SLICE
   parameter C_S_AXI_REG_EN1               = 20'h00000;
                                             // Instatiates register slices after the upsizer.
                                             // The type of register is specified for each channel
                                             // in a vector. 4 bits per channel are used.
                                             // C_S_AXI_REG_EN1[03:00] = AW CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN1[07:04] =  W CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN1[11:08] =  B CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN1[15:12] = AR CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN1[20:16] =  R CHANNEL REGISTER SLICE
                                             // Possible values for each channel are:
                                             //
                                             //   0 => BYPASS    = The channel is just wired through the
                                             //                    module.
                                             //   1 => FWD       = The master VALID and payload signals
                                             //                    are registrated.
                                             //   2 => REV       = The slave ready signal is registrated
                                             //   3 => FWD_REV   = Both FWD and REV
                                             //   4 => SLAVE_FWD = All slave side signals and master
                                             //                    VALID and payload are registrated.
                                             //   5 => SLAVE_RDY = All slave side signals and master
                                             //                    READY are registrated.
                                             //   6 => INPUTS    = Slave and Master side inputs are
                                             //                    registrated.
                                             //   7 => ADDRESS   = Optimized for address channel
   parameter C_S_AXI_CTRL_ADDR_WIDTH       = 32;
                                             // Width of AXI-4-Lite address bus
   parameter C_S_AXI_CTRL_DATA_WIDTH       = 32;
                                             // Width of AXI-4-Lite data buses
   parameter C_S_AXI_BASEADDR              = 32'h0000_0000;
                                             // Base address of AXI4 Memory Mapped bus.
   parameter C_ECC_ONOFF_RESET_VALUE       = 1;
                                             // Controls ECC on/off value at startup/reset
   parameter C_ECC_CE_COUNTER_WIDTH        = 8;
                                             // The external memory to controller clock ratio.

   //***************************************************************************
   // Debug parameters
   //***************************************************************************
   parameter DEBUG_PORT            = "OFF";
                                     // # = "ON" Enable debug signals/controls.
                                     //   = "OFF" Disable debug signals/controls.

   //***************************************************************************
   // Temparature monitor parameter
   //***************************************************************************
   parameter TEMP_MON_CONTROL                          = "INTERNAL";
                                     // # = "INTERNAL"; "EXTERNAL"

   parameter RST_ACT_LOW           = 0;
                                     // =1 for active low reset,
                                     // =0 for active high.

//-----------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
	   inout [DQ_WIDTH-1:0]                         ddr3_dq;
	   inout [DQS_WIDTH-1:0]                        ddr3_dqs_n;
	   inout [DQS_WIDTH-1:0]                        ddr3_dqs_p;

	   // Outputs
	   output [ROW_WIDTH-1:0]                       ddr3_addr;
	   output [BANK_WIDTH-1:0]                      ddr3_ba;
	   output                                       ddr3_ras_n;
	   output                                       ddr3_cas_n;
	   output                                       ddr3_we_n;
	   output                                       ddr3_reset_n;
	   output [CK_WIDTH-1:0]                        ddr3_ck_p;
	   output [CK_WIDTH-1:0]                        ddr3_ck_n;
	   output [CKE_WIDTH-1:0]                       ddr3_cke;
	   output [CS_WIDTH*nCS_PER_RANK-1:0]           ddr3_cs_n;
	   output [DM_WIDTH-1:0]                        ddr3_dm;
	   output [ODT_WIDTH-1:0]                       ddr3_odt;

	   // Inputs
	   // Differential system clocks
	   input                                        clk_in_p;
	   input                                        clk_in_n;

	   // System reset - Default polarity of sys_rst pin is Active Low.
	   // System reset polarity will change based on the option
	   // selected in GUI.

	   input                                        sys_rst;


//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

    output 	design_clk;
    output 	reset;
    wire  	enable;

                
    // inter prediction filter interface            
    input                                           			valid_in					;
    input 														res_present_in				;
    input 														bi_pred_block_cache_in		;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      		x0_tu_end_in_min_tus_in		;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      		y0_tu_end_in_min_tus_in		;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] 				xT_in_min_tus_in			;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] 				yT_in_min_tus_in     		;
	input  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] 		luma_ref_start_x_in  		;	
	input  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] 		luma_ref_start_y_in  		;
	input  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] 		chma_ref_start_x_in  		;	
	input  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0] 		chma_ref_start_y_in  		;	
	input  [LUMA_DIM_WDTH - 1:0]   								chma_ref_width_x_in  		;	
    input  [LUMA_DIM_WDTH - 1:0]            					chma_ref_height_y_in 		;   
	input  [LUMA_DIM_WDTH - 1:0]   								luma_ref_width_x_in  		;	
    input  [LUMA_DIM_WDTH - 1:0]            					luma_ref_height_y_in 		;
    input  [MV_C_FRAC_WIDTH_HIGH -1:0]      					ch_frac_x					;
    input  [MV_C_FRAC_WIDTH_HIGH -1:0]      					ch_frac_y					;
	input			[REF_ADDR_WDTH-1:0]		            		ref_idx_in_in				;
    input  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  		pic_width					;   
    input  signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1:0]  		pic_height					;   
	output cache_idle_out;
	


        


    // axi read interface
    wire      [AXI_ADDR_WDTH-1:0]                   ref_pix_axi_ar_addr     ;
    wire      [7:0]                                 ref_pix_axi_ar_len      ;
    wire      [2:0]                                 ref_pix_axi_ar_size     ;
    wire      [1:0]                                 ref_pix_axi_ar_burst    ;
    wire      [2:0]                                 ref_pix_axi_ar_prot     ;
    wire                                            ref_pix_axi_ar_valid    ;
    wire                                            ref_pix_axi_ar_ready    ;

    wire       [AXI_CACHE_DATA_WDTH-1:0]        	ref_pix_axi_r_data      ;
    wire       [1:0]                               	ref_pix_axi_r_resp      ;
    wire                                           	ref_pix_axi_r_last      ;
    wire                                        	ref_pix_axi_r_valid     ;
    wire                                           	ref_pix_axi_r_ready     ;

    //-----------end of axi interface

	wire clk_200;
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
  assign enable = 1;



  axi_inter_con_mig_top #
  (

     .SIMULATION                (SIMULATION),
     .BL_WIDTH                  (BL_WIDTH),
     .PORT_MODE                 (PORT_MODE),
     .DATA_MODE                 (DATA_MODE),
     .ADDR_MODE                 (ADDR_MODE),
     .TST_MEM_INSTR_MODE        (TST_MEM_INSTR_MODE),
     .EYE_TEST                  (EYE_TEST),
     .DATA_PATTERN              (DATA_PATTERN),
     .CMD_PATTERN               (CMD_PATTERN),
     .BEGIN_ADDRESS             (BEGIN_ADDRESS),
     .END_ADDRESS               (END_ADDRESS),
     .PRBS_EADDR_MASK_POS       (PRBS_EADDR_MASK_POS),
     .SEL_VICTIM_LINE           (SEL_VICTIM_LINE),

     .BANK_WIDTH                (BANK_WIDTH),
     .CK_WIDTH                  (CK_WIDTH),
     .COL_WIDTH                 (COL_WIDTH),
     .CS_WIDTH                  (CS_WIDTH),
     .nCS_PER_RANK              (nCS_PER_RANK),
     .CKE_WIDTH                 (CKE_WIDTH),
     .DATA_BUF_ADDR_WIDTH       (DATA_BUF_ADDR_WIDTH),
     .DQ_CNT_WIDTH              (DQ_CNT_WIDTH),
     .DQ_PER_DM                 (DQ_PER_DM),
     .DM_WIDTH                  (DM_WIDTH),

     .DQ_WIDTH                  (DQ_WIDTH),
     .DQS_WIDTH                 (DQS_WIDTH),
     .DQS_CNT_WIDTH             (DQS_CNT_WIDTH),
     .DRAM_WIDTH                (DRAM_WIDTH),
     .ECC                       (ECC),
     .nBANK_MACHS               (nBANK_MACHS),
     .RANKS                     (RANKS),
     .ODT_WIDTH                 (ODT_WIDTH),
     .ROW_WIDTH                 (ROW_WIDTH),
     .ADDR_WIDTH                (ADDR_WIDTH),
     .USE_CS_PORT               (USE_CS_PORT),
     .USE_DM_PORT               (USE_DM_PORT),
     .USE_ODT_PORT              (USE_ODT_PORT),

     .AL                        (AL),
     .nAL                       (nAL),
     .BURST_MODE                (BURST_MODE),
     .BURST_TYPE                (BURST_TYPE),
     .CL                        (CL),
     .CWL                       (CWL),
     .OUTPUT_DRV                (OUTPUT_DRV),
     .RTT_NOM                   (RTT_NOM),
     .RTT_WR                    (RTT_WR),
     .ADDR_CMD_MODE             (ADDR_CMD_MODE),
     .REG_CTRL                  (REG_CTRL),
     .CA_MIRROR                 (CA_MIRROR),


     .CLKIN_PERIOD              (CLKIN_PERIOD),
     .CLKFBOUT_MULT             (CLKFBOUT_MULT),
     .DIVCLK_DIVIDE             (DIVCLK_DIVIDE),
     .CLKOUT0_DIVIDE            (CLKOUT0_DIVIDE),
     .CLKOUT1_DIVIDE            (CLKOUT1_DIVIDE),
     .CLKOUT2_DIVIDE            (CLKOUT2_DIVIDE),
     .CLKOUT3_DIVIDE            (CLKOUT3_DIVIDE),


     .tCKE                      (tCKE),
     .tFAW                      (tFAW),
     .tRAS                      (tRAS),
     .tRCD                      (tRCD),
     .tREFI                     (tREFI),
     .tRFC                      (tRFC),
     .tRP                       (tRP),
     .tRRD                      (tRRD),
     .tRTP                      (tRTP),
     .tWTR                      (tWTR),
     .tZQI                      (tZQI),
     .tZQCS                     (tZQCS),

     .SIM_BYPASS_INIT_CAL       (SIM_BYPASS_INIT_CAL),

     .BYTE_LANES_B0             (BYTE_LANES_B0),
     .BYTE_LANES_B1             (BYTE_LANES_B1),
     .BYTE_LANES_B2             (BYTE_LANES_B2),
     .BYTE_LANES_B3             (BYTE_LANES_B3),
     .BYTE_LANES_B4             (BYTE_LANES_B4),
     .DATA_CTL_B0               (DATA_CTL_B0),
     .DATA_CTL_B1               (DATA_CTL_B1),
     .DATA_CTL_B2               (DATA_CTL_B2),
     .DATA_CTL_B3               (DATA_CTL_B3),
     .DATA_CTL_B4               (DATA_CTL_B4),
     .PHY_0_BITLANES            (PHY_0_BITLANES),
     .PHY_1_BITLANES            (PHY_1_BITLANES),
     .PHY_2_BITLANES            (PHY_2_BITLANES),
     .CK_BYTE_MAP               (CK_BYTE_MAP),
     .ADDR_MAP                  (ADDR_MAP),
     .BANK_MAP                  (BANK_MAP),
     .CAS_MAP                   (CAS_MAP),
     .CKE_ODT_BYTE_MAP          (CKE_ODT_BYTE_MAP),
     .CKE_MAP                   (CKE_MAP),
     .ODT_MAP                   (ODT_MAP),
     .CS_MAP                    (CS_MAP),
     .PARITY_MAP                (PARITY_MAP),
     .RAS_MAP                   (RAS_MAP),
     .WE_MAP                    (WE_MAP),
     .DQS_BYTE_MAP              (DQS_BYTE_MAP),
     .DATA0_MAP                 (DATA0_MAP),
     .DATA1_MAP                 (DATA1_MAP),
     .DATA2_MAP                 (DATA2_MAP),
     .DATA3_MAP                 (DATA3_MAP),
     .DATA4_MAP                 (DATA4_MAP),
     .DATA5_MAP                 (DATA5_MAP),
     .DATA6_MAP                 (DATA6_MAP),
     .DATA7_MAP                 (DATA7_MAP),
     .DATA8_MAP                 (DATA8_MAP),
     .DATA9_MAP                 (DATA9_MAP),
     .DATA10_MAP                (DATA10_MAP),
     .DATA11_MAP                (DATA11_MAP),
     .DATA12_MAP                (DATA12_MAP),
     .DATA13_MAP                (DATA13_MAP),
     .DATA14_MAP                (DATA14_MAP),
     .DATA15_MAP                (DATA15_MAP),
     .DATA16_MAP                (DATA16_MAP),
     .DATA17_MAP                (DATA17_MAP),
     .MASK0_MAP                 (MASK0_MAP),
     .MASK1_MAP                 (MASK1_MAP),
     .SLOT_0_CONFIG             (SLOT_0_CONFIG),
     .SLOT_1_CONFIG             (SLOT_1_CONFIG),
     .MEM_ADDR_ORDER            (MEM_ADDR_ORDER),

     .IODELAY_HP_MODE           (IODELAY_HP_MODE),
     .IBUF_LPWR_MODE            (IBUF_LPWR_MODE),
     .DATA_IO_IDLE_PWRDWN       (DATA_IO_IDLE_PWRDWN),
     .DATA_IO_PRIM_TYPE         (DATA_IO_PRIM_TYPE),
     .USER_REFRESH              (USER_REFRESH),
     .WRLVL                     (WRLVL),
     .ORDERING                  (ORDERING),
     .CALIB_ROW_ADD             (CALIB_ROW_ADD),
     .CALIB_COL_ADD             (CALIB_COL_ADD),
     .CALIB_BA_ADD              (CALIB_BA_ADD),
     .TCQ                       (TCQ),


    .IODELAY_GRP               (IODELAY_GRP),
    .SYSCLK_TYPE               (SYSCLK_TYPE),
    .REFCLK_TYPE               (REFCLK_TYPE),
    .DRAM_TYPE                 (DRAM_TYPE),
    .CAL_WIDTH                 (CAL_WIDTH),
    .STARVE_LIMIT              (STARVE_LIMIT),


    .REFCLK_FREQ               (REFCLK_FREQ),


    .tCK                       (tCK),
    .nCK_PER_CLK               (nCK_PER_CLK),


     .C_S_AXI_ID_WIDTH          (C_S_AXI_ID_WIDTH),
     .C_S_AXI_ADDR_WIDTH        (C_S_AXI_ADDR_WIDTH),
     .C_S_AXI_DATA_WIDTH        (C_S_AXI_DATA_WIDTH),
     .C_MC_nCK_PER_CLK          (C_MC_nCK_PER_CLK),
     .C_S_AXI_SUPPORTS_NARROW_BURST (C_S_AXI_SUPPORTS_NARROW_BURST),
     .C_RD_WR_ARB_ALGORITHM      (C_RD_WR_ARB_ALGORITHM),
     .C_S_AXI_REG_EN0           (C_S_AXI_REG_EN0),
     .C_S_AXI_REG_EN1           (C_S_AXI_REG_EN1),

     .DEBUG_PORT                (DEBUG_PORT),

     .RST_ACT_LOW               (RST_ACT_LOW)
  )
  axi_mig_block
  (

		.mv_col_axi_awid		(),
		.mv_col_axi_awlen		(),
		.mv_col_axi_awsize		(),
		.mv_col_axi_awburst		(),
		.mv_col_axi_awlock		(),
		.mv_col_axi_awcache		(),
		.mv_col_axi_awprot		(),
		.mv_col_axi_awvalid		(0),
		.mv_col_axi_awaddr		(),
		.mv_col_axi_awready		(),
		.mv_col_axi_wstrb		(),
		.mv_col_axi_wlast		(),
		.mv_col_axi_wvalid		(0),
		.mv_col_axi_wdata		(),
		.mv_col_axi_wready		(),
		.mv_col_axi_bresp		(),
		.mv_col_axi_bvalid		(),
		.mv_col_axi_bready		(),
		.mv_pref_axi_araddr		(),
		.mv_pref_axi_arlen		(),
		.mv_pref_axi_arsize		(),
		.mv_pref_axi_arburst	(),
		.mv_pref_axi_arprot		(),
		.mv_pref_axi_arvalid	(0),
		.mv_pref_axi_arready	(),
		.mv_pref_axi_rdata		(),
		.mv_pref_axi_rresp		(),
		.mv_pref_axi_rlast		(),
		.mv_pref_axi_rvalid		(),
		.mv_pref_axi_rready		(),
		.mv_pref_axi_arlock		(),
		.mv_pref_axi_arid		(),
		.mv_pref_axi_arcache	(),
		.ref_pix_axi_ar_addr	(ref_pix_axi_ar_addr),
		.ref_pix_axi_ar_len		(ref_pix_axi_ar_len),
		.ref_pix_axi_ar_size	(ref_pix_axi_ar_size),
		.ref_pix_axi_ar_burst	(ref_pix_axi_ar_burst),
		.ref_pix_axi_ar_prot	(ref_pix_axi_ar_prot),
		.ref_pix_axi_ar_valid   (ref_pix_axi_ar_valid),
		.ref_pix_axi_ar_ready   (ref_pix_axi_ar_ready),
		.ref_pix_axi_r_data		(ref_pix_axi_r_data),
		.ref_pix_axi_r_resp		(ref_pix_axi_r_resp),
		.ref_pix_axi_r_last		(ref_pix_axi_r_last),
		.ref_pix_axi_r_valid	(ref_pix_axi_r_valid),
		.ref_pix_axi_r_ready	(ref_pix_axi_r_ready),

		.pixel_write_axi_awid   	(   	),
		.pixel_write_axi_awlen  	(  	),
		.pixel_write_axi_awsize 	( 	),
		.pixel_write_axi_awburst	(	),
		.pixel_write_axi_awlock 	( 	),
		.pixel_write_axi_awcache	(	),
		.pixel_write_axi_awprot 	( 	),
		.pixel_write_axi_awvalid	(0	),
		.pixel_write_axi_awaddr		(	),
		.pixel_write_axi_awready	(	),
		.pixel_write_axi_wstrb		(	),
		.pixel_write_axi_wlast		(	),
		.pixel_write_axi_wvalid		(	),
		.pixel_write_axi_wdata		(	),
		.pixel_write_axi_wready		(	),
		.pixel_write_axi_bid		(		),
		.pixel_write_axi_bresp		(	),
		.pixel_write_axi_bvalid		(	),
		.pixel_write_axi_bready		(	),

`ifndef SOFT_MEM_SLAVE
		.ddr3_dq		(ddr3_dq		),
		.ddr3_dqs_n		(ddr3_dqs_n		),
		.ddr3_dqs_p		(ddr3_dqs_p		),
		.ddr3_addr		(ddr3_addr		),
		.ddr3_ba		(ddr3_ba		),
		.ddr3_ras_n		(ddr3_ras_n		),
		.ddr3_cas_n		(ddr3_cas_n		),
		.ddr3_we_n		(ddr3_we_n		),
		.ddr3_reset_n	(ddr3_reset_n	),
		.ddr3_ck_p		(ddr3_ck_p		),
		.ddr3_ck_n		(ddr3_ck_n		),
		.ddr3_cke		(ddr3_cke		),
		.ddr3_cs_n		(ddr3_cs_n		),
		.ddr3_dm		(ddr3_dm		),
		.ddr3_odt		(ddr3_odt		),
		.sys_clk_p		(clk_in_p		),
		.sys_clk_n		(clk_in_n		),
		.sys_rst		(sys_rst		),
`endif
		.init_calib_complete(init_calib_complete),
		.design_clk(design_clk),
		.clk_200(clk_200),
		.calib_complete_reset(reset)
    );


// synthesis translate_off
inter_cache_pipe_hit_pipe cache_block

(
    .clk(design_clk),
    .reset(reset),

    .luma_ref_start_x_in         (luma_ref_start_x_in)  ,
    .luma_ref_start_y_in         (luma_ref_start_y_in)  ,
    .chma_ref_start_x_in         (chma_ref_start_x_in)  ,
    .chma_ref_start_y_in         (chma_ref_start_y_in)  ,
        
    .chma_ref_width_x_in         (chma_ref_width_x_in)  ,
    .luma_ref_height_y_in        (luma_ref_height_y_in)  ,
    .chma_ref_height_y_in        (chma_ref_height_y_in)  ,
    .luma_ref_width_x_in         (luma_ref_width_x_in)  ,

    .luma_ref_start_x_out      (    )              ,
    .luma_ref_start_y_out      (    )              ,
    .chma_ref_start_x_out      (    )              ,
    .chma_ref_start_y_out      (    )              ,
    .luma_ref_width_x_out      (    )             ,
    .chma_ref_width_x_out      (    )             ,
    .luma_ref_height_y_out     (   )             ,
    .chma_ref_height_y_out     (   )             ,

    .block_x_offset_luma        (     ) ,
    .block_y_offset_luma        (     ) ,
    .block_x_offset_chma        (     ) ,
    .block_y_offset_chma        (     ) ,
    .block_x_end_luma           (        ) ,
    .block_y_end_luma           (        ) ,
    .block_x_end_chma           (        ) ,
    .block_y_end_chma           (        ) ,

    .res_present_in         (res_present_in),
    .res_present_out        (),
    .bi_pred_block_cache_in (bi_pred_block_cache_in),
    .bi_pred_block_cache_out(),
    .x0_tu_end_in_min_tus_in (x0_tu_end_in_min_tus_in),
    .x0_tu_end_in_min_tus_out(),
    .y0_tu_end_in_min_tus_in (y0_tu_end_in_min_tus_in),
    .y0_tu_end_in_min_tus_out(),
    .cache_idle_out           (cache_idle_out)  ,
    
    .pic_width                ({3'd0,pic_width})  ,
    .pic_height               ({3'd0,pic_height})  ,
            
    .ch_frac_x                (ch_frac_x)  ,
    .ch_frac_y                (ch_frac_y)  ,
    .ch_frac_x_out            ()  ,
    .ch_frac_y_out            (),           
    .xT_in_min_tus_in         (xT_in_min_tus_in)  ,
    .yT_in_min_tus_in         (yT_in_min_tus_in)  ,
    .xT_in_min_tus_out        ()  ,
    .yT_in_min_tus_out        ()  ,

    .filer_idle_in            (1 )  ,
    .ref_idx_in_in            (ref_idx_in_in)  ,
    .valid_in                 (valid_in)  ,
    .block_121_out            ( )  ,
    .block_25cb_out           ()  ,
    .block_25cr_out           ()  ,
    .block_ready              ()  ,
            
    .ref_pix_axi_ar_addr      (ref_pix_axi_ar_addr   )  ,
    .ref_pix_axi_ar_len       (ref_pix_axi_ar_len    )  ,
    .ref_pix_axi_ar_size      (ref_pix_axi_ar_size   )  ,
    .ref_pix_axi_ar_burst     (ref_pix_axi_ar_burst  )  ,
    .ref_pix_axi_ar_prot      (ref_pix_axi_ar_prot   )  ,   
    .ref_pix_axi_ar_valid     (ref_pix_axi_ar_valid  )  ,
    .ref_pix_axi_ar_ready     (ref_pix_axi_ar_ready  )  ,
    .ref_pix_axi_r_data       (ref_pix_axi_r_data    )  ,
    .ref_pix_axi_r_resp       (ref_pix_axi_r_resp    )  ,
    .ref_pix_axi_r_last       (ref_pix_axi_r_last    )  ,
    .ref_pix_axi_r_valid      (ref_pix_axi_r_valid   )  ,
    .ref_pix_axi_r_ready      (ref_pix_axi_r_ready   )
);
// synthesis translate_on




endmodule

