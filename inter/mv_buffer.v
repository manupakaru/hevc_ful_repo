`timescale 1ns / 1ps
module mv_buffer(
    clk,
    reset,
    enable,
    current_pic_dpb_idx,
   // temporal_enable,
    
    bs_left_valid_feed_in,
    bs_top_valid_feed_in,
    
    x_N_pu_in ,
    y_N_pu_in,
    available_flag_in,
    mv_read_from_top,
    is_cand_in_prev_pu_in,
    cand_x_prev_top_left_in,
    
    mv_field_pred_flag_l0_out,
    mv_field_pred_flag_l1_out,
    mv_field_ref_idx_l0_out,
    mv_field_ref_idx_l1_out,
    mv_field_mv_x_l0_out,
    mv_field_mv_y_l0_out,
    mv_field_mv_x_l1_out,
    mv_field_mv_y_l1_out,
    

    
    x_P_pu_start_in,
    x_P_pu_end_in,
    y_P_pu_start_in,
    y_P_pu_end_in,
    pu_mv_buffer_done_out,
    
    prev_mv_field_pred_flag_l0_in,
    prev_mv_field_pred_flag_l1_in,
    prev_mv_field_ref_idx_l0_in,
    prev_mv_field_ref_idx_l1_in,
    prev_mv_field_mv_x_l0_in,
    prev_mv_field_mv_x_l1_in,
    prev_mv_field_mv_y_l0_in,
    prev_mv_field_mv_y_l1_in,
    prev_mv_field_valid_in,
	prev_mv_field_valid_now,
	prev_mv_field_dpb_idx_l0_in,    
	prev_mv_field_dpb_idx_l1_in,

	
    mv_col_axi_awid,     
    mv_col_axi_awlen,    
    mv_col_axi_awsize,   
    mv_col_axi_awburst,  
    mv_col_axi_awlock,   
    mv_col_axi_awcache,  
    mv_col_axi_awprot,   
    mv_col_axi_awvalid,
    mv_col_axi_awaddr,
    mv_col_axi_awready,
    mv_col_axi_wstrb,
    mv_col_axi_wlast,
    mv_col_axi_wvalid,
    mv_col_axi_wdata,
    mv_col_axi_wready,
    // mv_col_axi_bid,
    mv_col_axi_bresp,
    mv_col_axi_bvalid,
    mv_col_axi_bready,    

    last_pb_of_ctu_in,              // send this when all the motion vectors are coded
    new_ctu_start_in,           // sent to mark the start of a ctu, before sending any of the pu info
    ctu_write_done_out,
    col_write_done_out,
	
    top_mvs_to_bs_out,
    left_mvs_to_bs_out,
    x_rel_top_out,
    y_rel_left_out,
    bs_left_valid_out,
    bs_top_valid_out,
    
    pic_width_in_pu,
    log2_ctb_size_in
	,col_state_axi_write
    // is_now_cand_b1_in
    
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"
    `include "../sim/inter_axi_def.v"  // always define below pred_def
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------

    parameter                   MV_COL_AXI_AX_SIZE  = `AX_SIZE_64;
    parameter                   MV_COL_AXI_AX_LEN   = `AX_LEN_4;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam  STATE_LINE_WRITE_IDLE = 0;
    localparam  STATE_LINE_WRITE_BS_MV_MOVE = 1;
    localparam  STATE_LINE_WRITE_CONRNER_MV_MOVE = 3;
    localparam  STATE_LINE_WRITE_BS_MV_MOVE_WAIT = 2;
    localparam  STATE_LINE_WRITE_REST_MV_MOVE = 4;
    localparam  STATE_CORNER_REINSTATE = 5;
    localparam  STATE_LINE_WRITE_BS_TOP_END_MOVE = 6;
    
    localparam  STATE_COL_WRITE_IDLE = 0;
    localparam  STATE_COL_WRITE_START = 1;
    
    localparam  STATE_AXI_WRITE_WAIT = 0;
    localparam  STATE_AXI_WRITE_ADDRESS_SEND = 1;
    localparam  STATE_AXI_WRITE_DATA_SEND = 2;
    localparam  STATE_AXI_WRITE_DATA_ACK = 3;
    
    
    
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
    input enable;
  //  input   temporal_enable;
  
    input bs_left_valid_feed_in;
    input bs_top_valid_feed_in;
    input   [DPB_ADDR_WIDTH -1: 0]                  current_pic_dpb_idx;

    input   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   x_N_pu_in;
    input   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   y_N_pu_in;
    input                                           available_flag_in;
    input                                           mv_read_from_top;
    input                                           is_cand_in_prev_pu_in;
    input                                           cand_x_prev_top_left_in;
    
    
    output reg                           mv_field_pred_flag_l0_out;
    output reg                           mv_field_pred_flag_l1_out;
    output reg [REF_IDX_LX_WIDTH -1:0]   mv_field_ref_idx_l0_out;
    output reg [REF_IDX_LX_WIDTH -1:0]   mv_field_ref_idx_l1_out;
    output reg [MVD_WIDTH -1:0]          mv_field_mv_x_l0_out;
    output reg [MVD_WIDTH -1:0]          mv_field_mv_y_l0_out;
    output reg [MVD_WIDTH -1:0]          mv_field_mv_x_l1_out;
    output reg [MVD_WIDTH -1:0]          mv_field_mv_y_l1_out;

    
    input                           prev_mv_field_pred_flag_l0_in;
    input                           prev_mv_field_pred_flag_l1_in;
    input [REF_IDX_LX_WIDTH -1:0]   prev_mv_field_ref_idx_l0_in;
    input [REF_IDX_LX_WIDTH -1:0]   prev_mv_field_ref_idx_l1_in;
    input [MVD_WIDTH -1:0]          prev_mv_field_mv_x_l0_in;
    input [MVD_WIDTH -1:0]          prev_mv_field_mv_y_l0_in;
    input [MVD_WIDTH -1:0]          prev_mv_field_mv_x_l1_in;
    input [MVD_WIDTH -1:0]          prev_mv_field_mv_y_l1_in;
    input                           prev_mv_field_valid_in;
    input                           prev_mv_field_valid_now;
    input [DPB_ADDR_WIDTH -1:0]     prev_mv_field_dpb_idx_l0_in;    
    input [DPB_ADDR_WIDTH -1:0]     prev_mv_field_dpb_idx_l1_in; 
	
    input   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   x_P_pu_start_in;
    input   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   x_P_pu_end_in;
    input   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   y_P_pu_start_in;
    input   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   y_P_pu_end_in;  
    output reg  pu_mv_buffer_done_out;
    
    output [MV_FIELD_DATA_WIDTH -1:0]   top_mvs_to_bs_out;
    output [MV_FIELD_DATA_WIDTH -1:0]   left_mvs_to_bs_out;
    output reg [CTB_SIZE_WIDTH -1:0]    x_rel_top_out;
    output reg [CTB_SIZE_WIDTH -1:0]    y_rel_left_out;
    output bs_left_valid_out;
    output bs_top_valid_out;
    
    input       [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE - 1:0]            pic_width_in_pu;
    // input       is_now_cand_b1_in;
    input       [LOG2_CTB_WIDTH - 1: 0]                             log2_ctb_size_in;
    
	 // write address channel
     input                                      last_pb_of_ctu_in;
     input                                      new_ctu_start_in;
     output  reg                                ctu_write_done_out;
     output                                     col_write_done_out;
	 output                                     mv_col_axi_awid    ;// = 0;
	 output      [7:0]                          mv_col_axi_awlen   ;// = MV_COL_AXI_AX_LEN;
	 output      [2:0]                          mv_col_axi_awsize  ;// = MV_COL_AXI_AX_SIZE;
	 output      [1:0]                          mv_col_axi_awburst ;// = `AX_BURST_INC;
	 output                        	            mv_col_axi_awlock  ;// = `AX_LOCK_DEFAULT;
	 output      [3:0]                          mv_col_axi_awcache ;// = `AX_CACHE_DEFAULT;
	 output      [2:0]                          mv_col_axi_awprot  ;// = `AX_PROT_DATA;
	 output reg                                 mv_col_axi_awvalid;
     output reg  [AXI_ADDR_WDTH-1:0]            mv_col_axi_awaddr;

	 input                       	            mv_col_axi_awready;
	 
	 // write data channel
	 output      [64-1:0]                       mv_col_axi_wstrb;
	 output reg                                 mv_col_axi_wlast;
	 output reg                                 mv_col_axi_wvalid;
	 output      [MV_COL_AXI_DATA_WIDTH -1:0]   mv_col_axi_wdata;
	 
	 output [7:0] col_state_axi_write;
     
	 input	                                    mv_col_axi_wready;
	 
	 //write response channel
	 // input                       	            mv_col_axi_bid;
	 input       [1:0]                          mv_col_axi_bresp;
	 input                       	            mv_col_axi_bvalid;
	 output  reg                                mv_col_axi_bready;    
    

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    integer                         state_write_line;
    integer                         state_col_write;
    integer                         state_axi_write;
    
    reg   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   x_P_pu_start_in_d;
    reg   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   x_P_pu_end_in_d;
    reg   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   x_P_pu_end_in_d_mv_buf_neibr_check;
    reg   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   x_P_pu_end_in_always_d;
    // reg   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   x_P_pu_end_in_2d;
    reg   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   y_P_pu_start_in_d;
    reg   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   y_P_pu_end_in_d;  
    
    reg   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   last_greatest_xP_end_in_pu_1;  
    reg   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   last_greatest_xP_end_in_pu_1_st_2;  
    reg   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   last_greatest_xP_end_in_pu_1_st_3;  
    reg   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]   last_greatest_xP_end_in_pu_1_st_4;  
    
    reg                           prev_mv_field_pred_flag_l0;
    reg                           prev_mv_field_pred_flag_l1;
    reg [REF_IDX_LX_WIDTH -1:0]   prev_mv_field_ref_idx_l0;
    reg [REF_IDX_LX_WIDTH -1:0]   prev_mv_field_ref_idx_l1;
    reg [MVD_WIDTH -1:0]          prev_mv_field_mv_x_l0;
    reg [MVD_WIDTH -1:0]          prev_mv_field_mv_y_l0;
    reg [MVD_WIDTH -1:0]          prev_mv_field_mv_x_l1;
    reg [MVD_WIDTH -1:0]          prev_mv_field_mv_y_l1;
	reg [DPB_ADDR_WIDTH -1:0]     prev_mv_field_dpb_idx_l0;    
	reg [DPB_ADDR_WIDTH -1:0]     prev_mv_field_dpb_idx_l1; 

    
    reg                                             mv_top_wr_en;
    reg   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]     mv_top_r_addr;
    reg   [X11_ADDR_WDTH + 1 - LOG2_MIN_PU_SIZE- 1:0]     mv_top_w_addr;
    reg   [MV_FIELD_DATA_WIDTH + DPB_ADDR_WIDTH*2-1:0]                mv_field_top_in;
    wire  [MV_FIELD_DATA_WIDTH + DPB_ADDR_WIDTH*2-1:0]                mv_field_top_out;
 
    reg                                             mv_left_wr_en;
    reg   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]     mv_left_r_addr;
    reg   [X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]     mv_left_w_addr;
    reg   [MV_FIELD_DATA_WIDTH + DPB_ADDR_WIDTH*2-1:0]                mv_field_left_in;
    reg   [MV_FIELD_DATA_WIDTH + DPB_ADDR_WIDTH*2-1:0]                mv_field_left_to_top_move;
    wire  [MV_FIELD_DATA_WIDTH + DPB_ADDR_WIDTH*2-1:0]                mv_field_left_out; 
    
    reg                                             is_cand_in_prev_pu_d;
    reg                                             mv_read_from_top_d;
    
    reg                                             top_line_done;
    reg                                             left_line_done;
    reg                                             col_write_done;
    reg                                             line_write_done;
    
    wire    [MV_FIELD_AXI_DATA_WIDTH *4- 1:0]                       col_mv_ctu_buffer_rdata;
    reg     [MV_FIELD_AXI_DATA_WIDTH    -1:0]                       col_mv_ctu_buffer_wdata;
    //[0 : (1<<(CTB_SIZE_WIDTH - LOG2_MIN_COLLOCATE_SIZE)) -1][0 : (1<<(CTB_SIZE_WIDTH - LOG2_MIN_COLLOCATE_SIZE)) -1];
    reg     [(CTB_SIZE_WIDTH - LOG2_MIN_COLLOCATE_SIZE) -1:0]   col_x_addr;
    reg     [(CTB_SIZE_WIDTH - LOG2_MIN_COLLOCATE_SIZE) -1:0]   col_y_addr;
    
    wire    [(CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE) -1:0]   x_P_pu_end_in_1 = ((x_P_pu_end_in -1));//%(1<<(CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE));
    wire    [(CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE) -1:0]   y_P_pu_end_in_1 = ((y_P_pu_end_in -1));//%(1<<(CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE));
    
    wire    [(LOG2_MIN_COLLOCATE_SIZE - LOG2_MIN_PU_SIZE) -1:0]       inner_col_block_start_x = x_P_pu_start_in[(LOG2_MIN_COLLOCATE_SIZE - LOG2_MIN_PU_SIZE) -1:0];
    // wire    [(CTB_SIZE_WIDTH - LOG2_MIN_COLLOCATE_SIZE) -1:0]       inner_col_block_end_x = x_P_pu_end_in_1[(CTB_SIZE_WIDTH - LOG2_MIN_COLLOCATE_SIZE) -1:0];
    wire    [(LOG2_MIN_COLLOCATE_SIZE - LOG2_MIN_PU_SIZE) -1:0]       inner_col_block_start_y = y_P_pu_start_in[(LOG2_MIN_COLLOCATE_SIZE - LOG2_MIN_PU_SIZE) -1:0];
    // wire    [(CTB_SIZE_WIDTH - LOG2_MIN_COLLOCATE_SIZE) -1:0]       inner_col_block_end_y = y_P_pu_end_in_1[(CTB_SIZE_WIDTH - LOG2_MIN_COLLOCATE_SIZE) -1:0];
 
 
    reg    [(LOG2_MIN_COLLOCATE_SIZE - LOG2_MIN_PU_SIZE) -1:0]     col_block_start_x_idx 	;//	= x_P_pu_start_in[(CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE) -1:(LOG2_MIN_COLLOCATE_SIZE - LOG2_MIN_PU_SIZE)];
    reg    [(LOG2_MIN_COLLOCATE_SIZE - LOG2_MIN_PU_SIZE) -1:0]     col_block_end_x_idx 		;//	= x_P_pu_end_in_1[(CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE) -1:(LOG2_MIN_COLLOCATE_SIZE - LOG2_MIN_PU_SIZE)];
    reg    [(LOG2_MIN_COLLOCATE_SIZE - LOG2_MIN_PU_SIZE) -1:0]     col_block_start_y_idx 	;//	= y_P_pu_start_in[(CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE) -1:(LOG2_MIN_COLLOCATE_SIZE - LOG2_MIN_PU_SIZE)];    
    reg    [(LOG2_MIN_COLLOCATE_SIZE - LOG2_MIN_PU_SIZE) -1:0]     col_block_end_y_idx 		;//	= y_P_pu_end_in_1[(CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE) -1:(LOG2_MIN_COLLOCATE_SIZE - LOG2_MIN_PU_SIZE)];    
    
    reg    [X11_ADDR_WDTH - CTB_SIZE_WIDTH -1:0] x_ctb_addr;// = x_P_pu_start_in[X11_ADDR_WDTH - LOG2_MIN_PU_SIZE -1: CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE];
    reg    [X11_ADDR_WDTH - CTB_SIZE_WIDTH -1:0] y_ctb_addr;//= y_P_pu_start_in[X11_ADDR_WDTH - LOG2_MIN_PU_SIZE -1: CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE];

    reg [31:0] axi_frame_offet;
    reg [31:0] axi_ctu_row_offset;
    reg [31:0] axi_ctu_offset;    
    reg [2-1:0] mv_row_offset_wire;
    reg [2-1:0] mv_row_offset;
    
    reg [MV_FIELD_AXI_DATA_WIDTH * 4 - 1:0]   mv_col_axi_wdata_d;
    reg [3:0] col_mv_buffer_w_addr;
    reg col_write_en;
    
    reg end_of_ctu_pic;
    reg is_now_cand_b2;
    reg is_now_cand_b1;
    reg is_now_cand_b0;
    
    reg pull_next_cycle;
    
    reg signed   [X11_ADDR_WDTH + 1 - LOG2_MIN_PU_SIZE- 1:0]   x_P_pu_end_in_d_minus_2 ;
    
    reg cand_x_prev_top_left_d;


    reg bs_send_left_minus_1_as_top_last;
    
    reg bs_left_valid_feed_d;
    reg bs_top_valid_feed_d;
    reg available_flag_d;
    
    reg bs_left_valid;
    reg bs_top_valid;
	
	wire  [MV_FIELD_DATA_WIDTH -1:0]                mv_field_left_bs; 
	wire  [MV_FIELD_DATA_WIDTH -1:0]                mv_field_top_bs; 
	
    wire                           top_pred_flag_l0_out;
    wire                           top_pred_flag_l1_out;
    wire [REF_IDX_LX_WIDTH -1:0]   top_ref_idx_l0_out;
    wire [REF_IDX_LX_WIDTH -1:0]   top_ref_idx_l1_out;
    wire [DPB_ADDR_WIDTH -1:0]     top_dpb_idx_l0_out;
    wire [DPB_ADDR_WIDTH -1:0]     top_dpb_idx_l1_out;
    wire [MVD_WIDTH -1:0]          top_mv_x_l0_out;
    wire [MVD_WIDTH -1:0]          top_mv_y_l0_out;
    wire [MVD_WIDTH -1:0]          top_mv_x_l1_out;
    wire [MVD_WIDTH -1:0]          top_mv_y_l1_out;

    wire                           left_pred_flag_l0_out;
    wire                           left_pred_flag_l1_out;
    wire [REF_IDX_LX_WIDTH -1:0]   left_ref_idx_l0_out;
    wire [REF_IDX_LX_WIDTH -1:0]   left_ref_idx_l1_out;
    wire [DPB_ADDR_WIDTH -1:0]     left_dpb_idx_l0_out;
    wire [DPB_ADDR_WIDTH -1:0]     left_dpb_idx_l1_out;
    wire [MVD_WIDTH -1:0]          left_mv_x_l0_out;
    wire [MVD_WIDTH -1:0]          left_mv_y_l0_out;
    wire [MVD_WIDTH -1:0]          left_mv_x_l1_out;
    wire [MVD_WIDTH -1:0]          left_mv_y_l1_out;
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

    //todo: form axi write interface for collocated motion vectors. data width 512 to burst fist 4 MVs of CTU need 4 of such bursts - Col mv offset = 2^25, col_mv_frame_offset = 2^17 get ctu_x_offset and ctu_y_offset from prev_mv
    //-------done
    //Collocated MVs expect to write motion vector related to CUs that are coded INTRA, to achieve this on every the two pred_flag for collocated MV buffer is kept as registers and cleared at every ctu cycle
    
    
    assign bs_left_valid_out = available_flag_d ? 1'b0: bs_left_valid;
    assign bs_top_valid_out = available_flag_d ? 1'b0: bs_top_valid;
    
    assign col_write_done_out = col_write_done;
    col_mv_buffer_mem col_buffer_block (
    .clk(clk), 
    .clear_pred_flags(new_ctu_start_in),
    .r_addr_in(mv_row_offset_wire), 
    .w_addr_in(col_mv_buffer_w_addr), 
    .r_data_out(col_mv_ctu_buffer_rdata), 
    .w_data_in(col_mv_ctu_buffer_wdata), 
    .w_en_in(col_write_en), 
    .enable(enable)
    );

`ifdef CHIPSCOPE_DEBUG
    assign col_state_axi_write = state_axi_write;
`endif
    
	 assign                 mv_col_axi_awid     = 0;
	 assign                 mv_col_axi_awlen    = MV_COL_AXI_AX_LEN;
	 assign                 mv_col_axi_awsize   = MV_COL_AXI_AX_SIZE;
	 assign                 mv_col_axi_awburst  = `AX_BURST_INC;
	 assign    	            mv_col_axi_awlock   = `AX_LOCK_DEFAULT;
	 assign                 mv_col_axi_awcache  = `AX_CACHE_DEFAULT;
	 assign                 mv_col_axi_awprot   = `AX_PROT_DATA;    
     assign                 mv_col_axi_wstrb = 64'hFFFFF_FFFFF_FFFFF;       // 40 bytes
    
    assign mv_col_axi_wdata = {192'd0,mv_col_axi_wdata_d};
    
    wire stack_update_cond = x_P_pu_end_in_always_d != x_P_pu_end_in;
    reg stack_update_cond_d;
    
    always@(posedge clk) begin
        stack_update_cond_d <= stack_update_cond;
    end
    
    always@(posedge clk) begin
        if (reset) begin
            pull_next_cycle <= 0;
        end
        else begin
            if(stack_update_cond_d) begin
                if((x_P_pu_end_in -1)== last_greatest_xP_end_in_pu_1) begin
                    pull_next_cycle <= 1;
                end
                else begin
                    pull_next_cycle <= 0;
                end                
            end
        end
    end
    
 assign {
		 left_dpb_idx_l0_out    ,
		 left_dpb_idx_l1_out	,
		 left_pred_flag_l0_out	,
		 left_pred_flag_l1_out	,
		 left_ref_idx_l0_out	,
		 left_ref_idx_l1_out	,
		 left_mv_x_l0_out		,
		 left_mv_y_l0_out		,
		 left_mv_x_l1_out		,
		 left_mv_y_l1_out		
		 
		 } =    mv_field_left_out;
		 
 assign {
		 top_dpb_idx_l0_out    	,
		 top_dpb_idx_l1_out		,
		 top_pred_flag_l0_out	,
		 top_pred_flag_l1_out	,
		 top_ref_idx_l0_out		,
		 top_ref_idx_l1_out		,
		 top_mv_x_l0_out		,
		 top_mv_y_l0_out		,
		 top_mv_x_l1_out		,
		 top_mv_y_l1_out		
		 
		 } =    mv_field_top_out;

	assign 	 mv_field_top_bs = {top_pred_flag_l0_out	,
	                            top_pred_flag_l1_out	,
	                            top_dpb_idx_l0_out		,
	                            top_dpb_idx_l1_out		,
	                            top_mv_x_l0_out			,
	                            top_mv_y_l0_out			,
	                            top_mv_x_l1_out			,
	                            top_mv_y_l1_out	} ;

	assign 	 mv_field_left_bs = {left_pred_flag_l0_out	,
	                            left_pred_flag_l1_out	,
	                            left_dpb_idx_l0_out		,
	                            left_dpb_idx_l1_out		,
	                            left_mv_x_l0_out			,
	                            left_mv_y_l0_out			,
	                            left_mv_x_l1_out			,
	                            left_mv_y_l1_out	} ;
								
    assign top_mvs_to_bs_out  = ((state_write_line== STATE_LINE_WRITE_BS_TOP_END_MOVE) && bs_send_left_minus_1_as_top_last) ? mv_field_left_bs:mv_field_top_bs;
    assign left_mvs_to_bs_out = mv_field_left_bs;

    always@(posedge clk) begin
        x_ctb_addr <= x_P_pu_start_in[X11_ADDR_WDTH - LOG2_MIN_PU_SIZE -1: CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE];
        y_ctb_addr <= y_P_pu_start_in[X11_ADDR_WDTH - LOG2_MIN_PU_SIZE -1: CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE];
    end
    
    always@(*) begin
        pu_mv_buffer_done_out = col_write_done & line_write_done;
    end
    always@(posedge clk) begin
        is_cand_in_prev_pu_d <= is_cand_in_prev_pu_in;
        mv_read_from_top_d <= mv_read_from_top;
        cand_x_prev_top_left_d <= cand_x_prev_top_left_in;
        col_mv_buffer_w_addr <= (col_y_addr + col_x_addr *4)%16;      // set col write address one cycle after col_x_addr and col_y_addr
    end
    

    always@(posedge clk) begin
        axi_frame_offet <= `COL_MV_ADDR_OFFSET + ((`COL_MV_FRAME_OFFSET*current_pic_dpb_idx));//%(1<<31));
    end
    always@(posedge clk) begin
        axi_ctu_row_offset <= (axi_frame_offet + `COL_MV_CTU_ROW_OFFSET* y_ctb_addr);//%(1<<(31));;
    end
    always@(posedge clk) begin
        axi_ctu_offset <= (axi_ctu_row_offset + `COL_MV_CTU_OFFSET * x_ctb_addr);//%(1<<(31));
    end
    
    always@(posedge clk) begin
        if(reset) begin
            x_P_pu_end_in_always_d <= 0;
        end
        else begin
            x_P_pu_end_in_always_d <= x_P_pu_end_in;
        end
    end
    
    always@(posedge clk) begin
        if(reset) begin
            x_P_pu_end_in_d_mv_buf_neibr_check <= 0;
        end
        else begin
            if(prev_mv_field_valid_in) begin
                x_P_pu_end_in_d_mv_buf_neibr_check <= x_P_pu_end_in;
            end
        end
    end
    
    
    // LOG2CTB size assumed to be constant 64
    
    always@(posedge clk) begin
        if(reset) begin
                    last_greatest_xP_end_in_pu_1 <= 0;
                    last_greatest_xP_end_in_pu_1_st_2 <= 0;
                    last_greatest_xP_end_in_pu_1_st_3 <= 0;
                    last_greatest_xP_end_in_pu_1_st_4 <= 0;
        end
        else begin
            if(x_P_pu_end_in_always_d != x_P_pu_end_in) begin
                //if(x_P_pu_start_in == 0 && (!(|y_P_pu_start_in[CTB_SIZE_WIDTH -LOG2_MIN_PU_SIZE -1:0]))) begin      // reset condition
                    // last_greatest_xP_end_in_pu_1 <= 0;
                    // last_greatest_xP_end_in_pu_1_st_2 <= 0;
                    // last_greatest_xP_end_in_pu_1_st_3 <= 0;
                    // last_greatest_xP_end_in_pu_1_st_4 <= 0;
                //end
                //else 
                if((x_P_pu_end_in_d_mv_buf_neibr_check > x_P_pu_end_in) ) begin
                    if( ((x_P_pu_end_in_always_d -1) !=last_greatest_xP_end_in_pu_1)) begin
                        last_greatest_xP_end_in_pu_1 <= x_P_pu_end_in_always_d -1; // push condition
                        //if(last_greatest_xP_end_in_pu_1 != last_greatest_xP_end_in_pu_1_st_2) begin
                            last_greatest_xP_end_in_pu_1_st_2 <= last_greatest_xP_end_in_pu_1;
                        //end
                        //if(last_greatest_xP_end_in_pu_1_st_2 != last_greatest_xP_end_in_pu_1_st_3) begin
                            last_greatest_xP_end_in_pu_1_st_3 <= last_greatest_xP_end_in_pu_1_st_2;
                        //end
                        //if(last_greatest_xP_end_in_pu_1_st_3 != last_greatest_xP_end_in_pu_1_st_4) begin
                            last_greatest_xP_end_in_pu_1_st_4 <= last_greatest_xP_end_in_pu_1_st_3;
                        //end
                    end
                end                
                else if(pull_next_cycle)begin  // pull condition
                    // if(last_greatest_xP_end_in_pu_1_st_2 > last_greatest_xP_end_in_pu_1) begin
                        last_greatest_xP_end_in_pu_1 <= last_greatest_xP_end_in_pu_1_st_2;
                    // end
                    // if(last_greatest_xP_end_in_pu_1_st_3 > last_greatest_xP_end_in_pu_1_st_2) begin
                        last_greatest_xP_end_in_pu_1_st_2 <= last_greatest_xP_end_in_pu_1_st_3;
                    // end
                    // if(last_greatest_xP_end_in_pu_1_st_4 > last_greatest_xP_end_in_pu_1_st_3) begin
                        last_greatest_xP_end_in_pu_1_st_3 <= last_greatest_xP_end_in_pu_1_st_4;
                    // end
                    
                end
            end
        end
    end
    
    always@(*) begin
        if((x_P_pu_start_in -1 == x_N_pu_in) && (y_P_pu_start_in -1 == y_N_pu_in)) begin
            is_now_cand_b2 <= 1;
        end
        else begin
            is_now_cand_b2 <= 0;
        end
        
    end
 
    always@(*) begin
        if((x_P_pu_end_in -1 == x_N_pu_in) && (y_P_pu_start_in -1 == y_N_pu_in)) begin
            is_now_cand_b1 <= 1;
        end
        else begin
            is_now_cand_b1 <= 0;
        end
        
    end

    always@(*) begin
        if((x_P_pu_end_in == x_N_pu_in) && (y_P_pu_start_in -1 == y_N_pu_in)) begin
            is_now_cand_b0 <= 1;
        end
        else begin
            is_now_cand_b0 <= 0;
        end
        
    end    
    
    always@(posedge clk) begin
        if(is_now_cand_b1) begin
            if((x_N_pu_in == pic_width_in_pu -1) || (( x_N_pu_in == last_greatest_xP_end_in_pu_1))) begin  // assuming last_greatest_xP_end_in_pu_1 check does not come immediately after it was set
                end_of_ctu_pic <= 1;
            end
            else begin
                end_of_ctu_pic <= 0;
            end        
        end
        else if(is_now_cand_b2) begin
            end_of_ctu_pic <= 0;
        end
        else if(is_now_cand_b0) begin
            if(( x_N_pu_in == last_greatest_xP_end_in_pu_1)) begin
                end_of_ctu_pic <= 1;
            end
            else begin
                end_of_ctu_pic <= 0;
            end
        end
    end
    
    
    always@(posedge clk) begin
        if (reset) begin
            state_axi_write <= STATE_AXI_WRITE_WAIT;
            ctu_write_done_out <= 1;
            mv_col_axi_awvalid <= 0;
            mv_col_axi_wvalid <= 0;
            mv_col_axi_bready <= 0;
        end
        else if(enable) begin
            case(state_axi_write)
                STATE_AXI_WRITE_WAIT: begin
                    if (last_pb_of_ctu_in) begin
                        state_axi_write <= STATE_AXI_WRITE_ADDRESS_SEND;
                        ctu_write_done_out <= 0;
                        mv_col_axi_awaddr <= axi_ctu_offset;
                        mv_col_axi_awvalid <= 1;  
                        mv_row_offset <= 0;
                    end
                    else begin
                        ctu_write_done_out <= 1;
                        mv_col_axi_awvalid <= 0;
                        mv_col_axi_bready <= 0;
                    end
                end
                STATE_AXI_WRITE_ADDRESS_SEND: begin
                    if(mv_col_axi_awready) begin
                        mv_col_axi_awvalid <= 0;
                        mv_col_axi_wlast <= 0;
                        state_axi_write <= STATE_AXI_WRITE_DATA_SEND;
                    end
                end
                STATE_AXI_WRITE_DATA_SEND: begin
                    if(mv_col_axi_wready) begin
                        mv_row_offset <= (mv_row_offset + 1'b1)%4;
                        mv_col_axi_wvalid <= 1;
                        mv_col_axi_wdata_d <= {  col_mv_ctu_buffer_rdata};                      
                        if(mv_row_offset == 3) begin
                            mv_col_axi_wlast <= 1;
                            state_axi_write <= STATE_AXI_WRITE_DATA_ACK;
							mv_col_axi_bready <= 1;
                        end
                        else begin
                            mv_col_axi_wlast <= 0;
                        end
                    end
                end
                STATE_AXI_WRITE_DATA_ACK: begin
					if(mv_col_axi_wready) begin
						mv_col_axi_wvalid <= 0;
					end
                    
                    if(mv_col_axi_bvalid) begin
                        if(mv_col_axi_bresp == `XRESP_SLAV_ERROR) begin
                            state_axi_write <= STATE_AXI_WRITE_ADDRESS_SEND;
                            ctu_write_done_out <= 0;
                            mv_col_axi_awvalid <= 1;  
                            mv_row_offset <= 0;                            
                        end
                        else begin
                            state_axi_write <= STATE_AXI_WRITE_WAIT;
                            //todo analysis write response
                            ctu_write_done_out <= 1;                        
                        end
                    end
                end
            endcase
        end
    end
    
    always@(*) begin
		mv_row_offset_wire = mv_row_offset;
		case(state_axi_write)
			STATE_AXI_WRITE_WAIT: begin

			end
			STATE_AXI_WRITE_ADDRESS_SEND: begin
				if(mv_col_axi_awready) begin
				end
			end
			STATE_AXI_WRITE_DATA_SEND: begin
				if(mv_col_axi_wready) begin
					mv_row_offset_wire = (mv_row_offset + 1'b1)%4;
				end
			end
			STATE_AXI_WRITE_DATA_ACK: begin
			end
		endcase
    end
    
        
    
    always@(posedge clk) begin
        if(reset) begin
            state_col_write <= STATE_COL_WRITE_IDLE;
            col_write_done <= 1;
            col_write_en <= 0;

        end
        else begin
            case(state_col_write)
                STATE_COL_WRITE_IDLE: begin
                    col_write_en <= 0;
                    if (prev_mv_field_valid_now) begin
                        state_col_write <= STATE_COL_WRITE_START;
                        col_x_addr <= x_P_pu_start_in[(CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE) -1:(LOG2_MIN_COLLOCATE_SIZE - LOG2_MIN_PU_SIZE)];
                        col_y_addr <= y_P_pu_start_in[(CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE) -1:(LOG2_MIN_COLLOCATE_SIZE - LOG2_MIN_PU_SIZE)];
						col_block_start_x_idx 		<= x_P_pu_start_in[(CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE) -1:(LOG2_MIN_COLLOCATE_SIZE - LOG2_MIN_PU_SIZE)];
						col_block_end_x_idx 		<= x_P_pu_end_in_1[(CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE) -1:(LOG2_MIN_COLLOCATE_SIZE - LOG2_MIN_PU_SIZE)];
						col_block_start_y_idx 		<= y_P_pu_start_in[(CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE) -1:(LOG2_MIN_COLLOCATE_SIZE - LOG2_MIN_PU_SIZE)];    
						col_block_end_y_idx 		<= y_P_pu_end_in_1[(CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE) -1:(LOG2_MIN_COLLOCATE_SIZE - LOG2_MIN_PU_SIZE)];    
						
						col_write_done <= 0;
    
                    end
                    else begin
                        col_write_done <= 1;
                    end
                end
                STATE_COL_WRITE_START: begin
                    if(col_x_addr == col_block_end_x_idx) begin         // any reason why it was like this? // if(col_x_addr == col_block_end_y_idx) begin   
                        col_x_addr <= col_block_start_x_idx;
                        col_y_addr <= (col_y_addr + 1)%(1<<((CTB_SIZE_WIDTH - LOG2_MIN_COLLOCATE_SIZE)));
                    end
                    else begin
                        col_x_addr <= (col_x_addr + 1)%(1<<((CTB_SIZE_WIDTH - LOG2_MIN_COLLOCATE_SIZE)));
                    end
                    if(col_x_addr == col_block_end_x_idx && col_y_addr == col_block_end_y_idx) begin        
                        state_col_write <= STATE_COL_WRITE_IDLE;
                    end
					if(prev_mv_field_valid_in ==0 ) begin
					                                col_mv_ctu_buffer_wdata <=                  {
                                                                                prev_mv_field_pred_flag_l0   ,
                                                                                prev_mv_field_pred_flag_l1   ,
                                                                                prev_mv_field_ref_idx_l0     ,
                                                                                prev_mv_field_ref_idx_l1     ,
                                                                                prev_mv_field_mv_x_l0        ,
                                                                                prev_mv_field_mv_y_l0        ,
                                                                                prev_mv_field_mv_x_l1        ,
                                                                                prev_mv_field_mv_y_l1        ,
                                                                                prev_mv_field_mv_x_l0[5:0]//6'd0
                                                                            };
					end
					else begin
					                                col_mv_ctu_buffer_wdata <=                  {
                                                                                prev_mv_field_pred_flag_l0_in   ,
                                                                                prev_mv_field_pred_flag_l1_in   ,
                                                                                prev_mv_field_ref_idx_l0_in     ,
                                                                                prev_mv_field_ref_idx_l1_in     ,
                                                                                prev_mv_field_mv_x_l0_in        ,
                                                                                prev_mv_field_mv_y_l0_in        ,
                                                                                prev_mv_field_mv_x_l1_in        ,
                                                                                prev_mv_field_mv_y_l1_in        ,
                                                                                prev_mv_field_mv_x_l0_in[5:0]//6'd0
                                                                            };					
					end
                    case({col_x_addr == col_block_start_x_idx,col_y_addr == col_block_start_y_idx})
                        2'b11: begin
                            if (inner_col_block_start_x == 0 && inner_col_block_start_y == 0) begin
                                col_write_en <=1;
                            end
                            else begin
                                col_write_en <=0;
                            end
                        end
                        2'b10: begin
                            if(inner_col_block_start_x == 0) begin
                                col_write_en <= 1;
                            end
                            else begin
                                col_write_en <=0;
                            end
                        end
                        2'b01: begin
                            if(inner_col_block_start_y == 0) begin
                                col_write_en <= 1;
                            end 
                            else begin
                                col_write_en <=0;
                            end
                        end
                        2'b00: begin
                                col_write_en <= 1;
                        end
                    endcase
                end
            endcase
        end
    end
    
    always@(*) begin
        if(is_cand_in_prev_pu_d) begin
            mv_field_pred_flag_l0_out = prev_mv_field_pred_flag_l0;
            mv_field_pred_flag_l1_out = prev_mv_field_pred_flag_l1;
            mv_field_ref_idx_l0_out = prev_mv_field_ref_idx_l0;
            mv_field_ref_idx_l1_out = prev_mv_field_ref_idx_l1;
            mv_field_mv_x_l0_out = prev_mv_field_mv_x_l0;
            mv_field_mv_y_l0_out = prev_mv_field_mv_y_l0;
            mv_field_mv_x_l1_out = prev_mv_field_mv_x_l1;
            mv_field_mv_y_l1_out = prev_mv_field_mv_y_l1;
        end
        else if(cand_x_prev_top_left_d) begin
            {   mv_field_pred_flag_l0_out,
                    mv_field_pred_flag_l1_out,
                    mv_field_ref_idx_l0_out,
                    mv_field_ref_idx_l1_out,
                    mv_field_mv_x_l0_out,
                    mv_field_mv_y_l0_out,
                    mv_field_mv_x_l1_out,
                    mv_field_mv_y_l1_out  } = mv_field_left_to_top_move[MV_FIELD_DATA_WIDTH-1:0];
        end
        else begin
            if((mv_read_from_top_d && (end_of_ctu_pic==0)  )) begin// && (cand_x_prev_top_left_in==0))) begin
                {   mv_field_pred_flag_l0_out,
                    mv_field_pred_flag_l1_out,
                    mv_field_ref_idx_l0_out,
                    mv_field_ref_idx_l1_out,
                    mv_field_mv_x_l0_out,
                    mv_field_mv_y_l0_out,
                    mv_field_mv_x_l1_out,
                    mv_field_mv_y_l1_out  } = mv_field_top_out[MV_FIELD_DATA_WIDTH-1:0];
            end
            else begin
                {   mv_field_pred_flag_l0_out,
                    mv_field_pred_flag_l1_out,
                    mv_field_ref_idx_l0_out,
                    mv_field_ref_idx_l1_out,
                    mv_field_mv_x_l0_out,
                    mv_field_mv_y_l0_out,
                    mv_field_mv_x_l1_out,
                    mv_field_mv_y_l1_out  } = mv_field_left_out[MV_FIELD_DATA_WIDTH-1:0];            
            end
        end
    end
    
    always@(*) begin
        if(available_flag_in) begin
            mv_left_r_addr = y_N_pu_in;
            mv_top_r_addr = x_N_pu_in;
        end
        else begin
			mv_top_r_addr = {((X11_ADDR_WDTH - LOG2_MIN_PU_SIZE)){1'bx}};
			mv_left_r_addr = {((X11_ADDR_WDTH - LOG2_MIN_PU_SIZE)){1'bx}};	
            case(state_write_line)
                STATE_LINE_WRITE_IDLE: begin
                    if(prev_mv_field_valid_in)begin     // last cyle of write idle sets to starting xp,yp
                        mv_top_r_addr = x_P_pu_start_in;
                        mv_left_r_addr = y_P_pu_start_in;                      
                    end
                    else begin  // set left_r_addr to corner pixel (assume there is at least one cycle where prev_mv_field_valid_in is down)
                        mv_left_r_addr = y_P_pu_end_in -1;     
                        mv_top_r_addr = x_P_pu_start_in;
                    end
                end
                STATE_LINE_WRITE_BS_MV_MOVE: begin
                    mv_top_r_addr = mv_top_w_addr[X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0];

                    if(top_line_done & left_line_done) begin        // when condition terminated r addr of mv left made yp  end -1
                        if(bs_send_left_minus_1_as_top_last) begin
                            mv_left_r_addr = y_P_pu_start_in_d -1;
                        end
                        else begin
                            mv_left_r_addr = (y_P_pu_end_in_d - 1)%(1<<(X11_ADDR_WDTH - LOG2_MIN_PU_SIZE));    
                        end
                        
                    end
                    else begin
                        mv_left_r_addr = mv_left_w_addr;
                    end
                end
                STATE_LINE_WRITE_BS_TOP_END_MOVE:begin
                    mv_left_r_addr = (y_P_pu_end_in_d - 1)%(1<<(X11_ADDR_WDTH - LOG2_MIN_PU_SIZE));    
                end
                STATE_CORNER_REINSTATE: begin
                    mv_left_r_addr = (y_P_pu_end_in_d - 1)%(1<<(X11_ADDR_WDTH - LOG2_MIN_PU_SIZE));
                    mv_top_r_addr = mv_top_w_addr[X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0];
                end
                default: begin
                    mv_top_r_addr = {((X11_ADDR_WDTH - LOG2_MIN_PU_SIZE)){1'bx}};
                    mv_left_r_addr = {((X11_ADDR_WDTH - LOG2_MIN_PU_SIZE)){1'bx}};
                end
            endcase
        end
        
    end

// Instantiate the module
d_sync_ram  #(
    .DATA_WIDTH(MV_FIELD_DATA_WIDTH+DPB_ADDR_WIDTH+DPB_ADDR_WIDTH),
    .ADDR_WIDTH(X11_ADDR_WDTH - LOG2_MIN_PU_SIZE),
    .DATA_DEPTH((MAX_PIC_WIDTH>>LOG2_MIN_PU_SIZE))
) mv_top_line_buffer
(
    .clk(clk), 
    .r_addr_in(mv_top_r_addr), 
    .w_addr_in(mv_top_w_addr[X11_ADDR_WDTH - LOG2_MIN_PU_SIZE- 1:0]),
    .r_data_out(mv_field_top_out), 
    .w_data_in(mv_field_top_in), 
    .w_en_in(mv_top_wr_en), 
    .enable(enable)
);

d_sync_ram  #(
    .DATA_WIDTH(MV_FIELD_DATA_WIDTH+DPB_ADDR_WIDTH+DPB_ADDR_WIDTH),
    .ADDR_WIDTH(X11_ADDR_WDTH - LOG2_MIN_PU_SIZE),
    .DATA_DEPTH((MAX_PIC_HEIGHT>>LOG2_MIN_PU_SIZE))
) mv_left_line_buffer
(
    .clk(clk), 
    .r_addr_in(mv_left_r_addr), 
    .w_addr_in(mv_left_w_addr),
    .r_data_out(mv_field_left_out), 
    .w_data_in(mv_field_left_in), 
    .w_en_in(mv_left_wr_en), 
    .enable(enable)
);



always@(posedge clk) begin    
    if (prev_mv_field_valid_in) begin
        prev_mv_field_pred_flag_l1     <= prev_mv_field_pred_flag_l1_in;
        prev_mv_field_pred_flag_l0     <= prev_mv_field_pred_flag_l0_in;
        prev_mv_field_ref_idx_l0       <= prev_mv_field_ref_idx_l0_in;   
        prev_mv_field_ref_idx_l1       <= prev_mv_field_ref_idx_l1_in;
        prev_mv_field_mv_x_l0          <= prev_mv_field_mv_x_l0_in;
        prev_mv_field_mv_y_l0          <= prev_mv_field_mv_y_l0_in;
        prev_mv_field_mv_x_l1          <= prev_mv_field_mv_x_l1_in;
        prev_mv_field_mv_y_l1          <= prev_mv_field_mv_y_l1_in;   
		prev_mv_field_dpb_idx_l0	   <= prev_mv_field_dpb_idx_l0_in;
		prev_mv_field_dpb_idx_l1	   <= prev_mv_field_dpb_idx_l1_in;
    end

end

always@(posedge clk) begin
    // if (state_write_line == STATE_LINE_WRITE_CONRNER_MV_MOVE) begin
    // end
    // else begin

    // end
    available_flag_d <= available_flag_in;
     mv_field_left_in <= {  prev_mv_field_dpb_idx_l0	 ,
							prev_mv_field_dpb_idx_l1	 ,
                            prev_mv_field_pred_flag_l0   ,
                            prev_mv_field_pred_flag_l1   ,
                            prev_mv_field_ref_idx_l0     ,
                            prev_mv_field_ref_idx_l1     ,
                            prev_mv_field_mv_x_l0        ,
                            prev_mv_field_mv_y_l0        ,
                            prev_mv_field_mv_x_l1        ,
                            prev_mv_field_mv_y_l1 };   
end


always@(posedge clk) begin
    if (reset) begin
        state_write_line <= STATE_LINE_WRITE_IDLE;
        mv_top_w_addr   <= 0;
        mv_left_w_addr  <= 0;
        mv_top_wr_en    <= 0;
        mv_left_wr_en   <= 0;
        top_line_done <= 0;
        left_line_done <= 0;
        line_write_done <= 0;
        // x_P_pu_end_in_2d <= {(X11_ADDR_WDTH - LOG2_MIN_PU_SIZE){1'b1}};
        x_P_pu_end_in_d <= {(X11_ADDR_WDTH - LOG2_MIN_PU_SIZE){1'b0}};
        bs_send_left_minus_1_as_top_last <= 0;
    end
    else if(available_flag_in == 0) begin
        case(state_write_line)
            STATE_LINE_WRITE_IDLE: begin
                mv_top_wr_en    <= 0;
                mv_left_wr_en   <= 0;                
                
                if(prev_mv_field_valid_in)begin
                    
                    mv_field_left_to_top_move <= mv_field_left_out;
                    bs_left_valid_feed_d <= bs_left_valid_feed_in;
                    bs_top_valid_feed_d <= bs_top_valid_feed_in;
                    
                    // top_line_done <= 0;
                    // left_line_done <= 0; 
                    line_write_done <= 0;
                    mv_top_w_addr <= (x_P_pu_start_in + 1)%(1<<(X11_ADDR_WDTH - LOG2_MIN_PU_SIZE));
                    mv_left_w_addr <= (y_P_pu_start_in + 1)%(1<<(X11_ADDR_WDTH - LOG2_MIN_PU_SIZE));
                    x_P_pu_start_in_d <= x_P_pu_start_in;
                    y_P_pu_start_in_d <= y_P_pu_start_in;
                    x_P_pu_end_in_d <= x_P_pu_end_in;
                    x_P_pu_end_in_d_minus_2 <= x_P_pu_end_in - 2'd2;
                    y_P_pu_end_in_d <= y_P_pu_end_in;
                    
                    x_rel_top_out <= 0;
                    y_rel_left_out <= 0;
                    
                    if(bs_left_valid_feed_in) begin
                        bs_left_valid <= 1;
                        left_line_done <= 0;
                    end
                    else begin
                        bs_left_valid <= 0;
                        left_line_done <= 1;
                    end
                    if(bs_top_valid_feed_in) begin
                        top_line_done <= 0;
                        if((last_greatest_xP_end_in_pu_1 == (x_P_pu_end_in -1)) ||(x_P_pu_end_in == pic_width_in_pu)) begin
                            bs_send_left_minus_1_as_top_last <= 1;
                            if((x_P_pu_start_in == (last_greatest_xP_end_in_pu_1))) begin
                                bs_top_valid <= 0;
                            end
                            else begin
                                bs_top_valid <= 1;   
                            end
                            
                        end
                        else begin
                            bs_send_left_minus_1_as_top_last <= 0;
                            bs_top_valid <= 1;    
                        end

                    end
                    else begin
                        top_line_done <= 1;
                        bs_top_valid <= 0;
                        bs_send_left_minus_1_as_top_last <= 0;
                    end
                    
                    // if(available_flag_in) begin
                        // state_write_line <= STATE_LINE_WRITE_BS_MV_MOVE_WAIT;
                    // end
                    // else begin
                        // 
                    // end
                    if(bs_top_valid_feed_in || bs_left_valid_feed_in) begin
                        state_write_line <= STATE_LINE_WRITE_BS_MV_MOVE; ////////// assume that when pred_m_field_valid is high there is no available flag request from mv_derive engine
                    end
                    else begin
                        state_write_line <= STATE_LINE_WRITE_REST_MV_MOVE;
                    end
                end
                else begin
                    line_write_done <= 1;
                end
            end
            STATE_LINE_WRITE_BS_MV_MOVE: begin      
                if(bs_top_valid_feed_d) begin
                    if(mv_top_w_addr < x_P_pu_end_in_d) begin
                        mv_top_w_addr <= (mv_top_w_addr + 1)%(1<<(X11_ADDR_WDTH - LOG2_MIN_PU_SIZE));
                        x_rel_top_out <= (x_rel_top_out + 1)%(1<<CTB_SIZE_WIDTH);
                        if(bs_send_left_minus_1_as_top_last && (mv_top_w_addr == (x_P_pu_end_in_d-1))) begin
                            bs_top_valid <= 0;
                        end
                        else begin
                            bs_top_valid <= 1;
                        end
                        top_line_done <= 0;
                    end
                    else begin
                        top_line_done <= 1;
                        bs_top_valid <= 0;
                    end
                end
                if(bs_left_valid_feed_d) begin
                    if(mv_left_w_addr < y_P_pu_end_in_d) begin
                        left_line_done <= 0;
                        y_rel_left_out <= (y_rel_left_out +1)%(1<<CTB_SIZE_WIDTH);
                        bs_left_valid <= 1;
                        // if(mv_left_w_addr <= (y_P_pu_end_in_d -1)) begin       // in order to keep left_read address pointed at the last lcatiion of the pb left buffer for the tranfer to top buffer in next state
                            mv_left_w_addr <= (mv_left_w_addr + 1)%(1<<(X11_ADDR_WDTH - LOG2_MIN_PU_SIZE));
                        // end
                    end
                    else begin
                        left_line_done <= 1;
                        bs_left_valid <= 0;
                    end
                end

                if(bs_send_left_minus_1_as_top_last) begin
                    if(top_line_done & left_line_done) begin
                        state_write_line <= STATE_LINE_WRITE_BS_TOP_END_MOVE;
                        bs_top_valid <= 1;
                    end
                end
                else begin
                    if(top_line_done & left_line_done) begin
                        state_write_line <= STATE_LINE_WRITE_REST_MV_MOVE;
                        top_line_done <= 0;
                        left_line_done <= 0;
                        mv_field_top_in <= mv_field_left_to_top_move;
                        mv_top_w_addr <= x_P_pu_start_in_d -1;
                        mv_left_w_addr <= y_P_pu_start_in_d;          // during corner mv move top's corner pixel is moved and left's first pixel is also moved
                        mv_left_wr_en <= 1;
                        if(x_P_pu_start_in_d != 0) begin
                            mv_top_wr_en <= 1;
                        end
                    end
                end
                    
            end
            STATE_LINE_WRITE_BS_TOP_END_MOVE: begin
                bs_top_valid <= 0;

                state_write_line <= STATE_LINE_WRITE_REST_MV_MOVE;
                top_line_done <= 0;
                left_line_done <= 0;
                mv_field_top_in <= mv_field_left_to_top_move;
                mv_top_w_addr <= x_P_pu_start_in_d -1;
                mv_left_w_addr <= y_P_pu_start_in_d;          // during corner mv move top's corner pixel is moved and left's first pixel is also moved
                mv_left_wr_en <= 1;
                if(x_P_pu_start_in_d != 0) begin
                    mv_top_wr_en <= 1;
                end
            end
            STATE_LINE_WRITE_CONRNER_MV_MOVE: begin
                state_write_line <= STATE_LINE_WRITE_REST_MV_MOVE;
                top_line_done <= 0;
                left_line_done <= 0;
                mv_field_top_in <= mv_field_left_out;
                mv_top_w_addr <= x_P_pu_start_in_d -1;
                mv_left_w_addr <= y_P_pu_start_in_d;          // during corner mv move top's corner pixel is moved and left's first pixel is also moved
                mv_left_wr_en <= 1;
                if(x_P_pu_start_in_d != 0) begin
                    mv_top_wr_en <= 1;
                end
            end
            STATE_LINE_WRITE_REST_MV_MOVE: begin
                bs_top_valid <= 0;
                mv_field_top_in <= {
							prev_mv_field_dpb_idx_l0	 ,
							prev_mv_field_dpb_idx_l1     ,
                            prev_mv_field_pred_flag_l0   ,
                            prev_mv_field_pred_flag_l1   ,
                            prev_mv_field_ref_idx_l0     ,
                            prev_mv_field_ref_idx_l1     ,
                            prev_mv_field_mv_x_l0        ,
                            prev_mv_field_mv_y_l0        ,
                            prev_mv_field_mv_x_l1        ,
                            prev_mv_field_mv_y_l1 };
                if(($signed({mv_top_w_addr}) < $signed(x_P_pu_end_in_d_minus_2)) )begin  // last pixel not written to top mv  -
                    mv_top_w_addr <= (mv_top_w_addr + 1)%(1<<(X11_ADDR_WDTH - LOG2_MIN_PU_SIZE));
                    mv_top_wr_en <= 1;
                end
                else begin
                    mv_top_wr_en <= 0;
                    top_line_done <= 1; 
                end
                if(mv_left_w_addr < y_P_pu_end_in_d -1) begin
                    mv_left_w_addr <= (mv_left_w_addr  + 1)%(1<<(X11_ADDR_WDTH - LOG2_MIN_PU_SIZE));
                    mv_left_wr_en <= 1;
                end
                else begin
                    mv_left_wr_en <= 0;
                    left_line_done <= 1;  
                end
                if(left_line_done & top_line_done) begin
                    state_write_line <= STATE_LINE_WRITE_IDLE;
                end
            end
            // STATE_LINE_WRITE_BS_MV_MOVE_WAIT: begin
            //     if(available_flag_in == 0)begin
            //         state_write_line <= STATE_LINE_WRITE_BS_MV_MOVE;
            //     end
            // end
            STATE_CORNER_REINSTATE: begin
                state_write_line <= STATE_LINE_WRITE_CONRNER_MV_MOVE;
            end
        endcase
    end
    else begin // available_flag = 1;
        if(state_write_line == STATE_LINE_WRITE_CONRNER_MV_MOVE) begin
            state_write_line <= STATE_CORNER_REINSTATE;
        end
    end
end


// synthesis translate_off
    always@(posedge clk ) begin
        if(available_flag_in & prev_mv_field_valid_in) begin
            $display(" availability flag and prev_mv_field_valid_in cant get high at the same time!!");
            #100;
            $stop;
        end
        
        if(available_flag_d && (bs_top_valid_out|| bs_left_valid_out)) begin
            $display("bs valid while availability request!!");
            #100;
            $stop;
        end
    end
// synthesis translate_on


endmodule