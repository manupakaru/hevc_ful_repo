`timescale 1ns / 1ps
module chroma_4_tap_filter(
    clk,
    filt_type,
    level_in,
    a_ref_pixel_in ,
    b_ref_pixel_in ,
    c_ref_pixel_in ,
    d_ref_pixel_in ,
    pred_pixel_out
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter FILTER_PIXEL_WIDTH_IN = FILTER_PIXEL_WIDTH;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
    localparam SHIFT2 = 3'd6;
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input [1:0] filt_type;
    input level_in;
    input [FILTER_PIXEL_WIDTH_IN -1: 0] a_ref_pixel_in;
    input [FILTER_PIXEL_WIDTH_IN -1: 0] b_ref_pixel_in;
    input [FILTER_PIXEL_WIDTH_IN -1: 0] c_ref_pixel_in;
    input [FILTER_PIXEL_WIDTH_IN -1: 0] d_ref_pixel_in;
    output reg [FILTER_PIXEL_WIDTH -1: 0] pred_pixel_out;
    

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    // input [FILTER_PIXEL_WIDTH -1: 0] a_ref_pixel_in_temp;
    // input [FILTER_PIXEL_WIDTH -1: 0] b_ref_pixel_in_temp;
    // input [FILTER_PIXEL_WIDTH -1: 0] c_ref_pixel_in_temp;
    // input [FILTER_PIXEL_WIDTH -1: 0] d_ref_pixel_in_temp;
    
    reg [FILTER_PIXEL_WIDTH +6 -1: 0] pred_pixel_ch_out_inmd1;
    reg [FILTER_PIXEL_WIDTH +6 -1: 0] pred_pixel_ch_out_inmd2;
    reg [FILTER_PIXEL_WIDTH +6 -1: 0] pred_pixel_ch_out_inmd3;
    reg [FILTER_PIXEL_WIDTH +6 -1: 0] pred_pixel_ch_out_inmd4;
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module

    reg signed [FILTER_PIXEL_WIDTH +6 -1: 0]    coef_a;       // max -6
    reg [6 -1:0]        coef_b;               // max 58
    reg [6 -1:0]        coef_c;               // max 36
    reg signed [FILTER_PIXEL_WIDTH +6 -1: 0]    coef_d;        // max -4
    
    reg level_d;
always@(posedge clk) begin    
    level_d <= level_in;
end

always@(*) begin
    case(filt_type)
        `CH_TYPE_1: begin
            coef_a = -22'd2;
            coef_b = 58;
            coef_c = 10;
            coef_d = -22'd2;
        end
        `CH_TYPE_2: begin
            coef_a = -22'd4;
            coef_b = 54;
            coef_c = 16;
            coef_d = -22'd2;
        end
        `CH_TYPE_3: begin
            coef_a = -22'd6;
            coef_b = 46;
            coef_c = 28;
            coef_d = -22'd4;
        end
        `CH_TYPE_4: begin
            coef_a = -22'd4;
            coef_b = 36;
            coef_c = 36;
            coef_d = -22'd4;
        end
    endcase
end


always@(posedge clk) begin
    pred_pixel_ch_out_inmd1 <= (a_ref_pixel_in) * $signed(coef_a);
    pred_pixel_ch_out_inmd2 <= (b_ref_pixel_in) * $signed(coef_b);
    pred_pixel_ch_out_inmd3 <= (c_ref_pixel_in) * $signed(coef_c);
    pred_pixel_ch_out_inmd4 <= (d_ref_pixel_in) * $signed(coef_d);
end

always@(posedge clk) begin
    if(level_d) begin
        pred_pixel_out <= (pred_pixel_ch_out_inmd1 + pred_pixel_ch_out_inmd2 + pred_pixel_ch_out_inmd3 + pred_pixel_ch_out_inmd4)>>SHIFT2;  
    end
    else begin
        pred_pixel_out <= pred_pixel_ch_out_inmd1 + pred_pixel_ch_out_inmd2 + pred_pixel_ch_out_inmd3 + pred_pixel_ch_out_inmd4;  
    end
    
end


endmodule 