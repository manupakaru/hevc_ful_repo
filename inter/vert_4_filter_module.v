`timescale 1ns / 1ps
module vert_4_filter_module(
    clk,
    reset,
    bi_pred_block_filter_in,
    bi_pred_block_filter_out,
    x0_tu_end_in_min_tus_in,
    x0_tu_end_in_min_tus_out,
    y0_tu_end_in_min_tus_in,
    y0_tu_end_in_min_tus_out,

    res_present_in,
    res_present_out,
    xT_4x4_in,
    yT_4x4_in, 
    l_frac_y,
    l_frac_x,
    valid_in,
 
	filter1_in_0 	,
	filter1_in_1    ,
	filter1_in_2    ,
	filter1_in_3    ,
	filter1_in_4    ,
	filter1_in_5    ,
	filter1_in_6    ,
	filter1_in_7    ,
	filter1_in_8    ,
	filter1_in_9    ,
	filter1_in_10   ,
	
	valid_out,
	vert_idle_out,
  vert_full_idle_out,  
    pred_pix_out_4x4,
    xT_4x4_out,
    yT_4x4_out
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter   REF_BLOCK_WIDTH = 4'd11;
    parameter   BLOCK_WIDTH_4x4 = 3'd4;
    parameter   FRAC_WIDTH = 2'd2;
    
    parameter   SHIFT4 = 4'd8;
    parameter   SHIFT3 = 3'd6;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
	localparam STATE_IDLE = 0;
	localparam STATE_BUSY1 = 1;
	localparam STATE_BUSY2 = 2;
	localparam STATE_BUSY3 = 3;
	localparam STATE_BUSY4 = 4;
    
    parameter                           LUMA_DIM_WDTH		    	= 4;        // out block dimension  max 11

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
    input res_present_in;
    output reg res_present_out;
    input bi_pred_block_filter_in;
    output reg bi_pred_block_filter_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      x0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      y0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_out;


    input                           valid_in;
    output reg                  	valid_out;
    output reg                  	vert_idle_out;
    output vert_full_idle_out;
    
    input [FRAC_WIDTH -1: 0] l_frac_y;
    input [FRAC_WIDTH -1: 0] l_frac_x;
    
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4_in;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4_in;
    
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4_out;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4_out;
    
    output  [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 * FILTER_PIXEL_WIDTH -1:0]  pred_pix_out_4x4;  
    
    input [FILTER_PIXEL_WIDTH-1:0] filter1_in_0 ;
    input [FILTER_PIXEL_WIDTH-1:0] filter1_in_1 ;
    input [FILTER_PIXEL_WIDTH-1:0] filter1_in_2 ;
    input [FILTER_PIXEL_WIDTH-1:0] filter1_in_3 ;
    input [FILTER_PIXEL_WIDTH-1:0] filter1_in_4 ;
    input [FILTER_PIXEL_WIDTH-1:0] filter1_in_5 ;
    input [FILTER_PIXEL_WIDTH-1:0] filter1_in_6 ;
    input [FILTER_PIXEL_WIDTH-1:0] filter1_in_7 ;
    input [FILTER_PIXEL_WIDTH-1:0] filter1_in_8 ;
    input [FILTER_PIXEL_WIDTH-1:0] filter1_in_9 ;
    input [FILTER_PIXEL_WIDTH-1:0] filter1_in_10 ; 
	
	
	
    wire [FILTER_PIXEL_WIDTH-1:0] filter_out_1 ; 
    wire [FILTER_PIXEL_WIDTH-1:0] filter_out_2 ; 
    wire [FILTER_PIXEL_WIDTH-1:0] filter_out_3 ; 
    wire [FILTER_PIXEL_WIDTH-1:0] filter_out_4 ; 

    reg [FILTER_PIXEL_WIDTH - 1:0] out_store[BLOCK_WIDTH_4x4 - 1: 0][BLOCK_WIDTH_4x4 -1: 0];	
	reg core_out_valid;
	
	reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4;
    reg res_present;
    reg bi_pred_block_filter;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      x0_tu_end_in_min_tus;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      y0_tu_end_in_min_tus;
	
    reg [FRAC_WIDTH -1: 0] l_frac_y_d;
    reg [FRAC_WIDTH -1: 0] l_frac_x_d;
	
    reg [FRAC_WIDTH -1: 0] l_frac_y_filt_in;
    reg [FRAC_WIDTH -1: 0] l_frac_x_filt_in;
	

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

	integer vert_state;
	integer out_assign_state;
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module


vert_luma_8_tap_filter filter_1_1 (
    .clk(clk), 
    .filt_type(l_frac_y_filt_in), 
    .filt_type_x(l_frac_x_filt_in), 
    .a_ref_pixel_in(filter1_in_0), 
    .b_ref_pixel_in(filter1_in_1), 
    .c_ref_pixel_in(filter1_in_2), 
    .d_ref_pixel_in(filter1_in_3), 
    .e_ref_pixel_in(filter1_in_4), 
    .f_ref_pixel_in(filter1_in_5), 
    .g_ref_pixel_in(filter1_in_6), 
    .h_ref_pixel_in(filter1_in_7), 
    .pred_pixel_out(filter_out_1)
    );
    
vert_luma_8_tap_filter filter_1_2 (
    .clk(clk), 
    .filt_type(l_frac_y_filt_in), 
    .filt_type_x(l_frac_x_filt_in), 
    .a_ref_pixel_in(filter1_in_1), 
    .b_ref_pixel_in(filter1_in_2), 
    .c_ref_pixel_in(filter1_in_3), 
    .d_ref_pixel_in(filter1_in_4), 
    .e_ref_pixel_in(filter1_in_5), 
    .f_ref_pixel_in(filter1_in_6), 
    .g_ref_pixel_in(filter1_in_7), 
    .h_ref_pixel_in(filter1_in_8), 
    .pred_pixel_out(filter_out_2)
    );
    
vert_luma_8_tap_filter filter_1_3 (
    .clk(clk), 
    .filt_type(l_frac_y_filt_in), 
    .filt_type_x(l_frac_x_filt_in), 
    .a_ref_pixel_in(filter1_in_2), 
    .b_ref_pixel_in(filter1_in_3), 
    .c_ref_pixel_in(filter1_in_4), 
    .d_ref_pixel_in(filter1_in_5), 
    .e_ref_pixel_in(filter1_in_6), 
    .f_ref_pixel_in(filter1_in_7), 
    .g_ref_pixel_in(filter1_in_8), 
    .h_ref_pixel_in(filter1_in_9), 
    .pred_pixel_out(filter_out_3)
    );
    
vert_luma_8_tap_filter filter_1_4 (
    .clk(clk), 
    .filt_type(l_frac_y_filt_in), 
    .filt_type_x(l_frac_x_filt_in), 
    .a_ref_pixel_in(filter1_in_3), 
    .b_ref_pixel_in(filter1_in_4), 
    .c_ref_pixel_in(filter1_in_5), 
    .d_ref_pixel_in(filter1_in_6), 
    .e_ref_pixel_in(filter1_in_7), 
    .f_ref_pixel_in(filter1_in_8), 
    .g_ref_pixel_in(filter1_in_9), 
    .h_ref_pixel_in(filter1_in_10), 
    .pred_pixel_out(filter_out_4)
    );
	
	always@(*) begin
		if(vert_state == STATE_IDLE) begin
			l_frac_y_filt_in = l_frac_y;
			l_frac_x_filt_in = l_frac_x;
		end
		else begin
			l_frac_y_filt_in = l_frac_y_d;
			l_frac_x_filt_in = l_frac_x_d;		
		end
	end
	
    generate
        genvar ii;
        genvar jj;

		for(jj=0;jj <BLOCK_WIDTH_4x4 ; jj=jj+1 ) begin : row_iteration2
			for(ii=0 ; ii < BLOCK_WIDTH_4x4 ; ii = ii+1) begin : column_iteration2
				assign pred_pix_out_4x4[(jj*BLOCK_WIDTH_4x4 + ii + 1)*FILTER_PIXEL_WIDTH -1:(jj*BLOCK_WIDTH_4x4 + ii)*FILTER_PIXEL_WIDTH] = out_store[jj][ii];
			end
		end
	endgenerate

  assign vert_full_idle_out = (vert_state == STATE_IDLE) && (out_assign_state == STATE_IDLE) && !valid_in && !valid_out;

	always@(*) begin
		vert_idle_out = 0;
		case(vert_state)
			STATE_IDLE: begin
				if(!valid_in) begin
					vert_idle_out = 1;	
				end
			end
		endcase
	end
	always@(posedge clk) begin
		if(reset) begin
			vert_state <= STATE_IDLE;
			core_out_valid <= 0;
		end
		else begin
			case(vert_state)
				STATE_IDLE: begin
					if(valid_in) begin
						vert_state <= STATE_BUSY1;
						xT_4x4 <= xT_4x4_in;
						yT_4x4 <= yT_4x4_in;
						res_present <= res_present_in;
						bi_pred_block_filter <= bi_pred_block_filter_in;
						x0_tu_end_in_min_tus <= x0_tu_end_in_min_tus_in;
						y0_tu_end_in_min_tus <= y0_tu_end_in_min_tus_in;
						l_frac_y_d <= l_frac_y;
						l_frac_x_d <= l_frac_x;
					end
					core_out_valid <= 0;
				end
				STATE_BUSY1: begin
					vert_state <= STATE_BUSY2;
					core_out_valid <= 1;
				end
				STATE_BUSY2: begin
					vert_state <= STATE_BUSY3;
				end
				STATE_BUSY3: begin
					vert_state <= STATE_IDLE;
				end
			endcase
		end
	end

	always@(posedge clk) begin
		if(reset) begin
			out_assign_state <= STATE_IDLE;
			valid_out <= 0;
		end
		else begin
			case(out_assign_state) 
				STATE_IDLE: begin
					if(core_out_valid) begin
						xT_4x4_out <= xT_4x4;
						yT_4x4_out <= yT_4x4;
						res_present_out <= res_present;
						bi_pred_block_filter_out <= bi_pred_block_filter;
						x0_tu_end_in_min_tus_out <= x0_tu_end_in_min_tus;
						y0_tu_end_in_min_tus_out <= y0_tu_end_in_min_tus;
						
						out_store[0][0] <= filter_out_1;
						out_store[1][0] <= filter_out_2;
						out_store[2][0] <= filter_out_3;
						out_store[3][0] <= filter_out_4;
						out_assign_state <= STATE_BUSY1;
					end
					valid_out <= 0;
				end
				STATE_BUSY1: begin	
					out_assign_state <= STATE_BUSY2;
					out_store[0][1] <= filter_out_1;
					out_store[1][1] <= filter_out_2;
					out_store[2][1] <= filter_out_3;
					out_store[3][1] <= filter_out_4;
				end
				STATE_BUSY2: begin
					out_assign_state <= STATE_BUSY3;
					out_store[0][2] <= filter_out_1;
					out_store[1][2] <= filter_out_2;
					out_store[2][2] <= filter_out_3;
					out_store[3][2] <= filter_out_4;
				end
				STATE_BUSY3: begin
					out_assign_state <= STATE_IDLE;
					out_store[0][3] <= filter_out_1;
					out_store[1][3] <= filter_out_2;
					out_store[2][3] <= filter_out_3;
					out_store[3][3] <= filter_out_4;
					valid_out <= 1;
				end
			endcase
		end
	end
endmodule 