`timescale 1ns / 1ps
module bi_pred_gen(
                                        clk,
                                        clear_list,
                                        merge_array_idx,
                                        merge_cand_list_in,

                                        a0_mv_field_pred_flag_l0         ,   
                                        a0_mv_field_pred_flag_l1         ,
                                        a0_mv_field_mv_x_l0              ,
                                        a0_mv_field_mv_y_l0              ,
                                        a0_mv_field_mv_x_l1              ,
                                        a0_mv_field_mv_y_l1              ,

                                        a1_mv_field_pred_flag_l0         ,
                                        a1_mv_field_pred_flag_l1         ,
                                        a1_mv_field_mv_x_l0              ,
                                        a1_mv_field_mv_y_l0              ,
                                        a1_mv_field_mv_x_l1              ,
                                        a1_mv_field_mv_y_l1              ,

                                        b0_mv_field_pred_flag_l0         ,   
                                        b0_mv_field_pred_flag_l1         ,  
                                        b0_mv_field_mv_x_l0              ,   
                                        b0_mv_field_mv_y_l0              ,   
                                        b0_mv_field_mv_x_l1              ,   
                                        b0_mv_field_mv_y_l1              ,   

                                        b1_mv_field_pred_flag_l0         ,   
                                        b1_mv_field_pred_flag_l1         ,  
                                        b1_mv_field_mv_x_l0              ,   
                                        b1_mv_field_mv_y_l0              ,   
                                        b1_mv_field_mv_x_l1              ,   
                                        b1_mv_field_mv_y_l1              ,   

                                        b2_mv_field_pred_flag_l0         ,   
                                        b2_mv_field_pred_flag_l1         ,  
                                        b2_mv_field_mv_x_l0              ,   
                                        b2_mv_field_mv_y_l0              ,   
                                        b2_mv_field_mv_x_l1              ,   
                                        b2_mv_field_mv_y_l1              ,     
                                        
                                        col_mv_field_pred_flag_l0         ,   
                                        col_mv_field_pred_flag_l1         ,  
                                        col_mv_field_mv_x_l0              ,   
                                        col_mv_field_mv_y_l0              ,   
                                        col_mv_field_mv_x_l1              ,   
                                        col_mv_field_mv_y_l1              ,    
    
                                        ref_poc_a0_l0,
                                        ref_poc_a0_l1,
                                        ref_poc_a1_l0,
                                        ref_poc_a1_l1,
                                        ref_poc_b0_l0,
                                        ref_poc_b0_l1,
                                        ref_poc_b1_l0,
                                        ref_poc_b1_l1,
                                        ref_poc_b2_l0,
                                        ref_poc_b2_l1,    
                                        ref_poc_col_l0,    
                                        ref_poc_col_l1,    
    
                                        bi_pred_cand_valid,
                                        bi_cand_type_out
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input   clk;
    input   clear_list;
    input         [MAX_MERGE_CAND_WIDTH -1:0]                                   merge_array_idx;
    input         [MERGE_CAND_TYPE_WIDTH*MAX_NUM_MERGE_CAND_CONST -1:0]         merge_cand_list_in;

    input                            a0_mv_field_pred_flag_l0         ;   
    input                            a0_mv_field_pred_flag_l1         ;
    input [MVD_WIDTH -1:0]           a0_mv_field_mv_x_l0              ;
    input [MVD_WIDTH -1:0]           a0_mv_field_mv_y_l0              ;
    input [MVD_WIDTH -1:0]           a0_mv_field_mv_x_l1              ;
    input [MVD_WIDTH -1:0]           a0_mv_field_mv_y_l1              ;

    input                            a1_mv_field_pred_flag_l0         ;
    input                            a1_mv_field_pred_flag_l1         ;
    input [MVD_WIDTH -1:0]           a1_mv_field_mv_x_l0              ;
    input [MVD_WIDTH -1:0]           a1_mv_field_mv_y_l0              ;
    input [MVD_WIDTH -1:0]           a1_mv_field_mv_x_l1              ;
    input [MVD_WIDTH -1:0]           a1_mv_field_mv_y_l1              ;

    input                            b0_mv_field_pred_flag_l0         ;   
    input                            b0_mv_field_pred_flag_l1         ;  
    input [MVD_WIDTH -1:0]           b0_mv_field_mv_x_l0              ;   
    input [MVD_WIDTH -1:0]           b0_mv_field_mv_y_l0              ;   
    input [MVD_WIDTH -1:0]           b0_mv_field_mv_x_l1              ;   
    input [MVD_WIDTH -1:0]           b0_mv_field_mv_y_l1              ;   

    input                            b1_mv_field_pred_flag_l0         ;   
    input                            b1_mv_field_pred_flag_l1         ;  
    input [MVD_WIDTH -1:0]           b1_mv_field_mv_x_l0              ;   
    input [MVD_WIDTH -1:0]           b1_mv_field_mv_y_l0              ;   
    input [MVD_WIDTH -1:0]           b1_mv_field_mv_x_l1              ;   
    input [MVD_WIDTH -1:0]           b1_mv_field_mv_y_l1              ;   

    input                            b2_mv_field_pred_flag_l0         ;   
    input                            b2_mv_field_pred_flag_l1         ;  
    input [MVD_WIDTH -1:0]           b2_mv_field_mv_x_l0              ;   
    input [MVD_WIDTH -1:0]           b2_mv_field_mv_y_l0              ;   
    input [MVD_WIDTH -1:0]           b2_mv_field_mv_x_l1              ;   
    input [MVD_WIDTH -1:0]           b2_mv_field_mv_y_l1              ;     
    
    input                            col_mv_field_pred_flag_l0         ;   
    input                            col_mv_field_pred_flag_l1         ;  
    input [MVD_WIDTH -1:0]           col_mv_field_mv_x_l0              ;   
    input [MVD_WIDTH -1:0]           col_mv_field_mv_y_l0              ;   
    input [MVD_WIDTH -1:0]           col_mv_field_mv_x_l1              ;   
    input [MVD_WIDTH -1:0]           col_mv_field_mv_y_l1              ;    
    
    input signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_a0_l0;
    input signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_a0_l1;
    input signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_a1_l0;
    input signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_a1_l1;
    input signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_b0_l0;
    input signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_b0_l1;
    input signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_b1_l0;
    input signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_b1_l1;
    input signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_b2_l0;
    input signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_b2_l1;    
    input signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_col_l0;    
    input signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_col_l1;    
    
    output reg [NUM_BI_PRED_CANDS-1:0] bi_pred_cand_valid;
    output reg [NUM_BI_PRED_CANDS_TYPES*MERGE_CAND_TYPE_WIDTH -1:0 ]    bi_cand_type_out   ; 


//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

    reg signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_cand0_l0;
    reg signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_cand0_l1;
    reg signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_cand1_l0;
    reg signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_cand1_l1;
    reg signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_cand2_l0;
    reg signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_cand2_l1;
    reg signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_cand3_l0;
    reg signed [REF_PIC_LIST_POC_DATA_WIDTH -1:0]  ref_poc_cand3_l1;  
    
    
    reg                            cand0_pred_flag_l0         ;   
    reg                            cand0_pred_flag_l1         ;   
    reg [MVD_WIDTH -1:0]           cand0_mv_x_l0              ;   
    reg [MVD_WIDTH -1:0]           cand0_mv_y_l0              ;   
    reg [MVD_WIDTH -1:0]           cand0_mv_x_l1              ;   
    reg [MVD_WIDTH -1:0]           cand0_mv_y_l1              ;   

    reg                            cand1_pred_flag_l0         ;   
    reg                            cand1_pred_flag_l1         ;  
    reg [MVD_WIDTH -1:0]           cand1_mv_x_l0              ;   
    reg [MVD_WIDTH -1:0]           cand1_mv_y_l0              ;   
    reg [MVD_WIDTH -1:0]           cand1_mv_x_l1              ;   
    reg [MVD_WIDTH -1:0]           cand1_mv_y_l1              ;   
    
    reg                            cand2_pred_flag_l0         ;   
    reg                            cand2_pred_flag_l1         ;   
    reg [MVD_WIDTH -1:0]           cand2_mv_x_l0              ;   
    reg [MVD_WIDTH -1:0]           cand2_mv_y_l0              ;   
    reg [MVD_WIDTH -1:0]           cand2_mv_x_l1              ;   
    reg [MVD_WIDTH -1:0]           cand2_mv_y_l1              ;   
    
    reg                            cand3_pred_flag_l0         ;   
    reg                            cand3_pred_flag_l1         ;  
    reg [MVD_WIDTH -1:0]           cand3_mv_x_l0              ;   
    reg [MVD_WIDTH -1:0]           cand3_mv_y_l0              ;   
    reg [MVD_WIDTH -1:0]           cand3_mv_x_l1              ;   
    reg [MVD_WIDTH -1:0]           cand3_mv_y_l1              ;   
    

    reg         [MERGE_CAND_TYPE_WIDTH -1:0]            merge_cand_list[0:MAX_NUM_MERGE_CAND_CONST -1]; 
    reg         [MAX_MERGE_CAND_WIDTH -1:0]             merge_array_idx_d;
    reg         [MAX_MERGE_CAND_WIDTH -1:0]             merge_array_idx_2d;
    reg                                                 list_inc;
    reg                                                 list_inc_d;

//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module
integer i,j;
always@* begin
    for ( i= 0; i<MAX_NUM_MERGE_CAND_CONST; i=i+1) begin
          for ( j= 0; j<MERGE_CAND_TYPE_WIDTH; j=j+1) begin
                merge_cand_list[i][j] = merge_cand_list_in[i*MERGE_CAND_TYPE_WIDTH + j];
          end
    end
end

always@(*) begin
    list_inc = (merge_array_idx > merge_array_idx_d)? 1'b1: 1'b0;
end


always@(posedge clk) begin    
    merge_array_idx_d <= merge_array_idx;
    merge_array_idx_2d <= merge_array_idx_d;
    list_inc_d          <= list_inc;
end



always@(posedge clk) begin
    if(list_inc) begin
        case(merge_cand_list[0])
            `MV_MERGE_CAND_A0: begin
                cand0_pred_flag_l0        <= a0_mv_field_pred_flag_l0   ;
                cand0_pred_flag_l1        <= a0_mv_field_pred_flag_l1   ;
                cand0_mv_x_l0             <= a0_mv_field_mv_x_l0        ;
                cand0_mv_y_l0             <= a0_mv_field_mv_y_l0        ;
                cand0_mv_x_l1             <= a0_mv_field_mv_x_l1        ;
                cand0_mv_y_l1             <= a0_mv_field_mv_y_l1        ;
                
                ref_poc_cand0_l0            <= ref_poc_a0_l0;
                ref_poc_cand0_l1            <= ref_poc_a0_l1;
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*1 -1 : MERGE_CAND_TYPE_WIDTH* 0] <= `MV_MERGE_CAND_A0;
            end
            `MV_MERGE_CAND_A1: begin
                cand0_pred_flag_l0        <= a1_mv_field_pred_flag_l0   ;
                cand0_pred_flag_l1        <= a1_mv_field_pred_flag_l1   ;
                cand0_mv_x_l0             <= a1_mv_field_mv_x_l0        ;
                cand0_mv_y_l0             <= a1_mv_field_mv_y_l0        ;
                cand0_mv_x_l1             <= a1_mv_field_mv_x_l1        ;
                cand0_mv_y_l1             <= a1_mv_field_mv_y_l1        ;
                
                ref_poc_cand0_l0            <= ref_poc_a1_l0;
                ref_poc_cand0_l1            <= ref_poc_a1_l1;            
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*1 -1 : MERGE_CAND_TYPE_WIDTH* 0] <= `MV_MERGE_CAND_A1;
            end
            `MV_MERGE_CAND_B0: begin
                cand0_pred_flag_l0        <= b0_mv_field_pred_flag_l0   ;
                cand0_pred_flag_l1        <= b0_mv_field_pred_flag_l1   ;
                cand0_mv_x_l0             <= b0_mv_field_mv_x_l0        ;
                cand0_mv_y_l0             <= b0_mv_field_mv_y_l0        ;
                cand0_mv_x_l1             <= b0_mv_field_mv_x_l1        ;
                cand0_mv_y_l1             <= b0_mv_field_mv_y_l1        ;
                
                ref_poc_cand0_l0            <= ref_poc_b0_l0;
                ref_poc_cand0_l1            <= ref_poc_b0_l1;          
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*1 -1 : MERGE_CAND_TYPE_WIDTH* 0] <= `MV_MERGE_CAND_B0;                
            end
            `MV_MERGE_CAND_B1: begin
                cand0_pred_flag_l0        <= b1_mv_field_pred_flag_l0   ;
                cand0_pred_flag_l1        <= b1_mv_field_pred_flag_l1   ;
                cand0_mv_x_l0             <= b1_mv_field_mv_x_l0        ;
                cand0_mv_y_l0             <= b1_mv_field_mv_y_l0        ;
                cand0_mv_x_l1             <= b1_mv_field_mv_x_l1        ;
                cand0_mv_y_l1             <= b1_mv_field_mv_y_l1        ;
                
                ref_poc_cand0_l0            <= ref_poc_b1_l0;
                ref_poc_cand0_l1            <= ref_poc_b1_l1;        
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*1 -1 : MERGE_CAND_TYPE_WIDTH* 0] <= `MV_MERGE_CAND_B1;
            end
            `MV_MERGE_CAND_B2: begin
                cand0_pred_flag_l0        <= b2_mv_field_pred_flag_l0   ;
                cand0_pred_flag_l1        <= b2_mv_field_pred_flag_l1   ;
                cand0_mv_x_l0             <= b2_mv_field_mv_x_l0        ;
                cand0_mv_y_l0             <= b2_mv_field_mv_y_l0        ;
                cand0_mv_x_l1             <= b2_mv_field_mv_x_l1        ;
                cand0_mv_y_l1             <= b2_mv_field_mv_y_l1        ;
                
                ref_poc_cand0_l0            <= ref_poc_b2_l0;
                ref_poc_cand0_l1            <= ref_poc_b2_l1;            
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*1 -1 : MERGE_CAND_TYPE_WIDTH* 0] <= `MV_MERGE_CAND_B2;
            end           
            `MV_MERGE_CAND_COL: begin
                cand0_pred_flag_l0        <= col_mv_field_pred_flag_l0   ;
                cand0_pred_flag_l1        <= col_mv_field_pred_flag_l1   ;
                cand0_mv_x_l0             <= col_mv_field_mv_x_l0        ;
                cand0_mv_y_l0             <= col_mv_field_mv_y_l0        ;
                cand0_mv_x_l1             <= col_mv_field_mv_x_l1        ;
                cand0_mv_y_l1             <= col_mv_field_mv_y_l1        ;
                
                ref_poc_cand0_l0            <= ref_poc_col_l0;
                ref_poc_cand0_l1            <= ref_poc_col_l1;               
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*1 -1 : MERGE_CAND_TYPE_WIDTH* 0] <= `MV_MERGE_CAND_COL;
            end
        endcase  

        case(merge_cand_list[1])
            `MV_MERGE_CAND_A0: begin
                cand1_pred_flag_l0        <= a0_mv_field_pred_flag_l0   ;
                cand1_pred_flag_l1        <= a0_mv_field_pred_flag_l1   ;
                cand1_mv_x_l0             <= a0_mv_field_mv_x_l0        ;
                cand1_mv_y_l0             <= a0_mv_field_mv_y_l0        ;
                cand1_mv_x_l1             <= a0_mv_field_mv_x_l1        ;
                cand1_mv_y_l1             <= a0_mv_field_mv_y_l1        ;
                
                ref_poc_cand1_l0            <= ref_poc_a0_l0;
                ref_poc_cand1_l1            <= ref_poc_a0_l1;
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*2 -1 : MERGE_CAND_TYPE_WIDTH* 1] <= `MV_MERGE_CAND_A0;
            end
            `MV_MERGE_CAND_A1: begin
                cand1_pred_flag_l0        <= a1_mv_field_pred_flag_l0   ;
                cand1_pred_flag_l1        <= a1_mv_field_pred_flag_l1   ;
                cand1_mv_x_l0             <= a1_mv_field_mv_x_l0        ;
                cand1_mv_y_l0             <= a1_mv_field_mv_y_l0        ;
                cand1_mv_x_l1             <= a1_mv_field_mv_x_l1        ;
                cand1_mv_y_l1             <= a1_mv_field_mv_y_l1        ;
                
                ref_poc_cand1_l0            <= ref_poc_a1_l0;
                ref_poc_cand1_l1            <= ref_poc_a1_l1;    
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*2 -1 : MERGE_CAND_TYPE_WIDTH* 1] <= `MV_MERGE_CAND_A1;
            end
            `MV_MERGE_CAND_B0: begin
                cand1_pred_flag_l0        <= b0_mv_field_pred_flag_l0   ;
                cand1_pred_flag_l1        <= b0_mv_field_pred_flag_l1   ;
                cand1_mv_x_l0             <= b0_mv_field_mv_x_l0        ;
                cand1_mv_y_l0             <= b0_mv_field_mv_y_l0        ;
                cand1_mv_x_l1             <= b0_mv_field_mv_x_l1        ;
                cand1_mv_y_l1             <= b0_mv_field_mv_y_l1        ;
                
                ref_poc_cand1_l0            <= ref_poc_b0_l0;
                ref_poc_cand1_l1            <= ref_poc_b0_l1;            
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*2 -1 : MERGE_CAND_TYPE_WIDTH* 1] <= `MV_MERGE_CAND_B0;
            end
            `MV_MERGE_CAND_B1: begin
                cand1_pred_flag_l0        <= b1_mv_field_pred_flag_l0   ;
                cand1_pred_flag_l1        <= b1_mv_field_pred_flag_l1   ;
                cand1_mv_x_l0             <= b1_mv_field_mv_x_l0        ;
                cand1_mv_y_l0             <= b1_mv_field_mv_y_l0        ;
                cand1_mv_x_l1             <= b1_mv_field_mv_x_l1        ;
                cand1_mv_y_l1             <= b1_mv_field_mv_y_l1        ;
                
                ref_poc_cand1_l0            <= ref_poc_b1_l0;
                ref_poc_cand1_l1            <= ref_poc_b1_l1;            
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*2 -1 : MERGE_CAND_TYPE_WIDTH* 1] <= `MV_MERGE_CAND_B1;
                end
            `MV_MERGE_CAND_B2: begin
                cand1_pred_flag_l0        <= b2_mv_field_pred_flag_l0   ;
                cand1_pred_flag_l1        <= b2_mv_field_pred_flag_l1   ;
                cand1_mv_x_l0             <= b2_mv_field_mv_x_l0        ;
                cand1_mv_y_l0             <= b2_mv_field_mv_y_l0        ;
                cand1_mv_x_l1             <= b2_mv_field_mv_x_l1        ;
                cand1_mv_y_l1             <= b2_mv_field_mv_y_l1        ;
                
                ref_poc_cand1_l0            <= ref_poc_b2_l0;
                ref_poc_cand1_l1            <= ref_poc_b2_l1;            
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*2 -1 : MERGE_CAND_TYPE_WIDTH* 1] <= `MV_MERGE_CAND_B2;
            end 
            `MV_MERGE_CAND_COL: begin
                cand1_pred_flag_l0        <= col_mv_field_pred_flag_l0   ;
                cand1_pred_flag_l1        <= col_mv_field_pred_flag_l1   ;
                cand1_mv_x_l0             <= col_mv_field_mv_x_l0        ;
                cand1_mv_y_l0             <= col_mv_field_mv_y_l0        ;
                cand1_mv_x_l1             <= col_mv_field_mv_x_l1        ;
                cand1_mv_y_l1             <= col_mv_field_mv_y_l1        ;
                
                ref_poc_cand1_l0            <= ref_poc_col_l0;
                ref_poc_cand1_l1            <= ref_poc_col_l1;     
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*2 -1 : MERGE_CAND_TYPE_WIDTH* 1] <= `MV_MERGE_CAND_COL;
            end
        endcase      
        
        case(merge_cand_list[2])
            `MV_MERGE_CAND_A0: begin
                cand2_pred_flag_l0        <= a0_mv_field_pred_flag_l0   ;
                cand2_pred_flag_l1        <= a0_mv_field_pred_flag_l1   ;
                cand2_mv_x_l0             <= a0_mv_field_mv_x_l0        ;
                cand2_mv_y_l0             <= a0_mv_field_mv_y_l0        ;
                cand2_mv_x_l1             <= a0_mv_field_mv_x_l1        ;
                cand2_mv_y_l1             <= a0_mv_field_mv_y_l1        ;
                                          
                ref_poc_cand2_l0            <= ref_poc_a0_l0;
                ref_poc_cand2_l1            <= ref_poc_a0_l1;
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*3 -1 : MERGE_CAND_TYPE_WIDTH* 2] <= `MV_MERGE_CAND_A0;
            end                           
            `MV_MERGE_CAND_A1: begin      
                cand2_pred_flag_l0        <= a1_mv_field_pred_flag_l0   ;
                cand2_pred_flag_l1        <= a1_mv_field_pred_flag_l1   ;
                cand2_mv_x_l0             <= a1_mv_field_mv_x_l0        ;
                cand2_mv_y_l0             <= a1_mv_field_mv_y_l0        ;
                cand2_mv_x_l1             <= a1_mv_field_mv_x_l1        ;
                cand2_mv_y_l1             <= a1_mv_field_mv_y_l1        ;
                                          
                ref_poc_cand2_l0            <= ref_poc_a1_l0;
                ref_poc_cand2_l1            <= ref_poc_a1_l1;        
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*3 -1 : MERGE_CAND_TYPE_WIDTH* 2] <= `MV_MERGE_CAND_A1;
            end                           
            `MV_MERGE_CAND_B0: begin      
                cand2_pred_flag_l0        <= b0_mv_field_pred_flag_l0   ;
                cand2_pred_flag_l1        <= b0_mv_field_pred_flag_l1   ;
                cand2_mv_x_l0             <= b0_mv_field_mv_x_l0        ;
                cand2_mv_y_l0             <= b0_mv_field_mv_y_l0        ;
                cand2_mv_x_l1             <= b0_mv_field_mv_x_l1        ;
                cand2_mv_y_l1             <= b0_mv_field_mv_y_l1        ;
                                          
                ref_poc_cand2_l0            <= ref_poc_b0_l0;
                ref_poc_cand2_l1            <= ref_poc_b0_l1;            
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*3 -1 : MERGE_CAND_TYPE_WIDTH* 2] <= `MV_MERGE_CAND_B0;
            end                           
            `MV_MERGE_CAND_B1: begin      
                cand2_pred_flag_l0        <= b1_mv_field_pred_flag_l0   ;
                cand2_pred_flag_l1        <= b1_mv_field_pred_flag_l1   ;
                cand2_mv_x_l0             <= b1_mv_field_mv_x_l0        ;
                cand2_mv_y_l0             <= b1_mv_field_mv_y_l0        ;
                cand2_mv_x_l1             <= b1_mv_field_mv_x_l1        ;
                cand2_mv_y_l1             <= b1_mv_field_mv_y_l1        ;
                                          
                ref_poc_cand2_l0            <= ref_poc_b1_l0;
                ref_poc_cand2_l1            <= ref_poc_b1_l1;            
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*3 -1 : MERGE_CAND_TYPE_WIDTH* 2] <= `MV_MERGE_CAND_B1;
            end                           
            `MV_MERGE_CAND_B2: begin      
                cand2_pred_flag_l0        <= b2_mv_field_pred_flag_l0   ;
                cand2_pred_flag_l1        <= b2_mv_field_pred_flag_l1   ;
                cand2_mv_x_l0             <= b2_mv_field_mv_x_l0        ;
                cand2_mv_y_l0             <= b2_mv_field_mv_y_l0        ;
                cand2_mv_x_l1             <= b2_mv_field_mv_x_l1        ;
                cand2_mv_y_l1             <= b2_mv_field_mv_y_l1        ;
                                          
                ref_poc_cand2_l0            <= ref_poc_b2_l0;
                ref_poc_cand2_l1            <= ref_poc_b2_l1;            
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*3 -1 : MERGE_CAND_TYPE_WIDTH* 2] <= `MV_MERGE_CAND_B2;
            end
            `MV_MERGE_CAND_COL: begin
                cand2_pred_flag_l0        <= col_mv_field_pred_flag_l0   ;
                cand2_pred_flag_l1        <= col_mv_field_pred_flag_l1   ;
                cand2_mv_x_l0             <= col_mv_field_mv_x_l0        ;
                cand2_mv_y_l0             <= col_mv_field_mv_y_l0        ;
                cand2_mv_x_l1             <= col_mv_field_mv_x_l1        ;
                cand2_mv_y_l1             <= col_mv_field_mv_y_l1        ;
                
                ref_poc_cand2_l0            <= ref_poc_col_l0;
                ref_poc_cand2_l1            <= ref_poc_col_l1;          
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*3 -1 : MERGE_CAND_TYPE_WIDTH* 2] <= `MV_MERGE_CAND_COL;
            end
        endcase                           
                                          
        case(merge_cand_list[3])          
            `MV_MERGE_CAND_A0: begin      
                cand3_pred_flag_l0         <= a0_mv_field_pred_flag_l0   ;
                cand3_pred_flag_l1         <= a0_mv_field_pred_flag_l1   ;
                cand3_mv_x_l0              <= a0_mv_field_mv_x_l0        ;
                cand3_mv_y_l0              <= a0_mv_field_mv_y_l0        ;
                cand3_mv_x_l1              <= a0_mv_field_mv_x_l1        ;
                cand3_mv_y_l1              <= a0_mv_field_mv_y_l1        ;
                                           
                ref_poc_cand3_l0             <= ref_poc_a0_l0;
                ref_poc_cand3_l1             <= ref_poc_a0_l1;
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*4 -1 : MERGE_CAND_TYPE_WIDTH* 3] <= `MV_MERGE_CAND_A0;                           
            end                            
            `MV_MERGE_CAND_A1: begin       
                cand3_pred_flag_l0         <= a1_mv_field_pred_flag_l0   ;
                cand3_pred_flag_l1         <= a1_mv_field_pred_flag_l1   ;
                cand3_mv_x_l0              <= a1_mv_field_mv_x_l0        ;
                cand3_mv_y_l0              <= a1_mv_field_mv_y_l0        ;
                cand3_mv_x_l1              <= a1_mv_field_mv_x_l1        ;
                cand3_mv_y_l1              <= a1_mv_field_mv_y_l1        ;
                                           
                ref_poc_cand3_l0             <= ref_poc_a1_l0;
                ref_poc_cand3_l1             <= ref_poc_a1_l1;     
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*4 -1 : MERGE_CAND_TYPE_WIDTH* 3] <= `MV_MERGE_CAND_A1;
            end                            
            `MV_MERGE_CAND_B0: begin       
                cand3_pred_flag_l0         <= b0_mv_field_pred_flag_l0   ;
                cand3_pred_flag_l1         <= b0_mv_field_pred_flag_l1   ;
                cand3_mv_x_l0              <= b0_mv_field_mv_x_l0        ;
                cand3_mv_y_l0              <= b0_mv_field_mv_y_l0        ;
                cand3_mv_x_l1              <= b0_mv_field_mv_x_l1        ;
                cand3_mv_y_l1              <= b0_mv_field_mv_y_l1        ;
                                           
                ref_poc_cand3_l0             <= ref_poc_b0_l0;
                ref_poc_cand3_l1             <= ref_poc_b0_l1;            
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*4 -1 : MERGE_CAND_TYPE_WIDTH* 3] <= `MV_MERGE_CAND_B0;
            end                            
            `MV_MERGE_CAND_B1: begin       
                cand3_pred_flag_l0         <= b1_mv_field_pred_flag_l0   ;
                cand3_pred_flag_l1         <= b1_mv_field_pred_flag_l1   ;
                cand3_mv_x_l0              <= b1_mv_field_mv_x_l0        ;
                cand3_mv_y_l0              <= b1_mv_field_mv_y_l0        ;
                cand3_mv_x_l1              <= b1_mv_field_mv_x_l1        ;
                cand3_mv_y_l1              <= b1_mv_field_mv_y_l1        ;
                                           
                ref_poc_cand3_l0             <= ref_poc_b1_l0;
                ref_poc_cand3_l1             <= ref_poc_b1_l1;            
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*4 -1 : MERGE_CAND_TYPE_WIDTH* 3] <= `MV_MERGE_CAND_B1;
            end                            
            `MV_MERGE_CAND_B2: begin       
                cand3_pred_flag_l0         <= b2_mv_field_pred_flag_l0   ;
                cand3_pred_flag_l1         <= b2_mv_field_pred_flag_l1   ;
                cand3_mv_x_l0              <= b2_mv_field_mv_x_l0        ;
                cand3_mv_y_l0              <= b2_mv_field_mv_y_l0        ;
                cand3_mv_x_l1              <= b2_mv_field_mv_x_l1        ;
                cand3_mv_y_l1              <= b2_mv_field_mv_y_l1        ;
                                           
                ref_poc_cand3_l0             <= ref_poc_b2_l0;
                ref_poc_cand3_l1             <= ref_poc_b2_l1;            
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*4 -1 : MERGE_CAND_TYPE_WIDTH* 3] <= `MV_MERGE_CAND_B2;
            end         
            `MV_MERGE_CAND_COL: begin
                cand3_pred_flag_l0        <= col_mv_field_pred_flag_l0   ;
                cand3_pred_flag_l1        <= col_mv_field_pred_flag_l1   ;
                cand3_mv_x_l0             <= col_mv_field_mv_x_l0        ;
                cand3_mv_y_l0             <= col_mv_field_mv_y_l0        ;
                cand3_mv_x_l1             <= col_mv_field_mv_x_l1        ;
                cand3_mv_y_l1             <= col_mv_field_mv_y_l1        ;
                
                ref_poc_cand3_l0            <= ref_poc_col_l0;
                ref_poc_cand3_l1            <= ref_poc_col_l1;               
                bi_cand_type_out[MERGE_CAND_TYPE_WIDTH*4 -1 : MERGE_CAND_TYPE_WIDTH* 3] <= `MV_MERGE_CAND_COL;
            end
        endcase                      
    
    end
  
end

always@(posedge clk) begin
    if(clear_list) begin
        bi_pred_cand_valid <= 12'd0;
    end
    else begin
        if(list_inc_d) begin
            case(merge_array_idx_2d)
                1: begin
                    if(cand0_pred_flag_l0 == 1 && cand1_pred_flag_l1 == 1) begin
                        if((ref_poc_cand0_l0 != ref_poc_cand1_l1) || (cand0_mv_x_l0 != cand1_mv_x_l1) || (cand0_mv_y_l0 != cand1_mv_y_l1)) begin
                            bi_pred_cand_valid[0] <= 1;
                        end
                        else begin
                            bi_pred_cand_valid[0] <= 0;
                        end
                    end
                    if(cand1_pred_flag_l0 == 1 && cand0_pred_flag_l1 == 1) begin
                        if((ref_poc_cand1_l0 != ref_poc_cand0_l1) || (cand1_mv_x_l0 != cand0_mv_x_l1) || (cand1_mv_y_l0 != cand0_mv_y_l1)) begin
                            bi_pred_cand_valid[1] <= 1;
                        end
                        else begin
                            bi_pred_cand_valid[1] <= 0;
                        end
                    end                
                end
                
                2: begin
                    if(cand0_pred_flag_l0 == 1 && cand2_pred_flag_l1 == 1) begin
                        if((ref_poc_cand0_l0 != ref_poc_cand2_l1) || (cand0_mv_x_l0 != cand2_mv_x_l1) || (cand0_mv_y_l0 != cand2_mv_y_l1)) begin
                            bi_pred_cand_valid[2] <= 1;
                        end
                        else begin
                            bi_pred_cand_valid[2] <= 0;
                        end
                    end
        
                    if(cand2_pred_flag_l0 == 1 && cand0_pred_flag_l1 == 1) begin
                        if((ref_poc_cand2_l0 != ref_poc_cand0_l1) || (cand2_mv_x_l0 != cand0_mv_x_l1) || (cand2_mv_y_l0 != cand0_mv_y_l1)) begin
                            bi_pred_cand_valid[3] <= 1;
                        end
                        else begin
                            bi_pred_cand_valid[3] <= 0;
                        end
                    end
     
                    if(cand1_pred_flag_l0 == 1 && cand2_pred_flag_l1 == 1) begin
                        if((ref_poc_cand1_l0 != ref_poc_cand2_l1) || (cand1_mv_x_l0 != cand2_mv_x_l1) || (cand1_mv_y_l0 != cand2_mv_y_l1)) begin
                            bi_pred_cand_valid[4] <= 1;
                        end
                        else begin
                            bi_pred_cand_valid[4] <= 0;
                        end
                    end  
                    if(cand2_pred_flag_l0 == 1 && cand1_pred_flag_l1 == 1) begin
                        if((ref_poc_cand2_l0 != ref_poc_cand1_l1) || (cand2_mv_x_l0 != cand1_mv_x_l1) || (cand2_mv_y_l0 != cand1_mv_y_l1)) begin
                            bi_pred_cand_valid[5] <= 1;
                        end
                        else begin
                            bi_pred_cand_valid[5] <= 0;
                        end
                    end
        
                    
                end
                3: begin
                    if(cand0_pred_flag_l0 == 1 && cand3_pred_flag_l1 == 1) begin
                        if((ref_poc_cand0_l0 != ref_poc_cand3_l1) || (cand0_mv_x_l0 != cand3_mv_x_l1) || (cand0_mv_y_l0 != cand3_mv_y_l1)) begin
                            bi_pred_cand_valid[6] <= 1;
                        end
                        else begin
                            bi_pred_cand_valid[6] <= 0;
                        end
                    end
                    
                
                    if(cand3_pred_flag_l0 == 1 && cand0_pred_flag_l1 == 1) begin
                        if((ref_poc_cand3_l0 != ref_poc_cand0_l1) || (cand3_mv_x_l0 != cand0_mv_x_l1) || (cand3_mv_y_l0 != cand0_mv_y_l1)) begin
                            bi_pred_cand_valid[7] <= 1;
                        end
                        else begin
                            bi_pred_cand_valid[7] <= 0;
                        end
                    end

                    if(cand1_pred_flag_l0 == 1 && cand3_pred_flag_l1 == 1) begin
                        if((ref_poc_cand1_l0 != ref_poc_cand3_l1) || (cand1_mv_x_l0 != cand3_mv_x_l1) || (cand1_mv_y_l0 != cand3_mv_y_l1)) begin
                            bi_pred_cand_valid[8] <= 1;
                        end
                        else begin
                            bi_pred_cand_valid[8] <= 0;
                        end
                    end
        
        
                    if(cand3_pred_flag_l0 == 1 && cand1_pred_flag_l1 == 1) begin
                        if((ref_poc_cand3_l0 != ref_poc_cand1_l1) || (cand3_mv_x_l0 != cand1_mv_x_l1) || (cand3_mv_y_l0 != cand1_mv_y_l1)) begin
                            bi_pred_cand_valid[9] <= 1;
                        end
                        else begin
                            bi_pred_cand_valid[9] <= 0;
                        end
                    end
        
                    if(cand2_pred_flag_l0 == 1 && cand3_pred_flag_l1 == 1) begin
                        if((ref_poc_cand2_l0 != ref_poc_cand3_l1) || (cand2_mv_x_l0 != cand3_mv_x_l1) || (cand2_mv_y_l0 != cand3_mv_y_l1)) begin
                            bi_pred_cand_valid[10] <= 1;
                        end
                        else begin
                            bi_pred_cand_valid[10] <= 0;
                        end
                    end
        
                    if(cand3_pred_flag_l0 == 1 && cand2_pred_flag_l1 == 1) begin
                        if((ref_poc_cand3_l0 != ref_poc_cand2_l1) || (cand3_mv_x_l0 != cand2_mv_x_l1) || (cand3_mv_y_l0 != cand2_mv_y_l1)) begin
                            bi_pred_cand_valid[11] <= 1;
                        end
                        else begin
                            bi_pred_cand_valid[11] <= 0;
                        end
                    end
        
                end
            endcase    
        end
  
    end
end
endmodule