`timescale 1ns / 1ps
module col_pref_mv_buffer_mem(
    clk,
    r_addr_in,
    w_addr_in,
	r_data_out,
	w_data_in,
	w_en_in,
    enable

);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    	parameter       RDATA_WIDTH = 80;
    	parameter       WDATA_WIDTH = 80*4;
        parameter       RADDR_WIDTH = 5;
        parameter       WADDR_WIDTH = 3;
        
        parameter       DATA_DEPTH = 1<<WADDR_WIDTH;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input                           					clk;
	input	[WDATA_WIDTH - 1:0]             		    w_data_in;
	input	[RADDR_WIDTH - 1:0]				            r_addr_in;			
	input	[WADDR_WIDTH - 1:0]				            w_addr_in;			
	input												w_en_in;
    input                                               enable;
	output  reg [RDATA_WIDTH - 1:0]	        		    r_data_out;

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    reg     [WDATA_WIDTH - 1:0]                          col_pref_mem[0 : DATA_DEPTH -1];   
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module



always@(posedge clk) begin    
    if(enable) begin
        if(w_en_in) begin
            col_pref_mem[w_addr_in] <= w_data_in;
        end
        case(r_addr_in[1:0])
            2'd0: begin
                r_data_out <= col_pref_mem[r_addr_in[RADDR_WIDTH-1:2]][1*RDATA_WIDTH -1: 0];
            end
            2'd1: begin
                r_data_out <= col_pref_mem[r_addr_in[RADDR_WIDTH-1:2]][2*RDATA_WIDTH -1: 1*RDATA_WIDTH];            
            end
            2'd2: begin            
                r_data_out <= col_pref_mem[r_addr_in[RADDR_WIDTH-1:2]][3*RDATA_WIDTH -1: 2*RDATA_WIDTH];
            end
            2'd3: begin
                r_data_out <= col_pref_mem[r_addr_in[RADDR_WIDTH-1:2]][4*RDATA_WIDTH -1: 3*RDATA_WIDTH];            
            end
            
        endcase
    end
end

endmodule 