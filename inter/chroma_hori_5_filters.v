`timescale 1ns / 1ps
module chroma_hori_5_filters(
    clk,
    reset,
	valid_in,
	valid_out,
	ready_out,
	
    c_frac_x,
    c_frac_y,
	
    ref_c_start_x ,
    ref_c_start_y,
    ref_c_height,
    ref_c_width,
    block_start_x_ch,
    block_start_y_ch,
    block_end_x_ch,
    block_end_y_ch,	
	
    bi_pred_block_filter_in,
    bi_pred_block_filter_out,
    x0_tu_end_in_min_tus_in,
    x0_tu_end_in_min_tus_out,
    y0_tu_end_in_min_tus_in,
    y0_tu_end_in_min_tus_out,
	
    ref_pix_l_in,  
	
	frac_y_out,
	frac_x_out,
	pic_width,
	pic_height,
	
    filter_1_out ,
    filter_2_out ,
    filter_3_out ,
    filter_4_out ,
    filter_5_out ,

	vert_idle_in,
    xT_4x4_in,
    yT_4x4_in,	
	xT_4x4_out,
    yT_4x4_out
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter   REF_BLOCK_WIDTH = 3'd5;
    parameter   BLOCK_WIDTH_4x4 = 3'd4;
    parameter   BLOCK_WIDTH_2x2 = 2'd2;
    parameter   FRAC_WIDTH = 2'd3;
    
    parameter   SHIFT4 = 4'd8;
    parameter   SHIFT3 = 3'd6;
	
	
    parameter                           LUMA_DIM_WDTH		    	= 4;        // out block dimension  max 11
    parameter                           CHMA_DIM_WDTH               = 3;        // max 5 
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
	localparam STATE_IDLE = 0;
	localparam STATE_CORNER_FILL = 1;
	localparam STATE_FIL_START1 = 2;
	localparam STATE_FIL_START2 = 5;
	localparam STATE_FIL_START3 = 4;
	localparam STATE_FIL_WAIT = 3;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
	input valid_in;  // downstream module should know its gonnabe valid for 4 consecutive cycles
	output reg valid_out;  // valid out for 4 consecutive cycles
	output reg ready_out;
	
	
    input bi_pred_block_filter_in;
    output reg bi_pred_block_filter_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      x0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      y0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_out;	
	
	input [FRAC_WIDTH -1:0] c_frac_x;
	input [FRAC_WIDTH -1:0] c_frac_y;
	
	input vert_idle_in;
	
    input signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0]   ref_c_start_x;
    input signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0]  ref_c_start_y;
    input [LUMA_DIM_WDTH -1: 0]                 ref_c_height;
    input [LUMA_DIM_WDTH -1: 0]                 ref_c_width;
    input [CHMA_DIM_WDTH -1: 0]                 block_start_x_ch;
    input [CHMA_DIM_WDTH -1: 0]                 block_start_y_ch;
    input [CHMA_DIM_WDTH -1: 0]                 block_end_x_ch;
    input [CHMA_DIM_WDTH -1: 0]                 block_end_y_ch;

    input [PIC_WIDTH_WIDTH -1:0]          pic_width;
    input [PIC_WIDTH_WIDTH -1:0]          pic_height;
	
    input [REF_BLOCK_WIDTH* REF_BLOCK_WIDTH * PIXEL_WIDTH -1:0] ref_pix_l_in;
	
	output [FRAC_WIDTH -1:0] frac_y_out;
	output [FRAC_WIDTH -1:0] frac_x_out;
	
	
    output [FILTER_PIXEL_WIDTH-1:0] filter_1_out ;
    output [FILTER_PIXEL_WIDTH-1:0] filter_2_out ;
    output [FILTER_PIXEL_WIDTH-1:0] filter_3_out ;
    output [FILTER_PIXEL_WIDTH-1:0] filter_4_out ;
    output [FILTER_PIXEL_WIDTH-1:0] filter_5_out ;

	// pipe line interface
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4_in;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4_in;
    
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4_out;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4_out;
	
	
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    reg bi_pred_block_filter;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      x0_tu_end_in_min_tus;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      y0_tu_end_in_min_tus;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4;	
	
    wire [PIXEL_WIDTH - 1:0] in_store_wire[REF_BLOCK_WIDTH - 1: 0][REF_BLOCK_WIDTH -1: 0];
    
    reg [PIXEL_WIDTH - 1:0] in_store[REF_BLOCK_WIDTH - 1: 0][REF_BLOCK_WIDTH -1: 0];
	
	integer hori_state;
	integer hori_next_state;
	
    reg [FRAC_WIDTH -1:0] c_frac_x_d ;//= l_frac_x[FRAC_WIDTH -2:0];
    reg [FRAC_WIDTH -1:0] c_frac_y_d ;//= l_frac_y[FRAC_WIDTH -2:0];
    
    reg [3 -1: 0]                 corner_x_start;
    reg [3 -1: 0]                 corner_x_end;
    reg [3 -1: 0]                 corner_y_start;
    reg [3 -1: 0]                 corner_y_end;
    
    reg [3 -1: 0]                 corner_idx_x;
    reg [3 -1: 0]                 corner_idx_y;
	
	assign frac_y_out = c_frac_y_d;
	assign frac_x_out = c_frac_x_d;
	integer i,j;
	
	reg valid_reg;
	reg valid_reg_d;
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------



chroma_hori_4_tap_filter_paramrized 
#(.FILTER_PIXEL_WIDTH_IN(PIXEL_WIDTH))
filter_1 (
    .clk(clk), 
    .filt_type(c_frac_x_d), 
    .a_ref_pixel_in(in_store[0][0]), 
    .b_ref_pixel_in(in_store[0][1]), 
    .c_ref_pixel_in(in_store[0][2]), 
    .d_ref_pixel_in(in_store[0][3]), 
    .pred_pixel_out(filter_1_out)
    );
	
chroma_hori_4_tap_filter_paramrized 
#(.FILTER_PIXEL_WIDTH_IN(PIXEL_WIDTH))
filter_2 (
    .clk(clk), 
    .filt_type(c_frac_x_d), 
    .a_ref_pixel_in(in_store[1][0]), 
    .b_ref_pixel_in(in_store[1][1]), 
    .c_ref_pixel_in(in_store[1][2]), 
    .d_ref_pixel_in(in_store[1][3]), 
    .pred_pixel_out(filter_2_out)
    );


chroma_hori_4_tap_filter_paramrized 
#(.FILTER_PIXEL_WIDTH_IN(PIXEL_WIDTH))
filter_3 (
    .clk(clk), 
    .filt_type(c_frac_x_d), 
    .a_ref_pixel_in(in_store[2][0]), 
    .b_ref_pixel_in(in_store[2][1]), 
    .c_ref_pixel_in(in_store[2][2]), 
    .d_ref_pixel_in(in_store[2][3]), 
    .pred_pixel_out(filter_3_out)
    );

chroma_hori_4_tap_filter_paramrized 
#(.FILTER_PIXEL_WIDTH_IN(PIXEL_WIDTH))
filter_4 (
    .clk(clk), 
    .filt_type(c_frac_x_d), 
    .a_ref_pixel_in(in_store[3][0]), 
    .b_ref_pixel_in(in_store[3][1]), 
    .c_ref_pixel_in(in_store[3][2]), 
    .d_ref_pixel_in(in_store[3][3]), 
    .pred_pixel_out(filter_4_out)
    );

chroma_hori_4_tap_filter_paramrized 
#(.FILTER_PIXEL_WIDTH_IN(PIXEL_WIDTH))
filter_5 (
    .clk(clk), 
    .filt_type(c_frac_x_d), 
    .a_ref_pixel_in(in_store[4][0]), 
    .b_ref_pixel_in(in_store[4][1]), 
    .c_ref_pixel_in(in_store[4][2]), 
    .d_ref_pixel_in(in_store[4][3]), 
    .pred_pixel_out(filter_5_out)
    );

	
	
    generate
        genvar ii;
        genvar jj;
		for(jj=0;jj <REF_BLOCK_WIDTH ; jj=jj+1 ) begin : row_iteration1
			for(ii=0 ; ii < REF_BLOCK_WIDTH ; ii = ii+1) begin : column_iteration1
				assign in_store_wire[jj][ii] = ref_pix_l_in[(jj*REF_BLOCK_WIDTH + ii + 1)*PIXEL_WIDTH-1:(jj*REF_BLOCK_WIDTH + ii)*PIXEL_WIDTH];
			end
		end

	endgenerate

always@(posedge clk) begin    
	if(reset) begin
		hori_state <= STATE_IDLE;
	end
	else begin
		hori_state <= hori_next_state;
	end
end

always@(*) begin
	hori_next_state = hori_state;
	ready_out = 0;
	case(hori_state)
		STATE_IDLE: begin
			ready_out = 1;
			if(valid_in) begin
				if (((ref_c_start_x[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1]) ) || (ref_c_start_y[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] ) || ((ref_c_start_x + ref_c_width) >= (pic_width>>1) ) || ((ref_c_start_y + ref_c_height) >= (pic_height>>1) )) begin
                    hori_next_state = STATE_CORNER_FILL;
                end
                else begin
                    hori_next_state = STATE_FIL_START1;
                end
			end
		end
		STATE_CORNER_FILL: begin
            hori_next_state = STATE_FIL_START1;
        end
		STATE_FIL_START1: begin
			if(vert_idle_in) begin
				hori_next_state = STATE_IDLE;
			end
			else begin
				hori_next_state = STATE_FIL_WAIT;
			end
		end
		STATE_FIL_WAIT: begin
			if(vert_idle_in) begin
				hori_next_state = STATE_IDLE;
			end
		end
	endcase
end

always@(posedge clk) begin
	valid_reg <= 0;
    case(hori_state)
        STATE_IDLE: begin
            if(valid_in) begin	
                c_frac_x_d <= c_frac_x;
                c_frac_y_d <= c_frac_y;
				if (((ref_c_start_x[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1]) ) || (ref_c_start_y[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] ) || ((ref_c_start_x + ref_c_width) >= (pic_width>>1) ) || ((ref_c_start_y + ref_c_height) >= (pic_height>>1) )) begin
                	valid_reg <= 0;
				end else begin
					valid_reg <= 1;
				end

				// for(k=0; k< PIXEL_WIDTH ; k = k+1) begin
					for(j=0;j <REF_BLOCK_WIDTH ; j=j+1 ) begin
						for(i=0 ; i < REF_BLOCK_WIDTH ; i = i+1) begin 
							in_store[j][i] <= in_store_wire[j][i];
						end
					end
				// end

                if(((ref_c_start_x[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1]) )) begin
                    corner_x_start <= 0;
                    corner_x_end <= block_start_x_ch;
                    corner_idx_x <= block_start_x_ch;
                end
                else if((ref_c_start_x + ref_c_width) >= (pic_width>>1) ) begin
                    corner_x_start <= block_end_x_ch;
                    corner_x_end <= REF_BLOCK_WIDTH;
                    corner_idx_x <= block_end_x_ch;
                end
                else begin
                    corner_x_start <= REF_BLOCK_WIDTH;
                    corner_x_end <= 0;
                end                
                if((ref_c_start_y[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1]) ) begin
                    corner_y_start <= 0;
                    corner_y_end <= block_start_y_ch;
                    corner_idx_y <= block_start_y_ch;
                end
                else if((ref_c_start_y + ref_c_height) >= (pic_height>>1) ) begin
                    corner_y_start <= block_end_y_ch;
                    corner_y_end <= REF_BLOCK_WIDTH;
                    corner_idx_y <= block_end_y_ch;
                end
                else begin
                    corner_y_start <= REF_BLOCK_WIDTH;
                    corner_y_end <= 0;
                end
            end
        end
        STATE_CORNER_FILL: begin
			valid_reg <= 1;
			for(j=0;j <REF_BLOCK_WIDTH ; j=j+1 ) begin
				for(i=0 ; i < REF_BLOCK_WIDTH ; i = i+1) begin
					if( (corner_x_start <= i) && (corner_x_end > i) && (corner_y_start <= j) && (corner_y_end > j)) begin
						in_store[j][i] <= in_store[corner_idx_y][corner_idx_x];
					end
					else if( (corner_x_start <= i) && (corner_x_end > i)) begin
						in_store[j][i] <= in_store[j][corner_idx_x];
					end
					else if( (corner_y_start <= j) && (corner_y_end > j)) begin
						in_store[j][i] <= in_store[corner_idx_y][i];
					end
				end
			end			
        end
		STATE_FIL_START1: begin
			valid_reg <= 0;
			// if(vert_idle_in) begin
				for(j=0; j< REF_BLOCK_WIDTH -1 ; j= j+1) begin
					in_store[0][j] <= in_store[0][j+1]; // left shift
					in_store[1][j] <= in_store[1][j+1];
					in_store[2][j] <= in_store[2][j+1];
					in_store[3][j] <= in_store[3][j+1];
					in_store[4][j] <= in_store[4][j+1];

				// end
			end
		end
	endcase
end
		


always@(posedge clk) begin
		if(valid_in) begin 
			xT_4x4 <= xT_4x4_in;
			yT_4x4 <= yT_4x4_in;
			bi_pred_block_filter <= bi_pred_block_filter_in;
			x0_tu_end_in_min_tus <= x0_tu_end_in_min_tus_in;
			y0_tu_end_in_min_tus <= y0_tu_end_in_min_tus_in;
		end
		if(valid_reg_d) begin
			xT_4x4_out <= xT_4x4;
			yT_4x4_out <= yT_4x4;
			bi_pred_block_filter_out <= bi_pred_block_filter;
			x0_tu_end_in_min_tus_out <= x0_tu_end_in_min_tus;
			y0_tu_end_in_min_tus_out <= y0_tu_end_in_min_tus;		
		end
		valid_reg_d <= valid_reg;
		valid_out <= valid_reg_d;
end


endmodule