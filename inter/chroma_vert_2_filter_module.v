`timescale 1ns / 1ps
module chroma_vert_2_filter_module(
    clk,
    reset,
    bi_pred_block_filter_in,
    bi_pred_block_filter_out,
    x0_tu_end_in_min_tus_in,
    x0_tu_end_in_min_tus_out,
    y0_tu_end_in_min_tus_in,
    y0_tu_end_in_min_tus_out,

    xT_4x4_in,
    yT_4x4_in, 
    c_frac_y,
    c_frac_x,
    valid_in,
 
	filter_in_0 	,
	filter_in_1    ,
	filter_in_2    ,
	filter_in_3    ,
	filter_in_4    ,
	
	valid_out,
	vert_idle_out,
  vert_full_idle_out,
    pred_pix_out_4x4,
    xT_4x4_out,
    yT_4x4_out
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter   REF_BLOCK_WIDTH = 3'd5;
    parameter   BLOCK_WIDTH_2x2 = 3'd2;
    parameter   FRAC_WIDTH = 2'd3;
    
    parameter   SHIFT4 = 4'd8;
    parameter   SHIFT3 = 3'd6;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
	localparam STATE_IDLE = 0;
	localparam STATE_BUSY1 = 1;
	localparam STATE_BUSY2 = 2;
    
    parameter                           LUMA_DIM_WDTH		    	= 3;        // out block dimension  max 11

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
    input bi_pred_block_filter_in;
    output reg bi_pred_block_filter_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      x0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      y0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_out;


    input                           valid_in;
    output reg                  	valid_out;
    output reg                  	vert_idle_out;
    output vert_full_idle_out;
    
    input [FRAC_WIDTH -1: 0] c_frac_y;
    input [FRAC_WIDTH -1: 0] c_frac_x;
    
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4_in;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4_in;
    
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4_out;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4_out;
    
    output  [BLOCK_WIDTH_2x2 *  BLOCK_WIDTH_2x2 * FILTER_PIXEL_WIDTH -1:0]  pred_pix_out_4x4;  
    
    input [FILTER_PIXEL_WIDTH-1:0] filter_in_0 ;
    input [FILTER_PIXEL_WIDTH-1:0] filter_in_1 ;
    input [FILTER_PIXEL_WIDTH-1:0] filter_in_2 ;
    input [FILTER_PIXEL_WIDTH-1:0] filter_in_3 ;
    input [FILTER_PIXEL_WIDTH-1:0] filter_in_4 ;
	
	
	
    wire [FILTER_PIXEL_WIDTH-1:0] filter_out_1 ; 
    wire [FILTER_PIXEL_WIDTH-1:0] filter_out_2 ;  

    reg [FILTER_PIXEL_WIDTH - 1:0] out_store[BLOCK_WIDTH_2x2 - 1: 0][BLOCK_WIDTH_2x2 -1: 0];	
	reg core_out_valid;
	
	reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4;
    reg res_present;
    reg bi_pred_block_filter;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      x0_tu_end_in_min_tus;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      y0_tu_end_in_min_tus;
	
    reg [FRAC_WIDTH -1: 0] c_frac_y_d;
    reg [FRAC_WIDTH -1: 0] c_frac_x_d;
	
    reg [FRAC_WIDTH -1: 0] c_frac_y_filt_in;
    reg [FRAC_WIDTH -1: 0] c_frac_x_filt_in;
	

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

	integer vert_state;
	integer out_assign_state;
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module


chroma_vert_4_tap_filter_paramrized 
#(.FILTER_PIXEL_WIDTH_IN(FILTER_PIXEL_WIDTH))
filter_1 (
    .clk(clk), 
    .filt_type(c_frac_y_filt_in), 
    .level_in({c_frac_x_filt_in == 0}),
    .a_ref_pixel_in(filter_in_0), 
    .b_ref_pixel_in(filter_in_1), 
    .c_ref_pixel_in(filter_in_2), 
    .d_ref_pixel_in(filter_in_3), 
    .pred_pixel_out(filter_out_1)
    );
 
chroma_vert_4_tap_filter_paramrized 
#(.FILTER_PIXEL_WIDTH_IN(FILTER_PIXEL_WIDTH))
filter_2 (
    .clk(clk), 
    .filt_type(c_frac_y_filt_in), 
    .level_in({c_frac_x_filt_in == 0}),
    .a_ref_pixel_in(filter_in_1), 
    .b_ref_pixel_in(filter_in_2), 
    .c_ref_pixel_in(filter_in_3), 
    .d_ref_pixel_in(filter_in_4), 
    .pred_pixel_out(filter_out_2)
    );
	
	
	
	always@(*) begin
		if(vert_state == STATE_IDLE) begin
			c_frac_y_filt_in = c_frac_y;
			c_frac_x_filt_in = c_frac_x;
		end
		else begin
			c_frac_y_filt_in = c_frac_y_d;
			c_frac_x_filt_in = c_frac_x_d;		
		end
	end
	
    generate
        genvar ii;
        genvar jj;

		for(jj=0;jj <BLOCK_WIDTH_2x2 ; jj=jj+1 ) begin : row_iteration2
			for(ii=0 ; ii < BLOCK_WIDTH_2x2 ; ii = ii+1) begin : column_iteration2
				assign pred_pix_out_4x4[(jj*BLOCK_WIDTH_2x2 + ii + 1)*FILTER_PIXEL_WIDTH -1:(jj*BLOCK_WIDTH_2x2 + ii)*FILTER_PIXEL_WIDTH] = out_store[jj][ii];
			end
		end
	endgenerate


  assign vert_full_idle_out = (vert_state == STATE_IDLE) && (out_assign_state == STATE_IDLE) && !valid_in && !valid_out;
	always@(*) begin
		vert_idle_out = 0;
		case(vert_state)
			STATE_IDLE: begin
				if(!valid_in) begin
					vert_idle_out = 1;	
				end
			end
		endcase
	end
	
	
	always@(posedge clk) begin
		if(reset) begin
			vert_state <= STATE_IDLE;
			core_out_valid <= 0;
		end
		else begin
			case(vert_state)
				STATE_IDLE: begin
					if(valid_in) begin
						vert_state <= STATE_BUSY1;
						xT_4x4 <= xT_4x4_in;
						yT_4x4 <= yT_4x4_in;
						bi_pred_block_filter <= bi_pred_block_filter_in;
						x0_tu_end_in_min_tus <= x0_tu_end_in_min_tus_in;
						y0_tu_end_in_min_tus <= y0_tu_end_in_min_tus_in;
						c_frac_y_d <= c_frac_y;
						c_frac_x_d <= c_frac_x;
					end
					core_out_valid <= 0;
				end
				STATE_BUSY1: begin
					vert_state <= STATE_BUSY2;
					
				end
				STATE_BUSY2: begin
					vert_state <= STATE_IDLE;
					core_out_valid <= 1;
				end
			endcase
		end
	end

	always@(posedge clk) begin
		if(reset) begin
			out_assign_state <= STATE_IDLE;
			valid_out <= 0;
		end
		else begin
			case(out_assign_state) 
				STATE_IDLE: begin
					if(core_out_valid) begin
						xT_4x4_out <= xT_4x4;
						yT_4x4_out <= yT_4x4;
						bi_pred_block_filter_out <= bi_pred_block_filter;
						x0_tu_end_in_min_tus_out <= x0_tu_end_in_min_tus;
						y0_tu_end_in_min_tus_out <= y0_tu_end_in_min_tus;
						
						out_store[0][0] <= filter_out_1;
						out_store[1][0] <= filter_out_2;
						out_assign_state <= STATE_BUSY1;
					end
					valid_out <= 0;
				end
				STATE_BUSY1: begin	
					valid_out <= 1;
					out_assign_state <= STATE_IDLE;
					out_store[0][1] <= filter_out_1;
					out_store[1][1] <= filter_out_2;
				end
			endcase
		end
	end
endmodule 