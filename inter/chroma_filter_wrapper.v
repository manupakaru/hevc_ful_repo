`timescale 1ns / 1ps
module chroma_filter_wrapper(
    clk,
    reset,
    bi_pred_block_filter_in,
    bi_pred_block_filter_out,
    x0_tu_end_in_min_tus_in,
    x0_tu_end_in_min_tus_out,
    y0_tu_end_in_min_tus_in,
    y0_tu_end_in_min_tus_out,

    ref_c_start_x,
    ref_c_start_y,
    ref_c_height,
    ref_c_width,
    block_start_x_ch,
    block_start_y_ch,
    block_end_x_ch,
    block_end_y_ch,
    xT_4x4_in,
    yT_4x4_in,    
    c_frac_x,
    c_frac_y,
    
    ref_pix_l_in,    
    valid_in,
    
    pic_width,
    pic_height,
    
    block_ready_out,
    filter_idle_out,

    
    pred_pix_out_4x4,
    xT_4x4_out,
    yT_4x4_out
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter   REF_BLOCK_WIDTH = 3'd5;
    parameter   BLOCK_WIDTH_2x2 = 2'd2;
    parameter   FRAC_WIDTH = 2'd3;
    
    parameter   SHIFT4 = 4'd8;
    parameter   SHIFT3 = 3'd6;
    
    parameter                           LUMA_DIM_WDTH		    	= 4;        // out block dimension  max 11
    parameter                           CHMA_DIM_WDTH               = 3;        // max 5 
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam STATE_IDLE = 0;
    localparam STATE_FIL_START = 1;
    localparam STATE_CORNER_FILL    = 2;
    localparam STATE_HOR_FILT1      = 3;
    localparam STATE_HOR_FILT2      = 4;
    localparam STATE_HOR_FILT3      = 5;
    localparam STATE_HOR_FILT4      = 6;
    localparam STATE_HOR_VERT_FILT3 = 7;
    localparam STATE_HOR_VERT_FILT4 = 8;
    localparam STATE_VER_FILT1      = 9;
    localparam STATE_VER_FILT2      = 10;
    localparam STATE_VER_FILT3      = 11;
    localparam STATE_VER_FILT4      = 12;
    localparam STATE_HOR_VERT_FILT5 = 13;
    localparam STATE_HOR_VERT_FILT6 = 14;
    
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
    input bi_pred_block_filter_in;
    output reg bi_pred_block_filter_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      x0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] x0_tu_end_in_min_tus_out;

    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]      y0_tu_end_in_min_tus_in;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] y0_tu_end_in_min_tus_out;

    input signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0]   ref_c_start_x;
    input signed [MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1: 0]   ref_c_start_y;
    input [LUMA_DIM_WDTH -1: 0]                 ref_c_height;
    input [LUMA_DIM_WDTH -1: 0]                 ref_c_width;
    input [CHMA_DIM_WDTH -1: 0]                 block_start_x_ch;
    input [CHMA_DIM_WDTH -1: 0]                 block_start_y_ch;
    input [CHMA_DIM_WDTH -1: 0]                 block_end_x_ch;
    input [CHMA_DIM_WDTH -1: 0]                 block_end_y_ch;
    input                           valid_in;
    
    input [PIC_WIDTH_WIDTH -1:0]          pic_width;
    input [PIC_WIDTH_WIDTH -1:0]          pic_height;
    
    input [REF_BLOCK_WIDTH* REF_BLOCK_WIDTH * PIXEL_WIDTH -1:0] ref_pix_l_in;
    output reg block_ready_out;
    output reg filter_idle_out;
    
    input [FRAC_WIDTH -1: 0] c_frac_x;
    input [FRAC_WIDTH -1: 0] c_frac_y;
    
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4_in;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4_in;
    
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4_out;
    output reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4_out;
    
	output reg [BLOCK_WIDTH_2x2 *  BLOCK_WIDTH_2x2 * FILTER_PIXEL_WIDTH -1:0]  pred_pix_out_4x4;    

       

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    integer filter_state;
    integer next_state;
    
    reg [PIXEL_WIDTH - 1:0] in_store_wire[REF_BLOCK_WIDTH - 1: 0][REF_BLOCK_WIDTH -1: 0];
    
    reg [PIXEL_WIDTH - 1:0] in_store[REF_BLOCK_WIDTH - 1: 0][REF_BLOCK_WIDTH -1: 0];
    reg [FILTER_PIXEL_WIDTH - 1:0] out_store_1r[BLOCK_WIDTH_2x2 -1: 0];
    reg [FILTER_PIXEL_WIDTH - 1:0] out_store_2r[BLOCK_WIDTH_2x2 -1: 0];
    reg [FILTER_PIXEL_WIDTH - 1:0] out_store_3r[BLOCK_WIDTH_2x2 -1: 0];
    reg [FILTER_PIXEL_WIDTH - 1:0] out_store_4r;//[BLOCK_WIDTH_2x2 -2: 0];
    reg [FILTER_PIXEL_WIDTH - 1:0] out_store_5r;//[BLOCK_WIDTH_2x2 -2: 0];
    
    reg [FILTER_PIXEL_WIDTH -1:0] filter1_in_1 [REF_BLOCK_WIDTH -1: 0];
    reg [FILTER_PIXEL_WIDTH -1:0] filter1_in_2 [REF_BLOCK_WIDTH -1: 0];
    
    reg [FILTER_PIXEL_WIDTH -1:0] filter2_in_1 [REF_BLOCK_WIDTH -1: 0];

    

    reg [FRAC_WIDTH -1:0] c_frac_x_d ;//= l_frac_x[FRAC_WIDTH -2:0];
    reg [FRAC_WIDTH -1:0] c_frac_y_d ;//= l_frac_y[FRAC_WIDTH -2:0];
    
    reg [3 -1: 0]                 corner_x_start;
    reg [3 -1: 0]                 corner_x_end;
    reg [3 -1: 0]                 corner_y_start;
    reg [3 -1: 0]                 corner_y_end;
    
    reg [3 -1: 0]                 corner_idx_x;
    reg [3 -1: 0]                 corner_idx_y;
    
    wire [FILTER_PIXEL_WIDTH -1:0] filter1_out_1;
    wire [FILTER_PIXEL_WIDTH -1:0] filter1_out_2;
    wire [FILTER_PIXEL_WIDTH -1:0] filter2_out_1;
    
    reg [1:0] filt_type;
    reg level;
    
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module
chroma_4_tap_filter filter_1_1 (
    .clk(clk), 
    .filt_type(filt_type), 
    .level_in(level),
    .a_ref_pixel_in(filter1_in_1[0]), 
    .b_ref_pixel_in(filter1_in_1[1]), 
    .c_ref_pixel_in(filter1_in_1[2]), 
    .d_ref_pixel_in(filter1_in_1[3]), 
    .pred_pixel_out(filter1_out_1)
    );
    
chroma_4_tap_filter filter_1_2 (
    .clk(clk), 
    .filt_type(filt_type),
    .level_in(level), 
    .a_ref_pixel_in(filter1_in_2[0]), 
    .b_ref_pixel_in(filter1_in_2[1]), 
    .c_ref_pixel_in(filter1_in_2[2]), 
    .d_ref_pixel_in(filter1_in_2[3]), 
    .pred_pixel_out(filter1_out_2)
    );
    

chroma_4_tap_filter 
// #
    // (.FILTER_PIXEL_WIDTH_IN(PIXEL_WIDTH)
    // )
    filter_2_1 (
    .clk(clk), 
    .filt_type(filt_type), 
    .level_in(level),
    .a_ref_pixel_in(filter2_in_1[0]), 
    .b_ref_pixel_in(filter2_in_1[1]), 
    .c_ref_pixel_in(filter2_in_1[2]), 
    .d_ref_pixel_in(filter2_in_1[3]), 
    .pred_pixel_out(filter2_out_1)
    );

    
integer i,j,k;

wire [1:0] filt_type_alt = -c_frac_x_d[1:0];
wire [1:0] filt_type_y_alt = -c_frac_y_d[1:0];

always@(*) begin
    for(j=0;j <REF_BLOCK_WIDTH ; j=j+1 ) begin
        for(i=0 ; i < REF_BLOCK_WIDTH ; i = i+1) begin
            for(k=0; k< PIXEL_WIDTH ; k = k+1) begin
                in_store_wire[j][i][k] = ref_pix_l_in[(j*REF_BLOCK_WIDTH + i)*PIXEL_WIDTH + k];
            end
        end
    end
end

always@(*) begin
    // for(j=0;j < BLOCK_WIDTH_2x2 ; j=j+1 ) begin
        for(i=0 ; i < BLOCK_WIDTH_2x2 ; i = i+1) begin
            for(k=0; k< FILTER_PIXEL_WIDTH ; k = k+1) begin
                pred_pix_out_4x4[(0*BLOCK_WIDTH_2x2 + i)*FILTER_PIXEL_WIDTH + k] = out_store_1r[i][k];
            end
        end
        for(i=0 ; i < BLOCK_WIDTH_2x2 ; i = i+1) begin
            for(k=0; k< FILTER_PIXEL_WIDTH ; k = k+1) begin
                pred_pix_out_4x4[(1*BLOCK_WIDTH_2x2 + i)*FILTER_PIXEL_WIDTH + k] = out_store_2r[i][k];
            end
        end
    // end
end

always@(*) begin    
    next_state = filter_state;
    filter_idle_out = 0;
    case(filter_state)
        STATE_IDLE: begin
            filter_idle_out = 1;
            if(valid_in) begin
                if (((ref_c_start_x[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1]) ) || (ref_c_start_y[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1] ) || ((ref_c_start_x + ref_c_width) >= (pic_width>>1) ) || ((ref_c_start_y + ref_c_height) >= (pic_height>>1) )) begin
                    next_state = STATE_CORNER_FILL;
                end
                else begin
                    next_state = STATE_FIL_START;
                end
            end
        end
        STATE_FIL_START: begin
            if(c_frac_x_d ==0 && c_frac_y_d == 0) begin
                next_state = STATE_IDLE;
            end
            else if(c_frac_x_d != 0) begin
                next_state = STATE_HOR_FILT1;
            end
            else begin // (l_frac_y != 0)
                next_state = STATE_VER_FILT1;
            end
        end
        STATE_CORNER_FILL: begin
            next_state = STATE_FIL_START;
        end
        STATE_HOR_FILT1: begin
            next_state = STATE_HOR_FILT2;
        end
        STATE_HOR_FILT2: begin
            if((|c_frac_y_d)) begin
                next_state = STATE_HOR_VERT_FILT3;
            end
            else begin
                next_state = STATE_HOR_FILT3;
            end
        end
        
        STATE_HOR_FILT3: begin
           next_state = STATE_HOR_FILT4; 
        end
        STATE_HOR_FILT4: begin
            next_state = STATE_IDLE;
        end
        STATE_HOR_VERT_FILT3: begin
            next_state = STATE_HOR_VERT_FILT4;
        end
        STATE_HOR_VERT_FILT4: begin
            next_state = STATE_HOR_VERT_FILT5;
        end
        STATE_HOR_VERT_FILT5: begin
            next_state = STATE_HOR_VERT_FILT6;
        end
        STATE_HOR_VERT_FILT6: begin
            next_state = STATE_VER_FILT1;
        end
        STATE_VER_FILT1: begin
            next_state = STATE_VER_FILT2;
        end
        STATE_VER_FILT2: begin
            next_state = STATE_VER_FILT3;
        end
        STATE_VER_FILT3: begin
            next_state = STATE_VER_FILT4;
        end
        STATE_VER_FILT4: begin
            next_state = STATE_IDLE;
        end
    endcase
end


always@(posedge clk) begin
    case(filter_state)
        STATE_IDLE: begin
            block_ready_out <= 0;
            if(valid_in) begin
                xT_4x4_out <= xT_4x4_in;
                yT_4x4_out <= yT_4x4_in;
                c_frac_x_d <= c_frac_x;
                c_frac_y_d <= c_frac_y;
                bi_pred_block_filter_out <= bi_pred_block_filter_in;
                x0_tu_end_in_min_tus_out <= x0_tu_end_in_min_tus_in;
                y0_tu_end_in_min_tus_out <= y0_tu_end_in_min_tus_in;
                for(j=0;j <REF_BLOCK_WIDTH ; j=j+1 ) begin
                    for(i=0 ; i < REF_BLOCK_WIDTH ; i = i+1) begin
                        // for(k=0; k< PIXEL_WIDTH ; k = k+1) begin
                            in_store[j][i] <= in_store_wire[j][i];
                        // end
                    end
                end
                if(((ref_c_start_x[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1]) )) begin
                    corner_x_start <= 0;
                    corner_x_end <= block_start_x_ch;
                    corner_idx_x <= block_start_x_ch;
                end
                else if((ref_c_start_x + ref_c_width) >= (pic_width>>1) ) begin
                    corner_x_start <= block_end_x_ch;
                    corner_x_end <= REF_BLOCK_WIDTH;
                    corner_idx_x <= block_end_x_ch;
                end
                else begin
                    corner_x_start <= REF_BLOCK_WIDTH;
                    corner_x_end <= 0;
                end                
                if((ref_c_start_y[MVD_WIDTH - MV_L_FRAC_WIDTH_HIGH -1]) ) begin
                    corner_y_start <= 0;
                    corner_y_end <= block_start_y_ch;
                    corner_idx_y <= block_start_y_ch;
                end
                else if((ref_c_start_y + ref_c_height) >= (pic_height>>1) ) begin
                    corner_y_start <= block_end_y_ch;
                    corner_y_end <= REF_BLOCK_WIDTH;
                    corner_idx_y <= block_end_y_ch;
                end
                else begin
                    corner_y_start <= REF_BLOCK_WIDTH;
                    corner_y_end <= 0;
                end
            end
        end
        STATE_CORNER_FILL: begin
            for(j=0;j <REF_BLOCK_WIDTH ; j=j+1 ) begin
                for(i=0 ; i < REF_BLOCK_WIDTH ; i = i+1) begin
                    if( (corner_x_start <= i) && (corner_x_end > i) && (corner_y_start <= j) && (corner_y_end > j)) begin
                        // for(k=0; k< PIXEL_WIDTH ; k = k+1) begin
                            in_store[j][i] <= in_store[corner_idx_y][corner_idx_x];
                        // end
                    end
                    else if( (corner_x_start <= i) && (corner_x_end > i)) begin
                        // for(k=0; k< PIXEL_WIDTH ; k = k+1) begin
                            in_store[j][i] <= in_store[j][corner_idx_x];
                        // end
                    end
                    else if( (corner_y_start <= j) && (corner_y_end > j)) begin
                        // for(k=0; k< PIXEL_WIDTH ; k = k+1) begin
                            in_store[j][i] <= in_store[corner_idx_y][i];
                        // end
                    end
                end
            end
        end
        STATE_FIL_START: begin
            level <= 0;
            case({c_frac_x_d,c_frac_y_d})
                6'b000_000: begin
                    block_ready_out <= 1;
                    // for(j = 0; j < BLOCK_WIDTH_2x2; j=j+1) begin
                        for(i=0; i< BLOCK_WIDTH_2x2 ; i = i+1) begin
                            // for(k=0;k< PIXEL_WIDTH ; k= k+1) begin
                                out_store_1r[i] <= in_store[0][i] << SHIFT3;
                                out_store_2r[i] <= in_store[1][i] << SHIFT3;
                                // out_store_3r[i] <= in_store[3-0][i] << SHIFT3;
                                // out_store_4r[i] <= in_store[4-0][i] << SHIFT3;
                                // out_store_5r[i] <= in_store[5-0][i] << SHIFT3;
                            // end
                        end
                    // end
                end
                6'b001_000, 6'b001_001, 6'b001_010, 6'b001_011, 6'b001_100, 6'b001_101, 6'b001_110, 6'b001_111,
                6'b010_000, 6'b010_001, 6'b010_010, 6'b010_011, 6'b010_100, 6'b010_101, 6'b010_110, 6'b010_111,
                6'b011_000, 6'b011_001, 6'b011_010, 6'b011_011, 6'b011_100, 6'b011_101, 6'b011_110, 6'b011_111,
                6'b100_000, 6'b100_001, 6'b100_010, 6'b100_011, 6'b100_100, 6'b100_101, 6'b100_110, 6'b100_111 : begin
                    filt_type <= c_frac_x_d[1:0];
                    for(j=0; j< REF_BLOCK_WIDTH ; j= j+1) begin
                        filter1_in_1[j] <= {{SHIFT4{1'b0}},in_store[0][j]};
                        filter1_in_2[j] <= {{SHIFT4{1'b0}},in_store[1][j]};
                        
                        filter2_in_1[j] <= {{SHIFT4{1'b0}},in_store[2][j]};
                    end
                end
                6'b101_000, 6'b101_001, 6'b101_010, 6'b101_011, 6'b101_100, 6'b101_101, 6'b101_110, 6'b101_111,
                6'b110_000, 6'b110_001, 6'b110_010, 6'b110_011, 6'b110_100, 6'b110_101, 6'b110_110, 6'b110_111,
                6'b111_000, 6'b111_001, 6'b111_010, 6'b111_011, 6'b111_100, 6'b111_101, 6'b111_110, 6'b111_111 : begin
                    filt_type <= filt_type_alt;
                    for(j=0; j< REF_BLOCK_WIDTH ; j= j+1) begin
                        filter1_in_1[j] <= {{SHIFT4{1'b0}},in_store[0][4 -j]};
                        filter1_in_2[j] <= {{SHIFT4{1'b0}},in_store[1][4 -j]};
                        
                        filter2_in_1[j] <= {{SHIFT4{1'b0}},in_store[2][4 -j]};
                    end
                end
                6'b000_001, 
                6'b000_010, 
                6'b000_011, 
                6'b000_100 : begin
                    filt_type <= c_frac_y_d[1:0];
                    for(j=0; j< REF_BLOCK_WIDTH ; j= j+1) begin
                        filter1_in_1[j] <= {{SHIFT4{1'b0}},in_store[j][0]};
                        filter1_in_2[j] <= {{SHIFT4{1'b0}},in_store[j][1]};
                    end
                end
                default: begin
                    filt_type <= filt_type_y_alt;
                    for(j=0; j< REF_BLOCK_WIDTH ; j= j+1) begin
                        filter1_in_1[j] <= {{SHIFT4{1'b0}},in_store[4-j][0]};
                        filter1_in_2[j] <= {{SHIFT4{1'b0}},in_store[4-j][1]};
                    end
                end
            endcase
        end
        STATE_HOR_FILT1: begin
            for(j=0; j< REF_BLOCK_WIDTH -1 ; j= j+1) begin
                filter1_in_1[j] <= filter1_in_1[j+1]; // left shift
                filter1_in_2[j] <= filter1_in_2[j+1];

                filter2_in_1[j] <= filter2_in_1[j+1];
            end
        end
        STATE_HOR_FILT2: begin
            // if(c_frac_y_d != {FRAC_WIDTH{1'b0}}) begin
                if(c_frac_x_d[2] == 1'b1 && (|(c_frac_x_d[1:0]))) begin
                    for(j=0; j< REF_BLOCK_WIDTH  ; j= j+1) begin
                        filter1_in_1[j] <= {{SHIFT4{1'b0}},in_store[3][4-j]}; 
                        filter1_in_2[j] <= {{SHIFT4{1'b0}},in_store[4][4-j]};
                    end              
                end
                else begin
                    for(j=0; j< REF_BLOCK_WIDTH  ; j= j+1) begin
                        filter1_in_1[j] <= {{SHIFT4{1'b0}},in_store[3][j]}; 
                        filter1_in_2[j] <= {{SHIFT4{1'b0}},in_store[4][j]};
                    end   
                end
            // end
        end
        STATE_HOR_FILT3: begin
            if(c_frac_x_d[2] == 1'b1 && (|(c_frac_x_d[1:0]))) begin
                out_store_1r[1] <= filter1_out_1 ;
                out_store_2r[1] <= filter1_out_2 ;
            end
            else begin
                out_store_1r[0] <= filter1_out_1 ;
                out_store_2r[0] <= filter1_out_2 ;                
            end
        end
        STATE_HOR_FILT4: begin
            if(c_frac_x_d[2] == 1'b1 && (|(c_frac_x_d[1:0]))) begin
                out_store_1r[0] <= filter1_out_1 ;
                out_store_2r[0] <= filter1_out_2 ;
            end
            else begin
                out_store_1r[1] <= filter1_out_1 ;
                out_store_2r[1] <= filter1_out_2 ;                
            end
            block_ready_out <= 1;
        end
        STATE_HOR_VERT_FILT3: begin
            if(c_frac_x_d[2] == 1'b1 && (|(c_frac_x_d[1:0]))) begin
                out_store_1r[1] <= filter1_out_1 ;
                out_store_2r[1] <= filter1_out_2 ;
                out_store_3r[1] <= filter2_out_1;
            end
            else begin
                out_store_1r[0] <= filter1_out_1 ;
                out_store_2r[0] <= filter1_out_2 ;
                out_store_3r[0] <= filter2_out_1;                
            end
            for(j=0; j< REF_BLOCK_WIDTH -1 ; j= j+1) begin
                filter1_in_1[j] <= filter1_in_1[j+1]; // left shift
                filter1_in_2[j] <= filter1_in_2[j+1];
            end
        end
        STATE_HOR_VERT_FILT4: begin
            if(c_frac_x_d[2] == 1'b1 && (|(c_frac_x_d[1:0]))) begin
                out_store_1r[0] <= filter1_out_1 ;
                out_store_2r[0] <= filter1_out_2 ;
                out_store_3r[0] <= filter2_out_1 ;     
            end
            else begin
                out_store_1r[1] <= filter1_out_1 ;
                out_store_2r[1] <= filter1_out_2 ;
                out_store_3r[1] <= filter2_out_1 ;                            
            end       
        end
        STATE_HOR_VERT_FILT5: begin
            // if(c_frac_x_d[2] == 1'b1 && (|(c_frac_x_d[1:0]))) begin
            //     out_store_4r[1] <= filter1_out_1 ;
            //     out_store_5r[1] <= filter1_out_2 ;
            // end
            // else begin
                out_store_4r <= filter1_out_1 ;
                out_store_5r <= filter1_out_2 ;                
            // end
        end
        STATE_HOR_VERT_FILT6: begin
            // out_store_4r[1] <= filter1_out_1 ;
            // out_store_5r[1] <= filter1_out_2 ;
            level <= 1;
            if(c_frac_y_d[2] == 0 || c_frac_y_d == 3'b100) begin    // no flip condition
                filt_type <= c_frac_y_d[1:0];
                    filter1_in_1[0] <= {out_store_1r[0]};
                    filter1_in_1[1] <= {out_store_2r[0]};
                    filter1_in_1[2] <= {out_store_3r[0]};
                if(c_frac_x_d[2] == 1'b1 && (|(c_frac_x_d[1:0]))) begin
                    filter1_in_1[3] <= {filter1_out_1};
                    filter1_in_1[4] <= {filter1_out_2};  
                    filter1_in_2[3] <= {out_store_4r};
                    filter1_in_2[4] <= {out_store_5r}; 
                end
                else begin
                    filter1_in_1[3] <= {out_store_4r};
                    filter1_in_1[4] <= {out_store_5r};
                    filter1_in_2[3] <= {filter1_out_1};
                    filter1_in_2[4] <= {filter1_out_2};  
                end
                filter1_in_2[0] <= {out_store_1r[1]};
                filter1_in_2[1] <= {out_store_2r[1]};
                filter1_in_2[2] <= {out_store_3r[1]};          
            end
            else begin
                filt_type <= filt_type_y_alt;
                filter1_in_1[4] <= {out_store_1r[0]};
                filter1_in_1[3] <= {out_store_2r[0]};
                filter1_in_1[2] <= {out_store_3r[0]};
                if(c_frac_x_d[2] == 1'b1 && (|(c_frac_x_d[1:0]))) begin
                    filter1_in_1[1] <= {filter1_out_1};
                    filter1_in_1[0] <= {filter1_out_2};  
                    filter1_in_2[1] <= {out_store_4r};
                    filter1_in_2[0] <= {out_store_5r}; 
                end
                else begin
                    filter1_in_1[1] <= {out_store_4r};
                    filter1_in_1[0] <= {out_store_5r};
                    filter1_in_2[1] <= {filter1_out_1};
                    filter1_in_2[0] <= {filter1_out_2};  
                end
                filter1_in_2[4] <= {out_store_1r[1]};
                filter1_in_2[3] <= {out_store_2r[1]};
                filter1_in_2[2] <= {out_store_3r[1]};
            end
        end      
        STATE_VER_FILT1: begin
            for(j=0; j< REF_BLOCK_WIDTH -1 ; j= j+1) begin
                filter1_in_1[j] <= filter1_in_1[j+1]; // left shift
                filter1_in_2[j] <= filter1_in_2[j+1];
            end
        end
        STATE_VER_FILT2: begin
        end
        STATE_VER_FILT3: begin
            if(c_frac_y_d[2] == 0 || c_frac_y_d == 3'b100) begin    // no flip condition
                out_store_1r[0] <= filter1_out_1 ;
                out_store_1r[1] <= filter1_out_2 ;
            end
            else begin
                out_store_2r[0] <= filter1_out_1 ;
                out_store_2r[1] <= filter1_out_2 ;                
            end

        end
        STATE_VER_FILT4: begin
            if(c_frac_y_d[2] == 0 || c_frac_y_d == 3'b100) begin    // no flip condition
                out_store_2r[0] <= filter1_out_1 ;
                out_store_2r[1] <= filter1_out_2 ;
            end
            else begin
                out_store_1r[0] <= filter1_out_1 ;
                out_store_1r[1] <= filter1_out_2 ;                
            end
            block_ready_out <= 1;
        end

    endcase
end



always@(posedge clk) begin    
    if(reset) begin
        filter_state <= STATE_IDLE;
    end
    else begin
        filter_state <= next_state;
    end
end


endmodule 