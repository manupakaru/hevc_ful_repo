#! /usr/bin/env python

import os
import shutil

testDirPath=os.getcwd()
if not os.path.exists(testDirPath + "/simout/"): os.makedirs(testDirPath + "/simout/")


#The single design we started to test and continued to use the same name
#need to do better naming for the test cases in future
magicName = "BQSquare_416x240_60_qp37_416x240_8bit"

print 'hello'

#Copy input files
shutil.copyfile("input_data/residual_to_inter",os.environ["HEVCPATH"] + "/sim/residual_to_inter")
shutil.copyfile("input_data/residual_to_inter_Cb",os.environ["HEVCPATH"] + "/sim/residual_to_inter_Cb")
shutil.copyfile("input_data/residual_to_inter_Cr",os.environ["HEVCPATH"] + "/sim/residual_to_inter_Cr")
shutil.copyfile("input_data/residual_to_inter_Y",os.environ["HEVCPATH"] + "/sim/residual_to_inter_Y")

#Copy verify files
shutil.copyfile("verify_files/" + magicName + "_final.yuv",os.environ["HEVCPATH"] + "/sim/" + magicName + "_final.yuv")
shutil.copyfile("verify_files/" + magicName + "_predicted.yuv",os.environ["HEVCPATH"] + "/sim/" + magicName + "_predicted.yuv")
shutil.copyfile("verify_files/" + magicName + "_pre-lp.yuv",os.environ["HEVCPATH"] + "/sim/" + magicName + "_pre-lp.yuv")
shutil.copyfile("verify_files/" + magicName + "_pre-sao.yuv",os.environ["HEVCPATH"] + "/sim/" + magicName + "_pre-sao.yuv")
try :
    shutil.copyfile("verify_files/" + magicName + "_final_dec_order.yuv",os.environ["HEVCPATH"] + "/sim/" + magicName + "_final_dec_order.yuv")
except :
    print 'final decoding order file not found! , might be an old testcase'

#Copy output files
shutil.copyfile("output_data/pred_to_dbf_bs",os.environ["HEVCPATH"] + "/sim/pred_to_dbf_bs")
shutil.copyfile("output_data/pred_to_dbf_cb",os.environ["HEVCPATH"] + "/sim/pred_to_dbf_cb")
shutil.copyfile("output_data/pred_to_dbf_cr",os.environ["HEVCPATH"] + "/sim/pred_to_dbf_cr")
shutil.copyfile("output_data/pred_to_dbf_yy",os.environ["HEVCPATH"] + "/sim/pred_to_dbf_yy")
shutil.copyfile("output_data/pred_to_dbf_config",os.environ["HEVCPATH"] + "/sim/pred_to_dbf_config")

os.chdir(os.environ["HEVCPATH"] + "/sim/")
os.system("rm *.wlf")
os.system("vsim -c -do hevc_test_tb_4k_fast.do")
shutil.copyfile("transcript",testDirPath + "/simout/transcript")
os.system("cp *.wlf " + testDirPath + "/simout/")
os.chdir(testDirPath)










