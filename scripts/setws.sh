echo 'Please make sure that you have configured VSIM and XILINX variables properly'
echo 'If you do not know how to do this, have a look .bashrc of manupa@pqCentosServ1'

export HEVCPATH=$(pwd) 
export XILINX=$XILINX_VIVADO/data 
export PATH=$HEVCPATH/scripts/:$PATH

echo 'Setting workspace at '$HEVCPATH
