// Global parameter definitions
`define     QP_WIDTH            6
`define     BS_WIDTH            2
`define     VERT_UP             3'b000
`define     VERT_DOWN           3'b001
`define     HOR_LEFT            3'b010
`define     HOR_RIGHT           3'b011
`define     HOR_FAR_RIGHT       3'b100
`define     CB                  0
`define     CR                  1
`define     MAX_PIC_WIDTH       1920
`define     MAX_PIC_HEIGHT      1080