vlog -L work ../dbf/rtl/*.v
vlog -L work ../inter/*.v
vlog -L work ../intra/rtl/*.v
vlog -L work ../rtl/*.v
vlog -L work ../inter/cache_single_bank/*.v

vlog  -L work ../dbf/rtl/ipcore_dir/fifo_block_ram_yy/dbf_yy_fifo.v
vlog  -L work ../dbf/rtl/ipcore_dir/fifo_block_ram_ch/dbf_ch_fifo.v
vlog  -L work ../dbf/rtl/ipcore_dir/dbf_fifo_config/dbf_config_fifo.v
vlog  -L work ../dbf/rtl/ipcore_dir/fifo_ref_buf_yy/fifo_ref_buf_yy.v
vlog  $env(XILINX)/verilog/src/glbl.v

vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_of_pre_fifo.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_if_post_fifo.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_byte_group_io.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_byte_lane.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/controller/mig_7series_v1_9_round_robin_arb.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_phy_4lanes.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/controller/mig_7series_v1_9_bank_state.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/controller/mig_7series_v1_9_bank_queue.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/controller/mig_7series_v1_9_bank_compare.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/controller/mig_7series_v1_9_arb_select.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/controller/mig_7series_v1_9_arb_row_col.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_ddr_carry_latch_or.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_ddr_carry_and.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_prbs_gen.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_phy_wrlvl_off_delay.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_phy_wrlvl.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_phy_wrcal.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_phy_tempmon.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_phy_rdlvl.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_phy_prbs_rdlvl.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_phy_oclkdelay_cal.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_phy_init.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_phy_dqs_found_cal_hr.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_phy_dqs_found_cal.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_phy_ck_addr_cmd_delay.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_mc_phy.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/controller/mig_7series_v1_9_rank_common.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/controller/mig_7series_v1_9_rank_cntrl.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/controller/mig_7series_v1_9_bank_common.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/controller/mig_7series_v1_9_bank_cntrl.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/controller/mig_7series_v1_9_arb_mux.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_ddr_comparator_sel_static.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_ddr_comparator_sel.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_ddr_comparator.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_ddr_command_fifo.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_ddr_carry_or.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_ddr_carry_latch_and.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_ddr_axic_register_slice.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_mc_wrap_cmd.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_mc_incr_cmd.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_mc_phy_wrapper.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_calib_top.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/ecc/mig_7series_v1_9_ecc_merge_enc.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/ecc/mig_7series_v1_9_ecc_gen.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/ecc/mig_7series_v1_9_ecc_dec_fix.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/ecc/mig_7series_v1_9_ecc_buf.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/controller/mig_7series_v1_9_rank_mach.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/controller/mig_7series_v1_9_col_mach.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/controller/mig_7series_v1_9_bank_mach.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_ddr_w_upsizer.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_ddr_r_upsizer.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_ddr_a_upsizer.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_ddr_axi_register_slice.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_mc_wr_cmd_fsm.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_mc_simple_fifo.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_mc_cmd_translator.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_mc_cmd_fsm.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_ctrl_reg.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_ctrl_addr_decode.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/ui/mig_7series_v1_9_ui_wr_data.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/ui/mig_7series_v1_9_ui_rd_data.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/ui/mig_7series_v1_9_ui_cmd.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/phy/mig_7series_v1_9_ddr_phy_top.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/controller/mig_7series_v1_9_mc.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_ddr_axi_upsizer.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_mc_w_channel.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_mc_r_channel.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_mc_cmd_arbiter.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_mc_b_channel.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_mc_aw_channel.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_mc_ar_channel.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_ctrl_write.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_ctrl_reg_bank.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_ctrl_read.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/ui/mig_7series_v1_9_ui_top.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/ip_top/mig_7series_v1_9_mem_intfc.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_mc.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/axi/mig_7series_v1_9_axi_ctrl_top.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/ip_top/mig_7series_v1_9_memc_ui_top_axi.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/clocking/mig_7series_v1_9_tempmon.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/clocking/mig_7series_v1_9_iodelay_ctrl.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/clocking/mig_7series_v1_9_infrastructure.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/clocking/mig_7series_v1_9_clk_ibuf.v"
vlog  "../memory_slave/ipcore_dir/ddr_mig/ddr_mig/user_design/rtl/ddr_mig.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_a_axi3_conv.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_addr_arbiter_sasd.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_addr_arbiter.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_addr_decoder.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_a_downsizer.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_arbiter_resp.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_a_upsizer.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_axi3_conv.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_axic_fifo.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_axi_clock_converter.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_axic_register_slice.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_axic_reg_srl_fifo.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_axi_crossbar.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_axic_srl_fifo.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_axi_data_fifo.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_axi_downsizer.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_axilite_conv.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_axi_protocol_converter.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_axi_register_slice.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_axi_upsizer.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_b_downsizer.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_carry_and.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_carry_latch_and.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_carry_latch_or.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_carry_or.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_carry.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_command_fifo.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_comparator_mask_static.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_comparator_mask.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_comparator_sel_mask_static.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_comparator_sel_mask.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_comparator_sel_static.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_comparator_sel.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_comparator_static.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_comparator.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_converter_bank.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_crossbar_sasd.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_crossbar.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_data_fifo_bank.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_decerr_slave.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_fifo_gen.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_mux_enc.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_mux.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_ndeep_srl.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_nto1_mux.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_protocol_conv_bank.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_r_axi3_conv.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_r_downsizer.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_register_slice_bank.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_r_upsizer.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_si_transactor.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_splitter.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_w_axi3_conv.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_wdata_mux.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_wdata_router.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_w_downsizer.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_w_upsizer.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_axic_sample_cycle_ratio.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_axic_sync_clock_converter.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/ict106_axi_interconnect.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con/hdl/verilog/axi_interconnect_v1_06_a.v"
vlog  "../dbf/rtl/ipcore_dir/inter_con_core/axi_inter_con_sim.v"


vlog -sv -L work ../memory_slave/rtl/add_read.v 
vlog -sv -L work ../memory_slave/rtl/data_read.v 
vlog -sv -L work ../memory_slave/rtl/add_write.v 
vlog -sv -L work ../memory_slave/rtl/data_write.v 
vlog -sv -L work ../memory_slave/rtl/data_resp.v 
vlog -sv -L work ../memory_slave/rtl/memory_module_virtual_memory.sv 
vlog -sv -L work ../memory_slave/rtl/mem_slave_top_module.v 
vlog -sv -L work ../memory_slave/rtl/dummy_pll.v 

vlog  "../memory_slave/ddr_sim/wiredly.v"
vlog  "../memory_slave/ddr_sim/ddr3_model.v"

vlog -sv -L work inter_pred_iface.svi
vlog -L work pred_top_tb.v



vsim -novopt -t 1ps -L xilinxcorelib_ver -L unisims_ver -L unimacro_ver -L secureip -sv_lib pred_inter_intra_software -sv_lib ../memory_slave/rtl/gb1_memory -lib work work.pred_top_tb work.glbl

log -r /*
#run -all