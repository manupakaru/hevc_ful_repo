module hevc_zync_pl_test;

	wire [31:0] fifo_in;
	wire 		write_en_in;
	wire 		input_fifo_is_full;
	
	wire [143:0] 	y_residual_fifo_in;
	wire [8*24-1:0] y_residual_fifo_read;
	wire 			y_residual_fifo_write_en_in;
	
	wire [143:0] 	cb_residual_fifo_in;
	wire [8*24-1:0] cb_residual_fifo_read;
	wire 			cb_residual_fifo_write_en_in;
	
	wire [143:0] 	cr_residual_fifo_in;
	wire [8*24-1:0] cr_residual_fifo_read;
	wire 			cr_residual_fifo_write_en_in;
	
	wire cr_residual_fifo_is_full_out;
	wire cb_residual_fifo_is_full_out;
	wire y_residual_fifo_is_full_out;	
   //***************************************************************************
   // Traffic Gen related parameters
   //***************************************************************************
   parameter SIMULATION            = "TRUE";
//   parameter BL_WIDTH              = 10;
//   parameter ADDR_MODE             = 4'b0011;
   parameter BEGIN_ADDRESS         = 32'h00000000;
   parameter END_ADDRESS           = 32'h00000fff;
   parameter PRBS_EADDR_MASK_POS   = 32'hff000000;

   //***************************************************************************
   // The following parameters refer to width of various ports
   //***************************************************************************
//   parameter BANK_WIDTH            = 3;
                                     // # of memory Bank Address bits.
//   parameter CK_WIDTH              = 1;
                                     // # of CK/CK# outputs to memory.
   parameter COL_WIDTH             = 10;
                                     // # of memory Column Address bits.
   parameter CS_WIDTH              = 1;
                                     // # of unique CS outputs to memory.
//   parameter nCS_PER_RANK          = 1;
                                     // # of unique CS outputs per rank for phy
//   parameter CKE_WIDTH             = 1;
                                     // # of CKE outputs to memory.
//   parameter DATA_BUF_ADDR_WIDTH   = 5;
//   parameter DQ_CNT_WIDTH          = 6;
                                     // = ceil(log2(DQ_WIDTH))
//   parameter DQ_PER_DM             = 8;
   parameter DM_WIDTH              = 8;
                                     // # of DM (data mask)
   parameter DQ_WIDTH              = 64;
                                     // # of DQ (data)
   parameter DQS_WIDTH             = 8;
   parameter DQS_CNT_WIDTH         = 3;
                                     // = ceil(log2(DQS_WIDTH))
   parameter DRAM_WIDTH            = 8;
                                     // # of DQ per DQS
   parameter ECC                   = "OFF";
//   parameter nBANK_MACHS           = 4;
   parameter RANKS                 = 1;
                                     // # of Ranks.
   parameter ODT_WIDTH             = 1;
                                     // # of ODT outputs to memory.
   parameter ROW_WIDTH             = 14;
                                     // # of memory Row Address bits.
   parameter ADDR_WIDTH            = 28;
                                     // # = RANK_WIDTH + BANK_WIDTH
                                     //     + ROW_WIDTH + COL_WIDTH;
                                     // Chip Select is always tied to low for
                                     // single rank devices
//   parameter USE_CS_PORT          = 1;
                                     // # = 1, When CS output is enabled
                                     //   = 0, When CS output is disabled
                                     // If CS_N disabled, user must connect
                                     // DRAM CS_N input(s) to ground
//   parameter USE_DM_PORT           = 1;
                                     // # = 1, When Data Mask option is enabled
                                     //   = 0, When Data Mask option is disbaled
                                     // When Data Mask option is disabled in
                                     // MIG Controller Options page, the logic
                                     // related to Data Mask should not get
                                     // synthesized
//   parameter USE_ODT_PORT          = 1;
                                     // # = 1, When ODT output is enabled
                                     //   = 0, When ODT output is disabled
                                     // Parameter configuration for Dynamic ODT support:
                                     // USE_ODT_PORT = 0, RTT_NOM = "DISABLED", RTT_WR = "60/120".
                                     // This configuration allows to save ODT pin mapping from FPGA.
                                     // The user can tie the ODT input of DRAM to HIGH.

   //***************************************************************************
   // The following parameters are mode register settings
   //***************************************************************************
//   parameter AL                    = "0";
                                     // DDR3 SDRAM:
                                     // Additive Latency (Mode Register 1).
                                     // # = "0", "CL-1", "CL-2".
                                     // DDR2 SDRAM:
                                     // Additive Latency (Extended Mode Register).
//   parameter nAL                   = 0;
                                     // # Additive Latency in number of clock
                                     // cycles.
   parameter BURST_MODE            = "8";
                                     // DDR3 SDRAM:
                                     // Burst Length (Mode Register 0).
                                     // # = "8", "4", "OTF".
                                     // DDR2 SDRAM:
                                     // Burst Length (Mode Register).
                                     // # = "8", "4".
//   parameter BURST_TYPE            = "SEQ";
                                     // DDR3 SDRAM: Burst Type (Mode Register 0).
                                     // DDR2 SDRAM: Burst Type (Mode Register).
                                     // # = "SEQ" - (Sequential),
                                     //   = "INT" - (Interleaved).
//   parameter CL                    = 6;
                                     // in number of clock cycles
                                     // DDR3 SDRAM: CAS Latency (Mode Register 0).
                                     // DDR2 SDRAM: CAS Latency (Mode Register).
//   parameter CWL                   = 5;
                                     // in number of clock cycles
                                     // DDR3 SDRAM: CAS Write Latency (Mode Register 2).
                                     // DDR2 SDRAM: Can be ignored
//   parameter OUTPUT_DRV            = "LOW";
                                     // Output Driver Impedance Control (Mode Register 1).
                                     // # = "HIGH" - RZQ/7,
                                     //   = "LOW" - RZQ/6.
//   parameter RTT_NOM               = "40";
                                     // RTT_NOM (ODT) (Mode Register 1).
                                     // # = "DISABLED" - RTT_NOM disabled,
                                     //   = "120" - RZQ/2,
                                     //   = "60"  - RZQ/4,
                                     //   = "40"  - RZQ/6.
//   parameter RTT_WR                = "OFF";
                                     // RTT_WR (ODT) (Mode Register 2).
                                     // # = "OFF" - Dynamic ODT off,
                                     //   = "120" - RZQ/2,
                                     //   = "60"  - RZQ/4,
//   parameter ADDR_CMD_MODE         = "1T" ;
                                     // # = "1T", "2T".
//   parameter REG_CTRL              = "OFF";
                                     // # = "ON" - RDIMMs,
                                     //   = "OFF" - Components, SODIMMs, UDIMMs.
   parameter CA_MIRROR             = "OFF";
                                     // C/A mirror opt for DDR3 dual rank
   
   //***************************************************************************
   // The following parameters are multiplier and divisor factors for PLLE2.
   // Based on the selected design frequency these parameters vary.
   //***************************************************************************
   parameter CLKIN_PERIOD          = 5000;
                                     // Input Clock Period
//   parameter CLKFBOUT_MULT         = 4;
                                     // write PLL VCO multiplier
//   parameter DIVCLK_DIVIDE         = 1;
                                     // write PLL VCO divisor
//   parameter CLKOUT0_DIVIDE        = 2;
                                     // VCO output divisor for PLL output clock (CLKOUT0)
//   parameter CLKOUT1_DIVIDE        = 2;
                                     // VCO output divisor for PLL output clock (CLKOUT1)
//   parameter CLKOUT2_DIVIDE        = 32;
                                     // VCO output divisor for PLL output clock (CLKOUT2)
//   parameter CLKOUT3_DIVIDE        = 8;
                                     // VCO output divisor for PLL output clock (CLKOUT3)

   //***************************************************************************
   // Memory Timing Parameters. These parameters varies based on the selected
   // memory part.
   //***************************************************************************
//   parameter tCKE                  = 5000;
                                     // memory tCKE paramter in pS
//   parameter tFAW                  = 30000;
                                     // memory tRAW paramter in pS.
//   parameter tRAS                  = 35000;
                                     // memory tRAS paramter in pS.
//   parameter tRCD                  = 13125;
                                     // memory tRCD paramter in pS.
//   parameter tREFI                 = 7800000;
                                     // memory tREFI paramter in pS.
//   parameter tRFC                  = 110000;
                                     // memory tRFC paramter in pS.
//   parameter tRP                   = 13125;
                                     // memory tRP paramter in pS.
//   parameter tRRD                  = 6000;
                                     // memory tRRD paramter in pS.
//   parameter tRTP                  = 7500;
                                     // memory tRTP paramter in pS.
//   parameter tWTR                  = 7500;
                                     // memory tWTR paramter in pS.
//   parameter tZQI                  = 128_000_000;
                                     // memory tZQI paramter in nS.
//   parameter tZQCS                 = 64;
                                     // memory tZQCS paramter in clock cycles.

   //***************************************************************************
   // Simulation parameters
   //***************************************************************************
   parameter SIM_BYPASS_INIT_CAL   = "FAST";
                                     // # = "SIM_INIT_CAL_FULL" -  Complete
                                     //              memory init &
                                     //              calibration sequence
                                     // # = "SKIP" - Not supported
                                     // # = "FAST" - Complete memory init & use
                                     //              abbreviated calib sequence

   //***************************************************************************
   // The following parameters varies based on the pin out entered in MIG GUI.
   // Do not change any of these parameters directly by editing the RTL.
   // Any changes required should be done through GUI and the design regenerated.
   //***************************************************************************
//   parameter BYTE_LANES_B0         = 4'b1111;
                                     // Byte lanes used in an IO column.
//   parameter BYTE_LANES_B1         = 4'b1110;
                                     // Byte lanes used in an IO column.
//   parameter BYTE_LANES_B2         = 4'b1111;
                                     // Byte lanes used in an IO column.
//   parameter BYTE_LANES_B3         = 4'b0000;
                                     // Byte lanes used in an IO column.
//   parameter BYTE_LANES_B4         = 4'b0000;
                                     // Byte lanes used in an IO column.
//   parameter DATA_CTL_B0           = 4'b1111;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
//   parameter DATA_CTL_B1           = 4'b0000;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
//   parameter DATA_CTL_B2           = 4'b1111;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
//   parameter DATA_CTL_B3           = 4'b0000;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
//   parameter DATA_CTL_B4           = 4'b0000;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
//   parameter PHY_0_BITLANES        = 48'h3FE_1FF_1FF_2FF;
//   parameter PHY_1_BITLANES        = 48'hFFE_F30_CB4_000;
//   parameter PHY_2_BITLANES        = 48'h3FE_3FE_3BF_2FF;

   // control/address/data pin mapping parameters
//   parameter CK_BYTE_MAP
//     = 144'h00_00_00_00_00_00_00_00_00_00_00_00_00_00_00_00_00_11;
//   parameter ADDR_MAP
//     = 192'h000_000_132_136_135_133_139_124_131_129_137_134_13A_128_138_13B;
//   parameter BANK_MAP   = 36'h125_12A_12B;
//   parameter CAS_MAP    = 12'h115;
//   parameter CKE_ODT_BYTE_MAP = 8'h00;
//   parameter CKE_MAP    = 96'h000_000_000_000_000_000_000_117;
//   parameter ODT_MAP    = 96'h000_000_000_000_000_000_000_112;
//   parameter CS_MAP     = 120'h000_000_000_000_000_000_000_000_000_114;
//   parameter PARITY_MAP = 12'h000;
//   parameter RAS_MAP    = 12'h11A;
//   parameter WE_MAP     = 12'h11B;
//   parameter DQS_BYTE_MAP
//     = 144'h00_00_00_00_00_00_00_00_00_00_20_21_22_23_03_02_01_00;
//   parameter DATA0_MAP  = 96'h009_000_003_001_007_006_005_002;
//   parameter DATA1_MAP  = 96'h014_018_010_011_017_016_012_013;
//   parameter DATA2_MAP  = 96'h021_022_025_020_027_023_026_028;
//   parameter DATA3_MAP  = 96'h033_039_031_035_032_038_034_037;
//   parameter DATA4_MAP  = 96'h231_238_237_236_233_232_234_239;
//   parameter DATA5_MAP  = 96'h226_227_225_229_221_222_224_228;
//   parameter DATA6_MAP  = 96'h214_215_210_218_217_213_219_212;
//   parameter DATA7_MAP  = 96'h207_203_204_206_202_201_205_209;
//   parameter DATA8_MAP  = 96'h000_000_000_000_000_000_000_000;
//   parameter DATA9_MAP  = 96'h000_000_000_000_000_000_000_000;
//   parameter DATA10_MAP = 96'h000_000_000_000_000_000_000_000;
//   parameter DATA11_MAP = 96'h000_000_000_000_000_000_000_000;
//   parameter DATA12_MAP = 96'h000_000_000_000_000_000_000_000;
//   parameter DATA13_MAP = 96'h000_000_000_000_000_000_000_000;
//   parameter DATA14_MAP = 96'h000_000_000_000_000_000_000_000;
//   parameter DATA15_MAP = 96'h000_000_000_000_000_000_000_000;
//   parameter DATA16_MAP = 96'h000_000_000_000_000_000_000_000;
//   parameter DATA17_MAP = 96'h000_000_000_000_000_000_000_000;
//   parameter MASK0_MAP  = 108'h000_200_211_223_235_036_024_015_004;
//   parameter MASK1_MAP  = 108'h000_000_000_000_000_000_000_000_000;

//   parameter SLOT_0_CONFIG         = 8'b0000_0001;
                                     // Mapping of Ranks.
//   parameter SLOT_1_CONFIG         = 8'b0000_0000;
                                     // Mapping of Ranks.
//   parameter MEM_ADDR_ORDER        = "BANK_ROW_COLUMN";
                                      //Possible Parameters
                                      //1.BANK_ROW_COLUMN : Address mapping is
                                      //                    in form of Bank Row Column.
                                      //2.ROW_BANK_COLUMN : Address mapping is
                                      //                    in the form of Row Bank Column.
                                      //3.TG_TEST : Scrambles Address bits
                                      //            for distributed Addressing.
   //***************************************************************************
   // IODELAY and PHY related parameters
   //***************************************************************************
//   parameter IBUF_LPWR_MODE        = "OFF";
                                     // to phy_top
//   parameter DATA_IO_IDLE_PWRDWN   = "OFF";
                                     // # = "ON", "OFF"
//   parameter DATA_IO_PRIM_TYPE     = "DEFAULT";
                                     // # = "HP_LP", "HR_LP", "DEFAULT"
//   parameter USER_REFRESH          = "OFF";
//   parameter WRLVL                 = "ON";
                                     // # = "ON" - DDR3 SDRAM
                                     //   = "OFF" - DDR2 SDRAM.
//   parameter ORDERING              = "NORM";
                                     // # = "NORM", "STRICT", "RELAXED".
//   parameter CALIB_ROW_ADD         = 16'h0000;
                                     // Calibration row address will be used for
                                     // calibration read and write operations
//   parameter CALIB_COL_ADD         = 12'h000;
                                     // Calibration column address will be used for
                                     // calibration read and write operations
//   parameter CALIB_BA_ADD          = 3'h0;
                                     // Calibration bank address will be used for
                                     // calibration read and write operations
   parameter TCQ                   = 100;
   //***************************************************************************
   // IODELAY and PHY related parameters
   //***************************************************************************
//   parameter IODELAY_GRP           = "MIG_7SERIES_0_IODELAY_MIG";
                                     // It is associated to a set of IODELAYs with
                                     // an IDELAYCTRL that have same IODELAY CONTROLLER
                                     // clock frequency.
//   parameter SYSCLK_TYPE           = "DIFFERENTIAL";
                                     // System clock type DIFFERENTIAL, SINGLE_ENDED,
                                     // NO_BUFFER
//   parameter REFCLK_TYPE           = "USE_SYSTEM_CLOCK";
                                     // Reference clock type DIFFERENTIAL, SINGLE_ENDED,
                                     // NO_BUFFER, USE_SYSTEM_CLOCK
   parameter RST_ACT_LOW           = 0;
                                     // =1 for active low reset,
                                     // =0 for active high.
//   parameter CAL_WIDTH             = "HALF";
//   parameter STARVE_LIMIT          = 2;
                                     // # = 2,3,4.

   //***************************************************************************
   // Referece clock frequency parameters
   //***************************************************************************
   parameter REFCLK_FREQ           = 200.0;
                                     // IODELAYCTRL reference clock frequency
   //***************************************************************************
   // System clock frequency parameters
   //***************************************************************************
   parameter tCK                   = 2500;
                                     // memory tCK paramter.
                     // # = Clock Period in pS.
   parameter nCK_PER_CLK           = 4;
                                     // # of memory CKs per fabric CLK

   
   //***************************************************************************
   // AXI4 Shim parameters
   //***************************************************************************
   parameter C_S_AXI_ID_WIDTH              = 4;
                                             // Width of all master and slave ID signals.
                                             // # = >= 1.
   parameter C_S_AXI_ADDR_WIDTH            = 30;
                                             // Width of S_AXI_AWADDR, S_AXI_ARADDR, M_AXI_AWADDR and
                                             // M_AXI_ARADDR for all SI/MI slots.
                                             // # = 32.
   parameter C_S_AXI_DATA_WIDTH            = 512;
                                             // Width of WDATA and RDATA on SI slot.
                                             // Must be <= APP_DATA_WIDTH.
                                             // # = 32, 64, 128, 256.
//   parameter C_MC_nCK_PER_CLK              = 4;
                                             // Indicates whether to instatiate upsizer
                                             // Range: 0, 1
   parameter C_S_AXI_SUPPORTS_NARROW_BURST = 0;
                                             // Indicates whether to instatiate upsizer
                                             // Range: 0, 1
//   parameter C_RD_WR_ARB_ALGORITHM          = "RD_PRI_REG";
                                             // Indicates the Arbitration
                                             // Allowed values - "TDM", "ROUND_ROBIN",
                                             // "RD_PRI_REG", "RD_PRI_REG_STARVE_LIMIT"
//   parameter C_S_AXI_REG_EN0               = 20'h00000;
                                             // C_S_AXI_REG_EN0[00] = Reserved
                                             // C_S_AXI_REG_EN0[04] = AW CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN0[05] =  W CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN0[06] =  B CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN0[07] =  R CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN0[08] = AW CHANNEL UPSIZER REGISTER SLICE
                                             // C_S_AXI_REG_EN0[09] =  W CHANNEL UPSIZER REGISTER SLICE
                                             // C_S_AXI_REG_EN0[10] = AR CHANNEL UPSIZER REGISTER SLICE
                                             // C_S_AXI_REG_EN0[11] =  R CHANNEL UPSIZER REGISTER SLICE
//   parameter C_S_AXI_REG_EN1                 = 20'h00000;
                                             // Instatiates register slices after the upsizer.
                                             // The type of register is specified for each channel
                                             // in a vector. 4 bits per channel are used.
                                             // C_S_AXI_REG_EN1[03:00] = AW CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN1[07:04] =  W CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN1[11:08] =  B CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN1[15:12] = AR CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN1[20:16] =  R CHANNEL REGISTER SLICE
                                             // Possible values for each channel are:
                                             //
                                             //   0 => BYPASS    = The channel is just wired through the
                                             //                    module.
                                             //   1 => FWD       = The master VALID and payload signals
                                             //                    are registrated.
                                             //   2 => REV       = The slave ready signal is registrated
                                             //   3 => FWD_REV   = Both FWD and REV
                                             //   4 => SLAVE_FWD = All slave side signals and master
                                             //                    VALID and payload are registrated.
                                             //   5 => SLAVE_RDY = All slave side signals and master
                                             //                    READY are registrated.
                                             //   6 => INPUTS    = Slave and Master side inputs are
                                             //                    registrated.
                                             //   7 => ADDRESS   = Optimized for address channel

   //***************************************************************************
   // Debug and Internal parameters
   //***************************************************************************
   parameter DEBUG_PORT            = "OFF";
                                     // # = "ON" Enable debug signals/controls.
                                     //   = "OFF" Disable debug signals/controls.
   //***************************************************************************
   // Debug and Internal parameters
   //***************************************************************************
   parameter DRAM_TYPE             = "DDR3";

    

  //**************************************************************************//
  // Local parameters Declarations
  //**************************************************************************//

  localparam real TPROP_DQS          = 0.00;
                                       // Delay for DQS signal during Write Operation
  localparam real TPROP_DQS_RD       = 0.00;
                       // Delay for DQS signal during Read Operation
  localparam real TPROP_PCB_CTRL     = 0.00;
                       // Delay for Address and Ctrl signals
  localparam real TPROP_PCB_DATA     = 0.00;
                       // Delay for data signal during Write operation
  localparam real TPROP_PCB_DATA_RD  = 0.00;
                       // Delay for data signal during Read operation

  localparam MEMORY_WIDTH            = 8;
  localparam NUM_COMP                = DQ_WIDTH/MEMORY_WIDTH;
  localparam ECC_TEST 		   	= "OFF" ;
  localparam ERR_INSERT = (ECC_TEST == "ON") ? "OFF" : ECC ;
  

  localparam real REFCLK_PERIOD = (1000000.0/(2*REFCLK_FREQ));
  localparam RESET_PERIOD = 200000; //in pSec  
  localparam real SYSCLK_PERIOD = tCK;
    
    

  //**************************************************************************//
  // Wire Declarations
	reg wr_clk;
	wire display_fifo_read_inf_empty;
	wire display_fifo_read_inf_rd_en;
	wire [784-1:0] display_fifo_read_inf_rd_data;
  //**************************************************************************//
  reg                                sys_rst_n;
  wire                               sys_rst;


  reg                     sys_clk_i;
  wire                               sys_clk_p;
  wire                               sys_clk_n;
    

  reg clk_ref_i;

  
  wire                               ddr3_reset_n;
  wire [DQ_WIDTH-1:0]                ddr3_dq_fpga;
  wire [DQS_WIDTH-1:0]               ddr3_dqs_p_fpga;
  wire [DQS_WIDTH-1:0]               ddr3_dqs_n_fpga;
  wire [ROW_WIDTH-1:0]               ddr3_addr_fpga;
  wire [3-1:0]              ddr3_ba_fpga;
  wire                               ddr3_ras_n_fpga;
  wire                               ddr3_cas_n_fpga;
  wire                               ddr3_we_n_fpga;
  wire [1-1:0]               ddr3_cke_fpga;
  wire [1-1:0]                ddr3_ck_p_fpga;
  wire [1-1:0]                ddr3_ck_n_fpga;
    
  
  wire                               init_calib_complete;
  wire                               tg_compare_error;
  wire [(CS_WIDTH*1)-1:0] ddr3_cs_n_fpga;
    
  wire [DM_WIDTH-1:0]                ddr3_dm_fpga;
    
  wire [ODT_WIDTH-1:0]               ddr3_odt_fpga;
    
  
  reg [(CS_WIDTH*1)-1:0] ddr3_cs_n_sdram_tmp;
    
  reg [DM_WIDTH-1:0]                 ddr3_dm_sdram_tmp;
    
  reg [ODT_WIDTH-1:0]                ddr3_odt_sdram_tmp;
    

  
  wire [DQ_WIDTH-1:0]                ddr3_dq_sdram;
  reg [ROW_WIDTH-1:0]                ddr3_addr_sdram [0:1];
  reg [3-1:0]               ddr3_ba_sdram [0:1];
  reg                                ddr3_ras_n_sdram;
  reg                                ddr3_cas_n_sdram;
  reg                                ddr3_we_n_sdram;
  wire [(CS_WIDTH*1)-1:0] ddr3_cs_n_sdram;
  wire [ODT_WIDTH-1:0]               ddr3_odt_sdram;
  reg [1-1:0]                ddr3_cke_sdram;
  wire [DM_WIDTH-1:0]                ddr3_dm_sdram;
  wire [DQS_WIDTH-1:0]               ddr3_dqs_p_sdram;
  wire [DQS_WIDTH-1:0]               ddr3_dqs_n_sdram;
  reg [1-1:0]                 ddr3_ck_p_sdram;
  reg [1-1:0]                 ddr3_ck_n_sdram;
  
    

//**************************************************************************//

  //**************************************************************************//
  // Reset Generation
  //**************************************************************************//
  initial begin
    sys_rst_n = 1'b0;
    #RESET_PERIOD
      sys_rst_n = 1'b1;
   end

   assign sys_rst = RST_ACT_LOW ? sys_rst_n : ~sys_rst_n;

  //**************************************************************************//
  // Clock Generation
  //**************************************************************************//

  initial
    sys_clk_i = 1'b0;
  always
    sys_clk_i = #(CLKIN_PERIOD/2.0) ~sys_clk_i;

  assign sys_clk_p = sys_clk_i;
  assign sys_clk_n = ~sys_clk_i;

  initial
    clk_ref_i = 1'b0;
  always
    clk_ref_i = #REFCLK_PERIOD ~clk_ref_i;




  always @( * ) begin
    ddr3_ck_p_sdram      <=  #(TPROP_PCB_CTRL) ddr3_ck_p_fpga;
    ddr3_ck_n_sdram      <=  #(TPROP_PCB_CTRL) ddr3_ck_n_fpga;
    ddr3_addr_sdram[0]   <=  #(TPROP_PCB_CTRL) ddr3_addr_fpga;
    ddr3_addr_sdram[1]   <=  #(TPROP_PCB_CTRL) (CA_MIRROR == "ON") ?
                                                 {ddr3_addr_fpga[ROW_WIDTH-1:9],
                                                  ddr3_addr_fpga[7], ddr3_addr_fpga[8],
                                                  ddr3_addr_fpga[5], ddr3_addr_fpga[6],
                                                  ddr3_addr_fpga[3], ddr3_addr_fpga[4],
                                                  ddr3_addr_fpga[2:0]} :
                                                 ddr3_addr_fpga;
    ddr3_ba_sdram[0]     <=  #(TPROP_PCB_CTRL) ddr3_ba_fpga;
    ddr3_ba_sdram[1]     <=  #(TPROP_PCB_CTRL) (CA_MIRROR == "ON") ?
                                                 {ddr3_ba_fpga[3-1:2],
                                                  ddr3_ba_fpga[0],
                                                  ddr3_ba_fpga[1]} :
                                                 ddr3_ba_fpga;
    ddr3_ras_n_sdram     <=  #(TPROP_PCB_CTRL) ddr3_ras_n_fpga;
    ddr3_cas_n_sdram     <=  #(TPROP_PCB_CTRL) ddr3_cas_n_fpga;
    ddr3_we_n_sdram      <=  #(TPROP_PCB_CTRL) ddr3_we_n_fpga;
    ddr3_cke_sdram       <=  #(TPROP_PCB_CTRL) ddr3_cke_fpga;
  end
    

  always @( * )
    ddr3_cs_n_sdram_tmp   <=  #(TPROP_PCB_CTRL) ddr3_cs_n_fpga;
  assign ddr3_cs_n_sdram =  ddr3_cs_n_sdram_tmp;
    

  always @( * )
    ddr3_dm_sdram_tmp <=  #(TPROP_PCB_DATA) ddr3_dm_fpga;//DM signal generation
  assign ddr3_dm_sdram = ddr3_dm_sdram_tmp;
    

  always @( * )
    ddr3_odt_sdram_tmp  <=  #(TPROP_PCB_CTRL) ddr3_odt_fpga;
  assign ddr3_odt_sdram =  ddr3_odt_sdram_tmp;
    

// Controlling the bi-directional BUS

  genvar dqwd;
  generate
    for (dqwd = 1;dqwd < DQ_WIDTH;dqwd = dqwd+1) begin : dq_delay
      WireDelay #
       (
        .Delay_g    (TPROP_PCB_DATA),
        .Delay_rd   (TPROP_PCB_DATA_RD),
        .ERR_INSERT ("OFF")
       )
      u_delay_dq
       (
        .A             (ddr3_dq_fpga[dqwd]),
        .B             (ddr3_dq_sdram[dqwd]),
        .reset         (sys_rst_n),
        .phy_init_done (init_calib_complete)
       );
    end
    // For ECC ON case error is inserted on LSB bit from DRAM to FPGA
          WireDelay #
       (
        .Delay_g    (TPROP_PCB_DATA),
        .Delay_rd   (TPROP_PCB_DATA_RD),
        .ERR_INSERT (ERR_INSERT)
       )
      u_delay_dq_0
       (
        .A             (ddr3_dq_fpga[0]),
        .B             (ddr3_dq_sdram[0]),
        .reset         (sys_rst_n),
        .phy_init_done (init_calib_complete)
       );
  endgenerate

  genvar dqswd;
  generate
    for (dqswd = 0;dqswd < DQS_WIDTH;dqswd = dqswd+1) begin : dqs_delay
      WireDelay #
       (
        .Delay_g    (TPROP_DQS),
        .Delay_rd   (TPROP_DQS_RD),
        .ERR_INSERT ("OFF")
       )
      u_delay_dqs_p
       (
        .A             (ddr3_dqs_p_fpga[dqswd]),
        .B             (ddr3_dqs_p_sdram[dqswd]),
        .reset         (sys_rst_n),
        .phy_init_done (init_calib_complete)
       );

      WireDelay #
       (
        .Delay_g    (TPROP_DQS),
        .Delay_rd   (TPROP_DQS_RD),
        .ERR_INSERT ("OFF")
       )
      u_delay_dqs_n
       (
        .A             (ddr3_dqs_n_fpga[dqswd]),
        .B             (ddr3_dqs_n_sdram[dqswd]),
        .reset         (sys_rst_n),
        .phy_init_done (init_calib_complete)
       );
    end
  endgenerate
    

    
ful_design_wrapper ful_design_i
	(
        .config_fifo_write_inf_full(input_fifo_is_full),
        .config_fifo_write_inf_wr_data(fifo_in),
        .config_fifo_write_inf_wr_en(write_en_in),
		
        .y_residual_fifo_write_full(y_residual_fifo_is_full_out),
        .y_residual_fifo_write_wr_data(y_residual_fifo_in),
        .y_residual_fifo_write_wr_en(y_residual_fifo_write_en_in),
		
        .cb_residual_fifo_write_inf_full(cb_residual_fifo_is_full_out),
        .cb_residual_fifo_write_inf_wr_data(cb_residual_fifo_in),
        .cb_residual_fifo_write_inf_wr_en(cb_residual_fifo_write_en_in),
		
        .cr_residual_fifo_write_inf_full(cr_residual_fifo_is_full_out),
        .cr_residual_fifo_write_inf_wr_data(cr_residual_fifo_in),
        .cr_residual_fifo_write_inf_wr_en(cr_residual_fifo_write_en_in),
		
        .ddr3_inf_addr		(ddr3_inf_addr),
        .ddr3_inf_ba		(ddr3_inf_ba),
        .ddr3_inf_cas_n		(ddr3_inf_cas_n),
        .ddr3_inf_ck_n		(ddr3_inf_ck_n),
        .ddr3_inf_ck_p		(ddr3_inf_ck_p),
        .ddr3_inf_cke		(ddr3_inf_cke),
        .ddr3_inf_cs_n		(ddr3_inf_cs_n),
        .ddr3_inf_dm		(ddr3_inf_dm),
        .ddr3_inf_dq		(ddr3_inf_dq),
        .ddr3_inf_dqs_n		(ddr3_inf_dqs_n),
        .ddr3_inf_dqs_p		(ddr3_inf_dqs_p),
        .ddr3_inf_odt		(ddr3_inf_odt),
        .ddr3_inf_ras_n		(ddr3_inf_ras_n),
        .ddr3_inf_reset_n	(ddr3_inf_reset_n),
        .ddr3_inf_we_n		(ddr3_inf_we_n),
        .enable				(1),
		.up_stream_reset	(up_stream_reset),
        .ref_input_clk_clk_n(ref_input_clk_clk_n),
        .ref_input_clk_clk_p(ref_input_clk_clk_p),
        .sys_rst			(sys_rst),

        .display_fifo_read_inf_empty(display_fifo_read_inf_empty),
        .display_fifo_read_inf_rd_data(display_fifo_read_inf_rd_data),
        .display_fifo_read_inf_rd_en(display_fifo_read_inf_rd_en),
		
        .wr_clk				(wr_clk)

		
		);
		
	//replace with display buffer write module
fifo_read_driver display_read (
    .empty(display_fifo_read_inf_empty), 
    .rd_en(display_fifo_read_inf_rd_en)
    );

	
fifo_write_driver 
	#(
		.WIDTH 		(32),
		.FILE_NAME  ("residual_to_inter"),
		.EMPTY_MODEL(0)
	)
	pred_config_driver (
    .clk(wr_clk), 
    .reset(up_stream_reset), 
    .out(fifo_in), 
    .prog_full(input_fifo_is_full), 
    .wr_en(write_en_in)
    );
	

fifo_write_driver 
	#(
		.WIDTH 		(8*24),
		.FILE_NAME  ("residual_to_inter_Y"),
		.EMPTY_MODEL(0)
	)
	pred_res_yy_driver (
    .clk(wr_clk), 
    .reset(up_stream_reset), 
    .out(y_residual_fifo_read), 
    .prog_full(y_residual_fifo_is_full_out), 
    .wr_en(y_residual_fifo_write_en_in)
    );	
	
	assign y_residual_fifo_in = {
				y_residual_fifo_read[8*22-8:8*21],
				y_residual_fifo_read[8*21-1:8*20],
				y_residual_fifo_read[8*20-6:8*19],
				y_residual_fifo_read[8*19-1:8*18],
				y_residual_fifo_read[8*18-1:8*17],
				y_residual_fifo_read[8*17-1:8*16],
				y_residual_fifo_read[8*16-6:8*15],
				y_residual_fifo_read[8*15-1:8*14],
				y_residual_fifo_read[8*14-1:8*13],
				y_residual_fifo_read[8*13-1:8*12],
				y_residual_fifo_read[8*12-6:8*11],
				y_residual_fifo_read[8*11-1:8*10],
				y_residual_fifo_read[8*10-1:8* 9],
				y_residual_fifo_read[8* 9-1:8* 8],
				y_residual_fifo_read[8* 8-6:8* 7],
				y_residual_fifo_read[8* 7-1:8* 6],
				y_residual_fifo_read[8* 6-1:8* 5],
				y_residual_fifo_read[8* 5-1:8* 4],
				y_residual_fifo_read[8* 4-6:8* 3],
				y_residual_fifo_read[8* 3-1:8* 2],
				y_residual_fifo_read[8* 2-1:8* 1],
				y_residual_fifo_read[8* 1-1:8* 0]
	};

fifo_write_driver 
	#(
		.WIDTH 		(8*24),
		.FILE_NAME  ("residual_to_inter_Cb"),
		.EMPTY_MODEL(0)
	)
	pred_res_cb_driver (
    .clk(wr_clk), 
    .reset(up_stream_reset), 
    .out(cb_residual_fifo_read), 
    .prog_full(cb_residual_fifo_is_full_out), 
    .wr_en(cb_residual_fifo_write_en_in)
    );	
	
	assign cb_residual_fifo_in = {
				cb_residual_fifo_read[8*22-8:8*21],
				cb_residual_fifo_read[8*21-1:8*20],
				cb_residual_fifo_read[8*20-6:8*19],
				cb_residual_fifo_read[8*19-1:8*18],
				cb_residual_fifo_read[8*18-1:8*17],
				cb_residual_fifo_read[8*17-1:8*16],
				cb_residual_fifo_read[8*16-6:8*15],
				cb_residual_fifo_read[8*15-1:8*14],
				cb_residual_fifo_read[8*14-1:8*13],
				cb_residual_fifo_read[8*13-1:8*12],
				cb_residual_fifo_read[8*12-6:8*11],
				cb_residual_fifo_read[8*11-1:8*10],
				cb_residual_fifo_read[8*10-1:8* 9],
				cb_residual_fifo_read[8* 9-1:8* 8],
				cb_residual_fifo_read[8* 8-6:8* 7],
				cb_residual_fifo_read[8* 7-1:8* 6],
				cb_residual_fifo_read[8* 6-1:8* 5],
				cb_residual_fifo_read[8* 5-1:8* 4],
				cb_residual_fifo_read[8* 4-6:8* 3],
				cb_residual_fifo_read[8* 3-1:8* 2],
				cb_residual_fifo_read[8* 2-1:8* 1],
				cb_residual_fifo_read[8* 1-1:8* 0]
	};


fifo_write_driver 
	#(
		.WIDTH 		(8*24),
		.FILE_NAME  ("residual_to_inter_Cr"),
		.EMPTY_MODEL(0)
	)
	pred_res_cr_driver (
    .clk(wr_clk), 
    .reset(up_stream_reset), 
    .out(cr_residual_fifo_read), 
    .prog_full(cr_residual_fifo_is_full_out), 
    .wr_en(cr_residual_fifo_write_en_in)
    );	
	
	assign cr_residual_fifo_in = {
				cr_residual_fifo_read[8*22-8:8*21],
				cr_residual_fifo_read[8*21-1:8*20],
				cr_residual_fifo_read[8*20-6:8*19],
				cr_residual_fifo_read[8*19-1:8*18],
				cr_residual_fifo_read[8*18-1:8*17],
				cr_residual_fifo_read[8*17-1:8*16],
				cr_residual_fifo_read[8*16-6:8*15],
				cr_residual_fifo_read[8*15-1:8*14],
				cr_residual_fifo_read[8*14-1:8*13],
				cr_residual_fifo_read[8*13-1:8*12],
				cr_residual_fifo_read[8*12-6:8*11],
				cr_residual_fifo_read[8*11-1:8*10],
				cr_residual_fifo_read[8*10-1:8* 9],
				cr_residual_fifo_read[8* 9-1:8* 8],
				cr_residual_fifo_read[8* 8-6:8* 7],
				cr_residual_fifo_read[8* 7-1:8* 6],
				cr_residual_fifo_read[8* 6-1:8* 5],
				cr_residual_fifo_read[8* 5-1:8* 4],
				cr_residual_fifo_read[8* 4-6:8* 3],
				cr_residual_fifo_read[8* 3-1:8* 2],
				cr_residual_fifo_read[8* 2-1:8* 1],
				cr_residual_fifo_read[8* 1-1:8* 0]
	};

			
  genvar r,ii;
  generate
    for (r = 0; r < CS_WIDTH; r = r + 1) begin: mem_rnk
      for (ii = 0; ii < NUM_COMP; ii = ii + 1) begin: gen_mem
        ddr3_model u_comp_ddr3
          (
           .rst_n   (ddr3_reset_n),
           .ck      (ddr3_ck_p_sdram[(ii*MEMORY_WIDTH)/72]),
           .ck_n    (ddr3_ck_n_sdram[(ii*MEMORY_WIDTH)/72]),
           .cke     (ddr3_cke_sdram[((ii*MEMORY_WIDTH)/72)+(nCS_PER_RANK*r)]),
           .cs_n    (ddr3_cs_n_sdram[((ii*MEMORY_WIDTH)/72)+(nCS_PER_RANK*r)]),
           .ras_n   (ddr3_ras_n_sdram),
           .cas_n   (ddr3_cas_n_sdram),
           .we_n    (ddr3_we_n_sdram),
           .dm_tdqs (ddr3_dm_sdram[ii]),
           .ba      (ddr3_ba_sdram[r]),
           .addr    (ddr3_addr_sdram[r]),
           .dq      (ddr3_dq_sdram[MEMORY_WIDTH*(ii+1)-1:MEMORY_WIDTH*(ii)]),
           .dqs     (ddr3_dqs_p_sdram[ii]),
           .dqs_n   (ddr3_dqs_n_sdram[ii]),
           .tdqs_n  (),
           .odt     (ddr3_odt_sdram[((ii*MEMORY_WIDTH)/72)+(nCS_PER_RANK*r)])
           );
      end
    end
  endgenerate
  
  initial begin
	wr_clk = 0;
  end
  
  always
	#10 wr_clk <= ~wr_clk;
  
  
endmodule