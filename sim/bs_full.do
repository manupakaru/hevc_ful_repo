onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/bs_fifo_data_in
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/bs_fifo_full_out
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/bs_fifo_wr_en_in
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/cnfig_dbf_fifo_data_in
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/cnfig_dbf_fifo_is_full_out
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/cnfig_dbf_fifo_wr_en_in
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/y_dbf_fifo_data_in
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/y_dbf_fifo_is_full_out
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/y_dbf_fifo_wr_en_in
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/read_stage_block/dbf_bs_fifo_block/din
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/read_stage_block/dbf_bs_fifo_block/dout
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/read_stage_block/dbf_bs_fifo_block/empty
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/read_stage_block/dbf_bs_fifo_block/full
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/read_stage_block/dbf_bs_fifo_block/rd_en
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/read_stage_block/dbf_bs_fifo_block/wr_en
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {320586887 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 290
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {361078435 ps} {376029959 ps}
