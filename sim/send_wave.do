onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/rd_en
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/empty
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/wr_en
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/xx_pb
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/yy_pb
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/next_cb_xx_in_min_tus
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/next_cb_yy_in_min_tus
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/ctb_xx
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/ctb_yy
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/bs_core_block/res_present_pu_range_4by4_valid
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/bs_core_block/intra_tu_4by4_valid_in
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/bs_core_block/x0_tu_in_min_tus_in
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/bs_core_block/y0_tu_in_min_tus_in
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/bs_core_block/xT_in_min_tus_in
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/bs_core_block/yT_in_min_tus_in
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/bs_core_block/cb_yy_min_tus_in
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/bs_core_block/cb_xx_min_tus_in
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {640516177 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 232
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {640227734 ps} {641793277 ps}
