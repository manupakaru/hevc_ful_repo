onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_wstrb
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_wready
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_wlast
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_wdata
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_bvalid
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_bresp
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_bready
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_awvalid
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_awsize
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_awready
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_awprot
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_awlock
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_awlen
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_awid
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_awcache
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_awburst
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_awaddr
add wave -noupdate -radix unsigned /pred_top_tb/y_col_pu
add wave -noupdate -radix unsigned /pred_top_tb/x_col_pu
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_wvalid
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_y_addr
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_x_addr
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_mv_buffer_w_addr
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_block_start_x_idx
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_start_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_end_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_write_en
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/state_col_write
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_mv_ctu_buffer_wdata
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_mv_ctu_buffer_rdata
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/ctu_write_done_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_buffer_block/w_en_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_buffer_block/w_data_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_buffer_block/w_addr_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_buffer_block/r_data_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_buffer_block/r_addr_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_buffer_block/col_mem
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/last_pb_of_ctu_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/pu_mv_buffer_done
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/pu_mv_buffer_done_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/line_write_done
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_write_done
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/yy_pb
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/xx_pb
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_mv_buffer_w_addr
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_derive_state
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/prev_mv_field_valid_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/prev_mv_field_ref_idx_l1
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/prev_mv_field_ref_idx_l0
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/prev_mv_field_pred_flag_l1
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/prev_mv_field_pred_flag_l0
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/prev_mv_field_mv_y_l1
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/prev_mv_field_mv_x_l1
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/prev_mv_field_mv_x_l0
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/prev_mv_field_mv_y_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_pred_flag_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_pred_flag_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_ref_idx_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_ref_idx_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_mv_x_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_mv_y_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_mv_x_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_mv_y_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_dpb_idx_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_dpb_idx_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_valid
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/prev_mv_field_valid_now
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/state_col_write
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_buffer_block/w_en_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_buffer_block/w_data_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_buffer_block/w_addr_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_buffer_block/r_data_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_buffer_block/r_addr_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_buffer_block/enable
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_buffer_block/clk
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/col_buffer_block/clear_pred_flags
add wave -noupdate -radix decimal /pred_top_tb/col_mv_hard_soft_compare/y_pu
add wave -noupdate -radix decimal /pred_top_tb/col_mv_hard_soft_compare/x_pu
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_wdata
add wave -noupdate /pred_top_tb/col_mv_hard_soft_compare/col_mv_field
add wave -noupdate /pred_top_tb/col_mv_hard_soft_compare/col_test_mv_field_ref_idx_l1
add wave -noupdate /pred_top_tb/col_mv_hard_soft_compare/col_test_mv_field_ref_idx_l0
add wave -noupdate /pred_top_tb/col_mv_hard_soft_compare/col_test_mv_field_pred_flag_l1
add wave -noupdate /pred_top_tb/col_mv_hard_soft_compare/col_test_mv_field_pred_flag_l0
add wave -noupdate -radix decimal /pred_top_tb/col_mv_hard_soft_compare/col_test_mv_field_mv_y_l1
add wave -noupdate -radix decimal /pred_top_tb/col_mv_hard_soft_compare/col_test_mv_field_mv_x_l1
add wave -noupdate -radix decimal /pred_top_tb/col_mv_hard_soft_compare/col_test_mv_field_mv_y_l0
add wave -noupdate -radix decimal /pred_top_tb/col_mv_hard_soft_compare/col_test_mv_field_mv_x_l0
add wave -noupdate /pred_top_tb/col_mv_hard_soft_compare/col_ref_mv_field_ref_idx_l1
add wave -noupdate /pred_top_tb/col_mv_hard_soft_compare/col_ref_mv_field_ref_idx_l0
add wave -noupdate /pred_top_tb/col_mv_hard_soft_compare/col_ref_mv_field_pred_flag_l1
add wave -noupdate /pred_top_tb/col_mv_hard_soft_compare/col_ref_mv_field_pred_flag_l0
add wave -noupdate -radix decimal /pred_top_tb/col_mv_hard_soft_compare/col_ref_mv_field_mv_y_l1
add wave -noupdate -radix decimal /pred_top_tb/col_mv_hard_soft_compare/col_ref_mv_field_mv_y_l0
add wave -noupdate -radix decimal /pred_top_tb/col_mv_hard_soft_compare/col_ref_mv_field_mv_x_l1
add wave -noupdate -radix decimal /pred_top_tb/col_mv_hard_soft_compare/col_ref_mv_field_mv_x_l0
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_awvalid
add wave -noupdate /pred_top_tb/uut/pred_block/mv_col_axi_wvalid
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{w address 9} {16586175825 ps} 1} {{Cursor 2} {1813160375 ps} 1} {{Cursor 3} {16586142348 ps} 0}
quietly wave cursor active 3
configure wave -namecolwidth 213
configure wave -valuecolwidth 231
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {16586097998 ps} {16586252799 ps}
