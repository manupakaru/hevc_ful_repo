onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /hevc_cabac_tb/uut/cabac_trnfm_block/entropy/mcs_0/Trace_PC
add wave -noupdate /hevc_cabac_tb/uut/cabac_trnfm_block/entropy/rst
add wave -noupdate /hevc_cabac_tb/uut/cabac_trnfm_block/entropy/clk
add wave -noupdate /hevc_cabac_tb/uut/cabac_trnfm_block/entropy/mmap/cu_offload/x0
add wave -noupdate -radix unsigned /hevc_cabac_tb/uut/cabac_trnfm_block/entropy/mmap/cu_offload/state
add wave -noupdate -radix unsigned /hevc_cabac_tb/uut/cabac_trnfm_block/entropy/mmap/cu_offload/pic_height_in_luma_samples
add wave -noupdate -radix unsigned /hevc_cabac_tb/uut/cabac_trnfm_block/entropy/mmap/cu_offload/pic_width_in_luma_samples
add wave -noupdate /hevc_cabac_tb/uut/cabac_trnfm_block/entropy/mmap/cu_offload/ctbAddrInRS
add wave -noupdate -radix unsigned /hevc_cabac_tb/uut/cabac_trnfm_block/entropy/mmap/cu_offload/mmap_data
add wave -noupdate /hevc_cabac_tb/uut/cabac_trnfm_block/entropy/mmap/cu_offload/mmap_valid
add wave -noupdate -radix unsigned /hevc_cabac_tb/uut/cabac_trnfm_block/entropy/mmap/cu_offload/mmap_type
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {399745857 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 197
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {399620577 ps} {399851623 ps}
