`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   02:39:58 12/01/2013
// Design Name:   pred_top
// Module Name:   D:/090250V/SEMESTER 7/HEVC decoder/HDL codes/pred_top/sim/pred_top_tb.v
// Project Name:  prediction
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: pred_top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module pred_top_tb;

    `include "pred_def.v"
    `include "cache_configs_def.v"
    `include "inter_axi_def.v"
    localparam                          STATE_READ_WAIT = 0;
    localparam                          STATE_ACTIVE = 1;
    localparam                          STATE_WRITE_WAIT = 2;
    localparam                          STATE_ERROR = 3;
    localparam                          STATE_CURRENT_POC = 4;
    localparam                          STATE_CUR_PIC_FIL_IDX = 5;
    localparam                          STATE_REF_PIC_LIST_5_UPDATE = 6;
    localparam                          STATE_REF_PIC_LIST_5_SCAN   = 7;
    localparam                          STATE_SEND_POC_TO_DBF = 8;
    localparam                          STATE_WRITE_WAIT_CUR_DPB_IDX_TO_DBF = 9;
    localparam                          STATE_REF_PIC_WRITE_WAIT = 10;
    localparam                          STATE_DPB_ADD_PREV_PIC = 11;
    localparam                          STATE_POC_READ_WAIT = 12;
    localparam                          STATE_MVD1_READ_WAIT = 13;
    localparam                          STATE_MVD2_READ_WAIT = 14;
    localparam                          STATE_MVD1_READ = 15;
    localparam                          STATE_MVD2_READ = 16;
    localparam                          STATE_MV_RETURN_WAIT1 = 17;
    localparam                          STATE_MV_RETURN_WAIT2 = 18;
    localparam                          STATE_MV_RETURN_WAIT3 = 19;
    localparam                          STATE_INTER_PREFETCH_COL_WRITE_WAIT = 20;
    localparam                          STATE_GET_ST_RPS_ENTRY = 21;
    localparam                          STATE_GET_ST_RPS_HEADER = 22;
    localparam                          STATE_SLICE_1_WRITE_WAIT = 23;
    localparam                          STATE_REF_PIC_LIST_TRANSFER = 24;
    localparam                          STATE_SET_ST_RPS_ENTRY_ADDR_FOR_REF_POC5 = 25;
    
    localparam                          WRITE_REF_PIC_STATE_IDLE = 0;
    localparam                          WRITE_REF_PIC_STATE_CHECK = 1;
    localparam                          WRITE_REF_PIC_STATE_POC_WRITE = 2;
    localparam                          WRITE_REF_PIC_STATE_IDX_WRITE = 3;
    localparam                          WRITE_REF_PIC_STATE_DONE = 4;
    localparam                          WRITE_REF_PIC_FIND_FOL_IDX = 5;

    localparam                          STATE_SET_INPUTS                = 0;    // the state that is entered upon reset
    localparam                          STATE_TAG_READ                  = 1;
    localparam                          STATE_TAG_COMPARE               = 2;
    localparam                          STATE_C_LINE_HIT_FETCH          = 3;
    localparam                          STATE_C_LINE_MISS_FETCH         = 4;
    localparam                          STATE_DEST_FILL                 = 5;
    localparam                          STATE_CACHE_READY               = 6;
    localparam                          STATE_MISS_AR_REDY_WAIT         = 7;
    
    localparam                          STATE_MV_DERIVE_CONFIG_UPDATE = 0;
    localparam                          STATE_PREFETCH_COL_WRITE_WAIT = 1;
    localparam                          STATE_MV_DERIVE_PU_OVERWRIT_3 = 2;
    localparam                          STATE_MV_DERIVE_AVAILABLE_CHECK4 = 3;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_9 = 4;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_10 = 5;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_11 = 6;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_12 = 7;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_13 = 8;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_MERGE_14     = 9;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_MERGE_15     = 10;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_MERGE_16     = 11;
    localparam                          STATE_MV_DERIVE_COMPARE_MVS       = 12;
    localparam                          STATE_MV_DERIVE_ZERO_MERGE            = 13;
    localparam                          STATE_MV_DERIVE_ADD_ZEROS_AMVP           = 14;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_9 = 16;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_10 = 17;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_11 = 18;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_12 = 19;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_13 = 20;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP14   = 21;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP15   = 22;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP16   = 23;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP17   = 24;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP18   = 25;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP19   = 26;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP20   = 27;
    localparam                          STATE_MV_DERIVE_COL_MV_WAIT         = 28;
    localparam                          STATE_MV_DERIVE_MVD_ADD_AMVP       = 29;
    localparam                          STATE_MV_DERIVE_SET_DUMMY_INTRA_MV = 31;
    localparam                          STATE_MV_DERIVE_SET_PRE_INTRA_MV    = 32;    
    localparam                          STATE_MV_DERIVE_DONE    = 33;
    localparam                          STATE_MV_DERIVE_CTU_DONE    = 34;
    localparam                          STATE_MV_DERIVE_SET_INTER_MV    = 35;
    
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS1       = 36;
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS2       = 37;
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS3       = 38;
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS4       = 39;
    
    localparam                          PIC_HEIGHT = 1080;
    localparam                          PIC_WIDTH  = 1920;
    
    
	// Inputs
	reg clk;
	reg reset;
	reg enable;
	reg [31:0] fifo_in;
	reg input_fifo_is_empty;
	reg output_fifo_is_full;
	reg [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0] y_residual_fifo_in;
	reg y_residual_fifo_is_empty_in;
	reg [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0] cb_residual_fifo_in;
	reg cb_residual_fifo_is_empty_in;
	reg [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0] cr_residual_fifo_in;
	reg cr_residual_fifo_is_empty_in;
	reg y_dbf_fifo_is_full;
	reg cb_dbf_fifo_is_full;
	reg cr_dbf_fifo_is_full;

	reg mv_col_axi_awready;
	reg mv_col_axi_wready;
	reg [1:0] mv_col_axi_bresp;
	reg mv_col_axi_bvalid;
	reg mv_pref_axi_arready;
	reg [511:0] mv_pref_axi_rdata;
	reg [1:0] mv_pref_axi_rresp;
	reg mv_pref_axi_rlast;
	reg mv_pref_axi_rvalid;
    
    
	// Outputs
	wire read_en_out;
	wire [31:0] fifo_out;
	wire write_en_out;
	wire y_residual_fifo_read_en_out;
	wire cb_residual_fifo_read_en_out;
	wire cr_residual_fifo_read_en_out;

    wire mv_col_axi_awid;
	wire [7:0] mv_col_axi_awlen;
	wire [2:0] mv_col_axi_awsize;
	wire [1:0] mv_col_axi_awburst;
	wire mv_col_axi_awlock;
	wire [3:0] mv_col_axi_awcache;
	wire [2:0] mv_col_axi_awprot;
	wire mv_col_axi_awvalid;
	wire [31:0] mv_col_axi_awaddr;
	wire [63:0] mv_col_axi_wstrb;
	wire mv_col_axi_wlast;
	wire mv_col_axi_wvalid;
	wire [511:0] mv_col_axi_wdata;
	wire mv_col_axi_bready;
	wire [31:0] mv_pref_axi_araddr;
	wire [7:0] mv_pref_axi_arlen;
	wire [2:0] mv_pref_axi_arsize;
	wire [1:0] mv_pref_axi_arburst;
	wire [2:0] mv_pref_axi_arprot;
	wire mv_pref_axi_arvalid;
	wire mv_pref_axi_rready;
	wire mv_pref_axi_arlock;
	wire mv_pref_axi_arid;
	wire [3:0] mv_pref_axi_arcache;

    // axi master interface         
    wire        [32-1:0]                           ref_pix_axi_ar_addr     ;
    wire        [7:0]                               ref_pix_axi_ar_len      ;
    wire        [2:0]                               ref_pix_axi_ar_size     ;
    wire        [1:0]                               ref_pix_axi_ar_burst    ;
    wire        [2:0]                               ref_pix_axi_ar_prot     ;
    wire                                            ref_pix_axi_ar_valid    ;
    reg                                             ref_pix_axi_ar_ready    ;
                
    reg         [512-1:0]                            ref_pix_axi_r_data      ;
    reg         [1:0]                               ref_pix_axi_r_resp      ;
    reg                                             ref_pix_axi_r_last      ;
    reg                                             ref_pix_axi_r_valid     ;
    wire                                            ref_pix_axi_r_ready     ;
    
    wire new_poc = uut.state == STATE_SEND_POC_TO_DBF;
	wire [0:0] temp_port;
    
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               intra_y_predsample_4by4_x_reg;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               intra_y_predsample_4by4_y_reg;
    wire        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] y_res_added_int;
    wire        [PIXEL_WIDTH - 1:0]                                     y_res_added_int_arr[OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
    wire                                                                y_valid_addition_int;

    
    integer file_in,rfile;
    integer y_file_in;
    integer cb_file_in;
    integer cr_file_in;
    
    integer file_out;    
    integer cache_in_file;
    integer cache_in_file_great;
    integer cache_verify_file;
    integer final_pixl_file;

    integer correct_res_added_file;


    reg [7:0] mem1,mem2, mem3,mem4;
    reg [7:0]   
                y_mem24,
                y_mem23,
                y_mem22,
                y_mem21,
                y_mem20,
                y_mem19,
                y_mem18,
                y_mem17,
                y_mem16,
                y_mem15,
                y_mem14,
                y_mem13,
                y_mem12,
                y_mem11,
                y_mem10,
                y_mem9,
                y_mem8,
                y_mem7,
                y_mem6,
                y_mem5,
                y_mem4,
                y_mem3,
                y_mem2,
                y_mem1;
    reg [9-1:0]   
                temp_y_mem16,
                temp_y_mem15,
                temp_y_mem14,
                temp_y_mem13,
                temp_y_mem12,
                temp_y_mem11,
                temp_y_mem10,
                temp_y_mem9,
                temp_y_mem8,
                temp_y_mem7,
                temp_y_mem6,
                temp_y_mem5,
                temp_y_mem4,
                temp_y_mem3,
                temp_y_mem2,
                temp_y_mem1;            
                

    reg [7:0]   
                cb_mem24,
                cb_mem23,
                cb_mem22,
                cb_mem21,
                cb_mem20,
                cb_mem19,
                cb_mem18,
                cb_mem17,
                cb_mem16,
                cb_mem15,
                cb_mem14,
                cb_mem13,
                cb_mem12,
                cb_mem11,
                cb_mem10,
                cb_mem9,
                cb_mem8,
                cb_mem7,
                cb_mem6,
                cb_mem5,
                cb_mem4,
                cb_mem3,
                cb_mem2,
                cb_mem1;
    reg [9-1:0]   
                temp_cb_mem16,
                temp_cb_mem15,
                temp_cb_mem14,
                temp_cb_mem13,
                temp_cb_mem12,
                temp_cb_mem11,
                temp_cb_mem10,
                temp_cb_mem9,
                temp_cb_mem8,
                temp_cb_mem7,
                temp_cb_mem6,
                temp_cb_mem5,
                temp_cb_mem4,
                temp_cb_mem3,
                temp_cb_mem2,
                temp_cb_mem1; 

    reg [7:0]   
                cr_mem24,
                cr_mem23,
                cr_mem22,
                cr_mem21,
                cr_mem20,
                cr_mem19,
                cr_mem18,
                cr_mem17,
                cr_mem16,
                cr_mem15,
                cr_mem14,
                cr_mem13,
                cr_mem12,
                cr_mem11,
                cr_mem10,
                cr_mem9,
                cr_mem8,
                cr_mem7,
                cr_mem6,
                cr_mem5,
                cr_mem4,
                cr_mem3,
                cr_mem2,
                cr_mem1;
    reg [9-1:0]   
                temp_cr_mem16,
                temp_cr_mem15,
                temp_cr_mem14,
                temp_cr_mem13,
                temp_cr_mem12,
                temp_cr_mem11,
                temp_cr_mem10,
                temp_cr_mem9,
                temp_cr_mem8,
                temp_cr_mem7,
                temp_cr_mem6,
                temp_cr_mem5,
                temp_cr_mem4,
                temp_cr_mem3,
                temp_cr_mem2,
                temp_cr_mem1; 
                
    integer file_location;
    integer pic_num;
    
    wire new_ctu = uut.state == STATE_INTER_PREFETCH_COL_WRITE_WAIT;
    wire cache_check = uut.inter_top_block.pred_sample_gen_block.cache_block.state == STATE_CACHE_READY;
    
    // wire [MV_FIELD_DATA_WIDTH -1:0] cur_mv_field_wire = {
                            // uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l0   ,
                            // uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l1   ,
                            // uut.inter_top_block.mv_derive_block.current_mv_field_ref_idx_l0     ,
                            // uut.inter_top_block.mv_derive_block.current_mv_field_ref_idx_l1     ,
                            // uut.inter_top_block.mv_derive_block.current_mv_field_mv_x_l0        ,
                            // uut.inter_top_block.mv_derive_block.current_mv_field_mv_y_l0        ,
                            // uut.inter_top_block.mv_derive_block.current_mv_field_mv_x_l1        ,
                            // uut.inter_top_block.mv_derive_block.current_mv_field_mv_y_l1 };   
    
	// Instantiate the Unit Under Test (UUT)
	pred_top uut (
		.clk(clk), 
		.reset(reset), 
		.enable(enable), 
		.fifo_in(fifo_in), 
		.input_fifo_is_empty(input_fifo_is_empty), 
		.read_en_out(read_en_out), 
		.fifo_out(fifo_out), 
		.output_fifo_is_full(output_fifo_is_full), 
		.write_en_out(write_en_out), 
		.y_residual_fifo_in(y_residual_fifo_in), 
		.y_residual_fifo_is_empty_in(y_residual_fifo_is_empty_in), 
		.y_residual_fifo_read_en_out(y_residual_fifo_read_en_out), 
		.cb_residual_fifo_in(cb_residual_fifo_in), 
		.cb_residual_fifo_is_empty_in(cb_residual_fifo_is_empty_in), 
		.cb_residual_fifo_read_en_out(cb_residual_fifo_read_en_out), 
		.cr_residual_fifo_in(cr_residual_fifo_in), 
		.cr_residual_fifo_is_empty_in(cr_residual_fifo_is_empty_in), 
		.cr_residual_fifo_read_en_out(cr_residual_fifo_read_en_out), 
		.y_dbf_fifo_is_full(y_dbf_fifo_is_full), 
		.cb_dbf_fifo_is_full(cb_dbf_fifo_is_full), 
		.cr_dbf_fifo_is_full(cr_dbf_fifo_is_full), 
        
        .ref_pix_axi_ar_addr(ref_pix_axi_ar_addr),
        .ref_pix_axi_ar_len(ref_pix_axi_ar_len), 
        .ref_pix_axi_ar_size(ref_pix_axi_ar_size), 
        .ref_pix_axi_ar_burst(ref_pix_axi_ar_burst), 
        .ref_pix_axi_ar_prot(ref_pix_axi_ar_prot), 
        .ref_pix_axi_ar_valid(ref_pix_axi_ar_valid), 
        .ref_pix_axi_ar_ready(ref_pix_axi_ar_ready), 
        .ref_pix_axi_r_data(ref_pix_axi_r_data), 
        .ref_pix_axi_r_resp(ref_pix_axi_r_resp), 
        .ref_pix_axi_r_last(ref_pix_axi_r_last), 
        .ref_pix_axi_r_valid(ref_pix_axi_r_valid), 
        .ref_pix_axi_r_ready(ref_pix_axi_r_ready),
        
        .mv_col_axi_awid(mv_col_axi_awid), 
		.mv_col_axi_awlen(mv_col_axi_awlen), 
		.mv_col_axi_awsize(mv_col_axi_awsize), 
		.mv_col_axi_awburst(mv_col_axi_awburst), 
		.mv_col_axi_awlock(mv_col_axi_awlock), 
		.mv_col_axi_awcache(mv_col_axi_awcache), 
		.mv_col_axi_awprot(mv_col_axi_awprot), 
		.mv_col_axi_awvalid(mv_col_axi_awvalid), 
		.mv_col_axi_awaddr(mv_col_axi_awaddr), 
		.mv_col_axi_awready(mv_col_axi_awready), 
		.mv_col_axi_wstrb(mv_col_axi_wstrb), 
		.mv_col_axi_wlast(mv_col_axi_wlast), 
		.mv_col_axi_wvalid(mv_col_axi_wvalid), 
		.mv_col_axi_wdata(mv_col_axi_wdata), 
		.mv_col_axi_wready(mv_col_axi_wready), 
		.mv_col_axi_bresp(mv_col_axi_bresp), 
		.mv_col_axi_bvalid(mv_col_axi_bvalid), 
		.mv_col_axi_bready(mv_col_axi_bready), 
		.mv_pref_axi_araddr(mv_pref_axi_araddr), 
		.mv_pref_axi_arlen(mv_pref_axi_arlen), 
		.mv_pref_axi_arsize(mv_pref_axi_arsize), 
		.mv_pref_axi_arburst(mv_pref_axi_arburst), 
		.mv_pref_axi_arprot(mv_pref_axi_arprot), 
		.mv_pref_axi_arvalid(mv_pref_axi_arvalid), 
		.mv_pref_axi_arready(mv_pref_axi_arready), 
		.mv_pref_axi_rdata(mv_pref_axi_rdata), 
		.mv_pref_axi_rresp(mv_pref_axi_rresp), 
		.mv_pref_axi_rlast(mv_pref_axi_rlast), 
		.mv_pref_axi_rvalid(mv_pref_axi_rvalid), 
		.mv_pref_axi_rready(mv_pref_axi_rready), 
		.mv_pref_axi_arlock(mv_pref_axi_arlock), 
		.mv_pref_axi_arid(mv_pref_axi_arid), 
		.mv_pref_axi_arcache(mv_pref_axi_arcache), 
//		.temp_port(temp_port),
        
        .intra_y_predsample_4by4_x_reg(intra_y_predsample_4by4_x_reg),
        .intra_y_predsample_4by4_y_reg(intra_y_predsample_4by4_y_reg),
        .y_res_added_int(y_res_added_int),
        .y_valid_addition_out(y_valid_addition_int)
           
	);

	initial begin
		// Initialize Inputs
        pic_num = -1;
        file_in = $fopen("residual_to_inter","rb");
        y_file_in = $fopen("residual_to_inter_Y","rb");
        cb_file_in = $fopen("residual_to_inter_Cb","rb");
        cr_file_in = $fopen("residual_to_inter_Cr","rb");
        file_out = $fopen("inter_to_dbf","wb");  
        //correct_res_added_file = $fopen("BQSquare_pre-lp.yuv","rb");
        correct_res_added_file = $fopen("avatar_pre-lp.yuv","rb");

        cache_in_file_great = $fopen("y_scn_dump_great.csv","r");
        cache_verify_file = $fopen("results.csv","r");
        final_pixl_file = $fopen("BQSquare_416x240_60_qp37_416x240_8bit_final.yuv","rb");



        mv_col_axi_awready = 1; 
        mv_col_axi_wready  = 1;
        mv_col_axi_bresp   = 0;
        mv_col_axi_bvalid  = 1;
        mv_pref_axi_arready = 1;
        mv_pref_axi_rdata = 0;
        mv_pref_axi_rresp = 0;
        mv_pref_axi_rvalid = 1;
        mv_pref_axi_rlast = 0;
        
        ref_pix_axi_r_valid = 1;
        ref_pix_axi_ar_ready = 1;
        
        file_location = -1;
		//clk = 0;
		reset = 1;
		enable = 1;
		fifo_in = 0;
		input_fifo_is_empty = 1;
		output_fifo_is_full = 0;
		y_residual_fifo_in = 0;
		y_residual_fifo_is_empty_in = 1;
		cb_residual_fifo_in = 0;
		cb_residual_fifo_is_empty_in = 0;
		cr_residual_fifo_in = 0;
		cr_residual_fifo_is_empty_in = 0;
		y_dbf_fifo_is_full = 0;
		cb_dbf_fifo_is_full = 0;
		cr_dbf_fifo_is_full = 0;

		// Wait 100 ns for global reset to finish
		#100;
        @(posedge clk);
        reset = 0;
        input_fifo_is_empty = 0;
        y_residual_fifo_is_empty_in = 0;
        read_y_res_fifo();
        read_cb_res_fifo();
        read_cr_res_fifo();
		// Add stimulus here

	end
//always #10 clk = ~clk;

initial begin
    clk = 0;
    forever begin
        #10 clk = ~clk;
    end
end


always @(posedge clk) begin
    if(reset) begin
    end
    else begin
        if(read_en_out) begin
            file_location = file_location + 1;
            mem1 = $fgetc( file_in); 
            mem2 = $fgetc( file_in); 
            mem3 = $fgetc( file_in); 
            mem4 = $fgetc( file_in); 
            $display("%x, header passed: %x",$time, mem1);
            fifo_in = {mem4,mem3,mem2,mem1};
            
            //$display("data passed: %x %x %x",mem4, mem3, mem2);
        end
    end
    
end

always@(posedge clk) begin
    if(uut.state == 3) begin
        $display("STATE_ERROR");
        $stop();
    end
    if(new_poc) begin
        pic_num = pic_num + 1;
        $display("%d",pic_num);
        //if(pic_num == 1)
        $stop();
    end
    cache_input_verify();
//    verify_luma_filter();
    
//    if(pic_num == 1 && uut.inter_top_block.mv_derive_block.xx_pb == 144 && uut.inter_top_block.mv_derive_block.yy_pb == 32)
//        $stop();
//    if(pic_num == 1 && uut.inter_top_block.mv_derive_block.current_mv_field_valid == 1) begin
//        $stop();
//    end
end

integer candidate_type_l0;
integer candidate_type_l1;

task ref_pix_axi_data_set();
    reg [31:0] axi_addr;
    reg [31:0] ref_idx;
    reg [31:0] x_val, y_val;
    integer i,j;
    integer frame_offset ;
    integer cb_offset;
    integer cr_offset;
    reg [7:0] temp_pixl;
    reg [7:0] y_pixl_arry[32-1:0];
    reg [7:0] cb_pixl_arry[8-1:0];
    reg [7:0] cr_pixl_arry[8-1:0];
    begin
        if(ref_pix_axi_ar_valid ==1) begin
            x_val = 0;
            y_val = 0;
            ref_idx = ref_pix_axi_ar_addr/`REF_PIX_FRAME_OFFSET;
            axi_addr = ref_pix_axi_ar_addr%`REF_PIX_FRAME_OFFSET;
            {y_val[9:4]} = axi_addr/`REF_PIX_IU_ROW_OFFSET;
            axi_addr = axi_addr%`REF_PIX_IU_ROW_OFFSET;
            {x_val[8:3]} = axi_addr/`REF_PIX_IU_OFFSET;
            axi_addr = axi_addr%`REF_PIX_IU_OFFSET;
            {y_val[3:0],x_val[2:0]} = axi_addr/`REF_PIX_BU_OFFSET;
            x_val = x_val << 3;
            y_val = y_val << 2;
            ref_idx = 0;
            frame_offset = ref_idx*PIC_WIDTH*PIC_HEIGHT*3/2;
            cb_offset = frame_offset + PIC_WIDTH*PIC_HEIGHT;
            cr_offset = cb_offset + PIC_WIDTH*PIC_HEIGHT/4;
            for(j=0;j<4;j=j+1)begin
                for(i=0;i<8;i=i+1)begin
                    rfile = $fseek(final_pixl_file,(frame_offset + (PIC_WIDTH)*(j + y_val) + x_val + i),0);
                    y_pixl_arry[8*j+i] = $fgetc(final_pixl_file);
                end
            end
            x_val = x_val >>1;
            y_val = y_val >>1;
            for(j=0;j<2;j=j+1)begin
                for(i=0;i<4;i=i+1)begin
                    rfile = $fseek(final_pixl_file,(cb_offset + (PIC_WIDTH>>1)*(j + y_val) + x_val + i),0);
                    cb_pixl_arry[4*j+i] = $fgetc(final_pixl_file);
                end
            end
            for(j=0;j<2;j=j+1)begin
                for(i=0;i<4;i=i+1)begin
                    rfile = $fseek(final_pixl_file,(cr_offset + (PIC_WIDTH>>1)*(j + y_val) + x_val + i),0);
                    cr_pixl_arry[4*j+i] = $fgetc(final_pixl_file);
                end
            end
            ref_pix_axi_r_data = {
                                    cr_pixl_arry[7 ],
                                    cr_pixl_arry[6 ],
                                    cr_pixl_arry[5 ],
                                    cr_pixl_arry[4 ],
                                    cr_pixl_arry[3 ],
                                    cr_pixl_arry[2 ],
                                    cr_pixl_arry[1 ],
                                    cr_pixl_arry[0 ],
                                    cb_pixl_arry[7 ],
                                    cb_pixl_arry[6 ],
                                    cb_pixl_arry[5 ],
                                    cb_pixl_arry[4 ],
                                    cb_pixl_arry[3 ],
                                    cb_pixl_arry[2 ],
                                    cb_pixl_arry[1 ],
                                    cb_pixl_arry[0 ],
                                    y_pixl_arry[31],
                                    y_pixl_arry[30],
                                    y_pixl_arry[29],
                                    y_pixl_arry[28],
                                    y_pixl_arry[27],
                                    y_pixl_arry[26],
                                    y_pixl_arry[25],
                                    y_pixl_arry[24],
                                    y_pixl_arry[23],
                                    y_pixl_arry[22],
                                    y_pixl_arry[21],
                                    y_pixl_arry[20],
                                    y_pixl_arry[19],
                                    y_pixl_arry[18],
                                    y_pixl_arry[17],
                                    y_pixl_arry[16],
                                    y_pixl_arry[15],
                                    y_pixl_arry[14],
                                    y_pixl_arry[13],
                                    y_pixl_arry[12],
                                    y_pixl_arry[11],
                                    y_pixl_arry[10],
                                    y_pixl_arry[9 ],
                                    y_pixl_arry[8 ],
                                    y_pixl_arry[7 ],
                                    y_pixl_arry[6 ],
                                    y_pixl_arry[5 ],
                                    y_pixl_arry[4 ],
                                    y_pixl_arry[3 ],
                                    y_pixl_arry[2 ],
                                    y_pixl_arry[1 ],
                                    y_pixl_arry[0 ]

                                };
        end 
    end
endtask


task cache_input_verify;
    reg       [X_ADDR_WDTH-1:0]    	            start_x_file_in;
    reg       [X_ADDR_WDTH-1:0]    	            end_x_file_in;    
    reg       [Y_ADDR_WDTH-1:0]    	            start_y_file_in;
    reg       [Y_ADDR_WDTH-1:0]    	            end_y_file_in;
    integer		        ref_idx_file_in; 
    integer      mv_block_nm;

	begin
        ref_pix_axi_data_set();
        if(uut.inter_top_block.pred_sample_gen_block.cache_block.state == STATE_CACHE_READY && uut.inter_top_block.pred_sample_gen_block.cache_block.filer_idle_in) begin
            rfile = $fscanf(cache_in_file_great,"%d,%d,%d,%d,%d,%d\n",start_x_file_in,end_x_file_in,start_y_file_in,end_y_file_in,ref_idx_file_in,mv_block_nm);
            $display("%d,%d,%d,%d,%d,%d",start_x_file_in,end_x_file_in,start_y_file_in,end_y_file_in,ref_idx_file_in,mv_block_nm);
            //if(start_x_file_in > 140)
            //$stop();
            if(start_x_file_in == uut.inter_top_block.pred_sample_gen_block.cache_block.start_great_x_in) begin
            end
            else begin
                $display("hard start x= %d soft start x = %d",uut.inter_top_block.pred_sample_gen_block.cache_block.start_great_x_in,start_x_file_in);
                $stop();
            end
            if(start_y_file_in == uut.inter_top_block.pred_sample_gen_block.cache_block.start_great_y_in) begin
            end
            else begin
                $display("hard start y= %d soft start y = %d",uut.inter_top_block.pred_sample_gen_block.cache_block.start_great_y_in,start_y_file_in);
                $stop();
            end
            
            if((end_y_file_in -start_y_file_in) == uut.inter_top_block.pred_sample_gen_block.cache_block.rf_blk_great_hgt_in) begin
            end
            else begin
                $display("hard hgt = %d soft hgt = %d",uut.inter_top_block.pred_sample_gen_block.cache_block.rf_blk_great_hgt_in,(end_y_file_in -start_y_file_in));
                $stop();
            end
            if((end_x_file_in - start_x_file_in) == uut.inter_top_block.pred_sample_gen_block.cache_block.rf_blk_great_wdt_in) begin
            end
            else begin
                $display("hard wdt = %d soft wdt = %d",uut.inter_top_block.pred_sample_gen_block.cache_block.rf_blk_great_wdt_in,(end_x_file_in -start_x_file_in));
                $stop();
            end
            cache_verify_internal(0);
        end
	end
endtask

task cache_verify_internal;
    input [31:0]            ref_idx_file_in; 
    integer                 start_x_file_in;    
    integer                 start_y_file_in;
    integer                 w_x_file_in;    
    integer                 h_y_file_in;

    reg       [(3)-1:0]             ages_set[(1<<3)-1:0];
    reg       [19-1:0]              ref_tags [(1<<7)-1:0]; 
    reg      [(3)*(1<<3)-1:0]       temp;
    reg [7:0] y_pixl_arry[11-1:0][11-1:0];
    reg [7:0] cb_pixl_arry[5-1:0][5-1:0];
    reg [7:0] cr_pixl_arry[5-1:0][5-1:0];
    integer i,j,k;
    integer frame_offset ;
    integer cb_offset;
    integer cr_offset;
    reg [LUMA_BITS-1:0] gold_pixel;
    integer x_block_offset, y_block_offset;
    // reg [LUMA_BITS-1:0] dirty_pixel;
    
    begin
        for(i=0;i<16;i=i+1) begin
            for(j=7;j>=0;j=j-1) begin
                    rfile = $fscanf(cache_verify_file,"%d,%d\n",ages_set[j],ref_tags[j]);   
                if(uut.inter_top_block.pred_sample_gen_block.cache_block.tag_block.valid_bits[(1<<C_N_WAY)*i+j] == 1) begin
                    if(uut.inter_top_block.pred_sample_gen_block.cache_block.tag_block.mem[(1<<C_N_WAY)*i+j] !== ref_tags[j]) begin
                        $display("tag mismatch @set %d @set_idx %d, soft=%d, hard=%d",i,j,ref_tags[j],uut.inter_top_block.pred_sample_gen_block.cache_block.tag_block.mem[(1<<C_N_WAY)*i+j]);
                        $stop;                        
                    end
                end
            end
            rfile = $fscanf(cache_verify_file,"\n");
                temp = {ages_set[7],
                                        ages_set[6],
                                        ages_set[5],
                                        ages_set[4],
                                        ages_set[3],
                                        ages_set[2],
                                        ages_set[1],
                                        ages_set[0]};
                                        
            if(uut.inter_top_block.pred_sample_gen_block.cache_block.age_block.mem[i] !== temp) begin
                $display("age mismatch @ %d, soft = %d hard = %d",i,temp,uut.inter_top_block.pred_sample_gen_block.cache_block.age_block.mem[i]);
                $stop;
            end
        end
            frame_offset = ref_idx_file_in*PIC_WIDTH*PIC_HEIGHT*3/2;
            cb_offset = frame_offset + PIC_WIDTH*PIC_HEIGHT;
            cr_offset = cb_offset + PIC_WIDTH*PIC_HEIGHT/4;

            start_x_file_in = uut.inter_top_block.pred_sample_gen_block.cache_block.start_x_in ;
            start_y_file_in = uut.inter_top_block.pred_sample_gen_block.cache_block.start_y_in ;
            w_x_file_in     = uut.inter_top_block.pred_sample_gen_block.cache_block.rf_blk_wdt_in;
            h_y_file_in     = uut.inter_top_block.pred_sample_gen_block.cache_block.rf_blk_hgt_in;
            x_block_offset = uut.inter_top_block.pred_sample_gen_block.cache_block.block_x_offset_luma;
            y_block_offset = uut.inter_top_block.pred_sample_gen_block.cache_block.block_y_offset_luma;
            for(j= 0;j<=h_y_file_in;j=j+1)begin
                for(i= 0;i<=w_x_file_in;i=i+1)begin
                    rfile = $fseek(final_pixl_file,(frame_offset + (PIC_WIDTH)*(j + start_y_file_in) + start_x_file_in + i),0);
                    //$display("fseek @ %d",(frame_offset + (PIC_WIDTH)*(j + start_y_file_in) + start_x_file_in + i));
                    y_pixl_arry[j][i] = $fgetc(final_pixl_file);
                    if (y_pixl_arry[j][i] == uut.inter_top_block.pred_sample_gen_block.cache_block.block_11x11[j+y_block_offset][i+x_block_offset] ) begin
                        //$display("cache ref pixel match luma @x=%d,y=%d, soft=%x, hard=%x",i,j,y_pixl_arry[j][i],uut.inter_top_block.pred_sample_gen_block.cache_block.block_11x11[j+y_block_offset][i+x_block_offset]);
                    end
                    else begin
                        $display("cache ref pixel mismatch luma @x=%d,y=%d, soft=%x, hard=%x",i,j,y_pixl_arry[j][i],uut.inter_top_block.pred_sample_gen_block.cache_block.block_11x11[j+y_block_offset][i+x_block_offset]);
                        $stop();
                    end
                end
            end
            start_x_file_in = uut.inter_top_block.pred_sample_gen_block.cache_block.start_x_ch ;
            start_y_file_in = uut.inter_top_block.pred_sample_gen_block.cache_block.start_y_ch ;
            w_x_file_in     = uut.inter_top_block.pred_sample_gen_block.cache_block.rf_blk_wdt_ch;
            h_y_file_in     = uut.inter_top_block.pred_sample_gen_block.cache_block.rf_blk_hgt_ch;
            x_block_offset = uut.inter_top_block.pred_sample_gen_block.cache_block.block_x_offset_chma;
            y_block_offset = uut.inter_top_block.pred_sample_gen_block.cache_block.block_y_offset_chma;    
            //if(start_y_file_in==4 && start_x_file_in==0)
            //$stop();        
            for(j=0;j<=h_y_file_in;j=j+1)begin
                for(i=0;i<=w_x_file_in;i=i+1)begin
                    rfile = $fseek(final_pixl_file,(cb_offset + (PIC_WIDTH>>1)*(j + start_y_file_in) + start_x_file_in + i),0);
                    cb_pixl_arry[j][i] = $fgetc(final_pixl_file);
                    if (cb_pixl_arry[j][i] == uut.inter_top_block.pred_sample_gen_block.cache_block.block_5x5_cb[j+y_block_offset][i+x_block_offset] ) begin
                        //$display("cache ref pixel match cb @x=%d,y=%d, soft=%x, hard=%x",i,j,cb_pixl_arry[j][i],uut.inter_top_block.pred_sample_gen_block.cache_block.block_5x5_cb[j+y_block_offset][i+x_block_offset]); 
                    end
                    else begin
                        $display("cache ref pixel mismatch cb @x=%d,y=%d, soft=%x, hard=%x",i,j,cb_pixl_arry[j][i],uut.inter_top_block.pred_sample_gen_block.cache_block.block_5x5_cb[j+y_block_offset][i+x_block_offset]);
                        $stop();
                    end
                end
            end
            for(j=0;j<=h_y_file_in;j=j+1)begin
                for(i=0;i<=w_x_file_in;i=i+1)begin
                    rfile = $fseek(final_pixl_file,(cr_offset + (PIC_WIDTH>>1)*(j + start_y_file_in) + start_x_file_in + i),0);
                    cr_pixl_arry[j][i] = $fgetc(final_pixl_file);
                    if (cr_pixl_arry[j][i] == uut.inter_top_block.pred_sample_gen_block.cache_block.block_5x5_cr[j+y_block_offset][i+x_block_offset]  ) begin
                        //$display("cache ref pixel match cr @x=%d,y=%d, soft=%x, hard=%x",i,j,cr_pixl_arry[j][i],uut.inter_top_block.pred_sample_gen_block.cache_block.block_5x5_cr[j+y_block_offset][i+x_block_offset] );
                    end
                    else begin
                        $display("cache ref pixel mismatch cr @x=%d,y=%d, soft=%x, hard=%x",i,j,cr_pixl_arry[j][i],uut.inter_top_block.pred_sample_gen_block.cache_block.block_5x5_cr[j+y_block_offset][i+x_block_offset] );
                        $stop();
                    end
                end
            end
    end
endtask


always@(posedge clk) begin
    if(y_residual_fifo_read_en_out) begin
        read_y_res_fifo();
    end
    if(cb_residual_fifo_read_en_out) begin
        read_cb_res_fifo();
    end
    if(cr_residual_fifo_read_en_out) begin
        read_cr_res_fifo();
    end
end

task read_y_res_fifo;
    begin
        y_mem1 = $fgetc( y_file_in); 
        y_mem2 = $fgetc( y_file_in); 
        y_mem3 = $fgetc( y_file_in); 
        y_mem4 = $fgetc( y_file_in);

        {   temp_y_mem3,
            temp_y_mem2,
            temp_y_mem1 } = {y_mem4[2:0],y_mem3,y_mem2,y_mem1};
        
        y_mem5 = $fgetc( y_file_in); 
        y_mem6 = $fgetc( y_file_in); 
        y_mem7 =  $fgetc( y_file_in); 
        y_mem8 =  $fgetc( y_file_in);

        {   temp_y_mem6,
            temp_y_mem5,
            temp_y_mem4 } = {y_mem8[2:0],y_mem7,y_mem6,y_mem5};
        
        y_mem9 =  $fgetc( y_file_in); 
        y_mem10 =  $fgetc( y_file_in); 
        y_mem11 =  $fgetc( y_file_in); 
        y_mem12 =  $fgetc( y_file_in);

        {   temp_y_mem9,
            temp_y_mem8,
            temp_y_mem7 } = {y_mem12[2:0],y_mem11,y_mem10,y_mem9};
        
        y_mem13 =  $fgetc( y_file_in); 
        y_mem14 =  $fgetc( y_file_in); 
        y_mem15 =  $fgetc( y_file_in); 
        y_mem16 = $fgetc( y_file_in);

        {   temp_y_mem12,
            temp_y_mem11,
            temp_y_mem10 } = {y_mem16[2:0],y_mem15,y_mem14,y_mem13};
        
        y_mem17 = $fgetc( y_file_in); 
        y_mem18 = $fgetc( y_file_in); 
        y_mem19 = $fgetc( y_file_in); 
        y_mem20 = $fgetc( y_file_in); 

        {   temp_y_mem15,
            temp_y_mem14,
            temp_y_mem13 } = {y_mem20[2:0],y_mem19,y_mem18,y_mem17};

        y_mem21 = $fgetc( y_file_in); 
        y_mem22 = $fgetc( y_file_in); 
        y_mem23 = $fgetc( y_file_in); 
        y_mem24 = $fgetc( y_file_in); 

        temp_y_mem16 ={y_mem22[0],y_mem21};


        y_residual_fifo_in = {
                                    temp_y_mem16,
                                    temp_y_mem15,
                                    temp_y_mem14,
                                    temp_y_mem13,
                                    temp_y_mem12,
                                    temp_y_mem11,
                                    temp_y_mem10,
                                    temp_y_mem9,
                                    temp_y_mem8,
                                    temp_y_mem7,
                                    temp_y_mem6,
                                    temp_y_mem5,
                                    temp_y_mem4,
                                    temp_y_mem3,
                                    temp_y_mem2,
                                    temp_y_mem1
                            };
        
       // $stop();
        //$display("data passed: %x %x %x",mem4, mem3, mem2);

    end
endtask

task read_cb_res_fifo;
    begin
        cb_mem1 = $fgetc( cb_file_in); 
        cb_mem2 = $fgetc( cb_file_in); 
        cb_mem3 = $fgetc( cb_file_in); 
        cb_mem4 = $fgetc( cb_file_in);

        {   temp_cb_mem3,
            temp_cb_mem2,
            temp_cb_mem1 } = {cb_mem4[2:0],cb_mem3,cb_mem2,cb_mem1};
        
        cb_mem5 = $fgetc( cb_file_in); 
        cb_mem6 = $fgetc( cb_file_in); 
        cb_mem7 =  $fgetc( cb_file_in); 
        cb_mem8 =  $fgetc( cb_file_in);

        {   temp_cb_mem6,
            temp_cb_mem5,
            temp_cb_mem4 } = {cb_mem8[2:0],cb_mem7,cb_mem6,cb_mem5};
        
        cb_mem9 =  $fgetc( cb_file_in); 
        cb_mem10 =  $fgetc( cb_file_in); 
        cb_mem11 =  $fgetc( cb_file_in); 
        cb_mem12 =  $fgetc( cb_file_in);

        {   temp_cb_mem9,
            temp_cb_mem8,
            temp_cb_mem7 } = {cb_mem12[2:0],cb_mem11,cb_mem10,cb_mem9};
        
        cb_mem13 =  $fgetc( cb_file_in); 
        cb_mem14 =  $fgetc( cb_file_in); 
        cb_mem15 =  $fgetc( cb_file_in); 
        cb_mem16 = $fgetc( cb_file_in);

        {   temp_cb_mem12,
            temp_cb_mem11,
            temp_cb_mem10 } = {cb_mem16[2:0],cb_mem15,cb_mem14,cb_mem13};
        
        cb_mem17 = $fgetc( cb_file_in); 
        cb_mem18 = $fgetc( cb_file_in); 
        cb_mem19 = $fgetc( cb_file_in); 
        cb_mem20 = $fgetc( cb_file_in); 

        {   temp_cb_mem15,
            temp_cb_mem14,
            temp_cb_mem13 } = {cb_mem20[2:0],cb_mem19,cb_mem18,cb_mem17};

        cb_mem21 = $fgetc( cb_file_in); 
        cb_mem22 = $fgetc( cb_file_in); 
        cb_mem23 = $fgetc( cb_file_in); 
        cb_mem24 = $fgetc( cb_file_in); 

        temp_cb_mem16 ={cb_mem22[0],cb_mem21};


        cb_residual_fifo_in = {
                                    temp_cb_mem16,
                                    temp_cb_mem15,
                                    temp_cb_mem14,
                                    temp_cb_mem13,
                                    temp_cb_mem12,
                                    temp_cb_mem11,
                                    temp_cb_mem10,
                                    temp_cb_mem9,
                                    temp_cb_mem8,
                                    temp_cb_mem7,
                                    temp_cb_mem6,
                                    temp_cb_mem5,
                                    temp_cb_mem4,
                                    temp_cb_mem3,
                                    temp_cb_mem2,
                                    temp_cb_mem1
                            };
        
       // $stop();
        //$display("data passed: %x %x %x",mem4, mem3, mem2);

    end
endtask

task read_cr_res_fifo;
    begin
        cr_mem1 = $fgetc( cr_file_in); 
        cr_mem2 = $fgetc( cr_file_in); 
        cr_mem3 = $fgetc( cr_file_in); 
        cr_mem4 = $fgetc( cr_file_in);

        {   temp_cr_mem3,
            temp_cr_mem2,
            temp_cr_mem1 } = {cr_mem4[2:0],cr_mem3,cr_mem2,cr_mem1};
        
        cr_mem5 = $fgetc( cr_file_in); 
        cr_mem6 = $fgetc( cr_file_in); 
        cr_mem7 =  $fgetc( cr_file_in); 
        cr_mem8 =  $fgetc( cr_file_in);

        {   temp_cr_mem6,
            temp_cr_mem5,
            temp_cr_mem4 } = {cr_mem8[2:0],cr_mem7,cr_mem6,cr_mem5};
        
        cr_mem9 =  $fgetc( cr_file_in); 
        cr_mem10 =  $fgetc( cr_file_in); 
        cr_mem11 =  $fgetc( cr_file_in); 
        cr_mem12 =  $fgetc( cr_file_in);

        {   temp_cr_mem9,
            temp_cr_mem8,
            temp_cr_mem7 } = {cr_mem12[2:0],cr_mem11,cr_mem10,cr_mem9};
        
        cr_mem13 =  $fgetc( cr_file_in); 
        cr_mem14 =  $fgetc( cr_file_in); 
        cr_mem15 =  $fgetc( cr_file_in); 
        cr_mem16 = $fgetc( cr_file_in);

        {   temp_cr_mem12,
            temp_cr_mem11,
            temp_cr_mem10 } = {cr_mem16[2:0],cr_mem15,cr_mem14,cr_mem13};
        
        cr_mem17 = $fgetc( cr_file_in); 
        cr_mem18 = $fgetc( cr_file_in); 
        cr_mem19 = $fgetc( cr_file_in); 
        cr_mem20 = $fgetc( cr_file_in); 

        {   temp_cr_mem15,
            temp_cr_mem14,
            temp_cr_mem13 } = {cr_mem20[2:0],cr_mem19,cr_mem18,cr_mem17};

        cr_mem21 = $fgetc( cr_file_in); 
        cr_mem22 = $fgetc( cr_file_in); 
        cr_mem23 = $fgetc( cr_file_in); 
        cr_mem24 = $fgetc( cr_file_in); 

        temp_cr_mem16 ={cr_mem22[0],cr_mem21};


        cr_residual_fifo_in = {
                                    temp_cr_mem16,
                                    temp_cr_mem15,
                                    temp_cr_mem14,
                                    temp_cr_mem13,
                                    temp_cr_mem12,
                                    temp_cr_mem11,
                                    temp_cr_mem10,
                                    temp_cr_mem9,
                                    temp_cr_mem8,
                                    temp_cr_mem7,
                                    temp_cr_mem6,
                                    temp_cr_mem5,
                                    temp_cr_mem4,
                                    temp_cr_mem3,
                                    temp_cr_mem2,
                                    temp_cr_mem1
                            };
        
       // $stop();
        //$display("data passed: %x %x %x",mem4, mem3, mem2);

    end
endtask


//always@(posedge clk) begin
//    if(uut.state == 3) begin
//        $display("STATE_ERROR");
//        $stop();
//    end
//    if(new_poc) begin
//        pic_num = pic_num + 1;
//        $display("%d",pic_num);
//        //if(pic_num == 1)
//        $stop();
//    end
    
    
    
//    if(pic_num == 1 && uut.inter_top_block.mv_derive_block.xx_pb == 144 && uut.inter_top_block.mv_derive_block.yy_pb == 32)
//        $stop();
//    if(pic_num == 1 && uut.inter_top_block.mv_derive_block.current_mv_field_valid == 1) begin
//        $stop();
//    end
//end

always @(posedge clk) begin
  intra_verify();
  //post_intra_verify();
end

generate
    genvar i;
    genvar j;
    
    for (i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin : row_iteration
        for (j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin : column_iteration
            assign y_res_added_int_arr[i][j] = y_res_added_int[i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE +(j+1)*PIXEL_WIDTH - 1:j*PIXEL_WIDTH + i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE];
        end
    end
endgenerate

task post_intra_verify;
    integer i;
    integer j;
    reg [7:0] temp;
    begin

        if(y_valid_addition_int == 1'b1) begin
            for (i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin
                for (j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin
                    $fseek(correct_res_added_file,((416)*(j + intra_y_predsample_4by4_y_reg) + intra_y_predsample_4by4_x_reg + i),0);
                    temp = $fgetc(correct_res_added_file);

                    if (temp != y_res_added_int_arr[i][j]) begin
                        $display("correct_res_added_file : %d , uut.y_res_added_int_arr : %d \n at (y = %d, x = %d)",temp,y_res_added_int_arr[i][j],(j + intra_y_predsample_4by4_y_reg),(intra_y_predsample_4by4_x_reg + i));
                        $stop;
                    end
                end
            end
        end
    end
endtask


task intra_verify;
    integer i;
    integer j;
    reg [7:0] temp;
    begin

        if(uut.y_valid_addition_reg == 1'b1) begin
            for (i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin
                for (j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin
                    $fseek(correct_res_added_file,((uut.pic_width)*(j + uut.intra_y_predsample_4by4_y_reg) + uut.intra_y_predsample_4by4_x_reg + i),0);
                    temp = $fgetc(correct_res_added_file);

                    if (temp != uut.y_res_added_int_arr[i][j]) begin
                        $display("correct_res_added_file : %d , uut.y_res_added_int_arr : %d \n at (y = %d, x = %d)",temp,uut.y_res_added_int_arr[i][j],(j + uut.intra_y_predsample_4by4_y_reg),(uut.intra_y_predsample_4by4_x_reg + i));
                        $stop;
                    end
                end
            end
        end
        
        if(uut.cb_valid_addition_reg == 1'b1) begin
            for (i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin
                for (j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin
                    $fseek(correct_res_added_file,((uut.pic_width*uut.pic_height)+(uut.pic_width/2)*(j + uut.intra_cb_predsample_4by4_y_reg) + uut.intra_cb_predsample_4by4_x_reg + i),0);
                    temp = $fgetc(correct_res_added_file);

                    if (temp != uut.cb_res_added_int_arr[i][j]) begin
                        $display("correct_res_added_file : %d , uut.cb_res_added_int_arr : %d \n at (y = %d, x = %d)",temp,uut.cb_res_added_int_arr[i][j],(j + uut.intra_cb_predsample_4by4_y_reg),(uut.intra_cb_predsample_4by4_x_reg + i));
                        $stop;
                    end
                end
            end
        end
        
        if(uut.cr_valid_addition_reg == 1'b1) begin
            for (i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin
                for (j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin
                    $fseek(correct_res_added_file,(((uut.pic_width/2)*(uut.pic_height/2))+(uut.pic_width*uut.pic_height)+(uut.pic_width/2)*(j + uut.intra_cr_predsample_4by4_y_reg) + uut.intra_cr_predsample_4by4_x_reg + i),0);
                    temp = $fgetc(correct_res_added_file);

                    if (temp != uut.cr_res_added_int_arr[i][j]) begin
                        $display("correct_res_added_file : %d , uut.cr_res_added_int_arr : %d \n at (y = %d, x = %d)",temp,uut.cr_res_added_int_arr[i][j],(j + uut.intra_cr_predsample_4by4_y_reg),(uut.intra_cr_predsample_4by4_x_reg + i));
                        $stop;
                    end
                end
            end
        end
          
    end
endtask


//integer candidate_type_l0;
//integer candidate_type_l1;

// always@(*) begin
    // if(uut.inter_top_block.mv_derive_block.current_mv_field_valid == 1) begin
        // if(uut.inter_top_block.mv_derive_block.cb_pred_mode == `MODE_INTRA) begin
                // candidate_type_l0 = "aINT";
                // candidate_type_l1 = "aINT";
        // end
        // else begin
            // if(uut.inter_top_block.mv_derive_block.merge_flag) begin
                // case(uut.inter_top_block.mv_derive_block.merge_cand_list[uut.inter_top_block.mv_derive_block.merge_idx])
                    // `MV_MERGE_CAND_A0: begin
                        // candidate_type_l0 = "aMA0";         
                        // candidate_type_l1 = "aMA0";         
                    // end
                    // `MV_MERGE_CAND_A1: begin
                        // candidate_type_l0 = "aMA1"; 
                        // candidate_type_l1 = "aMA1"; 
                    // end
                    // `MV_MERGE_CAND_B0: begin
                        // candidate_type_l0 = "aMB0"; 
                        // candidate_type_l1 = "aMB0"; 
                    // end
                    // `MV_MERGE_CAND_B1: begin
                        // candidate_type_l0 = "aMB1"; 
                        // candidate_type_l1 = "aMB1"; 
                    // end
                    // `MV_MERGE_CAND_B2: begin
                        // candidate_type_l0 = "aMB2"; 
                        // candidate_type_l1 = "aMB2"; 
                    // end
                    // `MV_MERGE_CAND_COL: begin
                        // candidate_type_l0 = "aMCL"; 
                        // candidate_type_l1 = "aMCL"; 
                    // end
                    // `MV_MERGE_CAND_BI: begin
                        // candidate_type_l0 = "aMBI"; 
                        // candidate_type_l1 = "aMBI"; 
                    // end
                    // `MV_MERGE_CAND_ZERO: begin
                        // candidate_type_l0 = "aMZR";                         
                        // candidate_type_l1 = "aMZR";                         
                    // end
                // endcase
            // end
            // else begin  // AMVP mode
                // if(uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l0) begin
                    // case(uut.inter_top_block.mv_derive_block.mvpl0_cand_list[uut.inter_top_block.mv_derive_block.mvp_l0_flag])
                        // `MV_AMVP_CAND_A: begin
                            // candidate_type_l0 = "aPA";                         
                        // end
                        // `MV_AMVP_CAND_B: begin
                            // candidate_type_l0 = "aPB";                                                   
                        // end
                        // `MV_AMVP_CAND_COL: begin
                            // candidate_type_l0 = "aPC";                         
                        // end
                        // `MV_AMVP_CAND_ZERO: begin
                            // candidate_type_l0 = "aPZR";                                       
                        // end
                    // endcase
                // end
                // else begin
                    // candidate_type_l0 = "aPNA";          
                // end

                // if(uut.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l1) begin
                    // case(uut.inter_top_block.mv_derive_block.mvpl1_cand_list[uut.inter_top_block.mv_derive_block.mvp_l1_flag])
                        // `MV_AMVP_CAND_A: begin
                            // candidate_type_l1 = "aPA"; 
                        // end                                              
                        // `MV_AMVP_CAND_B: begin                           
                            // candidate_type_l1 = "aPB"; 
                        // end
                        // `MV_AMVP_CAND_COL: begin
                            // candidate_type_l1 = "aPC"; 
                        // end                        
                        // `MV_AMVP_CAND_ZERO: begin
                            // candidate_type_l1 = "aPZR";                   
                        // end
                    // endcase
                // end
                // else begin
                    // candidate_type_l1 = "aPNA";                 
                // end   
            // end
        // end
    // end
// end


endmodule

