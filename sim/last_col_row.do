onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /pred_top_tb/uut/pred_block/y_valid_addition_out
add wave -noupdate /pred_top_tb/uut/pred_block/cb_valid_addition_reg
add wave -noupdate /pred_top_tb/uut/pred_block/cr_valid_addition_reg
add wave -noupdate /pred_top_tb/uut/pred_block/intra_y_predsample_4by4_last_row_int
add wave -noupdate /pred_top_tb/uut/pred_block/intra_y_predsample_4by4_last_col_int
add wave -noupdate /pred_top_tb/uut/pred_block/intra_cb_predsample_4by4_last_row_int
add wave -noupdate /pred_top_tb/uut/pred_block/intra_cb_predsample_4by4_last_col_int
add wave -noupdate /pred_top_tb/uut/pred_block/intra_cr_predsample_4by4_last_row_int
add wave -noupdate /pred_top_tb/uut/pred_block/intra_cr_predsample_4by4_last_col_int
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/x0_tu_end_in_min_tus_yy_wgt_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/x0_tu_end_in_min_tus_cr_wgt_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/x0_tu_end_in_min_tus_cb_wgt_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/xT_in_min_cr_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/xT_in_min_cb_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_y_predsample_4by4_last_row
add wave -noupdate /pred_top_tb/uut/pred_block/inter_y_predsample_4by4_last_col
add wave -noupdate /pred_top_tb/uut/pred_block/inter_cb_predsample_4by4_last_row
add wave -noupdate /pred_top_tb/uut/pred_block/inter_cb_predsample_4by4_last_col
add wave -noupdate /pred_top_tb/uut/pred_block/inter_cr_predsample_4by4_last_row
add wave -noupdate /pred_top_tb/uut/pred_block/inter_cr_predsample_4by4_last_col
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2165977 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 354
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {1852655 ps} {2607345 ps}
