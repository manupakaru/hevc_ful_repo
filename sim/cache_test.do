onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/miss_elem_fifo_empty
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/hit_elem_fifo_empty
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/output_fifo_empty
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/miss_elem_fifo_full
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/hit_elem_fifo_full
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/output_fifo_full
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/miss_elem_fifo_wr_en
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/hit_elem_fifo_wr_en
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/hit_elem_fifo_rd_en
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/miss_elem_fifo_rd_en
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/test_cache_en
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/state_data_read
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_x_3d
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_y_3d
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_x_hit
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_y_hit
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_x_read
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_y_read
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_valid
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/test_ref_block_en
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/state_tag_compare
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/dest_enable_wire_valid
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/state_tag_read
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/set_input_stage_valid
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_x
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_y
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/state_set_input
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/valid_in
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {14847782713 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 214
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {14847276071 ps} {14848204929 ps}
