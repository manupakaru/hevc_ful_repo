onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/inter_pred_sample_state
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/end_of_tu
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/end_of_cu
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/xT_in_min_tus
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/yT_in_min_tus
add wave -noupdate /pred_top_tb/uut/pred_block/cb_valid_addition_reg
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/cb_common_predsample_4by4_x_int
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/intra_cb_predsample_4by4_x_reg
add wave -noupdate /pred_top_tb/uut/pred_block/intra_cb_predsample_4by4_y_reg
add wave -noupdate /pred_top_tb/uut/pred_block/cb_common_predsample_4by4_y_int
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/intra_cb_predsample_4by4_x_int
add wave -noupdate /pred_top_tb/uut/pred_block/clk
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/y_common_predsample_4by4_y_int
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/y_common_predsample_4by4_x_int
add wave -noupdate /pred_top_tb/uut/pred_block/y_valid_addition_reg
add wave -noupdate /pred_top_tb/uut/pred_block/y_residual_adder_block/res_present_in
add wave -noupdate /pred_top_tb/uut/pred_block/y_residual_adder_block/ans_4by4_out
add wave -noupdate -radix unsigned -childformat {{{/pred_top_tb/uut/pred_block/y_residual_adder_block/temp_pre_ans_wire[3]} -radix unsigned} {{/pred_top_tb/uut/pred_block/y_residual_adder_block/temp_pre_ans_wire[2]} -radix unsigned} {{/pred_top_tb/uut/pred_block/y_residual_adder_block/temp_pre_ans_wire[1]} -radix unsigned} {{/pred_top_tb/uut/pred_block/y_residual_adder_block/temp_pre_ans_wire[0]} -radix unsigned}} -expand -subitemconfig {{/pred_top_tb/uut/pred_block/y_residual_adder_block/temp_pre_ans_wire[3]} {-height 15 -radix unsigned} {/pred_top_tb/uut/pred_block/y_residual_adder_block/temp_pre_ans_wire[2]} {-height 15 -radix unsigned} {/pred_top_tb/uut/pred_block/y_residual_adder_block/temp_pre_ans_wire[1]} {-height 15 -radix unsigned} {/pred_top_tb/uut/pred_block/y_residual_adder_block/temp_pre_ans_wire[0]} {-height 15 -radix unsigned}} /pred_top_tb/uut/pred_block/y_residual_adder_block/temp_pre_ans_wire
add wave -noupdate -radix unsigned -childformat {{{/pred_top_tb/uut/pred_block/y_residual_adder_block/residual_4by4_int_arr[3]} -radix decimal} {{/pred_top_tb/uut/pred_block/y_residual_adder_block/residual_4by4_int_arr[2]} -radix decimal} {{/pred_top_tb/uut/pred_block/y_residual_adder_block/residual_4by4_int_arr[1]} -radix decimal} {{/pred_top_tb/uut/pred_block/y_residual_adder_block/residual_4by4_int_arr[0]} -radix decimal}} -expand -subitemconfig {{/pred_top_tb/uut/pred_block/y_residual_adder_block/residual_4by4_int_arr[3]} {-height 15 -radix decimal} {/pred_top_tb/uut/pred_block/y_residual_adder_block/residual_4by4_int_arr[2]} {-height 15 -radix decimal} {/pred_top_tb/uut/pred_block/y_residual_adder_block/residual_4by4_int_arr[1]} {-height 15 -radix decimal} {/pred_top_tb/uut/pred_block/y_residual_adder_block/residual_4by4_int_arr[0]} {-height 15 -radix decimal}} /pred_top_tb/uut/pred_block/y_residual_adder_block/residual_4by4_int_arr
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/y_residual_adder_block/predsamples_4by4_valid_in
add wave -noupdate -radix unsigned -childformat {{{/pred_top_tb/uut/pred_block/y_residual_adder_block/predsamples_4by4_int_arr[3]} -radix unsigned} {{/pred_top_tb/uut/pred_block/y_residual_adder_block/predsamples_4by4_int_arr[2]} -radix unsigned} {{/pred_top_tb/uut/pred_block/y_residual_adder_block/predsamples_4by4_int_arr[1]} -radix unsigned} {{/pred_top_tb/uut/pred_block/y_residual_adder_block/predsamples_4by4_int_arr[0]} -radix unsigned}} -expand -subitemconfig {{/pred_top_tb/uut/pred_block/y_residual_adder_block/predsamples_4by4_int_arr[3]} {-height 15 -radix unsigned} {/pred_top_tb/uut/pred_block/y_residual_adder_block/predsamples_4by4_int_arr[2]} {-height 15 -radix unsigned} {/pred_top_tb/uut/pred_block/y_residual_adder_block/predsamples_4by4_int_arr[1]} {-height 15 -radix unsigned} {/pred_top_tb/uut/pred_block/y_residual_adder_block/predsamples_4by4_int_arr[0]} {-height 15 -radix unsigned}} /pred_top_tb/uut/pred_block/y_residual_adder_block/predsamples_4by4_int_arr
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/tu_empty
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/tu_data_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/res_present_wire
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/y0_tu_in_ctu_wire
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/x0_tu_in_ctu_wire
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/pred_pixl_gen_idle_out_to_pred_top
add wave -noupdate -radix unsigned /pred_top_tb/y_common_predsample_4by4_y_int
add wave -noupdate -radix unsigned /pred_top_tb/y_common_predsample_4by4_x_int
add wave -noupdate -childformat {{{/pred_top_tb/y_res_added_int_arr[3]} -radix unsigned} {{/pred_top_tb/y_res_added_int_arr[2]} -radix unsigned} {{/pred_top_tb/y_res_added_int_arr[1]} -radix unsigned} {{/pred_top_tb/y_res_added_int_arr[0]} -radix unsigned}} -expand -subitemconfig {{/pred_top_tb/y_res_added_int_arr[3]} {-height 15 -radix unsigned} {/pred_top_tb/y_res_added_int_arr[2]} {-height 15 -radix unsigned} {/pred_top_tb/y_res_added_int_arr[1]} {-height 15 -radix unsigned} {/pred_top_tb/y_res_added_int_arr[0]} {-height 15 -radix unsigned}} /pred_top_tb/y_res_added_int_arr
add wave -noupdate -radix unsigned /pred_top_tb/uut/dbf_block/dbf_fifo_control_state
add wave -noupdate -radix unsigned /pred_top_tb/uut/dbf_block/ctb_xx_at_dbf
add wave -noupdate -radix unsigned /pred_top_tb/uut/dbf_block/ctb_yy_at_dbf
add wave -noupdate -radix unsigned /pred_top_tb/uut/dbf_block/ctb_xx
add wave -noupdate -radix unsigned /pred_top_tb/uut/dbf_block/ctb_yy
add wave -noupdate -radix unsigned /pred_top_tb/uut/dbf_block/yy_8_by_8_y
add wave -noupdate -radix unsigned /pred_top_tb/uut/dbf_block/yy_8_by_8_x
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2449351941 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 209
configure wave -valuecolwidth 268
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {2435065911 ps} {2443725132 ps}
