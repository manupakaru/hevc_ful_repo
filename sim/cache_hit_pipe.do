onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_valid
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_last
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_ready
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_data
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/rid
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/rdata
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/rlast
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/rvalid
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/clk
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/miss_elem_fifo_wr_en
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/miss_elem_fifo_rd_en
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/hit_elem_fifo_wr_en
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/hit_elem_fifo_rd_en
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/output_fifo_full
add wave -noupdate -color Orange /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/data_read_stage_valid
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/output_fifo_full
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/output_fifo_almost_full_only
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/output_fifo_program_full
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/state_data_read
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_x_3d
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_y_3d
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_x_read
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_y_read
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_x_hit
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_y_hit
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/delta_y_hit
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/delta_x_hit
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_number_3_hit
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_number_4
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_number_3_read
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/xT_in_min_tus_read
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/yT_in_min_tus_read
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/bi_pred_block_cache_read
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/xT_in_min_tus_hit
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/yT_in_min_tus_hit
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/bi_pred_block_cache_hit
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/hit_elem_fifo_empty
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/miss_elem_fifo_empty
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/cache_r_addr
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/cache_w_addr
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/cache_wr_en
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/ref_pix_axi_ar_ready
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/tag_mem_wr_en
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/tag_addr
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/tag_write_addr
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/tag_block/mem
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/set_addr
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/tag_rdata_set
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/tag_compare_block/valid_bits_in
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/tag_addr_2d
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/set_idx_miss
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/age_wr_en
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/age_val_set
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/set_addr_d
add wave -noupdate -expand /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/age_block/mem
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/set_addr_2d
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/new_age_set
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/is_hit_d
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/is_hit
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_ready
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/cache_idle_out
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/ctu_monitor_block/new_ctu
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/ctu_monitor_block/ctu_time
add wave -noupdate -color Orange /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/valid_in
add wave -noupdate -color Orange /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/set_input_stage_valid
add wave -noupdate -expand -group stage1 -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/xT_in_min_tus
add wave -noupdate -expand -group stage1 -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/yT_in_min_tus
add wave -noupdate -expand -group stage1 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/state_set_input
add wave -noupdate -expand -group stage1 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/set_input_stage_ready
add wave -noupdate -expand -group stage1 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_x
add wave -noupdate -expand -group stage1 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_y
add wave -noupdate -color Orange /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/dest_enable_wire_valid
add wave -noupdate -group stage2 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/last_block_valid_1d
add wave -noupdate -group stage2 -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/xT_in_min_tus_2
add wave -noupdate -group stage2 -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/yT_in_min_tus_2
add wave -noupdate -group stage2 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/dst_strt_x_luma
add wave -noupdate -group stage2 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/dest_end_x_luma
add wave -noupdate -group stage2 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/state_tag_read
add wave -noupdate -group stage2 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/tag_read_stage_ready
add wave -noupdate -group stage2 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_x_d
add wave -noupdate -group stage2 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_y_d
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/tag_compare_stage_valid
add wave -noupdate -expand -group stage3 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_x_2d
add wave -noupdate -expand -group stage3 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_y_2d
add wave -noupdate -expand -group stage3 -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/xT_in_min_tus_3
add wave -noupdate -expand -group stage3 -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/yT_in_min_tus_3
add wave -noupdate -expand -group stage3 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/bi_pred_block_cache_3
add wave -noupdate -expand -group stage3 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/miss_elem_fifo_wr_en
add wave -noupdate -expand -group stage3 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/last_block_valid_2d
add wave -noupdate -expand -group stage3 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/is_hit_d
add wave -noupdate -expand -group stage3 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/state_tag_compare
add wave -noupdate -expand -group stage3 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/tag_compare_stage_ready
add wave -noupdate -expand -group stage3 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/dest_fill_x_mask_luma
add wave -noupdate -color Orange /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/data_read_stage_valid
add wave -noupdate -expand -group stage4 -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/xT_in_min_tus_4
add wave -noupdate -expand -group stage4 -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/yT_in_min_tus_4
add wave -noupdate -expand -group stage4 -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/bi_pred_block_cache_4
add wave -noupdate -expand -group stage4 /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/state_data_read
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/xT_in_min_tus_read
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/yT_in_min_tus_read
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/bi_pred_block_cache_read
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/miss_elem_fifo_empty
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/miss_elem_fifo_wr_en
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/last_block_valid_2d_read
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_last
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_valid
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/filer_idle_in
add wave -noupdate -divider {New Divider}
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_valid
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_last
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/dest_fill_x_mask_luma_read
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/ref_pix_axi_ar_ready
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/ref_pix_axi_ar_valid
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/cache_mem_block/r_addr_in
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/cache_mem_block/w_addr_in
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/cache_mem_block/w_en_in
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/cache_mem_block/w_data_in
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/ref_pix_axi_ar_addr
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/dest_fill_x_mask_luma_hit
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/dest_fill_y_mask_luma_hit
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_ready_reg
add wave -noupdate -expand /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_11x11
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/xT_in_min_tus_fifo_in
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/yT_in_min_tus_fifo_in
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_ready_internal
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_ready_reg
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/output_fifo_full
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/bi_pred_block_cache_fifo_in
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/cache_print_block/block_serve_time
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/valid_in
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_ready_state
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/xT_in_min_tus_out
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/yT_in_min_tus_out
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/filer_idle_in
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/ctb_xx
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/ctb_yy
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/read_stage_block/y_dbf_fifo_rd_en_out
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/read_stage_block/Xc
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/read_stage_block/Yc
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/read_stage_block/current__poc
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/xT_in_4x4_luma_out
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/yT_in_4x4_luma_out
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/luma_wght_pred_valid
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/read_stage_block/y_dbf_fifo_is_prog_empty
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/read_stage_block/y_dbf_fifo_wr_en_in
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/inter_filter_cache_idle_out
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_idle_in
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/inter_idle_to_intra
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {start {7704651349 ps} 0} {{is_hit should be high} {7704660314 ps} 1} {wrong_new_age_set {7704665000 ps} 1} {wrong! {7704675000 ps} 1}
quietly wave cursor active 1
configure wave -namecolwidth 170
configure wave -valuecolwidth 345
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {7704572840 ps} {7704757160 ps}
