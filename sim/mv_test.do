onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/read_stage_block/Xc_out
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/read_stage_block/Yc_out
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/xx_pb
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/yy_pb
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/current_dpb_idx_l0
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/current_dpb_idx_l1
add wave -noupdate -radix decimal /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/current_mv_field_mv_x_l0
add wave -noupdate -radix decimal /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/current_mv_field_mv_x_l1
add wave -noupdate -radix decimal /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/current_mv_field_mv_y_l0
add wave -noupdate -radix decimal /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/current_mv_field_mv_y_l1
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/current_mv_field_pred_flag_l0
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/current_mv_field_pred_flag_l1
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/current_mv_field_ref_idx_l0
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/current_mv_field_ref_idx_l1
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/current_mv_field_valid
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/current_mv_valid_now
add wave -noupdate -radix decimal /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/mv_l0_a_x
add wave -noupdate -radix decimal /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/mv_l0_a_y
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/a0_mv_field_mv_x_l0
add wave -noupdate -radix decimal /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/new_mv_l0_x_clipped
add wave -noupdate -radix decimal /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/new_mv_l0_x
add wave -noupdate -radix decimal /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/abs_dist_scale_into_mv_l0_x_plus_127
add wave -noupdate -radix decimal /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/dist_scale_into_mv_l0_x_d
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/distscale_factor_l0_clipped
add wave -noupdate -radix decimal /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/pipe_line_mv_l0_x
add wave -noupdate -radix decimal /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/mv_l0_a_x
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/distscale_factor_l1_clipped
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/distscale_factor_l0_clipped
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/a0_mv_field_mv_x_l0
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {40055026589 ps} 1} {{Cursor 2} {40055010938 ps} 1} {{Cursor 3} {40124711696 ps} 0}
quietly wave cursor active 3
configure wave -namecolwidth 266
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {40124258167 ps} {40125474071 ps}
