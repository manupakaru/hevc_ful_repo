onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /pred_top_tb/new_ctu
add wave -noupdate /pred_top_tb/uut/dbf_block/cnfig_dbf_fifo_is_full_out
add wave -noupdate /pred_top_tb/uut/dbf_block/cnfig_dbf_fifo_data_in
add wave -noupdate /pred_top_tb/uut/dbf_block/config_fifo_rd_en
add wave -noupdate -radix unsigned /pred_top_tb/uut/dbf_block/next_state
add wave -noupdate /pred_top_tb/uut/pred_block/fifo_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/state
add wave -noupdate /pred_top_tb/uut/pred_block/clk
add wave -noupdate /pred_top_tb/uut/dbf_block/cnfig_dbf_fifo_wr_en_in
add wave -noupdate /pred_top_tb/uut/pred_block/fifo_out
add wave -noupdate /pred_top_tb/uut/dbf_block/is_config_fifo_empty
add wave -noupdate /pred_top_tb/uut/dbf_block/cnfig_dbf_fifo_data_out
add wave -noupdate /pred_top_tb/uut/dbf_block/y_dbf_fifo_is_full_out
add wave -noupdate /pred_top_tb/uut/dbf_block/du_yy
add wave -noupdate /pred_top_tb/uut/dbf_block/du_xx
add wave -noupdate /pred_top_tb/uut/dbf_block/log2_du_size
add wave -noupdate -radix unsigned /pred_top_tb/uut/dbf_block/dbf_control_state
add wave -noupdate /pred_top_tb/uut/dbf_block/y_sao_fifo_wr_en
add wave -noupdate /pred_top_tb/uut/dbf_block/y_sao_fifo_data_out
add wave -noupdate /pred_top_tb/uut/dbf_block/y_dbf_fifo_rd_en_out
add wave -noupdate /pred_top_tb/uut/dbf_block/y_dbf_fifo_data
add wave -noupdate /pred_top_tb/uut/dbf_block/y_sao_fifo_data_out
add wave -noupdate /pred_top_tb/uut/dbf_block/y_common_predsample_4by4_x_int
add wave -noupdate /pred_top_tb/uut/dbf_block/y_sao_fifo_wr_en_out
add wave -noupdate /pred_top_tb/uut/dbf_block/ctb_yy
add wave -noupdate /pred_top_tb/uut/dbf_block/ctb_xx
add wave -noupdate -radix unsigned /pred_top_tb/uut/dbf_block/y_common_predsample_4by4_y_int
add wave -noupdate -radix unsigned /pred_top_tb/uut/dbf_block/y_common_predsample_4by4_x_int
add wave -noupdate /pred_top_tb/uut/dbf_block/end_of_du
add wave -noupdate /pred_top_tb/uut/dbf_block/yy_8_by_8_y_sent
add wave -noupdate /pred_top_tb/uut/dbf_block/yy_8_by_8_x_sent
add wave -noupdate /pred_top_tb/uut/dbf_block/du_size
add wave -noupdate /pred_top_tb/uut/dbf_block/test_y
add wave -noupdate /pred_top_tb/uut/dbf_block/test_x
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {16401044 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 243
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {28146439 ps}
