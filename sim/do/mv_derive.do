onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /pred_top_tb/clk
add wave -noupdate -radix decimal /pred_top_tb/file_location
add wave -noupdate /pred_top_tb/soft_file_loc
add wave -noupdate /pred_top_tb/uut/pred_block/mv_done
add wave -noupdate /pred_top_tb/new_poc
add wave -noupdate /pred_top_tb/uut/pred_block/write_en_out
add wave -noupdate /pred_top_tb/uut/pred_block/read_en_out
add wave -noupdate /pred_top_tb/clk
add wave -noupdate /pred_top_tb/reset
add wave -noupdate /pred_top_tb/enable
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/state_write_line
add wave -noupdate /pred_top_tb/fifo_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/y_P_pu_start_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/y_P_pu_end_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/y_P_pu_end_in_1
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_start_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_end_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_end_in_1
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/y_rel_left_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/y_ctb_addr
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/y_N_pu_in
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_rel_top_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_ctb_addr
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_N_pu_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/top_mvs_to_bs_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/top_line_done
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/state_col_write
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/state_axi_write
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/reset
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/yy_pb_temp
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/xx_pb_temp
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/yy_pb
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/xx_pb
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/cb_yy
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/cb_xx
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/prev_mv_field_valid_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/config_mode_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/cb_size_wire
add wave -noupdate /pred_top_tb/uut/pred_block/state
add wave -noupdate /pred_top_tb/fifo_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/state_write_line
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/y_rel_left_out
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/current__poc
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/merge_flag
add wave -noupdate -expand -group current_mv /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_mv_x_l0
add wave -noupdate -expand -group current_mv /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_mv_x_l1
add wave -noupdate -expand -group current_mv /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_mv_y_l0
add wave -noupdate -expand -group current_mv /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_mv_y_l1
add wave -noupdate -expand -group current_mv /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_pred_flag_l0
add wave -noupdate -expand -group current_mv /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_pred_flag_l1
add wave -noupdate -expand -group current_mv /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_ref_idx_l0
add wave -noupdate -expand -group current_mv /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_ref_idx_l1
add wave -noupdate -expand -group current_mv /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_valid
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mvp_l1_flag
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mvp_l1_idx
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_derive_state
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {88168749602 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 231
configure wave -valuecolwidth 231
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {88163623600 ps} {88173876400 ps}
