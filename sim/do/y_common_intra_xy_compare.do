onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /pred_top_tb/uut/pred_block/y_common_predsample_4by4_y_int
add wave -noupdate /pred_top_tb/uut/pred_block/y_common_predsample_4by4_x_int
add wave -noupdate /pred_top_tb/uut/pred_block/cb_valid_addition_reg
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/cb_common_predsample_4by4_x_int
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/intra_cb_predsample_4by4_x_reg
add wave -noupdate /pred_top_tb/uut/pred_block/intra_cb_predsample_4by4_y_reg
add wave -noupdate /pred_top_tb/uut/pred_block/cb_common_predsample_4by4_y_int
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/intra_cb_predsample_4by4_x_int
add wave -noupdate /pred_top_tb/uut/pred_block/clk
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {85790289 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 290
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {85784196 ps} {85799230 ps}
