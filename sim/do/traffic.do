onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /pred_top_tb/clk
add wave -noupdate /pred_top_tb/file_location
add wave -noupdate /pred_top_tb/soft_file_loc
add wave -noupdate /pred_top_tb/fifo_in
add wave -noupdate /pred_top_tb/new_poc
add wave -noupdate /pred_top_tb/soft_dpb_filled
add wave -noupdate /pred_top_tb/uut/write_en_out
add wave -noupdate /pred_top_tb/uut/read_en_out
add wave -noupdate /pred_top_tb/verify_dpb_poc/i
add wave -noupdate /pred_top_tb/verify_dpb_poc/soft_dpb_poc
add wave -noupdate /pred_top_tb/uut/pred_block/mv_done
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_derive_state
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cache_block/state
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/luma_filter_wrapper_block/filter_state
add wave -noupdate /pred_top_tb/cache_start
add wave -noupdate /pred_top_tb/new_ctu
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bi_pred_block_cache_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/inter_pred_sample_state
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cache_block/state
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2023972237 ps} 0} {{Cursor 2} {5152130000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 219
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {2023917803 ps} {2024026671 ps}
