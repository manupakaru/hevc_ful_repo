onerror {resume}
quietly set dataset_list [list compare vsim_old vsim1 vsim]
if {[catch {datasetcheck $dataset_list}]} {abort}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix unsigned compare:/pred_top_tb/uut/pred_block/\\inter_filter_cache_idle<>inter_filter_cache_idle\\
add wave -noupdate -format Logic -radix unsigned -childformat {{/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cache_block_ready -radix unsigned} {/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cache_block_ready -radix unsigned}} -expand -subitemconfig {/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cache_block_ready {-radix unsigned} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cache_block_ready {-radix unsigned}} compare:/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/\\cache_block_ready<>cache_block_ready\\
add wave -noupdate -format Logic -radix unsigned -childformat {{/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cache_idle_in -radix unsigned} {/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cache_idle_in -radix unsigned}} -subitemconfig {/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cache_idle_in {-radix unsigned} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cache_idle_in {-radix unsigned}} compare:/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/\\cache_idle_in<>cache_idle_in\\
add wave -noupdate -format Logic -radix unsigned compare:/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/\\end_of_cu<>end_of_cu\\
add wave -noupdate -format Logic -radix unsigned -childformat {{/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/end_of_tu -radix unsigned} {/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/end_of_tu -radix unsigned}} -subitemconfig {/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/end_of_tu {-radix unsigned} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/end_of_tu {-radix unsigned}} compare:/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/\\end_of_tu<>end_of_tu\\
add wave -noupdate -radix unsigned -childformat {{/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/inter_pred_sample_state -radix unsigned} {/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/inter_pred_sample_state -radix unsigned}} -expand -subitemconfig {/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/inter_pred_sample_state {-radix unsigned} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/inter_pred_sample_state {-radix unsigned}} compare:/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/\\inter_pred_sample_state<>inter_pred_sample_state\\
add wave -noupdate -radix unsigned compare:/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/\\xT_in_min_tus<>xT_in_min_tus\\
add wave -noupdate -radix unsigned compare:/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/\\yT_in_min_tus<>yT_in_min_tus\\
add wave -noupdate -radix unsigned compare:/pred_top_tb/uut/pred_block/\\state<>state\\
add wave -noupdate -radix unsigned -childformat {{/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/luma_filter_wrapper_block/filter_state -radix unsigned} {/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/luma_filter_wrapper_block/filter_state -radix unsigned}} -expand -subitemconfig {/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/luma_filter_wrapper_block/filter_state {-radix unsigned} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/luma_filter_wrapper_block/filter_state {-radix unsigned}} compare:/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/luma_filter_wrapper_block/\\filter_state<>filter_state\\
add wave -noupdate -format Logic -expand compare:/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cache_block/\\valid_in<>valid_in\\
add wave -noupdate -radix unsigned -childformat {{/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cache_block/state -radix unsigned} {/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cache_block/state -radix unsigned}} -expand -subitemconfig {/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cache_block/state {-radix unsigned} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cache_block/state {-radix unsigned}} compare:/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cache_block/\\state<>state\\
add wave -noupdate -format Logic -expand compare:/pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/luma_filter_wrapper_block/\\valid_in<>valid_in\\
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1812956732 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 426
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {1812870610 ps} {1813395446 ps}
