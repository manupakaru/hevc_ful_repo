onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /hevc_test_tb/uut/hdmi_fifo_monitor_block/full
add wave -noupdate /hevc_test_tb/uut/hdmi_fifo_monitor_block/hdmi_fifo_in
add wave -noupdate /hevc_test_tb/uut/hdmi_fifo_monitor_block/reset
add wave -noupdate /hevc_test_tb/uut/hdmi_fifo_monitor_block/wr_en
add wave -noupdate /hevc_test_tb/uut/hdmi_fifo_monitor_block/clk
add wave -noupdate -expand -group ref_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/dpb_buffer/din
add wave -noupdate -expand -group ref_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/dpb_buffer/rd_en
add wave -noupdate -expand -group ref_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/dpb_buffer/rst
add wave -noupdate -expand -group ref_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/dpb_buffer/wr_en
add wave -noupdate -expand -group ref_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/dpb_buffer/dout
add wave -noupdate -expand -group ref_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/dpb_buffer/empty
add wave -noupdate -expand -group ref_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/dpb_buffer/full
add wave -noupdate -expand -group ref_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/dpb_buffer/prog_full
add wave -noupdate -expand -group disp_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/display_buffer/din
add wave -noupdate -expand -group disp_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/display_buffer/rd_en
add wave -noupdate -expand -group disp_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/display_buffer/rst
add wave -noupdate -expand -group disp_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/display_buffer/wr_en
add wave -noupdate -expand -group disp_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/display_buffer/dout
add wave -noupdate -expand -group disp_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/display_buffer/empty
add wave -noupdate -expand -group disp_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/display_buffer/full
add wave -noupdate -expand -group disp_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/display_buffer/prog_full
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/log2_ctu_size_out
add wave -noupdate /hevc_test_tb/uut/hdmi_fifo_monitor_block/wr_en
add wave -noupdate /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rddata_state
add wave -noupdate /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rvalid
add wave -noupdate /hevc_test_tb/uut/axi_displaybuffer_handle_block/reset
add wave -noupdate /hevc_test_tb/uut/axi_displaybuffer_handle_block/clk
add wave -noupdate /hevc_test_tb/uut/axi_displaybuffer_handle_block/hdmi_fifo_almost_full
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {382799641 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 374
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {631538648 ps}
