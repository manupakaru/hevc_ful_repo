onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_idx_in_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/yT_in_min_tus_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/xT_in_min_tus_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/valid_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/next_mv_field_valid_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/next_dpb_idx_l1_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/next_dpb_idx_l0_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/next_mv_field_mv_y_l1_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/next_mv_field_mv_x_l1_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/next_mv_field_mv_y_l0_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/next_mv_field_mv_x_l0_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/next_mv_field_pred_flag_l1_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/next_mv_field_pred_flag_l0_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/current_mv_field_ref_idx_l0
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/current_mv_field_ref_idx_l1
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/ref_dpb_idx_l0
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/ref_dpb_idx_l1
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_x_end_chma
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_y_end_luma
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_x_end_luma
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_y_offset_chma
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_x_offset_chma
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_y_offset_luma
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_x_offset_luma
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_ready
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_ar_valid
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_ar_prot
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_ar_burst
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_ar_size
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_ar_len
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_ar_addr
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_25cr_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_25cb_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_121_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ch_frac_y_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ch_frac_x_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/luma_ref_height_y_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/luma_ref_width_x_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/chma_ref_height_y_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/chma_ref_width_x_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/chma_ref_start_y_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/chma_ref_start_x_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/luma_ref_start_y_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/luma_ref_start_x_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/y0_tu_end_in_min_tus_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/x0_tu_end_in_min_tus_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/bi_pred_block_cache_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/res_present_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/cache_idle_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_ready
add wave -noupdate -radix unsigned /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/yT_in_min_tus_out
add wave -noupdate -radix unsigned /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/xT_in_min_tus_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/bu_idx
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/bu_row_idx
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_x_addr_reg
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/curr_y_addr_reg
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_y_end_chma
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/bu_idx_val
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/iu_idx_val
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/iu_idx_row_val
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_idx_val
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_ar_addr
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_ar_burst
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_ar_len
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_ar_prot
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_ar_ready
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_ar_size
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_ar_valid
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_data
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_ready
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_resp
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_last
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_valid
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/luma_filter_wrapper_block/yT_4x4_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/luma_filter_wrapper_block/xT_4x4_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/luma_filter_wrapper_block/block_ready_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/luma_filter_wrapper_block/pred_pix_out_4x4
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/luma_filter_wrapper_block/yT_4x4_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/luma_filter_wrapper_block/xT_4x4_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/luma_filter_wrapper_block/ref_pix_l_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/luma_filter_wrapper_block/valid_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/current__poc
add wave -noupdate /intra_pred_tb/uut/axi_write_block/axi_awvalid
add wave -noupdate /intra_pred_tb/uut/axi_write_block/axi_awaddr
add wave -noupdate /intra_pred_tb/uut/axi_write_block/axi_wvalid
add wave -noupdate /intra_pred_tb/uut/axi_write_block/axi_wdata
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/Xc
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/Yc
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/read_stage_valid
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/y_q_block_1
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/y_q_block_2
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/y_q_block_3
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/y_q_block_4
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/dbf_main_to_sao_valid
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/y_q_block_1_dbf_to_sao
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/y_q_block_2_dbf_to_sao
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/dpb_fifo_rd_en_in
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/dpb_fifo_data_out
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/y_q_block_2_in
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/data_valid_in
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/dpb_fifo_data_out
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/ready_out
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/valid_out
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/y_K_block_out
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/y_q_block_2_out
add wave -noupdate -group dbf_signals -radix unsigned /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/state
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/display_fifo_data_wr_en
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/valid_out
add wave -noupdate -group dbf_signals /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/display_fifo_data_in
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/start
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/q_block_2_in
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/K_block_out
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/row_we_a
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/row_en_a
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/row_we_b
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/row_en_b
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/row_addr_a
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/row_addr_b
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/row_data_in_a
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/row_data_in_b
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/row_data_out_a
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/row_data_out_b
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/col_we_a
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/col_en_a
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/col_we_b
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/col_en_b
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/col_addr_a
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/col_addr_b
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/col_data_in_a
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/col_data_in_b
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/col_data_out_a
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/col_data_out_b
add wave -noupdate -group sao_luma_controlle /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/q_block_2_out
add wave -noupdate -group sao_luma_controlle -radix decimal /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/state
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/luma_sao_controller_module/A_block_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_stage_block/luma_db_controller_module/start
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/read_stage_block/y_data_out_a
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/read_stage_block/y_A_block_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/read_stage_block/y_we_a
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/read_stage_block/y_q_block_2_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_stage_block/luma_db_controller_module/done
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_stage_block/slice_dbf_disable_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_stage_block/luma_db_controller_module/slice_dbf_disable_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_stage_block/luma_db_controller_module/A_block_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_stage_block/luma_db_controller_module/A_block_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_stage_block/luma_db_controller_module/done_filter
add wave -noupdate -radix unsigned /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_stage_block/luma_db_controller_module/state
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 2} {2010631962 ps} 1} {{Cursor 2} {2010751967 ps} 1} {{Cursor 3} {633301349 ps} 1} {{q2 in} {202250000 ps} 1} {{Cursor 5} {220290000 ps} 0}
quietly wave cursor active 5
configure wave -namecolwidth 194
configure wave -valuecolwidth 325
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {219964308 ps} {220615692 ps}
