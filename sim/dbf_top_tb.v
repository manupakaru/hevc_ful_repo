`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:53:47 04/03/2014
// Design Name:   dbf_top
// Module Name:   C:/Users/Geethan Karunaratne/Desktop/DBFcore/dbf_core/testbench/dbf_top_tb.v
// Project Name:  dbf_core
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: dbf_top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module dbf_top_tb;
    `include "pred_def.v"
    `include "inter_axi_def.v"
    
	// Inputs
	reg clk;
	reg reset;
	wire [7:0] 		bs_fifo_data_read_out;
	wire [7:0] 		bs_fifo_data_in;
	wire 			bs_fifo_wr_en_in;
	
	wire [31:0] 	cnfig_dbf_fifo_data_in;
	wire 			cnfig_dbf_fifo_wr_en_in;
	reg [133:0]		y_dbf_fifo_data_in;
	reg 			y_dbf_fifo_wr_en_in;
	wire [127:0] 	cb_dbf_fifo_data_in;
	wire 			cb_dbf_fifo_wr_en_in;
	wire [127:0] 	cr_dbf_fifo_data_in;
	wire 			cr_dbf_fifo_wr_en_in;

	// Outputs
	wire bs_fifo_full_out;
	wire cnfig_dbf_fifo_is_full_out;
	wire y_dbf_fifo_is_full_out;
	wire cb_dbf_fifo_is_full_out;
	wire cr_dbf_fifo_is_full_out;
	
	
    wire [11:0] PIC_HEIGHT;// = 240;
    wire [11:0] PIC_WIDTH; //= 416;   
    assign PIC_HEIGHT = uut.read_stage_block.pic_height;
    assign PIC_WIDTH = uut.read_stage_block.pic_width;
    
    integer y_file_in;
    integer cb_file_in;
    integer cr_file_in;   
    integer config_file;   
    integer bs_file;   
    integer bs_count;
    
    integer final_pixl_file;                                            
    integer pre_sao_file;   
    
	wire 										display_fifo_is_empty_out;
	wire  										display_fifo_rd_en_in;
	wire  [SAO_OUT_FIFO_WIDTH-1:0]	display_fifo_data_out;
	
	wire  									dpb_fifo_is_empty_out;
	wire   									dpb_fifo_rd_en_in;
	wire  [SAO_OUT_FIFO_WIDTH-1:0]	dpb_fifo_data_out;    
	
	wire       [PIC_WIDTH_WIDTH-1:0] pic_width_out;
	wire       [PIC_WIDTH_WIDTH-1:0] pic_height_out;

	wire [AXI_ADDR_WDTH -1:0]			dpb_axi_addr_out;
	wire [AXI_ADDR_WDTH -1:0]			poc_axi_addr_out;
    
	// Instantiate the Unit Under Test (UUT)
	dbf_top uut (
		.clk(clk), 
		.reset(reset), 
		.bs_fifo_data_in				(bs_fifo_data_in), 
		.bs_fifo_wr_en_in				(bs_fifo_wr_en_in), 
		.bs_fifo_full_out				(bs_fifo_full_out), 
		
		.cnfig_dbf_fifo_is_full_out		(cnfig_dbf_fifo_is_full_out), 
		.cnfig_dbf_fifo_data_in			(cnfig_dbf_fifo_data_in), 
		.cnfig_dbf_fifo_wr_en_in		(cnfig_dbf_fifo_wr_en_in), 
		
		.y_dbf_fifo_is_full_out			(y_dbf_fifo_is_full_out), 
		.y_dbf_fifo_data_in				(y_dbf_fifo_data_in), 
		.y_dbf_fifo_wr_en_in			(y_dbf_fifo_wr_en_in), 
		.cb_dbf_fifo_is_full_out		(cb_dbf_fifo_is_full_out), 
		.cb_dbf_fifo_data_in			(cb_dbf_fifo_data_in), 
		.cb_dbf_fifo_wr_en_in			(cb_dbf_fifo_wr_en_in), 
		.cr_dbf_fifo_is_full_out		(cr_dbf_fifo_is_full_out), 
		.cr_dbf_fifo_data_in			(cr_dbf_fifo_data_in), 
		.cr_dbf_fifo_wr_en_in			(cr_dbf_fifo_wr_en_in), 
		
		.display_fifo_is_empty_out	(display_fifo_is_empty_out),
		.display_fifo_rd_en_in		(display_fifo_rd_en_in),				
		.display_fifo_data_out		(display_fifo_data_out),
		.dpb_fifo_is_empty_out		(dpb_fifo_is_empty_out),
		.dpb_fifo_rd_en_in			(dpb_fifo_rd_en_in),		
		.dpb_fifo_data_out			(dpb_fifo_data_out),
		.dpb_axi_addr_out			(dpb_axi_addr_out),
		.poc_axi_addr_out			(poc_axi_addr_out) ,
		
		.pic_width_out				(pic_width_out),	
		.pic_height_out				(pic_height_out)
		
	);
	
fifo_write_driver 
	#(
		.WIDTH 		(32),
		.FILE_NAME  ("pred_to_dbf_config")
	)
	config_driver (
    .clk(clk), 
    .reset(reset), 
    .out(cnfig_dbf_fifo_data_in), 
    .prog_full(cnfig_dbf_fifo_is_full_out), 
    .wr_en(cnfig_dbf_fifo_wr_en_in)
    );

fifo_write_driver 
	#(
		.WIDTH 		(8),
		.FILE_NAME  ("pred_to_dbf_bs")
	)
	bs_driver (
    .clk(clk), 
    .reset(reset), 
    .out(bs_fifo_data_read_out), 
    .prog_full(bs_fifo_full_out), 
    .wr_en(bs_fifo_wr_en_in)
    );


fifo_write_driver 
	#(
		.WIDTH 		(128),
		.FILE_NAME  ("pred_to_dbf_cb")
	)
	dbf_cb_driver (
    .clk(clk), 
    .reset(reset), 
    .out(cb_dbf_fifo_data_in), 
    .prog_full(cb_dbf_fifo_is_full_out), 
    .wr_en(cb_dbf_fifo_wr_en_in)
    );

fifo_write_driver 
	#(
		.WIDTH 		(128),
		.FILE_NAME  ("pred_to_dbf_cr")
	)
	dbf_cr_driver (
    .clk(clk), 
    .reset(reset), 
    .out(cr_dbf_fifo_data_in), 
    .prog_full(cr_dbf_fifo_is_full_out), 
    .wr_en(cr_dbf_fifo_wr_en_in)
    );
	
fifo_read_driver dpb_read (
    .empty(dpb_fifo_is_empty_out), 
    .rd_en(dpb_fifo_rd_en_in)
    );
	
fifo_read_driver display_read (
    .empty(display_fifo_is_empty_out), 
    .rd_en(display_fifo_rd_en_in)
    );
	
	assign {bs_fifo_data_in[01:00],bs_fifo_data_in[03:02],bs_fifo_data_in[05:04],bs_fifo_data_in[07:06]} = {bs_fifo_data_read_out};
	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 1;

		y_dbf_fifo_data_in = 0;
		y_dbf_fifo_wr_en_in = 0;

		// Wait 100 ns for global reset to finish
        
        y_file_in = $fopen("pred_to_dbf_yy","rb");
        
        final_pixl_file = $fopen("BQSquare_416x240_60_qp37_416x240_8bit_final.yuv","rb");
        pre_sao_file = $fopen("BQSquare_416x240_60_qp37_416x240_8bit_pre-sao.yuv","rb");
        
		#100;
        reset = 0;
		// Add stimulus here

	end
  


    always #10 clk = ~clk;
   
     always@(posedge clk) begin
		if(reset) begin
		end
		else begin
			write_dbf_yy_fifo();
		end
	end
 
    task write_dbf_yy_fifo;
    
        reg [7:0] soft_pixel  ;
        reg [7:0] soft_pixel1  ;
        reg [7:0] soft_pixel2  ;
        reg [7:0] soft_pixel3  ;
        reg [7:0] soft_pixel4  ;
        reg [7:0] soft_pixel5  ;
        reg [7:0] soft_pixel6  ;
        reg [7:0] soft_pixel7  ;
        reg [7:0] soft_pixel8  ;
        reg [7:0] soft_pixel9  ;
        reg [7:0] soft_pixel10  ;
        reg [7:0] soft_pixel11 ;
        reg [7:0] soft_pixel12  ;
        reg [7:0] soft_pixel13  ;
        reg [7:0] soft_pixel14  ;
        reg [7:0] soft_pixel15  ;
        reg [7:0] soft_pixel16  ;
        
        reg [15:0] dbf_soft_x8, dbf_soft_y8;   
            
        begin
            if(y_dbf_fifo_is_full_out ==0) begin
            
                dbf_soft_y8[7:0]   = $fgetc(y_file_in);
                dbf_soft_y8[15:8]  = $fgetc(y_file_in);
                dbf_soft_x8[7:0]   = $fgetc(y_file_in);
                dbf_soft_x8[15:8]  = $fgetc(y_file_in);         


                soft_pixel1   = $fgetc(y_file_in);
                soft_pixel2   = $fgetc(y_file_in);
                soft_pixel3   = $fgetc(y_file_in);
                soft_pixel4   = $fgetc(y_file_in);
                soft_pixel5   = $fgetc(y_file_in);
                soft_pixel6   = $fgetc(y_file_in);
                soft_pixel7   = $fgetc(y_file_in);
                soft_pixel8   = $fgetc(y_file_in);
                soft_pixel9   = $fgetc(y_file_in);
                soft_pixel10  = $fgetc(y_file_in);
                soft_pixel11  = $fgetc(y_file_in);
                soft_pixel12  = $fgetc(y_file_in);
                soft_pixel13  = $fgetc(y_file_in);
                soft_pixel14  = $fgetc(y_file_in);
                soft_pixel15  = $fgetc(y_file_in);
                soft_pixel16  = $fgetc(y_file_in);
                        
                y_dbf_fifo_data_in = #1 {dbf_soft_y8[2:0], dbf_soft_x8[2:0],
                                            soft_pixel16,soft_pixel15,soft_pixel14,soft_pixel13,
                                            soft_pixel12,soft_pixel11,soft_pixel10,soft_pixel9 ,
                                            soft_pixel8 ,soft_pixel7 ,soft_pixel6 ,soft_pixel5 ,
                                            soft_pixel4 ,soft_pixel3 ,soft_pixel2 ,soft_pixel1 }  ; 
                y_dbf_fifo_wr_en_in = #1 1'b1;
            end
            else begin
                y_dbf_fifo_wr_en_in = #1 1'b0;
            end
        end
    
    endtask
    
    
	
	always@(posedge clk) begin
		sao_out_monitor();
	end
	
	
	reg [3:0] poc_4bits;
    reg     [PIC_WIDTH_WIDTH-1:0]          							  pic_width_now;
    reg     [PIC_WIDTH_WIDTH-1:0]          							  pic_height_now;
	
	task sao_out_monitor;

        // reg [7:0] soft_yy_pixel [7:0][7:0]  ;
        // reg [7:0] soft_cb_pixel [3:0][3:0]  ;
        // reg [7:0] soft_cr_pixel [3:0][3:0]  ;
		
        reg [7:0] hard_yy_pixel [7:0][7:0]  ;
        reg [7:0] hard_cb_pixel [3:0][3:0]  ;
        reg [7:0] hard_cr_pixel [3:0][3:0]  ;

		reg [X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1: 0]				Xc_out_8x8;
		reg [X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1: 0]				Yc_out_8x8;	

		integer frame_offset_sao_out;
		reg[7:0] temp;
		integer i,j;
		begin
			if(display_fifo_is_empty_out == 0) begin
				if(display_fifo_rd_en_in) begin
					{Xc_out_8x8,Yc_out_8x8 ,
						hard_yy_pixel[7][7],hard_yy_pixel[7][6],hard_yy_pixel[7][5],hard_yy_pixel[7][4],hard_yy_pixel[7][3],hard_yy_pixel[7][2],hard_yy_pixel[7][1],hard_yy_pixel[7][0],
						hard_yy_pixel[6][7],hard_yy_pixel[6][6],hard_yy_pixel[6][5],hard_yy_pixel[6][4],hard_yy_pixel[6][3],hard_yy_pixel[6][2],hard_yy_pixel[6][1],hard_yy_pixel[6][0],
						hard_yy_pixel[5][7],hard_yy_pixel[5][6],hard_yy_pixel[5][5],hard_yy_pixel[5][4],hard_yy_pixel[5][3],hard_yy_pixel[5][2],hard_yy_pixel[5][1],hard_yy_pixel[5][0],
						hard_yy_pixel[4][7],hard_yy_pixel[4][6],hard_yy_pixel[4][5],hard_yy_pixel[4][4],hard_yy_pixel[4][3],hard_yy_pixel[4][2],hard_yy_pixel[4][1],hard_yy_pixel[4][0],
						hard_yy_pixel[3][7],hard_yy_pixel[3][6],hard_yy_pixel[3][5],hard_yy_pixel[3][4],hard_yy_pixel[3][3],hard_yy_pixel[3][2],hard_yy_pixel[3][1],hard_yy_pixel[3][0],
						hard_yy_pixel[2][7],hard_yy_pixel[2][6],hard_yy_pixel[2][5],hard_yy_pixel[2][4],hard_yy_pixel[2][3],hard_yy_pixel[2][2],hard_yy_pixel[2][1],hard_yy_pixel[2][0],
						hard_yy_pixel[1][7],hard_yy_pixel[1][6],hard_yy_pixel[1][5],hard_yy_pixel[1][4],hard_yy_pixel[1][3],hard_yy_pixel[1][2],hard_yy_pixel[1][1],hard_yy_pixel[1][0],
						hard_yy_pixel[0][7],hard_yy_pixel[0][6],hard_yy_pixel[0][5],hard_yy_pixel[0][4],hard_yy_pixel[0][3],hard_yy_pixel[0][2],hard_yy_pixel[0][1],hard_yy_pixel[0][0],
						
						hard_cb_pixel[3][3],hard_cb_pixel[3][2],hard_cb_pixel[3][1],hard_cb_pixel[3][0],
						hard_cb_pixel[2][3],hard_cb_pixel[2][2],hard_cb_pixel[2][1],hard_cb_pixel[2][0],
						hard_cb_pixel[1][3],hard_cb_pixel[1][2],hard_cb_pixel[1][1],hard_cb_pixel[1][0],
						hard_cb_pixel[0][3],hard_cb_pixel[0][2],hard_cb_pixel[0][1],hard_cb_pixel[0][0],
						
						hard_cr_pixel[3][3],hard_cr_pixel[3][2],hard_cr_pixel[3][1],hard_cr_pixel[3][0],
						hard_cr_pixel[2][3],hard_cr_pixel[2][2],hard_cr_pixel[2][1],hard_cr_pixel[2][0],
						hard_cr_pixel[1][3],hard_cr_pixel[1][2],hard_cr_pixel[1][1],hard_cr_pixel[1][0],
						hard_cr_pixel[0][3],hard_cr_pixel[0][2],hard_cr_pixel[0][1],hard_cr_pixel[0][0]		
						} = display_fifo_data_out;
						
					if(Xc_out_8x8 ==0 && Yc_out_8x8 ==0 ) begin
						poc_4bits = poc_axi_addr_out/`REF_PIX_FRAME_OFFSET;
						pic_width_now = pic_width_out;
						pic_height_now = pic_height_out;
					end

					
					frame_offset_sao_out = pic_height_now*pic_width_now * poc_4bits *3 /2;
					$display("dbf out block x=%d y=%d",Xc_out_8x8,Yc_out_8x8);
					for (j = 0 ; j < DBF_OUT_Y_BLOCK_SIZE ; j = j + 1) begin
						for (i = 0 ; i < DBF_OUT_Y_BLOCK_SIZE ; i = i + 1) begin
							temp = $fseek(pre_sao_file,(frame_offset_sao_out + (pic_width_now)*(j + 8*Yc_out_8x8) + 8*Xc_out_8x8 + i),0);
							temp = $fgetc(pre_sao_file);
							if(temp != hard_yy_pixel[j][i]) begin
								$display("luma sao out wrong @ x=%d y=%d hard %x soft %x offset %d",8*Xc_out_8x8 + i,j + 8*Yc_out_8x8,hard_yy_pixel[j][i],temp,frame_offset_sao_out );
								$stop;
							end
						end
					end		
					frame_offset_sao_out = pic_height_now*pic_width_now * poc_4bits *3 /2 + pic_height_now*pic_width_now;
					for (j = 0 ; j < DBF_OUT_Y_BLOCK_SIZE/2 ; j = j + 1) begin
						for (i = 0 ; i < DBF_OUT_Y_BLOCK_SIZE/2 ; i = i + 1) begin
							temp = $fseek(pre_sao_file,(frame_offset_sao_out + (pic_width_now/2)*(j + 8*Yc_out_8x8/2) + 8*Xc_out_8x8/2 + i),0);
							temp = $fgetc(pre_sao_file);
							if(temp != hard_cb_pixel[j][i]) begin
								$display("cb sao out wrong @ x=%d y=%d hard %x soft %x",8*Xc_out_8x8/2 + i,j + 8*Yc_out_8x8/2,hard_cb_pixel[j][i],temp,frame_offset_sao_out );
								$stop;
							end
						end
					end		
					frame_offset_sao_out = pic_height_now*pic_width_now * poc_4bits *3 /2 + pic_height_now*pic_width_now + pic_height_now*pic_width_now/4;
					for (j = 0 ; j < DBF_OUT_Y_BLOCK_SIZE/2 ; j = j + 1) begin
						for (i = 0 ; i < DBF_OUT_Y_BLOCK_SIZE/2 ; i = i + 1) begin
							temp = $fseek(pre_sao_file,(frame_offset_sao_out + (pic_width_now/2)*(j + (8*Yc_out_8x8/2)) + ((8*Xc_out_8x8/2) + i)),0);
							temp = $fgetc(pre_sao_file);
							if(temp != hard_cr_pixel[j][i]) begin
								$display("cr sao out wrong @ x=%d y=%d hard %x soft %x",((8*Xc_out_8x8/2) + i),(j + (8*Yc_out_8x8/2)),hard_cr_pixel[j][i],temp,frame_offset_sao_out );
								$stop;
							end
							else begin
								// $display("cr sao out wrong @ x=%d y=%d hard %x soft %x",((8*Xc_out_8x8/2) + i),(j + (8*Yc_out_8x8/2)),hard_cr_pixel[j][i],temp,frame_offset_sao_out );
							end
						end
					end		
					
				end
			end
			else begin
				if(display_fifo_rd_en_in) begin
					$display("reading fifo when empty");
					$stop;
				end
			end
		end
	endtask
endmodule

