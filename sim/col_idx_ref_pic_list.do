onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list0_poc_addr
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/from_top_ref_pic_list0_poc_addr
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list0_poc_data_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list0_poc_data_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list0_poc_wr_en
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list1_poc_addr
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list1_poc_data_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list1_poc_data_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list1_poc_wr_en
add wave -noupdate -divider {New Divider}
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/dpb_idx_ref_poc_l0_r_addr
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/ref_idx_l0_col_poc_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/from_top_ref_pic_list_poc_data_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/from_top_ref_pic_list_poc_wr_en
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/dpb_idx_ref_poc_l0_w_addr
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/tbl0_clipped
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/tdl0_clipped
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/tbl1_clipped
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/tdl1_clipped
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/state_temporal
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_l0_poc_diff
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_l1_poc_diff
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_l0_poc_diff_wire
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/ref_idx_l1_col
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/ref_idx_l0_col
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {473868296 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 313
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {4080532553 ps}
