onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /cache_only_tb/uut/cache_block/bi_pred_block_cache_in
add wave -noupdate /cache_only_tb/uut/cache_block/ch_frac_x
add wave -noupdate /cache_only_tb/uut/cache_block/ch_frac_y
add wave -noupdate /cache_only_tb/uut/cache_block/chma_ref_height_y_in
add wave -noupdate /cache_only_tb/uut/cache_block/chma_ref_start_x_in
add wave -noupdate /cache_only_tb/uut/cache_block/chma_ref_start_y_in
add wave -noupdate /cache_only_tb/uut/cache_block/chma_ref_width_x_in
add wave -noupdate /cache_only_tb/uut/cache_block/clk
add wave -noupdate /cache_only_tb/uut/cache_block/filer_idle_in
add wave -noupdate /cache_only_tb/uut/cache_block/luma_ref_height_y_in
add wave -noupdate /cache_only_tb/uut/cache_block/luma_ref_start_x_in
add wave -noupdate /cache_only_tb/uut/cache_block/luma_ref_start_y_in
add wave -noupdate /cache_only_tb/uut/cache_block/luma_ref_width_x_in
add wave -noupdate /cache_only_tb/uut/cache_block/pic_height
add wave -noupdate /cache_only_tb/uut/cache_block/pic_width
add wave -noupdate /cache_only_tb/uut/cache_block/ref_idx_in_in
add wave -noupdate /cache_only_tb/uut/cache_block/ref_pix_axi_ar_ready
add wave -noupdate /cache_only_tb/uut/cache_block/ref_pix_axi_r_data
add wave -noupdate /cache_only_tb/uut/cache_block/ref_pix_axi_r_last
add wave -noupdate /cache_only_tb/uut/cache_block/ref_pix_axi_r_resp
add wave -noupdate /cache_only_tb/uut/cache_block/ref_pix_axi_r_valid
add wave -noupdate /cache_only_tb/uut/cache_block/res_present_in
add wave -noupdate /cache_only_tb/uut/cache_block/reset
add wave -noupdate /cache_only_tb/uut/cache_block/valid_in
add wave -noupdate /cache_only_tb/uut/cache_block/x0_tu_end_in_min_tus_in
add wave -noupdate /cache_only_tb/uut/cache_block/y0_tu_end_in_min_tus_in
add wave -noupdate -radix unsigned /cache_only_tb/uut/cache_block/yT_in_min_tus_in
add wave -noupdate -radix unsigned /cache_only_tb/uut/cache_block/xT_in_min_tus_in
add wave -noupdate /cache_only_tb/design_clk
add wave -noupdate /cache_only_tb/uut/design_clk
add wave -noupdate /cache_only_tb/uut/cache_block/cache_idle_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {37990000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 360
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {48304266 ps}
