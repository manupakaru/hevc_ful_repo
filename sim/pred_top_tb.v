`timescale 1ps/100fs

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   02:39:58 12/01/2013
// Design Name:   pred_top
// Module Name:   D:/090250V/SEMESTER 7/HEVC decoder/HDL codes/pred_top/sim/pred_top_tb.v
// Project Name:  prediction
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: pred_top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module pred_top_tb;

    `include "pred_def.v"
    `include "cache_configs_def.v"
    `include "inter_axi_def.v"
    
    parameter SOFT_RUN = 1;
    localparam                          STATE_READ_WAIT = 0;
    localparam                          STATE_ACTIVE = 1;
    localparam                          STATE_WRITE_WAIT = 2;
    localparam                          STATE_ERROR = 3;
    localparam                          STATE_CURRENT_POC = 4;
    localparam                          STATE_CUR_PIC_FIL_IDX = 5;
    localparam                          STATE_REF_PIC_LIST_5_UPDATE = 6;
    localparam                          STATE_REF_PIC_LIST_5_SCAN   = 7;
    localparam                          STATE_SEND_POC_TO_DBF = 8;
    localparam                          STATE_WRITE_WAIT_CUR_DPB_IDX_TO_DBF = 9;
    localparam                          STATE_REF_PIC_WRITE_WAIT = 10;
    localparam                          STATE_DPB_ADD_PREV_PIC = 11;
    localparam                          STATE_POC_READ_WAIT = 12;
    localparam                          STATE_MVD1_READ_WAIT = 13;
    localparam                          STATE_MVD2_READ_WAIT = 14;
    localparam                          STATE_MVD1_READ = 15;
    localparam                          STATE_MVD2_READ = 16;
    localparam                          STATE_MV_RETURN_WAIT1 = 17;
    localparam                          STATE_MV_RETURN_WAIT2 = 18;
    localparam                          STATE_MV_RETURN_WAIT3 = 19;
    localparam                          STATE_INTER_PREFETCH_COL_WRITE_WAIT = 20;
    localparam                          STATE_GET_ST_RPS_ENTRY = 21;
    localparam                          STATE_GET_ST_RPS_HEADER = 22;
    localparam                          STATE_SLICE_1_WRITE_WAIT = 23;
    localparam                          STATE_REF_PIC_LIST_TRANSFER = 24;
    localparam                          STATE_SET_ST_RPS_ENTRY_ADDR_FOR_REF_POC5 = 25;
    
    localparam                          WRITE_REF_PIC_STATE_IDLE = 0;
    localparam                          WRITE_REF_PIC_STATE_CHECK = 1;
    localparam                          WRITE_REF_PIC_STATE_POC_WRITE = 2;
    localparam                          WRITE_REF_PIC_STATE_IDX_WRITE = 3;
    localparam                          WRITE_REF_PIC_STATE_DONE = 4;
    localparam                          WRITE_REF_PIC_FIND_FOL_IDX = 5;

    localparam                          STATE_SET_INPUTS                = 0;    // the state that is entered upon reset
    localparam                          STATE_TAG_READ                  = 1;
    localparam                          STATE_TAG_COMPARE               = 2;
    localparam                          STATE_C_LINE_HIT_FETCH          = 3;
    localparam                          STATE_C_LINE_MISS_FETCH         = 4;
    localparam                          STATE_DEST_FILL                 = 5;
    localparam                          STATE_CACHE_READY               = 6;
    localparam                          STATE_MISS_AR_REDY_WAIT         = 7;
    
    localparam                          STATE_MV_DERIVE_CONFIG_UPDATE = 0;
    localparam                          STATE_PREFETCH_COL_WRITE_WAIT = 1;
    localparam                          STATE_MV_DERIVE_PU_OVERWRIT_3 = 2;
    localparam                          STATE_MV_DERIVE_AVAILABLE_CHECK4 = 3;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_9 = 4;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_10 = 5;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_11 = 6;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_12 = 7;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_MERGE_13 = 8;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_MERGE_14     = 9;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_MERGE_15     = 10;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_MERGE_16     = 11;
    localparam                          STATE_MV_DERIVE_COMPARE_MVS       = 12;
    localparam                          STATE_MV_DERIVE_ZERO_MERGE            = 13;
    localparam                          STATE_MV_DERIVE_ADD_ZEROS_AMVP           = 14;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_9 = 16;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_10 = 17;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_11 = 18;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_12 = 19;
    localparam                          STATE_MV_DERIVE_MV_AVAILABLE_AMVP_13 = 20;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP14   = 21;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP15   = 22;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP16   = 23;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP17   = 24;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP18   = 25;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP19   = 26;
    localparam                          STATE_MV_DERIVE_MV_PROCESS_AMVP20   = 27;
    localparam                          STATE_MV_DERIVE_COL_MV_WAIT         = 28;
    localparam                          STATE_MV_DERIVE_MVD_ADD_AMVP       = 29;
    localparam                          STATE_MV_DERIVE_SET_DUMMY_INTRA_MV = 31;
    localparam                          STATE_MV_DERIVE_SET_PRE_INTRA_MV    = 32;    
    localparam                          STATE_MV_DERIVE_DONE    = 33;
    localparam                          STATE_MV_DERIVE_CTU_DONE    = 34;
    localparam                          STATE_MV_DERIVE_SET_INTER_MV    = 35;
    localparam                          STATE_MV_DERIVE_CTU_DONE_WAIT    = 41;  
	
	
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS1       = 36;
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS2       = 37;
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS3       = 38;
    localparam                          STATE_MV_DERIVE_BI_PRED_CANDS4       = 39;
 
    wire [11:0] PIC_HEIGHT;// = 240;
    wire [11:0] PIC_WIDTH; //= 416;   
    assign PIC_HEIGHT = uut.pred_block.inter_top_block.pic_height;
    assign PIC_WIDTH = uut.pred_block.inter_top_block.pic_width;
    
	// Inputs
	reg enable;
	reg [31:0] fifo_in;
	reg input_fifo_is_empty;
	reg  [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0] y_residual_fifo_in;
	reg y_residual_fifo_is_empty_in;
	reg [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]  cb_residual_fifo_in;
	reg cb_residual_fifo_is_empty_in;
	reg [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]  cr_residual_fifo_in;
	reg cr_residual_fifo_is_empty_in;

    //luma dbf fifo interface
    reg                                                                       y_dbf_fifo_is_full;
    wire  [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE + 9 + 9 - 1:0]     y_dbf_fifo_data_out;
    wire                                                                      y_dbf_fifo_wr_en_out;
    
    //cb dbf fifo interface
    reg                                                                       cb_dbf_fifo_is_full;
    wire  [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]             cb_dbf_fifo_data_out;
    wire                                                                      cb_dbf_fifo_wr_en_out;
    
    //cr dbf fifo interface
    reg                                                                       cr_dbf_fifo_is_full;
    wire  [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]             cr_dbf_fifo_data_out;
    wire                                                                      cr_dbf_fifo_wr_en_out;


    
    
	// Outputs
	wire read_en_out;
	wire y_residual_fifo_read_en_out;
	wire cb_residual_fifo_read_en_out;
	wire cr_residual_fifo_read_en_out;


    
    wire new_poc = uut.pred_block.state == STATE_SEND_POC_TO_DBF;
    integer ctu_start_time;
    real poc_time;
    real clock_period_in_ps;
	wire [0:0] temp_port;

    reg [31:0] soft_file_loc;
    
    integer file_in,rfile;
    integer cache_in_file;
    integer return_value;
    reg [7:0] mem1,mem2, mem3,mem4;
    integer correct_res_added_file;
    


    integer file_location;
    wire new_ctu = (uut.pred_block.inter_top_block.mv_derive_block.mv_derive_state == STATE_MV_DERIVE_CTU_DONE_WAIT) && (uut.pred_block.inter_top_block.mv_derive_block.col_write_done_out);
    // wire new_ctu = ~(|uut.y_common_predsample_4by4_x_int[CTB_SIZE_WIDTH-1:0]) && (~(|uut.y_common_predsample_4by4_y_int[CTB_SIZE_WIDTH-1:0])) && uut.y_sao_fifo_wr_en_out;
    
    reg [15:0] soft_dpb_filled;

    integer lx_frac, ly_frac, ref_l_startx, ref_l_starty;
    integer ch_x_frac, ch_y_frac, ref_cb_startx, ref_cb_starty;
    integer frame_offset_pred_pixel;

    
    
    
    reg                                  test_mv_field_pred_flag_l0;
    reg                                  test_mv_field_pred_flag_l1;
    reg [REF_IDX_LX_WIDTH -1:0]          test_mv_field_ref_idx_l0;
    reg [REF_IDX_LX_WIDTH -1:0]          test_mv_field_ref_idx_l1;
    reg signed [MVD_WIDTH -1:0]          test_mv_field_mv_x_l0;
    reg signed [MVD_WIDTH -1:0]          test_mv_field_mv_y_l0;
    reg signed [MVD_WIDTH -1:0]          test_mv_field_mv_x_l1;
    reg signed [MVD_WIDTH -1:0]          test_mv_field_mv_y_l1;    

  
    
    
    
    wire       [PIXEL_WIDTH - 1:0]                                      y_res_added_int_arr[OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
    wire       [PIXEL_WIDTH - 1:0]                                      cb_res_added_int_arr[OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
    wire       [PIXEL_WIDTH - 1:0]                                      cr_res_added_int_arr[OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
    

    integer final_pixl_file;                                            
    integer pred_pixl_file;   
    
    integer y_file_in;
    integer cb_file_in;
    integer cr_file_in;   

   
    integer dpb_idx_cache_in;
    integer dpb_idx_cache_out;
    
    integer x_col_pu, y_col_pu;
    integer x_pref_mv_pu, y_pref_mv_pu,pref_dpb_idx;
    
    reg [7:0]   y_mem24,
                y_mem23,
                y_mem22,
                y_mem21,
                y_mem20,
                y_mem19,
                y_mem18,
                y_mem17,
                y_mem16,
                y_mem15,
                y_mem14,
                y_mem13,
                y_mem12,
                y_mem11,
                y_mem10,
                y_mem9,
                y_mem8,
                y_mem7,
                y_mem6,
                y_mem5,
                y_mem4,
                y_mem3,
                y_mem2,
                y_mem1;
    reg [9-1:0]   
                temp_y_mem16,
                temp_y_mem15,
                temp_y_mem14,
                temp_y_mem13,
                temp_y_mem12,
                temp_y_mem11,
                temp_y_mem10,
                temp_y_mem9,
                temp_y_mem8,
                temp_y_mem7,
                temp_y_mem6,
                temp_y_mem5,
                temp_y_mem4,
                temp_y_mem3,
                temp_y_mem2,
                temp_y_mem1;            
                
    
reg [7:0]   
                cb_mem24,
                cb_mem23,
                cb_mem22,
                cb_mem21,
                cb_mem20,
                cb_mem19,
                cb_mem18,
                cb_mem17,
                cb_mem16,
                cb_mem15,
                cb_mem14,
                cb_mem13,
                cb_mem12,
                cb_mem11,
                cb_mem10,
                cb_mem9,
                cb_mem8,
                cb_mem7,
                cb_mem6,
                cb_mem5,
                cb_mem4,
                cb_mem3,
                cb_mem2,
                cb_mem1;
    reg [9-1:0]   
                temp_cb_mem16,
                temp_cb_mem15,
                temp_cb_mem14,
                temp_cb_mem13,
                temp_cb_mem12,
                temp_cb_mem11,
                temp_cb_mem10,
                temp_cb_mem9,
                temp_cb_mem8,
                temp_cb_mem7,
                temp_cb_mem6,
                temp_cb_mem5,
                temp_cb_mem4,
                temp_cb_mem3,
                temp_cb_mem2,
                temp_cb_mem1; 

    reg [7:0]   
                cr_mem24,
                cr_mem23,
                cr_mem22,
                cr_mem21,
                cr_mem20,
                cr_mem19,
                cr_mem18,
                cr_mem17,
                cr_mem16,
                cr_mem15,
                cr_mem14,
                cr_mem13,
                cr_mem12,
                cr_mem11,
                cr_mem10,
                cr_mem9,
                cr_mem8,
                cr_mem7,
                cr_mem6,
                cr_mem5,
                cr_mem4,
                cr_mem3,
                cr_mem2,
                cr_mem1;
    reg [9-1:0]   
                temp_cr_mem16,
                temp_cr_mem15,
                temp_cr_mem14,
                temp_cr_mem13,
                temp_cr_mem12,
                temp_cr_mem11,
                temp_cr_mem10,
                temp_cr_mem9,
                temp_cr_mem8,
                temp_cr_mem7,
                temp_cr_mem6,
                temp_cr_mem5,
                temp_cr_mem4,
                temp_cr_mem3,
                temp_cr_mem2,
                temp_cr_mem1; 
                    
    
    
       wire cache_start = uut.pred_block.inter_top_block.pred_sample_gen_block.cache_block.state == 0;
 

//==========================================================================
  //***************************************************************************
   // Traffic Gen related parameters
   //***************************************************************************
   parameter SIMULATION            = "TRUE";
   parameter BL_WIDTH              = 10;
   parameter PORT_MODE             = "BI_MODE";
   parameter DATA_MODE             = 4'b0010;
   parameter ADDR_MODE             = 4'b0011;
   parameter TST_MEM_INSTR_MODE    = "R_W_INSTR_MODE";
   parameter EYE_TEST              = "FALSE";
                                     // set EYE_TEST = "TRUE" to probe memory
                                     // signals. Traffic Generator will only
                                     // write to one single location and no
                                     // read transactions will be generated.
   parameter DATA_PATTERN          = "DGEN_ALL";
                                      // For small devices, choose one only.
                                      // For large device, choose "DGEN_ALL"
                                      // "DGEN_HAMMER", "DGEN_WALKING1",
                                      // "DGEN_WALKING0","DGEN_ADDR","
                                      // "DGEN_NEIGHBOR","DGEN_PRBS","DGEN_ALL"
   parameter CMD_PATTERN           = "CGEN_ALL";
                                      // "CGEN_PRBS","CGEN_FIXED","CGEN_BRAM",
                                      // "CGEN_SEQUENTIAL", "CGEN_ALL"
   parameter BEGIN_ADDRESS         = 32'h00000000;
   parameter END_ADDRESS           = 32'h00000fff;
   parameter PRBS_EADDR_MASK_POS   = 32'hff000000;
   parameter SEL_VICTIM_LINE       = 11;

   //***************************************************************************
   // The following parameters refer to width of various ports
   //***************************************************************************
   parameter BANK_WIDTH            = 3;
                                     // # of memory Bank Address bits.
   parameter CK_WIDTH              = 1;
                                     // # of CK/CK# outputs to memory.
   parameter COL_WIDTH             = 10;
                                     // # of memory Column Address bits.
   parameter CS_WIDTH              = 1;
                                     // # of unique CS outputs to memory.
   parameter nCS_PER_RANK          = 1;
                                     // # of unique CS outputs per rank for phy
   parameter CKE_WIDTH             = 1;
                                     // # of CKE outputs to memory.
   parameter DATA_BUF_ADDR_WIDTH   = 5;
   parameter DQ_CNT_WIDTH          = 6;
                                     // = ceil(log2(DQ_WIDTH))
   parameter DQ_PER_DM             = 8;
   parameter DM_WIDTH              = 8;
                                     // # of DM (data mask)
   parameter DQ_WIDTH              = 64;
                                     // # of DQ (data)
   parameter DQS_WIDTH             = 8;
   parameter DQS_CNT_WIDTH         = 3;
                                     // = ceil(log2(DQS_WIDTH))
   parameter DRAM_WIDTH            = 8;
                                     // # of DQ per DQS
   parameter ECC                   = "OFF";
   parameter nBANK_MACHS           = 4;
   parameter RANKS                 = 1;
                                     // # of Ranks.
   parameter ODT_WIDTH             = 1;
                                     // # of ODT outputs to memory.
   parameter ROW_WIDTH             = 14;
                                     // # of memory Row Address bits.
   parameter ADDR_WIDTH            = 28;
                                     // # = RANK_WIDTH + BANK_WIDTH
                                     //     + ROW_WIDTH + COL_WIDTH;
                                     // Chip Select is always tied to low for
                                     // single rank devices
   parameter USE_CS_PORT          = 1;
                                     // # = 1, When CS output is enabled
                                     //   = 0, When CS output is disabled
                                     // If CS_N disabled, user must connect
                                     // DRAM CS_N input(s) to ground
   parameter USE_DM_PORT           = 1;
                                     // # = 1, When Data Mask option is enabled
                                     //   = 0, When Data Mask option is disbaled
                                     // When Data Mask option is disabled in
                                     // MIG Controller Options page, the logic
                                     // related to Data Mask should not get
                                     // synthesized
   parameter USE_ODT_PORT          = 1;
                                     // # = 1, When ODT output is enabled
                                     //   = 0, When ODT output is disabled
                                     // Parameter configuration for Dynamic ODT support:
                                     // USE_ODT_PORT = 0, RTT_NOM = "DISABLED", RTT_WR = "60/120".
                                     // This configuration allows to save ODT pin mapping from FPGA.
                                     // The user can tie the ODT input of DRAM to HIGH.

   //***************************************************************************
   // The following parameters are mode register settings
   //***************************************************************************
   parameter AL                    = "0";
                                     // DDR3 SDRAM:
                                     // Additive Latency (Mode Register 1).
                                     // # = "0", "CL-1", "CL-2".
                                     // DDR2 SDRAM:
                                     // Additive Latency (Extended Mode Register).
   parameter nAL                   = 0;
                                     // # Additive Latency in number of clock
                                     // cycles.
   parameter BURST_MODE            = "8";
                                     // DDR3 SDRAM:
                                     // Burst Length (Mode Register 0).
                                     // # = "8", "4", "OTF".
                                     // DDR2 SDRAM:
                                     // Burst Length (Mode Register).
                                     // # = "8", "4".
   parameter BURST_TYPE            = "SEQ";
                                     // DDR3 SDRAM: Burst Type (Mode Register 0).
                                     // DDR2 SDRAM: Burst Type (Mode Register).
                                     // # = "SEQ" - (Sequential),
                                     //   = "INT" - (Interleaved).
   parameter CL                    = 9;
                                     // in number of clock cycles
                                     // DDR3 SDRAM: CAS Latency (Mode Register 0).
                                     // DDR2 SDRAM: CAS Latency (Mode Register).
   parameter CWL                   = 7;
                                     // in number of clock cycles
                                     // DDR3 SDRAM: CAS Write Latency (Mode Register 2).
                                     // DDR2 SDRAM: Can be ignored
   parameter OUTPUT_DRV            = "HIGH";
                                     // Output Driver Impedance Control (Mode Register 1).
                                     // # = "HIGH" - RZQ/7,
                                     //   = "LOW" - RZQ/6.
   parameter RTT_NOM               = "40";
                                     // RTT_NOM (ODT) (Mode Register 1).
                                     // # = "DISABLED" - RTT_NOM disabled,
                                     //   = "120" - RZQ/2,
                                     //   = "60"  - RZQ/4,
                                     //   = "40"  - RZQ/6.
   parameter RTT_WR                = "OFF";
                                     // RTT_WR (ODT) (Mode Register 2).
                                     // # = "OFF" - Dynamic ODT off,
                                     //   = "120" - RZQ/2,
                                     //   = "60"  - RZQ/4,
   parameter ADDR_CMD_MODE         = "1T" ;
                                     // # = "1T", "2T".
   parameter REG_CTRL              = "OFF";
                                     // # = "ON" - RDIMMs,
                                     //   = "OFF" - Components, SODIMMs, UDIMMs.
   parameter CA_MIRROR             = "OFF";
                                     // C/A mirror opt for DDR3 dual rank
   
   //***************************************************************************
   // The following parameters are multiplier and divisor factors for PLLE2.
   // Based on the selected design frequency these parameters vary.
   //***************************************************************************
   parameter CLKIN_PERIOD          = 4998;
                                     // Input Clock Period
   parameter CLKFBOUT_MULT         = 6;
                                     // write PLL VCO multiplier
   parameter DIVCLK_DIVIDE         = 1;
                                     // write PLL VCO divisor
   parameter CLKOUT0_DIVIDE        = 2;
                                     // VCO output divisor for PLL output clock (CLKOUT0)
   parameter CLKOUT1_DIVIDE        = 2;
                                     // VCO output divisor for PLL output clock (CLKOUT1)
   parameter CLKOUT2_DIVIDE        = 32;
                                     // VCO output divisor for PLL output clock (CLKOUT2)
   parameter CLKOUT3_DIVIDE        = 8;
                                     // VCO output divisor for PLL output clock (CLKOUT3)

   //***************************************************************************
   // Memory Timing Parameters. These parameters varies based on the selected
   // memory part.
   //***************************************************************************
   parameter tCKE                  = 5000;
                                     // memory tCKE paramter in pS
   parameter tFAW                  = 30000;
                                     // memory tRAW paramter in pS.
   parameter tRAS                  = 35000;
                                     // memory tRAS paramter in pS.
   parameter tRCD                  = 13125;
                                     // memory tRCD paramter in pS.
   parameter tREFI                 = 7800000;
                                     // memory tREFI paramter in pS.
   parameter tRFC                  = 110000;
                                     // memory tRFC paramter in pS.
   parameter tRP                   = 13125;
                                     // memory tRP paramter in pS.
   parameter tRRD                  = 6000;
                                     // memory tRRD paramter in pS.
   parameter tRTP                  = 7500;
                                     // memory tRTP paramter in pS.
   parameter tWTR                  = 7500;
                                     // memory tWTR paramter in pS.
   parameter tZQI                  = 128_000_000;
                                     // memory tZQI paramter in nS.
   parameter tZQCS                 = 64;
                                     // memory tZQCS paramter in clock cycles.

   //***************************************************************************
   // Simulation parameters
   //***************************************************************************
   parameter SIM_BYPASS_INIT_CAL   = "FAST";
                                     // # = "SIM_INIT_CAL_FULL" -  Complete
                                     //              memory init &
                                     //              calibration sequence
                                     // # = "SKIP" - Not supported
                                     // # = "FAST" - Complete memory init & use
                                     //              abbreviated calib sequence

   //***************************************************************************
   // The following parameters varies based on the pin out entered in MIG GUI.
   // Do not change any of these parameters directly by editing the RTL.
   // Any changes required should be done through GUI and the design regenerated.
   //***************************************************************************
   parameter BYTE_LANES_B0         = 4'b1111;
                                     // Byte lanes used in an IO column.
   parameter BYTE_LANES_B1         = 4'b1110;
                                     // Byte lanes used in an IO column.
   parameter BYTE_LANES_B2         = 4'b1111;
                                     // Byte lanes used in an IO column.
   parameter BYTE_LANES_B3         = 4'b0000;
                                     // Byte lanes used in an IO column.
   parameter BYTE_LANES_B4         = 4'b0000;
                                     // Byte lanes used in an IO column.
   parameter DATA_CTL_B0           = 4'b1111;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
   parameter DATA_CTL_B1           = 4'b0000;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
   parameter DATA_CTL_B2           = 4'b1111;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
   parameter DATA_CTL_B3           = 4'b0000;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
   parameter DATA_CTL_B4           = 4'b0000;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
   parameter PHY_0_BITLANES        = 48'h3FE_1FF_1FF_2FF;
   parameter PHY_1_BITLANES        = 48'hFFE_F30_CB4_000;
   parameter PHY_2_BITLANES        = 48'h3FE_3FE_3BF_2FF;

   // control/address/data pin mapping parameters
   parameter CK_BYTE_MAP
     = 144'h00_00_00_00_00_00_00_00_00_00_00_00_00_00_00_00_00_11;
   parameter ADDR_MAP
     = 192'h000_000_132_136_135_133_139_124_131_129_137_134_13A_128_138_13B;
   parameter BANK_MAP   = 36'h125_12A_12B;
   parameter CAS_MAP    = 12'h115;
   parameter CKE_ODT_BYTE_MAP = 8'h00;
   parameter CKE_MAP    = 96'h000_000_000_000_000_000_000_117;
   parameter ODT_MAP    = 96'h000_000_000_000_000_000_000_112;
   parameter CS_MAP     = 120'h000_000_000_000_000_000_000_000_000_114;
   parameter PARITY_MAP = 12'h000;
   parameter RAS_MAP    = 12'h11A;
   parameter WE_MAP     = 12'h11B;
   parameter DQS_BYTE_MAP
     = 144'h00_00_00_00_00_00_00_00_00_00_20_21_22_23_03_02_01_00;
   parameter DATA0_MAP  = 96'h009_000_003_001_007_006_005_002;
   parameter DATA1_MAP  = 96'h014_018_010_011_017_016_012_013;
   parameter DATA2_MAP  = 96'h021_022_025_020_027_023_026_028;
   parameter DATA3_MAP  = 96'h033_039_031_035_032_038_034_037;
   parameter DATA4_MAP  = 96'h231_238_237_236_233_232_234_239;
   parameter DATA5_MAP  = 96'h226_227_225_229_221_222_224_228;
   parameter DATA6_MAP  = 96'h214_215_210_218_217_213_219_212;
   parameter DATA7_MAP  = 96'h207_203_204_206_202_201_205_209;
   parameter DATA8_MAP  = 96'h000_000_000_000_000_000_000_000;
   parameter DATA9_MAP  = 96'h000_000_000_000_000_000_000_000;
   parameter DATA10_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA11_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA12_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA13_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA14_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA15_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA16_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA17_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter MASK0_MAP  = 108'h000_200_211_223_235_036_024_015_004;
   parameter MASK1_MAP  = 108'h000_000_000_000_000_000_000_000_000;

   parameter SLOT_0_CONFIG         = 8'b0000_0001;
                                     // Mapping of Ranks.
   parameter SLOT_1_CONFIG         = 8'b0000_0000;
                                     // Mapping of Ranks.
   parameter MEM_ADDR_ORDER
     = "TG_TEST";

   //***************************************************************************
   // IODELAY and PHY related parameters
   //***************************************************************************
   parameter IODELAY_HP_MODE       = "ON";
                                     // to phy_top
   parameter IBUF_LPWR_MODE        = "OFF";
                                     // to phy_top
   parameter DATA_IO_IDLE_PWRDWN   = "OFF";
                                     // # = "ON", "OFF"
   parameter DATA_IO_PRIM_TYPE     = "DEFAULT";
                                     // # = "HP_LP", "HR_LP", "DEFAULT"
   parameter USER_REFRESH          = "OFF";
   parameter WRLVL                 = "ON";
                                     // # = "ON" - DDR3 SDRAM
                                     //   = "OFF" - DDR2 SDRAM.
   parameter ORDERING              = "NORM";
                                     // # = "NORM", "STRICT", "RELAXED".
   parameter CALIB_ROW_ADD         = 16'h0000;
                                     // Calibration row address will be used for
                                     // calibration read and write operations
   parameter CALIB_COL_ADD         = 12'h000;
                                     // Calibration column address will be used for
                                     // calibration read and write operations
   parameter CALIB_BA_ADD          = 3'h0;
                                     // Calibration bank address will be used for
                                     // calibration read and write operations
   parameter TCQ                   = 100;
   //***************************************************************************
   // IODELAY and PHY related parameters
   //***************************************************************************
   parameter IODELAY_GRP           = "IODELAY_MIG";
                                     // It is associated to a set of IODELAYs with
                                     // an IDELAYCTRL that have same IODELAY CONTROLLER
                                     // clock frequency.
   parameter SYSCLK_TYPE           = "DIFFERENTIAL";
                                     // System clock type DIFFERENTIAL, SINGLE_ENDED,
                                     // NO_BUFFER
   parameter REFCLK_TYPE           = "USE_SYSTEM_CLOCK";
                                     // Reference clock type DIFFERENTIAL, SINGLE_ENDED,
                                     // NO_BUFFER, USE_SYSTEM_CLOCK
   parameter RST_ACT_LOW           = 0;
                                     // =1 for active low reset,
                                     // =0 for active high.
   parameter CAL_WIDTH             = "HALF";
   parameter STARVE_LIMIT          = 2;
                                     // # = 2,3,4.

   //***************************************************************************
   // Referece clock frequency parameters
   //***************************************************************************
   parameter REFCLK_FREQ           = 200.0;
                                     // IODELAYCTRL reference clock frequency
   //***************************************************************************
   // System clock frequency parameters
   //***************************************************************************
   parameter tCK                   = 1666;
                                     // memory tCK paramter.
                     // # = Clock Period in pS.
   parameter nCK_PER_CLK           = 4;
                                     // # of memory CKs per fabric CLK

   
   //***************************************************************************
   // AXI4 Shim parameters
   //***************************************************************************
   parameter C_S_AXI_ID_WIDTH              = 4;
                                             // Width of all master and slave ID signals.
                                             // # = >= 1.
   parameter C_S_AXI_ADDR_WIDTH            = 32;
                                             // Width of S_AXI_AWADDR, S_AXI_ARADDR, M_AXI_AWADDR and
                                             // M_AXI_ARADDR for all SI/MI slots.
                                             // # = 32.
   parameter C_S_AXI_DATA_WIDTH            = AXI_MIG_DATA_WIDTH;
                                             // Width of WDATA and RDATA on SI slot.
                                             // Must be <= APP_DATA_WIDTH.
                                             // # = 32, 64, 128, 256.
   parameter C_MC_nCK_PER_CLK              = 4;
                                             // Indicates whether to instatiate upsizer
                                             // Range: 0, 1
   parameter C_S_AXI_SUPPORTS_NARROW_BURST = 1;
                                             // Indicates whether to instatiate upsizer
                                             // Range: 0, 1
   parameter C_RD_WR_ARB_ALGORITHM          = "RD_PRI_REG";
                                             // Indicates the Arbitration
                                             // Allowed values - "TDM", "ROUND_ROBIN",
                                             // "RD_PRI_REG", "RD_PRI_REG_STARVE_LIMIT"
   parameter C_S_AXI_REG_EN0               = 20'h00000;
                                             // C_S_AXI_REG_EN0[00] = Reserved
                                             // C_S_AXI_REG_EN0[04] = AW CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN0[05] =  W CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN0[06] =  B CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN0[07] =  R CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN0[08] = AW CHANNEL UPSIZER REGISTER SLICE
                                             // C_S_AXI_REG_EN0[09] =  W CHANNEL UPSIZER REGISTER SLICE
                                             // C_S_AXI_REG_EN0[10] = AR CHANNEL UPSIZER REGISTER SLICE
                                             // C_S_AXI_REG_EN0[11] =  R CHANNEL UPSIZER REGISTER SLICE
   parameter C_S_AXI_REG_EN1                 = 20'h00000;
                                             // Instatiates register slices after the upsizer.
                                             // The type of register is specified for each channel
                                             // in a vector. 4 bits per channel are used.
                                             // C_S_AXI_REG_EN1[03:00] = AW CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN1[07:04] =  W CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN1[11:08] =  B CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN1[15:12] = AR CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN1[20:16] =  R CHANNEL REGISTER SLICE
                                             // Possible values for each channel are:
                                             //
                                             //   0 => BYPASS    = The channel is just wired through the
                                             //                    module.
                                             //   1 => FWD       = The master VALID and payload signals
                                             //                    are registrated.
                                             //   2 => REV       = The slave ready signal is registrated
                                             //   3 => FWD_REV   = Both FWD and REV
                                             //   4 => SLAVE_FWD = All slave side signals and master
                                             //                    VALID and payload are registrated.
                                             //   5 => SLAVE_RDY = All slave side signals and master
                                             //                    READY are registrated.
                                             //   6 => INPUTS    = Slave and Master side inputs are
                                             //                    registrated.
                                             //   7 => ADDRESS   = Optimized for address channel

   //***************************************************************************
   // Debug and Internal parameters
   //***************************************************************************
   parameter DEBUG_PORT            = "OFF";
                                     // # = "ON" Enable debug signals/controls.
                                     //   = "OFF" Disable debug signals/controls.
   //***************************************************************************
   // Debug and Internal parameters
   //***************************************************************************
   parameter DRAM_TYPE             = "DDR3";

    

  //**************************************************************************//
  // Local parameters Declarations
  //**************************************************************************//

  localparam real TPROP_DQS          = 0.00;
                                       // Delay for DQS signal during Write Operation
  localparam real TPROP_DQS_RD       = 0.00;
                       // Delay for DQS signal during Read Operation
  localparam real TPROP_PCB_CTRL     = 0.00;
                       // Delay for Address and Ctrl signals
  localparam real TPROP_PCB_DATA     = 0.00;
                       // Delay for data signal during Write operation
  localparam real TPROP_PCB_DATA_RD  = 0.00;
                       // Delay for data signal during Read operation

  localparam MEMORY_WIDTH            = 8;
  localparam NUM_COMP                = DQ_WIDTH/MEMORY_WIDTH;

  localparam real REFCLK_PERIOD = (1000000.0/(2*REFCLK_FREQ));
  localparam RESET_PERIOD = 2000000; //in pSec  
  localparam real SYSCLK_PERIOD = tCK;
    
  //**************************************************************************//
  // Wire Declarations
  //**************************************************************************//
  reg                                sys_rst_n;
  wire                               sys_rst;    
  
  reg                     sys_clk_i;
  wire                               sys_clk_p;
  wire                               sys_clk_n;
  
 reg clk_ref_i;
//---------------------------------------------------------------------------- 

  wire                               ddr3_reset_n;
  wire [DQ_WIDTH-1:0]                ddr3_dq_fpga;
  wire [DQS_WIDTH-1:0]               ddr3_dqs_p_fpga;
  wire [DQS_WIDTH-1:0]               ddr3_dqs_n_fpga;
  wire [ROW_WIDTH-1:0]               ddr3_addr_fpga;
  wire [BANK_WIDTH-1:0]              ddr3_ba_fpga;
  wire                               ddr3_ras_n_fpga;
  wire                               ddr3_cas_n_fpga;
  wire                               ddr3_we_n_fpga;
  wire [CKE_WIDTH-1:0]               ddr3_cke_fpga;
  wire [CK_WIDTH-1:0]                ddr3_ck_p_fpga;
  wire [CK_WIDTH-1:0]                ddr3_ck_n_fpga;
    
  
  wire                               init_calib_complete;
  wire [(CS_WIDTH*nCS_PER_RANK)-1:0] ddr3_cs_n_fpga;
    
  wire [DM_WIDTH-1:0]                ddr3_dm_fpga;
    
  wire [ODT_WIDTH-1:0]               ddr3_odt_fpga;
    
  
  reg [(CS_WIDTH*nCS_PER_RANK)-1:0] ddr3_cs_n_sdram_tmp;
    
  reg [DM_WIDTH-1:0]                 ddr3_dm_sdram_tmp;
    
  reg [ODT_WIDTH-1:0]                ddr3_odt_sdram_tmp;
    

  
  wire [DQ_WIDTH-1:0]                ddr3_dq_sdram;
  reg [ROW_WIDTH-1:0]                ddr3_addr_sdram [0:1];
  reg [BANK_WIDTH-1:0]               ddr3_ba_sdram [0:1];
  reg                                ddr3_ras_n_sdram;
  reg                                ddr3_cas_n_sdram;
  reg                                ddr3_we_n_sdram;
  wire [(CS_WIDTH*nCS_PER_RANK)-1:0] ddr3_cs_n_sdram;
  wire [ODT_WIDTH-1:0]               ddr3_odt_sdram;
  reg [CKE_WIDTH-1:0]                ddr3_cke_sdram;
  wire [DM_WIDTH-1:0]                ddr3_dm_sdram;
  wire [DQS_WIDTH-1:0]               ddr3_dqs_p_sdram;
  wire [DQS_WIDTH-1:0]               ddr3_dqs_n_sdram;
  reg [CK_WIDTH-1:0]                 ddr3_ck_p_sdram;
  reg [CK_WIDTH-1:0]                 ddr3_ck_n_sdram;
  
    

//**************************************************************************//

  //**************************************************************************//
  // Reset Generation
  //**************************************************************************//
  initial begin
    sys_rst_n = 1'b0;
    #RESET_PERIOD
      sys_rst_n = 1'b1;
   end
  assign sys_rst = RST_ACT_LOW ? sys_rst_n : ~sys_rst_n;

  //**************************************************************************//
  // Clock Generation
  //**************************************************************************//

  initial
    sys_clk_i = 1'b0;
  always
    sys_clk_i = #(CLKIN_PERIOD/2.0) ~sys_clk_i;

  assign sys_clk_n = ~sys_clk_i;
  
  wire g_clk = sys_clk_i;
  assign sys_clk_p = sys_clk_i;

  initial
    clk_ref_i = 1'b0;
  always
    clk_ref_i = #REFCLK_PERIOD ~clk_ref_i;



  always @( * ) begin
    ddr3_ck_p_sdram      <=  #(TPROP_PCB_CTRL) ddr3_ck_p_fpga;
    ddr3_ck_n_sdram      <=  #(TPROP_PCB_CTRL) ddr3_ck_n_fpga;
    ddr3_addr_sdram[0]   <=  #(TPROP_PCB_CTRL) ddr3_addr_fpga;
    ddr3_addr_sdram[1]   <=  #(TPROP_PCB_CTRL) (CA_MIRROR == "ON") ?
                                                 {ddr3_addr_fpga[ROW_WIDTH-1:9],
                                                  ddr3_addr_fpga[7], ddr3_addr_fpga[8],
                                                  ddr3_addr_fpga[5], ddr3_addr_fpga[6],
                                                  ddr3_addr_fpga[3], ddr3_addr_fpga[4],
                                                  ddr3_addr_fpga[2:0]} :
                                                 ddr3_addr_fpga;
    ddr3_ba_sdram[0]     <=  #(TPROP_PCB_CTRL) ddr3_ba_fpga;
    ddr3_ba_sdram[1]     <=  #(TPROP_PCB_CTRL) (CA_MIRROR == "ON") ?
                                                 {ddr3_ba_fpga[BANK_WIDTH-1:2],
                                                  ddr3_ba_fpga[0],
                                                  ddr3_ba_fpga[1]} :
                                                 ddr3_ba_fpga;
    ddr3_ras_n_sdram     <=  #(TPROP_PCB_CTRL) ddr3_ras_n_fpga;
    ddr3_cas_n_sdram     <=  #(TPROP_PCB_CTRL) ddr3_cas_n_fpga;
    ddr3_we_n_sdram      <=  #(TPROP_PCB_CTRL) ddr3_we_n_fpga;
    ddr3_cke_sdram       <=  #(TPROP_PCB_CTRL) ddr3_cke_fpga;
  end
    

  always @( * )
    ddr3_cs_n_sdram_tmp   <=  #(TPROP_PCB_CTRL) ddr3_cs_n_fpga;
  assign ddr3_cs_n_sdram =  ddr3_cs_n_sdram_tmp;
    

  always @( * )
    ddr3_dm_sdram_tmp <=  #(TPROP_PCB_DATA) ddr3_dm_fpga;//DM signal generation
  assign ddr3_dm_sdram = ddr3_dm_sdram_tmp;
    

  always @( * )
    ddr3_odt_sdram_tmp  <=  #(TPROP_PCB_CTRL) ddr3_odt_fpga;
  assign ddr3_odt_sdram =  ddr3_odt_sdram_tmp;
    

// Controlling the bi-directional BUS

  genvar dqwd;
  generate
    for (dqwd = 1;dqwd < DQ_WIDTH;dqwd = dqwd+1) begin : dq_delay
      WireDelay #
       (
        .Delay_g    (TPROP_PCB_DATA),
        .Delay_rd   (TPROP_PCB_DATA_RD),
        .ERR_INSERT ("OFF")
       )
      u_delay_dq
       (
        .A             (ddr3_dq_fpga[dqwd]),
        .B             (ddr3_dq_sdram[dqwd]),
        .reset         (sys_rst_n),
        .phy_init_done (init_calib_complete)
       );
    end
    // For ECC ON case error is inserted on LSB bit from DRAM to FPGA
          WireDelay #
       (
        .Delay_g    (TPROP_PCB_DATA),
        .Delay_rd   (TPROP_PCB_DATA_RD),
        .ERR_INSERT (ECC)
       )
      u_delay_dq_0
       (
        .A             (ddr3_dq_fpga[0]),
        .B             (ddr3_dq_sdram[0]),
        .reset         (sys_rst_n),
        .phy_init_done (init_calib_complete)
       );
  endgenerate

  genvar dqswd;
  generate
    for (dqswd = 0;dqswd < DQS_WIDTH;dqswd = dqswd+1) begin : dqs_delay
      WireDelay #
       (
        .Delay_g    (TPROP_DQS),
        .Delay_rd   (TPROP_DQS_RD),
        .ERR_INSERT ("OFF")
       )
      u_delay_dqs_p
       (
        .A             (ddr3_dqs_p_fpga[dqswd]),
        .B             (ddr3_dqs_p_sdram[dqswd]),
        .reset         (sys_rst_n),
        .phy_init_done (init_calib_complete)
       );

      WireDelay #
       (
        .Delay_g    (TPROP_DQS),
        .Delay_rd   (TPROP_DQS_RD),
        .ERR_INSERT ("OFF")
       )
      u_delay_dqs_n
       (
        .A             (ddr3_dqs_n_fpga[dqswd]),
        .B             (ddr3_dqs_n_sdram[dqswd]),
        .reset         (sys_rst_n),
        .phy_init_done (init_calib_complete)
       );
    end
  endgenerate
    

  
  
    inter_pred_iface inter_pred_iface_block();
	// Instantiate the Unit Under Test (UUT)
	pred_dbf_wrapper #
	(
     .SIMULATION                (SIMULATION),
     .BL_WIDTH                  (BL_WIDTH),
     .PORT_MODE                 (PORT_MODE),
     .DATA_MODE                 (DATA_MODE),
     .ADDR_MODE                 (ADDR_MODE),
     .TST_MEM_INSTR_MODE        (TST_MEM_INSTR_MODE),
     .EYE_TEST                  (EYE_TEST),
     .DATA_PATTERN              (DATA_PATTERN),
     .CMD_PATTERN               (CMD_PATTERN),
     .BEGIN_ADDRESS             (BEGIN_ADDRESS),
     .END_ADDRESS               (END_ADDRESS),
     .PRBS_EADDR_MASK_POS       (PRBS_EADDR_MASK_POS),
     .SEL_VICTIM_LINE           (SEL_VICTIM_LINE),

     .BANK_WIDTH                (BANK_WIDTH),
     .CK_WIDTH                  (CK_WIDTH),
     .COL_WIDTH                 (COL_WIDTH),
     .CS_WIDTH                  (CS_WIDTH),
     .nCS_PER_RANK              (nCS_PER_RANK),
     .CKE_WIDTH                 (CKE_WIDTH),
     .DATA_BUF_ADDR_WIDTH       (DATA_BUF_ADDR_WIDTH),
     .DQ_CNT_WIDTH              (DQ_CNT_WIDTH),
     .DQ_PER_DM                 (DQ_PER_DM),
     .DM_WIDTH                  (DM_WIDTH),
    
     .DQ_WIDTH                  (DQ_WIDTH),
     .DQS_WIDTH                 (DQS_WIDTH),
     .DQS_CNT_WIDTH             (DQS_CNT_WIDTH),
     .DRAM_WIDTH                (DRAM_WIDTH),
     .ECC                       (ECC),
     .nBANK_MACHS               (nBANK_MACHS),
     .RANKS                     (RANKS),
     .ODT_WIDTH                 (ODT_WIDTH),
     .ROW_WIDTH                 (ROW_WIDTH),
     .ADDR_WIDTH                (ADDR_WIDTH),
     .USE_CS_PORT               (USE_CS_PORT),
     .USE_DM_PORT               (USE_DM_PORT),
     .USE_ODT_PORT              (USE_ODT_PORT),

     .AL                        (AL),
     .nAL                       (nAL),
     .BURST_MODE                (BURST_MODE),
     .BURST_TYPE                (BURST_TYPE),
     .CL                        (CL),
     .CWL                       (CWL),
     .OUTPUT_DRV                (OUTPUT_DRV),
     .RTT_NOM                   (RTT_NOM),
     .RTT_WR                    (RTT_WR),
     .ADDR_CMD_MODE             (ADDR_CMD_MODE),
     .REG_CTRL                  (REG_CTRL),
     .CA_MIRROR                 (CA_MIRROR),


     .CLKIN_PERIOD              (CLKIN_PERIOD),
     .CLKFBOUT_MULT             (CLKFBOUT_MULT),
     .DIVCLK_DIVIDE             (DIVCLK_DIVIDE),
     .CLKOUT0_DIVIDE            (CLKOUT0_DIVIDE),
     .CLKOUT1_DIVIDE            (CLKOUT1_DIVIDE),
     .CLKOUT2_DIVIDE            (CLKOUT2_DIVIDE),
     .CLKOUT3_DIVIDE            (CLKOUT3_DIVIDE),
    

     .tCKE                      (tCKE),
     .tFAW                      (tFAW),
     .tRAS                      (tRAS),
     .tRCD                      (tRCD),
     .tREFI                     (tREFI),
     .tRFC                      (tRFC),
     .tRP                       (tRP),
     .tRRD                      (tRRD),
     .tRTP                      (tRTP),
     .tWTR                      (tWTR),
     .tZQI                      (tZQI),
     .tZQCS                     (tZQCS),

     .SIM_BYPASS_INIT_CAL       (SIM_BYPASS_INIT_CAL),

     .BYTE_LANES_B0             (BYTE_LANES_B0),
     .BYTE_LANES_B1             (BYTE_LANES_B1),
     .BYTE_LANES_B2             (BYTE_LANES_B2),
     .BYTE_LANES_B3             (BYTE_LANES_B3),
     .BYTE_LANES_B4             (BYTE_LANES_B4),
     .DATA_CTL_B0               (DATA_CTL_B0),
     .DATA_CTL_B1               (DATA_CTL_B1),
     .DATA_CTL_B2               (DATA_CTL_B2),
     .DATA_CTL_B3               (DATA_CTL_B3),
     .DATA_CTL_B4               (DATA_CTL_B4),
     .PHY_0_BITLANES            (PHY_0_BITLANES),
     .PHY_1_BITLANES            (PHY_1_BITLANES),
     .PHY_2_BITLANES            (PHY_2_BITLANES),
     .CK_BYTE_MAP               (CK_BYTE_MAP),
     .ADDR_MAP                  (ADDR_MAP),
     .BANK_MAP                  (BANK_MAP),
     .CAS_MAP                   (CAS_MAP),
     .CKE_ODT_BYTE_MAP          (CKE_ODT_BYTE_MAP),
     .CKE_MAP                   (CKE_MAP),
     .ODT_MAP                   (ODT_MAP),
     .CS_MAP                    (CS_MAP),
     .PARITY_MAP                (PARITY_MAP),
     .RAS_MAP                   (RAS_MAP),
     .WE_MAP                    (WE_MAP),
     .DQS_BYTE_MAP              (DQS_BYTE_MAP),
     .DATA0_MAP                 (DATA0_MAP),
     .DATA1_MAP                 (DATA1_MAP),
     .DATA2_MAP                 (DATA2_MAP),
     .DATA3_MAP                 (DATA3_MAP),
     .DATA4_MAP                 (DATA4_MAP),
     .DATA5_MAP                 (DATA5_MAP),
     .DATA6_MAP                 (DATA6_MAP),
     .DATA7_MAP                 (DATA7_MAP),
     .DATA8_MAP                 (DATA8_MAP),
     .DATA9_MAP                 (DATA9_MAP),
     .DATA10_MAP                (DATA10_MAP),
     .DATA11_MAP                (DATA11_MAP),
     .DATA12_MAP                (DATA12_MAP),
     .DATA13_MAP                (DATA13_MAP),
     .DATA14_MAP                (DATA14_MAP),
     .DATA15_MAP                (DATA15_MAP),
     .DATA16_MAP                (DATA16_MAP),
     .DATA17_MAP                (DATA17_MAP),
     .MASK0_MAP                 (MASK0_MAP),
     .MASK1_MAP                 (MASK1_MAP),
     .SLOT_0_CONFIG             (SLOT_0_CONFIG),
     .SLOT_1_CONFIG             (SLOT_1_CONFIG),
     .MEM_ADDR_ORDER            (MEM_ADDR_ORDER),

     .IODELAY_HP_MODE           (IODELAY_HP_MODE),
     .IBUF_LPWR_MODE            (IBUF_LPWR_MODE),
     .DATA_IO_IDLE_PWRDWN       (DATA_IO_IDLE_PWRDWN),
     .DATA_IO_PRIM_TYPE         (DATA_IO_PRIM_TYPE),
     .USER_REFRESH              (USER_REFRESH),
     .WRLVL                     (WRLVL),
     .ORDERING                  (ORDERING),
     .CALIB_ROW_ADD             (CALIB_ROW_ADD),
     .CALIB_COL_ADD             (CALIB_COL_ADD),
     .CALIB_BA_ADD              (CALIB_BA_ADD),
     .TCQ                       (TCQ),

     
    .IODELAY_GRP               (IODELAY_GRP),
    .SYSCLK_TYPE               (SYSCLK_TYPE),
    .REFCLK_TYPE               (REFCLK_TYPE),
    .DRAM_TYPE                 (DRAM_TYPE),
    .CAL_WIDTH                 (CAL_WIDTH),
    .STARVE_LIMIT              (STARVE_LIMIT),
    
     
    .REFCLK_FREQ               (REFCLK_FREQ),
    
     
    .tCK                       (tCK),
    .nCK_PER_CLK               (nCK_PER_CLK),
    
     
     .C_S_AXI_ID_WIDTH          (C_S_AXI_ID_WIDTH),
     .C_S_AXI_ADDR_WIDTH        (C_S_AXI_ADDR_WIDTH),
     .C_S_AXI_DATA_WIDTH        (C_S_AXI_DATA_WIDTH),
     .C_MC_nCK_PER_CLK          (C_MC_nCK_PER_CLK),
     .C_S_AXI_SUPPORTS_NARROW_BURST (C_S_AXI_SUPPORTS_NARROW_BURST),
     .C_RD_WR_ARB_ALGORITHM      (C_RD_WR_ARB_ALGORITHM),
     .C_S_AXI_REG_EN0           (C_S_AXI_REG_EN0),
     .C_S_AXI_REG_EN1           (C_S_AXI_REG_EN1),
    
     .DEBUG_PORT                (DEBUG_PORT),
    
     .RST_ACT_LOW               (RST_ACT_LOW)	
	)
	
	uut (
	
`ifndef SOFT_MEM_SLAVE
		.ddr3_dq              (ddr3_dq_fpga),
		.ddr3_dqs_n           (ddr3_dqs_n_fpga),
		.ddr3_dqs_p           (ddr3_dqs_p_fpga),

		.ddr3_addr            (ddr3_addr_fpga),
		.ddr3_ba              (ddr3_ba_fpga),
		.ddr3_ras_n           (ddr3_ras_n_fpga),
		.ddr3_cas_n           (ddr3_cas_n_fpga),
		.ddr3_we_n            (ddr3_we_n_fpga),
		.ddr3_reset_n         (ddr3_reset_n),
		.ddr3_ck_p            (ddr3_ck_p_fpga),
		.ddr3_ck_n            (ddr3_ck_n_fpga),
		.ddr3_cke             (ddr3_cke_fpga),
		.ddr3_cs_n            (ddr3_cs_n_fpga),

		.ddr3_dm              (ddr3_dm_fpga),

		.ddr3_odt             (ddr3_odt_fpga),


		.sys_clk_p            (sys_clk_p),
		.sys_clk_n            (sys_clk_n),


		
		.sys_rst             (sys_rst),

`endif  
		.init_calib_complete (init_calib_complete),
		.enable(enable), 
		.fifo_in(fifo_in), 
		.input_fifo_is_empty(input_fifo_is_empty), 
		.read_en_out(read_en_out), 
		.y_residual_fifo_in(y_residual_fifo_in), 
		.y_residual_fifo_is_empty_in(y_residual_fifo_is_empty_in), 
		.y_residual_fifo_read_en_out(y_residual_fifo_read_en_out), 
		.cb_residual_fifo_in(cb_residual_fifo_in), 
		.cb_residual_fifo_is_empty_in(cb_residual_fifo_is_empty_in), 
		.cb_residual_fifo_read_en_out(cb_residual_fifo_read_en_out), 
		.cr_residual_fifo_in(cr_residual_fifo_in), 
		.cr_residual_fifo_is_empty_in(cr_residual_fifo_is_empty_in), 
		.cr_residual_fifo_read_en_out(cr_residual_fifo_read_en_out)
        
      
	);

	
	
	
`ifndef SOFT_MEM_SLAVE
  //**************************************************************************//
  // Memory Models instantiations
  //**************************************************************************//

  genvar r,ii;
  generate
    for (r = 0; r < CS_WIDTH; r = r + 1) begin: mem_rnk
      for (ii = 0; ii < NUM_COMP; ii = ii + 1) begin: gen_mem
        ddr3_model u_comp_ddr3
          (
           .rst_n   (ddr3_reset_n),
           .ck      (ddr3_ck_p_sdram[(ii*MEMORY_WIDTH)/72]),
           .ck_n    (ddr3_ck_n_sdram[(ii*MEMORY_WIDTH)/72]),
           .cke     (ddr3_cke_sdram[((ii*MEMORY_WIDTH)/72)+(nCS_PER_RANK*r)]),
           .cs_n    (ddr3_cs_n_sdram[((ii*MEMORY_WIDTH)/72)+(nCS_PER_RANK*r)]),
           .ras_n   (ddr3_ras_n_sdram),
           .cas_n   (ddr3_cas_n_sdram),
           .we_n    (ddr3_we_n_sdram),
           .dm_tdqs (ddr3_dm_sdram[ii]),
           .ba      (ddr3_ba_sdram[r]),
           .addr    (ddr3_addr_sdram[r]),
           .dq      (ddr3_dq_sdram[MEMORY_WIDTH*(ii+1)-1:MEMORY_WIDTH*(ii)]),
           .dqs     (ddr3_dqs_p_sdram[ii]),
           .dqs_n   (ddr3_dqs_n_sdram[ii]),
           .tdqs_n  (),
           .odt     (ddr3_odt_sdram[((ii*MEMORY_WIDTH)/72)+(nCS_PER_RANK*r)])
           );
      end
    end
  endgenerate
`endif
	
	
	initial begin
		// Initialize Inputs
        ctu_start_time = 0;
        soft_file_loc = 0;
        if(SOFT_RUN) begin
            inter_pred_iface_block.sv_inter_pred_reset();
            inter_pred_iface_block.sv_inter_pred_step(soft_file_loc);
        end
        
        file_in = $fopen("residual_to_inter","rb");
        y_file_in = $fopen("residual_to_inter_Y","rb");
        cb_file_in = $fopen("residual_to_inter_Cb","rb");
        cr_file_in = $fopen("residual_to_inter_Cr","rb");
        
        correct_res_added_file = $fopen("BQSquare_416x240_60_qp37_416x240_8bit_pre-lp.yuv","rb");
        
        
        cache_in_file = $fopen("y_scn_dump.csv","rb");
        final_pixl_file = $fopen("BQSquare_416x240_60_qp37_416x240_8bit_final.yuv","rb");
        pred_pixl_file = $fopen("BQSquare_416x240_60_qp37_416x240_8bit_predicted.yuv","rb");
        


        mem1 = $fgetc( file_in); 
        mem2 = $fgetc( file_in); 
        mem3 = $fgetc( file_in); 
        mem4 = $fgetc( file_in); 
        fifo_in = {mem4,mem3,mem2,mem1};
        
    
        file_location = -1;
		enable = 1;
        // fifo_in = 0;
		input_fifo_is_empty = 1;
		y_residual_fifo_in = 144'd0;
		y_residual_fifo_is_empty_in = 0;
		cb_residual_fifo_in = 0;
		cb_residual_fifo_is_empty_in = 0;
		cr_residual_fifo_in = 0;
		cr_residual_fifo_is_empty_in = 0;
		y_dbf_fifo_is_full = 0;
		cb_dbf_fifo_is_full = 0;
		cr_dbf_fifo_is_full = 0;

		// Wait 100 ns for global reset to finish
		#100;
        @(posedge uut.clk);
        // reset = 0;
        input_fifo_is_empty = 0;
        read_y_res_fifo();
        read_cb_res_fifo();
        read_cr_res_fifo();
		// Add stimulus here

	end
//always #10 clk = ~clk;



always @(posedge uut.clk) begin
    if(uut.reset) begin
    end
    else begin
        if(read_en_out) begin
            if(file_location  >= soft_file_loc) begin
                if(SOFT_RUN) begin
                    //if($time >= 5146350) $stop;
                    inter_pred_iface_block.sv_inter_pred_step(soft_file_loc);
					
					if(soft_file_loc == -2) begin
						$display("%d, soft returned: %x",$time, soft_file_loc);
						$stop;
					end
                end
            end
            file_location <= file_location + 1;
            mem1 = $fgetc( file_in); 
            mem2 = $fgetc( file_in); 
            mem3 = $fgetc( file_in); 
            mem4 = $fgetc( file_in); 
            //$display("%d, header passed: %x",$time, mem1);
            fifo_in = #1 {mem4,mem3,mem2,mem1};
            //$display("data passed: %x %x %x",mem4, mem3, mem2);

        end
        // else begin
            // if(mv_status_d == 1 && mv_status == 0) begin
                // inter_pred_iface_block.sv_inter_pred_step(soft_file_loc);
            // end
        // end
    end
    
end




always@(posedge uut.clk) begin
    if(y_residual_fifo_read_en_out) begin
        read_y_res_fifo();
    end
    if(cb_residual_fifo_read_en_out) begin
        read_cb_res_fifo();
    end
    if(cr_residual_fifo_read_en_out) begin
        read_cr_res_fifo();
    end
end




always@(posedge uut.clk) begin
    if(uut.pred_block.state == 3) begin
        $display("STATE_ERROR");
        $stop();
    end

	
	/// all tasks here ////////////////////////////////////////////////////////
    ctu_count();
    //dpb_fill_verify();
    //verify_dpb_poc();
    ref_pic_list_verify();
    current_mv_verify();
    verify_luma_filter();
    //verify_ch_filter();
    //final_pred_pixl_check();
    col_mv_verify();
	pref_mv_verify();
end

task read_y_res_fifo;
    begin
        y_mem1 = $fgetc( y_file_in); 
        y_mem2 = $fgetc( y_file_in); 
        y_mem3 = $fgetc( y_file_in); 
        y_mem4 = $fgetc( y_file_in);

        {   temp_y_mem3,
            temp_y_mem2,
            temp_y_mem1 } = {y_mem4[2:0],y_mem3,y_mem2,y_mem1};
        
        y_mem5 = $fgetc( y_file_in); 
        y_mem6 = $fgetc( y_file_in); 
        y_mem7 =  $fgetc( y_file_in); 
        y_mem8 =  $fgetc( y_file_in);

        {   temp_y_mem6,
            temp_y_mem5,
            temp_y_mem4 } = {y_mem8[2:0],y_mem7,y_mem6,y_mem5};
        
        y_mem9 =  $fgetc( y_file_in); 
        y_mem10 =  $fgetc( y_file_in); 
        y_mem11 =  $fgetc( y_file_in); 
        y_mem12 =  $fgetc( y_file_in);

        {   temp_y_mem9,
            temp_y_mem8,
            temp_y_mem7 } = {y_mem12[2:0],y_mem11,y_mem10,y_mem9};
        
        y_mem13 =  $fgetc( y_file_in); 
        y_mem14 =  $fgetc( y_file_in); 
        y_mem15 =  $fgetc( y_file_in); 
        y_mem16 = $fgetc( y_file_in);

        {   temp_y_mem12,
            temp_y_mem11,
            temp_y_mem10 } = {y_mem16[2:0],y_mem15,y_mem14,y_mem13};
        
        y_mem17 = $fgetc( y_file_in); 
        y_mem18 = $fgetc( y_file_in); 
        y_mem19 = $fgetc( y_file_in); 
        y_mem20 = $fgetc( y_file_in); 

        {   temp_y_mem15,
            temp_y_mem14,
            temp_y_mem13 } = {y_mem20[2:0],y_mem19,y_mem18,y_mem17};

        y_mem21 = $fgetc( y_file_in); 
        y_mem22 = $fgetc( y_file_in); 
        y_mem23 = $fgetc( y_file_in); 
        y_mem24 = $fgetc( y_file_in); 

        temp_y_mem16 ={y_mem22[0],y_mem21};


        y_residual_fifo_in = {
                                    temp_y_mem16,
                                    temp_y_mem15,
                                    temp_y_mem14,
                                    temp_y_mem13,
                                    temp_y_mem12,
                                    temp_y_mem11,
                                    temp_y_mem10,
                                    temp_y_mem9,
                                    temp_y_mem8,
                                    temp_y_mem7,
                                    temp_y_mem6,
                                    temp_y_mem5,
                                    temp_y_mem4,
                                    temp_y_mem3,
                                    temp_y_mem2,
                                    temp_y_mem1
                            };
        
       // $stop();
        //$display("data passed: %x %x %x",mem4, mem3, mem2);

    end
endtask

task read_cb_res_fifo;
    begin
        cb_mem1 = $fgetc( cb_file_in); 
        cb_mem2 = $fgetc( cb_file_in); 
        cb_mem3 = $fgetc( cb_file_in); 
        cb_mem4 = $fgetc( cb_file_in);

        {   temp_cb_mem3,
            temp_cb_mem2,
            temp_cb_mem1 } = {cb_mem4[2:0],cb_mem3,cb_mem2,cb_mem1};
        
        cb_mem5 = $fgetc( cb_file_in); 
        cb_mem6 = $fgetc( cb_file_in); 
        cb_mem7 =  $fgetc( cb_file_in); 
        cb_mem8 =  $fgetc( cb_file_in);

        {   temp_cb_mem6,
            temp_cb_mem5,
            temp_cb_mem4 } = {cb_mem8[2:0],cb_mem7,cb_mem6,cb_mem5};
        
        cb_mem9 =  $fgetc( cb_file_in); 
        cb_mem10 =  $fgetc( cb_file_in); 
        cb_mem11 =  $fgetc( cb_file_in); 
        cb_mem12 =  $fgetc( cb_file_in);

        {   temp_cb_mem9,
            temp_cb_mem8,
            temp_cb_mem7 } = {cb_mem12[2:0],cb_mem11,cb_mem10,cb_mem9};
        
        cb_mem13 =  $fgetc( cb_file_in); 
        cb_mem14 =  $fgetc( cb_file_in); 
        cb_mem15 =  $fgetc( cb_file_in); 
        cb_mem16 = $fgetc( cb_file_in);

        {   temp_cb_mem12,
            temp_cb_mem11,
            temp_cb_mem10 } = {cb_mem16[2:0],cb_mem15,cb_mem14,cb_mem13};
        
        cb_mem17 = $fgetc( cb_file_in); 
        cb_mem18 = $fgetc( cb_file_in); 
        cb_mem19 = $fgetc( cb_file_in); 
        cb_mem20 = $fgetc( cb_file_in); 

        {   temp_cb_mem15,
            temp_cb_mem14,
            temp_cb_mem13 } = {cb_mem20[2:0],cb_mem19,cb_mem18,cb_mem17};

        cb_mem21 = $fgetc( cb_file_in); 
        cb_mem22 = $fgetc( cb_file_in); 
        cb_mem23 = $fgetc( cb_file_in); 
        cb_mem24 = $fgetc( cb_file_in); 

        temp_cb_mem16 ={cb_mem22[0],cb_mem21};


        cb_residual_fifo_in = {
                                    temp_cb_mem16,
                                    temp_cb_mem15,
                                    temp_cb_mem14,
                                    temp_cb_mem13,
                                    temp_cb_mem12,
                                    temp_cb_mem11,
                                    temp_cb_mem10,
                                    temp_cb_mem9,
                                    temp_cb_mem8,
                                    temp_cb_mem7,
                                    temp_cb_mem6,
                                    temp_cb_mem5,
                                    temp_cb_mem4,
                                    temp_cb_mem3,
                                    temp_cb_mem2,
                                    temp_cb_mem1
                            };
        
       // $stop();
        //$display("data passed: %x %x %x",mem4, mem3, mem2);

    end
endtask

task read_cr_res_fifo;
    begin
        cr_mem1 = $fgetc( cr_file_in); 
        cr_mem2 = $fgetc( cr_file_in); 
        cr_mem3 = $fgetc( cr_file_in); 
        cr_mem4 = $fgetc( cr_file_in);

        {   temp_cr_mem3,
            temp_cr_mem2,
            temp_cr_mem1 } = {cr_mem4[2:0],cr_mem3,cr_mem2,cr_mem1};
        
        cr_mem5 = $fgetc( cr_file_in); 
        cr_mem6 = $fgetc( cr_file_in); 
        cr_mem7 =  $fgetc( cr_file_in); 
        cr_mem8 =  $fgetc( cr_file_in);

        {   temp_cr_mem6,
            temp_cr_mem5,
            temp_cr_mem4 } = {cr_mem8[2:0],cr_mem7,cr_mem6,cr_mem5};
        
        cr_mem9 =  $fgetc( cr_file_in); 
        cr_mem10 =  $fgetc( cr_file_in); 
        cr_mem11 =  $fgetc( cr_file_in); 
        cr_mem12 =  $fgetc( cr_file_in);

        {   temp_cr_mem9,
            temp_cr_mem8,
            temp_cr_mem7 } = {cr_mem12[2:0],cr_mem11,cr_mem10,cr_mem9};
        
        cr_mem13 =  $fgetc( cr_file_in); 
        cr_mem14 =  $fgetc( cr_file_in); 
        cr_mem15 =  $fgetc( cr_file_in); 
        cr_mem16 = $fgetc( cr_file_in);

        {   temp_cr_mem12,
            temp_cr_mem11,
            temp_cr_mem10 } = {cr_mem16[2:0],cr_mem15,cr_mem14,cr_mem13};
        
        cr_mem17 = $fgetc( cr_file_in); 
        cr_mem18 = $fgetc( cr_file_in); 
        cr_mem19 = $fgetc( cr_file_in); 
        cr_mem20 = $fgetc( cr_file_in); 

        {   temp_cr_mem15,
            temp_cr_mem14,
            temp_cr_mem13 } = {cr_mem20[2:0],cr_mem19,cr_mem18,cr_mem17};

        cr_mem21 = $fgetc( cr_file_in); 
        cr_mem22 = $fgetc( cr_file_in); 
        cr_mem23 = $fgetc( cr_file_in); 
        cr_mem24 = $fgetc( cr_file_in); 

        temp_cr_mem16 ={cr_mem22[0],cr_mem21};


        cr_residual_fifo_in = {
                                    temp_cr_mem16,
                                    temp_cr_mem15,
                                    temp_cr_mem14,
                                    temp_cr_mem13,
                                    temp_cr_mem12,
                                    temp_cr_mem11,
                                    temp_cr_mem10,
                                    temp_cr_mem9,
                                    temp_cr_mem8,
                                    temp_cr_mem7,
                                    temp_cr_mem6,
                                    temp_cr_mem5,
                                    temp_cr_mem4,
                                    temp_cr_mem3,
                                    temp_cr_mem2,
                                    temp_cr_mem1
                            };
        
       // $stop();
        //$display("data passed: %x %x %x",mem4, mem3, mem2);

    end
endtask

generate
    genvar i;
    genvar j;
    
    for (i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin : row_iteration
        for (j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin : column_iteration
            assign  y_res_added_int_arr[i][j] =  y_dbf_fifo_data_out[i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE +(j+1)*PIXEL_WIDTH - 1:j*PIXEL_WIDTH + i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE];
            assign cb_res_added_int_arr[i][j] = cb_dbf_fifo_data_out[i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE +(j+1)*PIXEL_WIDTH - 1:j*PIXEL_WIDTH + i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE];
            assign cr_res_added_int_arr[i][j] = cr_dbf_fifo_data_out[i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE +(j+1)*PIXEL_WIDTH - 1:j*PIXEL_WIDTH + i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE];
        end
    end
endgenerate

task ctu_count;
begin

    if(new_ctu) begin
       $display("@ %d, ctu_time = %d x= %d, y=%d",$time,ctu_start_time, uut.pred_block.inter_top_block.mv_derive_block.ctb_xx, uut.pred_block.inter_top_block.mv_derive_block.ctb_yy);
       ctu_start_time = 0;
       if(uut.pred_block.pred_mode == `MODE_INTER) begin
       //$stop(); 
       end
    end
    else begin
      ctu_start_time = ctu_start_time + 1;
    end
	
	if(new_poc) begin
        $display("new poc %d cycles %d usec %f", uut.pred_block.current__poc,poc_time,(poc_time/1000000000)*(($time-clock_period_in_ps)));
        poc_time = 0;
    end
	else begin
		poc_time = poc_time + 1;
	end
	clock_period_in_ps = $time;
end
endtask
    
task verify_dpb_poc;
    integer i;
    reg [31:0] soft_dpb_poc;
    begin
        if(uut.pred_block.write_ref_pic_state == WRITE_REF_PIC_STATE_DONE) begin
            for(i=0;i< 16; i=i+1) begin
                inter_pred_iface_block.sv_get_dpb_poc(i,soft_dpb_poc);
                if(uut.pred_block.dpb_filled_flag_new[i] == 1) begin
                    if(soft_dpb_poc == uut.pred_block.dpb_block.mem[i]) begin
                        $display("dpb_poc ok");
                    end
                    else begin
                        $display("dpb_poc no ok soft %d hard %d idx %d",soft_dpb_poc,uut.pred_block.dpb_block.mem[i],i );
                        $stop();
                    end
                end
            end
        end
    end
endtask    
    
    
task dpb_fill_verify;
integer i;
begin
    for(i=0;i< 16; i=i+1) begin
        inter_pred_iface_block.sv_get_dpb_filled(i,soft_dpb_filled[i]);
    end
    if(uut.pred_block.write_ref_pic_state == WRITE_REF_PIC_STATE_DONE) begin
        if(uut.pred_block.dpb_filled_flag_new == soft_dpb_filled) begin
            $display("dpb ok");
        end
        else begin
            $display("dpb not ok");
            $stop();
        end
    end
end
endtask

task verify_luma_filter;
    integer i,j;
    integer quad_pred_pixl;
    integer x_ofset, y_ofset;
    begin
        if(uut.pred_block.inter_top_block.pred_sample_gen_block.cache_block.valid_in) begin
            dpb_idx_cache_in <= uut.pred_block.inter_top_block.pred_sample_gen_block.cache_block.ref_idx_in_in;
        end
        if(uut.pred_block.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.valid_in) begin
            dpb_idx_cache_out <= dpb_idx_cache_in;
        end
        if(uut.pred_block.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.block_ready_out) begin
            //$display("frac_x=%d, frac_y=%d",lx_frac,ly_frac);
            for(i=0;i<4;i=i+1)begin
                for(j=0;j<4;j=j+1) begin
                    case({lx_frac})
                        2'b00: begin
                            x_ofset = 0;   
                        end
                        2'b01,2'b10: begin
                            x_ofset = 3;
                        end
                        2'b11: begin
                            x_ofset = 2;
                        end
                    endcase
                    case({ly_frac})
                        2'b00: begin
                            y_ofset = 0;   
                        end
                        2'b01,2'b10: begin
                            y_ofset = 3;
                        end
                        2'b11: begin
                            y_ofset = 2;
                        end
                    endcase
                    inter_pred_iface_block.sv_get_frac_pixl(dpb_idx_cache_out,j+ref_l_startx+x_ofset,i+ref_l_starty+y_ofset,lx_frac,ly_frac,quad_pred_pixl);
                    if(quad_pred_pixl== $signed(uut.pred_block.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.out_store[i][j])) begin
                        //$display("%d luma filter out match @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",$time,uut.pred_block.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.xT_4x4_out,uut.pred_block.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.yT_4x4_out,j,i,quad_pred_pixl,uut.pred_block.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.out_store[i][j]);
                    end
                    else begin
                        $display("luma filter out mismatch @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",uut.pred_block.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.xT_4x4_out,uut.pred_block.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.yT_4x4_out,j,i,quad_pred_pixl,uut.pred_block.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.out_store[i][j]);
                        $stop();
                    end
                end
            end
        end
        if(uut.pred_block.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.valid_in) begin
            ref_l_startx = uut.pred_block.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.ref_l_start_x;
            ref_l_starty = uut.pred_block.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.ref_l_start_y;
            //ref_l_width = uut.pred_block.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.ref_l_width;
            //ref_l_hgt = uut.pred_block.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.ref_l_height;
            ly_frac = uut.pred_block.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.l_frac_y;
            lx_frac = uut.pred_block.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.l_frac_x;
        end
    end
endtask

task verify_ch_filter;
    integer i,j;
    integer quad_pred_pixl;
    integer x_ofset, y_ofset;
    begin
        if(uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.block_ready_out) begin
            $display("frac_x=%d, frac_y=%d",ch_x_frac,ch_y_frac);
            for(i=0;i<2;i=i+1)begin
                for(j=0;j<2;j=j+1) begin
                    case({ch_x_frac})
                        3'b000: begin
                            x_ofset = 0;   
                        end
                        default: begin
                            x_ofset = 1;
                        end
                    endcase
                    case({ch_y_frac})
                        3'b000: begin
                            y_ofset = 0;   
                        end
                        default:begin
                            y_ofset = 1;
                        end
                    endcase
                    if(i==0) begin
                        inter_pred_iface_block.sv_get_cb_frac_pixl(0,j+ref_cb_startx+x_ofset,i+ref_cb_starty+y_ofset,ch_x_frac,ch_y_frac,quad_pred_pixl);
                        if(quad_pred_pixl== $signed(uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.out_store_1r[j])) begin
                            $display("cb filter out match @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.xT_4x4_out,uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.yT_4x4_out,j,i,quad_pred_pixl,uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.out_store_1r[j]);
                        end
                        else begin
                            $display("cb filter out mismatch @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.xT_4x4_out,uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.yT_4x4_out,j,i,quad_pred_pixl,uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.out_store_1r[j]);
                            $stop();
                        end
                        inter_pred_iface_block.sv_get_cr_frac_pixl(0,j+ref_cb_startx+x_ofset,i+ref_cb_starty+y_ofset,ch_x_frac,ch_y_frac,quad_pred_pixl);
                        if(quad_pred_pixl== $signed(uut.pred_block.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.out_store_1r[j])) begin
                            $display("cr filter out match @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",uut.pred_block.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.xT_4x4_out,uut.pred_block.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.yT_4x4_out,j,i,quad_pred_pixl,uut.pred_block.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.out_store_1r[j]);
                        end
                        else begin
                            $display("cr filter out mismatch @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",uut.pred_block.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.xT_4x4_out,uut.pred_block.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.yT_4x4_out,j,i,quad_pred_pixl,uut.pred_block.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.out_store_1r[j]);
                            $stop();
                        end
                    end
                    else begin
                        inter_pred_iface_block.sv_get_cb_frac_pixl(0,j+ref_cb_startx+x_ofset,i+ref_cb_starty+y_ofset,ch_x_frac,ch_y_frac,quad_pred_pixl);
                        if(quad_pred_pixl== $signed(uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.out_store_2r[j])) begin
                            $display("cb filter out match @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.xT_4x4_out,uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.yT_4x4_out,j,i,quad_pred_pixl,uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.out_store_2r[j]);
                        end
                        else begin
                            $display("cb filter out mismatch @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.xT_4x4_out,uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.yT_4x4_out,j,i,quad_pred_pixl,uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.out_store_2r[j]);
                            $stop();
                        end
                        inter_pred_iface_block.sv_get_cr_frac_pixl(0,j+ref_cb_startx+x_ofset,i+ref_cb_starty+y_ofset,ch_x_frac,ch_y_frac,quad_pred_pixl);
                        if(quad_pred_pixl== $signed(uut.pred_block.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.out_store_2r[j])) begin
                            $display("cr filter out match @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",uut.pred_block.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.xT_4x4_out,uut.pred_block.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.yT_4x4_out,j,i,quad_pred_pixl,uut.pred_block.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.out_store_2r[j]);
                        end
                        else begin
                            $display("cr filter out mismatch @startx =%d, starty=%d, x=%d, y=%d, soft=%d, hard=%d",uut.pred_block.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.xT_4x4_out,uut.pred_block.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.yT_4x4_out,j,i,quad_pred_pixl,uut.pred_block.inter_top_block.pred_sample_gen_block.cr_filter_wrapper.out_store_2r[j]);
                            $stop();
                        end
                    end
                end
            end
        end
        if(uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.valid_in) begin
            ref_cb_startx = uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.ref_c_start_x;
            ref_cb_starty = uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.ref_c_start_y;
            //ref_l_width = uut.pred_block.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.ref_l_width;
            //ref_l_hgt = uut.pred_block.inter_top_block.pred_sample_gen_block.luma_filter_wrapper_block.ref_l_height;
            ch_y_frac = uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.c_frac_y;
            ch_x_frac = uut.pred_block.inter_top_block.pred_sample_gen_block.cb_filter_wrapper.c_frac_x;
        end
    end
endtask

task ref_pic_list_verify;
integer i;
integer ref_pic_list0_num_pic;
integer ref_pic_list1_num_pic;
integer ref_pic_list0_list;
integer ref_pic_list0_idx;
integer ref_pic_list1_list;
integer ref_pic_list1_idx;

begin
    if(uut.pred_block.write_ref_pic_state == WRITE_REF_PIC_STATE_DONE) begin
        inter_pred_iface_block.sv_get_ref_pic_list_num_pic(1,110,ref_pic_list0_num_pic);
        inter_pred_iface_block.sv_get_ref_pic_list_num_pic(0,110,ref_pic_list1_num_pic);
        for(i=0;i< 16; i=i+1) begin
            if(i < ref_pic_list0_num_pic) begin
                inter_pred_iface_block.sv_get_ref_pic_list_poc(i,0,ref_pic_list0_list);
                inter_pred_iface_block.sv_get_ref_pic_list_idx(i,0,ref_pic_list0_idx);
                if(ref_pic_list0_list == uut.pred_block.inter_top_block.mv_derive_block.ref_pic_list_0.mem[i] && ref_pic_list0_idx == uut.pred_block.inter_top_block.mv_derive_block.ref_pic_list0_dpb_idx.mem[i]) begin
                    
                end
                else begin
                    $display("ref pic list 0 not ok idx=%d, soft poc=%d, hard poc= %d, soft idx=%d, hard_idx=%d", i,ref_pic_list0_list,uut.pred_block.inter_top_block.mv_derive_block.ref_pic_list_0.mem[i],ref_pic_list0_idx,uut.pred_block.inter_top_block.mv_derive_block.ref_pic_list0_dpb_idx.mem[i]);
                    $stop();
                end
            end
            if(i < ref_pic_list1_num_pic) begin
                inter_pred_iface_block.sv_get_ref_pic_list_poc(i,1,ref_pic_list1_list);
                inter_pred_iface_block.sv_get_ref_pic_list_idx(i,1,ref_pic_list1_idx);
                if(ref_pic_list1_list == uut.pred_block.inter_top_block.mv_derive_block.ref_pic_list_1.mem[i] && ref_pic_list1_idx == uut.pred_block.inter_top_block.mv_derive_block.ref_pic_list1_dpb_idx.mem[i]) begin
                    
                end
                else begin
                    $display("ref pic list 1 not ok idx=%d, soft poc=%d, hard poc= %d, soft idx=%d, hard_idx=%d", i,ref_pic_list1_list,uut.pred_block.inter_top_block.mv_derive_block.ref_pic_list_1.mem[i],ref_pic_list1_idx,uut.pred_block.inter_top_block.mv_derive_block.ref_pic_list1_dpb_idx.mem[i]);
                    $stop();
                end
            end
        end
    end
    
end
endtask

task current_mv_verify;
begin
    if(uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_valid == 1) begin
        if(uut.pred_block.inter_top_block.mv_derive_block.cb_pred_mode == `MODE_INTER) begin
            if(test_mv_field_pred_flag_l0 == uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l0) begin
                
            end
            else begin
                $display(" pred flag l0 soft = %d hard = %d @ xP = %d, yP = %d",test_mv_field_pred_flag_l0,uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l0,uut.pred_block.inter_top_block.mv_derive_block.xx_pb,uut.pred_block.inter_top_block.mv_derive_block.yy_pb);
                $stop();
            end
            if(test_mv_field_pred_flag_l1 ==uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l1) begin
            
            end
            else begin
                $display(" pred flag l1 soft = %d hard = %d @ xP = %d, yP = %d",test_mv_field_pred_flag_l1,uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l1,uut.pred_block.inter_top_block.mv_derive_block.xx_pb,uut.pred_block.inter_top_block.mv_derive_block.yy_pb);
                $stop();
            end
            if(test_mv_field_pred_flag_l0) begin
                if(test_mv_field_ref_idx_l0 == uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_ref_idx_l0) begin
                
                end
                else begin
                    $display(" ref idx l0 not equal soft %d hard %d @ xPb = %d, yPb = %d",test_mv_field_ref_idx_l0,uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_ref_idx_l0,uut.pred_block.inter_top_block.mv_derive_block.xx_pb,uut.pred_block.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
                end
                if(test_mv_field_mv_x_l0 == uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_mv_x_l0) begin
                
                end
                else begin
                    $display(" x l0 not equal soft %d hard %d @ xPb = %d, yPb = %d",test_mv_field_mv_x_l0,uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_mv_x_l0,uut.pred_block.inter_top_block.mv_derive_block.xx_pb,uut.pred_block.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
                end
                if(test_mv_field_mv_y_l0 == uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_mv_y_l0) begin
                
                end
                else begin
                    $display(" y l0 not equal soft %d hard %d @ xPb = %d, yPb = %d",test_mv_field_mv_y_l0,uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_mv_y_l0,uut.pred_block.inter_top_block.mv_derive_block.xx_pb,uut.pred_block.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
                end
            end
            if(test_mv_field_pred_flag_l1) begin
                if(test_mv_field_ref_idx_l1 == uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_ref_idx_l1) begin
                
                end
                else begin
                    $display(" x l1 not equal soft %d hard %d @ xPb = %d, yPb = %d",test_mv_field_ref_idx_l1,uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_ref_idx_l1,uut.pred_block.inter_top_block.mv_derive_block.xx_pb,uut.pred_block.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
                end
                if(test_mv_field_mv_x_l1 == uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_mv_x_l1) begin
                
                end
                else begin
                    $display(" x l1 not equal soft %d hard %d @ xPb = %d, yPb = %d",test_mv_field_mv_x_l1,uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_mv_x_l1,uut.pred_block.inter_top_block.mv_derive_block.xx_pb,uut.pred_block.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
                end
                if(test_mv_field_mv_y_l1 == uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_mv_y_l1) begin
                
                end
                else begin
                    $display(" y l1 not equal soft %d hard %d @ xPb = %d, yPb = %d",test_mv_field_mv_y_l1,uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_mv_y_l1,uut.pred_block.inter_top_block.mv_derive_block.xx_pb,uut.pred_block.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
                end
            end
        end
        if(uut.pred_block.inter_top_block.mv_derive_block.cb_pred_mode == `MODE_INTRA) begin 
            if(uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l0 == 0 && uut.pred_block.inter_top_block.mv_derive_block.current_mv_field_pred_flag_l1 == 0) begin
                
            end
            else begin
                $display(" intra pu not equal @ xPb = %d, yPb = %d",uut.pred_block.inter_top_block.mv_derive_block.xx_pb,uut.pred_block.inter_top_block.mv_derive_block.yy_pb);
                    $stop();
            end
        end
    end
    inter_pred_iface_block.sv_get_current_mv(0,0,test_mv_field_pred_flag_l0);
    inter_pred_iface_block.sv_get_current_mv(1,0,test_mv_field_pred_flag_l1);
    inter_pred_iface_block.sv_get_current_mv(0,1,test_mv_field_ref_idx_l0);
    inter_pred_iface_block.sv_get_current_mv(1,1,test_mv_field_ref_idx_l1);
    inter_pred_iface_block.sv_get_current_mv(0,2,test_mv_field_mv_x_l0);
    inter_pred_iface_block.sv_get_current_mv(1,2,test_mv_field_mv_x_l1);
    inter_pred_iface_block.sv_get_current_mv(0,3,test_mv_field_mv_y_l0);
    inter_pred_iface_block.sv_get_current_mv(1,3,test_mv_field_mv_y_l1);
end
endtask



task final_pred_pixl_check;
    integer i,j;    
    integer cb_offset_pred_pixel;
    integer cr_offset_pred_pixl;
    reg [7:0] pixl_arry_4x4;
    reg [7:0] hard_pixl_arry_4x4;
    integer xT_in_4x4_luma_out;
    integer yT_in_4x4_luma_out;
    integer xT_in_4x4_cr_out  ;
    integer yT_in_4x4_cr_out  ;
    integer yT_in_4x4_cb_out  ;
    integer xT_in_4x4_cb_out  ;
    integer k;
    begin
        if(uut.pred_block.inter_top_block.luma_wght_pred_valid) begin
            if(uut.pred_block.inter_top_block.xT_in_4x4_luma_out == 0 && uut.pred_block.inter_top_block.yT_in_4x4_luma_out==0)begin
                frame_offset_pred_pixel = uut.pred_block.current__poc*PIC_WIDTH*PIC_HEIGHT*3/2;
            end
            cb_offset_pred_pixel = frame_offset_pred_pixel + PIC_WIDTH*PIC_HEIGHT;
            cr_offset_pred_pixl = cb_offset_pred_pixel + PIC_WIDTH*PIC_HEIGHT/4;
            xT_in_4x4_luma_out = uut.pred_block.inter_top_block.xT_in_4x4_luma_out;
            yT_in_4x4_luma_out = uut.pred_block.inter_top_block.yT_in_4x4_luma_out;
            for(j=0;j<4;j=j+1)begin
                for(i=0;i<4;i=i+1)begin
                    rfile = $fseek(pred_pixl_file,(frame_offset_pred_pixel + (PIC_WIDTH)*(j + yT_in_4x4_luma_out*4) + xT_in_4x4_luma_out*4 + i),0);
                    pixl_arry_4x4= $fgetc(pred_pixl_file);
                    for(k=0;k<8;k=k+1)begin
                        hard_pixl_arry_4x4[k] = uut.pred_block.inter_top_block.luma_wgt_pred_out[(4*j+i)*8+k];
                    end
                    if(pixl_arry_4x4 == hard_pixl_arry_4x4) begin
                        // $display("luma final pred pixl match @loc=%d, x=%d, y=%d, soft=%d, hard=%d",(frame_offset_pred_pixel + (PIC_WIDTH)*(j + yT_in_4x4_luma_out*4) + xT_in_4x4_luma_out*4 + i),xT_in_4x4_luma_out*4 + i,j + yT_in_4x4_luma_out*4,pixl_arry_4x4,hard_pixl_arry_4x4);
                    end
                    else begin
                        $display("luma final pred pixl mismatch @loc=%d, x=%d, y=%d, soft=%d, hard=%d",(frame_offset_pred_pixel + (PIC_WIDTH)*(j + yT_in_4x4_luma_out*4) + xT_in_4x4_luma_out*4 + i),xT_in_4x4_luma_out*4 + i,j + yT_in_4x4_luma_out*4,pixl_arry_4x4,hard_pixl_arry_4x4);
                        $stop();
                    end
                end
            end
        end
        if(uut.pred_block.inter_top_block.cb_wght_pred_valid) begin
            xT_in_4x4_cb_out = uut.pred_block.inter_top_block.xT_in_4x4_cb_out>>1;
            yT_in_4x4_cb_out = uut.pred_block.inter_top_block.yT_in_4x4_cb_out>>1;
            for(j=0;j<4;j=j+1)begin
                for(i=0;i<4;i=i+1)begin
                    rfile = $fseek(pred_pixl_file,(cb_offset_pred_pixel + (PIC_WIDTH>>1)*(j + yT_in_4x4_cb_out*4) + xT_in_4x4_cb_out*4 + i),0);
                    pixl_arry_4x4 = $fgetc(pred_pixl_file);
                    for(k=0;k<8;k=k+1)begin
                        hard_pixl_arry_4x4[k] = uut.pred_block.inter_top_block.cb_wgt_pred_out[(4*j+i)*8+k];
                    end
                    if(pixl_arry_4x4 == hard_pixl_arry_4x4) begin
                        // $display("cb final pred pixl match @loc=%d, @ x=%d, y=%d, soft=%d, hard=%d",(cb_offset_pred_pixel + (PIC_WIDTH>>1)*(j + yT_in_4x4_cb_out*4) + xT_in_4x4_cb_out*4 + i),xT_in_4x4_cb_out*4+i,yT_in_4x4_cb_out*4+j,pixl_arry_4x4,hard_pixl_arry_4x4);
                    end
                    else begin
                        $display("cb final pred pixl mismatch @ x=%d, y=%d, soft=%d, hard=%d",xT_in_4x4_cb_out*4+i,yT_in_4x4_cb_out*4+j,pixl_arry_4x4,hard_pixl_arry_4x4);
                        $stop();
                    end
                    rfile = $fseek(pred_pixl_file,(cr_offset_pred_pixl + (PIC_WIDTH>>1)*(j + yT_in_4x4_cb_out*4) + xT_in_4x4_cb_out*4 + i),0);
                    pixl_arry_4x4 = $fgetc(pred_pixl_file);
                    for(k=0;k<8;k=k+1)begin
                        hard_pixl_arry_4x4[k] = uut.pred_block.inter_top_block.cr_wgt_pred_out[(4*j+i)*8+k];
                    end
                    if(pixl_arry_4x4 == hard_pixl_arry_4x4) begin

                    end
                    else begin
                        $display("cr final pred pixl mismatch @ x=%d, y=%d, soft=%d, hard=%d",xT_in_4x4_cb_out*4+i,yT_in_4x4_cb_out*4+j,pixl_arry_4x4,hard_pixl_arry_4x4);
                        $stop();
                    end
                end
            end
        end
    end
endtask




task col_mv_verify;

    reg [31:0] temp;
    
    reg     [MV_FIELD_AXI_DATA_WIDTH    -1:0]                       col_mv_field[3:0];  
    
    integer i;
    
    begin
        if(uut.mv_col_axi_awvalid & uut.mv_col_axi_awready) begin
            temp = uut.mv_col_axi_awaddr - `COL_MV_ADDR_OFFSET;
			// $display("col mv dpb idx %d ",temp/`COL_MV_FRAME_OFFSET);
            temp = temp % `COL_MV_FRAME_OFFSET;
            y_col_pu = temp / `COL_MV_CTU_ROW_OFFSET * 64/4;
            temp = temp % `COL_MV_CTU_ROW_OFFSET;
            x_col_pu = temp / `COL_MV_CTU_OFFSET * 64/4;
            

        end
        if(uut.mv_col_axi_wvalid && uut.mv_col_axi_wready) begin
        

                                                                      
            {col_mv_field[3],col_mv_field[2],col_mv_field[1],col_mv_field[0]} = uut.mv_col_axi_wdata;
            for(i=0;i<4;i=i+1) begin
				if(x_col_pu < (PIC_WIDTH>>2) && (y_col_pu+4*i) < (PIC_HEIGHT>>2)) begin
					col_mv_hard_soft_compare(col_mv_field[i],x_col_pu ,y_col_pu + 4*i,0,1);
				end                
                if(i<4-1) begin
                    #2;
                end
            end
            x_col_pu = x_col_pu + 4;
        end
    end
endtask

task pref_mv_verify;

    reg [31:0] temp;
    
        

    reg     [MV_FIELD_AXI_DATA_WIDTH    -1:0]                       col_mv_field[3:0];  
    
    integer i;
    
    begin
        if(uut.mv_pref_axi_arvalid & uut.mv_pref_axi_arready) begin
            temp = uut.mv_pref_axi_araddr - `COL_MV_ADDR_OFFSET;
			pref_dpb_idx = temp / `COL_MV_FRAME_OFFSET;
            temp = temp % `COL_MV_FRAME_OFFSET;
            y_pref_mv_pu = temp / `COL_MV_CTU_ROW_OFFSET * 64/16;
            temp = temp % `COL_MV_CTU_ROW_OFFSET;
            x_pref_mv_pu = temp / `COL_MV_CTU_OFFSET * 64/16;
            

        end
        if(uut.mv_pref_axi_rvalid & uut.mv_pref_axi_rready) begin
        

                                                                      
            {col_mv_field[3],col_mv_field[2],col_mv_field[1],col_mv_field[0]} = uut.mv_pref_axi_rdata;
            for(i=0;i<4;i=i+1) begin
				// $display("pref check %x dpb idx %d ", col_mv_field[0],pref_dpb_idx);
				if(x_pref_mv_pu < (PIC_WIDTH>>4) && (y_pref_mv_pu+i) < (PIC_HEIGHT>>4)) begin
					col_mv_hard_soft_compare(col_mv_field[i],x_pref_mv_pu ,y_pref_mv_pu + i,pref_dpb_idx,0);
					// short refs in software are downsampled to 16x16 where as tab_mvfs are in 4x4
				end                
                if(i<4-1) begin
                    #2;
                end
            end
            x_pref_mv_pu = x_pref_mv_pu + 1;
        end
    end
endtask 	


task  col_mv_hard_soft_compare;
    input     [MV_FIELD_AXI_DATA_WIDTH    -1:0]                       col_mv_field;  
    input [31:0] x_pu;
    input [31:0] y_pu;
	input [31:0] dpb_idx;
	input mode;
    
    reg                                  col_ref_mv_field_pred_flag_l0;
    reg                                  col_ref_mv_field_pred_flag_l1;
    reg [REF_IDX_LX_WIDTH -1:0]          col_ref_mv_field_ref_idx_l0;
    reg [REF_IDX_LX_WIDTH -1:0]          col_ref_mv_field_ref_idx_l1;
    reg signed [MVD_WIDTH -1:0]          col_ref_mv_field_mv_x_l0;
    reg signed [MVD_WIDTH -1:0]          col_ref_mv_field_mv_y_l0;
    reg signed [MVD_WIDTH -1:0]          col_ref_mv_field_mv_x_l1;
    reg signed [MVD_WIDTH -1:0]          col_ref_mv_field_mv_y_l1;
    
    reg                                  col_test_mv_field_pred_flag_l0;
    reg                                  col_test_mv_field_pred_flag_l1;
    reg [REF_IDX_LX_WIDTH -1:0]          col_test_mv_field_ref_idx_l0;
    reg [REF_IDX_LX_WIDTH -1:0]          col_test_mv_field_ref_idx_l1;
    reg signed [MVD_WIDTH -1:0]          col_test_mv_field_mv_x_l0;
    reg signed [MVD_WIDTH -1:0]          col_test_mv_field_mv_y_l0;
    reg signed [MVD_WIDTH -1:0]          col_test_mv_field_mv_x_l1;
    reg signed [MVD_WIDTH -1:0]          col_test_mv_field_mv_y_l1;
    integer temp;
    
    
    begin
            {
                col_test_mv_field_pred_flag_l0,
                col_test_mv_field_pred_flag_l1,
                col_test_mv_field_ref_idx_l0,
                col_test_mv_field_ref_idx_l1,
                col_test_mv_field_mv_x_l0,
                col_test_mv_field_mv_y_l0,
                col_test_mv_field_mv_x_l1,
                col_test_mv_field_mv_y_l1, temp[5:0]
            } = col_mv_field;

				if(mode == 1) begin
                inter_pred_iface_block.sv_get_mv(0,0,(x_pu),y_pu,col_ref_mv_field_pred_flag_l0);
                inter_pred_iface_block.sv_get_mv(1,0,(x_pu),y_pu,col_ref_mv_field_pred_flag_l1);
                inter_pred_iface_block.sv_get_mv(0,1,(x_pu),y_pu,col_ref_mv_field_ref_idx_l0);
                inter_pred_iface_block.sv_get_mv(1,1,(x_pu),y_pu,col_ref_mv_field_ref_idx_l1);
                inter_pred_iface_block.sv_get_mv(0,2,(x_pu),y_pu,col_ref_mv_field_mv_x_l0);
                inter_pred_iface_block.sv_get_mv(1,2,(x_pu),y_pu,col_ref_mv_field_mv_x_l1);
                inter_pred_iface_block.sv_get_mv(0,3,(x_pu),y_pu,col_ref_mv_field_mv_y_l0);
                inter_pred_iface_block.sv_get_mv(1,3,(x_pu),y_pu,col_ref_mv_field_mv_y_l1);  
				end
				else begin
                inter_pred_iface_block.sv_get_dpb_mv(dpb_idx,0,0,(x_pu),y_pu,col_ref_mv_field_pred_flag_l0);
                inter_pred_iface_block.sv_get_dpb_mv(dpb_idx,1,0,(x_pu),y_pu,col_ref_mv_field_pred_flag_l1);
                inter_pred_iface_block.sv_get_dpb_mv(dpb_idx,0,1,(x_pu),y_pu,col_ref_mv_field_ref_idx_l0);
                inter_pred_iface_block.sv_get_dpb_mv(dpb_idx,1,1,(x_pu),y_pu,col_ref_mv_field_ref_idx_l1);
                inter_pred_iface_block.sv_get_dpb_mv(dpb_idx,0,2,(x_pu),y_pu,col_ref_mv_field_mv_x_l0);
                inter_pred_iface_block.sv_get_dpb_mv(dpb_idx,1,2,(x_pu),y_pu,col_ref_mv_field_mv_x_l1);
                inter_pred_iface_block.sv_get_dpb_mv(dpb_idx,0,3,(x_pu),y_pu,col_ref_mv_field_mv_y_l0);
                inter_pred_iface_block.sv_get_dpb_mv(dpb_idx,1,3,(x_pu),y_pu,col_ref_mv_field_mv_y_l1);  				
				end
				
					if(
						(col_ref_mv_field_pred_flag_l1 == col_test_mv_field_pred_flag_l1) && 
						(col_ref_mv_field_pred_flag_l0 == col_test_mv_field_pred_flag_l0)
					) begin
					
					end
					else begin
								$display("col mv l0 write wrong pu xy %d %d \n soft %d ref %d x %d y %d \n hard %d ref %d x %d y %d", x_pu, y_pu,
								col_ref_mv_field_pred_flag_l0, 
								col_ref_mv_field_ref_idx_l0,
								col_ref_mv_field_mv_x_l0,
								col_ref_mv_field_mv_y_l0,
								col_test_mv_field_pred_flag_l0, 
								col_test_mv_field_ref_idx_l0,
								col_test_mv_field_mv_x_l0,
								col_test_mv_field_mv_y_l0
								);
								$stop;
					end
			case({col_ref_mv_field_pred_flag_l0,col_ref_mv_field_pred_flag_l1})
				2'b10,2'b11,2'b01: begin
					if(col_ref_mv_field_pred_flag_l0) begin
						if(
							(col_ref_mv_field_pred_flag_l0 == col_test_mv_field_pred_flag_l0) &&
							(col_ref_mv_field_ref_idx_l0 == col_test_mv_field_ref_idx_l0) &&
							(col_ref_mv_field_mv_x_l0 == col_test_mv_field_mv_x_l0) && 
							(col_ref_mv_field_mv_y_l0 == col_test_mv_field_mv_y_l0) 
						) begin  
							if(mode==1) begin
								// $display("col mv write error");
							end
							else begin

								// $display("col mv l0 write wrong pu xy %d %d \n soft %d ref %d x %d y %d \n hard %d ref %d x %d y %d", x_pu, y_pu,
								// col_ref_mv_field_pred_flag_l0, 
								// col_ref_mv_field_ref_idx_l0,
								// col_ref_mv_field_mv_x_l0,
								// col_ref_mv_field_mv_y_l0,
								// col_test_mv_field_pred_flag_l0, 
								// col_test_mv_field_ref_idx_l0,
								// col_test_mv_field_mv_x_l0,
								// col_test_mv_field_mv_y_l0
								// );
							end
						end
						else begin
							if(mode==1) begin
								$display("col mv write error");
							end
							else begin
								$display("pref mv read error");
							end
							$display("time = %d col mv l0 write wrong pu xy %d %d \n soft %d ref %d x %d y %d \n hard %d ref %d x %d y %d", $time, x_pu, y_pu,
							col_ref_mv_field_pred_flag_l0, 
							col_ref_mv_field_ref_idx_l0,
							col_ref_mv_field_mv_x_l0,
							col_ref_mv_field_mv_y_l0,
							col_test_mv_field_pred_flag_l0, 
							col_test_mv_field_ref_idx_l0,
							col_test_mv_field_mv_x_l0,
							col_test_mv_field_mv_y_l0
							);
							
							$stop;                    
						end
					end
					
					if(col_ref_mv_field_pred_flag_l1) begin
						if(
							(col_ref_mv_field_pred_flag_l1 == col_test_mv_field_pred_flag_l1) && 
							(col_ref_mv_field_ref_idx_l1 == col_test_mv_field_ref_idx_l1) && 
							(col_ref_mv_field_mv_x_l1 == col_test_mv_field_mv_x_l1) && 
							(col_ref_mv_field_mv_y_l1 == col_test_mv_field_mv_y_l1) 
						) begin
							// $display("col mv l1 write wrong pu xy %d %d \n soft %d ref %d x %d y %d \n hard %d ref %d x %d y %d", x_pu, y_pu,
							// col_ref_mv_field_pred_flag_l1,
							// col_ref_mv_field_ref_idx_l1,
							// col_ref_mv_field_mv_x_l1,
							// col_ref_mv_field_mv_y_l1,
							// col_test_mv_field_pred_flag_l1,
							// col_test_mv_field_ref_idx_l1,
							// col_test_mv_field_mv_x_l1,
							// col_test_mv_field_mv_y_l1
							// );
								
						end
						else begin
							if(mode==1) begin
								$display("col mv write error");
							end
							else begin
								$display("pref mv read error");
							end
							$display("time %d col mv l1 write wrong pu xy %d %d \n soft %d ref %d x %d y %d \n hard %d ref %d x %d y %d", $time,x_pu, y_pu,
							col_ref_mv_field_pred_flag_l1,
							col_ref_mv_field_ref_idx_l1,
							col_ref_mv_field_mv_x_l1,
							col_ref_mv_field_mv_y_l1,
							col_test_mv_field_pred_flag_l1,
							col_test_mv_field_ref_idx_l1,
							col_test_mv_field_mv_x_l1,
							col_test_mv_field_mv_y_l1
							);
							
							$stop;    
						end
					end
							
				end
				2'b00: begin
							if(mode==1) begin
								// $display("col mv write error");
							end
							else begin

								// $display("col mv l0 write wrong pu xy %d %d \n soft %d ref %d x %d y %d \n hard %d ref %d x %d y %d", x_pu, y_pu,
								// col_ref_mv_field_pred_flag_l0, 
								// col_ref_mv_field_ref_idx_l0,
								// col_ref_mv_field_mv_x_l0,
								// col_ref_mv_field_mv_y_l0,
								// col_test_mv_field_pred_flag_l0, 
								// col_test_mv_field_ref_idx_l0,
								// col_test_mv_field_mv_x_l0,
								// col_test_mv_field_mv_y_l0
								// );
							end
				end
				default: begin
					$display("col pred flag write wrong pu xy %d %d", x_pu, y_pu);
					$stop;
				end
			endcase
            
    end
endtask


endmodule

