onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/data_read_buf_block/rid
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/data_read_buf_block/rdata
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/data_read_buf_block/rresp
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/data_read_buf_block/rlast
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/data_read_buf_block/rvalid
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/data_read_buf_block/rready
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/data_read_buf_block/rid_in
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/data_read_buf_block/rdata_in
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/data_read_buf_block/rresp_in
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/data_read_buf_block/rlast_in
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/data_read_buf_block/rvalid_in
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/data_read_buf_block/rid_int
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/data_read_buf_block/rdata_int
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/data_read_buf_block/rresp_int
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/data_read_buf_block/rlast_int
add wave -noupdate /hevc_test_tb/uut/axi_mig_block/mem_slave/data_read_buf_block/rvalid_int
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {3795 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 293
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {20977 ps}
