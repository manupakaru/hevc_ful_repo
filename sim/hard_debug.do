onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/clk
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/filer_idle_in
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_last
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_ar_valid
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_ready
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/cache_idle_out
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/ctu_monitor_block/new_ctu
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/valid_in
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_valid
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_last
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/ref_pix_axi_ar_ready
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/ref_pix_axi_ar_valid
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/ref_pix_axi_ar_addr
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_11x11
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_ready
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/xT_in_min_tus_out
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/yT_in_min_tus_out
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_121_out
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/xT_in_min_luma_out
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/yT_in_min_luma_out
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/luma_wght_pred_valid
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/luma_wgt_pred_out
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/state
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/intra_y_cu_done_int
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/intra_cb_cu_done_int
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/intra_cr_cu_done_int
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/fifo_in
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/cache_addr
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/cache_rdata
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/cache_wr_en
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{age val for setadd3} {854725000 ps} 0} {cache_out1_4x4 {855935000 ps} 1} {cache_out2_4x4 {855995000 ps} 1} {inter_out4x4 {856105000 ps} 1}
quietly wave cursor active 1
configure wave -namecolwidth 189
configure wave -valuecolwidth 340
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {854379608 ps} {854990392 ps}
