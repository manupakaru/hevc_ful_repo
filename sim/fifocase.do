onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/clk
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/din
add wave -noupdate -radix unsigned /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/prog_empty_thresh
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/rd_en
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/rst
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/wr_en
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/dout
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/empty
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/full
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/prog_empty
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/prog_full
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/tb_4by4_size_wire
add wave -noupdate -radix decimal /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/state
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/fifo_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/input_fifo_is_empty
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/tb_4by4_size
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/c_idx_wire
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/pred_mode
add wave -noupdate -radix unsigned /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/mv_derive_state
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/current_mv_field_pred_flag_l0
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/is_mvd_l0_filled
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/current_mv_field_pred_flag_l1
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/is_mvd_l1_filled
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/config_mode_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/dout
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/prog_empty
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/full
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/empty
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/prog_full
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/clk
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/rst
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/wr_en
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/rd_en
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/din
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/prog_empty_thresh
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/prediction_y_residual_data_fifo_in_block/dout
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {187590000 ps} 1} {{Cursor 2} {281330000 ps} 1} {{Cursor 3} {366732350100 ps} 0}
quietly wave cursor active 3
configure wave -namecolwidth 213
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {368377057490 ps} {368377449606 ps}
