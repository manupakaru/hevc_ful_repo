onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/xx_pb
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/yy_pb
add wave -noupdate -radix unsigned /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/mv_derive_state
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/pu_mv_buffer_done
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/mv_derive_block/pred_sample_gen_idle_in
add wave -noupdate -radix unsigned /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/inter_pred_sample_state
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/tu_empty
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/tu_fifo/d_in
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/tu_fifo/d_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/tu_fifo/empty
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/tu_fifo/full
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/tu_fifo/next_rd_pointer
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/tu_fifo/next_wr_pointer
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/tu_fifo/rd_en
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/tu_fifo/rd_pointer
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/tu_fifo/reset
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/tu_fifo/wr_en
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/pred_pixl_gen_idle_out_to_pred_top
add wave -noupdate -radix unsigned /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/tb_size
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/fifo_in
add wave -noupdate -radix unsigned /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/state
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/cb_yy
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/cb_xx
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/mv_done
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/c_idx_wire
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/intra_ru_read_ready
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_ru_read_ready
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{tu fifo wr enable should be high here} {298982841141 ps} 1}
quietly wave cursor active 0
configure wave -namecolwidth 295
configure wave -valuecolwidth 50
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {298982400888 ps} {298982899112 ps}
