onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/read_stage_block/y_dbf_fifo_rd_en_out
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/read_stage_block/Xc
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/read_stage_block/Yc
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/read_stage_block/dbf_yy_fifo_monitor_block/clk
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/y_residual_adder_block/ans_4by4_out
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/y_residual_adder_block/predsamples_4by4_in
add wave -noupdate -expand /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/y_residual_adder_block/predsamples_4by4_int_arr
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/y_residual_adder_block/predsamples_4by4_valid_in
add wave -noupdate -expand /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/y_residual_adder_block/residual_4by4_int_arr
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/y_common_predsample_4by4_valid_int
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/y_common_predsample_4by4_x_int
add wave -noupdate -radix unsigned /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/pred_wrap_block/pred_top_block/y_common_predsample_4by4_y_int
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {100180867 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 205
configure wave -valuecolwidth 222
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {100125081 ps} {100215416 ps}
