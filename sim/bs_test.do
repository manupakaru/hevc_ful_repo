onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/pred_pixl_gen_idle_out_to_mv_derive
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/pb_part_idx
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/next_mv_field_valid_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/bs_left_valid
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/bs_top_valid
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/end_of_cu
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/cb_yy_min_tus_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/cb_xx_min_tus_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/next_cb_yy_in_min_tus
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/next_cb_xx_in_min_tus
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_fifo_wr_en_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_fifo_full_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/yT_in_min_tus_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/xT_in_min_tus_in
add wave -noupdate -radix binary /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_fifo_data_out
add wave -noupdate -expand -group {pq_mvs for mv based bs} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/p_mv_available
add wave -noupdate -expand -group {pq_mvs for mv based bs} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/q_mv_field_pred_flagl1
add wave -noupdate -expand -group {pq_mvs for mv based bs} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/q_mv_field_pred_flagl0
add wave -noupdate -expand -group {pq_mvs for mv based bs} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/q_mv_field_mv_y_l1
add wave -noupdate -expand -group {pq_mvs for mv based bs} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/q_mv_field_mv_y_l0
add wave -noupdate -expand -group {pq_mvs for mv based bs} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/q_mv_field_mv_x_l1
add wave -noupdate -expand -group {pq_mvs for mv based bs} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/q_mv_field_mv_x_l0
add wave -noupdate -expand -group {pq_mvs for mv based bs} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/q_dpb_idx_l1
add wave -noupdate -expand -group {pq_mvs for mv based bs} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/q_dpb_idx_l0
add wave -noupdate -expand -group {pq_mvs for mv based bs} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/p_mv_field_pred_flagl1
add wave -noupdate -expand -group {pq_mvs for mv based bs} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/p_mv_field_pred_flagl0
add wave -noupdate -expand -group {pq_mvs for mv based bs} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/p_mv_field_mv_y_l1
add wave -noupdate -expand -group {pq_mvs for mv based bs} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/p_mv_field_mv_y_l0
add wave -noupdate -expand -group {pq_mvs for mv based bs} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/p_mv_field_mv_x_l1
add wave -noupdate -expand -group {pq_mvs for mv based bs} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/p_mv_field_mv_x_l0
add wave -noupdate -expand -group {pq_mvs for mv based bs} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/p_dpb_idx_l1
add wave -noupdate -expand -group {pq_mvs for mv based bs} /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/p_dpb_idx_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_v2
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_v1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_h2
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_h1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/intra_tu_4by4_valid_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/inter_pred_sample_state
add wave -noupdate /pred_top_tb/uut/dbf_block/clk
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/cand_mv_cache_valid
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/end_of_tu
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/yy_pb0_strt_in_min_tus
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/xx_pb0_strt_in_min_tus
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/x0_tu_in_min_tus_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/y0_tu_in_min_tus_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/yT_in_min_tus_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/xT_in_min_tus_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_state
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/intra_tu_4by4_valid_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/res_present_pu_range_4by4_valid
add wave -noupdate -expand -group mv_top_left_fifos -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/now_cb_yy_in_min_tus
add wave -noupdate -expand -group mv_top_left_fifos -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/now_cb_xx_in_min_tus
add wave -noupdate -expand -group mv_top_left_fifos /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_top_wr_en_in
add wave -noupdate -expand -group mv_top_left_fifos /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/bs_top_valid
add wave -noupdate -expand -group mv_top_left_fifos -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/cb_xx
add wave -noupdate -expand -group mv_top_left_fifos -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/cb_yy
add wave -noupdate -expand -group mv_top_left_fifos -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/xx_pb
add wave -noupdate -expand -group mv_top_left_fifos -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/yy_pb
add wave -noupdate -expand -group mv_top_left_fifos /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_left_wr_en_in
add wave -noupdate -expand -group mv_top_left_fifos /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/full
add wave -noupdate -expand -group mv_top_left_fifos /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/empty
add wave -noupdate -expand -group mv_top_left_fifos /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/wr_en
add wave -noupdate -expand -group mv_top_left_fifos -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/wr_pointer
add wave -noupdate -expand -group mv_top_left_fifos -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/rd_pointer
add wave -noupdate -expand -group mv_top_left_fifos /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/d_out
add wave -noupdate -expand -group mv_top_left_fifos /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/d_in
add wave -noupdate -expand -group mv_top_left_fifos /pred_top_tb/uut/pred_block/inter_top_block/bs_top_valid
add wave -noupdate -expand -group mv_top_left_fifos /pred_top_tb/uut/pred_block/inter_top_block/bs_left_valid
add wave -noupdate -expand -group mv_top_left_fifos /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/rd_en
add wave -noupdate -expand -group mv_top_left_fifos -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_row_mvs/wr_pointer
add wave -noupdate -expand -group mv_top_left_fifos -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_row_mvs/rd_pointer
add wave -noupdate -expand -group mv_top_left_fifos /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_row_mvs/full
add wave -noupdate -expand -group mv_top_left_fifos /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_row_mvs/empty
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/xx_pb
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/yy_pb
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/x_rel_top_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/y_rel_left_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_top_wr_en_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_left_wr_en_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/res_present_pu_range_4by4_valid
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/p_mv_available
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/is_num_mv_diff
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/is_mv_p_bi_pred
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/is_ref_pic_p_l0l1_q_l0l1_diff
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/is_ref_pic_p_l0l1_q_l1l0_diff
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/p_dpb_idx_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/q_dpb_idx_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/p_dpb_idx_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/q_dpb_idx_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_mv_field_ref_idx_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_mv_field_ref_idx_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_mv_field_pred_flag_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_mv_field_pred_flag_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_mv_field_mv_y_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_mv_field_mv_y_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_mv_field_mv_x_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_mv_field_mv_x_l0
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_state
add wave -noupdate -group left_mv_fifo /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_row_mvs/wr_pointer
add wave -noupdate -group left_mv_fifo /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_row_mvs/wr_en
add wave -noupdate -group left_mv_fifo /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_row_mvs/rd_pointer
add wave -noupdate -group left_mv_fifo /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_row_mvs/rd_en
add wave -noupdate -group left_mv_fifo /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_row_mvs/empty
add wave -noupdate -group left_mv_fifo /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_row_mvs/d_out
add wave -noupdate -group left_mv_fifo /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_row_mvs/d_in
add wave -noupdate -group top_mv_fifo /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/wr_pointer
add wave -noupdate -group top_mv_fifo /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/wr_en
add wave -noupdate -group top_mv_fifo /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/rd_pointer
add wave -noupdate -group top_mv_fifo /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/rd_en
add wave -noupdate -group top_mv_fifo /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/full
add wave -noupdate -group top_mv_fifo /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/empty
add wave -noupdate -group top_mv_fifo /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/d_out
add wave -noupdate -group top_mv_fifo /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs/d_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/state_write_line
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/bs_send_left_minus_1_as_top_last
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_field_left_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_field_top_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/last_greatest_xP_end_in_pu_1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_end_in_d_mv_buf_neibr_check
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_end_in_always_d
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/prev_mv_field_valid_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_derive_state
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_end_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/last_greatest_xP_end_in_pu_1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/last_greatest_xP_end_in_pu_1_st_2
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_end_in_always_d
add wave -noupdate -group topin -expand -group mv_derive_out /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/top_mvs_to_bs_out
add wave -noupdate -group topin -expand -group mv_derive_out /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/left_mvs_to_bs_out
add wave -noupdate -group topin -expand -group mv_derive_out /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_top_w_addr
add wave -noupdate -group topin /pred_top_tb/uut/pred_block/inter_top_block/top_mv_field_ref_idx_l1
add wave -noupdate -group topin /pred_top_tb/uut/pred_block/inter_top_block/top_mv_field_ref_idx_l0
add wave -noupdate -group topin -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/top_mv_field_mv_y_l1
add wave -noupdate -group topin -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/top_mv_field_mv_y_l0
add wave -noupdate -group topin -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/top_mv_field_mv_x_l1
add wave -noupdate -group topin -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/top_mv_field_mv_x_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/top_mv_field_pred_flag_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/top_mv_field_pred_flag_l0
add wave -noupdate -group LEFTIN /pred_top_tb/uut/pred_block/inter_top_block/left_mv_field_pred_flag_l1
add wave -noupdate -group LEFTIN /pred_top_tb/uut/pred_block/inter_top_block/left_mv_field_pred_flag_l0
add wave -noupdate -group LEFTIN /pred_top_tb/uut/pred_block/inter_top_block/left_mv_field_ref_idx_l1
add wave -noupdate -group LEFTIN /pred_top_tb/uut/pred_block/inter_top_block/left_mv_field_ref_idx_l0
add wave -noupdate -group LEFTIN -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/left_mv_field_mv_y_l1
add wave -noupdate -group LEFTIN -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/left_mv_field_mv_y_l0
add wave -noupdate -group LEFTIN -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/left_mv_field_mv_x_l1
add wave -noupdate -group LEFTIN -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/left_mv_field_mv_x_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/res_top_pres_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/res_present_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/res_pres_top_wr_en
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/res_pres_lef_wr_en
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/res_lef_pres_out
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/yT_in_min_tus_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/xT_in_min_tus_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/res_present_pu_range_4by4_valid
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/intra_tu_4by4_valid_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_mv_field_pred_flag_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_mv_field_pred_flag_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_mv_field_pred_flag_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_mv_field_pred_flag_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/top_mvs_to_bs_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/left_mvs_to_bs_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_top_wr_en_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_left_wr_en_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_top_rd_en_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_left_rd_en_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs_fifo_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_row_mvs_fifo_out
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/cb_yy_min_tus_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/cb_xx_min_tus_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/is_top_mv_field_intra
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/is_left_mv_field_intra
add wave -noupdate -expand -group top_mv_line_buffer_memory /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_top_line_buffer/w_en_in
add wave -noupdate -expand -group top_mv_line_buffer_memory /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_top_line_buffer/w_data_in
add wave -noupdate -expand -group top_mv_line_buffer_memory /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_top_line_buffer/w_addr_in
add wave -noupdate -expand -group top_mv_line_buffer_memory /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_top_line_buffer/r_data_out
add wave -noupdate -expand -group top_mv_line_buffer_memory /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_top_line_buffer/r_addr_in
add wave -noupdate -expand -group top_mv_line_buffer_memory /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_top_line_buffer/enable
add wave -noupdate -expand -group top_mv_line_buffer_memory -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/state_write_line
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/bs_top_valid
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_top_r_addr
add wave -noupdate -group left_mv_line_buffer_memory /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_left_line_buffer/w_en_in
add wave -noupdate -group left_mv_line_buffer_memory /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_left_line_buffer/w_data_in
add wave -noupdate -group left_mv_line_buffer_memory -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_left_line_buffer/w_addr_in
add wave -noupdate -group left_mv_line_buffer_memory /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_left_line_buffer/r_data_out
add wave -noupdate -group left_mv_line_buffer_memory -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_left_line_buffer/r_addr_in
add wave -noupdate -group left_mv_line_buffer_memory /pred_top_tb/uut/pred_block/inter_top_block/bs_left_valid
add wave -noupdate -group left_mv_line_buffer_memory /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_left_line_buffer/enable
add wave -noupdate -group left_mv_line_buffer_memory -divider -height 25 {New Divider}
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/state_write_line
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/state_axi_write
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_top_wr_en
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_top_w_addr
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_top_r_addr
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_field_top_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_end_in_d_mv_buf_neibr_check
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_end_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_start_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/last_greatest_xP_end_in_pu_1_st_4
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/last_greatest_xP_end_in_pu_1_st_3
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/last_greatest_xP_end_in_pu_1_st_2
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/last_greatest_xP_end_in_pu_1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/bs_send_left_minus_1_as_top_last
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/bs_top_valid
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/bs_left_valid
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/xx_pb
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/yy_pb
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/cb_xx
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/cb_yy
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/next_mv_field_valid_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/available_flag_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/current_mv_field_valid
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/current_mv_field_ref_idx_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/current_mv_field_ref_idx_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/current_mv_field_pred_flag_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/current_mv_field_pred_flag_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/current_mv_field_mv_y_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/current_mv_field_mv_y_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/current_mv_field_mv_x_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/current_mv_field_mv_x_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/current_dpb_idx_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/current_dpb_idx_l0
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/inter_pred_sample_state
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/pb_part_idx
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cand_mv_field_pred_flag_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cand_mv_field_pred_flag_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cand_mv_field_mv_y_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cand_mv_field_mv_y_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cand_mv_field_mv_x_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cand_mv_field_mv_x_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cand_mv_cache_valid
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cand_dpb_idx_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/cand_dpb_idx_l0
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/yT_in_min_tus_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/xT_in_min_tus_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/pu_3_in_range
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/pu_2_in_range
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/pu_1_in_range
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/pu_0_in_range
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_end_in_d_mv_buf_neibr_check
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_start_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/y_P_pu_start_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_end_in_always_d
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/reset
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_end_in
add wave -noupdate /pred_top_tb/uut/pred_block/state
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ctb_yy
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ctb_xx
add wave -noupdate /pred_top_tb/new_ctu
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/is_top_mv_field_intra
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_mv_field_pred_flag_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_mv_field_pred_flag_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_mv_field_pred_flag_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/left_mv_field_pred_flag_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/top_row_mvs_fifo_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/is_top_mvs_empty
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_top_rd_en_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/pred_sample_gen_block/bs_core_block/bs_top_wr_en_in
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2392983236 ps} 1} {{Cursor 2} {2246842631 ps} 1} {{Cursor 3} {2549532558 ps} 1} {{top fifo write} {62471591 ps} 1} {{Cursor 5} {62778044 ps} 1} {{Cursor 6} {1628941809 ps} 0}
quietly wave cursor active 6
configure wave -namecolwidth 271
configure wave -valuecolwidth 223
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {1628854663 ps} {1629021511 ps}
