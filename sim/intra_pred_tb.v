`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:23:09 05/29/2014
// Design Name:   pred_top_wrapper
// Module Name:   C:/Users/IC_2/Desktop/pred_top_total_repo_/sim/intra_pred_tb.v
// Project Name:  pred_ise_project
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: pred_top_wrapper
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module intra_pred_tb;

    `include "../sim/pred_def.v"
    `include "../sim/inter_axi_def.v"
	
	// Inputs
	reg clk;
	reg reset;
	reg enable;
	
	wire [31:0] fifo_in;
	wire 		write_en_in;
	wire 		input_fifo_is_full;
	
	wire [143:0] 	y_residual_fifo_in;
	wire [8*24-1:0] y_residual_fifo_read;
	wire 			y_residual_fifo_write_en_in;
	
	wire [143:0] 	cb_residual_fifo_in;
	wire [8*24-1:0] cb_residual_fifo_read;
	wire 			cb_residual_fifo_write_en_in;
	
	wire [143:0] 	cr_residual_fifo_in;
	wire [8*24-1:0] cr_residual_fifo_read;
	wire 			cr_residual_fifo_write_en_in;
	
	
	wire 		mv_col_axi_awready;
	wire 		mv_col_axi_wready;
	wire [1:0] 	mv_col_axi_bresp;
	wire 		mv_col_axi_bvalid;
	wire 		mv_pref_axi_arready;
	wire [511:0] mv_pref_axi_rdata;
	wire [1:0] 	mv_pref_axi_rresp;
	wire 		mv_pref_axi_rlast;
	wire 		mv_pref_axi_rvalid;
	
	wire 			ref_pix_axi_ar_ready;
	wire [511:0] 	ref_pix_axi_r_data;
	wire [1:0] 		ref_pix_axi_r_resp;
	wire 			ref_pix_axi_r_last;
	wire 			ref_pix_axi_r_valid;

	// Outputs
	
	wire 			y_residual_fifo_is_full_out;
	wire 			cb_residual_fifo_is_full_out;
	wire 			cr_residual_fifo_is_full_out;

	
	
	wire 			mv_col_axi_awid;
	wire [7:0] 		mv_col_axi_awlen;
	wire [2:0] 		mv_col_axi_awsize;
	wire [1:0] 		mv_col_axi_awburst;
	wire 			mv_col_axi_awlock;
	wire [3:0] 		mv_col_axi_awcache;
	wire [2:0] 		mv_col_axi_awprot;
	wire 			mv_col_axi_awvalid;
	wire [31:0] 	mv_col_axi_awaddr;
	wire [63:0] 	mv_col_axi_wstrb;
	wire 			mv_col_axi_wlast;
	wire 			mv_col_axi_wvalid;
	wire [511:0] 	mv_col_axi_wdata;
	wire 			mv_col_axi_bready;
	wire [31:0] 	mv_pref_axi_araddr;
	wire [7:0] 		mv_pref_axi_arlen;
	wire [2:0] 		mv_pref_axi_arsize;
	wire [1:0] 		mv_pref_axi_arburst;
	wire [2:0] 		mv_pref_axi_arprot;
	wire 			mv_pref_axi_arvalid;
	wire 			mv_pref_axi_rready;
	wire 			mv_pref_axi_arlock;
	wire 			mv_pref_axi_arid;
	wire [3:0] 		mv_pref_axi_arcache;

	wire 			mv_pref_axi_rid;
	wire 			mv_col_axi_wid;
	wire 			mv_col_axi_bid;	
	
	wire [31:0] 	ref_pix_axi_ar_addr;
	wire [7:0] 		ref_pix_axi_ar_len;
	wire [2:0] 		ref_pix_axi_ar_size;
	wire [1:0] 		ref_pix_axi_ar_burst;
	wire [2:0] 		ref_pix_axi_ar_prot;
	wire 			ref_pix_axi_ar_valid;
	wire 			ref_pix_axi_r_ready;
	
    wire                                      	ref_pic_write_axi_awid    	;
    wire      [7:0]                           	ref_pic_write_axi_awlen   	;
    wire      [2:0]                           	ref_pic_write_axi_awsize  	;
    wire      [1:0]                           	ref_pic_write_axi_awburst 	;
    wire                        	    		ref_pic_write_axi_awlock  	;
    wire      [3:0]                           	ref_pic_write_axi_awcache 	;
    wire      [2:0]                           	ref_pic_write_axi_awprot  	;
    wire 		                            	ref_pic_write_axi_awvalid	;
    wire 		[AXI_ADDR_WDTH-1:0]             ref_pic_write_axi_awaddr	;
    wire                      	            	ref_pic_write_axi_awready	;
	
    // write data channel	
    wire	     [AXI_CACHE_DATA_WDTH/8-1:0]	ref_pic_write_axi_wstrb		;
    wire	                                	ref_pic_write_axi_wlast		;
    wire	                                	ref_pic_write_axi_wvalid	;
    wire	    [AXI_CACHE_DATA_WDTH -1:0]		ref_pic_write_axi_wdata		;
	
    wire	                                    ref_pic_write_axi_wready	;
	
    //write response channel	
    wire                       	            	ref_pic_write_axi_bid		;
    wire       [1:0]                           	ref_pic_write_axi_bresp		;
    wire                       	            	ref_pic_write_axi_bvalid	;
    wire  	                                	ref_pic_write_axi_bready	; 
	
	wire 									display_fifo_is_empty_out;
	wire  									display_fifo_rd_en_in;
	wire  [SAO_OUT_FIFO_WIDTH-1:0]			display_fifo_data_out;
	
	wire  									dpb_fifo_is_empty_out;
	wire   									dpb_fifo_rd_en_in;
	wire  [SAO_OUT_FIFO_WIDTH-1:0]			dpb_fifo_data_out;    
	
	wire       [PIC_WIDTH_WIDTH-1:0] pic_width_out;
	wire       [PIC_WIDTH_WIDTH-1:0] pic_height_out;

	wire [AXI_ADDR_WDTH -1:0]			dpb_axi_addr_out;
	wire [AXI_ADDR_WDTH -1:0]			poc_axi_addr_out;

	// Instantiate the Unit Under Test (UUT)
	hevc_top uut (
		.clk(clk), 
		.reset(reset), 
		.enable(enable), 
		
		.fifo_in(fifo_in), 
		.input_fifo_is_full(input_fifo_is_full), 
		.write_en_in(write_en_in), 
		
		.y_residual_fifo_in(y_residual_fifo_in), 
		.y_residual_fifo_is_full_out(y_residual_fifo_is_full_out), 
		.y_residual_fifo_write_en_in(y_residual_fifo_write_en_in), 
		
		.cb_residual_fifo_in(cb_residual_fifo_in), 
		.cb_residual_fifo_is_full_out(cb_residual_fifo_is_full_out), 
		.cb_residual_fifo_write_en_in(cb_residual_fifo_write_en_in), 
		
		.cr_residual_fifo_in(cr_residual_fifo_in), 
		.cr_residual_fifo_is_full_out(cr_residual_fifo_is_full_out), 
		.cr_residual_fifo_write_en_in(cr_residual_fifo_write_en_in), 
		
		.display_fifo_is_empty_out	(display_fifo_is_empty_out),
		.display_fifo_rd_en_in		(display_fifo_rd_en_in),				
		.display_fifo_data_out		(display_fifo_data_out),
		.poc_axi_addr_out			(poc_axi_addr_out) ,
		
		.pic_width_out				(pic_width_out),	
		.pic_height_out				(pic_height_out),
		
		.mv_col_axi_awid(mv_col_axi_awid), 
		.mv_col_axi_awlen(mv_col_axi_awlen), 
		.mv_col_axi_awsize(mv_col_axi_awsize), 
		.mv_col_axi_awburst(mv_col_axi_awburst), 
		.mv_col_axi_awlock(mv_col_axi_awlock), 
		.mv_col_axi_awcache(mv_col_axi_awcache), 
		.mv_col_axi_awprot(mv_col_axi_awprot), 
		.mv_col_axi_awvalid(mv_col_axi_awvalid), 
		.mv_col_axi_awaddr(mv_col_axi_awaddr), 
		.mv_col_axi_awready(mv_col_axi_awready), 
		.mv_col_axi_wstrb(mv_col_axi_wstrb), 
		.mv_col_axi_wlast(mv_col_axi_wlast), 
		.mv_col_axi_wvalid(mv_col_axi_wvalid), 
		.mv_col_axi_wdata(mv_col_axi_wdata), 
		.mv_col_axi_wready(mv_col_axi_wready), 
		.mv_col_axi_bresp(mv_col_axi_bresp), 
		.mv_col_axi_bvalid(mv_col_axi_bvalid), 
		.mv_col_axi_bready(mv_col_axi_bready), 
		.mv_pref_axi_araddr(mv_pref_axi_araddr), 
		.mv_pref_axi_arlen(mv_pref_axi_arlen), 
		.mv_pref_axi_arsize(mv_pref_axi_arsize), 
		.mv_pref_axi_arburst(mv_pref_axi_arburst), 
		.mv_pref_axi_arprot		(mv_pref_axi_arprot), 
		.mv_pref_axi_arvalid	(mv_pref_axi_arvalid), 
		.mv_pref_axi_arready	(mv_pref_axi_arready), 
		.mv_pref_axi_rdata		(mv_pref_axi_rdata), 
		.mv_pref_axi_rresp		(mv_pref_axi_rresp), 
		.mv_pref_axi_rlast		(mv_pref_axi_rlast), 
		.mv_pref_axi_rvalid		(mv_pref_axi_rvalid), 
		.mv_pref_axi_rready		(mv_pref_axi_rready), 
		.mv_pref_axi_arlock		(mv_pref_axi_arlock), 
		.mv_pref_axi_arid		(mv_pref_axi_arid), 
		.mv_pref_axi_arcache	(mv_pref_axi_arcache), 
		.ref_pix_axi_ar_addr	(ref_pix_axi_ar_addr), 
		.ref_pix_axi_ar_len		(ref_pix_axi_ar_len), 
		.ref_pix_axi_ar_size	(ref_pix_axi_ar_size), 
		.ref_pix_axi_ar_burst	(ref_pix_axi_ar_burst), 
		.ref_pix_axi_ar_prot	(ref_pix_axi_ar_prot), 
		.ref_pix_axi_ar_valid   (ref_pix_axi_ar_valid), 
		.ref_pix_axi_ar_ready   (ref_pix_axi_ar_ready), 
		.ref_pix_axi_r_data		(ref_pix_axi_r_data), 
		.ref_pix_axi_r_resp		(ref_pix_axi_r_resp), 
		.ref_pix_axi_r_last		(ref_pix_axi_r_last), 
		.ref_pix_axi_r_valid	(ref_pix_axi_r_valid), 
		.ref_pix_axi_r_ready	(ref_pix_axi_r_ready),
		
		.ref_pic_write_axi_awid   	(ref_pic_write_axi_awid   	),
		.ref_pic_write_axi_awlen  	(ref_pic_write_axi_awlen  	),
		.ref_pic_write_axi_awsize 	(ref_pic_write_axi_awsize 	),
		.ref_pic_write_axi_awburst	(ref_pic_write_axi_awburst	),
		.ref_pic_write_axi_awlock 	(ref_pic_write_axi_awlock 	),
		.ref_pic_write_axi_awcache	(ref_pic_write_axi_awcache	),
		.ref_pic_write_axi_awprot 	(ref_pic_write_axi_awprot 	),
		.ref_pic_write_axi_awvalid	(ref_pic_write_axi_awvalid	),				
		.ref_pic_write_axi_awaddr	(ref_pic_write_axi_awaddr	),						
		.ref_pic_write_axi_awready	(ref_pic_write_axi_awready	),			
		.ref_pic_write_axi_wstrb	(ref_pic_write_axi_wstrb	),	
		.ref_pic_write_axi_wlast	(ref_pic_write_axi_wlast	),	
		.ref_pic_write_axi_wvalid	(ref_pic_write_axi_wvalid	),	
		.ref_pic_write_axi_wdata	(ref_pic_write_axi_wdata	),	
		.ref_pic_write_axi_wready	(ref_pic_write_axi_wready	),	
		.ref_pic_write_axi_bid		(ref_pic_write_axi_bid		),	
		.ref_pic_write_axi_bresp	(ref_pic_write_axi_bresp	),	
		.ref_pic_write_axi_bvalid	(ref_pic_write_axi_bvalid	),	
		.ref_pic_write_axi_bready	(ref_pic_write_axi_bready	)
	);

	
fifo_write_driver 
	#(
		.WIDTH 		(32),
		.FILE_NAME  ("residual_to_inter")
	)
	pred_config_driver (
    .clk(clk), 
    .reset(reset), 
    .out(fifo_in), 
    .prog_full(input_fifo_is_full), 
    .wr_en(write_en_in)
    );
	

fifo_write_driver 
	#(
		.WIDTH 		(8*24),
		.FILE_NAME  ("residual_to_inter_Y")
	)
	pred_res_yy_driver (
    .clk(clk), 
    .reset(reset), 
    .out(y_residual_fifo_read), 
    .prog_full(y_residual_fifo_is_full_out), 
    .wr_en(y_residual_fifo_write_en_in)
    );	
	
	assign y_residual_fifo_in = {
				y_residual_fifo_read[8*22-8:8*21],
				y_residual_fifo_read[8*21-1:8*20],
				y_residual_fifo_read[8*20-6:8*19],
				y_residual_fifo_read[8*19-1:8*18],
				y_residual_fifo_read[8*18-1:8*17],
				y_residual_fifo_read[8*17-1:8*16],
				y_residual_fifo_read[8*16-6:8*15],
				y_residual_fifo_read[8*15-1:8*14],
				y_residual_fifo_read[8*14-1:8*13],
				y_residual_fifo_read[8*13-1:8*12],
				y_residual_fifo_read[8*12-6:8*11],
				y_residual_fifo_read[8*11-1:8*10],
				y_residual_fifo_read[8*10-1:8* 9],
				y_residual_fifo_read[8* 9-1:8* 8],
				y_residual_fifo_read[8* 8-6:8* 7],
				y_residual_fifo_read[8* 7-1:8* 6],
				y_residual_fifo_read[8* 6-1:8* 5],
				y_residual_fifo_read[8* 5-1:8* 4],
				y_residual_fifo_read[8* 4-6:8* 3],
				y_residual_fifo_read[8* 3-1:8* 2],
				y_residual_fifo_read[8* 2-1:8* 1],
				y_residual_fifo_read[8* 1-1:8* 0]
	};

fifo_write_driver 
	#(
		.WIDTH 		(8*24),
		.FILE_NAME  ("residual_to_inter_Cb")
	)
	pred_res_cb_driver (
    .clk(clk), 
    .reset(reset), 
    .out(cb_residual_fifo_read), 
    .prog_full(cb_residual_fifo_is_full_out), 
    .wr_en(cb_residual_fifo_write_en_in)
    );	
	
	assign cb_residual_fifo_in = {
				cb_residual_fifo_read[8*22-8:8*21],
				cb_residual_fifo_read[8*21-1:8*20],
				cb_residual_fifo_read[8*20-6:8*19],
				cb_residual_fifo_read[8*19-1:8*18],
				cb_residual_fifo_read[8*18-1:8*17],
				cb_residual_fifo_read[8*17-1:8*16],
				cb_residual_fifo_read[8*16-6:8*15],
				cb_residual_fifo_read[8*15-1:8*14],
				cb_residual_fifo_read[8*14-1:8*13],
				cb_residual_fifo_read[8*13-1:8*12],
				cb_residual_fifo_read[8*12-6:8*11],
				cb_residual_fifo_read[8*11-1:8*10],
				cb_residual_fifo_read[8*10-1:8* 9],
				cb_residual_fifo_read[8* 9-1:8* 8],
				cb_residual_fifo_read[8* 8-6:8* 7],
				cb_residual_fifo_read[8* 7-1:8* 6],
				cb_residual_fifo_read[8* 6-1:8* 5],
				cb_residual_fifo_read[8* 5-1:8* 4],
				cb_residual_fifo_read[8* 4-6:8* 3],
				cb_residual_fifo_read[8* 3-1:8* 2],
				cb_residual_fifo_read[8* 2-1:8* 1],
				cb_residual_fifo_read[8* 1-1:8* 0]
	};


fifo_write_driver 
	#(
		.WIDTH 		(8*24),
		.FILE_NAME  ("residual_to_inter_Cr")
	)
	pred_res_cr_driver (
    .clk(clk), 
    .reset(reset), 
    .out(cr_residual_fifo_read), 
    .prog_full(cr_residual_fifo_is_full_out), 
    .wr_en(cr_residual_fifo_write_en_in)
    );	
	
	assign cr_residual_fifo_in = {
				cr_residual_fifo_read[8*22-8:8*21],
				cr_residual_fifo_read[8*21-1:8*20],
				cr_residual_fifo_read[8*20-6:8*19],
				cr_residual_fifo_read[8*19-1:8*18],
				cr_residual_fifo_read[8*18-1:8*17],
				cr_residual_fifo_read[8*17-1:8*16],
				cr_residual_fifo_read[8*16-6:8*15],
				cr_residual_fifo_read[8*15-1:8*14],
				cr_residual_fifo_read[8*14-1:8*13],
				cr_residual_fifo_read[8*13-1:8*12],
				cr_residual_fifo_read[8*12-6:8*11],
				cr_residual_fifo_read[8*11-1:8*10],
				cr_residual_fifo_read[8*10-1:8* 9],
				cr_residual_fifo_read[8* 9-1:8* 8],
				cr_residual_fifo_read[8* 8-6:8* 7],
				cr_residual_fifo_read[8* 7-1:8* 6],
				cr_residual_fifo_read[8* 6-1:8* 5],
				cr_residual_fifo_read[8* 5-1:8* 4],
				cr_residual_fifo_read[8* 4-6:8* 3],
				cr_residual_fifo_read[8* 3-1:8* 2],
				cr_residual_fifo_read[8* 2-1:8* 1],
				cr_residual_fifo_read[8* 1-1:8* 0]
	};


fifo_read_driver display_read (
    .empty(display_fifo_is_empty_out), 
    .rd_en(display_fifo_rd_en_in)
    );
	
	
mem_slave_top_module
#(.DUMMY_MEM(0))
col_mv_axi_slave_block
(	.clk	(clk),
	.reset	(reset),
    .arid	(mv_pref_axi_arid),
    .araddr (mv_pref_axi_araddr),
    .arlen	(mv_pref_axi_arlen),
    .arsize	(mv_pref_axi_arsize),
    .arburst(mv_pref_axi_arburst),
    .arlock	(mv_pref_axi_arlock),
    .arcache(mv_pref_axi_arcache),
    .arprot	(mv_pref_axi_arprot),
    .arvalid(mv_pref_axi_arvalid),
    .arready(mv_pref_axi_arready),
    .rid	(mv_pref_axi_rid),
    .rdata	(mv_pref_axi_rdata),
    .rresp	(mv_pref_axi_rresp),
    .rlast	(mv_pref_axi_rlast),
    .rvalid	(mv_pref_axi_rvalid),
    .rready	(mv_pref_axi_rready),
    .awid	(mv_col_axi_awid),
    .awaddr	(mv_col_axi_awaddr),
    .awlen	(mv_col_axi_awlen),
    .awsize	(mv_col_axi_awsize),
    .awburst(mv_col_axi_awburst),
    .awlock	(mv_col_axi_awlock),
    .awcache(mv_col_axi_awcache),
    .awprot	(mv_col_axi_awprot),
    .awvalid(mv_col_axi_awvalid),
    .awready(mv_col_axi_awready),
    .wid	(mv_col_axi_wid),
    .wdata	(mv_col_axi_wdata),
    .wstrb	(mv_col_axi_wstrb),
    .wvalid	(mv_col_axi_wvalid),
    .wlast	(mv_col_axi_wlast),
    .wready	(mv_col_axi_wready),
    .bid	(mv_col_axi_bid),	
    .bresp	(mv_col_axi_bresp),
    .bvalid	(mv_col_axi_bvalid),
    .bready  (mv_col_axi_bready)
 );	
 
 mem_slave_top_module
#(.DUMMY_MEM(0))
ref_pic_axi_slave_block
(	.clk	(clk),
	.reset	(reset),
    .arid	(0),		
    .araddr (ref_pix_axi_ar_addr),  
    .arlen	(ref_pix_axi_ar_len),   
    .arsize	(ref_pix_axi_ar_size),  
    .arburst(ref_pix_axi_ar_burst), 
    .arlock	(0),   
    .arcache(0),  
    .arprot	(ref_pix_axi_ar_prot),  
    .arvalid(ref_pix_axi_ar_valid), 
    .arready(ref_pix_axi_ar_ready), 
    .rid	(),      
    .rdata	(ref_pix_axi_r_data),   
    .rresp	(ref_pix_axi_r_resp),
    .rlast	(ref_pix_axi_r_last),
    .rvalid	(ref_pix_axi_r_valid),
    .rready	(ref_pix_axi_r_ready),
    .awid	(ref_pic_write_axi_awid),		
    .awaddr	(ref_pic_write_axi_awaddr),     
    .awlen	(ref_pic_write_axi_awlen),      
    .awsize	(ref_pic_write_axi_awsize),     
    .awburst(ref_pic_write_axi_awburst),    
    .awlock	(ref_pic_write_axi_awlock),     
    .awcache(ref_pic_write_axi_awcache),    
    .awprot	(ref_pic_write_axi_awprot),     
    .awvalid(ref_pic_write_axi_awvalid),    
    .awready(ref_pic_write_axi_awready),    
    .wid	(),          			
    .wdata	(ref_pic_write_axi_wdata),      
    .wstrb	(ref_pic_write_axi_wstrb),      
    .wvalid	(ref_pic_write_axi_wvalid	),  
    .wlast	(ref_pic_write_axi_wlast),      
    .wready	(ref_pic_write_axi_wready),     
    .bid	(ref_pic_write_axi_bid),	    
    .bresp	(ref_pic_write_axi_bresp),      
    .bvalid	(ref_pic_write_axi_bvalid),     
    .bready  (ref_pic_write_axi_bready)
 );	
 
	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 1;
		enable = 1;
				
		
		// Wait 100 ns for global reset to finish
		#100;
		@(posedge clk);
        reset = 0;
		// Add stimulus here

	end
    always #10 clk = ~clk;  
endmodule

