onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_store_poc_mem
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_store_valids
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_wr_ptr
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_wr_start
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_wr_done
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_rd_ptr
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_rd_start
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_rd_done
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_store_read_pointer
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_store_write_pointer
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/poc_4bits_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/poc_4bits_in
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_samples
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_samples_d1
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_samples_d2
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_samples_d3
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_counter
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/max_frame_store_idx
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/config_state
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_store_poc_mem
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_store_valids
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_wr_ptr
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_wr_start
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_wr_done
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_rd_ptr
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_rd_start
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_rd_done
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_store_read_pointer
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/poc_read
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/old_frame_store_read_pointer
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_store_write_pointer_old
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_store_write_pointer
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_store_read_pointer
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/poc_4bits_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/poc_4bits_in
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_samples
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_samples_d1
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_samples_d2
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_samples_d3
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_counter
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/max_frame_store_idx
add wave -noupdate -expand -group displaubuffer_handle -expand -group frame_store_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/config_state
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wraddr_current_ctu
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done_d
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/last_8x8_block_in_pic
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_read_done_rows_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -expand /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix decimal /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_offset_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wraddr_counter
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wraddr_state
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wraddr_current_ctu
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_awaddr
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix decimal /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_offset_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_x
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix decimal /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_y
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_x_minus8
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix decimal /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_y_minus8
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_sao_x_8x8_waddr_wire_temp
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_sao_y_8x8_waddr_wire_temp
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_awvalid
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_awready
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/pic_width_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wraddr_counter
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_wr_ptr
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_rd_done
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_row_counter_data
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_row_counter_addr
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/poc_read
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_store_idx_mem
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_store_write_pointer
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wraddr_current_ctu
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/last_8x8_block_in_pic
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_read_done_rows_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -expand /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix decimal /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_offset_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wraddr_counter
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wraddr_state
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wraddr_current_ctu
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_awaddr
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix decimal /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_offset_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_x
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix decimal /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_y
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_x_minus8
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done_d
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix decimal /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_y_minus8
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_x_minus8_d0
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_x_minus8_d1
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_x_minus8_d2
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_x_minus8_d3
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_x_minus8_d4
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_y_minus8_d1
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_y_minus8_d0
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_y_minus8_d2
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_y_minus8_d3
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_y_minus8_d4
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_sao_x_8x8_waddr_wire_temp
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_sao_y_8x8_waddr_wire_temp
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_awvalid
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_awready
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/pic_width_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wraddr_counter
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_wr_ptr
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_rd_done
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_row_counter_data
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_row_counter_addr
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/poc_read
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_store_idx_mem
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_waddr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_store_write_pointer
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_sao_x_8x8_waddr_wire_temp
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_sao_y_8x8_waddr_wire_temp
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_x
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_y
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_read_done_rows_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_ctu_row_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm -radix binary -childformat {{{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[0]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[1]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[2]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[3]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[4]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[5]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[6]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[7]} -radix binary}} -expand -subitemconfig {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[0]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[1]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[2]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[3]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[4]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[5]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[6]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[7]} {-height 15 -radix binary}} /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm -radix binary -childformat {{{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[7]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[6]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[5]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[4]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[3]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[2]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[1]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[0]} -radix binary}} -subitemconfig {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[7]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[6]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[5]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[4]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[3]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[2]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[1]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[0]} {-height 15 -radix binary}} /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done_d
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wrdata_state
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wrdata_burst_state
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wrdata_counter
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wrdata_subcounter
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wdata
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wvalid
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wready
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/display_buffer_inner_y_addr_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/dispbuf_ddr_wdata_mem
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/dispbuf_ddr_wdata_wire
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_read_done_rows_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_ctu_row_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm -radix binary -childformat {{{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[0]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[1]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[2]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[3]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[4]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[5]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[6]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[7]} -radix binary}} -expand -subitemconfig {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[0]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[1]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[2]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[3]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[4]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[5]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[6]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[7]} {-height 15 -radix binary}} /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm -radix binary -childformat {{{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[7]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[6]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[5]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[4]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[3]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[2]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[1]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[0]} -radix binary}} -subitemconfig {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[7]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[6]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[5]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[4]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[3]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[2]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[1]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done[0]} {-height 15 -radix binary}} /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wrdata_state
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wrdata_burst_state
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wrdata_counter
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wrdata_subcounter
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wdata
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wvalid
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wready
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/display_buffer_inner_y_addr_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/dispbuf_ddr_wdata_mem
add wave -noupdate -expand -group displaubuffer_handle -expand -group axi_wdata_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/dispbuf_ddr_wdata_wire
add wave -noupdate -expand -group displaubuffer_handle -expand -group input_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/displaybuffer_fifo_data_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group input_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/displaybuffer_fifo_data_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/clk
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/reset
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/displaybuffer_fifo_data_in
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/displaybuffer_fifo_read_en_out
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/displaybuffer_fifo_empty_in
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/displaybuffer_fifo_almost_empty_in
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/log2_ctu_size_in
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/poc_4bits_in
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/pic_width_in
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/pic_height_in
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/config_valid_in
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_aclk
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_awaddr
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_awvalid
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_awready
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_awlen
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wdata
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wlast
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wvalid
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wready
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wstrb
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_bresp
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_bvalid
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_bready
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_araddr
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_arvalid
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_arready
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_arlen
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_arburst
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rdata
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rresp
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rlast
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rready
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rvalid
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/hdmi_fifo_almost_full
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/hdmi_fifo_data_out
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/hdmi_fifo_w_en
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/clk
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/reset
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rdaddr_state
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/poc_read
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_rd_done
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_row_counter_addr
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_arlen
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/pic_width_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_rd_start
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_araddr
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_rd_ptr
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_arvalid
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/pic_height_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_row_counter_data
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_arready
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/clk
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/reset
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rdaddr_state
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/poc_read
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_rd_done
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_row_counter_addr
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_arlen
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/pic_width_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_rd_start
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_araddr
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_rd_ptr
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_arvalid
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/pic_height_reg
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_row_counter_data
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_addr_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_arready
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/clk
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/reset
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rddata_state
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rready
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rddata_counter
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_row_counter_data
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rvalid
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/hdmi_fifo_almost_full
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rready
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rddata_counter
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/hdmi_fifo_data_out
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rdata
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/hdmi_fifo_w_en
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_column_counter
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rlast
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_rd_done
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/clk
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/reset
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rddata_state
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rready
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rddata_counter
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_row_counter_data
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rvalid
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/hdmi_fifo_almost_full
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rready
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rddata_counter
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/hdmi_fifo_data_out
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rdata
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/hdmi_fifo_w_en
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_column_counter
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rlast
add wave -noupdate -expand -group displaubuffer_handle -expand -group read_data_fsm /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_rd_done
add wave -noupdate -expand -group displaubuffer_handle -expand /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_y_data_arr_wire
add wave -noupdate -expand -group displaubuffer_handle -expand /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_cb_data_arr_wire
add wave -noupdate -expand -group displaubuffer_handle -expand /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_cr_data_arr_wire
add wave -noupdate -expand -group displaubuffer_handle -expand /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram_wdata_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram0_porta_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram0_porta_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram1_porta_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram1_porta_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram2_porta_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram2_porta_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram3_porta_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram3_porta_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram4_porta_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram4_porta_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram5_porta_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram5_porta_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram6_porta_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram6_porta_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram7_porta_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram7_porta_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram_porta_wen_reg
add wave -noupdate -expand -group displaubuffer_handle -expand /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram_wdata_wire
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_sao_x_8x8_waddr_wire
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_sao_y_8x8_waddr_wire
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_sao_x_8x8_waddr_wire_temp
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_sao_y_8x8_waddr_wire_temp
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/displaybuffer_fifo_read_en_out
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/displaybuffer_fifo_empty_in
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/displaybuffer_fifo_almost_empty_in
add wave -noupdate -expand -group displaubuffer_handle -childformat {{{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[0]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[1]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[2]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[3]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[4]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[5]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[6]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[7]} -radix binary}} -expand -subitemconfig {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[0]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[1]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[2]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[3]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[4]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[5]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[6]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[7]} {-height 15 -radix binary}} /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem
add wave -noupdate -expand -group displaubuffer_handle -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/displaybuffer_fifo_data_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_y_data_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_cb_data_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_cr_data_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/log2_ctu_size_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram0_porta_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram0_porta_rdata_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram0_portb_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram0_portb_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/display_buffer_inner_y_addr_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram0_portb_addr_reg
add wave -noupdate -expand -group displaubuffer_handle -expand /hevc_test_tb/uut/axi_displaybuffer_handle_block/dispbuf_ddr_wdata_mem
add wave -noupdate -expand -group displaubuffer_handle -expand /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram_portb_rdata_wire_arr
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram1_porta_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram1_porta_rdata_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram1_portb_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram1_portb_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram1_portb_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram2_porta_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram2_porta_rdata_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram2_portb_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram2_portb_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram2_portb_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram3_porta_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram3_porta_rdata_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram3_portb_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram3_portb_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram3_portb_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram4_porta_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram4_porta_rdata_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram4_portb_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram4_portb_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram4_portb_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram5_porta_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram5_porta_rdata_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram5_portb_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram5_portb_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram5_portb_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram6_porta_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram6_porta_rdata_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram6_portb_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram6_portb_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram6_portb_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram7_porta_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram7_porta_rdata_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram7_portb_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram7_portb_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram7_portb_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_addr_offset_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/pic_width_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/pic_height_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_x
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_y
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/last_8x8_block_in_pic
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rddata_state
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rdaddr_state
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_store_read_pointer
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/poc_read
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/old_poc_read
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/old_frame_store_read_pointer
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_row_counter_addr
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_row_counter_data
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rddata_state
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_rd_done
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rddata_counter
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wraddr_counter
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_sao_x_8x8_waddr_wire_temp
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_sao_y_8x8_waddr_wire_temp
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_x
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_y
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/clk
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/reset
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/displaybuffer_fifo_data_in
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/displaybuffer_fifo_read_en_out
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/displaybuffer_fifo_empty_in
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/displaybuffer_fifo_almost_empty_in
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/log2_ctu_size_in
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/poc_4bits_in
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/pic_width_in
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/pic_height_in
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/config_valid_in
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_aclk
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_awaddr
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_awvalid
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_awready
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_awlen
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wdata
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wlast
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wvalid
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wready
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wstrb
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_bresp
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_bvalid
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_bready
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_araddr
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_arvalid
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_arready
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_arlen
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_arburst
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rdata
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rresp
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rlast
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rready
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rvalid
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/hdmi_fifo_almost_full
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/hdmi_fifo_data_out
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/hdmi_fifo_w_en
add wave -noupdate -expand -group displaubuffer_handle -expand /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_y_data_arr_wire
add wave -noupdate -expand -group displaubuffer_handle -expand /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_cb_data_arr_wire
add wave -noupdate -expand -group displaubuffer_handle -expand /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_cr_data_arr_wire
add wave -noupdate -expand -group displaubuffer_handle -expand /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram_wdata_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram0_porta_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram0_porta_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram1_porta_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram1_porta_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram2_porta_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram2_porta_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram3_porta_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram3_porta_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram4_porta_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram4_porta_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram5_porta_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram5_porta_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram6_porta_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram6_porta_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram7_porta_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram7_porta_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram_porta_wen_reg
add wave -noupdate -expand -group displaubuffer_handle -expand /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram_wdata_wire
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_sao_x_8x8_waddr_wire
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_sao_y_8x8_waddr_wire
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_sao_x_8x8_waddr_wire_temp
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_sao_y_8x8_waddr_wire_temp
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_sao_x_8x8_waddr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/displaybuffer_fifo_read_en_out
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/displaybuffer_fifo_empty_in
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/displaybuffer_fifo_almost_empty_in
add wave -noupdate -expand -group displaubuffer_handle -childformat {{{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[0]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[1]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[2]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[3]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[4]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[5]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[6]} -radix binary} {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[7]} -radix binary}} -expand -subitemconfig {{/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[0]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[1]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[2]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[3]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[4]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[5]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[6]} {-height 15 -radix binary} {/hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem[7]} {-height 15 -radix binary}} /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_8by8_write_done_mask_mem
add wave -noupdate -expand -group displaubuffer_handle -radix binary /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_row_done
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/displaybuffer_fifo_data_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_y_data_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_cb_data_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/current_cr_data_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/log2_ctu_size_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram0_porta_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram0_porta_rdata_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram0_portb_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram0_portb_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/display_buffer_inner_y_addr_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram0_portb_addr_reg
add wave -noupdate -expand -group displaubuffer_handle -expand /hevc_test_tb/uut/axi_displaybuffer_handle_block/dispbuf_ddr_wdata_mem
add wave -noupdate -expand -group displaubuffer_handle -expand /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram_portb_rdata_wire_arr
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram1_porta_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram1_porta_rdata_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram1_portb_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram1_portb_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram1_portb_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram2_porta_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram2_porta_rdata_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram2_portb_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram2_portb_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram2_portb_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram3_porta_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram3_porta_rdata_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram3_portb_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram3_portb_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram3_portb_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram4_porta_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram4_porta_rdata_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram4_portb_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram4_portb_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram4_portb_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram5_porta_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram5_porta_rdata_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram5_portb_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram5_portb_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram5_portb_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram6_porta_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram6_porta_rdata_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram6_portb_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram6_portb_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram6_portb_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram7_porta_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram7_porta_rdata_wire
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram7_portb_en_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram7_portb_wdata_reg
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/blockram7_portb_addr_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_addr_offset_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/pic_width_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/pic_height_reg
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_x
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/ctu_y
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/last_8x8_block_in_pic
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rddata_state
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rdaddr_state
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_store_read_pointer
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/poc_read
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/old_poc_read
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/old_frame_store_read_pointer
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_row_counter_addr
add wave -noupdate -expand -group displaubuffer_handle -radix unsigned /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_row_counter_data
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rddata_state
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/frame_ddr_rd_done
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rddata_counter
add wave -noupdate -expand -group displaubuffer_handle /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_wraddr_counter
add wave -noupdate /hevc_test_tb/uut/hdmi_fifo_monitor_block/full
add wave -noupdate /hevc_test_tb/uut/hdmi_fifo_monitor_block/hdmi_fifo_in
add wave -noupdate /hevc_test_tb/uut/hdmi_fifo_monitor_block/reset
add wave -noupdate /hevc_test_tb/uut/hdmi_fifo_monitor_block/wr_en
add wave -noupdate /hevc_test_tb/uut/hdmi_fifo_monitor_block/clk
add wave -noupdate -expand -group ref_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/dpb_buffer/din
add wave -noupdate -expand -group ref_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/dpb_buffer/rd_en
add wave -noupdate -expand -group ref_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/dpb_buffer/rst
add wave -noupdate -expand -group ref_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/dpb_buffer/wr_en
add wave -noupdate -expand -group ref_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/dpb_buffer/dout
add wave -noupdate -expand -group ref_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/dpb_buffer/empty
add wave -noupdate -expand -group ref_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/dpb_buffer/full
add wave -noupdate -expand -group ref_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/dpb_buffer/prog_full
add wave -noupdate -expand -group disp_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/display_buffer/din
add wave -noupdate -expand -group disp_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/display_buffer/rd_en
add wave -noupdate -expand -group disp_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/display_buffer/rst
add wave -noupdate -expand -group disp_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/display_buffer/wr_en
add wave -noupdate -expand -group disp_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/display_buffer/dout
add wave -noupdate -expand -group disp_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/display_buffer/empty
add wave -noupdate -expand -group disp_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/display_buffer/full
add wave -noupdate -expand -group disp_buf_signals /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/display_buffer/prog_full
add wave -noupdate /hevc_test_tb/uut/hevc_dec_block/pred_dbf_block/dbf_block/main_sao_block/log2_ctu_size_out
add wave -noupdate /hevc_test_tb/uut/hdmi_fifo_monitor_block/wr_en
add wave -noupdate /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rddata_state
add wave -noupdate /hevc_test_tb/uut/axi_displaybuffer_handle_block/axi_rvalid
add wave -noupdate /hevc_test_tb/uut/axi_displaybuffer_handle_block/reset
add wave -noupdate /hevc_test_tb/uut/axi_displaybuffer_handle_block/clk
add wave -noupdate /hevc_test_tb/uut/axi_displaybuffer_handle_block/hdmi_fifo_almost_full
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {371815000 ps} 1} {{Cursor 2} {13360795051 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 374
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {5640930942 ps} {18981314161 ps}
