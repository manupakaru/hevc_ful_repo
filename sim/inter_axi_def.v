
`define AX_SIZE_1        3'b000 
`define AX_SIZE_2        3'b001 
`define AX_SIZE_4        3'b010 
`define AX_SIZE_8        3'b011 
`define AX_SIZE_16       3'b100 
`define AX_SIZE_32       3'b101 
`define AX_SIZE_64       3'b110 
`define AX_SIZE_128      3'b111 

`define AX_LEN_1     8'b0000_0000 
`define AX_LEN_2     8'b0000_0001 
`define AX_LEN_3     8'b0000_0010 
`define AX_LEN_4     8'b0000_0011 
`define AX_LEN_5     8'b0000_0100 
`define AX_LEN_6     8'b0000_0101 
`define AX_LEN_7     8'b0000_0110 
`define AX_LEN_8     8'b0000_0111 
`define AX_LEN_9     8'b0000_1000 
`define AX_LEN_10    8'b0000_1001 
`define AX_LEN_11    8'b0000_1010 
`define AX_LEN_12    8'b0000_1011 
`define AX_LEN_13    8'b0000_1100 
`define AX_LEN_14    8'b0000_1101 
`define AX_LEN_15    8'b0000_1110 
`define AX_LEN_16    8'b0000_1111 
`define AX_LEN_96    8'd95 
`define AX_LEN_128    8'd127

`define AX_BURST_INC       2'b01
`define AX_LOCK_DEFAULT    1'b0
`define AX_CACHE_DEFAULT    4'b0000

`define AX_PROT_DATA    3'b000



parameter MV_FIELD_AXI_DATA_WIDTH  = 80;

`define XRESP_DEC_ERROR 2'b11
`define XRESP_SLAV_ERROR 2'b10

parameter AXI_ADDR_WDTH = 32;
parameter AXI_CACHE_DATA_WDTH = 512;
parameter AXI_MIG_DATA_WIDTH = 512;

`define REF_PIX_BU_OFFSET (8'd128)
`define REF_PIX_BU_OFFSET_OLD (8'd64)
`define REF_PIX_BU_ROW_OFFSET (11'd1024)
`define REF_PIX_IU_OFFSET (14'd8192)


`ifdef HEVC_4K
	`define COL_MV_ADDR_OFFSET (32'h07E0_0000) 
	`define COL_MV_CTU_ROW_OFFSET (16'd16384)
	`define COL_MV_CTU_OFFSET (9'd256)
	`define COL_MV_INNER_CTU_ROW_OFFSET (7'd64)
	`define CTUS_PER_ROW (8'64)
	`define CTUS_PER_COL (8'36)
`else
	`define COL_MV_CTU_ROW_OFFSET (32'd7680)
	`define COL_MV_CTU_OFFSET (32'd256)
	`define COL_MV_INNER_CTU_ROW_OFFSET (32'd64)
	`define COL_MV_ADDR_OFFSET (32'h200_0000)
	`define CTUS_PER_ROW (8'30)
	`define CTUS_PER_COL (8'17)
`endif
`ifdef HEVC_4K
	`define REF_PIX_IU_ROW_OFFSET (20'd524288)
	`define REF_PIX_FRAME_OFFSET (29'h01200000)
	`define REF_PIX_FRAME_OFFSET_VAL (11'h012)
	`define REF_PIX_FRAME_OFFSET_SHIFT (8'd20)

	`define COL_MV_FRAME_OFFSET (28'h90000)
	`define COL_MV_FRAME_OFFSET_VAL (10'h009)
	`define COL_MV_FRAME_OFFSET_SHIFT (8'd16) 
`else
	`define REF_PIX_IU_ROW_OFFSET (32'd245760)
	`define REF_PIX_FRAME_OFFSET (28'h040_0000)
	`define REF_PIX_FRAME_OFFSET_VAL (3'h4)
	`define REF_PIX_FRAME_OFFSET_SHIFT (8'd20)

	`define COL_MV_FRAME_OFFSET (32'h00020000)
	`define COL_MV_FRAME_OFFSET_VAL (3'h2)
	`define COL_MV_FRAME_OFFSET_SHIFT (8'd16)
`endif