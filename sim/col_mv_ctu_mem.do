onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /pred_top_tb/uut/pred_block/st_rps_entry_block/DATA_WIDTH
add wave -noupdate -expand -group {col mv verify} /pred_top_tb/col_mv_hard_soft_compare/col_ref_mv_field_pred_flag_l0
add wave -noupdate -expand -group {col mv verify} /pred_top_tb/col_mv_hard_soft_compare/col_ref_mv_field_pred_flag_l1
add wave -noupdate -expand -group {col mv verify} /pred_top_tb/col_mv_hard_soft_compare/col_ref_mv_field_ref_idx_l0
add wave -noupdate -expand -group {col mv verify} /pred_top_tb/col_mv_hard_soft_compare/col_ref_mv_field_ref_idx_l1
add wave -noupdate -expand -group {col mv verify} /pred_top_tb/col_mv_hard_soft_compare/col_ref_mv_field_mv_x_l0
add wave -noupdate -expand -group {col mv verify} /pred_top_tb/col_mv_hard_soft_compare/col_ref_mv_field_mv_y_l0
add wave -noupdate -expand -group {col mv verify} /pred_top_tb/col_mv_hard_soft_compare/col_ref_mv_field_mv_x_l1
add wave -noupdate -expand -group {col mv verify} /pred_top_tb/col_mv_hard_soft_compare/col_ref_mv_field_mv_y_l1
add wave -noupdate -expand -group {col mv verify} /pred_top_tb/col_mv_hard_soft_compare/col_test_mv_field_pred_flag_l0
add wave -noupdate -expand -group {col mv verify} /pred_top_tb/col_mv_hard_soft_compare/col_test_mv_field_pred_flag_l1
add wave -noupdate -expand -group {col mv verify} /pred_top_tb/col_mv_hard_soft_compare/col_test_mv_field_ref_idx_l0
add wave -noupdate -expand -group {col mv verify} /pred_top_tb/col_mv_hard_soft_compare/col_test_mv_field_ref_idx_l1
add wave -noupdate -expand -group {col mv verify} /pred_top_tb/col_mv_hard_soft_compare/col_test_mv_field_mv_x_l0
add wave -noupdate -expand -group {col mv verify} /pred_top_tb/col_mv_hard_soft_compare/col_test_mv_field_mv_y_l0
add wave -noupdate -expand -group {col mv verify} /pred_top_tb/col_mv_hard_soft_compare/col_test_mv_field_mv_x_l1
add wave -noupdate -expand -group {col mv verify} /pred_top_tb/col_mv_hard_soft_compare/col_test_mv_field_mv_y_l1
add wave -noupdate -expand -group {col mv verify} /pred_top_tb/col_mv_hard_soft_compare/x_pu
add wave -noupdate -expand -group {col mv verify} /pred_top_tb/col_mv_hard_soft_compare/y_pu
add wave -noupdate -radix decimal /pred_top_tb/file_location
add wave -noupdate /pred_top_tb/soft_file_loc
add wave -noupdate /pred_top_tb/uut/pred_block/mv_done
add wave -noupdate /pred_top_tb/new_poc
add wave -noupdate /pred_top_tb/uut/pred_block/write_en_out
add wave -noupdate /pred_top_tb/uut/pred_block/read_en_out
add wave -noupdate /pred_top_tb/enable
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/state_write_line
add wave -noupdate /pred_top_tb/fifo_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/y_P_pu_start_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/y_P_pu_end_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/y_P_pu_end_in_1
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_start_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_end_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_P_pu_end_in_1
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/y_rel_left_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/y_ctb_addr
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/y_N_pu_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/yN_pu_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/yy_pb
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/hh_pb
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/cb_size
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ww_pb
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ww_pb_temp
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/hh_pb_temp
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_rel_top_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_ctb_addr
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/x_N_pu_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/top_mvs_to_bs_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/top_line_done
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/state_col_write
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/state_axi_write
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/reset
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/yy_pb_temp
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/xx_pb_temp
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/cb_yy
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/cb_xx
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/prev_mv_field_valid_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/config_mode_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/cb_size_wire
add wave -noupdate /pred_top_tb/uut/pred_block/state
add wave -noupdate /pred_top_tb/fifo_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/state_write_line
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/y_rel_left_out
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/current__poc
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/merge_flag
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/merge_idx
add wave -noupdate -expand -group current_mv -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_mv_x_l0
add wave -noupdate -expand -group current_mv -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_mv_x_l1
add wave -noupdate -expand -group current_mv -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_mv_y_l0
add wave -noupdate -expand -group current_mv -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_mv_y_l1
add wave -noupdate -expand -group current_mv /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_pred_flag_l0
add wave -noupdate -expand -group current_mv /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_pred_flag_l1
add wave -noupdate -expand -group current_mv /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_ref_idx_l0
add wave -noupdate -expand -group current_mv /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_ref_idx_l1
add wave -noupdate -expand -group current_mv /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_mv_field_valid
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mvp_l1_flag
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mvp_l1_idx
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_derive_state
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/merge_cand_found
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/merge_array_idx
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/yy_pb
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/xx_pb
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/col_mv_field_mv_x_l0_to_inter
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/col_mv_field_mv_y_l0_to_inter
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_mv_field_pred_flag_l0_to_inter
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_mv_field_pred_flag_l1_to_inter
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/pred_sample_gen_idle_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/merge_cand_list
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/available_col_l0_flag
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/available_col_l1_flag
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/col_addr_valid_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_br_boundary_ok_d
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_ctr_mv_field_pred_flag_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_ctr_mv_field_pred_flag_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_mv_field_pred_flag_l0_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_mv_field_pred_flag_l1_out
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/state_temporal
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_br_boundary_ok_d
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_cand_num
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/col_ybr_addr
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/col_xbr_addr
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_x_addr
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_y_addr
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/yy_pb_hh2_add
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/yy_pb
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/hh_pb
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/collocated_dpb_idx
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/state_axi_read
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/mv_pref_axi_araddr
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_derive_state
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/state_temporal
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/axi_ctu_read_done_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/collocated_dpb_idx
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/y_ctb_addr
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/x_ctb_addr
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/new_ctu_start_in
add wave -noupdate -expand -group ctu_pref_mem /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/col_pref_ctu_mem/col_pref_mem
add wave -noupdate -expand -group ctu_pref_mem /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/col_pref_ctu_mem/enable
add wave -noupdate -expand -group ctu_pref_mem /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/col_pref_ctu_mem/r_addr_in
add wave -noupdate -expand -group ctu_pref_mem /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/col_pref_ctu_mem/r_data_out
add wave -noupdate -expand -group ctu_pref_mem /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/col_pref_ctu_mem/w_addr_in
add wave -noupdate -expand -group ctu_pref_mem /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/col_pref_ctu_mem/w_data_in
add wave -noupdate -expand -group ctu_pref_mem /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/col_pref_ctu_mem/w_en_in
add wave -noupdate -group mv_pref_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/mv_pref_axi_rvalid
add wave -noupdate -group mv_pref_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/mv_pref_axi_rresp
add wave -noupdate -group mv_pref_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/mv_pref_axi_rready
add wave -noupdate -group mv_pref_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/mv_pref_axi_rlast
add wave -noupdate -group mv_pref_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/mv_pref_axi_rdata
add wave -noupdate -group mv_pref_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/mv_pref_axi_araddr
add wave -noupdate -group mv_pref_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/mv_pref_axi_arvalid
add wave -noupdate -group mv_pref_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/mv_pref_axi_arsize
add wave -noupdate -group mv_pref_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/mv_pref_axi_arready
add wave -noupdate -group mv_pref_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/mv_pref_axi_arprot
add wave -noupdate -group mv_pref_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/mv_pref_axi_arlock
add wave -noupdate -group mv_pref_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/mv_pref_axi_arlen
add wave -noupdate -group mv_pref_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/mv_pref_axi_arid
add wave -noupdate -group mv_pref_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/mv_pref_axi_arcache
add wave -noupdate -group mv_pref_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/mv_pref_axi_arburst
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/state_axi_read
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/col_mv_field_r_data_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/col_mv_field_pred_flag_l0_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/col_mv_field_pred_flag_l1_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/col_mv_field_ref_idx_l0_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/col_mv_field_ref_idx_l1_out
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/col_mv_field_mv_x_l0_out
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/col_mv_field_mv_y_l0_out
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/col_mv_field_mv_x_l1_out
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/col_mv_field_mv_y_l1_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_mv_field_pred_flag_l0_to_inter
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_mv_field_pred_flag_l1_to_inter
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_mv_field_mv_x_l0_to_inter
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_mv_field_mv_y_l0_to_inter
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_mv_field_mv_y_l0_out
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/state_temporal
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_br_boundary_ok_d
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_mv_field_mv_y_l1_to_inter
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_mv_field_mv_x_l1_to_inter
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_wvalid
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_wstrb
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_wready
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_wlast
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_wdata_d
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_wdata
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_bvalid
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_bresp
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_bready
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_awvalid
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_awsize
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_awready
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_awprot
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_awlock
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_awlen
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_awid
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_awcache
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_awburst
add wave -noupdate -group col_mv_axi_inf /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/mv_buffer_block/mv_col_axi_awaddr
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_l0_poc_diff
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_l0_poc_diff_wire
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_pic
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/ref_idx_l0_col_poc_out
add wave -noupdate -group col_poc_l0_mem /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/dpb_idx_ref_poc_list0_mem/w_en_in
add wave -noupdate -group col_poc_l0_mem /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/dpb_idx_ref_poc_list0_mem/w_data_in
add wave -noupdate -group col_poc_l0_mem /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/dpb_idx_ref_poc_list0_mem/w_addr_in
add wave -noupdate -group col_poc_l0_mem /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/dpb_idx_ref_poc_list0_mem/r_data_out
add wave -noupdate -group col_poc_l0_mem /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/dpb_idx_ref_poc_list0_mem/r_addr_in
add wave -noupdate -group col_poc_l0_mem /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/mv_pref_buffer_block/dpb_idx_ref_poc_list0_mem/mem
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/from_top_ref_pic_list_poc_wr_en
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/from_top_ref_pic_list0_poc_wr_en
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/from_top_ref_pic_list1_poc_wr_en
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/tdl0_clipped
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/tdl1_clipped
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/tbl1_clipped
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/tbl0_clipped
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/distscale_factor_l0
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/state_temporal
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/available_col_l0_flag
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/available_col_l1_flag
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/list_l0_col
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/list_l1_col
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/cur_poc_l0_diff
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/cur_poc_l1_diff
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_l0_poc_diff_wire
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/col_l0_poc_diff
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_poc
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_poc_l0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_poc_l1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/tbl0
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/tbl1
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/merge_flag
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/col_mv_ready
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/distscale_factor_l1
add wave -noupdate -radix decimal /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/temporal_pred_block/distscale_factor_l1_clipped
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/axi_col_mv_frame_offet
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list0_idx_data_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list0_idx_data_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list1_idx_data_out
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/collocated_from_l0_flag
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/config_mode_in
add wave -noupdate /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/current_poc
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list1_poc_data_out
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list1_poc_addr
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list1_poc_wr_en
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list1_poc_data_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list0_poc_addr
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list0_poc_data_out
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list0_poc_data_in
add wave -noupdate -radix unsigned /pred_top_tb/uut/pred_block/inter_top_block/mv_derive_block/ref_pic_list0_poc_wr_en
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{col_br_x_doesntgo beyond_ctu} {2039349319 ps} 1} {{x_br y_br wrong} {2008935702 ps} 1} {{y_ctr wrong} {2054483280 ps} 1} {{col br_mv at x=184 y=32 wrong} {3884921844 ps} 1} {{poc=6 x=0 y=0 error} {6783554358 ps} 1} {{temporal switch to ctr at poc 4 x=184 y40} {2324944139 ps} 1}
quietly wave cursor active 5
configure wave -namecolwidth 231
configure wave -valuecolwidth 122
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {2324865981 ps} {2324960189 ps}
