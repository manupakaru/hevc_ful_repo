`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   00:45:40 12/11/2013
// Design Name:   luma_8_tap_filter
// Module Name:   D:/090250V/SEMESTER 7/HEVC decoder/HDL codes/pred_top/sim/filter_test.v
// Project Name:  prediction
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: luma_8_tap_filter
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module filter_test;
    `include "pred_def.v"
	// Inputs
	reg clk;
	reg reset;
	reg filt_type;
	reg [FILTER_PIXEL_WIDTH -1: 0]a_ref_pixel_in;
	reg [FILTER_PIXEL_WIDTH -1: 0]b_ref_pixel_in;
	reg [FILTER_PIXEL_WIDTH -1: 0]c_ref_pixel_in;
	reg [FILTER_PIXEL_WIDTH -1: 0]d_ref_pixel_in;
	reg [FILTER_PIXEL_WIDTH -1: 0]e_ref_pixel_in;
	reg [FILTER_PIXEL_WIDTH -1: 0]f_ref_pixel_in;
	reg [FILTER_PIXEL_WIDTH -1: 0]g_ref_pixel_in;
	reg [FILTER_PIXEL_WIDTH -1: 0]h_ref_pixel_in;

	// Outputs
	wire [13:0] pred_pixel_out;

	// Instantiate the Unit Under Test (UUT)
	luma_8_tap_filter uut (
		.clk(clk), 
		.reset(reset), 
		.filt_type(filt_type), 
		.a_ref_pixel_in(a_ref_pixel_in), 
		.b_ref_pixel_in(b_ref_pixel_in), 
		.c_ref_pixel_in(c_ref_pixel_in), 
		.d_ref_pixel_in(d_ref_pixel_in), 
		.e_ref_pixel_in(e_ref_pixel_in), 
		.f_ref_pixel_in(f_ref_pixel_in), 
		.g_ref_pixel_in(g_ref_pixel_in), 
		.h_ref_pixel_in(h_ref_pixel_in), 
		.pred_pixel_out(pred_pixel_out)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 0;
		filt_type = 1;
        @(posedge clk);
        @(posedge clk);
        @(posedge clk);
		a_ref_pixel_in = 50;
		b_ref_pixel_in = 50;
		c_ref_pixel_in = 49;
		d_ref_pixel_in = 45;
		e_ref_pixel_in = 47;
		f_ref_pixel_in = 50;
		g_ref_pixel_in = 57;
		h_ref_pixel_in = 67;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
  always 
    #10 clk = ~clk;
endmodule







