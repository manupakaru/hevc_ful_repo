`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:38:17 11/25/2013 
// Design Name: 
// Module Name:    invScanOrder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module invScanOrder(
    input [1:0] blk_size,
    input [1:0] scanIdx,
    input [2:0] x,
    input [2:0] y,
    output reg [5:0] invScanOrder
    );
	
	always @(*) begin
		case (blk_size) 
			0: begin
				case (scanIdx)
					0: begin
						invScanOrder = 6'd0;
					end
					1: begin
						invScanOrder = 6'd0;
					end
					2: begin
						invScanOrder = 6'd0;
					end
					default : invScanOrder = 6'dx;
				endcase
			end 
			1: begin
				case (scanIdx)
					0: begin
						case ( {x,y})
							0: invScanOrder = 0;
							1: invScanOrder = 1;
							8: invScanOrder = 2;
							9: invScanOrder = 3;
							default : invScanOrder = x;
						endcase
					end
					1: begin
						case ({x,y})
							0	: invScanOrder = 0;
							8	: invScanOrder = 1;
							1	: invScanOrder = 2;
							9	: invScanOrder = 3;
							default : invScanOrder = x;
						endcase
					end
					2: begin
						case ( {x,y})
							0	: invScanOrder = 0;
							1	: invScanOrder = 1;
							8	: invScanOrder = 2;
							9	: invScanOrder = 3;
							default : invScanOrder = x;
						endcase
					end
					default : invScanOrder = 6'dx;
				endcase
			end
			2: begin
				case (scanIdx)
					0: begin
						case ( {x,y})
							  0 : invScanOrder = 0;
							  1 : invScanOrder = 1;
							  8 : invScanOrder = 2;
							  2 : invScanOrder = 3;
							  9 : invScanOrder = 4;
							 16 : invScanOrder = 5;
							  3 : invScanOrder = 6;
							 10 : invScanOrder = 7;
							 17 : invScanOrder = 8;
							 24 : invScanOrder = 9;
							 11 : invScanOrder = 10;
							 18 : invScanOrder = 11;
							 25 : invScanOrder = 12;
							 19 : invScanOrder = 13;
							 26 : invScanOrder = 14;
							 27 : invScanOrder = 15;
							default : invScanOrder = x;
						endcase
					end
					1: begin
						case ({x,y})
							  0 : invScanOrder = 0;
							  8 : invScanOrder = 1;
							 16 : invScanOrder = 2;
							 24 : invScanOrder = 3;
							  1 : invScanOrder = 4;
							  9 : invScanOrder = 5;
							 17 : invScanOrder = 6;
							 25 : invScanOrder = 7;
							  2 : invScanOrder = 8;
							 10 : invScanOrder = 9;
							 18 : invScanOrder = 10;
							 26 : invScanOrder = 11;
							  3 : invScanOrder = 12;
							 11 : invScanOrder = 13;
							 19 : invScanOrder = 14;
							 27 : invScanOrder = 15;
							default : invScanOrder = x;
						endcase
					end
					2: begin
						case ( {x,y})
							  0 : invScanOrder = 0;
							  1 : invScanOrder = 1;
							  2 : invScanOrder = 2;
							  3 : invScanOrder = 3;
							  8 : invScanOrder = 4;
							  9 : invScanOrder = 5;
							 10 : invScanOrder = 6;
							 11 : invScanOrder = 7;
							 16 : invScanOrder = 8;
							 17 : invScanOrder = 9;
							 18 : invScanOrder = 10;
							 19 : invScanOrder = 11;
							 24 : invScanOrder = 12;
							 25 : invScanOrder = 13;
							 26 : invScanOrder = 14;
							 27 : invScanOrder = 15;
							default : invScanOrder = x;
						endcase
					end
					default : invScanOrder = 6'dx;
				endcase
			end
			3: begin
				case (scanIdx)
					0: begin
						case ( {x,y})
							  0 : invScanOrder = 0;
							  1 : invScanOrder = 1;
							  8 : invScanOrder = 2;
							  2 : invScanOrder = 3;
							  9 : invScanOrder = 4;
							 16 : invScanOrder = 5;
							  3 : invScanOrder = 6;
							 10 : invScanOrder = 7;
							 17 : invScanOrder = 8;
							 24 : invScanOrder = 9;
							  4 : invScanOrder = 10;
							 11 : invScanOrder = 11;
							 18 : invScanOrder = 12;
							 25 : invScanOrder = 13;
							 32 : invScanOrder = 14;
							  5 : invScanOrder = 15;
							 12 : invScanOrder = 16;
							 19 : invScanOrder = 17;
							 26 : invScanOrder = 18;
							 33 : invScanOrder = 19;
							 40 : invScanOrder = 20;
							  6 : invScanOrder = 21;
							 13 : invScanOrder = 22;
							 20 : invScanOrder = 23;
							 27 : invScanOrder = 24;
							 34 : invScanOrder = 25;
							 41 : invScanOrder = 26;
							 48 : invScanOrder = 27;
							  7 : invScanOrder = 28;
							 14 : invScanOrder = 29;
							 21 : invScanOrder = 30;
							 28 : invScanOrder = 31;
							 35 : invScanOrder = 32;
							 42 : invScanOrder = 33;
							 49 : invScanOrder = 34;
							 56 : invScanOrder = 35;
							 15 : invScanOrder = 36;
							 22 : invScanOrder = 37;
							 29 : invScanOrder = 38;
							 36 : invScanOrder = 39;
							 43 : invScanOrder = 40;
							 50 : invScanOrder = 41;
							 57 : invScanOrder = 42;
							 23 : invScanOrder = 43;
							 30 : invScanOrder = 44;
							 37 : invScanOrder = 45;
							 44 : invScanOrder = 46;
							 51 : invScanOrder = 47;
							 58 : invScanOrder = 48;
							 31 : invScanOrder = 49;
							 38 : invScanOrder = 50;
							 45 : invScanOrder = 51;
							 52 : invScanOrder = 52;
							 59 : invScanOrder = 53;
							 39 : invScanOrder = 54;
							 46 : invScanOrder = 55;
							 53 : invScanOrder = 56;
							 60 : invScanOrder = 57;
							 47 : invScanOrder = 58;
							 54 : invScanOrder = 59;
							 61 : invScanOrder = 60;
							 55 : invScanOrder = 61;
							 62 : invScanOrder = 62;
							 63 : invScanOrder = 63;
							default : invScanOrder = x;
						endcase
					end
					1: begin
						case ({x,y})
							  0 : invScanOrder = 0;
							  8 : invScanOrder = 1;
							 16 : invScanOrder = 2;
							 24 : invScanOrder = 3;
							 32 : invScanOrder = 4;
							 40 : invScanOrder = 5;
							 48 : invScanOrder = 6;
							 56 : invScanOrder = 7;
							  1 : invScanOrder = 8;
							  9 : invScanOrder = 9;
							 17 : invScanOrder = 10;
							 25 : invScanOrder = 11;
							 33 : invScanOrder = 12;
							 41 : invScanOrder = 13;
							 49 : invScanOrder = 14;
							 57 : invScanOrder = 15;
							  2 : invScanOrder = 16;
							 10 : invScanOrder = 17;
							 18 : invScanOrder = 18;
							 26 : invScanOrder = 19;
							 34 : invScanOrder = 20;
							 42 : invScanOrder = 21;
							 50 : invScanOrder = 22;
							 58 : invScanOrder = 23;
							  3 : invScanOrder = 24;
							 11 : invScanOrder = 25;
							 19 : invScanOrder = 26;
							 27 : invScanOrder = 27;
							 35 : invScanOrder = 28;
							 43 : invScanOrder = 29;
							 51 : invScanOrder = 30;
							 59 : invScanOrder = 31;
							  4 : invScanOrder = 32;
							 12 : invScanOrder = 33;
							 20 : invScanOrder = 34;
							 28 : invScanOrder = 35;
							 36 : invScanOrder = 36;
							 44 : invScanOrder = 37;
							 52 : invScanOrder = 38;
							 60 : invScanOrder = 39;
							  5 : invScanOrder = 40;
							 13 : invScanOrder = 41;
							 21 : invScanOrder = 42;
							 29 : invScanOrder = 43;
							 37 : invScanOrder = 44;
							 45 : invScanOrder = 45;
							 53 : invScanOrder = 46;
							 61 : invScanOrder = 47;
							  6 : invScanOrder = 48;
							 14 : invScanOrder = 49;
							 22 : invScanOrder = 50;
							 30 : invScanOrder = 51;
							 38 : invScanOrder = 52;
							 46 : invScanOrder = 53;
							 54 : invScanOrder = 54;
							 62 : invScanOrder = 55;
							  7 : invScanOrder = 56;
							 15 : invScanOrder = 57;
							 23 : invScanOrder = 58;
							 31 : invScanOrder = 59;
							 39 : invScanOrder = 60;
							 47 : invScanOrder = 61;
							 55 : invScanOrder = 62;
							 63 : invScanOrder = 63;
							default : invScanOrder = x;
						endcase
					end
					2: begin
						case ( {x,y})
							  0 : invScanOrder = 0;
							  1 : invScanOrder = 1;
							  2 : invScanOrder = 2;
							  3 : invScanOrder = 3;
							  4 : invScanOrder = 4;
							  5 : invScanOrder = 5;
							  6 : invScanOrder = 6;
							  7 : invScanOrder = 7;
							  8 : invScanOrder = 8;
							  9 : invScanOrder = 9;
							 10 : invScanOrder = 10;
							 11 : invScanOrder = 11;
							 12 : invScanOrder = 12;
							 13 : invScanOrder = 13;
							 14 : invScanOrder = 14;
							 15 : invScanOrder = 15;
							 16 : invScanOrder = 16;
							 17 : invScanOrder = 17;
							 18 : invScanOrder = 18;
							 19 : invScanOrder = 19;
							 20 : invScanOrder = 20;
							 21 : invScanOrder = 21;
							 22 : invScanOrder = 22;
							 23 : invScanOrder = 23;
							 24 : invScanOrder = 24;
							 25 : invScanOrder = 25;
							 26 : invScanOrder = 26;
							 27 : invScanOrder = 27;
							 28 : invScanOrder = 28;
							 29 : invScanOrder = 29;
							 30 : invScanOrder = 30;
							 31 : invScanOrder = 31;
							 32 : invScanOrder = 32;
							 33 : invScanOrder = 33;
							 34 : invScanOrder = 34;
							 35 : invScanOrder = 35;
							 36 : invScanOrder = 36;
							 37 : invScanOrder = 37;
							 38 : invScanOrder = 38;
							 39 : invScanOrder = 39;
							 40 : invScanOrder = 40;
							 41 : invScanOrder = 41;
							 42 : invScanOrder = 42;
							 43 : invScanOrder = 43;
							 44 : invScanOrder = 44;
							 45 : invScanOrder = 45;
							 46 : invScanOrder = 46;
							 47 : invScanOrder = 47;
							 48 : invScanOrder = 48;
							 49 : invScanOrder = 49;
							 50 : invScanOrder = 50;
							 51 : invScanOrder = 51;
							 52 : invScanOrder = 52;
							 53 : invScanOrder = 53;
							 54 : invScanOrder = 54;
							 55 : invScanOrder = 55;
							 56 : invScanOrder = 56;
							 57 : invScanOrder = 57;
							 58 : invScanOrder = 58;
							 59 : invScanOrder = 59;
							 60 : invScanOrder = 60;
							 61 : invScanOrder = 61;
							 62 : invScanOrder = 62;
							 63 : invScanOrder = 63;
							default : invScanOrder = x;
						endcase
					end
					default : invScanOrder = 6'dx;
				endcase
			end
		endcase 
	
	end

endmodule
