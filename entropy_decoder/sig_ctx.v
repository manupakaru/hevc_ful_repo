`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:54:46 12/28/2013 
// Design Name: 
// Module Name:    sig_ctx 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module sig_ctx(
		xC,
		yC,
		prevCsbf,
		cIdx,
		log2TrafoSize,
		scanIdx,
		
		sig_ctx

    );
	
	// significant_coeff_flag
	reg [1:0] sig_ctx_1;
	reg [4:0] sig_ctx_add;
	output reg [4:0] sig_ctx;
	input [4:0] xC, yC;
	wire  [1:0] xP, yP;
	input [1:0] prevCsbf;
	input [1:0] cIdx;
	input [2:0] log2TrafoSize;
	input [1:0] scanIdx;
	wire [2:0] xP_yP;
	//assign xC = se_param[18:14]; assign yC = se_param[13:9];
	assign xP = xC[1:0]; assign yP = yC[1:0];
	assign xP_yP = xP + yP;
	always @ (*) begin
		case (prevCsbf) //prevCsbf 
			0: sig_ctx_1 = (xP_yP==0) ? 2'd2 : (xP_yP < 3'd3)? 2'd1 : 2'd0;
			1: sig_ctx_1 = (yP==0) ? 2'd2: (yP==1) ? 2'd1 : 2'd0;
			2: sig_ctx_1 = (xP==0) ? 2'd2: (xP==1) ? 2'd1 : 2'd0;
			3: sig_ctx_1 = 2'd2;
		endcase
	end
	always @(*) begin
		if (cIdx == 2'b0) begin
			if (xC[4:2] == 3'd0 & yC[4:2] == 3'd0) begin
				sig_ctx_add = (log2TrafoSize ==3'd3) ? ( (scanIdx==0) ? 5'd9 : 5'd15 ) : 5'd21;
			end else begin
				sig_ctx_add = (log2TrafoSize ==3'd3) ? ( (scanIdx==0) ? 5'd12 : 5'd18 ) : 5'd24;
			end
		end else begin
			sig_ctx_add = (log2TrafoSize == 3'd3) ? 5'd9 : 5'd12;
		end
	end
	always @(*) begin
		if (log2TrafoSize == 3'd2) begin
			case ( {yC[1:0] , xC[1:0] }  )
				0: sig_ctx = 5'd0;
				1: sig_ctx = 5'd1;
				2: sig_ctx = 5'd4;
				3: sig_ctx = 5'd5;
				4: sig_ctx = 5'd2;
				5: sig_ctx = 5'd3;
				6: sig_ctx = 5'd4;
				7: sig_ctx = 5'd5;
				8: sig_ctx = 5'd6;
				9: sig_ctx = 5'd6;
				10: sig_ctx = 5'd8;
				11: sig_ctx = 5'd8;
				12: sig_ctx = 5'd7;
				13: sig_ctx = 5'd7;
				14: sig_ctx = 5'd8;
				default: sig_ctx = 5'dx;
			endcase
		end else if (xC ==0 & yC==0) begin
			sig_ctx = 5'd0;
		end else begin
			sig_ctx = sig_ctx_1 + sig_ctx_add;
		end
	end

endmodule
