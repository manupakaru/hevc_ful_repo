/*
 * defines.h
 *
 *  Created on: Nov 17, 2013
 *      Author: Maleen
 */

#ifndef DEFINES_H_
#define DEFINES_H_

#define MAX_CTU 4096*16
#define MAX_WIDTH_CTU 256
#define MAX_REF_PICS 16
#define MAX_RPS 17
#define MAX_TILE_COLS 11
#define MAX_TILE_ROWS 11


typedef struct {
	int general_profile_space;
	int general_tier_flag;
	int general_profile_idc;
	unsigned int general_profile_compatibility;
	int general_level_idc;
} profile_tier_level_struct;

typedef struct {
	unsigned NumNegativePics;
	unsigned NumPositivePics;
	char UsedByCurrPicS0[MAX_REF_PICS];
	char UsedByCurrPicS1[MAX_REF_PICS];
	char DeltaPocS0[MAX_REF_PICS];
	char DeltaPocS1[MAX_REF_PICS];
	unsigned char NumDeltaPocs;
} short_term_ref_pic_set_struct;


typedef struct {
	int vps_video_parameter_set_id;
	int vps_max_sub_layers_minus1;
	int vps_temporal_id_nesting_flag;
	profile_tier_level_struct vps_profile_tier_level;
	int vps_sub_layer_ordering_info_present_flag;
	int vps_max_dec_pic_buffering; // ideally these should be arrays. But we are using only 1 sub pic
	int vps_max_num_reorder_pics;
	int vps_max_latency_increase;
	int vps_max_layer_id;
	int vps_num_layer_sets_minus1;

	int vps_timing_info_present_flag;
	int vps_num_hrd_parameters;
	int vps_max_nuh_reserved_zero_layer_id;
	//int vps_num_op_sets_minus1;
	int vps_extension_flag;
} video_param_struct;

typedef struct {
	short sps_video_parameter_set_id;
	short sps_max_sub_layers_minus1;
	short sps_temporal_sub_layer_nesting_flag;
	profile_tier_level_struct sps_profile_tier_level;
	short sps_seq_parameter_set_id;
	short chroma_format_idc;
	short pic_width_in_luma_samples;
	short pic_height_in_luma_samples;
	short conformance_window_flag;
		short conf_win_left_offset;
		short conf_win_right_offset;
		short conf_win_top_offset;
		short conf_win_bottom_offset;
	short bit_depth_luma_minus8;
	short bit_depth_chroma_minus8;
	short log2_max_pic_order_cnt_lsb_minus4;
	short sps_sub_layer_ordering_info_present_flag;
		short sps_max_dec_pic_buffering; // ideally these should be arrays. But we are using only 1 sub pic
		short sps_max_num_reorder_pics;
		short sps_max_latency_increase;
	short log2_min_luma_coding_block_size_minus3;
	short log2_diff_max_min_luma_coding_block_size;
	short log2_min_transform_block_size_minus2;
	short log2_diff_max_min_transform_block_size;
	short max_transform_hierarchy_depth_inter;
	short max_transform_hierarchy_depth_intra;
	short scaling_list_enable_flag;
	short amp_enabled_flag; // asymetric motion partitions
	short sample_adaptive_offset_enabled_flag;
	short pcm_enabled_flag;
			short pcm_sample_bit_depth_luma_minus1;
			short pcm_sample_bit_depth_chroma_minus1;
			short log2_min_pcm_luma_coding_block_size_minus3;
			short log2_diff_max_min_pcm_luma_coding_block_size;
			short pcm_loop_filter_disable_flag;
			short PCMBitDepthY;
			short PCMBitDepthC;
			short Log2MinIpcmCbSizeY;
			short Log2MaxIpcmCbSizeY;
	short num_short_term_ref_pic_sets;
	short long_term_ref_pics_present_flag;
	short sps_temporal_mvp_enable_flag;
	short strong_intra_smoothing_enable_flag;
	short vui_parameters_present_flag;
	short sps_extension_flag;

	short_term_ref_pic_set_struct stRPS[MAX_RPS];

	int Log2MinCbSizeY;
	int Log2CtbSizeY;
	int PicWidthInCtbsY;
	int PicHeightInCtbsY;
	int PicSizeInCtbsY;
	int Log2MinTrafoSize;
	int Log2MaxTrafoSize;
	int MaxTrafoDepth;
	int Log2MinCuQpDeltaSize;
} sequence_param_struct;

typedef struct {
	int pps_pic_parameter_set_id;
	int pps_seq_parameter_set_id ;
	int dependent_slice_segments_enabled_flag;
	int output_flag_present_flag;
	short sign_data_hiding_flag;
	short cabac_init_present_flag;
	short num_ref_idx_l0_default_active_minus1;
	short num_ref_idx_l1_default_active_minus1;
	short init_qp_minus26;
	short constrained_intra_pred_flag;
	int transform_skip_enabled_flag;
	short cu_qp_delta_enabled_flag;
	short diff_cu_qp_delta_depth;
	short pps_cb_qp_offset;
	short pps_cr_qp_offset;
	short pps_slice_chroma_qp_offsets_present_flag;
	short weighted_pred_flag;
	short weighted_bipred_flag;
	short transquant_bypass_enable_flag;
	short tiles_enabled_flag;
	short num_tile_columns_minus1;
	short num_tile_rows_minus1;
		short column_width_minus1[9];
		short row_height_minus1[9];
	short uniform_spacing_flag;
	short loop_filter_across_tiles_enabled_flag;
	short entropy_coding_sync_enabled_flag;
	short loop_filter_across_slices_enabled_flag;
	short deblocking_filter_control_present_flag;
	short pps_deblocking_filter_override_enabled_flag;
	short pps_disable_deblocking_filter_flag;
	short pps_beta_offset_div2;
	short pps_tc_offset_div2;
	short pps_scaling_list_data_present_flag;
	short lists_modification_present_flag;
	short log2_parallel_merge_level_minus2;
	short num_extra_slice_header_bits;
	short slice_segment_header_extension_present_flag;
	short pps_extension_flag;
} picture_param_struct;


typedef struct {
//	int IdrPicFlag;
//	int RapPicFlag; // random access picture nal_unit_type -(16,31)
	int first_slice_segment_in_pic_flag;
	int no_output_of_prior_pics_flag;
	int slice_pic_parameter_set_id;
	int dependent_slice_segment_flag;
	int slice_segment_address;
	int slice_type;
	int pic_output_flag;
	int pic_order_cnt_lsb;
	int short_term_ref_pic_set_sps_flag;
	int short_term_ref_pic_set_idx;
	int slice_temporal_mvp_enable_flag;

	int slice_sao_luma_flag;
	int slice_sao_chroma_flag;

	int num_ref_idx_active_override_flag;
	int num_ref_idx_l0_active_minus1;
	int num_ref_idx_l1_active_minus1;
	int mvd_l1_zero_flag;
	int cabac_init_flag;
	int collocated_from_l0_flag;
	int collocated_ref_idx;
	int five_minus_max_num_merge_cand;

	int slice_qp_delta;
	int slice_cb_qp_offset;
	int slice_cr_qp_offset;
	int deblocking_filter_override_flag;
	int slice_disable_deblocking_filter_flag;
	int slice_beta_offset_div2;
	int slice_tc_offset_div2;
	int slice_loop_filter_across_slices_enabled_flag;
	int num_entry_point_offsets;
} slice_segment_struct;

#define TRAIL_N 0
#define TRAIL_R 1
#define RASL_R 9
#define IDR_W_DLP 19
#define CRA_NUT 21
#define NAL_VPS_NUT 32
#define NAL_SPS_NUT 33
#define NAL_PPS_NUT 34

#define MODE_INTRA 0
#define MODE_INTER 1
#define MODE_SKIP 2

#define PART_2Nx2N 0
#define PART_2NxN 1
#define PART_Nx2N 2
#define PART_NxN 3
#define PART_2NxnU 4
#define PART_2NxnD 5
#define PART_nLx2N 6
#define PART_nRx2N 7

#define PRED_L0 0
#define PRED_L1 1
#define PRED_BI 2

#define SLICE_B 0
#define SLICE_P 1
#define SLICE_I 2

short IdrPicFlag;
short RapPicFlag;
short RefPicture;
short prevPicOrderCntMsb, prevPicOrderCntLsb;

short ctbAddrInRS;
short ctbAddrInTS;
short ctbAddrshortS;
short SliceQPY;

short MaxNumMergeCand;

unsigned char colWidth[MAX_TILE_COLS];
unsigned char rowHeight[MAX_TILE_ROWS];
unsigned char colBd[MAX_TILE_COLS+1];
unsigned char rowBd[MAX_TILE_ROWS+1];

short firstCtbTS[MAX_TILE_ROWS][MAX_TILE_COLS];
short firstCtbRS[MAX_TILE_ROWS][MAX_TILE_COLS];
//unsigned short CtbAddrRstoTs[MAX_CTU];

//unsigned char TileId[MAX_CTU];


short curr_slice;
short curr_tile;
short tile_i, tile_j;

int SliceAddrRS;

int xCtb, yCtb;

int available_left(int xCurr);
int available_up(int yCurr);

int warning_temp; // used to stop unused variable warnings
int header_save;

short decodeNAL();
void video_parameter_set_rbsp(video_param_struct* vps);
void sequence_parameter_set_rbsp();
void picture_parameter_set_rbsp();
void profile_tier_level(profile_tier_level_struct* ptl, int profilePresentFlag, int maxNumSubLayersMinus1);
void bit_rate_pic_rate_info(void* brpr,int TempLevelLow, int TempLevelHigh );
void pred_weight_table();
void scaling_list_data();
void short_term_ref_pic_set(sequence_param_struct* sps,int  idxRps);
void pred_weight_table();
void slice_segment_layer_rbsp(slice_segment_struct* ssl);
void coding_tree_unit();
void sao(int rx, int ry);
void coding_quadtree(int x0, int y0, int log2CbSize, int cqtDepth);
void coding_quadtree_6(int x0, int y0);
void coding_quadtree_5(int x0, int y0);
void coding_quadtree_4(int x0, int y0);
void coding_quadtree_3(int x0, int y0);
void coding_unit(int x0,int y0, int log2CbSize);
void prediction_unit(int x0, int y0, int nPbW, int nPbH, int partIdx);
inline void transform_tree_D0(int x0, int y0, int xBase, int yBase, int log2TrafoSize, int blkIdx);
inline void transform_tree_D1(int x0, int y0, int xBase, int yBase, int log2TrafoSize, int blkIdx);
inline void transform_tree_D2(int x0, int y0, int xBase, int yBase, int log2TrafoSize, int blkIdx);
inline void transform_unit(int x0, int y0, int xBase, int yBase, int log2TrafoSize, int trafoDepth, int blkIdx);
void residual_coding(int x0, int y0, int log2TrafoSize, int cIdx);

void send_new_tile();

int derive_intra_mode(int xB, int yB,int prev_intra_luma_pred_flag,int mpm_idx,int rem_intra_luma_pred_mode, int intra_chroma_pred_mode);

int available_z(int xCurr,int  yCurr,int  xN,int  yN);
void send_tu_residual_v2(unsigned short xB,unsigned short  yB, int log2TrafoSize,int cidx, unsigned char mode, int res_present);

short read_ue();
short read_se();
short read_bits(unsigned char n);
void byte_alignment();
void byte_alignment_if();
unsigned char log2_int(unsigned short v);

void initialize_blocks();

#define SKIP_TO_NAL() *((volatile u32* ) 0xC0000000) = 0
#define READ_BITS_1 *((volatile u32* ) 0xC0000100)
#define READ_BITS_2 *((volatile u32* ) 0xC0000200)
#define READ_BITS_3 *((volatile u32* ) 0xC0000300)
#define READ_BITS_4 *((volatile u32* ) 0xC0000400)
#define READ_BITS_5 *((volatile u32* ) 0xC0000500)
#define READ_BITS_6 *((volatile u32* ) 0xC0000600)
#define READ_BITS_7 *((volatile u32* ) 0xC0000700)
#define READ_BITS_8 *(( volatile u32* ) 0xC0000800)
#define EXIT() *((volatile u32* ) 0xF0000000) = 0

#define read_u(x) READ_BITS_##x
#define SEND(x) *((volatile u32*) 0xC0004000 ) = *((u32*) & x )
#define CABAC_REQ(x) *((volatile u32* ) 0xC0001000) = (x);
#define CABAC_READ() *((volatile u32* ) 0xC0002000)
#define G0_OUT(x) *((volatile u32* ) 0x80000010) = (x);


#define SET_H30(x) *((volatile u32* ) 0xD0003000) = (x);
#define PUSH_H30() *((volatile u32* ) 0xD0003010) = (0)
#define SET_H40(x) *((volatile u32* ) 0xD0004000) = (x);
#define PUSH_H40() *((volatile u32* ) 0xD0004020) = (0)
#define PUSH_H41() *((volatile u32* ) 0xD0004110) = (0)

int ReferenceScalingList_012[64] = { 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 17, 16, 17, 16, 17, 18,
									 17, 18, 18, 17, 18, 21, 19, 20, 21, 20, 19, 21, 24, 22, 22, 24,
									 24, 22, 22, 24, 25, 25, 27, 30, 27, 25, 25, 29, 31, 35, 35, 31,
									 29, 36, 41, 44, 41, 36, 47, 54, 54, 45, 65, 70, 65, 88, 88, 115 };
int ReferenceScalingList_345[64] =   { 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17, 17, 17, 18,
									   18, 18, 18, 18, 18, 20, 20, 20, 20, 20, 20, 20, 24, 24, 24, 24,
									   24, 24, 24, 24, 25, 25, 25, 25, 25, 25, 25, 25, 28, 28, 28, 28,
									   28, 33, 33, 33, 33, 33, 41, 41, 41, 41, 54, 54, 54, 71, 71, 91 };


struct parameters_0 {
	unsigned header: 8;
	unsigned pic_width: 12;
	unsigned pic_height: 12;
};
struct parameters_1 {
	unsigned header :8;
	unsigned Log2CtbSizeY : 3;
	signed pps_cb_qp_offset :5;
	signed pps_cr_qp_offset :5;
	signed pps_beta_offset_div2 : 4;
	signed pps_tc_offset_div2: 4;
	unsigned strong_intra_smoothing :1;
	unsigned constrained_intra_pred : 1;
	unsigned pcm_loop_filter_disable_flag : 1;
};
struct parameters_2 {
	unsigned header :8;
	unsigned num_short_term_ref : 6;
	unsigned weighted_pred:1;
	unsigned weighted_bipred:1;
	unsigned parallel_merge_level:3;
	unsigned loop_filter_across_tiles_enabled_flag :1;
	unsigned scaling_list_enable_flag : 1;
};
struct parameters_3 {
	unsigned header :8;
	unsigned rps_id: 8;
	unsigned num_positive:8;
	unsigned num_negative:8;
};
struct parameters_4 {
	unsigned header :8;
	unsigned rps_id: 7;
	unsigned used:1;
	signed delta_POC:16;
};

struct scaling_list_pkt {
	unsigned header : 8;
	unsigned sizeId : 4;
	unsigned matrixId : 4;
	unsigned i : 8;
	unsigned val : 8;

};
struct slice_0 {
	unsigned header : 8;
	unsigned null:24;
	//signed POC: 32;
};
int POC;
struct new_slice_tile{
	unsigned header : 8;
	unsigned xC : 12;
	unsigned yC : 12;
};
struct new_tile_width {
	unsigned header :8;
	unsigned width :16;
};
struct slice_1{
	unsigned header :8;
	unsigned short_term_ref_pic_sps : 1;
	unsigned short_term_ref_pic_idx: 4;
	unsigned temporal_mvp_enabled : 1;
	unsigned sao_luma : 1;
	unsigned sao_chroma :1;
	unsigned num_ref_idx_l0_minus1 : 4;
	unsigned num_ref_idx_l1_minus1 : 4;
	unsigned five_minus_max_merge_cand : 3;
	unsigned slice_type :2;
	unsigned collocated_from_l0:1;
	unsigned slice_loop_filter_across_slices_enabled_flag: 1;
};
struct slice_2 {
	unsigned header :8;
	signed slice_cb_qp_offset:5;
	signed slice_cr_qp_offset:5;
	unsigned disable_dbf : 1;
	signed slice_beta_offset_div2 : 4;
	signed slice_tc_offset_div2 : 4;
	unsigned collocated_ref_idx : 5;
};
struct slice_3 {
	unsigned header :8;
	unsigned rps_id: 8;
	unsigned num_positive:8;
	unsigned num_negative:8;
};
struct slice_4 {
	unsigned header :8;
	unsigned rps_id: 7;
	unsigned used:1;
	signed delta_POC:16;
};
struct pred_weight_1 {
	unsigned header :8;
	unsigned luma_log2_weight_denom : 3;
	unsigned ChromaLog2WeightDenom  : 3;
};
struct pred_weight_2 {
	unsigned header : 8;
	unsigned ref_pic : 4;
	unsigned list : 1;
	unsigned cIdx : 2;
	signed Weight : 9;
	signed Offset : 8;
};
struct CTU_0 {
	unsigned header : 8;
	unsigned xC :12;
	unsigned yC:12;
};
struct CTU_1 {
	unsigned header : 8;
	unsigned tile_id : 8;
	unsigned slice_id : 8;
};
struct CTU_2 {
	unsigned header :8;
	unsigned SaoType:2;
	unsigned BandPos_EO : 6;
	unsigned sao_offset_abs_0 :3 ;
	unsigned sao_offset_sign_0 :1;
	unsigned sao_offset_abs_1 :3 ;
	unsigned sao_offset_sign_1 :1;
	unsigned sao_offset_abs_2 :3 ;
	unsigned sao_offset_sign_2 :1;
	unsigned sao_offset_abs_3 :3 ;
	unsigned sao_offset_sign_3 :1;
};



//struct RU_0 h_40;
#endif /* DEFINES_H_ */
