`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:50:03 04/09/2014
// Design Name:   inv_transform
// Module Name:   C:/Users/Maleen/Uni/HEVC/repo/sources/hevc_maleen/inv_transform/test_inv_transform.v
// Project Name:  hevc_top
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: inv_transform
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_inv_transform;

	// Inputs
	reg clk;
	reg rst;
	reg [31:0] read_in;
	reg read_empty;
	reg fullConfig;
	reg fullY;
	reg fullCb;
	reg fullCr;

	// Outputs
	wire read_next;
	wire [31:0] outConfig;
	wire validConfig;
	wire [143:0] outY;
	wire validY;
	wire [143:0] outCb;
	wire validCb;
	wire [143:0] outCr;
	wire validCr;
	
	
	integer fd;
	integer line;
	integer   fdConfig, fdY, fdCb, fdCr;
	
	reg [8:0] read_in_res [0:3][0:3];
	reg [31:0] read_in_config;
	reg [7:0] null;
	reg matchedY, matchedCb, matchedCr, matchedConfig, matched;
	
	
	integer wait_cycles;

	// Instantiate the Unit Under Test (UUT)
	inv_transform uut (
		.clk(clk), 
		.rst(rst), 
		.read_in(read_in), 
		.read_next(read_next), 
		.read_empty(read_empty), 
		.outConfig(outConfig), 
		.validConfig(validConfig), 
		.fullConfig(fullConfig), 
		.outY(outY), 
		.validY(validY), 
		.fullY(fullY), 
		.outCb(outCb), 
		.validCb(validCb), 
		.fullCb(fullCb), 
		.outCr(outCr), 
		.validCr(validCr), 
		.fullCr(fullCr)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0;
		read_in = 0;
		read_empty = 0;
		fullConfig = 0;
		fullY = 0;
		fullCb = 0;
		fullCr = 0;
		
		wait_cycles = 0;

		
		fd = $fopen("../cabac_to_residual_soft","rb");
		fdConfig = $fopen("../residual_to_inter","rb");
		fdY = $fopen("../residual_to_inter_Y","rb");
		fdCb = $fopen("../residual_to_inter_Cb","rb");
		fdCr = $fopen("../residual_to_inter_Cr","rb");
		matchedY = 1;
		matchedCb = 1;
		matchedCr = 1;
		matchedConfig =1;
		
		read_in[ 7: 0] = $fgetc(fd);
		read_in[15: 8] = $fgetc(fd);
		read_in[23:16] = $fgetc(fd);
		read_in[31:24] = $fgetc(fd);

		line = 0;
		// Wait 100 ns for global reset to finish
		#98;
        
		// Add stimulus here
		rst = 1;

	end
	
	always begin
		#5 clk = ~clk;
	end
	
	always @ (posedge clk) begin
		if ( read_next ) begin
			if (wait_cycles == 3) begin
				read_in[ 7: 0] = $fgetc(fd);
				read_in[15: 8] = $fgetc(fd);
				read_in[23:16] = $fgetc(fd);
				read_in[31:24] = $fgetc(fd);		
					
				line = line +4;
				
				wait_cycles = 0;
				read_empty = 0;
			end else begin
				wait_cycles = wait_cycles+1;
				read_empty = 1;
			end
		end else begin
			if (wait_cycles >0 ) begin
				if (wait_cycles == 1) begin
					read_in[ 7: 0] = $fgetc(fd);
					read_in[15: 8] = $fgetc(fd);
					read_in[23:16] = $fgetc(fd);
					read_in[31:24] = $fgetc(fd);		
						
					line = line +4;
					
					wait_cycles = 0;
					read_empty = 0;
				end else begin
					wait_cycles = wait_cycles+1;
					read_empty = 1;
				end
			end
		end
	end
	
	always @(posedge clk) begin
		if (validY) begin
			read(fdY);
			compare(outY,matchedY);
		end
		if (validCb) begin
			read(fdCb);
			compare(outCb,matchedCb);
		end
		if (validCr) begin
			read(fdCr);
			compare(outCr,matchedCr);
		end
		if (validConfig) begin
			read_in_config[ 7: 0] = $fgetc(fdConfig);			
			read_in_config[15: 8] = $fgetc(fdConfig);
			read_in_config[23:16] = $fgetc(fdConfig);
			read_in_config[31:24] = $fgetc(fdConfig);
			if (read_in_config == outConfig) 
				matchedConfig = 1;
			else 
				matchedConfig = 0;
		end
		if (!matched)
		   $stop();
	end
		
	
	always @(*) 
		matched = matchedY & matchedCb & matchedCr & matchedConfig;
		
	
	task read;
		input integer f;
		begin
			read_in_res[0][0][7:0] = $fgetc(f);
			{read_in_res[1][0][6:0] , read_in_res[0][0][8]} = $fgetc(f);
			{read_in_res[2][0][5:0] , read_in_res[1][0][8:7]} = $fgetc(f);
			{null[4:0] , read_in_res[2][0][8:6]} = $fgetc(f);
			
			read_in_res[3][0][7:0] = $fgetc(f);
			{read_in_res[0][1][6:0] , read_in_res[3][0][8]} = $fgetc(f);
			{read_in_res[1][1][5:0] , read_in_res[0][1][8:7]} = $fgetc(f);
			{null[4:0] , read_in_res[1][1][8:6]} = $fgetc(f);
			
			read_in_res[2][1][7:0] = $fgetc(f);
			{read_in_res[3][1][6:0] , read_in_res[2][1][8]} = $fgetc(f);
			{read_in_res[0][2][5:0] , read_in_res[3][1][8:7]} = $fgetc(f);
			{null[4:0] , read_in_res[0][2][8:6]} = $fgetc(f);
			
			read_in_res[1][2][7:0] = $fgetc(f);
			{read_in_res[2][2][6:0] , read_in_res[1][2][8]} = $fgetc(f);
			{read_in_res[3][2][5:0] , read_in_res[2][2][8:7]} = $fgetc(f);
			{null[4:0] , read_in_res[3][2][8:6]} = $fgetc(f);
			
			read_in_res[0][3][7:0] = $fgetc(f);
			{read_in_res[1][3][6:0] , read_in_res[0][3][8]} = $fgetc(f);
			{read_in_res[2][3][5:0] , read_in_res[1][3][8:7]} = $fgetc(f);
			{null[4:0] , read_in_res[2][3][8:6]} = $fgetc(f);
			
			read_in_res[3][3][7:0] = $fgetc(f);
			{null[6:0] , read_in_res[3][3][8]} = $fgetc(f);
			null = $fgetc(f);
			null = $fgetc(f);
			
		end
	endtask
	
	task compare;
		input [143:0] sim;
		output OK;
		begin
			if ( sim == { read_in_res[0][0] ,read_in_res[0][1],read_in_res[0][2],read_in_res[0][3], read_in_res[1][0] ,read_in_res[1][1],read_in_res[1][2],read_in_res[1][3], read_in_res[2][0] ,read_in_res[2][1],read_in_res[2][2],read_in_res[2][3], read_in_res[3][0] ,read_in_res[3][1],read_in_res[3][2],read_in_res[3][3] } ) begin
				OK = 1;
			end else begin
				OK = 0;
			end
		end
	endtask
      
endmodule

