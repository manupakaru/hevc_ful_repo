`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   07:22:09 08/06/2014
// Design Name:   global_top
// Module Name:   G:/HEVC/axi_cabac/sources/entropy_decoder/testbenches/axi_test.v
// Project Name:  axi_cabac
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: global_top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module axi_test;

	// Inputs
	reg clk;
	reg rst;
	reg [15:0] axi_araddr;
	reg [1:0] axi_arburst;
	reg [3:0] axi_arcache;
	reg [11:0] axi_arid;
	reg [7:0] axi_arlen;
	reg axi_arlock;
	reg [2:0] axi_arprot;
	reg [2:0] axi_arsize;
	reg axi_arvalid;
	reg [15:0] axi_awaddr;
	reg [1:0] axi_awburst;
	reg [3:0] axi_awcache;
	reg [11:0] axi_awid;
	reg [7:0] axi_awlen;
	reg axi_awlock;
	reg [2:0] axi_awprot;
	reg [2:0] axi_awsize;
	reg axi_awvalid;
	reg axi_bready;
	reg axi_rready;
	reg [31:0] axi_wdata;
	reg axi_wlast;
	reg [3:0] axi_wstrb;
	reg axi_wvalid;
	reg fullCont;
	reg fullY;
	reg fullCb;
	reg fullCr;

	// Outputs
	wire axi_arready;
	wire axi_awready;
	wire [11:0] axi_bid;
	wire [1:0] axi_bresp;
	wire axi_bvalid;
	wire [31:0] axi_rdata;
	wire [11:0] axi_rid;
	wire axi_rlast;
	wire [1:0] axi_rresp;
	wire axi_rvalid;
	wire axi_wready;
	wire [31:0] outCont;
	wire validCont;
	wire [143:0] outY;
	wire validY;
	wire [143:0] outCb;
	wire validCb;
	wire [143:0] outCr;
	wire validCr;
	wire input_hevc_full_out_test;
	wire input_hevc_empty_out_test;
	wire input_hevc_almost_empty_out_test;
	wire push_full_out_test;
	wire push_empty_out_test;
	wire [31:0] B_test;
	wire [6:0] cu_state;
	wire [4:0] ru_state;
	wire [11:0] cu_x0;
	wire [11:0] cu_y0;
	wire [255:0] cu_other;
	wire [11:0] trafo_x0;
	wire [11:0] trafo_y0;
	wire [15:0] trafo_POC;
	wire trafo_clk_en;
	wire [9:0] cabac_to_res_data_count;
	wire [13:0] input_hevc_data_count;
	wire [31:0] cabac_to_res_data;
	wire cabac_to_res_wr_en;
	wire [31:0] push_data_out;
	wire push_empty;
	wire push_read;
	wire [27:0] port5;
	wire [27:0] port6;
	wire [26:0] port7;
	wire [71:0] port8;
	wire [5:0] port9;
	
	reg [7:0] null;
	reg [31:0] expected_read_data;
	
	
	integer fd_axi, fd_hevc;
	integer loc_axi, loc_hevc;
	
	integer fd_res, fd_Cont, fd_Y, fd_Cb, fd_Cr;
	integer loc_res, loc_Cont, loc_Y, loc_Cb, loc_Cr;
	
	reg [31:0] read_res, read_Cont;
	reg [8:0] read_in_res [0:3][0:3];
	reg matched_res, matched_Cont, matched_Y, matched_Cb, matched_Cr;
	
	integer rand, new_rand;
	
	reg in_read;
	integer read_counter;
	reg [31:0] input_counter;

	// Instantiate the Unit Under Test (UUT)
	global_top uut (
		.clk(clk), 
		.rst(rst), 
		.axi_araddr(axi_araddr), 
		.axi_arburst(axi_arburst), 
		.axi_arcache(axi_arcache), 
		.axi_arid(axi_arid), 
		.axi_arlen(axi_arlen), 
		.axi_arlock(axi_arlock), 
		.axi_arprot(axi_arprot), 
		.axi_arready(axi_arready), 
		.axi_arsize(axi_arsize), 
		.axi_arvalid(axi_arvalid), 
		.axi_awaddr(axi_awaddr), 
		.axi_awburst(axi_awburst), 
		.axi_awcache(axi_awcache), 
		.axi_awid(axi_awid), 
		.axi_awlen(axi_awlen), 
		.axi_awlock(axi_awlock), 
		.axi_awprot(axi_awprot), 
		.axi_awready(axi_awready), 
		.axi_awsize(axi_awsize), 
		.axi_awvalid(axi_awvalid), 
		.axi_bid(axi_bid), 
		.axi_bready(axi_bready), 
		.axi_bresp(axi_bresp), 
		.axi_bvalid(axi_bvalid), 
		.axi_rdata(axi_rdata), 
		.axi_rid(axi_rid), 
		.axi_rlast(axi_rlast), 
		.axi_rready(axi_rready), 
		.axi_rresp(axi_rresp), 
		.axi_rvalid(axi_rvalid), 
		.axi_wdata(axi_wdata), 
		.axi_wlast(axi_wlast), 
		.axi_wready(axi_wready), 
		.axi_wstrb(axi_wstrb), 
		.axi_wvalid(axi_wvalid), 
		.outCont(outCont), 
		.validCont(validCont), 
		.fullCont(fullCont), 
		.outY(outY), 
		.validY(validY), 
		.fullY(fullY), 
		.outCb(outCb), 
		.validCb(validCb), 
		.fullCb(fullCb), 
		.outCr(outCr), 
		.validCr(validCr), 
		.fullCr(fullCr), 
		.input_hevc_full_out_test(input_hevc_full_out_test), 
		.input_hevc_empty_out_test(input_hevc_empty_out_test), 
		.input_hevc_almost_empty_out_test(input_hevc_almost_empty_out_test), 
		.push_full_out_test(push_full_out_test), 
		.push_empty_out_test(push_empty_out_test), 
		.B_test(B_test), 
		.cu_state(cu_state), 
		.ru_state(ru_state), 
		.cu_x0(cu_x0), 
		.cu_y0(cu_y0), 
		.cu_other(cu_other), 
		.trafo_x0(trafo_x0), 
		.trafo_y0(trafo_y0), 
		.trafo_POC(trafo_POC), 
		.trafo_clk_en(trafo_clk_en), 
		.cabac_to_res_data_count(cabac_to_res_data_count), 
		.input_hevc_data_count(input_hevc_data_count), 
		.cabac_to_res_data(cabac_to_res_data), 
		.cabac_to_res_wr_en(cabac_to_res_wr_en), 
		.push_data_out(push_data_out), 
		.push_empty(push_empty), 
		.push_read(push_read), 
		.port5(port5), 
		.port6(port6), 
		.port7(port7), 
		.port8(port8), 
		.port9(port9)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0;
		axi_araddr = 0;
		axi_arburst = 0;
		axi_arcache = 0;
		axi_arid = 0;
		axi_arlen = 0;
		axi_arlock = 0;
		axi_arprot = 0;
		axi_arsize = 0;
		axi_arvalid = 0;
		axi_awaddr = 0;
		axi_awburst = 0;
		axi_awcache = 0;
		axi_awid = 0;
		axi_awlen = 0;
		axi_awlock = 0;
		axi_awprot = 0;
		axi_awsize = 0;
		axi_awvalid = 0;
		axi_bready = 1;
		axi_rready = 1;
		axi_wdata = 0;
		axi_wlast = 0;
		axi_wstrb = 0;
		axi_wvalid = 0;
		fullCont = 0;
		fullY = 0;
		fullCb = 0;
		fullCr = 0;
		
		loc_axi = 0;
		loc_hevc = 0;
		loc_res = 0;
		loc_Cont = 0;
		loc_Y=0;
		loc_Cb=0;
		loc_Cr=0;
		
		rand =0;
		
		in_read = 0;
		read_counter = 0;
		input_counter = 0;
		
		matched_res = 1;
		matched_Cont = 1;
		matched_Y = 1;
		matched_Cb = 1;
		matched_Cr = 1;
		fd_axi =  $fopen("G:/HEVC/axi_cabac/sim/axi_trans","rb"); 
		fd_hevc =  $fopen("G:/HEVC/axi_cabac/sim/1080_64ctu.hevc","rb"); 
		fd_res =  $fopen("G:/HEVC/axi_cabac/sim/cabac_to_residual_soft","rb");
		
		fd_Cont =  $fopen("G:/HEVC/axi_cabac/sim/residual_to_inter","rb");
		fd_Y =  $fopen("G:/HEVC/axi_cabac/sim/residual_to_inter_Y","rb");
		fd_Cb =  $fopen("G:/HEVC/axi_cabac/sim/residual_to_inter_Cb","rb");
		fd_Cr =  $fopen("G:/HEVC/axi_cabac/sim/residual_to_inter_Cr","rb");
		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		rst = 1;
		//read_res[31:24] = $fgetc(fd_res);
		//read_res[23:16] = $fgetc(fd_res);
		//read_res[15: 8] = $fgetc(fd_res);
		//read_res[ 7: 0] = $fgetc(fd_res);	
		//loc_res = loc_res+4;
		//rand = ({$random} >> 26)+1;
	end
	
	always begin
		#5 clk = ~clk;
	end
	reg [15:0] addr;
	
	always @(negedge clk) begin
		axi_awvalid = 1'b0;
		axi_arvalid = 1'b0;
		axi_wvalid = 1'b0;
		if (axi_awready) begin
			//rand = rand -1;
			if (rand ==0) begin
				if (in_read) begin
					if (read_counter > 100) begin
						axi_arvalid = 1'b1;
						axi_araddr = addr;
						read_counter = 0;
					end else begin
						read_counter = read_counter + 1;
					end
				end else begin			
					addr[ 7: 0] = $fgetc(fd_axi);
					addr[15: 8] = $fgetc(fd_axi);
					null = $fgetc(fd_axi);
					null = $fgetc(fd_axi);
					loc_axi = loc_axi + 4;
					
					in_read = (addr[11:8] == 4 || addr[11:8] == 9 || addr == 16'h0840) ? 1 :0 ;
					if (in_read) begin
						axi_arvalid = 1'b1;
						axi_araddr = addr;
						expected_read_data[ 7: 0] = $fgetc(fd_axi);
						expected_read_data[15: 8] = $fgetc(fd_axi);
						expected_read_data[23:16] = $fgetc(fd_axi);
						expected_read_data[31:24] = $fgetc(fd_axi);
						loc_axi = loc_axi + 4;
						in_read = 1;
					end else begin
						//new_rand = ({$random} >> 23)+1;
						//rand = new_rand;
						axi_awvalid = 1'b1;
						if (addr == 16'h0a00) begin
							axi_awaddr = 16'h1000;
							input_counter[ 7: 0] = $fgetc(fd_axi);
							input_counter[15: 8] = $fgetc(fd_axi);
							input_counter[23:16] = $fgetc(fd_axi);
							input_counter[31:24] = $fgetc(fd_axi);
							loc_axi = loc_axi + 4;
						end else begin
							axi_awaddr = addr;
						end
					end
					
				end
			end
		end else if (axi_wready) begin
			if (addr == 16'h0a00) begin
				axi_wdata[ 7: 0] = $fgetc(fd_hevc);
				axi_wdata[15: 8] = $fgetc(fd_hevc);
				axi_wdata[23:16] = $fgetc(fd_hevc);			
				axi_wdata[31:24] = $fgetc(fd_hevc);
				
				loc_hevc = loc_hevc + 4;
				input_counter = input_counter - 1;
				axi_wlast = (input_counter == 0) ? 1'b1 : 1'b0;
			end else begin
				axi_wdata[ 7: 0] = $fgetc(fd_axi);
				axi_wdata[15: 8] = $fgetc(fd_axi);
				axi_wdata[23:16] = $fgetc(fd_axi);
				axi_wdata[31:24] = $fgetc(fd_axi);
				loc_axi = loc_axi + 4;
				axi_wlast = 1'b1;
			end
			axi_wvalid = 1'b1;
			
		end else if (axi_arready) begin
			
		end
		
		if (axi_rvalid) begin
			if (axi_rdata == expected_read_data) begin
				in_read = 0;
			end
			if (addr[11:8] == 9) begin
				if (axi_rdata != expected_read_data) begin
					$display("axi_read %d %d %d",loc_axi-8, axi_rdata, expected_read_data);	
					$stop();
				end
			end
			
					
		end	
		
		if (cabac_to_res_wr_en) begin
			read_res[31:24] = $fgetc(fd_res);
			read_res[23:16] = $fgetc(fd_res);
			read_res[15: 8] = $fgetc(fd_res);
			read_res[ 7: 0] = $fgetc(fd_res);	
		
			if (cabac_to_res_data==read_res)begin
				matched_res = 1;
			end else begin
				matched_res = 0;
				$display("cabac_to_res: line %d expected %8H actual %8H", loc_res, read_res, cabac_to_res_data);
				$finish();
			end
			loc_res = loc_res+4;
			
		end
		
		if (validCont) begin
			read_Cont[ 7: 0] = $fgetc(fd_Cont);	
			read_Cont[15: 8] = $fgetc(fd_Cont);
			read_Cont[23:16] = $fgetc(fd_Cont);
			read_Cont[31:24] = $fgetc(fd_Cont);
			
			if (outCont==read_Cont)begin
				matched_Cont = 1;
			end else begin
				matched_Cont = 0;
				$display("residual_to_inter_config: line %d expected %8H actual %8H", loc_Cont, read_Cont, outCont);
				$finish();
			end
			loc_Cont = loc_Cont + 4;
		end
		
		if (validY) begin
			read(fd_Y);
			compare(outY, matched_Y);
			if (!matched_Y) begin
				$display("residual_to_inter_Y: line %d",loc_Y);
				print4x4(outY);
				$finish();
			end
			loc_Y = loc_Y + 24;
		end
		if (validCb) begin
			read(fd_Cb);	
			compare(outCb, matched_Cb);
			if (!matched_Cb) begin
				$display("residual_to_inter_Cb: line %d",loc_Cb);
				print4x4(outCb);
				$finish();
			end
			loc_Cb = loc_Cb + 24;
		end
		if (validCr) begin
			read(fd_Cr);
			compare(outCr, matched_Cr);
			if (!matched_Cr) begin
				$display("residual_to_inter_Cr: line %d",loc_Cr);
				print4x4(outCr);
				$finish();
			end
			loc_Cr = loc_Cr + 24;
		end
	end
	
	task read;
		input integer f;
		begin
			read_in_res[0][0][7:0] = $fgetc(f);
			{read_in_res[1][0][6:0] , read_in_res[0][0][8]} = $fgetc(f);
			{read_in_res[2][0][5:0] , read_in_res[1][0][8:7]} = $fgetc(f);
			{null[4:0] , read_in_res[2][0][8:6]} = $fgetc(f);
			
			read_in_res[3][0][7:0] = $fgetc(f);
			{read_in_res[0][1][6:0] , read_in_res[3][0][8]} = $fgetc(f);
			{read_in_res[1][1][5:0] , read_in_res[0][1][8:7]} = $fgetc(f);
			{null[4:0] , read_in_res[1][1][8:6]} = $fgetc(f);
			
			read_in_res[2][1][7:0] = $fgetc(f);
			{read_in_res[3][1][6:0] , read_in_res[2][1][8]} = $fgetc(f);
			{read_in_res[0][2][5:0] , read_in_res[3][1][8:7]} = $fgetc(f);
			{null[4:0] , read_in_res[0][2][8:6]} = $fgetc(f);
			
			read_in_res[1][2][7:0] = $fgetc(f);
			{read_in_res[2][2][6:0] , read_in_res[1][2][8]} = $fgetc(f);
			{read_in_res[3][2][5:0] , read_in_res[2][2][8:7]} = $fgetc(f);
			{null[4:0] , read_in_res[3][2][8:6]} = $fgetc(f);
			
			read_in_res[0][3][7:0] = $fgetc(f);
			{read_in_res[1][3][6:0] , read_in_res[0][3][8]} = $fgetc(f);
			{read_in_res[2][3][5:0] , read_in_res[1][3][8:7]} = $fgetc(f);
			{null[4:0] , read_in_res[2][3][8:6]} = $fgetc(f);
			
			read_in_res[3][3][7:0] = $fgetc(f);
			{null[6:0] , read_in_res[3][3][8]} = $fgetc(f);
			null = $fgetc(f);
			null = $fgetc(f);
			
		end
	endtask
	
	integer i,j,index;
	integer cycles;
	initial begin
		cycles = 0;
	end
	always @(posedge clk)
		cycles = cycles+1;
	always @(trafo_y0[11:6]) begin  
		$display("POC %4d (%4d,%4d) = %12d",trafo_POC,trafo_x0[11:6],trafo_y0[11:6], cycles);
	end
	
	task compare;
		input [143:0] sim;
		output OK;
		begin
			OK = 1;
			for (i=0;i<4;i=i+1) begin
				for (j=0;j<4;j=j+1) begin
					//index = (3-i)*4 + (3-j);
					index = j*4 + i;
					if (sim[index*9 +: 9] != read_in_res[i][j]) begin
						OK = 0;
					end
				end
			end
			//if ( sim == { read_in_res[0][0] ,read_in_res[0][1],read_in_res[0][2],read_in_res[0][3], read_in_res[1][0] ,read_in_res[1][1],read_in_res[1][2],read_in_res[1][3], read_in_res[2][0] ,read_in_res[2][1],read_in_res[2][2],read_in_res[2][3], read_in_res[3][0] ,read_in_res[3][1],read_in_res[3][2],read_in_res[3][3] } ) begin
			//	OK = 1;
			//end else begin
			//	OK = 0;
			//end
		end
	endtask
	
	task print4x4;
		input [143:0] sim;
		begin
			$display("expected");
			
			for (i=0;i<4;i=i+1) begin
				for (j=0;j<4;j=j+1) begin
					$write("%4d ",read_in_res[i][j]);
				end
				$write("\n");
			end
			$display("actual");
			for (i=0;i<4;i=i+1) begin
				for (j=0;j<4;j=j+1) begin
					//index = (3-i)*4 + (3-j);
					index = j*4 + i;
					$write("%4d ",sim[index*9 +: 9]);
				end
				$write("\n");
			end
		end
	endtask

      
endmodule

