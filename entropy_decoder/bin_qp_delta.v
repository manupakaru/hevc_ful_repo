`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:06:42 04/01/2014 
// Design Name: 
// Module Name:    bin_qp_delta 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module bin_qp_delta(
    input clk,
    input rst,
    input [5:0] bin_no,
    input binVal,
    input stall,
    output reg [31:0] decoded_se,
    output reg decoded_valid
    );
	
	reg [2:0] ones;
	reg [1:0] state;
	
	reg [5:0] egk;
	reg [5:0] egk_suf;
	
	localparam READING_TU = 2'b00;
	localparam READING_ONES = 2'b01;
	localparam READING_SUFFIX = 2'b10;
	
	always @(posedge clk) begin
		if (rst) begin
			state <= READING_TU;
		end else begin
			if (~stall) begin
				case (state) 
					READING_TU : begin
						if (!binVal) begin
							state <= READING_TU;
						end else begin
							if (bin_no == 6'd5) begin
								state <= READING_ONES;
								ones <= 3'd1;
								egk <= 6'd2; 
								egk_suf <= 6'd0;
							end
						end
					end
					READING_ONES: begin
						if (binVal) begin
							ones <= ones + 1'b1;
							egk <= (egk<<1);
						end else begin
							state <= READING_SUFFIX;
						end
					end
					READING_SUFFIX: begin
						if (ones==3'd1) begin
							state <= READING_TU;
							ones <= 3'd1;
							egk <= 6'd0;
						end else begin
							ones <= ones -1'b1;
							egk_suf <= {egk_suf[4:0], binVal};
						end
					end
				endcase
			end
		end
	end 
	
	always @(*) begin
		decoded_se = 32'dx;
		decoded_valid = 1'b0;
		if (~stall) begin
			case (state) 
				READING_TU : begin
					if (!binVal) begin
						decoded_se = bin_no;
						decoded_valid = 1'b1;
					end
				end
				READING_SUFFIX: begin
					if (ones==3'd1) begin
						decoded_se = egk+{egk_suf[4:0], binVal} +4;
						decoded_valid = 1'b1;
					end
				end
			endcase
		end
	end

endmodule
