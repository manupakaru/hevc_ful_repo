`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:29:24 12/16/2013
// Design Name:   ctx_table_dist
// Module Name:   C:/Users/Maleen/Uni/HEVC/EntropyDecoder/ctx_table_dist_test.v
// Project Name:  EntropyDecoder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ctx_table_dist
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module ctx_table_dist_test;

	// Inputs
	reg [7:0] a;
	reg [6:0] d;
	reg [7:0] dpra;
	reg clk;
	reg we;

	// Outputs
	wire [6:0] qdpo;

	// Instantiate the Unit Under Test (UUT)
	ctx_table_dist uut (
		.a(a), 
		.d(d), 
		.dpra(dpra), 
		.clk(clk), 
		.we(we), 
		.qdpo(qdpo)
	);

	initial begin
		// Initialize Inputs
		a = 0;
		d = 0;
		dpra = 0;
		clk = 0;
		we = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		dpra = 100;
		d = 200;
		a= 100;
		we = 1;
		#10;
		we =0;
	end
	always begin
		#5 clk = ~clk;
	end
      
endmodule

