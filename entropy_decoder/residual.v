`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:12:04 11/25/2013 
// Design Name: 
// Module Name:    residual 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module residual(
		clk,
		rst,	
		
		mmap_data,
		mmap_valid,
		
		cabac_in_data,
		cabac_in_valid,
		
		cabac_out_data,
		cabac_out_valid,
		cabac_out_read,
		
		out_fifo_data,
		out_fifo_valid,
		out_fifo_full,
		in_fifo_almost_empty,
		
		done,
		
		cIdx,
		trafoSize,
		scanIdx,
		sign_data_hiding_flag,
		
		ru_state
    );
	 input clk;
	 input rst;
	 
	 input [8:0] mmap_data;
	 input mmap_valid;
	 
	 output reg [31:0] cabac_in_data;
	 output reg cabac_in_valid;
	 
	 input [31:0] cabac_out_data;
	 input cabac_out_valid;
	 output reg cabac_out_read;
	 
	 output [31:0] out_fifo_data;
	 output reg out_fifo_valid;
	 input out_fifo_full;
	 input in_fifo_almost_empty;
	 
	 output reg done;
	 
	 input [1:0] cIdx; // convert to input
	 input [1:0] scanIdx;
	 input [2:0] trafoSize; 
	 
	 input sign_data_hiding_flag;
	 reg cu_transquant_bypass_flag;
	 
	 reg [5:0] state;
	 
	 reg [4:0] last_significant_coeff_x, last_significant_coeff_y;
	 reg [5:0] lastSubBlock,i;
	 reg [3:0] lastScanPos,n;
	 reg inferSbDcSigCoeffFlag;
	 
	 reg [1:0] invScanOrder_blk_size;
	 reg [2:0] invScanOrder_x, invScanOrder_y;
	 wire [5:0] invScanOrder_invScanOrder;
	 
	 reg [1:0] scanOrder_blk_size;
	 wire [2:0] scanOrder_x, scanOrder_y;
	 reg [5:0] scanOrder_scanPos;
	 
	 reg [2:0] xS, yS;
	 reg [4:0] xC, yC; // wires
	 reg [0:7] csbf_d ;
	 reg [0:7] csbf_r ;
	 reg csbf_cur;
	 
	 reg [1:0] ctxSet;
	 reg [3:0] greater1Ctx;
	 reg [2:0] cRiceParam;
	 reg greater1_first1_met;
	 reg greater1_invoked, greater1_invoked_sb;
	 
	 
	 reg [3:0] firstSigScanPos ;
	 reg [3:0] numGreater1Flag,lastSigScanPos;
	 reg signed [4:0] lastGreater1ScanPos;
	 reg [3:0] numSigCoeff;
	 reg sumAbsLevel;
	 
	 reg [0:15] significant_coeff_flag;
	 reg [0:15] coeff_abs_level_greater1_flag;
	 reg [0:15] coeff_abs_level_greater2_flag;
	 reg [0:15] coeff_sign_flag;
	 
	 reg [4:0] xT, yT; // send to transform index
	 //reg [15:0] coeff_abs_level_remaining [0:15]; 
	 reg signHidden;
	 
	 reg signed [15:0] coeffVal;
	 reg coeffSign; // wire
	 
	 wire [1:0] baseLevel;
	 wire read_rem;
	 wire [4:0] sig_coeff_ctx;
	 
	 integer j;
	 //reg firstCoeffInSubBlock; // used to skip initial pipe stall in state 13
	 
	 assign baseLevel = (significant_coeff_flag[n] ? 2'b01 : 2'b00) + coeff_abs_level_greater1_flag[n] + coeff_abs_level_greater2_flag[n];
	 assign read_rem = (baseLevel == ( (numSigCoeff < 8) ? (( {1'b0 ,n}==lastGreater1ScanPos) ? 2'd3 : 2'd2) : 2'd1));
	 
	 //wire [15:0] transcoeff_out0, transcoeff_out1;
	 reg transcoeff_wea0, transcoeff_wea1;
	 reg [8:0] transcoeff_addr;
	 reg transcoeff_en;
	 
	 output [6:0] ru_state;
	 assign ru_state = state;
	
	// assign transcoeff_addr = ( state==24 || state==25|| state == 26) ? {yT[4:1] , xT} : {yC[4:1] , xC}; // reverse xT yT when reading
	 
	 invScanOrder invScanOrder(
		.blk_size(invScanOrder_blk_size),
      .scanIdx(scanIdx),
      .x(invScanOrder_x),
      .y(invScanOrder_y),
      .invScanOrder(invScanOrder_invScanOrder)
	 );
	 
	 scanOrder scanOrder(
		.blk_size(scanOrder_blk_size),
      .scanIdx(scanIdx),
      .scanPos(scanOrder_scanPos),
      .scanOrder( {scanOrder_x, scanOrder_y})
	 );
	 
	 Transcoeff_m bank0 (
	  .clka(clk), // input clka
	  .wea(transcoeff_wea0), // input [0 : 0] wea
	  .addra( transcoeff_addr), // input [8 : 0] addra
	  .dina(coeffVal), // input [15 : 0] dina
	  .douta(out_fifo_data[31:16]), // output [15 : 0] douta
	  .ena(transcoeff_en)
	  
	);
	Transcoeff_m bank1 (
	  .clka(clk), // input clka
	  .wea(transcoeff_wea1), // input [0 : 0] wea
	  .addra( transcoeff_addr), // input [8 : 0] addra
	  .dina(coeffVal), // input [15 : 0] dina
	  .douta(out_fifo_data[15:0]), // output [15 : 0] douta
	  .ena(transcoeff_en)
	  
	);
   
//	(* ram_style = "block" *)
//	reg [15:0] bank0 [0:511];
//	reg [15:0] bank1 [0:511];
//	
//	
//	always @(posedge clk) begin
//		if (state==63) begin
//			bank0[i_rst] <= 16'd0;
//			bank1[i_rst] <= 16'd0;
//		end else begin
//			if (!out_fifo_full) begin
//				out_fifo_data <= {bank0[transcoeff_addr] , bank1[transcoeff_addr]};
//			end
//			if (transcoeff_wea0) begin
//				bank0[transcoeff_addr] <= coeffVal;
//			end
//			if (transcoeff_wea1) begin
//				bank1[transcoeff_addr] <= coeffVal;
//			end
//		end
//	end
	reg [8:0] i_rst;
	always @(posedge clk) begin
		if (!rst) begin
			state <= 63;
			i_rst <= 9'd0;
			
		end else begin
			//cabac_in_valid <= 1'b0;
			case (state) 
				63: begin
					i_rst <= i_rst + 1'b1;
					//transcoeff_addr <= i_rst;
					//coeffVal <= 16'd0;
					if (i_rst == 9'd511) 
						state <= 0;
				end
				0: begin // read_next_residual
					if (mmap_valid) begin
						//sign_data_hiding_flag = mmap_data[8];
						cu_transquant_bypass_flag <= 1'b0;
						//cIdx <= mmap_data[6:5];
						//scanIdx <= mmap_data[1:0];
						//trafoSize <= mmap_data[2:0];
						state <= 1;
						csbf_d <= 8'd0;
						csbf_r <= 8'd0;
						significant_coeff_flag <= 16'd0;
						greater1Ctx <= 4'd1;
						greater1_invoked <= 1'b0;
						greater1_invoked_sb <= 1'b0;
						case (trafoSize) 
							2: begin
								xT <= 5'd3;
								yT <= 5'd0;
							end
							3: begin
								xT <= 5'd7;
								yT <= 5'd0;
							end
							4: begin
								xT <= 5'd15;
								yT <= 5'd0;
							end
							5: begin
								xT <= 5'd31;
								yT <= 5'd0;
							end
						endcase
						done <= 1'b0;
					end
				end
				1: begin // push coeff_x_prefix
					if (!in_fifo_almost_empty) state <= 2;
				end
				2: begin // push coeff_y_prefix
					if (!in_fifo_almost_empty) state <= 3;
				end
				3 : begin // read coeff_x_prefix and push out suffix
					if (!in_fifo_almost_empty) begin
						if (cabac_out_valid) begin
							if (cabac_out_data[3:0] > 3 ) begin
								case (cabac_out_data[3:0]) // this is later directly added to suffix
									4: last_significant_coeff_x <= 5'd4;
									5: last_significant_coeff_x <= 5'd6;
									6: last_significant_coeff_x <= 5'd8;
									7: last_significant_coeff_x <= 5'd12;
									8: last_significant_coeff_x <= 5'd16;
									9: last_significant_coeff_x <= 5'd24;
								endcase
							end else begin
								last_significant_coeff_x <= cabac_out_data[3:0];			
							end
							state <= 4;
						end
					end
				end
				4 : begin // read coeff_y_prefix and push out suffix
					if (!in_fifo_almost_empty) begin
						if (cabac_out_valid) begin
							if (cabac_out_data[3:0] > 3 ) begin
								case (cabac_out_data[3:0]) 
									4: last_significant_coeff_y <= 5'd4;
									5: last_significant_coeff_y <= 5'd6;
									6: last_significant_coeff_y <= 5'd8;
									7: last_significant_coeff_y <= 5'd12;
									8: last_significant_coeff_y <= 5'd16;
									9: last_significant_coeff_y <= 5'd24;
								endcase
							end else begin
								last_significant_coeff_y <= cabac_out_data[3:0];
							end
							
							if (last_significant_coeff_x >3 )
								state <= 5;
							else if (cabac_out_data[3:0] > 3) 
								state <= 6;
							else 
								state <= 7;
						end
					end
				end
				5 : begin // read_x_suffix
					if (cabac_out_valid) begin
						last_significant_coeff_x <= last_significant_coeff_x + cabac_out_data[3:0];
							if (last_significant_coeff_y > 3) 
							state <= 6;
						else 
							state <= 7;
					end
					
				end
				6 : begin // read_y_suffix
					if (cabac_out_valid) begin
						last_significant_coeff_y <= last_significant_coeff_y + cabac_out_data[3:0];
						state <= 7;
					end
				end
				7 : begin // swap if scanIdx ==2
					if (scanIdx == 2'd2) begin
						{last_significant_coeff_y , last_significant_coeff_x} <= { last_significant_coeff_x, last_significant_coeff_y};
					end
					state <= 8;
				end
				8 : begin // lastSubblock
					lastSubBlock <= invScanOrder_invScanOrder;
					i <= invScanOrder_invScanOrder;
					state <= 9;
				end
				9 : begin // lastScanPos
					lastScanPos <= invScanOrder_invScanOrder[3:0];
					state <= 10;
					
				end
				10: begin // xS, yS
					
					xS <= scanOrder_x;
					yS <= scanOrder_y;
					if ( (i< lastSubBlock) && (i>0)) begin
						inferSbDcSigCoeffFlag <= 1'b1;
						state <= 12;
					end else begin
						if (i==lastSubBlock &&  lastScanPos == 0) inferSbDcSigCoeffFlag <= 1;
						else inferSbDcSigCoeffFlag <= 0;
						//csbf_d[scanOrder_x] <= 1'b1;
						//csbf_r[scanOrder_y] <= 1'b1;
						csbf_cur <= 1'b1;
						state <= 13;
					end
					if (i==lastSubBlock && lastScanPos ==0) begin
						n <= 4'd0;
					end else begin
						n <= (i==lastSubBlock) ? lastScanPos - 1'b1 : 4'd15;
					end
					// not good practice
					significant_coeff_flag <= 16'd0;
					if (i==lastSubBlock)
						significant_coeff_flag[lastScanPos] <= 1'b1;
				//	firstCoeffInSubBlock <= 1'b1;
					
					coeff_abs_level_greater1_flag <= 16'd0;
					coeff_abs_level_greater2_flag <= 16'd0;
					coeff_sign_flag <= 16'd0;
				//	for (j=0;j<16;j=j+1) begin
				//		coeff_abs_level_remaining[j] <= 16'd0;
				//	end
					sumAbsLevel <= 1'b0;
					numSigCoeff <= 4'd0;
					cRiceParam <= 3'd0;
					ctxSet[1] <=( (i==0) || (cIdx>0) )? 1'b0 : 1'b1;
					if (!greater1_invoked) ctxSet[0] <= 1'b0; 
					else ctxSet[0] <= (greater1Ctx == 0 ) ? 1'b1 : 1'b0;
					greater1_first1_met <= 1'b0;
					greater1_invoked_sb <= 1'b0;
				end
				11 : begin // push coded_sub_block_flag
					if (!in_fifo_almost_empty) state <= 12;
				end
				12 : begin // read coded_sub_block_flag
					if (cabac_out_valid) begin
						//csbf_d [xS] <= cabac_out_data[0];
						//csbf_r [yS] <= cabac_out_data[0];
						csbf_cur <= cabac_out_data[0];
						if (cabac_out_data[0]) begin
							state <= 13;
							

						end else begin 
							// TODO if i==0 end of trafo
							csbf_d [xS] <= 1'b0;
							csbf_r [yS] <= 1'b0;
							//greater1Ctx <= 4'd0;
							i <= i-1'b1; 
							state <= 10;
						end
					end
				end
				13 : begin // push_significant_coeff_flag
					//firstCoeffInSubBlock <= 1'b0;
					if (!in_fifo_almost_empty) begin
						if (n == 4'b0001) begin // begin reading
							//firstCoeffInSubBlock <= 1'b1;
							n <= (i==lastSubBlock) ? lastScanPos - 1'b1 : 4'd15;
							state <= 14; // n==15 after pipeline finish 
						end else if (n==4'b0000) begin // push final coeff
							n <= 4'd0;
							state <= 14; 
						end else begin
							//firstCoeffInSubBlock <= 1'b0;
							n <= n-1'b1;
							state <= 13;
						end
					end
				end
				14: begin // read_significant coeff_flag
					if (csbf_cur && (n>0 || !inferSbDcSigCoeffFlag)) begin
						if (cabac_out_valid) begin
							significant_coeff_flag[n] <= cabac_out_data[0];
							if (n==1) begin
								state <= 13;
								n <= 4'b0000;
							end else if (n==0) begin
								state <= 15;	
							end else begin
								n <= n-1'b1;
							end
							if (cabac_out_data[0]) begin
								inferSbDcSigCoeffFlag <= 1'b0;
							end
						end 
					end else begin // n==0 & inferSb
						significant_coeff_flag[0] <= 1'b1;
						if (n==1) begin
							state <= 13;
							n <= 4'b0000;
						end else if (n==0) begin
							state <= 15;	
						end else begin
							n <= n-1'b1;
						end
					end
					
				end
				15 : begin
					// temp 
					csbf_d [xS] <= csbf_cur;
					csbf_r [yS] <= csbf_cur;
					//inferSbDcSigCoeffFlag <= 1'b1; // Remove
					firstSigScanPos <= 4'd15;
					lastSigScanPos <= 4'd0;
					numGreater1Flag <= 4'd0;
					lastGreater1ScanPos <= -4'd1;
					n <= (i==lastSubBlock) ? lastScanPos : 4'd15;
					state <= 16;
				end
				16 : begin // push abs_greater1
					if (!in_fifo_almost_empty) begin
						if (significant_coeff_flag[n]) begin
							if (numGreater1Flag < 8) begin
								numGreater1Flag <= numGreater1Flag + 1'b1;
								state <= 17;
								greater1_invoked <= 1'b1;
								if (!greater1_invoked_sb) greater1Ctx <= 4'd1;
								greater1_invoked_sb <= 1'b1;
							end else begin
								if (n==0) begin
									state <= 18;
								end else begin
									n <= n-1'b1;
								end
							end
							if (lastSigScanPos == 0) lastSigScanPos <= n;
							firstSigScanPos <= n;
						end else begin
							if (n==0) begin
								state <= 18;
							end else begin
								n <= n-1'b1;
							end
						end
					end
				end
				17 : begin // read abs_greater1
					if (cabac_out_valid) begin
						if (n==0) begin
							state <= 18;
						end else begin
							n <= n-1'b1;
							state <= 16;
						end
						coeff_abs_level_greater1_flag[n] <= cabac_out_data[0];
						if (cabac_out_data[0]) begin
							greater1_first1_met <= 1'b1;
							greater1Ctx <= 4'd0;
							if (lastGreater1ScanPos == -1) lastGreater1ScanPos <= n;
						end else begin
							if (!greater1_first1_met) begin
								greater1Ctx <= greater1Ctx + 1'b1;
							end
						end	
					end
				end
				18 : begin //push_abs greater2
					if (!in_fifo_almost_empty) begin
						signHidden <= (( {1'b0 ,lastSigScanPos} > { 1'b0, firstSigScanPos} + 5'd3) && !cu_transquant_bypass_flag);
						if (lastGreater1ScanPos != -1) begin
							state <= 19;
						end else begin
							state <= 20;
						end
						n <= (i==lastSubBlock) ? lastScanPos : 4'd15;
					end
				end
				19 : begin // read coeff_abs_greater2
					if (cabac_out_valid) begin
						coeff_abs_level_greater2_flag[ lastGreater1ScanPos[3:0] ] <= cabac_out_data[0];
						state <= 20;
					end
				end
				20 : begin // push coeff_sign_flag
					if (!in_fifo_almost_empty)	begin
						if (n==0) begin
							state <= 21;
							n <= (i==lastSubBlock) ? lastScanPos : 4'd15;
						end else begin
							n <= n-1'b1;
						end
					end
				end
				21 : begin // read_coeff_sign_flag
					if (significant_coeff_flag[n] && ( !sign_data_hiding_flag || !signHidden || (n != firstSigScanPos[3:0]) ) ) begin
						if (cabac_out_valid) begin
							if (n==0) begin
								state <= 22;
								n <= (i==lastSubBlock) ? lastScanPos : 4'd15;
							end else begin
								n <= n-1'b1;
							end
							coeff_sign_flag[n] <= cabac_out_data[0];
						end
					end else  begin
						if (n==0) begin
							state <= 22;
							n <= (i==lastSubBlock) ? lastScanPos : 4'd15;
						end else begin
							n <= n-1'b1;
						end
					end
				end
				22 : begin // push coeff_abs_level_remaining
					//state <= 23;
					if (!in_fifo_almost_empty)	begin

						if (significant_coeff_flag[n]) numSigCoeff <= numSigCoeff + 1'b1;
						if (significant_coeff_flag[n] && read_rem) begin
							state <= 23; 
						end else begin
							sumAbsLevel <= sumAbsLevel ^ baseLevel[0];
							if (n==0) begin// end of subblock
								if (i==0) begin
									state <= 26;
									
								end else begin
									i <= i-1'b1;
									state <= 10;
								end
							end else begin
								n <= n-1'b1;
							end
						end 
					end
				end 
				23 : begin // read coeff_abs_level_remaining
					if (cabac_out_valid) begin
						//coeff_abs_level_remaining[n] <= (cabac_out_data[15:0]+baseLevel);
						sumAbsLevel <= sumAbsLevel ^ baseLevel[0] ^ cabac_out_data[0];
						// RiceParam
						case (cRiceParam) 
							0: cRiceParam <= ((cabac_out_data[15:0]+baseLevel) > 3 )? 3'd1 : 3'd0;
							1: cRiceParam <= ((cabac_out_data[15:0]+baseLevel) > 6 )? 3'd2 : 3'd1;
							2: cRiceParam <= ((cabac_out_data[15:0]+baseLevel) > 12 )? 3'd3 : 3'd2;
							3: cRiceParam <= ((cabac_out_data[15:0]+baseLevel) > 24 )? 3'd4 : 3'd3;
							4: cRiceParam <= 3'd4;
						endcase
						if (n==0) begin// end of subblock
							if (i==0) begin
								state <= 26;
							end else begin
								i <= i-1'b1;
								state <= 10;
							end
						end else begin
							n <= n-1'b1;
							state <= 22;
						end
					end								
				end
				26 : begin // extra cycle to stabilize memory inputs
					state <= 24;
				end
				24 : begin // push to transform
					if ( !out_fifo_full) begin
						//xT_d <= xT;
						//yT_d <= yT;
						case (trafoSize)
							2: begin
								if (yT == 2) begin
									if (xT == 0) begin
										state <= 25;
									end else begin
										xT <= xT - 5'd1;
										yT <= 5'd0;
									end
								end else begin
									yT <= yT + 5'd2;
								end
							end
							3: begin
								if (yT == 6) begin
									if (xT == 0) begin
										state <= 25;
									end else begin
										xT <= xT - 5'd1;
										yT <= 5'd0;
									end
								end else begin
									yT <= yT + 5'd2;
								end
							end
							4: begin
								if (yT == 14) begin
									if (xT == 0) begin
										state <= 25;
									end else begin
										xT <= xT - 5'd1;
										yT <= 5'd0;
									end
								end else begin
									yT <= yT + 5'd2;
								end
							end
							5: begin
								if (yT == 30) begin
									if (xT == 0) begin
										state <= 25;
									end else begin
										xT <= xT - 5'd1;
										yT <= 5'd0;
									end
								end else begin
									yT <= yT + 5'd2;
								end
							end
						endcase
					end
					//if (coeff_sign_flag[n] || coeff_abs_level_remaining[n] ==0) state <= 25;
				end
				25 : begin
					if (!out_fifo_full) begin
						state <= 0;
						done <= 1'b1;
						// up finish_flag
					end
				end
			endcase
		end
	end
	 
	always @ (*) begin
		cabac_out_read = 1'b0;
		cabac_in_data = 32'dx;
		cabac_in_valid = 1'b0;
		invScanOrder_blk_size = 2'bx;
		invScanOrder_x = 3'bx;
		invScanOrder_y = 3'bx;
		scanOrder_scanPos = 5'bx;
		scanOrder_blk_size = 2'bx;
		xC = 5'dx;
		yC = 5'dx;
		out_fifo_valid = 1'b0;
		transcoeff_wea0 = 1'b0;
		transcoeff_wea1 = 1'b0;
		transcoeff_en = 1'b1;
		coeffVal = 16'd0;
		transcoeff_addr = 9'dx;
		if (~rst) begin
			cabac_out_read = 1'b0;
		end else begin
			case (state) 
				63: begin
					transcoeff_addr = i_rst;
					transcoeff_wea0 = 1'b1;
					transcoeff_wea1 = 1'b1;
					
				end
				1: begin
					cabac_in_data = { 8'd36, trafoSize, cIdx, 19'd0};
					cabac_in_valid = ~in_fifo_almost_empty;
					
				end 
				2: begin
					cabac_in_data = { 8'd37, trafoSize, cIdx, 19'd0};
					cabac_in_valid = ~in_fifo_almost_empty;
				end 
				3: begin
					if (!in_fifo_almost_empty) begin
						if (cabac_out_valid) begin
							if (cabac_out_data[3:0] > 3 ) begin
								cabac_in_data = { 8'd38, cabac_out_data[3:0], 20'd0};
								cabac_in_valid = 1;
							end
							cabac_out_read = 1'b1;
						end
					end
				end
				4: begin
					if (!in_fifo_almost_empty) begin
						if (cabac_out_valid) begin
							if (cabac_out_data[3:0] > 3 ) begin
								cabac_in_data = { 8'd39, cabac_out_data[3:0], 20'd0};
								cabac_in_valid = 1;
							end
							cabac_out_read = 1'b1;
						end
					end
				end
				5,6 : begin
					if (cabac_out_valid) cabac_out_read = 1'b1;
				end
				8: begin
					invScanOrder_blk_size = (trafoSize - 2'd2) % 4 ;
					invScanOrder_x = last_significant_coeff_x[4:2];
					invScanOrder_y = last_significant_coeff_y[4:2];
				end
				9: begin
					invScanOrder_blk_size = 2;
					invScanOrder_x = last_significant_coeff_x[1:0];
					invScanOrder_y = last_significant_coeff_y[1:0];
				end
				10: begin
					scanOrder_blk_size = (trafoSize - 2'd2) % 4 ;
					scanOrder_scanPos = i;
				end
				11: begin
					cabac_in_data = { 8'd40, trafoSize, cIdx, csbf_d[xS], csbf_r[yS], 17'd0};
					if (!in_fifo_almost_empty) cabac_in_valid = 1;
				end
				12: begin
					if (cabac_out_valid) cabac_out_read = 1'b1;
				end
				13: begin
					if (!in_fifo_almost_empty) begin
						scanOrder_blk_size = 2'b10 ;
						scanOrder_scanPos = n;
						xC = {xS , scanOrder_x[1:0]};
						yC = {yS , scanOrder_y[1:0]};
						if (csbf_cur & (n>0 || !inferSbDcSigCoeffFlag)) begin
							cabac_in_data = { 8'd41, trafoSize, cIdx, csbf_d[xS], csbf_r[yS], xC, yC, scanIdx, sig_coeff_ctx};
							cabac_in_valid = 1;
						end
					end
				end
				14: begin
					if (csbf_cur && (n>0 || !inferSbDcSigCoeffFlag)) begin
						if (cabac_out_valid) begin
							cabac_out_read = 1'b1;
						end 
					end
				end
				16: begin
					if (!in_fifo_almost_empty) begin
						scanOrder_blk_size = 2'b10 ;
						scanOrder_scanPos = n;
						if (significant_coeff_flag[n]) begin
							if (numGreater1Flag < 8) begin
								if (!greater1_invoked_sb)
									cabac_in_data = { 8'd42, 3'b000, cIdx, 4'd1, ctxSet, 13'd0};
								else 
									cabac_in_data = { 8'd42, 3'b000, cIdx, greater1Ctx[3:0], ctxSet, 13'd0};
								cabac_in_valid = 1;
							end
						end
					end
				end
				17: begin
					if (cabac_out_valid) begin
						cabac_out_read = 1'b1;
					end
				end
				18: begin
					if (!in_fifo_almost_empty) begin
						if (lastGreater1ScanPos != -1) begin
							cabac_in_data = {8'd43, 3'b000, cIdx, 4'b0000, ctxSet, 13'd0};
							cabac_in_valid = 1;
						end
					end
				end
				19: begin
					if (cabac_out_valid) begin
						cabac_out_read = 1'b1;
					end
				end
				20: begin
					if (!in_fifo_almost_empty) begin
						if (significant_coeff_flag[n] && ( !sign_data_hiding_flag || !signHidden || (n != firstSigScanPos[3:0]) ) ) begin
							cabac_in_data = {8'd45, 24'd0};
							cabac_in_valid = 1;
						end
					end
				end
				21: begin
					if (significant_coeff_flag[n] && ( !sign_data_hiding_flag || !signHidden || (n != firstSigScanPos[3:0]) ) ) begin
						if (cabac_out_valid) begin
							cabac_out_read = 1'b1;
						end
					end
				end
				22: begin
					if (!in_fifo_almost_empty) begin
						scanOrder_blk_size = 2'b10 ;
						scanOrder_scanPos = n;
						xC = {xS , scanOrder_x[1:0]};
						yC = {yS , scanOrder_y[1:0]};
						transcoeff_addr = {yC[4:1] , xC};
						if (significant_coeff_flag[n]) begin
							if ( read_rem) begin
								cabac_in_data = {8'd44,cRiceParam,  21'd0};
								cabac_in_valid = 1;
							end else begin
								if (sign_data_hiding_flag && signHidden && n==firstSigScanPos) begin
									coeffSign = (sumAbsLevel ^ baseLevel[0]);
								end else begin
									coeffSign = coeff_sign_flag[n];
								end
								if (coeffSign==0) begin
									coeffVal = baseLevel; 
								end else begin
									coeffVal = -baseLevel;
								end
								transcoeff_wea0 = (yC[0] == 1'b0);
								transcoeff_wea1 = (yC[0] == 1'b1);
							end
						end 
					end
				end
				23: begin
					scanOrder_blk_size = 2'b10 ;
					scanOrder_scanPos = n;
					xC = {xS , scanOrder_x[1:0]};
					yC = {yS , scanOrder_y[1:0]};
					transcoeff_addr = {yC[4:1] , xC};
					if (cabac_out_valid) begin
						cabac_out_read = 1'b1;
						if (sign_data_hiding_flag && signHidden && n==firstSigScanPos) begin
							coeffSign = (sumAbsLevel ^ baseLevel[0] ^ cabac_out_data[0]);
						end else begin
							coeffSign = coeff_sign_flag[n];
						end
						if (coeffSign==0) begin
							coeffVal = (baseLevel + cabac_out_data[15:0]); 
						end else begin
							coeffVal = (-baseLevel - cabac_out_data[15:0]);
						end
						transcoeff_wea0 = (yC[0] == 1'b0);
						transcoeff_wea1 = (yC[0] == 1'b1);
					end
				end
				24 : begin
					transcoeff_addr = {yT[4:1] , xT};
					if (!out_fifo_full) begin
						case (trafoSize) 
							2: out_fifo_valid = ~(xT==3 && yT==0);
							3: out_fifo_valid = ~(xT==7 && yT==0);
							4: out_fifo_valid = ~(xT==15 && yT==0);	
							5: out_fifo_valid = ~(xT==31 && yT==0);
							default: out_fifo_valid = 1'b0;
						endcase
						coeffVal = 16'd0;
						transcoeff_wea0 = 1'b1;
						transcoeff_wea1 = 1'b1;
					end else begin
						transcoeff_en = 1'b0;
					end
				end
				25 : begin
					transcoeff_addr = {yT[4:1] , xT};
					if (!out_fifo_full) begin
						out_fifo_valid = 1'b1;
						coeffVal = 16'd0;
						transcoeff_wea0 = 1'b1;
						transcoeff_wea1 = 1'b1;
					end else begin
						transcoeff_en = 1'b0;
					end
				end
				26 : begin
					transcoeff_addr = {yT[4:1] , xT};
					if (out_fifo_full) begin
						transcoeff_en = 1'b0;
					end
				end
			endcase 
		end
	end
	

	sig_ctx sig_ctx(
		.xC( {xS , scanOrder_x[1:0]} ),
		.yC( {yS , scanOrder_y[1:0]} ),
		.prevCsbf( {csbf_d[xS], csbf_r[yS]}),
		.cIdx ( cIdx),
		.log2TrafoSize(trafoSize),
		.scanIdx( scanIdx),
		.sig_ctx(sig_coeff_ctx)
	);
	
endmodule
