`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:36:26 12/06/2013
// Design Name:   axi_fifo
// Module Name:   C:/Users/Maleen/Uni/HEVC/EntropyDecoder/axi_fifo_test.v
// Project Name:  EntropyDecoder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: axi_fifo
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module axi_fifo_test;

	// Inputs
	reg clk;
	reg rst;
	reg [31:0] din;
	reg wr_en;
	reg rd_en;

	// Outputs
	wire [31:0] dout;
	wire full;
	wire wr_ack;
	wire empty;
	wire valid;

	// Instantiate the Unit Under Test (UUT)
	axi_fifo uut (
		.clk(clk), 
		.rst(rst), 
		.din(din), 
		.wr_en(wr_en), 
		.rd_en(rd_en), 
		.dout(dout), 
		.full(full), 
		.wr_ack(wr_ack), 
		.empty(empty), 
		.valid(valid)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0;
		din = 0;
		wr_en = 0;
		rd_en = 0;

		// Wait 100 ns for global reset to finish
		#100;
      rst = 1;
		// Add stimulus here
		din = 100;
		wr_en = 1;
		#10;
		wr_en = 0;
	end
	always begin
		#5 clk = ~clk;
	end
      
endmodule

