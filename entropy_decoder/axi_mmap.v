`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:59:04 08/05/2014 
// Design Name: 
// Module Name:    axi_mmap 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module axi_mmap(
		clk,
		rst,
		
		axi_araddr,
		axi_arburst,
		axi_arcache,
		axi_arid,
		axi_arlen,
		axi_arlock,
		axi_arprot,
		axi_arready,
		axi_arsize,
		axi_arvalid,
		
		axi_awaddr,
		axi_awburst,
		axi_awcache,
		axi_awid,
		axi_awlen,
		axi_awlock,
		axi_awprot,
		axi_awready,
		axi_awsize,
		axi_awvalid,
		
		axi_bid,
		axi_bready,
		axi_bresp,
		axi_bvalid,
		
		axi_rdata,
		axi_rid,
		axi_rlast,
		axi_rready,
		axi_rresp,
		axi_rvalid,
		
		axi_wdata,
		axi_wlast,
		axi_wready,
		axi_wstrb,
		axi_wvalid,
		
		annexb_next_bits,
		annexb_out_valid,
		annexb_read_bits,
		annexb_read_valid,
		annexb_control, // 0-ublaze, 1-cabac
		annexb_offload_override,
		annexb_pos,
		annexb_skip_to_NAL,
		//annexb_rst,
	  
		entropy_in,
		entropy_valid,
		entropy_full,
	  
		push_data,
		push_valid,
		push_full,
		in_fifo_almost_empty,
		
		input_hevc_data_count,
		cabac_to_res_data_count,

		cabac_QP,
		cabac_init_type,
		cabac_op,
		cabac_op_valid,
		cabac_op_done,

		cabac_in_fifo_din,
		cabac_in_fifo_valid,
		cabac_out_fifo_dout,
		cabac_out_fifo_empty,
		cabac_out_fifo_read,

		cu_state,
		ru_state,
		cu_x0,
		cu_y0,
		cu_other,
		
		axi_debug
    );
	 
	 localparam STATE_WAITING_AWVALID = 3'b000;
	 localparam STATE_WAITING_WVALID = 3'b001;
	 localparam STATE_WAITING_BVALID = 3'b010;
	 localparam STATE_WAITING_RVALID = 3'b011;
	 
	 localparam RESET_ACTIVE = 1'b0;
	 
	input clk;
	input rst;
	
	input [15:0] 		axi_araddr;
	input [1:0] 		axi_arburst;
	input [3:0] 		axi_arcache;
	input [11:0] 		axi_arid;
	input [7:0] 		axi_arlen;
	input 				axi_arlock;
	input [2:0]			axi_arprot;
	output  reg			axi_arready;
	input [2:0]			axi_arsize;
	input					axi_arvalid;
	
	input [15:0] 		axi_awaddr;
	input [1:0] 		axi_awburst;
	input [3:0] 		axi_awcache;
	input [11:0] 		axi_awid;
	input [7:0] 		axi_awlen;
	input 				axi_awlock;
	input [2:0]			axi_awprot;
	output reg			axi_awready;
	input [2:0]			axi_awsize;
	input					axi_awvalid;
	
	output reg [11:0] axi_bid;
	input					axi_bready;
	output reg [1:0] 	axi_bresp;
	output reg			axi_bvalid;
	
	output reg [31:0] axi_rdata;
	output reg[11:0]	axi_rid;
	output reg			axi_rlast;
	input					axi_rready;
	output reg [1:0] 	axi_rresp;
	output reg			axi_rvalid;
	
	input [31:0] 		axi_wdata;
	input					axi_wlast;
	output reg			axi_wready;
	input [3:0]			axi_wstrb;
	input					axi_wvalid;
	
	input [7:0] 		annexb_next_bits;
	input 				annexb_out_valid;
	output reg [3:0] 	annexb_read_bits;
	output reg 			annexb_read_valid;
	output reg 			annexb_control;
	output 				annexb_offload_override;
	input [4:0] 		annexb_pos;
	//output reg 			annexb_rst; //goes active when read is made, inactive when write to input_hevc
	output reg			annexb_skip_to_NAL;
	
	output reg [31:0] entropy_in;
	output reg			entropy_valid;
	input					entropy_full;
	
	output reg [31:0] push_data;
	output reg 			push_valid;
	input 				push_full;
	input 				in_fifo_almost_empty;
	
	input [13:0] 		input_hevc_data_count;
	input [13:0] 		cabac_to_res_data_count;
	
	output reg [6:0] cabac_QP;
	output reg [1:0] cabac_init_type;
	output reg [1:0] cabac_op;
	output reg cabac_op_valid;
	input cabac_op_done;
	
	output reg [31:0] cabac_in_fifo_din;
	output reg cabac_in_fifo_valid;
	
	input [31:0] cabac_out_fifo_dout;
	input cabac_out_fifo_empty;
	output reg cabac_out_fifo_read;
	
	//test
	output [6:0] cu_state, ru_state;
	output [11:0] cu_x0, cu_y0;
	output [255:0] cu_other;
	
	output [95:0] axi_debug;
	
// Internal
	reg [2:0] state;
	
	reg [11:0] reg_id;
	
	
	
	//reg mmap_valid;
	wire [31:0] res_cabac_in_data, res_out_fifo_data;
	wire res_cabac_in_valid , res_out_fifo_valid;
	wire res_cabac_out_read;
	
	reg [15:0] cu_mmap_data;
	reg [7:0] cu_mmap_type;
	reg cu_mmap_valid;
	wire [31:0] cu_cabac_in_data, cu_out_fifo_data;
	wire cu_cabac_in_valid , cu_out_fifo_valid;
	wire cu_cabac_out_read;
	wire cu_done;
	

	wire [3:0] offload_read_bits;
	wire offload_read_valid;
	
	wire [1:0] offload_cabac_op;
	wire offload_cabac_op_valid;

	wire [15:0] ctbAddrInTS;
	wire end_of_slice_segment_flag;
	
	
	cu_offload cu_offload(
		.clk(clk),
		.rst(rst),
		
		.mmap_data(cu_mmap_data),
		.mmap_type(cu_mmap_type),
		.mmap_valid(cu_mmap_valid),
		
		.cabac_in_data(cu_cabac_in_data),
		.cabac_in_valid(cu_cabac_in_valid),
		
		.cabac_out_data(cabac_out_fifo_dout),
		.cabac_out_valid(!cabac_out_fifo_empty),
		.cabac_out_read(cu_cabac_out_read),
		
		.ctbAddrInTS(ctbAddrInTS),
		.end_of_slice_segment_flag(end_of_slice_segment_flag),
		
		.out_fifo_data(cu_out_fifo_data),
		.out_fifo_valid(cu_out_fifo_valid),
		.out_fifo_full(push_full),
		.in_fifo_almost_empty(in_fifo_almost_empty),
		
		.annexb_next_bits(annexb_next_bits),
		.annexb_out_valid(annexb_out_valid),
		.annexb_read_bits(offload_read_bits),
		.annexb_read_valid(offload_read_valid),
		.annexb_offload_override(annexb_offload_override),
		.annexb_pos(annexb_pos),
		
		.cabac_op(offload_cabac_op),
		.cabac_op_valid(offload_cabac_op_valid),
		.cabac_op_done(cabac_op_done),
		
		.done(cu_done),
		// test signals
		.cu_state			(cu_state),
		.ru_state			(ru_state),
		.cu_x0				(cu_x0),
		.cu_y0				(cu_y0),
		.cu_other			(cu_other)
    );
	 
	 reg [15:0] addr;
	 reg [31:0] data;
	 reg [31:0] axi_count;
	 reg [31:0] cycle_count;
	 
	 reg ssd_finished;
	 
	 	assign axi_debug = {axi_count, data, addr, state, 13'd0};

	reg[2:0] read_wait;
	 
	always @(posedge clk) begin
		if (!rst) begin
			state <= STATE_WAITING_AWVALID;
			ssd_finished <= 1'b0;
			//annexb_rst <= RESET_ACTIVE;
			axi_count <= 0;
			cycle_count <=0;
		end else begin
			if (!in_fifo_almost_empty && !push_full) begin
				cycle_count <= cycle_count +1;
			end
		
			if (state==STATE_WAITING_BVALID && addr[15:10] == 6'd0) begin
				ssd_finished <= 1'b0;
			end else begin
				if (cu_done)
					ssd_finished <= 1'b1;
			end
			
			case (state) 
				STATE_WAITING_AWVALID: begin
					if (axi_awvalid) begin
						state <= STATE_WAITING_WVALID;
						addr <= axi_awaddr;
						axi_count <= axi_count + 1;
						reg_id <= axi_awid;
					end else if (axi_arvalid) begin
						state <= STATE_WAITING_RVALID;
						read_wait <= 7;
						addr <= axi_araddr;
						axi_count <= axi_count + 1;
						reg_id <= axi_arid;
					end
				end
				STATE_WAITING_WVALID: begin
					if (!entropy_full) begin
						if (axi_wvalid) begin
							if ( addr[15:12] == 4'b0000) begin
								state <= STATE_WAITING_BVALID;
								data <= axi_wdata;
							end else begin
								if (axi_wlast) begin
									state <= STATE_WAITING_BVALID;
								end
							end
						end
					end
				end
				STATE_WAITING_BVALID: begin
					if ( axi_bready) begin
						if (addr[15:10] == 6'd0) begin
							state <= STATE_WAITING_AWVALID;
							if (addr[9:4] == 6'd3) begin
								cabac_QP <= data[6:0]; //intercept SliceQPY here
								//annexb_rst <= ~RESET_ACTIVE; // for WPP
							end
						end else begin
							if (addr[15:12] != 4'b0000) begin // when bitstream word
								state <= STATE_WAITING_AWVALID;
							end else begin
								case (addr) 
									16'h0500: begin
										if (!push_full) begin
											state <= STATE_WAITING_AWVALID;
											//annexb_rst <= ~RESET_ACTIVE;
										end
									end
									16'h0600, 16'h0610, 16'h0620, 16'h0630: begin
										if (cabac_op_done) begin
											state <= STATE_WAITING_AWVALID;
										end
									end
									16'h0640: begin
										cabac_init_type <= data[1:0];
										state <= STATE_WAITING_AWVALID;
										//annexb_rst <= ~RESET_ACTIVE;
									end
									16'h0700: begin
										if (!entropy_full) begin
											state <= STATE_WAITING_AWVALID;
										end
									end
									16'h0810, 16'h0820: begin
										if (annexb_out_valid) begin
											state <= STATE_WAITING_AWVALID;
										end
									end
									16'h0830: begin
										state <= STATE_WAITING_AWVALID;
									end

								endcase
							end
						end
					end
				end
				STATE_WAITING_RVALID: begin
					if (axi_rready) begin
						if ( addr[15:8] == 8'h09) begin
							// give 7 cycles to handle any skip_bytes
							if (annexb_out_valid | read_wait == 3'b000) begin
								state <= STATE_WAITING_AWVALID;
							end
							read_wait <= read_wait - 1'b1;
						end else begin
							case (addr)
								16'h0400: begin
								// assumes always ready
									state <= STATE_WAITING_AWVALID;
									if (ssd_finished) begin
										ssd_finished <= 1'b0;
										//annexb_rst <= RESET_ACTIVE; // rst active
									end
								end 
								16'h0410, 16'h0420, 16'h430, 16'h0440: begin
									state <= STATE_WAITING_AWVALID;
								end
								16'h0840: begin
									state <= STATE_WAITING_AWVALID;
								end
								
							endcase
						end
					end
				end	
			endcase
		end
	end
	 
	always @(*) begin
		push_valid = 1'b0;
		push_data = 32'dx;
		cabac_in_fifo_din = {cu_cabac_in_data[29:0] , 2'b00};
		cabac_in_fifo_valid = cu_cabac_in_valid;
		cabac_out_fifo_read = cu_cabac_out_read;
		
		annexb_read_bits = 4'dx;
		annexb_read_valid = 1'b0;
		
		annexb_control = 1'b1; // default zynq
		annexb_skip_to_NAL = 1'b0;
		
		cu_mmap_valid = 1'b0;
		cu_mmap_data = 16'dx;
		cu_mmap_type = 8'dx;
		
		cabac_op = 2'dx;
		cabac_op_valid = 1'b0;
		
		entropy_valid = 1'b0;
		entropy_in = 32'dx;
		
		axi_arready = 1'b0;
		axi_wready = 1'b0;
		axi_awready = 1'b0;
		
		axi_bid = reg_id;
		axi_bresp = 2'b00;
		axi_bvalid = 1'b0;
		
		axi_rdata = 32'd0;
		axi_rid = reg_id;
		axi_rlast = 1'b0;
		axi_rresp = 2'b00;
		axi_rvalid = 1'b0;
		if (!rst) begin
		
		end else begin
			push_data = cu_out_fifo_data;
			push_valid = cu_out_fifo_valid;
			cabac_op = offload_cabac_op;
			cabac_op_valid = offload_cabac_op_valid;
			annexb_read_bits = offload_read_bits;
			annexb_read_valid = offload_read_valid;
			case (state) 
				STATE_WAITING_AWVALID: begin
					axi_awready = 1'b1;
					axi_arready = 1'b1;
				end
				STATE_WAITING_WVALID: begin
					if (!entropy_full) begin
						axi_wready = 1'b1;
						if (addr[15:12] != 4'b0000) begin
							entropy_in =  {axi_wdata[7:0], axi_wdata[15:8], axi_wdata[23:16], axi_wdata[31:24]};									
							entropy_valid = 1'b1;							
						end
					end
					
				end
				STATE_WAITING_BVALID: begin
					if (axi_bready) begin
						if (addr[15:10] == 6'd0) begin
							cu_mmap_type = addr[9:4];
							cu_mmap_data = data[15:0];
							cu_mmap_valid = 1'b1;
							axi_bvalid = 1'b1;
						end else begin
							if (addr[15:12] != 4'b0000) begin
								axi_bvalid = 1'b1;
							end else begin
								case (addr) 								
									16'h0500: begin
										push_data = {data[7:0], data[15:8], data[23:16], data[31:24]};
										if (!push_full) begin
											push_valid = 1'b1;
											axi_bvalid = 1'b1;
										end
									end
									16'h0600, 16'h0610, 16'h0620, 16'h0630: begin
										cabac_op = addr[5:4];
										cabac_op_valid = 1'b1;
										if (cabac_op_done) axi_bvalid = 1'b1;
									end
									16'h0640: begin
										axi_bvalid = 1'b1;
									end
									16'h0700: begin
										if (!entropy_full) begin
											axi_bvalid = 1'b1;
											entropy_in =  {data[7:0], data[15:8], data[23:16], data[31:24]};									
											entropy_valid = 1'b1;							
										end
									end
									16'h810, 16'h820: begin
										if (annexb_out_valid) begin
											axi_bvalid = 1'b1;
											if (addr == 16'h810) begin
												annexb_read_bits = 4'd8-annexb_pos[2:0];
											end else begin
												annexb_read_bits = (annexb_pos[2:0] == 3'b000) ? 4'd0 : 4'd8-annexb_pos[2:0];
											end
											annexb_read_valid = 1'b1;
											annexb_control = 1'b0;
										end
									end
									16'h830: begin
										axi_bvalid = 1'b1;
										annexb_skip_to_NAL = 1'b1;
									end
								endcase
							end
						end
					end
				end
				STATE_WAITING_RVALID: begin
					if (axi_rready) begin
						if ( addr[15:8] == 8'h09) begin
							// give 7 cycles to handle any skip_bytes
							if (annexb_out_valid | read_wait == 3'b000) begin
								axi_rlast = 1'b1;
								axi_rvalid = 1'b1;
								axi_rdata[31] = !annexb_out_valid;
								case (addr[7:4]) 
									1: axi_rdata[30:0] = { 29'b0, annexb_next_bits[7] };
									2: axi_rdata[30:0] = { 28'b0, annexb_next_bits[7:6] };
									3: axi_rdata[30:0] = { 27'b0, annexb_next_bits[7:5] };
									4: axi_rdata[30:0] = { 26'b0, annexb_next_bits[7:4] };
									5: axi_rdata[30:0] = { 25'b0, annexb_next_bits[7:3] };
									6: axi_rdata[30:0] = { 24'b0, annexb_next_bits[7:2] };
									7: axi_rdata[30:0] = { 23'b0, annexb_next_bits[7:1] };
									8: axi_rdata[30:0] = { 22'b0, annexb_next_bits[7:0] };
									default : axi_rdata[30:0] = 30'b0;
								endcase
								annexb_control = 1'b0;
								annexb_read_bits = addr[7:4];
								annexb_read_valid = annexb_out_valid;
							end
						end else begin
							case (addr)
							
								16'h0400: begin
								//if (ssd_finished) begin
									axi_rdata = {(ssd_finished ? 16'd0: 16'd1) ,ctbAddrInTS };
									axi_rlast = 1'b1;
									axi_rvalid = 1'b1;
									
								//end
								end
								16'h0410: begin
									axi_rdata = input_hevc_data_count;
									axi_rlast = 1'b1;
									axi_rvalid = 1'b1;
								end
								16'h0420: begin
									axi_rdata = cabac_to_res_data_count;
									axi_rlast = 1'b1;
									axi_rvalid = 1'b1;
								end
								16'h0430: begin
									axi_rdata = cycle_count;
									axi_rlast = 1'b1;
									axi_rvalid = 1'b1;
								end
								16'h0440: begin
									axi_rdata = end_of_slice_segment_flag;
									axi_rlast = 1'b1;
									axi_rvalid = 1'b1;
								end
								16'h0840: begin
									axi_rdata = annexb_out_valid;
									axi_rlast = 1'b1;
									axi_rvalid = 1'b1;
								end
								
							
							endcase
						end
					end
				end
			endcase
		end
	end
endmodule
