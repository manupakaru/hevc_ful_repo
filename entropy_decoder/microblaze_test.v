`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   14:50:49 12/26/2013
// Design Name:   mb
// Module Name:   C:/Users/Maleen/Uni/HEVC/EntropyDecoder/microblaze_test.v
// Project Name:  EntropyDecoder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: mb
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module microblaze_test;

	// Inputs
	reg Clk;
	reg Reset;
	reg IO_Ready;
	reg [31:0] IO_Read_Data;

	// Outputs
	wire IO_Addr_Strobe;
	wire IO_Read_Strobe;
	wire IO_Write_Strobe;
	wire Trace_Valid_Instr;
	wire Trace_Reg_Write;
	wire Trace_Jump_Taken;
	wire Trace_Delay_Slot;
	wire Trace_Data_Access;
	wire Trace_Data_Read;
	wire Trace_Data_Write;
	wire Trace_MB_Halted;
	wire [31:0] IO_Address;
	wire [3:0] IO_Byte_Enable;
	wire [31:0] IO_Write_Data;
	wire [31:0] GPO1;
	wire [0:31] Trace_Instruction;
	wire [0:31] Trace_PC;
	wire [0:4] Trace_Reg_Addr;
	wire [0:14] Trace_MSR_Reg;
	wire [0:31] Trace_New_Reg_Value;
	wire [0:31] Trace_Data_Address;
	wire [0:31] Trace_Data_Write_Value;
	wire [0:3] Trace_Data_Byte_Enable;

	// Instantiate the Unit Under Test (UUT)
	mb mcs_0 (
		.Clk(Clk), 
		.Reset(Reset), 
		.IO_Ready(IO_Ready), 
		.IO_Addr_Strobe(IO_Addr_Strobe), 
		.IO_Read_Strobe(IO_Read_Strobe), 
		.IO_Write_Strobe(IO_Write_Strobe), 
		.Trace_Valid_Instr(Trace_Valid_Instr), 
		.Trace_Reg_Write(Trace_Reg_Write), 
		.Trace_Jump_Taken(Trace_Jump_Taken), 
		.Trace_Delay_Slot(Trace_Delay_Slot), 
		.Trace_Data_Access(Trace_Data_Access), 
		.Trace_Data_Read(Trace_Data_Read), 
		.Trace_Data_Write(Trace_Data_Write), 
		.Trace_MB_Halted(Trace_MB_Halted), 
		.IO_Read_Data(IO_Read_Data), 
		.IO_Address(IO_Address), 
		.IO_Byte_Enable(IO_Byte_Enable), 
		.IO_Write_Data(IO_Write_Data), 
		.GPO1(GPO1), 
		.Trace_Instruction(Trace_Instruction), 
		.Trace_PC(Trace_PC), 
		.Trace_Reg_Addr(Trace_Reg_Addr), 
		.Trace_MSR_Reg(Trace_MSR_Reg), 
		.Trace_New_Reg_Value(Trace_New_Reg_Value), 
		.Trace_Data_Address(Trace_Data_Address), 
		.Trace_Data_Write_Value(Trace_Data_Write_Value), 
		.Trace_Data_Byte_Enable(Trace_Data_Byte_Enable)
	);

	initial begin
		// Initialize Inputs
		Clk = 0;
		Reset = 0;
		IO_Ready = 0;
		IO_Read_Data = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
	always begin
		#5 Clk = ~Clk;
	end   
      
endmodule

