`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:02:32 12/26/2013 
// Design Name: 
// Module Name:    header_parser 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module header_parser(
    input clk,
    input rst,
    output reg  IO_Addr_Strobe,
    output reg [31:0] IO_Address,
    output reg [31:0] IO_Write_Data,
    input [31:0] IO_Read_Data,
    input IO_Ready,
    output reg [31:0] GPO1,
    input in_fifo_almost_empty,
    
    output reg [6:0] NAL_header,
    output reg [11:0] ctbAddrInTS,
    output reg [6:0] state
    );
	`include "def_cu.v"
	
	
	
	
	reg [3:0] push_count;
	reg [7:0] wait_count;
	
	reg [6:0] ret_state;
	
	reg [3:0] ue_state;
	reg [3:0] ue_numZeros;
	reg ue_addr_strobe;
	reg [3:0] ue_req_bits;
	reg [15:0] ue_return;
	reg ue_valid, ue_done;
	
	reg [11:0] xCtb, yCtb;
	
	reg [2:0] div_state;
	reg [11:0] div_d1, div_d2, div_quot, div_rem;
	reg div_valid, div_done;
	
	reg RapPicFlag, IdrPicFlag, RefPicture;
	
	reg [11:0] pic_width_in_luma_samples, pic_height_in_luma_samples;
	
	reg [2:0] Log2CtbSizeY;
	reg signed [4:0] pps_cb_qp_offset;
	reg signed [4:0] pps_cr_qp_offset;
	reg signed [3:0] pps_beta_offset_div2;
	reg signed [3:0] pps_tc_offset_div2;
	reg strong_intra_smoothing;
	reg constrained_intra_pred;
	
	reg [5:0] num_short_term_ref;
	reg weighted_pred;
	reg weighted_bipred;
	reg [2:0] parallel_merge_level;
	
	reg sign_data_hiding_flag;
	reg transform_skip_enabled_flag;
	
	reg sps_temporal_mvp_enable_flag;
	reg loop_filter_across_slices_enabled_flag;
	
	reg first_slice_segment_in_pic_flag;
	reg no_output_of_prior_pics_flag;
	reg [7:0] slice_pic_parameter_set_id;
	reg [9:0] slice_segment_address;
	reg dependent_slice_segment_flag;
	
	reg [9:0] ctbAddrInRS;
	reg [7:0] pic_order_cnt_lsb;
	reg [1:0] slice_type;
	reg short_term_ref_pic_set_sps_flag;
	reg slice_temporal_mvp_enable_flag;
	
	reg [7:0] prevPicOrderCntLsb, MaxPicOrderCntLsb;
	reg [15:0] prevPicOrderCntMsb, PicOrderCntMsb;
	reg [15:0] PicOrderCntVal;
	
	reg signed [6:0] slice_qp_delta;
	reg [6:0] SliceQPY;
	
	always @(posedge clk) begin
		if (~rst) begin
			IO_Addr_Strobe <= 1'b0;
			state <= `STATE_DECODE_NAL;
			push_count <= 4'd0;
			ue_valid <= 1'b0;
			
			Log2CtbSizeY <= 3'd6;
			pps_cb_qp_offset <= 5'd0;
			pps_cr_qp_offset <= 5'd0;
			pps_beta_offset_div2 <= 4'd0;
			pps_tc_offset_div2 <= 4'd0;
			strong_intra_smoothing <= 1'b1;
			constrained_intra_pred <= 1'b0;
			
			num_short_term_ref <= 6'd1;
			weighted_pred <= 1'b0;
			weighted_bipred <= 1'b0;
			parallel_merge_level <= 3'd0;
			
			sign_data_hiding_flag <= 1'b1;
			transform_skip_enabled_flag <= 1'b1;
			
			sps_temporal_mvp_enable_flag <= 1'b1;
			loop_filter_across_slices_enabled_flag <= 1'b1;
		end else begin
			IO_Addr_Strobe <= 1'b0;
			case (state)
				`STATE_DECODE_NAL : begin //0
                    if(!in_fifo_almost_empty)
                        state <= `STATE_DECODE_NAL_HEADER_PUSH;
				end
				`STATE_DECODE_NAL_HEADER_PUSH : begin //1
					state <= `STATE_DECODE_NAL_HEADER_READ;
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hC0000700;
				end
				`STATE_DECODE_NAL_HEADER_READ : begin //2
					if (IO_Ready) begin
						NAL_header <= IO_Read_Data[6:0];
						state <= `STATE_DECODE_NAL_RESERVED_ZERO_6_PUSH;
						GPO1 <= IO_Read_Data[6:0];
					end
				end
				`STATE_DECODE_NAL_RESERVED_ZERO_6_PUSH :begin //3
					state <= `STATE_DECODE_NAL_RESERVED_ZERO_6_READ;
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hC0000600;
				end
				`STATE_DECODE_NAL_RESERVED_ZERO_6_READ: begin //4
					if (IO_Ready) begin
						state <= `STATE_DECODE_NAL_TEMPORAL_ID_PUSH;
					end
				end
				`STATE_DECODE_NAL_TEMPORAL_ID_PUSH :begin //5
					state <= `STATE_DECODE_NAL_TEMPORAL_ID_READ;
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hC0000300;
				end
				`STATE_DECODE_NAL_TEMPORAL_ID_READ: begin //6
					if (IO_Ready) begin
						push_count <= 4'd0;
						case (NAL_header)
							32 : state <= `STATE_SKIP_TO_NAL_PUSH;
							33 : state <= `STATE_SPS_8_BITS_PUSH;
							34 : state <= `STATE_SEND_H00;
							19 : begin
								RapPicFlag <= 1'b1;
								IdrPicFlag <= 1'b1;
								RefPicture <= 1'b1;
								state <= `STATE_SSL_FIRST_SLICE_SEGMENT_PUSH;
							end
							0, 1, 9: begin
								RapPicFlag <= 1'b0;
								IdrPicFlag <= 1'b0;
								RefPicture <= (NAL_header == 6'd1 || NAL_header == 6'd9);
								state <= `STATE_SSL_FIRST_SLICE_SEGMENT_PUSH;
							end
							21 : begin //CRA_NUT
								RapPicFlag <= 1'b1;
								IdrPicFlag <= 1'b0;
								RefPicture <= 1'b1;
								state <= `STATE_SSL_FIRST_SLICE_SEGMENT_PUSH;
							end
						endcase
					end
				end
				
				// SPS
				`STATE_SPS_8_BITS_PUSH: begin //7
					state <= `STATE_SPS_8_BITS_READ;
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hC0000800;
					push_count <= push_count +1'b1;
				end
				`STATE_SPS_8_BITS_READ: begin //8
					if (IO_Ready) begin
						if (push_count == 4'd13) begin
							state <= `STATE_SPS_UE_1_PUSH;
							push_count <= 4'd0;
						end else begin
							state <= `STATE_SPS_8_BITS_PUSH;
						end
					end
				end
				`STATE_SPS_UE_1_PUSH : begin //9 
					// 0 -sps_seq_parameter_set_id, 1 - chroma_format_idc, 2-pic_width, 3 - pic_height
					//ue_numZeros <= 4'd0;
					//ue_state <= `STATE_UE_BIT_PUSH;
					state <= `STATE_SPS_UE_1_READ; 
					ue_valid <= 1'b1;
					//ue_return <= 16'd1;
					push_count <= push_count +1'b1;
				end
				`STATE_SPS_UE_1_READ : begin //10
					if (!ue_done) begin
						IO_Addr_Strobe <= ue_addr_strobe;
						if (ue_addr_strobe) begin // otherwise latch
							IO_Address <= {20'hC0000, ue_req_bits , 8'h00};
						end 
					end else begin
						ue_valid <= 1'b0;
						if (push_count == 4'd4) begin
							state <= `STATE_SPS_CONFORMANCE_PUSH;
							push_count <= 4'd0;
						end else begin
							state <= `STATE_SPS_UE_1_PUSH;
						end
						case (push_count) 
							3: begin
								pic_width_in_luma_samples <= ue_return[11:0];
								IO_Address <= 32'hC0005000;
								IO_Write_Data <= ue_return;
								IO_Addr_Strobe <= 1'b1;
							end
							4: begin
								pic_height_in_luma_samples <= ue_return[11:0];
								IO_Address <= 32'hC0005100;
								IO_Write_Data <= ue_return;
								IO_Addr_Strobe <= 1'b1;
							end
						endcase
					end
				end
				`STATE_SPS_CONFORMANCE_PUSH : begin //11
					state <= `STATE_SPS_CONFORMANCE_READ;
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hC0000100;
				end
				`STATE_SPS_CONFORMANCE_READ: begin //12
					if (IO_Ready) begin
						state <= `STATE_SPS_UE_2_PUSH;
						if (IO_Read_Data[0]) begin
							push_count <= 4'd7;
						end else begin
							push_count <= 4'd3;
						end
					end
				end
				`STATE_SPS_UE_2_PUSH : begin //13 
					// 0 -sps_seq_parameter_set_id, 1 - chroma_format_idc, 2-pic_width, 3 - pic_height
					//ue_numZeros <= 4'd0;
					//ue_state <= `STATE_UE_BIT_PUSH;
					state <= `STATE_SPS_UE_2_READ; 
					ue_valid <= 1'b1;
					//ue_return <= 16'd1;
					push_count <= push_count -1'b1;
				end
				`STATE_SPS_UE_2_READ : begin //14
					if (!ue_done) begin
						IO_Addr_Strobe <= ue_addr_strobe;
						if (ue_addr_strobe) begin // otherwise latch
							IO_Address <= {20'hC0000, ue_req_bits , 8'h00};
						end 
					end else begin
						ue_valid <= 1'b0;
						if (push_count == 4'd0) begin
							state <= `STATE_SKIP_TO_NAL_PUSH; // stop here for now
							push_count <= 4'd0;
						end else begin
							state <= `STATE_SPS_UE_2_PUSH;
						end
					end
				end
				
				`STATE_SSL_FIRST_SLICE_SEGMENT_PUSH : begin //64
					state <= `STATE_SSL_FIRST_SLICE_SEGMENT_READ;
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hC0000100;
				end
				`STATE_SSL_FIRST_SLICE_SEGMENT_READ : begin //65
					if (IO_Ready) begin
						state <= `STATE_SSL_NO_OUTPUT_FLAG_PUSH;
						first_slice_segment_in_pic_flag <= IO_Read_Data[0];
					end
				end
				`STATE_SSL_NO_OUTPUT_FLAG_PUSH : begin //66
					if (RapPicFlag) begin
						state <= `STATE_SSL_NO_OUTPUT_FLAG_READ;
						IO_Addr_Strobe <= 1'b1;
						IO_Address <= 32'hC0000100;
					end else begin
						state <= `STATE_SSL_PIC_PARAMETER_ID_PUSH;
					end
				end
				`STATE_SSL_NO_OUTPUT_FLAG_READ: begin //67
					if (IO_Ready) begin
						state <= `STATE_SSL_PIC_PARAMETER_ID_PUSH;
						no_output_of_prior_pics_flag <= IO_Read_Data[0];
					end
				end
				`STATE_SSL_PIC_PARAMETER_ID_PUSH : begin //68
					//ue_numZeros <= 4'd0;
					//ue_state <= `STATE_UE_BIT_PUSH;
					state <= `STATE_SSL_PIC_PARAMETER_ID_READ; 
					ue_valid <= 1'b1;
					//ue_return <= 16'd1;
				end
				`STATE_SSL_PIC_PARAMETER_ID_READ : begin //69
					if (!ue_done) begin
						IO_Addr_Strobe <= ue_addr_strobe;
						if (ue_addr_strobe) begin // otherwise latch
							IO_Address <= {20'hC0000, ue_req_bits , 8'h00};
						end 
					end else begin
						ue_valid <= 1'b0;
						slice_pic_parameter_set_id <= ue_return[7:0];
						state <= `STATE_SSL_NOT_FIRST_SLICE;
					end
				end
				`STATE_SSL_NOT_FIRST_SLICE : begin //70
					slice_segment_address <= 10'd0;
					dependent_slice_segment_flag <= 1'b0;
					ctbAddrInRS <= 10'd0;
					ctbAddrInTS <= 10'd0;
					pic_order_cnt_lsb <= 4'd0;
					PicOrderCntVal <= 10'd0;
					PicOrderCntMsb <= 6'd0;
					MaxPicOrderCntLsb <= 4'd15;
					slice_temporal_mvp_enable_flag <= 1'b0;
					state <= `STATE_SSL_SLICE_TYPE_PUSH;
				end
				`STATE_SSL_SLICE_TYPE_PUSH : begin //71
					//ue_numZeros <= 4'd0;
					//ue_state <= `STATE_UE_BIT_PUSH;
					state <= `STATE_SSL_SLICE_TYPE_READ; 
					ue_valid <= 1'b1;
					//ue_return <= 16'd1;
				end
				`STATE_SSL_SLICE_TYPE_READ : begin //72
					if (!ue_done) begin
						IO_Addr_Strobe <= ue_addr_strobe;
						if (ue_addr_strobe) begin // otherwise latch
							IO_Address <= {20'hC0000, ue_req_bits , 8'h00};
						end 
					end else begin
						ue_valid <= 1'b0;
						slice_type <= ue_return[1:0];
						state <= `STATE_SSL_PIC_ORDER_CNT_PUSH;
					end
				end
				`STATE_SSL_PIC_ORDER_CNT_PUSH : begin //73
					if (!IdrPicFlag) begin
						state <= `STATE_SSL_PIC_ORDER_CNT_READ;
						IO_Addr_Strobe <= 1'b1;
						IO_Address <= 32'hC0000800;
					end else begin
						state <= `STATE_SEND_H10;
					end
				end
				`STATE_SSL_PIC_ORDER_CNT_READ : begin //74
					if (IO_Ready) begin
						state <= `STATE_SSL_SHORT_TERM_RPS_PUSH;
						pic_order_cnt_lsb <= IO_Read_Data[7:0];
					end
				end
				`STATE_SSL_SHORT_TERM_RPS_PUSH : begin //75
					if ( (pic_order_cnt_lsb < prevPicOrderCntLsb) & ( (prevPicOrderCntLsb-pic_order_cnt_lsb) >= 4'd7)) begin
							PicOrderCntMsb <= prevPicOrderCntMsb + MaxPicOrderCntLsb;
					end else if ( (pic_order_cnt_lsb > prevPicOrderCntLsb) & ( (pic_order_cnt_lsb-prevPicOrderCntLsb) >= 4'd7)) begin
							PicOrderCntMsb <= prevPicOrderCntMsb - MaxPicOrderCntLsb;
					end else begin
						PicOrderCntMsb <= prevPicOrderCntMsb;
					end
					state <= `STATE_SSL_SHORT_TERM_RPS_READ;
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hC0000100;
				end
				`STATE_SSL_SHORT_TERM_RPS_READ : begin //76
					PicOrderCntVal <= PicOrderCntMsb + pic_order_cnt_lsb;
					if (IO_Ready) begin
						state <= `STATE_SSL_SLICE_TEMPORAL_MVP_PUSH;
						short_term_ref_pic_set_sps_flag <= IO_Read_Data[0];
					end
				end
				`STATE_SSL_SLICE_TEMPORAL_MVP_PUSH : begin //77
					
					if (sps_temporal_mvp_enable_flag) begin
						state <= `STATE_SSL_SLICE_TEMPORAL_MVP_READ;
						IO_Addr_Strobe <= 1'b1;
						IO_Address <= 32'hC0000100;
					end else begin
						state <= `STATE_SEND_H10;
					end
				end
				`STATE_SSL_SLICE_TEMPORAL_MVP_READ : begin //78
					if (IO_Ready) begin
						state <= `STATE_SEND_H10;
						slice_temporal_mvp_enable_flag <= IO_Read_Data[0];
					end
				end
				
				`STATE_SEND_H10 : begin //79
					prevPicOrderCntLsb <= pic_order_cnt_lsb;
					prevPicOrderCntMsb <= PicOrderCntMsb;
					if (first_slice_segment_in_pic_flag) begin
						IO_Addr_Strobe <= 1'b1;
						IO_Address <= 32'hC0004000;
						IO_Write_Data <= { 24'd0,8'h10 };
						state <= `STATE_SEND_POC;
					end else begin
						state <= `STATE_SSL_SLICE_QP_PUSH;
					end
				end
				
				`STATE_SEND_POC : begin //80
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hC0004000;
					IO_Write_Data <= { PicOrderCntVal };
					state <= `STATE_SSL_SLICE_QP_PUSH;
					GPO1 <= {PicOrderCntVal[15:0],16'd5000};
				end
				`STATE_SSL_SLICE_QP_PUSH: begin //81
					//ue_numZeros <= 4'd0;
					//ue_state <= `STATE_UE_BIT_PUSH;
					state <= `STATE_SSL_SLICE_QP_READ; 
					ue_valid <= 1'b1;
					GPO1 <= NAL_header;
					//ue_return <= 16'd1;
				end
				`STATE_SSL_SLICE_QP_READ : begin //82
					//ue_valid <= 1'b0;
					if (!ue_done) begin
						IO_Addr_Strobe <= ue_addr_strobe;
						if (ue_addr_strobe) begin // otherwise latch
							IO_Address <= {20'hC0000, ue_req_bits , 8'h00};
						end 
					end else begin
						ue_valid <= 1'b0;
						slice_qp_delta <= ue_return[0] ? (ue_return[6:0]+1'b1)>>1 : -ue_return[6:1];
						state <= `STATE_SSL_LF_ACROSS_SLICES_PUSH;
					end
				end
				`STATE_SSL_LF_ACROSS_SLICES_PUSH : begin //83
					SliceQPY <= 7'd26 + slice_qp_delta;
					if (loop_filter_across_slices_enabled_flag) begin
						state <= `STATE_SSL_LF_ACROSS_SLICES_READ;
						IO_Addr_Strobe <= 1'b1;
						IO_Address <= 32'hC0000100;
					end else begin
						state <= `STATE_SSL_BYTE_ALIGNMENT;
					end
				end
				`STATE_SSL_LF_ACROSS_SLICES_READ : begin //84
					if (IO_Ready) begin
						state <= `STATE_SSL_BYTE_ALIGNMENT;					
					end 
				end
				`STATE_SSL_BYTE_ALIGNMENT : begin //85
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hC0001300;
					IO_Write_Data <= 32'd1;
					state <= `STATE_SSL_SEND_QP1;
				end
				`STATE_SSL_SEND_QP1 : begin //86
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hC0001100;
					IO_Write_Data <= SliceQPY;
					state <= `STATE_SSL_SEND_QP2;
				end
				`STATE_SSL_SEND_QP2 : begin //87
					IO_Addr_Strobe <= 1'b1; 
					IO_Address <= 32'hC0005200;
					IO_Write_Data <= SliceQPY;
					state <= `STATE_SSL_SEND_TO_HARD;
				end
				`STATE_SSL_SEND_TO_HARD : begin //88
					IO_Addr_Strobe <= 1'b1; 
					IO_Address <= 32'hD0003020;
					IO_Write_Data <= 32'd0;
					state <= `STATE_SSL_SEND_INITTYPE;
				end
				`STATE_SSL_SEND_INITTYPE : begin //89
					IO_Addr_Strobe <= 1'b1; 
					IO_Address <= 32'hC0001200;
					IO_Write_Data <= 32'd0;
					state <= `STATE_SSL_INITIALIZE_CABAC;
				end
				`STATE_SSL_INITIALIZE_CABAC : begin //90
					IO_Addr_Strobe <= 1'b1; 
					IO_Address <= 32'hC0001400;
					IO_Write_Data <= 32'd1;
					state <= `STATE_WAIT_FOR_CABAC_INITIALIZE;
					wait_count <= 8'd255;
				end
				`STATE_WAIT_FOR_CABAC_INITIALIZE : begin //91
					if (wait_count ==8'd0) 
						state <= `STATE_SEND_H15;
					wait_count <= wait_count - 1'd1;
				end
				`STATE_SEND_H15 : begin //92
					IO_Addr_Strobe <= 1'b1; 
					IO_Address <= 32'hC0004000;
					IO_Write_Data <= { 12'd0, 12'd0, 8'h15};
					ret_state <= `STATE_SEND_H11;
					state <= `STATE_WAIT_TILL_READY;
				end
				`STATE_SEND_H11 : begin //93
					IO_Addr_Strobe <= 1'b1; 
					IO_Address <= 32'hC0004000;
					IO_Write_Data <= { 2'd0, 1'b0, 2'd2, 3'd0,8'd0, 2'd0, slice_temporal_mvp_enable_flag, 4'd0, 1'b1, 8'h11};
					ret_state <= `STATE_SEND_H12;
					state <= `STATE_WAIT_TILL_READY;
				end
				`STATE_SEND_H12 : begin //94
					IO_Addr_Strobe <= 1'b1; 
					IO_Address <= 32'hC0004000;
					IO_Write_Data <= {24'd0, 8'h12};
					ret_state <= `STATE_SEND_H16;
					state <= `STATE_WAIT_TILL_READY;
				end
				`STATE_SEND_H16 : begin //95
					IO_Addr_Strobe <= 1'b1; 
					IO_Address <= 32'hC0004000;
					IO_Write_Data <= {24'd0, 8'h16};
					ret_state <= `STATE_SEND_H17;
					state <= `STATE_WAIT_TILL_READY;
				end
				`STATE_SEND_H17 : begin //96
					IO_Addr_Strobe <= 1'b1; 
					IO_Address <= 32'hC0004000;
					IO_Write_Data <= {16'd0, 8'h1e, 8'h17};
					ret_state <= `STATE_CTU_BEGIN;
					state <= `STATE_WAIT_TILL_READY;
				end
				
				
				`STATE_SEND_H00 : begin //100
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hC0004000;
					IO_Write_Data <= { pic_height_in_luma_samples, pic_width_in_luma_samples,8'h00 };
					state <= `STATE_SEND_H01;
				end
				`STATE_SEND_H01 : begin //101
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hC0004000;
					IO_Write_Data <= { 1'b0,constrained_intra_pred,strong_intra_smoothing,pps_tc_offset_div2,pps_beta_offset_div2,pps_cr_qp_offset,pps_cb_qp_offset,Log2CtbSizeY,8'h01};
					state <= `STATE_SEND_H02;
				end
				`STATE_SEND_H02 : begin //102
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hC0004000;
					IO_Write_Data <= {15'd0, parallel_merge_level,weighted_bipred,weighted_pred, num_short_term_ref,8'h02};
					state <= `STATE_SEND_H03;
				end
				`STATE_SEND_H03 : begin // 103
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hC0004000;
					IO_Write_Data <= {8'd0, 8'd0, 8'd0, 8'h03 };
					state <= `STATE_SEND_H04;
				end
				`STATE_SEND_H04 : begin //104
					IO_Addr_Strobe <= 1'b0;
					//IO_Address <= 32'hC0004000;
					//IO_Write_Data <= {8'd0, 8'd0, 8'd0, 8'h03 };
					state <= `STATE_SEND_SIGN_DATA_HIDING;
				end
				`STATE_SEND_SIGN_DATA_HIDING : begin //105
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hD0100010;
					IO_Write_Data <= sign_data_hiding_flag;
					state <= `STATE_SEND_TRANSFORM_SKIP_ENABLED;
				end
				`STATE_SEND_TRANSFORM_SKIP_ENABLED : begin //106
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hD0100010;
					IO_Write_Data <= transform_skip_enabled_flag;
					state <= `STATE_SKIP_TO_NAL_PUSH;
				end
				
				`STATE_CTU_BEGIN : begin //110
					div_d1 <= ctbAddrInRS;
					div_d2 <= 30; // PicWidthInCtbsY
					div_valid <= 1'b1;
					state <= `STATE_CTU_XY_READ;
					GPO1 <= 1000;
				end
				`STATE_CTU_XY_READ : begin //111
					//div_valid <= 1'b0;
					if (div_done) begin
						div_valid <= 1'b0;
						xCtb <= {div_rem[5:0], 6'd0};
						yCtb <= {div_quot[5:0], 6'd0};
						state <= `STATE_SEND_H20;
					end
				end
				`STATE_SEND_H20 : begin //112
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hC0004000;
					IO_Write_Data <= {yCtb, xCtb, 8'h20 };
					ret_state <= `STATE_SEND_H21;
					state <= `STATE_WAIT_TILL_READY;
				end
				`STATE_SEND_H21 : begin //113
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hC0004000;
					IO_Write_Data <= {24'd0, 8'h21 };
					push_count <= 4'd0;
					ret_state <= `STATE_SEND_H30_PARAMS;
					state <= `STATE_WAIT_TILL_READY;
				end
				`STATE_SEND_H30_PARAMS : begin //114
					push_count <= push_count + 1'd1;
					IO_Addr_Strobe <= 1'b1;
					IO_Address <=  32'hD0003000;
					case (push_count)
						0: IO_Write_Data <= xCtb;
						1: IO_Write_Data <= yCtb;
						2: IO_Write_Data <= 6;
						3: IO_Write_Data <= 1;
						4: IO_Write_Data <= 1;
					endcase
					if (push_count ==4) state <= `STATE_CTU_CALL_CTU;
				end	
				`STATE_CTU_CALL_CTU : begin //115
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hD0003010;
					GPO1 <= 32'd3000;
					ret_state <= `STATE_CTU_END_OF_SLICE_SEGMENT_FLAG_PUSH;
					state <= `STATE_WAIT_TILL_READY;
				end
				`STATE_CTU_END_OF_SLICE_SEGMENT_FLAG_PUSH : begin //116
					GPO1 <= 32'd4000;
					if ( ctbAddrInRS == 509) begin // do not read end_of_slice_segment_flag when it is one; (CABAC reads 7 additional bits)
						state <= `STATE_SKIP_TO_NAL_PUSH;
						IO_Addr_Strobe <= 1'b1;
						IO_Address <= 32'hC0001400; // transfer annexb_control back to mb
						IO_Write_Data <= 0;
					end else begin
						IO_Addr_Strobe <= 1'b1;
						IO_Address <= 32'hC0001000;
						IO_Write_Data <= {6'd7, 26'd0};
						state <= `STATE_CTU_CABAC_READ;
					end
				end
				`STATE_CTU_CABAC_READ : begin
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hC0002000;
					state <= `STATE_CTU_END_OF_SLICE_SEGMENT_FLAG_READ;
				end
				`STATE_CTU_END_OF_SLICE_SEGMENT_FLAG_READ : begin
					if (IO_Ready) begin
						ctbAddrInTS <= ctbAddrInTS + 1'b1;
						ctbAddrInRS <= ctbAddrInRS + 1'b1;
						if (IO_Read_Data[0] ) begin
							state <= `STATE_SKIP_TO_NAL_PUSH;
						end else begin
							state <= `STATE_CTU_BEGIN;
						end
					end
				end
				
				`STATE_WAIT_TILL_READY : begin //125
					if (IO_Ready) begin
						state <= ret_state;
					end
				end
				`STATE_SKIP_TO_NAL_PUSH: begin //126
					IO_Addr_Strobe <= 1'b1;
					IO_Address <= 32'hC0000000;
					state <= `STATE_SKIP_TO_NAL_READ;
				end
				`STATE_SKIP_TO_NAL_READ: begin //127
					if (IO_Ready) begin			
						state <= `STATE_DECODE_NAL;
					end
				end			

			endcase
		
		end
	end
	
	always @(posedge clk) begin
		if (~rst) begin
			ue_state <= `STATE_UE_BIT_PUSH;
			ue_done <= 1'b0;
			ue_numZeros <= 4'd0;
			ue_return <= 16'd1;
		end else begin
			if (ue_valid) begin
				case (ue_state) 
					`STATE_UE_BIT_PUSH: begin //0
						ue_state <= `STATE_UE_BIT_READ;
					end
					`STATE_UE_BIT_READ : begin //1
						if (IO_Ready) begin
							if (IO_Read_Data[0] == 1'b1) begin
								ue_return <= ue_return - 1'b1;
								ue_state <= `STATE_UE_READ_BITS_1_PUSH;
							end else begin
								ue_state <= `STATE_UE_BIT_PUSH;
								ue_numZeros <= ue_numZeros + 1'b1;
								ue_return <= {ue_return[14:0] , 1'b0};
							end
						end
					end
					`STATE_UE_READ_BITS_1_PUSH : begin //2
						if (ue_numZeros > 8) begin
							ue_numZeros <= 4'd8;
							ue_state <= `STATE_UE_READ_BITS_1_READ;
						end else begin
							if (ue_numZeros == 4'd0) begin
								ue_done <= 1'b1;
								ue_state <= `STATE_UE_DONE;
							end else begin
								ue_state <= `STATE_UE_READ_BITS_2_PUSH;
							end
						end
					end
					`STATE_UE_READ_BITS_1_READ : begin //3
						if (IO_Ready) begin
							ue_return <= ue_return + {IO_Read_Data[7:0] , 8'd0};
							ue_state <= `STATE_UE_READ_BITS_2_PUSH;
						end
					end 
					`STATE_UE_READ_BITS_2_PUSH : begin //4
						ue_state <= `STATE_UE_READ_BITS_2_READ; 
					end
					`STATE_UE_READ_BITS_2_READ : begin //5
						if (IO_Ready) begin
							ue_return <= ue_return + IO_Read_Data[7:0];
							ue_state <= `STATE_UE_DONE;
							ue_done <= 1'b1;
						end
					end 
					`STATE_UE_DONE : begin
						ue_state <= `STATE_UE_BIT_PUSH;
						ue_done <= 1'b0;
						ue_numZeros <= 4'd0;
						ue_return <= 16'd1;
					end
				endcase 
			end
		end
	end
	always @(*) begin
		ue_addr_strobe = 1'd0;
		ue_req_bits = 4'dx;
		if (ue_valid) begin
			case (ue_state) 
				`STATE_UE_BIT_PUSH: begin
					ue_req_bits = 4'd1;
					ue_addr_strobe = 1'b1;
				end
				
				`STATE_UE_READ_BITS_1_PUSH : begin
					if (ue_numZeros > 8) begin
						ue_req_bits = ue_numZeros - 4'd8;
						ue_addr_strobe = 1'b1;
					end
				end
				`STATE_UE_READ_BITS_2_PUSH : begin //4
					ue_req_bits = ue_numZeros;
					ue_addr_strobe = 1'b1;
				end
			endcase 
		end
	end
	
	always @(posedge clk) begin // divider
		if (~rst) begin
			 div_state <= 2'd0;
		end else begin
			if (div_valid) begin
				case (div_state) 
					0: begin
						div_quot <= 12'd0;
						div_rem <= div_d1;
						div_state <= 3'd1;
						div_done <= 1'b0;
					end
					1: begin
						if (div_rem < div_d2) begin
							div_state <= 3'd2;
						end else begin
							div_rem <= div_rem - div_d2;
							div_quot <= div_quot + 1'b1;
						end
					end
					2: begin
						div_done <= 1'b1;
						div_state <= 3'd3;
					end
					3: begin
						div_done <= 1'b0;
						div_state <= 3'd0;
					end
				
				endcase
			end
		end
	end
	
endmodule
