`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:24:56 11/25/2013 
// Design Name: 
// Module Name:    scanOrder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module scanOrder(
    input [1:0] blk_size,
    input [1:0] scanIdx,
    input [5:0] scanPos,
    output reg [5:0] scanOrder
    );
	always @ (*) begin
		case (blk_size)
			0: begin
				case (scanIdx)
					0: begin
						scanOrder = 6'd0;
					end
					1: begin
						scanOrder = 6'd0;
					end
					2: begin
						scanOrder = 6'd0;
					end
					default : scanOrder = 6'dx;
				endcase
			end 
			1: begin
				case (scanIdx)
					0: begin
						case (scanPos[1:0])
							0: scanOrder = 0;
							1: scanOrder = 1;
							2: scanOrder = 8;
							3: scanOrder = 9;
						endcase
					end
					1: begin
						case (scanPos[1:0])
							0: scanOrder = 0;
							1: scanOrder = 8;
							2: scanOrder = 1;
							3: scanOrder = 9;
						endcase
					end
					2: begin
						case (scanPos[1:0])
							0: scanOrder = 0;
							1: scanOrder = 1;
							2: scanOrder = 8;
							3: scanOrder = 9;
						endcase
					end
					default : scanOrder = 6'dx;
				endcase
			end
			2: begin
				case (scanIdx)
					0: begin
						case (scanPos[3:0])
							  0 : scanOrder = 0;
							  1 : scanOrder = 1;
							  2 : scanOrder = 8;
							  3 : scanOrder = 2;
							  4 : scanOrder = 9;
							  5 : scanOrder = 16;
							  6 : scanOrder = 3;
							  7 : scanOrder = 10;
							  8 : scanOrder = 17;
							  9 : scanOrder = 24;
							 10 : scanOrder = 11;
							 11 : scanOrder = 18;
							 12 : scanOrder = 25;
							 13 : scanOrder = 19;
							 14 : scanOrder = 26;
							 15 : scanOrder = 27;
						endcase
					end
					1: begin
						case (scanPos[3:0])
							  0 : scanOrder = 0;
							  1 : scanOrder = 8;
							  2 : scanOrder = 16;
							  3 : scanOrder = 24;
							  4 : scanOrder = 1;
							  5 : scanOrder = 9;
							  6 : scanOrder = 17;
							  7 : scanOrder = 25;
							  8 : scanOrder = 2;
							  9 : scanOrder = 10;
							 10 : scanOrder = 18;
							 11 : scanOrder = 26;
							 12 : scanOrder = 3;
							 13 : scanOrder = 11;
							 14 : scanOrder = 19;
							 15 : scanOrder = 27;
						endcase
					end
					2: begin
						case (scanPos[3:0])
							  0 : scanOrder = 0;
							  1 : scanOrder = 1;
							  2 : scanOrder = 2;
							  3 : scanOrder = 3;
							  4 : scanOrder = 8;
							  5 : scanOrder = 9;
							  6 : scanOrder = 10;
							  7 : scanOrder = 11;
							  8 : scanOrder = 16;
							  9 : scanOrder = 17;
							 10 : scanOrder = 18;
							 11 : scanOrder = 19;
							 12 : scanOrder = 24;
							 13 : scanOrder = 25;
							 14 : scanOrder = 26;
							 15 : scanOrder = 27;

						endcase
					end
					default : scanOrder = 6'dx;
				endcase
			end
			3: begin
				case (scanIdx)
					0: begin
						case (scanPos)
							  0 : scanOrder = 0;
							  1 : scanOrder = 1;
							  2 : scanOrder = 8;
							  3 : scanOrder = 2;
							  4 : scanOrder = 9;
							  5 : scanOrder = 16;
							  6 : scanOrder = 3;
							  7 : scanOrder = 10;
							  8 : scanOrder = 17;
							  9 : scanOrder = 24;
							 10 : scanOrder = 4;
							 11 : scanOrder = 11;
							 12 : scanOrder = 18;
							 13 : scanOrder = 25;
							 14 : scanOrder = 32;
							 15 : scanOrder = 5;
							 16 : scanOrder = 12;
							 17 : scanOrder = 19;
							 18 : scanOrder = 26;
							 19 : scanOrder = 33;
							 20 : scanOrder = 40;
							 21 : scanOrder = 6;
							 22 : scanOrder = 13;
							 23 : scanOrder = 20;
							 24 : scanOrder = 27;
							 25 : scanOrder = 34;
							 26 : scanOrder = 41;
							 27 : scanOrder = 48;
							 28 : scanOrder = 7;
							 29 : scanOrder = 14;
							 30 : scanOrder = 21;
							 31 : scanOrder = 28;
							 32 : scanOrder = 35;
							 33 : scanOrder = 42;
							 34 : scanOrder = 49;
							 35 : scanOrder = 56;
							 36 : scanOrder = 15;
							 37 : scanOrder = 22;
							 38 : scanOrder = 29;
							 39 : scanOrder = 36;
							 40 : scanOrder = 43;
							 41 : scanOrder = 50;
							 42 : scanOrder = 57;
							 43 : scanOrder = 23;
							 44 : scanOrder = 30;
							 45 : scanOrder = 37;
							 46 : scanOrder = 44;
							 47 : scanOrder = 51;
							 48 : scanOrder = 58;
							 49 : scanOrder = 31;
							 50 : scanOrder = 38;
							 51 : scanOrder = 45;
							 52 : scanOrder = 52;
							 53 : scanOrder = 59;
							 54 : scanOrder = 39;
							 55 : scanOrder = 46;
							 56 : scanOrder = 53;
							 57 : scanOrder = 60;
							 58 : scanOrder = 47;
							 59 : scanOrder = 54;
							 60 : scanOrder = 61;
							 61 : scanOrder = 55;
							 62 : scanOrder = 62;
							 63 : scanOrder = 63;
						endcase
					end
					1: begin
						case (scanPos)
							  0 : scanOrder = 0;
							  1 : scanOrder = 8;
							  2 : scanOrder = 16;
							  3 : scanOrder = 24;
							  4 : scanOrder = 32;
							  5 : scanOrder = 40;
							  6 : scanOrder = 48;
							  7 : scanOrder = 56;
							  8 : scanOrder = 1;
							  9 : scanOrder = 9;
							 10 : scanOrder = 17;
							 11 : scanOrder = 25;
							 12 : scanOrder = 33;
							 13 : scanOrder = 41;
							 14 : scanOrder = 49;
							 15 : scanOrder = 57;
							 16 : scanOrder = 2;
							 17 : scanOrder = 10;
							 18 : scanOrder = 18;
							 19 : scanOrder = 26;
							 20 : scanOrder = 34;
							 21 : scanOrder = 42;
							 22 : scanOrder = 50;
							 23 : scanOrder = 58;
							 24 : scanOrder = 3;
							 25 : scanOrder = 11;
							 26 : scanOrder = 19;
							 27 : scanOrder = 27;
							 28 : scanOrder = 35;
							 29 : scanOrder = 43;
							 30 : scanOrder = 51;
							 31 : scanOrder = 59;
							 32 : scanOrder = 4;
							 33 : scanOrder = 12;
							 34 : scanOrder = 20;
							 35 : scanOrder = 28;
							 36 : scanOrder = 36;
							 37 : scanOrder = 44;
							 38 : scanOrder = 52;
							 39 : scanOrder = 60;
							 40 : scanOrder = 5;
							 41 : scanOrder = 13;
							 42 : scanOrder = 21;
							 43 : scanOrder = 29;
							 44 : scanOrder = 37;
							 45 : scanOrder = 45;
							 46 : scanOrder = 53;
							 47 : scanOrder = 61;
							 48 : scanOrder = 6;
							 49 : scanOrder = 14;
							 50 : scanOrder = 22;
							 51 : scanOrder = 30;
							 52 : scanOrder = 38;
							 53 : scanOrder = 46;
							 54 : scanOrder = 54;
							 55 : scanOrder = 62;
							 56 : scanOrder = 7;
							 57 : scanOrder = 15;
							 58 : scanOrder = 23;
							 59 : scanOrder = 31;
							 60 : scanOrder = 39;
							 61 : scanOrder = 47;
							 62 : scanOrder = 55;
							 63 : scanOrder = 63;
						endcase
					end
					2: begin
						case (scanPos)
							  0 : scanOrder = 0;
							  1 : scanOrder = 1;
							  2 : scanOrder = 2;
							  3 : scanOrder = 3;
							  4 : scanOrder = 4;
							  5 : scanOrder = 5;
							  6 : scanOrder = 6;
							  7 : scanOrder = 7;
							  8 : scanOrder = 8;
							  9 : scanOrder = 9;
							 10 : scanOrder = 10;
							 11 : scanOrder = 11;
							 12 : scanOrder = 12;
							 13 : scanOrder = 13;
							 14 : scanOrder = 14;
							 15 : scanOrder = 15;
							 16 : scanOrder = 16;
							 17 : scanOrder = 17;
							 18 : scanOrder = 18;
							 19 : scanOrder = 19;
							 20 : scanOrder = 20;
							 21 : scanOrder = 21;
							 22 : scanOrder = 22;
							 23 : scanOrder = 23;
							 24 : scanOrder = 24;
							 25 : scanOrder = 25;
							 26 : scanOrder = 26;
							 27 : scanOrder = 27;
							 28 : scanOrder = 28;
							 29 : scanOrder = 29;
							 30 : scanOrder = 30;
							 31 : scanOrder = 31;
							 32 : scanOrder = 32;
							 33 : scanOrder = 33;
							 34 : scanOrder = 34;
							 35 : scanOrder = 35;
							 36 : scanOrder = 36;
							 37 : scanOrder = 37;
							 38 : scanOrder = 38;
							 39 : scanOrder = 39;
							 40 : scanOrder = 40;
							 41 : scanOrder = 41;
							 42 : scanOrder = 42;
							 43 : scanOrder = 43;
							 44 : scanOrder = 44;
							 45 : scanOrder = 45;
							 46 : scanOrder = 46;
							 47 : scanOrder = 47;
							 48 : scanOrder = 48;
							 49 : scanOrder = 49;
							 50 : scanOrder = 50;
							 51 : scanOrder = 51;
							 52 : scanOrder = 52;
							 53 : scanOrder = 53;
							 54 : scanOrder = 54;
							 55 : scanOrder = 55;
							 56 : scanOrder = 56;
							 57 : scanOrder = 57;
							 58 : scanOrder = 58;
							 59 : scanOrder = 59;
							 60 : scanOrder = 60;
							 61 : scanOrder = 61;
							 62 : scanOrder = 62;
							 63 : scanOrder = 63;
						endcase
					end
					default : scanOrder = 6'dx;
				endcase
			end
		endcase
	end

endmodule
