`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    08:45:02 11/17/2013 
// Design Name: 
// Module Name:    mmap 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mmap(
	  clk,
	  rst,
		
	  IO_Addr_Strobe, // output IO_Addr_Strobe
	  IO_Read_Strobe, // output IO_Read_Strobe
	  IO_Write_Strobe, // output IO_Write_Strobe
	  IO_Address, // output [31 : 0] IO_Address
	  IO_Write_Data, // output [31 : 0] IO_Write_Data
	  IO_Read_Data, // input [31 : 0] IO_Read_Data
	  IO_Ready, // input IO_Ready
	  
	  annexb_next_bits,
	  annexb_out_valid,
	  annexb_read_bits,
	  annexb_read_valid,
	  annexb_control, // 0-ublaze, 1-cabac
	  annexb_offload_override,
	  annexb_pos,
	  
	  annexb_skip_to_NAL,
	  
	  push_data,
	  push_valid,
	  push_full,
	  in_fifo_almost_empty,
	  
	  cabac_QP,
	  cabac_init_type,
	  cabac_op,
	  cabac_op_valid,
	  cabac_op_done,
	  
	  cabac_in_fifo_din,
	  cabac_in_fifo_valid,
	  cabac_out_fifo_dout,
	  cabac_out_fifo_empty,
	  cabac_out_fifo_read,
		
		cu_state,
		ru_state,
		cu_x0,
		cu_y0,
		cu_other
			
    );
	`define STATE_CONT_READY 2'b00
	`define STATE_CONT_BUSY 2'b01
	`define STATE_CONT_RES 2'b10
	`define STATE_CONT_CU 2'b11

	input clk;
	input rst;
	
	input IO_Addr_Strobe; // output IO_Addr_Strobe
	input IO_Read_Strobe; // output IO_Read_Strobe
	input IO_Write_Strobe; // output IO_Write_Strobe
	input [31:0] IO_Address; // output [31 : 0] IO_Address
	input [31:0] IO_Write_Data; // output [31 : 0] IO_Write_Data
	output reg [31:0] IO_Read_Data; // input [31 : 0] IO_Read_Data
	output reg IO_Ready; // input IO_Ready
	
	input [7:0] annexb_next_bits;
	input annexb_out_valid;
	output reg [3:0] annexb_read_bits;
	output reg annexb_read_valid;
	output reg annexb_control;
	output annexb_offload_override;
	input [4:0] annexb_pos;
	
	output annexb_skip_to_NAL;
	
	output reg [31:0] push_data;
	output reg push_valid;
	input push_full;
	input in_fifo_almost_empty;
	
	output reg [6:0] cabac_QP;
	output reg [1:0] cabac_init_type;
	output reg [1:0] cabac_op;
	output reg cabac_op_valid;
	input cabac_op_done;
	
	output reg [31:0] cabac_in_fifo_din;
	output reg cabac_in_fifo_valid;
	
	input [31:0] cabac_out_fifo_dout;
	input cabac_out_fifo_empty;
	output reg cabac_out_fifo_read;
	
	//test
	output [6:0] cu_state, ru_state;
	output [11:0] cu_x0, cu_y0;
	output [255:0] cu_other;
	
	
	// Internal
	reg [1:0] state;
	//reg control_with_res;
	wire [7:0] printf_data;
	wire printf_valid;
	wire exit;
	
	reg[2:0]  h_30_state;
	//reg [31:0] h_40, h_41;
	
	reg [11:0] x0, y0;
	reg [2:0] log2CbSize;
	reg same_slice_tile_left, same_slice_tile_up;
	reg cu_transquant_bypass_flag;
	
	reg transform_skip_enabled_flag;
	reg sign_data_hiding_flag;
	
	reg [11:0] pic_width_in_luma_samples, pic_height_in_luma_samples;
	reg [5:0] SliceQPY;
	
	//reg mmap_valid;
	wire [31:0] res_cabac_in_data, res_out_fifo_data;
	wire res_cabac_in_valid , res_out_fifo_valid;
	wire res_cabac_out_read;
//	wire res_done;
	
	reg [15:0] cu_mmap_data;
	reg [7:0] cu_mmap_type;
	reg cu_mmap_valid;
	wire [31:0] cu_cabac_in_data, cu_out_fifo_data;
	wire cu_cabac_in_valid , cu_out_fifo_valid;
	wire cu_cabac_out_read;
	wire cu_done;
	
	reg [1:0] cIdx;
	reg [2:0] trafoSize;
	
	//wire [5:0] IntraPredModeY0, IntraPredModeY1 , IntraPredModeY2, IntraPredModeY3, IntraPredModeC;
	wire [1:0] CuPredMode_cur;
	wire IntraSplitFlag;
	
	assign annexb_skip_to_NAL  = IO_Addr_Strobe & (IO_Address==32'hC0000000);

	wire [3:0] offload_read_bits;
	wire offload_read_valid;
	
	wire [1:0] offload_cabac_op;
	wire offload_cabac_op_valid;

	// Handles FIFOs
	always @(*) begin
		push_valid = 1'b0;
		push_data = 32'dx;
		cabac_in_fifo_din = 32'dx;
		cabac_in_fifo_valid = 1'b0;
		cabac_out_fifo_read = 1'b0;
		case (state) 
		`STATE_CONT_READY, `STATE_CONT_BUSY : begin
			cabac_out_fifo_read = (IO_Addr_Strobe | state == `STATE_CONT_BUSY) & (IO_Address==32'hC0002000) & ~cabac_out_fifo_empty;
			case (IO_Address) 

				default : begin
					push_valid = (IO_Addr_Strobe | state == `STATE_CONT_BUSY) & (IO_Address==32'hC0004000) & ~push_full;
					push_data[7:0] = IO_Write_Data[31:24];
					push_data[15:8] = IO_Write_Data[23:16];
					push_data[23:16] = IO_Write_Data[15:8];
					push_data[31:24] = IO_Write_Data[7:0];
				end
			endcase
			
			cabac_in_fifo_din = IO_Write_Data;
			cabac_in_fifo_valid = (IO_Addr_Strobe & (IO_Address==32'hC0001000)) ;
		end 
		`STATE_CONT_CU : begin
			cabac_out_fifo_read =  cu_cabac_out_read;
			push_valid = cu_out_fifo_valid;
			push_data = cu_out_fifo_data;
			cabac_in_fifo_din = {cu_cabac_in_data[29:0], 2'b00};
			cabac_in_fifo_valid = cu_cabac_in_valid;
		end
		endcase
	end
	
	wire [15:0] ctbAddrInTS;
	wire end_of_slice_segment_flag;
	
	cu_offload cu_offload(
		.clk(clk),
		.rst(rst),
		
		.mmap_data(cu_mmap_data),
		.mmap_type(cu_mmap_type),
		.mmap_valid(cu_mmap_valid),
		
		.cabac_in_data(cu_cabac_in_data),
		.cabac_in_valid(cu_cabac_in_valid),
		
		.cabac_out_data(cabac_out_fifo_dout),
		.cabac_out_valid(!cabac_out_fifo_empty),
		.cabac_out_read(cu_cabac_out_read),
		
		.ctbAddrInTS(ctbAddrInTS),
		.end_of_slice_segment_flag(end_of_slice_segment_flag),
		
		.out_fifo_data(cu_out_fifo_data),
		.out_fifo_valid(cu_out_fifo_valid),
		.out_fifo_full(push_full),
		.in_fifo_almost_empty(in_fifo_almost_empty),
		
		.annexb_next_bits(annexb_next_bits),
		.annexb_out_valid(annexb_out_valid),
		.annexb_read_bits(offload_read_bits),
		.annexb_read_valid(offload_read_valid),
		.annexb_offload_override(annexb_offload_override),
		.annexb_pos(annexb_pos),
		
		.cabac_op(offload_cabac_op),
		.cabac_op_valid(offload_cabac_op_valid),
		.cabac_op_done(cabac_op_done),
		
		.done(cu_done),
		// test signals
		.cu_state			(cu_state),
		.ru_state			(ru_state),
		.cu_x0				(cu_x0),
		.cu_y0				(cu_y0),
		.cu_other			(cu_other)
    );
	 
	
	// STATE TRANSITION 
	always @(posedge clk) begin
		if (~rst) begin
			state <= `STATE_CONT_READY;
			annexb_control <= 1'b0;
			h_30_state <= 3'b0;
			cu_transquant_bypass_flag <= 1'b0;
		end else begin
			case (state) 
				`STATE_CONT_READY : begin
					if (IO_Addr_Strobe) begin
						case (IO_Address) 
							32'hC0000100, 32'hC0000200, 32'hC0000300, 32'hC0000400, 32'hC0000500, 32'hC0000600, 32'hC0000700, 32'hC0000800: begin
								state <= annexb_out_valid ? `STATE_CONT_READY : `STATE_CONT_BUSY;
							end
							32'hC0001100 : begin
								cabac_QP <= IO_Write_Data[6:0];
							end
							32'hC0001200 : begin
								cabac_init_type <= IO_Write_Data[1:0];
							end
							32'hC0001300, 32'hC0001310, 32'hC0001320, 32'hC0001330: begin
								state <= `STATE_CONT_BUSY;
							end
							32'hC0001400 : begin
								annexb_control <= IO_Write_Data[0];
							end
							32'hC0001500, 32'hC0001510 : begin
								state <= annexb_out_valid ? `STATE_CONT_READY : `STATE_CONT_BUSY;
							end
							32'hC0002000 : begin
								state <= ~cabac_out_fifo_empty ? `STATE_CONT_READY : `STATE_CONT_BUSY;
							end
							32'hC0004000 : begin
								state <= ~push_full ? `STATE_CONT_READY : `STATE_CONT_BUSY;
							end
							32'hC0006000 : begin // 
								state <= `STATE_CONT_CU;
							end

							default: 
								state <= `STATE_CONT_READY;
						endcase
					end else begin
						state <= `STATE_CONT_READY;
					end
				end
				`STATE_CONT_BUSY : begin
				//end else begin // `STATE_CONT_BUSY
					case (IO_Address) 
						32'hC0000100, 32'hC0000200, 32'hC0000300, 32'hC0000400, 32'hC0000500, 32'hC0000600, 32'hC0000700, 32'hC0000800 : begin						
							state <= annexb_out_valid ? `STATE_CONT_READY : `STATE_CONT_BUSY;
						end
						32'hC0001300, 32'hC0001310, 32'hC0001320, 32'hC0001330: begin
							if (cabac_op_done) begin
								state <= `STATE_CONT_READY;
							end
						end
						32'hC0001500, 32'hC0001510 : begin
							state <= annexb_out_valid ? `STATE_CONT_READY : `STATE_CONT_BUSY;
						end
						32'hC0002000 : begin
							state <= ~cabac_out_fifo_empty ? `STATE_CONT_READY : `STATE_CONT_BUSY;
						end
						32'hC0004000 : begin
							state <= ~push_full ? `STATE_CONT_READY : `STATE_CONT_BUSY;
						end
					
					endcase
				end
				`STATE_CONT_CU : begin
					if (cu_done) begin
						state <= `STATE_CONT_READY;
					end
				end
			endcase
		end
	end

	
	// OUTPUT LOGIC
	always @ (*) begin
		//mmap_valid = 1'b0;
		IO_Read_Data = 32'dx;
		annexb_read_bits = 4'dx;
		annexb_read_valid = 1'b0;
		IO_Ready = 1'b0;
		cu_mmap_valid = 1'b0;
		cu_mmap_data = 16'dx;
		cu_mmap_type = 8'dx;
		
		cabac_op = 2'dx;
		cabac_op_valid = 1'b0;
		case (state)
			`STATE_CONT_READY: begin
				if (IO_Addr_Strobe) begin
					case (IO_Address) 
						32'hC0000000 : begin // skip_to_NAL
							IO_Ready = 1'b1;
						end
						32'hC0000100, 32'hC0000200, 32'hC0000300, 32'hC0000400, 32'hC0000500, 32'hC0000600, 32'hC0000700, 32'hC0000800  : begin
							if (annexb_out_valid) begin
								annexb_read_bits = IO_Address[11:8];
								annexb_read_valid = 1'b1;
								IO_Ready = 1'b1;
							end
						end
						32'hC0001000, 32'hC0001100, 32'hC0001200, 32'hC0001400 : begin // cabac
							IO_Ready = 1'b1;
						end
						32'hC0001300, 32'hC0001310, 32'hC0001320, 32'hC0001330: begin
							cabac_op = IO_Address[5:4];
							cabac_op_valid = 1'b1;
						end
						32'hC0001500 : begin // Byte Alignment
							if (annexb_out_valid) begin
								annexb_read_bits = 4'd8-annexb_pos[2:0];
								annexb_read_valid = 1'b1;
								IO_Ready = 1'b1;
							end
						end
						32'hC0001510 : begin // Byte Alignment if bit!=0
							if (annexb_out_valid) begin
								if (annexb_pos[2:0] != 3'd0) begin
									annexb_read_bits = 4'd8-annexb_pos[2:0];
									annexb_read_valid = 1'b1;
									IO_Ready = 1'b1;
								end else begin
									annexb_read_valid = 1'b0;
									IO_Ready = 1'b1;
								end
							end
						end
						32'hC0002000: begin
							IO_Ready = ~cabac_out_fifo_empty;
						end
						32'hC0004000 : begin
							IO_Ready = ~push_full;
						end
						// parameter_passing 
						32'hC0005000, 32'hC0005010, 32'hC0005020, 32'hC0005030, 32'hC0005040, 32'hC0005050, 32'hC0005060, 32'hC0005070,
						32'hC0005080, 32'hC0005090, 32'hC00050a0, 32'hC00050b0, 32'hC00050c0, 32'hC00050d0, 32'hC00050e0, 32'hC00050f0,
						32'hC0005100, 32'hC0005110, 32'hC0005120, 32'hC0005130, 32'hC0005140, 32'hC0005150, 32'hC0005160, 32'hC0005170,
						32'hC0005180, 32'hC0005190, 32'hC00051a0, 32'hC00051b0, 32'hC00051c0, 32'hC00051d0, 32'hC00051e0, 32'hC00051f0,
						32'hC0005200, 32'hC0005210, 32'hC0005220, 32'hC0005230, 32'hC0005240, 32'hC0005250, 32'hC0005260, 32'hC0005270
							: begin // pic_width
							cu_mmap_type = IO_Address[11:4];
							cu_mmap_data = IO_Write_Data[15:0];
							cu_mmap_valid = 1'b1;
							IO_Ready = 1'b1;
						end
						32'hC0006000 : begin
							cu_mmap_type = 8'd255;
							cu_mmap_data = IO_Write_Data[15:0];
							cu_mmap_valid = 1'b1;
						end
									
					endcase
					case (IO_Address)
						32'hC0000100 : IO_Read_Data = {31'd0 , annexb_next_bits[7]};
						32'hC0000200 : IO_Read_Data = {30'd0 , annexb_next_bits[7:6]};	
						32'hC0000300 : IO_Read_Data = {29'd0 , annexb_next_bits[7:5]};		
						32'hC0000400 : IO_Read_Data = {28'd0 , annexb_next_bits[7:4]};	
						32'hC0000500 : IO_Read_Data = {27'd0 , annexb_next_bits[7:3]};	
						32'hC0000600 : IO_Read_Data = {26'd0 , annexb_next_bits[7:2]};	
						32'hC0000700 : IO_Read_Data = {25'd0 , annexb_next_bits[7:1]};	
						32'hC0000800 : IO_Read_Data = {24'd0 , annexb_next_bits[7:0]};		
						32'hC0002000 : IO_Read_Data = cabac_out_fifo_dout;
						
					endcase
				end
			end //else begin // `STATE_CONT_BUSY
			`STATE_CONT_BUSY : begin
				case (IO_Address) 
					32'hC0000100, 32'hC0000200, 32'hC0000300, 32'hC0000400, 32'hC0000500, 32'hC0000600, 32'hC0000700, 32'hC0000800 : begin
						if (annexb_out_valid) begin
							annexb_read_bits = IO_Address[11:8];
							annexb_read_valid = 1'b1;
							IO_Ready = 1'b1;
						end else begin
							IO_Ready = 1'b0;
						end
					end
					32'hC0001300, 32'hC0001310, 32'hC0001320, 32'hC0001330: begin
						cabac_op = IO_Address[5:4];
						if (cabac_op_done) begin
							IO_Ready = 1'b1;
						end
					end
					32'hC0001500 : begin // Byte Alignment
						if (annexb_out_valid) begin
							annexb_read_bits = 4'd8-annexb_pos[2:0];
							annexb_read_valid = 1'b1;
							IO_Ready = 1'b1;
						end
					end
					32'hC0001510 : begin
						if (annexb_out_valid) begin
							if (annexb_pos[2:0] != 3'd0) begin
								annexb_read_bits = 4'd8-annexb_pos[2:0];
								annexb_read_valid = 1'b1;
								IO_Ready = 1'b1;
							end else begin
								annexb_read_valid = 1'b0;
								IO_Ready = 1'b1;
							end
						end
					end
					32'hC0002000: begin
						IO_Ready = ~cabac_out_fifo_empty;
					end
					32'hC0004000 : begin // push to transform
						IO_Ready = ~push_full;
					end
				
				endcase
				case (IO_Address)
					32'hC0000100 : IO_Read_Data = {31'd0 , annexb_next_bits[7]};
					32'hC0000200 : IO_Read_Data = {30'd0 , annexb_next_bits[7:6]};	
					32'hC0000300 : IO_Read_Data = {29'd0 , annexb_next_bits[7:5]};		
					32'hC0000400 : IO_Read_Data = {28'd0 , annexb_next_bits[7:4]};	
					32'hC0000500 : IO_Read_Data = {27'd0 , annexb_next_bits[7:3]};	
					32'hC0000600 : IO_Read_Data = {26'd0 , annexb_next_bits[7:2]};	
					32'hC0000700 : IO_Read_Data = {25'd0 , annexb_next_bits[7:1]};	
					32'hC0000800 : IO_Read_Data = {24'd0 , annexb_next_bits[7:0]};
					32'hC0002000 : IO_Read_Data = cabac_out_fifo_dout;
					default : IO_Read_Data = 32'dx;
				endcase
			end
			`STATE_CONT_CU : begin
				if (cu_done) begin
					IO_Read_Data = {15'd0 ,end_of_slice_segment_flag, ctbAddrInTS} ;
					IO_Ready = 1'b1;
				end
				annexb_read_bits = offload_read_bits;
				annexb_read_valid = offload_read_valid;
				cabac_op = offload_cabac_op;
				cabac_op_valid = offload_cabac_op_valid;
			end
		endcase 
	end
	
endmodule
