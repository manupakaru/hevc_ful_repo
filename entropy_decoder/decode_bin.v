`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:29:32 10/29/2013 
// Design Name: 
// Module Name:    decode_bin 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module decode_bin(
		clk,
		rst,
		next_bits,
		next_valid,
		read_bits,
		read_valid,
		
		init_engine,
		init_done,
		
		stall,
		
		read_ctx_data, 
		write_ctx_data,
		read_ctx_addr, // (255- bypass) // delay by 1 and pass
		bypass,
		terminate,
		write_ctx_addr,
		write_wea,
		bin
    );
	 
`define STATE_DB_N 2'b00
`define STATE_DB_9 2'b01 //9 bits required
`define STATE_DB_1 2'b10

	input clk;
	input rst;
	input [7:0] next_bits;
	input next_valid;
	output reg [3:0]read_bits;
	output reg read_valid;
	
	input init_engine;
	output init_done;
	
	input stall;
	
	input [6:0]read_ctx_data;
	output reg [6:0] write_ctx_data;
	
	input [7:0] read_ctx_addr;
	output reg [7:0] write_ctx_addr;
	input bypass, terminate;
	output reg write_wea;
	output reg bin;
	
	reg [8:0] offset, range; 
	reg [8:0] nextOffset, nextRange, renorm_range, renorm_offset;
	
	reg [1:0] state;
	wire [7:0] codIRangeLPS;
	wire [5:0] transIdxLPS, transIdxMPS;
	
	reg [8:0] newRange;
	reg [9:0] newOffset;
	
	reg [3:0] renorm_bits;
	reg renorm_valid;
	
		
	wire signed [9:0] OmR = offset - range;
	wire signed [9:0] OmR_LPS = OmR + codIRangeLPS;

	assign init_done = (state == `STATE_DB_N);
	
	always @(posedge clk) begin
		if (~rst) begin
			state <= `STATE_DB_N;
		end else begin
			if (!init_engine) begin
				case (state)
					`STATE_DB_9: begin
						if( next_valid) begin
							state <= `STATE_DB_1;
							offset <= { next_bits, 1'b0};
							range <= 9'd510;
						end else begin
							state <= `STATE_DB_9;
						end
					end
					`STATE_DB_1: begin
						if( next_valid) begin
							state <= `STATE_DB_N;
							offset <= { offset[8:1], next_bits[7]};
							range <= 9'd510;
						end else begin
							state <= `STATE_DB_1;
						end
					end
					default: begin
						state <= `STATE_DB_N;
						if ( ~stall) begin
							range <= renorm_range;
							offset <= renorm_offset;
						end
					end
				endcase
			end else begin // if (init_engine)
				state <= `STATE_DB_9;
			end
		end	
	end
	
	rangeTabLPS rangeTabLPS(
		.pStateIdx(read_ctx_data[5:0]),
		.qCodIRangeIdx(range[7:6]),
		.codIRangeLPS(codIRangeLPS)
	);
	
	transIdx transIdx(
		.pStateIdx(read_ctx_data[5:0]),
		.transIdxLPS(transIdxLPS),
		.transIdxMPS(transIdxMPS)
	);


	always @ (*) begin
		//newRange = terminate ? range -9'd2 : range - codIRangeLPS;
		newRange = range-codIRangeLPS;
		//newRange = range-1'b1;
		newOffset = {offset[8:0], next_bits[7]};
		if (~bypass) begin
			//if ( ~terminate) begin
				if (offset >= newRange) begin
					bin = ~read_ctx_data[6];
					nextOffset = offset - newRange;
					nextRange = codIRangeLPS;
					write_ctx_data[5:0] = transIdxLPS;
					write_ctx_data[6] = read_ctx_data[5:0]==6'd0 ? ~read_ctx_data[6] : read_ctx_data[6];
				end else begin
					bin = read_ctx_data[6];
					nextOffset = offset;
					nextRange = newRange;
					write_ctx_data[5:0] = transIdxMPS;
					write_ctx_data[6] = read_ctx_data[6];
				end

//			end else begin
//				if (offset >= newRange) begin
//					bin = 1'b1;
//				end else begin
//					bin = 1'b0;		
//				end
//				nextOffset = offset;
//				nextRange = newRange;
//				write_ctx_data = 7'dx;
//			end

			//(* parallel_case *)
			casex (nextRange)
				9'b1xxxxxxxx : begin
					renorm_bits = 4'd0;
					renorm_range = nextRange;
					renorm_offset = nextOffset;
				end
				9'b01xxxxxxx : begin
					renorm_bits = 4'd1;
					renorm_range = {nextRange[7:0], 1'b0};
					renorm_offset = {nextOffset[7:0], next_bits[7]};
				end
				9'b001xxxxxx : begin
					renorm_bits = 4'd2;
					renorm_range = {nextRange[6:0], 2'd0};
					renorm_offset = {nextOffset[6:0], next_bits[7:6]};
				end
				9'b0001xxxxx : begin
					renorm_bits = 4'd3;
					renorm_range = {nextRange[5:0], 3'd0};
					renorm_offset = {nextOffset[5:0], next_bits[7:5]};
				end
				9'b00001xxxx : begin
					renorm_bits = 4'd4;
					renorm_range = {nextRange[4:0], 4'd0};
					renorm_offset = {nextOffset[4:0], next_bits[7:4]};
				end
				9'b000001xxx : begin
					renorm_bits = 4'd5;
					renorm_range = {nextRange[3:0], 5'd0};
					renorm_offset = {nextOffset[3:0], next_bits[7:3]};
				end
				9'b0000001xx : begin
					renorm_bits = 4'd6;
					renorm_range = {nextRange[2:0], 6'd0};
					renorm_offset = {nextOffset[2:0], next_bits[7:2]};
				end
				9'b00000001x : begin
					renorm_bits = 4'd7;
					renorm_range = {nextRange[1:0], 7'd0};
					renorm_offset = {nextOffset[1:0], next_bits[7:1]};
				end
				9'b000000001 : begin
					renorm_bits = 4'd8;
					renorm_range = {nextRange[0], 8'd0};
					renorm_offset = {nextOffset[0], next_bits[7:0]};
				end
				default : begin
					renorm_bits = 4'd0;
					renorm_range = nextRange;
					renorm_offset = nextOffset;
				end
			endcase
			//renorm_bits = 4'b0;
		end else begin
			if (newOffset >= range) begin
				bin  = 1'b1;
				nextOffset = (newOffset - range) % 512;
				nextRange = range;
				write_ctx_data = 7'dx;
			end else begin
				bin = 1'b0;
				nextOffset = newOffset % 512;
				nextRange = range;	
				write_ctx_data = 7'dx;				
			end
			renorm_bits = 4'b1;
			renorm_range = nextRange;
			renorm_offset = nextOffset;
		end
		
		write_ctx_addr = read_ctx_addr;
		write_wea = (stall | bypass | terminate ) ? 1'b0 : 1'b1;
	end
	
	
	// input side
	always @ (*) begin
		if (~rst) begin
			read_bits = 4'bxxxx;
			read_valid = 3'b0;	
		end else begin
			case (state)
				`STATE_DB_9: begin
					read_bits = 4'd8;
					read_valid = next_valid;
				end
				`STATE_DB_1: begin
					read_bits = 4'd1;
					read_valid = next_valid;
				end
				default: begin
					// dependent on renorm requirement
					read_bits = renorm_bits;
					read_valid = ~stall & ~ (terminate & (bin==1'b1));
				end
			endcase
		end
	end
	

	
endmodule
