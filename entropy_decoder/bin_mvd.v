`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:26:56 11/13/2013 
// Design Name: 
// Module Name:    bin_mvd 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module bin_mvd(
    input clk,
	 input rst,
    input [5:0] bin_no,
    input binVal,
    input stall,
    output reg [31:0] decoded_se,
    output reg decoded_valid
    );

	`define STATE_MVD_ONES 1'b0
	`define STATE_MVD_EG1 1'b1
	
	reg state;
	reg [3:0] egk_ones ,i;
	reg [15:0] egk_suf, ret;

	always @(posedge clk) begin
		if (rst) begin
			state <= `STATE_MVD_ONES;
			ret <= 16'd0;
			i <= 4'd0;
		end else begin
			case (state)
				`STATE_MVD_ONES: begin
					if (~stall) begin
						state <= (binVal == 1'b1) ? `STATE_MVD_ONES : `STATE_MVD_EG1;
						i <= 4'd0;
						egk_suf <= 16'd0;
						egk_ones <= (binVal == 1'b1) ? bin_no[3:0] +1'b1 : bin_no[3:0];
						ret <= (binVal == 1'b1) ? {ret[14:0], 1'b1} : ret;
					end
				end
				`STATE_MVD_EG1 : begin
					if (~stall) begin
						if ( i < egk_ones) begin
							state <= `STATE_MVD_EG1;
							egk_suf <= {egk_suf[14:0], binVal};
							i <= i+4'd1;
						end else begin 
							state <= `STATE_MVD_ONES;
							i <= 4'd0;
						end
					end
				end
			endcase
		end
	end

	always @(*) begin
		case (state)
			`STATE_MVD_ONES: begin
				decoded_se = 32'dx;
				decoded_valid = 1'b0;
			end
			`STATE_MVD_EG1 : begin
				if (~stall) begin
					if ( i < egk_ones) begin
						decoded_se = 32'dx;
						decoded_valid = 1'b0;
					end else begin
						decoded_se =  {ret[15:0],1'b0} + {egk_suf[15:0], binVal};
						decoded_valid = 1'b1;
					end
				end else begin
					decoded_se = 32'dx;
					decoded_valid = 1'b0;
				end
			end
		endcase
	end
endmodule
