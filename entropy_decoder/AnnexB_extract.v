`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:29:31 10/16/2013 
// Design Name: 
// Module Name:    AnnexB_extract 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


module AnnexB_extract(
		clk,
		rst,
		read_next,
		read_in,
		read_empty,
		next_bits,
		out_valid, 
		read_bits,
		read_valid,
		skip_to_NAL,
		pos,
        B_test
    );
`define STATE_ANNEXB_N 3'b000
`define STATE_ANNEXB_S0 3'b001
`define STATE_ANNEXB_S1 3'b010
`define STATE_ANNEXB_S2 3'b011
`define STATE_ANNEXB_S3 3'b100
`define STATE_ANNEXB_R  3'b101 // just after reset


	input clk, rst;
	output read_next;
	input [31:0] read_in;
	input read_empty;

	output reg [7:0] next_bits;
	output reg out_valid;
	input [3:0] read_bits; // should be between 0-8
	input read_valid;
	input skip_to_NAL;
	
	output reg [4:0] pos; //[4:3] byte, [2:0] bit
	reg [7:0] B0, B1, B2, B3;
	
	wire skip_byte;
	reg [4:0] next_pos;
	reg [3:0] read_pos;
	
	reg [3:0] state,next_state;
	
	reg [3:0] read_bits_D1;
	reg read_valid_D1;
	
	reg [1:0] bytes_after_skip;
	
	always @(posedge clk) begin
		read_bits_D1 <= read_bits;
		read_valid_D1 <= read_valid;
	end
	
	assign skip_byte = (B1==8'h00 & B2==8'h00 & B3==8'h03) & (bytes_after_skip==2'b00);
	assign read_next = (pos[4:3] == 2'b11 & next_pos[4:3] == 2'b00) ? 1'b1: 1'b0;
	
	always @(posedge clk) begin
		if (~rst) begin // this should always be accompanied by a skip to nal
			state <= `STATE_ANNEXB_R;
			pos <= 5'b00000;
			{B0,B1,B2,B3} <= {32'hFFFFFFFF};
			bytes_after_skip <= 2'b00;
		end else begin
			state <= next_state;
			pos <= next_pos;
			if (pos[4:3] != next_pos[4:3]) begin
				if (skip_byte) begin
					bytes_after_skip <= 2'b10;
				end else begin
					bytes_after_skip <= (bytes_after_skip ==2'b00) ? 2'b00 : bytes_after_skip - 1'b1;
				end
				if (~skip_byte) {B0, B1, B2} <= {B1, B2, B3};
				case (pos[4:3])
					2'b00: B3 <= read_in[31:24];
					2'b01: B3 <= read_in[23:16];
					2'b10: B3 <= read_in[15: 8];
					2'b11: B3 <= read_in[ 7: 0];
				endcase
			end
		end
	
	end
	
	always @ (*) begin
		read_pos = 4'dx;
		if (read_empty) begin
			next_state = state; 
			out_valid = 1'b0;
			next_pos = pos;
		end else begin
			
			case (state) 
				`STATE_ANNEXB_R: begin //simulate skip_to_NAL
					if (skip_to_NAL) begin
						next_state = `STATE_ANNEXB_S0; out_valid = 1'b0; next_pos = { (pos[4:3]+2'b01), 3'b000};
					end else begin
						next_state = `STATE_ANNEXB_R; out_valid = 1'b0; next_pos = pos;
					end
				end
				`STATE_ANNEXB_N: begin
					if (skip_to_NAL) begin
						next_state = `STATE_ANNEXB_S0; out_valid = 1'b0; next_pos = { ( (pos[2:0]==3'b000) ? pos[4:3] : (pos[4:3]+2'b01)), 3'b000};
					end else begin
						next_state = `STATE_ANNEXB_N; out_valid = ~skip_byte;
						if (skip_byte) begin
							if (read_valid_D1) begin
								next_pos = pos + read_bits_D1+5'b01000;
								read_pos = pos[2:0] + read_bits_D1;
							end else begin
								next_pos = pos+5'b01000;
								read_pos = pos[2:0];
							end
							//next_pos <= pos + 5'b01000;
							//read_pos <= pos[2:0] + 5'b01000;
						end else begin
							if (read_valid_D1) begin
								next_pos = pos + read_bits_D1;
								read_pos = pos[2:0] + read_bits_D1;
							end else begin
								next_pos = pos;
								read_pos = pos[2:0];
							end
						end
					end
				end
				`STATE_ANNEXB_S0: begin
					if ( {B0,B1,B2} == 24'h000001 ) begin
						next_state = `STATE_ANNEXB_S2; out_valid = 1'b0; next_pos = pos + 5'd8;
					end else if ( {B0,B1,B2,B3} == 32'h00000001 ) begin
						next_state = `STATE_ANNEXB_S3; out_valid = 1'b0; next_pos = pos + 5'd8;
					end else begin
						next_state = `STATE_ANNEXB_S0; out_valid = 1'b0; next_pos = pos + 5'd8;
					end
				end
				
				`STATE_ANNEXB_S3 :begin
					next_state = `STATE_ANNEXB_S2; out_valid = 1'b0; next_pos = pos + 5'd8;
				end
				`STATE_ANNEXB_S2 :begin
					next_state = `STATE_ANNEXB_S1; out_valid = 1'b0; next_pos = pos + 5'd8;
				end
				`STATE_ANNEXB_S1 :begin
					if (skip_byte) next_state = `STATE_ANNEXB_S1; //special case when 000003 occurs immediately after NAL delimeter
					else next_state = `STATE_ANNEXB_N;
					out_valid = 1'b0; next_pos = pos + 5'd8;
				end
				default: begin // should not be here
					next_state = `STATE_ANNEXB_S0; out_valid = 1'b0; next_pos = pos + 5'd8;
				end
			endcase
		end
	
	end
	
	
//	always @ (*) begin
//		case (pos[2:0]) 
//			0: next_bits = {B0[7:0]};
//			1: next_bits = {B0[6:0], B1[7]};
//			2: next_bits = {B0[5:0], B1[7:6]};
//			3: next_bits = {B0[4:0], B1[7:5]};
//			4: next_bits = {B0[3:0], B1[7:4]};
//			5: next_bits = {B0[2:0], B1[7:3]};
//			6: next_bits = {B0[1:0], B1[7:2]};
//			7: next_bits = {B0[0], B1[7:1]};
//		endcase
//	end
	
	always @ (*) begin
		case (read_pos[3:0]) 
			0: next_bits = {B0[7:0]};
			1: next_bits = {B0[6:0], B1[7]};
			2: next_bits = {B0[5:0], B1[7:6]};
			3: next_bits = {B0[4:0], B1[7:5]};
			4: next_bits = {B0[3:0], B1[7:4]};
			5: next_bits = {B0[2:0], B1[7:3]};
			6: next_bits = {B0[1:0], B1[7:2]};
			7: next_bits = {B0[0], B1[7:1]};
			8: next_bits = {B1[7:0]};
			9: next_bits = {B1[6:0], B2[7]};
			10: next_bits = {B1[5:0], B2[7:6]};
			11: next_bits = {B1[4:0], B2[7:5]};
			12: next_bits = {B1[3:0], B2[7:4]};
			13: next_bits = {B1[2:0], B2[7:3]};
			14: next_bits = {B1[1:0], B2[7:2]};
			15: next_bits = {B1[0], B2[7:1]};
			//16: next_bits = {B2[7:0]};
		endcase
	end
	
	output wire [31:0] B_test;
	assign B_test = {B0, B1, B2, B3};
endmodule
