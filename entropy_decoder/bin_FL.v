`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:38:15 11/11/2013 
// Design Name: 
// Module Name:    bin_FL 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module bin_FL(
	 input clk,
    input [5:0] bin_no,
    input [5:0] cMax,
    input binVal,
    input stall,
    output reg [31:0] decoded_se,
    output reg decoded_valid
    );
	
	reg [4:1] current_bits; // [0] optimized out
	
	always @ (posedge clk) begin
		if (~stall) begin
			if (bin_no == (cMax-6'd1) ) begin
				current_bits <= 5'd0;
			end else begin
				case (bin_no)
					0: current_bits[4] <= binVal;
					1: current_bits[3] <= binVal;
					2: current_bits[2] <= binVal;
					3: current_bits[1] <= binVal;
					//default: current_bits[0] <= binVal;
				endcase
				
			end
		end
	end
	
	always @(*) begin
		if (~stall) begin
			if (bin_no == (cMax-6'd1)) begin
				case (cMax) 
					1: decoded_se = {31'd0, binVal};
					2: decoded_se = {30'd0, current_bits[4], binVal};
					3: decoded_se = {29'd0, current_bits[4:3], binVal};
					4: decoded_se = {28'd0, current_bits[4:2], binVal};
					5: decoded_se = {27'd0, current_bits[4:1], binVal};
					default : decoded_se = 32'dx; // should not occur
				endcase
				decoded_valid = 1'b1;
			end else begin
				decoded_valid = 1'b0;
				decoded_se = 32'dx;
			end
		
		end else begin
			decoded_valid = 1'b0;
			decoded_se = 32'dx;
		end
	
	end

endmodule
