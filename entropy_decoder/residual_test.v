`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:58:44 11/27/2013
// Design Name:   residual
// Module Name:   C:/Users/Maleen/Uni/HEVC/EntropyDecoder/residual_test.v
// Project Name:  EntropyDecoder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: residual
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module residual_test;

	// Inputs
	reg clk;
	reg rst;
	reg [8:0] mmap_data;
	reg mmap_valid;
	reg [31:0] cabac_out_data;
	reg cabac_out_valid;
	reg out_fifo_full;

	// Outputs
	wire [31:0] cabac_in_data;
	wire cabac_in_valid;
	wire [31:0] out_fifo_data;
	wire out_fifo_valid;
	wire cabac_out_read;

	reg [31:0] read_in, read_in_r;
	
	reg [31:0] read_res_only;

	integer fd, fdr, fd_res_only;
	integer line, liner;
	
	integer x0, y0;
	
	reg matched, out_matched;

	// Instantiate the Unit Under Test (UUT)
	residual uut (
		.clk(clk), 
		.rst(rst), 
		.mmap_data(mmap_data), 
		.mmap_valid(mmap_valid), 
		.cabac_in_data(cabac_in_data), 
		.cabac_in_valid(cabac_in_valid), 
		.cabac_out_data(cabac_out_data), 
		.cabac_out_valid(cabac_out_valid), 
		.cabac_out_read(cabac_out_read),
		.out_fifo_data(out_fifo_data), 
		.out_fifo_valid(out_fifo_valid), 
		.out_fifo_full(out_fifo_full)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0;
		mmap_data = 0;
		mmap_valid = 0;
		cabac_out_data = 0;
		cabac_out_valid = 0;
		out_fifo_full = 0;
		line = -4; liner = -4;
		fd = $fopen("BQSquare_416x240_60_qp37_cabac_residual_in.bin","rb"); 
		fdr = $fopen("BQSquare_416x240_60_qp37_cabac_residual_out.bin","rb"); 
		fd_res_only = $fopen("BQSquare_416x240_60_qp37_residual_only.bin","rb"); 
		read_in[ 7: 0] = $fgetc(fd);
		read_in[15: 8] = $fgetc(fd);
		read_in[23:16] = $fgetc(fd);
		read_in[31:24] = $fgetc(fd);
		matched = 1;
		out_matched = 1;
		
		line = line+4;
		if (read_in[31:24] == 254) begin
			x0 = read_in[23:12]; y0 = read_in[11:0];
			read_in[ 7: 0] = $fgetc(fd);
			read_in[15: 8] = $fgetc(fd);
			read_in[23:16] = $fgetc(fd);
			read_in[31:24] = $fgetc(fd);	
			line = line+4;
		end 
		if (read_in[31:24] == 255) begin
			mmap_data = read_in[8:0]; mmap_valid = 1;
			read_in[ 7: 0] = $fgetc(fd);
			read_in[15: 8] = $fgetc(fd);
			read_in[23:16] = $fgetc(fd);
			read_in[31:24] = $fgetc(fd);
			line = line+4;
		end
		cabac_out_data = read_in; cabac_out_valid = 1;
		
		read_in_r[ 7: 0] = $fgetc(fdr);
		read_in_r[15: 8] = $fgetc(fdr);
		read_in_r[23:16] = $fgetc(fdr);
		read_in_r[31:24] = $fgetc(fdr);
		liner = liner+4;
		
		read_res_only [23:16] = $fgetc(fd_res_only);
		read_res_only [31:24] = $fgetc(fd_res_only);
		read_res_only [7:0] = $fgetc(fd_res_only);
		read_res_only [15:8] = $fgetc(fd_res_only);

		///read_in_r[31:24] = $fgetc(fdr);
		//read_in_r[23:16] = $fgetc(fdr);
		//read_in_r[15: 8] = $fgetc(fdr);
		///read_in_r[ 7: 0] = $fgetc(fdr);	
		//liner = liner+4;
		
		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		rst = 1;
		
	end
	
	always begin
		#5 clk = ~clk;
	end
   always @ (posedge clk) begin
		if (cabac_out_read) begin
			read_in[ 7: 0] = $fgetc(fd);
			read_in[15: 8] = $fgetc(fd);
			read_in[23:16] = $fgetc(fd);
			read_in[31:24] = $fgetc(fd);	
			line = line+4;
			
			if (read_in[31:24] == 254) begin
				x0 = read_in[23:12]; y0 = read_in[11:0];
				read_in[ 7: 0] = $fgetc(fd);
				read_in[15: 8] = $fgetc(fd);
				read_in[23:16] = $fgetc(fd);
				read_in[31:24] = $fgetc(fd);	
				line = line+4;
			end 
			
			
			if (read_in[31:24] == 255) begin
				mmap_data = read_in[8:0]; mmap_valid = 1;
				read_in[ 7: 0] = $fgetc(fd);
				read_in[15: 8] = $fgetc(fd);
				read_in[23:16] = $fgetc(fd);
				read_in[31:24] = $fgetc(fd);	
				line = line+4;
			end else 
				mmap_valid = 0;
			cabac_out_data = read_in; cabac_out_valid = 1;
		end
		
		if (cabac_in_valid) begin
			if (read_in_r == cabac_in_data) begin
				matched = 1;
			end else begin 
				matched = 0;
			end
			read_in_r[ 7: 0] = $fgetc(fdr);
			read_in_r[15: 8] = $fgetc(fdr);
			read_in_r[23:16] = $fgetc(fdr);
			read_in_r[31:24] = $fgetc(fdr);
			liner = liner+4;
		end
		
		if (out_fifo_valid) begin
			
			if (read_res_only == out_fifo_data) begin
				out_matched = 1;
			end else begin
				out_matched = 0;
			end
			read_res_only [23:16] = $fgetc(fd_res_only);
			read_res_only [31:24] = $fgetc(fd_res_only);
			read_res_only [7:0] = $fgetc(fd_res_only);
			read_res_only [15:8] = $fgetc(fd_res_only);
			
		end
	end
      
endmodule

