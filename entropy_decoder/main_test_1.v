`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:36:08 07/30/2013
// Design Name:   main
// Module Name:   C:/Users/Maleen/FPGA/EntropyDecoder/main_test_1.v
// Project Name:  EntropyDecoder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: main
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module main_test_1;

	// Inputs
	reg [4:0] se_type;
	reg [26:0] se_param;
	reg [1:0] op;
	reg [1:0] cabac_init_type;
	reg [5:0] QP;
	reg clk;

	// Outputs
	wire read_next;
	wire [31:0] decoded_SE;
	wire decoded_valid;

	// Instantiate the Unit Under Test (UUT)
	main uut (
		.se_type(se_type), 
		.se_param(se_param), 
		.read_next(read_next), 
		.decoded_SE(decoded_SE), 
		.decoded_valid(decoded_valid), 
		.op(op), 
		.cabac_init_type(cabac_init_type), 
		.QP(QP), 
		.clk(clk)
	);
	initial begin
		// Initialize Inputs
		se_type = 0;
		se_param = 0;
		op = 0;
		QP = 37;
		clk = 1;
		cabac_init_type= 2'b00;

		// Wait 100 ns for global reset to finish
		#100;
      op = 2'b00; # 5;
		// Add stimulus here
		op = 2'b01; #100 ;
		op = 2'b10; #10;
		op = 2'b11; # 10; $finish;

	end
	 
	always begin
		#5 clk = ~clk;
	end
		      
endmodule

