`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:06:00 08/16/2013 
// Design Name: 
// Module Name:    ctx_sel 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ctx_sel(
    input [5:0] se_type,
    input [25:0] se_param,
	 input se_empty,
	 input clk,
	 input rst,
    output read_next, 
    input take_next_se, 
    output reg [31:10] se_type_D1,
    output reg [5:0] bin_no_D1,
    output reg [7:0] ctx_addr,
	 output reg bypass,
	 output terminate,
	 
	 input bits_valid, // stall if this is low
	 output reg stall
    );
		
	reg [5:0] bin_no;
	wire [5:0] max_bins;
	reg [5:0] max_bins_D1;
	//reg [5:0] bin_no_D2, max_bins_D2;
	
	wire [2:0] log2TrafoSize;
	wire [1:0] cIdx;

	reg [5:0] ctxInc;
	
	max_bins_module max_bins_d(
		.se_type(se_type),
		.max_bins(max_bins)
	);
		// do not consider take_next_se if bin_no_D1 = max_bins_D1
	assign read_next = (take_next_se & bin_no_D1<max_bins_D1) | (bin_no == max_bins);
	//assign read_next = (take_next_se & bin_no_D2<max_bins_D2) | (bin_no == max_bins);
	
	assign log2TrafoSize = se_param[25:23];
	assign cIdx = se_param[22:21];
	
// for last_significant_coeff_x_prefix
	reg [3:0] ctxOffset;
	reg [1:0] ctxShift;
	always @(*) begin
		if (cIdx==2'b00) begin
			case (log2TrafoSize)
				2: ctxOffset = 4'd0;
				3: ctxOffset = 4'd3;
				4: ctxOffset = 4'd6;
				5: ctxOffset = 4'd10;
				default: ctxOffset = 4'dx;
			endcase
			ctxShift = log2TrafoSize >3'd2;
		end else begin
			ctxOffset = 4'd15;
			ctxShift = (log2TrafoSize - 3'd2) % 4;
		end
		
	end
	
	// significant_coeff_flag
	reg [1:0] sig_ctx_1;
	reg [4:0] sig_ctx_add;
	reg [4:0] sig_ctx;
	wire [4:0] xC, yC;
	wire [1:0] xP, yP;
	wire [2:0] xP_yP;
	assign xC = se_param[18:14]; assign yC = se_param[13:9];
	assign xP = xC[1:0]; assign yP = yC[1:0];
	assign xP_yP = xP + yP;
	always @ (*) begin
		case (se_param[20:19]) //prevCsbf 
			0: sig_ctx_1 = (xP+yP==0) ? 2'd2 : (xP_yP < 3'd3)? 2'd1 : 2'd0;
			1: sig_ctx_1 = (yP==0) ? 2'd2: (yP==1) ? 2'd1 : 2'd0;
			2: sig_ctx_1 = (xP==0) ? 2'd2: (xP==1) ? 2'd1 : 2'd0;
			3: sig_ctx_1 = 2'd2;
		endcase
	end
	always @(*) begin
		if (cIdx == 2'b0) begin
			if (xC[4:2] == 3'd0 & yC[4:2] == 3'd0) begin
				sig_ctx_add = (log2TrafoSize ==3'd3) ? ( (se_param[8:7]==0) ? 5'd9 : 5'd15 ) : 5'd21;
			end else begin
				sig_ctx_add = (log2TrafoSize ==3'd3) ? ( (se_param[8:7]==0) ? 5'd12 : 5'd18 ) : 5'd24;
			end
		end else begin
			sig_ctx_add = (log2TrafoSize == 3'd3) ? 5'd9 : 5'd12;
		end
	end
	always @(*) begin
		if (log2TrafoSize == 3'd2) begin
			case ( {yC[1:0] , xC[1:0] }  )
				0: sig_ctx = 5'd0;
				1: sig_ctx = 5'd1;
				2: sig_ctx = 5'd4;
				3: sig_ctx = 5'd5;
				4: sig_ctx = 5'd2;
				5: sig_ctx = 5'd3;
				6: sig_ctx = 5'd4;
				7: sig_ctx = 5'd5;
				8: sig_ctx = 5'd6;
				9: sig_ctx = 5'd6;
				10: sig_ctx = 5'd8;
				11: sig_ctx = 5'd8;
				12: sig_ctx = 5'd7;
				13: sig_ctx = 5'd7;
				14: sig_ctx = 5'd8;
				default: sig_ctx = 5'dx;
			endcase
		end else if (xC ==0 & yC==0) begin
			sig_ctx = 5'd0;
		end else begin
			sig_ctx = sig_ctx_1 + sig_ctx_add;
		end
	end
	
	//coeff_abs_level_greater_1/2
	wire [3:0] greater1Ctx;
	wire [1:0] ctxSet;
	wire [1:0] min_greater1Ctx;
	
	assign greater1Ctx = se_param[20:17];
	assign ctxSet = se_param[16:15];
	assign min_greater1Ctx = greater1Ctx > 4'd3 ? 2'd3 : greater1Ctx[1:0];
	
	//ctxInc 
	always @(*) begin
		case (se_type)
			36,37: ctxInc = (bin_no >> ctxShift) + ctxOffset;
			40 : ctxInc = (cIdx >0 ) ? {5'b00001 , (se_param[20] | se_param[19])} : {5'b00000 , (se_param[20] | se_param[19])};
			41 : ctxInc = (cIdx > 0) ? se_param[6:2] + 5'd27 : se_param[6:2];
			42 : ctxInc = (cIdx > 0) ? {2'b01, ctxSet, min_greater1Ctx} : {2'b00, ctxSet, min_greater1Ctx};
			43 : ctxInc = (cIdx > 0) ? {4'b0001 , ctxSet} :  {4'b0000 , ctxSet} ;
			default: ctxInc = 6'dx;
		endcase
	
	end
	
	always @ (posedge clk) begin
		if (~rst) begin
			se_type_D1 <= 22'dx;
			stall <= 1'b1;
			bin_no <= 6'd0;
		end else begin
			se_type_D1 <= {se_type , se_param[25:10]};
			stall <= (take_next_se & bin_no_D1<max_bins_D1) | se_empty | ~bits_valid ; // (take_next_se & bin_no==0) // if take_next_se was asserted at max_bin
			//stall <= (take_next_se & bin_no_D2<max_bins_D2) | se_empty | ~bits_valid ; // (take_next_se & bin_no==0) // if take_next_se was asserted at max_bin
			// take_next_se_D1 & bin_no_D1 <max_bins_D1 ??
			if (~bits_valid) 
				bin_no <= bin_no;
			else
				bin_no <= ((read_next & bin_no_D1<max_bins_D1)|se_empty | bin_no == max_bins) ? 6'd0 : bin_no +6'd1; //se_empty needed for initializing
				//bin_no <= ((read_next & bin_no_D2<max_bins_D2)|se_empty | bin_no == max_bins) ? 6'd0 : bin_no +6'd1; //se_empty needed for initializing
		end
		bin_no_D1 <= bin_no;
		//bin_no_D2 <= bin_no_D1;
		max_bins_D1 <= max_bins;
		//max_bins_D2 <= max_bins_D1;
		
	end

	assign terminate = (se_type==6'd7 || se_type==6'd8 || se_type==6'd16) & !se_empty;
	
	always @(*) begin
		case (se_type) 
			1: begin // sao_merge_left/up
				ctx_addr = 8'd0;
				bypass = 1'b0;
			end
			2: begin //sao_type_idx
				ctx_addr = (bin_no ==6'd0) ? 8'd1 : 8'dx;
				bypass = (bin_no==6'd1);
			end
			3,4,5: begin //sao_band_pos , sao_offset_abs, sao_offset_sign
				ctx_addr = 8'dx;
				bypass = 1'b1;
			end
			6: begin // sao_eo_class
				ctx_addr = 8'dx;
				bypass = 1'b1;
			end
			7,8: begin // end of slice_segment, end_of_substream_one_bit
				ctx_addr = 8'dx;
				bypass = 1'b0;
			end
			9: begin //split_cu_flag
				ctx_addr = 8'd2 + (se_param[5] & se_param[4]) + (se_param[3] & se_param[2]);
				bypass = 1'b0;
			end
			11: begin //cu_skip_flag
				ctx_addr = 8'd6 + (se_param[5] & se_param[4]) + (se_param[3] & se_param[2]);
				bypass = 1'b0;
			end
			12: begin // cu_qp_delta_abs
				case (bin_no)
					0: ctx_addr = 8'd9;
					1,2,3,4: ctx_addr = 8'd10;
					default: ctx_addr = 8'dx;
				endcase
				bypass = (bin_no > 4);
			end
			13: begin // cu_qp_delta_sign
				ctx_addr = 8'dx;
				bypass = 1'b1;
			end
			14: begin // pred_mode_flag
				ctx_addr = 8'd11;
				bypass = 1'b0;
			end
			15: begin  //part_mode
				case (bin_no) 
					0: ctx_addr = 8'd12;
					1: ctx_addr = 8'd13;
					2: ctx_addr = (se_param[25:23]==se_param[22:20]) ? 8'd14 : 8'd15;
					default: ctx_addr = 8'dx;
				endcase
				bypass = (bin_no==6'd3);
			end
			17: begin // prev_intra_luma
				ctx_addr = 8'd16;
				bypass = 1'b0;
			end
			18,19: begin //mpm_idx , rem_intra_luma
				ctx_addr = 8'dx;
				bypass = 1'b1;
			end
			20: begin //intra_chroma_pred_mode
				ctx_addr = (bin_no==6'd0) ? 8'd17 : 8'dx;
				bypass = (bin_no==6'd0) ? 1'b0 : 1'b1;
			end
			21: begin // merge_flag
				ctx_addr = 8'd18;
				bypass = 1'b0;
			end
			22: begin //merge_idx
				ctx_addr = (bin_no ==0) ? 8'd19 : 8'dx ;
				bypass = (bin_no ==0) ? 1'b0 : 1'b1;
			end
			23: begin // inter_pred_idc
				if (bin_no==0)
					ctx_addr = 8'd20 + ((se_param[25:18] + se_param[17:10] != 12) ? se_param[9:7] : 8'd4);
				else 
					ctx_addr = 8'd24;
				bypass = 1'b0;
			end
			24,25: begin //ref_idx
				ctx_addr = 8'd25+bin_no[0];
				bypass = (bin_no >1);
			end
			26: begin // abs_mvd_greater_0
				ctx_addr = 8'd27;
				bypass = 1'b0;
			end	
			27: begin // abs_mvd_greater_1
				ctx_addr = 8'd28;
				bypass = 1'b0;
			end	
			28: begin // abs_mvd_greater_1
				ctx_addr = 8'dx;
				bypass = 1'b1;
			end				
			29: begin //mvd_sign_flag
				ctx_addr = 8'dx;
				bypass = 1'b1;
			end
			30: begin //mvp_lx_flag
				ctx_addr = 8'd29;
				bypass = 1'b0;
			end
			31: begin //rqt_root_cbf
				ctx_addr = 8'd30;
				bypass = 1'b0;
			end
			32 : begin //split_transform_flag
				case (se_param[4:2])
					0: ctx_addr = 8'd31;
					1: ctx_addr = 8'd32;
					2: ctx_addr = 8'd33;
					default: ctx_addr = 8'dx;
				endcase
				bypass = 1'b0;
			end
			33 : begin
				ctx_addr = (se_param[4:2] == 3'b0) ? 8'd35 : 8'd34;
				bypass = 1'b0;
			end
			34 : begin //cbf_cx
				case (se_param[4:2])
					0: ctx_addr = 8'd36;
					1: ctx_addr = 8'd37;
					2: ctx_addr = 8'd38;
					3: ctx_addr = 8'd39;
					default: ctx_addr = 8'dx;
				endcase
				bypass = 1'b0;
			end
			35 : begin // transform_skip
				if (se_param[3:2] == 2'b00)
					ctx_addr = 8'd40;
				else 
					ctx_addr = 8'd41;
				bypass = 1'b0;
			end
			36 : begin //last_significant_coef_x_prefix
				ctx_addr = 8'd42 + ctxInc;
				bypass = 1'b0;
			end
			37 : begin //last_significant_coef_y_prefix
				ctx_addr = 8'd60 + ctxInc;
				bypass = 1'b0;
			end
			38,39: begin //last_significant_coef_x_suffix
				ctx_addr = 8'dx;
				bypass = 1'b1;
			end
			40 : begin //coded_sub_block_flag
				ctx_addr = 8'd78 + ctxInc;
				bypass = 1'b0;
			end
			41 : begin //significant_coeff_flag
				ctx_addr = 8'd82 + ctxInc;
				bypass = 1'b0;
			end
			42 : begin //coeff_abs_level_greater1_flag
				ctx_addr = 8'd124 + ctxInc;
				bypass = 1'b0;
			end
			43 : begin //coeff_abs_level_greater2_flag
				ctx_addr = 8'd148 + ctxInc;
				bypass = 1'b0;
			end
			44, 45: begin //coeff_abs_level_remaining, coeff_sign_flag
				ctx_addr = 8'dx;
				bypass = 1'b1;
			end
			default : begin
				ctx_addr = 6'dx;
				bypass = 1'b0;
			end
		endcase
		
	end
	
	
endmodule
