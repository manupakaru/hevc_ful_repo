`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:38:43 10/27/2013
// Design Name:   AnnexB_extract
// Module Name:   C:/Users/Maleen/Uni/HEVC/EntropyDecoder/AnnexB_test.v
// Project Name:  EntropyDecoder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: AnnexB_extract
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module AnnexB_test;

	// Inputs
	reg clk;
	reg rst;
	reg [31:0] read_in;
	reg read_empty;
	reg [3:0] read_bits;
	reg read_valid;
	reg skip_to_NAL;

	// Outputs
	wire read_next;
	wire [7:0] next_bits;
	wire out_valid;

	integer   fd, fdw;
   integer   line;

	
	// Instantiate the Unit Under Test (UUT)
	AnnexB_extract uut (
		.clk(clk), 
		.rst(rst), 
		.read_next(read_next), 
		.read_in(read_in), 
		.read_empty(read_empty), 
		.next_bits(next_bits), 
		.out_valid(out_valid), 
		.read_bits(read_bits), 
		.read_valid(read_valid), 
		.skip_to_NAL(skip_to_NAL)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0; #10 rst=1;
		read_in = 32'h11223344;
		read_empty = 1;
		read_bits = 0;
		read_valid = 0;
		skip_to_NAL = 0;
		
		fd = $fopen("BQSquare_416x240_60_qp37.bin","rb"); 
		line = -4;

		// Wait 100 ns for global reset to finish
		#90;
        
		// Add stimulus here
		read_empty = 0; 
		skip_to_NAL = 1; #10; skip_to_NAL = 0;
		
		#150
		read_bits = 1; read_valid = 1;
		#10
		read_bits = 4; read_valid = 1;	
		#10
		read_bits = 7; read_valid = 1;	
		#10
		read_bits = 2; read_valid = 1;	
		#10
		read_bits = 5; read_valid = 1;	
		#150;
		skip_to_NAL = 1; #10;
		skip_to_NAL = 0;	
	
	end
   always 
		#5 clk = ~clk;
		
	always @ (posedge clk) begin
		if (read_next) begin // should only change at cycle edge
			read_in[31:24] = $fgetc(fd);
			read_in[23:16] = $fgetc(fd);
			read_in[15: 8] = $fgetc(fd);
			read_in[ 7: 0] = $fgetc(fd);
			
			//$display("data  = %h", data);
			
			line = line +4;
			if (line > 180)  begin
				//$fclose(fdw);
				//$finish;
			end
		end

	end
endmodule

