`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:49:45 12/08/2013 
// Design Name: 
// Module Name:    cu_offload 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module cu_offload(
		clk,
		rst,
		
		mmap_data,
		mmap_type,
		mmap_valid,
		
		cabac_in_data,
		cabac_in_valid,
		
		cabac_out_data,
		cabac_out_valid,
		cabac_out_read,
		
		ctbAddrInTS,
		end_of_slice_segment_flag,
		
		out_fifo_data,
		out_fifo_valid,
		out_fifo_full,
		in_fifo_almost_empty,
		
		annexb_next_bits,
		annexb_out_valid,
		annexb_read_bits,
		annexb_read_valid,
		annexb_offload_override,
		annexb_pos,
		
		cabac_op,
		cabac_op_valid,
		cabac_op_done,
		
		done,
		
		cu_state,
		ru_state,
		cu_x0,
		cu_y0,
		cu_other
    );
	 `include "def_cu.v"
	 // I/O
	 input clk, rst;
	 
	 input [15:0] mmap_data; 
	 input [7:0] mmap_type; 
	 input mmap_valid;
	 
	 output reg [31:0] cabac_in_data;
	 output reg cabac_in_valid;
	 
	 input [31:0] cabac_out_data;
	 input cabac_out_valid;
	 output reg cabac_out_read;
	
	 output reg [15:0] ctbAddrInTS ;
	 output reg end_of_slice_segment_flag;
	 
	 output reg [31:0] out_fifo_data;
	 output reg out_fifo_valid;
	 input out_fifo_full;
	 input in_fifo_almost_empty;

	 input [7:0] annexb_next_bits;
	 input  annexb_out_valid;
	 output reg [3:0] annexb_read_bits;
	 output reg annexb_read_valid;
	 output reg annexb_offload_override;
 	 input [4:0] annexb_pos;
	 
	 output reg [1:0] cabac_op;
	 output reg cabac_op_valid;
	 input cabac_op_done;
 
	 output reg done; 
	 
	 output [6:0] cu_state, ru_state;
	 output [11:0] cu_x0, cu_y0;
	 output [255:0] cu_other;
	 
	 
	 
	 // internal
	 reg [6:0] state;
	 reg [6:0] pu_ret_state;
	 reg [11:0] xCtb, yCtb;
	 reg [15:0] ctbAddrInRS, SliceAddrRS, ctbAddrInTS_begin, ctbAddrInTile;
	 reg [7:0] ctbAddrInTileRow; // tile no. within the current tile row
	 reg same_slice_tile_left;
	 reg same_slice_tile_up;
	 
	 // parameters received from host
	 reg [13:0] pic_width_in_luma_samples, pic_height_in_luma_samples;
	 reg [5:0] SliceQPY; 
	 reg transquant_bypass_enable_flag;
	 reg sign_data_hiding_flag ;
	 reg [1:0] slice_type;
	 reg transform_skip_enabled_flag;
	 reg [2:0] MaxNumMergeCand;
	 reg [3:0] num_ref_idx_l0_active_minus1, num_ref_idx_l1_active_minus1;
	 reg mvd_l1_zero_flag;
	 reg [2:0] max_transform_hierarchy_depth_intra;
	 reg [2:0] max_transform_hierarchy_depth_inter;
	 reg amp_enabled_flag;
	 reg slice_sao_luma_flag;
	 reg slice_sao_chroma_flag;
	 reg cu_qp_delta_enabled_flag;
	 reg entropy_coding_sync_enabled_flag;
	 reg [7:0] curr_slice;
	 reg [7:0] curr_tile;
	 reg [7:0] tile_width;
	 reg [15:0] TileSizeCTU;
	 reg [13:0] tile_x;
	 reg [2:0] Log2CtbSizeY;
	 reg [2:0] Log2MinCbSizeY;
	 reg [2:0] Log2MaxTrafoSize;
	 reg [2:0] Log2MinTrafoSize;
	 reg pcm_enabled_flag;
	 reg [3:0] PCMBitDepthY, PCMBitDepthC;
	 reg [2:0] Log2MinIpcmCbSizeY, Log2MaxIpcmCbSizeY;

		// TODO
	 
	 
	 
	 // parameters derived
	 reg [7:0] PicWidthInCtbsY;
	 
	 // internal variables
		// cqt
	 reg split_cu_flag;
		// coding unit
	 reg cu_transquant_bypass_flag;
	 reg cu_skip_flag;
	 reg pcm_flag;
	 reg merge_flag;
	 reg rqt_root_cbf;
	 reg [2:0] MaxTrafoDepth;
		// transform
	 reg split_transform_flag ;// only need to maintain for current trafoDepth
	 reg transform_skip_flag;
	 reg res_present;
	 reg cbf_cb_pushed, cbf_cr_pushed;
	 reg [2:0] ru_trafoSize;
	 reg [1:0] ru_cIdx;
	 reg [11:0] ru_x0, ru_y0;
	 
	 reg [1:0] CuPredMode_cur;
	 reg [2:0] PartMode;
	 reg prev_intra_luma_pred_flag [0:3] ;
	 reg [1:0] mpm_idx [0:3];
	 reg [4:0] rem_intra_luma_pred_mode [0:3];
	 
	 //reg end_of_slice_segment_flag;
	 
	 // line buffers
	 reg CuPredMode_buf [0:15]; // 1 if Inter
	 reg CuPredMode_buf_left [0:15];
	 
	 reg [5:0] IntraPredModeY_buf [0:15];
	 reg [5:0] IntraPredModeY_buf_left [0:15];
	 
	 reg [1:0] CtDepth_buf [0:`LINE_BUF_CU];
	 reg [1:0] CtDepth_buf_left[0:7];
	 
	 reg cu_skip_flag_buf [0:`LINE_BUF_CU];
	 reg cu_skip_flag_buf_left[0:7];
	 
	 reg [5:0] qPY_left[0:7];
	 reg [5:0] qPY_top [0:7];
	 
	 // cu_qp_delta
	 reg [5:0] qPY;
	 reg IsCuQpDeltaCoded;
	 reg [2:0] Log2MinCuQpDeltaSize;
	 reg [6:0] cu_qp_delta;
	 
	 
	 // division
	 reg [2:0] div_state;
	 reg [11:0] div_d1, div_d2, div_quot, div_rem;
	 reg div_valid, div_done;
	 
	 // cu level input
	 reg [11:0] x0,y0;
	 reg [2:0] log2CbSize;
	 
	 // Intra Modes
	 reg [5:0] IntraPredModeY0;
	 reg [5:0] IntraPredModeY1;
	 reg [5:0] IntraPredModeY2;
	 reg [5:0] IntraPredModeY3;
	 reg [5:0] IntraPredModeC;
	 reg IntraSplitFlag;
	 reg [5:0] candIntraPredModeA, candIntraPredModeB, mode;
	 reg [5:0] candModeList [0:2];
	 reg availableLeft, availableTop; //combinationsl
	
	 reg i,j;
	
	 reg [11:0] xB, yB;
	 reg [3:0] size;
	 reg [3:0] x;
	 reg [1:0] rem_i;
	 reg [2:0] tu_i; // used in qpy line buffer update at tu level
	 
	 reg [4:0] pcm_x, pcm_y;
	 
	 // Stack
	 reg [11:0] T_x0 [0:3];
	 reg [11:0] T_y0 [0:3];
	 reg [11:0] T_xB [0:3];
	 reg [11:0] T_yB [0:3];
	 reg [2:0] T_trafoSize [0:3];
	 reg [1:0] T_blkIdx [0:3];
	 reg [6:0] T_ret_state [0:3];
	 reg [1:0] trafoDepth;
	 reg cbf_cb_0[0:3];
	 reg cbf_cr_0[0:3];
	 reg cbf_cb_B[0:3];
	 reg cbf_cr_B[0:3];
	 reg cbf_luma_0[0:3];
	 reg [11:0] cqt_x0 [0:3];
	 reg [11:0] cqt_y0 [0:3];
	 reg [6:0] cqt_ret[0:3];
	 reg [1:0] cqtDepth;
	 reg [2:0] cqt_i, cqt_i_size;
	 reg [11:0] cqt_x1, cqt_y1; // wire
	 reg [6:0] cqt_cbSize; // wire
	 reg [2:0] cqt_log2CbSize; //wire
	 
	 reg availableL, availableA, condL, condA;
	 
	 reg [6:0] push_h40_return_state;
	 
	 // test
	 assign cu_other = {130'd0, cqt_x0[0], cqt_y0[0], cqt_ret[0], cqt_x0[1], cqt_y0[1], cqt_ret[1], cqt_x0[2], cqt_y0[2], cqt_ret[2], cqt_x0[3], cqt_y0[3], cqt_ret[3], cqtDepth } ;
	 
	 assign cu_state = state;
	 assign cu_x0 = x0;
	 assign cu_y0 = y0;
	 
	 always @(*) begin
		case (Log2CtbSizeY)
			6: availableLeft = (xB[5:0] == 0) ? ( (xB==0) ? 1'b0 : same_slice_tile_left ) : 1'b1;
			5: availableLeft = (xB[4:0] == 0) ? ( (xB==0) ? 1'b0 : same_slice_tile_left ) : 1'b1;
			4: availableLeft = (xB[3:0] == 0) ? ( (xB==0) ? 1'b0 : same_slice_tile_left ) : 1'b1;
			default : availableLeft = 1'dx;
		endcase
		case (Log2CtbSizeY)
			6: availableTop	= (yB[5:0] ==0) ? 1'b0 : 1'b1;
			5: availableTop	= (yB[4:0] ==0) ? 1'b0 : 1'b1;
			4: availableTop	= (yB[3:0] ==0) ? 1'b0 : 1'b1;
			default : availableTop = 1'dx;
		endcase
	 end
	 //assign availableLeft = (xB[5:0] == 0) ? ( (xB==0) ? 1'b0 : same_slice_tile_left ) : 1'b1;
	 //assign availableTop = (yB[5:0] == 0) ? ( (yB==0) ? 1'b0 : same_slice_tile_up ) : 1'b1;
	 //assign availableTop	= (yB[5:0] ==0) ? 1'b0 : 1'b1;
	 reg [11:0] x1, y1;
	 always @ (*) begin
		case (T_trafoSize[trafoDepth])
			3: x1 = T_x0[trafoDepth] + 12'd4;
			4: x1 = T_x0[trafoDepth] + 12'd8;
			5: x1 = T_x0[trafoDepth] + 12'd16;
			6: x1 = T_x0[trafoDepth] + 12'd32;
			default x1 = 12'dx;
		endcase
		case (T_trafoSize[trafoDepth])
			3: y1 = T_y0[trafoDepth] + 12'd4;
			4: y1 = T_y0[trafoDepth] + 12'd8;
			5: y1 = T_y0[trafoDepth] + 12'd16;
			6: y1 = T_y0[trafoDepth] + 12'd32;
			default y1 = 12'dx;
		endcase
	 end
	 // cqt_
	 always @(*) begin
		case (cqtDepth + 6 - Log2CtbSizeY)
			0: begin
				cqt_cbSize = 7'd64;
				cqt_log2CbSize = 3'd6;
				cqt_i_size = 3'd7;
				cqt_x1 = cqt_x0[0] + 6'd32;
				cqt_y1 = cqt_y0[0] + 6'd32;
			end
			1: begin
				cqt_cbSize = 7'd32;
				cqt_log2CbSize = 3'd5;
				cqt_i_size = 3'd3;
				if (Log2CtbSizeY == 3'd5) begin
					cqt_x1 = cqt_x0[0] + 6'd16;
					cqt_y1 = cqt_y0[0] + 6'd16;
				end else begin
					cqt_x1 = cqt_x0[1] + 6'd16;
					cqt_y1 = cqt_y0[1] + 6'd16;
				end
			end
			2: begin
				cqt_cbSize = 7'd16;
				cqt_log2CbSize = 3'd4;
				cqt_i_size = 3'd1;
				if (Log2CtbSizeY == 3'd6) begin
					cqt_x1 = cqt_x0[2] + 6'd8;
					cqt_y1 = cqt_y0[2] + 6'd8;
				end else if (Log2CtbSizeY == 3'd5) begin
					cqt_x1 = cqt_x0[1] + 6'd8;
					cqt_y1 = cqt_y0[1] + 6'd8;
				end else if (Log2CtbSizeY == 3'd4) begin
					cqt_x1 = cqt_x0[0] + 6'd8;
					cqt_y1 = cqt_y0[0] + 6'd8;
				end else begin
				    cqt_x1 = 12'dx; // shouldn't be needed
                    cqt_y1 = 12'dx;
				end
			end
			3: begin
				cqt_cbSize = 7'd8;
				cqt_log2CbSize = 3'd3;
				cqt_i_size = 3'd0;
				cqt_x1 = 12'dx; // shouldn't be needed
				cqt_y1 = 12'dx;
			end
			default: begin
				cqt_cbSize = 7'dx;
				cqt_log2CbSize = 3'dx;
				cqt_i_size = 3'dx;
				cqt_x1 = 12'dx; // shouldn't be needed
				cqt_y1 = 12'dx;
			end
		endcase
	 end
	 
	reg [5:0] predModeIntra;
	
	reg [1:0] residual_quad;
	always @ (*) begin // predModeIntra
		if ( ru_cIdx == 2'b00) begin
			case (Log2MinCbSizeY)
				6: residual_quad =  {ru_y0[5], ru_x0[5] };
				5: residual_quad =  {ru_y0[4], ru_x0[4] };
				4: residual_quad =  {ru_y0[3], ru_x0[3] };
				3: residual_quad =  {ru_y0[2], ru_x0[2] };
				default : residual_quad = 2'bxx;
			endcase
			case ( residual_quad )
				2'b00 : predModeIntra = IntraPredModeY0;
				2'b01 : predModeIntra = IntraPredModeY1;
				2'b10 : predModeIntra = IntraPredModeY2;
				2'b11 : predModeIntra = IntraPredModeY3;
			endcase
		end else begin
			predModeIntra = IntraPredModeC;
		end
	end
	
	reg new_QG; // if current (x0,y0) is start of new quantization group
	always @(*) begin
		case (Log2MinCuQpDeltaSize)
			3: new_QG = ( x0[2:0] == 0 && y0[2:0] ==0);
			4: new_QG = ( x0[3:0] == 0 && y0[3:0] ==0);
			5: new_QG = ( x0[4:0] == 0 && y0[4:0] ==0);
			6: new_QG = ( x0[5:0] == 0 && y0[5:0] ==0);
			default : new_QG = 1'bx;
		endcase
	end
	
	reg [2:0] qp_x0, qp_y0; // index into the qpY_left and qpY_top
	reg [4:0] ru_x0_CTU, ru_y0_CTU; // used in normal h40 packet
	reg [4:0] x0_CTU, y0_CTU; // used in rqt_false h40 packets
	always @(*) begin
		case (Log2CtbSizeY)
			4: begin
				qp_x0 = {2'b00, x0[3]}; qp_y0 = {2'b00, y0[3]};
				ru_x0_CTU = {2'b00, ru_x0[3:1]};
				ru_y0_CTU = {2'b00, ru_y0[3:1]};
				y0_CTU = {2'b00, y0[3:1]};
				x0_CTU = {2'b00, x0[3:1]};
			end
			5: begin
				qp_x0 = {1'b0, x0[4:3]}; qp_y0 = {1'b0, y0[4:3]};
				ru_x0_CTU = {1'b0, ru_x0[4:1]};
				ru_y0_CTU = {1'b0, ru_y0[4:1]};
				x0_CTU = {1'b0, x0[4:1]};
				y0_CTU = {1'b0, y0[4:1]};
			end
			6: begin
				qp_x0 = {x0[5:3]}; qp_y0 = {y0[5:3]};
				ru_x0_CTU = ru_x0[5:1];
				ru_y0_CTU = ru_y0[5:1];
				x0_CTU = x0[5:1];
				y0_CTU = y0[5:1];
			end
			default: begin
				qp_x0 = 3'dx; qp_y0 = 3'dx;
				ru_x0_CTU = 5'dx;
				ru_y0_CTU = 5'dx;
				x0_CTU = 5'dx;
				y0_CTU = 5'dx;
			end
				
		endcase
	end
	
	reg [7:0] pcm_sample;
	reg [3:0] PCMBitDepth;
	always @(*) begin
		PCMBitDepth = (ru_cIdx==2'b00 ? PCMBitDepthY : PCMBitDepthC);
		case (PCMBitDepth) 
			1 : pcm_sample = {annexb_next_bits[7], 7'd0};
			2 : pcm_sample = {annexb_next_bits[7:6], 6'd0};
			3 : pcm_sample = {annexb_next_bits[7:5], 5'd0};
			4 : pcm_sample = {annexb_next_bits[7:4], 4'd0};
			5 : pcm_sample = {annexb_next_bits[7:3], 3'd0};
			6 : pcm_sample = {annexb_next_bits[7:2], 2'd0};
			7 : pcm_sample = {annexb_next_bits[7:1], 1'd0};
			8 : pcm_sample = {annexb_next_bits[7:0] };
			default : pcm_sample = 8'dx;
		endcase
		
	end
	
	reg res_mmap_valid;
	reg [1:0] ru_scanIdx;
	wire res_done;
	
	wire [31:0] res_cabac_in_data, res_out_fifo_data;
	wire res_cabac_in_valid, res_out_fifo_valid;
	wire res_cabac_out_read;
	
	residual_coding residual(
		.clk(clk),
		.rst(rst),
		
		.mmap_data(9'd0),// [8],h_30[27], [6:0], 7-transquantbypass in h_30[27]
		.mmap_valid(res_mmap_valid),
		
		.cabac_in_data(res_cabac_in_data),
		.cabac_in_valid(res_cabac_in_valid),
		
		.cabac_out_data(cabac_out_data),
		.cabac_out_valid(cabac_out_valid),
		.cabac_out_read(res_cabac_out_read),
	
		.out_fifo_data(res_out_fifo_data),
		.out_fifo_valid(res_out_fifo_valid),
		.out_fifo_full(out_fifo_full),
		.in_fifo_almost_empty(in_fifo_almost_empty),
		
		.done(res_done),
		
		.cIdx(ru_cIdx),
		.trafoSize(ru_trafoSize),
		.scanIdx(ru_scanIdx),
		.sign_data_hiding_flag(sign_data_hiding_flag),
		.cu_transquant_bypass_flag(cu_transquant_bypass_flag),
		.state(ru_state[4:0])
	);
	
	assign ru_state[5] = 1'b0;
	reg pu_valid;
	reg [1:0] pu_part_idx;
	reg [6:0] pu_nPbW, pu_nPbH;
	
	wire [31:0] pu_push_data;
	wire pu_push_valid;
	wire pu_done;
	wire pu_merge_flag;
	
	wire [31:0] pu_cabac_in_data;
	wire pu_cabac_in_valid;
	wire pu_cabac_out_read;
	
	prediction_unit pu(
     .clk(clk),
     .rst(rst),
     .valid(pu_valid),
	  .part_idx(pu_part_idx),
	  .cu_skip_flag(cu_skip_flag),
	  .nPbW(pu_nPbW),
	  .nPbH(pu_nPbH),
	  .CqtDepth(cqtDepth),
     .MaxNumMergeCand(MaxNumMergeCand),
     .slice_type(slice_type),
     .num_ref_idx_l0_active_minus_1(num_ref_idx_l0_active_minus1),
     .num_ref_idx_l1_active_minus_1(num_ref_idx_l1_active_minus1),
     .mvd_l1_zero_flag(mvd_l1_zero_flag),
     .push_full(out_fifo_full),
     .push_out(pu_push_data),
     .push_valid(pu_push_valid),
     .pu_done(pu_done),
	  .merge_flag(pu_merge_flag),
	 
	  .cabac_in_data(pu_cabac_in_data),
	  .cabac_in_valid(pu_cabac_in_valid),
	 
	  .cabac_out_valid(cabac_out_valid),
	  .cabac_out_data(cabac_out_data),
	  .cabac_out_read(pu_cabac_out_read)
    );
	 
	 reg sao_valid;
	 
	 wire [31:0] sao_push_data;
	 wire sao_push_valid;
	 wire sao_done;
	
	 wire [31:0] sao_cabac_in_data;
	 wire sao_cabac_in_valid;
	 wire sao_cabac_out_read;
	 
	 reg [7:0] sao_rx, sao_ry;
	 always @ (*) begin
		case (Log2CtbSizeY) 
			6: begin
				sao_rx = {2'b00 ,xCtb[11:6]};
				sao_ry = {2'b00 ,yCtb[11:6]};
			end
			5: begin
				sao_rx = {1'b0 ,xCtb[11:5]};
				sao_ry = {1'b0 ,yCtb[11:5]};
			end
			4: begin
				sao_rx = xCtb[11:4];
				sao_ry = yCtb[11:4];
			end
			default: begin
				sao_rx = 8'dx;
				sao_ry = 8'dx;
			end
		endcase
	 end
	 
	sao sao(
    .clk(clk),
    .rst(rst),
	 
	 .valid(sao_valid),
	 .rx(sao_rx),
	 .ry(sao_ry),
	 .same_slice_tile_left(same_slice_tile_left),
	 .same_slice_tile_up(same_slice_tile_up),
	 
	 .slice_sao_luma_flag(slice_sao_luma_flag),
	 .slice_sao_chroma_flag(slice_sao_chroma_flag),
	 
	 .push_out(sao_push_data),
    .push_valid(sao_push_valid),
    .push_full(out_fifo_full),
	 
    .sao_done(sao_done),
	 
	 .cabac_in_data(sao_cabac_in_data),
	 .cabac_in_valid(sao_cabac_in_valid),
	 
	 .cabac_out_valid(cabac_out_valid),
	 .cabac_out_data(cabac_out_data),
	 .cabac_out_read(sao_cabac_out_read)
    );
	 
	always @(posedge clk) begin
		if (~rst) begin
			//Log2MinCbSizeY <= 3'd3;
			//Log2MaxTrafoSize <= 3'd5;
			//Log2MinTrafoSize <= 3'd2;
			//Log2CtbSizeY <= 3'd6;
			
			state <= `STATE_CU_INIT;
			//pcm_flag <= 1'b0;
		end else begin
			case (state) 
				`STATE_CU_INIT : begin //0
					if (mmap_valid) begin
						case (mmap_type)
							0: begin
								ctbAddrInTS <= mmap_data;
								ctbAddrInTS_begin <= mmap_data;
								end_of_slice_segment_flag <= 1'b0;
								
							end						
							1: begin
								pic_width_in_luma_samples <= mmap_data;

							end
							2: pic_height_in_luma_samples <= mmap_data;
							3: begin 
								SliceQPY <= mmap_data[5:0];
								qPY <= mmap_data[5:0];
							end
							4: transquant_bypass_enable_flag <= mmap_data[0];
							5: sign_data_hiding_flag <= mmap_data[0];
							6: slice_type <= mmap_data[1:0];
							7: transform_skip_enabled_flag <= mmap_data[0];
							8: amp_enabled_flag <= mmap_data[0];
							9: MaxNumMergeCand <= mmap_data[2:0];
							10: num_ref_idx_l0_active_minus1 <= mmap_data[3:0];
							11: num_ref_idx_l1_active_minus1 <= mmap_data[3:0];
							12: mvd_l1_zero_flag <= mmap_data[0];
							13: max_transform_hierarchy_depth_inter <= mmap_data[1:0];
							14: max_transform_hierarchy_depth_intra <= mmap_data[1:0];
							15: {slice_sao_luma_flag, slice_sao_chroma_flag} <= mmap_data[1:0];
							16: SliceAddrRS <= mmap_data;
							17: cu_qp_delta_enabled_flag <= mmap_data[0];
							18: Log2MinCuQpDeltaSize <= mmap_data[2:0];
							19: entropy_coding_sync_enabled_flag <= mmap_data[0];
							20: curr_slice <= mmap_data[7:0];
							21: curr_tile <= mmap_data[7:0];
							22: ctbAddrInRS <= mmap_data;
							23: begin 
								tile_width <= mmap_data[7:0];
								ctbAddrInTile <= 15'd0;
								ctbAddrInTileRow <= 8'd0;
								
							end
							24: TileSizeCTU <= mmap_data;
							26: begin
								Log2CtbSizeY <= mmap_data[2:0];
								case (mmap_data[2:0]) 
									6: begin
										if (pic_width_in_luma_samples[5:0] == 6'd0) 
											PicWidthInCtbsY <= pic_width_in_luma_samples[13:6];
										else 
											PicWidthInCtbsY <= pic_width_in_luma_samples[13:6] + 1'b1;
									end
									5: begin
										if (pic_width_in_luma_samples[4:0] == 5'd0) 
											PicWidthInCtbsY <= pic_width_in_luma_samples[13:5];
										else 
											PicWidthInCtbsY <= pic_width_in_luma_samples[13:5] + 1'b1;
									end
									4: begin
										if (pic_width_in_luma_samples[3:0] == 4'd0) 
											PicWidthInCtbsY <= pic_width_in_luma_samples[13:4];
										else 
											PicWidthInCtbsY <= pic_width_in_luma_samples[13:4] + 1'b1;
									end
								endcase 
							end
							27: Log2MinCbSizeY <= mmap_data[2:0];
							28: Log2MaxTrafoSize <= mmap_data[2:0];
							29: Log2MinTrafoSize <= mmap_data[2:0];
							30: pcm_enabled_flag <= mmap_data[0];
							31: PCMBitDepthY <= mmap_data[3:0];
							32: PCMBitDepthC <= mmap_data[3:0];
							33: Log2MinIpcmCbSizeY <= mmap_data[2:0];
							34: Log2MaxIpcmCbSizeY <= mmap_data[2:0];
							
							63: begin
								state <= `STATE_SSD_TS_TO_RS_CONV;
							end
						endcase
					end
				end
				`STATE_SSD_TS_TO_RS_CONV : begin
					if (ctbAddrInTileRow == tile_width ) begin
						ctbAddrInRS <= ctbAddrInRS + PicWidthInCtbsY - tile_width;
						ctbAddrInTileRow <= 8'd0;
					end
						
					state <= `STATE_SSD_BEGIN;
				end
				`STATE_SSD_BEGIN : begin
					
					//if ( ctbAddrInTS ==0) begin // or new tile
					//	state <= `STATE_SSD_SEND_H16;
					//end else begin
						state <= `STATE_SSD_CTU_BEGIN;
					//end
				end
				// moved to uBlaze
//				`STATE_SSD_SEND_H16 : begin
//					if (!out_fifo_full) begin
//						state <= `STATE_SSD_SEND_H17;
//					end
//				end
//				`STATE_SSD_SEND_H17 : begin
//					if (!out_fifo_full) begin
//						state <= `STATE_SSD_CTU_BEGIN;
//					end
//				end
				`STATE_SSD_CTU_BEGIN : begin
					div_d1 <= ctbAddrInRS;
					div_d2 <= PicWidthInCtbsY;
					div_valid <= 1'b1;
					state <= `STATE_SSD_CTU_XY_READ;
					//GPO <= 1000;
				end
				`STATE_SSD_CTU_XY_READ : begin 
					//div_valid <= 1'b0;
					if (div_done) begin
						div_valid <= 1'b0;
						case (Log2CtbSizeY) 
							6: begin
								xCtb <= {div_rem[5:0], 6'd0};
								yCtb <= {div_quot[5:0], 6'd0};
							end
							5: begin
								xCtb <= {div_rem[6:0], 5'd0};
								yCtb <= {div_quot[6:0], 5'd0};
							end
							4: begin
								xCtb <= {div_rem[7:0], 4'd0};
								yCtb <= {div_quot[7:0], 4'd0};
							end
						endcase
						if (entropy_coding_sync_enabled_flag && ( div_rem[5:0] == 2 || div_rem[5:0] ==0)) begin
							// change for other CTU sizes - should be ok
							// only go back to ublaze if its requested ctbAddr is different from current. Otherwise endless loop here
													
							if (ctbAddrInTS != ctbAddrInTS_begin) begin
								if (div_rem[5:0] == 2)
									state <= `STATE_SSD_SAVE_CABAC; 
								else // start of WPP Roe
									state <= `STATE_SSD_BYTE_ALIGNMENT_IF; 
							end else begin
								state <= `STATE_SSD_SEND_H20;
							end
						end else begin
							state <= `STATE_SSD_SEND_H20;
						end
					end
				end
				`STATE_SSD_BYTE_ALIGNMENT_IF: begin
					if (annexb_out_valid) begin
						qPY <= SliceQPY;
						state <= `STATE_SSD_LOAD_INIT_CABAC;
					end
				end
				`STATE_SSD_LOAD_INIT_CABAC: begin
					if (cabac_op_done) begin
						state <= `STATE_SSD_INIT_ENGINE;
					end
				end
				`STATE_SSD_INIT_ENGINE: begin
					state <= `STATE_SSD_INIT_ENGINE_DONE;
				end
				`STATE_SSD_INIT_ENGINE_DONE: begin
					if (cabac_op_done) begin
						state <= `STATE_SSD_SEND_H20;
					end
				end
				`STATE_SSD_SAVE_CABAC: begin
					if (cabac_op_done) begin
						state <= `STATE_SSD_SEND_H20;
					end
				end
				`STATE_SSD_SEND_H20 : begin
					if (!out_fifo_full) begin
						state <= `STATE_SSD_SEND_H21;
					end
					
					if (xCtb==0) begin
						same_slice_tile_left <= 1'b0;
					end else if (SliceAddrRS > (ctbAddrInRS-1)) begin
						same_slice_tile_left <= 1'b0;
					end else if (ctbAddrInTileRow ==0) begin
						same_slice_tile_left <= 1'b0;
					end else begin
						same_slice_tile_left <= 1'b1;
					end
					
					if (yCtb==0) begin
						same_slice_tile_up <= 1'b0;
					end else if (SliceAddrRS > (ctbAddrInRS-PicWidthInCtbsY)) begin
						same_slice_tile_up <= 1'b0;
					end else if (ctbAddrInTile < tile_width) begin
						same_slice_tile_up <= 1'b0;
					end else begin
						same_slice_tile_up <= 1'b1;
					end
					//same_slice_tile_left  <= (xCtb == 0) ? 1'b0 :  ((SliceAddrRS > (ctbAddrInRS-  				1))? 1'b0 : 1'b1);
					//same_slice_tile_up	 <= (yCtb == 0) ? 1'b0 :  ((SliceAddrRS > (ctbAddrInRS-PicWidthInCtbsY))? 1'b0 : 1'b1);
					
				end
				`STATE_SSD_SEND_H21 : begin
					if (!out_fifo_full) begin
						state <= `STATE_CTU_START_SAO;
					end
				end
				`STATE_CTU_START_SAO : begin
				// If sao_flag == 1 move there
					if (slice_sao_luma_flag || slice_sao_chroma_flag) begin
						state <= `STATE_CTU_SAO_WAIT;
					end else begin
						state <= `STATE_CTU_CALL_CQT;
					end
				end
				`STATE_CTU_SAO_WAIT: begin
					if (sao_done) begin
						state <= `STATE_CTU_CALL_CQT;
					end
				end
				`STATE_CTU_CALL_CQT : begin //90
					x0 <= xCtb;
					y0 <= yCtb;
					xB <= xCtb;
					yB <= yCtb;
					log2CbSize <= Log2CtbSizeY;
					
					//same_slice_tile_left <= mmap_data[1];
					//same_slice_tile_up <= mmap_data[0];
					state <= `STATE_CQT_AVAILABLEN;
					cqt_x0[0] <= xCtb;
					cqt_y0[0] <= yCtb;
					cqtDepth <= 2'd0;
					
					cqt_ret[0] <= `STATE_SSD_END_OF_SLICE_SEGMENT_FLAG_PUSH;
				end
				`STATE_SSD_END_OF_SLICE_SEGMENT_FLAG_PUSH : begin //91
					state <= `STATE_SSD_END_OF_SLICE_SEGMENT_FLAG_READ;
					ctbAddrInTS <= ctbAddrInTS + 1'b1;
					ctbAddrInRS <= ctbAddrInRS + 1'b1;
					ctbAddrInTileRow <= ctbAddrInTileRow + 1'b1;
					ctbAddrInTile <= ctbAddrInTile + 1'b1;
					
				end
				`STATE_SSD_END_OF_SLICE_SEGMENT_FLAG_READ : begin //92
					if (cabac_out_valid) begin
						end_of_slice_segment_flag <= cabac_out_data[0];
						if (cabac_out_data[0] == 1'b1) begin
							state <= `STATE_SSD_END;
						end else begin
							if (ctbAddrInTile == TileSizeCTU) begin
								state <= `STATE_SSD_END;
							end else begin
								state <= `STATE_SSD_TS_TO_RS_CONV;
							end
						end
					end
				end
				
	//-----------------------------------------------------------------------------------------			
				`STATE_CQT_AVAILABLEN : begin // 64
					if (  (cqt_x0[cqtDepth] + cqt_cbSize) <= pic_width_in_luma_samples  &
							(cqt_y0[cqtDepth] + cqt_cbSize) <= pic_height_in_luma_samples &
							cqt_log2CbSize > Log2MinCbSizeY) begin
							case (Log2CtbSizeY) 
								6: begin
									availableL <= ( cqt_x0[cqtDepth][5:0] == 6'd0) ?  ( (cqt_x0[cqtDepth] ==12'd0) ?  1'b0 : same_slice_tile_left ) : 1'b1; 
									availableA <= ( cqt_y0[cqtDepth][5:0] == 6'd0) ?  ( (cqt_y0[cqtDepth] ==12'd0) ?  1'b0 : same_slice_tile_up   ) : 1'b1; 
								end
								5: begin
									availableL <= ( cqt_x0[cqtDepth][4:0] == 5'd0) ?  ( (cqt_x0[cqtDepth] ==12'd0) ?  1'b0 : same_slice_tile_left ) : 1'b1; 
									availableA <= ( cqt_y0[cqtDepth][4:0] == 5'd0) ?  ( (cqt_y0[cqtDepth] ==12'd0) ?  1'b0 : same_slice_tile_up   ) : 1'b1; 
								end
								4: begin
									availableL <= ( cqt_x0[cqtDepth][3:0] == 4'd0) ?  ( (cqt_x0[cqtDepth] ==12'd0) ?  1'b0 : same_slice_tile_left ) : 1'b1; 
									availableA <= ( cqt_y0[cqtDepth][3:0] == 4'd0) ?  ( (cqt_y0[cqtDepth] ==12'd0) ?  1'b0 : same_slice_tile_up   ) : 1'b1; 
								end	
							endcase 
							condL <= CtDepth_buf_left[ cqt_y0[cqtDepth][5:3] ] > cqtDepth;
							condA <= CtDepth_buf[ cqt_x0[cqtDepth][11:3] ] > cqtDepth;
							state <= `STATE_CQT_SPLIT_PUSH;
					end else begin
						if (cqt_log2CbSize > Log2MinCbSizeY) begin
							split_cu_flag <= 1'b1;
						end else begin
							split_cu_flag <= 1'b0;
						end
						state <= `STATE_CQT_UPDATE_BUF;
					end
					cqt_i <= cqt_i_size;
				end
				`STATE_CQT_SPLIT_PUSH : begin //65
					if (!in_fifo_almost_empty) begin
						state <= `STATE_CQT_SPLIT_READ;
					end
				end
				`STATE_CQT_SPLIT_READ : begin //66
					if (cabac_out_valid) begin
						split_cu_flag <= cabac_out_data[0];
						state <= `STATE_CQT_UPDATE_BUF;
					end
					
				end
				`STATE_CQT_UPDATE_BUF : begin //67
					if (!split_cu_flag) begin
						if (cqt_i == 3'd0) begin
							state <= `STATE_CQT_QT_0;
						end 
						CtDepth_buf_left [ cqt_y0[cqtDepth][5:3]+cqt_i ] <= cqtDepth;
						CtDepth_buf[ cqt_x0[cqtDepth][11:3]  + cqt_i] <= cqtDepth;
						cqt_i <= cqt_i -1'b1;
					end else begin
						state <= `STATE_CQT_QT_0;
					end
					if (cu_qp_delta_enabled_flag && (cqt_log2CbSize >= Log2MinCuQpDeltaSize)) begin
						IsCuQpDeltaCoded = 1'b0;
					end
				end
				`STATE_CQT_QT_0 : begin //68
					if (split_cu_flag) begin
						state <= `STATE_CQT_AVAILABLEN;
						cqt_x0[cqtDepth + 1'b1] <= cqt_x0[cqtDepth];
						cqt_y0[cqtDepth + 1'b1] <= cqt_y0[cqtDepth];
						cqtDepth <= cqtDepth + 1'b1;
						cqt_ret[cqtDepth + 1'b1] <= `STATE_CQT_QT_1;
					end else begin
						x0 <= cqt_x0[cqtDepth];
						y0 <= cqt_y0[cqtDepth];
						xB <= cqt_x0[cqtDepth];
						yB <= cqt_y0[cqtDepth];
						log2CbSize <= cqt_log2CbSize;
						state <= `STATE_CU_TRANSQUANT_BYPASS_PUSH;
					end
				end
				`STATE_CQT_QT_1 : begin //69
					if (cqt_x1 < pic_width_in_luma_samples) begin
						state <= `STATE_CQT_AVAILABLEN;
						cqt_x0[cqtDepth + 1'b1] <= cqt_x1;
						cqt_y0[cqtDepth + 1'b1] <= cqt_y0[cqtDepth];
						cqtDepth <= cqtDepth + 1'b1;
						cqt_ret[cqtDepth + 1'b1] <= `STATE_CQT_QT_2;
					end else begin
						state <= `STATE_CQT_QT_2;
					end
				end
				`STATE_CQT_QT_2 : begin //70
					if (cqt_y1 < pic_height_in_luma_samples) begin
						state <= `STATE_CQT_AVAILABLEN;
						cqt_x0[cqtDepth + 1'b1] <= cqt_x0[cqtDepth];
						cqt_y0[cqtDepth + 1'b1] <= cqt_y1;
						cqtDepth <= cqtDepth + 1'b1;
						cqt_ret[cqtDepth + 1'b1] <= `STATE_CQT_QT_3;
					end else begin
						state <= `STATE_CQT_QT_3;
					end
				end
				`STATE_CQT_QT_3 : begin //71
					if ( (cqt_x1 < pic_width_in_luma_samples) & (cqt_y1 < pic_height_in_luma_samples)) begin
						state <= `STATE_CQT_AVAILABLEN;
						cqt_x0[cqtDepth + 1'b1] <= cqt_x1;
						cqt_y0[cqtDepth + 1'b1] <= cqt_y1;
						cqtDepth <= cqtDepth + 1'b1;
						cqt_ret[cqtDepth + 1'b1] <= `STATE_CQT_END;
					end else begin
						state <= `STATE_CQT_END;
					end
				end
				
				`STATE_CQT_END : begin //72
					state <= cqt_ret[cqtDepth];
					cqtDepth <= cqtDepth - 1'b1;
				end
				
				`STATE_CU_TRANSQUANT_BYPASS_PUSH : begin //95
					// qPy_pred could be done here, line buffer updates later
					if (!in_fifo_almost_empty) begin 
						if (new_QG) begin
							qPY <= ( 1 + (qp_x0==3'd0 ? qPY : qPY_left[qp_y0]) + (qp_y0==3'd0 ? qPY : qPY_top[qp_x0]) )>>1;
						end
						if (transquant_bypass_enable_flag) begin
							state <= `STATE_CU_TRANSQUANT_BYPASS_READ;
						end else begin
							state <= `STATE_CU_SKIP_PUSH;
							cu_transquant_bypass_flag <= 1'b0;
						end
						case (Log2CtbSizeY) 
							6 : begin
								availableL <= ( x0[5:0] == 6'd0) ?  ( (x0 ==12'd0) ?  1'b0 : same_slice_tile_left ) : 1'b1; 
								availableA <= ( y0[5:0] == 6'd0) ?  ( (y0 ==12'd0) ?  1'b0 : same_slice_tile_up   ) : 1'b1; 
							end
							5 : begin
								availableL <= ( x0[4:0] == 5'd0) ?  ( (x0 ==12'd0) ?  1'b0 : same_slice_tile_left ) : 1'b1; 
								availableA <= ( y0[4:0] == 5'd0) ?  ( (y0 ==12'd0) ?  1'b0 : same_slice_tile_up   ) : 1'b1; 
							end
							4 : begin
								availableL <= ( x0[3:0] == 4'd0) ?  ( (x0 ==12'd0) ?  1'b0 : same_slice_tile_left ) : 1'b1; 
								availableA <= ( y0[3:0] == 4'd0) ?  ( (y0 ==12'd0) ?  1'b0 : same_slice_tile_up   ) : 1'b1; 
							end
						endcase 
						condL <= cu_skip_flag_buf_left[ y0[5:3] ];
						condA <= cu_skip_flag_buf[ x0[11:3] ];
						IntraPredModeY0 <= 6'd35;
						IntraPredModeY1 <= 6'd35;
						IntraPredModeY2 <= 6'd35;
						IntraPredModeY3 <= 6'd35;
						IntraPredModeC <= 6'd35;
						IntraSplitFlag <= 1'b0;
						pcm_flag <= 1'b0;
					end
				end
				`STATE_CU_TRANSQUANT_BYPASS_READ : begin //96
					if (cabac_out_valid) begin
						cu_transquant_bypass_flag <= cabac_out_data[0];
						state <= `STATE_CU_SKIP_PUSH;
					end
				end
				`STATE_CU_SKIP_PUSH : begin //97
					if (slice_type != `SLICE_I) begin
						state <= `STATE_CU_SKIP_READ;
					end else begin
						cu_skip_flag <= 1'b0;
						state <= `STATE_CU_PREDMODE_PUSH;
					end
				end
				`STATE_CU_SKIP_READ: begin //98
					if (cabac_out_valid) begin
						cu_skip_flag <= cabac_out_data[0];
						if (cabac_out_data[0]) begin
							CuPredMode_cur <= 2'b01;
						end
						state <= `STATE_CU_SKIP_UPDATE;
					end
					case (log2CbSize) 
						3: cqt_i <= 3'd0;
						4: cqt_i <= 3'd1;
						5: cqt_i <= 3'd3;
						6: cqt_i <= 3'd7;
					endcase
				end
				`STATE_CU_SKIP_UPDATE : begin //99
					if (cqt_i == 3'd0) begin
						if (cu_skip_flag) 
							state <= `STATE_CU_SENDH30;
							 
						else
							state <= `STATE_CU_PREDMODE_PUSH;
					end 
					cu_skip_flag_buf_left[ y0[5:3]+cqt_i ] <= cu_skip_flag;
					cu_skip_flag_buf[ x0[11:3]  + cqt_i] <= cu_skip_flag;
					if (cu_skip_flag) begin
						CuPredMode_buf_left [ { (y0[5:3] + cqt_i),  1'b0} ] <= 1'b1;
						CuPredMode_buf_left [ { (y0[5:3] + cqt_i),  1'b1} ] <= 1'b1;
						CuPredMode_buf [ { (x0[5:3] + cqt_i),  1'b0} ] <= 1'b1;
						CuPredMode_buf [ { (x0[5:3] + cqt_i),  1'b1} ] <= 1'b1;
						qPY_left[qp_y0 + cqt_i] <= qPY;
						qPY_top [qp_x0 + cqt_i] <= qPY;
					end
					cqt_i <= cqt_i -1'b1;
				end
				
				`STATE_CU_PREDMODE_PUSH : begin //1
					if (slice_type == `SLICE_I) begin
						CuPredMode_cur <= `MODE_INTRA;
						state <= `STATE_CU_PARTMODE_PUSH;
						merge_flag <= 1'b0;
					end else begin
						state <= `STATE_CU_PREDMODE_READ;
					end
				end
				`STATE_CU_PREDMODE_READ : begin //2
					if (cabac_out_valid) begin
						// cabac_out_data , 1=INTRA, 0 = INTER
						CuPredMode_cur <= {1'b0, ~cabac_out_data[0]};
						if (cabac_out_data == 1'b1) begin // if INTRA
							state <= `STATE_CU_PARTMODE_PUSH;
						end else begin
							state <= `STATE_CU_PREDMODE_UPDATE;
							case (log2CbSize) 
								3: cqt_i <= 3'd0;
								4: cqt_i <= 3'd1;
								5: cqt_i <= 3'd3;
								6: cqt_i <= 3'd7;
							endcase
						end
					end
				end
				`STATE_CU_PREDMODE_UPDATE : begin // only enters here if inter , for intra buffer update is done later
					if (cqt_i == 3'd0) begin
						state <= `STATE_CU_PARTMODE_PUSH;
					end 
					CuPredMode_buf_left [ { (y0[5:3] + cqt_i),  1'b0} ] = 1'b1;
					CuPredMode_buf_left [ { (y0[5:3] + cqt_i),  1'b1} ] = 1'b1;
					CuPredMode_buf [ { (x0[5:3] + cqt_i),  1'b0} ] = 1'b1;
					CuPredMode_buf [ { (x0[5:3] + cqt_i),  1'b1} ] = 1'b1;
					qPY_left[ qp_y0 + cqt_i] <= qPY;
					qPY_top [ qp_x0 + cqt_i] <= qPY;
					cqt_i <= cqt_i -1'b1;
				end
				`STATE_CU_PARTMODE_PUSH : begin // 3
					if ( (CuPredMode_cur !=`MODE_INTRA) || (log2CbSize ==Log2MinCbSizeY)) begin
						state <= `STATE_CU_PARTMODE_READ;
					end else begin
						PartMode <= `PART_2Nx2N;
						IntraSplitFlag <= 1'b0;
						if ( CuPredMode_cur == `MODE_INTRA && pcm_enabled_flag && log2CbSize >= Log2MinIpcmCbSizeY && log2CbSize <= Log2MaxIpcmCbSizeY) begin
							state <= `STATE_CU_PCM_FLAG_PUSH;
						end else begin
							state <= `STATE_CU_SENDH30;
						end
					end
				end
				`STATE_CU_PARTMODE_READ : begin //4
					if (cabac_out_valid) begin
						if (CuPredMode_cur == `MODE_INTRA) begin
							IntraSplitFlag <= (cabac_out_data[2:0] == `PART_NxN );
						end
						PartMode <= cabac_out_data[2:0];
						
						if ( CuPredMode_cur == `MODE_INTRA && cabac_out_data[2:0]==`PART_2Nx2N && pcm_enabled_flag && log2CbSize >= Log2MinIpcmCbSizeY && log2CbSize <= Log2MaxIpcmCbSizeY) begin
							state <= `STATE_CU_PCM_FLAG_PUSH;
						end else begin
							state <= `STATE_CU_SENDH30;
						end
					end
				end
				`STATE_CU_PCM_FLAG_PUSH : begin //115
					state <= `STATE_CU_PCM_FLAG_READ;
				end
				`STATE_CU_PCM_FLAG_READ : begin //116
					if (cabac_out_valid) begin
						pcm_flag <= cabac_out_data[0];
						state <= `STATE_CU_SENDH30;
					end
				end
				`STATE_CU_SENDH30 : begin //5
					if (!out_fifo_full) begin	
						if (!pcm_flag) begin
							if (CuPredMode_cur == `MODE_INTRA) begin
								state <= `STATE_CU_PREV_PUSH;
							end else begin
								if (cu_skip_flag) begin
									pu_nPbW <= cqt_cbSize;
									pu_nPbH <= cqt_cbSize;
									pu_part_idx <= 2'b00;
									state <= `STATE_CU_PU_WAIT;
									pu_ret_state <= `STATE_CU_RQT_FALSE_H40Y;
								end else begin
									state <= `STATE_CU_PU_1; 
								end
								
							end
							{j,i} <= 2'b00;
						end else begin
							state <= `STATE_CU_PCM_BYTE_ALIGN;
						end
					end
				end
				`STATE_CU_PCM_BYTE_ALIGN : begin //117
					if (annexb_out_valid) begin
						ru_cIdx <= 2'b00;
						ru_x0 <= x0;
						ru_y0 <= y0;
						ru_trafoSize <= log2CbSize;
						res_present <= 1'b1;
						state <= `STATE_PUSH_H40;
						push_h40_return_state <= `STATE_CU_PCM_READ;
						pcm_x <= 5'd0;
						pcm_y <= 5'd0;
					end
				end
				`STATE_CU_PCM_READ : begin //118
					if (annexb_out_valid) begin
						if (!out_fifo_full) begin
							IntraPredModeY_buf[ x0[5:2] + pcm_x[4:2] ] <= 6'd1;
							IntraPredModeY_buf_left[ y0[5:2] + pcm_y[4:2] ] <= 6'd1;
							if (pcm_x == ( 1 << ru_trafoSize) - 1'b1) begin
								if (pcm_y == ( 1 << ru_trafoSize) - 1'b1) begin
									ru_trafoSize <= log2CbSize - 1'b1;
									if (ru_cIdx == 2'd2) begin
										state <= `STATE_CU_PCM_INIT_CABAC;
									end else begin
										ru_cIdx <= ru_cIdx + 1'b1;
										state <= `STATE_PUSH_H40;
										push_h40_return_state <= `STATE_CU_PCM_READ;
									end
									pcm_x <= 5'd0;
									pcm_y <= 5'd0;
								end else begin
									pcm_x <= 5'd0;
									pcm_y <= pcm_y+1'b1;
								end
							end else begin
								pcm_x <= pcm_x + 1'b1;
							end
						end
					end
				end
				`STATE_CU_PCM_INIT_CABAC : begin //119
					if (cabac_op_done) begin
						state <= `STATE_CU_END;
					end
				end
				
				`STATE_CU_PREV_PUSH : begin //6
					if (IntraSplitFlag && ( {j,i} != 2'b11 )) begin	
						{j,i} <= {j,i} + 1'b1;
					end else begin
						{j,i} <= 2'b00;
						state <= `STATE_CU_PREV_READ; 
					end
				end
				`STATE_CU_PREV_READ : begin //7
					if (cabac_out_valid) begin
						prev_intra_luma_pred_flag[ {j,i} ] <= cabac_out_data[0];
						if (IntraSplitFlag && ( {j,i} != 2'b11 )) begin	
							{j,i} <= {j,i} + 1'b1;
						end else begin
							{j,i} <= 2'b00;
							state <= `STATE_CU_MPM_PUSH; 
						end
					end
				end
				`STATE_CU_MPM_PUSH : begin //8
					if (IntraSplitFlag && ( {j,i} != 2'b11 )) begin	
						{j,i} <= {j,i} + 1'b1;
					end else begin
						{j,i} <= 2'b00;
						state <= `STATE_CU_MPM_READ; 
					end
				end
				`STATE_CU_MPM_READ : begin //9
					if (cabac_out_valid) begin
						if ( prev_intra_luma_pred_flag[ {j,i} ] ) begin
							mpm_idx[ {j,i} ] <= cabac_out_data[1:0];
						end else begin
							rem_intra_luma_pred_mode[ {j,i} ] <= cabac_out_data[4:0];
						end
						
						if (IntraSplitFlag && ( {j,i} != 2'b11 )) begin	
							{j,i} <= {j,i} + 1'b1;
						end else begin
							{j,i} <= 2'b00;
							state <= `STATE_CU_DERIVE_1; 
						end
					end
				end
				`STATE_CU_DERIVE_1 : begin //10
					if (!availableLeft || CuPredMode_buf_left[yB[5:2]] ) begin
						candIntraPredModeA <= 6'd1;
					end else begin
						candIntraPredModeA <= IntraPredModeY_buf_left[yB[5:2]];
					end
					if (!availableTop || CuPredMode_buf[xB[5:2]] ) begin
						candIntraPredModeB <= 6'd1;
					end else begin
						// if top pixel out of current CTU
						if ( (Log2CtbSizeY == 3'd5 & yB[4:0] == 5'd0) || (Log2CtbSizeY == 3'd4 & yB[3:0] == 4'd0) ) 
							candIntraPredModeB <= 6'd1;
						else
							candIntraPredModeB <= IntraPredModeY_buf[xB[5:2]];
					end	
					state <= `STATE_CU_DERIVE_2;
				end
				`STATE_CU_DERIVE_2 : begin //11
					if( candIntraPredModeA == candIntraPredModeB) begin
						if (candIntraPredModeA<2) begin
							candModeList[0] <= 6'd0;
							candModeList[1] <= 6'd1;
							candModeList[2] <= 6'd26;
						end else begin
							candModeList[0] <= candIntraPredModeA;
							candModeList[1] <= 6'd2+ ((candIntraPredModeA+6'd29) & 6'd31 );
							candModeList[2] <= 6'd2+ ((candIntraPredModeA-6'd1) & 6'd31);
						end
					end else begin
						candModeList[0] <= candIntraPredModeA;
						candModeList[1] <= candIntraPredModeB;
						if (candIntraPredModeA != 0 && candIntraPredModeB!= 0)
							candModeList[2] <= 6'd0;
						else if(candIntraPredModeA != 1 && candIntraPredModeB!= 1) 
							candModeList[2] <= 6'd1;
						else 
							candModeList[2] <= 6'd26;
					end
					state <= `STATE_CU_DERIVE_3;
				end
				`STATE_CU_DERIVE_3 : begin //12
					if (prev_intra_luma_pred_flag[ {j,i} ]) begin
//						case ( {j,i} ) 
//							2'b00 : IntraPredModeY0 <= candModeList[ mpm_idx[0]];
//							2'b01 : IntraPredModeY1 <= candModeList[ mpm_idx[1]];
//							2'b10 : IntraPredModeY2 <= candModeList[ mpm_idx[2]];
//							2'b11 : IntraPredModeY3 <= candModeList[ mpm_idx[3]];
//						endcase
						mode <= candModeList[ mpm_idx[{j,i}]];
						state <= `STATE_CU_DERIVE_POST;
					end else begin
						state <= `STATE_CU_DERIVE_SORT;
					end
					
				end
				`STATE_CU_DERIVE_SORT : begin //
					case ( { (candModeList[0] >= candModeList[1]), (candModeList[0] >= candModeList[2]), (candModeList[1] >= candModeList[2])} )
						3'b000: begin // 0 < 1 < 2
							candModeList[0] <= candModeList[0];
							candModeList[1] <= candModeList[1];
							candModeList[2] <= candModeList[2];
						end
						3'b001: begin // 0 < 2 < 1
							candModeList[0] <= candModeList[0];
							candModeList[1] <= candModeList[2];
							candModeList[2] <= candModeList[1];
						end // 010 - x
						3'b011: begin
							candModeList[0] <= candModeList[2];
							candModeList[1] <= candModeList[0];
							candModeList[2] <= candModeList[1];
						end
						3'b100: begin
							candModeList[0] <= candModeList[1];
							candModeList[1] <= candModeList[0];
							candModeList[2] <= candModeList[2];
						end // 101 - x
						3'b110: begin
							candModeList[0] <= candModeList[1];
							candModeList[1] <= candModeList[2];
							candModeList[2] <= candModeList[0];
						end
						3'b111: begin
							candModeList[0] <= candModeList[2];
							candModeList[1] <= candModeList[1];
							candModeList[2] <= candModeList[0];
						end
					endcase
					state <= `STATE_CU_DERIVE_REM;
					rem_i <= 2'b0;
					mode <= rem_intra_luma_pred_mode[ {j,i}];
				end
				`STATE_CU_DERIVE_REM : begin
					if ( rem_i == 2'd3 || mode < candModeList[rem_i]) begin
						state <= `STATE_CU_DERIVE_POST;
					end else begin
						rem_i <= rem_i + 1'b1;
						mode <= mode + 1'b1;
					end
				end
				`STATE_CU_DERIVE_POST : begin //13
					if (IntraSplitFlag) begin
						case ( {j,i} ) 
							2'b00 : IntraPredModeY0 <= mode;
							2'b01 : IntraPredModeY1 <= mode;
							2'b10 : IntraPredModeY2 <= mode;
							2'b11 : IntraPredModeY3 <= mode;
						endcase
						case (log2CbSize) 
							3'd3 : size <= 4'd0;
							3'd4 : size <= 4'd1;
							3'd5 : size <= 4'd3;
							3'd6 : size <= 4'd7;
						endcase
					end else begin
						IntraPredModeY0 <= mode;
						IntraPredModeY1 <= mode;
						IntraPredModeY2 <= mode;
						IntraPredModeY3 <= mode;
						case (log2CbSize) 
							3'd3 : size <= 4'd1;
							3'd4 : size <= 4'd3;
							3'd5 : size <= 4'd7;
							3'd6 : size <= 4'd15;
						endcase
					end
					x <= 4'd0;
					state <= `STATE_CU_DERIVE_WRITE;
				end
				`STATE_CU_DERIVE_WRITE : begin //14
					IntraPredModeY_buf[ xB[5:2]+x] <= mode;
					IntraPredModeY_buf_left[ yB[5:2] +x] <= mode;
					CuPredMode_buf[ xB[5:2]+x] <= 1'b0;
					CuPredMode_buf_left[ yB[5:2] +x] <= 1'b0;
					qPY_left[ qp_y0 + x[3:1]] <= qPY;
					qPY_top [ qp_x0 + x[3:1]] <= qPY;
					
					if (x<size) begin
						x <= x+1'b1;
					end else begin
						if (IntraSplitFlag && ( {j,i} != 2'b11 )) begin
							{j,i} <= {j,i} + 1'b1;
							case (log2CbSize) // i==1 implies moving back
								3'd3 : xB <= (i==0) ?  x0 + 6'd4 : x0;
								3'd4 : xB <= (i==0) ?  x0 + 6'd8 : x0;
								3'd5 : xB <= (i==0) ?  x0 + 6'd16 : x0;
								3'd6 : xB <= (i==0) ?  x0 + 6'd32 : x0;
							endcase
							case (log2CbSize) // i==1 implies moving back
								3'd3 : yB <= ( {j,i} >= 1) ?  y0 + 6'd4 : y0;
								3'd4 : yB <= ( {j,i} >= 1) ?  y0 + 6'd8 : y0;
								3'd5 : yB <= ( {j,i} >= 1) ?  y0 + 6'd16 : y0;
								3'd6 : yB <= ( {j,i} >= 1) ?  y0 + 6'd32 : y0;
							endcase
							state <= `STATE_CU_DERIVE_1;
						end else begin
							state <= `STATE_CU_CHROMA_PUSH;
						end
					end
				end
				`STATE_CU_CHROMA_PUSH : begin
					state <= `STATE_CU_CHROMA_READ;
				end
				`STATE_CU_CHROMA_READ : begin
					if (cabac_out_valid) begin
						case (cabac_out_data[2:0])
							0: IntraPredModeC <= (IntraPredModeY0 == 0) ? 6'd34 : 6'd0;
							1: IntraPredModeC <= (IntraPredModeY0 == 26) ? 6'd34 : 6'd26;
							2: IntraPredModeC <= (IntraPredModeY0 == 10) ? 6'd34 : 6'd10;
							3: IntraPredModeC <= (IntraPredModeY0 == 1) ? 6'd34 : 6'd1;
							4: IntraPredModeC <= IntraPredModeY0;
						endcase
						state <= `STATE_CU_RQT_PUSH;
					end
				end
				
				// INTER
				`STATE_CU_PU_1 : begin
					case (PartMode) 
						`PART_2Nx2N : begin
							pu_nPbW <= cqt_cbSize;
							pu_nPbH <= cqt_cbSize;
							pu_ret_state <= `STATE_CU_RQT_PUSH;
						end
						`PART_2NxN : begin
							pu_nPbW <= cqt_cbSize;
							pu_nPbH <= {1'b0,cqt_cbSize[6:1]}; // *1/2
							pu_ret_state <= `STATE_CU_PU_2;
						end
						`PART_Nx2N : begin
							pu_nPbW <= {1'b0,cqt_cbSize[6:1]}; // *1/2
							pu_nPbH <= cqt_cbSize;
							pu_ret_state <= `STATE_CU_PU_2;
						end
						`PART_2NxnU : begin
							pu_nPbW <= cqt_cbSize;
							pu_nPbH <= {2'b00,cqt_cbSize[6:2]}; // * 1/4
							pu_ret_state <= `STATE_CU_PU_2;
						end
						`PART_2NxnD : begin
							pu_nPbW <= cqt_cbSize;
							pu_nPbH <= {1'b0,cqt_cbSize[6:1]} | {2'b00,cqt_cbSize[6:2]} ; // * 3/4
							pu_ret_state <= `STATE_CU_PU_2;
						end
						`PART_nLx2N : begin
							pu_nPbW <= {2'b00,cqt_cbSize[6:2]}; // * 1/4
							pu_nPbH <= cqt_cbSize;
							pu_ret_state <= `STATE_CU_PU_2;
						end
						`PART_nRx2N : begin
							pu_nPbW <= {1'b0,cqt_cbSize[6:1]} | {2'b00,cqt_cbSize[6:2]} ; // * 3/4
							pu_nPbH <= cqt_cbSize;
							pu_ret_state <= `STATE_CU_PU_2;
						end
						`PART_NxN : begin
							pu_nPbW <= {1'b0,cqt_cbSize[6:1]}; // *1/2
							pu_nPbH <= {1'b0,cqt_cbSize[6:1]}; // *1/2
							pu_ret_state <= `STATE_CU_PU_2;
						end
					endcase
					pu_part_idx <= 2'b00;
					state <= `STATE_CU_PU_WAIT;
				end
				`STATE_CU_PU_2 : begin
					case (PartMode) 
						`PART_2NxN : begin
							pu_nPbW <= cqt_cbSize;
							pu_nPbH <= {1'b0,cqt_cbSize[6:1]}; // *1/2
							pu_ret_state <= `STATE_CU_RQT_PUSH;
						end
						`PART_Nx2N : begin
							pu_nPbW <= {1'b0,cqt_cbSize[6:1]}; // *1/2
							pu_nPbH <= cqt_cbSize;
							pu_ret_state <= `STATE_CU_RQT_PUSH;
						end
						`PART_2NxnU : begin
							pu_nPbW <= cqt_cbSize;
							pu_nPbH <= {1'b0,cqt_cbSize[6:1]} | {2'b00,cqt_cbSize[6:2]} ; // * 3/4
							pu_ret_state <= `STATE_CU_RQT_PUSH;
						end
						`PART_2NxnD : begin
							pu_nPbW <= cqt_cbSize;
							pu_nPbH <= {2'b00,cqt_cbSize[6:2]}; // * 1/4
							pu_ret_state <= `STATE_CU_RQT_PUSH;
						end
						`PART_nLx2N : begin
							pu_nPbW <= {1'b0,cqt_cbSize[6:1]} | {2'b00,cqt_cbSize[6:2]} ; // * 3/4
							pu_nPbH <= cqt_cbSize;
							pu_ret_state <= `STATE_CU_RQT_PUSH;
						end
						`PART_nRx2N : begin
							pu_nPbW <= {2'b00,cqt_cbSize[6:2]}; // * 1/4
							pu_nPbH <= cqt_cbSize;
							pu_ret_state <= `STATE_CU_RQT_PUSH;
						end
						`PART_NxN : begin
							pu_nPbW <= {1'b0,cqt_cbSize[6:1]}; // *1/2
							pu_nPbH <= {1'b0,cqt_cbSize[6:1]}; // *1/2
							pu_ret_state <= `STATE_CU_PU_3;
						end
					endcase
					pu_part_idx <= 2'b01;
					state <= `STATE_CU_PU_WAIT;
				end
				`STATE_CU_PU_3 : begin // only NxN is possible
					pu_nPbW <= {1'b0,cqt_cbSize[6:1]}; // *1/2
					pu_nPbH <= {1'b0,cqt_cbSize[6:1]}; // *1/2
					pu_ret_state <= `STATE_CU_PU_4;
					pu_part_idx <= 2'b10;
					state <= `STATE_CU_PU_WAIT;
				end
				`STATE_CU_PU_4 : begin // only NxN is possible
					pu_nPbW <= {1'b0,cqt_cbSize[6:1]}; // *1/2
					pu_nPbH <= {1'b0,cqt_cbSize[6:1]}; // *1/2
					pu_ret_state <= `STATE_CU_RQT_PUSH;
					pu_part_idx <= 2'b11;
					state <= `STATE_CU_PU_WAIT;
				end
				
				`STATE_CU_PU_WAIT: begin //114
					if (pu_done) begin
						if (pu_part_idx == 2'b00) begin
							merge_flag <= pu_merge_flag;
						end
						state <= pu_ret_state;
					end
				end
				
				`STATE_CU_RQT_PUSH : begin //19
					if (!pcm_flag) begin
						if (CuPredMode_cur != `MODE_INTRA && !( PartMode==`PART_2Nx2N && merge_flag) ) begin
							state <= `STATE_CU_RQT_READ;
							
						end else begin
							state <= `STATE_CU_TRANS_TREE_CALL;
							rqt_root_cbf <= 1'b1;
						end
					end else begin
						state <= `STATE_CU_RQT_FALSE_H40Y;
					end
				end
				`STATE_CU_RQT_READ : begin //20
					if (cabac_out_valid) begin
						rqt_root_cbf <= cabac_out_data[0];
						state <= `STATE_CU_TRANS_TREE_CALL;
						if (!cabac_out_data[0]) begin
							//qPY <= ( 1 + (x0[5:3]==3'd0 ? qPY : qPY_left[y0[5:3]]) + (y0[5:3]==3'd0 ? qPY : qPY_top[x0[5:3]]) )>>1;
						end
					end
				end
				`STATE_CU_TRANS_TREE_CALL: begin //21
					if (rqt_root_cbf) begin
						MaxTrafoDepth = ( CuPredMode_cur == `MODE_INTRA ? max_transform_hierarchy_depth_intra + IntraSplitFlag : max_transform_hierarchy_depth_inter);
						T_x0[0] <= x0;
						T_y0[0] <= y0;
						T_xB[0] <= x0;
						T_yB[0] <= y0;
						T_trafoSize[0] <= log2CbSize;
						T_blkIdx[0] <= 2'b0;
						T_ret_state[0] <= `STATE_CU_END;
						trafoDepth <= 2'b00;
						state <= `STATE_TT_SPLIT_PUSH;
					end else begin
						state <= `STATE_CU_RQT_FALSE_H40Y;
						// if CU 8x8 
						/*
						qPY_left[y0[5:3]]  <= qPY;
						qPY_top [x0[5:3]]  <= qPY;
						if (log2CbSize > 3) begin // 16x16
							qPY_left[y0[5:3]+1'b1]  <= qPY;
							qPY_top [x0[5:3]+1'b1]  <= qPY;
						end
						if (log2CbSize > 4) begin // 32x32
							qPY_left[y0[5:3]+2'b10]  <= qPY;
							qPY_left[y0[5:3]+2'b11]  <= qPY;
							qPY_top [x0[5:3]+2'b10]  <= qPY;
							qPY_top [x0[5:3]+2'b11]  <= qPY;
						end */
						// No need for 64x64 
					end
				end
				`STATE_CU_RQT_FALSE_H40Y : begin //22
					if (!out_fifo_full) begin
						state <= `STATE_CU_RQT_FALSE_H41;
					end
				end
				`STATE_CU_RQT_FALSE_H41 : begin //23
					if (!out_fifo_full) begin
						state <= `STATE_CU_RQT_FALSE_H40CB;
					end
				end
				`STATE_CU_RQT_FALSE_H40CB : begin //24
					if (!out_fifo_full) begin
						state <= `STATE_CU_RQT_FALSE_H40CR;
					end
				end
				`STATE_CU_RQT_FALSE_H40CR : begin //25
					if (!out_fifo_full) begin
						if (log2CbSize == 3'd6) begin
							if ( {y0[5],x0[5]} == 2'b11) begin
								state <= `STATE_CU_END;
							end else begin
								state <= `STATE_CU_RQT_FALSE_H40Y;
								{y0[5],x0[5]} <= {y0[5],x0[5]} + 1'b1;
							end
						end else begin
							state <= `STATE_CU_END;
						end
					end 
				end
				`STATE_TT_SPLIT_PUSH: begin //30
					if (T_trafoSize[trafoDepth] > Log2MaxTrafoSize) begin
						split_transform_flag <= 1'b1;
						state <= `STATE_TT_CBFCB_PUSH;
					end else if (IntraSplitFlag ==1'b1 && trafoDepth==2'b00) begin
						split_transform_flag <= 1'b1;
						state <= `STATE_TT_CBFCB_PUSH;
					end else if (max_transform_hierarchy_depth_inter == 0 && CuPredMode_cur != `MODE_INTRA && PartMode != `PART_2Nx2N && trafoDepth == 0) begin
						split_transform_flag <= 1'b1;
						state <= `STATE_TT_CBFCB_PUSH;
					end else if ( T_trafoSize[trafoDepth] <= Log2MinTrafoSize) begin
						split_transform_flag <= 1'b0;
						state <= `STATE_TT_CBFCB_PUSH;
					end else if (trafoDepth >= MaxTrafoDepth) begin
						split_transform_flag <= 1'b0;
						state <= `STATE_TT_CBFCB_PUSH;
					end else begin
						state <= `STATE_TT_SPLIT_READ;
					end
						
				end
				`STATE_TT_SPLIT_READ: begin //31
					if (cabac_out_valid) begin
						split_transform_flag <= cabac_out_data[0];
						state <= `STATE_TT_CBFCB_PUSH;
					end
				end
				`STATE_TT_CBFCB_PUSH : begin //32
					if (T_trafoSize[trafoDepth]>2) begin
						if (trafoDepth==2'b00 || cbf_cb_B[trafoDepth-1'b1]) begin
						// push cbf_cb
							state <= `STATE_TT_CBFCR_PUSH;
							cbf_cb_pushed <= 1'b1;
						end else begin
							cbf_cb_0[trafoDepth] = 1'b0;
							cbf_cb_pushed <= 1'b0;
							state <= `STATE_TT_CBFCR_PUSH;
						end
					end else begin
						cbf_cb_pushed <= 1'b0;
						if (trafoDepth>0) begin
							cbf_cb_0[trafoDepth] = cbf_cb_B[trafoDepth-1'b1];
						end else begin
							cbf_cb_0[trafoDepth] = 1'b0;
						end
						state <= `STATE_TT_CBFCR_PUSH;
					end
				end
				`STATE_TT_CBFCR_PUSH : begin //33
					if (T_trafoSize[trafoDepth]>2) begin
						if (trafoDepth==2'b00 || cbf_cr_B[trafoDepth-1'b1]) begin
							cbf_cr_pushed <= 1'b1;
							state <= `STATE_TT_CBFCB_READ;
						end else begin
							cbf_cr_pushed <= 1'b0;
							cbf_cr_0[trafoDepth] = 1'b0;
							state <= `STATE_TT_CBFCB_READ;
						end
					end else begin
						cbf_cr_pushed <= 1'b0;
						if (trafoDepth>0) begin
							cbf_cr_0[trafoDepth] = cbf_cr_B[trafoDepth-1'b1];
						end else begin
							cbf_cr_0[trafoDepth] = 1'b0;
						end
						state <= `STATE_TT_CBFCB_READ;

					end
				end
				`STATE_TT_CBFCB_READ : begin //34
					if (cbf_cb_pushed) begin 
						if (cabac_out_valid) begin
							cbf_cb_0[trafoDepth] = cabac_out_data[0];
							state <= `STATE_TT_CBFCR_READ;
						end
					end else begin
						state <= `STATE_TT_CBFCR_READ;
					end
				end
				`STATE_TT_CBFCR_READ : begin //35
					if (cbf_cr_pushed) begin 
						if (cabac_out_valid) begin
							cbf_cr_0[trafoDepth] = cabac_out_data[0];
							state <= `STATE_TT_QT_0;
						end
					end else begin
						state <= `STATE_TT_QT_0;
					end
				end
				`STATE_TT_QT_0 : begin //36
					cbf_cb_B[trafoDepth]  <= cbf_cb_0[trafoDepth];
					cbf_cr_B[trafoDepth]  <= cbf_cr_0[trafoDepth];
					if (split_transform_flag) begin
						T_x0[trafoDepth+1'b1] <= T_x0[trafoDepth];
						T_y0[trafoDepth+1'b1] <= T_y0[trafoDepth];
						T_xB[trafoDepth+1'b1] <= T_x0[trafoDepth];
						T_yB[trafoDepth+1'b1] <= T_y0[trafoDepth];
						T_trafoSize[trafoDepth+1'b1] <= T_trafoSize[trafoDepth] -1'b1;
						T_blkIdx[trafoDepth+1'b1] <= 2'b00;
						T_ret_state[trafoDepth+1'b1] <= `STATE_TT_QT_1;
						trafoDepth <= trafoDepth+1'b1;
						state <= `STATE_TT_SPLIT_PUSH;
					end else begin
						state <= `STATE_TT_CBFY_PUSH;
					end
				end
				`STATE_TT_QT_1 : begin //37
					T_x0[trafoDepth+1'b1] <= x1;
					T_y0[trafoDepth+1'b1] <= T_y0[trafoDepth];
					T_xB[trafoDepth+1'b1] <= T_x0[trafoDepth];
					T_yB[trafoDepth+1'b1] <= T_y0[trafoDepth];
					T_trafoSize[trafoDepth+1'b1] <= T_trafoSize[trafoDepth] -1'b1;
					T_blkIdx[trafoDepth+1'b1] <= 2'b01;
					T_ret_state[trafoDepth+1'b1] <= `STATE_TT_QT_2;
					trafoDepth <= trafoDepth+1'b1;
					state <= `STATE_TT_SPLIT_PUSH;
					
				end
				`STATE_TT_QT_2 : begin //38
					T_x0[trafoDepth+1'b1] <= T_x0[trafoDepth];
					T_y0[trafoDepth+1'b1] <= y1;
					T_xB[trafoDepth+1'b1] <= T_x0[trafoDepth];
					T_yB[trafoDepth+1'b1] <= T_y0[trafoDepth];
					T_trafoSize[trafoDepth+1'b1] <= T_trafoSize[trafoDepth] -1'b1;
					T_blkIdx[trafoDepth+1'b1] <= 2'b10;
					T_ret_state[trafoDepth+1'b1] <= `STATE_TT_QT_3;
					trafoDepth <= trafoDepth+1'b1;
					state <= `STATE_TT_SPLIT_PUSH;
					
				end
				`STATE_TT_QT_3 : begin //39
					T_x0[trafoDepth+1'b1] <= x1;
					T_y0[trafoDepth+1'b1] <= y1;
					T_xB[trafoDepth+1'b1] <= T_x0[trafoDepth];
					T_yB[trafoDepth+1'b1] <= T_y0[trafoDepth];
					T_trafoSize[trafoDepth+1'b1] <= T_trafoSize[trafoDepth] -1'b1;
					T_blkIdx[trafoDepth+1'b1] <= 2'b11;
					T_ret_state[trafoDepth+1'b1] <= `STATE_TT_END;
					trafoDepth <= trafoDepth+1'b1;
					state <=  `STATE_TT_SPLIT_PUSH;
				end
				`STATE_TT_CBFY_PUSH : begin //40
					if (CuPredMode_cur==`MODE_INTRA || trafoDepth !=0 || cbf_cb_0[trafoDepth] || cbf_cr_0[trafoDepth] ) begin
						state <= `STATE_TT_CBFY_READ;
					end else begin
						cbf_luma_0[trafoDepth] <= 1'b1;
						state <= `STATE_TU_QP_ABS_PUSH;
					end
				end
				`STATE_TT_CBFY_READ : begin //41
					if (cabac_out_valid) begin
						cbf_luma_0[trafoDepth] <= cabac_out_data[0];
						state <= `STATE_TU_QP_ABS_PUSH;
					end
				end
				`STATE_TT_END : begin //42
					state <= T_ret_state[trafoDepth];
					trafoDepth <= trafoDepth -1'b1;
				end
				
				`STATE_TU_QP_ABS_PUSH : begin //105
					if (cbf_luma_0[trafoDepth] || cbf_cb_0[trafoDepth] || cbf_cr_0[trafoDepth]) begin
						if (cu_qp_delta_enabled_flag & !IsCuQpDeltaCoded) begin
							IsCuQpDeltaCoded = 1'b1;
							state <= `STATE_TU_QP_ABS_READ;
						end else begin
							state <= `STATE_TU_CALL_RUY;
						end
					end else begin
						state <= `STATE_TU_CALL_RUY; // can probably change with TU_END
					end
				end
				`STATE_TU_QP_ABS_READ : begin
					if (cabac_out_valid) begin
						cu_qp_delta <= cabac_out_data[5:0];
						if (cabac_out_data[5:0] == 6'd0) begin
							state <= `STATE_TU_QP_UPDATE;
							case (log2CbSize) 
								3: tu_i <= 3'd0;
								4: tu_i <= 3'd1;
								5: tu_i <= 3'd3;
								6: tu_i <= 3'd7;
							endcase
						end else begin
							state <= `STATE_TU_QP_SIGN_READ;
						end
					end
				end
				`STATE_TU_QP_SIGN_READ : begin
					if (cabac_out_valid) begin
						if (cabac_out_data[0]) begin
							cu_qp_delta <= (~cu_qp_delta) + 1'b1;
							qPY <= qPY + (~cu_qp_delta) + 1'b1;
						end else begin
							qPY <= qPY + cu_qp_delta;
						end
						state <= `STATE_TU_QP_UPDATE;
						case (log2CbSize) 
							3: tu_i <= 3'd0;
							4: tu_i <= 3'd1;
							5: tu_i <= 3'd3;
							6: tu_i <= 3'd7;
						endcase
					end
				end
				`STATE_TU_QP_UPDATE : begin
					if (tu_i == 3'd0) begin
						state <= `STATE_TU_CALL_RUY;
					end
					qPY_left[qp_y0+ tu_i] <= qPY;
					qPY_top [qp_x0 + tu_i] <= qPY;
					tu_i <= tu_i - 1'b1;
				end
				
				`STATE_TU_CALL_RUY : begin //45
					ru_trafoSize <= T_trafoSize[trafoDepth];
					ru_cIdx <= 2'b00;
					ru_x0 <= T_x0[trafoDepth];
					ru_y0 <= T_y0[trafoDepth];
					if (cbf_luma_0[trafoDepth]) begin
						state <= `STATE_RU_SKIP_PUSH;
						res_present <= 1'b1;
					end else begin
						transform_skip_flag <= 1'b0;
						state <= `STATE_PUSH_H40;
						res_present <= 1'b0;
						push_h40_return_state <= `STATE_TU_CALL_RUCB1;
						//push_h_40, push_H41
					end
				end
				`STATE_TU_CALL_RUCB1 : begin //46
					ru_trafoSize <= T_trafoSize[trafoDepth]-1'b1;
					ru_cIdx <= 2'b01;
					ru_x0 <= T_x0[trafoDepth];
					ru_y0 <= T_y0[trafoDepth];
					if (T_trafoSize[trafoDepth] > 3'd2) begin
						if (cbf_cb_0[trafoDepth]) begin
							state <= `STATE_RU_SKIP_PUSH;
							res_present <= 1'b1;
						end else begin
							transform_skip_flag <= 1'b0;
							state <= `STATE_PUSH_H40;
							res_present <= 1'b0;
							push_h40_return_state <= `STATE_TU_CALL_RUCR1;
							//push_h_40, push_H41
						end
					end else begin
						state <= `STATE_TU_CALL_RUCB2;
					end
				end
				`STATE_TU_CALL_RUCR1 : begin //47
					ru_trafoSize <= T_trafoSize[trafoDepth]-1'b1;
					ru_cIdx <= 2'b10;
					ru_x0 <= T_x0[trafoDepth];
					ru_y0 <= T_y0[trafoDepth];
					if (T_trafoSize[trafoDepth] > 3'd2) begin
						if (cbf_cr_0[trafoDepth]) begin
							state <= `STATE_RU_SKIP_PUSH;
							res_present <= 1'b1;
						end else begin
							transform_skip_flag <= 1'b0;
							state <= `STATE_PUSH_H40;
							res_present <= 1'b0;
							push_h40_return_state <= `STATE_TU_END;
							//push_h_40, push_H41
						end
					end else begin
						state <= `STATE_TU_CALL_RUCB2;
					end
				end
				`STATE_TU_CALL_RUCB2 : begin //48
					ru_trafoSize <= T_trafoSize[trafoDepth];
					ru_cIdx <= 2'b01;
					ru_x0 <= T_xB[trafoDepth];
					ru_y0 <= T_yB[trafoDepth];
					if (T_blkIdx[trafoDepth]==2'd3) begin
						if (cbf_cb_B[trafoDepth]) begin
							state <= `STATE_RU_SKIP_PUSH;
							res_present <= 1'b1;
						end else begin
							transform_skip_flag <= 1'b0;
							state <= `STATE_PUSH_H40;
							res_present <= 1'b0;
							push_h40_return_state <= `STATE_TU_CALL_RUCR2;
							//push_h_40, push_H41
						end
					end else begin
						state <= `STATE_TU_CALL_RUCR2;
					end
				end
				`STATE_TU_CALL_RUCR2 : begin //49
					ru_trafoSize <= T_trafoSize[trafoDepth];
					ru_cIdx <= 2'b10;
					ru_x0 <= T_xB[trafoDepth];
					ru_y0 <= T_yB[trafoDepth];
					if (T_blkIdx[trafoDepth]==2'd3) begin
						if (cbf_cr_B[trafoDepth]) begin
							state <= `STATE_RU_SKIP_PUSH;
							res_present <= 1'b1;
						end else begin
							transform_skip_flag <= 1'b0;
							state <= `STATE_PUSH_H40;
							res_present <= 1'b0;
							push_h40_return_state <= `STATE_TU_END;
							//push_h_40, push_H41
						end
					end else begin
						state <= `STATE_TU_END;
					end
				end
				`STATE_TU_END : begin //50 
					state <= `STATE_TT_END;
				end
				
				`STATE_RU_SKIP_PUSH : begin //55
					if (transform_skip_enabled_flag) begin
						if (ru_trafoSize == 3'd2)  begin
							state <= `STATE_RU_SKIP_READ;
						end else begin
							transform_skip_flag <= 1'b0;
							state <= `STATE_PUSH_H40;
							push_h40_return_state <= `STATE_RU_CALL_ACCEL;	
						end
					end else begin
						transform_skip_flag <= 1'b0;	
						state <= `STATE_PUSH_H40;
						//res_present = 1'b1;
						push_h40_return_state <= `STATE_RU_CALL_ACCEL;						
					end
				end

				`STATE_RU_SKIP_READ : begin //56
					if (cabac_out_valid) begin
						transform_skip_flag <= cabac_out_data[0];
						state <= `STATE_PUSH_H40;
						//res_present = 1'b1;
						push_h40_return_state <= `STATE_RU_CALL_ACCEL;
					end
				end
				`STATE_RU_CALL_ACCEL : begin //57
					state <= `STATE_RU_WAIT;
					if (CuPredMode_cur == 2'b00 && ( ru_trafoSize == 3'd2 || (ru_trafoSize==3'd3 && ru_cIdx==0) )) begin
						if (predModeIntra >=6 && predModeIntra <=14 ) ru_scanIdx <= 2'd2;
						else if (predModeIntra >=22 && predModeIntra <=30 ) ru_scanIdx <= 2'd1;
						else ru_scanIdx <= 2'd0;
					end else begin
						ru_scanIdx <= 2'b00;
					end
				end
				`STATE_RU_WAIT: begin //58
					if (res_done) begin
						case (ru_cIdx)
							2'b00 : state <= `STATE_TU_CALL_RUCB1;
							2'b01 : state <= T_trafoSize[trafoDepth] > 3'd2 ? `STATE_TU_CALL_RUCR1: `STATE_TU_CALL_RUCR2;
							2'b10 : state <=  `STATE_TU_END;
						endcase 
					end
				end
				
				`STATE_PUSH_H40: begin //60
					if (!out_fifo_full) begin
						if (ru_cIdx ==2'b00) begin
							state <= `STATE_PUSH_H41;
						end else begin
							state <= push_h40_return_state;
						end
					end
				end
				`STATE_PUSH_H41: begin //61
					if (!out_fifo_full) begin
						state <= push_h40_return_state;
					end
				end
				`STATE_CU_END : begin //62
					state <= `STATE_CQT_END;
				end
				
				`STATE_SSD_END : begin //93
					state <= `STATE_CU_INIT;
				end
			endcase
		end
	end
	
	always @(*) begin
		out_fifo_valid = 1'b0;
		cabac_out_read = 1'b0;
		cabac_in_valid = 1'b0;
		cabac_in_data = 32'dx;
		out_fifo_data = 32'dx;
		out_fifo_valid = 1'b0;
		done = 1'b0;
		
		res_mmap_valid = 1'b0;
		pu_valid = 1'b0;
		sao_valid = 1'b0;
		
		annexb_offload_override = 1'b0;
		annexb_read_bits = 4'dx;
		annexb_read_valid = 1'b0;
		
		cabac_op = 2'bxx;
		cabac_op_valid = 1'b0;
		
		case (state) 
		// moved to uBlaze
//			`STATE_SSD_SEND_H16 : begin
//				if (!out_fifo_full) begin
//					out_fifo_valid = 1'b1;
//					{out_fifo_data[7:0],out_fifo_data[15:8] ,out_fifo_data[23:16] ,out_fifo_data[31:24] } = {24'd0,8'h16};
//				end
//			end
//			`STATE_SSD_SEND_H17 : begin
//				if (!out_fifo_full) begin
//					out_fifo_valid = 1'b1;
//					{out_fifo_data[7:0],out_fifo_data[15:8] ,out_fifo_data[23:16] ,out_fifo_data[31:24] } = {8'd0,tile_width,8'h17};
//				end
//			end
			`STATE_SSD_CTU_XY_READ : begin
				if (div_done & entropy_coding_sync_enabled_flag) begin
					if (ctbAddrInTS != ctbAddrInTS_begin) begin
						if (div_rem[5:0] == 2) begin
							cabac_op = 2'b00;
							cabac_op_valid = 1'b1;
						end
						
					end
				end
			end
			`STATE_SSD_BYTE_ALIGNMENT_IF: begin
				if (annexb_out_valid) begin
					annexb_read_bits = (annexb_pos[2:0] == 3'b000) ? 4'd0 : 4'd8-annexb_pos[2:0];
					annexb_read_valid = 1;
					if (SliceAddrRS <= ((ctbAddrInTS - PicWidthInCtbsY)+1'b1)) begin
						cabac_op = 2'b01;
						cabac_op_valid = 1'b1;
					end else begin
						cabac_op = 2'b11;
						cabac_op_valid = 1'b1;
					end
					annexb_offload_override = 1'b1;
				end
			end
			`STATE_SSD_LOAD_INIT_CABAC: begin
				//if (cabac_op_done) begin
					
				//end else begin
					if (SliceAddrRS <= ((ctbAddrInTS - PicWidthInCtbsY)+1'b1)) begin
						cabac_op = 2'b01;
					end else begin
						cabac_op = 2'b11;
					end
				//end
			end
			`STATE_SSD_INIT_ENGINE: begin
				cabac_op = 2'b10;
				cabac_op_valid = 1'b1;
			end
			`STATE_SSD_INIT_ENGINE_DONE: begin
				cabac_op = 2'b10;
			end
			`STATE_SSD_SAVE_CABAC: begin
				cabac_op = 2'b00;
			end
			`STATE_SSD_SEND_H20 : begin
				if (!out_fifo_full) begin
					out_fifo_valid = 1'b1;
					{out_fifo_data[7:0],out_fifo_data[15:8] ,out_fifo_data[23:16] ,out_fifo_data[31:24] } = {yCtb[11:0], xCtb[11:0] ,8'h20};
				end
			end
			`STATE_SSD_SEND_H21 : begin
				if (!out_fifo_full) begin
					out_fifo_valid = 1'b1;
					{out_fifo_data[7:0],out_fifo_data[15:8] ,out_fifo_data[23:16] ,out_fifo_data[31:24] } = {8'd0,curr_slice,curr_tile,8'h21};
				end
			end
			`STATE_CTU_START_SAO : begin
				if (slice_sao_luma_flag | slice_sao_chroma_flag) begin
					sao_valid = 1'b1;
				end
			end
			`STATE_CTU_SAO_WAIT : begin
				cabac_in_data = sao_cabac_in_data;
				cabac_in_valid = sao_cabac_in_valid;
				cabac_out_read = sao_cabac_out_read;
				out_fifo_data = sao_push_data;
				out_fifo_valid = sao_push_valid;
			end
			`STATE_SSD_END_OF_SLICE_SEGMENT_FLAG_PUSH: begin
				cabac_in_data = { 8'd7, 24'd0};
				cabac_in_valid = 1'b1;
			end
			`STATE_SSD_END_OF_SLICE_SEGMENT_FLAG_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
					//if (cabac_out_data[0] == 1'b1) begin
					//	done = 1'b1;
					//end
				end
			end
			
			`STATE_SSD_END : begin
				done = 1'b1;		
			end
			
			`STATE_CQT_SPLIT_PUSH : begin //65
				if (!in_fifo_almost_empty) begin
					cabac_in_data = { 8'd9, 20'd0, availableL, (availableL & condL), availableA, (availableA & condA)};
					cabac_in_valid = 1'b1;
				end
			end
			`STATE_CQT_SPLIT_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			`STATE_CU_TRANSQUANT_BYPASS_PUSH : begin //95
				if (!in_fifo_almost_empty) begin
					if (transquant_bypass_enable_flag) begin
						cabac_in_data = { 8'd10, 24'd0};
						cabac_in_valid = 1'b1;
					end
				end
			end	
			`STATE_CU_TRANSQUANT_BYPASS_READ : begin //96
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			`STATE_CU_SKIP_PUSH : begin
				if (slice_type != `SLICE_I) begin
					cabac_in_data = { 8'd11, 20'd0, availableL, (availableL & condL), availableA, (availableA & condA)};
					cabac_in_valid = 1'b1;
				end
			end
			`STATE_CU_SKIP_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			
			`STATE_CU_PREDMODE_PUSH : begin
				if (slice_type != `SLICE_I) begin
					cabac_in_data = { 8'd14, 24'd0};
					cabac_in_valid = 1'b1;
				end
			end
			`STATE_CU_PREDMODE_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			`STATE_CU_PARTMODE_PUSH : begin
				if ( (CuPredMode_cur !=`MODE_INTRA) || (log2CbSize ==Log2MinCbSizeY)) begin
					cabac_in_data = { 8'd15, log2CbSize, Log2MinCbSizeY, CuPredMode_cur[0], amp_enabled_flag, 16'd0};
					cabac_in_valid = 1'b1;
				end
			end
			`STATE_CU_PARTMODE_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			`STATE_CU_PCM_FLAG_PUSH : begin
				cabac_in_data = { 8'd16, 24'd0};
				cabac_in_valid = 1'b1;
			end
			`STATE_CU_PCM_FLAG_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			`STATE_CU_SENDH30 : begin //5
				if (!out_fifo_full) begin
					out_fifo_valid = 1'b1;
					if (cu_skip_flag) begin
						pu_valid = 1'b1;
						{out_fifo_data[7:0],out_fifo_data[15:8] ,out_fifo_data[23:16] ,out_fifo_data[31:24] } =
							{ 3'b0 ,pcm_flag, cu_transquant_bypass_flag , 3'b000 , 1'b1 , log2CbSize[2:0] ,{y0_CTU,1'b0},{x0_CTU,1'b0} ,8'h30}; 
						// y0_CTU is used for h40 which dropped the least significant bit. 
					end else begin
						{out_fifo_data[7:0],out_fifo_data[15:8] ,out_fifo_data[23:16] ,out_fifo_data[31:24] } =
							{ 3'b0 ,pcm_flag , cu_transquant_bypass_flag , PartMode[2:0] , (CuPredMode_cur != `MODE_INTRA) , log2CbSize[2:0] ,{y0_CTU,1'b0},{x0_CTU,1'b0} ,8'h30};
					end
				end
			end
			`STATE_CU_PCM_BYTE_ALIGN : begin //117
				if (annexb_out_valid) begin
					if (annexb_pos[2:0] != 3'b000) begin
						annexb_read_bits = 4'd8 - annexb_pos[2:0];
						annexb_read_valid = 1'b1;
						annexb_offload_override = 1'b1;
					end
				end
			end
			`STATE_CU_PCM_INIT_CABAC: begin
				cabac_op = 2'b10;
				cabac_op_valid = 1'b1;
			end
			`STATE_CU_PCM_READ : begin //118
				if (annexb_out_valid) begin
					if (!out_fifo_full) begin
						annexb_read_bits = ru_cIdx == 2'b00 ? PCMBitDepthY : PCMBitDepthC;
						annexb_read_valid = 1'b1;
						annexb_offload_override = 1'b1;
						{out_fifo_data[7:0],out_fifo_data[15:8] ,out_fifo_data[23:16] ,out_fifo_data[31:24] } = 
							{ 8'd0, pcm_sample, pcm_y, pcm_x, 6'd63};
						out_fifo_valid = 1'b1;
					end
				end
			end
			`STATE_CU_PREV_PUSH : begin
				cabac_in_data = { 8'd17, 24'd0};
				cabac_in_valid = 1'b1;
			end
			`STATE_CU_PREV_READ : begin
				if (cabac_out_valid) begin //7
					cabac_out_read = 1'b1;
				end
			end
			`STATE_CU_MPM_PUSH : begin
				if (prev_intra_luma_pred_flag[ {j,i} ]) begin
					cabac_in_data = { 8'd18, 24'd0};
				end else begin
					cabac_in_data = { 8'd19, 24'd0};
				end
				cabac_in_valid = 1'b1;
			end
			`STATE_CU_MPM_READ : begin
				if (cabac_out_valid) begin //9
					cabac_out_read = 1'b1;
				end
			end
			`STATE_CU_CHROMA_PUSH : begin //15
				cabac_in_data = { 8'd20, 24'd0};
				cabac_in_valid = 1'b1;
			end
			`STATE_CU_CHROMA_READ : begin //16
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
					//done = 1'b1;
				end
			end
			
			`STATE_CU_PU_1, `STATE_CU_PU_2, `STATE_CU_PU_3, `STATE_CU_PU_4  : begin
				pu_valid = 1'b1;
			end
			`STATE_CU_PU_WAIT : begin
				cabac_in_data = pu_cabac_in_data;
				cabac_in_valid = pu_cabac_in_valid;
				cabac_out_read = pu_cabac_out_read;
				out_fifo_data = pu_push_data;
				out_fifo_valid = pu_push_valid;
			end
			
			`STATE_CU_RQT_PUSH : begin //19
				if (!pcm_flag) begin
					if (CuPredMode_cur != `MODE_INTRA && !( PartMode==`PART_2Nx2N && merge_flag) ) begin
						cabac_in_data = { 8'd31, 24'd0};
						cabac_in_valid = 1'b1;
					end
				end
			end
			`STATE_CU_RQT_READ : begin //20
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
					//done = 1'b1;
				end
			end
			`STATE_CU_RQT_FALSE_H40Y : begin //22
				if (!out_fifo_full) begin
					out_fifo_valid = 1'b1;
					{out_fifo_data[7:0],out_fifo_data[15:8] ,out_fifo_data[23:16] ,out_fifo_data[31:24] } = { 1'b0 ,1'b0 , 1'b0 , 1'b0 , 6'd35 , ( (log2CbSize[2:0] == 3'd6) ? 3'd5 : log2CbSize[2:0]) , 2'b00 ,y0_CTU,x0_CTU ,8'h40};
				end
			end
			`STATE_CU_RQT_FALSE_H41 : begin //23
				if (!out_fifo_full) begin
					out_fifo_valid = 1'b1;
					{out_fifo_data[7:0],out_fifo_data[15:8] ,out_fifo_data[23:16] ,out_fifo_data[31:24] } = {14'd0 , qPY , 4'd0 , 8'h41};
				end
			end
			`STATE_CU_RQT_FALSE_H40CB : begin //24
				if (!out_fifo_full) begin
					out_fifo_valid = 1'b1;
					{out_fifo_data[7:0],out_fifo_data[15:8] ,out_fifo_data[23:16] ,out_fifo_data[31:24] } = { 1'b0 , 1'b0 , 1'b0 , 6'd35 , ( (log2CbSize[2:0] == 3'd6) ? 3'd4 : log2CbSize[2:0]-1'b1), 2'b01 ,y0_CTU ,x0_CTU ,8'h40};
				end
			end
			`STATE_CU_RQT_FALSE_H40CR : begin //25
				if (!out_fifo_full) begin
					out_fifo_valid = 1'b1;
					{out_fifo_data[7:0],out_fifo_data[15:8] ,out_fifo_data[23:16] ,out_fifo_data[31:24] } = { 1'b0 , 1'b0 , 1'b0 , 6'd35 , ( (log2CbSize[2:0] == 3'd6) ? 3'd4 : log2CbSize[2:0]-1'b1), 2'b10 ,y0_CTU ,x0_CTU ,8'h40};
				end
			end
			`STATE_TT_SPLIT_PUSH: begin //30
				if ( (T_trafoSize[trafoDepth] > Log2MaxTrafoSize)
					|| (IntraSplitFlag ==1'b1 && trafoDepth==2'b00) 
					|| ( T_trafoSize[trafoDepth] <= Log2MinTrafoSize) 
					|| (trafoDepth >= MaxTrafoDepth) ) begin
					cabac_in_valid = 1'b0;	
				end else begin
					cabac_in_data = { 8'd32, 21'd0, (3'd5-T_trafoSize[trafoDepth])};
					cabac_in_valid = 1'b1;
				end
			end
			`STATE_TT_SPLIT_READ : begin //31
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			`STATE_TT_CBFCB_PUSH : begin //32
				if (T_trafoSize[trafoDepth]>2) begin
					if (trafoDepth==2'b00 || cbf_cb_B[trafoDepth-1'b1]) begin
						cabac_in_data = { 8'd34, 22'd0, trafoDepth};
						cabac_in_valid = 1'b1;
					end
				end
			end
			`STATE_TT_CBFCR_PUSH : begin //33
				if (T_trafoSize[trafoDepth]>2) begin
					if (trafoDepth==2'b00 || cbf_cr_B[trafoDepth-1'b1]) begin
						cabac_in_data = { 8'd34, 22'd0, trafoDepth};
						cabac_in_valid = 1'b1;
					end
				end
			end
			`STATE_TT_CBFCB_READ : begin //34
				if (cbf_cb_pushed & cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			`STATE_TT_CBFCR_READ : begin //35
				if (cbf_cr_pushed & cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			`STATE_TT_CBFY_PUSH : begin //40
				if (CuPredMode_cur== `MODE_INTRA || trafoDepth !=0 || cbf_cb_0[trafoDepth] || cbf_cr_0[trafoDepth] ) begin
					cabac_in_data = { 8'd33, 22'd0, trafoDepth};
					cabac_in_valid = 1'b1;
				end
			end
			`STATE_TT_CBFY_READ : begin //41
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			
			`STATE_TU_QP_ABS_PUSH : begin //105
				if (cbf_luma_0[trafoDepth] || cbf_cb_0[trafoDepth] || cbf_cr_0[trafoDepth]) begin
					if (cu_qp_delta_enabled_flag & !IsCuQpDeltaCoded) begin
						cabac_in_data = { 8'd12, 24'd0};
						cabac_in_valid = 1'b1;
					end
				end
			end
			`STATE_TU_QP_ABS_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
					if (|cabac_out_data[5:0]) begin // != 0
						cabac_in_data = { 8'd13, 24'd0};
						cabac_in_valid = 1'b1;
					end
				end
			end
			`STATE_TU_QP_SIGN_READ: begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			
			`STATE_RU_SKIP_PUSH : begin //55
				if (transform_skip_enabled_flag) begin
					if (ru_trafoSize == 2)  begin
						cabac_in_data = { 8'd35, 22'd0, ru_cIdx};
						cabac_in_valid = 1'b1;
					end
				end
			end
			`STATE_RU_SKIP_READ : begin //56
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			`STATE_RU_CALL_ACCEL : begin
				res_mmap_valid = 1'b1;
			end
			`STATE_RU_WAIT : begin //58
				cabac_in_data = res_cabac_in_data;
				cabac_in_valid = res_cabac_in_valid;
				cabac_out_read = res_cabac_out_read;
				out_fifo_data = res_out_fifo_data;
				out_fifo_valid = res_out_fifo_valid;
			end
			
			`STATE_PUSH_H40 : begin //60
				if (!out_fifo_full) begin
					out_fifo_valid = 1'b1;
					{out_fifo_data[7:0],out_fifo_data[15:8] ,out_fifo_data[23:16] ,out_fifo_data[31:24] } = { 1'b0 , transform_skip_flag , res_present , predModeIntra , ru_trafoSize, ru_cIdx ,ru_y0_CTU,ru_x0_CTU ,8'h40};
				end
			end 
			`STATE_PUSH_H41 : begin //61
				if (!out_fifo_full) begin
					out_fifo_valid = 1'b1;
					{out_fifo_data[7:0],out_fifo_data[15:8] ,out_fifo_data[23:16] ,out_fifo_data[31:24] } = {14'd0 , qPY , 4'd0 , 8'h41};
				end
			end 
			
		endcase
	end
	
	always @(posedge clk) begin // divider
		if (~rst) begin
			 div_state <= 2'd0;
		end else begin
			if (div_valid) begin
				case (div_state) 
					0: begin
						div_quot <= 12'd0;
						div_rem <= div_d1;
						div_state <= 3'd1;
						div_done <= 1'b0;
					end
					1: begin
						if (div_rem < div_d2) begin
							div_state <= 3'd2;
						end else begin
							div_rem <= div_rem - div_d2;
							div_quot <= div_quot + 1'b1;
						end
					end
					2: begin
						div_done <= 1'b1;
						div_state <= 3'd3;
					end
					3: begin
						div_done <= 1'b0;
						div_state <= 3'd0;
					end
				
				endcase
			end
		end
	end

endmodule
