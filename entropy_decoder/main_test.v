`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   23:36:14 07/31/2013
// Design Name:   main
// Module Name:   C:/Users/Maleen/FPGA/EntropyDecoder/main_test.v
// Project Name:  EntropyDecoder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: main
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module main_test;

	// Inputs
	reg [5:0] se_type;
	reg [25:0] se_param;
	reg wr_en;
	reg [1:0] op;
	reg [1:0] cabac_init_type;
	reg [5:0] QP;
	reg clk;
	reg pipe_reset;

	// Outputs

	wire [31:0] decoded_se;
	wire decoded_valid;

	// Instantiate the Unit Under Test (UUT)
	cabac uut (
		.se_type(se_type), 
		.se_param(se_param), 
		.wr_en(wr_en), 
		.pipe_reset(pipe_reset),
		.decoded_se(decoded_se), 
		.decoded_valid(decoded_valid), 
		.op(op), 
		.cabac_init_type(cabac_init_type), 
		.QP(QP), 
		.clk(clk)
	);

	initial begin
		// Initialize Inputs
		se_type = 0;
		se_param = 0;
		op = 0;
		QP = 32;
		clk = 0;
		wr_en = 0;
		cabac_init_type= 2'b00;

		// Wait 100 ns for global reset to finish
		#100;
	
      op = 2'b00; # 10;
		// Add stimulus here
		op = 2'b01; #1550 ; // 155 cycle delay
		op = 2'b00; pipe_reset = 1; #10; // get current_ctx to zero 
		pipe_reset = 0;
		
		se_type = 10; se_param = 0; wr_en = 1;  #10
		se_type = 11; se_param = 0; wr_en = 1; #10
		se_type = 12; se_param = 0; wr_en = 1; #10
		wr_en = 1; #10;
		op = 2'b10; #20; // save state 
		$finish;
		op = 2'b11; # 10; 

	end
	 
	always begin
		#5 clk = ~clk;
	end
      
endmodule

