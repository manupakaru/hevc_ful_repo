`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:09:48 12/23/2013 
// Design Name: 
// Module Name:    global_top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module cabac_and_residual_top(
		clk,
		rst,
		
		entropy_in,
		entropy_valid,
		entropy_full,
		
		outCont,
		validCont,
		fullCont,
		
		outY,
		validY,
		fullY,
		
		outCb,
		validCb,
		fullCb,
		
		outCr,
		validCr,
		fullCr,
		
		GPO
		
    );
	input clk;
	input rst;
	
	output [31:0] GPO;
	
	input [31:0] entropy_in;
	input entropy_valid;
	output entropy_full;
	
	output [31:0] outCont;
	output validCont;
	input fullCont;
	output [143:0] outY, outCb, outCr;
	output validY, validCb, validCr;
	input fullY, fullCb, fullCr;
	
	wire [31:0] cabac_to_residual_data;
	wire cabac_to_residual_empty;
	wire cabac_to_residual_read;
	
	wire [31:0] cabac_to_res_data_rev = {cabac_to_residual_data[7:0], cabac_to_residual_data[15:8], cabac_to_residual_data[23:16], cabac_to_residual_data[31:24]};
	 
	 
	
	 
	main entropy(
		.clk(clk),
		.rst(rst),
		
		.entropy_in(entropy_in),
		.entropy_valid(entropy_valid),
		.entropy_full(entropy_full),

//		.entropy_in(queue_in),
//		.entropy_valid(queue_valid),
//		.entropy_full(queue_full),
//		
		.cabac_to_residual_data(cabac_to_residual_data),
		.cabac_to_residual_empty(cabac_to_residual_empty),
		.cabac_to_residual_read(cabac_to_residual_read),
		
		.GPO(GPO)
	);
	
	top_level transform(
		.clk(clk),
		.rst(rst),
		
		.data_in(cabac_to_res_data_rev),
		.empty(cabac_to_residual_empty),
		.read_next(cabac_to_residual_read),
		
		.data_out(outCont),
		.output_valid_cont(validCont),
		.emptyCont(fullCont),
		
		.outY(outY),
		.out_validY(validY),
		.emptyY(fullY),
		
		.outCb(outCb),
		.out_validCb(validCb),
		.emptyCb(fullCb),
		
		.outCr(outCr),
		.out_validCr(validCr),
		.emptyCr(fullCr)
	);
	
//	wire [7:0] outCont_8;
//	wire outCont_8_empty;
//	//wire 
//	
//	
//	fifo_32_to_8 your_instance_name (
//	  .rst(~rst), // input rst
//	  .wr_clk(clk), // input wr_clk
//	  .rd_clk(clk), // input rd_clk
//	  .din(outCont), // input [31 : 0] din
//	  .wr_en(validCont), // input wr_en
//	  .rd_en(1'b0), // input rd_en
//	  .dout(dout), // output [7 : 0] dout
//	  //.full(full), // output full
//	  .empty(empty) // output empty
//	);

endmodule
