`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:04:51 12/27/2013 
// Design Name: 
// Module Name:    Transcoeff_m 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Transcoeff_m(
    input clka,
    input wea,
    input [8:0] addra,
    input [15:0] dina,
    output reg [15:0] douta,
	 input ena
    );

	reg [15:0] bank [0:511];
	
	always @(posedge clka) begin
		if (wea) begin
			bank[addra] <= dina;
		end
		if (ena) begin
			douta <= bank[addra];
		end
	end
endmodule
