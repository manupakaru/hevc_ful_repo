`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:58:48 11/17/2013
// Design Name:   main
// Module Name:   C:/Users/Maleen/Uni/HEVC/EntropyDecoder/mcs_test.v
// Project Name:  EntropyDecoder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: main
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

//source ipcore_dir/microblaze_mcs_setup.tcl // microblaze_mcs_data2mem workspace/mcs_1/Debug/mcs_1.elf 


module mcs_test;

	// Inputs
	reg clk;
	reg rst;
	reg [31:0] entropy_in;
	reg entropy_valid;

	// Outputs
	wire entropy_full;
	wire [31:0] GPO;
	//wire push_valid;
	//wire [31:0] push_data;
	wire [31:0] PC, Ins;

	integer fd, fd_res;
	integer line, line_res;
	reg [31:0] read_res;
	reg matched_res;
	
	reg[5:0] cu_x, cu_y;
	integer time_counter [1:3];
	integer CTU_no;
	integer POC;
	// Instantiate the Unit Under Test (UUT)
	main uut (
		.clk(clk), 
		.rst(rst), 
		.entropy_in(entropy_in),
		.entropy_valid(entropy_valid),
		.entropy_full(entropy_full),
		.GPO(GPO)
//		.push_valid(push_valid),
//		.push_data(push_data)
		
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0;
		entropy_in = 0;
		entropy_valid = 0;
		line = -4;
		line_res = -4;
		//fd = $fopen("BQSquare_416x240_60_qp37.bin","rb"); 
		//fd = $fopen("avatar.hevc","rb"); 
		fd = $fopen("input.hevc","rb");
		fd_res = $fopen("cabac_to_residual_soft","rb");
		matched_res = 1;
		time_counter[1] = 0; time_counter[2] = 0; time_counter[3] = 0;
		CTU_no = 0;
		POC_printed = 0;
		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		rst = 1;
//		read_empty = 0;
//		read_in[31:24] = $fgetc(fd);
//		read_in[23:16] = $fgetc(fd);
//		read_in[15: 8] = $fgetc(fd);
//		read_in[ 7: 0] = $fgetc(fd);	
//		line = line+4;
		
		read_res[31:24] = $fgetc(fd_res);
		read_res[23:16] = $fgetc(fd_res);
		read_res[15: 8] = $fgetc(fd_res);
		read_res[ 7: 0] = $fgetc(fd_res);	
		line_res = line_res+4;
	end
	
	always begin
		#5 clk = ~clk;
	end
	reg time_printed, POC_printed;
   always @ (posedge clk) begin
		case (GPO[15:0])
			1000: begin
				time_counter[1] = time_counter[1]+1;
				time_printed = 0;
				POC_printed = 0;
			end
			2000: time_counter[2] = time_counter[2]+1;
			3000: time_counter[3] = time_counter[3]+1;
			4000: begin
				if (!time_printed) begin
					$display("CTU %3d (%5d, %5d, %5d)", CTU_no, time_counter[1], time_counter[2], time_counter[3]);
					CTU_no = CTU_no+1;
				end
				time_counter[1] = 0;
				time_counter[2] = 0;
				time_counter[3] = 0;
				time_printed = 1;
			end
			5000: begin
				POC = GPO[31:16];
				if (!POC_printed) begin
					$display ("POC %d", POC);
				end
				POC_printed = 1;
				
			end
		endcase
//		if (read_next) begin
//			read_in[31:24] = $fgetc(fd);
//			read_in[23:16] = $fgetc(fd);
//			read_in[15: 8] = $fgetc(fd);
//			read_in[ 7: 0] = $fgetc(fd);			
//			
//			//$display("data  = %h", data);
//			
//			line = line +4;
//		end
		if (uut.push_valid) begin
			if (uut.push_data==read_res)begin
				matched_res = 1;
			end else begin
				matched_res = 0;
				$finish();
			end
			if (read_res[31:24] == 8'h30) begin
				{cu_y , cu_x} = {read_res[11:8], read_res[23:16]};
			end
			read_res[31:24] = $fgetc(fd_res);
			read_res[23:16] = $fgetc(fd_res);
			read_res[15: 8] = $fgetc(fd_res);
			read_res[ 7: 0] = $fgetc(fd_res);	
			line_res = line_res+4;
		end
//		if (uut.mmap.printf_valid) begin
//			$write("%c",uut.mmap.printf_data[7:0]);
//		end
//		if (uut.mmap.exit) begin
//			$finish();
//		end
	end
	always @ (negedge clk) begin
		if ($time > 200 && ($time % 100==0)) begin
			if (!entropy_full & rst) begin
				entropy_in[31:24] = $fgetc(fd);
				entropy_in[23:16] = $fgetc(fd);
				entropy_in[15: 8] = $fgetc(fd);
				entropy_in[ 7: 0] = $fgetc(fd);			
				
				//$display("data  = %h", data);
				entropy_valid = 1'b1;
				line = line +4;
			end else begin
				entropy_valid = 1'b0;
			end
		end else begin
			entropy_valid = 1'b0;
		end
	end
endmodule
