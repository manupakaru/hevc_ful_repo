`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    00:20:09 11/11/2013 
// Design Name: 
// Module Name:    binarize 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module binarize(
    input clk,
	 
    input [5:0] se_type,
    input [25:10] se_param,
	 input [5:0] bin_no,
	 
	 input stall,
	 input binVal,
	 
	// output reg take_next_se,
	 
	 output reg [31:0] decoded_se,
	 output reg decoded_valid
	 
    );
	 
	 reg part_mode_bin2;
	 
	 reg [5:0] cMax;
	 
	 wire [31:0] TU_decoded_se, FL_decoded_se, rem_decoded_se, mvd_decoded_se, qp_decoded_se;
	 wire TU_decoded_valid, FL_decoded_valid, rem_decoded_valid, mvd_decoded_valid, qp_decoded_valid;
	 wire rem_rst, mvd_rst, qp_rst;
	 
	 wire [2:0] log2TrafoSize;
	 assign log2TrafoSize = se_param[25:23];
	 
	 bin_TU bin_TU(
		.bin_no(bin_no),
		.cMax(cMax),
		.binVal(binVal),
		.stall(stall),
		.decoded_se(TU_decoded_se),
		.decoded_valid(TU_decoded_valid)
	 );
	  bin_qp_delta bin_qp_delta(
	   .clk(clk),
		.rst(qp_rst),
		.bin_no(bin_no),
		.binVal(binVal),
		.stall(stall),
		.decoded_se(qp_decoded_se),
		.decoded_valid(qp_decoded_valid)
	 );
	 bin_FL bin_FL(
		.clk(clk),
		.bin_no(bin_no),
		.cMax(cMax),
		.binVal(binVal),
		.stall(stall),
		.decoded_se(FL_decoded_se),
		.decoded_valid(FL_decoded_valid)
	 );
	 bin_rem bin_rem(
		.clk(clk),
		.rst(rem_rst),
		.bin_no(bin_no),
		.baseLevel(se_param[25:24]),
		.binVal(binVal),
		.stall(stall),
		.decoded_se(rem_decoded_se),
		.decoded_valid(rem_decoded_valid)
	 );
	 bin_mvd bin_mvd(
		.clk(clk),
		.rst(mvd_rst),
		.bin_no(bin_no),
		.binVal(binVal),
		.stall(stall),
		.decoded_se(mvd_decoded_se),
		.decoded_valid(mvd_decoded_valid)
	 );

	assign rem_rst = (se_type != 44) && !stall ;
	assign mvd_rst = (se_type != 28);
	assign qp_rst = (se_type != 12);

	always @(*) begin
		case (se_type)
			1: begin //sao_merge_left/up
				cMax = 6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			2: begin // sao_type_idx
				cMax = 6'd2;
				decoded_se = TU_decoded_se;
				decoded_valid = TU_decoded_valid;
			end
			3: begin // sao_band_pos
				cMax = 6'd5;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			4: begin // sao_offset_abs
				cMax = 6'd7;
				decoded_se = TU_decoded_se;
				decoded_valid = TU_decoded_valid;
			end
			5: begin // sao_offset_sign
				cMax = 6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			6: begin // sao_eo_class
				cMax = 6'd2;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			7,8: begin //end_of_slice_segment_flag
				cMax = 6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			9: begin // split_cu_flag
				cMax = 6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			11: begin // cu_skip_flag
				cMax = 6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			12: begin
				cMax = 6'dx;
				decoded_se = qp_decoded_se;
				decoded_valid = qp_decoded_valid;
			end
			13: begin // cu_qp_delta_sign
				cMax = 6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			14: begin // pred_mode_flag
				cMax = 6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			15: begin // part_mode
				cMax = 6'dx;
				if (se_param[19]) begin 
					if (~stall) begin
						case (bin_no)
							0: begin
								decoded_se = (binVal==1'b1) ? 32'd0 : 32'dx;
								decoded_valid = (binVal==1'b1);
							end
							1: begin
								if (se_param[25:23] == se_param[22:20]) begin
									if (binVal == 1'b1) begin
										decoded_se = 32'd1; decoded_valid = 1'b1;
									end else begin
										if (se_param[25:23] == 3'd3) begin
											decoded_se = 32'd2; decoded_valid = 1'b1;
										end else begin
											decoded_se = 32'dx; decoded_valid = 1'b0;
										end
									end
								end else begin
									if (binVal == 1'b1) begin
										if (~se_param[18]) begin // amp_enabled false
											decoded_se = 32'd1; decoded_valid = 1'b1;
										end else begin
											decoded_se = 32'dx; decoded_valid = 1'b0;
										end
									end else begin
										if (~se_param[18]) begin // amp_enabled false
											decoded_se = 32'd2; decoded_valid = 1'b1;
										end else begin
											decoded_se = 32'dx; decoded_valid = 1'b0;
										end
									end
								end
							end
							2: begin
								if (se_param[25:23] == se_param[22:20]) begin
									decoded_se = (binVal==1'b1) ? 32'd2 : 32'd3;
									decoded_valid = 1'b1;
								end else begin
									if (binVal==1'b1) begin
										decoded_se = (part_mode_bin2) ? 32'd1 : 32'd2;
										decoded_valid = 1'b1;
									end else begin
										decoded_se = 32'dx; decoded_valid = 1'b0;
									end
								end
							end
							3: begin
								case ( {part_mode_bin2, binVal} )
									0: decoded_se = 32'd6;
									1: decoded_se = 32'd7;
									2: decoded_se = 32'd4;
									3: decoded_se = 32'd5;
								endcase
								decoded_valid = 1'b1;
							end
							default: begin
								decoded_se = 32'dx; decoded_valid = 1'b0;
							end
						endcase
					end else begin
						decoded_se = 32'dx;
						decoded_valid = 1'b0;
					end

				end else begin
					decoded_se = binVal ? 32'd0 : 32'd3;
					decoded_valid = ~stall;
				end
			end
			16: begin //pcm_flag
				cMax = 6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			17: begin //prev_intra_luma_pred
				cMax = 6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end	
			18: begin //mpm_idx
				cMax = 6'd2;
				decoded_se = TU_decoded_se;
				decoded_valid = TU_decoded_valid;
			end
			19: begin //rem_intra_luma
				cMax = 6'd5;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			20: begin // intra_chroma_pred_mode
				cMax = 6'd3;
				if (bin_no == 6'd0 & binVal == 1'b0) begin
					decoded_se = 32'd4;
					decoded_valid = ~stall;
				end else begin
					case (FL_decoded_se) 
						4: decoded_se = 32'd0;
						5: decoded_se = 32'd1;
						6: decoded_se = 32'd2;
						7: decoded_se = 32'd3;
						default: decoded_se = 32'dx;
					endcase
					decoded_valid = FL_decoded_valid;
				end
			end
			21: begin //merge_flag
				cMax = 6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end	
			22: begin //merge_idx
				cMax = se_param[25:23] - 3'd1;
				decoded_se = TU_decoded_se;
				decoded_valid = TU_decoded_valid;
			end
			23: begin // inter_pred_idc
				cMax = 6'dx;
				if (bin_no == 6'd0) begin
					if (se_param[25:18] + se_param[17:10] != 12) begin
						decoded_se = (binVal==1'b1) ? 32'd2 : 32'dx;
						decoded_valid = (binVal==1'b1) ? ~stall : 1'b0;
					end else begin
						decoded_se = binVal;
						decoded_valid = ~stall;
					end
				end else begin 
					decoded_se = binVal;
					decoded_valid = ~stall;
				end
			end
			24,25: begin // ref_idx
				cMax = se_param[23:18];
				decoded_se = TU_decoded_se;
				decoded_valid = TU_decoded_valid;
			end
			26,27: begin //abs_mvd_greater_0/1
				cMax = 6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			28 : begin // abs_mvd_minus2
				cMax = 6'dx;
				decoded_se = mvd_decoded_se;
				decoded_valid = mvd_decoded_valid;
			end
			29: begin //mvd_sign_flag
				cMax = 6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			30: begin //mvp_lx_flag
				cMax = 6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			31: begin //rqt_root_cbf
				cMax = 6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			32: begin //split_transform_flag
				cMax = 6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			33,34,35: begin //cbf_luma ,cbf_cx, transform_skip
				cMax = 6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			36,37: begin //last_signficant_coef_x_prefix
				case (log2TrafoSize)
					2: cMax = 6'd3;
					3: cMax = 6'd5;
					4: cMax = 6'd7;
					5: cMax = 6'd9;
					default: cMax = 6'dx;
				endcase
				decoded_se = TU_decoded_se;
				decoded_valid = TU_decoded_valid;
			end
			38, 39: begin //last_significat_coef_x/y_suffix
				cMax = (se_param[25:22]>>1)-6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;				
			end
			40,41,42,43 : begin //coded_sub_block_flag, significant coeff flag, coeff_abs_level_greater1/2_flag
				cMax = 6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			44 : begin //coeff_abs_level_remaining
				cMax = 6'dx;
				decoded_se = rem_decoded_se;
				decoded_valid = rem_decoded_valid;
			end
			45 : begin //coeff_sign_flag
				cMax = 6'd1;
				decoded_se = FL_decoded_se;
				decoded_valid = FL_decoded_valid;
			end
			default : begin
				cMax = 6'dx;
				decoded_valid = 1'b0;
				decoded_se = 32'dx;
			end

		endcase
		
	end
	
	always @(posedge clk) begin
		
			part_mode_bin2 <= (bin_no==6'd1) ? binVal : part_mode_bin2;
		
		
	end

endmodule
