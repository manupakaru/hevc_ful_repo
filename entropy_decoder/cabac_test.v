`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:10:34 11/05/2013
// Design Name:   cabac
// Module Name:   C:/Users/Maleen/Uni/HEVC/EntropyDecoder/cabac_test.v
// Project Name:  EntropyDecoder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: cabac
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module cabac_test;

	// Inputs
	reg clk;
	reg rst;
	reg [31:0] se_type;
	reg se_empty;
	reg [1:0] op;
	reg [1:0] cabac_init_type;
	reg [5:0] QP;
	reg [7:0] next_bits;
	reg next_bits_valid;

	// Outputs
	wire se_read;
	wire [31:0] decoded_se;
	wire decoded_valid;
	wire [2:0] read_bits;
	wire read_valid;

	// Instantiate the Unit Under Test (UUT)
	cabac uut (
		.clk(clk), 
		.rst(rst), 
		.se_type(se_type), 
		.se_empty(se_empty), 
		.se_read(se_read), 
		.op(op), 
		.cabac_init_type(cabac_init_type), 
		.QP(QP), 
		.decoded_se(decoded_se), 
		.decoded_valid(decoded_valid), 
		.next_bits(next_bits), 
		.next_bits_valid(next_bits_valid), 
		.read_bits(read_bits), 
		.read_valid(read_valid)
	);

	initial begin
		// Initialize Inputs
		clk=0;
		rst = 0; #10 rst=1;
		
		se_type = 0;
		se_empty = 0;
		op = 0;
		cabac_init_type = 0;
		QP = 32;
		next_bits = 0;
		next_bits_valid = 0;

		// Wait 100 ns for global reset to finish
		#90;
        
		// Add stimulus here
		rst = 1;
		op = 1;
		#10; 
		
	end
  
	always begin
		#5 clk = ~clk;
	end
endmodule

