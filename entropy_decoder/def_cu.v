`ifndef SLICE_I
    `define SLICE_I 2'b10
`endif
`ifndef SLICE_B
    `define SLICE_B 2'b00
`endif


`ifndef MODE_INTRA
    `define MODE_INTRA 2'b00
`endif

`ifndef PART_2Nx2N
    `define PART_2Nx2N 3'b000
`endif
`ifndef PART_2NxN
    `define PART_2NxN 3'b001
`endif
`ifndef PART_Nx2N
    `define PART_Nx2N 3'b010
`endif
`ifndef PART_NxN
    `define PART_NxN 3'b011
`endif
`ifndef PART_2NxnU
    `define PART_2NxnU 3'b100
`endif
`ifndef PART_2NxnD
    `define PART_2NxnD 3'b101
`endif
`ifndef PART_nLx2N
    `define PART_nLx2N 3'b110
`endif
`ifndef PART_nRx2N
    `define PART_nRx2N 3'b111
`endif

`ifndef PRED_L0
    `define PRED_L0 2'b00
`endif
`ifndef PRED_L1
    `define PRED_L1 2'b01
`endif
`ifndef PRED_BI
    `define PRED_BI 2'b10
`endif


//`define MAX_WIDTH_CU 240
`define LINE_BUF_CU 511


`define STATE_CU_INIT 7'd0

`define STATE_CU_PREDMODE_PUSH 7'd1
`define STATE_CU_PREDMODE_READ 7'd2
`define STATE_CU_PARTMODE_PUSH 7'd3
`define STATE_CU_PARTMODE_READ 7'd4
`define STATE_CU_SENDH30 7'd5
`define STATE_CU_PREV_PUSH 7'd6
`define STATE_CU_PREV_READ 7'd7
`define STATE_CU_MPM_PUSH 7'd8
`define STATE_CU_MPM_READ 7'd9
// 
`define STATE_CU_DERIVE_1 7'd10 
`define STATE_CU_DERIVE_2 7'd11
`define STATE_CU_DERIVE_3 7'd12
`define STATE_CU_DERIVE_POST 7'd13
`define STATE_CU_DERIVE_WRITE 7'd14

`define STATE_CU_CHROMA_PUSH 7'd15
`define STATE_CU_CHROMA_READ 7'd16

`define STATE_CU_DERIVE_SORT 7'd17
`define STATE_CU_DERIVE_REM 7'd18

`define STATE_CU_RQT_PUSH 7'd19
`define STATE_CU_RQT_READ 7'd20
`define STATE_CU_TRANS_TREE_CALL 7'd21
`define STATE_CU_RQT_FALSE_H40Y 7'd22 
`define STATE_CU_RQT_FALSE_H41 7'd23 
`define STATE_CU_RQT_FALSE_H40CB 7'd24 
`define STATE_CU_RQT_FALSE_H40CR 7'd25 

`define STATE_TT_SPLIT_PUSH 7'd30
`define STATE_TT_SPLIT_READ 7'd31
`define STATE_TT_CBFCB_PUSH 7'd32
`define STATE_TT_CBFCR_PUSH 7'd33
`define STATE_TT_CBFCB_READ 7'd34
`define STATE_TT_CBFCR_READ 7'd35
`define STATE_TT_QT_0 7'd36
`define STATE_TT_QT_1 7'd37
`define STATE_TT_QT_2 7'd38
`define STATE_TT_QT_3 7'd39
`define STATE_TT_CBFY_PUSH 7'd40
`define STATE_TT_CBFY_READ 7'd41
`define STATE_TT_END 7'd42
//`define STATE_TT_CALL_TU 7'd42

`define STATE_TU_CALL_RUY 7'd45
`define STATE_TU_CALL_RUCB1 7'd46
`define STATE_TU_CALL_RUCR1 7'd47
`define STATE_TU_CALL_RUCB2 7'd48
`define STATE_TU_CALL_RUCR2 7'd49
`define STATE_TU_END 7'd50

`define STATE_RU_SKIP_PUSH 7'd55
`define STATE_RU_SKIP_READ 7'd56
`define STATE_RU_CALL_ACCEL 7'd57
`define STATE_RU_WAIT 7'd58

`define STATE_PUSH_H40 7'd60
`define STATE_PUSH_H41 7'd61

`define STATE_CU_END 7'd62
`define STATE_CU_EMPTY 7'd63

`define STATE_CQT_AVAILABLEN 7'd64
`define STATE_CQT_SPLIT_PUSH 7'd65
`define STATE_CQT_SPLIT_READ 7'd66
`define STATE_CQT_UPDATE_BUF 7'd67
`define STATE_CQT_QT_0 7'd68 // also calls coding unit
`define STATE_CQT_QT_1 7'd69
`define STATE_CQT_QT_2 7'd70
`define STATE_CQT_QT_3 7'd71

`define STATE_CQT_END 7'd72
`define STATE_CTU_END 7'd73

// WPP Row
`define STATE_SSD_BYTE_ALIGNMENT_IF 7'd75
`define STATE_SSD_LOAD_INIT_CABAC 7'd76
`define STATE_SSD_INIT_ENGINE 7'd77
`define STATE_SSD_INIT_ENGINE_DONE 7'd78

`define STATE_SSD_TS_TO_RS_CONV 7'd80
`define STATE_SSD_BEGIN 7'd81
`define STATE_SSD_SEND_H16 7'd82 // moved to ublaze
`define STATE_SSD_SEND_H17 7'd83 // moved to ublaze
`define STATE_SSD_CTU_BEGIN 7'd84
`define STATE_SSD_CTU_XY_READ 7'd85
`define STATE_SSD_SEND_H20 7'd86
`define STATE_SSD_SEND_H21 7'd87
`define STATE_CTU_START_SAO 7'd88
`define STATE_CTU_SAO_WAIT 7'd89
`define STATE_CTU_CALL_CQT 7'd90
`define STATE_SSD_END_OF_SLICE_SEGMENT_FLAG_PUSH 7'd91
`define STATE_SSD_END_OF_SLICE_SEGMENT_FLAG_READ 7'd92
`define STATE_SSD_END 7'd93
`define STATE_SSD_SAVE_CABAC 7'd94

`define STATE_CU_TRANSQUANT_BYPASS_PUSH 7'd95
`define STATE_CU_TRANSQUANT_BYPASS_READ 7'd96
`define STATE_CU_SKIP_PUSH 7'd97
`define STATE_CU_SKIP_READ 7'd98
`define STATE_CU_SKIP_UPDATE 7'd99
`define STATE_CU_PREDMODE_UPDATE 7'd100

`define STATE_TU_QP_ABS_PUSH 7'd105
`define STATE_TU_QP_ABS_READ 7'd106 // and sign push
`define STATE_TU_QP_SIGN_READ 7'd107
`define STATE_TU_QP_UPDATE 7'd108

`define STATE_CU_PU_1 7'd110
`define STATE_CU_PU_2 7'd111
`define STATE_CU_PU_3 7'd112
`define STATE_CU_PU_4 7'd113
`define STATE_CU_PU_WAIT 7'd114

`define STATE_CU_PCM_FLAG_PUSH 7'd115
`define STATE_CU_PCM_FLAG_READ 7'd116
`define STATE_CU_PCM_BYTE_ALIGN 7'd117
`define STATE_CU_PCM_READ 7'd118
`define STATE_CU_PCM_INIT_CABAC 7'd119
