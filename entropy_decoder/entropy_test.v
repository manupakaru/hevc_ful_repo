`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:43:23 11/05/2013
// Design Name:   entropy
// Module Name:   C:/Users/Maleen/Uni/HEVC/EntropyDecoder/entropy_test.v
// Project Name:  EntropyDecoder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: entropy
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module entropy_test;

	// Inputs
	reg clk;
	reg rst;
	reg [31:0] read_in;
	reg read_empty;
	reg skip_to_NAL;
	reg [31:0] test_se_type;
	reg test_se_empty;
	reg [1:0] test_op;
	reg [1:0] test_cabac_init_type;
	reg [6:0] test_QP;

	// Outputs
	wire read_next;
	wire [31:0] write_out;
	wire write_valid;
	wire test_se_read;
	wire [31:0] test_decoded_se;
	wire test_decoded_valid;

	integer   fd, fdc, fd_res;
   integer   line;

	reg [31:0] cabac_in;
	reg [31:0] cabac_res_in;
	
	reg match_fail;
	reg[31:0] CTU_no;
	
	// Instantiate the Unit Under Test (UUT)
	entropy uut (
		.clk(clk), 
		.rst(rst), 
		.read_in(read_in), 
		.read_empty(read_empty), 
		.read_next(read_next), 
		.skip_to_NAL(skip_to_NAL),
		.write_out(write_out), 
		.write_valid(write_valid),  
		.test_se_type(test_se_type), 
		.test_se_empty(test_se_empty), 
		.test_se_read(test_se_read), 
		.test_decoded_se(test_decoded_se), 
		.test_decoded_valid(test_decoded_valid), 
		.test_op(test_op), 
		.test_cabac_init_type(test_cabac_init_type), 
		.test_QP(test_QP)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0; #10 rst=1;
		line = -4;
		fd = $fopen("BQSquare_416x240_60_qp37_skipped_to_cabac.bin","rb"); 
		fdc = $fopen("BQSquare_416x240_60_qp37_cabac.bin","rb"); 
		fd_res = $fopen("BQSquare_416x240_60_qp37_cabac_res.bin","rb"); 
//		
		//fd = $fopen("BQSquare_416x240_60_qp37_skipped_to_cabac.bin","rb"); 
		//fdc = $fopen("BQSquare_416x240_60_qp37_cabac.bin","rb"); 
		//fd_res = $fopen("BQSquare_416x240_60_qp37_cabac_res_POC_8.bin","rb"); 
		
		read_in = 0;
		read_empty = 1;
		skip_to_NAL = 0;
		
		test_se_type = 0;
		test_se_empty = 1;
		test_op = 1;
		test_cabac_init_type = 0;
	//	test_cabac_init_type = 2;
		test_QP = 37;
	//	test_QP = 38;
		
		match_fail = 0;
		CTU_no = 0;
		// Wait 100 ns for global reset to finish
		#90;
        
		// Add stimulus here
		read_empty = 0;
		read_in[31:24] = $fgetc(fd);
		read_in[23:16] = $fgetc(fd);
		read_in[15: 8] = $fgetc(fd);
		read_in[ 7: 0] = $fgetc(fd);	
		line = line+4;

		cabac_in[ 7: 0] = $fgetc(fdc);
		cabac_in[15: 8] = $fgetc(fdc);
		cabac_in[23:16] = $fgetc(fdc);
		cabac_in[31:24] = $fgetc(fdc);
		print(cabac_in);
		test_se_type = {cabac_in[29:0] , 2'b00};
		
		test_se_empty = 0;
		skip_to_NAL = 1; #10; skip_to_NAL = 0;

		#5000;
		//$fclose(fd);
		//$finish;
		
	end
	
	always 
      #5 clk=~clk;
		
	always @ (posedge clk) begin
		if (read_next) begin
			read_in[31:24] = $fgetc(fd);
			read_in[23:16] = $fgetc(fd);
			read_in[15: 8] = $fgetc(fd);
			read_in[ 7: 0] = $fgetc(fd);			
			
			//$display("data  = %h", data);
			
			line = line +4;
		end
	end
	
	always @ (posedge clk) begin
		if (test_se_read) begin
			cabac_in[ 7: 0] = $fgetc(fdc);
			cabac_in[15: 8] = $fgetc(fdc);
			cabac_in[23:16] = $fgetc(fdc);
			cabac_in[31:24] = $fgetc(fdc);
			print(cabac_in);
			while(cabac_in[31:30] != 2'b00) begin
				cabac_in[ 7: 0] = $fgetc(fdc);
				cabac_in[15: 8] = $fgetc(fdc);
				cabac_in[23:16] = $fgetc(fdc);
				cabac_in[31:24] = $fgetc(fdc);
				print(cabac_in);
			end
			test_se_type = {cabac_in[29:0] , 2'b00};
			//test_se_empty = 1; 
			test_se_empty = 0;
		end
	end	
	
	always @ (negedge clk) begin
		if (test_decoded_valid) begin
			cabac_res_in[ 7: 0] = $fgetc(fd_res);
			cabac_res_in[15: 8] = $fgetc(fd_res);
			cabac_res_in[23:16] = $fgetc(fd_res);
			cabac_res_in[31:24] = $fgetc(fd_res);
			if (cabac_res_in[23:0] == test_decoded_se[23:0]) begin
				$display("   SUCCESS decoded se %d  (%d %d)  ", cabac_res_in[31:24], cabac_res_in[7:0], test_decoded_se);
				match_fail = 0;
				if (cabac_res_in[31:24] == 7) 
					CTU_no = CTU_no+1;
			end else begin
				$display("   FAIL decoded se %d  (%d %d)  ", cabac_res_in[31:24], cabac_res_in[7:0], test_decoded_se);
				match_fail = 1;
				//$finish();
			end
		end
	
	end
	
	
	task print;
		input [31:0] cabac_in;
		if (cabac_in[31] == 1'b0) begin
			$display ("se type %d", cabac_in[29:24]);
		end else begin
			$display ("offset %d range %d pStateIdx %d valMPS %d ctxInc %d", cabac_in[30:22], cabac_in[21:13], cabac_in[12:7], cabac_in[6], cabac_in[5:0]);
//		end else begin
//			$display ("decoded_se %d", cabac_in[29:0]);
		end
	endtask
endmodule

