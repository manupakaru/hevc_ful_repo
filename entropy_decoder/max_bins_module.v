`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:26:31 11/06/2013 
// Design Name: 
// Module Name:    max_bins_module 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module max_bins_module(
    input [5:0] se_type,
    output reg [5:0] max_bins
    );
	
	always @ (*) begin
		case (se_type) 
			1 : max_bins = 6'd0;
			2 : max_bins = 6'd1;
			3 : max_bins = 6'd4;
			4 : max_bins = 6'd6;
			5 : max_bins = 6'd0;
			6 : max_bins = 6'd1;
			7 : max_bins = 6'd0;
			8 : max_bins = 6'd0;
			9 : max_bins = 6'd0;
			10 : max_bins = 6'd0;
			11 : max_bins = 6'd0;
			12 : max_bins = 6'd9;
			13 : max_bins = 6'd0;
			14 : max_bins = 6'd0;
			15 : max_bins = 6'd3;
			16 : max_bins = 6'd0;
			17 : max_bins = 6'd0;
			18 : max_bins = 6'd1;
			19 : max_bins = 6'd4;
			20 : max_bins = 6'd2;
			21 : max_bins = 6'd0;
			22 : max_bins = 6'd3;
			23 : max_bins = 6'd1;
			24 : max_bins = 6'd13;
			25 : max_bins = 6'd13;
			26 : max_bins = 6'd0;
			27 : max_bins = 6'd0;
			28 : max_bins = 6'd63;
			29 : max_bins = 6'd0;
			30 : max_bins = 6'd0;
			31 : max_bins = 6'd0;
			32 : max_bins = 6'd0;
			33 : max_bins = 6'd0;
			34 : max_bins = 6'd0;
			35 : max_bins = 6'd0;
			36 : max_bins = 6'd8;
			37 : max_bins = 6'd8;
			38 : max_bins = 6'd3;
			39 : max_bins = 6'd3;
			40 : max_bins = 6'd0;
			41 : max_bins = 6'd0;
			42 : max_bins = 6'd0;
			43 : max_bins = 6'd0;
			44 : max_bins = 6'd63;
			45 : max_bins = 6'd0;

			default: max_bins = 6'd63;
		endcase
	end

endmodule
