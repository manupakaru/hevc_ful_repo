`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:38:17 03/31/2014 
// Design Name: 
// Module Name:    saved_ctx 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module saved_ctx(
    input clk,
    input wea,
    input [7:0] addra,
    input [6:0] dina,
    output reg [6:0] douta
    );

		reg [6:0] ctx_table [0:255];
		
		always @(posedge clk) begin
			if (wea) begin
				ctx_table[addra] <= dina;
			end 
			douta <= ctx_table[addra];
		end
endmodule
