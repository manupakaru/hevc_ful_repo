`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:39:40 07/29/2013 
// Design Name: 
// Module Name:    main 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module cabac(
	 input clk,
	 input rst,

    input [31:0] se_type,
    input se_empty,
	 output se_read,
	 
	 //input pipe_reset, // initialize decoding_engine
	 
    input [1:0] op, // save, load, init engine, init_context
	 input op_valid,
	 output reg op_done, 
	 
	 input [1:0] cabac_init_type,
    input [6:0] QP,

	 output [31:0] decoded_se,
    output decoded_valid,

	 input [7:0] next_bits,
	 input next_bits_valid,
	 output [3:0] read_bits,
	 output read_valid
    );

	localparam CABAC_SAVE_CTX = 2'b00;
	localparam CABAC_LOAD_CTX = 2'b01;
	localparam CABAC_INIT_ENGINE = 2'b10;
	localparam CABAC_INIT_CTX = 2'b11;
	
	localparam STATE_INIT = 1'b1;
	localparam STATE_NORMAL = 1'b0;

	reg [6:0] FSM_data;
	
	reg [8:0] initIndex; // depends on current_ctx and cabac_init_type

	
	wire [6:0] FSM_write_data; // from cabac_init
	wire [6:0] update_write_data; // from context update pipe stage
	wire [6:0] load_write_data;
	reg [6:0] ctx_write_data;
	
	wire [7:0] FSM_write_addr;
	wire [7:0] update_write_addr;
	reg [7:0] load_write_addr;
	reg [7:0] ctx_write_addr;
	
	wire [7:0] pipe_read_addr; // input from Ctx Load Pipe Stage
	reg [7:0] pipe_read_addr_D1;
	reg [7:0] save_read_addr; // same as current_ctx
	reg [7:0] ctx_read_addr;
	reg [6:0] ctx_read_data; // also output to pipe stage
	
	//remove
	
	
	wire update_wea;
	
	reg ctx_wea;
	reg save_wea;
	
	reg [7:0] current_ctx; // upto 152
	
	reg [7:0] save_ctx_addr;
	
	wire read_next;
	wire fifo_empty;
	wire fifo_full;
	wire [31:0] in_fifo_dout;
	wire [31:0]ctx_sel_in;
	
	wire binVal;

	reg init_state; //0- normal, 1-initializing
	//assign ctx_sel_in = se_empty ? 32'b0 : se_type;
	
	wire stall;
	wire [31:10] se_type_D1;
	wire [5:0] bin_no_D1;
	//wire take_next_se;
	
	wire bypass, terminate;
	reg bypass_D1, terminate_D1;
	
	always @(posedge clk) begin
		pipe_read_addr_D1 <= pipe_read_addr;
		bypass_D1 <= bypass;
		terminate_D1 <= terminate;
	end
		
	
	wire sel_rst;
	assign sel_rst = (rst & (init_state==1'b0));
	
	ctx_sel ctx_sel(
		 .se_type(se_type[31:26]),
		 .se_param(se_type[25:0]),
		 .se_empty(se_empty),
		 .read_next(se_read),
		 .clk(clk),
		 .rst(sel_rst),
		 .take_next_se(decoded_valid), // set by 2nd stage
		 .se_type_D1(se_type_D1), // goes to bin_match

		 //.bits_valid(next_bits_valid),
		 .bits_valid(1'b1),
		 .bin_no_D1(bin_no_D1), // goes  to bin match
		 .ctx_addr(pipe_read_addr),
		 .stall(stall),
		 .bypass(bypass),
		 .terminate(terminate)
    );

	//assign decoded_valid = fifo_full;
	reg [6:0] ctx_read_data_D1;
	always @(posedge clk) begin
		ctx_read_data_D1 <= ctx_read_data;
	end
	
	reg init_engine;
	wire init_done;
	
	
	decode_bin decode_bin(
		.clk(clk),
		.rst(rst),
		.next_bits(next_bits),
		.next_valid(next_bits_valid),
		.read_bits(read_bits),
		.read_valid(read_valid),
		
		.init_engine(init_engine),
		.init_done(init_done),
		
		.stall(stall),
		
		.read_ctx_data(ctx_read_data_D1), 
		.write_ctx_data(update_write_data),
		.read_ctx_addr(pipe_read_addr_D1), // (511- bypass) // delay by 1 and pass
		.write_ctx_addr(update_write_addr),
		.write_wea(update_wea),
		.bin(binVal),
		.bypass(bypass_D1),
		.terminate(terminate_D1)
    );
	
	binarize binarize(
		.clk(clk),
		.se_type(se_type_D1[31:26]),
		.se_param(se_type_D1[25:10]),
		.bin_no(bin_no_D1),
		.stall(stall),
		.binVal(binVal),
		//.take_next_se(decoded_valid), // redundant
		.decoded_se(decoded_se),
		.decoded_valid(decoded_valid)
	
	);

	(* ram_style = "distributed" *) 
	reg [6:0] ctx_table [0:255];
	
	always @(posedge clk) begin
		if (ctx_wea) begin
			ctx_table[ctx_write_addr] <= ctx_write_data;
		end
		//ctx_read_data <= ctx_table[ctx_read_addr];
	end
	always @(*) begin
		if (~terminate) 
			ctx_read_data = (ctx_write_addr==ctx_read_addr && ctx_wea) ? ctx_write_data : ctx_table[ctx_read_addr];
		else 
			ctx_read_data = 7'd63;
	end

	cabac_init_logic cabac_init_logic(
		.initIndex(initIndex),
		.QP(QP),
		.in_ctx(current_ctx),
		.clk(clk),
		.out(FSM_write_data),
		.out_ctx(FSM_write_addr)
	);
	
	saved_ctx ctx_saved_d (
	  .clk(clk), // input clka
	  .wea(save_wea), // input [0 : 0] wea
	  .addra(current_ctx), // input [7 : 0] addra
	  .dina(ctx_read_data), // input [6 : 0] dina
	  .douta(load_write_data) // output [6 : 0] douta
	);
	
	// always read_addr = current_ctx
	// for saving, since memory is distributed, write_addr = current_ctx
	// for load, one cycle delay

	always @ (posedge clk) begin  
		if (~rst) begin
			current_ctx <= 0;
			init_state <= STATE_NORMAL;
		end else begin
			if (init_state==STATE_NORMAL) begin
				if (op_valid) begin
					init_state <= STATE_INIT;
					current_ctx <= 0;
					save_ctx_addr <=0;
				end
			end else begin
				case (op)
					CABAC_SAVE_CTX: begin 
						if (current_ctx == 8'd153) begin
							init_state <= STATE_NORMAL;
							current_ctx <= 8'd0;
						end else begin
							current_ctx <= current_ctx+1'b1;
						end
					end
					CABAC_LOAD_CTX: begin // load saved state
						save_ctx_addr <= save_ctx_addr + 1'b1;
						if (current_ctx == 8'd154) begin
							init_state <= STATE_NORMAL;
							current_ctx <= 8'd0;
						end else begin
							current_ctx <= current_ctx+1'b1;
						end
					end
					CABAC_INIT_ENGINE: begin
						if (init_done) begin
							init_state <= STATE_NORMAL;
						end
					end
					CABAC_INIT_CTX: begin 
						if (FSM_write_addr == 8'd154) begin
							init_state <= STATE_NORMAL;
							current_ctx <= 8'd0;
						end else begin
							current_ctx <= current_ctx+1'b1;
						end
					end		
				endcase 
			end
		end
	end
	
	always @(*) begin
		init_engine = 1'b0;
		ctx_write_data = 7'dx;
		ctx_write_addr = 8'dx;
		ctx_read_addr = 8'dx;
		ctx_wea = 1'b0;
		save_wea = 1'b0;
		op_done = 1'b0;
		if (init_state==STATE_NORMAL) begin
			ctx_write_data = update_write_data;
			ctx_write_addr = update_write_addr;
			ctx_read_addr = pipe_read_addr;
			ctx_wea = update_wea;
			save_wea = 0;
			if (op_valid && op==CABAC_INIT_ENGINE) begin
				init_engine =1'b1;
			end
		end else begin
			case (op)
				CABAC_SAVE_CTX: begin 
					ctx_read_addr = current_ctx;
					save_wea = 1'b1;
					if (current_ctx == 8'd153) begin
						op_done = 1'b1;
					end
				end
				CABAC_LOAD_CTX: begin // load saved state
					ctx_write_data = load_write_data;
					ctx_write_addr = current_ctx - 1'b1;
					ctx_wea = 1'b1;
					if (current_ctx == 8'd154) begin
						op_done = 1'b1;
					end
				end
				CABAC_INIT_ENGINE: begin
					if (init_done) begin
						op_done = 1'b1;
					end
				end
				CABAC_INIT_CTX: begin 
					ctx_write_data = FSM_write_data;
					ctx_write_addr = FSM_write_addr;
					ctx_read_addr = pipe_read_addr;
					ctx_wea = 1'b1; // also 1 in the intial two cycles of latency. But doesnt matter since it will get replaced eventually
					if (FSM_write_addr == 8'd154) begin
						op_done = 1'b1;
					end
				end
			endcase 
		end
	end
	
	//Context Initialize Logic
	always @ (*) begin
		initIndex = current_ctx + 8'd154*cabac_init_type[1:0];
	end

endmodule
