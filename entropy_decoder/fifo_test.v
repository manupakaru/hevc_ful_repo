`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   21:05:32 08/17/2013
// Design Name:   fifo
// Module Name:   C:/Users/Maleen/Uni/HEVC/EntropyDecoder/fifo_test.v
// Project Name:  EntropyDecoder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: fifo
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module fifo_test;

	// Inputs
	reg clk;
	reg rst;
	reg [31:0] din;
	reg wr_en;
	reg rd_en;

	// Outputs
	wire [31:0] dout;
	wire full;
	wire empty;

	// Instantiate the Unit Under Test (UUT)
	fifo uut (
		.clk(clk), 
		.rst(rst), 
		.din(din), 
		.wr_en(wr_en), 
		.rd_en(rd_en), 
		.dout(dout), 
		.full(full), 
		.empty(empty)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0;
		din = 0;
		wr_en = 0;
		rd_en = 0;

		// Wait 100 ns for global reset to finish
		#100;
      #6;  
		// Add stimulus here
		din = 50;
		wr_en = 1; #10
		wr_en = 1; #30
		rd_en = 1; #5 rd_en = 0;
	end
	always begin
		#5 clk = ~clk;
	end
      
endmodule

