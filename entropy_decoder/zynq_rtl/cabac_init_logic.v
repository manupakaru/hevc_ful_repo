`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:54:37 07/31/2013 
// Design Name: 
// Module Name:    cabac_init_logic 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module cabac_init_logic(
    input [8:0] initIndex,
    input signed [6:0] QP,
	 input [7:0] in_ctx,
	 input clk,
    output reg [6:0] out,
	 output reg[7:0] out_ctx
    );
	 
	reg [7:0] initValue;
	reg [7:0] initValue_D1;
	reg [3:0] initValue_D2, initValue_D3;
	
	reg [7:0] ctx_D1;
	reg [7:0] ctx_D2, ctx_D3;
//	always @ (*) begin
//		out = initValue[6:0];
//	end 
	reg signed [11:0] preStateCtx;
	reg signed [7:0] preStateCtx_shift;
	reg signed [7:0] preStateCtx_plusn;
	reg [6:0] preStateCtx_clipped;
	always @ (*) begin
		case (initIndex)
			0: begin initValue =  153; end
			1: begin initValue =  200; end
			2: begin initValue =  139; end
			3: begin initValue =  141; end
			4: begin initValue =  157; end
			5: begin initValue =  154; end
			6: begin initValue =  0; end
			7: begin initValue =  0; end
			8: begin initValue =  0; end
			9: begin initValue =  154; end
			10: begin initValue =  154; end
			11: begin initValue =  0; end
			12: begin initValue =  184; end
			13: begin initValue =  0; end
			14: begin initValue =  0; end
			15: begin initValue =  0; end
			16: begin initValue =  184; end
			17: begin initValue =  63; end
			18: begin initValue =  0; end
			19: begin initValue =  0; end
			20: begin initValue =  0; end
			21: begin initValue =  0; end
			22: begin initValue =  0; end
			23: begin initValue =  0; end
			24: begin initValue =  0; end
			25: begin initValue =  0; end
			26: begin initValue =  0; end
			27: begin initValue =  0; end
			28: begin initValue =  0; end
			29: begin initValue =  0; end
			30: begin initValue =  0; end
			31: begin initValue =  153; end
			32: begin initValue =  138; end
			33: begin initValue =  138; end
			34: begin initValue =  111; end
			35: begin initValue =  141; end
			36: begin initValue =  94; end
			37: begin initValue =  138; end
			38: begin initValue =  182; end
			39: begin initValue =  154; end
			40: begin initValue =  139; end
			41: begin initValue =  139; end
			42: begin initValue =  110; end
			43: begin initValue =  110; end
			44: begin initValue =  124; end
			45: begin initValue =  125; end
			46: begin initValue =  140; end
			47: begin initValue =  153; end
			48: begin initValue =  125; end
			49: begin initValue =  127; end
			50: begin initValue =  140; end
			51: begin initValue =  109; end
			52: begin initValue =  111; end
			53: begin initValue =  143; end
			54: begin initValue =  127; end
			55: begin initValue =  111; end
			56: begin initValue =  79; end
			57: begin initValue =  108; end
			58: begin initValue =  123; end
			59: begin initValue =  63; end
			60: begin initValue =  110; end
			61: begin initValue =  110; end
			62: begin initValue =  124; end
			63: begin initValue =  125; end
			64: begin initValue =  140; end
			65: begin initValue =  153; end
			66: begin initValue =  125; end
			67: begin initValue =  127; end
			68: begin initValue =  140; end
			69: begin initValue =  109; end
			70: begin initValue =  111; end
			71: begin initValue =  143; end
			72: begin initValue =  127; end
			73: begin initValue =  111; end
			74: begin initValue =  79; end
			75: begin initValue =  108; end
			76: begin initValue =  123; end
			77: begin initValue =  63; end
			78: begin initValue =  91; end
			79: begin initValue =  171; end
			80: begin initValue =  134; end
			81: begin initValue =  141; end
			82: begin initValue =  111; end
			83: begin initValue =  111; end
			84: begin initValue =  125; end
			85: begin initValue =  110; end
			86: begin initValue =  110; end
			87: begin initValue =  94; end
			88: begin initValue =  124; end
			89: begin initValue =  108; end
			90: begin initValue =  124; end
			91: begin initValue =  107; end
			92: begin initValue =  125; end
			93: begin initValue =  141; end
			94: begin initValue =  179; end
			95: begin initValue =  153; end
			96: begin initValue =  125; end
			97: begin initValue =  107; end
			98: begin initValue =  125; end
			99: begin initValue =  141; end
			100: begin initValue =  179; end
			101: begin initValue =  153; end
			102: begin initValue =  125; end
			103: begin initValue =  107; end
			104: begin initValue =  125; end
			105: begin initValue =  141; end
			106: begin initValue =  179; end
			107: begin initValue =  153; end
			108: begin initValue =  125; end
			109: begin initValue =  140; end
			110: begin initValue =  139; end
			111: begin initValue =  182; end
			112: begin initValue =  182; end
			113: begin initValue =  152; end
			114: begin initValue =  136; end
			115: begin initValue =  152; end
			116: begin initValue =  136; end
			117: begin initValue =  153; end
			118: begin initValue =  136; end
			119: begin initValue =  139; end
			120: begin initValue =  111; end
			121: begin initValue =  136; end
			122: begin initValue =  139; end
			123: begin initValue =  111; end
			124: begin initValue =  140; end
			125: begin initValue =  92; end
			126: begin initValue =  137; end
			127: begin initValue =  138; end
			128: begin initValue =  140; end
			129: begin initValue =  152; end
			130: begin initValue =  138; end
			131: begin initValue =  139; end
			132: begin initValue =  153; end
			133: begin initValue =  74; end
			134: begin initValue =  149; end
			135: begin initValue =  92; end
			136: begin initValue =  139; end
			137: begin initValue =  107; end
			138: begin initValue =  122; end
			139: begin initValue =  152; end
			140: begin initValue =  140; end
			141: begin initValue =  179; end
			142: begin initValue =  166; end
			143: begin initValue =  182; end
			144: begin initValue =  140; end
			145: begin initValue =  227; end
			146: begin initValue =  122; end
			147: begin initValue =  197; end
			148: begin initValue =  138; end
			149: begin initValue =  153; end
			150: begin initValue =  136; end
			151: begin initValue =  167; end
			152: begin initValue =  152; end
			153: begin initValue =  152; end
			154: begin initValue =  153; end
			155: begin initValue =  185; end
			156: begin initValue =  107; end
			157: begin initValue =  139; end
			158: begin initValue =  126; end
			159: begin initValue =  154; end
			160: begin initValue =  197; end
			161: begin initValue =  185; end
			162: begin initValue =  201; end
			163: begin initValue =  154; end
			164: begin initValue =  154; end
			165: begin initValue =  149; end
			166: begin initValue =  154; end
			167: begin initValue =  139; end
			168: begin initValue =  154; end
			169: begin initValue =  154; end
			170: begin initValue =  154; end
			171: begin initValue =  152; end
			172: begin initValue =  110; end
			173: begin initValue =  122; end
			174: begin initValue =  95; end
			175: begin initValue =  79; end
			176: begin initValue =  63; end
			177: begin initValue =  31; end
			178: begin initValue =  31; end
			179: begin initValue =  153; end
			180: begin initValue =  153; end
			181: begin initValue =  140; end
			182: begin initValue =  198; end //182: begin initValue =  169; end
			183: begin initValue =  168; end
			184: begin initValue =  79; end
			185: begin initValue =  124; end
			186: begin initValue =  138; end
			187: begin initValue =  94; end
			188: begin initValue =  153; end
			189: begin initValue =  111; end
			190: begin initValue =  149; end
			191: begin initValue =  107; end
			192: begin initValue =  167; end
			193: begin initValue =  154; end
			194: begin initValue =  139; end
			195: begin initValue =  139; end
			196: begin initValue =  125; end
			197: begin initValue =  110; end
			198: begin initValue =  94; end
			199: begin initValue =  110; end
			200: begin initValue =  95; end
			201: begin initValue =  79; end
			202: begin initValue =  125; end
			203: begin initValue =  111; end
			204: begin initValue =  110; end
			205: begin initValue =  78; end
			206: begin initValue =  110; end
			207: begin initValue =  111; end
			208: begin initValue =  111; end
			209: begin initValue =  95; end
			210: begin initValue =  94; end
			211: begin initValue =  108; end
			212: begin initValue =  123; end
			213: begin initValue =  108; end
			214: begin initValue =  125; end
			215: begin initValue =  110; end
			216: begin initValue =  94; end
			217: begin initValue =  110; end
			218: begin initValue =  95; end
			219: begin initValue =  79; end
			220: begin initValue =  125; end
			221: begin initValue =  111; end
			222: begin initValue =  110; end
			223: begin initValue =  78; end
			224: begin initValue =  110; end
			225: begin initValue =  111; end
			226: begin initValue =  111; end
			227: begin initValue =  95; end
			228: begin initValue =  94; end
			229: begin initValue =  108; end
			230: begin initValue =  123; end
			231: begin initValue =  108; end
			232: begin initValue =  121; end
			233: begin initValue =  140; end
			234: begin initValue =  61; end
			235: begin initValue =  154; end
			236: begin initValue =  155; end
			237: begin initValue =  154; end
			238: begin initValue =  139; end
			239: begin initValue =  153; end
			240: begin initValue =  139; end
			241: begin initValue =  123; end
			242: begin initValue =  123; end
			243: begin initValue =  63; end
			244: begin initValue =  153; end
			245: begin initValue =  166; end
			246: begin initValue =  183; end
			247: begin initValue =  140; end
			248: begin initValue =  136; end
			249: begin initValue =  153; end
			250: begin initValue =  154; end
			251: begin initValue =  166; end
			252: begin initValue =  183; end
			253: begin initValue =  140; end
			254: begin initValue =  136; end
			255: begin initValue =  153; end
			256: begin initValue =  154; end
			257: begin initValue =  166; end
			258: begin initValue =  183; end
			259: begin initValue =  140; end
			260: begin initValue =  136; end
			261: begin initValue =  153; end
			262: begin initValue =  154; end
			263: begin initValue =  170; end
			264: begin initValue =  153; end
			265: begin initValue =  123; end
			266: begin initValue =  123; end
			267: begin initValue =  107; end
			268: begin initValue =  121; end
			269: begin initValue =  107; end
			270: begin initValue =  121; end
			271: begin initValue =  167; end
			272: begin initValue =  151; end
			273: begin initValue =  183; end
			274: begin initValue =  140; end
			275: begin initValue =  151; end
			276: begin initValue =  183; end
			277: begin initValue =  140; end
			278: begin initValue =  154; end
			279: begin initValue =  196; end
			280: begin initValue =  196; end
			281: begin initValue =  167; end
			282: begin initValue =  154; end
			283: begin initValue =  152; end
			284: begin initValue =  167; end
			285: begin initValue =  182; end
			286: begin initValue =  182; end
			287: begin initValue =  134; end
			288: begin initValue =  149; end
			289: begin initValue =  136; end
			290: begin initValue =  153; end
			291: begin initValue =  121; end
			292: begin initValue =  136; end
			293: begin initValue =  137; end
			294: begin initValue =  169; end
			295: begin initValue =  194; end
			296: begin initValue =  166; end
			297: begin initValue =  167; end
			298: begin initValue =  154; end
			299: begin initValue =  167; end
			300: begin initValue =  137; end
			301: begin initValue =  182; end
			302: begin initValue =  107; end
			303: begin initValue =  167; end
			304: begin initValue =  91; end
			305: begin initValue =  122; end
			306: begin initValue =  107; end
			307: begin initValue =  167; end
			308: begin initValue =  153; end
			309: begin initValue =  160; end
			310: begin initValue =  107; end
			311: begin initValue =  139; end
			312: begin initValue =  126; end
			313: begin initValue =  154; end
			314: begin initValue =  197; end
			315: begin initValue =  185; end
			316: begin initValue =  201; end
			317: begin initValue =  154; end
			318: begin initValue =  154; end
			319: begin initValue =  134; end
			320: begin initValue =  154; end
			321: begin initValue =  139; end
			322: begin initValue =  154; end
			323: begin initValue =  154; end
			324: begin initValue =  183; end
			325: begin initValue =  152; end
			326: begin initValue =  154; end
			327: begin initValue =  137; end
			328: begin initValue =  95; end
			329: begin initValue =  79; end
			330: begin initValue =  63; end
			331: begin initValue =  31; end
			332: begin initValue =  31; end
			333: begin initValue =  153; end
			334: begin initValue =  153; end
			335: begin initValue =  169; end //335: begin initValue =  198; end
			336: begin initValue =  198; end
			337: begin initValue =  168; end
			338: begin initValue =  79; end
			339: begin initValue =  224; end
			340: begin initValue =  167; end
			341: begin initValue =  122; end
			342: begin initValue =  153; end
			343: begin initValue =  111; end
			344: begin initValue =  149; end
			345: begin initValue =  92; end
			346: begin initValue =  167; end
			347: begin initValue =  154; end
			348: begin initValue =  139; end
			349: begin initValue =  139; end
			350: begin initValue =  125; end
			351: begin initValue =  110; end
			352: begin initValue =  124; end
			353: begin initValue =  110; end
			354: begin initValue =  95; end
			355: begin initValue =  94; end
			356: begin initValue =  125; end
			357: begin initValue =  111; end
			358: begin initValue =  111; end
			359: begin initValue =  79; end
			360: begin initValue =  125; end
			361: begin initValue =  126; end
			362: begin initValue =  111; end
			363: begin initValue =  111; end
			364: begin initValue =  79; end
			365: begin initValue =  108; end
			366: begin initValue =  123; end
			367: begin initValue =  93; end
			368: begin initValue =  125; end
			369: begin initValue =  110; end
			370: begin initValue =  124; end
			371: begin initValue =  110; end
			372: begin initValue =  95; end
			373: begin initValue =  94; end
			374: begin initValue =  125; end
			375: begin initValue =  111; end
			376: begin initValue =  111; end
			377: begin initValue =  79; end
			378: begin initValue =  125; end
			379: begin initValue =  126; end
			380: begin initValue =  111; end
			381: begin initValue =  111; end
			382: begin initValue =  79; end
			383: begin initValue =  108; end
			384: begin initValue =  123; end
			385: begin initValue =  93; end
			386: begin initValue =  121; end
			387: begin initValue =  140; end
			388: begin initValue =  61; end
			389: begin initValue =  154; end
			390: begin initValue =  170; end
			391: begin initValue =  154; end
			392: begin initValue =  139; end
			393: begin initValue =  153; end
			394: begin initValue =  139; end
			395: begin initValue =  123; end
			396: begin initValue =  123; end
			397: begin initValue =  63; end
			398: begin initValue =  124; end
			399: begin initValue =  166; end
			400: begin initValue =  183; end
			401: begin initValue =  140; end
			402: begin initValue =  136; end
			403: begin initValue =  153; end
			404: begin initValue =  154; end
			405: begin initValue =  166; end
			406: begin initValue =  183; end
			407: begin initValue =  140; end
			408: begin initValue =  136; end
			409: begin initValue =  153; end
			410: begin initValue =  154; end
			411: begin initValue =  166; end
			412: begin initValue =  183; end
			413: begin initValue =  140; end
			414: begin initValue =  136; end
			415: begin initValue =  153; end
			416: begin initValue =  154; end
			417: begin initValue =  170; end
			418: begin initValue =  153; end
			419: begin initValue =  138; end
			420: begin initValue =  138; end
			421: begin initValue =  122; end
			422: begin initValue =  121; end
			423: begin initValue =  122; end
			424: begin initValue =  121; end
			425: begin initValue =  167; end
			426: begin initValue =  151; end
			427: begin initValue =  183; end
			428: begin initValue =  140; end
			429: begin initValue =  151; end
			430: begin initValue =  183; end
			431: begin initValue =  140; end
			432: begin initValue =  154; end
			433: begin initValue =  196; end
			434: begin initValue =  167; end
			435: begin initValue =  167; end
			436: begin initValue =  154; end
			437: begin initValue =  152; end
			438: begin initValue =  167; end
			439: begin initValue =  182; end
			440: begin initValue =  182; end
			441: begin initValue =  134; end
			442: begin initValue =  149; end
			443: begin initValue =  136; end
			444: begin initValue =  153; end
			445: begin initValue =  121; end
			446: begin initValue =  136; end
			447: begin initValue =  122; end
			448: begin initValue =  169; end
			449: begin initValue =  208; end
			450: begin initValue =  166; end
			451: begin initValue =  167; end
			452: begin initValue =  154; end
			453: begin initValue =  152; end
			454: begin initValue =  167; end
			455: begin initValue =  182; end
			456: begin initValue =  107; end
			457: begin initValue =  167; end
			458: begin initValue =  91; end
			459: begin initValue =  107; end
			460: begin initValue =  107; end
			461: begin initValue =  167; end
			default : begin initValue = 0; end
		endcase
		// pipelined here
		
		 //preStateCtx = ( (initValue_D1[7:4]*3'd5 - 6'd45)* QP);

		 preStateCtx_shift = preStateCtx[11:4];
		 preStateCtx_plusn = preStateCtx_shift + ((initValue_D3[3:0]<<3)-5'd16);
		 if (preStateCtx_plusn < 1) begin
			preStateCtx_clipped = 1; 
		 end else if (preStateCtx_plusn > 126) begin
			preStateCtx_clipped = 126; 
		 end else begin
			preStateCtx_clipped = preStateCtx_plusn[6:0];
		 end
		 out[6] = preStateCtx_clipped > 63;
		 out[5:0] = (out[6] ? preStateCtx_clipped[5:0] : 6'd63 - preStateCtx_clipped[5:0]); 
		 out_ctx = ctx_D3;
	end
	
	reg signed [7:0] initM45;
	
	always @ (posedge clk) begin
		initValue_D1 <= initValue;
		ctx_D1 <= in_ctx;
		initM45 <= (initValue_D1[7:4]*3'd5 - 6'd45);
		preStateCtx <= ( initM45 * QP);
		initValue_D2 <= initValue_D1[3:0];
		initValue_D3 <= initValue_D2;
		ctx_D2 <= ctx_D1;
		ctx_D3 <= ctx_D2;
	end

endmodule
