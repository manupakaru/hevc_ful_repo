`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:09:48 12/23/2013 
// Design Name: 
// Module Name:    global_top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
(* use_dsp48 = "no" *)  

//source ipcore_dir/microblaze_mcs_setup.tcl // microblaze_mcs_data2mem ../workspace/header_parser/Debug/header_parser.elf 


module global_top(
		clk,
		rst,
		
		axi_araddr,
		axi_arburst,
		axi_arcache,
		axi_arid,
		axi_arlen,
		axi_arlock,
		axi_arprot,
		axi_arready,
		axi_arsize,
		axi_arvalid,
		
		axi_awaddr,
		axi_awburst,
		axi_awcache,
		axi_awid,
		axi_awlen,
		axi_awlock,
		axi_awprot,
		axi_awready,
		axi_awsize,
		axi_awvalid,
		
		axi_bid,
		axi_bready,
		axi_bresp,
		axi_bvalid,
		
		axi_rdata,
		axi_rid,
		axi_rlast,
		axi_rready,
		axi_rresp,
		axi_rvalid,
		
		axi_wdata,
		axi_wlast,
		axi_wready,
		axi_wstrb,
		axi_wvalid,
		
		outCont,
		validCont,
		fullCont,
		
		outY,
		validY,
		fullY,
		
		outCb,
		validCb,
		fullCb,
		
		outCr,
		validCr,
		fullCr,
		
		
     // test signals   
	  
		input_hevc_full_out_test,
		input_hevc_empty_out_test,
		input_hevc_almost_empty_out_test,
		push_full_out_test,
		push_empty_out_test,
		B_test, //32
		cu_state, //7
		ru_state, //7
		cu_x0, //12
		cu_y0, //12
		cu_other, //255
		trafo_x0, //12
		trafo_y0, //12
		trafo_POC, //32
		trafo_clk_en, 
		cabac_to_res_data_count, // 10
		input_hevc_data_count, //14
		cabac_to_res_data, // 32
		cabac_to_res_wr_en,
		
		push_data_out, //32
		push_empty,
		push_read,
		
		total_push,
		total_config,
		total_y,
		total_cb,
		total_cr,
		
	  port5,
	  port6,
	  port7,
	  port8,
	  port9,
	  
	  axi_count,
	  axi_data,
	  axi_addr,
	  axi_state
    );
	input clk;
	input rst;
	
	input [15:0] 	axi_araddr;
	input [1:0] 	axi_arburst;
	input [3:0] 	axi_arcache;
	input [11:0] 	axi_arid;
	input [7:0] 	axi_arlen;
	input 			axi_arlock;
	input [2:0]		axi_arprot;
	output 			axi_arready;
	input [2:0]		axi_arsize;
	input				axi_arvalid;
	
	input [15:0] 	axi_awaddr;
	input [1:0] 	axi_awburst;
	input [3:0] 	axi_awcache;
	input [11:0] 	axi_awid;
	input [7:0] 	axi_awlen;
	input 			axi_awlock;
	input [2:0]		axi_awprot;
	output 			axi_awready;
	input [2:0]		axi_awsize;
	input				axi_awvalid;
	
	output [11:0] 	axi_bid;
	input				axi_bready;
	output [1:0] 	axi_bresp;
	output			axi_bvalid;
	
	output [31:0] 	axi_rdata;
	output [11:0]	axi_rid;
	output			axi_rlast;
	input				axi_rready;
	output [1:0] 	axi_rresp;
	output			axi_rvalid;
	
	input [31:0] 	axi_wdata;
	input				axi_wlast;
	output			axi_wready;
	input [3:0]		axi_wstrb;
	input				axi_wvalid;
   
	output [6:0]  cu_state;
	output [4:0] ru_state;
	output [31:0] B_test;
	
	output [9:0] cabac_to_res_data_count;
	output [13:0] input_hevc_data_count;
	output [31:0] outCont;
	output validCont;
	input fullCont;
	output [143:0] outY, outCb, outCr;
	output validY, validCb, validCr;
	input fullY, fullCb, fullCr;
    
    output    input_hevc_full_out_test;
    output    input_hevc_empty_out_test;
    output    input_hevc_almost_empty_out_test;
        
    output    push_full_out_test;
	output	  push_empty_out_test;
	
	output [11:0] cu_x0, cu_y0;
	output [255:0] cu_other;
	
	output [11:0] trafo_x0, trafo_y0;
	output [15:0] trafo_POC;
	output trafo_clk_en;
	
	output [31:0] cabac_to_res_data, push_data_out;
	output cabac_to_res_wr_en;
	
	output reg [31:0] total_push;
	output reg [31:0] total_config;
	output reg [31:0] total_y;
	output reg [31:0] total_cb;
	output reg [31:0] total_cr;
	
	output push_read, push_empty;
	output [27:0] port5, port6;
	output [26:0] port7;
	output [71:0] port8;
	output [5:0] port9;
	
	output [31:0] axi_count;
	output [31:0] axi_data;
	output [15:0] axi_addr;
	output  [2:0] axi_state;
	
	wire [95:0] axi_debug;
	
	wire [31:0] cabac_to_residual_data;
	wire cabac_to_residual_empty;
	wire cabac_to_residual_read;
	
	wire [31:0] cabac_to_res_data_rev = {cabac_to_residual_data[7:0], cabac_to_residual_data[15:8], cabac_to_residual_data[23:16], cabac_to_residual_data[31:24]};
	 
	assign push_data_out = cabac_to_residual_data;
	assign push_empty = cabac_to_residual_empty;
	assign push_read = cabac_to_residual_read;
	
	assign {axi_count, axi_data, axi_addr, state} = axi_debug[95:13];
	
	wire [143:0] y_sw, cb_sw, cr_sw;
	
	localparam RESIDUAL_WIDTH  = 9;
	generate 
	genvar i,j;
	for (i=0;i<4;i=i+1) begin
		for (j=0;j<4;j=j+1) begin
			assign outY[RESIDUAL_WIDTH*(i+4*j+1)-1:RESIDUAL_WIDTH*(i+4*j)]   = y_sw[RESIDUAL_WIDTH*((4*(3-i)+(3-j))+1)-1:RESIDUAL_WIDTH*(4*(3-i)+(3-j))];
			assign outCb[RESIDUAL_WIDTH*(i+4*j+1)-1:RESIDUAL_WIDTH*(i+4*j)]  = cb_sw[RESIDUAL_WIDTH*((4*(3-i)+(3-j))+1)-1:RESIDUAL_WIDTH*(4*(3-i)+(3-j))];
			assign outCr[RESIDUAL_WIDTH*(i+4*j+1)-1:RESIDUAL_WIDTH*(i+4*j)]  = cr_sw[RESIDUAL_WIDTH*((4*(3-i)+(3-j))+1)-1:RESIDUAL_WIDTH*(4*(3-i)+(3-j))];
		end
	end	

	endgenerate
	
	entropy entropy(
		.clk(clk),
		.rst(rst),
		
		.axi_araddr	  (axi_araddr	),
		.axi_arburst  (axi_arburst ),
		.axi_arcache  (axi_arcache ),
		.axi_arid     (axi_arid    ),
		.axi_arlen    (axi_arlen	),
		.axi_arlock   (axi_arlock	),
		.axi_arprot   (axi_arprot	),
		.axi_arready  (axi_arready	),
		.axi_arsize   (axi_arsize	),
		.axi_arvalid  (axi_arvalid	),
		               
		.axi_awaddr   (axi_awaddr	),
		.axi_awburst  (axi_awburst	),
		.axi_awcache  (axi_awcache	),
		.axi_awid     (axi_awid		),
		.axi_awlen    (axi_awlen	),
		.axi_awlock   (axi_awlock	),
		.axi_awprot   (axi_awprot	),
		.axi_awready  (axi_awready	),
		.axi_awsize   (axi_awsize	),
		.axi_awvalid  (axi_awvalid	),
		               
		.axi_bid      (axi_bid		),
		.axi_bready   (axi_bready	),
		.axi_bresp    (axi_bresp	),
		.axi_bvalid   (axi_bvalid	),
		               
		.axi_rdata    (axi_rdata	),
		.axi_rid      (axi_rid		),
		.axi_rlast    (axi_rlast	),
		.axi_rready   (axi_rready	),
		.axi_rresp    (axi_rresp	),
		.axi_rvalid   (axi_rvalid	),
		               
		.axi_wdata    (axi_wdata	),
		.axi_wlast    (axi_wlast	),
		.axi_wready   (axi_wready	),
		.axi_wstrb    (axi_wstrb	),
		.axi_wvalid   (axi_wvalid	),
//		
		.cabac_to_residual_data(cabac_to_residual_data),
		.cabac_to_residual_empty(cabac_to_residual_empty),
		.cabac_to_residual_read(cabac_to_residual_read),
		
        .input_hevc_full_out_test(input_hevc_full_out_test),
        .input_hevc_empty_out_test(input_hevc_empty_out_test),
        .input_hevc_almost_empty_out_test(input_hevc_almost_empty_out_test),
        
        .push_full_out_test(push_full_out_test),
        .push_empty_out_test(push_empty_out_test),
        
        .B_test(B_test),
		  .cu_state			(cu_state),
		  .ru_state			(ru_state),
		  .cu_x0				(cu_x0),
		  .cu_y0				(cu_y0),
		  .cu_other			(cu_other),
		  .cabac_to_res_data_count(cabac_to_res_data_count),
		  .input_hevc_data_count(input_hevc_data_count),
		  .cabac_to_res_data(cabac_to_res_data),
		  .cabac_to_res_wr_en(cabac_to_res_wr_en),
		  .axi_debug(axi_debug)
	);
	
	inv_transform transform(
		.clk(clk),
		.rst(rst),
		
		.read_in(cabac_to_res_data_rev),
		.read_empty(cabac_to_residual_empty),
		.read_next(cabac_to_residual_read),
		
		.outConfig(outCont),
		.validConfig(validCont),
		.fullConfig(fullCont),
		
		.outY(y_sw),
		.validY(validY),
		.fullY(fullY),
		
		.outCb(cb_sw),
		.validCb(validCb),
		.fullCb(fullCb),
		
		.outCr(cr_sw),
		.validCr(validCr),
		.fullCr(fullCr),
		
		.trafo_x0(trafo_x0),
		.trafo_y0(trafo_y0),
		.trafo_POC(trafo_POC),
		.trafo_clk_en(trafo_clk_en),
		
		.port5(port5),
		.port6(port6),
		.port7(port7),
		.port8(port8),
		.port9(port9)
	);
	
	always @(posedge clk) begin
		if (!rst) begin
			total_push <= 0;
			total_config <= 0;
			total_y <= 0;
			total_cb <= 0;
			total_cr <= 0;
		end else begin
			if (cabac_to_res_wr_en) total_push <= total_push+1;
			if (validCont) total_config <=total_config+1;
			if (validY) total_y <= total_y + 1;
			if (validCb) total_cb <= total_cb + 1;
			if (validCr) total_cr <= total_cr + 1;
		end
	end

endmodule
