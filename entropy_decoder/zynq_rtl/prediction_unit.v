//timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:02:17 03/19/2014 
// Design Name: 
// Module Name:    prediction_unit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module prediction_unit(
    input clk,
    input rst,
    input valid,
	 input [1:0] part_idx,
	 input cu_skip_flag,
	 input [6:0] nPbW,
	 input [6:0] nPbH,
	 input [1:0] CqtDepth,
    input [2:0] MaxNumMergeCand,
    input [1:0] slice_type,
    input [3:0] num_ref_idx_l0_active_minus_1,
    input [3:0] num_ref_idx_l1_active_minus_1,
    input mvd_l1_zero_flag,
    input push_full,
    output reg [31:0] push_out,
    output reg push_valid,
    output reg pu_done,
	 output reg merge_flag,
	 
	 output reg [31:0] cabac_in_data,
	 output reg cabac_in_valid,
	 
	 input cabac_out_valid,
	 input [31:0] cabac_out_data,
	 output reg cabac_out_read
    );
	 
	 // need to consider in_fifo_almost_empty

	reg [4:0] state ;
	
	localparam STATE_PU_INIT = 5'd0;
	localparam STATE_MERGE_FLAG_PUSH = 5'd1;
	localparam STATE_MERGE_FLAG_READ = 5'd2;
	localparam STATE_MERGE_IDX_PUSH = 5'd3;
	localparam STATE_MERGE_IDX_READ = 5'd4;
	localparam STATE_INTER_PRED_IDC_PUSH = 5'd5;
	localparam STATE_INTER_PRED_IDC_READ = 5'd6;
	localparam STATE_L0_BEGIN = 5'd7;
	localparam STATE_REF_IDX_L0_PUSH = 5'd8;
	localparam STATE_REF_IDX_L0_READ = 5'd9;
	localparam STATE_MVP_L0_PUSH = 5'd10;
	localparam STATE_MVP_L0_READ = 5'd11;
	localparam STATE_L1_BEGIN = 5'd15;
	localparam STATE_REF_IDX_L1_PUSH = 5'd16;
	localparam STATE_REF_IDX_L1_READ = 5'd17;
	localparam STATE_MVP_L1_PUSH = 5'd18;
	localparam STATE_MVP_L1_READ = 5'd19;
	
	localparam STATE_MVD_GREATER0_PUSH = 5'd20; 
	localparam STATE_MVD_GREATER0_READ = 5'd21;
	localparam STATE_MVD_GREATER1_PUSH = 5'd22; 
	localparam STATE_MVD_GREATER1_READ = 5'd23;
	localparam STATE_MVD_MINUS2_PUSH = 5'd24;
	localparam STATE_MVD_MINUS2_READ = 5'd25;
	localparam STATE_MVD_SIGN_PUSH = 5'd26;
	localparam STATE_MVD_SIGN_READ = 5'd27;
	
	localparam STATE_SEND_H50 = 5'd29;
	localparam STATE_SEND_MV = 5'd30;
	localparam STATE_PU_DONE = 5'd31;
	
	localparam PRED_L0 = 2'b00;
	localparam PRED_L1 = 2'b01;
	localparam PRED_BI = 2'b10;
	
	
	//reg merge_flag;
	reg [2:0] merge_idx;
	reg [4:0] ret_state;
	reg [1:0] inter_pred_idc;
	reg mvp_l0_flag, mvp_l1_flag;
	reg [3:0] ref_idx_l0, ref_idx_l1;
	
	reg abs_mvd_greater0_flag [0:1];
	reg abs_mvd_greater1_flag [0:1];
	reg [16:0] abs_mvd_minus2 [0:1]; 
	reg mvd_sign_flag [0:1];
	
	reg [15:0] mvd_L0 [0:1];
	reg [15:0] mvd_L1 [0:1];
	
	wire [15:0] abs_mvd_x, abs_mvd_y;
	
	reg mvd_i;
	
	assign abs_mvd_x = abs_mvd_greater0_flag[0] + abs_mvd_greater1_flag[0] + abs_mvd_minus2[0];
	assign abs_mvd_y = abs_mvd_greater0_flag[1] + abs_mvd_greater1_flag[1] + abs_mvd_minus2[1];
	
	always @(posedge clk) begin
		if (~rst) begin
			state <= STATE_PU_INIT;
		end else begin
			case (state) 
				STATE_PU_INIT : begin
					if (valid) begin
						inter_pred_idc <= 2'd0;
						mvp_l0_flag <= 1'b0;
						mvp_l1_flag <= 1'b0;
						ref_idx_l0 <= 4'd0;
						ref_idx_l1 <= 4'd0;
						merge_idx <= 3'd0;
						if (cu_skip_flag) begin
							state <= STATE_MERGE_IDX_PUSH;
							merge_flag <= 1'b1;
						end else begin
							state <= STATE_MERGE_FLAG_PUSH;
						end
						mvd_L0[0] <= 16'd0;
						mvd_L0[1] <= 16'd0;
						mvd_L1[0] <= 16'd0;
						mvd_L1[1] <= 16'd0;
					end
				end
				STATE_MERGE_FLAG_PUSH: begin
					state <= STATE_MERGE_FLAG_READ;
				end
				STATE_MERGE_FLAG_READ : begin
					if (cabac_out_valid) begin
						merge_flag <= cabac_out_data[0];
						if (cabac_out_data[0]) begin
							state <= STATE_MERGE_IDX_PUSH;
						end else begin
							state <= STATE_INTER_PRED_IDC_PUSH; // push only if slice type=B
						end
					end
				end
				STATE_MERGE_IDX_PUSH : begin
					if (MaxNumMergeCand > 1) begin
						state <= STATE_MERGE_IDX_READ;
					end else begin
						ret_state <= STATE_PU_DONE;
						state <= STATE_SEND_H50;
					end
				end
				STATE_MERGE_IDX_READ : begin
					if (cabac_out_valid) begin
						merge_idx <= cabac_out_data[2:0];
						ret_state <= STATE_PU_DONE;
						state <= STATE_SEND_H50;
					end
				end
				STATE_INTER_PRED_IDC_PUSH : begin
					if (slice_type == 2'b00) begin
						state <= STATE_INTER_PRED_IDC_READ;
					end else begin
						state <= STATE_L0_BEGIN;
						inter_pred_idc <= PRED_L0;
					end
				end	
				STATE_INTER_PRED_IDC_READ : begin
					if (cabac_out_valid) begin
						state <= STATE_L0_BEGIN;
						inter_pred_idc <= cabac_out_data[1:0];
					end
				end
				STATE_L0_BEGIN : begin
					if (inter_pred_idc != PRED_L1) begin
						if (num_ref_idx_l0_active_minus_1 > 0) begin
							state <= STATE_REF_IDX_L0_READ;
						end else begin
							ref_idx_l0 <= 4'd0;
							state <= STATE_MVD_GREATER0_PUSH;
							ret_state <= STATE_MVP_L0_PUSH;
						end
					end else begin
						ref_idx_l0 <= 4'd0;
						state <= STATE_L1_BEGIN;
					end
					mvd_i <= 1'b0;
				end
				STATE_REF_IDX_L0_READ : begin
					if (cabac_out_valid) begin
						ref_idx_l0 <= cabac_out_data[3:0];
						state <= STATE_MVD_GREATER0_PUSH;
						ret_state <= STATE_MVP_L0_PUSH;
					end
				end
				STATE_MVP_L0_PUSH : begin
					mvd_L0[0] <= (mvd_sign_flag[0] ? ( (~abs_mvd_x) + 1'b1) : abs_mvd_x );
					mvd_L0[1] <= (mvd_sign_flag[1] ? ( (~abs_mvd_y) + 1'b1) : abs_mvd_y );
					state <= STATE_MVP_L0_READ;
				end
				STATE_MVP_L0_READ : begin
					if (cabac_out_valid) begin
						mvp_l0_flag <= cabac_out_data[0];
						state <= STATE_L1_BEGIN;
					end
				end
				STATE_L1_BEGIN : begin
					if (inter_pred_idc != PRED_L0) begin
						if (num_ref_idx_l1_active_minus_1 > 0) begin
							state <= STATE_REF_IDX_L1_READ;
						end else begin
							ref_idx_l1 <= 4'd0;
							if (mvd_l1_zero_flag && (inter_pred_idc == PRED_BI) ) begin
								state <= STATE_MVP_L1_PUSH;
							end else begin
								state <= STATE_MVD_GREATER0_PUSH;
							end
							ret_state <= STATE_MVP_L1_PUSH;
						end
					end else begin
						ref_idx_l1 <= 4'd0;
						state <= STATE_SEND_H50;
						ret_state <= STATE_SEND_MV;
					end
					mvd_i <= 1'b0;
				end
				STATE_REF_IDX_L1_READ : begin
					if (cabac_out_valid) begin
						ref_idx_l1 <= cabac_out_data[3:0];
						if (mvd_l1_zero_flag && (inter_pred_idc == PRED_BI) ) begin
							state <= STATE_MVP_L1_PUSH;
						end else begin
							state <= STATE_MVD_GREATER0_PUSH;
						end
						ret_state <= STATE_MVP_L1_PUSH;
					end
				end
				STATE_MVP_L1_PUSH : begin
					if (mvd_l1_zero_flag && (inter_pred_idc == PRED_BI) ) begin
						mvd_L1[0] <= 16'd0;
						mvd_L1[1] <= 16'd0;
					end else begin
						mvd_L1[0] <= (mvd_sign_flag[0] ? ( (~abs_mvd_x) + 1'b1) : abs_mvd_x );
						mvd_L1[1] <= (mvd_sign_flag[1] ? ( (~abs_mvd_y) + 1'b1) : abs_mvd_y );
					end
					state <= STATE_MVP_L1_READ;
				end
				STATE_MVP_L1_READ : begin
					if (cabac_out_valid) begin
						mvp_l1_flag <= cabac_out_data[0];
						state <= STATE_SEND_H50;
						ret_state <= STATE_SEND_MV;
					end
				end
				
				STATE_MVD_GREATER0_PUSH: begin
					if (mvd_i == 1'b1) begin
						state <= STATE_MVD_GREATER0_READ;
					end
					mvd_i <= ~mvd_i;
				end
				STATE_MVD_GREATER0_READ: begin
					if (cabac_out_valid) begin
						if (mvd_i == 1'b1) begin
							state <= STATE_MVD_GREATER1_PUSH;
						end
						abs_mvd_greater0_flag[mvd_i] <= cabac_out_data[0];
						mvd_i <= ~mvd_i;
					end
				end
				STATE_MVD_GREATER1_PUSH: begin
					if (mvd_i == 1'b1) begin
						state <= STATE_MVD_GREATER1_READ;
					end
					mvd_i <= ~mvd_i;
				end
				STATE_MVD_GREATER1_READ: begin //23
					if (abs_mvd_greater0_flag[mvd_i]) begin
						if (cabac_out_valid) begin
							if (mvd_i == 1'b1) begin
								state <= STATE_MVD_MINUS2_PUSH;
							end
							abs_mvd_greater1_flag[mvd_i] <= cabac_out_data[0];
							mvd_i <= ~mvd_i;
						end
					end else begin
						abs_mvd_greater1_flag[mvd_i] <= 1'b0;
						mvd_i <= ~mvd_i;
						if (mvd_i == 1'b1) begin
							state <= STATE_MVD_MINUS2_PUSH;
						end
					end
				end
				STATE_MVD_MINUS2_PUSH: begin //24
					if (abs_mvd_greater0_flag[mvd_i]) begin
						if (abs_mvd_greater1_flag[mvd_i]) begin
							state <= STATE_MVD_MINUS2_READ;
						end else begin
							abs_mvd_minus2[mvd_i] <= 16'd0;
							state <= STATE_MVD_SIGN_PUSH;
						end
					end else begin
						if (mvd_i == 1'b1) begin
							state <= ret_state;
						end
						abs_mvd_minus2[mvd_i] <= 16'd0;
						mvd_sign_flag[mvd_i] <= 1'b0;
						mvd_i <= ~mvd_i;
					end
				end
				STATE_MVD_MINUS2_READ: begin
					if (cabac_out_valid) begin
						abs_mvd_minus2[mvd_i] <= cabac_out_data[15:0];
						state <= STATE_MVD_SIGN_PUSH;
					end
				end
				STATE_MVD_SIGN_PUSH : begin
					state <= STATE_MVD_SIGN_READ;
				end
				STATE_MVD_SIGN_READ : begin
					if (cabac_out_valid) begin
						mvd_sign_flag[mvd_i] <= cabac_out_data[0];
						if (mvd_i == 1'b1) begin
							state <= ret_state;
						end else begin
							state <= STATE_MVD_MINUS2_PUSH;
						end
						mvd_i <= ~mvd_i;
					end
				end
				
				STATE_SEND_H50 : begin
					if (!push_full) begin
						state <= ret_state;
						mvd_i <= 1'b0;
					end
				end
				STATE_SEND_MV : begin
					if (~push_full) begin
						if (mvd_i == 1'b1) begin
							state <= STATE_PU_DONE;
						end
						mvd_i <= ~mvd_i;
					end
				end
				
				
				STATE_PU_DONE : begin
					state <= STATE_PU_INIT;
				end
				
			endcase
		end
	end
	
	always @ (*) begin
		push_out = 32'dx;
		push_valid = 1'b0;
		pu_done = 1'b0;
		cabac_in_data = 32'dx;
		cabac_in_valid = 1'b0;
		cabac_out_read = 1'b0;
		case (state) 
			STATE_MERGE_FLAG_PUSH : begin
				cabac_in_data = {8'd21, 24'd0};
				cabac_in_valid = 1'b1;
			end
			STATE_MERGE_FLAG_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			STATE_MERGE_IDX_PUSH : begin
				if (MaxNumMergeCand > 1) begin
					cabac_in_data = {8'd22, MaxNumMergeCand, 21'd0};
					cabac_in_valid = 1'b1;
				end
			end
			STATE_MERGE_IDX_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			STATE_INTER_PRED_IDC_PUSH : begin
				if (slice_type == 2'b00) begin
					cabac_in_data = {8'd23, 1'b0, nPbW, 1'b0, nPbH,1'b0, CqtDepth,  5'd0};
					cabac_in_valid = 1'b1;
				end
			end
			STATE_INTER_PRED_IDC_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			STATE_L0_BEGIN : begin
				if (inter_pred_idc != PRED_L1) begin
					if (num_ref_idx_l0_active_minus_1 > 0) begin
						cabac_in_data = {8'd24,4'd0, num_ref_idx_l0_active_minus_1,  16'd0};
						cabac_in_valid = 1'b1;
					end
				end
			end
			STATE_REF_IDX_L0_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			STATE_MVP_L0_PUSH : begin
				cabac_in_data = {8'd30, 24'd0};
				cabac_in_valid = 1'b1;
			end
			STATE_MVP_L0_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			STATE_L1_BEGIN : begin
				if (inter_pred_idc != PRED_L0) begin
					if (num_ref_idx_l1_active_minus_1 > 0) begin
						cabac_in_data = {8'd25,4'd0, num_ref_idx_l1_active_minus_1,  16'd0};
						cabac_in_valid = 1'b1;
					end
				end
			end
			STATE_REF_IDX_L1_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			STATE_MVP_L1_PUSH : begin
				cabac_in_data = {8'd30, 24'd0};
				cabac_in_valid = 1'b1;
			end
			STATE_MVP_L1_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			
			STATE_MVD_GREATER0_PUSH: begin
				cabac_in_data = {8'd26,24'd0};
				cabac_in_valid = 1'b1;
			end
			STATE_MVD_GREATER0_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			STATE_MVD_GREATER1_PUSH: begin
				if (abs_mvd_greater0_flag[mvd_i]) begin
					cabac_in_data = {8'd27,24'd0};
					cabac_in_valid = 1'b1;
				end
			end
			STATE_MVD_GREATER1_READ : begin
				if (abs_mvd_greater0_flag[mvd_i]) begin
					if (cabac_out_valid) begin
						cabac_out_read = 1'b1;
					end
				end
			end
			STATE_MVD_MINUS2_PUSH: begin
				if (abs_mvd_greater0_flag[mvd_i]) begin
					if (abs_mvd_greater1_flag[mvd_i]) begin
						cabac_in_data = {8'd28,24'd0};
						cabac_in_valid = 1'b1;
					end
				end 
			end
			STATE_MVD_MINUS2_READ: begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			STATE_MVD_SIGN_PUSH: begin
				cabac_in_data = {8'd29,24'd0};
				cabac_in_valid = 1'b1;
			end
			STATE_MVD_SIGN_READ: begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
				
			STATE_SEND_H50 : begin
				if (!push_full) begin
					push_valid  = 1'b1;
					{push_out[7:0] , push_out[15:8], push_out[23:16], push_out[31:24]} =
						{ 6'd0, ref_idx_l1, ref_idx_l0, mvp_l1_flag, mvp_l0_flag, merge_idx, merge_flag, inter_pred_idc, part_idx  ,8'h50};
				end
			end
			STATE_SEND_MV : begin
				if (~push_full) begin
					if (mvd_i == 1'b0) begin
						{push_out[7:0] , push_out[15:8], push_out[23:16], push_out[31:24]} = {mvd_L0[1] , mvd_L0[0] };
					end else begin
						{push_out[7:0] , push_out[15:8], push_out[23:16], push_out[31:24]} = {mvd_L1[1] , mvd_L1[0] };
					end
					push_valid = 1'b1;
				end
			end
			
			STATE_PU_DONE : begin 
				pu_done = 1'b1;
			end
		
		endcase
	end


endmodule
