`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:09:38 11/11/2013 
// Design Name: 
// Module Name:    bin_TU 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module bin_TU(
    input [5:0] bin_no,
    input [5:0] cMax,
	 input binVal,
	 input stall,
    output reg [31:0] decoded_se,
    output reg decoded_valid
    //output reg take_next_se
    );
	always @(*) begin
		if (binVal == 1'b0) begin
			decoded_se = bin_no;
			decoded_valid = ~stall;
			//take_next_se = 1'b1;
		end else begin
			//take_next_se = (bin_no == cMax);
			decoded_valid = (bin_no == cMax-1) & ~stall;
			decoded_se = bin_no+1;
		end	
	end
endmodule
