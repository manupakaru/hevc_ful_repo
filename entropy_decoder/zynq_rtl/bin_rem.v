`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:20:34 11/12/2013 
// Design Name: 
// Module Name:    bin_rem 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module bin_rem(
    input clk,
	 input rst,
    input [5:0] bin_no,
    input [1:0] baseLevel,
	 input binVal,
    input stall,
    output reg [31:0] decoded_se,
    output reg decoded_valid
    );
	`define STATE_REM_PREFIX 3'b000
	`define STATE_REM_ONES 3'b001
	`define STATE_REM_EGK 3'b010
	`define STATE_REM_FL 3'b011
	
	reg [2:0] state;
	reg [15:0] ret;
	reg [3:0] FL_i;
	reg [3:0] egk_ones;
	reg [15:0] egk_suf;
	
	reg [2:0] cRiceParam;
	
	always @ (posedge clk) begin
		if (rst) begin
			state <= `STATE_REM_PREFIX;
			ret <= 16'd0;
			FL_i <= 2'd1;
			cRiceParam <= 3'd0;
		end else begin
			case (state)
				`STATE_REM_PREFIX: begin
					if (~stall) begin
						if (binVal == 1'b0) begin
							state <= (cRiceParam == 2'd0) ? `STATE_REM_PREFIX : `STATE_REM_FL;
							ret <= (cRiceParam == 2'd0) ? 16'd0 : ret; 
							FL_i <= 2'd1;
						end else begin
							state <= (bin_no <6'd3) ? `STATE_REM_PREFIX : `STATE_REM_ONES;
							ret <= ret + 16'd1;
							FL_i <= 2'd1;
						end
						egk_ones <= 4'd0;
					end
				end
				
				`STATE_REM_FL : begin
					if (~stall) begin
						state <= (FL_i == cRiceParam) ? `STATE_REM_PREFIX : `STATE_REM_FL;
						ret <= (FL_i == cRiceParam) ? 16'd0 : {ret[14:0], binVal};
						FL_i <= (FL_i == cRiceParam) ? 2'd0 : FL_i +2'd1;
						egk_ones <= 4'd0;
					end
				end
				
				`STATE_REM_ONES : begin
					if (~stall) begin
						state <= (binVal == 1'b1) ? `STATE_REM_ONES : `STATE_REM_EGK;
						egk_ones <= (binVal == 1'b1) ? egk_ones + 4'd1 : egk_ones;
						FL_i <= 4'd0;
						ret <= 16'd1;
						egk_suf <= 15'd0;
					end
				end
				
				`STATE_REM_EGK : begin
					if (~stall) begin
						state <= (FL_i < egk_ones + cRiceParam) ? `STATE_REM_EGK : `STATE_REM_PREFIX;
						FL_i <= (FL_i < egk_ones + cRiceParam) ? FL_i+ 3'd1 : 3'd1;
						if (FL_i < egk_ones) begin
							ret <= {ret[14:0], 1'b0};
						end else if (FL_i == egk_ones) begin
							ret <= (cRiceParam == 2'd0) ? 16'd0 : ret + 1'b1;
						end else begin
							ret <= (FL_i < egk_ones + cRiceParam) ? {ret[14:0], 1'b0} : 16'd0;
						end
						
						egk_suf <= { egk_suf[14:0] , binVal };
						
					end
				end
				
				default: begin
					state <= `STATE_REM_PREFIX;
					ret <= 16'd0;
					FL_i <= 2'd1;
				end
			endcase
			
			if (decoded_valid) begin // decoded_valid
				case (cRiceParam)
					0 : if ((decoded_se + baseLevel) > 3) cRiceParam <= 3'd1;
					1 : if ((decoded_se + baseLevel) > 6) cRiceParam <= 3'd2;
					2 : if ((decoded_se + baseLevel) > 12) cRiceParam <= 3'd3;
					3 : if ((decoded_se + baseLevel) > 24) cRiceParam <= 3'd4;
				endcase
			end
		end
	end
	
	always @ (*) begin
		case (state)
			`STATE_REM_PREFIX: begin
				if (~stall) begin
					if (binVal == 1'b0) begin
						decoded_se = (cRiceParam == 2'd0) ? bin_no : 32'dx;
						decoded_valid = (cRiceParam == 2'd0) ? 1'b1 : 1'b0;
					end else begin
						decoded_se = 32'dx;
						decoded_valid = 1'b0;
					end
				end else begin
					decoded_se = 32'dx;
					decoded_valid = 1'b0;
				end
			end
			
			`STATE_REM_FL : begin
				if (~stall) begin
					decoded_se = (FL_i == cRiceParam) ? {ret[14:0], binVal} :  32'dx;
					decoded_valid = (FL_i == cRiceParam) ? 1'b1 : 1'b0 ;
				end else begin
					decoded_se = 32'dx;
					decoded_valid = 1'b0;
				end				
			end
			
			`STATE_REM_ONES :begin
				decoded_se = 32'dx;
				decoded_valid = 1'b0;
			end
			
			`STATE_REM_EGK : begin
				if (cRiceParam == 2'd0) begin
					decoded_se = { ret[15:0] + 1'b1, 1'b0} + { egk_suf[15:0] , binVal };
				end else begin
					decoded_se = { ret[14:0] , 2'b00} + { egk_suf[15:0] , binVal };
				end
				decoded_valid = (FL_i < egk_ones + cRiceParam) ? 1'b0 : 1'b1;
				
			end
			
			default: begin
				decoded_se = 32'dx;
				decoded_valid = 1'b0;
			end
		endcase
	end

endmodule
