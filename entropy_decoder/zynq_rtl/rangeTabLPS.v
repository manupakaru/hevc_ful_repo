`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:17:13 11/07/2013 
// Design Name: 
// Module Name:    rangeTabLPS 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rangeTabLPS(
    input [5:0] pStateIdx,
    input [1:0] qCodIRangeIdx,
    output reg [7:0] codIRangeLPS
    );
	 
	always @(*) begin
		case ( {pStateIdx , qCodIRangeIdx} )
			{ 6'd0 , 2'd0} : codIRangeLPS = 8'd128;
			{ 6'd1 , 2'd0} : codIRangeLPS = 8'd128;
			{ 6'd2 , 2'd0} : codIRangeLPS = 8'd128;
			{ 6'd3 , 2'd0} : codIRangeLPS = 8'd123;
			{ 6'd4 , 2'd0} : codIRangeLPS = 8'd116;
			{ 6'd5 , 2'd0} : codIRangeLPS = 8'd111;
			{ 6'd6 , 2'd0} : codIRangeLPS = 8'd105;
			{ 6'd7 , 2'd0} : codIRangeLPS = 8'd100;
			{ 6'd8 , 2'd0} : codIRangeLPS = 8'd95;
			{ 6'd9 , 2'd0} : codIRangeLPS = 8'd90;
			{ 6'd10 , 2'd0} : codIRangeLPS = 8'd85;
			{ 6'd11 , 2'd0} : codIRangeLPS = 8'd81;
			{ 6'd12 , 2'd0} : codIRangeLPS = 8'd77;
			{ 6'd13 , 2'd0} : codIRangeLPS = 8'd73;
			{ 6'd14 , 2'd0} : codIRangeLPS = 8'd69;
			{ 6'd15 , 2'd0} : codIRangeLPS = 8'd66;
			{ 6'd16 , 2'd0} : codIRangeLPS = 8'd62;
			{ 6'd17 , 2'd0} : codIRangeLPS = 8'd59;
			{ 6'd18 , 2'd0} : codIRangeLPS = 8'd56;
			{ 6'd19 , 2'd0} : codIRangeLPS = 8'd53;
			{ 6'd20 , 2'd0} : codIRangeLPS = 8'd51;
			{ 6'd21 , 2'd0} : codIRangeLPS = 8'd48;
			{ 6'd22 , 2'd0} : codIRangeLPS = 8'd46;
			{ 6'd23 , 2'd0} : codIRangeLPS = 8'd43;
			{ 6'd24 , 2'd0} : codIRangeLPS = 8'd41;
			{ 6'd25 , 2'd0} : codIRangeLPS = 8'd39;
			{ 6'd26 , 2'd0} : codIRangeLPS = 8'd37;
			{ 6'd27 , 2'd0} : codIRangeLPS = 8'd35;
			{ 6'd28 , 2'd0} : codIRangeLPS = 8'd33;
			{ 6'd29 , 2'd0} : codIRangeLPS = 8'd32;
			{ 6'd30 , 2'd0} : codIRangeLPS = 8'd30;
			{ 6'd31 , 2'd0} : codIRangeLPS = 8'd29;
			{ 6'd32 , 2'd0} : codIRangeLPS = 8'd27;
			{ 6'd33 , 2'd0} : codIRangeLPS = 8'd26;
			{ 6'd34 , 2'd0} : codIRangeLPS = 8'd24;
			{ 6'd35 , 2'd0} : codIRangeLPS = 8'd23;
			{ 6'd36 , 2'd0} : codIRangeLPS = 8'd22;
			{ 6'd37 , 2'd0} : codIRangeLPS = 8'd21;
			{ 6'd38 , 2'd0} : codIRangeLPS = 8'd20;
			{ 6'd39 , 2'd0} : codIRangeLPS = 8'd19;
			{ 6'd40 , 2'd0} : codIRangeLPS = 8'd18;
			{ 6'd41 , 2'd0} : codIRangeLPS = 8'd17;
			{ 6'd42 , 2'd0} : codIRangeLPS = 8'd16;
			{ 6'd43 , 2'd0} : codIRangeLPS = 8'd15;
			{ 6'd44 , 2'd0} : codIRangeLPS = 8'd14;
			{ 6'd45 , 2'd0} : codIRangeLPS = 8'd14;
			{ 6'd46 , 2'd0} : codIRangeLPS = 8'd13;
			{ 6'd47 , 2'd0} : codIRangeLPS = 8'd12;
			{ 6'd48 , 2'd0} : codIRangeLPS = 8'd12;
			{ 6'd49 , 2'd0} : codIRangeLPS = 8'd11;
			{ 6'd50 , 2'd0} : codIRangeLPS = 8'd11;
			{ 6'd51 , 2'd0} : codIRangeLPS = 8'd10;
			{ 6'd52 , 2'd0} : codIRangeLPS = 8'd10;
			{ 6'd53 , 2'd0} : codIRangeLPS = 8'd9;
			{ 6'd54 , 2'd0} : codIRangeLPS = 8'd9;
			{ 6'd55 , 2'd0} : codIRangeLPS = 8'd8;
			{ 6'd56 , 2'd0} : codIRangeLPS = 8'd8;
			{ 6'd57 , 2'd0} : codIRangeLPS = 8'd7;
			{ 6'd58 , 2'd0} : codIRangeLPS = 8'd7;
			{ 6'd59 , 2'd0} : codIRangeLPS = 8'd7;
			{ 6'd60 , 2'd0} : codIRangeLPS = 8'd6;
			{ 6'd61 , 2'd0} : codIRangeLPS = 8'd6;
			{ 6'd62 , 2'd0} : codIRangeLPS = 8'd6;
			{ 6'd63 , 2'd0} : codIRangeLPS = 8'd2;
			{ 6'd0 , 2'd1} : codIRangeLPS = 8'd176;
			{ 6'd1 , 2'd1} : codIRangeLPS = 8'd167;
			{ 6'd2 , 2'd1} : codIRangeLPS = 8'd158;
			{ 6'd3 , 2'd1} : codIRangeLPS = 8'd150;
			{ 6'd4 , 2'd1} : codIRangeLPS = 8'd142;
			{ 6'd5 , 2'd1} : codIRangeLPS = 8'd135;
			{ 6'd6 , 2'd1} : codIRangeLPS = 8'd128;
			{ 6'd7 , 2'd1} : codIRangeLPS = 8'd122;
			{ 6'd8 , 2'd1} : codIRangeLPS = 8'd116;
			{ 6'd9 , 2'd1} : codIRangeLPS = 8'd110;
			{ 6'd10 , 2'd1} : codIRangeLPS = 8'd104;
			{ 6'd11 , 2'd1} : codIRangeLPS = 8'd99;
			{ 6'd12 , 2'd1} : codIRangeLPS = 8'd94;
			{ 6'd13 , 2'd1} : codIRangeLPS = 8'd89;
			{ 6'd14 , 2'd1} : codIRangeLPS = 8'd85;
			{ 6'd15 , 2'd1} : codIRangeLPS = 8'd80;
			{ 6'd16 , 2'd1} : codIRangeLPS = 8'd76;
			{ 6'd17 , 2'd1} : codIRangeLPS = 8'd72;
			{ 6'd18 , 2'd1} : codIRangeLPS = 8'd69;
			{ 6'd19 , 2'd1} : codIRangeLPS = 8'd65;
			{ 6'd20 , 2'd1} : codIRangeLPS = 8'd62;
			{ 6'd21 , 2'd1} : codIRangeLPS = 8'd59;
			{ 6'd22 , 2'd1} : codIRangeLPS = 8'd56;
			{ 6'd23 , 2'd1} : codIRangeLPS = 8'd53;
			{ 6'd24 , 2'd1} : codIRangeLPS = 8'd50;
			{ 6'd25 , 2'd1} : codIRangeLPS = 8'd48;
			{ 6'd26 , 2'd1} : codIRangeLPS = 8'd45;
			{ 6'd27 , 2'd1} : codIRangeLPS = 8'd43;
			{ 6'd28 , 2'd1} : codIRangeLPS = 8'd41;
			{ 6'd29 , 2'd1} : codIRangeLPS = 8'd39;
			{ 6'd30 , 2'd1} : codIRangeLPS = 8'd37;
			{ 6'd31 , 2'd1} : codIRangeLPS = 8'd35;
			{ 6'd32 , 2'd1} : codIRangeLPS = 8'd33;
			{ 6'd33 , 2'd1} : codIRangeLPS = 8'd31;
			{ 6'd34 , 2'd1} : codIRangeLPS = 8'd30;
			{ 6'd35 , 2'd1} : codIRangeLPS = 8'd28;
			{ 6'd36 , 2'd1} : codIRangeLPS = 8'd27;
			{ 6'd37 , 2'd1} : codIRangeLPS = 8'd26;
			{ 6'd38 , 2'd1} : codIRangeLPS = 8'd24;
			{ 6'd39 , 2'd1} : codIRangeLPS = 8'd23;
			{ 6'd40 , 2'd1} : codIRangeLPS = 8'd22;
			{ 6'd41 , 2'd1} : codIRangeLPS = 8'd21;
			{ 6'd42 , 2'd1} : codIRangeLPS = 8'd20;
			{ 6'd43 , 2'd1} : codIRangeLPS = 8'd19;
			{ 6'd44 , 2'd1} : codIRangeLPS = 8'd18;
			{ 6'd45 , 2'd1} : codIRangeLPS = 8'd17;
			{ 6'd46 , 2'd1} : codIRangeLPS = 8'd16;
			{ 6'd47 , 2'd1} : codIRangeLPS = 8'd15;
			{ 6'd48 , 2'd1} : codIRangeLPS = 8'd14;
			{ 6'd49 , 2'd1} : codIRangeLPS = 8'd14;
			{ 6'd50 , 2'd1} : codIRangeLPS = 8'd13;
			{ 6'd51 , 2'd1} : codIRangeLPS = 8'd12;
			{ 6'd52 , 2'd1} : codIRangeLPS = 8'd12;
			{ 6'd53 , 2'd1} : codIRangeLPS = 8'd11;
			{ 6'd54 , 2'd1} : codIRangeLPS = 8'd11;
			{ 6'd55 , 2'd1} : codIRangeLPS = 8'd10;
			{ 6'd56 , 2'd1} : codIRangeLPS = 8'd9;
			{ 6'd57 , 2'd1} : codIRangeLPS = 8'd9;
			{ 6'd58 , 2'd1} : codIRangeLPS = 8'd9;
			{ 6'd59 , 2'd1} : codIRangeLPS = 8'd8;
			{ 6'd60 , 2'd1} : codIRangeLPS = 8'd8;
			{ 6'd61 , 2'd1} : codIRangeLPS = 8'd7;
			{ 6'd62 , 2'd1} : codIRangeLPS = 8'd7;
			{ 6'd63 , 2'd1} : codIRangeLPS = 8'd2;
			{ 6'd0 , 2'd2} : codIRangeLPS = 8'd208;
			{ 6'd1 , 2'd2} : codIRangeLPS = 8'd197;
			{ 6'd2 , 2'd2} : codIRangeLPS = 8'd187;
			{ 6'd3 , 2'd2} : codIRangeLPS = 8'd178;
			{ 6'd4 , 2'd2} : codIRangeLPS = 8'd169;
			{ 6'd5 , 2'd2} : codIRangeLPS = 8'd160;
			{ 6'd6 , 2'd2} : codIRangeLPS = 8'd152;
			{ 6'd7 , 2'd2} : codIRangeLPS = 8'd144;
			{ 6'd8 , 2'd2} : codIRangeLPS = 8'd137;
			{ 6'd9 , 2'd2} : codIRangeLPS = 8'd130;
			{ 6'd10 , 2'd2} : codIRangeLPS = 8'd123;
			{ 6'd11 , 2'd2} : codIRangeLPS = 8'd117;
			{ 6'd12 , 2'd2} : codIRangeLPS = 8'd111;
			{ 6'd13 , 2'd2} : codIRangeLPS = 8'd105;
			{ 6'd14 , 2'd2} : codIRangeLPS = 8'd100;
			{ 6'd15 , 2'd2} : codIRangeLPS = 8'd95;
			{ 6'd16 , 2'd2} : codIRangeLPS = 8'd90;
			{ 6'd17 , 2'd2} : codIRangeLPS = 8'd86;
			{ 6'd18 , 2'd2} : codIRangeLPS = 8'd81;
			{ 6'd19 , 2'd2} : codIRangeLPS = 8'd77;
			{ 6'd20 , 2'd2} : codIRangeLPS = 8'd73;
			{ 6'd21 , 2'd2} : codIRangeLPS = 8'd69;
			{ 6'd22 , 2'd2} : codIRangeLPS = 8'd66;
			{ 6'd23 , 2'd2} : codIRangeLPS = 8'd63;
			{ 6'd24 , 2'd2} : codIRangeLPS = 8'd59;
			{ 6'd25 , 2'd2} : codIRangeLPS = 8'd56;
			{ 6'd26 , 2'd2} : codIRangeLPS = 8'd54;
			{ 6'd27 , 2'd2} : codIRangeLPS = 8'd51;
			{ 6'd28 , 2'd2} : codIRangeLPS = 8'd48;
			{ 6'd29 , 2'd2} : codIRangeLPS = 8'd46;
			{ 6'd30 , 2'd2} : codIRangeLPS = 8'd43;
			{ 6'd31 , 2'd2} : codIRangeLPS = 8'd41;
			{ 6'd32 , 2'd2} : codIRangeLPS = 8'd39;
			{ 6'd33 , 2'd2} : codIRangeLPS = 8'd37;
			{ 6'd34 , 2'd2} : codIRangeLPS = 8'd35;
			{ 6'd35 , 2'd2} : codIRangeLPS = 8'd33;
			{ 6'd36 , 2'd2} : codIRangeLPS = 8'd32;
			{ 6'd37 , 2'd2} : codIRangeLPS = 8'd30;
			{ 6'd38 , 2'd2} : codIRangeLPS = 8'd29;
			{ 6'd39 , 2'd2} : codIRangeLPS = 8'd27;
			{ 6'd40 , 2'd2} : codIRangeLPS = 8'd26;
			{ 6'd41 , 2'd2} : codIRangeLPS = 8'd25;
			{ 6'd42 , 2'd2} : codIRangeLPS = 8'd23;
			{ 6'd43 , 2'd2} : codIRangeLPS = 8'd22;
			{ 6'd44 , 2'd2} : codIRangeLPS = 8'd21;
			{ 6'd45 , 2'd2} : codIRangeLPS = 8'd20;
			{ 6'd46 , 2'd2} : codIRangeLPS = 8'd19;
			{ 6'd47 , 2'd2} : codIRangeLPS = 8'd18;
			{ 6'd48 , 2'd2} : codIRangeLPS = 8'd17;
			{ 6'd49 , 2'd2} : codIRangeLPS = 8'd16;
			{ 6'd50 , 2'd2} : codIRangeLPS = 8'd15;
			{ 6'd51 , 2'd2} : codIRangeLPS = 8'd15;
			{ 6'd52 , 2'd2} : codIRangeLPS = 8'd14;
			{ 6'd53 , 2'd2} : codIRangeLPS = 8'd13;
			{ 6'd54 , 2'd2} : codIRangeLPS = 8'd12;
			{ 6'd55 , 2'd2} : codIRangeLPS = 8'd12;
			{ 6'd56 , 2'd2} : codIRangeLPS = 8'd11;
			{ 6'd57 , 2'd2} : codIRangeLPS = 8'd11;
			{ 6'd58 , 2'd2} : codIRangeLPS = 8'd10;
			{ 6'd59 , 2'd2} : codIRangeLPS = 8'd10;
			{ 6'd60 , 2'd2} : codIRangeLPS = 8'd9;
			{ 6'd61 , 2'd2} : codIRangeLPS = 8'd9;
			{ 6'd62 , 2'd2} : codIRangeLPS = 8'd8;
			{ 6'd63 , 2'd2} : codIRangeLPS = 8'd2;
			{ 6'd0 , 2'd3} : codIRangeLPS = 8'd240;
			{ 6'd1 , 2'd3} : codIRangeLPS = 8'd227;
			{ 6'd2 , 2'd3} : codIRangeLPS = 8'd216;
			{ 6'd3 , 2'd3} : codIRangeLPS = 8'd205;
			{ 6'd4 , 2'd3} : codIRangeLPS = 8'd195;
			{ 6'd5 , 2'd3} : codIRangeLPS = 8'd185;
			{ 6'd6 , 2'd3} : codIRangeLPS = 8'd175;
			{ 6'd7 , 2'd3} : codIRangeLPS = 8'd166;
			{ 6'd8 , 2'd3} : codIRangeLPS = 8'd158;
			{ 6'd9 , 2'd3} : codIRangeLPS = 8'd150;
			{ 6'd10 , 2'd3} : codIRangeLPS = 8'd142;
			{ 6'd11 , 2'd3} : codIRangeLPS = 8'd135;
			{ 6'd12 , 2'd3} : codIRangeLPS = 8'd128;
			{ 6'd13 , 2'd3} : codIRangeLPS = 8'd122;
			{ 6'd14 , 2'd3} : codIRangeLPS = 8'd116;
			{ 6'd15 , 2'd3} : codIRangeLPS = 8'd110;
			{ 6'd16 , 2'd3} : codIRangeLPS = 8'd104;
			{ 6'd17 , 2'd3} : codIRangeLPS = 8'd99;
			{ 6'd18 , 2'd3} : codIRangeLPS = 8'd94;
			{ 6'd19 , 2'd3} : codIRangeLPS = 8'd89;
			{ 6'd20 , 2'd3} : codIRangeLPS = 8'd85;
			{ 6'd21 , 2'd3} : codIRangeLPS = 8'd80;
			{ 6'd22 , 2'd3} : codIRangeLPS = 8'd76;
			{ 6'd23 , 2'd3} : codIRangeLPS = 8'd72;
			{ 6'd24 , 2'd3} : codIRangeLPS = 8'd69;
			{ 6'd25 , 2'd3} : codIRangeLPS = 8'd65;
			{ 6'd26 , 2'd3} : codIRangeLPS = 8'd62;
			{ 6'd27 , 2'd3} : codIRangeLPS = 8'd59;
			{ 6'd28 , 2'd3} : codIRangeLPS = 8'd56;
			{ 6'd29 , 2'd3} : codIRangeLPS = 8'd53;
			{ 6'd30 , 2'd3} : codIRangeLPS = 8'd50;
			{ 6'd31 , 2'd3} : codIRangeLPS = 8'd48;
			{ 6'd32 , 2'd3} : codIRangeLPS = 8'd45;
			{ 6'd33 , 2'd3} : codIRangeLPS = 8'd43;
			{ 6'd34 , 2'd3} : codIRangeLPS = 8'd41;
			{ 6'd35 , 2'd3} : codIRangeLPS = 8'd39;
			{ 6'd36 , 2'd3} : codIRangeLPS = 8'd37;
			{ 6'd37 , 2'd3} : codIRangeLPS = 8'd35;
			{ 6'd38 , 2'd3} : codIRangeLPS = 8'd33;
			{ 6'd39 , 2'd3} : codIRangeLPS = 8'd31;
			{ 6'd40 , 2'd3} : codIRangeLPS = 8'd30;
			{ 6'd41 , 2'd3} : codIRangeLPS = 8'd28;
			{ 6'd42 , 2'd3} : codIRangeLPS = 8'd27;
			{ 6'd43 , 2'd3} : codIRangeLPS = 8'd25;
			{ 6'd44 , 2'd3} : codIRangeLPS = 8'd24;
			{ 6'd45 , 2'd3} : codIRangeLPS = 8'd23;
			{ 6'd46 , 2'd3} : codIRangeLPS = 8'd22;
			{ 6'd47 , 2'd3} : codIRangeLPS = 8'd21;
			{ 6'd48 , 2'd3} : codIRangeLPS = 8'd20;
			{ 6'd49 , 2'd3} : codIRangeLPS = 8'd19;
			{ 6'd50 , 2'd3} : codIRangeLPS = 8'd18;
			{ 6'd51 , 2'd3} : codIRangeLPS = 8'd17;
			{ 6'd52 , 2'd3} : codIRangeLPS = 8'd16;
			{ 6'd53 , 2'd3} : codIRangeLPS = 8'd15;
			{ 6'd54 , 2'd3} : codIRangeLPS = 8'd14;
			{ 6'd55 , 2'd3} : codIRangeLPS = 8'd14;
			{ 6'd56 , 2'd3} : codIRangeLPS = 8'd13;
			{ 6'd57 , 2'd3} : codIRangeLPS = 8'd12;
			{ 6'd58 , 2'd3} : codIRangeLPS = 8'd12;
			{ 6'd59 , 2'd3} : codIRangeLPS = 8'd11;
			{ 6'd60 , 2'd3} : codIRangeLPS = 8'd11;
			{ 6'd61 , 2'd3} : codIRangeLPS = 8'd10;
			{ 6'd62 , 2'd3} : codIRangeLPS = 8'd9;
			{ 6'd63 , 2'd3} : codIRangeLPS = 8'd2;

		
		endcase
	
	end

endmodule
