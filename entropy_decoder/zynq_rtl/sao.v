`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:52:36 03/21/2014 
// Design Name: 
// Module Name:    sao 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module sao(
    input clk,
    input rst,
	 
	 input valid,
	 input [7:0] rx,
	 input [7:0] ry,
	 input same_slice_tile_left,
	 input same_slice_tile_up,
	 
	 input slice_sao_luma_flag,
	 input slice_sao_chroma_flag,
	 
	 output reg [31:0] push_out,
    output reg push_valid,
	 input push_full,
	 
    output reg sao_done,
	 
	 output reg [31:0] cabac_in_data,
	 output reg cabac_in_valid,
	 
	 input cabac_out_valid,
	 input [31:0] cabac_out_data,
	 output reg cabac_out_read
    );

	reg [4:0] state;
	
	// move these into BRAM
	reg [23:0] sao_buf_top_Y [0:255]; 
	reg [23:0] sao_buf_top_Cb [0:255]; 
	reg [23:0] sao_buf_top_Cr [0:255]; 
	
	reg [23:0] sao_buf_left [0:2];
	
	reg y_wr_en, cb_wr_en, cr_wr_en;
	reg [23:0] sao_buf_top_out[0:2];
	
	reg [23:0] sao_word [0:2];
	
	always @(posedge clk) begin
		if (y_wr_en)
			sao_buf_top_Y[rx] <= sao_word[0];
		if (cb_wr_en)
			sao_buf_top_Cb[rx] <= sao_word[1];
		if (cr_wr_en)
			sao_buf_top_Cr[rx] <= sao_word[2];
		sao_buf_top_out[0] <= sao_buf_top_Y[rx];
		sao_buf_top_out[1] <= sao_buf_top_Cb[rx];
		sao_buf_top_out[2] <= sao_buf_top_Cr[rx];
	end
	
	localparam STATE_INIT = 5'd0;
	localparam STATE_MERGE_LEFT_PUSH = 5'd1;
	localparam STATE_MERGE_LEFT_READ = 5'd2;
	localparam STATE_MERGE_UP_PUSH = 5'd3;
	localparam STATE_MERGE_UP_READ = 5'd4;
	localparam STATE_TYPE_PUSH = 5'd5;
	localparam STATE_TYPE_READ = 5'd6;
	localparam STATE_OFFSET_PUSH = 5'd7;
	localparam STATE_OFFSET_READ = 5'd8;
	localparam STATE_SIGN_PUSH = 5'd9; // or EO_CLASS
	localparam STATE_SIGN_READ = 5'd10;
	localparam STATE_BAND_POS_PUSH = 5'd11;
	localparam STATE_BAND_POS_READ = 5'd12;
	localparam STATE_EO_CLASS_READ = 5'd13;
	
	localparam STATE_SEND_H22 = 5'd22;
	localparam STATE_SEND_H23 = 5'd23;
	localparam STATE_SEND_H24 = 5'd24;
	localparam STATE_SAO_DONE = 5'd25;
	
	reg sao_merge_left_flag;
	reg sao_merge_up_flag;
	reg [1:0] SaoTypeIdx [0:2];
	reg [2:0] sao_offset_abs[0:2][0:3]; //[cIdx][i]
	reg sao_offset_sign[0:2][0:3];
	reg [1:0] sao_eo_class[0:2];
	reg [5:0] sao_band_position[0:2];
	reg [1:0] cIdx;
	
	reg [1:0] i;
	
	
	
	genvar j;
	generate for (j=0;j<3;j=j+1) 
		always @(*) begin
			if (sao_merge_left_flag) begin
				sao_word [j] = sao_buf_left[j];
			end else if (sao_merge_up_flag) begin
				sao_word [j] = sao_buf_top_out[j];
			end else begin
				sao_word[j] [1:0]  = SaoTypeIdx[j];
				case (SaoTypeIdx[j] )
					2'b00: sao_word[j] [7:2] = 6'd0;
					2'b01: sao_word[j] [7:2] = sao_band_position[j];
					2'b10: sao_word[j] [7:2] = {4'd0, sao_eo_class[j]};
					2'b11: sao_word[j] [7:2] = 6'd0; // cannot occur
				endcase
				sao_word[j] [10:8] = (SaoTypeIdx[j] != 2'b00) ? sao_offset_abs[j][0] : 3'd0;
				sao_word[j] [11]	 = sao_offset_sign[j][0];
				sao_word[j] [14:12]= (SaoTypeIdx[j] != 2'b00) ? sao_offset_abs[j][1] : 3'd0;
				sao_word[j] [15]	 = sao_offset_sign[j][1];
				sao_word[j] [18:16]= (SaoTypeIdx[j] != 2'b00) ? sao_offset_abs[j][2] : 3'd0;
				sao_word[j] [19]	 = sao_offset_sign[j][2];
				sao_word[j] [22:20]= (SaoTypeIdx[j] != 2'b00) ? sao_offset_abs[j][3] : 3'd0;
				sao_word[j] [23]	 = sao_offset_sign[j][3];
			end
		
		end
	endgenerate
	
	always @(posedge clk) begin
		if (~rst) begin
			state <= STATE_INIT; 
		end else begin
			case (state) 
				STATE_INIT: begin
					if (valid) begin
						state <= STATE_MERGE_LEFT_PUSH;
						cIdx <= 2'b00;
						{ sao_offset_sign [0][0], sao_offset_sign [0][1], sao_offset_sign [0][2], sao_offset_sign [0][3]} <= 4'd0;
						{ sao_offset_sign [1][0], sao_offset_sign [1][1], sao_offset_sign [1][2], sao_offset_sign [1][3]} <= 4'd0;
						{ sao_offset_sign [2][0], sao_offset_sign [2][1], sao_offset_sign [2][2], sao_offset_sign [2][3]} <= 4'd0;
					end
				end
				STATE_MERGE_LEFT_PUSH : begin
					if ( rx > 0 & same_slice_tile_left) begin
						state <= STATE_MERGE_LEFT_READ;
					end else begin
						sao_merge_left_flag <= 1'b0;
						state <= STATE_MERGE_UP_PUSH;
					end
				end
				STATE_MERGE_LEFT_READ : begin
					if (cabac_out_valid) begin
						sao_merge_left_flag <= cabac_out_data[0];
						if (cabac_out_data[0]) begin
							state <= STATE_SEND_H22;
						end else begin
							state <= STATE_MERGE_UP_PUSH;
						end
					end
				end
				STATE_MERGE_UP_PUSH : begin
					if ( ry > 0 & same_slice_tile_up) begin
						state <= STATE_MERGE_UP_READ;
					end else begin
						sao_merge_up_flag <= 1'b0;
						state <= STATE_TYPE_PUSH;
					end
				end
				STATE_MERGE_UP_READ : begin
					if (cabac_out_valid) begin
						sao_merge_up_flag <= cabac_out_data[0];
						if (cabac_out_data[0]) begin
							state <= STATE_SEND_H22;
						end else begin
							state <= STATE_TYPE_PUSH;
						end
					end
				end
				STATE_TYPE_PUSH : begin
					if ( (slice_sao_luma_flag & cIdx == 2'b00) | (slice_sao_chroma_flag & cIdx > 2'b00)) begin
						if (cIdx == 2'b00) begin
							state <= STATE_TYPE_READ;
						end else if (cIdx == 2'b01) begin
							state <= STATE_TYPE_READ;
						end else begin
							state <= STATE_OFFSET_PUSH;
							i <= 2'b00;
						end
					end else begin
						if (cIdx == 2'b10) begin
							state <= STATE_SEND_H22;
						end else begin
							cIdx <= cIdx + 1'b1;
						end
					end
				end
				STATE_TYPE_READ : begin
					if (cabac_out_valid) begin
						if (cIdx == 2'b00) begin
							SaoTypeIdx[0] <= cabac_out_data[1:0];
						end else begin
							SaoTypeIdx[1] <= cabac_out_data[1:0];
							SaoTypeIdx[2] <= cabac_out_data[1:0];
						end
						state <= STATE_OFFSET_PUSH;
						i <= 2'b00;
					end	
				end
				STATE_OFFSET_PUSH: begin
					if (SaoTypeIdx[cIdx] != 2'b00) begin
						if (i==2'b11) begin
							state <= STATE_OFFSET_READ;
						end 
						i <= i+1'b1;
					end else begin
						cIdx <= cIdx + 1'b1;
						if (cIdx == 2'b10) begin
							state <= STATE_SEND_H22;
						end else begin
							state <= STATE_TYPE_PUSH;
						end
					end
				end
				STATE_OFFSET_READ: begin
					if (cabac_out_valid) begin
						sao_offset_abs[cIdx][i] <= cabac_out_data[2:0];
						i <= i + 1'b1;
						if (i==2'b11) begin
							state <= STATE_SIGN_PUSH;
						end
					end
				end
				STATE_SIGN_PUSH: begin
					if (SaoTypeIdx[cIdx] == 2'b01) begin
						if (i==2'b11) begin
							state <= STATE_SIGN_READ;
						end 
						i <= i+1'b1;
					end else begin
						if (cIdx == 2'b10) begin
							state <= STATE_SEND_H22;
						end else begin
							state <= STATE_EO_CLASS_READ;
						end
					end
				end
				STATE_SIGN_READ: begin
					if (sao_offset_abs[cIdx][i] != 3'b000) begin
						if (cabac_out_valid) begin
							sao_offset_sign[cIdx][i] <= cabac_out_data[0];
							i <= i + 1'b1;
							if (i==2'b11) state <= STATE_BAND_POS_PUSH;
						end
					end else begin
						i <= i + 1'b1;
						if (i==2'b11) state <= STATE_BAND_POS_PUSH;
					end
				end
				STATE_BAND_POS_PUSH : begin
					state <= STATE_BAND_POS_READ;
				end
				STATE_BAND_POS_READ : begin
					if (cabac_out_valid) begin
						sao_band_position[cIdx] = cabac_out_data[5:0];
						if (cIdx == 2'b10) begin
							state <= STATE_SEND_H22;
						end else begin
							state <= STATE_TYPE_PUSH;
							cIdx <= cIdx + 1'b1;
						end
					end
				end
				STATE_EO_CLASS_READ: begin
					if (cabac_out_valid) begin
						if (cIdx == 2'b00) 
							sao_eo_class[0] = cabac_out_data[1:0];
						else begin
							sao_eo_class[1] = cabac_out_data[1:0];
							sao_eo_class[2] = cabac_out_data[1:0];
						end
						state <= STATE_TYPE_PUSH;
						cIdx <= cIdx + 1'b1;
					end
				end	
				STATE_SEND_H22 : begin
					if (!push_full) begin
						state <= STATE_SEND_H23;
						sao_buf_left[0] <= sao_word[0];
						//sao_buf_top [rx][0] <= sao_word[0];
					end
				end
				STATE_SEND_H23 : begin
					if (!push_full) begin
						state <= STATE_SEND_H24;
						sao_buf_left[1] <= sao_word[1];
						//sao_buf_top [rx][1] <= sao_word[1];
					end
				end
				STATE_SEND_H24 : begin
					if (!push_full) begin
						state <= STATE_SAO_DONE;
						sao_buf_left[2] <= sao_word[2];
						//sao_buf_top [rx][2] <= sao_word[2];
					end
				end
				STATE_SAO_DONE: begin
					state <= STATE_INIT;
				end
				
			endcase
		end
	end
	
	always @ (*) begin
		push_out = 32'dx;
		push_valid = 1'b0;
		sao_done = 1'b0;
		cabac_in_data = 32'dx;
		cabac_in_valid = 1'b0;
		cabac_out_read = 1'b0;
		y_wr_en = 1'b0;
		cb_wr_en = 1'b0;
		cr_wr_en = 1'b0;
		case (state) 
			STATE_MERGE_LEFT_PUSH : begin
				if ( rx > 0 & same_slice_tile_left) begin
					cabac_in_data = {8'd1, 24'd0};
					cabac_in_valid = 1'b1;
				end
			end
			STATE_MERGE_LEFT_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			STATE_MERGE_UP_PUSH : begin
				if ( ry > 0 & same_slice_tile_up) begin
					cabac_in_data = {8'd1, 24'd0};
					cabac_in_valid = 1'b1;
				end
			end
			STATE_MERGE_UP_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			STATE_TYPE_PUSH : begin
				if ( (slice_sao_luma_flag & cIdx == 2'b00) | (slice_sao_chroma_flag & cIdx > 2'b00)) begin
					if (cIdx == 2'b00) begin
						cabac_in_data = {8'd2, 24'd0};
						cabac_in_valid = 1'b1;
					end else if (cIdx == 2'b01) begin
						cabac_in_data = {8'd2, 24'd0};
						cabac_in_valid = 1'b1;
					end
				end
			end
			STATE_TYPE_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			STATE_OFFSET_PUSH: begin
				if (SaoTypeIdx[cIdx] != 2'b00) begin 
					cabac_in_data = {8'd4, 24'd0};
					cabac_in_valid = 1'b1;
				end
			end
			STATE_OFFSET_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			STATE_SIGN_PUSH: begin
				if (SaoTypeIdx[cIdx] == 2'b01) begin
					if (sao_offset_abs[cIdx][i]) begin
						cabac_in_data = {8'd5, 24'd0};
						cabac_in_valid = 1'b1;
					end 
				end else begin
					if (cIdx != 2'b10) begin
						cabac_in_data = {8'd6, 24'd0};
						cabac_in_valid = 1'b1;
					end
				end
			end
			STATE_SIGN_READ: begin
				if (sao_offset_abs[cIdx][i] != 3'b000) begin
					if (cabac_out_valid) begin
						cabac_out_read = 1'b1;
					end
				end
			end
			STATE_BAND_POS_PUSH : begin
				cabac_in_data = {8'd3, 24'd0};
				cabac_in_valid = 1'b1;
			end
			STATE_BAND_POS_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			STATE_EO_CLASS_READ : begin
				if (cabac_out_valid) begin
					cabac_out_read = 1'b1;
				end
			end
			STATE_SEND_H22 : begin
				if (!push_full) begin
					push_valid  = slice_sao_luma_flag;
					{push_out[7:0] , push_out[15:8], push_out[23:16], push_out[31:24]} =
						{sao_word[0], 8'h22};
					y_wr_en = 1'b1;
				end
			end
			STATE_SEND_H23 : begin
				if (!push_full) begin
					push_valid  = slice_sao_chroma_flag;
					{push_out[7:0] , push_out[15:8], push_out[23:16], push_out[31:24]} =
						{sao_word[1], 8'h23};
					cb_wr_en = 1'b1;
				end
			end
			STATE_SEND_H24 : begin
				if (!push_full) begin
					push_valid  = slice_sao_chroma_flag;
					{push_out[7:0] , push_out[15:8], push_out[23:16], push_out[31:24]} =
						{sao_word[2], 8'h24};
					cr_wr_en = 1'b1;
				end
			end
			STATE_SAO_DONE: begin
				sao_done = 1'b1;
			end
			
			
		endcase
	end
endmodule
