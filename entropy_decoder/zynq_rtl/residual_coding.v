`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:21:30 04/29/2014 
// Design Name: 
// Module Name:    residual_coding 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module residual_coding(
		clk,
		rst,	
		
		mmap_data,
		mmap_valid,
		
		cabac_in_data,
		cabac_in_valid,
		
		cabac_out_data,
		cabac_out_valid,
		cabac_out_read,
		
		out_fifo_data,
		out_fifo_valid,
		out_fifo_full,
		in_fifo_almost_empty,
		
		done,
		
		cIdx,
		trafoSize,
		scanIdx,
		sign_data_hiding_flag,
		cu_transquant_bypass_flag,
		
		state
    );

	 input clk;
	 input rst;
	 
	 input [8:0] mmap_data;
	 input mmap_valid;
	 
	 output reg [31:0] cabac_in_data;
	 output reg cabac_in_valid;
	 
	 input [31:0] cabac_out_data;
	 input cabac_out_valid;
	 output reg cabac_out_read;
	 
	 output reg [31:0] out_fifo_data;
	 output reg out_fifo_valid;
	 input out_fifo_full;
	 input in_fifo_almost_empty;
	 
	 output reg done;
	 
	 input [1:0] cIdx; // convert to input
	 input [1:0] scanIdx;
	 input [2:0] trafoSize; 
	 
	 input sign_data_hiding_flag;
	 input cu_transquant_bypass_flag;

	 output reg [4:0] state;
	 
	 localparam STATE_INIT = 5'd0;
	 localparam STATE_PUSH_X_PREFIX = 5'd1;
	 localparam STATE_PUSH_Y_PREFIX = 5'd2;
	 localparam STATE_PUSH_X_SUFFIX = 5'd3;
	 localparam STATE_PUSH_Y_SUFFIX = 5'd4;
	 localparam STATE_READ_X_SUFFIX = 5'd5;
	 localparam STATE_READ_Y_SUFFIX = 5'd6;
	 localparam STATE_SWAP = 5'd7;
	 localparam STATE_LASTSUBBLOCK = 5'd8;
	 localparam STATE_LASTSCANPOS = 5'd9;
	 localparam STATE_PUSH_CSBF = 5'd10;
	 localparam STATE_READ_CSBF = 5'd11;
	 localparam STATE_PUSH_SIG_COEFF = 5'd12;
	 localparam STATE_PUSH_ABS_GREATER_1 = 5'd13;
	 localparam STATE_PUSH_ABS_GREATER_2 = 5'd14;
	 localparam STATE_READ_ABS_GREATER_2 = 5'd15;
	 localparam STATE_PUSH_SIGN_FLAG = 5'd16;
	 localparam STATE_PUSH_REM = 5'd17;
	 localparam STATE_PUSH_ERU = 5'd18;
	 
	 localparam STATE_WAIT = 5'd31;
	 
	 // internal
	 reg [2:0] xS, yS;
	 reg [5:0] lastSubBlock,i;
	 reg [3:0] lastScanPos,n, read_n;
	 reg inferSbDcSigCoeffFlag, inferSbDcSigCoeffFlag_valid;
	 reg [1:0] ctxSet;
	 reg [3:0] greater1Ctx;
	 reg GreaterThanOneInPrevSubBlock;
	 reg finished_pushing;
	 
	 reg [3:0] firstSigScanPos ;
	 reg [3:0] numGreater1Flag, numGreater1Flag_rd;
	 reg [3:0] lastSigScanPos;
	 reg [4:0] lastGreater1ScanPos;
	 reg [3:0]  numSigCoeff_wr, numSigCoeff_rd;
	 reg absGreater1Wait;
	 reg signHidden;
	 reg [3:0] numSign_wr, numSign_rd;
	 reg sumAbsLevel;
	 
	 // combiational
	 reg [4:0] xC, yC; // wires
	 reg signed [15:0] coeffVal;
	 reg [15:0] coeffAbs;
	 wire [4:0] sig_coeff_ctx;
	 wire [1:0] baseLevel_wr, baseLevel_rd;
	
	 // syntax elements
	 reg [4:0] last_significant_coeff_x, last_significant_coeff_y;
	 reg [0:15] significant_coeff_flag, significant_coeff_flag_wr, significant_coeff_flag_rd;
	 reg [0:15] coeff_abs_level_greater1_flag;
	 reg [0:15] coeff_abs_level_greater2_flag;
	 reg [0:15] coeff_sign_flag;
	 reg [1:0] baseLevel [0:15];
	 
	 // largest_bit
	 wire [3:0] lbs_sig_coeff_wr, lbs_sig_coeff_rd;

	 //csbf line buffers
	 reg [0:7] csbf_d ;
	 reg [0:7] csbf_r ;
	 reg csbf_cur;
	 
	 
	// scanorder
	 reg [1:0] invScanOrder_blk_size;
	 reg [2:0] invScanOrder_x, invScanOrder_y;
	 wire [5:0] invScanOrder_invScanOrder;
	 
	 reg [1:0] scanOrder_blk_size;
	 wire [2:0] scanOrder_x, scanOrder_y;
	 reg [5:0] scanOrder_scanPos;	
	 
	 integer loop_i;
	 
	 assign baseLevel_wr = (coeff_abs_level_greater2_flag[lbs_sig_coeff_wr] ? 2'b11 : (coeff_abs_level_greater1_flag[lbs_sig_coeff_wr] ? 2'b10 : 2'b01)) ;
	 assign baseLevel_rd = (coeff_abs_level_greater2_flag[lbs_sig_coeff_rd] ? 2'b11 : (coeff_abs_level_greater1_flag[lbs_sig_coeff_rd] ? 2'b10 : 2'b01)) ;
	
		
	 invScanOrder invScanOrder(
		.blk_size(invScanOrder_blk_size),
      .scanIdx(scanIdx),
      .x(invScanOrder_x),
      .y(invScanOrder_y),
      .invScanOrder(invScanOrder_invScanOrder)
	 );
	 
	 scanOrder scanOrder(
		.blk_size(scanOrder_blk_size),
      .scanIdx(scanIdx),
      .scanPos(scanOrder_scanPos),
      .scanOrder( {scanOrder_x, scanOrder_y})
	 );
	 always @ (posedge clk) begin
		if (!rst) begin
			state <= STATE_INIT;
		end else begin
			case (state) 
				STATE_INIT: begin
					if (mmap_valid) begin
						state <= STATE_PUSH_X_PREFIX;
						csbf_d <= 8'd0;
						csbf_r <= 8'd0;
						absGreater1Wait <= 1'b0;
						significant_coeff_flag <= 16'd0;
						done <= 1'b0;
						GreaterThanOneInPrevSubBlock <= 1'b0;
					end
				end
				STATE_PUSH_X_PREFIX: begin // push coeff_x_prefix
					if (!in_fifo_almost_empty) state <= STATE_PUSH_Y_PREFIX;
				end
				STATE_PUSH_Y_PREFIX: begin // push coeff_y_prefix
					if (!in_fifo_almost_empty) state <= STATE_PUSH_X_SUFFIX;
				end
				STATE_PUSH_X_SUFFIX : begin // read coeff_x_prefix and push out suffix
					if (!in_fifo_almost_empty) begin
						if (cabac_out_valid) begin
							if (cabac_out_data[3:0] > 3 ) begin
								case (cabac_out_data[3:0]) // this is later directly added to suffix
									4: last_significant_coeff_x <= 5'd4;
									5: last_significant_coeff_x <= 5'd6;
									6: last_significant_coeff_x <= 5'd8;
									7: last_significant_coeff_x <= 5'd12;
									8: last_significant_coeff_x <= 5'd16;
									9: last_significant_coeff_x <= 5'd24;
								endcase
							end else begin
								last_significant_coeff_x <= cabac_out_data[3:0];			
							end
							state <= STATE_PUSH_Y_SUFFIX;
						end
					end
				end
				STATE_PUSH_Y_SUFFIX : begin // read coeff_y_prefix and push out suffix
					if (!in_fifo_almost_empty) begin
						if (cabac_out_valid) begin
							if (cabac_out_data[3:0] > 3 ) begin
								case (cabac_out_data[3:0]) 
									4: last_significant_coeff_y <= 5'd4;
									5: last_significant_coeff_y <= 5'd6;
									6: last_significant_coeff_y <= 5'd8;
									7: last_significant_coeff_y <= 5'd12;
									8: last_significant_coeff_y <= 5'd16;
									9: last_significant_coeff_y <= 5'd24;
								endcase
							end else begin
								last_significant_coeff_y <= cabac_out_data[3:0];
							end
							
							if (last_significant_coeff_x >3 )
								state <= STATE_READ_X_SUFFIX;
							else if (cabac_out_data[3:0] > 3) 
								state <= STATE_READ_Y_SUFFIX;
							else 
								state <= STATE_SWAP;
						end
					end
				end
				STATE_READ_X_SUFFIX : begin // read_x_suffix
					if (cabac_out_valid) begin
						last_significant_coeff_x <= last_significant_coeff_x + cabac_out_data[3:0];
							if (last_significant_coeff_y > 3) 
							state <= 6;
						else 
							state <= 7;
					end
					
				end
				STATE_READ_Y_SUFFIX : begin // read_y_suffix
					if (cabac_out_valid) begin
						last_significant_coeff_y <= last_significant_coeff_y + cabac_out_data[3:0];
						state <= 7;
					end
				end
				STATE_SWAP : begin // swap if scanIdx ==2
					if (scanIdx == 2'd2) begin
						{last_significant_coeff_y , last_significant_coeff_x} <= { last_significant_coeff_x, last_significant_coeff_y};
					end
					state <= STATE_LASTSUBBLOCK;
				end
				STATE_LASTSUBBLOCK : begin //8
					lastSubBlock <= invScanOrder_invScanOrder;
					i <= invScanOrder_invScanOrder;
					state <= STATE_LASTSCANPOS;
				end
				STATE_LASTSCANPOS : begin // 9
					lastScanPos <= invScanOrder_invScanOrder[3:0];
					state <= STATE_PUSH_CSBF;		
				end
				STATE_PUSH_CSBF: begin // 10
					if (!in_fifo_almost_empty) begin
						xS <= scanOrder_x;
						yS <= scanOrder_y;
						if ( (i< lastSubBlock) && (i>0)) begin
							inferSbDcSigCoeffFlag <= 1'b1;
							state <= STATE_READ_CSBF;
							// value of zero indicates we do not know, if inferSbDc is true;
							// in this wait until it becomes true;
							inferSbDcSigCoeffFlag_valid <= 1'b0;
						end else begin
							if (i==lastSubBlock &&  lastScanPos == 0) inferSbDcSigCoeffFlag <= 1;
							else inferSbDcSigCoeffFlag <= 0;
							inferSbDcSigCoeffFlag_valid <= 1'b1;
							// set to one here, if changing will set to zero in reading
							
							//csbf_cur <= 1'b1;
							if (i==lastSubBlock &&  lastScanPos == 0) begin
								state <= STATE_PUSH_ABS_GREATER_1;
							end else begin
								state <= STATE_PUSH_SIG_COEFF;
							end
						end
						if (i==lastSubBlock && lastScanPos ==0) begin
							n <= 4'd0;
							read_n <= 4'd0;
						end else begin
							n <= (i==lastSubBlock) ? lastScanPos - 1'b1 : 4'd15;
							read_n <= (i==lastSubBlock) ? lastScanPos - 1'b1 : 4'd15;
						end
						finished_pushing <= 1'b0;
						// not good practice
						significant_coeff_flag <= 16'd0;
						significant_coeff_flag_rd <= 16'd0;
						significant_coeff_flag_wr <= 16'd0;
						if (i==lastSubBlock) begin
							significant_coeff_flag[lastScanPos] <= 1'b1;
							significant_coeff_flag_wr[lastScanPos] <= 1'b1;
							significant_coeff_flag_rd[lastScanPos] <= 1'b1;
						end
						coeff_abs_level_greater1_flag <= 16'd0;
						coeff_abs_level_greater2_flag <= 16'd0;
						coeff_sign_flag <= 16'd0;
						ctxSet[1] <=( (i==0) || (cIdx>0) )? 1'b0 : 1'b1;
						ctxSet[0] <= GreaterThanOneInPrevSubBlock;
						greater1Ctx <= 3'd1;
						lastSigScanPos <= (i==lastSubBlock) ? lastScanPos:  4'd0; 
						firstSigScanPos <= (i==lastSubBlock) ? lastScanPos:  4'd0;
						numGreater1Flag <= 4'd0;
						numGreater1Flag_rd <= 4'd0;
						lastGreater1ScanPos <= 5'd31;
						numSign_rd <= 4'd0;
						numSign_wr <= 4'd0;
						sumAbsLevel <= 1'b0;
						for (loop_i=0;loop_i<16;loop_i=loop_i+1) begin
							baseLevel[loop_i] <= 2'b00;
						end
						 numSigCoeff_wr <= 4'd0;
						 numSigCoeff_rd <= 4'd0;
					end
				end
				STATE_READ_CSBF : begin
					if (cabac_out_valid) begin
						csbf_cur <= cabac_out_data[0];
						if (cabac_out_data[0]) begin
							state <= STATE_PUSH_SIG_COEFF;
						end else begin 
							// TODO if i==0 end of trafo
							csbf_d [xS] <= 1'b0;
							csbf_r [yS] <= 1'b0;
							//greater1Ctx <= 4'd0;
							i <= i-1'b1; 
							state <= STATE_PUSH_CSBF;
						end
					end
				end
				STATE_PUSH_SIG_COEFF: begin
					if (!in_fifo_almost_empty) begin
						GreaterThanOneInPrevSubBlock <= 1'b0;
						if (!in_fifo_almost_empty) begin
							 if (n==4'b0000) begin 
								if (inferSbDcSigCoeffFlag_valid) begin
									finished_pushing <= 1'b1;
								end
								// multiple drivers
								significant_coeff_flag[0] <= 1'b1; // this will get overriden if not
							end else begin
								n <= n-1'b1;
							end
						end
						if (cabac_out_valid) begin
							if (read_n == 4'd0 || ((read_n == 4'd1 ) & inferSbDcSigCoeffFlag & (inferSbDcSigCoeffFlag_valid | !cabac_out_data[0]))) begin
								state <= STATE_PUSH_ABS_GREATER_1;
								n <= lastSigScanPos;
								read_n <= lastSigScanPos;
								absGreater1Wait <= 1'b0;
								finished_pushing <= 1'b0;
								significant_coeff_flag_rd <= significant_coeff_flag;
								significant_coeff_flag_wr <= significant_coeff_flag;
								if (!cabac_out_data[0] && (significant_coeff_flag[1:15]==15'd0) && read_n ==4'd0) begin
									// case where first subblock is unused
									state <= STATE_PUSH_ERU;
									//done <= 1'b1;
								end
							end
							significant_coeff_flag[read_n] <= cabac_out_data[0];
							significant_coeff_flag_rd[read_n] <= cabac_out_data[0];
							significant_coeff_flag_wr[read_n] <= cabac_out_data[0];
							if (cabac_out_data[0]) begin
								inferSbDcSigCoeffFlag <= 1'b0;
								inferSbDcSigCoeffFlag_valid <= 1'b1;
								if (read_n != 4'd0 && lastSigScanPos == 4'd0)
									lastSigScanPos <= read_n;
								firstSigScanPos <= read_n;
							end else begin
								if (read_n == 4'd1) begin
									inferSbDcSigCoeffFlag_valid <= 1'b1;
								end
							end
							read_n <= read_n - 1'b1;
						end
					end
				end
				STATE_PUSH_ABS_GREATER_1: begin
					//sumAbsLevel <= (^significant_coeff_flag);
					if (!in_fifo_almost_empty) begin	  
						if ( (!absGreater1Wait || greater1Ctx == 4'd0) && !finished_pushing) begin
							significant_coeff_flag_wr[lbs_sig_coeff_wr] <= 1'b0;
							absGreater1Wait <= 1'b1;
							numGreater1Flag <= numGreater1Flag + 1'b1;
							if (lbs_sig_coeff_wr==firstSigScanPos || numGreater1Flag == 4'd7 ) begin 
								finished_pushing <= 1'b1;
							end
						end
						if (cabac_out_valid) begin
							numGreater1Flag_rd <= numGreater1Flag_rd + 1'b1;
							absGreater1Wait <= 1'b0;
							significant_coeff_flag_rd[lbs_sig_coeff_rd] <= 1'b0;
							coeff_abs_level_greater1_flag[lbs_sig_coeff_rd] <= cabac_out_data[0];
							if (lbs_sig_coeff_rd == firstSigScanPos || numGreater1Flag_rd == 4'd7) begin
								state <= STATE_PUSH_ABS_GREATER_2;
							end
							if (cabac_out_data[0]) begin
								greater1Ctx <= 4'd0; 
								GreaterThanOneInPrevSubBlock <= 1'b1;
								if (lastGreater1ScanPos == 5'd31) begin
									lastGreater1ScanPos <= lbs_sig_coeff_rd;
								end
							end else begin
								if (greater1Ctx != 4'd0) begin
									greater1Ctx <= greater1Ctx + 1'b1;
								end
							end
						end
					end
				end
				STATE_PUSH_ABS_GREATER_2: begin
					//sumAbsLevel <= sumAbsLevel ^ (^significant_coeff_flag);
					if (!in_fifo_almost_empty) begin
						significant_coeff_flag_rd <= significant_coeff_flag;
						significant_coeff_flag_wr <= significant_coeff_flag;
						finished_pushing <= 1'b0;
						signHidden = ( ((lastSigScanPos - firstSigScanPos) > 3) & !cu_transquant_bypass_flag);
						if (lastGreater1ScanPos != 5'd31) begin
							state <= STATE_READ_ABS_GREATER_2;
						end else begin
							state <= STATE_PUSH_SIGN_FLAG;
						end
					end
				end
				STATE_READ_ABS_GREATER_2: begin
					if (cabac_out_valid) begin
						state <= STATE_PUSH_SIGN_FLAG;
						coeff_abs_level_greater2_flag[lastGreater1ScanPos] <= cabac_out_data[0];
						//sumAbsLevel <= sumAbsLevel ^ (cabac_out_data[0]);
					end
				end
				STATE_PUSH_SIGN_FLAG: begin
					if (!in_fifo_almost_empty) begin
						//significant_coeff_flag_wr[lbs_sig_coeff_wr] <= 1'b0;
						if (lbs_sig_coeff_wr == firstSigScanPos) begin
							finished_pushing <= 1'b1;
						end
						if ( cabac_in_valid) begin
							numSign_wr <= numSign_wr + 1'b1;
						end
					
						if (cabac_out_valid) begin
							significant_coeff_flag_rd[lbs_sig_coeff_rd] <= 1'b0;
							coeff_sign_flag[lbs_sig_coeff_rd] <= cabac_out_data[0];
							numSign_rd <= numSign_rd + 1'b1;
							if ((numSign_rd == numSign_wr-1'b1) & finished_pushing) begin
								state <= STATE_PUSH_REM;
								finished_pushing <= 1'b0; // MULTIPLE DRIVERS
								significant_coeff_flag_rd <= significant_coeff_flag;
							end else begin
								significant_coeff_flag_rd[lbs_sig_coeff_rd] <= 1'b0;
							end
						end else begin
							if ((numSign_rd == numSign_wr) & finished_pushing) begin
								state <= STATE_PUSH_REM;
								finished_pushing <= 1'b0; // MULTIPLE DRIVERS
								significant_coeff_flag_rd <= significant_coeff_flag;
							end
						end
						if (!finished_pushing) begin
							if (!in_fifo_almost_empty)
								significant_coeff_flag_wr[lbs_sig_coeff_wr] <= 1'b0;
						end else begin
							significant_coeff_flag_wr <= significant_coeff_flag;
						end
					end
				end
				STATE_PUSH_REM: begin
					// cabac_push
					if (!in_fifo_almost_empty) begin
						if (baseLevel_wr == ( ( numSigCoeff_wr < 8) ? (( {1'b0 ,lbs_sig_coeff_wr}==lastGreater1ScanPos) ? 2'd3 : 2'd2) : 2'd1)) begin
							// nothing sequential
						end 
						significant_coeff_flag_wr[lbs_sig_coeff_wr] <= 1'b0;
						baseLevel[lbs_sig_coeff_wr] <= baseLevel_wr;
						if (lbs_sig_coeff_wr == firstSigScanPos) begin
							finished_pushing <= 1'b1;
						end
						 numSigCoeff_wr <=  numSigCoeff_wr + 1'b1;
					
						// read
						if (! out_fifo_full) begin
							// update signiicance map
							if (baseLevel_rd == ( ( numSigCoeff_rd < 8) ? (( {1'b0 ,lbs_sig_coeff_rd}==lastGreater1ScanPos) ? 2'd3 : 2'd2) : 2'd1)) begin
								if (cabac_out_valid) begin
									significant_coeff_flag_rd[lbs_sig_coeff_rd] <= 1'b0;
									numSigCoeff_rd <=  numSigCoeff_rd + 1'b1;
								end
							end else begin
								significant_coeff_flag_rd[lbs_sig_coeff_rd] <= 1'b0;
								numSigCoeff_rd <=  numSigCoeff_rd + 1'b1;
							end
							
							
							if (out_fifo_valid) begin
								sumAbsLevel <= (sumAbsLevel ^ coeffAbs[0]);
								if (lbs_sig_coeff_rd == firstSigScanPos) begin
									if (i==6'd0) begin
										state <= STATE_PUSH_ERU;
										//done <= 1'b1;
									end else begin
										i <= i - 1'b1;
										csbf_d[xS] <= 1'b1;
										csbf_r[yS] <= 1'b1;
										state <= STATE_PUSH_CSBF;
									end
								end
							end
						end
					end
				end
				STATE_PUSH_ERU: begin
					if (!out_fifo_full) begin
						state <= STATE_INIT;
						done <= 1'b1;
					end
				end
			endcase
		end
	 end
	 
	 always @ (*) begin
		cabac_out_read = 1'b0;
		cabac_in_data = 32'dx;
		cabac_in_valid = 1'b0;
		invScanOrder_blk_size = 2'bx;
		invScanOrder_x = 3'bx;
		invScanOrder_y = 3'bx;
		scanOrder_scanPos = 5'bx;
		scanOrder_blk_size = 2'bx;
		xC = 5'dx;
		yC = 5'dx;
		out_fifo_valid = 1'b0;
		coeffVal = 16'd0;
		coeffAbs = 16'd0;
		out_fifo_data = 32'dx;
		
		if (~rst) begin
			cabac_out_read = 1'b0;
		end else begin
			case (state) 
				STATE_PUSH_X_PREFIX: begin
					cabac_in_data = { 8'd36, trafoSize, cIdx, 19'd0};
					cabac_in_valid = ~in_fifo_almost_empty;
					
				end 
				STATE_PUSH_Y_PREFIX: begin
					cabac_in_data = { 8'd37, trafoSize, cIdx, 19'd0};
					cabac_in_valid = ~in_fifo_almost_empty;
				end 
				STATE_PUSH_X_SUFFIX: begin
					if (!in_fifo_almost_empty) begin
						if (cabac_out_valid) begin
							if (cabac_out_data[3:0] > 3 ) begin
								cabac_in_data = { 8'd38, cabac_out_data[3:0], 20'd0};
								cabac_in_valid = 1;
							end
							cabac_out_read = 1'b1;
						end
					end
				end
				STATE_PUSH_Y_SUFFIX: begin
					if (!in_fifo_almost_empty) begin
						if (cabac_out_valid) begin
							if (cabac_out_data[3:0] > 3 ) begin
								cabac_in_data = { 8'd39, cabac_out_data[3:0], 20'd0};
								cabac_in_valid = 1;
							end
							cabac_out_read = 1'b1;
						end
					end
				end
				STATE_READ_X_SUFFIX, STATE_READ_Y_SUFFIX : begin //5,6
					if (cabac_out_valid) cabac_out_read = 1'b1;
				end
				STATE_LASTSUBBLOCK: begin
					invScanOrder_blk_size = (trafoSize - 2'd2) % 4 ;
					invScanOrder_x = last_significant_coeff_x[4:2];
					invScanOrder_y = last_significant_coeff_y[4:2];
				end
				STATE_LASTSCANPOS: begin
					invScanOrder_blk_size = 2;
					invScanOrder_x = last_significant_coeff_x[1:0];
					invScanOrder_y = last_significant_coeff_y[1:0];
				end
				STATE_PUSH_CSBF: begin
					scanOrder_blk_size = (trafoSize - 2'd2) % 4 ;
					scanOrder_scanPos = i;
					
					cabac_in_data = { 8'd40, trafoSize, cIdx, csbf_d[scanOrder_x], csbf_r[scanOrder_y], 17'd0};
					if ( (i< lastSubBlock) && (i>0)) begin
						if (!in_fifo_almost_empty) cabac_in_valid = 1;
					end
				end
				STATE_READ_CSBF: begin
					if (cabac_out_valid) cabac_out_read = 1'b1;
				end
				STATE_PUSH_SIG_COEFF : begin
					if (!in_fifo_almost_empty) begin
						scanOrder_blk_size = 2'b10 ;
						scanOrder_scanPos = n;
						xC = {xS , scanOrder_x[1:0]};
						yC = {yS , scanOrder_y[1:0]};
						if ( (n>0 ||  (!inferSbDcSigCoeffFlag & inferSbDcSigCoeffFlag_valid) ) & !finished_pushing ) begin
							cabac_in_data = { 8'd41, trafoSize, cIdx, csbf_d[xS], csbf_r[yS], xC, yC, scanIdx, sig_coeff_ctx};
							cabac_in_valid = 1;
						end
						if (cabac_out_valid) cabac_out_read = 1'b1;
					end
					
				end
				STATE_PUSH_ABS_GREATER_1: begin
					if (!in_fifo_almost_empty) begin
						if (  (!absGreater1Wait || greater1Ctx == 4'd0) & !finished_pushing) begin
							cabac_in_data = { 8'd42, 3'b000, cIdx, greater1Ctx[3:0], ctxSet, 13'd0};
							cabac_in_valid = 1'b1;
						end
						if (cabac_out_valid) cabac_out_read = 1'b1;
					end
					
				end
				
				STATE_PUSH_ABS_GREATER_2: begin
					if (!in_fifo_almost_empty) begin
						if (lastGreater1ScanPos != 5'd31) begin
							cabac_in_data = {8'd43, 3'b000, cIdx, 4'b0000, ctxSet, 13'd0};
							cabac_in_valid = 1;
						end
					end
				end
				STATE_READ_ABS_GREATER_2: begin
					if (cabac_out_valid) begin
						cabac_out_read = 1'b1;
					end
				end
				STATE_PUSH_SIGN_FLAG: begin
					if (!in_fifo_almost_empty) begin
						if ( ((lbs_sig_coeff_wr != firstSigScanPos) | !sign_data_hiding_flag | !signHidden) & !finished_pushing) begin
							cabac_in_data = {8'd45, 24'd0};
							cabac_in_valid = 1'b1;
						end
						if (cabac_out_valid) cabac_out_read = 1'b1;
					end
					
				end
				STATE_PUSH_REM: begin
					
					if (!in_fifo_almost_empty) begin
						if (baseLevel_wr == ( ( numSigCoeff_wr < 8) ? (( {1'b0 ,lbs_sig_coeff_wr}==lastGreater1ScanPos) ? 2'd3 : 2'd2) : 2'd1)) begin
							if (!finished_pushing) begin
								cabac_in_data = {8'd44,baseLevel_wr,  22'd0};
								cabac_in_valid = 1;
							end
						end
					
						if (!out_fifo_full) begin
							if (baseLevel_rd == ( ( numSigCoeff_rd < 8) ? (( {1'b0 ,lbs_sig_coeff_rd}==lastGreater1ScanPos) ? 2'd3 : 2'd2) : 2'd1)) begin
								if (cabac_out_valid) begin
									coeffAbs = (cabac_out_data[15:0] + baseLevel_rd);
									out_fifo_valid = 1'b1;
									cabac_out_read = 1'b1;
								end
							end else begin
								coeffAbs = baseLevel_rd;
								out_fifo_valid = 1'b1;
							end	
							
							if (signHidden & sign_data_hiding_flag & (lbs_sig_coeff_rd==firstSigScanPos) & (sumAbsLevel ^ coeffAbs[0])) begin
								coeffVal = -coeffAbs;
							end else begin
								if (coeff_sign_flag[lbs_sig_coeff_rd]) begin
									coeffVal = -coeffAbs;
								end else begin
									coeffVal = coeffAbs;
								end
							end
							scanOrder_blk_size = 2'b10 ;
							scanOrder_scanPos = lbs_sig_coeff_rd;
							{out_fifo_data[7:0],out_fifo_data[15:8] ,out_fifo_data[23:16] ,out_fifo_data[31:24] } = { coeffVal ,{yS , scanOrder_y[1:0]} ,{xS , scanOrder_x[1:0]},  6'd63};
						end
					end
				end
				STATE_PUSH_ERU: begin
					if (!out_fifo_full) begin
						{out_fifo_data[7:0],out_fifo_data[15:8] ,out_fifo_data[23:16] ,out_fifo_data[31:24] } = { 26'd0,  6'd62};
						out_fifo_valid = 1'b1;
					end
				end
			endcase
		end
	end
	
	sig_ctx sig_ctx(
		.xC( {xS , scanOrder_x[1:0]} ),
		.yC( {yS , scanOrder_y[1:0]} ),
		.prevCsbf( {csbf_d[xS], csbf_r[yS]}),
		.cIdx ( cIdx),
		.log2TrafoSize(trafoSize),
		.scanIdx( scanIdx),
		.sig_ctx(sig_coeff_ctx)
	);
	
	 largest_bit_set_16 lbs_sig_coeff_wr_inst(
		.bitmap(significant_coeff_flag_wr),
		.index(lbs_sig_coeff_wr),
		.no_bit_set()
    );
	 
	  largest_bit_set_16 lbs_sig_coeff_rd_inst(
		.bitmap(significant_coeff_flag_rd),
		.index(lbs_sig_coeff_rd),
		.no_bit_set()
    );
endmodule
