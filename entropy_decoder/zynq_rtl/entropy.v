`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    02:51:53 11/17/2013 
// Design Name: 
// Module Name:    main 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module entropy(
		clk,
		rst,
		
		axi_araddr,
		axi_arburst,
		axi_arcache,
		axi_arid,
		axi_arlen,
		axi_arlock,
		axi_arprot,
		axi_arready,
		axi_arsize,
		axi_arvalid,
		
		axi_awaddr,
		axi_awburst,
		axi_awcache,
		axi_awid,
		axi_awlen,
		axi_awlock,
		axi_awprot,
		axi_awready,
		axi_awsize,
		axi_awvalid,
		
		axi_bid,
		axi_bready,
		axi_bresp,
		axi_bvalid,
		
		axi_rdata,
		axi_rid,
		axi_rlast,
		axi_rready,
		axi_rresp,
		axi_rvalid,
		
		axi_wdata,
		axi_wlast,
		axi_wready,
		axi_wstrb,
		axi_wvalid,
		
		cabac_to_residual_data,
		cabac_to_residual_empty,
		cabac_to_residual_read,
        
		// test
		input_hevc_full_out_test,
		input_hevc_empty_out_test,
		input_hevc_almost_empty_out_test,

		push_full_out_test,
		push_empty_out_test,
		
		B_test,
		cu_state,
		ru_state,
		cu_x0,
		cu_y0,
		cu_other,
		cabac_to_res_data_count,
		input_hevc_data_count,
		cabac_to_res_data,
		cabac_to_res_wr_en,
		
		axi_debug,
    );

	
	input 			clk; 
	input 			rst;
	    
	input [15:0] 	axi_araddr;
	input [1:0] 	axi_arburst;
	input [3:0] 	axi_arcache;
	input [11:0] 	axi_arid;
	input [7:0] 	axi_arlen;
	input 			axi_arlock;
	input [2:0]		axi_arprot;
	output 			axi_arready;
	input [2:0]		axi_arsize;
	input				axi_arvalid;
	
	input [15:0] 	axi_awaddr;
	input [1:0] 	axi_awburst;
	input [3:0] 	axi_awcache;
	input [11:0] 	axi_awid;
	input [7:0] 	axi_awlen;
	input 			axi_awlock;
	input [2:0]		axi_awprot;
	output 			axi_awready;
	input [2:0]		axi_awsize;
	input				axi_awvalid;
	
	output [11:0] 	axi_bid;
	input				axi_bready;
	output [1:0] 	axi_bresp;
	output			axi_bvalid;
	
	output [31:0] 	axi_rdata;
	output [11:0]	axi_rid;
	output			axi_rlast;
	input				axi_rready;
	output [1:0] 	axi_rresp;
	output			axi_rvalid;
	
	input [31:0] 	axi_wdata;
	input				axi_wlast;
	output			axi_wready;
	input [3:0]		axi_wstrb;
	input				axi_wvalid;
	
	output [6:0] 	cu_state;
	output [4:0] 	ru_state;
	output [31:0]	B_test;
	output [11:0] 	cu_x0, cu_y0;
	output [255:0] cu_other;

	
	output [31:0] 	cabac_to_residual_data;
	output 			cabac_to_residual_empty;
	input				cabac_to_residual_read;
    
    //test
	output    		input_hevc_full_out_test;
	output    		input_hevc_empty_out_test;
	output   		input_hevc_almost_empty_out_test;
	  
	output    		push_full_out_test;
	output	  		push_empty_out_test;
	output [9:0] 	cabac_to_res_data_count;
	output [13:0] 	input_hevc_data_count;

	output [31:0] 	cabac_to_res_data;
	output 			cabac_to_res_wr_en;
	output [95:0]  axi_debug;
	
	reg in_fifo_almost_empty;
	wire prog_empty;
	wire [31:0] read_in;
	wire read_empty;
	wire read_next;
	
	wire IO_Addr_Strobe; // output IO_Addr_Strobe
	wire IO_Read_Strobe; // output IO_Read_Strobe
	wire IO_Write_Strobe; // output IO_Write_Strobe
	wire [31:0] IO_Address; // output [31 : 0] IO_Address
	wire [31:0] IO_Write_Data; // output [31 : 0] IO_Write_Data
	wire [31:0] IO_Read_Data; // input [31 : 0] IO_Read_Data
	wire IO_Ready; //
	
	wire[7:0] annexb_next_bits;
	wire annexb_out_valid;
	wire [3:0] annexb_read_bits;
	wire annexb_read_valid;
	wire annexb_control;
	wire annexb_offload_override;
	wire [4:0] annexb_pos;
	wire annexb_rst;
	
	wire [31:0] entropy_in;
	wire entropy_valid;
	wire entropy_full;
	
	wire  [31:0] push_data;
	wire  push_valid;
	wire push_full;
	
	wire [31:0] cabac_in_data;
	wire cabac_in_valid;
	
	wire [31:0] cabac_in_fifo_dout;
	wire cabac_in_fifo_read;
	wire cabac_in_fifo_empty;
	
	wire [31:0] cabac_out_data;
	wire cabac_out_valid;
	wire cabac_out_empty;
	
	wire [31:0] cabac_out_fifo_din;
	wire cabac_out_fifo_valid;
	wire [31:0] cabac_out_fifo_dout;
	wire cabac_out_fifo_empty;
	
	wire [31:0] cabac_in_fifo_din;
	wire cabac_in_fifo_valid;
	
	wire [6:0] cabac_QP;
	wire [1:0] cabac_init_type;
	wire [1:0] cabac_op;
	wire cabac_op_valid;
	wire cabac_op_done;
	
	wire [3:0] read_bits_cabac, read_bits_mmap;
	wire read_valid_cabac, read_valid_mmap;
	
	//wire cabac_reset;
	
	assign annexb_read_bits = (annexb_control && !annexb_offload_override) ? read_bits_cabac : read_bits_mmap;
	assign annexb_read_valid = (annexb_control && !annexb_offload_override) ? read_valid_cabac : read_valid_mmap;
	
	assign cabac_to_res_data = push_data;
	assign cabac_to_res_wr_en = push_valid;
	
	
	AnnexB_extract annexb(
			.clk(clk),
			.rst(annexb_rst),
			
			.read_next(read_next),
			.read_in(read_in),
			.read_empty(read_empty),
			
			.next_bits(annexb_next_bits),
			.read_bits(annexb_read_bits),
			.read_valid(annexb_read_valid),
			.out_valid(annexb_out_valid),
			
			.skip_to_NAL(annexb_skip_to_NAL),
			
			.pos(annexb_pos),
            .B_test(/*B_test*/)
		);
		
	axi_mmap mmap (
		.clk(clk),
		.rst(rst),
		
		.axi_araddr	  (axi_araddr	),
		.axi_arburst  (axi_arburst ),
		.axi_arcache  (axi_arcache ),
		.axi_arid     (axi_arid    ),
		.axi_arlen    (axi_arlen	),
		.axi_arlock   (axi_arlock	),
		.axi_arprot   (axi_arprot	),
		.axi_arready  (axi_arready	),
		.axi_arsize   (axi_arsize	),
		.axi_arvalid  (axi_arvalid	),
		               
		.axi_awaddr   (axi_awaddr	),
		.axi_awburst  (axi_awburst	),
		.axi_awcache  (axi_awcache	),
		.axi_awid     (axi_awid		),
		.axi_awlen    (axi_awlen	),
		.axi_awlock   (axi_awlock	),
		.axi_awprot   (axi_awprot	),
		.axi_awready  (axi_awready	),
		.axi_awsize   (axi_awsize	),
		.axi_awvalid  (axi_awvalid	),
		               
		.axi_bid      (axi_bid		),
		.axi_bready   (axi_bready	),
		.axi_bresp    (axi_bresp	),
		.axi_bvalid   (axi_bvalid	),
		               
		.axi_rdata    (axi_rdata	),
		.axi_rid      (axi_rid		),
		.axi_rlast    (axi_rlast	),
		.axi_rready   (axi_rready	),
		.axi_rresp    (axi_rresp	),
		.axi_rvalid   (axi_rvalid	),
		               
		.axi_wdata    (axi_wdata	),
		.axi_wlast    (axi_wlast	),
		.axi_wready   (axi_wready	),
		.axi_wstrb    (axi_wstrb	),
		.axi_wvalid   (axi_wvalid	),
		
		.annexb_next_bits(annexb_next_bits),
	   .annexb_out_valid(annexb_out_valid),
	   .annexb_read_bits(read_bits_mmap),
	   .annexb_read_valid(read_valid_mmap),
		.annexb_control(annexb_control),
		.annexb_offload_override(annexb_offload_override),
		.annexb_skip_to_NAL(annexb_skip_to_NAL),
		.annexb_pos(annexb_pos),
		.annexb_rst(annexb_rst),
		
		.entropy_in(entropy_in),
		.entropy_valid(entropy_valid),
		.entropy_full(entropy_full),
		
		.push_data(push_data),
		.push_valid(push_valid),
		.push_full(push_full),
		.in_fifo_almost_empty(in_fifo_almost_empty),
		
		.input_hevc_data_count(input_hevc_data_count),
		.cabac_to_res_data_count(cabac_to_res_data_count),
		
		.cabac_QP(cabac_QP),
		.cabac_init_type(cabac_init_type),
		.cabac_op(cabac_op),
		.cabac_op_valid(cabac_op_valid),
		.cabac_op_done(cabac_op_done),
		
		.cabac_in_fifo_din(cabac_in_fifo_din),
		.cabac_in_fifo_valid(cabac_in_fifo_valid),
		
		.cabac_out_fifo_dout(cabac_out_fifo_dout),
		.cabac_out_fifo_empty(cabac_out_fifo_empty),
		.cabac_out_fifo_read(cabac_out_fifo_read),
	   .cu_state			(cu_state),
	   .ru_state			(ru_state),
		.cu_x0				(cu_x0),
		.cu_y0				(cu_y0),
		.cu_other			(cu_other),
		.axi_debug			(axi_debug)
	);
	
//	fifo push (
//	  .clk(clk), // input clk
//	  .rst(~rst), // input rst
//	  .din(push_data), // input [31 : 0] din
//	  //.wr_en(push_valid), // input wr_en
//	  .wr_en(1'b0), // input wr_en // changed for TESTING
//	  //.rd_en(rd_en), // input rd_en
//	  //.dout(dout), // output [31 : 0] dout
//	  .full(push_full) // output full
//	  //.empty(empty) // output empty
//	);
//	

//	fifo input_hevc (
//	  .clk(clk), // input clk
//	  .rst(~rst), // input rst
//	  .din(entropy_in), // input [31 : 0] din
//	  .wr_en(entropy_valid), // input wr_en
//	  //.wr_en(1'b0), // input wr_en // changed for TESTING
//	  .rd_en(read_next), // input rd_en
//	  .dout(read_in), // output [31 : 0] dout
//	  .full(entropy_full), // output full
//	  .empty(read_empty) // output empty
//	);
	
	reg in_fifo_rst;
	reg [2:0] rst_count;
	reg rst_state; //0 - on
	
	always @(posedge clk) begin
		if (~rst) begin
			rst_count <= 3'd4;
			rst_state <= 1'b0;
		end else begin
			rst_count <= rst_count -1'b1;
			case (rst_state)
				0: begin
					if (rst_count ==0) begin
						rst_state <= 1'b1;
					end
				end
				1: rst_state <= 1'b1;
			endcase
		end
	end
	
	always @(*) begin
		if (~rst) begin
			in_fifo_rst <= 1'b0;
		end else begin
			case (rst_state)
				0: in_fifo_rst <= 1'b0;
				1: in_fifo_rst <= 1'b1;
			endcase
		end
	end
	
	always @(posedge clk)
		in_fifo_almost_empty <= prog_empty;
	
	fifo_input input_hevc (
	  //.wr_clk(wr_clk), // input clk
	  //.rd_clk(clk),
	  .clk(clk),
	  .rst(~in_fifo_rst), // input rst
	  .din(entropy_in), // input [31 : 0] din
	  .wr_en(entropy_valid), // input wr_en
	  .rd_en(read_next), // input rd_en
	  .dout(read_in), // output [31 : 0] dout
	  .full(), // output full
	  .overflow(),
	  .data_count(input_hevc_data_count),
	  .empty(read_empty), // output empty
	  .prog_empty(prog_empty),
	  .prog_full(entropy_full) // output prog_full
	);
    
    assign B_test = read_in;
    
    

	fifo_cabac_to_res push (
	  .clk(clk), // input clk
	  .rst(~rst), // input rst
	  .din(push_data), // input [31 : 0] din
	  .wr_en(push_valid), // input wr_en
	  //.wr_en(1'b0), // input wr_en // changed for TESTING
	  .rd_en(cabac_to_residual_read), // input rd_en
	  .dout(cabac_to_residual_data), // output [31 : 0] dout
	  .full(),
	  .data_count(cabac_to_res_data_count),
	  .prog_full(push_full), // output full
	 // .prog_empty(cabac_to_residual_empty), // output empty
	  .empty(cabac_to_residual_empty)
	);
	/*
	config_fifo_monitor #(
			.STRING("cabac_to_res"),
			.FILE_NAME("test_files/cabac_to_residual_soft"),
			.OUT_VERIFY (1),
			.DEBUG (0)
	) cabac_to_residual_monitor (
		.clk(clk),
		.reset(~rst),
		.out(cabac_to_residual_data),
		.empty(cabac_to_residual_empty),
		.rd_en(cabac_to_residual_read)
	);
	
*/
			

//manupa added hard large fifo

//    fifo push (
//      .clk(clk), // input clk
//      .rst(~rst), // input rst
//      .din(push_data), // input [31 : 0] din
//      //.wr_en(push_valid), // input wr_en
//		.wr_en(1'b0),
//      .rd_en(cabac_to_residual_read), // input rd_en
//      .dout(cabac_to_residual_data), // output [31 : 0] dout
//      .full(), // output full
//      .empty(), // output empty
//      .prog_full(push_full), // output prog_full
//      .prog_empty(cabac_to_residual_empty) // output prog_empty
//    );

	//assign push_full = 1'b0;
	
//	fifo cabac_in (
//	  .clk(clk), // input clk
//	  .rst(~rst), // input rst
//	  .din(cabac_in_fifo_din), // input [31 : 0] din
//	  .wr_en(cabac_in_fifo_valid), // input wr_en
//	  .rd_en(cabac_in_fifo_read), // input rd_en
//	  .dout(cabac_in_fifo_dout), // output [31 : 0] dout
//	  //.full(push_full) // output full
//	  .empty(cabac_in_fifo_empty) // output empty
//	);
	
	zero_delay_fifo cabac_in (
	  .clk(clk), // input clk
	  .rst(rst), // input rst
	  .din(cabac_in_fifo_din), // input [31 : 0] din
	  .wr_en(cabac_in_fifo_valid), // input wr_en
	  .rd_en(cabac_in_fifo_read), // input rd_en
	  .dout(cabac_in_fifo_dout), // output [31 : 0] dout
	  //.full(push_full) // output full
	  .empty(cabac_in_fifo_empty) // output empty
	);
	
	zero_delay_fifo cabac_out (
	  .clk(clk), // input clk
	  .rst(rst), // input rst
	  .din(cabac_out_fifo_din), // input [31 : 0] din
	  .wr_en(cabac_out_fifo_valid), // input wr_en
	  .rd_en(cabac_out_fifo_read), // input rd_en
	  .dout(cabac_out_fifo_dout), // output [31 : 0] dout
	  //.full(push_full) // output full
	  .empty(cabac_out_fifo_empty) // output empty
	);
	cabac cabac(
		.clk(clk),
		.rst(rst),
		.se_type(cabac_in_fifo_dout),
		.se_empty(cabac_in_fifo_empty ),
		.se_read(cabac_in_fifo_read),
		
		.decoded_se(cabac_out_fifo_din),
		.decoded_valid(cabac_out_fifo_valid),
		.cabac_init_type(cabac_init_type),
		.QP(cabac_QP),
		.op(cabac_op),
		.op_valid(cabac_op_valid),
		.op_done(cabac_op_done),

		.next_bits(annexb_next_bits),
		.next_bits_valid(annexb_out_valid),
		//.in_fifo_almost_empty(in_fifo_almost_empty),
		.read_bits(read_bits_cabac),
		.read_valid(read_valid_cabac)
	);

    assign input_hevc_almost_empty_out_test = prog_empty;
    assign input_hevc_empty_out_test = read_empty;
    assign input_hevc_full_out_test = entropy_full;
    
    assign push_empty_out_test = cabac_to_residual_empty;
    assign push_full_out_test = push_full;

endmodule
