`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:54:13 12/07/2013 
// Design Name: 
// Module Name:    zero_delay_fifo 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module zero_delay_fifo(
		clk, 
		rst,
		din,
		wr_en,
		rd_en,
		dout,
		empty
    );
	 
	 input clk;
	 input rst;
	 input [31:0] din;
	 input wr_en, rd_en;
	 (* register_balancing = "no" *)
	 output reg [31:0] dout;
	 output reg empty;

	/*
	reg [31:0] first;
	reg first_valid;
	
	wire f_empty;
	
	fifo f (
	  .clk(clk), // input clk
	  .rst(rst), // input rst
	  .din(din), // input [31 : 0] din
	  .wr_en(wr_en), // input wr_en
	  .rd_en(rd_en), // input rd_en
	  .dout(dout), // output [31 : 0] dout
	  //.full(full), // output full
	  .empty(f_empty) // output empty
	);
	
	always @(posedge clk) begin
		if (rst) begin
			first_valid <= 1'b0;
		end else begin
			if (wr_en) begin
				if (~first_valid)
			end
		end
	end
	*/
	
	 reg [31:0]       mem [31:0];
	 reg [4:0] wr_pointer, rd_pointer;
	 wire [4:0] new_rd_pointer, new_wr_pointer;
	 
	 assign new_rd_pointer = (rd_en & ~empty) ? rd_pointer + 1'b1 : rd_pointer;
	 assign new_wr_pointer = (wr_en) ? wr_pointer + 1'b1 : wr_pointer;

//	zdfifo_bram f (
//	  .clka(clk), // input clka
//	  .wea(wr_en), // input [0 : 0] wea
//	  .addra(wr_pointer), // input [3 : 0] addra
//	  .dina(din), // input [31 : 0] dina
//	  .clkb(clk), // input clkb
//	  .addrb(new_rd_pointer), // input [3 : 0] addrb
//	  .doutb(dout) // output [31 : 0] doutb
//	);
   always @ (posedge clk) begin
		if (~rst) begin
			empty <= 1'b1;
		end else begin 
			if (wr_en) begin
				mem[wr_pointer] <= #1 din;
			end
			dout <= (wr_en & (wr_pointer==new_rd_pointer)) ? din :  mem[new_rd_pointer];
			empty <= (new_wr_pointer==new_rd_pointer);
		end
   end

   //assign #1 dout = mem[rd_pointer];
	//always @ (posedge clk) begin
	//	if (~rst) begin
	//		dout <= #1 0;
	//	end else  begin
	//		dout <= #1 mem[rd_pointer];
	//	end
	//end
	//assign 
   always @ (posedge clk) begin
      if (~rst) begin
         wr_pointer <=  0;
			
      end else if (wr_en) begin
         wr_pointer <=  wr_pointer + 1'b1;
      end
   end

   always @ (posedge clk) begin
      if (~rst) begin
         rd_pointer <=  0;
      end else if (rd_en & ~empty) begin
         rd_pointer <=  rd_pointer + 1'b1;
      end
   end
endmodule
