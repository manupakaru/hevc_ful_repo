`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:21:28 04/17/2014 
// Design Name: 
// Module Name:    largest_bit_set 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module largest_bit_set_16(
		bitmap,
		index,
		no_bit_set
    );
	 input [0:15] bitmap;
	 output reg [3:0] index;
	 output reg no_bit_set;
	 
	 always @(*) begin
		casex (bitmap)
			16'b 1000_0000_0000_0000 : index = 4'd0;
			16'b x100_0000_0000_0000 : index = 4'd1;
			16'b xx10_0000_0000_0000 : index = 4'd2;
			16'b xxx1_0000_0000_0000 : index = 4'd3;
			
			16'b xxxx_1000_0000_0000 : index = 4'd4;
			16'b xxxx_x100_0000_0000 : index = 4'd5;
			16'b xxxx_xx10_0000_0000 : index = 4'd6;
			16'b xxxx_xxx1_0000_0000 : index = 4'd7;
			
			16'b xxxx_xxxx_1000_0000 : index = 4'd8;
			16'b xxxx_xxxx_x100_0000 : index = 4'd9;
			16'b xxxx_xxxx_xx10_0000 : index = 4'd10;
			16'b xxxx_xxxx_xxx1_0000 : index = 4'd11;
			
			16'b xxxx_xxxx_xxxx_1000 : index = 4'd12;
			16'b xxxx_xxxx_xxxx_x100 : index = 4'd13;
			16'b xxxx_xxxx_xxxx_xx10 : index = 4'd14;
			16'b xxxx_xxxx_xxxx_xxx1 : index = 4'd15;
			default 						 : index = 4'dx;
		endcase
		no_bit_set = ~(|bitmap);
	 end


endmodule
