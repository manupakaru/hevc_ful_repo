`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:23:45 12/23/2013
// Design Name:   global_top
// Module Name:   C:/Users/Maleen/Uni/HEVC/EntropyDecoder/global_test.v
// Project Name:  EntropyDecoder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: global_top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module global_test;

	// Inputs
	reg clk;
	reg rst;
	reg [31:0] entropy_in;
	reg entropy_valid;
	reg emptyCont;
	reg emptyY;
	reg emptyCb;
	reg emptyCr;

	// Outputs
	wire entropy_full;
	wire [31:0] outCont;
	wire validCont;
	wire [143:0] outY;
	wire validY;
	wire [143:0] outCb;
	wire validCb;
	wire [143:0] outCr;
	wire validCr;
	wire [31:0] GPO;
	
	integer fd, fd_res;
	integer line, line_res;
	reg [31:0] read_res;
	reg matched_res;
	
	integer   fdCont, fdY, fdCb, fdCr;
   integer   lineY;
   
	reg [8:0] read_in_res [0:3][0:3];
	reg [31:0] read_in_cont;
	reg [7:0] null;
	reg matchedY, matchedCb, matchedCr, matchedCont, matched;

	integer time_counter [1:3];
	integer CTU_no;
	integer POC;
		reg [4:0] em_time;
		
	reg time_printed, POC_printed;
	// Instantiate the Unit Under Test (UUT)
	global_top uut (
		.clk(clk), 
		.wr_clk(clk),
		.rst(rst), 
		.entropy_in(entropy_in),
		.entropy_valid(entropy_valid),
		.entropy_full(entropy_full),
		.outCont(outCont), 
		.validCont(validCont), 
		.emptyCont(emptyCont), 
		.outY(outY), 
		.validY(validY), 
		.emptyY(emptyY), 
		.outCb(outCb), 
		.validCb(validCb), 
		.emptyCb(emptyCb), 
		.outCr(outCr), 
		.validCr(validCr), 
		.emptyCr(emptyCr),
		.GPO(GPO)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0;
		entropy_in = 0;
		entropy_valid = 0;
		emptyCont = 0;
		emptyY = 0;
		emptyCb = 0;
		emptyCr = 0;
		em_time = 0;

		line = -4;
		line_res = -4;
		//fd = $fopen("BQSquare_416x240_60_qp37.bin","rb"); 
		fd = $fopen("test_files/input.hevc","rb"); 
		fd_res = $fopen("test_files/cabac_to_residual_soft","rb");
		matched_res = 1;
		fdCont = $fopen("test_files/residual_to_inter","rb");
		fdY = $fopen("test_files/residual_to_inter_Y","rb");
		fdCb = $fopen("test_files/residual_to_inter_Cb","rb");
		fdCr = $fopen("test_files/residual_to_inter_Cr","rb");
		matchedY = 1;
		matchedCb = 1;
		matchedCr = 1;
		matchedCont =1;
		time_counter[1] = 0; time_counter[2] = 0; time_counter[3] = 0;
		CTU_no = 0;
		POC_printed = 0;
		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		rst = 1;
//		read_empty = 0;
//		entropy_in[31:24] = $fgetc(fd);
//		read_in[23:16] = $fgetc(fd);
//		read_in[15: 8] = $fgetc(fd);
//		read_in[ 7: 0] = $fgetc(fd);	
//		line = line+4;
		
		read_res[31:24] = $fgetc(fd_res);
		read_res[23:16] = $fgetc(fd_res);
		read_res[15: 8] = $fgetc(fd_res);
		read_res[ 7: 0] = $fgetc(fd_res);	
		line_res = line_res+4;
	end
  
	always begin
		#5 clk = ~clk;
	end
	
   always @ (posedge clk) begin
		case (GPO[15:0])
			1000: begin
				time_counter[1] = time_counter[1]+1;
				time_printed = 0;
				POC_printed = 0;
			end
			2000: time_counter[2] = time_counter[2]+1;
			3000: time_counter[3] = time_counter[3]+1;
			4000: begin
				if (!time_printed) begin
					$display("CTU %3d (%5d, %5d, %5d)", CTU_no, time_counter[1], time_counter[2], time_counter[3]);
					CTU_no = CTU_no+1;
				end
				time_counter[1] = 0;
				time_counter[2] = 0;
				time_counter[3] = 0;
				time_printed = 1;
			end
			5000: begin
				POC = GPO[31:16];
				if (!POC_printed) begin
					$display ("POC %d", POC);
				end
				POC_printed = 1;
				
			end
		endcase
	end
	integer rand;

//	always @(posedge clk) begin
//		em_time = em_time + 1;
//		rand = {$random} % 100;
//		if ($time > 10000 & (em_time==0)) begin
//			//if (emptyY ==0)
//			//rand = {$random} % 100;
//			//rand = {$random(100)} %100;
//			//emptyY = (rand <10) ? ~emptyY : emptyY;
//			emptyY = (rand < 20);
//		end
//	
//	end
//	always @(posedge clk) begin
//		if (!matched)
//			$finish();
//	end
	
   always @ (negedge clk) begin
		if ($time > 200 & ($time % 100==0) ) begin
			if (!entropy_full & rst) begin
				entropy_in[31:24] = $fgetc(fd);
				entropy_in[23:16] = $fgetc(fd);
				entropy_in[15: 8] = $fgetc(fd);
				entropy_in[ 7: 0] = $fgetc(fd);			
				
				//$display("data  = %h", data);
				entropy_valid = 1'b1;
				line = line +4;
			end else begin
				entropy_valid = 1'b0;
			end
		end else begin
			entropy_valid = 1'b0;
		end
		if (uut.entropy.push_valid) begin
			if (uut.entropy.push_data==read_res)begin
				matched_res = 1;
			end else begin
				matched_res = 0;
				$finish();
			end
			
			read_res[31:24] = $fgetc(fd_res);
			read_res[23:16] = $fgetc(fd_res);
			read_res[15: 8] = $fgetc(fd_res);
			read_res[ 7: 0] = $fgetc(fd_res);	
			line_res = line_res+4;
		end
	end
	
	always @(negedge clk) begin
		
		if (validY) begin
			read(fdY);
			compare(outY,matchedY);
			lineY = lineY + 24;
		end
		if (validCb) begin
			read(fdCb);
			compare(outCb,matchedCb);
		end
		if (validCr) begin
			read(fdCr);
			compare(outCr,matchedCr);
		end
		if (validCont) begin
			read_in_cont[ 7: 0] = $fgetc(fdCont);
			read_in_cont[15: 8] = $fgetc(fdCont);
			read_in_cont[23:16] = $fgetc(fdCont);
			read_in_cont[31:24] = $fgetc(fdCont);
			if (read_in_cont == outCont) 
				matchedCont = 1;
				
			else 
				matchedCont = 0;
				//$display( "%h %h",read_in_cont, data_out[31:0]);
			//matchedCont = (read_in_cont == data_out);
		end
	end
	
	always @(*) 
		matched = matchedY & matchedCb & matchedCr & matchedCont;
		
		
	task read;
		input integer f;
		begin
			read_in_res[0][0][7:0] = $fgetc(f);
			{read_in_res[1][0][6:0] , read_in_res[0][0][8]} = $fgetc(f);
			{read_in_res[2][0][5:0] , read_in_res[1][0][8:7]} = $fgetc(f);
			{null[4:0] , read_in_res[2][0][8:6]} = $fgetc(f);
			
			read_in_res[3][0][7:0] = $fgetc(f);
			{read_in_res[0][1][6:0] , read_in_res[3][0][8]} = $fgetc(f);
			{read_in_res[1][1][5:0] , read_in_res[0][1][8:7]} = $fgetc(f);
			{null[4:0] , read_in_res[1][1][8:6]} = $fgetc(f);
			
			read_in_res[2][1][7:0] = $fgetc(f);
			{read_in_res[3][1][6:0] , read_in_res[2][1][8]} = $fgetc(f);
			{read_in_res[0][2][5:0] , read_in_res[3][1][8:7]} = $fgetc(f);
			{null[4:0] , read_in_res[0][2][8:6]} = $fgetc(f);
			
			read_in_res[1][2][7:0] = $fgetc(f);
			{read_in_res[2][2][6:0] , read_in_res[1][2][8]} = $fgetc(f);
			{read_in_res[3][2][5:0] , read_in_res[2][2][8:7]} = $fgetc(f);
			{null[4:0] , read_in_res[3][2][8:6]} = $fgetc(f);
			
			read_in_res[0][3][7:0] = $fgetc(f);
			{read_in_res[1][3][6:0] , read_in_res[0][3][8]} = $fgetc(f);
			{read_in_res[2][3][5:0] , read_in_res[1][3][8:7]} = $fgetc(f);
			{null[4:0] , read_in_res[2][3][8:6]} = $fgetc(f);
			
			read_in_res[3][3][7:0] = $fgetc(f);
			{null[6:0] , read_in_res[3][3][8]} = $fgetc(f);
			null = $fgetc(f);
			null = $fgetc(f);
			
		end
	endtask
	
	task compare;
		input [143:0] sim;
		output OK;
		begin
			if ( sim == { read_in_res[0][0] ,read_in_res[0][1],read_in_res[0][2],read_in_res[0][3], read_in_res[1][0] ,read_in_res[1][1],read_in_res[1][2],read_in_res[1][3], read_in_res[2][0] ,read_in_res[2][1],read_in_res[2][2],read_in_res[2][3], read_in_res[3][0] ,read_in_res[3][1],read_in_res[3][2],read_in_res[3][3] } ) begin
				OK = 1;
			end else begin
				OK = 0;
			end
		end
	endtask
endmodule

