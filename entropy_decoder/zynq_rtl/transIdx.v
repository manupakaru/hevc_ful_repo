`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:44:27 11/10/2013 
// Design Name: 
// Module Name:    transIdx 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module transIdx(
    input [5:0] pStateIdx,
    output reg [5:0] transIdxLPS,
    output reg [5:0] transIdxMPS
    );

	always @ (*) begin
		case (pStateIdx)
			0: {transIdxLPS, transIdxMPS} = {6'd0, 6'd1};
			1: {transIdxLPS, transIdxMPS} = {6'd0, 6'd2};
			2: {transIdxLPS, transIdxMPS} = {6'd1, 6'd3};
			3: {transIdxLPS, transIdxMPS} = {6'd2, 6'd4};
			4: {transIdxLPS, transIdxMPS} = {6'd2, 6'd5};
			5: {transIdxLPS, transIdxMPS} = {6'd4, 6'd6};
			6: {transIdxLPS, transIdxMPS} = {6'd4, 6'd7};
			7: {transIdxLPS, transIdxMPS} = {6'd5, 6'd8};
			8: {transIdxLPS, transIdxMPS} = {6'd6, 6'd9};
			9: {transIdxLPS, transIdxMPS} = {6'd7, 6'd10};
			10: {transIdxLPS, transIdxMPS} = {6'd8, 6'd11};
			11: {transIdxLPS, transIdxMPS} = {6'd9, 6'd12};
			12: {transIdxLPS, transIdxMPS} = {6'd9, 6'd13};
			13: {transIdxLPS, transIdxMPS} = {6'd11, 6'd14};
			14: {transIdxLPS, transIdxMPS} = {6'd11, 6'd15};
			15: {transIdxLPS, transIdxMPS} = {6'd12, 6'd16};
			16: {transIdxLPS, transIdxMPS} = {6'd13, 6'd17};
			17: {transIdxLPS, transIdxMPS} = {6'd13, 6'd18};
			18: {transIdxLPS, transIdxMPS} = {6'd15, 6'd19};
			19: {transIdxLPS, transIdxMPS} = {6'd15, 6'd20};
			20: {transIdxLPS, transIdxMPS} = {6'd16, 6'd21};
			21: {transIdxLPS, transIdxMPS} = {6'd16, 6'd22};
			22: {transIdxLPS, transIdxMPS} = {6'd18, 6'd23};
			23: {transIdxLPS, transIdxMPS} = {6'd18, 6'd24};
			24: {transIdxLPS, transIdxMPS} = {6'd19, 6'd25};
			25: {transIdxLPS, transIdxMPS} = {6'd19, 6'd26};
			26: {transIdxLPS, transIdxMPS} = {6'd21, 6'd27};
			27: {transIdxLPS, transIdxMPS} = {6'd21, 6'd28};
			28: {transIdxLPS, transIdxMPS} = {6'd22, 6'd29};
			29: {transIdxLPS, transIdxMPS} = {6'd22, 6'd30};
			30: {transIdxLPS, transIdxMPS} = {6'd23, 6'd31};
			31: {transIdxLPS, transIdxMPS} = {6'd24, 6'd32};
			32: {transIdxLPS, transIdxMPS} = {6'd24, 6'd33};
			33: {transIdxLPS, transIdxMPS} = {6'd25, 6'd34};
			34: {transIdxLPS, transIdxMPS} = {6'd26, 6'd35};
			35: {transIdxLPS, transIdxMPS} = {6'd26, 6'd36};
			36: {transIdxLPS, transIdxMPS} = {6'd27, 6'd37};
			37: {transIdxLPS, transIdxMPS} = {6'd27, 6'd38};
			38: {transIdxLPS, transIdxMPS} = {6'd28, 6'd39};
			39: {transIdxLPS, transIdxMPS} = {6'd29, 6'd40};
			40: {transIdxLPS, transIdxMPS} = {6'd29, 6'd41};
			41: {transIdxLPS, transIdxMPS} = {6'd30, 6'd42};
			42: {transIdxLPS, transIdxMPS} = {6'd30, 6'd43};
			43: {transIdxLPS, transIdxMPS} = {6'd30, 6'd44};
			44: {transIdxLPS, transIdxMPS} = {6'd31, 6'd45};
			45: {transIdxLPS, transIdxMPS} = {6'd32, 6'd46};
			46: {transIdxLPS, transIdxMPS} = {6'd32, 6'd47};
			47: {transIdxLPS, transIdxMPS} = {6'd33, 6'd48};
			48: {transIdxLPS, transIdxMPS} = {6'd33, 6'd49};
			49: {transIdxLPS, transIdxMPS} = {6'd33, 6'd50};
			50: {transIdxLPS, transIdxMPS} = {6'd34, 6'd51};
			51: {transIdxLPS, transIdxMPS} = {6'd34, 6'd52};
			52: {transIdxLPS, transIdxMPS} = {6'd35, 6'd53};
			53: {transIdxLPS, transIdxMPS} = {6'd35, 6'd54};
			54: {transIdxLPS, transIdxMPS} = {6'd35, 6'd55};
			55: {transIdxLPS, transIdxMPS} = {6'd36, 6'd56};
			56: {transIdxLPS, transIdxMPS} = {6'd36, 6'd57};
			57: {transIdxLPS, transIdxMPS} = {6'd36, 6'd58};
			58: {transIdxLPS, transIdxMPS} = {6'd37, 6'd59};
			59: {transIdxLPS, transIdxMPS} = {6'd37, 6'd60};
			60: {transIdxLPS, transIdxMPS} = {6'd37, 6'd61};
			61: {transIdxLPS, transIdxMPS} = {6'd38, 6'd62};
			62: {transIdxLPS, transIdxMPS} = {6'd38, 6'd62};
			63: {transIdxLPS, transIdxMPS} = {6'd63, 6'd63};

		endcase 
	
	end

endmodule
