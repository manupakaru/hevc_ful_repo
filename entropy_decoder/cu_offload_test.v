`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:00:23 12/08/2013
// Design Name:   cu_offload
// Module Name:   C:/Users/Maleen/Uni/HEVC/EntropyDecoder/cu_offload_test.v
// Project Name:  EntropyDecoder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: cu_offload
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module cu_offload_test;

	// Inputs
	reg clk;
	reg rst;
	reg [15:0] mmap_data;
	reg mmap_level;
	reg mmap_valid;
	reg [31:0] cabac_out_data;
	reg cabac_out_valid;
	reg out_fifo_full;
	

	// Outputs
	wire [31:0] cabac_in_data;
	wire cabac_in_valid;
	wire cabac_out_read;
	wire [31:0] out_fifo_data;
	wire out_fifo_valid;
	wire [5:0] IntraPredModeY0; 
	wire [5:0] IntraPredModeY1;
	wire [5:0] IntraPredModeY2;
	wire [5:0] IntraPredModeY3;
	wire [5:0] IntraPredModeC;
	wire IntraSplitFlag;
	wire [1:0] CuPredMode_cur;
	wire done;

	integer fd_mmap, fd_out, fd_cabac_in, fd_cabac_out;
	integer line_mmap, line_out, line_cabac_in, line_cabac_out;
	reg [31:0] read_in_mmap, read_in_out, read_in_cabac_in, read_in_cabac_out;
	
	reg matched_cabac_in;
	reg matched_out;

	// Instantiate the Unit Under Test (UUT)
	cu_offload uut (
		.clk(clk), 
		.rst(rst), 
		.mmap_data(read_in_mmap[29:0]), 
		.mmap_level(read_in_mmap[30]), 
		.mmap_valid(mmap_valid), 
		.cabac_in_data(cabac_in_data), 
		.cabac_in_valid(cabac_in_valid), 
		.cabac_out_data(read_in_cabac_out), 
		.cabac_out_valid(cabac_out_valid), 
		.cabac_out_read(cabac_out_read), 
		.out_fifo_data(out_fifo_data), 
		.out_fifo_valid(out_fifo_valid), 
		.out_fifo_full(out_fifo_full), 
		.IntraPredModeY0(IntraPredModeY0), 
		.IntraPredModeY1(IntraPredModeY1), 
		.IntraPredModeY2(IntraPredModeY2), 
		.IntraPredModeY3(IntraPredModeY3), 
		.IntraPredModeC(IntraPredModeC), 
		.IntraSplitFlag(IntraSplitFlag), 
		.CuPredMode_cur(CuPredMode_cur), 
		.done(done)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0;
		mmap_data = 0;
		mmap_level = 0;
		mmap_valid = 0;
		cabac_out_data = 0;
		cabac_out_valid = 0;
		out_fifo_full = 0;
		fd_mmap = $fopen("BQSquare_416x240_60_qp37_cu_mmap.bin","rb");  line_mmap = -4;
		fd_out = $fopen("BQSquare_416x240_60_qp37_cu_out.bin","rb"); 	 line_out = -4;
		fd_cabac_in = $fopen("BQSquare_416x240_60_qp37_cu_cabac_in.bin","rb");  line_cabac_in =-4;
		fd_cabac_out = $fopen("BQSquare_416x240_60_qp37_cu_cabac_out.bin","rb"); line_cabac_out = -4;

		read_in_mmap[ 7: 0] = $fgetc(fd_mmap);
		read_in_mmap[15: 8] = $fgetc(fd_mmap);
		read_in_mmap[23:16] = $fgetc(fd_mmap);
		read_in_mmap[31:24] = $fgetc(fd_mmap); line_mmap = line_mmap +4;
		mmap_valid = 1;
		read_in_cabac_out[ 7: 0] = $fgetc(fd_cabac_out);
		read_in_cabac_out[15: 8] = $fgetc(fd_cabac_out);
		read_in_cabac_out[23:16] = $fgetc(fd_cabac_out);
		read_in_cabac_out[31:24] = $fgetc(fd_cabac_out); line_cabac_out = line_cabac_out +4;
		cabac_out_valid = 1;
		
		matched_cabac_in = 1;
		matched_out = 1;
		// Wait 100 ns for global reset to finish
		#100;
      rst = 1;   
		// Add stimulus here

	end
	always begin
		#5 clk = ~clk;
	end
	
	always @(posedge clk) begin
		if (done) begin
			read_in_mmap[ 7: 0] = $fgetc(fd_mmap);
			read_in_mmap[15: 8] = $fgetc(fd_mmap);
			read_in_mmap[23:16] = $fgetc(fd_mmap);
			read_in_mmap[31:24] = $fgetc(fd_mmap); line_mmap = line_mmap +4;
			mmap_valid = 1;
		end
	end
	
	always @(posedge clk) begin
		if (cabac_out_read) begin
			read_in_cabac_out[ 7: 0] = $fgetc(fd_cabac_out);
			read_in_cabac_out[15: 8] = $fgetc(fd_cabac_out);
			read_in_cabac_out[23:16] = $fgetc(fd_cabac_out);
			read_in_cabac_out[31:24] = $fgetc(fd_cabac_out); line_cabac_out = line_cabac_out +4;
			cabac_out_valid = 1;
		end
	end
	
	always @(negedge clk) begin
		if (out_fifo_valid) begin
			read_in_out[31:24] = $fgetc(fd_out);
			read_in_out[23:16] = $fgetc(fd_out);
			read_in_out[15: 8] = $fgetc(fd_out);
			read_in_out[ 7: 0] = $fgetc(fd_out); line_out = line_out +4;
			//mmap_valid = 1;
			if (read_in_out == out_fifo_data) begin
			//if (read_in_out == {1'b0, IntraSplitFlag, IntraPredModeC, IntraPredModeY0, IntraPredModeY1, IntraPredModeY2, IntraPredModeY3}) begin
				matched_out = 1;
			end else begin
				matched_out = 0; #50 ; $finish();
			end
		end
	end
	
	always @(negedge clk) begin
		if (cabac_in_valid) begin
			read_in_cabac_in[ 7: 0] = $fgetc(fd_cabac_in);
			read_in_cabac_in[15: 8] = $fgetc(fd_cabac_in);
			read_in_cabac_in[23:16] = $fgetc(fd_cabac_in);
			read_in_cabac_in[31:24] = $fgetc(fd_cabac_in); line_cabac_in = line_cabac_in +4;
			if (read_in_cabac_in == cabac_in_data) begin
				matched_cabac_in = 1;
			end else begin
				matched_cabac_in = 0;
				
			end
		end
	end
      
endmodule

