onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /intra_pred_tb/uut/prediction_cr_residual_data_fifo_monitor_block/clk
add wave -noupdate /intra_pred_tb/uut/prediction_cr_residual_data_fifo_monitor_block/reset
add wave -noupdate -radix unsigned /intra_pred_tb/uut/pred_top_block/state
add wave -noupdate /intra_pred_tb/uut/prediction_cr_residual_data_fifo_monitor_block/full
add wave -noupdate /intra_pred_tb/uut/prediction_cr_residual_data_fifo_monitor_block/empty
add wave -noupdate /intra_pred_tb/uut/prediction_cr_residual_data_fifo_monitor_block/wr_en
add wave -noupdate /intra_pred_tb/uut/prediction_cr_residual_data_fifo_monitor_block/rd_en
add wave -noupdate /intra_pred_tb/uut/prediction_cr_residual_data_fifo_monitor_block/in
add wave -noupdate /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/rst
add wave -noupdate /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/din
add wave -noupdate /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/wr_en
add wave -noupdate /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/rd_en
add wave -noupdate /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/prog_empty_thresh
add wave -noupdate /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/dout
add wave -noupdate /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/full
add wave -noupdate /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/empty
add wave -noupdate /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/prog_full
add wave -noupdate /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/prog_empty
add wave -noupdate /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/inst/conv_fifo/fifo_generator_v9_3_conv_dut/block1/gen_ss/rst_i
add wave -noupdate -radix unsigned /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/inst/conv_fifo/fifo_generator_v9_3_conv_dut/block1/gen_ss/srst_i
add wave -noupdate -radix unsigned /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/inst/conv_fifo/fifo_generator_v9_3_conv_dut/block1/gen_ss/almost_full_i
add wave -noupdate -radix unsigned /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/inst/conv_fifo/fifo_generator_v9_3_conv_dut/block1/gen_ss/diff_pntr_pe
add wave -noupdate -radix unsigned /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/inst/conv_fifo/fifo_generator_v9_3_conv_dut/block1/gen_ss/pe3_assert_val
add wave -noupdate -radix unsigned /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/inst/conv_fifo/fifo_generator_v9_3_conv_dut/block1/gen_ss/diff_pntr_pe
add wave -noupdate -radix unsigned /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/inst/conv_fifo/fifo_generator_v9_3_conv_dut/block1/gen_ss/pe3_assert_val
add wave -noupdate -radix unsigned /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/inst/conv_fifo/fifo_generator_v9_3_conv_dut/block1/gen_ss/write_only_q
add wave -noupdate /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/inst/conv_fifo/fifo_generator_v9_3_conv_dut/block1/gen_ss/PROG_EMPTY_THRESH
add wave -noupdate /intra_pred_tb/uut/prediction_cb_residual_data_fifo_in_block/inst/conv_fifo/fifo_generator_v9_3_conv_dut/block1/gen_ss/C_PRELOAD_LATENCY
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {129653515 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 307
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {1922056500 ps}
bookmark add wave bookmark3 {{20626931 ps} {21268346 ps}} 375
