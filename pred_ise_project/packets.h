/*
 * packets.h
 *
 *  Created on: May 20, 2013
 *      Author: maleen
 */

#ifndef PACKETS_H_
#define PACKETS_H_

struct parameters_0 {
	unsigned header: 8;
	unsigned pic_width: 12;
	unsigned pic_height: 12;
};
struct parameters_1 {
	unsigned header :8;
	unsigned Log2CtbSizeY : 3;
	signed pps_cb_qp_offset :5;
	signed pps_cr_qp_offset :5;
	signed pps_beta_offset_div2 : 4;
	signed pps_tc_offset_div2: 4;
	unsigned strong_intra_smoothing :1;
	unsigned constrained_intra_pred : 1;
};
struct parameters_2 {
	unsigned header :8;
	unsigned num_short_term_ref : 6;
	unsigned weighted_pred:1;
	unsigned weighted_bipred:1;
	unsigned parallel_merge_level:3;
};
struct parameters_3 {
	unsigned header :8;
	unsigned rps_id: 8;
	unsigned num_positive:8;
	unsigned num_negative:8;
};
struct parameters_4 {
	unsigned header :8;
	unsigned rps_id: 7;
	unsigned used:1;
	signed delta_POC:16;
};
struct slice_0 {
	unsigned header : 8;
	unsigned null:24;
	signed POC: 32;
};
struct slice_1{
	unsigned header :8;
	unsigned short_term_ref_pic_sps : 1;
	unsigned short_term_ref_pic_idx: 4;
	unsigned temporal_mvp_enabled : 1;
	unsigned sao_luma : 1;
	unsigned sao_chroma :1;
	unsigned num_ref_idx_l0_minus1 : 4;
	unsigned num_ref_idx_l1_minus1 : 4;
	unsigned five_minus_max_merge_cand : 3;
	unsigned slice_type :2;
	unsigned collocated_from_l0 :1;
};
struct slice_2 {
	unsigned header :8;
	signed slice_cb_qp_offset:5;
	signed slice_cr_qp_offset:5;
	unsigned disable_dbf : 1;
	signed slice_beta_offset_div2 : 4;
	signed slice_tc_offset_div2 : 4;
};
struct slice_3 {
	unsigned header :8;
	unsigned rps_id: 8;
	unsigned num_positive:8;
	unsigned num_negative:8;
};
struct slice_4 {
	unsigned header :8;
	unsigned rps_id: 7;
	unsigned used:1;
	signed delta_POC:16;
};
struct CTU_0 {
	unsigned header : 8;
	unsigned xC :12;
	unsigned yC:12;
};
struct CTU_1 {
	unsigned header : 8;
	unsigned tile_id : 8;
	unsigned slice_id : 8;
};
struct CTU_2 {
	unsigned header :8;
	unsigned SaoType:2;
	unsigned BandPos_EO : 6;
	unsigned sao_offset_abs_0 :3 ;
	unsigned sao_offset_sign_0 :1;
	unsigned sao_offset_abs_1 :3 ;
	unsigned sao_offset_sign_1 :1;
	unsigned sao_offset_abs_2 :3 ;
	unsigned sao_offset_sign_2 :1;
	unsigned sao_offset_abs_3 :3 ;
	unsigned sao_offset_sign_3 :1;
};
struct CU_0 {
	unsigned header :8;
	unsigned x0 :6;
	unsigned y0 :6;
	unsigned log2_cb_size:3;
	unsigned predMode : 1;
	unsigned PartMode :3;
	unsigned bypass : 1;
	unsigned pcm:1;
};
struct PU_0{
	unsigned header : 8;
	unsigned part_idx : 2;
	unsigned pred_idc :2;
	unsigned merge_flag:1;
	unsigned merge_idx :3;
	unsigned mvp_l0_flag:1;
	unsigned mvp_l1_flag:1;
	unsigned ref_idx_l0 :4;
	unsigned ref_idx_l1 :4;
};
struct RU_0{
	unsigned header : 8;
	unsigned xT : 5;
	unsigned yT : 5;
	unsigned cidx : 2;
	unsigned size : 3;
	unsigned IntraMode :6;
	unsigned res_present:1;
	unsigned transform_skip_flag : 1;
};
struct RU_1 {
	unsigned header: 8;
	unsigned BSh:2;
	unsigned BSv:2;
	unsigned QP:6;
};
struct fifo_element {
	signed a0 :9;
	signed a1 :9;
	signed a2 :9;
	signed a3 :9;
	signed b0 :9;
	signed b1 :9;
	signed b2 :9;
	signed b3 :9;
	signed c0 :9;
	signed c1 :9;
	signed c2 :9;
	signed c3 :9;
	signed d0 :9;
	signed d1 :9;
	signed d2 :9;
	signed d3 :9;
};
#endif /* PACKETS_H_ */
