// (c) Copyright 1995-2015 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:user:mig_setup_reset:1.2
// IP Revision: 7

(* X_CORE_INFO = "mig_setup_reset,Vivado 2014.2" *)
(* CHECK_LICENSE_TYPE = "zynq_design_1_mig_setup_reset_0_0,mig_setup_reset,{}" *)
(* CORE_GENERATION_INFO = "zynq_design_1_mig_setup_reset_0_0,mig_setup_reset,{x_ipProduct=Vivado 2014.2,x_ipVendor=xilinx.com,x_ipLibrary=user,x_ipName=mig_setup_reset,x_ipVersion=1.2,x_ipCoreRevision=7,x_ipLanguage=VERILOG}" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module zynq_design_1_mig_setup_reset_0_0 (
  rst,
  clk,
  areset_n,
  init_calib_complete,
  up_stream_reset,
  design_reset,
  design_reset_n,
  mmcm_locked
);

(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 signal_reset RST" *)
input wire rst;
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 signal_clock CLK" *)
input wire clk;
output wire areset_n;
input wire init_calib_complete;
output wire up_stream_reset;
output wire design_reset;
output wire design_reset_n;
input wire mmcm_locked;

  mig_setup_reset inst (
    .rst(rst),
    .clk(clk),
    .areset_n(areset_n),
    .init_calib_complete(init_calib_complete),
    .up_stream_reset(up_stream_reset),
    .design_reset(design_reset),
    .design_reset_n(design_reset_n),
    .mmcm_locked(mmcm_locked)
  );
endmodule
