 `timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    13:02:03 11/15/2013
// Design Name:
// Module Name:    intra_prediction_top
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
(* register_balancing = "yes" *)
(* use_dsp48 = "no" *)
module intra_prediction_top
    (
        clk,
        reset,
        enable,

        valid_in,
        xt_in,
        yt_in,
        ntbs_in,
        intramode_in,
        new_frame_in,
        // ready_out,

        predsample_4by4_out,
        predsample_4by4_valid_out,
        residual_fifo_is_empty_in,
        cu_done_out,

        //Line buffer write interfaces
        top_portw_addr_in,
        top_portw_yaddr_in,
        top_portw_data0_in,
        top_portw_data1_in,
        top_portw_data2_in,
        top_portw_data3_in,
        top_portw_data4_in,
        top_portw_data5_in,
        top_portw_data6_in,
        top_portw_data7_in,
        top_portw_en_mask_in,


        left_portw_addr_in,
        left_portw_xaddr_in,
        left_portw_data0_in,
        left_portw_data1_in,
        left_portw_data2_in,
        left_portw_data3_in,
        left_portw_data4_in,
        left_portw_data5_in,
        left_portw_data6_in,
        left_portw_data7_in,
        left_portw_en_mask_in,

        //CONFIG
        frame_height_in,
        frame_width_in,
        tile_width_in,
        tile_xc_in,
        tile_yc_in,
        slice_x_in,
        slice_y_in,
        strong_intra_smoothing_flag_in,
        constrained_intra_pred_flag_in,

        log2ctbsize_in,
        log2ctbsize_config_in,

        top_nt_out,
        left_nt_out,
        nt_pixels_valid_out






        // config_data
        // config_data_in,
        // config_data_valid_in

    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES :
     * Always include global constant header files that have constant definitions before all other items
     * these files will contain constants in the form of localparams of `define directives
     */
   `include "../sim/pred_def.v"

//----------------------------------------------------
// PARAMETERS
//----------------------------------------------------
    parameter                       CIDX = 2'd0;


//----------------------------------------------------
//LOCALPARAMS
//----------------------------------------------------
    // localparam                      MAX_NTBS_SIZE           = 6;
    localparam                      NUM_OF_DATA_REGS        = 5;
    // localparam                      PIXEL_WIDTH             = 8;
    // localparam                      LOG2_FRAME_SIZE         = 13;
    localparam                      MODE_WIDTH              = 2;
    // localparam                      INTRA_MODE_WIDTH        = 6;
    // localparam                      OUTPUT_BLOCK_SIZE       = 4;
    localparam                      INTRA_PRED_ANGLE_WIDTH  = 6;
    // localparam                      BIT_DEPTH               = 8;

    localparam                      TOP_PRED_MODE_WIDTH         = 2;
    localparam                      TOP_PRED_MODE_DC            = 0;
    localparam                      TOP_PRED_MODE_PLANAR        = 1;
    localparam                      TOP_PRED_MODE_ANGULAR_TOP   = 2;
    localparam                      TOP_PRED_MODE_ANGULAR_LEFT  = 3;

    // localparam                      PIXEL_ADDR_LENGTH              = 13;
    // localparam                      MAX_LOG2CTBSIZE_WIDTH          = 3;

    localparam                      READ                    = 1'b0;
    localparam                      WRITE                   = 1'b1;

    localparam                      STATE_INIT              = 0;
    localparam                      STATE_WAIT              = 1;
    localparam                      STATE_FILTER            = 3;
    localparam                      STATE_PREDTICTION       = 4;
    localparam                      STATE_DONE              = 5;
    localparam                      STATE_FILTER_WAIT1      = 6;
    localparam                      STATE_FILTER_WAIT2      = 7;
    localparam                      STATE_PREWAIT           = 8;

    localparam                      LINEBUF_ADDR_STATE_INIT         = 0;
    localparam                      LINEBUF_ADDR_STATE_ZEROTH       = 1;
    localparam                      LINEBUF_ADDR_STATE_NT           = 2;
    localparam                      LINEBUF_ADDR_STATE_2NT          = 3;
    localparam                      LINEBUF_ADDR_STATE_DC           = 4;
    localparam                      LINEBUF_ADDR_STATE_ANGULAR_LEFT = 5;
    localparam                      LINEBUF_ADDR_STATE_ANGULAR_TOP  = 6;
    localparam                      LINEBUF_ADDR_STATE_FILTER       = 7;
    localparam                      LINEBUF_ADDR_STATE_FILTER_WAIT1  = 8;
    localparam                      LINEBUF_ADDR_STATE_FILTER_WAIT2  = 9;
    localparam                      LINEBUF_ADDR_STATE_FILTER_WAIT3  = 10;
    localparam                      LINEBUF_ADDR_STATE_FILTER_WAIT4  = 11;
    localparam                      LINEBUF_ADDR_STATE_FILTER_WAIT5  = 12;
    localparam                      LINEBUF_ADDR_STATE_FILTER_WAIT6  = 13;

    localparam                      TOPWR_STATE_LOW4                 = 0;
    localparam                      TOPWR_STATE_HIGH4                = 1;
    localparam                      LEFTWR_STATE_LOW4                = 0;
    localparam                      LEFTWR_STATE_HIGH4               = 1;

    localparam                      INTRAHORVERDISTTHRES_WIDTH      = 3;

    localparam                      MODE_INTER = 1;
    localparam                      MODE_INTRA = 0;

    //New states for fix constrained intra pred flag problem
    localparam                      RESET_STATE_INIT = 0;
    localparam                      RESET_STATE_ACTIVE = 1;

    localparam                      MIN_RANGE_UPDATE_STATE_INIT = 0;
    localparam                      MIN_RANGE_UPDATE_STATE_ACTIVE = 1;
    localparam                      MIN_RANGE_UPDATE_STATE_WRITE = 2;
    localparam                      MIN_RANGE_UPDATE_STATE_READ = 3;

    // Config params
    // localparam                      CONFIG_DATA_BUS_WIDTH           = 32;
    // localparam  [16 - 1:0]          CONFIG_HEADER_LOG2CTBSIZE       =

//----------------------------------------------------
// IO Signals
//----------------------------------------------------

    input                                                               clk;
    input                                                               reset;
    input                                                               enable;

    input                                                               valid_in;
    input       [LOG2_FRAME_SIZE - 1:0]                                 xt_in;
    input       [LOG2_FRAME_SIZE - 1:0]                                 yt_in;
    input       [MAX_NTBS_SIZE - 1:0]                                   ntbs_in;
    input       [INTRA_MODE_WIDTH - 1:0]                                intramode_in;
    input                                                               new_frame_in;
    // output reg                                                          ready_out;

    output      [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] predsample_4by4_out;
    output                                                              predsample_4by4_valid_out;
    input                                                               residual_fifo_is_empty_in;
    output reg                                                          cu_done_out;

    //topLine buffer write interface
    input       [LOG2_FRAME_SIZE - 1:0]                                 top_portw_addr_in;
    input       [LOG2_FRAME_SIZE - 1:0]                                 top_portw_yaddr_in;
    input       [PIXEL_WIDTH - 1 :0]                                    top_portw_data0_in;
    input       [PIXEL_WIDTH - 1 :0]                                    top_portw_data1_in;
    input       [PIXEL_WIDTH - 1 :0]                                    top_portw_data2_in;
    input       [PIXEL_WIDTH - 1 :0]                                    top_portw_data3_in;
    input       [PIXEL_WIDTH - 1 :0]                                    top_portw_data4_in;
    input       [PIXEL_WIDTH - 1 :0]                                    top_portw_data5_in;
    input       [PIXEL_WIDTH - 1 :0]                                    top_portw_data6_in;
    input       [PIXEL_WIDTH - 1 :0]                                    top_portw_data7_in;
    input       [PIXEL_WIDTH - 1 :0]                                    top_portw_en_mask_in;

    //left line buffer read interface
    input       [LOG2_FRAME_SIZE - 1:0]                                 left_portw_addr_in;
    input       [LOG2_FRAME_SIZE - 1:0]                                 left_portw_xaddr_in;
    input       [PIXEL_WIDTH - 1 :0]                                    left_portw_data0_in;
    input       [PIXEL_WIDTH - 1 :0]                                    left_portw_data1_in;
    input       [PIXEL_WIDTH - 1 :0]                                    left_portw_data2_in;
    input       [PIXEL_WIDTH - 1 :0]                                    left_portw_data3_in;
    input       [PIXEL_WIDTH - 1 :0]                                    left_portw_data4_in;
    input       [PIXEL_WIDTH - 1 :0]                                    left_portw_data5_in;
    input       [PIXEL_WIDTH - 1 :0]                                    left_portw_data6_in;
    input       [PIXEL_WIDTH - 1 :0]                                    left_portw_data7_in;
    input       [PIXEL_WIDTH - 1 :0]                                    left_portw_en_mask_in;

    //CONFIG DATA
    input         [PIXEL_ADDR_LENGTH - 1:0]                             frame_height_in;
    input         [PIXEL_ADDR_LENGTH - 1:0]                             frame_width_in;
    input         [PIXEL_ADDR_LENGTH - 1:0]                             tile_width_in;
    input         [PIXEL_ADDR_LENGTH - 1:0]                             tile_xc_in;
    input         [PIXEL_ADDR_LENGTH - 1:0]                             tile_yc_in;
    input         [PIXEL_ADDR_LENGTH - 1:0]                             slice_x_in;
    input         [PIXEL_ADDR_LENGTH - 1:0]                             slice_y_in;
    input                                                               strong_intra_smoothing_flag_in;
    input                                                               constrained_intra_pred_flag_in;

    input         [MAX_LOG2CTBSIZE_WIDTH - 1:0]                         log2ctbsize_in;
    input                                                               log2ctbsize_config_in;

    output      [PIXEL_WIDTH - 1:0]                                     top_nt_out;
    output      [PIXEL_WIDTH - 1:0]                                     left_nt_out;
    output reg                                                          nt_pixels_valid_out;


    // input       [CONFIG_DATA_BUS_WIDTH - 1:0]                           config_data_in;
    // input                                                               config_data_valid_in;

//----------------------------------------------------
// Internal Regs and Wires
//----------------------------------------------------

    reg         [MAX_NTBS_SIZE - 1:0]                                   inner_x_reg;
    // reg         [MAX_NTBS_SIZE - 2 - 1:0]                               inner_x_shifted_2_reg;
    reg         [MAX_NTBS_SIZE - 1:0]                                   inner_y_reg;
    // reg         [MAX_NTBS_SIZE - 2 - 1:0]                               inner_y_shifted_2_reg;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 xt_reg;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 yt_reg;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 xt_nt_reg;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 yt_nt_reg;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 xt_2nt_reg;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 yt_2nt_reg;
    reg         [MAX_NTBS_SIZE - 1:0]                                   ntbs_reg;
    reg         [INTRA_MODE_WIDTH - 1:0]                                intramode_reg;

    reg         [INTRA_PRED_ANGLE_WIDTH - 1:0]                          abs_intrapredangle_reg;
    reg                                                                 sgn_intrapredangle_reg;
    reg         [TOP_PRED_MODE_WIDTH - 1:0]                             top_pred_mode_reg;

    reg         [PIXEL_ADDR_LENGTH - 1:0]                               frame_height_reg;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               frame_width_reg;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               tile_width_reg;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               tile_xc_reg;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               tile_yc_reg;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               slice_x_reg;
    reg         [PIXEL_ADDR_LENGTH - 1:0]                               slice_y_reg;
    reg                                                                 strong_intra_smoothing_flag_reg;
    reg                                                                 constrained_intra_pred_flag;

    reg         [MAX_LOG2CTBSIZE_WIDTH - 1:0]                           log2ctbsize_reg;
    reg                                                                 log2ctbsize_config_reg;
    reg                                                                 curr_valid_reg;

    wire        [PIXEL_ADDR_LENGTH - 1:0]                               validrange_top_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               validrange_left_int;
    // wire                                                                validrange_topleft_int;
    wire                                                                all_invalid_top_int;
    wire                                                                all_invalid_left_int;
    wire                                                                available_pixel_checker_valid_int;

    //For Top Line buffer

    reg         [LOG2_FRAME_SIZE - 1:0]                                 top_portw_addr  [31:0];
    reg         [LOG2_FRAME_SIZE - 1:0]                                 top_portw_yaddr [31:0];
    reg         [PIXEL_WIDTH - 1 :0]                                    top_portw_data0 [31:0];
    reg         [PIXEL_WIDTH - 1 :0]                                    top_portw_data1 [31:0];
    reg         [PIXEL_WIDTH - 1 :0]                                    top_portw_data2 [31:0];
    reg         [PIXEL_WIDTH - 1 :0]                                    top_portw_data3 [31:0];
    reg         [PIXEL_WIDTH - 1 :0]                                    top_portw_data4 [31:0];
    reg         [PIXEL_WIDTH - 1 :0]                                    top_portw_data5 [31:0];
    reg         [PIXEL_WIDTH - 1 :0]                                    top_portw_data6 [31:0];
    reg         [PIXEL_WIDTH - 1 :0]                                    top_portw_data7 [31:0];
    reg         [PIXEL_WIDTH - 1 :0]                                    top_portw_en_mask[31:0];

    integer                                                             topwr_state;
    reg         [4:0]                                                   topwr_counter;

    reg         [LOG2_FRAME_SIZE - 1:0]                                 top_portr_addr;
    wire        [PIXEL_WIDTH - 1:0]                                     linebuf_top_portr_data0;
    wire        [PIXEL_WIDTH - 1:0]                                     linebuf_top_portr_data1;
    wire        [PIXEL_WIDTH - 1:0]                                     linebuf_top_portr_data2;
    wire        [PIXEL_WIDTH - 1:0]                                     linebuf_top_portr_data3;
    wire        [PIXEL_WIDTH - 1:0]                                     linebuf_top_portr_data4;
    wire        [PIXEL_WIDTH - 1:0]                                     linebuf_top_portr_data5;
    wire        [PIXEL_WIDTH - 1:0]                                     linebuf_top_portr_data6;
    wire        [PIXEL_WIDTH - 1:0]                                     linebuf_top_portr_data7;
    reg                                                                 top_portr_en;

    //For Left Line buffer

    reg         [LOG2_FRAME_SIZE - 1:0]                                 left_portw_addr     [31:0];
    reg         [LOG2_FRAME_SIZE - 1:0]                                 left_portw_xaddr    [31:0];
    reg         [PIXEL_WIDTH - 1 :0]                                    left_portw_data0    [31:0];
    reg         [PIXEL_WIDTH - 1 :0]                                    left_portw_data1    [31:0];
    reg         [PIXEL_WIDTH - 1 :0]                                    left_portw_data2    [31:0];
    reg         [PIXEL_WIDTH - 1 :0]                                    left_portw_data3    [31:0];
    reg         [PIXEL_WIDTH - 1 :0]                                    left_portw_data4    [31:0];
    reg         [PIXEL_WIDTH - 1 :0]                                    left_portw_data5    [31:0];
    reg         [PIXEL_WIDTH - 1 :0]                                    left_portw_data6    [31:0];
    reg         [PIXEL_WIDTH - 1 :0]                                    left_portw_data7    [31:0];
    reg         [PIXEL_WIDTH - 1 :0]                                    left_portw_en_mask  [31:0];

    integer                                                             leftwr_state;
    reg         [4:0]                                                   leftwr_counter;

    reg         [LOG2_FRAME_SIZE - 1:0]                                 left_portr_addr;
    wire        [PIXEL_WIDTH - 1:0]                                     linebuf_left_portr_data0;
    wire        [PIXEL_WIDTH - 1:0]                                     linebuf_left_portr_data1;
    wire        [PIXEL_WIDTH - 1:0]                                     linebuf_left_portr_data2;
    wire        [PIXEL_WIDTH - 1:0]                                     linebuf_left_portr_data3;
    wire        [PIXEL_WIDTH - 1:0]                                     linebuf_left_portr_data4;
    wire        [PIXEL_WIDTH - 1:0]                                     linebuf_left_portr_data5;
    wire        [PIXEL_WIDTH - 1:0]                                     linebuf_left_portr_data6;
    wire        [PIXEL_WIDTH - 1:0]                                     linebuf_left_portr_data7;
    reg                                                                 left_portr_en;

    // For Filter
    wire                                                                filter_done_int;
    wire        [MAX_NTBS_SIZE - 1:0]                                   filter_offset_int;
    wire        [PIXEL_WIDTH - 1:0]                                     corner_pixel_filtered_int;

//    reg         [LOG2_FRAME_SIZE - 1:0]                                 filter_top_portr_addr;
    wire        [PIXEL_WIDTH - 1:0]                                     filter_top_portr_data0;
    wire        [PIXEL_WIDTH - 1:0]                                     filter_top_portr_data1;
    wire        [PIXEL_WIDTH - 1:0]                                     filter_top_portr_data2;
    wire        [PIXEL_WIDTH - 1:0]                                     filter_top_portr_data3;
    wire        [PIXEL_WIDTH - 1:0]                                     filter_top_portr_data4;
    wire        [PIXEL_WIDTH - 1:0]                                     filter_top_portr_data5;
    wire        [PIXEL_WIDTH - 1:0]                                     filter_top_portr_data6;
    wire        [PIXEL_WIDTH - 1:0]                                     filter_top_portr_data7;
//    reg                                                                 filter_top_portr_en;

//    reg         [LOG2_FRAME_SIZE - 1:0]                                 filter_left_portr_addr;
    wire        [PIXEL_WIDTH - 1:0]                                     filter_left_portr_data0;
    wire        [PIXEL_WIDTH - 1:0]                                     filter_left_portr_data1;
    wire        [PIXEL_WIDTH - 1:0]                                     filter_left_portr_data2;
    wire        [PIXEL_WIDTH - 1:0]                                     filter_left_portr_data3;
    wire        [PIXEL_WIDTH - 1:0]                                     filter_left_portr_data4;
    wire        [PIXEL_WIDTH - 1:0]                                     filter_left_portr_data5;
    wire        [PIXEL_WIDTH - 1:0]                                     filter_left_portr_data6;
    wire        [PIXEL_WIDTH - 1:0]                                     filter_left_portr_data7;
//    reg                                                                 filter_left_portr_en;

    //COMMON for filter and linebuf
    reg         [PIXEL_WIDTH - 1:0]                                     top_portr_data0;
    reg         [PIXEL_WIDTH - 1:0]                                     top_portr_data1;
    reg         [PIXEL_WIDTH - 1:0]                                     top_portr_data2;
    reg         [PIXEL_WIDTH - 1:0]                                     top_portr_data3;
    reg         [PIXEL_WIDTH - 1:0]                                     top_portr_data4;
    reg         [PIXEL_WIDTH - 1:0]                                     top_portr_data5;
    reg         [PIXEL_WIDTH - 1:0]                                     top_portr_data6;
    reg         [PIXEL_WIDTH - 1:0]                                     top_portr_data7;

    reg         [PIXEL_WIDTH - 1:0]                                     left_portr_data0;
    reg         [PIXEL_WIDTH - 1:0]                                     left_portr_data1;
    reg         [PIXEL_WIDTH - 1:0]                                     left_portr_data2;
    reg         [PIXEL_WIDTH - 1:0]                                     left_portr_data3;
    reg         [PIXEL_WIDTH - 1:0]                                     left_portr_data4;
    reg         [PIXEL_WIDTH - 1:0]                                     left_portr_data5;
    reg         [PIXEL_WIDTH - 1:0]                                     left_portr_data6;
    reg         [PIXEL_WIDTH - 1:0]                                     left_portr_data7;



    //Corner Pixel Memory
    reg         [MAX_NTBS_SIZE - 2 - 1:0]                               corner_pixel_mem_port_a_xt_div_4_reg;
    reg         [MAX_NTBS_SIZE - 2 - 1:0]                               corner_pixel_mem_port_a_yt_div_4_reg;
    reg         [MAX_NTBS_SIZE - 2 - 1:0]                               corner_pixel_mem_port_a_xt_div_4_raddr;
    reg         [MAX_NTBS_SIZE - 2 - 1:0]                               corner_pixel_mem_port_a_yt_div_4_raddr;
    reg         [MAX_NTBS_SIZE - 2 - 1:0]                               corner_pixel_mem_port_a_xt_div_4_waddr;
    reg         [MAX_NTBS_SIZE - 2 - 1:0]                               corner_pixel_mem_port_a_yt_div_4_waddr;


    wire        [PIXEL_WIDTH - 1:0]                                     corner_pixel_mem_port_a_rdata_int;
    reg         [PIXEL_WIDTH - 1:0]                                     corner_pixel_mem_port_a_wdata_int;
    reg                                                                 corner_pixel_mem_port_a_rw_int;
    reg                                                                 corner_pixel_mem_port_a_wen_int;

    reg         [MAX_NTBS_SIZE - 2 - 1:0]                               corner_pixel_mem_port_b_xt_div_4_reg;
    reg         [MAX_NTBS_SIZE - 2 - 1:0]                               corner_pixel_mem_port_b_yt_div_4_reg;
    // wire        [PIXEL_WIDTH - 1:0]                                     corner_pixel_mem_port_b_rdata_int;
    reg         [PIXEL_WIDTH - 1:0]                                     corner_pixel_mem_port_b_wdata_int;
    reg                                                                 corner_pixel_mem_port_b_rw_int;
    reg                                                                 corner_pixel_mem_port_b_wen_int;


    // Top read addr module
    wire                                                                top_read_from_aux_flag_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               top_read_addr_to_aux_flag_gen_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               top_read_addr_from_aux_flag_gen_int;
    wire        [4:0]                                                   top_aux_flags_int;
    // wire                                                                top_valid_int;

    wire        [5 - 1:0]                                               left_intrapredacc_lower5b_int;
    wire        [5 - 1:0]                                               top_intrapredacc_lower5b_int;
    reg         [5 - 1:0]                                               intrapredacc_lower5b_int;

    wire        [7 - 1:0]                                               left_extended_arr_index_4clks_delay_int;
    wire        [7 - 1:0]                                               top_extended_arr_index_4clks_delay_int;
    wire        [6 - 1:0]                                               left_extended_arr_index_int;
    wire        [6 - 1:0]                                               top_extended_arr_index_int;
    wire        [TOP_PRED_MODE_WIDTH - 1:0]                             top_mode_modified_for_ver_hor_int;
    wire        [TOP_PRED_MODE_WIDTH - 1:0]                             left_mode_modified_for_ver_hor_int;

    // Top Aux data supllier
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               top_aux_read_addr_int;
    wire        [PIXEL_WIDTH - 1:0]                                     top_aux_data_0_int;
    wire        [PIXEL_WIDTH - 1:0]                                     top_aux_data_1_int;
    wire        [PIXEL_WIDTH - 1:0]                                     top_aux_data_2_int;
    wire        [PIXEL_WIDTH - 1:0]                                     top_aux_data_3_int;
    wire        [PIXEL_WIDTH - 1:0]                                     top_aux_data_4_int;

    // Left read addr module
    wire                                                                left_read_from_aux_flag_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               left_read_addr_to_aux_flag_gen_int;
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               left_read_addr_from_aux_flag_gen_int;
    wire        [4:0]                                                   left_aux_flags_int;
    // wire                                                                left_valid_int;

    // Left Aux data supllier
    wire        [PIXEL_ADDR_LENGTH - 1:0]                               left_aux_read_addr_int;
    wire        [PIXEL_WIDTH - 1:0]                                     left_aux_data_0_int;
    wire        [PIXEL_WIDTH - 1:0]                                     left_aux_data_1_int;
    wire        [PIXEL_WIDTH - 1:0]                                     left_aux_data_2_int;
    wire        [PIXEL_WIDTH - 1:0]                                     left_aux_data_3_int;
    wire        [PIXEL_WIDTH - 1:0]                                     left_aux_data_4_int;

    // predsample gen module
    wire                                                                dc_init_done_int;
    wire        [MAX_NTBS_SIZE - 1:0]                                   dc_read_addr_offset_int;

    // Top Level FSMs

    integer                                                             state;
    integer                                                             linebuf_addr_state;

    reg         [PIXEL_WIDTH - 1:0]                                     corner_pixel_reg;
    reg         [PIXEL_WIDTH - 1:0]                                     top_zeroth_reg;
    reg         [PIXEL_WIDTH - 1:0]                                     left_zeroth_reg;
    reg         [PIXEL_WIDTH - 1:0]                                     top_nt_reg;
    reg         [PIXEL_WIDTH - 1:0]                                     left_nt_reg;
    reg         [PIXEL_WIDTH - 1:0]                                     top_nt_plus1_reg;
    reg         [PIXEL_WIDTH - 1:0]                                     left_nt_plus1_reg;
    reg         [PIXEL_WIDTH - 1:0]                                     top_2nt_reg;
    reg         [PIXEL_WIDTH - 1:0]                                     left_2nt_reg;

    wire        [PIXEL_WIDTH - 1:0]                                     top_zeroth_filtered_int;
    wire        [PIXEL_WIDTH - 1:0]                                     left_zeroth_filtered_int;
    wire        [PIXEL_WIDTH - 1:0]                                     top_nt_plus1_filtered_int;
    wire        [PIXEL_WIDTH - 1:0]                                     left_nt_plus1_filtered_int;

    reg         [PIXEL_WIDTH - 1:0]                                     top_zeroth_common_int;
    reg         [PIXEL_WIDTH - 1:0]                                     left_zeroth_common_int;
    reg         [PIXEL_WIDTH - 1:0]                                     top_nt_plus1_common_int;
    reg         [PIXEL_WIDTH - 1:0]                                     left_nt_plus1_common_int;


    reg         [MAX_NTBS_SIZE - 1:0]                                   mindistverhor_reg;
    reg         [INTRAHORVERDISTTHRES_WIDTH - 1:0]                      intrahorverdistthres_reg;
    reg                                                                 filter_flag;
    reg                                                                 filter_start;
    reg                                                                 bi_int_filter_flag;
    reg         [PIXEL_WIDTH + 2 - 1:0]                                 bi_int_flag_cond1;
    reg         [PIXEL_WIDTH + 2 - 1:0]                                 abs_bi_int_flag_cond1;
    reg         [PIXEL_WIDTH + 2 - 1:0]                                 bi_int_flag_cond2;
    reg         [PIXEL_WIDTH + 2 - 1:0]                                 abs_bi_int_flag_cond2;
    reg                                                                 input_muxer_valid_reg;
    reg                                                                 bi_int_filter_cond_ready;

    wire                                                                one_before_last_4by4_int;

//    reg
    reg                                                                 intra_inter_mode_reg;
    reg                                                                 cu_done_out_reg;


    // //intra min range signals
    // reg                                                                 intra_min_range_mem_porta_wen_reg;
    // reg         [LOG2_FRAME_SIZE - 3 - 1:0]                             intra_min_range_mem_porta_addr_reg;
    // reg         [LOG2_FRAME_SIZE - 3 - 1:0]                             intra_min_range_mem_porta_wdata_reg;
    // reg         [LOG2_FRAME_SIZE - 3 - 1:0]                             intra_min_range_mem_porta_rdata_wire;

    // reg                                                                 intra_min_range_mem_portb_wen_reg;
    // reg         [LOG2_FRAME_SIZE - 3 - 1:0]                             intra_min_range_mem_portb_addr_reg;
    // reg         [LOG2_FRAME_SIZE - 3 - 1:0]                             intra_min_range_mem_portb_wdata_reg;
    // reg         [LOG2_FRAME_SIZE - 3 - 1:0]                             intra_min_range_mem_portb_rdata_wire;

    // integer                                                             reset_state;
    // integer                                                             min_range_update_state;
    // reg                                                                 prior_update_check_logic;

    // reg         [5 - 1:0]                                               update_eightblock_offset_mem[4 - 1:0][5 - 1:0];
    // wire        [LOG2_FRAME_SIZE - 3 - 1:0]                             eightblock_offset_value[5 - 1:0];
    // reg         [3 - 1:0]                                               eightblock_offset_counter;



    //Constrained Range xt_reg and yt_reg
    reg         [LOG2_FRAME_SIZE - 3 - 1:0]                             constrained_range_x_div_8_reg;
    reg         [LOG2_FRAME_SIZE - 3 - 1:0]                             constrained_range_y_div_8_reg;
    reg         [MAX_NTBS_SIZE - 1:0]                                   constrained_range_ntbs_reg;
    reg                                                                 constrained_range_valid_reg;
    reg                                                                 constrained_range_intra_inter_reg;
    wire                                                                constrained_range_ready_wire;

    wire        [LOG2_FRAME_SIZE - 1 : 0]                               top_constrained_min_range_wire;
    wire        [LOG2_FRAME_SIZE - 1 : 0]                               left_constrained_inner_min_range_wire;
    reg         [LOG2_FRAME_SIZE - 1 : 0]                               left_constrained_min_range_reg;

    wire        [LOG2_FRAME_SIZE - 1:0]                                 leftwr_addr_plus_4_wire;
    wire        [LOG2_FRAME_SIZE - 1:0]                                 topwr_addr_plus_4_wire;


    reg                                                                 valid_reg;

    wire        [PIXEL_WIDTH - 1:0]                                     top_corner_cand1_wire;
    wire        [PIXEL_WIDTH - 1:0]                                     top_corner_cand2_wire;
    wire        [PIXEL_WIDTH - 1:0]                                     top_corner_cand2_reg;
    wire        [PIXEL_WIDTH - 1:0]                                     left_corner_cand1_wire;
    wire        [PIXEL_WIDTH - 1:0]                                     left_corner_cand2_wire;
    wire        [PIXEL_WIDTH - 1:0]                                     left_corner_cand2_reg;

    reg                                                                 top_portw_en_mask_3b_d;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 top_portw_yaddr_div4_d; 
    reg         [LOG2_FRAME_SIZE - 1:0]                                 top_portw_xaddr_div4_d; 

    reg                                                                 left_portw_en_mask_3b_d;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 left_portw_yaddr_div4_d;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 left_portw_xaddr_div4_d;






//----------------------------------------------------
// Implementation
//----------------------------------------------------

    // always @(posedge clk) begin
        // case(intramode_in)
            // 6'd0  :  abs_intrapredangle_reg <= (0 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd1  :  abs_intrapredangle_reg <= (0 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd2  :  abs_intrapredangle_reg <= (32)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd3  :  abs_intrapredangle_reg <= (26)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd4  :  abs_intrapredangle_reg <= (21)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd5  :  abs_intrapredangle_reg <= (17)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd6  :  abs_intrapredangle_reg <= (13)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd7  :  abs_intrapredangle_reg <= (9 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd8  :  abs_intrapredangle_reg <= (5 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd9  :  abs_intrapredangle_reg <= (2 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd10 :  abs_intrapredangle_reg <= (0 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd11 :  abs_intrapredangle_reg <= (2 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd12 :  abs_intrapredangle_reg <= (5 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd13 :  abs_intrapredangle_reg <= (9 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd14 :  abs_intrapredangle_reg <= (13)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd15 :  abs_intrapredangle_reg <= (17)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd16 :  abs_intrapredangle_reg <= (21)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd17 :  abs_intrapredangle_reg <= (26)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd18 :  abs_intrapredangle_reg <= (32)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd19 :  abs_intrapredangle_reg <= (26)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd20 :  abs_intrapredangle_reg <= (21)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd21 :  abs_intrapredangle_reg <= (17)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd22 :  abs_intrapredangle_reg <= (13)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd23 :  abs_intrapredangle_reg <= (9 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd24 :  abs_intrapredangle_reg <= (5 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd25 :  abs_intrapredangle_reg <= (2 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd26 :  abs_intrapredangle_reg <= (0 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd27 :  abs_intrapredangle_reg <= (2 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd28 :  abs_intrapredangle_reg <= (5 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd29 :  abs_intrapredangle_reg <= (9 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd30 :  abs_intrapredangle_reg <= (13)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd31 :  abs_intrapredangle_reg <= (17)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd32 :  abs_intrapredangle_reg <= (21)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd33 :  abs_intrapredangle_reg <= (26)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
            // 6'd34 :  abs_intrapredangle_reg <= (32)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
        // endcase
    // end

    // always @(posedge clk) begin
        // case(intramode_in)
            // 6'd0  :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd1  :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd2  :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd3  :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd4  :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd5  :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd6  :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd7  :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd8  :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd9  :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd10 :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd11 :  sgn_intrapredangle_reg <= 1'b1;
            // 6'd12 :  sgn_intrapredangle_reg <= 1'b1;
            // 6'd13 :  sgn_intrapredangle_reg <= 1'b1;
            // 6'd14 :  sgn_intrapredangle_reg <= 1'b1;
            // 6'd15 :  sgn_intrapredangle_reg <= 1'b1;
            // 6'd16 :  sgn_intrapredangle_reg <= 1'b1;
            // 6'd17 :  sgn_intrapredangle_reg <= 1'b1;
            // 6'd18 :  sgn_intrapredangle_reg <= 1'b1;
            // 6'd19 :  sgn_intrapredangle_reg <= 1'b1;
            // 6'd20 :  sgn_intrapredangle_reg <= 1'b1;
            // 6'd21 :  sgn_intrapredangle_reg <= 1'b1;
            // 6'd22 :  sgn_intrapredangle_reg <= 1'b1;
            // 6'd23 :  sgn_intrapredangle_reg <= 1'b1;
            // 6'd24 :  sgn_intrapredangle_reg <= 1'b1;
            // 6'd25 :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd26 :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd27 :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd28 :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd29 :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd30 :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd31 :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd32 :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd33 :  sgn_intrapredangle_reg <= 1'b0;
            // 6'd34 :  sgn_intrapredangle_reg <= 1'b0;
        // endcase
    // end

    always @(*) begin
        case(log2ctbsize_reg)
            3'd4 : begin
                left_constrained_min_range_reg = {yt_reg[LOG2_FRAME_SIZE - 1:4],left_constrained_inner_min_range_wire[3:0]};
            end
            3'd5 : begin
                left_constrained_min_range_reg = {yt_reg[LOG2_FRAME_SIZE - 1:5],left_constrained_inner_min_range_wire[4:0]};
            end
            default : begin
                left_constrained_min_range_reg = {yt_reg[LOG2_FRAME_SIZE - 1:6],left_constrained_inner_min_range_wire[5:0]};
            end
        endcase
    end

    assign top_nt_out = top_nt_reg;
    assign left_nt_out = left_nt_reg;

    always @(posedge clk) begin : delaying_valid
        if (reset) begin
            valid_reg <= 1'b0;
        end
        else begin
            valid_reg <= valid_in;
        end
    end

    always @(posedge clk) begin : delaying_cu_done_out
        cu_done_out_reg <= cu_done_out;
    end

    always @(posedge clk) begin
        if(reset) begin
            nt_pixels_valid_out <= 1'b0;
        end
        else begin
            case(linebuf_addr_state)
                LINEBUF_ADDR_STATE_2NT : begin
                    nt_pixels_valid_out <= 1'b1;
                end
                default : begin
                    nt_pixels_valid_out <= 1'b0;
                end
            endcase
        end
    end


    always @(posedge clk) begin : _config
        if(enable) begin
            if(CIDX == 0) begin
                frame_height_reg                <= frame_height_in;
                frame_width_reg                 <= frame_width_in;
                tile_width_reg                  <= tile_width_in;
                tile_xc_reg                     <= tile_xc_in;
                tile_yc_reg                     <= tile_yc_in;
                slice_x_reg                     <= slice_x_in;
                slice_y_reg                     <= slice_y_in;
                strong_intra_smoothing_flag_reg <= strong_intra_smoothing_flag_in;
                constrained_intra_pred_flag     <= constrained_intra_pred_flag_in;

                log2ctbsize_reg         <= log2ctbsize_in;
                log2ctbsize_config_reg  <= log2ctbsize_config_in;
            end
            else begin
                frame_height_reg                <= frame_height_in >> 1;
                frame_width_reg                 <= frame_width_in >> 1;
                tile_width_reg                  <= tile_width_in >> 1;
                tile_xc_reg                     <= tile_xc_in >> 1;
                tile_yc_reg                     <= tile_yc_in >> 1;
                slice_x_reg                     <= slice_x_in >> 1;
                slice_y_reg                     <= slice_y_in >> 1;
                strong_intra_smoothing_flag_reg <= strong_intra_smoothing_flag_in;
                constrained_intra_pred_flag     <= constrained_intra_pred_flag_in;

                log2ctbsize_reg         <= log2ctbsize_in - 1'b1;
                log2ctbsize_config_reg  <= log2ctbsize_config_in;
            end
        end
    end

    always @(posedge clk) begin
        if (enable) begin
            case(intramode_in)
                6'd1    :  top_pred_mode_reg <= TOP_PRED_MODE_DC;
                6'd0    :  top_pred_mode_reg <= TOP_PRED_MODE_PLANAR;
                default :  begin
                    if(intramode_in >= 6'd18 ) begin
                        top_pred_mode_reg <= TOP_PRED_MODE_ANGULAR_TOP;
                    end
                    else begin
                        top_pred_mode_reg <= TOP_PRED_MODE_ANGULAR_LEFT;
                    end
                end
            endcase
        end
    end

    always @(posedge clk) begin
        if(reset) begin
            linebuf_addr_state          <= LINEBUF_ADDR_STATE_INIT;
            input_muxer_valid_reg       <= 1'b0;
            bi_int_filter_cond_ready    <= 1'b0;
        end
        else if (enable) begin
            case(linebuf_addr_state)
                LINEBUF_ADDR_STATE_INIT : begin
                    input_muxer_valid_reg <= 1'b0;

                    if((cu_done_out_reg == 1'b0)&&(top_portw_en_mask_3b_d == 1'b0)) begin
                        linebuf_addr_state <= LINEBUF_ADDR_STATE_ZEROTH;
                        corner_pixel_mem_port_a_xt_div_4_raddr <= xt_reg[MAX_NTBS_SIZE - 1:2];
                        corner_pixel_mem_port_a_yt_div_4_raddr <= yt_reg[MAX_NTBS_SIZE - 1:2];
                    end
                end
                LINEBUF_ADDR_STATE_ZEROTH : begin
                    if(available_pixel_checker_valid_int == 1'b1) begin
                        linebuf_addr_state <=   LINEBUF_ADDR_STATE_NT;

                        if(validrange_top_int >= xt_reg + (ntbs_reg) - 1'b1) begin
                            xt_nt_reg <= xt_reg + ((ntbs_reg) - 1'b1);
                        end
                        else begin
                            xt_nt_reg <= validrange_top_int;
                        end

                        if(validrange_left_int >= yt_reg + (ntbs_reg ) - 1'b1) begin
                            yt_nt_reg <= yt_reg + ((ntbs_reg) - 1'b1);
                        end
                        else begin
                            yt_nt_reg <= validrange_left_int;
                        end
                    end
                end
                LINEBUF_ADDR_STATE_NT : begin

                    if(all_invalid_left_int & all_invalid_top_int) begin
                        top_zeroth_reg      <= (1'b1 << (BIT_DEPTH - 1));
                        left_zeroth_reg     <= (1'b1 << (BIT_DEPTH - 1));

                        corner_pixel_reg    <= (1'b1 << (BIT_DEPTH - 1));
                        bi_int_flag_cond1   <= (1'b1 << (BIT_DEPTH - 1));
                        bi_int_flag_cond2   <= (1'b1 << (BIT_DEPTH - 1));
                    end
                    else if(all_invalid_left_int) begin
                        top_zeroth_reg      <= linebuf_top_portr_data0;
                        left_zeroth_reg     <= linebuf_top_portr_data0;

                        corner_pixel_reg    <= linebuf_top_portr_data0;
                        bi_int_flag_cond1   <= linebuf_top_portr_data0;
                        bi_int_flag_cond2   <= linebuf_top_portr_data0;
                    end
                    else if(all_invalid_top_int) begin
                        top_zeroth_reg      <= linebuf_left_portr_data0;
                        left_zeroth_reg     <= linebuf_left_portr_data0;

                        corner_pixel_reg    <= linebuf_left_portr_data0;
                        bi_int_flag_cond1   <= linebuf_left_portr_data0;
                        bi_int_flag_cond2   <= linebuf_left_portr_data0;
                    end
                    else begin
                        top_zeroth_reg      <= linebuf_top_portr_data0;
                        left_zeroth_reg     <= linebuf_left_portr_data0;

                        if(top_constrained_min_range_wire < xt_reg) begin
                            corner_pixel_reg    <= corner_pixel_mem_port_a_rdata_int;
                        end
                        else begin
                            corner_pixel_reg    <= linebuf_left_portr_data0;
                        end
                        bi_int_flag_cond1   <= corner_pixel_mem_port_a_rdata_int;
                        bi_int_flag_cond2   <= corner_pixel_mem_port_a_rdata_int;
                    end

                    linebuf_addr_state <=   LINEBUF_ADDR_STATE_2NT;

                    // corner_pixel_mem_port_a_xt_div_4_reg <= xt_reg[MAX_NTBS_SIZE - 1:2] + ntbs_reg[MAX_NTBS_SIZE - 1:2];
                    // corner_pixel_mem_port_a_yt_div_4_reg <= yt_reg[MAX_NTBS_SIZE - 1:2];

                    // corner_pixel_mem_port_b_xt_div_4_reg <= xt_reg[MAX_NTBS_SIZE - 1:2];
                    // corner_pixel_mem_port_b_yt_div_4_reg <= yt_reg[MAX_NTBS_SIZE - 1:2] + ntbs_reg[MAX_NTBS_SIZE - 1:2];



                    if(validrange_top_int >= xt_reg + (ntbs_reg << 1) - 1'b1) begin
                        xt_2nt_reg <= xt_reg + ((ntbs_reg << 1) - 1'b1);
                    end
                    else begin
                        xt_2nt_reg <= validrange_top_int;
                    end

                    if(validrange_left_int >= yt_reg + (ntbs_reg << 1) - 1'b1) begin
                        yt_2nt_reg <= yt_reg + ((ntbs_reg << 1) - 1'b1);
                    end
                    else begin
                        yt_2nt_reg <= validrange_left_int;
                    end
                end
                LINEBUF_ADDR_STATE_2NT : begin

                    if(all_invalid_left_int & all_invalid_top_int) begin
                        top_nt_reg          <= (1'b1 << (BIT_DEPTH - 1));
                        left_nt_reg         <= (1'b1 << (BIT_DEPTH - 1));
                        top_nt_plus1_reg    <= (1'b1 << (BIT_DEPTH - 1));
                        left_nt_plus1_reg   <= (1'b1 << (BIT_DEPTH - 1));

                        bi_int_flag_cond1   <= bi_int_flag_cond1 - (1'b1 << (BIT_DEPTH - 1));
                        bi_int_flag_cond2   <= bi_int_flag_cond2 - (1'b1 << (BIT_DEPTH - 1));
                    end
                    else if(all_invalid_left_int) begin
                        top_nt_reg          <= linebuf_top_portr_data0;
                        left_nt_reg         <= top_zeroth_reg;
                        top_nt_plus1_reg    <= linebuf_top_portr_data1;
                        left_nt_plus1_reg   <= top_zeroth_reg;

                        bi_int_flag_cond1   <= bi_int_flag_cond1 - (linebuf_top_portr_data0 << 1);
                        bi_int_flag_cond2   <= bi_int_flag_cond2 - (top_zeroth_reg << 1);
                    end
                    else if(all_invalid_top_int) begin
                        top_nt_reg          <= left_zeroth_reg;
                        left_nt_reg         <= linebuf_left_portr_data0;
                        top_nt_plus1_reg    <= left_zeroth_reg;
                        left_nt_plus1_reg   <= linebuf_left_portr_data1;

                        bi_int_flag_cond1   <= bi_int_flag_cond1 - (left_zeroth_reg << 1);
                        bi_int_flag_cond2   <= bi_int_flag_cond2 - (linebuf_left_portr_data0 << 1);
                    end
                    else begin
                        top_nt_reg          <= linebuf_top_portr_data0;
                        left_nt_reg         <= linebuf_left_portr_data0;
                        top_nt_plus1_reg    <= linebuf_top_portr_data1;
                        left_nt_plus1_reg   <= linebuf_left_portr_data1;

                        bi_int_flag_cond1   <= bi_int_flag_cond1 - (linebuf_top_portr_data0  << 1);
                        bi_int_flag_cond2   <= bi_int_flag_cond2 - (linebuf_left_portr_data0 << 1);
                    end

                    if(intra_inter_mode_reg == MODE_INTER) begin
                        // if(constrained_intra_pred_flag == 1'b0) begin
                        //     linebuf_addr_state <= LINEBUF_ADDR_STATE_INIT;
                        // end
                        // else begin
                        //     linebuf_addr_state <= LINEBUF_ADDR_STATE_INTER_COPY;
                        // end
                        linebuf_addr_state <= LINEBUF_ADDR_STATE_INIT;
                    end
                    else if(filter_flag == 1'b1) begin
                        linebuf_addr_state <= LINEBUF_ADDR_STATE_FILTER;
                    end
                    else begin
                        input_muxer_valid_reg <= 1'b1;
                        case(top_pred_mode_reg)
                            TOP_PRED_MODE_DC                                      : linebuf_addr_state  <= LINEBUF_ADDR_STATE_DC;
                            TOP_PRED_MODE_PLANAR,TOP_PRED_MODE_ANGULAR_TOP        : linebuf_addr_state  <= LINEBUF_ADDR_STATE_ANGULAR_TOP;
                            TOP_PRED_MODE_ANGULAR_LEFT                            : linebuf_addr_state  <= LINEBUF_ADDR_STATE_ANGULAR_LEFT;
                        endcase
                    end
                end
                LINEBUF_ADDR_STATE_FILTER : begin
                    if(all_invalid_left_int & all_invalid_top_int) begin
                        top_2nt_reg          <= (1'b1 << (BIT_DEPTH - 1));
                        left_2nt_reg         <= (1'b1 << (BIT_DEPTH - 1));

                        bi_int_flag_cond1   <= bi_int_flag_cond1 + (1'b1 << (BIT_DEPTH - 1));
                        bi_int_flag_cond2   <= bi_int_flag_cond2 + (1'b1 << (BIT_DEPTH - 1));
                    end
                    else if(all_invalid_left_int) begin
                        top_2nt_reg          <= linebuf_top_portr_data0;
                        left_2nt_reg         <= top_zeroth_reg;

                        bi_int_flag_cond1   <= bi_int_flag_cond1 + linebuf_top_portr_data0;
                        bi_int_flag_cond2   <= bi_int_flag_cond2 + top_zeroth_reg;
                    end
                    else if(all_invalid_top_int) begin
                        top_2nt_reg          <= left_zeroth_reg;
                        left_2nt_reg         <= linebuf_left_portr_data0;

                        bi_int_flag_cond1   <= bi_int_flag_cond1 + left_zeroth_reg;
                        bi_int_flag_cond2   <= bi_int_flag_cond2 + linebuf_left_portr_data0;
                    end
                    else begin
                        top_2nt_reg          <= linebuf_top_portr_data0;
                        left_2nt_reg         <= linebuf_left_portr_data0;

                        bi_int_flag_cond1   <= bi_int_flag_cond1 + linebuf_top_portr_data0;
                        bi_int_flag_cond2   <= bi_int_flag_cond2 + linebuf_left_portr_data0;
                    end

                    bi_int_filter_cond_ready <= 1'b1;

                    linebuf_addr_state <= LINEBUF_ADDR_STATE_FILTER_WAIT1;
                end
                LINEBUF_ADDR_STATE_FILTER_WAIT1 : begin
                    linebuf_addr_state <= LINEBUF_ADDR_STATE_FILTER_WAIT2;
                end
                LINEBUF_ADDR_STATE_FILTER_WAIT2 : begin
                    linebuf_addr_state <= LINEBUF_ADDR_STATE_FILTER_WAIT3;
                end
                LINEBUF_ADDR_STATE_FILTER_WAIT3 : begin
                    if(filter_done_int == 1'b1) begin
                        linebuf_addr_state <= LINEBUF_ADDR_STATE_FILTER_WAIT4;
                    end
                end
                LINEBUF_ADDR_STATE_FILTER_WAIT4 : begin
                    linebuf_addr_state <= LINEBUF_ADDR_STATE_FILTER_WAIT5;
                end
                LINEBUF_ADDR_STATE_FILTER_WAIT5 : begin
                    linebuf_addr_state <= LINEBUF_ADDR_STATE_FILTER_WAIT6;
                end
                LINEBUF_ADDR_STATE_FILTER_WAIT6 : begin
                    bi_int_filter_cond_ready <= 1'b0;

                    // if(filter_done_int == 1'b1) begin
                        input_muxer_valid_reg <= 1'b1;
                        case(top_pred_mode_reg)
                            TOP_PRED_MODE_DC                                      : linebuf_addr_state  <= LINEBUF_ADDR_STATE_DC;
                            TOP_PRED_MODE_PLANAR,TOP_PRED_MODE_ANGULAR_TOP        : linebuf_addr_state  <= LINEBUF_ADDR_STATE_ANGULAR_TOP;
                            TOP_PRED_MODE_ANGULAR_LEFT                            : linebuf_addr_state  <= LINEBUF_ADDR_STATE_ANGULAR_LEFT;
                        endcase
                    // end
                end
                LINEBUF_ADDR_STATE_ANGULAR_TOP,LINEBUF_ADDR_STATE_ANGULAR_LEFT,LINEBUF_ADDR_STATE_DC : begin

                    input_muxer_valid_reg <= 1'b0;

                    if(cu_done_out == 1'b1) begin
                        linebuf_addr_state          <= LINEBUF_ADDR_STATE_INIT;
                        input_muxer_valid_reg    <= 1'b0;
                    end

                end
                // LINEBUF_ADDR_STATE_INTER_COPY : begin

                // end
            endcase
        end
    end

    always @(*) begin
        case(linebuf_addr_state)
            LINEBUF_ADDR_STATE_ZEROTH : begin
                left_portr_addr = yt_reg;
                top_portr_addr  = xt_reg;
                top_portr_en    = 1'b1;
                left_portr_en   = 1'b1;

                // corner_pixel_mem_port_a_rw_int   = READ;
                // corner_pixel_mem_port_a_wen_int  = 1'b0;
                // corner_pixel_mem_port_a_wdata_int = {PIXEL_WIDTH{1'bx}};

                // corner_pixel_mem_port_b_rw_int   = READ;
                // corner_pixel_mem_port_b_wen_int  = 1'b0;
                // corner_pixel_mem_port_b_wdata_int = {PIXEL_WIDTH{1'bx}};

            end
            LINEBUF_ADDR_STATE_NT : begin
                left_portr_addr = yt_nt_reg;
                top_portr_addr  = xt_nt_reg;
                top_portr_en    = 1'b1;
                left_portr_en   = 1'b1;

                // corner_pixel_mem_port_a_rw_int   = READ;
                // corner_pixel_mem_port_a_wen_int  = 1'b0;
                // corner_pixel_mem_port_a_wdata_int = {PIXEL_WIDTH{1'bx}};

                // corner_pixel_mem_port_b_rw_int   = READ;
                // corner_pixel_mem_port_b_wen_int  = 1'b0;
                // corner_pixel_mem_port_b_wdata_int = {PIXEL_WIDTH{1'bx}};

            end
            LINEBUF_ADDR_STATE_2NT : begin
                left_portr_addr = yt_2nt_reg;
                top_portr_addr  = xt_2nt_reg;
                top_portr_en    = 1'b1;
                left_portr_en   = 1'b1;

                // corner_pixel_mem_port_a_rw_int    = WRITE;
                // corner_pixel_mem_port_a_wen_int   = 1'b1;
                // corner_pixel_mem_port_a_wdata_int = linebuf_top_portr_data0;

                // corner_pixel_mem_port_b_rw_int    = WRITE;

                // if((corner_pixel_mem_port_b_xt_div_4_reg == 4'd0)&&(corner_pixel_mem_port_b_yt_div_4_reg == 4'd0)) begin
                //     corner_pixel_mem_port_b_wen_int   = 1'b0;
                // end
                // else begin
                //     corner_pixel_mem_port_b_wen_int   = 1'b1;
                // end


                // corner_pixel_mem_port_b_wdata_int = linebuf_left_portr_data0;
            end
            LINEBUF_ADDR_STATE_ANGULAR_TOP : begin
                left_portr_addr = top_aux_read_addr_int;
                top_portr_addr  = top_read_addr_from_aux_flag_gen_int;
                top_portr_en    = 1'b1;
                left_portr_en   = 1'b1;

                // corner_pixel_mem_port_a_rw_int   = READ;
                // corner_pixel_mem_port_a_wen_int  = 1'b0;
                // corner_pixel_mem_port_a_wdata_int = {PIXEL_WIDTH{1'bx}};

                // corner_pixel_mem_port_b_rw_int   = READ;
                // corner_pixel_mem_port_b_wen_int  = 1'b0;
                // corner_pixel_mem_port_b_wdata_int = {PIXEL_WIDTH{1'bx}};
            end
            LINEBUF_ADDR_STATE_ANGULAR_LEFT : begin
                left_portr_addr = left_read_addr_from_aux_flag_gen_int;
                top_portr_addr  = left_aux_read_addr_int;
                top_portr_en    = 1'b1;
                left_portr_en   = 1'b1;

                // corner_pixel_mem_port_a_rw_int   = READ;
                // corner_pixel_mem_port_a_wen_int  = 1'b0;
                // corner_pixel_mem_port_a_wdata_int = {PIXEL_WIDTH{1'bx}};

                // corner_pixel_mem_port_b_rw_int   = READ;
                // corner_pixel_mem_port_b_wen_int  = 1'b0;
                // corner_pixel_mem_port_b_wdata_int = {PIXEL_WIDTH{1'bx}};
            end
            LINEBUF_ADDR_STATE_DC,LINEBUF_ADDR_STATE_FILTER : begin
                left_portr_addr = left_read_addr_from_aux_flag_gen_int;
                top_portr_addr  = top_read_addr_from_aux_flag_gen_int;
                top_portr_en    = 1'b1;
                left_portr_en   = 1'b1;

                // corner_pixel_mem_port_a_rw_int   = READ;
                // corner_pixel_mem_port_a_wen_int  = 1'b0;
                // corner_pixel_mem_port_a_wdata_int = {PIXEL_WIDTH{1'bx}};

                // corner_pixel_mem_port_b_rw_int   = READ;
                // corner_pixel_mem_port_b_wen_int  = 1'b0;
                // corner_pixel_mem_port_b_wdata_int = {PIXEL_WIDTH{1'bx}};
            end
            default : begin
                left_portr_addr = left_read_addr_from_aux_flag_gen_int;
                top_portr_addr  = top_read_addr_from_aux_flag_gen_int;
                top_portr_en    = 1'b0;
                left_portr_en   = 1'b0;

                // corner_pixel_mem_port_a_rw_int   = READ;
                // corner_pixel_mem_port_a_wen_int  = 1'b0;
                // corner_pixel_mem_port_a_wdata_int = {PIXEL_WIDTH{1'bx}};

                // corner_pixel_mem_port_b_rw_int   = READ;
                // corner_pixel_mem_port_b_wen_int  = 1'b0;
                // corner_pixel_mem_port_b_wdata_int = {PIXEL_WIDTH{1'bx}};
            end
        endcase
    end

    always @(*) begin
        case(linebuf_addr_state)
            LINEBUF_ADDR_STATE_ANGULAR_TOP  : intrapredacc_lower5b_int = top_intrapredacc_lower5b_int;
            LINEBUF_ADDR_STATE_ANGULAR_LEFT : intrapredacc_lower5b_int = left_intrapredacc_lower5b_int;
            default :  intrapredacc_lower5b_int = top_intrapredacc_lower5b_int;
        endcase
    end

    always @(posedge clk) begin
        if(reset) begin
            state               <= STATE_INIT;
            inner_x_reg         <= {MAX_NTBS_SIZE{1'b0}};
            inner_y_reg         <= {MAX_NTBS_SIZE{1'b0}};
            cu_done_out         <= 1'b1;
            filter_flag         <= 1'b0;
            filter_start        <= 1'b0;
        end
        else if (enable) begin
            case(state)
                STATE_INIT : begin
                    if(valid_in) begin
                        // ready_out <= 1'b0;

                        case(CIDX)
                            0 : begin
                                xt_reg  <= xt_in;
                                yt_reg  <= yt_in;
                                ntbs_reg <= ntbs_in;
                            end
                            default : begin
                                xt_reg  <= xt_in >> 1;
                                yt_reg  <= yt_in >> 1;
                                ntbs_reg <= ntbs_in >> 1;
                            end
                        endcase

                        inner_x_reg         <= {MAX_NTBS_SIZE{1'b0}};
                        inner_y_reg         <= {MAX_NTBS_SIZE{1'b0}};

                        cu_done_out     <= 1'b0;
                        // curr_valid_reg  <= 1'b1;
                        intramode_reg   <= intramode_in;

                        if(intramode_in == 6'd35) begin
                            intra_inter_mode_reg <= MODE_INTER;
                        end
                        else begin
                            intra_inter_mode_reg <= MODE_INTRA;
                        end

                        case(intramode_in)
                            6'd0  :  abs_intrapredangle_reg <= (0 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd1  :  abs_intrapredangle_reg <= (0 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd2  :  abs_intrapredangle_reg <= (32)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd3  :  abs_intrapredangle_reg <= (26)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd4  :  abs_intrapredangle_reg <= (21)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd5  :  abs_intrapredangle_reg <= (17)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd6  :  abs_intrapredangle_reg <= (13)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd7  :  abs_intrapredangle_reg <= (9 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd8  :  abs_intrapredangle_reg <= (5 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd9  :  abs_intrapredangle_reg <= (2 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd10 :  abs_intrapredangle_reg <= (0 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd11 :  abs_intrapredangle_reg <= (2 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd12 :  abs_intrapredangle_reg <= (5 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd13 :  abs_intrapredangle_reg <= (9 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd14 :  abs_intrapredangle_reg <= (13)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd15 :  abs_intrapredangle_reg <= (17)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd16 :  abs_intrapredangle_reg <= (21)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd17 :  abs_intrapredangle_reg <= (26)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd18 :  abs_intrapredangle_reg <= (32)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd19 :  abs_intrapredangle_reg <= (26)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd20 :  abs_intrapredangle_reg <= (21)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd21 :  abs_intrapredangle_reg <= (17)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd22 :  abs_intrapredangle_reg <= (13)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd23 :  abs_intrapredangle_reg <= (9 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd24 :  abs_intrapredangle_reg <= (5 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd25 :  abs_intrapredangle_reg <= (2 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd26 :  abs_intrapredangle_reg <= (0 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd27 :  abs_intrapredangle_reg <= (2 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd28 :  abs_intrapredangle_reg <= (5 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd29 :  abs_intrapredangle_reg <= (9 )  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd30 :  abs_intrapredangle_reg <= (13)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd31 :  abs_intrapredangle_reg <= (17)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd32 :  abs_intrapredangle_reg <= (21)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd33 :  abs_intrapredangle_reg <= (26)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                            6'd34 :  abs_intrapredangle_reg <= (32)  % ( 1'b1 << INTRA_PRED_ANGLE_WIDTH);
                        endcase

                        case(intramode_in)
                            6'd0  :  sgn_intrapredangle_reg <= 1'b0;
                            6'd1  :  sgn_intrapredangle_reg <= 1'b0;
                            6'd2  :  sgn_intrapredangle_reg <= 1'b0;
                            6'd3  :  sgn_intrapredangle_reg <= 1'b0;
                            6'd4  :  sgn_intrapredangle_reg <= 1'b0;
                            6'd5  :  sgn_intrapredangle_reg <= 1'b0;
                            6'd6  :  sgn_intrapredangle_reg <= 1'b0;
                            6'd7  :  sgn_intrapredangle_reg <= 1'b0;
                            6'd8  :  sgn_intrapredangle_reg <= 1'b0;
                            6'd9  :  sgn_intrapredangle_reg <= 1'b0;
                            6'd10 :  sgn_intrapredangle_reg <= 1'b0;
                            6'd11 :  sgn_intrapredangle_reg <= 1'b1;
                            6'd12 :  sgn_intrapredangle_reg <= 1'b1;
                            6'd13 :  sgn_intrapredangle_reg <= 1'b1;
                            6'd14 :  sgn_intrapredangle_reg <= 1'b1;
                            6'd15 :  sgn_intrapredangle_reg <= 1'b1;
                            6'd16 :  sgn_intrapredangle_reg <= 1'b1;
                            6'd17 :  sgn_intrapredangle_reg <= 1'b1;
                            6'd18 :  sgn_intrapredangle_reg <= 1'b1;
                            6'd19 :  sgn_intrapredangle_reg <= 1'b1;
                            6'd20 :  sgn_intrapredangle_reg <= 1'b1;
                            6'd21 :  sgn_intrapredangle_reg <= 1'b1;
                            6'd22 :  sgn_intrapredangle_reg <= 1'b1;
                            6'd23 :  sgn_intrapredangle_reg <= 1'b1;
                            6'd24 :  sgn_intrapredangle_reg <= 1'b1;
                            6'd25 :  sgn_intrapredangle_reg <= 1'b1;
                            6'd26 :  sgn_intrapredangle_reg <= 1'b0;
                            6'd27 :  sgn_intrapredangle_reg <= 1'b0;
                            6'd28 :  sgn_intrapredangle_reg <= 1'b0;
                            6'd29 :  sgn_intrapredangle_reg <= 1'b0;
                            6'd30 :  sgn_intrapredangle_reg <= 1'b0;
                            6'd31 :  sgn_intrapredangle_reg <= 1'b0;
                            6'd32 :  sgn_intrapredangle_reg <= 1'b0;
                            6'd33 :  sgn_intrapredangle_reg <= 1'b0;
                            6'd34 :  sgn_intrapredangle_reg <= 1'b0;
                        endcase

                        if( intramode_in > 5'd18 ) begin
                            if(intramode_in > 5'd26) begin
                                mindistverhor_reg <= intramode_in - 5'd26;
                            end
                            else begin
                                mindistverhor_reg <= 5'd26 - intramode_in;
                            end
                        end
                        else begin
                            if(intramode_in > 5'd10) begin
                                mindistverhor_reg <= intramode_in - 5'd10;
                            end
                            else begin
                                mindistverhor_reg <= 5'd10 - intramode_in;
                            end
                        end

                        case(ntbs_in)
                            {{(MAX_NTBS_SIZE - 4){1'b0}},4'd8}  : intrahorverdistthres_reg <= {{(INTRAHORVERDISTTHRES_WIDTH - 3){1'b0}},3'd7};
                            {{(MAX_NTBS_SIZE - 5){1'b0}},5'd16} : intrahorverdistthres_reg <= {{(INTRAHORVERDISTTHRES_WIDTH - 3){1'b0}},3'd1};
                            {{(MAX_NTBS_SIZE - 6){1'b0}},6'd32} : intrahorverdistthres_reg <= {{(INTRAHORVERDISTTHRES_WIDTH - 3){1'b0}},3'd0};
                            default                             : intrahorverdistthres_reg <= {{(INTRAHORVERDISTTHRES_WIDTH - 3){1'b0}},3'd0};
                        endcase


                        state <= STATE_PREWAIT;   // do you have to goto pre_wait state if mode is inter?
                    end
                end
                STATE_PREWAIT : begin
                    // cu_done_out     <= 1'b0;
                    curr_valid_reg  <= 1'b1;
                    state <= STATE_WAIT;
                end
                STATE_WAIT : begin
                    curr_valid_reg <= 1'b0;

                        if(intra_inter_mode_reg == MODE_INTER) begin
                            state <= STATE_INIT;
                            cu_done_out <= 1'b1;
                        end
                        else if((ntbs_reg == {{(MAX_NTBS_SIZE - 3){1'b0}},3'd4}) || (top_pred_mode_reg == TOP_PRED_MODE_DC)) begin
                            state <= STATE_PREDTICTION;
                        end
                        else begin
                            if((mindistverhor_reg > intrahorverdistthres_reg)&&(CIDX == 0)) begin
                                state <= STATE_FILTER;
                                filter_flag        <= 1'b1;
                            end
                            else begin
                                state <= STATE_PREDTICTION;
                            end
                        end
                end
                STATE_FILTER : begin
                    if(bi_int_filter_cond_ready == 1'b1) begin
                        filter_start <= 1'b1;

                        if ( (abs_bi_int_flag_cond1 < (1 << (BIT_DEPTH - 5))) && (abs_bi_int_flag_cond2 < (1 << (BIT_DEPTH - 5))) && (ntbs_reg == 6'd32) && (strong_intra_smoothing_flag_reg == 1'b1) ) begin
                            bi_int_filter_flag <=  1'b1;
                        end
                        else begin
                            bi_int_filter_flag <=  1'b0;
                        end

                        state <= STATE_FILTER_WAIT1;
                    end

                    // state <= STATE_PREDTICTION;
                end
                STATE_FILTER_WAIT1 : begin
                    state <= STATE_FILTER_WAIT2;
                    filter_start <= 1'b0;
                end
                STATE_FILTER_WAIT2 : begin
                    state <= STATE_PREDTICTION;
                end
                STATE_PREDTICTION : begin


                    if(filter_done_int || ~filter_flag) begin
//                        filter_flag <= 1'b0;

                        case(top_pred_mode_reg)
                            TOP_PRED_MODE_DC : begin
                                if(CIDX == 0) begin
                                    if(dc_init_done_int) begin
                                        if((inner_y_reg == ntbs_reg - 4'd4) && (inner_x_reg == ntbs_reg - 4'd4)) begin
                                            state <= STATE_DONE;
                                        end
                                        else if((inner_x_reg == ntbs_reg - 4'd4) && (inner_y_reg[2:0] == 3'd4)) begin
                                            inner_y_reg <= inner_y_reg + 4'd4;
                                            inner_x_reg <= {MAX_NTBS_SIZE{1'b0}};
                                        end
                                        else if((inner_x_reg[2:0] == 3'd4)&&(inner_y_reg[2:0] == 3'd4)) begin
                                            inner_x_reg <= inner_x_reg + 3'd4;
                                            inner_y_reg <= inner_y_reg - 3'd4;
                                        end
                                        else if((inner_x_reg[2:0] == 3'd4)&&(inner_y_reg[1:0] == 2'd0)) begin
                                            inner_x_reg <= inner_x_reg - 3'd4;
                                            inner_y_reg <= inner_y_reg + 3'd4;
                                        end
                                        else begin//if(inner_y_reg[1:0] == 2'd0) begin
                                            inner_x_reg <= inner_x_reg + 3'd4;
                                            // inner_y_reg <= inner_y_reg - 3'd3;
                                        end
                                        // else begin
                                        //     inner_y_reg <= inner_y_reg + 3'd1;
                                        // end
                                    end
                                end
                                else begin
                                    if(dc_init_done_int) begin
                                        if((inner_y_reg == ntbs_reg - 4'd4) && (inner_x_reg == ntbs_reg - 4'd4)) begin
                                            state <= STATE_DONE;
                                        end
                                        else if(inner_x_reg == ntbs_reg - 4'd4)begin
                                            inner_y_reg <= inner_y_reg + 4'd4;
                                            inner_x_reg <= {MAX_NTBS_SIZE{1'b0}};
                                        end
                                        else begin//if(inner_y_reg[1:0] == 2'd0) begin
                                            inner_x_reg <= inner_x_reg + 3'd4;
                                            // inner_y_reg <= inner_y_reg - 3'd3;
                                        end
                                        // else begin
                                        //     inner_y_reg <= inner_y_reg + 3'd1;
                                        // end
                                    end
                                end
                            end
                            TOP_PRED_MODE_ANGULAR_LEFT : begin
                                if(CIDX == 0) begin
                                    if((inner_x_reg == ntbs_reg - 1'b1) && (inner_y_reg == ntbs_reg - 4'd4)) begin
                                        state <= STATE_DONE;
                                    end
                                    else if((inner_x_reg == ntbs_reg - 1'b1)&&(inner_y_reg[2:0] == 3'd4)) begin
                                        inner_y_reg <= inner_y_reg + 3'd4;
                                        inner_x_reg <= {MAX_NTBS_SIZE{1'b0}};
                                    end
                                    else if ((inner_x_reg[2:0] == 3'd7) && (inner_y_reg[2:0] == 3'd0)) begin
                                        inner_y_reg <= inner_y_reg + 3'd4;
                                        inner_x_reg <= inner_x_reg - 3'd7;
                                    end
                                    else if ((inner_x_reg[2:0] == 3'd7) && (inner_y_reg[2:0] == 3'd4)) begin
                                        inner_y_reg <= inner_y_reg - 3'd4;
                                        inner_x_reg <= inner_x_reg + 3'd1;
                                    end
                                    else begin
                                        inner_x_reg <= inner_x_reg + 3'd1;
                                    end
                                end
                                else begin
                                    if((inner_x_reg == ntbs_reg - 1'b1) && (inner_y_reg == ntbs_reg - 4'd4)) begin
                                        state <= STATE_DONE;
                                    end
                                    else if(inner_x_reg == ntbs_reg - 1'b1) begin
                                        inner_y_reg <= inner_y_reg + 3'd4;
                                        inner_x_reg <= {MAX_NTBS_SIZE{1'b0}};
                                    end
                                    else begin
                                        inner_x_reg <= inner_x_reg + 3'd1;
                                    end
                                end
                            end
                            TOP_PRED_MODE_PLANAR,TOP_PRED_MODE_ANGULAR_TOP : begin

                                if(CIDX == 0) begin
                                    if((inner_y_reg == ntbs_reg - 1'b1) && (inner_x_reg == ntbs_reg - 4'd4)) begin
                                        state <= STATE_DONE;
                                    end
                                    else if((inner_x_reg == ntbs_reg - 4'd4) && (inner_y_reg[2:0] == 3'd7)) begin
                                        inner_y_reg <= inner_y_reg + 1'b1;
                                        inner_x_reg <= {MAX_NTBS_SIZE{1'b0}};
                                    end
                                    else if((inner_x_reg[2:0] == 3'd4)&&(inner_y_reg[2:0] == 3'd7)) begin
                                        inner_x_reg <= inner_x_reg + 3'd4;
                                        inner_y_reg <= inner_y_reg - 3'd7;
                                    end
                                    else if((inner_x_reg[2:0] == 3'd4)&&(inner_y_reg[1:0] == 2'd3)) begin
                                        inner_x_reg <= inner_x_reg - 3'd4;
                                        inner_y_reg <= inner_y_reg + 1'b1;
                                    end
                                    else if(inner_y_reg[1:0] == 2'd3) begin
                                        inner_x_reg <= inner_x_reg + 3'd4;
                                        inner_y_reg <= inner_y_reg - 3'd3;
                                    end
                                    else begin
                                        inner_y_reg <= inner_y_reg + 3'd1;
                                    end
                                end
                                else begin
                                    if((inner_y_reg == ntbs_reg - 1'b1) && (inner_x_reg == ntbs_reg - 4'd4)) begin
                                        state <= STATE_DONE;
                                    end
                                    else if((inner_x_reg == ntbs_reg - 4'd4) && (inner_y_reg[1:0] == 2'd3)) begin
                                        inner_y_reg <= inner_y_reg + 1'b1;
                                        inner_x_reg <= {MAX_NTBS_SIZE{1'b0}};
                                    end
                                    else if(inner_y_reg[1:0] == 2'd3) begin
                                        inner_x_reg <= inner_x_reg + 3'd4;
                                        inner_y_reg <= inner_y_reg - 2'd3;
                                    end
                                    else begin
                                        inner_y_reg <= inner_y_reg + 3'd1;
                                    end
                                end
                            end
                        endcase
                    end
                end
                STATE_DONE : begin  //TODO add the write back interface signals with inter
                    inner_y_reg <= {MAX_NTBS_SIZE{1'b0}};
                    inner_x_reg <= {MAX_NTBS_SIZE{1'b0}};

                    if((top_portw_en_mask[0][0] == 1'b1)&&(top_portw_addr[0] == (xt_reg + ntbs_reg - 4'd4))) begin
                        cu_done_out <= 1'b1;
                        state <= STATE_INIT;
                        filter_flag <= 1'b0;

                        // inner_y_reg <= {MAX_NTBS_SIZE{1'b0}};
                        // inner_x_reg <= {MAX_NTBS_SIZE{1'b0}};
                    end


                end
            endcase
        end
    end

    always @(*) begin : abs_bi_int_flags

        if(bi_int_flag_cond1[PIXEL_WIDTH + 2 - 1] == 1'b1) begin
            abs_bi_int_flag_cond1 = -bi_int_flag_cond1;
        end
        else begin
            abs_bi_int_flag_cond1 = bi_int_flag_cond1;
        end

        if(bi_int_flag_cond2[PIXEL_WIDTH + 2 - 1] == 1'b1) begin
            abs_bi_int_flag_cond2 = -bi_int_flag_cond2;
        end
        else begin
            abs_bi_int_flag_cond2 = bi_int_flag_cond2;
        end
    end

    assign topwr_addr_plus_4_wire = top_portw_addr[topwr_counter-1] + 3'd4;

    always @(posedge clk) begin : topwrite_fsm
        integer i;
        if (reset) begin
            // reset
            topwr_state    <= TOPWR_STATE_LOW4;
            topwr_counter  <= 1;
            top_portw_en_mask[0]  <= 8'b0000_0000;
        end
        // else if (enable) begin
        //     if(one_before_last_4by4_int == 1'b0 && intra_inter_mode_reg == MODE_INTRA) begin
        //         case(topwr_state)
        //             TOPWR_STATE_LOW4 : begin
        //                 if(top_portw_en_mask_in[0] == 1'b1) begin
        //                     top_portw_addr [topwr_counter]     <=   top_portw_addr_in;
        //                     top_portw_data0[topwr_counter]     <=   top_portw_data0_in;
        //                     top_portw_data1[topwr_counter]     <=   top_portw_data1_in;
        //                     top_portw_data2[topwr_counter]     <=   top_portw_data2_in;
        //                     top_portw_data3[topwr_counter]     <=   top_portw_data3_in;
        //                     top_portw_en_mask[topwr_counter]   <=   8'b0000_1111;
        //                     topwr_counter                   <=   topwr_counter + 1'b1;
        //                     topwr_state                     <=   TOPWR_STATE_HIGH4;
        //                 end
        //             end
        //             TOPWR_STATE_HIGH4 : begin
        //                 if(top_portw_en_mask_in[0] == 1'b1) begin
        //                     top_portw_data4[topwr_counter-1]     <=   top_portw_data0_in;
        //                     top_portw_data5[topwr_counter-1]     <=   top_portw_data1_in;
        //                     top_portw_data6[topwr_counter-1]     <=   top_portw_data2_in;
        //                     top_portw_data7[topwr_counter-1]     <=   top_portw_data3_in;
        //                     top_portw_en_mask[topwr_counter-1]   <=   8'b1111_1111;
        //                     topwr_state                     <=   TOPWR_STATE_LOW4;

        //                 end
        //             end
        //         endcase
        //     end
        else if (enable) begin
            if(one_before_last_4by4_int == 1'b0 && intra_inter_mode_reg == MODE_INTRA) begin
                case(topwr_state)
                    TOPWR_STATE_LOW4 : begin
                        if(top_portw_en_mask_in[0] == 1'b1) begin
                            top_portw_addr [topwr_counter]     <=   top_portw_addr_in;
                            top_portw_yaddr [topwr_counter]    <=   top_portw_yaddr_in;
                            top_portw_data0[topwr_counter]     <=   top_portw_data0_in;
                            top_portw_data1[topwr_counter]     <=   top_portw_data1_in;
                            top_portw_data2[topwr_counter]     <=   top_portw_data2_in;
                            top_portw_data3[topwr_counter]     <=   top_portw_data3_in;
                            top_portw_en_mask[topwr_counter]   <=   8'b0000_1111;
                            topwr_state                         <=   TOPWR_STATE_HIGH4;
                            topwr_counter                       <=   topwr_counter + 1'b1;
                        end
                    end
                    TOPWR_STATE_HIGH4 : begin
                        if(top_portw_en_mask_in[0] == 1'b1) begin
                            if(/*topwr_addr_plus_4_wiretop_portw_addr[topwr_counter-1] + 3'd4 == top_portw_addr_in*/0) begin
                                if(top_portw_en_mask_in[0] == 1'b1) begin
                                    top_portw_data4[topwr_counter-1]     <=   top_portw_data0_in;
                                    top_portw_data5[topwr_counter-1]     <=   top_portw_data1_in;
                                    top_portw_data6[topwr_counter-1]     <=   top_portw_data2_in;
                                    top_portw_data7[topwr_counter-1]     <=   top_portw_data3_in;
                                    top_portw_en_mask[topwr_counter-1]   <=   8'b1111_1111;
                                    topwr_state                        <=   TOPWR_STATE_LOW4;
                                end
                            end
                            else begin
                                top_portw_addr [topwr_counter]     <=   top_portw_addr_in;
                                top_portw_yaddr [topwr_counter]     <=   top_portw_yaddr_in;
                                top_portw_data0[topwr_counter]     <=   top_portw_data0_in;
                                top_portw_data1[topwr_counter]     <=   top_portw_data1_in;
                                top_portw_data2[topwr_counter]     <=   top_portw_data2_in;
                                top_portw_data3[topwr_counter]     <=   top_portw_data3_in;
                                top_portw_en_mask[topwr_counter]   <=   8'b0000_1111;
                                topwr_state                         <=   TOPWR_STATE_HIGH4;
                                topwr_counter                       <=   topwr_counter + 1'b1;
                            end
                        end
                    end
                endcase
            end
            else begin
                topwr_state                     <=   TOPWR_STATE_LOW4;
                if(top_portw_en_mask_in[0] == 1'b1) begin
                    top_portw_addr [0] <= top_portw_addr_in;
                    top_portw_yaddr [0] <= top_portw_yaddr_in;
                    top_portw_data0[0] <= top_portw_data0_in;
                    top_portw_data1[0] <= top_portw_data1_in;
                    top_portw_data2[0] <= top_portw_data2_in;
                    top_portw_data3[0] <= top_portw_data3_in;
                    top_portw_en_mask[0]  <= 8'b0000_1111;
                end
                else begin
                    if(topwr_counter == 1'b1) begin
                        top_portw_en_mask[0]  <=   8'b0000_0000;
                    end
                    else begin
                        for (i = 0 ; i < 30 ; i = i + 1) begin
                            top_portw_addr [i] <=   top_portw_addr[i + 1];
                            top_portw_yaddr [i] <=   top_portw_yaddr[i + 1];
                            top_portw_data0[i] <=   top_portw_data0[i + 1];
                            top_portw_data1[i] <=   top_portw_data1[i + 1];
                            top_portw_data2[i] <=   top_portw_data2[i + 1];
                            top_portw_data3[i] <=   top_portw_data3[i + 1];
                            top_portw_data4[i] <=   top_portw_data4[i + 1];
                            top_portw_data5[i] <=   top_portw_data5[i + 1];
                            top_portw_data6[i] <=   top_portw_data6[i + 1];
                            top_portw_data7[i] <=   top_portw_data7[i + 1];
                            top_portw_en_mask[i]  <=   top_portw_en_mask[i + 1];
                        end


                        topwr_counter <= topwr_counter - 1'b1;
                    end
                end
            end
        end
    end

    assign leftwr_addr_plus_4_wire = left_portw_addr[leftwr_counter-1] + 3'd4;

    always @(posedge clk) begin : leftwrite_fsm
        integer i;
        if (reset) begin
            // reset
            leftwr_state    <= LEFTWR_STATE_LOW4;
            leftwr_counter  <= 1;
            left_portw_en_mask[0]  <=   8'b0000_0000;
        end
        else if (enable) begin
            if(one_before_last_4by4_int == 1'b0 && intra_inter_mode_reg == MODE_INTRA) begin
                case(leftwr_state)
                    LEFTWR_STATE_LOW4 : begin
                        if(left_portw_en_mask_in[0] == 1'b1) begin
                            left_portw_addr [leftwr_counter]     <=   left_portw_addr_in;
                            left_portw_xaddr [leftwr_counter]    <=   left_portw_xaddr_in;
                            left_portw_data0[leftwr_counter]     <=   left_portw_data0_in;
                            left_portw_data1[leftwr_counter]     <=   left_portw_data1_in;
                            left_portw_data2[leftwr_counter]     <=   left_portw_data2_in;
                            left_portw_data3[leftwr_counter]     <=   left_portw_data3_in;
                            left_portw_en_mask[leftwr_counter]   <=   8'b0000_1111;
                            leftwr_state                         <=   LEFTWR_STATE_HIGH4;
                            leftwr_counter                       <=   leftwr_counter + 1'b1;
                        end
                    end
                    LEFTWR_STATE_HIGH4 : begin
                        if(left_portw_en_mask_in[0] == 1'b1) begin
                            if(/*leftwr_addr_plus_4_wireleft_portw_addr[leftwr_counter-1] + 3'd4 == left_portw_addr_in*/0) begin
                                if(left_portw_en_mask_in[0] == 1'b1) begin
                                    left_portw_data4[leftwr_counter-1]     <=   left_portw_data0_in;
                                    left_portw_data5[leftwr_counter-1]     <=   left_portw_data1_in;
                                    left_portw_data6[leftwr_counter-1]     <=   left_portw_data2_in;
                                    left_portw_data7[leftwr_counter-1]     <=   left_portw_data3_in;
                                    left_portw_en_mask[leftwr_counter-1]   <=   8'b1111_1111;
                                    leftwr_state                        <=   LEFTWR_STATE_LOW4;
                                end
                            end
                            else begin
                                left_portw_addr [leftwr_counter]     <=   left_portw_addr_in;
                                left_portw_xaddr [leftwr_counter]    <=   left_portw_xaddr_in;
                                left_portw_data0[leftwr_counter]     <=   left_portw_data0_in;
                                left_portw_data1[leftwr_counter]     <=   left_portw_data1_in;
                                left_portw_data2[leftwr_counter]     <=   left_portw_data2_in;
                                left_portw_data3[leftwr_counter]     <=   left_portw_data3_in;
                                left_portw_en_mask[leftwr_counter]   <=   8'b0000_1111;
                                leftwr_state                         <=   LEFTWR_STATE_HIGH4;
                                leftwr_counter                       <=   leftwr_counter + 1'b1;
                            end
                        end
                    end
                endcase
            end
            else begin
                leftwr_state <= LEFTWR_STATE_LOW4;
                if(left_portw_en_mask_in[0] == 1'b1) begin
                    left_portw_addr [0] <= left_portw_addr_in;
                    left_portw_xaddr [0] <= left_portw_xaddr_in;
                    left_portw_data0[0] <= left_portw_data0_in;
                    left_portw_data1[0] <= left_portw_data1_in;
                    left_portw_data2[0] <= left_portw_data2_in;
                    left_portw_data3[0] <= left_portw_data3_in;
                    left_portw_en_mask[0]  <= 8'b0000_1111;
                end
                else begin
                    if(leftwr_counter == 1'b1) begin
                        left_portw_en_mask[0]  <=   8'b0000_0000;
                    end
                    else begin
                        for (i = 0 ; i < 30 ; i = i + 1) begin
                            left_portw_addr [i] <=   left_portw_addr[i+1];
                            left_portw_xaddr [i]<=   left_portw_xaddr[i+1];
                            left_portw_data0[i] <=   left_portw_data0[i+1];
                            left_portw_data1[i] <=   left_portw_data1[i+1];
                            left_portw_data2[i] <=   left_portw_data2[i+1];
                            left_portw_data3[i] <=   left_portw_data3[i+1];
                            left_portw_data4[i] <=   left_portw_data4[i+1];
                            left_portw_data5[i] <=   left_portw_data5[i+1];
                            left_portw_data6[i] <=   left_portw_data6[i+1];
                            left_portw_data7[i] <=   left_portw_data7[i+1];
                            left_portw_en_mask[i]  <=   left_portw_en_mask[i+1];
                        end

                        leftwr_counter <= leftwr_counter - 1'b1;
                    end
                end
            end
        end
    end

    // always @(*) begin
    //     top_portw_addr      =   top_portw_addr_in;
    //     top_portw_data0     =   top_portw_data0_in;
    //     top_portw_data1     =   top_portw_data1_in;
    //     top_portw_data2     =   top_portw_data2_in;
    //     top_portw_data3     =   top_portw_data3_in;
    //     top_portw_data4     =   top_portw_data4_in;
    //     top_portw_data5     =   top_portw_data5_in;
    //     top_portw_data6     =   top_portw_data6_in;
    //     top_portw_data7     =   top_portw_data7_in;
    //     top_portw_en_mask   =   top_portw_en_mask_in;

    //     left_portw_addr     =   left_portw_addr_in;
    //     left_portw_data0    =   left_portw_data0_in;
    //     left_portw_data1    =   left_portw_data1_in;
    //     left_portw_data2    =   left_portw_data2_in;
    //     left_portw_data3    =   left_portw_data3_in;
    //     left_portw_data4    =   left_portw_data4_in;
    //     left_portw_data5    =   left_portw_data5_in;
    //     left_portw_data6    =   left_portw_data6_in;
    //     left_portw_data7    =   left_portw_data7_in;
    //     left_portw_en_mask  =   left_portw_en_mask_in;
    // end

    // always @(*) begin : update_eightblock_offset_mem_ROM

    //     update_eightblock_offset_mem[0][0] = -5'd4;
    //     update_eightblock_offset_mem[0][1] = -5'd2;
    //     update_eightblock_offset_mem[0][2] = -5'd1;
    //     update_eightblock_offset_mem[0][3] = 5'd0;
    //     update_eightblock_offset_mem[0][4] = 5'd1;

    //     update_eightblock_offset_mem[1][0] = -5'd5;
    //     update_eightblock_offset_mem[1][1] = -5'd3;
    //     update_eightblock_offset_mem[1][2] = -5'd1;
    //     update_eightblock_offset_mem[1][3] = 5'd0;
    //     update_eightblock_offset_mem[1][4] = 5'd1;

    //     update_eightblock_offset_mem[2][0] = -5'd6;
    //     update_eightblock_offset_mem[2][1] = -5'd2;
    //     update_eightblock_offset_mem[2][2] = -5'd1;
    //     update_eightblock_offset_mem[2][3] = 5'd0;
    //     update_eightblock_offset_mem[2][4] = 5'd1;

    //     update_eightblock_offset_mem[3][0] = -5'd7;
    //     update_eightblock_offset_mem[3][1] = -5'd3;
    //     update_eightblock_offset_mem[3][2] = -5'd1;
    //     update_eightblock_offset_mem[3][3] = 5'd0;
    //     update_eightblock_offset_mem[3][4] = 5'd1;

        // update_eightblock_offset_mem[4][0] = 5'd-4;
        // update_eightblock_offset_mem[4][1] = 5'd-2;
        // update_eightblock_offset_mem[4][2] = 5'd-1;
        // update_eightblock_offset_mem[4][3] = 5'd0;
        // update_eightblock_offset_mem[4][4] = 5'd1;

        // update_eightblock_offset_mem[5][0] = 5'd-5;
        // update_eightblock_offset_mem[5][1] = 5'd-3;
        // update_eightblock_offset_mem[5][2] = 5'd-1;
        // update_eightblock_offset_mem[5][3] = 5'd0;
        // update_eightblock_offset_mem[5][4] = 5'd1;

        // update_eightblock_offset_mem[6][0] = 5'd-6;
        // update_eightblock_offset_mem[6][1] = 5'd-2;
        // update_eightblock_offset_mem[6][2] = 5'd-1;
        // update_eightblock_offset_mem[6][3] = 5'd0;
        // update_eightblock_offset_mem[6][4] = 5'd1;

        // update_eightblock_offset_mem[7][0] = 5'd-7;
        // update_eightblock_offset_mem[7][1] = 5'd-3;
        // update_eightblock_offset_mem[7][2] = 5'd-1;
        // update_eightblock_offset_mem[7][3] = 5'd0;
        // update_eightblock_offset_mem[7][4] = 5'd1;

    // end

    // assign eightblock_offset_value[0] ={  {(LOG2_FRAME_SIZE - 3 - 5){update_eightblock_offset_mem[xt_reg[4:3]][0][4]}} ,{update_eightblock_offset_mem[xt_reg[4:3]][0]}};
    // assign eightblock_offset_value[1] ={  {(LOG2_FRAME_SIZE - 3 - 5){update_eightblock_offset_mem[xt_reg[4:3]][0][4]}} ,{update_eightblock_offset_mem[xt_reg[4:3]][1]}};
    // assign eightblock_offset_value[2] ={  {(LOG2_FRAME_SIZE - 3 - 5){update_eightblock_offset_mem[xt_reg[4:3]][0][4]}} ,{update_eightblock_offset_mem[xt_reg[4:3]][2]}};
    // assign eightblock_offset_value[3] ={  {(LOG2_FRAME_SIZE - 3 - 5){update_eightblock_offset_mem[xt_reg[4:3]][0][4]}} ,{update_eightblock_offset_mem[xt_reg[4:3]][3]}};
    // assign eightblock_offset_value[4] ={  {(LOG2_FRAME_SIZE - 3 - 5){update_eightblock_offset_mem[xt_reg[4:3]][0][4]}} ,{update_eightblock_offset_mem[xt_reg[4:3]][4]}};

    // always @(*) begin : proc_
    //     intra_min_range_mem_portb_wdata_reg = {(LOG2_FRAME_SIZE - 3){1'b1}};
    //     intra_min_range_mem_porta_wdata_reg = xt_reg[LOG2_FRAME_SIZE - 1:3];

    //     if(intra_min_range_mem_porta_rdata_wire > xt_reg[LOG2_FRAME_SIZE - 1:3]) begin
    //         prior_update_check_logic = 1'b1;
    //     end
    //     else begin
    //         prior_update_check_logic = 1'b0;
    //     end


    // end

    // always @(posedge clk) begin : resetting_the_intra_min_range_mem
    //     if(reset) begin
    //         reset_state <= RESET_STATE_INIT;
    //     end
    //     else if(enable) begin
    //         case(reset_state)
    //             RESET_STATE_INIT : begin
    //                 if((valid_in)&&(constrained_intra_pred_flag == 1'b1)) begin
    //                     if((xt_in == {LOG2_FRAME_SIZE{1'b0}}) && (yt_in == {LOG2_FRAME_SIZE{1'b0}})) begin
    //                         intra_min_range_mem_portb_addr_reg <= {(LOG2_FRAME_SIZE - 3){1'b0}};
    //                         intra_min_range_mem_portb_wen_reg  <= 1'b1;
    //                         reset_state                        <= RESET_STATE_ACTIVE;
    //                     end
    //                 end
    //             end
    //             RESET_STATE_ACTIVE : begin
    //                 if(intra_min_range_mem_portb_addr_reg == frame_width_reg[LOG2_FRAME_SIZE - 1 : 3]) begin
    //                     intra_min_range_mem_portb_wen_reg <= 1'b0;
    //                     reset_state                       <= RESET_STATE_INIT;
    //                 end
    //                 else begin
    //                     intra_min_range_mem_portb_addr_reg <= intra_min_range_mem_portb_addr_reg + 1'b1;
    //                     intra_min_range_mem_portb_wen_reg <= 1'b1;
    //                 end
    //             end
    //         endcase
    //     end
    // end

    // always @(posedge clk) begin : min_m_range_updation
    //     if(reset) begin
    //         min_range_update_state          <= MIN_RANGE_UPDATE_STATE_INIT;
    //         top_constrained_min_range_wire       <= {LOG2_FRAME_SIZE{1'b0}};
    //         left_constrained_inner_min_range_wire      <= {LOG2_FRAME_SIZE{1'b0}};
    //     end
    //     else begin
    //         case(min_range_update_state)
    //             MIN_RANGE_UPDATE_STATE_INIT : begin
    //                 if(valid_in) begin
    //                     if((intramode_in != 6'd35) && (constrained_intra_pred_flag == 1'b1)) begin
    //                         min_range_update_state              <= MIN_RANGE_UPDATE_STATE_ACTIVE;
    //                         intra_min_range_mem_porta_addr_reg  <= xt_in[LOG2_FRAME_SIZE - 1:3];
    //                         intra_min_range_mem_porta_wen_reg   <= 1'b0;
    //                     end
    //                 end

    //                 if(xt_reg[LOG2_FRAME_SIZE - 1:6] == {(LOG2_FRAME_SIZE - 3){1'b0}}) begin
    //                     case(xt_reg[5:3])
    //                         3'd0 : begin
    //                             eightblock_offset_counter <= 4'd3;
    //                         end
    //                         3'd1 : begin
    //                             eightblock_offset_counter <= 4'd2;
    //                         end
    //                         3'd2 : begin
    //                             eightblock_offset_counter <= 4'd1;
    //                         end
    //                         3'd3 : begin
    //                             eightblock_offset_counter <= 4'd1;
    //                         end
    //                         default : begin
    //                             eightblock_offset_counter <= 4'd0;
    //                         end
    //                     endcase
    //                 end
    //                 else begin
    //                     eightblock_offset_counter <= 4'd0;
    //                 end
    //             end
    //             MIN_RANGE_UPDATE_STATE_ACTIVE : begin
    //                 // intra_min_range_mem_porta_wdata_reg <= xt_reg[LOG2_FRAME_SIZE - 1:3];
    //                 top_constrained_min_range_wire      <= {intra_min_range_mem_porta_rdata_wire,3'd0};
    //                 intra_min_range_mem_porta_addr_reg  <= xt_reg[LOG2_FRAME_SIZE - 1:3] + eightblock_offset_value[eightblock_offset_counter];
    //                 intra_min_range_mem_porta_wen_reg   <= 1'b0;
    //                 min_range_update_state <= MIN_RANGE_UPDATE_STATE_WRITE;
    //             end
    //             MIN_RANGE_UPDATE_STATE_WRITE : begin
    //                 // intra_min_range_mem_porta_wdata_reg <= xt_reg[LOG2_FRAME_SIZE - 1:3];
    //                 intra_min_range_mem_porta_wen_reg   <= prior_update_check_logic;
    //                 min_range_update_state              <= MIN_RANGE_UPDATE_STATE_READ;

    //                 if(eightblock_offset_counter == 3'd4) begin
    //                     // eightblock_offset_counter           <= 4'd0;
    //                     min_range_update_state          <= MIN_RANGE_UPDATE_STATE_INIT;
    //                 end
    //                 else begin
    //                     eightblock_offset_counter           <= eightblock_offset_counter + 1'd1;
    //                     min_range_update_state          <= MIN_RANGE_UPDATE_STATE_READ;
    //                 end
    //             end
    //             MIN_RANGE_UPDATE_STATE_READ : begin
    //                 intra_min_range_mem_porta_addr_reg  <= xt_reg[LOG2_FRAME_SIZE - 1:3] + eightblock_offset_value[eightblock_offset_counter];
    //                 intra_min_range_mem_porta_wen_reg   <= 1'b0;
    //                 min_range_update_state              <= MIN_RANGE_UPDATE_STATE_WRITE;
    //             end
    //         endcase
    //     end
    // end

    // intra_min_range_mem
    // #(
    //     .LOG2_MEM_SIZE(LOG2_FRAME_SIZE - 3),
    //     .DATA_WIDTH(LOG2_FRAME_SIZE - 3)
    // )
    // intra_min_range_mem_block
    // (
    //     .clk                (clk),

    //     .porta_wen_in       (),
    //     .porta_addr_in      (),
    //     .porta_wdata_in     (),
    //     .porta_rdata_out    (),

    //     .portb_wen_in       (intra_min_range_mem_portb_wen_reg),
    //     .portb_addr_in      (intra_min_range_mem_portb_addr_reg),
    //     .portb_wdata_in     (intra_min_range_mem_portb_wdata_reg),
    //     .portb_rdata_out    ()
    // );

    always @(posedge clk) begin
        if(reset) begin
            constrained_range_x_div_8_reg <= {(LOG2_FRAME_SIZE - 3){1'b1}};
            constrained_range_y_div_8_reg <= {(LOG2_FRAME_SIZE - 3){1'b1}};
            constrained_range_valid_reg <= 1'b0;
        end
        else begin
            if(valid_in) begin
                if( (constrained_range_x_div_8_reg != xt_in[LOG2_FRAME_SIZE - 1:3]) || (constrained_range_y_div_8_reg != yt_in[LOG2_FRAME_SIZE - 1:3]) ) begin
                    constrained_range_valid_reg <= 1'b1;
                    constrained_range_x_div_8_reg <= xt_in[LOG2_FRAME_SIZE - 1:3];
                    constrained_range_y_div_8_reg <= yt_in[LOG2_FRAME_SIZE - 1:3];

                    if (intramode_in == 6'd35) begin
                        constrained_range_intra_inter_reg <= MODE_INTER;
                    end
                    else begin
                        constrained_range_intra_inter_reg <= MODE_INTRA;
                    end

                    if(ntbs_in == 6'd4) begin
                        constrained_range_ntbs_reg <= 6'd8;
                    end
                    else begin
                        constrained_range_ntbs_reg <= ntbs_in;
                    end

                end
                else begin
                    constrained_range_valid_reg <= 1'b0;
                end
            end
            else begin
                constrained_range_valid_reg <= 1'b0;
            end
        end
    end

    always @(posedge clk) begin : delay
        // top_portw_en_mask_7b_d <= top_portw_en_mask[0][7];
        top_portw_en_mask_3b_d <= top_portw_en_mask[0][3];
        top_portw_yaddr_div4_d <= top_portw_yaddr[0] >> 2;
        top_portw_xaddr_div4_d <= (top_portw_addr[0] >> 2) + 1'b1; 


        // left_portw_en_mask_7b_d <= left_portw_en_mask[0][7];
        left_portw_en_mask_3b_d <= left_portw_en_mask[0][3];
        left_portw_yaddr_div4_d <= (left_portw_addr[0] >> 2) + 1'b1;
        left_portw_xaddr_div4_d <= left_portw_xaddr[0] >> 2;
    end

    always @(posedge clk) begin : new_corner_update_policy
        if(reset) begin

        end
        else begin
            if(top_portw_en_mask_3b_d) begin
                corner_pixel_mem_port_a_xt_div_4_waddr <= top_portw_xaddr_div4_d[MAX_NTBS_SIZE - 1:0];
                corner_pixel_mem_port_a_yt_div_4_waddr <= top_portw_yaddr_div4_d[MAX_NTBS_SIZE - 1:0];
                corner_pixel_mem_port_a_wdata_int    <= top_corner_cand1_wire;
                corner_pixel_mem_port_a_rw_int       <= 1'b1;
                corner_pixel_mem_port_a_wen_int      <= 1'b1;
            end
            else begin
                corner_pixel_mem_port_a_rw_int       <= 1'b0;
                corner_pixel_mem_port_a_wen_int      <= 1'b0;
            end

            if(left_portw_en_mask_3b_d) begin
                corner_pixel_mem_port_b_xt_div_4_reg     <= left_portw_xaddr_div4_d[MAX_NTBS_SIZE - 1:0];
                corner_pixel_mem_port_b_yt_div_4_reg     <= left_portw_yaddr_div4_d[MAX_NTBS_SIZE - 1:0];
                corner_pixel_mem_port_b_wdata_int    <= left_corner_cand1_wire;

                if ((left_portw_xaddr_div4_d[MAX_NTBS_SIZE - 2 - 1:0] == 4'd0)&&(left_portw_yaddr_div4_d[MAX_NTBS_SIZE - 2 - 1:0] == 4'd0)) begin
                    corner_pixel_mem_port_b_rw_int       <= 1'b0;
                    corner_pixel_mem_port_b_wen_int      <= 1'b0;
                end
                else begin
                    corner_pixel_mem_port_b_rw_int       <= 1'b1;
                    corner_pixel_mem_port_b_wen_int      <= 1'b1;
                end


            end
            else begin
                corner_pixel_mem_port_b_rw_int       <= 1'b0;
                corner_pixel_mem_port_b_wen_int      <= 1'b0;
            end
        end
    end

    always @(*) begin
        if (corner_pixel_mem_port_a_wen_int) begin
            corner_pixel_mem_port_a_xt_div_4_reg = corner_pixel_mem_port_a_xt_div_4_waddr;
            corner_pixel_mem_port_a_yt_div_4_reg = corner_pixel_mem_port_a_yt_div_4_waddr;
        end
        else begin
            corner_pixel_mem_port_a_xt_div_4_reg = corner_pixel_mem_port_a_xt_div_4_raddr;
            corner_pixel_mem_port_a_yt_div_4_reg = corner_pixel_mem_port_a_yt_div_4_raddr;
        end
    end


    intra_constrained_range_gen intra_constrained_range_gen_block
    (
        .clk                            (clk),
        .reset                          (reset),
        .enable                         (1'b1),

        .xcu_in                         (constrained_range_x_div_8_reg),
        .ycu_in                         (constrained_range_y_div_8_reg),
        .ntbs_in                        (constrained_range_ntbs_reg),
        .intra_inter_mode_in            (constrained_range_intra_inter_reg),
        .valid_in                       (constrained_range_valid_reg),
        .ready_out                      (constrained_range_ready_wire),

        .log2_ctusize_in                (log2ctbsize_reg),
        .frame_width_in                 (frame_width_reg),
        .frame_height_in                (frame_height_reg),
        .constrained_intra_pred_flag_in (constrained_intra_pred_flag),
        .config_valid_in                (1'b1),

        .constrained_top_range_out      (top_constrained_min_range_wire),
        .constrained_left_range_out     (left_constrained_inner_min_range_wire),

        .x_second_bit_in                (xt_in[2]),
        .y_second_bit_in                (yt_in[2]),
        .valid_4by4_in                  (valid_in),
        .new_frame_in                   (new_frame_in)
    );

    available_pixel_checker available_pixel_checker_block
    (
        .clk                        (clk),
        .reset                      (reset),
        .enable                     (enable),

        .frame_width_in             (frame_width_reg),
        .frame_height_in            (frame_height_reg),
        .tile_width_in              (tile_width_reg),
        .tile_xc_in                 (tile_xc_reg),
        .tile_yc_in                 (tile_yc_reg),
        .slice_x_in                 (slice_x_reg),
        .slice_y_in                 (slice_y_reg),

        .log2ctbsize_in             (log2ctbsize_reg),
        .log2ctbsize_config_in      (log2ctbsize_config_reg),

        .curr_xin                   (xt_reg),
        .curr_yin                   (yt_reg),
        .ntbs_in                    (ntbs_reg),
        .min_top_available_range_in (top_constrained_min_range_wire),
        .min_left_available_range_in(left_constrained_inner_min_range_wire),
        .curr_valid_in              (curr_valid_reg),

        .validrange_top_out         (validrange_top_int),
        .validrange_left_out        (validrange_left_int),
//        .validrange_topleft_out     (validrange_topleft_int),
        .all_invalid_top_out        (all_invalid_top_int),
        .all_invalid_left_out       (all_invalid_left_int),
        .valid_out                  (available_pixel_checker_valid_int)
    );

    intra_line_buffer intra_line_buffer_top_block
    (
        .clk                         (clk),
        .reset                       (reset),
        .enable                      (enable),

        .portw_addr                  (top_portw_addr[0]),
        .portw_data0                 (top_portw_data0[0]),
        .portw_data1                 (top_portw_data1[0]),
        .portw_data2                 (top_portw_data2[0]),
        .portw_data3                 (top_portw_data3[0]),
        .portw_data4                 (top_portw_data4[0]),
        .portw_data5                 (top_portw_data5[0]),
        .portw_data6                 (top_portw_data6[0]),
        .portw_data7                 (top_portw_data7[0]),
        .portw_en_mask               (top_portw_en_mask[0]),

        .portw_prev_data0            (),
        .portw_prev_data1            (),
        .portw_prev_data2            (),
        .portw_prev_data3            (top_corner_cand1_wire),
        .portw_prev_data4            (),
        .portw_prev_data5            (),
        .portw_prev_data6            (),
        .portw_prev_data7            (),

        .portr_addr                  (top_portr_addr),
        .portr_data0                 (linebuf_top_portr_data0),
        .portr_data1                 (linebuf_top_portr_data1),
        .portr_data2                 (linebuf_top_portr_data2),
        .portr_data3                 (linebuf_top_portr_data3),
        .portr_data4                 (linebuf_top_portr_data4),
        .portr_data5                 (linebuf_top_portr_data5),
        .portr_data6                 (linebuf_top_portr_data6),
        .portr_data7                 (linebuf_top_portr_data7),
        // .portr_en                    (top_portr_en),

        .max_valid_range_in          (validrange_top_int),
        .min_valid_range_in          (top_constrained_min_range_wire)
    );

    intra_line_buffer intra_line_buffer_left_block
    (
        .clk                         (clk),
        .reset                       (reset),
        .enable                      (enable),

        .portw_addr                  (left_portw_addr[0]),
        .portw_data0                 (left_portw_data0[0]),
        .portw_data1                 (left_portw_data1[0]),
        .portw_data2                 (left_portw_data2[0]),
        .portw_data3                 (left_portw_data3[0]),
        .portw_data4                 (left_portw_data4[0]),
        .portw_data5                 (left_portw_data5[0]),
        .portw_data6                 (left_portw_data6[0]),
        .portw_data7                 (left_portw_data7[0]),
        .portw_en_mask               (left_portw_en_mask[0]),

        .portw_prev_data0            (),
        .portw_prev_data1            (),
        .portw_prev_data2            (),
        .portw_prev_data3            (left_corner_cand1_wire),
        .portw_prev_data4            (),
        .portw_prev_data5            (),
        .portw_prev_data6            (),
        .portw_prev_data7            (),

        .portr_addr                  (left_portr_addr),
        .portr_data0                 (linebuf_left_portr_data0),
        .portr_data1                 (linebuf_left_portr_data1),
        .portr_data2                 (linebuf_left_portr_data2),
        .portr_data3                 (linebuf_left_portr_data3),
        .portr_data4                 (linebuf_left_portr_data4),
        .portr_data5                 (linebuf_left_portr_data5),
        .portr_data6                 (linebuf_left_portr_data6),
        .portr_data7                 (linebuf_left_portr_data7),
        // .portr_en                    (left_portr_en),

        .max_valid_range_in          (validrange_left_int),
        .min_valid_range_in          (left_constrained_min_range_reg)
    );

    corner_pixel_memory corner_pixel_memory_block
    (
        .clk                         (clk),
        .reset                       (reset),
        .enable                      (enable),

        .port_a_xt_div_4_in          (corner_pixel_mem_port_a_xt_div_4_reg),
        .port_a_yt_div_4_in          (corner_pixel_mem_port_a_yt_div_4_reg),
        .port_a_rdata_out            (corner_pixel_mem_port_a_rdata_int),
        .port_a_wdata_in             (corner_pixel_mem_port_a_wdata_int),
        .port_a_rw                   (corner_pixel_mem_port_a_rw_int),
        .port_a_wen                  (corner_pixel_mem_port_a_wen_int),

        .port_b_xt_div_4_in          (corner_pixel_mem_port_b_xt_div_4_reg),
        .port_b_yt_div_4_in          (corner_pixel_mem_port_b_yt_div_4_reg),
//        .port_b_rdata_out            (corner_pixel_mem_port_b_rdata_int),
        .port_b_wdata_in             (corner_pixel_mem_port_b_wdata_int),
        .port_b_rw                   (corner_pixel_mem_port_b_rw_int),
        .port_b_wen                  (corner_pixel_mem_port_b_wen_int)
    );

    intra_angular_read_addr intra_angular_top_read_addr_block
    (
        .clk                                    (clk),
        .reset                                  (reset),
        .enable                                 (enable),

        .abs_intrapredangle_in                  (abs_intrapredangle_reg),
        .sgn_intrapredangle_in                  (sgn_intrapredangle_reg),
        .inner_mainaxis_shifted_2_in            ({inner_x_reg[MAX_NTBS_SIZE - 1:2],2'b00}),
        .inner_auxaxis_in                       (inner_y_reg),
        .main_t_in                              (xt_reg),
        // .aux_t_in                               (yt_reg),
        // .valid_in                               (1'b1),
        .mode_in                                (top_pred_mode_reg),
        .dc_done_in                             (dc_init_done_int),
        .dc_read_addr_offset_in                 (dc_read_addr_offset_int),
        .mode_modified_for_ver_hor_out          (top_mode_modified_for_ver_hor_int),

        .filter_flag_in                         (filter_flag),
        .filter_done_in                         (filter_done_int),
        .filter_addr_offset_in                  (filter_offset_int),
        .bi_int_flag_in                         (bi_int_filter_flag && filter_flag),

        .max_valid_range_in                     (validrange_top_int),
        .max_valid_range_valid_in               (available_pixel_checker_valid_int),

        .read_from_aux_flag                     (top_read_from_aux_flag_int),
        .read_addr_out                          (top_read_addr_to_aux_flag_gen_int),
//        .valid_out                              (top_valid_int),

        .intrapredacc_lower5b                   (top_intrapredacc_lower5b_int),
        // .intrapredacc_upper7b                   (top_intrapredacc_upper7b_int),
        .extended_arr_index_4clks_delay_out     (top_extended_arr_index_4clks_delay_int),
        .extended_arr_index_out                 (top_extended_arr_index_int)
    );

    intra_angular_read_addr intra_angular_left_read_addr_block
    (
        .clk                                    (clk),
        .reset                                  (reset),
        .enable                                 (enable),

        .abs_intrapredangle_in                  (abs_intrapredangle_reg),
        .sgn_intrapredangle_in                  (sgn_intrapredangle_reg),
        .inner_mainaxis_shifted_2_in            ({inner_y_reg[MAX_NTBS_SIZE - 1:2],2'b00}),
        .inner_auxaxis_in                       (inner_x_reg),
        .main_t_in                              (yt_reg),
        // .aux_t_in                               (xt_reg),
        // .valid_in                               (1'b1),
        .mode_in                                (top_pred_mode_reg),
        .dc_done_in                             (dc_init_done_int),
        .dc_read_addr_offset_in                 (dc_read_addr_offset_int),
        .mode_modified_for_ver_hor_out          (left_mode_modified_for_ver_hor_int),

        .filter_flag_in                         (filter_flag),
        .filter_done_in                         (filter_done_int),
        .filter_addr_offset_in                  (filter_offset_int),
        .bi_int_flag_in                         (bi_int_filter_flag && filter_flag),

        .max_valid_range_in                     (validrange_left_int),
        .max_valid_range_valid_in               (available_pixel_checker_valid_int),

        .read_from_aux_flag                     (left_read_from_aux_flag_int),
        .read_addr_out                          (left_read_addr_to_aux_flag_gen_int),
//        .valid_out                              (left_valid_int),

        .intrapredacc_lower5b                   (left_intrapredacc_lower5b_int),
        // .intrapredacc_upper7b                   (left_intrapredacc_upper7b_int),
        .extended_arr_index_4clks_delay_out     (left_extended_arr_index_4clks_delay_int),
        .extended_arr_index_out                 (left_extended_arr_index_int)
    );

    intra_angular_prediction_aux_data_supplier intra_angular_prediction_top_aux_data_supplier_block
    (
        .clk                                (clk),
        .reset                              (reset),
        .enable                             (enable),

        //ntbs_in,
        .abs_intrapredangle_in              (abs_intrapredangle_reg),

        .read_from_aux_flags_in             (top_read_from_aux_flag_int),
        .valid_in                           (1'b1),
        .mode_in                            (top_mode_modified_for_ver_hor_int),
        // .low_6bits_of_read_addr_0_in        (top_read_addr_to_aux_flag_gen_int[5:0]),
        .extended_arr_index_in              (top_extended_arr_index_int),
        .aux_axis_tb_corner_addr_in         (yt_reg),
        .inner_aux_in                       (inner_y_reg),
        .inner_mainaxis_shifted_2_in        ({inner_x_reg[MAX_NTBS_SIZE - 1:2],2'b00}),

        // .max_valid_aux_range_valid_in       (available_pixel_checker_valid_int),
        .max_valid_aux_range_in             (validrange_left_int),

        .filter_done_in                     (filter_done_int),
        .filter_flag_in                     (filter_flag),

        .aux_read_addr_out                  (top_aux_read_addr_int),
        .aux_read_en_out                    (),
        .aux_read_data_in                   (left_portr_data0),

        .corner_pixel_data_in               (corner_pixel_reg),
        .corner_pixel_filtered_data_in      (corner_pixel_filtered_int),

        .aux_data0_out                      (top_aux_data_0_int),
        .aux_data1_out                      (top_aux_data_1_int),
        .aux_data2_out                      (top_aux_data_2_int),
        .aux_data3_out                      (top_aux_data_3_int),
        .aux_data4_out                      (top_aux_data_4_int)
    );

    intra_angular_prediction_aux_data_supplier intra_angular_prediction_left_aux_data_supplier_block
    (
        .clk                                (clk),
        .reset                              (reset),
        .enable                             (enable),

        //ntbs_in,
        .abs_intrapredangle_in              (abs_intrapredangle_reg),

        .read_from_aux_flags_in             (left_read_from_aux_flag_int),
        .valid_in                           (1'b1),
        .mode_in                            (left_mode_modified_for_ver_hor_int),
        // .low_6bits_of_read_addr_0_in        (left_read_addr_to_aux_flag_gen_int[5:0]),
        .extended_arr_index_in              (left_extended_arr_index_int),
        .aux_axis_tb_corner_addr_in         (xt_reg),
        .inner_aux_in                       (inner_x_reg),
        .inner_mainaxis_shifted_2_in        ({inner_y_reg[MAX_NTBS_SIZE - 1:2],2'b00}),

        // .max_valid_aux_range_valid_in       (available_pixel_checker_valid_int),
        .max_valid_aux_range_in             (validrange_top_int),

        .filter_done_in                     (filter_done_int),
        .filter_flag_in                     (filter_flag),

        .aux_read_addr_out                  (left_aux_read_addr_int),
        .aux_read_en_out                    (),
        .aux_read_data_in                   (top_portr_data0),

        .corner_pixel_data_in               (corner_pixel_reg),
        .corner_pixel_filtered_data_in      (corner_pixel_filtered_int),

        .aux_data0_out                      (left_aux_data_0_int),
        .aux_data1_out                      (left_aux_data_1_int),
        .aux_data2_out                      (left_aux_data_2_int),
        .aux_data3_out                      (left_aux_data_3_int),
        .aux_data4_out                      (left_aux_data_4_int)
    );

    intra_angular_read_from_aux_flag_gen intra_angular_top_read_from_aux_flag_gen
    (
        .clk                                (clk),
        .reset                              (reset),
        .enable                             (enable),

        .read_from_aux_flag_in              (top_read_from_aux_flag_int),
        .read_addr_in                       (top_read_addr_to_aux_flag_gen_int),
        .extended_arr_index_in              (top_extended_arr_index_4clks_delay_int),
        .mode_in                            (top_mode_modified_for_ver_hor_int),

        .read_from_aux_flags_out            (top_aux_flags_int),
        .read_addr_out                      (top_read_addr_from_aux_flag_gen_int)
    );

    intra_angular_read_from_aux_flag_gen intra_angular_left_read_from_aux_flag_gen
    (
        .clk                                (clk),
        .reset                              (reset),
        .enable                             (enable),

        .read_from_aux_flag_in              (left_read_from_aux_flag_int),
        .read_addr_in                       (left_read_addr_to_aux_flag_gen_int),
        .extended_arr_index_in              (left_extended_arr_index_4clks_delay_int),
        .mode_in                            (left_mode_modified_for_ver_hor_int),

        .read_from_aux_flags_out            (left_aux_flags_int),
        .read_addr_out                      (left_read_addr_from_aux_flag_gen_int)
    );

//    generate
//
//        if( CIDX == 0 )  begin

            intra_neighbours_filter intra_neighbours_filter_block
            (
                .clk                                (clk),
                .reset                              (reset),
                .enable                             (enable),

                .top_zeroth_in                      (top_zeroth_reg),
                .left_zeroth_in                     (left_zeroth_reg),
                .top_2nt_in                         (top_2nt_reg),
                .left_2nt_in                        (left_2nt_reg),
                .corner_pixel_in                    (corner_pixel_reg),
                .bi_int_flag_in                     (bi_int_filter_flag),
                .ntbs_in                            (ntbs_reg),
                .top_all_invalid_in                 (all_invalid_top_int),
                .left_all_invalid_in                (all_invalid_left_int),

                .top_data_0_in                      (linebuf_top_portr_data0),
                .top_data_1_in                      (linebuf_top_portr_data1),
                .top_data_2_in                      (linebuf_top_portr_data2),
                .top_data_3_in                      (linebuf_top_portr_data3),
                .top_data_4_in                      (linebuf_top_portr_data4),
                .top_data_5_in                      (linebuf_top_portr_data5),
                .top_data_6_in                      (linebuf_top_portr_data6),
                .top_data_7_in                      (linebuf_top_portr_data7),

                .left_data_0_in                     (linebuf_left_portr_data0),
                .left_data_1_in                     (linebuf_left_portr_data1),
                .left_data_2_in                     (linebuf_left_portr_data2),
                .left_data_3_in                     (linebuf_left_portr_data3),
                .left_data_4_in                     (linebuf_left_portr_data4),
                .left_data_5_in                     (linebuf_left_portr_data5),
                .left_data_6_in                     (linebuf_left_portr_data6),
                .left_data_7_in                     (linebuf_left_portr_data7),

                .filter_start_in                    (filter_start),
                .filter_done_out                    (filter_done_int),
                .filter_offset_out                  (filter_offset_int),

                .corner_pixel_filtered_out          (corner_pixel_filtered_int),
                .top_nt_plus1_filtered_out          (top_nt_plus1_filtered_int),
                .left_nt_plus1_filtered_out         (left_nt_plus1_filtered_int),
                .top_zeroth_filtered_out            (top_zeroth_filtered_int),
                .left_zeroth_filtered_out           (left_zeroth_filtered_int),

                .top_portr_addr                     (top_portr_addr[MAX_NTBS_SIZE - 1:0]),
                .top_portr_data0                    (filter_top_portr_data0),
                .top_portr_data1                    (filter_top_portr_data1),
                .top_portr_data2                    (filter_top_portr_data2),
                .top_portr_data3                    (filter_top_portr_data3),
                .top_portr_data4                    (filter_top_portr_data4),
                .top_portr_data5                    (filter_top_portr_data5),
                .top_portr_data6                    (filter_top_portr_data6),
                .top_portr_data7                    (filter_top_portr_data7),
                // .top_portr_en                       (top_portr_en),

                .left_portr_addr                    (left_portr_addr[MAX_NTBS_SIZE - 1:0]),
                .left_portr_data0                   (filter_left_portr_data0),
                .left_portr_data1                   (filter_left_portr_data1),
                .left_portr_data2                   (filter_left_portr_data2),
                .left_portr_data3                   (filter_left_portr_data3),
                .left_portr_data4                   (filter_left_portr_data4),
                .left_portr_data5                   (filter_left_portr_data5),
                .left_portr_data6                   (filter_left_portr_data6),
                .left_portr_data7                   (filter_left_portr_data7)
                // .left_portr_en                      (left_portr_en)

            );

//        end
//
//    endgenerate

    always @(*) begin
        if(filter_flag == 1'b1) begin
            left_portr_data0 = filter_left_portr_data0;
            left_portr_data1 = filter_left_portr_data1;
            left_portr_data2 = filter_left_portr_data2;
            left_portr_data3 = filter_left_portr_data3;
            left_portr_data4 = filter_left_portr_data4;
            left_portr_data5 = filter_left_portr_data5;
            left_portr_data6 = filter_left_portr_data6;
            left_portr_data7 = filter_left_portr_data7;

            top_portr_data0 = filter_top_portr_data0;
            top_portr_data1 = filter_top_portr_data1;
            top_portr_data2 = filter_top_portr_data2;
            top_portr_data3 = filter_top_portr_data3;
            top_portr_data4 = filter_top_portr_data4;
            top_portr_data5 = filter_top_portr_data5;
            top_portr_data6 = filter_top_portr_data6;
            top_portr_data7 = filter_top_portr_data7;

            top_nt_plus1_common_int = top_nt_plus1_filtered_int;
            left_nt_plus1_common_int = left_nt_plus1_filtered_int;
            top_zeroth_common_int = top_zeroth_filtered_int;
            left_zeroth_common_int = left_zeroth_filtered_int;

        end
        else begin
            left_portr_data0 = linebuf_left_portr_data0;
            left_portr_data1 = linebuf_left_portr_data1;
            left_portr_data2 = linebuf_left_portr_data2;
            left_portr_data3 = linebuf_left_portr_data3;
            left_portr_data4 = linebuf_left_portr_data4;
            left_portr_data5 = linebuf_left_portr_data5;
            left_portr_data6 = linebuf_left_portr_data6;
            left_portr_data7 = linebuf_left_portr_data7;

            top_portr_data0  = linebuf_top_portr_data0;
            top_portr_data1  = linebuf_top_portr_data1;
            top_portr_data2  = linebuf_top_portr_data2;
            top_portr_data3  = linebuf_top_portr_data3;
            top_portr_data4  = linebuf_top_portr_data4;
            top_portr_data5  = linebuf_top_portr_data5;
            top_portr_data6  = linebuf_top_portr_data6;
            top_portr_data7  = linebuf_top_portr_data7;

            top_nt_plus1_common_int = top_nt_plus1_reg;
            left_nt_plus1_common_int = left_nt_plus1_reg;
            top_zeroth_common_int = top_zeroth_reg;
            left_zeroth_common_int = left_zeroth_reg;
        end
    end

    intra_angular_predsample_gen_input_muxer
    #(
        .CIDX(CIDX)
    )
    intra_angular_predsample_gen_input_muxer_block
    (
        .clk                                (clk),
        .reset                              (reset),
        .enable                             (enable),

        .top_all_invalid_in                 (all_invalid_top_int&(~filter_flag)),
        .left_all_invalid_in                (all_invalid_left_int&(~filter_flag)),
        // .filter_flag_in                     (1'b0),
        .filter_done_in                     (1'b1),                     //TODO : integrate the filter
        .read_from_top_aux_flags_in         (top_aux_flags_int),
        .read_from_left_aux_flags_in        (left_aux_flags_int),

        .top_line_buffer_portr_0_in         (top_portr_data0),
        .left_line_buffer_portr_0_in        (left_portr_data0),
        .top_aux_data_0_in                  (top_aux_data_0_int),
        .left_aux_data_0_in                 (left_aux_data_0_int),

        .top_line_buffer_portr_1_in         (top_portr_data1),
        .left_line_buffer_portr_1_in        (left_portr_data1),
        .top_aux_data_1_in                  (top_aux_data_1_int),
        .left_aux_data_1_in                 (left_aux_data_1_int),

        .top_line_buffer_portr_2_in         (top_portr_data2),
        .left_line_buffer_portr_2_in        (left_portr_data2),
        .top_aux_data_2_in                  (top_aux_data_2_int),
        .left_aux_data_2_in                 (left_aux_data_2_int),

        .top_line_buffer_portr_3_in         (top_portr_data3),
        .left_line_buffer_portr_3_in        (left_portr_data3),
        .top_aux_data_3_in                  (top_aux_data_3_int),
        .left_aux_data_3_in                 (left_aux_data_3_int),

        .top_line_buffer_portr_4_in         (top_portr_data4),
        .left_line_buffer_portr_4_in        (left_portr_data4),
        .top_aux_data_4_in                  (top_aux_data_4_int),
        .left_aux_data_4_in                 (left_aux_data_4_int),

        .corner_pixel_data_in               (corner_pixel_reg),

//        .dc_cidx_in                         (CIDX),   //TODO : to be decided whether implementing three modules or re-use
        // .dc_data_0_in                       (top_portr_data0),
        // .dc_data_1_in                       (top_portr_data1),
        // .dc_data_2_in                       (top_portr_data2),
        // .dc_data_3_in                       (top_portr_data3),
        // .dc_data_4_in                       (top_portr_data4),
        .dc_data_5_in                       (top_portr_data5),
        .dc_data_6_in                       (top_portr_data6),
        .dc_data_7_in                       (top_portr_data7),

        .dc_aux_0_in                        (left_portr_data0),
        .dc_aux_1_in                        (left_portr_data1),
        .dc_aux_2_in                        (left_portr_data2),
        .dc_aux_3_in                        (left_portr_data3),
        .dc_aux_4_in                        (left_portr_data4),
        .dc_aux_5_in                        (left_portr_data5),
        .dc_aux_6_in                        (left_portr_data6),
        .dc_aux_7_in                        (left_portr_data7),
        .dc_read_addr_offset_out            (dc_read_addr_offset_int),
        .dc_init_done_out                   (dc_init_done_int),

        .intrapredacc_lower5b_in            (intrapredacc_lower5b_int),
        .sgn_intrapredangle_in              (sgn_intrapredangle_reg),
        // .xt_in                              (xt_reg),
        // .yt_in                              (yt_reg),
        .inner_x_in                         (inner_x_reg),
        .inner_y_in                         (inner_y_reg),
        .top_zeroth_in                      (top_zeroth_common_int),
        .left_zeroth_in                     (left_zeroth_common_int),
        .planar_top_ntbs_in                 (top_nt_plus1_common_int),
        .planar_left_ntbs_in                (left_nt_plus1_common_int),
        .ntbs_in                            (ntbs_reg),
        .mode_in                            (top_pred_mode_reg),
        .top_mode_modified_for_ver_hor_in   (top_mode_modified_for_ver_hor_int),
        .left_mode_modified_for_ver_hor_in  (left_mode_modified_for_ver_hor_int),
//        .cidx_in                            (2'd0), //TODO : to be decided whether implementing three modules or re-use : this implemented block wise

        .one_before_last_4by4_out           (one_before_last_4by4_int),
        .predsample_4by4_out                (predsample_4by4_out),
        .predsample_4by4_valid_out          (predsample_4by4_valid_out),

        .valid_in                           (input_muxer_valid_reg)   //TODO :for readability add same signal
        // .valid_out                          ()

    );


endmodule
