`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:58:44 11/13/2013 
// Design Name: 
// Module Name:    corner_pixel_memory 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module corner_pixel_memory
    (
        clk,
        reset,
        enable,
        
        port_a_xt_div_4_in,
        port_a_yt_div_4_in,
        port_a_rdata_out,
        port_a_wdata_in,
        port_a_rw,
        port_a_wen,
        
        port_b_xt_div_4_in,
        port_b_yt_div_4_in,
        // port_b_rdata_out,
        port_b_wdata_in,
        port_b_rw,
        port_b_wen
    );
 
    `include "../sim/pred_def.v" 
//----------------------------------------------------
//LOCALPARAMS
//----------------------------------------------------
    localparam                      INTRA_PRED_ANGLE_WIDTH  = 6;
    // localparam                      MAX_NTBS_SIZE           = 6;
    localparam                      NUM_OF_DATA_REGS        = 5;
    // localparam                      PIXEL_WIDTH             = 8;
    // localparam                      LOG2_FRAME_SIZE         = 13;
    localparam                      MEM_SIZE               = 1 << MAX_NTBS_SIZE;
    
    localparam                      READ                    = 0;
    localparam                      WRITE                   = 1;
    
//----------------------------------------------------
// I/O Signals
//----------------------------------------------------

    input                                   clk;
    input                                   reset;
    input                                   enable;
    
    input       [MAX_NTBS_SIZE - 2 - 1:0]   port_a_xt_div_4_in;
    input       [MAX_NTBS_SIZE - 2 - 1:0]   port_a_yt_div_4_in;
    output reg  [PIXEL_WIDTH - 1:0]         port_a_rdata_out;
    input       [PIXEL_WIDTH - 1:0]         port_a_wdata_in;
    input                                   port_a_rw;
    input                                   port_a_wen;
    
    input       [MAX_NTBS_SIZE - 2 - 1:0]   port_b_xt_div_4_in;
    input       [MAX_NTBS_SIZE - 2 - 1:0]   port_b_yt_div_4_in;
    // output reg  [PIXEL_WIDTH - 1:0]         port_b_rdata_out;
    input       [PIXEL_WIDTH - 1:0]         port_b_wdata_in;
    input                                   port_b_rw;
    input                                   port_b_wen;
    
//----------------------------------------------------
//  Internal Regs and Wires 
//----------------------------------------------------
    
//    reg     [PIXEL_WIDTH - 1:0]             mem[MEM_SIZE - 1:0][MEM_SIZE - 1:0];
    reg        [PIXEL_WIDTH - 1:0]          mem[MEM_SIZE*MEM_SIZE - 1:0];
         
//----------------------------------------------------
//  Implementation
//----------------------------------------------------    
   
    
    always @(posedge clk) begin : port_a
        if(enable) begin
            port_a_rdata_out <= mem[port_a_xt_div_4_in + port_a_yt_div_4_in*(1 << (MAX_NTBS_SIZE - 2))];

            if(port_a_wen) begin
                mem[port_a_xt_div_4_in + port_a_yt_div_4_in*(1 << (MAX_NTBS_SIZE - 2))] <= port_a_wdata_in;        
            end
        end
    end
    
    always @(posedge clk) begin : port_b
        if(enable) begin
            // port_b_rdata_out <= mem[port_b_xt_div_4_in + port_b_yt_div_4_in*(1 << (MAX_NTBS_SIZE - 2))];
//        end
//        else begin
            if(port_b_wen) begin
                mem[port_b_xt_div_4_in + port_b_yt_div_4_in*(1 << (MAX_NTBS_SIZE - 2))] <= port_b_wdata_in;        
            end
        end
    end


endmodule
