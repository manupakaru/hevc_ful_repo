`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    08:42:11 11/11/2013 
// Design Name: 
// Module Name:    predsample_gen_angular 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module predsample_gen
    (
        clk,
        reset,
        enable,
        
        intrapredangle_low5b_in,
        sgn_intrapredangle_in,
        mode_in,
        angular_hor_ver_mode_in,
        angular_en_in,
        planar_en_in,
        dc_en_in,
        corner_pixel_in,
        top_zeroth_in,
        left_zeroth_in,
        
        dc_read_addr_offset_out,
        dc_init_done_out,
        
        data_0_in,
        data_1_in,
        data_2_in,
        data_3_in,
        data_4_in,
        data_5_in,
        data_6_in,
        data_7_in,
        
        dc_aux_0_in,
        dc_aux_1_in,
        dc_aux_2_in,
        dc_aux_3_in,
        dc_aux_4_in,
        dc_aux_5_in,
        dc_aux_6_in,
        dc_aux_7_in,
        
        planar_top_ntbs_in,
        planar_left_ntbs_in,
        inner_x_in,
        inner_y_in,
        ntbs_in,
        
        one_before_last_4by4_out,
        predsample_4by4_out,
        predsample_4by4_valid_out   
    );

    `include "../sim/pred_def.v"    
//----------------------------------------------------
// PARAMETER
//----------------------------------------------------
    parameter                       CIDX = 0;
    
//----------------------------------------------------
// LOCALPARAMS
//----------------------------------------------------
    localparam                      INTRA_PRED_ANGLE_WIDTH      = 6;
    // localparam                      MAX_NTBS_SIZE               = 6;
    localparam                      NUM_OF_DATA_REGS            = 5;
    // localparam                      PIXEL_WIDTH                 = 8;
    // localparam                      LOG2_FRAME_SIZE             = 13;
    localparam                      ANGULAR_MODE_WIDTH          = 1;
    // localparam                      OUTPUT_BLOCK_SIZE           = 4;
    localparam                      INTRAPRED_ANGLE_LOW5B_WIDTH = 5;
    localparam                      INNER_X_LOW2B_WIDTH         = 2;
    localparam                      INNER_Y_LOW2B_WIDTH         = 2;
    localparam                      DC_MAXWIDTH                 = 8 + 9;
    localparam                      DC_COUNTER_WIDTH            = 4;
    localparam                      DC_CIDX_WIDTH               = 2;

    localparam                      MODE_VERTICAL               = 0;
    localparam                      MODE_HORIZONTAL             = 1;
    
    localparam                      STATE_1_ROW_COLUMN          = 0;
    localparam                      STATE_2_ROW_COLUMN          = 1;
    localparam                      STATE_3_ROW_COLUMN          = 2;
    localparam                      STATE_4_ROW_COLUMN          = 3;
    localparam                      STATE_DC_INIT               = 4;
    localparam                      STATE_DC_WAIT1              = 5;
    localparam                      STATE_DC_WAIT2              = 6;
    localparam                      STATE_DC_WAIT3              = 7;
    

//----------------------------------------------------
// I/O
//----------------------------------------------------
    
    input                                                               clk;
    input                                                               reset;
    input                                                               enable;
                                    
    input   [INTRAPRED_ANGLE_LOW5B_WIDTH - 1:0]                         intrapredangle_low5b_in;
    input                                                               sgn_intrapredangle_in;
    input   [ANGULAR_MODE_WIDTH - 1:0]                                  mode_in;
    input                                                               angular_hor_ver_mode_in;  
    input                                                               angular_en_in;
    input                                                               planar_en_in;
    input                                                               dc_en_in;
    input   [PIXEL_WIDTH - 1:0]                                         corner_pixel_in;
    input   [PIXEL_WIDTH - 1:0]                                         top_zeroth_in;
    input   [PIXEL_WIDTH - 1:0]                                         left_zeroth_in;
                                    
    input   [PIXEL_WIDTH - 1:0]                                         dc_aux_0_in;
    input   [PIXEL_WIDTH - 1:0]                                         dc_aux_1_in;
    input   [PIXEL_WIDTH - 1:0]                                         dc_aux_2_in;
    input   [PIXEL_WIDTH - 1:0]                                         dc_aux_3_in;
    input   [PIXEL_WIDTH - 1:0]                                         dc_aux_4_in;
    input   [PIXEL_WIDTH - 1:0]                                         dc_aux_5_in;
    input   [PIXEL_WIDTH - 1:0]                                         dc_aux_6_in;
    input   [PIXEL_WIDTH - 1:0]                                         dc_aux_7_in;
    output  reg [MAX_NTBS_SIZE - 1:0]                                   dc_read_addr_offset_out;
    output  reg                                                         dc_init_done_out;
    
    input   [PIXEL_WIDTH - 1:0]                                         data_0_in;
    input   [PIXEL_WIDTH - 1:0]                                         data_1_in;
    input   [PIXEL_WIDTH - 1:0]                                         data_2_in;
    input   [PIXEL_WIDTH - 1:0]                                         data_3_in;
    input   [PIXEL_WIDTH - 1:0]                                         data_4_in;
    input   [PIXEL_WIDTH - 1:0]                                         data_5_in;
    input   [PIXEL_WIDTH - 1:0]                                         data_6_in;
    input   [PIXEL_WIDTH - 1:0]                                         data_7_in;
    
    input   [PIXEL_WIDTH - 1:0]                                         planar_left_ntbs_in;
    input   [PIXEL_WIDTH - 1:0]                                         planar_top_ntbs_in;
    input   [MAX_NTBS_SIZE - 1:0]                                       inner_x_in;
    input   [MAX_NTBS_SIZE - 1:0]                                       inner_y_in;
    input   [MAX_NTBS_SIZE - 1:0]                                       ntbs_in;
    
    
    output  reg                                                         one_before_last_4by4_out;
    output  [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]     predsample_4by4_out;
    output  reg                                                         predsample_4by4_valid_out;
    
//----------------------------------------------------
//  Internal Regs and Wires 
//----------------------------------------------------

    integer                                                             state;
    reg     [MAX_NTBS_SIZE - 1:0]                                       counter;
    
    reg     [4:0]                                                       angular_en_reg;
    reg     [4:0]                                                       planar_en_reg;
    reg                                                                 dc_en_reg;

    reg     [MAX_NTBS_SIZE - 1:0]                                       inner_x_reg[5:0];
    reg     [1:0]                                                       inner_x_reg0_is_zero;
    reg     [MAX_NTBS_SIZE - 1:0]                                       inner_y_reg[5:0];
    reg     [1:0]                                                       inner_y_reg0_is_zero;
    
    reg     [INTRAPRED_ANGLE_LOW5B_WIDTH - 1:0]                         intrapredangle_low5b_reg;
    reg     [MAX_NTBS_SIZE - 1:0]                                       ntbs_reg;
    reg     [MAX_NTBS_SIZE - 1:0]                                       max_4by4_blocks_minus1;
    reg     [PIXEL_WIDTH - 1:0]                                         planar_left_ntbs_reg;
    reg     [PIXEL_WIDTH - 1:0]                                         planar_top_ntbs_reg;
    reg     [PIXEL_WIDTH - 1:0]                                         corner_pixel_reg;
    reg     [PIXEL_WIDTH - 1:0]                                         top_zeroth_reg;
    reg     [PIXEL_WIDTH - 1:0]                                         left_zeroth_reg;
    
    reg     [PIXEL_WIDTH - 1:0]                                         data_0_reg;
    reg     [PIXEL_WIDTH - 1:0]                                         data_1_reg;
    reg     [PIXEL_WIDTH - 1:0]                                         data_2_reg;
    reg     [PIXEL_WIDTH - 1:0]                                         data_3_reg;
    reg     [PIXEL_WIDTH - 1:0]                                         data_4_reg;

    reg     [PIXEL_WIDTH - 1:0]                                         data_1_reg_d;
    reg     [PIXEL_WIDTH - 1:0]                                         data_2_reg_d;
    reg     [PIXEL_WIDTH - 1:0]                                         data_3_reg_d;
    reg     [PIXEL_WIDTH - 1:0]                                         data_4_reg_d;
    
    reg     [INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]                     planar_data_0_data_pre_mult;
    reg     [INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]                     planar_data_1_data_pre_mult;
    reg     [INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]                     planar_data_2_data_pre_mult;
    reg     [INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]                     planar_data_3_data_pre_mult;
    reg     [INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]                     planar_data_0_ntbs_pre_mult;
    reg     [INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]                     planar_data_1_ntbs_pre_mult;
    reg     [INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]                     planar_data_2_ntbs_pre_mult;
    reg     [INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]                     planar_data_3_ntbs_pre_mult;
    reg     [INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]                     planar_data_data_post_mult;
    reg     [INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]                     planar_data_ntbs_post_mult;

    
    reg     [PIXEL_WIDTH - 1:0]                                         angular_pred_data_0;
    reg     [PIXEL_WIDTH - 1:0]                                         angular_pred_data_1;
    reg     [PIXEL_WIDTH - 1:0]                                         angular_pred_data_2;
    reg     [PIXEL_WIDTH - 1:0]                                         angular_pred_data_3;
    
    reg     [MAX_NTBS_SIZE - 1:0]                                       log2ntbs;
    
    reg     [PIXEL_WIDTH - 1:0]                                         predsamples    [OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];
    
    reg     [DC_COUNTER_WIDTH - 1:0]                                    dc_counter;
    reg     [DC_MAXWIDTH - 1:0]                                         dc_top_level1_1;
    reg     [DC_MAXWIDTH - 1:0]                                         dc_top_level1_2;
    reg     [DC_MAXWIDTH - 1:0]                                         dc_top_level1_3;
    reg     [DC_MAXWIDTH - 1:0]                                         dc_top_level1_4;
    reg     [DC_MAXWIDTH - 1:0]                                         dc_top_level2_1;
    reg     [DC_MAXWIDTH - 1:0]                                         dc_top_level2_2;
    reg     [DC_MAXWIDTH - 1:0]                                         dc_top_level3_1;
    reg     [DC_MAXWIDTH - 1:0]                                         dc_left_level1_1;
    reg     [DC_MAXWIDTH - 1:0]                                         dc_left_level1_2;
    reg     [DC_MAXWIDTH - 1:0]                                         dc_left_level1_3;
    reg     [DC_MAXWIDTH - 1:0]                                         dc_left_level1_4;
    reg     [DC_MAXWIDTH - 1:0]                                         dc_left_level2_1;
    reg     [DC_MAXWIDTH - 1:0]                                         dc_left_level2_2;
    reg     [DC_MAXWIDTH - 1:0]                                         dc_left_level3_1;
    reg     [DC_MAXWIDTH - 1:0]                                         dc_top_left_merge;
    reg     [DC_MAXWIDTH - 1:0]                                         dc_acc;
    
 
//angular

    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       angular_data_0_interpolation_1tap;
    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       angular_data_0_interpolation_2tap;
    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH    - 1:INTRAPRED_ANGLE_LOW5B_WIDTH]       angular_next_pred_data_0;
    wire    [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       angular_next_pred_data_0_wire;
    
    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       angular_data_1_interpolation_1tap;
    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       angular_data_1_interpolation_2tap;
    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH    - 1:INTRAPRED_ANGLE_LOW5B_WIDTH]       angular_next_pred_data_1;
    wire    [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       angular_next_pred_data_1_wire;
    
    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       angular_data_2_interpolation_1tap;
    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       angular_data_2_interpolation_2tap;
    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH    - 1:INTRAPRED_ANGLE_LOW5B_WIDTH]       angular_next_pred_data_2;
    wire    [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       angular_next_pred_data_2_wire;
    
    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       angular_data_3_interpolation_1tap;
    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       angular_data_3_interpolation_2tap;
    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH    - 1:INTRAPRED_ANGLE_LOW5B_WIDTH]       angular_next_pred_data_3;
    wire    [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       angular_next_pred_data_3_wire;
    
    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       planar_data_0_from_leftandtop;//[2:0];
    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       planar_data_0_from_corners;//[2:0];   
    reg     [PIXEL_WIDTH - 1:0]                                         next_planar_data_0;
    wire    [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       next_planar_data_0_wire;

    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       planar_data_1_from_leftandtop;//[2:0];
    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       planar_data_1_from_corners;//[2:0];   
    reg     [PIXEL_WIDTH - 1:0]                                         next_planar_data_1;
    wire    [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       next_planar_data_1_wire;

    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       planar_data_2_from_leftandtop;//[2:0];
    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       planar_data_2_from_corners;//[2:0];   
    reg     [PIXEL_WIDTH - 1:0]                                         next_planar_data_2;
    wire    [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       next_planar_data_2_wire;

    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       planar_data_3_from_leftandtop;//[2:0];
    reg     [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       planar_data_3_from_corners;//[2:0];   
    reg     [PIXEL_WIDTH - 1:0]                                         next_planar_data_3;
    wire    [PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 1:0]       next_planar_data_3_wire;

    reg     [PIXEL_WIDTH + 2 - 1:1]                                     ver_hor_sub_data;
    wire    [PIXEL_WIDTH + 2 - 1:0]                                     ver_hor_sub_data_wire;

    reg     [PIXEL_WIDTH + 2 - 1:0]                                     ver_hor_sub_data_added_with_zeroth;                                                                       
    
    
//----------------------------------------------------
//  Implementation 
//---------------------------------------------------- 

//    assign angular_data_0_interpolation_1tap = data_0_in*((1'b1 << INTRAPRED_ANGLE_LOW5B_WIDTH) - intrapredangle_low5b_in);
//    assign angular_data_0_interpolation_2tap = data_1_in*(intrapredangle_low5b_in);
//    assign angular_next_pred_data_0          = (angular_data_0_interpolation_1tap + angular_data_0_interpolation_2tap) + {{(PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 5){1'b0}},5'd16};
    
//    assign angular_data_1_interpolation_1tap = data_1_in*((1'b1 << INTRAPRED_ANGLE_LOW5B_WIDTH) - intrapredangle_low5b_in);
//    assign angular_data_1_interpolation_2tap = data_2_in*(intrapredangle_low5b_in);
//    assign angular_next_pred_data_1          = (angular_data_1_interpolation_1tap + angular_data_1_interpolation_2tap) + {{(PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 5){1'b0}},5'd16};
    
//    assign angular_data_2_interpolation_1tap = data_2_in*((1'b1 << INTRAPRED_ANGLE_LOW5B_WIDTH) - intrapredangle_low5b_in);
//    assign angular_data_2_interpolation_2tap = data_3_in*(intrapredangle_low5b_in);
//    assign angular_next_pred_data_2          = (angular_data_2_interpolation_1tap + angular_data_2_interpolation_2tap) + {{(PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 5){1'b0}},5'd16};
    
//    assign angular_data_3_interpolation_1tap = data_3_in*((1'b1 << INTRAPRED_ANGLE_LOW5B_WIDTH) - intrapredangle_low5b_in);
//    assign angular_data_3_interpolation_2tap = data_4_in*(intrapredangle_low5b_in);
//    assign angular_next_pred_data_3          = (angular_data_3_interpolation_1tap + angular_data_3_interpolation_2tap) + {{(PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 5){1'b0}},5'd16};
    
    //For Planar Prediction
    
    always @(posedge clk) begin
        case(ntbs_in)
            {{(MAX_NTBS_SIZE - 6){1'b0}},6'd32} : log2ntbs <= 5;
            {{(MAX_NTBS_SIZE - 6){1'b0}},6'd16} : log2ntbs <= 4;
            {{(MAX_NTBS_SIZE - 6){1'b0}},6'd8}  : log2ntbs <= 3;
            {{(MAX_NTBS_SIZE - 6){1'b0}},6'd4 } : log2ntbs <= 2;
            default                             : log2ntbs <= 0;
        endcase
    end

    always @(posedge clk) begin
        if (reset) begin
            
        end
        else begin

            inner_x_reg[0] <= inner_x_reg[1];
            inner_x_reg[1] <= inner_x_reg[2];
            inner_x_reg[2] <= inner_x_reg[3];
            inner_x_reg[3] <= inner_x_reg[4];
            inner_x_reg[4] <= inner_x_reg[5];
            inner_x_reg[5] <= inner_x_in;

            if(inner_x_reg[0] == {MAX_NTBS_SIZE{1'b0}}) begin
                inner_x_reg0_is_zero[1] <= 1'b1;
            end
            else begin
                inner_x_reg0_is_zero[1] <= 1'b0;
            end

            inner_x_reg0_is_zero[0] <= inner_x_reg0_is_zero[1];


            inner_y_reg[0] <= inner_y_reg[1];
            inner_y_reg[1] <= inner_y_reg[2];
            inner_y_reg[2] <= inner_y_reg[3];
            inner_y_reg[3] <= inner_y_reg[4];
            inner_y_reg[4] <= inner_y_reg[5];
            inner_y_reg[5] <= inner_y_in;

            if(inner_y_reg[0] == {MAX_NTBS_SIZE{1'b0}}) begin
                inner_y_reg0_is_zero[1] <= 1'b1;
            end
            else begin
                inner_y_reg0_is_zero[1] <= 1'b0;
            end

            inner_y_reg0_is_zero[0] <= inner_y_reg0_is_zero[1];
        end
    end

    always @(posedge clk) begin
        if (reset) begin
            // reset
            
        end
        else if (enable) begin
            intrapredangle_low5b_reg <= intrapredangle_low5b_in;
        end
    end

    assign ver_hor_sub_data_wire = data_0_in - corner_pixel_reg;
    
    always @(posedge clk) begin
        if(enable) begin
            data_0_reg  <= data_0_in;
            data_1_reg  <= data_1_in;
            data_2_reg  <= data_2_in;
            data_3_reg  <= data_3_in;
            data_4_reg  <= data_4_in;

            ver_hor_sub_data <= ver_hor_sub_data_wire[PIXEL_WIDTH + 2 - 1:1];

            corner_pixel_reg    <= corner_pixel_in;
            top_zeroth_reg      <= top_zeroth_in;
            left_zeroth_reg     <= left_zeroth_in;
            
            planar_data_0_data_pre_mult <= ((ntbs_in - 1'b1) - inner_x_reg[0]);
            planar_data_1_data_pre_mult <= ((ntbs_in - 1'b1) - (inner_x_reg[0] + 1'b1));
            planar_data_2_data_pre_mult <= ((ntbs_in - 1'b1) - (inner_x_reg[0] + 2'd2));
            planar_data_3_data_pre_mult <= ((ntbs_in - 1'b1) - (inner_x_reg[0] + 2'd3));
            
            planar_data_0_ntbs_pre_mult <= (inner_x_reg[0] + 1'b1);
            planar_data_1_ntbs_pre_mult <= (inner_x_reg[0] + 2'd2);
            planar_data_2_ntbs_pre_mult <= (inner_x_reg[0] + 2'd3);
            planar_data_3_ntbs_pre_mult <= (inner_x_reg[0] + 3'd4);
             
            planar_data_data_post_mult  <= ((ntbs_in - 1'b1) - inner_y_reg[0]);
            planar_data_ntbs_post_mult  <= (inner_y_reg[0] + 1'b1);
            
            if(sgn_intrapredangle_in == 1'b0) begin
                angular_data_0_interpolation_1tap <= data_0_in*((1'b1 << INTRAPRED_ANGLE_LOW5B_WIDTH) - intrapredangle_low5b_reg);
                angular_data_0_interpolation_2tap <= data_1_in*(intrapredangle_low5b_reg);
                
                angular_data_1_interpolation_1tap <= data_1_in*((1'b1 << INTRAPRED_ANGLE_LOW5B_WIDTH) - intrapredangle_low5b_reg);
                angular_data_1_interpolation_2tap <= data_2_in*(intrapredangle_low5b_reg);
                
                angular_data_2_interpolation_1tap <= data_2_in*((1'b1 << INTRAPRED_ANGLE_LOW5B_WIDTH) - intrapredangle_low5b_reg);
                angular_data_2_interpolation_2tap <= data_3_in*(intrapredangle_low5b_reg);
                
                angular_data_3_interpolation_1tap <= data_3_in*((1'b1 << INTRAPRED_ANGLE_LOW5B_WIDTH) - intrapredangle_low5b_reg);

                if(intrapredangle_low5b_reg == 5'd0) begin                                                            //in the filtered there wil be no 2*ntbs pixel available when predangle = 32 (biggest angle)
                    angular_data_3_interpolation_2tap <= {(PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2){1'b0}};
                end
                else begin
                    angular_data_3_interpolation_2tap <= data_4_in*(intrapredangle_low5b_reg);
                end
            end
            else begin
                angular_data_0_interpolation_1tap <= data_1_in*((1'b1 << INTRAPRED_ANGLE_LOW5B_WIDTH) - intrapredangle_low5b_reg);
                angular_data_0_interpolation_2tap <= data_0_in*(intrapredangle_low5b_reg);
                
                angular_data_1_interpolation_1tap <= data_2_in*((1'b1 << INTRAPRED_ANGLE_LOW5B_WIDTH) - intrapredangle_low5b_reg);
                angular_data_1_interpolation_2tap <= data_1_in*(intrapredangle_low5b_reg);
                
                angular_data_2_interpolation_1tap <= data_3_in*((1'b1 << INTRAPRED_ANGLE_LOW5B_WIDTH) - intrapredangle_low5b_reg);
                angular_data_2_interpolation_2tap <= data_2_in*(intrapredangle_low5b_reg);
                
                angular_data_3_interpolation_1tap <= data_4_in*((1'b1 << INTRAPRED_ANGLE_LOW5B_WIDTH) - intrapredangle_low5b_reg);
                angular_data_3_interpolation_2tap <= data_3_in*(intrapredangle_low5b_reg);
            end
        end
    end
    
    always @(posedge clk) begin
        if(enable) begin
//        planar_data_0_from_leftandtop[1]   <= data_0_in*planar_data_0_data_pre_mult;
//        planar_data_0_from_leftandtop[2]   <= data_1_in*((ntbs_in - 1'b1) - inner_y_in);
//        planar_data_0_from_corners[1]      <= (inner_x_in + 1'b1)*planar_top_ntbs_in;
//        planar_data_0_from_corners[2]      <= (inner_y_in + 1'b1)*planar_left_ntbs_in + ntbs_in;
          ntbs_reg                  <= ntbs_in;
          max_4by4_blocks_minus1    <= (ntbs_reg[6 - 1:2]*ntbs_reg[6 - 1:2]) - 1'b1;
          planar_left_ntbs_reg      <= planar_left_ntbs_in;
          planar_top_ntbs_reg       <= planar_top_ntbs_in;
        end
    end
    
    always @(posedge clk) begin
        if(enable) begin
            if(mode_in == MODE_VERTICAL) begin
                ver_hor_sub_data_added_with_zeroth <= {ver_hor_sub_data[PIXEL_WIDTH + 2 - 1],ver_hor_sub_data[PIXEL_WIDTH + 2 - 1:1]} + top_zeroth_reg;
            end
            else begin //MODE HORIZONTAL
                ver_hor_sub_data_added_with_zeroth <= {ver_hor_sub_data[PIXEL_WIDTH + 2 - 1],ver_hor_sub_data[PIXEL_WIDTH + 2 - 1:1]} + left_zeroth_reg;
            end

            data_1_reg_d <= data_1_reg;
            data_2_reg_d <= data_2_reg;
            data_3_reg_d <= data_3_reg;
            data_4_reg_d <= data_4_reg;


            planar_data_0_from_leftandtop      <= data_0_reg*planar_data_0_data_pre_mult + data_1_reg*planar_data_data_post_mult;
            planar_data_0_from_corners         <= planar_data_0_ntbs_pre_mult*planar_top_ntbs_reg + planar_data_ntbs_post_mult*planar_left_ntbs_reg + ntbs_reg;
            
            planar_data_1_from_leftandtop      <= (data_0_reg*planar_data_1_data_pre_mult) + (data_2_reg*planar_data_data_post_mult);
            planar_data_1_from_corners         <= planar_data_1_ntbs_pre_mult*planar_top_ntbs_reg + planar_data_ntbs_post_mult*planar_left_ntbs_reg + ntbs_reg;
            
            planar_data_2_from_leftandtop      <= data_0_reg*planar_data_2_data_pre_mult + data_3_reg*planar_data_data_post_mult;
            planar_data_2_from_corners         <= planar_data_2_ntbs_pre_mult*planar_top_ntbs_reg + planar_data_ntbs_post_mult*planar_left_ntbs_reg + ntbs_reg;
            
            planar_data_3_from_leftandtop      <= data_0_reg*planar_data_3_data_pre_mult + data_4_reg*planar_data_data_post_mult;
            planar_data_3_from_corners         <= planar_data_3_ntbs_pre_mult*planar_top_ntbs_reg + planar_data_ntbs_post_mult*planar_left_ntbs_reg + ntbs_reg;
            
            // angular_next_pred_data_0           <= (angular_data_0_interpolation_1tap + angular_data_0_interpolation_2tap) + {{(PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 5){1'b0}},5'd16};
            // angular_next_pred_data_1           <= (angular_data_1_interpolation_1tap + angular_data_1_interpolation_2tap) + {{(PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 5){1'b0}},5'd16};
            // angular_next_pred_data_2           <= (angular_data_2_interpolation_1tap + angular_data_2_interpolation_2tap) + {{(PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 5){1'b0}},5'd16};
            // angular_next_pred_data_3           <= (angular_data_3_interpolation_1tap + angular_data_3_interpolation_2tap) + {{(PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 5){1'b0}},5'd16};
            angular_next_pred_data_0           <= angular_next_pred_data_0_wire[PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH - 1:INTRAPRED_ANGLE_LOW5B_WIDTH];
            angular_next_pred_data_1           <= angular_next_pred_data_1_wire[PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH - 1:INTRAPRED_ANGLE_LOW5B_WIDTH];
            angular_next_pred_data_2           <= angular_next_pred_data_2_wire[PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH - 1:INTRAPRED_ANGLE_LOW5B_WIDTH];
            angular_next_pred_data_3           <= angular_next_pred_data_3_wire[PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH - 1:INTRAPRED_ANGLE_LOW5B_WIDTH];
        end
    end
    
    assign angular_next_pred_data_0_wire = (angular_data_0_interpolation_1tap + angular_data_0_interpolation_2tap) + {{(PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 5){1'b0}},5'd16};
    assign angular_next_pred_data_1_wire = (angular_data_1_interpolation_1tap + angular_data_1_interpolation_2tap) + {{(PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 5){1'b0}},5'd16};
    assign angular_next_pred_data_2_wire = (angular_data_2_interpolation_1tap + angular_data_2_interpolation_2tap) + {{(PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 5){1'b0}},5'd16};
    assign angular_next_pred_data_3_wire = (angular_data_3_interpolation_1tap + angular_data_3_interpolation_2tap) + {{(PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH + 2 - 5){1'b0}},5'd16};

    assign next_planar_data_0_wire = (planar_data_0_from_leftandtop + planar_data_0_from_corners) >> (log2ntbs + 1'b1);
    assign next_planar_data_1_wire = (planar_data_1_from_leftandtop + planar_data_1_from_corners) >> (log2ntbs + 1'b1);
    assign next_planar_data_2_wire = (planar_data_2_from_leftandtop + planar_data_2_from_corners) >> (log2ntbs + 1'b1);
    assign next_planar_data_3_wire = (planar_data_3_from_leftandtop + planar_data_3_from_corners) >> (log2ntbs + 1'b1);



    always @(posedge clk) begin
        if(enable) begin
            next_planar_data_0  <=  next_planar_data_0_wire[PIXEL_WIDTH - 1:0];                        //(planar_data_0_from_leftandtop + planar_data_0_from_corners) >> (log2ntbs + 1'b1);
            next_planar_data_1  <=  next_planar_data_1_wire[PIXEL_WIDTH - 1:0];                        //(planar_data_1_from_leftandtop + planar_data_1_from_corners) >> (log2ntbs + 1'b1);
            next_planar_data_2  <=  next_planar_data_2_wire[PIXEL_WIDTH - 1:0];                        //(planar_data_2_from_leftandtop + planar_data_2_from_corners) >> (log2ntbs + 1'b1);
            next_planar_data_3  <=  next_planar_data_3_wire[PIXEL_WIDTH - 1:0];                        //(planar_data_3_from_leftandtop + planar_data_3_from_corners) >> (log2ntbs + 1'b1);
            
            if(angular_hor_ver_mode_in == 1'b1) begin

                if(mode_in == MODE_VERTICAL) begin
                    if((inner_x_reg0_is_zero[0])&&(CIDX == 0)&&(log2ntbs < 6'd5)) begin
                        case(ver_hor_sub_data_added_with_zeroth[PIXEL_WIDTH + 2 - 1:PIXEL_WIDTH + 2 - 2])
                            2'b10,2'b11 : angular_pred_data_0 <= {PIXEL_WIDTH{1'b0}};
                            2'b00       : angular_pred_data_0 <= ver_hor_sub_data_added_with_zeroth[PIXEL_WIDTH - 1:0];  
                            2'b01       : angular_pred_data_0 <= {PIXEL_WIDTH{1'b1}};
                        endcase
                    end
                    else begin
                        angular_pred_data_0 <= data_1_reg_d;
                    end
                end else begin
                    if((inner_y_reg0_is_zero[0])&&(CIDX == 0)&&(log2ntbs < 6'd5)) begin
                        case(ver_hor_sub_data_added_with_zeroth[PIXEL_WIDTH + 2 - 1:PIXEL_WIDTH + 2 - 2])
                            2'b10,2'b11 : angular_pred_data_0 <= {PIXEL_WIDTH{1'b0}};
                            2'b00       : angular_pred_data_0 <= ver_hor_sub_data_added_with_zeroth[PIXEL_WIDTH - 1:0];  
                            2'b01       : angular_pred_data_0 <= {PIXEL_WIDTH{1'b1}};
                        endcase
                    end
                    else begin
                        angular_pred_data_0 <= data_1_reg_d;
                    end
                end



                angular_pred_data_1 <= data_2_reg_d;
                angular_pred_data_2 <= data_3_reg_d;
                angular_pred_data_3 <= data_4_reg_d;

            end
            else begin
                angular_pred_data_0 <= angular_next_pred_data_0[PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH - 1:INTRAPRED_ANGLE_LOW5B_WIDTH];
                angular_pred_data_1 <= angular_next_pred_data_1[PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH - 1:INTRAPRED_ANGLE_LOW5B_WIDTH];
                angular_pred_data_2 <= angular_next_pred_data_2[PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH - 1:INTRAPRED_ANGLE_LOW5B_WIDTH];
                angular_pred_data_3 <= angular_next_pred_data_3[PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH - 1:INTRAPRED_ANGLE_LOW5B_WIDTH];
            end
           
        end
    end
    
//    assign planar_data_0_from_leftandtop      = data_0_in*(ntbs_in - 1'b1 - inner_x_in) + data_1_in*(ntbs_in - 1'b1 - inner_y_in);
//    assign planar_data_0_from_corners         = (inner_x_in + 1'b1)*planar_top_ntbs_in + (inner_y_in + 1'b1)*planar_left_ntbs_in + ntbs_in;
//    assign next_planar_data_0                 = (planar_data_0_from_leftandtop + planar_data_0_from_corners) >> (log2ntbs + 1'b1);

//   assign planar_data_1_from_leftandtop      = data_0_in*(ntbs_in - 1'b1 - (inner_x_in + 1'b1)) + data_2_in*(ntbs_in - 1'b1 - inner_y_in);
//    assign planar_data_1_from_corners         = (inner_x_in + 2'd2)*planar_top_ntbs_in + (inner_y_in + 1'b1)*planar_left_ntbs_in + ntbs_in;
//    assign next_planar_data_1                 = (planar_data_1_from_leftandtop + planar_data_1_from_corners) >> (log2ntbs + 1'b1);

//    assign planar_data_2_from_leftandtop      = data_0_in*(ntbs_in - 1'b1 - (inner_x_in + 2'd2)) + data_3_in*(ntbs_in - 1'b1 - inner_y_in);
//    assign planar_data_2_from_corners         = (inner_x_in + 2'd3)*planar_top_ntbs_in + (inner_y_in + 1'b1)*planar_left_ntbs_in + ntbs_in;
//    assign next_planar_data_2                 = (planar_data_2_from_leftandtop + planar_data_2_from_corners) >> (log2ntbs + 1'b1);

//    assign planar_data_3_from_leftandtop      = data_0_in*(ntbs_in - 1'b1 - (inner_x_in + 2'd3)) + data_4_in*(ntbs_in - 1'b1 - inner_y_in);
//    assign planar_data_3_from_corners         = (inner_x_in + 3'd4)*planar_top_ntbs_in + (inner_y_in + 1'b1)*planar_left_ntbs_in + ntbs_in;
//    assign next_planar_data_3                 = (planar_data_3_from_leftandtop + planar_data_3_from_corners) >> (log2ntbs + 1'b1);

    
//    always @(*) begin
//        angular_pred_data_0 = angular_next_pred_data_0[PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH - 1:INTRAPRED_ANGLE_LOW5B_WIDTH];
//        angular_pred_data_1 = angular_next_pred_data_1[PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH - 1:INTRAPRED_ANGLE_LOW5B_WIDTH];
//        angular_pred_data_2 = angular_next_pred_data_2[PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH - 1:INTRAPRED_ANGLE_LOW5B_WIDTH];
//        angular_pred_data_3 = angular_next_pred_data_3[PIXEL_WIDTH + INTRAPRED_ANGLE_LOW5B_WIDTH - 1:INTRAPRED_ANGLE_LOW5B_WIDTH];
//    end
    
    
    always @(posedge clk) begin
        if(reset) begin
            
        
        end
        else begin
        
        
        end 
    end

    always @(posedge clk) begin
        angular_en_reg[4]      <=  angular_en_in;
        angular_en_reg[3]      <=  angular_en_reg[4];
        angular_en_reg[2]      <=  angular_en_reg[3];
        angular_en_reg[1]      <=  angular_en_reg[2];

        planar_en_reg[4]       <=  planar_en_in;
        planar_en_reg[3]       <=  planar_en_reg[4];
        planar_en_reg[2]       <=  planar_en_reg[3];
        planar_en_reg[1]       <=  planar_en_reg[2];
    end
    
    
    
    always @(posedge clk) begin : main_fsm
        integer i;
        integer j;
        if(reset) begin
            state <= STATE_1_ROW_COLUMN;
            predsample_4by4_valid_out <= 1'b0;
            
            angular_en_reg[0]      <=  1'b0;
            planar_en_reg[0]       <=  1'b0;
            dc_en_reg              <=  1'b0;    
            dc_read_addr_offset_out <= {MAX_NTBS_SIZE{1'b0}};
            dc_init_done_out    <=  1'b0;
            counter             <=  6'd0;
            one_before_last_4by4_out <= 1'b1;
        end
        else if (enable) begin
            case(state)
                STATE_1_ROW_COLUMN : begin

                    if(counter == 6'd0) begin
                        angular_en_reg[0]      <=  angular_en_reg[1];
                        planar_en_reg[0]       <=  planar_en_reg[1];
                        dc_en_reg              <=  dc_en_in;
                    end
                    else if(counter == max_4by4_blocks_minus1) begin
                        counter <= {MAX_NTBS_SIZE{1'b1}};
                    end
                    
                    if(angular_en_reg[1] == 1'b1) begin
                        if(mode_in == MODE_HORIZONTAL) begin
                            predsamples[0][0] <= angular_pred_data_0;
                            predsamples[0][1] <= angular_pred_data_1;
                            predsamples[0][2] <= angular_pred_data_2;
                            predsamples[0][3] <= angular_pred_data_3;
                        end
                        else begin //MODE_VERTICAL
                            predsamples[0][0] <= angular_pred_data_0;
                            predsamples[1][0] <= angular_pred_data_1;
                            predsamples[2][0] <= angular_pred_data_2;
                            predsamples[3][0] <= angular_pred_data_3;
                        end
                        state <= STATE_2_ROW_COLUMN;
                        one_before_last_4by4_out <= 1'b0;
                    end
                    else if(planar_en_reg[1] == 1'b1) begin
                        predsamples[0][0] <= next_planar_data_0;//[PIXEL_WIDTH - 1:0];
                        predsamples[1][0] <= next_planar_data_1;//[PIXEL_WIDTH - 1:0];
                        predsamples[2][0] <= next_planar_data_2;//[PIXEL_WIDTH - 1:0];
                        predsamples[3][0] <= next_planar_data_3;//[PIXEL_WIDTH - 1:0];
                        state <= STATE_2_ROW_COLUMN;
                        one_before_last_4by4_out <= 1'b0;
                    end
                    else if(dc_en_in == 1'b1) begin
                        // one_before_last_4by4_out <= 1'b0;
                        if((inner_x_in == {(MAX_NTBS_SIZE){1'b0}}) && (inner_y_in == {(MAX_NTBS_SIZE){1'b0}})) begin
                            dc_counter              <= {DC_COUNTER_WIDTH{1'b0}};
                            dc_acc                  <= ntbs_in;
                            dc_read_addr_offset_out <= {{(MAX_NTBS_SIZE - 4){1'b0}},4'd8};
                            state                   <= STATE_DC_INIT;

                            dc_top_level1_1         <=  {DC_MAXWIDTH{1'b0}};
                            dc_top_level1_2         <=  {DC_MAXWIDTH{1'b0}};
                            dc_top_level1_3         <=  {DC_MAXWIDTH{1'b0}};
                            dc_top_level1_4         <=  {DC_MAXWIDTH{1'b0}};
                            dc_top_level2_1         <=  {DC_MAXWIDTH{1'b0}};
                            dc_top_level2_2         <=  {DC_MAXWIDTH{1'b0}};
                            dc_top_level3_1         <=  {DC_MAXWIDTH{1'b0}};
                            dc_left_level1_1        <=  {DC_MAXWIDTH{1'b0}};
                            dc_left_level1_2        <=  {DC_MAXWIDTH{1'b0}};
                            dc_left_level1_3        <=  {DC_MAXWIDTH{1'b0}};
                            dc_left_level1_4        <=  {DC_MAXWIDTH{1'b0}};
                            dc_left_level2_1        <=  {DC_MAXWIDTH{1'b0}};
                            dc_left_level2_2        <=  {DC_MAXWIDTH{1'b0}};
                            dc_left_level3_1        <=  {DC_MAXWIDTH{1'b0}};
                            dc_top_left_merge       <=  {DC_MAXWIDTH{1'b0}};
                        end
                        else begin
                            state       <= STATE_4_ROW_COLUMN;
                        end
                    end
                    else if(counter > 6'd0) begin
                        if(angular_en_reg[0] == 1'b1) begin
                            if(mode_in == MODE_HORIZONTAL) begin
                                predsamples[0][0] <= angular_pred_data_0;
                                predsamples[0][1] <= angular_pred_data_1;
                                predsamples[0][2] <= angular_pred_data_2;
                                predsamples[0][3] <= angular_pred_data_3;
                            end
                            else begin //MODE_VERTICAL
                                predsamples[0][0] <= angular_pred_data_0;
                                predsamples[1][0] <= angular_pred_data_1;
                                predsamples[2][0] <= angular_pred_data_2;
                                predsamples[3][0] <= angular_pred_data_3;
                            end
                        end
                        else if(planar_en_reg[0] == 1'b1) begin
                            predsamples[0][0] <= next_planar_data_0;//[PIXEL_WIDTH - 1:0];
                            predsamples[1][0] <= next_planar_data_1;//[PIXEL_WIDTH - 1:0];
                            predsamples[2][0] <= next_planar_data_2;//[PIXEL_WIDTH - 1:0];
                            predsamples[3][0] <= next_planar_data_3;//[PIXEL_WIDTH - 1:0];
                        end
                        state <= STATE_2_ROW_COLUMN;    
                    end
                    
                    predsample_4by4_valid_out <= 1'b0;
                end
                STATE_2_ROW_COLUMN : begin
                    if(angular_en_reg[0] == 1'b1) begin
                        if(mode_in == MODE_HORIZONTAL) begin
                            predsamples[1][0] <= angular_pred_data_0;
                            predsamples[1][1] <= angular_pred_data_1;
                            predsamples[1][2] <= angular_pred_data_2;
                            predsamples[1][3] <= angular_pred_data_3;
                        end
                        else begin //MODE_VERTICAL
                            predsamples[0][1] <= angular_pred_data_0;
                            predsamples[1][1] <= angular_pred_data_1;
                            predsamples[2][1] <= angular_pred_data_2;
                            predsamples[3][1] <= angular_pred_data_3;
                        end
                        if(max_4by4_blocks_minus1 != 0) begin
                            counter <= counter + 1'b1;
                        end
                        state <= STATE_3_ROW_COLUMN;
                    end
                    else if(planar_en_reg[0] == 1'b1) begin
                        predsamples[0][1] <= next_planar_data_0;//[PIXEL_WIDTH - 1:0];
                        predsamples[1][1] <= next_planar_data_1;//[PIXEL_WIDTH - 1:0];
                        predsamples[2][1] <= next_planar_data_2;//[PIXEL_WIDTH - 1:0];
                        predsamples[3][1] <= next_planar_data_3;//[PIXEL_WIDTH - 1:0];

                        if(max_4by4_blocks_minus1 != 0) begin
                            counter <= counter + 1'b1;
                        end
                        state <= STATE_3_ROW_COLUMN;
                    end
                end
                STATE_3_ROW_COLUMN : begin
                    if(angular_en_reg[0] == 1'b1) begin
                        if(mode_in == MODE_HORIZONTAL) begin
                            predsamples[2][0] <= angular_pred_data_0;
                            predsamples[2][1] <= angular_pred_data_1;
                            predsamples[2][2] <= angular_pred_data_2;
                            predsamples[2][3] <= angular_pred_data_3;
                        end
                        else begin //MODE_VERTICAL
                            predsamples[0][2] <= angular_pred_data_0;
                            predsamples[1][2] <= angular_pred_data_1;
                            predsamples[2][2] <= angular_pred_data_2;
                            predsamples[3][2] <= angular_pred_data_3;
                        end
                        state <= STATE_4_ROW_COLUMN;
                    end
                    else if(planar_en_reg[0] == 1'b1) begin
                        predsamples[0][2] <= next_planar_data_0;//[PIXEL_WIDTH - 1:0];
                        predsamples[1][2] <= next_planar_data_1;//[PIXEL_WIDTH - 1:0];
                        predsamples[2][2] <= next_planar_data_2;//[PIXEL_WIDTH - 1:0];
                        predsamples[3][2] <= next_planar_data_3;//[PIXEL_WIDTH - 1:0];
                        state <= STATE_4_ROW_COLUMN;
                    end

                    if(counter == (max_4by4_blocks_minus1)) begin
                        one_before_last_4by4_out <= 1'b1;
                    end
                end
                STATE_4_ROW_COLUMN : begin
                    if(angular_en_reg[0] == 1'b1) begin
                        if(mode_in == MODE_HORIZONTAL) begin
                            predsamples[3][0] <= angular_pred_data_0;
                            predsamples[3][1] <= angular_pred_data_1;
                            predsamples[3][2] <= angular_pred_data_2;
                            predsamples[3][3] <= angular_pred_data_3;
                        end
                        else begin //MODE_VERTICAL
                            predsamples[0][3] <= angular_pred_data_0;
                            predsamples[1][3] <= angular_pred_data_1;
                            predsamples[2][3] <= angular_pred_data_2;
                            predsamples[3][3] <= angular_pred_data_3;
                        end
                        state                       <= STATE_1_ROW_COLUMN;
                        predsample_4by4_valid_out   <= 1'b1;
                    end
                    else if(planar_en_reg[0] == 1'b1) begin
                        predsamples[0][3] <= next_planar_data_0;//[PIXEL_WIDTH - 1:0];
                        predsamples[1][3] <= next_planar_data_1;//[PIXEL_WIDTH - 1:0];
                        predsamples[2][3] <= next_planar_data_2;//[PIXEL_WIDTH - 1:0];
                        predsamples[3][3] <= next_planar_data_3;//[PIXEL_WIDTH - 1:0];
                        
                        state <= STATE_1_ROW_COLUMN;
                        predsample_4by4_valid_out   <= 1'b1;
                    end
                    else if(dc_en_reg   == 1'b1) begin
                        if ((log2ntbs < {{(MAX_NTBS_SIZE - 3){1'b0}},3'd5}) && (CIDX == 2'd0)) begin
                            
                            if((inner_x_reg[3] == {MAX_NTBS_SIZE{1'b0}}) && (inner_y_reg[3] == {MAX_NTBS_SIZE{1'b0}})) begin
                                for ( i = 1 ; i < 4 ; i = i + 1 ) begin
                                    for ( j = 1 ; j < 4 ; j = j + 1 ) begin
                                        predsamples[i][j] <= dc_acc[PIXEL_WIDTH - 1:0];
                                    end
                                end
                                
                                predsamples[0][0] <= (((data_0_in + dc_aux_0_in) + (2*dc_acc + 2'd2)) >> 2)%(1 << PIXEL_WIDTH);

                                // predsamples[0][0] <= ((data_0_in + 3*dc_acc + 2'd2) >> 2)%(1 << PIXEL_WIDTH);
                                predsamples[1][0] <= ((data_1_in + 3*dc_acc + 2'd2) >> 2)%(1 << PIXEL_WIDTH);
                                predsamples[2][0] <= ((data_2_in + 3*dc_acc + 2'd2) >> 2)%(1 << PIXEL_WIDTH);
                                predsamples[3][0] <= ((data_3_in + 3*dc_acc + 2'd2) >> 2)%(1 << PIXEL_WIDTH);
                                
                                // predsamples[0][0] <= ((dc_aux_0_in + 3*dc_acc + 2'd2) >> 2)%(1 << PIXEL_WIDTH);
                                predsamples[0][1] <= ((dc_aux_1_in + 3*dc_acc + 2'd2) >> 2)%(1 << PIXEL_WIDTH);
                                predsamples[0][2] <= ((dc_aux_2_in + 3*dc_acc + 2'd2) >> 2)%(1 << PIXEL_WIDTH);
                                predsamples[0][3] <= ((dc_aux_3_in + 3*dc_acc + 2'd2) >> 2)%(1 << PIXEL_WIDTH);
                                
                                if(( inner_x_reg[3] == ntbs_in - 3'd4 )&&(inner_y_reg[3] == ntbs_in - 3'd4 )) begin
                                    state                       <= STATE_1_ROW_COLUMN;
                                    dc_init_done_out            <=  1'b0;
                                    predsample_4by4_valid_out   <= 1'b1;
                                end
                                else begin
                                    state <= STATE_4_ROW_COLUMN;
                                    predsample_4by4_valid_out   <= 1'b1;
                                end

                            end
                            else if(inner_x_reg[3] == {MAX_NTBS_SIZE{1'b0}}) begin
                                predsamples[0][0] <= ((dc_aux_0_in + 3*dc_acc + 2'd2) >> 2)%(1 << PIXEL_WIDTH);
                                predsamples[0][1] <= ((dc_aux_1_in + 3*dc_acc + 2'd2) >> 2)%(1 << PIXEL_WIDTH);
                                predsamples[0][2] <= ((dc_aux_2_in + 3*dc_acc + 2'd2) >> 2)%(1 << PIXEL_WIDTH);
                                predsamples[0][3] <= ((dc_aux_3_in + 3*dc_acc + 2'd2) >> 2)%(1 << PIXEL_WIDTH);
                                
                                for ( i = 1 ; i < 4 ; i = i + 1 ) begin
                                    for ( j = 0 ; j < 4 ; j = j + 1 ) begin
                                        predsamples[i][j] <= dc_acc%(1 << PIXEL_WIDTH);
                                    end
                                end
                                if(( inner_x_reg[3] == ntbs_in - 3'd4 )&&(inner_y_reg[3] == ntbs_in - 3'd4 )) begin
                                    state                       <= STATE_1_ROW_COLUMN;
                                    dc_init_done_out            <=  1'b0;
                                    predsample_4by4_valid_out   <=  1'b1;
                                end
                                else begin
                                    state <= STATE_4_ROW_COLUMN;
                                    predsample_4by4_valid_out   <= 1'b1;
                                end

                            end
                            else if(inner_y_reg[3] == {MAX_NTBS_SIZE{1'b0}}) begin
                                predsamples[0][0] <= ((data_0_in + 3*dc_acc + 2'd2) >> 2)%(1 << PIXEL_WIDTH);
                                predsamples[1][0] <= ((data_1_in + 3*dc_acc + 2'd2) >> 2)%(1 << PIXEL_WIDTH);
                                predsamples[2][0] <= ((data_2_in + 3*dc_acc + 2'd2) >> 2)%(1 << PIXEL_WIDTH);
                                predsamples[3][0] <= ((data_3_in + 3*dc_acc + 2'd2) >> 2)%(1 << PIXEL_WIDTH);
                                
                                for ( i = 0 ; i < 4 ; i = i + 1 ) begin
                                    for ( j = 1 ; j < 4 ; j = j + 1 ) begin
                                        predsamples[i][j] <= dc_acc%(1 << PIXEL_WIDTH);
                                    end
                                end
                                if(( inner_x_reg[3] == ntbs_in - 3'd4 )&&(inner_y_reg[3] == ntbs_in - 3'd4 )) begin
                                    state                       <= STATE_1_ROW_COLUMN;
                                    dc_init_done_out            <=  1'b0;
                                    predsample_4by4_valid_out   <= 1'b1;
                                end
                                else begin
                                    state <= STATE_4_ROW_COLUMN;
                                    predsample_4by4_valid_out   <= 1'b1;
                                end
                            
                            end
                            else begin
                                for ( i = 0 ; i < 4 ; i = i + 1 ) begin
                                    for ( j = 0 ; j < 4 ; j = j + 1 ) begin
                                        predsamples[i][j] <= dc_acc%(1 << PIXEL_WIDTH);
                                    end
                                end
                                if(( inner_x_reg[3] == ntbs_in - 3'd4 )&&(inner_y_reg[3] == ntbs_in - 3'd4 )) begin
                                    state <= STATE_1_ROW_COLUMN;
                                    dc_init_done_out            <=  1'b0;
                                    predsample_4by4_valid_out   <= 1'b1;
                                end
                                else begin
                                    state <= STATE_4_ROW_COLUMN;
                                    predsample_4by4_valid_out   <= 1'b1;
                                end
                            end       
                        end
                        else begin
                            for ( i = 0 ; i < 4 ; i = i + 1 ) begin
                                for ( j = 0 ; j < 4 ; j = j + 1 ) begin
                                    predsamples[i][j] <= dc_acc%(1 << PIXEL_WIDTH);
                                end
                            end
                            if(( inner_x_reg[3] == ntbs_in - 3'd4 )&&(inner_y_reg[3] == ntbs_in - 3'd4 )) begin
                                state <= STATE_1_ROW_COLUMN;
                                dc_init_done_out            <=  1'b0;
                                predsample_4by4_valid_out   <= 1'b1;
                            end
                            else begin
                                state <= STATE_4_ROW_COLUMN;
                                predsample_4by4_valid_out   <= 1'b1;
                            end
                        end
                    end
                end
                //STATES ONLY FOR DC MODE
                STATE_DC_WAIT3 : begin
                    state <= STATE_4_ROW_COLUMN;
                end
                STATE_DC_WAIT2 : begin
                    state <= STATE_DC_WAIT3;
                end
                STATE_DC_WAIT1 : begin
                    state <= STATE_DC_WAIT2;
                end
                STATE_DC_INIT : begin
                    if(dc_counter == {{(DC_COUNTER_WIDTH - 4){1'b0}},((ntbs_in - 1'b1) >> 3) + 3'd7}) begin
                        //state <= STATE_4_ROW_COLUMN;
                        state <= STATE_DC_WAIT1;
                        dc_acc <= dc_acc >> ((log2ntbs) + 1'b1);
                        dc_read_addr_offset_out <= {MAX_NTBS_SIZE{1'b0}};
                        dc_init_done_out <= 1'b1;
                    end
                    else begin
                        if(dc_counter >= {{(DC_COUNTER_WIDTH - 2){1'b0}},2'd2}) begin
                            
                            dc_top_level1_1 <= data_0_in + data_1_in;
                            dc_top_level1_2 <= data_2_in + data_3_in;
                            dc_top_level1_3 <= data_4_in + data_5_in;
                            dc_top_level1_4 <= data_6_in + data_7_in;
                            
                            dc_top_level2_1 <= dc_top_level1_1 + dc_top_level1_2;
                            dc_top_level2_2 <= dc_top_level1_3 + dc_top_level1_4;
                            
                            if(log2ntbs == {{(MAX_NTBS_SIZE - 2){1'b0}},2'd2}) begin
                                dc_top_level3_1 <= dc_top_level2_1;
                            end
                            else begin
                                dc_top_level3_1 <= dc_top_level2_1 + dc_top_level2_2;
                            end
                                
                            
                            dc_left_level1_1 <= dc_aux_0_in + dc_aux_1_in;
                            dc_left_level1_2 <= dc_aux_2_in + dc_aux_3_in;
                            dc_left_level1_3 <= dc_aux_4_in + dc_aux_5_in;
                            dc_left_level1_4 <= dc_aux_6_in + dc_aux_7_in;
                            
                            dc_left_level2_1 <= dc_left_level1_1 + dc_left_level1_2;
                            dc_left_level2_2 <= dc_left_level1_3 + dc_left_level1_4;
                            
                            if(log2ntbs == {{(MAX_NTBS_SIZE - 2){1'b0}},2'd2}) begin
                                dc_left_level3_1 <= dc_left_level2_1;
                            end
                            else begin
                                dc_left_level3_1 <= dc_left_level2_1 + dc_left_level2_2;
                            end
                            
                            dc_top_left_merge <= dc_top_level3_1 + dc_left_level3_1;
                            dc_acc <= dc_acc + dc_top_left_merge;

                        end
                        

                        dc_counter              <= dc_counter + 1'b1;
                        dc_read_addr_offset_out <= dc_read_addr_offset_out + 4'd8;
                    end
                
                end
            endcase
        end
    end
    
    
    generate
        genvar i;
        genvar j;
        
        for (i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin : row_iteration
            for (j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin : column_iteration
                assign predsample_4by4_out[i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE +(j+1)*PIXEL_WIDTH - 1:j*PIXEL_WIDTH + i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE] = predsamples[i][j];
            end
        end
    endgenerate
    

endmodule
