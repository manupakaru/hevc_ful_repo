`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    07:55:48 10/17/2013
// Design Name:
// Module Name:    intra_line_buffer_membank
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module intra_constrained_range_gen(
        clk,
        reset,
        enable,

        xcu_in,
        ycu_in,
        ntbs_in,
        intra_inter_mode_in,
        valid_in,
        ready_out,

        log2_ctusize_in,
        frame_width_in,
        frame_height_in,
        constrained_intra_pred_flag_in,
        config_valid_in,

        constrained_top_range_out,
        constrained_left_range_out,

        x_second_bit_in,
        y_second_bit_in,
        valid_4by4_in,

        new_frame_in,
    );
    
   `include "../sim/pred_def.v"

//----------------------------------------------------
//PARAMETERS
//----------------------------------------------------
    //parameter                       LOG2_FRAME_SIZE = 13;
    //parameter                       MAX_LOG2CTBSIZE_WIDTH = 3;
    //parameter                       PIXEL_WIDTH = 8;



//----------------------------------------------------
//LOCALPARAMS
//----------------------------------------------------

    localparam                      TOP_BUFFER_MEM_SIZE_1D              = 8;
    localparam                      LEFT_BUFFER_MEM_SIZE_1D             = 8;
    localparam                      NUMBER_OF_3BIT_COMPARATORS      = 8;
    //localparam                      MAX_NTBS_SIZE                       = 6;
    localparam                      MAX_FRAMEWIDTH_IN_8X8               = LOG2_FRAME_SIZE - 3;

    localparam                      MAX_CTU_SIZE                        = 64;
    localparam                      MAX_CTU_SIZE_DIV_8                  = MAX_CTU_SIZE/8;
    localparam                      LOG2_MAX_CTU_SIZE_DIV_8             = 4;

    localparam                      MODE_INTER = 1;
    localparam                      MODE_INTRA = 0;

    localparam                      STATE_INIT                      = 0;
    localparam                      STATE_READ                      = 1;
    localparam                      STATE_DONE                      = 2;
    localparam                      STATE_UPDATE_TOPMEM_CTU16       = 3;
    localparam                      STATE_UPDATE_TOPMEM_CTU32       = 4;
    localparam                      STATE_UPDATE_TOPMEM_CTU64       = 5;
    localparam                      STATE_UPDATE_TOPMEM_CTU16_LAST  = 6;
    localparam                      STATE_UPDATE_TOPMEM_CTU32_LAST  = 7;
    localparam                      STATE_UPDATE_TOPMEM_CTU64_LAST  = 8;

    localparam                      RESET_STATE_INIT = 0;
    localparam                      RESET_STATE_ACTIVE = 1;


//----------------------------------------------------
//I/O Signals
//----------------------------------------------------

    input                                   clk;
    input                                   reset;
    input                                   enable;

    input   [LOG2_FRAME_SIZE - 3 - 1:0]     xcu_in;
    input   [LOG2_FRAME_SIZE - 3 - 1:0]     ycu_in;
    input   [MAX_NTBS_SIZE - 1:0]           ntbs_in;
    input                                   intra_inter_mode_in;
    input                                   valid_in;
    output reg                              ready_out;

    input   [MAX_LOG2CTBSIZE_WIDTH - 1:0]   log2_ctusize_in;
    input   [LOG2_FRAME_SIZE - 1:0]         frame_width_in;
    input   [LOG2_FRAME_SIZE - 1:0]         frame_height_in;
    input                                   constrained_intra_pred_flag_in;
    input                                   config_valid_in;

    output reg [LOG2_FRAME_SIZE - 1:0]      constrained_top_range_out;
    output reg [LOG2_FRAME_SIZE - 1:0]      constrained_left_range_out;

    input                                   x_second_bit_in;
    input                                   y_second_bit_in;
    input                                   valid_4by4_in;

    input                                   new_frame_in;

    // input                                                       portr_en;

//----------------------------------------------------
//  Internal Regs and Wires
//----------------------------------------------------

    //reg  [LOG2_MAX_CTU_SIZE_DIV_8 - 1:0]                   left_buffer_mem[LEFT_BUFFER_MEM_SIZE_1D - 1:0][LEFT_BUFFER_MEM_SIZE_1D - 1:0];
    reg  [LOG2_MAX_CTU_SIZE_DIV_8 - 1:0]                   left_buffer_mem[0:LEFT_BUFFER_MEM_SIZE_1D - 1][0:LEFT_BUFFER_MEM_SIZE_1D - 1];
    reg  [LOG2_MAX_CTU_SIZE_DIV_8 - 1:0]                   next_left_buffer_right_update_mem[LEFT_BUFFER_MEM_SIZE_1D - 1:0];

    //reg  [MAX_FRAMEWIDTH_IN_8X8 - 1:0]                top_buffer_mem[LEFT_BUFFER_MEM_SIZE_1D - 1:0][LEFT_BUFFER_MEM_SIZE_1D - 1 - 1:0];
    reg  [MAX_FRAMEWIDTH_IN_8X8 - 1:0]                top_buffer_mem[0:LEFT_BUFFER_MEM_SIZE_1D - 1][0:LEFT_BUFFER_MEM_SIZE_1D - 1 - 1];
    reg  [MAX_FRAMEWIDTH_IN_8X8 - 1:0]                next_top_buffer_bottom_update_mem[4 + LEFT_BUFFER_MEM_SIZE_1D + 1 - 1:0];

    reg                                                                 porta_wen_reg;
    reg         [LOG2_FRAME_SIZE - 3 - 1:0]                             porta_addr_reg;
    reg         [LOG2_FRAME_SIZE - 3 - 1:0]                             porta_wdata_reg;
    wire        [LOG2_FRAME_SIZE - 3 - 1:0]                             porta_rdata_wire;

    reg                                                                 portb_wen_reg;
    reg         [LOG2_FRAME_SIZE - 3 - 1:0]                             portb_addr_reg;
    wire        [LOG2_FRAME_SIZE - 3 - 1:0]                             portb_wdata_wire;
    wire        [LOG2_FRAME_SIZE - 3 - 1:0]                             portb_rdata_wire;

    reg         [LOG2_FRAME_SIZE - 1 : 0]                               xcu_reg;
    reg         [LOG2_FRAME_SIZE - 1 : 0]                               ycu_reg;
    reg         [MAX_NTBS_SIZE - 3 - 1:0]                               ntbs_div_8_reg;
    reg                                                                 intra_inter_mode_reg;


    integer                                                             state;
    integer                                                             old_state;
    reg                                                                 valid_reg;
    reg                                                                 valid_reg_d;
    reg                                                                 valid_reg_dd;

    reg                                                                 valid_reg_8by8_d;

    wire        [3 - 1:0]                                               ycu_inner_ctu_div_8_reg_minus_one_wire;
    wire        [3 - 1:0]                                               xcu_inner_ctu_div_8_reg_minus_one_wire;
    reg         [3 - 1:0]                                               ycu_inner_ctu_div_8_reg;
    reg         [3 - 1:0]                                               xcu_inner_ctu_div_8_reg;
    wire        [3 - 1:0]                                               xcu_3lsb_wire;

    reg         [LOG2_FRAME_SIZE - 1:0]                                 constrained_left_range_reg;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 constrained_top_range_reg;
    reg         [NUMBER_OF_3BIT_COMPARATORS - 1:0]                      top_eight_3bit_compare_result;
    reg         [NUMBER_OF_3BIT_COMPARATORS - 1:0]                      left_eight_3bit_compare_result;
    reg         [NUMBER_OF_3BIT_COMPARATORS + 1 - 1:-4]                 lastrow_top_eight_3bit_compare_result;
    reg         [NUMBER_OF_3BIT_COMPARATORS + 1 - 1:-4]                 lastcol_left_eight_3bit_compare_result;

    reg         [TOP_BUFFER_MEM_SIZE_1D - 1 :0]                         top_mem_update_control_reg;
    reg         [3 - 1 :0]                                              top_mem_update_control_offset_reg;
    reg         [TOP_BUFFER_MEM_SIZE_1D - 1 :0]                         left_mem_update_control_reg;
    reg         [3 - 1 :0]                                              left_mem_update_control_offset_reg;

    reg         [3 - 1: 0]                                              log2_ctusize_reg;
    reg                                                                 first_cu_row_reg;
    reg                                                                 first_cu_col_reg;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 frame_width_reg;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 frame_height_reg;
    reg                                                                 constrained_intra_pred_flag_reg;

    integer                                                             reset_state;
    wire                                                                new_ctu_wire;

    wire                                                                current_ctu_done_wire;
    reg                                                                 current_ctu_normal_done_wire;
    reg                                                                 current_ctu_horlast_done_wire;
    reg                                                                 current_ctu_verlast_done_wire;

    reg                                                                current_ctu_horlast_done_reg;
    reg                                                                current_ctu_verlast_done_reg;

    reg                                                                 current_ctu_done_reg;
    reg                                                                 current_ctu_done_reg_d;


    wire        [3 - 1:0]                                               ycu_inner_ctu_div_8_plus_ntbs_div_8_wire;
    wire        [3 - 1:0]                                               xcu_inner_ctu_div_8_plus_ntbs_div_8_wire;
    wire        [LOG2_FRAME_SIZE - 3:0]                                 ycu_div_8_plus_ntbs_div_8_wire;
    wire        [LOG2_FRAME_SIZE - 3:0]                                 xcu_div_8_plus_ntbs_div_8_wire;

    reg         [4 - 1:0]                                               topmem_ctu_update_addr_offset;

    reg         [LOG2_FRAME_SIZE - 1:0]                                 new_constrained_left_range_reg;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 new_constrained_top_range_reg;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 old_constrained_left_range_reg;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 old_constrained_top_range_reg;
    reg                                                                 use_new_constrained_top_range_reg;

    reg         [LOG2_FRAME_SIZE - 1:0]                                 constrained_top_range_wire;
    reg         [LOG2_FRAME_SIZE - 1:0]                                 constrained_left_range_wire;


//----------------------------------------------------
//  Implementation
//----------------------------------------------------

    // assign ycu_inner_ctu_div_8_reg            = ycu_reg[3 - 1:0];
    assign ycu_inner_ctu_div_8_reg_minus_one_wire  = ycu_inner_ctu_div_8_reg - 1'b1;
    assign xcu_inner_ctu_div_8_reg_minus_one_wire  = xcu_inner_ctu_div_8_reg - 1'b1;
    assign xcu_3lsb_wire            = xcu_reg[3 - 1:0];

    assign new_ctu_wire             = ((ycu_inner_ctu_div_8_reg == 3'd0) && (xcu_inner_ctu_div_8_reg == 3'd0) && (valid_reg_8by8_d == 1'b1));

    assign ycu_inner_ctu_div_8_plus_ntbs_div_8_wire = ycu_inner_ctu_div_8_reg + ntbs_div_8_reg;
    assign xcu_inner_ctu_div_8_plus_ntbs_div_8_wire = xcu_inner_ctu_div_8_reg + ntbs_div_8_reg;
    assign ycu_div_8_plus_ntbs_div_8_wire = ycu_reg + ntbs_div_8_reg;
    assign xcu_div_8_plus_ntbs_div_8_wire = xcu_reg + ntbs_div_8_reg;


    always @(*) begin
        if (state == STATE_INIT) begin
            current_ctu_normal_done_wire  = 1'b0;
            current_ctu_horlast_done_wire = 1'b0;
            current_ctu_verlast_done_wire = 1'b0;
        end
        else begin
            case(log2_ctusize_reg)
                3'd4 : begin
                    current_ctu_normal_done_wire = ((ycu_inner_ctu_div_8_plus_ntbs_div_8_wire == 3'd1 ) && (xcu_inner_ctu_div_8_plus_ntbs_div_8_wire == 3'd1));
                    current_ctu_horlast_done_wire = ((ycu_inner_ctu_div_8_plus_ntbs_div_8_wire == 3'd1 ) && (xcu_div_8_plus_ntbs_div_8_wire == frame_width_reg[LOG2_FRAME_SIZE - 1:3]));
                    current_ctu_verlast_done_wire = ((ycu_div_8_plus_ntbs_div_8_wire == frame_height_reg[LOG2_FRAME_SIZE - 1:3]) && (xcu_inner_ctu_div_8_plus_ntbs_div_8_wire == 3'd1));
                end
                3'd5 : begin
                    current_ctu_normal_done_wire = ((ycu_inner_ctu_div_8_plus_ntbs_div_8_wire == 3'd3 ) && (xcu_inner_ctu_div_8_plus_ntbs_div_8_wire == 3'd3));
                    current_ctu_horlast_done_wire = ((ycu_inner_ctu_div_8_plus_ntbs_div_8_wire == 3'd3 ) && (xcu_div_8_plus_ntbs_div_8_wire == frame_width_reg[LOG2_FRAME_SIZE - 1:3]));
                    current_ctu_verlast_done_wire = ((ycu_div_8_plus_ntbs_div_8_wire == frame_height_reg[LOG2_FRAME_SIZE - 1:3]) && (xcu_inner_ctu_div_8_plus_ntbs_div_8_wire == 3'd3));
                end
                default : begin
                    current_ctu_normal_done_wire = ((ycu_inner_ctu_div_8_plus_ntbs_div_8_wire == 3'd7 ) && (xcu_inner_ctu_div_8_plus_ntbs_div_8_wire == 3'd7));
                    current_ctu_horlast_done_wire = ((ycu_inner_ctu_div_8_plus_ntbs_div_8_wire == 3'd7 ) && (xcu_div_8_plus_ntbs_div_8_wire == frame_width_reg[LOG2_FRAME_SIZE - 1:3]));
                    current_ctu_verlast_done_wire = ((ycu_div_8_plus_ntbs_div_8_wire == frame_height_reg[LOG2_FRAME_SIZE - 1:3]) && (xcu_inner_ctu_div_8_plus_ntbs_div_8_wire == 3'd7));
                end
            endcase
        end
    end

    always @(posedge clk) begin : delay_range_out_signals
        constrained_left_range_out <= constrained_left_range_wire;
        constrained_top_range_out <= constrained_top_range_wire;
    end


    assign current_ctu_done_wire    = current_ctu_normal_done_wire | current_ctu_horlast_done_wire | current_ctu_verlast_done_wire;

    always @(posedge clk) begin
        if(reset) begin
            current_ctu_done_reg <= 1'b0;
        end
        else begin
            if((current_ctu_done_reg == 1'b0)||(state == STATE_INIT)) begin
                current_ctu_done_reg <= current_ctu_done_wire;
                current_ctu_horlast_done_reg <= current_ctu_horlast_done_wire;
                current_ctu_verlast_done_reg <= current_ctu_verlast_done_wire;
            end
            else begin
                current_ctu_done_reg <= |ntbs_div_8_reg;
            end
        end
    end

    always @(*) begin
        case(log2_ctusize_reg)
            3'd4 : begin
                ycu_inner_ctu_div_8_reg = {2'b00,ycu_reg[1 - 1:0]};
                xcu_inner_ctu_div_8_reg = {2'b00,xcu_reg[1 - 1:0]};
            end
            3'd5 : begin
                ycu_inner_ctu_div_8_reg = {1'b0,ycu_reg[2 - 1:0]};
                xcu_inner_ctu_div_8_reg = {1'b0,xcu_reg[2 - 1:0]};
            end
            default : begin //3'd6
                ycu_inner_ctu_div_8_reg = ycu_reg[3 - 1:0];
                xcu_inner_ctu_div_8_reg = xcu_reg[3 - 1:0];
            end
        endcase
    end


    intra_min_range_mem
        #(
            .LOG2_MEM_SIZE(LOG2_FRAME_SIZE - 3),
            .DATA_WIDTH(LOG2_FRAME_SIZE - 3)
        )
    intra_min_range_mem_block
        (
            .clk                (clk),

            .porta_wen_in       (porta_wen_reg),
            .porta_addr_in      (porta_addr_reg),
            .porta_wdata_in     (porta_wdata_reg),
            .porta_rdata_out    (porta_rdata_wire),

            .portb_wen_in       (portb_wen_reg),
            .portb_addr_in      (portb_addr_reg),
            .portb_wdata_in     (portb_wdata_wire),
            .portb_rdata_out    (portb_rdata_wire)
        );


    always @(*) begin : eight_3bit_comparators
        integer i;

        //top line buffer

        for ( i = 0 ; i < NUMBER_OF_3BIT_COMPARATORS ; i = i + 1) begin
            if(intra_inter_mode_reg == MODE_INTRA) begin
                top_eight_3bit_compare_result[i] = (top_buffer_mem[i][top_mem_update_control_offset_reg] > xcu_reg); //[x][y]
            end
            else begin
               top_eight_3bit_compare_result[i] = (i > xcu_3lsb_wire); //[x][y]
            end
        end

        for ( i = -4 ; i < NUMBER_OF_3BIT_COMPARATORS + 1 ; i = i + 1) begin
            if(intra_inter_mode_reg == MODE_INTRA) begin
                lastrow_top_eight_3bit_compare_result[i] = (next_top_buffer_bottom_update_mem[4 + i] > xcu_reg);
            end
            else begin
                lastrow_top_eight_3bit_compare_result[i] = (i > xcu_3lsb_wire) && (i >= 0);
            end
        end

        //left line buffer

        for ( i = 0 ; i < NUMBER_OF_3BIT_COMPARATORS ; i = i + 1) begin
            if(intra_inter_mode_reg == MODE_INTRA) begin
                left_eight_3bit_compare_result[i] = (left_buffer_mem[left_mem_update_control_offset_reg + 1][i] > ycu_inner_ctu_div_8_reg); //[x][y]
            end
            else begin
                left_eight_3bit_compare_result[i] = (i > ycu_inner_ctu_div_8_reg); //[x][y]
            end
        end

        for ( i = 0 ; i < NUMBER_OF_3BIT_COMPARATORS ; i = i + 1) begin
            if(intra_inter_mode_reg == MODE_INTRA) begin
                lastcol_left_eight_3bit_compare_result[i] = (next_left_buffer_right_update_mem[i] > ycu_inner_ctu_div_8_reg);
            end
            else begin
                lastcol_left_eight_3bit_compare_result[i] = (i > ycu_inner_ctu_div_8_reg);
            end
        end

    end

    always @(posedge clk) begin : mem_update
        integer i;
        integer j;



        if (reset) begin

            //top line buffer

            for (j = 0 ; j < TOP_BUFFER_MEM_SIZE_1D - 1 ; j = j + 1) begin
                for (i = 0 ; i < NUMBER_OF_3BIT_COMPARATORS ;i = i + 1) begin
                    top_buffer_mem[i][j] <= {MAX_FRAMEWIDTH_IN_8X8{1'b1}};
                end
            end

            for (i = -4 ; i < NUMBER_OF_3BIT_COMPARATORS + 1 ;i = i + 1) begin
                next_top_buffer_bottom_update_mem[4 + i] <= {MAX_FRAMEWIDTH_IN_8X8{1'b1}};
            end

            //left line buffer

            for (j = 0 ; j < LEFT_BUFFER_MEM_SIZE_1D ; j = j + 1) begin
                for (i = 0 ; i < NUMBER_OF_3BIT_COMPARATORS ;i = i + 1) begin
                    left_buffer_mem[j][i] <= {LOG2_MAX_CTU_SIZE_DIV_8{1'b1}};
                end
            end

            for (i = 0 ; i < NUMBER_OF_3BIT_COMPARATORS ;i = i + 1) begin
                next_left_buffer_right_update_mem[i] <= {LOG2_MAX_CTU_SIZE_DIV_8{1'b1}};
            end

        end
        else begin
            //top line buffer
            for (j = 0 ; j < TOP_BUFFER_MEM_SIZE_1D - 1 ; j = j + 1) begin
                for (i = 0 ; i < NUMBER_OF_3BIT_COMPARATORS ;i = i + 1) begin
                    if(top_mem_update_control_reg[j]) begin
                        if(top_eight_3bit_compare_result[i]) begin
                            if(intra_inter_mode_reg == MODE_INTRA) begin
                                top_buffer_mem[i][j] <= xcu_reg;
                            end
                            else begin
                                top_buffer_mem[i][j] <= {LOG2_FRAME_SIZE-3{1'b1}};
                            end
                        end
                    end
                    else if(current_ctu_done_reg) begin
                        if(i != 0 ) begin
                            top_buffer_mem[i][j] <= {(LOG2_FRAME_SIZE-3){1'b1}};
                        end
                        else begin
                            if (next_left_buffer_right_update_mem[i + 1] > j) begin
                                top_buffer_mem[i][j] <= {(LOG2_FRAME_SIZE-3){1'b1}};
                            end
                            else begin
                                top_buffer_mem[i][j] <= {xcu_reg[LOG2_FRAME_SIZE - 3:3],3'd7};
                            end
                        end
                    end
                end
            end

            //left line buffer
            for (j = 0 ; j < LEFT_BUFFER_MEM_SIZE_1D - 1 ; j = j + 1) begin
                for (i = 0 ; i < NUMBER_OF_3BIT_COMPARATORS ;i = i + 1) begin
                    if(left_mem_update_control_reg[j]) begin                                 //can use top_mem_update_control_reg
                        if(left_eight_3bit_compare_result[i]) begin
                            if(intra_inter_mode_reg == MODE_INTRA) begin
                                left_buffer_mem[j + 1][i] <= {1'b0,ycu_inner_ctu_div_8_reg};
                            end
                            else begin
                                left_buffer_mem[j + 1][i] <= {MAX_CTU_SIZE_DIV_8{1'b1}};
                            end
                        end
                    end
                    else if(current_ctu_done_reg) begin
                        left_buffer_mem[j + 1][i] <= {MAX_CTU_SIZE_DIV_8{1'b1}};
                    end
                end
            end

            for ( i = 0 ; i < NUMBER_OF_3BIT_COMPARATORS ; i = i + 1) begin
                if(current_ctu_done_reg_d) begin
                    if(current_ctu_horlast_done_wire) begin
                        left_buffer_mem[0][i] <= {MAX_CTU_SIZE_DIV_8{1'b1}};
                    end
                    else begin
                        left_buffer_mem[0][i] <= next_left_buffer_right_update_mem[i];
                    end

                end
            end

            // if(top_mem_update_control_reg[TOP_BUFFER_MEM_SIZE_1D - 1]) begin
            //     for (i = -4 ; i < NUMBER_OF_3BIT_COMPARATORS ;i = i + 1) begin
            //         if(lastrow_top_eight_3bit_compare_result[i]) begin
            //             if(intra_inter_mode_reg == MODE_INTRA) begin
            //                 next_top_buffer_bottom_update_mem[4 + i] <= xcu_reg;
            //             end
            //             else begin
            //                 next_top_buffer_bottom_update_mem[4 + i] <= {LOG2_FRAME_SIZE-3{1'b1}};
            //             end
            //         end
            //     end
            // end
            // else if (old_state == STATE_INIT) begin
            //     if(valid_reg) begin

            //         case(log2_ctusize_reg)
            //             3'd4 : begin
            //                 next_top_buffer_bottom_update_mem[4 + -1] <= next_top_buffer_bottom_update_mem[4 + 2];
            //                 next_top_buffer_bottom_update_mem[4 + -2] <= next_top_buffer_bottom_update_mem[4 + 1];
            //             end
            //             3'd5 : begin
            //                 next_top_buffer_bottom_update_mem[4 + -1] <= next_top_buffer_bottom_update_mem[4 + 3];
            //                 next_top_buffer_bottom_update_mem[4 + -2] <= next_top_buffer_bottom_update_mem[4 + 2];
            //                 next_top_buffer_bottom_update_mem[4 + -3] <= next_top_buffer_bottom_update_mem[4 + 1];
            //                 next_top_buffer_bottom_update_mem[4 + -4] <= next_top_buffer_bottom_update_mem[4 + 0];
            //             end
            //             default : begin
            //                 next_top_buffer_bottom_update_mem[4 + -1] <= next_top_buffer_bottom_update_mem[4 + 7];
            //                 next_top_buffer_bottom_update_mem[4 + -2] <= next_top_buffer_bottom_update_mem[4 + 6];
            //                 next_top_buffer_bottom_update_mem[4 + -3] <= next_top_buffer_bottom_update_mem[4 + 5];
            //                 next_top_buffer_bottom_update_mem[4 + -4] <= next_top_buffer_bottom_update_mem[4 + 4];
            //             end
            //         endcase

            //     end
            // end


            case(log2_ctusize_reg)
                3'd4 : begin
                    if(top_mem_update_control_reg[2 - 1]) begin

                        for (i = -2 ; i < 2 + 1 ;i = i + 1) begin
                            if(lastrow_top_eight_3bit_compare_result[i]) begin
                                if(intra_inter_mode_reg == MODE_INTRA) begin
                                    next_top_buffer_bottom_update_mem[4 + i] <= xcu_reg;
                                end
                                else begin
                                    next_top_buffer_bottom_update_mem[4 + i] <= {(LOG2_FRAME_SIZE-3){1'b1}};
                                end
                            end
                        end
                    end
                    else if(old_state == STATE_INIT) begin
                        if(/*valid_reg*/ new_ctu_wire) begin
                            next_top_buffer_bottom_update_mem[4 + 0]  <= next_top_buffer_bottom_update_mem[4 + 3];
                            next_top_buffer_bottom_update_mem[4 + -1] <= next_top_buffer_bottom_update_mem[4 + 2];
                            next_top_buffer_bottom_update_mem[4 + -2] <= next_top_buffer_bottom_update_mem[4 + 1];

                            for ( i = 1 ; i < NUMBER_OF_3BIT_COMPARATORS + 1 ; i = i + 1) begin
                                next_top_buffer_bottom_update_mem[4 + i] <= {(LOG2_FRAME_SIZE-3){1'b1}};
                            end
                        end
                    end

                    if(left_mem_update_control_reg[2 - 1]) begin
                        for(i = 0 ; i < 2 ; i = i + 1) begin
                            if(lastcol_left_eight_3bit_compare_result[i]) begin
                                if(intra_inter_mode_reg == MODE_INTRA) begin
                                    next_left_buffer_right_update_mem[i] <= {1'b0,ycu_inner_ctu_div_8_reg};
                                end
                                else begin
                                    next_left_buffer_right_update_mem[i] <= {(MAX_CTU_SIZE_DIV_8){1'b1}};
                                end
                            end
                        end
                    end
                end
                3'd5 : begin
                    if(top_mem_update_control_reg[4 - 1]) begin
                        for (i = -4 ; i < 4 + 1 ;i = i + 1) begin
                            if(lastrow_top_eight_3bit_compare_result[i]) begin
                                if(intra_inter_mode_reg == MODE_INTRA) begin
                                    next_top_buffer_bottom_update_mem[4 + i] <= xcu_reg;
                                end
                                else begin
                                    next_top_buffer_bottom_update_mem[4 + i] <= {(LOG2_FRAME_SIZE-3){1'b1}};
                                end
                            end
                        end
                    end
                    else if(old_state == STATE_INIT) begin
                        if(/*valid_reg*/new_ctu_wire) begin
                            next_top_buffer_bottom_update_mem[4 + 0]  <= next_top_buffer_bottom_update_mem[4 + 4];
                            next_top_buffer_bottom_update_mem[4 + -1] <= next_top_buffer_bottom_update_mem[4 + 3];
                            next_top_buffer_bottom_update_mem[4 + -2] <= next_top_buffer_bottom_update_mem[4 + 2];
                            next_top_buffer_bottom_update_mem[4 + -3] <= next_top_buffer_bottom_update_mem[4 + 1];
                            next_top_buffer_bottom_update_mem[4 + -4] <= next_top_buffer_bottom_update_mem[4 + 0];

                            for ( i = 1 ; i < NUMBER_OF_3BIT_COMPARATORS + 1 ; i = i + 1) begin
                                next_top_buffer_bottom_update_mem[4 + i] <= {(LOG2_FRAME_SIZE-3){1'b1}};
                            end
                        end
                    end

                    if(left_mem_update_control_reg[4 - 1]) begin
                        for(i = 0 ; i < 4 ; i = i + 1) begin
                            if(lastcol_left_eight_3bit_compare_result[i]) begin
                                if(intra_inter_mode_reg == MODE_INTRA) begin
                                    next_left_buffer_right_update_mem[i] <= {1'b0,ycu_inner_ctu_div_8_reg};
                                end
                                else begin
                                    next_left_buffer_right_update_mem[i] <= {(MAX_CTU_SIZE_DIV_8){1'b1}};
                                end
                            end
                        end
                    end
                end
                default : begin
                    if(top_mem_update_control_reg[8 - 1]) begin
                        for (i = -4 ; i < 8 + 1 ;i = i + 1) begin
                            if(lastrow_top_eight_3bit_compare_result[i]) begin
                                if(intra_inter_mode_reg == MODE_INTRA) begin
                                    next_top_buffer_bottom_update_mem[4 + i] <= xcu_reg;
                                end
                                else begin
                                    next_top_buffer_bottom_update_mem[4 + i] <= {(LOG2_FRAME_SIZE-3){1'b1}};
                                end
                            end
                        end
                    end
                    else if(old_state == STATE_INIT) begin
                        if(/*valid_reg*/new_ctu_wire) begin
                            next_top_buffer_bottom_update_mem[4 + 0]  <= next_top_buffer_bottom_update_mem[4 + 8];
                            next_top_buffer_bottom_update_mem[4 + -1] <= next_top_buffer_bottom_update_mem[4 + 7];
                            next_top_buffer_bottom_update_mem[4 + -2] <= next_top_buffer_bottom_update_mem[4 + 6];
                            next_top_buffer_bottom_update_mem[4 + -3] <= next_top_buffer_bottom_update_mem[4 + 5];
                            next_top_buffer_bottom_update_mem[4 + -4] <= next_top_buffer_bottom_update_mem[4 + 4];

                            for ( i = 1 ; i < NUMBER_OF_3BIT_COMPARATORS + 1 ; i = i + 1) begin
                                next_top_buffer_bottom_update_mem[4 + i] <= {(LOG2_FRAME_SIZE-3){1'b1}};
                            end
                        end
                    end

                    if(left_mem_update_control_reg[8 - 1]) begin
                        for(i = 0 ; i < 8 ; i = i + 1) begin
                            if(lastcol_left_eight_3bit_compare_result[i]) begin
                                if(intra_inter_mode_reg == MODE_INTRA) begin
                                    next_left_buffer_right_update_mem[i] <= {1'b0,ycu_inner_ctu_div_8_reg};
                                end
                                else begin
                                    next_left_buffer_right_update_mem[i] <= {(MAX_CTU_SIZE_DIV_8){1'b1}};
                                end
                            end
                        end
                    end


                end
            endcase


        end
                 //[x][y]
    end

    always @(posedge clk) begin : delay_signals
        old_state <= state;
        valid_reg <= valid_4by4_in;//valid_in;
        valid_reg_d <= valid_reg;
        valid_reg_dd <= valid_reg_d;

        current_ctu_done_reg_d <= current_ctu_done_reg;

        valid_reg_8by8_d <= valid_in;
    end


    always @(posedge clk) begin : main_fsm
        integer i;
        if(reset) begin
            state <= STATE_INIT;

            top_mem_update_control_reg <= {TOP_BUFFER_MEM_SIZE_1D{1'b0}};
            left_mem_update_control_reg <= {LEFT_BUFFER_MEM_SIZE_1D{1'b0}};

            xcu_reg         <= {LOG2_FRAME_SIZE{1'b0}};
            ycu_reg         <= {LOG2_FRAME_SIZE{1'b0}};
            ntbs_div_8_reg  <= {(MAX_NTBS_SIZE - 3){1'b0}};
            intra_inter_mode_reg <= 1'b0;
            topmem_ctu_update_addr_offset <= 4'd0;

            porta_wen_reg <= 1'b0;
        end
        else if(enable) begin
            case(state)
                STATE_INIT : begin
                    if(valid_in) begin
                        top_mem_update_control_reg[ycu_in[3 - 1:0]] <= 1'b1;
                        left_mem_update_control_reg[xcu_in[3 - 1:0]] <= 1'b1;
                        // if(ntbs_in[2] == 1) begin
                        //     top_mem_update_control_reg[ycu_inner_ctu_div_8_reg] <= 1'b1;
                        // end
                        // else if (ntbs_in[3] == 1) begin
                        //     top_mem_update_control_reg[ycu_inner_ctu_div_8_reg] <= 1'b1;
                        // end
                        // else if (ntbs_in[4] == 1) begin
                        //     top_mem_update_control_reg[ycu_inner_ctu_div_8_reg] <= 1'b1;
                        //     top_mem_update_control_reg[ycu_inner_ctu_div_8_reg+1] <= 1'b1;
                        // end
                        // else if (ntbs_in[5] == 1) begin
                        //     top_mem_update_control_reg[ycu_inner_ctu_div_8_reg] <= 1'b1;
                        //     top_mem_update_control_reg[ycu_inner_ctu_div_8_reg+1] <= 1'b1;
                        //     top_mem_update_control_reg[ycu_inner_ctu_div_8_reg+2] <= 1'b1;
                        //     top_mem_update_control_reg[ycu_inner_ctu_div_8_reg+3] <= 1'b1;
                        // end

                        if((ntbs_in == 4%(1 << MAX_NTBS_SIZE)) || (ntbs_in == 8%(1 << MAX_NTBS_SIZE))) begin
                            state <= STATE_DONE;
                            top_mem_update_control_offset_reg <= ycu_in[3 - 1:0];
                            left_mem_update_control_offset_reg <= xcu_in[3 - 1:0];
                        end
                        else begin
                            top_mem_update_control_offset_reg <= ycu_in[3 - 1:0];
                            left_mem_update_control_offset_reg <= xcu_in[3 - 1:0];
                            state <= STATE_READ;
                        end

                        xcu_reg                 <= xcu_in;
                        ycu_reg                 <= ycu_in;
                        // ntbs_div_8_reg          <= ntbs_in[MAX_NTBS_SIZE - 1:3] - 1'b1;
                        intra_inter_mode_reg    <= intra_inter_mode_in;

                        if(ntbs_in == 4%(1 << MAX_NTBS_SIZE)) begin
                            ntbs_div_8_reg      <= 0%(1 << MAX_NTBS_SIZE);
                        end
                        else begin
                            ntbs_div_8_reg      <= ntbs_in[MAX_NTBS_SIZE - 1:3] - 1'b1;
                        end



                        porta_wen_reg   <= 1'b0;
                        porta_addr_reg  <= xcu_in;
                    end
                end
                STATE_READ : begin
                    if(ntbs_div_8_reg != 3'd0) begin
                        top_mem_update_control_reg[top_mem_update_control_offset_reg]       <= 1'b0;
                        top_mem_update_control_reg[top_mem_update_control_offset_reg + 1]   <= 1'b1;
                        left_mem_update_control_reg[left_mem_update_control_offset_reg]     <= 1'b0;
                        left_mem_update_control_reg[left_mem_update_control_offset_reg + 1] <= 1'b1;


                        top_mem_update_control_offset_reg                                   <= top_mem_update_control_offset_reg + 1'b1;
                        left_mem_update_control_offset_reg                                  <= left_mem_update_control_offset_reg + 1'b1;
                        ntbs_div_8_reg                                                      <= ntbs_div_8_reg - 1'b1;
                    end
                    else begin
                        top_mem_update_control_reg[top_mem_update_control_offset_reg]       <= 1'b0;
                        left_mem_update_control_reg[left_mem_update_control_offset_reg]     <= 1'b0;

                        if(current_ctu_done_reg) begin
                            if(current_ctu_horlast_done_reg) begin
                                case(log2_ctusize_reg)
                                    3'd4 : begin
                                        state <= STATE_UPDATE_TOPMEM_CTU16_LAST;
                                    end
                                    3'd5 : begin
                                        state <= STATE_UPDATE_TOPMEM_CTU32_LAST;
                                    end
                                    default : begin
                                        state <= STATE_UPDATE_TOPMEM_CTU64_LAST;
                                    end
                                endcase
                            end
                            else begin
                                case(log2_ctusize_reg)
                                    3'd4 : begin
                                        state <= STATE_UPDATE_TOPMEM_CTU16;
                                    end
                                    3'd5 : begin
                                        state <= STATE_UPDATE_TOPMEM_CTU32;
                                    end
                                    default : begin
                                        state <= STATE_UPDATE_TOPMEM_CTU64;
                                    end
                                endcase
                            end

                            topmem_ctu_update_addr_offset <= 4'd0;
                        end
                        else begin
                            state <= STATE_DONE;
                        end

                        //if(xcu_reg[LOG2_FRAME_SIZE])

                    end
                end
                STATE_DONE : begin

                    // note : for 8by8 tu s at the end of the frame
                        if(current_ctu_done_wire) begin
                            if(current_ctu_horlast_done_wire) begin
                                case(log2_ctusize_reg)
                                    3'd4 : begin
                                        porta_addr_reg                  <= {xcu_reg[LOG2_FRAME_SIZE - 3:3],3'd0} -3'd2 +  topmem_ctu_update_addr_offset;
                                        state <= STATE_UPDATE_TOPMEM_CTU16_LAST;
                                    end
                                    3'd5 : begin
                                        porta_addr_reg                  <= {xcu_reg[LOG2_FRAME_SIZE - 3:3],3'd0} -3'd4 +  topmem_ctu_update_addr_offset;
                                        state <= STATE_UPDATE_TOPMEM_CTU32_LAST;
                                    end
                                    default : begin
                                        porta_addr_reg                  <= {xcu_reg[LOG2_FRAME_SIZE - 3:3],3'd0} -3'd4 +  topmem_ctu_update_addr_offset;
                                        state                           <= STATE_UPDATE_TOPMEM_CTU64_LAST;
                                    end
                                endcase
                            end
                            else begin
                                case(log2_ctusize_reg)
                                    3'd4 : begin
                                        state <= STATE_UPDATE_TOPMEM_CTU16;
                                    end
                                    3'd5 : begin
                                        state <= STATE_UPDATE_TOPMEM_CTU32;
                                    end
                                    default : begin
                                        state <= STATE_UPDATE_TOPMEM_CTU64;
                                    end
                                endcase
                            end

                            topmem_ctu_update_addr_offset <= 4'd0;
                        end
                        else begin
                            state <= STATE_INIT;
                        end
                    // end note

                    top_mem_update_control_reg[top_mem_update_control_offset_reg]           <= 1'b0;
                    left_mem_update_control_reg[left_mem_update_control_offset_reg]         <= 1'b0;
                    //state <= STATE_INIT;
                end
                STATE_UPDATE_TOPMEM_CTU64 : begin
                    if(topmem_ctu_update_addr_offset == 4'd8) begin
                        topmem_ctu_update_addr_offset   <= 4'd0;
                        porta_wen_reg                   <= 1'b0;
                        state                           <= STATE_INIT;
                    end
                    else begin
                        porta_wen_reg                   <= 1'b1;
                        porta_wdata_reg                 <= next_top_buffer_bottom_update_mem[4 + 3 - topmem_ctu_update_addr_offset];
                        porta_addr_reg                  <= {xcu_reg[LOG2_FRAME_SIZE - 3:3],3'd3} - topmem_ctu_update_addr_offset;
                        topmem_ctu_update_addr_offset   <= topmem_ctu_update_addr_offset + 1'b1;
                    end
                end
                STATE_UPDATE_TOPMEM_CTU64_LAST : begin
                    if(porta_addr_reg == frame_width_reg[LOG2_FRAME_SIZE - 1:3]) begin
                        topmem_ctu_update_addr_offset   <= 4'd0;
                        porta_wen_reg                   <= 1'b0;
                        state                           <= STATE_INIT;
                    end
                    else begin
                        porta_wen_reg                   <= 1'b1;
                        porta_wdata_reg                 <= next_top_buffer_bottom_update_mem[4 + -4 + topmem_ctu_update_addr_offset];
                        porta_addr_reg                  <= {xcu_reg[LOG2_FRAME_SIZE - 3:3],3'd0} -3'd4 +  topmem_ctu_update_addr_offset;
                        topmem_ctu_update_addr_offset   <= topmem_ctu_update_addr_offset + 1'b1;
                    end
                end
                STATE_UPDATE_TOPMEM_CTU32 : begin
                    if(topmem_ctu_update_addr_offset == 4'd4) begin
                        topmem_ctu_update_addr_offset   <= 4'd0;
                        porta_wen_reg                   <= 1'b0;
                        state                           <= STATE_INIT;
                    end
                    else begin
                        porta_wen_reg                   <= 1'b1;
                        porta_wdata_reg                 <= next_top_buffer_bottom_update_mem[4 + 0 - topmem_ctu_update_addr_offset];
                        porta_addr_reg                  <= {xcu_reg[LOG2_FRAME_SIZE - 3:3],3'd0} - topmem_ctu_update_addr_offset;
                        topmem_ctu_update_addr_offset   <= topmem_ctu_update_addr_offset + 1'b1;
                    end
                end
                STATE_UPDATE_TOPMEM_CTU32_LAST : begin
                    if(porta_addr_reg == frame_width_reg[LOG2_FRAME_SIZE - 1:3]) begin
                        topmem_ctu_update_addr_offset   <= 4'd0;
                        porta_wen_reg                   <= 1'b0;
                        state                           <= STATE_INIT;
                    end
                    else begin
                        porta_wen_reg                   <= 1'b1;
                        porta_wdata_reg                 <= next_top_buffer_bottom_update_mem[4 + -4 + topmem_ctu_update_addr_offset];
                        porta_addr_reg                  <= {xcu_reg[LOG2_FRAME_SIZE - 3:3],3'd0} -3'd4 +  topmem_ctu_update_addr_offset;
                        topmem_ctu_update_addr_offset   <= topmem_ctu_update_addr_offset + 1'b1;
                    end
                end
                STATE_UPDATE_TOPMEM_CTU16 : begin
                    if(topmem_ctu_update_addr_offset == 4'd2 ) begin
                        topmem_ctu_update_addr_offset   <= 4'd0;
                        porta_wen_reg                   <= 1'b0;
                        state                           <= STATE_INIT;
                    end
                    else begin
                        porta_wen_reg                   <= 1'b1;
                        porta_wdata_reg                 <= next_top_buffer_bottom_update_mem[4 + 0 - topmem_ctu_update_addr_offset];
                        porta_addr_reg                  <= {xcu_reg[LOG2_FRAME_SIZE - 3:3],3'd0} - topmem_ctu_update_addr_offset;
                        topmem_ctu_update_addr_offset   <= topmem_ctu_update_addr_offset + 1'b1;
                    end
                end
                STATE_UPDATE_TOPMEM_CTU16_LAST : begin
                    if(porta_addr_reg == frame_width_reg[LOG2_FRAME_SIZE - 1:3]) begin
                        topmem_ctu_update_addr_offset   <= 4'd0;
                        porta_wen_reg                   <= 1'b0;
                        state                           <= STATE_INIT;
                    end
                    else begin
                        porta_wen_reg                   <= 1'b1;
                        porta_wdata_reg                 <= next_top_buffer_bottom_update_mem[4 + -2 + topmem_ctu_update_addr_offset];
                        porta_addr_reg                  <= {xcu_reg[LOG2_FRAME_SIZE - 3:3],3'd0} -3'd2 +  topmem_ctu_update_addr_offset;
                        topmem_ctu_update_addr_offset   <= topmem_ctu_update_addr_offset + 1'b1;
                    end
                end
            endcase
        end
    end

    always @(posedge clk) begin : resetting_the_intra_min_range_mem
        if(reset) begin
            reset_state <= RESET_STATE_INIT;
            portb_wen_reg <= 1'b0;
        end
        else if(enable) begin
            case(reset_state)
                RESET_STATE_INIT : begin
                    if(new_frame_in) begin
                        // if((xcu_in == {(LOG2_FRAME_SIZE-3){1'b0}}) && (ycu_in == {(LOG2_FRAME_SIZE-3){1'b0}})) begin
                            portb_addr_reg <= {(LOG2_FRAME_SIZE - 3){1'b0}};
                            portb_wen_reg  <= 1'b1;
                            reset_state    <= RESET_STATE_ACTIVE;
                        // end
                    end
                end
                RESET_STATE_ACTIVE : begin
                    if(portb_addr_reg == frame_width_reg[LOG2_FRAME_SIZE - 1 : 3]) begin
                        portb_wen_reg      <= 1'b0;
                        reset_state        <= RESET_STATE_INIT;
                    end
                    else begin
                        portb_addr_reg <= portb_addr_reg + 1'b1;
                        portb_wen_reg  <= 1'b1;
                    end
                end
            endcase
        end
    end

    assign portb_wdata_wire = {(LOG2_FRAME_SIZE - 3){1'b1}};

    always @(posedge clk) begin
        constrained_top_range_reg <= constrained_top_range_wire;
        constrained_left_range_reg <= constrained_left_range_wire;

        if(valid_reg_d || valid_reg_dd) begin
            if(use_new_constrained_top_range_reg == 1'b0) begin
                old_constrained_top_range_reg <= constrained_top_range_reg;
                old_constrained_left_range_reg <= constrained_left_range_reg;
            end
        end

    end

    always @(*) begin
        if(constrained_intra_pred_flag_reg) begin
            // case(old_state)
                // STATE_READ,STATE_DONE : begin
                    if(valid_reg_dd || valid_reg_d) begin
                        if(use_new_constrained_top_range_reg) begin
                            constrained_top_range_wire = new_constrained_top_range_reg;
                            constrained_left_range_wire = new_constrained_left_range_reg;
                        end
                        else begin
                            if(first_cu_row_reg) begin
                                constrained_top_range_wire = {porta_rdata_wire,3'd0};
                            end
                            else begin
                                constrained_top_range_wire = {top_buffer_mem[xcu_3lsb_wire][ycu_inner_ctu_div_8_reg_minus_one_wire],3'd0};   //[x][y]
                            end

                            // if(first_cu_col_reg) begin
                            //     constrained_left_range_wire = {next_left_buffer_right_update_mem[ycu_inner_ctu_div_8_reg],3'd0};
                            // end
                            // else begin
                                if(left_buffer_mem[xcu_inner_ctu_div_8_reg][ycu_inner_ctu_div_8_reg] == 4'b1111) begin
                                    if(xcu_reg > constrained_top_range_wire[LOG2_FRAME_SIZE - 1:3]) begin
                                        constrained_left_range_wire = {LOG2_FRAME_SIZE{1'b0}};
                                    end
                                    else begin
                                        constrained_left_range_wire = {left_buffer_mem[xcu_inner_ctu_div_8_reg][ycu_inner_ctu_div_8_reg],3'd0};
                                    end
                                end
                                else begin
                                    constrained_left_range_wire = {left_buffer_mem[xcu_inner_ctu_div_8_reg][ycu_inner_ctu_div_8_reg],3'd0};
                                end
                            //constrained_left_range_wire = {left_buffer_mem[xcu_inner_ctu_div_8_reg][ycu_inner_ctu_div_8_reg],3'd0};
                        // end
                        end
                    end
                    else begin
                        constrained_top_range_wire = constrained_top_range_reg;
                        constrained_left_range_wire = constrained_left_range_reg;
                    end
                // end
                // default : begin
                    //constrained_top_range_wire = constrained_top_range_reg;
                    //constrained_left_range_wire = constrained_left_range_reg;
                // end
            // endcase
        end
        else begin
            constrained_top_range_wire  =  {(LOG2_FRAME_SIZE){1'b0}};
            constrained_left_range_wire =  {(LOG2_FRAME_SIZE){1'b0}};
        end
    end

    always @(posedge clk) begin
        if(config_valid_in) begin
            log2_ctusize_reg                <= log2_ctusize_in;
            frame_width_reg                 <= frame_width_in - 1'b1;
            frame_height_reg                <= frame_height_in - 1'b1;
            constrained_intra_pred_flag_reg <= constrained_intra_pred_flag_in;
        end
    end

    always @(*) begin
        case(log2_ctusize_reg)
            3'd4 : begin
                if(ycu_reg[1 - 1:0] == 1'd0) begin
                    first_cu_row_reg = 1'b1;
                end
                else begin
                    first_cu_row_reg = 1'b0;
                end

                if(xcu_reg[1 - 1:0] == 1'd0) begin
                    first_cu_col_reg = 1'b1;
                end
                else begin
                    first_cu_col_reg = 1'b0;
                end
            end
            3'd5 : begin
                if(ycu_reg[2 - 1:0] == 2'd0) begin
                    first_cu_row_reg = 1'b1;
                end
                else begin
                    first_cu_row_reg = 1'b0;
                end

                if(xcu_reg[2 - 1:0] == 1'd0) begin
                    first_cu_col_reg = 1'b1;
                end
                else begin
                    first_cu_col_reg = 1'b0;
                end
            end
            default : begin   //3'd6 included
                if(ycu_reg[3 - 1:0] == 3'd0) begin
                    first_cu_row_reg = 1'b1;
                end
                else begin
                    first_cu_row_reg = 1'b0;
                end

                if(xcu_reg[3 - 1:0] == 1'd0) begin
                    first_cu_col_reg = 1'b1;
                end
                else begin
                    first_cu_col_reg = 1'b0;
                end
            end
        endcase
    end

    always @(*) begin
        case(state)
            STATE_INIT : begin
                ready_out = 1'b1;
            end
            default : begin
                ready_out = 1'b0;
            end
        endcase
    end

    always @(posedge clk) begin
        if(reset) begin
            use_new_constrained_top_range_reg <= 1'b0;
        end
        else begin
            if(valid_4by4_in) begin
                case({x_second_bit_in,y_second_bit_in})
                    2'b10 : begin
                        if (old_constrained_top_range_reg[LOG2_FRAME_SIZE - 1:3] > xcu_reg) begin
                            new_constrained_left_range_reg <= {ycu_inner_ctu_div_8_reg,3'd0};
                        end
                        else begin
                            if(ycu_inner_ctu_div_8_reg == {(3){1'b0}}) begin
                                new_constrained_left_range_reg <= {ycu_inner_ctu_div_8_reg,3'd0};
                            end
                            else begin
                                new_constrained_left_range_reg <= {(ycu_inner_ctu_div_8_reg - 1'b1),3'd0};
                            end
                        end
                        new_constrained_top_range_reg <= old_constrained_top_range_reg;

                        use_new_constrained_top_range_reg <= 1'b1;
                    end
                    2'b01 : begin
                        if (old_constrained_left_range_reg[LOG2_FRAME_SIZE - 1:3] > ycu_reg) begin
                            new_constrained_top_range_reg <= {xcu_reg,3'd0};
                        end
                        else begin
                            if(xcu_reg == {(LOG2_FRAME_SIZE - 3){1'b0}}) begin
                                new_constrained_top_range_reg <= {xcu_reg,3'd0};
                            end
                            else begin
                                new_constrained_top_range_reg <= {(xcu_reg - 1'b1),3'd0};
                            end
                        end
                        new_constrained_left_range_reg <= old_constrained_left_range_reg;

                        use_new_constrained_top_range_reg <= 1'b1;
                    end
                    2'b11 : begin
                        new_constrained_top_range_reg <= {xcu_reg,3'd0};
                        new_constrained_left_range_reg <= {ycu_inner_ctu_div_8_reg,3'd0};
                        use_new_constrained_top_range_reg <= 1'b1;
                    end
                    2'b00 : begin
                        use_new_constrained_top_range_reg <= 1'b0;
                    end
                endcase
            end
        end

    end

    // always @(posedge clk) begin
    //     begin
    //         if (porta_wen_in) begin
    //             mem[porta_addr_in] <= porta_wdata_in;
    //         end
    //         porta_rdata_out <= mem[porta_addr_in];
    //     end
    // end

    // always @(posedge clk) begin
    //     begin
    //         if (portb_wen_in) begin
    //             mem[portb_addr_in] <= portb_wdata_in;
    //         end
    //         portb_rdata_out <= mem[portb_addr_in];
    //     end
    // end


endmodule
