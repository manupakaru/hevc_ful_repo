 `timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:42:50 10/16/2013 
// Design Name: 
// Module Name:    intra_angular_aux_addr_gen 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module intra_angular_aux_addr_gen(
    
    //ntbs_in,
    abs_intrapredangle_in,
   
    read_neg_addr_index_in,
    
    aux_read_addr_offset_out
    );
    
   `include "../sim/pred_def.v"
//----------------------------------------------------
//LOCALPARAMS
//----------------------------------------------------

    //localparam  MAX_LOG2_NTBS_WIDTH     = 3;
    localparam  INTRA_PRED_ANGLE_WIDTH  = 6;
    // localparam  MAX_NTBS_SIZE           = 6;



//----------------------------------------------------
// I/O
//----------------------------------------------------
    
    
    //input [MAX_LOG2_NTBS_WIDTH - 1:0]       ntbs_in;
    input [INTRA_PRED_ANGLE_WIDTH - 1:0]    abs_intrapredangle_in;  
    input [MAX_NTBS_SIZE - 1:0]             read_neg_addr_index_in;
    output reg [MAX_NTBS_SIZE - 1:0]        aux_read_addr_offset_out;
    
    
    always @(*) begin
        case(abs_intrapredangle_in)
            6'd2 : begin
                case(read_neg_addr_index_in)
                    6'd0 : aux_read_addr_offset_out = 6'd0; // left buffer has -1,-1 element its in the offset 0 position
                    6'd1 : aux_read_addr_offset_out = 6'd16;
                    6'd2 : aux_read_addr_offset_out = 6'd32;
                    default : aux_read_addr_offset_out = 6'd0;
                endcase
            end
            6'd5 : begin
                case(read_neg_addr_index_in)
                    6'd0 : aux_read_addr_offset_out = 6'd0; // left buffer has -1,-1 element its in the offset 0 position
                    6'd1 : aux_read_addr_offset_out = 6'd6;
                    6'd2 : aux_read_addr_offset_out = 6'd13;
                    6'd3 : aux_read_addr_offset_out = 6'd19;
                    6'd4 : aux_read_addr_offset_out = 6'd26;
                    6'd5 : aux_read_addr_offset_out = 6'd32;
                    default : aux_read_addr_offset_out = 6'd0;
                endcase
            end
            6'd9 : begin
                case(read_neg_addr_index_in)
                    6'd0 : aux_read_addr_offset_out = 6'd0; // left buffer has -1,-1 element its in the offset 0 position
                    6'd1 : aux_read_addr_offset_out = 6'd4;
                    6'd2 : aux_read_addr_offset_out = 6'd7;
                    6'd3 : aux_read_addr_offset_out = 6'd11;
                    6'd4 : aux_read_addr_offset_out = 6'd14;
                    6'd5 : aux_read_addr_offset_out = 6'd18;
                    6'd6 : aux_read_addr_offset_out = 6'd21;
                    6'd7 : aux_read_addr_offset_out = 6'd25;
                    6'd8 : aux_read_addr_offset_out = 6'd28;
                    6'd9 : aux_read_addr_offset_out = 6'd32;
                    default : aux_read_addr_offset_out = 6'd0;
                endcase
            end
            6'd13 : begin
                case(read_neg_addr_index_in)
                    6'd0 : aux_read_addr_offset_out = 6'd0; // left buffer has -1,-1 element its in the offset 0 position
                    6'd1 : aux_read_addr_offset_out = 6'd2;
                    6'd2 : aux_read_addr_offset_out = 6'd5;
                    6'd3 : aux_read_addr_offset_out = 6'd7;
                    6'd4 : aux_read_addr_offset_out = 6'd10;
                    6'd5 : aux_read_addr_offset_out = 6'd12;
                    6'd6 : aux_read_addr_offset_out = 6'd15;
                    6'd7 : aux_read_addr_offset_out = 6'd17;
                    6'd8 : aux_read_addr_offset_out = 6'd20;
                    6'd9 : aux_read_addr_offset_out = 6'd22;
                    6'd10 : aux_read_addr_offset_out = 6'd25;
                    6'd11 : aux_read_addr_offset_out = 6'd27;
                    6'd12 : aux_read_addr_offset_out = 6'd30;
                    6'd13 : aux_read_addr_offset_out = 6'd32;
                    default : aux_read_addr_offset_out = 6'd0;
                endcase
            end
            6'd17 : begin
                case(read_neg_addr_index_in)
                    6'd0 : aux_read_addr_offset_out = 6'd0; // left buffer has -1,-1 element its in the offset 0 position
                    6'd1 : aux_read_addr_offset_out = 6'd2;
                    6'd2 : aux_read_addr_offset_out = 6'd4;
                    6'd3 : aux_read_addr_offset_out = 6'd6;
                    6'd4 : aux_read_addr_offset_out = 6'd8;
                    6'd5 : aux_read_addr_offset_out = 6'd9;
                    6'd6 : aux_read_addr_offset_out = 6'd11;
                    6'd7 : aux_read_addr_offset_out = 6'd13;
                    6'd8 : aux_read_addr_offset_out = 6'd15;
                    6'd9 : aux_read_addr_offset_out = 6'd17;
                    6'd10 : aux_read_addr_offset_out = 6'd19;
                    6'd11 : aux_read_addr_offset_out = 6'd21;
                    6'd12 : aux_read_addr_offset_out = 6'd23;
                    6'd13 : aux_read_addr_offset_out = 6'd24;
                    6'd14 : aux_read_addr_offset_out = 6'd26;
                    6'd15 : aux_read_addr_offset_out = 6'd28;
                    6'd16 : aux_read_addr_offset_out = 6'd30;
                    6'd17 : aux_read_addr_offset_out = 6'd32;
                    default : aux_read_addr_offset_out = 6'd0;
                endcase
            
            end
            6'd21 : begin
                case(read_neg_addr_index_in)
                    6'd0 : aux_read_addr_offset_out = 6'd0; // left buffer has -1,-1 element its in the offset 0 position
                    6'd1 : aux_read_addr_offset_out = 6'd2;
                    6'd2 : aux_read_addr_offset_out = 6'd3;
                    6'd3 : aux_read_addr_offset_out = 6'd5;
                    6'd4 : aux_read_addr_offset_out = 6'd6;
                    6'd5 : aux_read_addr_offset_out = 6'd8;
                    6'd6 : aux_read_addr_offset_out = 6'd9;
                    6'd7 : aux_read_addr_offset_out = 6'd11;
                    6'd8 : aux_read_addr_offset_out = 6'd12;
                    6'd9 : aux_read_addr_offset_out = 6'd14;
                    6'd10 : aux_read_addr_offset_out = 6'd15;
                    6'd11 : aux_read_addr_offset_out = 6'd17;
                    6'd12 : aux_read_addr_offset_out = 6'd18;
                    6'd13 : aux_read_addr_offset_out = 6'd20;
                    6'd14 : aux_read_addr_offset_out = 6'd21;
                    6'd15 : aux_read_addr_offset_out = 6'd23;
                    6'd16 : aux_read_addr_offset_out = 6'd24;
                    6'd17 : aux_read_addr_offset_out = 6'd26;
                    6'd18 : aux_read_addr_offset_out = 6'd27;
                    6'd19 : aux_read_addr_offset_out = 6'd29;
                    6'd20 : aux_read_addr_offset_out = 6'd30;
                    6'd21 : aux_read_addr_offset_out = 6'd32;
                    default : aux_read_addr_offset_out = 6'd0;
                endcase
            
            end
            6'd26 : begin
                case(read_neg_addr_index_in)
                    6'd0 : aux_read_addr_offset_out = 6'd0; // left buffer has -1,-1 element its in the offset 0 position
                    6'd1 : aux_read_addr_offset_out = 6'd1;
                    6'd2 : aux_read_addr_offset_out = 6'd2;
                    6'd3 : aux_read_addr_offset_out = 6'd4;
                    6'd4 : aux_read_addr_offset_out = 6'd5;
                    6'd5 : aux_read_addr_offset_out = 6'd6;
                    6'd6 : aux_read_addr_offset_out = 6'd7;
                    6'd7 : aux_read_addr_offset_out = 6'd9;
                    6'd8 : aux_read_addr_offset_out = 6'd10;
                    6'd9 : aux_read_addr_offset_out = 6'd11;
                    6'd10 : aux_read_addr_offset_out = 6'd12;
                    6'd11 : aux_read_addr_offset_out = 6'd14;
                    6'd12 : aux_read_addr_offset_out = 6'd15;
                    6'd13 : aux_read_addr_offset_out = 6'd16;
                    6'd14 : aux_read_addr_offset_out = 6'd17;
                    6'd15 : aux_read_addr_offset_out = 6'd18;
                    6'd16 : aux_read_addr_offset_out = 6'd20;
                    6'd17 : aux_read_addr_offset_out = 6'd21;
                    6'd18 : aux_read_addr_offset_out = 6'd22;
                    6'd19 : aux_read_addr_offset_out = 6'd23;
                    6'd20 : aux_read_addr_offset_out = 6'd25;
                    6'd21 : aux_read_addr_offset_out = 6'd26;
                    6'd22 : aux_read_addr_offset_out = 6'd27;
                    6'd23 : aux_read_addr_offset_out = 6'd28;
                    6'd24 : aux_read_addr_offset_out = 6'd30;
                    6'd25 : aux_read_addr_offset_out = 6'd31;
                    6'd26 : aux_read_addr_offset_out = 6'd32;
                    default : aux_read_addr_offset_out = 6'd0;
                endcase
            end
            6'd32 : begin
                case(read_neg_addr_index_in)
                    6'd0 : aux_read_addr_offset_out = 6'd0; // left buffer has -1,-1 element its in the offset 0 position
                    6'd1 : aux_read_addr_offset_out = 6'd1;
                    6'd2 : aux_read_addr_offset_out = 6'd2;
                    6'd3 : aux_read_addr_offset_out = 6'd3;
                    6'd4 : aux_read_addr_offset_out = 6'd4;
                    6'd5 : aux_read_addr_offset_out = 6'd5;
                    6'd6 : aux_read_addr_offset_out = 6'd6;
                    6'd7 : aux_read_addr_offset_out = 6'd7;
                    6'd8 : aux_read_addr_offset_out = 6'd8;
                    6'd9 : aux_read_addr_offset_out = 6'd9;
                    6'd10 : aux_read_addr_offset_out = 6'd10;
                    6'd11 : aux_read_addr_offset_out = 6'd11;
                    6'd12 : aux_read_addr_offset_out = 6'd12;
                    6'd13 : aux_read_addr_offset_out = 6'd13;
                    6'd14 : aux_read_addr_offset_out = 6'd14;
                    6'd15 : aux_read_addr_offset_out = 6'd15;
                    6'd16 : aux_read_addr_offset_out = 6'd16;
                    6'd17 : aux_read_addr_offset_out = 6'd17;
                    6'd18 : aux_read_addr_offset_out = 6'd18;
                    6'd19 : aux_read_addr_offset_out = 6'd19;
                    6'd20 : aux_read_addr_offset_out = 6'd20;
                    6'd21 : aux_read_addr_offset_out = 6'd21;
                    6'd22 : aux_read_addr_offset_out = 6'd22;
                    6'd23 : aux_read_addr_offset_out = 6'd23;
                    6'd24 : aux_read_addr_offset_out = 6'd24;
                    6'd25 : aux_read_addr_offset_out = 6'd25;
                    6'd26 : aux_read_addr_offset_out = 6'd26;
                    6'd27 : aux_read_addr_offset_out = 6'd27;
                    6'd28 : aux_read_addr_offset_out = 6'd28;
                    6'd29 : aux_read_addr_offset_out = 6'd29;
                    6'd30 : aux_read_addr_offset_out = 6'd30;
                    6'd31 : aux_read_addr_offset_out = 6'd31;
                    6'd32 : aux_read_addr_offset_out = 6'd32;
                    default : aux_read_addr_offset_out = 6'd0;
                endcase
            end
            default : begin
                aux_read_addr_offset_out = 6'd0;
            end
        endcase
    end
endmodule
