module reference_pixel_valid_range_mem
    (
        curr_xin,
        curr_yin,
        
        valid_range_top_out,
        valid_range_left_out
    );
    
//-------------------------------------------------------------
//      Parameters
//-------------------------------------------------------------
    
    parameter            VALID_RANGE_WIDTH                      = 6;
    
    localparam           INNER_CTB_PIXEL_LOC_LENGTH             = 6;
    localparam           MAX_CTB_SIZE                           = 64;
    
//-------------------------------------------------------------
//      I/O
//-------------------------------------------------------------
    
    input       [INNER_CTB_PIXEL_LOC_LENGTH - 1:0]      curr_xin;
    input       [INNER_CTB_PIXEL_LOC_LENGTH - 1:0]      curr_yin;
    output reg  [VALID_RANGE_WIDTH - 1:0]               valid_range_top_out;
    output reg  [VALID_RANGE_WIDTH - 1:0]               valid_range_left_out;
    
//------------------------------------------------------------
//      Internal Reg and Wire
//------------------------------------------------------------
     
//    reg [VALID_RANGE_WIDTH - 1:0]       valid_range_mem_top [MAX_CTB_SIZE*MAX_CTB_SIZE/2 - 1:0];
//    reg [VALID_RANGE_WIDTH - 1:0]       valid_range_mem_left [MAX_CTB_SIZE*MAX_CTB_SIZE - 1:0];
    
    wire      [INNER_CTB_PIXEL_LOC_LENGTH - 1:0]         curr_yin_minus_2;
    wire      [INNER_CTB_PIXEL_LOC_LENGTH - 1:0]         curr_yin_minus_4;
    wire      [INNER_CTB_PIXEL_LOC_LENGTH - 1:0]         curr_xin_minus_2;
    wire      [INNER_CTB_PIXEL_LOC_LENGTH - 1:0]         curr_xin_minus_4;
    
//------------------------------------------------------------
//      Implementation
//------------------------------------------------------------    
    
    //initial begin
    //    $readmemh("valid_range_mem_top.mem", valid_range_mem_top);
    //    $readmemh("valid_range_mem_left.mem", valid_range_mem_left);
    //end
    
    assign curr_yin_minus_2 = curr_yin - 2'd2;
    assign curr_yin_minus_4 = curr_yin - 3'd4;
    assign curr_xin_minus_2 = curr_xin - 2'd2;
    assign curr_xin_minus_4 = curr_xin - 3'd4;
    
    always @(*) begin
    
        if(curr_yin  == 0) begin
            valid_range_top_out = {VALID_RANGE_WIDTH{1'b0}};
        end
        else if(curr_yin[0] == 1'b1) begin
            valid_range_top_out = (((curr_xin>>1)+1'b1)<<3) - 1'b1;
        end
        else if(curr_yin_minus_2[1:0] == 2'd0) begin
            valid_range_top_out = (((curr_xin>>2)+1'b1)<<4) - 1'b1;
        end
        else if(curr_yin_minus_4[2:0] == 3'd0) begin
            valid_range_top_out = (((curr_xin>>3)+1'b1)<<5) - 1'b1;
        end
        else if(curr_yin == 8) begin
            valid_range_top_out = (7'd64 - 1'b1)%(1 << VALID_RANGE_WIDTH);
        end
        else begin
            valid_range_top_out = {VALID_RANGE_WIDTH{1'b0}};
        end
        
        if(curr_xin  == 0) begin
            valid_range_left_out = {VALID_RANGE_WIDTH{1'b0}};
        end
        else if(curr_xin[0] == 1'b1) begin
            valid_range_left_out = ((curr_yin+1'b1)<<2) - 1'b1;
        end
        else if(curr_xin_minus_2[1:0] == 2'd0) begin
            valid_range_left_out = (((curr_yin>>1)+1'b1)<<3) - 1'b1;
        end
        else if(curr_xin_minus_4[2:0] == 3'd0) begin
            valid_range_left_out = (((curr_yin>>2)+1'b1)<<4) - 1'b1;
        end
        else if(curr_xin == 8) begin
            valid_range_left_out = (((curr_yin>>3)+1'b1)<<5) - 1'b1;
        end
        else begin
            valid_range_left_out = {VALID_RANGE_WIDTH{1'b0}};
        end
    
        
    end
    
endmodule
    
    

    