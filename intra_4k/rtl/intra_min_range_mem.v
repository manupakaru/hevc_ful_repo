`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    07:55:48 10/17/2013
// Design Name:
// Module Name:    intra_line_buffer_membank
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module intra_min_range_mem(
        clk,

        porta_wen_in,
        porta_addr_in,
        porta_wdata_in,
        porta_rdata_out,

        portb_wen_in,
        portb_addr_in,
        portb_wdata_in,
        portb_rdata_out
    );

//----------------------------------------------------
//PARAMETERS
//----------------------------------------------------
    parameter                       LOG2_MEM_SIZE          = 9;
    parameter                       DATA_WIDTH             = 9;


//----------------------------------------------------
//LOCALPARAMS
//----------------------------------------------------


    localparam                      MEM_SIZE            = 1 << (LOG2_MEM_SIZE);

//----------------------------------------------------
//I/O Signals
//----------------------------------------------------

    input                               clk;

    input                               porta_wen_in;
    input   [LOG2_MEM_SIZE - 1:0]       porta_addr_in;
    input   [DATA_WIDTH - 1:0]          porta_wdata_in;
    output reg [DATA_WIDTH - 1:0]       porta_rdata_out;


    input                               portb_wen_in;
    input   [LOG2_MEM_SIZE - 1:0]       portb_addr_in;
    input   [DATA_WIDTH - 1:0]          portb_wdata_in;
    output reg  [DATA_WIDTH - 1:0]      portb_rdata_out;

    // input                                                       portr_en;

//----------------------------------------------------
//  Internal Regs and Wires
//----------------------------------------------------

    reg         [DATA_WIDTH - 1:0]           mem [MEM_SIZE - 1:0];


//----------------------------------------------------
//  Implementation
//----------------------------------------------------

    always @(posedge clk) begin
        begin
            if (porta_wen_in) begin
                mem[porta_addr_in] <= porta_wdata_in;
            end
            porta_rdata_out <= mem[porta_addr_in];
        end
    end

    always @(posedge clk) begin
        begin
            if (portb_wen_in) begin
                mem[portb_addr_in] <= portb_wdata_in;
            end
            portb_rdata_out <= mem[portb_addr_in];
        end
    end


endmodule
