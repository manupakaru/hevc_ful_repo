`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:59:52 10/16/2013 
// Design Name: 
// Module Name:    intra_angular_top_prediction_aux_left_data_supplier 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module intra_angular_prediction_aux_data_supplier(
        clk,
        reset,
        enable,
        
        //ntbs_in,
        abs_intrapredangle_in,
        
        read_from_aux_flags_in,
        valid_in,
        mode_in,
        extended_arr_index_in,
        aux_axis_tb_corner_addr_in,
        inner_aux_in,
        inner_mainaxis_shifted_2_in,
        filter_done_in,
        filter_flag_in,
        
        // max_valid_aux_range_valid_in,
        max_valid_aux_range_in,
        
        aux_read_addr_out,
        aux_read_en_out,
        aux_read_data_in,
        
        corner_pixel_data_in,
        corner_pixel_filtered_data_in,

        aux_data0_out,
        aux_data1_out,
        aux_data2_out,
        aux_data3_out,
        aux_data4_out
    );
    
//----------------------------------------------------
//LOCALPARAMS
//----------------------------------------------------
   `include "../sim/pred_def.v"
    localparam                      INTRA_PRED_ANGLE_WIDTH  = 6;
    // localparam                      MAX_NTBS_SIZE           = 6;
    localparam                      NUM_OF_DATA_REGS        = 4;
    // localparam                      PIXEL_WIDTH             = 8;
    // localparam                      LOG2_FRAME_SIZE         = 13;
    localparam                      MODE_WIDTH              = 2;
    
    localparam                      STATE_WAIT              = 0;
    localparam                      STATE_READ              = 1;
    
    localparam                      TOP_PRED_MODE_WIDTH         = 2;
    localparam                      TOP_PRED_MODE_DC            = 0;
    localparam                      TOP_PRED_MODE_PLANAR        = 1;
    localparam                      TOP_PRED_MODE_ANGULAR_TOP   = 2;
    localparam                      TOP_PRED_MODE_ANGULAR_LEFT  = 3;
    
//----------------------------------------------------
// I/O Signals
//----------------------------------------------------

    input                                       clk;
    input                                       reset;
    input                                       enable;
    
    input       [INTRA_PRED_ANGLE_WIDTH - 1:0]  abs_intrapredangle_in;
    input                                       read_from_aux_flags_in;
    input                                       valid_in;
    input       [TOP_PRED_MODE_WIDTH - 1:0]     mode_in;
    input       [MAX_NTBS_SIZE - 1:0]           extended_arr_index_in;
    input       [LOG2_FRAME_SIZE - 1:0]         aux_axis_tb_corner_addr_in;
    input       [MAX_NTBS_SIZE - 1:0]           inner_aux_in;
    input       [MAX_NTBS_SIZE - 1:0]           inner_mainaxis_shifted_2_in;
    input                                       filter_done_in;
    input                                       filter_flag_in;
    
    // input                                       max_valid_aux_range_valid_in;
    input       [LOG2_FRAME_SIZE - 1:0]         max_valid_aux_range_in;
    
    output reg  [LOG2_FRAME_SIZE - 1:0]         aux_read_addr_out;
    output reg                                  aux_read_en_out;
    input       [PIXEL_WIDTH - 1:0]             aux_read_data_in;
    
    input       [PIXEL_WIDTH - 1:0]             corner_pixel_data_in;
    input       [PIXEL_WIDTH - 1:0]             corner_pixel_filtered_data_in;
    
    output reg  [PIXEL_WIDTH - 1:0]             aux_data0_out;
    output reg  [PIXEL_WIDTH - 1:0]             aux_data1_out;
    output reg  [PIXEL_WIDTH - 1:0]             aux_data2_out;
    output reg  [PIXEL_WIDTH - 1:0]             aux_data3_out;
    output reg  [PIXEL_WIDTH - 1:0]             aux_data4_out;
    
//----------------------------------------------------
//  Internal Regs and Wires 
//----------------------------------------------------    
    
    reg     [MAX_NTBS_SIZE - 1:0]               inner_mainaxis_shifted_2_reg[4:0];

    reg     [PIXEL_WIDTH - 1:0]                 aux_data_main0    [NUM_OF_DATA_REGS - 1:0];
    reg     [PIXEL_WIDTH - 1:0]                 aux_data_main4    [NUM_OF_DATA_REGS - 1:0];
    reg     [PIXEL_WIDTH - 1:0]                 aux_data_main8    [NUM_OF_DATA_REGS - 1:0];
    reg     [PIXEL_WIDTH - 1:0]                 aux_data_main12   [NUM_OF_DATA_REGS - 1:0];
    reg     [PIXEL_WIDTH - 1:0]                 aux_data_main16   [NUM_OF_DATA_REGS - 1:0];
    reg     [PIXEL_WIDTH - 1:0]                 aux_data_main20   [NUM_OF_DATA_REGS - 1:0];
    reg     [PIXEL_WIDTH - 1:0]                 aux_data_main24   [NUM_OF_DATA_REGS - 1:0];
    reg     [PIXEL_WIDTH - 1:0]                 aux_data_main28   [NUM_OF_DATA_REGS - 1:0];

    reg     [PIXEL_WIDTH - 1:0]                 aux_tempdata_main0 ; 
    reg     [PIXEL_WIDTH - 1:0]                 aux_tempdata_main4 ; 
    reg     [PIXEL_WIDTH - 1:0]                 aux_tempdata_main8 ; 
    reg     [PIXEL_WIDTH - 1:0]                 aux_tempdata_main12; 
    reg     [PIXEL_WIDTH - 1:0]                 aux_tempdata_main16; 
    reg     [PIXEL_WIDTH - 1:0]                 aux_tempdata_main20; 
    reg     [PIXEL_WIDTH - 1:0]                 aux_tempdata_main24; 
    reg     [PIXEL_WIDTH - 1:0]                 aux_tempdata_main28; 

    reg     [LOG2_FRAME_SIZE - 1:0]             aux_read_addr_prefilt;
    reg     [LOG2_FRAME_SIZE - 1:0]             aux_read_addr[1:0];
    wire    [MAX_NTBS_SIZE - 1:0]               aux_read_addr_offset;
    reg     [MAX_NTBS_SIZE - 1:0]               aux_read_addr_offset_reg[2:0];

    reg     [MAX_NTBS_SIZE - 1:0]               aux_read_addr_offset_main_0_reg;
    reg     [MAX_NTBS_SIZE - 1:0]               aux_read_addr_offset_main_4_reg;
    reg     [MAX_NTBS_SIZE - 1:0]               aux_read_addr_offset_main_8_reg;
    reg     [MAX_NTBS_SIZE - 1:0]               aux_read_addr_offset_main_12_reg;
    reg     [MAX_NTBS_SIZE - 1:0]               aux_read_addr_offset_main_16_reg;
    reg     [MAX_NTBS_SIZE - 1:0]               aux_read_addr_offset_main_20_reg;
    reg     [MAX_NTBS_SIZE - 1:0]               aux_read_addr_offset_main_24_reg;
    reg     [MAX_NTBS_SIZE - 1:0]               aux_read_addr_offset_main_28_reg;

    // reg     [LOG2_FRAME_SIZE - 1:0]             max_valid_aux_range_reg;
    //integer                                     state;
    
    intra_angular_aux_addr_gen intra_angular_aux_addr_gen_block(
        
        //.ntbs_in                    (ntbs_in),
        .abs_intrapredangle_in      (abs_intrapredangle_in),
        
        .read_neg_addr_index_in     (extended_arr_index_in),
        .aux_read_addr_offset_out   (aux_read_addr_offset)
    );


    always @(posedge clk) begin
        if(enable) begin
            inner_mainaxis_shifted_2_reg[4]  <= inner_mainaxis_shifted_2_in;
            inner_mainaxis_shifted_2_reg[3]  <= inner_mainaxis_shifted_2_reg[4];
            inner_mainaxis_shifted_2_reg[2]  <= inner_mainaxis_shifted_2_reg[3];
            inner_mainaxis_shifted_2_reg[1]  <= inner_mainaxis_shifted_2_reg[2];
            inner_mainaxis_shifted_2_reg[0]  <= inner_mainaxis_shifted_2_reg[1];
        end
    end
    
    always @(posedge clk) begin
        if(enable) begin
            aux_read_addr_offset_reg[2] <= aux_read_addr_offset;
            aux_read_addr_offset_reg[1] <= aux_read_addr_offset_reg[2];
            aux_read_addr_offset_reg[0] <= aux_read_addr_offset_reg[1];

            case(inner_mainaxis_shifted_2_reg[1])
                6'd0 : begin
                    aux_read_addr_offset_main_0_reg <= aux_read_addr_offset_reg[1];
                end 
                6'd4 : begin
                    aux_read_addr_offset_main_4_reg <= aux_read_addr_offset_reg[1];
                end 
                6'd8 : begin
                    aux_read_addr_offset_main_8_reg <= aux_read_addr_offset_reg[1];
                end 
                6'd12: begin
                    aux_read_addr_offset_main_12_reg <= aux_read_addr_offset_reg[1];
                end 
                6'd16: begin
                    aux_read_addr_offset_main_16_reg <= aux_read_addr_offset_reg[1];
                end
                6'd20: begin
                    aux_read_addr_offset_main_20_reg <= aux_read_addr_offset_reg[1];
                end
                6'd24: begin
                    aux_read_addr_offset_main_24_reg <= aux_read_addr_offset_reg[1];
                end
                6'd28: begin
                    aux_read_addr_offset_main_28_reg <= aux_read_addr_offset_reg[1];
                end
                default : begin

                end
            endcase
        end
    end


    always @(posedge clk) begin

    end

    always @(posedge clk) begin
        if (reset) begin
            // reset
            
        end
        else if (enable) begin
            if(filter_flag_in & filter_done_in) begin
                if(mode_in == TOP_PRED_MODE_PLANAR) begin
                    aux_read_addr_prefilt <= (1'b1 + inner_aux_in);
                end
                else begin
                    aux_read_addr_prefilt <= aux_read_addr_offset;
                end
            end
            else begin
                if(mode_in == TOP_PRED_MODE_PLANAR) begin
                    aux_read_addr_prefilt <= aux_axis_tb_corner_addr_in + (1'b1 + inner_aux_in);
                end
                else begin
                    aux_read_addr_prefilt <= aux_axis_tb_corner_addr_in + aux_read_addr_offset;
                end
            end 
        end
    end
    
    
//    assign aux_read_addr_prefilt = (mode_in == TOP_PRED_MODE_PLANAR) ?  aux_axis_tb_corner_addr_in + aux_read_addr_offset : aux_axis_tb_corner_addr_in + (1'b1 + inner_aux_in);
    

    always @(posedge clk) begin
        if (reset) begin
            // reset
            
        end
        else if (enable) begin
            if(aux_read_addr_prefilt == {LOG2_FRAME_SIZE{1'b0}}) begin
                aux_read_addr[1] <= {LOG2_FRAME_SIZE{1'b0}};
            end
            else if(aux_read_addr_prefilt > max_valid_aux_range_in) begin
                aux_read_addr[1] <= max_valid_aux_range_in;
            end
            else begin
                aux_read_addr[1] <= aux_read_addr_prefilt - 1'b1;
            end
        end
    end

    always @(posedge clk) begin
        if (reset) begin
            // reset
            
        end
        else if (enable) begin
            if(mode_in == TOP_PRED_MODE_PLANAR) begin
                aux_read_addr[0]  <= aux_read_addr[1];
                aux_read_addr_out <= aux_read_addr[0];
            end
            else begin
                // aux_read_addr[0]  <= aux_read_addr[1];
                // aux_read_addr_out <= aux_read_addr[1]; //need one clock earlier than the planar mode
                if(aux_read_addr_prefilt == {LOG2_FRAME_SIZE{1'b0}}) begin   //need two clock cyles earlier
                    aux_read_addr_out <= {LOG2_FRAME_SIZE{1'b0}};
                end
                else if(aux_read_addr_prefilt > max_valid_aux_range_in) begin
                    aux_read_addr_out <= max_valid_aux_range_in;
                end
                else begin
                    aux_read_addr_out <= aux_read_addr_prefilt - 1'b1;
                end
            end
        end
    end


    // always @(posedge clk) begin
    //     if (reset) begin
    //         // reset
    //     end
    //     else if (enable) begin
    //         if(max_valid_aux_range_valid_in == 1'b1) begin
    //             max_valid_aux_range_reg <= max_valid_aux_range_in;
    //         end
    //     end
    // end
    
    always @(*) begin
        aux_read_en_out = read_from_aux_flags_in & valid_in;
    end

//     always @(posedge clk) begin
//         if(reset) begin
//             state <= STATE_WAIT;
//         end
//         else if (enable) begin
//             case(state) 
//                 STATE_WAIT : begin
//                     if(read_from_aux_flags_in & valid_in) begin
//                         state <= STATE_READ;
//                     end 
//                 end
//                 STATE_READ : begin
                
//                     // if(aux_read_addr_offset == {MAX_NTBS_SIZE{1'b0}} ) begin
//                     //     if(filter_flag_in & filter_done_in) begin
//                     //         aux_data[0] <= corner_pixel_filtered_data_in;
//                     //     end
//                     //     else begin
//                     //         aux_data[0] <= corner_pixel_data_in;
//                     //     end
//                     // end
//                     // else begin
//                     //     aux_data[0] <= aux_read_data_in;
//                     // end
                    
//                     if(aux_read_addr_offset_reg[0] != aux_read_addr_offset_reg[1]) begin
//                         aux_data[0] <= aux_data0_out;
//                         aux_data[1] <= aux_data[0];
//                         aux_data[2] <= aux_data[1];
//                         aux_data[3] <= aux_data[2];
//                     end
// //                  aux_data[4] <= aux_data[3];
                
//                     if(read_from_aux_flags_in & valid_in) begin
//                         state <= STATE_READ;
//                     end
//                     else begin
//                         state <= STATE_WAIT;
//                     end
//                 end
//             endcase
//         end
//     end


    always @(posedge clk) begin
        if (reset) begin
            
        end
        else if (enable) begin
            case(inner_mainaxis_shifted_2_reg[1])
                6'd0 : begin
                    if((inner_aux_in == 6'd3)&&(inner_mainaxis_shifted_2_in == 6'd0)) begin  //just want to do this initially
                        if(filter_flag_in) begin
                            aux_data_main0[0] <= corner_pixel_filtered_data_in;  
                        end
                        else begin
                            aux_data_main0[0] <= corner_pixel_data_in; 
                        end
                    end
                    else if(aux_read_addr_offset_main_0_reg != aux_read_addr_offset_reg[1]) begin
                        if(inner_mainaxis_shifted_2_reg[1] == inner_mainaxis_shifted_2_reg[0]) begin
                            aux_data_main0[0] <= aux_data0_out;
                        end
                        else begin
                            aux_data_main0[0] <= aux_tempdata_main0;//aux_data0_out;
                        end


                        // else begin
                        //     aux_data_main0[1] <= aux_data_main0[0];  
                        // end

                        aux_data_main0[1] <= aux_data_main0[0];
                        aux_data_main0[2] <= aux_data_main0[1];
                        aux_data_main0[3] <= aux_data_main0[2];
                    end
                    // else if((inner_aux_in == 6'd1)&&(inner_mainaxis_shifted_2_in == 6'd0)) begin  //just want to do this initially
                    //     if(filter_flag_in) begin
                    //         aux_data_main0[0] <= corner_pixel_filtered_data_in;  
                    //     end
                    //     else begin
                    //         aux_data_main0[0] <= corner_pixel_data_in; 
                    //     end
                    // end
                end 
                6'd4 : begin
                    if(aux_read_addr_offset_main_4_reg != aux_read_addr_offset_reg[1]) begin
                        if(inner_mainaxis_shifted_2_reg[1] == inner_mainaxis_shifted_2_reg[0]) begin
                            aux_data_main4[0] <= aux_data0_out;
                        end
                        else begin
                            aux_data_main4[0] <= aux_tempdata_main4;//aux_data0_out;
                        end

                        //aux_data_main4[0] <= aux_tempdata_main4;//aux_data0_out;
                        aux_data_main4[1] <= aux_data_main4[0];
                        aux_data_main4[2] <= aux_data_main4[1];
                        aux_data_main4[3] <= aux_data_main4[2];
                    end
                end 
                6'd8 : begin
                    if(aux_read_addr_offset_main_8_reg != aux_read_addr_offset_reg[1]) begin

                        if(inner_mainaxis_shifted_2_reg[1] == inner_mainaxis_shifted_2_reg[0]) begin
                            aux_data_main8[0] <= aux_data0_out;
                        end
                        else begin
                            aux_data_main8[0] <= aux_tempdata_main8;//aux_data0_out;
                        end

                        //aux_data_main8[0] <= aux_tempdata_main8;//aux_data0_out;
                        aux_data_main8[1] <= aux_data_main8[0];
                        aux_data_main8[2] <= aux_data_main8[1];
                        aux_data_main8[3] <= aux_data_main8[2];
                    end
                end 
                6'd12: begin
                    if(aux_read_addr_offset_main_12_reg != aux_read_addr_offset_reg[1]) begin
                        if(inner_mainaxis_shifted_2_reg[1] == inner_mainaxis_shifted_2_reg[0]) begin
                            aux_data_main12[0] <= aux_data0_out;
                        end
                        else begin
                            aux_data_main12[0] <= aux_tempdata_main12;//aux_data0_out;
                        end

                    //    aux_data_main12[0] <= aux_tempdata_main12;//aux_data0_out;
                        aux_data_main12[1] <= aux_data_main12[0];
                        aux_data_main12[2] <= aux_data_main12[1];
                        aux_data_main12[3] <= aux_data_main12[2];
                    end
                end 
                6'd16: begin    
                    if(aux_read_addr_offset_main_16_reg != aux_read_addr_offset_reg[1]) begin
                        if(inner_mainaxis_shifted_2_reg[1] == inner_mainaxis_shifted_2_reg[0]) begin
                            aux_data_main16[0] <= aux_data0_out;
                        end
                        else begin
                            aux_data_main16[0] <= aux_tempdata_main16;//aux_data0_out;
                        end

                        //aux_data_main16[0] <= aux_tempdata_main16;//aux_data0_out;
                        aux_data_main16[1] <= aux_data_main16[0];
                        aux_data_main16[2] <= aux_data_main16[1];
                        aux_data_main16[3] <= aux_data_main16[2];
                    end
                end
                6'd20: begin
                    if(aux_read_addr_offset_main_20_reg != aux_read_addr_offset_reg[1]) begin
                        if(inner_mainaxis_shifted_2_reg[1] == inner_mainaxis_shifted_2_reg[0]) begin
                            aux_data_main20[0] <= aux_data0_out;
                        end
                        else begin
                            aux_data_main20[0] <= aux_tempdata_main20;//aux_data0_out;
                        end
                        //aux_data_main20[0] <= aux_tempdata_main20;//aux_data0_out;
                        aux_data_main20[1] <= aux_data_main20[0];
                        aux_data_main20[2] <= aux_data_main20[1];
                        aux_data_main20[3] <= aux_data_main20[2];
                    end
                end
                6'd24: begin
                    if(aux_read_addr_offset_main_24_reg != aux_read_addr_offset_reg[1]) begin
                        if(inner_mainaxis_shifted_2_reg[1] == inner_mainaxis_shifted_2_reg[0]) begin
                            aux_data_main24[0] <= aux_data0_out;
                        end
                        else begin
                            aux_data_main24[0] <= aux_tempdata_main24;//aux_data0_out;
                        end

                      //  aux_data_main24[0] <= aux_tempdata_main24;//aux_data0_out;
                        aux_data_main24[1] <= aux_data_main24[0];
                        aux_data_main24[2] <= aux_data_main24[1];
                        aux_data_main24[3] <= aux_data_main24[2];
                    end
                end
                6'd28: begin
                    if(aux_read_addr_offset_main_28_reg != aux_read_addr_offset_reg[1]) begin
                        if(inner_mainaxis_shifted_2_reg[1] == inner_mainaxis_shifted_2_reg[0]) begin
                            aux_data_main28[0] <= aux_data0_out;
                        end
                        else begin
                            aux_data_main28[0] <= aux_tempdata_main28;//aux_data0_out;
                        end

                        //aux_data_main28[0] <= aux_tempdata_main28;//aux_data0_out;
                        aux_data_main28[1] <= aux_data_main28[0];
                        aux_data_main28[2] <= aux_data_main28[1];
                        aux_data_main28[3] <= aux_data_main28[2];
                    end
                end
                default : begin
                    
                end
            endcase
        end
    end

always @(posedge clk) begin
    if (reset) begin
        
    end
    else if (enable) begin
        case(inner_mainaxis_shifted_2_reg[0])
            6'd0 : begin
                aux_tempdata_main0 <= aux_data0_out;
            end 
            6'd4 : begin
                aux_tempdata_main4 <= aux_data0_out;
            end 
            6'd8 : begin
                aux_tempdata_main8 <= aux_data0_out;
            end 
            6'd12: begin
                aux_tempdata_main12 <= aux_data0_out;
            end 
            6'd16: begin    
                aux_tempdata_main16 <= aux_data0_out;
            end
            6'd20: begin
                aux_tempdata_main20 <= aux_data0_out;
            end
            6'd24: begin
                aux_tempdata_main24 <= aux_data0_out;
            end
            6'd28: begin
                aux_tempdata_main28 <= aux_data0_out;
            end
            default : begin
                
            end
        endcase
    end
end

    always @(*) begin
        if(mode_in == TOP_PRED_MODE_PLANAR) begin
            aux_data0_out = aux_read_data_in;
        end
        else begin
            if(aux_read_addr_offset_reg[0] == {MAX_NTBS_SIZE{1'b0}} ) begin
                if(filter_flag_in & filter_done_in) begin
                    aux_data0_out = corner_pixel_filtered_data_in;
                end
                else begin
                    aux_data0_out = corner_pixel_data_in;
                end
            end
            else begin
                aux_data0_out = aux_read_data_in;
            end
        end    
    end
    
//    assign aux_data0_out = aux_read_data_in;

    always @(*) begin
        case(inner_mainaxis_shifted_2_reg[0])
            6'd0 : begin
                aux_data1_out = aux_data_main0[0];
                aux_data2_out = aux_data_main0[1];
                aux_data3_out = aux_data_main0[2];
                aux_data4_out = aux_data_main0[3];
            end 
            6'd4 : begin
                    aux_data1_out = aux_data_main4[0];
                    aux_data2_out = aux_data_main4[1];
                    aux_data3_out = aux_data_main4[2];
                    aux_data4_out = aux_data_main4[3];
            end 
            6'd8 : begin
                    aux_data1_out = aux_data_main8[0];
                    aux_data2_out = aux_data_main8[1];
                    aux_data3_out = aux_data_main8[2];
                    aux_data4_out = aux_data_main8[3];
            end 
            6'd12: begin
                    aux_data1_out = aux_data_main12[0];
                    aux_data2_out = aux_data_main12[1];
                    aux_data3_out = aux_data_main12[2];
                    aux_data4_out = aux_data_main12[3];
            end 
            6'd16: begin    
                    aux_data1_out = aux_data_main16[0];
                    aux_data2_out = aux_data_main16[1];
                    aux_data3_out = aux_data_main16[2];
                    aux_data4_out = aux_data_main16[3];
            end
            6'd20: begin
                    aux_data1_out = aux_data_main20[0];
                    aux_data2_out = aux_data_main20[1];
                    aux_data3_out = aux_data_main20[2];
                    aux_data4_out = aux_data_main20[3];
            end
            6'd24: begin
                    aux_data1_out = aux_data_main24[0];
                    aux_data2_out = aux_data_main24[1];
                    aux_data3_out = aux_data_main24[2];
                    aux_data4_out = aux_data_main24[3];
            end
            6'd28: begin
                    aux_data1_out = aux_data_main28[0];
                    aux_data2_out = aux_data_main28[1];
                    aux_data3_out = aux_data_main28[2];
                    aux_data4_out = aux_data_main28[3];
            end
            default : begin
                    aux_data1_out = 8'hff;
                    aux_data2_out = 8'hff;
                    aux_data3_out = 8'hff;
                    aux_data4_out = 8'hff;
            end
        endcase 
    end

    // assign aux_data1_out = aux_data[0];
    // assign aux_data2_out = aux_data[1];
    // assign aux_data3_out = aux_data[2];
    // assign aux_data4_out = aux_data[3];
    
endmodule
