`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:27:59 10/17/2013 
// Design Name: 
// Module Name:    intra_angular_read_from_aux_flag_gen 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module intra_angular_read_from_aux_flag_gen
    (
        clk,
        reset,
        enable,
        
        read_from_aux_flag_in,
        read_addr_in,
        extended_arr_index_in,
        mode_in,
        
        read_from_aux_flags_out,
        read_addr_out
    );
    
//----------------------------------------------------
//LOCALPARAMS
//----------------------------------------------------
   `include "../sim/pred_def.v"
    localparam                      INTRA_PRED_ANGLE_WIDTH  = 6;
    // localparam                      MAX_NTBS_SIZE           = 6;
    localparam                      NUM_OF_DATA_REGS        = 5;
    // localparam                      PIXEL_WIDTH             = 8;
    // localparam                      LOG2_FRAME_SIZE         = 13;    
//    localparam                      MODE_WIDTH              = 2;
    
//    localparam [MODE_WIDTH - 1:0]   MODE_ANGULAR                = (0)%(1 << MODE_WIDTH);
//    localparam [MODE_WIDTH - 1:0]   MODE_PLANAR                 = (1)%(1 << MODE_WIDTH);
//    localparam [MODE_WIDTH - 1:0]   MODE_DC                     = (2)%(1 << MODE_WIDTH);

    localparam                      TOP_PRED_MODE_WIDTH         = 2;
    localparam                      TOP_PRED_MODE_DC            = 0;
    localparam                      TOP_PRED_MODE_PLANAR        = 1;
    localparam                      TOP_PRED_MODE_ANGULAR_TOP   = 2;
    localparam                      TOP_PRED_MODE_ANGULAR_LEFT  = 3;
    
//----------------------------------------------------
//I/O Signals
//----------------------------------------------------
    
    input                                       clk;
    input                                       reset;
    input                                       enable;
    
    input                                       read_from_aux_flag_in;
    input       [LOG2_FRAME_SIZE - 1:0]         read_addr_in;
    input       [7 - 1:0]                       extended_arr_index_in;
    input       [TOP_PRED_MODE_WIDTH - 1:0]     mode_in;

    output reg  [4:0]                           read_from_aux_flags_out;
    output reg  [LOG2_FRAME_SIZE - 1:0]         read_addr_out;

//----------------------------------------------------
//  Implementation
//----------------------------------------------------

    always @(*) begin
        if(read_from_aux_flag_in) begin
            if(mode_in == TOP_PRED_MODE_PLANAR) begin
                read_from_aux_flags_out <= 5'b00001;
            end
            else begin
                case(extended_arr_index_in)
                    // 7'd0 : begin
                    //     read_from_aux_flags_out     <= 5'b00000;
                    // end
                    7'd0 : begin
                        read_from_aux_flags_out     <= 5'b00001;
                    end
                    7'd1 : begin
                        read_from_aux_flags_out     <= 5'b00011; 
                    end
                    7'd2 : begin
                        read_from_aux_flags_out     <= 5'b00111;
                    end
                    7'd3 : begin
                        read_from_aux_flags_out     <= 5'b01111;
                    end
                    default : begin
                        read_from_aux_flags_out     <= 5'b11111;
                    end
                endcase
            end
        end
        else begin
            read_from_aux_flags_out <= 5'b00000;
        end 
    end
    
    // always @(*) begin
    //     if(mode_in == TOP_PRED_MODE_PLANAR) begin
    //         read_addr_out = read_addr_in - 1'b1;
    //     end
    //     // else if(read_from_aux_flag_in) begin
    //     //     read_addr_out = -read_addr_in;
    //     // end
    //     else begin
    //         read_addr_out = read_addr_in;
    //     end
    // end

    always @(*) begin
        read_addr_out = read_addr_in;
    end


endmodule
