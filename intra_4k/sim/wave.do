onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider data_interface
add wave -noupdate -radix unsigned /intra_constrained_range_gen_tb/ycu_in
add wave -noupdate -radix unsigned /intra_constrained_range_gen_tb/xcu_in
add wave -noupdate /intra_constrained_range_gen_tb/intra_inter_mode_in
add wave -noupdate -radix unsigned /intra_constrained_range_gen_tb/ntbs_in
add wave -noupdate /intra_constrained_range_gen_tb/valid_in
add wave -noupdate -divider config_interface
add wave -noupdate /intra_constrained_range_gen_tb/log2_ctusize_in
add wave -noupdate /intra_constrained_range_gen_tb/frame_width_in
add wave -noupdate /intra_constrained_range_gen_tb/frame_height_in
add wave -noupdate /intra_constrained_range_gen_tb/config_valid_in
add wave -noupdate -divider outputs
add wave -noupdate -radix unsigned /intra_constrained_range_gen_tb/constrained_top_range_out
add wave -noupdate -radix unsigned /intra_constrained_range_gen_tb/constrained_left_range_out
add wave -noupdate /intra_constrained_range_gen_tb/clk
add wave -noupdate /intra_constrained_range_gen_tb/reset
add wave -noupdate /intra_constrained_range_gen_tb/enable
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/constrained_top_range_reg
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/first_cu_row_reg
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/intra_inter_mode_reg
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/lastrow_top_eight_3bit_compare_result
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/lastcol_left_eight_3bit_compare_result
add wave -noupdate -expand -group internal_uut -radix binary /intra_constrained_range_gen_tb/uut/left_eight_3bit_compare_result
add wave -noupdate -expand -group internal_uut -radix unsigned -childformat {{{/intra_constrained_range_gen_tb/uut/left_buffer_mem[7]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/left_buffer_mem[6]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/left_buffer_mem[5]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/left_buffer_mem[4]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/left_buffer_mem[3]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/left_buffer_mem[2]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/left_buffer_mem[1]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/left_buffer_mem[0]} -radix unsigned}} -expand -subitemconfig {{/intra_constrained_range_gen_tb/uut/left_buffer_mem[7]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/left_buffer_mem[6]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/left_buffer_mem[5]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/left_buffer_mem[4]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/left_buffer_mem[3]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/left_buffer_mem[2]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/left_buffer_mem[1]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/left_buffer_mem[0]} {-height 17 -radix unsigned}} /intra_constrained_range_gen_tb/uut/left_buffer_mem
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/next_left_buffer_right_update_mem
add wave -noupdate -expand -group internal_uut -radix unsigned {/intra_constrained_range_gen_tb/uut/left_buffer_mem[0]}
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/log2_ctusize_reg
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/ntbs_div_8_reg
add wave -noupdate -expand -group internal_uut -radix unsigned /intra_constrained_range_gen_tb/uut/state
add wave -noupdate -expand -group internal_uut -radix unsigned /intra_constrained_range_gen_tb/uut/old_state
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/porta_addr_reg
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/porta_rdata_wire
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/porta_wdata_reg
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/porta_wen_reg
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/portb_addr_reg
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/portb_wdata_wire
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/portb_wen_reg
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/portb_rdata_wire
add wave -noupdate -expand -group internal_uut -radix unsigned -childformat {{{/intra_constrained_range_gen_tb/uut/top_buffer_mem[7]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/top_buffer_mem[6]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/top_buffer_mem[5]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/top_buffer_mem[4]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/top_buffer_mem[3]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/top_buffer_mem[2]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/top_buffer_mem[1]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/top_buffer_mem[0]} -radix unsigned}} -expand -subitemconfig {{/intra_constrained_range_gen_tb/uut/top_buffer_mem[7]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/top_buffer_mem[6]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/top_buffer_mem[5]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/top_buffer_mem[4]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/top_buffer_mem[3]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/top_buffer_mem[2]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/top_buffer_mem[1]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/top_buffer_mem[0]} {-height 17 -radix unsigned}} /intra_constrained_range_gen_tb/uut/top_buffer_mem
add wave -noupdate -expand -group internal_uut -radix unsigned {/intra_constrained_range_gen_tb/uut/top_buffer_mem[0]}
add wave -noupdate -expand -group internal_uut -radix unsigned -childformat {{{/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[8]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[7]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[6]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[5]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[4]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[3]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[2]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[1]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[0]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[-1]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[-2]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[-3]} -radix unsigned} {{/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[-4]} -radix unsigned}} -expand -subitemconfig {{/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[8]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[7]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[6]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[5]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[4]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[3]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[2]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[1]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[0]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[-1]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[-2]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[-3]} {-height 17 -radix unsigned} {/intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem[-4]} {-height 17 -radix unsigned}} /intra_constrained_range_gen_tb/uut/next_top_buffer_bottom_update_mem
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/topmem_ctu_update_addr_offset
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/top_eight_3bit_compare_result
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/valid_reg
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/xcu_3lsb_wire
add wave -noupdate -expand -group internal_uut -radix unsigned /intra_constrained_range_gen_tb/uut/xcu_reg
add wave -noupdate -expand -group internal_uut -radix unsigned /intra_constrained_range_gen_tb/uut/ycu_reg
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/new_ctu_wire
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/top_mem_update_control_reg
add wave -noupdate -expand -group internal_uut /intra_constrained_range_gen_tb/uut/left_mem_update_control_reg
add wave -noupdate -radix unsigned /intra_constrained_range_gen_tb/uut/ycu_inner_ctu_div_8_reg
add wave -noupdate -radix unsigned /intra_constrained_range_gen_tb/uut/xcu_inner_ctu_div_8_reg
add wave -noupdate -expand -group min_range_mem -radix unsigned /intra_constrained_range_gen_tb/uut/intra_min_range_mem_block/porta_addr_in
add wave -noupdate -expand -group min_range_mem -radix unsigned /intra_constrained_range_gen_tb/uut/intra_min_range_mem_block/porta_wdata_in
add wave -noupdate -expand -group min_range_mem /intra_constrained_range_gen_tb/uut/intra_min_range_mem_block/porta_wen_in
add wave -noupdate -expand -group min_range_mem /intra_constrained_range_gen_tb/uut/intra_min_range_mem_block/portb_addr_in
add wave -noupdate -expand -group min_range_mem /intra_constrained_range_gen_tb/uut/intra_min_range_mem_block/portb_wdata_in
add wave -noupdate -expand -group min_range_mem /intra_constrained_range_gen_tb/uut/intra_min_range_mem_block/portb_wen_in
add wave -noupdate -expand -group min_range_mem /intra_constrained_range_gen_tb/uut/intra_min_range_mem_block/porta_rdata_out
add wave -noupdate -expand -group min_range_mem /intra_constrained_range_gen_tb/uut/intra_min_range_mem_block/portb_rdata_out
add wave -noupdate -expand -group min_range_mem /intra_constrained_range_gen_tb/uut/intra_min_range_mem_block/mem
add wave -noupdate /intra_constrained_range_gen_tb/uut/ycu_inner_ctu_div_8_reg
add wave -noupdate /intra_constrained_range_gen_tb/uut/current_ctu_done_wire
add wave -noupdate /intra_constrained_range_gen_tb/uut/ntbs_div_8_reg
add wave -noupdate /intra_constrained_range_gen_tb/uut/current_ctu_done_reg
add wave -noupdate /intra_constrained_range_gen_tb/uut/current_ctu_normal_done_wire
add wave -noupdate /intra_constrained_range_gen_tb/uut/current_ctu_horlast_done_wire
add wave -noupdate /intra_constrained_range_gen_tb/uut/current_ctu_verlast_done_wire
add wave -noupdate /intra_constrained_range_gen_tb/uut/current_ctu_horlast_done_reg
add wave -noupdate /intra_constrained_range_gen_tb/uut/current_ctu_verlast_done_reg
add wave -noupdate /intra_constrained_range_gen_tb/uut/current_ctu_done_reg
add wave -noupdate -radix unsigned /intra_constrained_range_gen_tb/uut/frame_width_reg
add wave -noupdate -radix unsigned /intra_constrained_range_gen_tb/uut/frame_height_reg
add wave -noupdate -radix unsigned /intra_constrained_range_gen_tb/uut/ycu_div_8_plus_ntbs_div_8_wire
add wave -noupdate -radix unsigned /intra_constrained_range_gen_tb/uut/xcu_div_8_plus_ntbs_div_8_wire
TreeUpdate [SetDefaultTree]
quietly WaveActivateNextPane
add wave -noupdate -radix unsigned /intra_constrained_range_gen_tb/uut/state
add wave -noupdate /intra_constrained_range_gen_tb/uut/ready_out
add wave -noupdate /intra_constrained_range_gen_tb/uut/ycu_inner_ctu_div_8_reg
add wave -noupdate /intra_constrained_range_gen_tb/uut/left_mem_update_control_offset_reg
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {12329867 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 254
configure wave -valuecolwidth 445
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {8510343 ps} {15107687 ps}
