`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:   11:28:58 04/16/2014
// Design Name:   intra_constrained_range
// Module Name:   F:/Manupa/pred_top_total_repo/intra/sim/intra_constrained_range_gen_tb.v
// Project Name:  pred_ise_project
// Target Device:
// Tool versions:
// Description:
//
// Verilog Test Fixture created by ISE for module: intra_constrained_range
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
////////////////////////////////////////////////////////////////////////////////

module intra_constrained_range_gen_tb;

//----------------------------------------------------
//PARAMETERS
//----------------------------------------------------
    parameter                       LOG2_FRAME_SIZE = 12;
    parameter                       MAX_LOG2CTBSIZE_WIDTH = 3;
    parameter                       PIXEL_WIDTH = 8;



//----------------------------------------------------
//LOCALPARAMS
//----------------------------------------------------

    localparam                      TOP_BUFFER_MEM_SIZE_1D              = 8;
    localparam                      LEFT_BUFFER_MEM_SIZE_1D             = 8;
    localparam                      NUMBER_OF_ROW_3BIT_COMPARATORS      = 8;
    localparam                      MAX_NTBS_SIZE                       = 6;
    localparam                      MAX_FRAMEWIDTH_IN_8X8               = LOG2_FRAME_SIZE - 3;

    localparam                      MODE_INTER = 1;
    localparam                      MODE_INTRA = 0;

    localparam                      STATE_INIT = 0;
    localparam                      STATE_READ = 1;
    localparam                      STATE_DONE = 2;

	// Inputs
	reg clk;
	reg reset;
	reg enable;
	reg [8:0] xcu_in;
	reg [8:0] ycu_in;
	reg [5:0] ntbs_in;
	reg intra_inter_mode_in;
	reg valid_in;
	reg [2:0] log2_ctusize_in;
	reg [11:0] frame_width_in;
	reg [11:0] frame_height_in;
	reg        config_valid_in;

	// Outputs
	wire 		ready_out;
	wire [11:0] constrained_top_range_out;
	wire [11:0] constrained_left_range_out;

	// Instantiate the Unit Under Test (UUT)
	intra_constrained_range_gen uut (
		.clk(clk),
		.reset(reset),
		.enable(enable),
		.xcu_in(xcu_in),

		.ycu_in(ycu_in),
		.ntbs_in(ntbs_in),
		.intra_inter_mode_in(intra_inter_mode_in),
		.valid_in(valid_in),
		.ready_out(ready_out),

		.log2_ctusize_in(log2_ctusize_in),
		.frame_width_in(frame_width_in),
		.frame_height_in(frame_height_in),
		.config_valid_in(config_valid_in),

		.constrained_top_range_out(constrained_top_range_out),
		.constrained_left_range_out(constrained_left_range_out)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 1;
		enable = 1;
		xcu_in = 0;
		ycu_in = 0;
		ntbs_in = 0;
		intra_inter_mode_in = 0;
		valid_in = 0;
		log2_ctusize_in = 0;
		frame_width_in = 0;
		frame_height_in = 0;
		config_valid_in = 0;

		// Wait 100 ns for global reset to finish
		#100;

		// Add stimulus here
		reset = 0;
		@(posedge clk);
		@(posedge clk);


		initialize(6,64*2 + 32,64*2 + 32); // 6 = 64 x 64 CTU size

		//1st CTU

			//1st 32x32
			feed_tu(0,0,0,0,MODE_INTER,8);
			feed_tu(0,0,8,0,MODE_INTRA,8);
			feed_tu(0,0,0,8,MODE_INTRA,8);
			feed_tu(0,0,8,8,MODE_INTRA,8);

			feed_tu(0,0,16,0,MODE_INTRA,8);
			feed_tu(0,0,24,0,MODE_INTRA,8);
		    feed_tu(0,0,16,8,MODE_INTER,8);
			feed_tu(0,0,24,8,MODE_INTRA,8);

			feed_tu(0,0,0,16,MODE_INTER,16);

			feed_tu(0,0,16,16,MODE_INTRA,16);

			//2nd 32x32
			feed_tu(0,0,32,0,MODE_INTRA,32);

			//3rd 32x32
			feed_tu(0,0,0,32,MODE_INTRA,32);

			//4th 32x32
			feed_tu(0,0,32,32,MODE_INTER,32);


		//2nd CTU

			//1st 32x32
			feed_tu(1,0,0,0,MODE_INTER,8);
			feed_tu(1,0,8,0,MODE_INTRA,8);
			feed_tu(1,0,0,8,MODE_INTRA,8);
			feed_tu(1,0,8,8,MODE_INTRA,8);

			feed_tu(1,0,16,0,MODE_INTRA,8);
			feed_tu(1,0,24,0,MODE_INTRA,8);
		    feed_tu(1,0,16,8,MODE_INTER,8);
			feed_tu(1,0,24,8,MODE_INTRA,8);

			feed_tu(1,0,0,16,MODE_INTER,16);

			feed_tu(1,0,16,16,MODE_INTRA,16);

			//2nd 32x32
			feed_tu(1,0,32,0,MODE_INTRA,32);

			//3rd 32x32
			feed_tu(1,0,0,32,MODE_INTRA,32);

			//4th 32x32
			feed_tu(1,0,32,32,MODE_INTER,32);

		//3rd CTU

			//1st 32x32
			feed_tu(2,0,0,0,MODE_INTER,8);
			feed_tu(2,0,8,0,MODE_INTRA,8);
			feed_tu(2,0,0,8,MODE_INTRA,8);
			feed_tu(2,0,8,8,MODE_INTRA,8);

			feed_tu(2,0,16,0,MODE_INTRA,8);
			feed_tu(2,0,24,0,MODE_INTRA,8);
		    feed_tu(2,0,16,8,MODE_INTER,8);
			feed_tu(2,0,24,8,MODE_INTRA,8);

			feed_tu(2,0,0,16,MODE_INTER,16);

			feed_tu(2,0,16,16,MODE_INTRA,16);

			// //2nd 32x32
			// feed_tu(2,0,32,0,MODE_INTRA,32);

			//3rd 32x32
			feed_tu(2,0,0,32,MODE_INTRA,32);

			// //4th 32x32
			// feed_tu(2,0,32,32,MODE_INTER,32);



		//4th CTU  2ndrow

			//1st 32x32
			feed_tu(0,1,0,0,MODE_INTER,8);
			feed_tu(0,1,8,0,MODE_INTRA,8);
			feed_tu(0,1,0,8,MODE_INTRA,8);
			feed_tu(0,1,8,8,MODE_INTRA,8);

			feed_tu(0,1,16,0,MODE_INTRA,8);
			feed_tu(0,1,24,0,MODE_INTRA,8);
		    feed_tu(0,1,16,8,MODE_INTER,8);
			feed_tu(0,1,24,8,MODE_INTRA,8);

			feed_tu(0,1,0,16,MODE_INTER,16);

			feed_tu(0,1,16,16,MODE_INTRA,16);

			//2nd 32x32
			feed_tu(0,1,32,0,MODE_INTRA,32);

			//3rd 32x32
			feed_tu(0,1,0,32,MODE_INTRA,32);

			//4th 32x32
			feed_tu(0,1,32,32,MODE_INTER,32);

		//5th CTU  2ndrow

			//1st 32x32
			feed_tu(1,1,0,0,MODE_INTER,8);
			feed_tu(1,1,8,0,MODE_INTRA,8);
			feed_tu(1,1,0,8,MODE_INTRA,8);
			feed_tu(1,1,8,8,MODE_INTRA,8);

			feed_tu(1,1,16,0,MODE_INTRA,8);
			feed_tu(1,1,24,0,MODE_INTRA,8);
		    feed_tu(1,1,16,8,MODE_INTER,8);
			feed_tu(1,1,24,8,MODE_INTRA,8);

			feed_tu(1,1,0,16,MODE_INTER,16);

			feed_tu(1,1,16,16,MODE_INTRA,16);

			//2nd 32x32
			feed_tu(1,1,32,0,MODE_INTRA,32);

			//3rd 32x32
			feed_tu(1,1,0,32,MODE_INTRA,32);

			//4th 32x32
			feed_tu(1,1,32,32,MODE_INTER,32);

		//6th CTU  2ndrow

			//1st 32x32
			feed_tu(2,1,0,0,MODE_INTER,8);
			feed_tu(2,1,8,0,MODE_INTRA,8);
			feed_tu(2,1,0,8,MODE_INTRA,8);
			feed_tu(2,1,8,8,MODE_INTRA,8);

			feed_tu(2,1,16,0,MODE_INTRA,8);
			feed_tu(2,1,24,0,MODE_INTRA,8);
		    feed_tu(2,1,16,8,MODE_INTER,8);
			feed_tu(2,1,24,8,MODE_INTRA,8);

			feed_tu(2,1,0,16,MODE_INTER,16);
			feed_tu(2,1,16,16,MODE_INTRA,16);

			// //2nd 32x32
			// feed_tu(2,1,32,0,MODE_INTRA,32);

			//3rd 32x32
			feed_tu(2,1,0,32,MODE_INTRA,32);

			// //4th 32x32
			// feed_tu(2,1,32,32,MODE_INTER,32);

		//7th CTU  3rd row

			//1st 32x32
			feed_tu(0,2,0,0,MODE_INTER,8);
			feed_tu(0,2,8,0,MODE_INTRA,8);
			feed_tu(0,2,0,8,MODE_INTRA,8);
			feed_tu(0,2,8,8,MODE_INTRA,8);

			feed_tu(0,2,16,0,MODE_INTRA,8);
			feed_tu(0,2,24,0,MODE_INTRA,8);
		    feed_tu(0,2,16,8,MODE_INTER,8);
			feed_tu(0,2,24,8,MODE_INTRA,8);

			feed_tu(0,2,0,16,MODE_INTER,16);
			feed_tu(0,2,16,16,MODE_INTRA,16);

			//2nd 32x32
			feed_tu(0,2,32,0,MODE_INTRA,32);

			// //3rd 32x32
			// feed_tu(2,1,0,32,MODE_INTRA,32);

			// //4th 32x32
			// feed_tu(2,1,32,32,MODE_INTER,32);

					//7th CTU  3rd row

		//8th CTU

			//1st 32x32
			feed_tu(1,2,0,0,MODE_INTER,8);
			feed_tu(1,2,8,0,MODE_INTRA,8);
			feed_tu(1,2,0,8,MODE_INTRA,8);
			feed_tu(1,2,8,8,MODE_INTRA,8);

			feed_tu(1,2,16,0,MODE_INTRA,8);
			feed_tu(1,2,24,0,MODE_INTRA,8);
		    feed_tu(1,2,16,8,MODE_INTER,8);
			feed_tu(1,2,24,8,MODE_INTRA,8);

			feed_tu(1,2,0,16,MODE_INTER,16);
			feed_tu(1,2,16,16,MODE_INTRA,16);

			//2nd 32x32
			feed_tu(1,2,32,0,MODE_INTRA,32);

			// //3rd 32x32
			// feed_tu(2,1,0,32,MODE_INTRA,32);

			// //4th 32x32
			// feed_tu(2,1,32,32,MODE_INTER,32);

		//9th CTU

			//1st 32x32
			feed_tu(2,2,0,0,MODE_INTER,8);
			feed_tu(2,2,8,0,MODE_INTRA,8);
			feed_tu(2,2,0,8,MODE_INTRA,8);
			feed_tu(2,2,8,8,MODE_INTRA,8);

			feed_tu(2,2,16,0,MODE_INTRA,8);
			feed_tu(2,2,24,0,MODE_INTRA,8);
		    feed_tu(2,2,16,8,MODE_INTER,8);
			feed_tu(2,2,24,8,MODE_INTRA,8);

			feed_tu(2,2,0,16,MODE_INTER,16);
			feed_tu(2,2,16,16,MODE_INTRA,16);

						//2nd 32x32
			feed_tu(1,2,32,0,MODE_INTRA,32);


			// //3rd 32x32
			// feed_tu(2,1,0,32,MODE_INTRA,32);

			// //4th 32x32
			// feed_tu(2,1,32,32,MODE_INTER,32);





	end

	initial begin
		forever begin
			#5 clk <= ~clk;
		end
	end

	task initialize;
		input [3 - 1:0] initialize_log2_ctusize_in;
		input [LOG2_FRAME_SIZE - 1 :0] initialize_frame_width_in;
		input [LOG2_FRAME_SIZE - 1 :0] initialize_frame_height_in;
		begin

			@(posedge clk);
			log2_ctusize_in <= initialize_log2_ctusize_in;
			frame_width_in <= initialize_frame_width_in;
			frame_height_in <= initialize_frame_height_in;
			config_valid_in <= 1'b1;

			@(posedge clk);
			config_valid_in <= 1'b0;

		end
	endtask

	task feed_tu;
		input [LOG2_FRAME_SIZE - 1:0] feed_tu_ctu_x_in;
		input [LOG2_FRAME_SIZE - 1:0] feed_tu_ctu_y_in;
		input [LOG2_FRAME_SIZE - 1:0] feed_tu_xcu_in;
		input [LOG2_FRAME_SIZE - 1:0] feed_tu_ycu_in;
		input 						  feed_tu_intra_inter_mode_in;
		input [MAX_NTBS_SIZE - 1:0]	  feed_tu_ntbs_in;
	    begin
	    	wait(ready_out);
			@(posedge clk);
			xcu_in 				<= (feed_tu_ctu_x_in*64 + feed_tu_xcu_in)/8;
			ycu_in 				<= (feed_tu_ctu_y_in*64 + feed_tu_ycu_in)/8;
			ntbs_in 			<= feed_tu_ntbs_in;
			intra_inter_mode_in <= feed_tu_intra_inter_mode_in;
			valid_in 			<= 1'b1;

			@(posedge clk);
			valid_in 			<= 1'b0;

			delay(10);
		end

	endtask

	task delay;
		input integer				no_of_cycles;
		begin : main
			integer i;

			for ( i = 0 ; i < no_of_cycles ; i = i + 1 ) begin
				@(posedge clk);
			end
		end
	endtask

endmodule

