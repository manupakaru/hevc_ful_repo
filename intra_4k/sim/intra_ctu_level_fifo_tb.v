`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   20:14:22 11/13/2013
// Design Name:   intra_ctu_level_fifo
// Module Name:   J:/Campus Electronics/FYP/ISE/hevc_ip/intra_pred/hevc_bitbucket/verilog/prediction/intra/sim/intra_ctu_level_fifo_tb.v
// Project Name:  intra_pred
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: intra_ctu_level_fifo
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module intra_ctu_level_fifo_tb;

	// Inputs
	reg clk;
	reg reset;
	reg read_en_in;
	reg [7:0] write_data_in;
	reg write_en_in;
	reg [7:0] counter;

	// Outputs
	wire [7:0] read_data_out;
	wire read_almost_empty_out;
	wire read_empty_out;
	wire write_almost_full_out;
	wire write_full_out;

	// Instantiate the Unit Under Test (UUT)
	intra_ctu_level_fifo uut (
		.clk(), 
		.reset(reset), 
		.read_data_out(read_data_out), 
		.read_en_in(read_en_in), 
		.read_almost_empty_out(read_almost_empty_out), 
		.read_empty_out(read_empty_out), 
		.write_data_in(write_data_in), 
		.write_en_in(write_en_in), 
		.write_almost_full_out(write_almost_full_out), 
		.write_full_out(write_full_out)
	);

	initial begin
		// Initialize Inputs
		clk <= 1;
		reset <= 1;
		read_en_in <= 0;
		write_data_in <= 0;
		write_en_in <= 0;
		counter <= 0;

		// Wait 100 ns for global reset to finish
		#100;
        reset <= 0;
        
        @(posedge clk);
            write_en_in <= 1'b1;
            write_data_in  <= 8'd1;
            
        @(posedge clk);
            write_en_in <= 1'b1;
            write_data_in  <= 8'd2;
            
        @(posedge clk);
            write_en_in <= 1'b1;
            write_data_in  <= 8'd3;
            
        @(posedge clk);
            write_en_in <= 1'b0;
        
        @(posedge clk);
            read_en_in  <= 1'b1;
        @(posedge clk);
            read_en_in  <= 1'b1;
        @(posedge clk);
            read_en_in  <= 1'b1;
        @(posedge clk);
            read_en_in  <= 1'b0;
        
		// Add stimulus here

	end
    
//    always begin
//        #5 clk <= ~clk;
//    end
      
endmodule

