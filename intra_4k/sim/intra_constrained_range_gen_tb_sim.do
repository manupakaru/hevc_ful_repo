vlib work;

vlog intra_constrained_range_gen_tb.v +cover
vlog ../rtl/intra_constrained_range_gen.v +cover
vlog ../rtl/intra_min_range_mem.v +cover

vsim -coverage -novopt -t 1ps -L xilinxcorelib_ver -L unisims_ver -L unimacro_ver -L secureip -lib work work.intra_constrained_range_gen_tb
log -r /*
do wave.do
run 15000ns