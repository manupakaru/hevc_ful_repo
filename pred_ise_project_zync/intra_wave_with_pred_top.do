onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/read_stage_block/dbf_in_cb_fifo/din
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/read_stage_block/dbf_in_cr_fifo/din
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/read_stage_block/dbf_in_cr_fifo/wr_en
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/read_stage_block/dbf_in_cb_fifo/wr_en
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/display_buffer/din
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/dbf_block/main_sao_block/display_buffer/wr_en
add wave -noupdate /intra_pred_tb/uut/axi_write_block/axi_wdata
add wave -noupdate /intra_pred_tb/uut/axi_write_block/axi_wvalid
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_data
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/ref_pix_axi_r_valid
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_11x11
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_25cb_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_25cr_out
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_ready
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_5x5_cb
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/block_5x5_cr
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/cache_rdata
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/cache_rdata_arr
add wave -noupdate /intra_pred_tb/uut/pred_dbf_block/pred_wrap_block/pred_top_block/inter_top_block/pred_sample_gen_block/cache_block/cache_w_data_arr
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {15291870000 ps} 1} {{Cursor 2} {15292402654 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 184
configure wave -valuecolwidth 320
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {15291186333 ps} {15292731389 ps}
