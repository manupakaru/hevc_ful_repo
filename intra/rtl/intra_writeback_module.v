
// ************************************************************************************************
//
// PROJECT      :   <project name where applicable>
// PRODUCT      :   <product name where applicable>
// FILE         :   <File Name/Module Name>
// AUTHOR       :   <Author's name>
// DESCRIPTION  :   <at least a short description about the module/file>
//                  Where does this file get inputs and send outputs?
//                  What does the guts of this file accomplish, and how does it do it?
//                  What module(s) does this file instantiate?
//
// ************************************************************************************************
//
// REVISIONS:
//
//	Date			Developer	Description
//	----			---------	-----------
//  29 Feb 2012		user123     bug fix - <jira issue id>
//  31 Oct 2007		userxyz		added xyz functionality - <jira issue id>
//  ...              ...         ...
//  ...              ...         ...
//
//**************************************************************************************************

`timescale 1ns / 1ps

module intra_writeback_module
    (
		clk,
		reset,

		intra_inter_mode_in,
		res_added_samples_in,
        res_added_4by4_x_in,
        res_added_4by4_y_in,
		last_row_in,
		last_col_in,
		valid_in,

        constrained_intra_pred_flag_in,
        log2_ctusize_in,
        // config_valid_in,

		//top writeback interface
		top_portw_addr_out,
        top_portw_yaddr_out,
		top_portw_data0_out,
		top_portw_data1_out,
		top_portw_data2_out,
		top_portw_data3_out,
		top_portw_data4_out,
		top_portw_data5_out,
		top_portw_data6_out,
		top_portw_data7_out,
		top_portw_en_mask_out,

		//left writeback interface
		left_portw_addr_out,
        left_portw_xaddr_out,
		left_portw_data0_out,
		left_portw_data1_out,
		left_portw_data2_out,
		left_portw_data3_out,
		left_portw_data4_out,
		left_portw_data5_out,
		left_portw_data6_out,
		left_portw_data7_out,
		left_portw_en_mask_out,

        top_nt_in,
        left_nt_in,
        nt_pixels_valid_in
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
  //  `include global_defs.v

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                       MAX_LOG2CTBSIZE_WIDTH = 3;

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

    localparam      MAX_CTU_SIZE                = 64;
    localparam      MAX_CTU_SIZE_DIV_8          = MAX_CTU_SIZE/8;

	localparam 		PIXEL_WIDTH 				= 8;
	localparam 		OUTPUT_BLOCK_SIZE 			= 4;
	localparam		PIXEL_ADDR_LENGTH           = 12;

    localparam      MODE_INTER                  = 1;
    localparam      MODE_INTRA                  = 0;

    localparam      STATE_WRITEUP_FIRST32       = 1;
    localparam      STATE_WRITEUP_SECOND32      = 2;
    localparam      STATE_DONE                  = 0;


//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input 																clk;
    input 																reset;

    input 																intra_inter_mode_in;
    input   [PIXEL_ADDR_LENGTH - 1:0]                                   res_added_4by4_x_in;
    input   [PIXEL_ADDR_LENGTH - 1:0]                                   res_added_4by4_y_in;
    input 	[PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]		res_added_samples_in;
    input 																last_row_in;
    input 																last_col_in;
    input 																valid_in;

    input                                                               constrained_intra_pred_flag_in;
    input   [MAX_LOG2CTBSIZE_WIDTH - 1:0]                               log2_ctusize_in;
    // input                                                               config_valid_in;

    output reg [PIXEL_ADDR_LENGTH - 1:0]                                top_portw_addr_out;
    output reg [PIXEL_ADDR_LENGTH - 1:0]								top_portw_yaddr_out;
    output reg [PIXEL_WIDTH - 1:0]										top_portw_data0_out;
    output reg [PIXEL_WIDTH - 1:0]										top_portw_data1_out;
    output reg [PIXEL_WIDTH - 1:0]										top_portw_data2_out;
    output reg [PIXEL_WIDTH - 1:0]										top_portw_data3_out;
    output reg [PIXEL_WIDTH - 1:0]										top_portw_data4_out;
    output reg [PIXEL_WIDTH - 1:0]										top_portw_data5_out;
    output reg [PIXEL_WIDTH - 1:0]										top_portw_data6_out;
    output reg [PIXEL_WIDTH - 1:0]										top_portw_data7_out;
    output reg [2*OUTPUT_BLOCK_SIZE - 1:0]								top_portw_en_mask_out;

    output reg [PIXEL_ADDR_LENGTH - 1:0]                                left_portw_addr_out;
    output reg [PIXEL_ADDR_LENGTH - 1:0]								left_portw_xaddr_out;
    output reg [PIXEL_WIDTH - 1:0]										left_portw_data0_out;
    output reg [PIXEL_WIDTH - 1:0]										left_portw_data1_out;
    output reg [PIXEL_WIDTH - 1:0]										left_portw_data2_out;
    output reg [PIXEL_WIDTH - 1:0]										left_portw_data3_out;
    output reg [PIXEL_WIDTH - 1:0]										left_portw_data4_out;
    output reg [PIXEL_WIDTH - 1:0]										left_portw_data5_out;
    output reg [PIXEL_WIDTH - 1:0]										left_portw_data6_out;
    output reg [PIXEL_WIDTH - 1:0]										left_portw_data7_out;
    output reg [2*OUTPUT_BLOCK_SIZE - 1:0]								left_portw_en_mask_out;

    input      [PIXEL_WIDTH - 1:0]                                      top_nt_in;
    input      [PIXEL_WIDTH - 1:0]                                      left_nt_in;
    input                                                               nt_pixels_valid_in;


//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    wire       [PIXEL_WIDTH - 1:0]                                      res_added_samples_arr_wire[OUTPUT_BLOCK_SIZE - 1:0][OUTPUT_BLOCK_SIZE - 1:0];

    //reg        [3 - 1:0]                                                left_buffer_inter_intra_cu_reg[MAX_CTU_SIZE_DIV_8 - 1:0][MAX_CTU_SIZE_DIV_8 - 1:0];
    reg        [4 - 1:0]                                                left_buffer_prior_inter_sum_first32_reg;
    reg        [4 - 1:0]                                                left_buffer_prior_inter_sum_second32_reg;
    reg        [4 - 1:0]                                                upwrite_counter_reg;
    reg        [8 - 1:0]                                                upwrite_data_reg;
    reg        [PIXEL_ADDR_LENGTH - 1:0]                                upwrite_addr;

    reg                                                                 constrained_intra_pred_flag_reg;
    reg        [MAX_CTU_SIZE_DIV_8 - 1:0]                               log2_ctusize_reg;

    integer                                                             state;

    wire     [PIXEL_WIDTH - 1:0]                                        left_nt_wire;
    reg                                                                 left_nt_fifo_rden_reg;
    wire                                                                left_nt_fifo_full_wire;
    wire                                                                left_nt_fifo_empty_wire;
    wire     [PIXEL_WIDTH - 1:0]                                        top_nt_wire;
    reg                                                                 top_nt_fifo_rden_reg;
    wire                                                                top_nt_fifo_full_wire;
    wire                                                                top_nt_fifo_empty_wire;

    reg                                                                 block_end_reg;
    reg      [PIXEL_WIDTH - 1:0]                                        threatned_x_reg;
    reg      [PIXEL_WIDTH - 1:0]                                        threatned_y_reg;


//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------

    geet_fifo
    #(
        .FIFO_DATA_WIDTH(PIXEL_WIDTH),
        .LOG2_FIFO_DEPTH(10)
    )
    top_fifo_block
    (
        .clk        (clk),
        .reset      (reset),

        .wr_en      (nt_pixels_valid_in),
        .d_in       (top_nt_in),
        .full       (top_nt_fifo_full_wire),

        .rd_en      (top_nt_fifo_rden_reg),
        .d_out      (top_nt_wire),
        .empty      (top_nt_fifo_empty_wire)
    );

    geet_fifo
    #(
        .FIFO_DATA_WIDTH(PIXEL_WIDTH),
        .LOG2_FIFO_DEPTH(10)
    )
    left_fifo_block
    (
        .clk        (clk),
        .reset      (reset),

        .wr_en      (nt_pixels_valid_in),
        .d_in       (left_nt_in),
        .full       (left_nt_fifo_full_wire),

        .rd_en      (left_nt_fifo_rden_reg),
        .d_out      (left_nt_wire),
        .empty      (left_nt_fifo_empty_wire)
    );



    generate
        genvar i;
        genvar j;

        for (i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin : row_iteration
            for (j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin : column_iteration
                assign  res_added_samples_arr_wire[j][i] = res_added_samples_in[i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE +(j+1)*PIXEL_WIDTH - 1:j*PIXEL_WIDTH + i*PIXEL_WIDTH*OUTPUT_BLOCK_SIZE];
            end
        end

    endgenerate


    always @(posedge clk) begin : left_buffer_special_copy
        integer i;
        if (reset) begin
            // reset
            left_buffer_prior_inter_sum_first32_reg     <= 4'd0;
            left_buffer_prior_inter_sum_second32_reg    <= 4'd0;
        end
        else begin
            if(valid_in) begin
                if(last_col_in) begin
                    case(log2_ctusize_reg)
                        3'd4 : begin
                            /// do nothing
                        end
                        3'd5 : begin
                            if(res_added_4by4_x_in[5 - 1:0] == 5'd28) begin
                                if(intra_inter_mode_in == MODE_INTER) begin
                                    left_buffer_prior_inter_sum_first32_reg <= left_buffer_prior_inter_sum_first32_reg + 1'b1;
                                end
                                else begin
                                    left_buffer_prior_inter_sum_first32_reg <= 4'd0;
                                end
                            end
                            else if(res_added_4by4_x_in[6 - 1:0] == 6'd0) begin
                                left_buffer_prior_inter_sum_first32_reg <= 4'd0;
                                left_buffer_prior_inter_sum_second32_reg <= 4'd0;
                            end
                        end
                        default /*3'd6*/ : begin
                            if(res_added_4by4_x_in[6 - 1:0] == 6'd28) begin
                                if(intra_inter_mode_in == MODE_INTER) begin
                                    left_buffer_prior_inter_sum_first32_reg <= left_buffer_prior_inter_sum_first32_reg + 1'b1;
                                end
                                else begin
                                    left_buffer_prior_inter_sum_first32_reg <= 4'd0;
                                end
                            end
                            else if(res_added_4by4_x_in[6 - 1:0] == 6'd60) begin
                                if(intra_inter_mode_in == MODE_INTER) begin
                                    left_buffer_prior_inter_sum_second32_reg <= left_buffer_prior_inter_sum_second32_reg + 1'b1;
                                end
                                else begin
                                    left_buffer_prior_inter_sum_second32_reg <= 4'd0;
                                end
                            end
                            else if(res_added_4by4_x_in[6 - 1:0] == 6'd0) begin
                                left_buffer_prior_inter_sum_first32_reg <= 4'd0;
                                left_buffer_prior_inter_sum_second32_reg <= 4'd0;
                            end
                        end
                    endcase
                end
            end
        end
    end

    always @(posedge clk) begin
        log2_ctusize_reg <= log2_ctusize_in;
        constrained_intra_pred_flag_reg <= constrained_intra_pred_flag_in;
    end

    always @(posedge clk) begin
        if(last_row_in & last_col_in & valid_in) begin
            top_nt_fifo_rden_reg <= 1'b1;
            left_nt_fifo_rden_reg <= 1'b1;
        end
        else begin
            top_nt_fifo_rden_reg <= 1'b0;
            left_nt_fifo_rden_reg <= 1'b0;
        end
    end



    always @(posedge clk) begin : main
        integer i;
        if (reset) begin
            top_portw_en_mask_out <= 8'b00000000;
            left_portw_en_mask_out <= 8'b00000000;


            // for (i = 0 ; i < MAX_CTU_SIZE_DIV_8 ; i = i + 1) begin
            //     left_buffer_inter_intra_cu_reg[i] <= 3'd0;
            // end
            state <= STATE_DONE;
            upwrite_counter_reg <= 4'd0;
            block_end_reg <= 1'b0;

            threatned_x_reg <= {PIXEL_WIDTH{1'b0}};
            threatned_y_reg <= {PIXEL_WIDTH{1'b0}};
        end
        else begin
            if(valid_in) begin

                if(block_end_reg) begin
                    threatned_x_reg <= res_added_4by4_x_in;
                    threatned_y_reg <= res_added_4by4_y_in;
                end
                block_end_reg <= 1'b0;


                if(last_row_in) begin
                    top_portw_addr_out              <= res_added_4by4_x_in;

                    if(block_end_reg) begin
                        top_portw_yaddr_out             <= res_added_4by4_y_in;
                    end
                    else begin
                        top_portw_yaddr_out             <= threatned_y_reg;
                    end



                    if((constrained_intra_pred_flag_reg == 1'b1)&&(intra_inter_mode_in == MODE_INTER)) begin
                        top_portw_data0_out             <= left_nt_wire;
                        top_portw_data1_out             <= left_nt_wire;
                        top_portw_data2_out             <= left_nt_wire;
                        top_portw_data3_out             <= left_nt_wire;
                        // left_nt_fifo_rden_reg           <= 1'b1;
                    end
                    else begin
                        top_portw_data0_out             <= res_added_samples_arr_wire[0][3];
                        top_portw_data1_out             <= res_added_samples_arr_wire[1][3];
                        top_portw_data2_out             <= res_added_samples_arr_wire[2][3];
                        top_portw_data3_out             <= res_added_samples_arr_wire[3][3];
                        // left_nt_fifo_rden_reg           <= 1'b0;
                    end

                    top_portw_en_mask_out           <= 8'b0000_1111;
                end
                else begin
                    top_portw_en_mask_out   <= 8'b0000_0000;
                end

                if(last_col_in) begin
                    left_portw_addr_out             <= res_added_4by4_y_in;

                    if(block_end_reg) begin
                        left_portw_xaddr_out            <= res_added_4by4_x_in;
                    end
                    else begin
                        left_portw_xaddr_out            <= threatned_x_reg;
                    end


                    if(last_row_in) begin
                        block_end_reg <= 1'b1;
                    end


                    if((constrained_intra_pred_flag_reg == 1'b1)&&(intra_inter_mode_in == MODE_INTER)) begin
                        left_portw_data0_out            <= top_nt_wire;
                        left_portw_data1_out            <= top_nt_wire;
                        left_portw_data2_out            <= top_nt_wire;
                        left_portw_data3_out            <= top_nt_wire;
                        // top_nt_fifo_rden_reg            <= 1'b1;
                    end
                    else begin
                        left_portw_data0_out            <= res_added_samples_arr_wire[3][0];
                        left_portw_data1_out            <= res_added_samples_arr_wire[3][1];
                        left_portw_data2_out            <= res_added_samples_arr_wire[3][2];
                        left_portw_data3_out            <= res_added_samples_arr_wire[3][3];
                        // top_nt_fifo_rden_reg            <= 1'b0;
                    end

                    if((constrained_intra_pred_flag_reg == 1'b1)&&(intra_inter_mode_in == MODE_INTRA)) begin
                        if(state == STATE_DONE) begin
                            case(log2_ctusize_reg)
                                3'd5 : begin
                                    if(res_added_4by4_x_in[5 - 1:0] == 5'd28) begin
                                        state <= STATE_WRITEUP_FIRST32;
                                        upwrite_counter_reg <= left_buffer_prior_inter_sum_first32_reg;
                                        upwrite_data_reg    <= res_added_samples_arr_wire[3][0];
                                        upwrite_addr        <= res_added_4by4_y_in - 3'd4;
                                    end
                                end
                                3'd6 : begin
                                    if(res_added_4by4_x_in[6 - 1:0] == 6'd28) begin
                                        state <= STATE_WRITEUP_FIRST32;
                                        upwrite_counter_reg <= left_buffer_prior_inter_sum_first32_reg;
                                        upwrite_data_reg    <= res_added_samples_arr_wire[3][0];
                                        upwrite_addr        <= res_added_4by4_y_in - 3'd4;
                                    end
                                    else if (res_added_4by4_x_in[6 - 1:0] == 6'd60) begin
                                        state <= STATE_WRITEUP_SECOND32;
                                        upwrite_counter_reg <= left_buffer_prior_inter_sum_second32_reg;
                                        upwrite_data_reg    <= res_added_samples_arr_wire[3][0];
                                        upwrite_addr        <= res_added_4by4_y_in - 3'd4;
                                    end
                                end
                                default /*3'd4 and others*/ begin

                                end
                            endcase
                        end
                    end

                    left_portw_en_mask_out          <= 8'b0000_1111;
                end
                else begin
                    left_portw_en_mask_out          <= 8'b0000_0000;
                end
            end
            else begin
                case(state)
                    STATE_WRITEUP_FIRST32,STATE_WRITEUP_SECOND32 : begin
                        if(upwrite_counter_reg == 4'd0) begin
                            state <= STATE_DONE;
                            left_portw_en_mask_out  <= 8'b0000_0000;
                        end
                        else begin
                            upwrite_counter_reg <= upwrite_counter_reg - 1'b1;
                            upwrite_addr        <= upwrite_addr - 3'd4;
                            left_portw_addr_out <= upwrite_addr;

                            left_portw_data0_out    <= upwrite_data_reg;
                            left_portw_data1_out    <= upwrite_data_reg;
                            left_portw_data2_out    <= upwrite_data_reg;
                            left_portw_data3_out    <= upwrite_data_reg;
                            left_portw_en_mask_out  <= 8'b0000_1111;
                        end
                    end
                    STATE_DONE : begin
                        left_portw_en_mask_out              <= 8'b0000_0000;
                    end
                endcase
            top_portw_en_mask_out               <= 8'b0000_0000;
            end
        end
    end

    //simulation assertions

        // synthesis translate_off
        always@(*) begin
            if(top_nt_fifo_full_wire & nt_pixels_valid_in) begin
                $display("writeback module : top_nt_pixels_fifo_overflow detected!!");
                $stop;
            end

            if(left_nt_fifo_full_wire & nt_pixels_valid_in) begin
                $display("writeback module : left_nt_pixels_fifo_oveflow_detected!!");
                $stop;
            end

            if(top_nt_fifo_empty_wire & top_nt_fifo_rden_reg) begin
                $display("writeback module : top_nt_pixels_fifo_underflow detected!!");
                $stop;
            end

            if(left_nt_fifo_empty_wire & left_nt_fifo_rden_reg) begin
                $display("writeback module : left_nt_pixels_fifo_underflow detected!!");
                $stop;
            end
        end
        // synthesis translate_on




    always @(posedge clk or posedge reset) begin : template_async_reset

    end

    always @(posedge clk) begin : template_sync_reset

    end

    always @(*) begin : template_combinatorial_logic

    end






endmodule