
// ************************************************************************************************
//
// PROJECT      :   <project name where applicable>
// PRODUCT      :   <product name where applicable>
// FILE         :   <File Name/Module Name>
// AUTHOR       :   <Author's name>
// DESCRIPTION  :   <at least a short description about the module/file>
//                  Where does this file get inputs and send outputs?
//                  What does the guts of this file accomplish, and how does it do it?
//                  What module(s) does this file instantiate?
//
// ************************************************************************************************
//
// REVISIONS:
//
//	Date			Developer	Description
//	----			---------	-----------
//  29 Feb 2012		user123     bug fix - <jira issue id>
//  31 Oct 2007		userxyz		added xyz functionality - <jira issue id>
//  ...              ...         ...
//  ...              ...         ...
//
//**************************************************************************************************

`timescale 1ns / 1ps

module truedual_port_blockram
    (
        clk,


        porta_en_in,
        porta_addr_in,
        porta_wen_in,
        porta_wdata_in,
        porta_rdata_out,

        portb_en_in,
        portb_addr_in,
        portb_wen_in,
        portb_wdata_in,
        portb_rdata_out,
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
  //  `include global_defs.v

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                       DATA_WIDTH          = 8;
    parameter                       ADDR_WIDTH          = 3;

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

    localparam                      MEM_SIZE            = 1 << ADDR_WIDTH;



//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                               clk;

    input                               porta_en_in;
    input   [ADDR_WIDTH - 1:0]          porta_addr_in;
    input                               porta_wen_in;
    input   [DATA_WIDTH - 1:0]          porta_wdata_in;
    output reg  [DATA_WIDTH - 1:0]      porta_rdata_out;

    input                               portb_en_in;
    input   [ADDR_WIDTH - 1:0]          portb_addr_in;
    input                               portb_wen_in;
    input   [DATA_WIDTH - 1:0]          portb_wdata_in;
    output reg  [DATA_WIDTH - 1:0]      portb_rdata_out;




//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

    reg     [0:MEM_SIZE - 1]            ram;


//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------

    always @(posedge clk) begin
        if (porta_en_in) begin
            if (porta_wen_in) begin
                ram[porta_addr_in] <= porta_wdata_in;
            end
            porta_rdata_out <= ram[porta_addr_in];
        end
    end

    always @(posedge clk) begin
        if (portb_en_in) begin
            if (portb_wen_in)
                ram[portb_addr_in] <= portb_wdata_in;
            portb_rdata_out <= ram[portb_addr_in];
        end
    end


endmodule