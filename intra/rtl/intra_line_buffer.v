`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    07:44:01 10/17/2013
// Design Name:
// Module Name:    intra_line_buffer
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module intra_line_buffer
    (
        clk,
        reset,
        enable,

        portw_addr,
        portw_data0,
        portw_data1,
        portw_data2,
        portw_data3,
        portw_data4,
        portw_data5,
        portw_data6,
        portw_data7,
        portw_en_mask,

        portw_prev_data0,
        portw_prev_data1,
        portw_prev_data2,
        portw_prev_data3,
        portw_prev_data4,
        portw_prev_data5,
        portw_prev_data6,
        portw_prev_data7,

        portr_addr,
        portr_data0,
        portr_data1,
        portr_data2,
        portr_data3,
        portr_data4,
        portr_data5,
        portr_data6,
        portr_data7,
        // portr_en,

        max_valid_range_in,
        min_valid_range_in
    );

//----------------------------------------------------
//  PARAMETERS
//----------------------------------------------------
    parameter                       LOG2_FRAME_SIZE         = 12;
//----------------------------------------------------
//LOCALPARAMS
//----------------------------------------------------
    localparam                      INTRA_PRED_ANGLE_WIDTH  = 6;
    localparam                      MAX_NTBS_SIZE           = 6;
    localparam                      NUM_OF_DATA_REGS        = 5;
    localparam                      PIXEL_WIDTH             = 8;

    localparam                      LOG2_NUM_OF_BANKS       = 3;
    localparam                      NUM_OF_BANKS            = 1 << LOG2_NUM_OF_BANKS;


//----------------------------------------------------
//I/O Signals
//----------------------------------------------------

    input                                   clk;
    input                                   reset;
    input                                   enable;

    input       [LOG2_FRAME_SIZE - 1:0]     portw_addr;
    input       [PIXEL_WIDTH - 1:0]         portw_data0;
    input       [PIXEL_WIDTH - 1:0]         portw_data1;
    input       [PIXEL_WIDTH - 1:0]         portw_data2;
    input       [PIXEL_WIDTH - 1:0]         portw_data3;
    input       [PIXEL_WIDTH - 1:0]         portw_data4;
    input       [PIXEL_WIDTH - 1:0]         portw_data5;
    input       [PIXEL_WIDTH - 1:0]         portw_data6;
    input       [PIXEL_WIDTH - 1:0]         portw_data7;
    input       [PIXEL_WIDTH - 1:0]         portw_en_mask;

    input       [LOG2_FRAME_SIZE - 1:0]     portr_addr;
    output reg  [PIXEL_WIDTH - 1:0]         portr_data0;
    output reg  [PIXEL_WIDTH - 1:0]         portr_data1;
    output reg  [PIXEL_WIDTH - 1:0]         portr_data2;
    output reg  [PIXEL_WIDTH - 1:0]         portr_data3;
    output reg  [PIXEL_WIDTH - 1:0]         portr_data4;
    output reg  [PIXEL_WIDTH - 1:0]         portr_data5;
    output reg  [PIXEL_WIDTH - 1:0]         portr_data6;
    output reg  [PIXEL_WIDTH - 1:0]         portr_data7;
    // input                                   portr_en;

    output reg [PIXEL_WIDTH - 1:0]             portw_prev_data0;
    output reg [PIXEL_WIDTH - 1:0]             portw_prev_data1;
    output reg [PIXEL_WIDTH - 1:0]             portw_prev_data2;
    output reg [PIXEL_WIDTH - 1:0]             portw_prev_data3;
    output reg [PIXEL_WIDTH - 1:0]             portw_prev_data4;
    output reg [PIXEL_WIDTH - 1:0]             portw_prev_data5;
    output reg [PIXEL_WIDTH - 1:0]             portw_prev_data6;
    output reg [PIXEL_WIDTH - 1:0]             portw_prev_data7;

    input       [LOG2_FRAME_SIZE - 1:0]     max_valid_range_in;
    input       [LOG2_FRAME_SIZE - 1:0]     min_valid_range_in;


//----------------------------------------------------
//  Internal Regs and Wires
//----------------------------------------------------
    reg         [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]   membank0w_addr;
    reg         [PIXEL_WIDTH - 1:0]                           membank0w_data;
    wire        [PIXEL_WIDTH - 1:0]                           membank0w_prev_data;
    // reg                                                       membank0w_en;
    reg         [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]   membank0r_addr;
    wire        [PIXEL_WIDTH - 1:0]                           membank0r_data;
    //reg                                                       membank0r_en;

    reg         [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]   membank1w_addr;
    reg         [PIXEL_WIDTH - 1:0]                           membank1w_data;
    wire        [PIXEL_WIDTH - 1:0]                           membank1w_prev_data;
    // reg                                                       membank1w_en;
    reg         [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]   membank1r_addr;
    wire        [PIXEL_WIDTH - 1:0]                           membank1r_data;
    //reg                                                       membank1r_en;

    reg         [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]   membank2w_addr;
    reg         [PIXEL_WIDTH - 1:0]                           membank2w_data;
    wire        [PIXEL_WIDTH - 1:0]                           membank2w_prev_data;
    // reg                                                       membank2w_en;
    reg         [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]   membank2r_addr;
    wire        [PIXEL_WIDTH - 1:0]                           membank2r_data;
    //reg                                                       membank2r_en;

    reg         [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]   membank3w_addr;
    reg         [PIXEL_WIDTH - 1:0]                           membank3w_data;
    wire        [PIXEL_WIDTH - 1:0]                           membank3w_prev_data;
    // reg                                                       membank3w_en;
    reg         [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]   membank3r_addr;
    wire        [PIXEL_WIDTH - 1:0]                           membank3r_data;
    //reg                                                       membank3r_en;

    reg         [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]   membank4w_addr;
    reg         [PIXEL_WIDTH - 1:0]                           membank4w_data;
    wire        [PIXEL_WIDTH - 1:0]                           membank4w_prev_data;
    // reg                                                       membank4w_en;
    reg         [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]   membank4r_addr;
    wire        [PIXEL_WIDTH - 1:0]                           membank4r_data;
    //reg                                                       membank4r_en;

    reg         [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]   membank5w_addr;
    reg         [PIXEL_WIDTH - 1:0]                           membank5w_data;
    wire        [PIXEL_WIDTH - 1:0]                           membank5w_prev_data;
    // reg                                                       membank5w_en;
    reg         [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]   membank5r_addr;
    wire        [PIXEL_WIDTH - 1:0]                           membank5r_data;
    //reg                                                       membank5r_en;

    reg         [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]   membank6w_addr;
    reg         [PIXEL_WIDTH - 1:0]                           membank6w_data;
    wire        [PIXEL_WIDTH - 1:0]                           membank6w_prev_data;
    // reg                                                       membank6w_en;
    reg         [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]   membank6r_addr;
    wire        [PIXEL_WIDTH - 1:0]                           membank6r_data;
    //reg                                                       membank6r_en;

    reg         [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]   membank7w_addr;
    reg         [PIXEL_WIDTH - 1:0]                           membank7w_data;
    wire        [PIXEL_WIDTH - 1:0]                           membank7w_prev_data;
    // reg                                                       membank7w_en;
    reg         [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]   membank7r_addr;
    wire        [PIXEL_WIDTH - 1:0]                           membank7r_data;
    //reg                                                       membank7r_en;

    reg         [NUM_OF_BANKS - 1:0]                          membanksw_en;
    // reg         [NUM_OF_BANKS - 1:0]                          membanksr_en;

    reg         [LOG2_FRAME_SIZE - 1:0]                       max_valid_sub_portr_addr;
    reg         [LOG2_FRAME_SIZE - 1:0]                       min_valid_sub_portr_addr;
    reg         [3 - 1:0]                                     portr_addr_reg;

    //constrained mod
    reg         [LOG2_FRAME_SIZE - 1:0]                       portr_addr_clipped;
    reg         [PIXEL_WIDTH - 1:0]                           membank0r_data_min;
    reg         [PIXEL_WIDTH - 1:0]                           membank1r_data_min;
    reg         [PIXEL_WIDTH - 1:0]                           membank2r_data_min;
    reg         [PIXEL_WIDTH - 1:0]                           membank3r_data_min;
    reg         [PIXEL_WIDTH - 1:0]                           membank4r_data_min;
    reg         [PIXEL_WIDTH - 1:0]                           membank5r_data_min;
    reg         [PIXEL_WIDTH - 1:0]                           membank6r_data_min;
    reg         [PIXEL_WIDTH - 1:0]                           membank7r_data_min;

    wire         [LOG2_FRAME_SIZE - 1:0]                      portr_addr_plus_8;

    reg         [LOG2_FRAME_SIZE - 1:0]                       portw_addr_reg;





//----------------------------------------------------
//  Implementation
//----------------------------------------------------

    assign portr_addr_plus_8 = portr_addr + 4'd8;

    always @(*) begin
        if(portr_addr_plus_8 <= min_valid_range_in) begin
            portr_addr_clipped = min_valid_range_in;
        end
        else begin
            portr_addr_clipped = portr_addr;
        end
    end

    intra_line_buffer_membank
    #(
        .LOG2_FRAME_SIZE(LOG2_FRAME_SIZE)
    )
    intra_line_buffer_membank0(
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),

        .portw_addr         (membank0w_addr),
        .portw_data         (membank0w_data),
        .portw_prev_data    (membank0w_prev_data),
        .portw_en           (membanksw_en[0]),
        .portr2_addr        (membank0w_addr),

        .portr_addr         (membank0r_addr),
        .portr_data         (membank0r_data)
        // .portr_en           (membanksr_en[0])
    );

    intra_line_buffer_membank
    #(
        .LOG2_FRAME_SIZE(LOG2_FRAME_SIZE)
    )
    intra_line_buffer_membank1(
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),

        .portw_addr         (membank1w_addr),
        .portw_data         (membank1w_data),
        .portw_prev_data    (membank1w_prev_data),
        .portw_en           (membanksw_en[1]),
        .portr2_addr        (membank1w_addr),

        .portr_addr         (membank1r_addr),
        .portr_data         (membank1r_data)
        // .portr_en           (membanksr_en[1])
    );

    intra_line_buffer_membank
    #(
        .LOG2_FRAME_SIZE(LOG2_FRAME_SIZE)
    )
    intra_line_buffer_membank2(
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),

        .portw_addr         (membank2w_addr),
        .portw_data         (membank2w_data),
        .portw_prev_data    (membank2w_prev_data),
        .portw_en           (membanksw_en[2]),
        .portr2_addr        (membank2w_addr),

        .portr_addr         (membank2r_addr),
        .portr_data         (membank2r_data)
        // .portr_en           (membanksr_en[2])
    );

    intra_line_buffer_membank
    #(
        .LOG2_FRAME_SIZE(LOG2_FRAME_SIZE)
    )
    intra_line_buffer_membank3(
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),

        .portw_addr         (membank3w_addr),
        .portw_data         (membank3w_data),
        .portw_prev_data    (membank3w_prev_data),
        .portw_en           (membanksw_en[3]),
        .portr2_addr        (membank3w_addr),

        .portr_addr         (membank3r_addr),
        .portr_data         (membank3r_data)
        // .portr_en           (membanksr_en[3])
    );

    intra_line_buffer_membank
    #(
        .LOG2_FRAME_SIZE(LOG2_FRAME_SIZE)
    )
    intra_line_buffer_membank4(
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),

        .portw_addr         (membank4w_addr),
        .portw_data         (membank4w_data),
        .portw_prev_data    (membank4w_prev_data),
        .portw_en           (membanksw_en[4]),
        .portr2_addr        (membank4w_addr),

        .portr_addr         (membank4r_addr),
        .portr_data         (membank4r_data)
        // .portr_en           (membanksr_en[4])
    );

    intra_line_buffer_membank
    #(
        .LOG2_FRAME_SIZE(LOG2_FRAME_SIZE)
    )
    intra_line_buffer_membank5(
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),

        .portw_addr         (membank5w_addr),
        .portw_data         (membank5w_data),
        .portw_prev_data    (membank5w_prev_data),
        .portw_en           (membanksw_en[5]),
        .portr2_addr        (membank5w_addr),


        .portr_addr         (membank5r_addr),
        .portr_data         (membank5r_data)
        // .portr_en           (membanksr_en[5])
    );

    intra_line_buffer_membank
    #(
        .LOG2_FRAME_SIZE(LOG2_FRAME_SIZE)
    )
    intra_line_buffer_membank6(
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),

        .portw_addr         (membank6w_addr),
        .portw_data         (membank6w_data),
        .portw_prev_data    (membank6w_prev_data),
        .portw_en           (membanksw_en[6]),
        .portr2_addr        (membank6w_addr),

        .portr_addr         (membank6r_addr),
        .portr_data         (membank6r_data)
        // .portr_en           (membanksr_en[6])
    );

    intra_line_buffer_membank
    #(
        .LOG2_FRAME_SIZE(LOG2_FRAME_SIZE)
    )
    intra_line_buffer_membank7(
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),

        .portw_addr         (membank7w_addr),
        .portw_data         (membank7w_data),
        .portw_prev_data    (membank7w_prev_data),
        .portw_en           (membanksw_en[7]),
        .portr2_addr        (membank7w_addr),

        .portr_addr         (membank7r_addr),
        .portr_data         (membank7r_data)
        // .portr_en           (membanksr_en[7])
    );

    always @(posedge clk) begin
        portw_addr_reg <= portw_addr;
    end

    always @(*) begin : write_prevdata_mux
        case(portw_addr_reg[2:0])
            3'd0 : begin
                portw_prev_data0 = membank0w_prev_data; 
                portw_prev_data1 = membank1w_prev_data;
                portw_prev_data2 = membank2w_prev_data;
                portw_prev_data3 = membank3w_prev_data;
                portw_prev_data4 = membank4w_prev_data;
                portw_prev_data5 = membank5w_prev_data;
                portw_prev_data6 = membank6w_prev_data;
                portw_prev_data7 = membank7w_prev_data;
            end
            3'd1 : begin
                portw_prev_data0 = membank7w_prev_data; 
                portw_prev_data1 = membank0w_prev_data;
                portw_prev_data2 = membank1w_prev_data;
                portw_prev_data3 = membank2w_prev_data;
                portw_prev_data4 = membank3w_prev_data;
                portw_prev_data5 = membank4w_prev_data;
                portw_prev_data6 = membank5w_prev_data;
                portw_prev_data7 = membank6w_prev_data;
            end
            3'd2 : begin
                portw_prev_data0 = membank6w_prev_data; 
                portw_prev_data1 = membank7w_prev_data;
                portw_prev_data2 = membank0w_prev_data;
                portw_prev_data3 = membank1w_prev_data;
                portw_prev_data4 = membank2w_prev_data;
                portw_prev_data5 = membank3w_prev_data;
                portw_prev_data6 = membank4w_prev_data;
                portw_prev_data7 = membank5w_prev_data;
            end

            3'd3 : begin
                portw_prev_data0 = membank5w_prev_data; 
                portw_prev_data1 = membank6w_prev_data;
                portw_prev_data2 = membank7w_prev_data;
                portw_prev_data3 = membank0w_prev_data;
                portw_prev_data4 = membank1w_prev_data;
                portw_prev_data5 = membank2w_prev_data;
                portw_prev_data6 = membank3w_prev_data;
                portw_prev_data7 = membank4w_prev_data;
            end

            3'd4 : begin
                portw_prev_data0 = membank4w_prev_data; 
                portw_prev_data1 = membank5w_prev_data;
                portw_prev_data2 = membank6w_prev_data;
                portw_prev_data3 = membank7w_prev_data;
                portw_prev_data4 = membank0w_prev_data;
                portw_prev_data5 = membank1w_prev_data;
                portw_prev_data6 = membank2w_prev_data;
                portw_prev_data7 = membank3w_prev_data;
            end

            3'd5 : begin
                portw_prev_data0 = membank3w_prev_data; 
                portw_prev_data1 = membank4w_prev_data;
                portw_prev_data2 = membank5w_prev_data;
                portw_prev_data3 = membank6w_prev_data;
                portw_prev_data4 = membank7w_prev_data;
                portw_prev_data5 = membank0w_prev_data;
                portw_prev_data6 = membank1w_prev_data;
                portw_prev_data7 = membank2w_prev_data;
            end

            3'd6 : begin
                portw_prev_data0 = membank2w_prev_data; 
                portw_prev_data1 = membank3w_prev_data;
                portw_prev_data2 = membank4w_prev_data;
                portw_prev_data3 = membank5w_prev_data;
                portw_prev_data4 = membank6w_prev_data;
                portw_prev_data5 = membank7w_prev_data;
                portw_prev_data6 = membank0w_prev_data;
                portw_prev_data7 = membank1w_prev_data;
            end
            3'd7 : begin
                portw_prev_data0 = membank1w_prev_data; 
                portw_prev_data1 = membank2w_prev_data;
                portw_prev_data2 = membank3w_prev_data;
                portw_prev_data3 = membank4w_prev_data;
                portw_prev_data4 = membank5w_prev_data;
                portw_prev_data5 = membank6w_prev_data;
                portw_prev_data6 = membank7w_prev_data;
                portw_prev_data7 = membank0w_prev_data;
            end
        endcase
    end

    always @(*) begin : write_mux
        case(portw_addr[2:0])
            3'd0 : begin
                membanksw_en    = portw_en_mask;//8'b00011111;

                membank0w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank1w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank2w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank3w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank4w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank5w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank6w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank7w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                membank0w_data  = portw_data0;
                membank1w_data  = portw_data1;
                membank2w_data  = portw_data2;
                membank3w_data  = portw_data3;
                membank4w_data  = portw_data4;
                membank5w_data  = portw_data5;
                membank6w_data  = portw_data6;
                membank7w_data  = portw_data7;

            end
            3'd1 : begin
                membanksw_en     = {portw_en_mask[PIXEL_WIDTH - 2:0],portw_en_mask[PIXEL_WIDTH-1]};//{2'b00,portw_en_mask,1'b0};//8'b00111110;

                membank0w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank1w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank2w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank3w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank4w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank5w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank6w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank7w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                membank0w_data  = portw_data7;
                membank1w_data  = portw_data0;
                membank2w_data  = portw_data1;
                membank3w_data  = portw_data2;
                membank4w_data  = portw_data3;
                membank5w_data  = portw_data4;
                membank6w_data  = portw_data5;
                membank7w_data  = portw_data6;
            end

            3'd2 : begin
                membanksw_en    = {portw_en_mask[PIXEL_WIDTH - 3:0],portw_en_mask[PIXEL_WIDTH-1:PIXEL_WIDTH-2]};//{1'b0,portw_en_mask,2'b00};//8'b01111100;

                membank0w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank1w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank2w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank3w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank4w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank5w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank6w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank7w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                membank0w_data  = portw_data6;
                membank1w_data  = portw_data7;
                membank2w_data  = portw_data0;
                membank3w_data  = portw_data1;
                membank4w_data  = portw_data2;
                membank5w_data  = portw_data3;
                membank6w_data  = portw_data4;
                membank7w_data  = portw_data5;

            end

            3'd3 : begin
                membanksw_en    = {portw_en_mask[PIXEL_WIDTH - 4:0],portw_en_mask[PIXEL_WIDTH-1:PIXEL_WIDTH-3]};//{portw_en_mask,3'b000};//8'b11111000;

                membank0w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank1w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank2w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank3w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank4w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank5w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank6w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank7w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                membank0w_data  = portw_data5;
                membank1w_data  = portw_data6;
                membank2w_data  = portw_data7;
                membank3w_data  = portw_data0;
                membank4w_data  = portw_data1;
                membank5w_data  = portw_data2;
                membank6w_data  = portw_data3;
                membank7w_data  = portw_data4;

            end

            3'd4 : begin
                membanksw_en    = {portw_en_mask[PIXEL_WIDTH - 5:0],portw_en_mask[PIXEL_WIDTH-1:PIXEL_WIDTH-4]};//{portw_en_mask[3:0],3'b000,portw_en_mask[0]};//8'b11110001;

                membank0w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank1w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank2w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank3w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank4w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank5w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank6w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank7w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                membank0w_data  = portw_data4;
                membank1w_data  = portw_data5;
                membank2w_data  = portw_data6;
                membank3w_data  = portw_data7;
                membank4w_data  = portw_data0;
                membank5w_data  = portw_data1;
                membank6w_data  = portw_data2;
                membank7w_data  = portw_data3;

            end

            3'd5 : begin
                membanksw_en    = {portw_en_mask[PIXEL_WIDTH - 6:0],portw_en_mask[PIXEL_WIDTH-1:PIXEL_WIDTH - 5]};//{portw_en_mask[2:0],3'b000,portw_en_mask[4:3]};//8'b11100011;

                membank0w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank1w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank2w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank3w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank4w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank5w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank6w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank7w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                membank0w_data  = portw_data3;
                membank1w_data  = portw_data4;
                membank2w_data  = portw_data5;
                membank3w_data  = portw_data6;
                membank4w_data  = portw_data7;
                membank5w_data  = portw_data0;
                membank6w_data  = portw_data1;
                membank7w_data  = portw_data2;

            end

            3'd6 : begin
                membanksw_en    = {portw_en_mask[PIXEL_WIDTH - 7:0],portw_en_mask[PIXEL_WIDTH-1:PIXEL_WIDTH - 6]};//{portw_en_mask[1:0],3'b000,portw_en_mask[4:2]};//8'b11000111;

                membank0w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank1w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank2w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank3w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank4w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank5w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank6w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank7w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                membank0w_data  = portw_data2;
                membank1w_data  = portw_data3;
                membank2w_data  = portw_data4;
                membank3w_data  = portw_data5;
                membank4w_data  = portw_data6;
                membank5w_data  = portw_data7;
                membank6w_data  = portw_data0;
                membank7w_data  = portw_data1;

            end

            3'd7 : begin
                membanksw_en    = {portw_en_mask[0],portw_en_mask[PIXEL_WIDTH-1:PIXEL_WIDTH - 7]};//{portw_en_mask[0],3'b000,portw_en_mask[4:1]};//8'b10001111;

                membank0w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank1w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank2w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank3w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank4w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank5w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank6w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank7w_addr  = portw_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                membank0w_data  = portw_data1;
                membank1w_data  = portw_data2;
                membank2w_data  = portw_data3;
                membank3w_data  = portw_data4;
                membank4w_data  = portw_data5;
                membank5w_data  = portw_data6;
                membank6w_data  = portw_data7;
                membank7w_data  = portw_data0;
            end
        endcase
    end

    always @(posedge clk) begin

        max_valid_sub_portr_addr <= max_valid_range_in - portr_addr_clipped;

        if(portr_addr_plus_8 <= min_valid_range_in) begin
            min_valid_sub_portr_addr <= {LOG2_FRAME_SIZE{1'b0}};
        end
        else begin
            min_valid_sub_portr_addr <= portr_addr_plus_8 - min_valid_range_in;
        end

        portr_addr_reg           <= portr_addr_clipped[3 - 1:0];
    end


    // assign max_valid_sub_portr_addr = max_valid_range_in - portr_addr;

    always @(*) begin : read_mux
        case(portr_addr_clipped[2:0])
            3'd0 : begin
                // membanksr_en    = {8{portr_en}};//8'b00011111 & {8{portr_en}};

                membank0r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank1r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank2r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank3r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank4r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank5r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank6r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank7r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

               //  case(max_valid_sub_portr_addr)
               //      12'd0 : begin
               //          portr_data0     = membank0r_data;
               //          portr_data1     = membank0r_data;
               //          portr_data2     = membank0r_data;
               //          portr_data3     = membank0r_data;
               //          portr_data4     = membank0r_data;
               //          portr_data5     = membank0r_data;
               //          portr_data6     = membank0r_data;
               //          portr_data7     = membank0r_data;
               //      end
               //     12'd1 : begin
               //          portr_data0     = membank0r_data;
               //          portr_data1     = membank1r_data;
               //          portr_data2     = membank1r_data;
               //          portr_data3     = membank1r_data;
               //          portr_data4     = membank1r_data;
               //          portr_data5     = membank1r_data;
               //          portr_data6     = membank1r_data;
               //          portr_data7     = membank1r_data;
               //      end
               //     12'd2 : begin
               //          portr_data0     = membank0r_data;
               //          portr_data1     = membank1r_data;
               //          portr_data2     = membank2r_data;
               //          portr_data3     = membank2r_data;
               //          portr_data4     = membank2r_data;
               //          portr_data5     = membank2r_data;
               //          portr_data6     = membank2r_data;
               //          portr_data7     = membank2r_data;
               //     end
               //     12'd3 : begin
               //          portr_data0     = membank0r_data;
               //          portr_data1     = membank1r_data;
               //          portr_data2     = membank2r_data;
               //          portr_data3     = membank3r_data;
               //          portr_data4     = membank3r_data;
               //          portr_data5     = membank3r_data;
               //          portr_data6     = membank3r_data;
               //          portr_data7     = membank3r_data;
               //     end
               //     12'd4 : begin
               //          portr_data0     = membank0r_data;
               //          portr_data1     = membank1r_data;
               //          portr_data2     = membank2r_data;
               //          portr_data3     = membank3r_data;
               //          portr_data4     = membank4r_data;
               //          portr_data5     = membank4r_data;
               //          portr_data6     = membank4r_data;
               //          portr_data7     = membank4r_data;
               //     end
               //     12'd5 : begin
               //          portr_data0     = membank0r_data;
               //          portr_data1     = membank1r_data;
               //          portr_data2     = membank2r_data;
               //          portr_data3     = membank3r_data;
               //          portr_data4     = membank4r_data;
               //          portr_data5     = membank5r_data;
               //          portr_data6     = membank5r_data;
               //          portr_data7     = membank5r_data;
               //     end
               //     12'd6 : begin
               //          portr_data0     = membank0r_data;
               //          portr_data1     = membank1r_data;
               //          portr_data2     = membank2r_data;
               //          portr_data3     = membank3r_data;
               //          portr_data4     = membank4r_data;
               //          portr_data5     = membank5r_data;
               //          portr_data6     = membank6r_data;
               //          portr_data7     = membank6r_data;
               //     end
               //     default: begin
               //          portr_data0     = membank0r_data;
               //          portr_data1     = membank1r_data;
               //          portr_data2     = membank2r_data;
               //          portr_data3     = membank3r_data;
               //          portr_data4     = membank4r_data;
               //          portr_data5     = membank5r_data;
               //          portr_data6     = membank6r_data;
               //          portr_data7     = membank7r_data;
               //     end
               // endcase
            end
            3'd1 : begin
                // membanksr_en    = {8{portr_en}};//8'b00111110 & {8{portr_en}};

                membank0r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank1r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank2r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank3r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank4r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank5r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank6r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank7r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                // case(max_valid_sub_portr_addr)
                //     12'd0 : begin
                //         portr_data0     =  membank1r_data;
                //         portr_data1     =  membank1r_data;
                //         portr_data2     =  membank1r_data;
                //         portr_data3     =  membank1r_data;
                //         portr_data4     =  membank1r_data;
                //         portr_data5     =  membank1r_data;
                //         portr_data6     =  membank1r_data;
                //         portr_data7     =  membank1r_data;
                //     end
                //     12'd1 : begin
                //         portr_data0     =  membank1r_data;
                //         portr_data1     =  membank2r_data;
                //         portr_data2     =  membank2r_data;
                //         portr_data3     =  membank2r_data;
                //         portr_data4     =  membank2r_data;
                //         portr_data5     =  membank2r_data;
                //         portr_data6     =  membank2r_data;
                //         portr_data7     =  membank2r_data;
                //     end
                //     12'd2 : begin
                //         portr_data0     =  membank1r_data;
                //         portr_data1     =  membank2r_data;
                //         portr_data2     =  membank3r_data;
                //         portr_data3     =  membank3r_data;
                //         portr_data4     =  membank3r_data;
                //         portr_data5     =  membank3r_data;
                //         portr_data6     =  membank3r_data;
                //         portr_data7     =  membank3r_data;
                //     end
                //     12'd3 : begin
                //         portr_data0     =  membank1r_data;
                //         portr_data1     =  membank2r_data;
                //         portr_data2     =  membank3r_data;
                //         portr_data3     =  membank4r_data;
                //         portr_data4     =  membank4r_data;
                //         portr_data5     =  membank4r_data;
                //         portr_data6     =  membank4r_data;
                //         portr_data7     =  membank4r_data;
                //     end
                //     12'd4 : begin
                //         portr_data0     =  membank1r_data;
                //         portr_data1     =  membank2r_data;
                //         portr_data2     =  membank3r_data;
                //         portr_data3     =  membank4r_data;
                //         portr_data4     =  membank5r_data;
                //         portr_data5     =  membank5r_data;
                //         portr_data6     =  membank5r_data;
                //         portr_data7     =  membank5r_data;
                //     end
                //     12'd5 : begin
                //         portr_data0     =  membank1r_data;
                //         portr_data1     =  membank2r_data;
                //         portr_data2     =  membank3r_data;
                //         portr_data3     =  membank4r_data;
                //         portr_data4     =  membank5r_data;
                //         portr_data5     =  membank6r_data;
                //         portr_data6     =  membank6r_data;
                //         portr_data7     =  membank6r_data;
                //     end
                //     12'd6 : begin
                //         portr_data0     =  membank1r_data;
                //         portr_data1     =  membank2r_data;
                //         portr_data2     =  membank3r_data;
                //         portr_data3     =  membank4r_data;
                //         portr_data4     =  membank5r_data;
                //         portr_data5     =  membank6r_data;
                //         portr_data6     =  membank7r_data;
                //         portr_data7     =  membank7r_data;
                //     end
                //     default : begin
                //         portr_data0     =  membank1r_data;
                //         portr_data1     =  membank2r_data;
                //         portr_data2     =  membank3r_data;
                //         portr_data3     =  membank4r_data;
                //         portr_data4     =  membank5r_data;
                //         portr_data5     =  membank6r_data;
                //         portr_data6     =  membank7r_data;
                //         portr_data7     =  membank0r_data;
                //     end
                // endcase
            end

            3'd2 : begin
                // membanksr_en    = {8{portr_en}};//8'b01111100 & {8{portr_en}};

                membank0r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank1r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank2r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank3r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank4r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank5r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank6r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank7r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                // case(max_valid_sub_portr_addr)
                //     12'd0 : begin
                //         portr_data0     =  membank2r_data;
                //         portr_data1     =  membank2r_data;
                //         portr_data2     =  membank2r_data;
                //         portr_data3     =  membank2r_data;
                //         portr_data4     =  membank2r_data;
                //         portr_data5     =  membank2r_data;
                //         portr_data6     =  membank2r_data;
                //         portr_data7     =  membank2r_data;
                //     end
                //     12'd1 : begin
                //         portr_data0     =  membank2r_data;
                //         portr_data1     =  membank3r_data;
                //         portr_data2     =  membank3r_data;
                //         portr_data3     =  membank3r_data;
                //         portr_data4     =  membank3r_data;
                //         portr_data5     =  membank3r_data;
                //         portr_data6     =  membank3r_data;
                //         portr_data7     =  membank3r_data;
                //     end
                //     12'd2 : begin
                //         portr_data0     =  membank2r_data;
                //         portr_data1     =  membank3r_data;
                //         portr_data2     =  membank4r_data;
                //         portr_data3     =  membank4r_data;
                //         portr_data4     =  membank4r_data;
                //         portr_data5     =  membank4r_data;
                //         portr_data6     =  membank4r_data;
                //         portr_data7     =  membank4r_data;
                //     end
                //     12'd3 : begin
                //         portr_data0     =  membank2r_data;
                //         portr_data1     =  membank3r_data;
                //         portr_data2     =  membank4r_data;
                //         portr_data3     =  membank5r_data;
                //         portr_data4     =  membank5r_data;
                //         portr_data5     =  membank5r_data;
                //         portr_data6     =  membank5r_data;
                //         portr_data7     =  membank5r_data;
                //     end
                //     12'd4 : begin
                //         portr_data0     =  membank2r_data;
                //         portr_data1     =  membank3r_data;
                //         portr_data2     =  membank4r_data;
                //         portr_data3     =  membank5r_data;
                //         portr_data4     =  membank6r_data;
                //         portr_data5     =  membank6r_data;
                //         portr_data6     =  membank6r_data;
                //         portr_data7     =  membank6r_data;
                //     end
                //     12'd5 : begin
                //         portr_data0     =  membank2r_data;
                //         portr_data1     =  membank3r_data;
                //         portr_data2     =  membank4r_data;
                //         portr_data3     =  membank5r_data;
                //         portr_data4     =  membank6r_data;
                //         portr_data5     =  membank7r_data;
                //         portr_data6     =  membank7r_data;
                //         portr_data7     =  membank7r_data;
                //     end
                //     12'd6 : begin
                //         portr_data0     =  membank2r_data;
                //         portr_data1     =  membank3r_data;
                //         portr_data2     =  membank4r_data;
                //         portr_data3     =  membank5r_data;
                //         portr_data4     =  membank6r_data;
                //         portr_data5     =  membank7r_data;
                //         portr_data6     =  membank0r_data;
                //         portr_data7     =  membank0r_data;
                //     end
                //     default : begin
                //         portr_data0     =  membank2r_data;
                //         portr_data1     =  membank3r_data;
                //         portr_data2     =  membank4r_data;
                //         portr_data3     =  membank5r_data;
                //         portr_data4     =  membank6r_data;
                //         portr_data5     =  membank7r_data;
                //         portr_data6     =  membank0r_data;
                //         portr_data7     =  membank1r_data;
                //     end
                // endcase
            end

            3'd3 : begin
                // membanksr_en    = {8{portr_en}};//8'b11111000 & {8{portr_en}};

                membank0r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank1r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank2r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank3r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank4r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank5r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank6r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank7r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                // case(max_valid_sub_portr_addr)
                //     12'd0 : begin
                //         portr_data0     =  membank3r_data;
                //         portr_data1     =  membank3r_data;
                //         portr_data2     =  membank3r_data;
                //         portr_data3     =  membank3r_data;
                //         portr_data4     =  membank3r_data;
                //         portr_data5     =  membank3r_data;
                //         portr_data6     =  membank3r_data;
                //         portr_data7     =  membank3r_data;
                //     end
                //     12'd1 : begin
                //         portr_data0     =  membank3r_data;
                //         portr_data1     =  membank4r_data;
                //         portr_data2     =  membank4r_data;
                //         portr_data3     =  membank4r_data;
                //         portr_data4     =  membank4r_data;
                //         portr_data5     =  membank4r_data;
                //         portr_data6     =  membank4r_data;
                //         portr_data7     =  membank4r_data;
                //     end
                //     12'd2 : begin
                //         portr_data0     =  membank3r_data;
                //         portr_data1     =  membank4r_data;
                //         portr_data2     =  membank5r_data;
                //         portr_data3     =  membank5r_data;
                //         portr_data4     =  membank5r_data;
                //         portr_data5     =  membank5r_data;
                //         portr_data6     =  membank5r_data;
                //         portr_data7     =  membank5r_data;
                //     end
                //     12'd3 : begin
                //         portr_data0     =  membank3r_data;
                //         portr_data1     =  membank4r_data;
                //         portr_data2     =  membank5r_data;
                //         portr_data3     =  membank6r_data;
                //         portr_data4     =  membank6r_data;
                //         portr_data5     =  membank6r_data;
                //         portr_data6     =  membank6r_data;
                //         portr_data7     =  membank6r_data;
                //     end
                //     12'd4 : begin
                //         portr_data0     =  membank3r_data;
                //         portr_data1     =  membank4r_data;
                //         portr_data2     =  membank5r_data;
                //         portr_data3     =  membank6r_data;
                //         portr_data4     =  membank7r_data;
                //         portr_data5     =  membank7r_data;
                //         portr_data6     =  membank7r_data;
                //         portr_data7     =  membank7r_data;
                //     end
                //     12'd5 : begin
                //         portr_data0     =  membank3r_data;
                //         portr_data1     =  membank4r_data;
                //         portr_data2     =  membank5r_data;
                //         portr_data3     =  membank6r_data;
                //         portr_data4     =  membank7r_data;
                //         portr_data5     =  membank0r_data;
                //         portr_data6     =  membank0r_data;
                //         portr_data7     =  membank0r_data;
                //     end
                //     12'd6 : begin
                //         portr_data0     =  membank3r_data;
                //         portr_data1     =  membank4r_data;
                //         portr_data2     =  membank5r_data;
                //         portr_data3     =  membank6r_data;
                //         portr_data4     =  membank7r_data;
                //         portr_data5     =  membank0r_data;
                //         portr_data6     =  membank1r_data;
                //         portr_data7     =  membank1r_data;
                //     end
                //     default : begin
                //         portr_data0     =  membank3r_data;
                //         portr_data1     =  membank4r_data;
                //         portr_data2     =  membank5r_data;
                //         portr_data3     =  membank6r_data;
                //         portr_data4     =  membank7r_data;
                //         portr_data5     =  membank0r_data;
                //         portr_data6     =  membank1r_data;
                //         portr_data7     =  membank2r_data;
                //     end
                // endcase
            end

            3'd4 : begin
                // membanksr_en    = {8{portr_en}};//8'b11110001 & {8{portr_en}};

                membank0r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank1r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank2r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank3r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank4r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank5r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank6r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank7r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                // case(max_valid_sub_portr_addr)
                //     12'd0 : begin
                //         portr_data0     =  membank4r_data;
                //         portr_data1     =  membank4r_data;
                //         portr_data2     =  membank4r_data;
                //         portr_data3     =  membank4r_data;
                //         portr_data4     =  membank4r_data;
                //         portr_data5     =  membank4r_data;
                //         portr_data6     =  membank4r_data;
                //         portr_data7     =  membank4r_data;
                //     end
                //     12'd1 : begin
                //         portr_data0     =  membank4r_data;
                //         portr_data1     =  membank5r_data;
                //         portr_data2     =  membank5r_data;
                //         portr_data3     =  membank5r_data;
                //         portr_data4     =  membank5r_data;
                //         portr_data5     =  membank5r_data;
                //         portr_data6     =  membank5r_data;
                //         portr_data7     =  membank5r_data;
                //     end
                //     12'd2 : begin
                //         portr_data0     =  membank4r_data;
                //         portr_data1     =  membank5r_data;
                //         portr_data2     =  membank6r_data;
                //         portr_data3     =  membank6r_data;
                //         portr_data4     =  membank6r_data;
                //         portr_data5     =  membank6r_data;
                //         portr_data6     =  membank6r_data;
                //         portr_data7     =  membank6r_data;
                //     end
                //     12'd3 : begin
                //         portr_data0     =  membank4r_data;
                //         portr_data1     =  membank5r_data;
                //         portr_data2     =  membank6r_data;
                //         portr_data3     =  membank7r_data;
                //         portr_data4     =  membank7r_data;
                //         portr_data5     =  membank7r_data;
                //         portr_data6     =  membank7r_data;
                //         portr_data7     =  membank7r_data;
                //     end
                //     12'd4 : begin
                //         portr_data0     =  membank4r_data;
                //         portr_data1     =  membank5r_data;
                //         portr_data2     =  membank6r_data;
                //         portr_data3     =  membank7r_data;
                //         portr_data4     =  membank0r_data;
                //         portr_data5     =  membank0r_data;
                //         portr_data6     =  membank0r_data;
                //         portr_data7     =  membank0r_data;
                //     end
                //     12'd5 : begin
                //         portr_data0     =  membank4r_data;
                //         portr_data1     =  membank5r_data;
                //         portr_data2     =  membank6r_data;
                //         portr_data3     =  membank7r_data;
                //         portr_data4     =  membank0r_data;
                //         portr_data5     =  membank1r_data;
                //         portr_data6     =  membank1r_data;
                //         portr_data7     =  membank1r_data;
                //     end
                //     12'd6 : begin
                //         portr_data0     =  membank4r_data;
                //         portr_data1     =  membank5r_data;
                //         portr_data2     =  membank6r_data;
                //         portr_data3     =  membank7r_data;
                //         portr_data4     =  membank0r_data;
                //         portr_data5     =  membank1r_data;
                //         portr_data6     =  membank2r_data;
                //         portr_data7     =  membank2r_data;
                //     end
                //     default : begin
                //         portr_data0     =  membank4r_data;
                //         portr_data1     =  membank5r_data;
                //         portr_data2     =  membank6r_data;
                //         portr_data3     =  membank7r_data;
                //         portr_data4     =  membank0r_data;
                //         portr_data5     =  membank1r_data;
                //         portr_data6     =  membank2r_data;
                //         portr_data7     =  membank3r_data;
                //     end
                // endcase
            end

            3'd5 : begin
                // membanksr_en    = {8{portr_en}};//8'b11000111 & {8{portr_en}};

                membank0r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank1r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank2r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank3r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank4r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank5r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank6r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank7r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                // case(max_valid_sub_portr_addr)
                //     12'd0 : begin
                //         portr_data0     =  membank5r_data;
                //         portr_data1     =  membank5r_data;
                //         portr_data2     =  membank5r_data;
                //         portr_data3     =  membank5r_data;
                //         portr_data4     =  membank5r_data;
                //         portr_data5     =  membank5r_data;
                //         portr_data6     =  membank5r_data;
                //         portr_data7     =  membank5r_data;
                //     end
                //     12'd1 : begin
                //         portr_data0     =  membank5r_data;
                //         portr_data1     =  membank6r_data;
                //         portr_data2     =  membank6r_data;
                //         portr_data3     =  membank6r_data;
                //         portr_data4     =  membank6r_data;
                //         portr_data5     =  membank6r_data;
                //         portr_data6     =  membank6r_data;
                //         portr_data7     =  membank6r_data;
                //     end
                //     12'd2 : begin
                //         portr_data0     =  membank5r_data;
                //         portr_data1     =  membank6r_data;
                //         portr_data2     =  membank7r_data;
                //         portr_data3     =  membank7r_data;
                //         portr_data4     =  membank7r_data;
                //         portr_data5     =  membank7r_data;
                //         portr_data6     =  membank7r_data;
                //         portr_data7     =  membank7r_data;
                //     end
                //     12'd3 : begin
                //         portr_data0     =  membank5r_data;
                //         portr_data1     =  membank6r_data;
                //         portr_data2     =  membank7r_data;
                //         portr_data3     =  membank0r_data;
                //         portr_data4     =  membank0r_data;
                //         portr_data5     =  membank0r_data;
                //         portr_data6     =  membank0r_data;
                //         portr_data7     =  membank0r_data;
                //     end
                //     12'd4 : begin
                //         portr_data0     =  membank5r_data;
                //         portr_data1     =  membank6r_data;
                //         portr_data2     =  membank7r_data;
                //         portr_data3     =  membank0r_data;
                //         portr_data4     =  membank1r_data;
                //         portr_data5     =  membank1r_data;
                //         portr_data6     =  membank1r_data;
                //         portr_data7     =  membank1r_data;
                //     end
                //     12'd5 : begin
                //         portr_data0     =  membank5r_data;
                //         portr_data1     =  membank6r_data;
                //         portr_data2     =  membank7r_data;
                //         portr_data3     =  membank0r_data;
                //         portr_data4     =  membank1r_data;
                //         portr_data5     =  membank2r_data;
                //         portr_data6     =  membank2r_data;
                //         portr_data7     =  membank2r_data;
                //     end
                //     12'd6 : begin
                //         portr_data0     =  membank5r_data;
                //         portr_data1     =  membank6r_data;
                //         portr_data2     =  membank7r_data;
                //         portr_data3     =  membank0r_data;
                //         portr_data4     =  membank1r_data;
                //         portr_data5     =  membank2r_data;
                //         portr_data6     =  membank3r_data;
                //         portr_data7     =  membank3r_data;
                //     end
                //     default : begin
                //         portr_data0     =  membank5r_data;
                //         portr_data1     =  membank6r_data;
                //         portr_data2     =  membank7r_data;
                //         portr_data3     =  membank0r_data;
                //         portr_data4     =  membank1r_data;
                //         portr_data5     =  membank2r_data;
                //         portr_data6     =  membank3r_data;
                //         portr_data7     =  membank4r_data;
                //     end
                // endcase
            end

            3'd6 : begin
                // membanksr_en     = {8{portr_en}};//8'b10001111 & {8{portr_en}};

                membank0r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank1r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank2r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank3r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank4r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank5r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank6r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                membank7r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                // case(max_valid_sub_portr_addr)
                //     12'd0 : begin
                //         portr_data0     =  membank6r_data;
                //         portr_data1     =  membank6r_data;
                //         portr_data2     =  membank6r_data;
                //         portr_data3     =  membank6r_data;
                //         portr_data4     =  membank6r_data;
                //         portr_data5     =  membank6r_data;
                //         portr_data6     =  membank6r_data;
                //         portr_data7     =  membank6r_data;
                //     end
                //     12'd1 : begin
                //         portr_data0     =  membank6r_data;
                //         portr_data1     =  membank7r_data;
                //         portr_data2     =  membank7r_data;
                //         portr_data3     =  membank7r_data;
                //         portr_data4     =  membank7r_data;
                //         portr_data5     =  membank7r_data;
                //         portr_data6     =  membank7r_data;
                //         portr_data7     =  membank7r_data;
                //     end
                //     12'd2 : begin
                //         portr_data0     =  membank6r_data;
                //         portr_data1     =  membank7r_data;
                //         portr_data2     =  membank0r_data;
                //         portr_data3     =  membank0r_data;
                //         portr_data4     =  membank0r_data;
                //         portr_data5     =  membank0r_data;
                //         portr_data6     =  membank0r_data;
                //         portr_data7     =  membank0r_data;
                //     end
                //     12'd3 : begin
                //         portr_data0     =  membank6r_data;
                //         portr_data1     =  membank7r_data;
                //         portr_data2     =  membank0r_data;
                //         portr_data3     =  membank1r_data;
                //         portr_data4     =  membank1r_data;
                //         portr_data5     =  membank1r_data;
                //         portr_data6     =  membank1r_data;
                //         portr_data7     =  membank1r_data;
                //     end
                //     12'd4 : begin
                //         portr_data0     =  membank6r_data;
                //         portr_data1     =  membank7r_data;
                //         portr_data2     =  membank0r_data;
                //         portr_data3     =  membank1r_data;
                //         portr_data4     =  membank2r_data;
                //         portr_data5     =  membank2r_data;
                //         portr_data6     =  membank2r_data;
                //         portr_data7     =  membank2r_data;
                //     end
                //     12'd5 : begin
                //         portr_data0     =  membank6r_data;
                //         portr_data1     =  membank7r_data;
                //         portr_data2     =  membank0r_data;
                //         portr_data3     =  membank1r_data;
                //         portr_data4     =  membank2r_data;
                //         portr_data5     =  membank3r_data;
                //         portr_data6     =  membank3r_data;
                //         portr_data7     =  membank3r_data;
                //     end
                //     12'd6 : begin
                //         portr_data0     =  membank6r_data;
                //         portr_data1     =  membank7r_data;
                //         portr_data2     =  membank0r_data;
                //         portr_data3     =  membank1r_data;
                //         portr_data4     =  membank2r_data;
                //         portr_data5     =  membank3r_data;
                //         portr_data6     =  membank4r_data;
                //         portr_data7     =  membank4r_data;
                //     end
                //     default : begin
                //         portr_data0     =  membank6r_data;
                //         portr_data1     =  membank7r_data;
                //         portr_data2     =  membank0r_data;
                //         portr_data3     =  membank1r_data;
                //         portr_data4     =  membank2r_data;
                //         portr_data5     =  membank3r_data;
                //         portr_data6     =  membank4r_data;
                //         portr_data7     =  membank5r_data;
                //     end
                // endcase
            end
            3'd7 : begin
                // membanksr_en     = {8{portr_en}};//8'b10001111 & {8{portr_en}};

                membank0r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank1r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank2r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank3r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank4r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank5r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank6r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                membank7r_addr  = portr_addr_clipped[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                // case(max_valid_sub_portr_addr)
                //     12'd0 : begin
                //         portr_data0     =  membank7r_data;
                //         portr_data1     =  membank7r_data;
                //         portr_data2     =  membank7r_data;
                //         portr_data3     =  membank7r_data;
                //         portr_data4     =  membank7r_data;
                //         portr_data5     =  membank7r_data;
                //         portr_data6     =  membank7r_data;
                //         portr_data7     =  membank7r_data;
                //     end
                //     12'd1 : begin
                //         portr_data0     =  membank7r_data;
                //         portr_data1     =  membank0r_data;
                //         portr_data2     =  membank0r_data;
                //         portr_data3     =  membank0r_data;
                //         portr_data4     =  membank0r_data;
                //         portr_data5     =  membank0r_data;
                //         portr_data6     =  membank0r_data;
                //         portr_data7     =  membank0r_data;
                //     end
                //     12'd2 : begin
                //         portr_data0     =  membank7r_data;
                //         portr_data1     =  membank0r_data;
                //         portr_data2     =  membank1r_data;
                //         portr_data3     =  membank1r_data;
                //         portr_data4     =  membank1r_data;
                //         portr_data5     =  membank1r_data;
                //         portr_data6     =  membank1r_data;
                //         portr_data7     =  membank1r_data;
                //     end
                //     12'd3 : begin
                //         portr_data0     =  membank7r_data;
                //         portr_data1     =  membank0r_data;
                //         portr_data2     =  membank1r_data;
                //         portr_data3     =  membank2r_data;
                //         portr_data4     =  membank2r_data;
                //         portr_data5     =  membank2r_data;
                //         portr_data6     =  membank2r_data;
                //         portr_data7     =  membank2r_data;
                //     end
                //     12'd4 : begin
                //         portr_data0     =  membank7r_data;
                //         portr_data1     =  membank0r_data;
                //         portr_data2     =  membank1r_data;
                //         portr_data3     =  membank2r_data;
                //         portr_data4     =  membank3r_data;
                //         portr_data5     =  membank3r_data;
                //         portr_data6     =  membank3r_data;
                //         portr_data7     =  membank3r_data;
                //     end
                //     12'd5 : begin
                //         portr_data0     =  membank7r_data;
                //         portr_data1     =  membank0r_data;
                //         portr_data2     =  membank1r_data;
                //         portr_data3     =  membank2r_data;
                //         portr_data4     =  membank3r_data;
                //         portr_data5     =  membank4r_data;
                //         portr_data6     =  membank4r_data;
                //         portr_data7     =  membank4r_data;
                //     end
                //     12'd6 : begin
                //         portr_data0     =  membank7r_data;
                //         portr_data1     =  membank0r_data;
                //         portr_data2     =  membank1r_data;
                //         portr_data3     =  membank2r_data;
                //         portr_data4     =  membank3r_data;
                //         portr_data5     =  membank4r_data;
                //         portr_data6     =  membank5r_data;
                //         portr_data7     =  membank5r_data;
                //     end
                //     default : begin
                //         portr_data0     =  membank7r_data;
                //         portr_data1     =  membank0r_data;
                //         portr_data2     =  membank1r_data;
                //         portr_data3     =  membank2r_data;
                //         portr_data4     =  membank3r_data;
                //         portr_data5     =  membank4r_data;
                //         portr_data6     =  membank5r_data;
                //         portr_data7     =  membank6r_data;
                //     end
                // endcase
            end
        endcase
    end

    always @(*) begin : min_range_mux
        case(portr_addr_reg)
            3'd0 : begin
                // membanksr_en    = {8{portr_en}};//8'b00011111 & {8{portr_en}};

                // membank0r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank1r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank2r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank3r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank4r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank5r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank6r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank7r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                case(min_valid_sub_portr_addr)
                    12'd0 : begin
                        membank0r_data_min     = membank0r_data;
                        membank1r_data_min     = membank0r_data;
                        membank2r_data_min     = membank0r_data;
                        membank3r_data_min     = membank0r_data;
                        membank4r_data_min     = membank0r_data;
                        membank5r_data_min     = membank0r_data;
                        membank6r_data_min     = membank0r_data;
                        membank7r_data_min     = membank0r_data;
                    end
                   12'd1 : begin
                        membank0r_data_min     = membank7r_data;
                        membank1r_data_min     = membank7r_data;
                        membank2r_data_min     = membank7r_data;
                        membank3r_data_min     = membank7r_data;
                        membank4r_data_min     = membank7r_data;
                        membank5r_data_min     = membank7r_data;
                        membank6r_data_min     = membank7r_data;
                        membank7r_data_min     = membank7r_data;
                    end
                   12'd2 : begin
                        membank0r_data_min     = membank6r_data;
                        membank1r_data_min     = membank6r_data;
                        membank2r_data_min     = membank6r_data;
                        membank3r_data_min     = membank6r_data;
                        membank4r_data_min     = membank6r_data;
                        membank5r_data_min     = membank6r_data;
                        membank6r_data_min     = membank6r_data;
                        membank7r_data_min     = membank7r_data;
                   end
                   12'd3 : begin
                        membank0r_data_min     = membank5r_data;
                        membank1r_data_min     = membank5r_data;
                        membank2r_data_min     = membank5r_data;
                        membank3r_data_min     = membank5r_data;
                        membank4r_data_min     = membank5r_data;
                        membank5r_data_min     = membank5r_data;
                        membank6r_data_min     = membank6r_data;
                        membank7r_data_min     = membank7r_data;
                   end
                   12'd4 : begin
                        membank0r_data_min     = membank4r_data;
                        membank1r_data_min     = membank4r_data;
                        membank2r_data_min     = membank4r_data;
                        membank3r_data_min     = membank4r_data;
                        membank4r_data_min     = membank4r_data;
                        membank5r_data_min     = membank5r_data;
                        membank6r_data_min     = membank6r_data;
                        membank7r_data_min     = membank7r_data;
                   end
                   12'd5 : begin
                        membank0r_data_min     = membank3r_data;
                        membank1r_data_min     = membank3r_data;
                        membank2r_data_min     = membank3r_data;
                        membank3r_data_min     = membank3r_data;
                        membank4r_data_min     = membank4r_data;
                        membank5r_data_min     = membank5r_data;
                        membank6r_data_min     = membank6r_data;
                        membank7r_data_min     = membank7r_data;
                   end
                   12'd6 : begin
                        membank0r_data_min     = membank2r_data;
                        membank1r_data_min     = membank2r_data;
                        membank2r_data_min     = membank2r_data;
                        membank3r_data_min     = membank3r_data;
                        membank4r_data_min     = membank4r_data;
                        membank5r_data_min     = membank5r_data;
                        membank6r_data_min     = membank6r_data;
                        membank7r_data_min     = membank7r_data;
                   end
                   12'd7 : begin
                        membank0r_data_min     = membank1r_data;
                        membank1r_data_min     = membank1r_data;
                        membank2r_data_min     = membank2r_data;
                        membank3r_data_min     = membank3r_data;
                        membank4r_data_min     = membank4r_data;
                        membank5r_data_min     = membank5r_data;
                        membank6r_data_min     = membank6r_data;
                        membank7r_data_min     = membank7r_data;
                   end
                   default: begin
                        membank0r_data_min     = membank0r_data;
                        membank1r_data_min     = membank1r_data;
                        membank2r_data_min     = membank2r_data;
                        membank3r_data_min     = membank3r_data;
                        membank4r_data_min     = membank4r_data;
                        membank5r_data_min     = membank5r_data;
                        membank6r_data_min     = membank6r_data;
                        membank7r_data_min     = membank7r_data;
                   end
               endcase
            end
            3'd1 : begin
                // membanksr_en    = {8{portr_en}};//8'b00111110 & {8{portr_en}};

                // membank0r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank1r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank2r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank3r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank4r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank5r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank6r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank7r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                case(min_valid_sub_portr_addr)
                    12'd0 : begin
                        membank0r_data_min     = membank1r_data;
                        membank1r_data_min     = membank1r_data;
                        membank2r_data_min     = membank1r_data;
                        membank3r_data_min     = membank1r_data;
                        membank4r_data_min     = membank1r_data;
                        membank5r_data_min     = membank1r_data;
                        membank6r_data_min     = membank1r_data;
                        membank7r_data_min     = membank1r_data;
                    end
                   12'd1 : begin
                        membank0r_data_min     = membank0r_data;
                        membank1r_data_min     = membank0r_data;
                        membank2r_data_min     = membank0r_data;
                        membank3r_data_min     = membank0r_data;
                        membank4r_data_min     = membank0r_data;
                        membank5r_data_min     = membank0r_data;
                        membank6r_data_min     = membank0r_data;
                        membank7r_data_min     = membank0r_data;
                    end
                   12'd2 : begin
                        membank0r_data_min     = membank7r_data;
                        membank1r_data_min     = membank7r_data;
                        membank2r_data_min     = membank7r_data;
                        membank3r_data_min     = membank7r_data;
                        membank4r_data_min     = membank7r_data;
                        membank5r_data_min     = membank7r_data;
                        membank6r_data_min     = membank7r_data;
                        membank7r_data_min     = membank0r_data;
                   end
                   12'd3 : begin
                        membank0r_data_min     = membank6r_data;
                        membank1r_data_min     = membank6r_data;
                        membank2r_data_min     = membank6r_data;
                        membank3r_data_min     = membank6r_data;
                        membank4r_data_min     = membank6r_data;
                        membank5r_data_min     = membank6r_data;
                        membank6r_data_min     = membank7r_data;
                        membank7r_data_min     = membank0r_data;
                   end
                   12'd4 : begin
                        membank0r_data_min     = membank5r_data;
                        membank1r_data_min     = membank5r_data;
                        membank2r_data_min     = membank5r_data;
                        membank3r_data_min     = membank5r_data;
                        membank4r_data_min     = membank5r_data;
                        membank5r_data_min     = membank6r_data;
                        membank6r_data_min     = membank7r_data;
                        membank7r_data_min     = membank0r_data;
                   end
                   12'd5 : begin
                        membank0r_data_min     = membank4r_data;
                        membank1r_data_min     = membank4r_data;
                        membank2r_data_min     = membank4r_data;
                        membank3r_data_min     = membank4r_data;
                        membank4r_data_min     = membank5r_data;
                        membank5r_data_min     = membank6r_data;
                        membank6r_data_min     = membank7r_data;
                        membank7r_data_min     = membank0r_data;
                   end
                   12'd6 : begin
                        membank0r_data_min     = membank3r_data;
                        membank1r_data_min     = membank3r_data;
                        membank2r_data_min     = membank3r_data;
                        membank3r_data_min     = membank4r_data;
                        membank4r_data_min     = membank5r_data;
                        membank5r_data_min     = membank6r_data;
                        membank6r_data_min     = membank7r_data;
                        membank7r_data_min     = membank0r_data;
                   end
                   12'd7 : begin
                        membank0r_data_min     = membank2r_data;
                        membank1r_data_min     = membank2r_data;
                        membank2r_data_min     = membank3r_data;
                        membank3r_data_min     = membank4r_data;
                        membank4r_data_min     = membank5r_data;
                        membank5r_data_min     = membank6r_data;
                        membank6r_data_min     = membank7r_data;
                        membank7r_data_min     = membank0r_data;
                   end
                   default: begin
                        membank0r_data_min     = membank1r_data;
                        membank1r_data_min     = membank2r_data;
                        membank2r_data_min     = membank3r_data;
                        membank3r_data_min     = membank4r_data;
                        membank4r_data_min     = membank5r_data;
                        membank5r_data_min     = membank6r_data;
                        membank6r_data_min     = membank7r_data;
                        membank7r_data_min     = membank0r_data;
                   end
               endcase
            end

            3'd2 : begin
                // membanksr_en    = {8{portr_en}};//8'b01111100 & {8{portr_en}};

                // membank0r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank1r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank2r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank3r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank4r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank5r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank6r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank7r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                case(min_valid_sub_portr_addr)
                    12'd0 : begin
                        membank0r_data_min     = membank2r_data;
                        membank1r_data_min     = membank2r_data;
                        membank2r_data_min     = membank2r_data;
                        membank3r_data_min     = membank2r_data;
                        membank4r_data_min     = membank2r_data;
                        membank5r_data_min     = membank2r_data;
                        membank6r_data_min     = membank2r_data;
                        membank7r_data_min     = membank2r_data;
                    end
                   12'd1 : begin
                        membank0r_data_min     = membank1r_data;
                        membank1r_data_min     = membank1r_data;
                        membank2r_data_min     = membank1r_data;
                        membank3r_data_min     = membank1r_data;
                        membank4r_data_min     = membank1r_data;
                        membank5r_data_min     = membank1r_data;
                        membank6r_data_min     = membank1r_data;
                        membank7r_data_min     = membank1r_data;
                    end
                   12'd2 : begin
                        membank0r_data_min     = membank0r_data;
                        membank1r_data_min     = membank0r_data;
                        membank2r_data_min     = membank0r_data;
                        membank3r_data_min     = membank0r_data;
                        membank4r_data_min     = membank0r_data;
                        membank5r_data_min     = membank0r_data;
                        membank6r_data_min     = membank0r_data;
                        membank7r_data_min     = membank1r_data;
                   end
                   12'd3 : begin
                        membank0r_data_min     = membank7r_data;
                        membank1r_data_min     = membank7r_data;
                        membank2r_data_min     = membank7r_data;
                        membank3r_data_min     = membank7r_data;
                        membank4r_data_min     = membank7r_data;
                        membank5r_data_min     = membank7r_data;
                        membank6r_data_min     = membank0r_data;
                        membank7r_data_min     = membank1r_data;
                   end
                   12'd4 : begin
                        membank0r_data_min     = membank6r_data;
                        membank1r_data_min     = membank6r_data;
                        membank2r_data_min     = membank6r_data;
                        membank3r_data_min     = membank6r_data;
                        membank4r_data_min     = membank6r_data;
                        membank5r_data_min     = membank7r_data;
                        membank6r_data_min     = membank0r_data;
                        membank7r_data_min     = membank1r_data;
                   end
                   12'd5 : begin
                        membank0r_data_min     = membank5r_data;
                        membank1r_data_min     = membank5r_data;
                        membank2r_data_min     = membank5r_data;
                        membank3r_data_min     = membank5r_data;
                        membank4r_data_min     = membank6r_data;
                        membank5r_data_min     = membank7r_data;
                        membank6r_data_min     = membank0r_data;
                        membank7r_data_min     = membank1r_data;
                   end
                   12'd6 : begin
                        membank0r_data_min     = membank4r_data;
                        membank1r_data_min     = membank4r_data;
                        membank2r_data_min     = membank4r_data;
                        membank3r_data_min     = membank5r_data;
                        membank4r_data_min     = membank6r_data;
                        membank5r_data_min     = membank7r_data;
                        membank6r_data_min     = membank0r_data;
                        membank7r_data_min     = membank1r_data;
                   end
                   12'd7 : begin
                        membank0r_data_min     = membank3r_data;
                        membank1r_data_min     = membank3r_data;
                        membank2r_data_min     = membank4r_data;
                        membank3r_data_min     = membank5r_data;
                        membank4r_data_min     = membank6r_data;
                        membank5r_data_min     = membank7r_data;
                        membank6r_data_min     = membank0r_data;
                        membank7r_data_min     = membank1r_data;
                   end
                   default: begin
                        membank0r_data_min     = membank2r_data;
                        membank1r_data_min     = membank3r_data;
                        membank2r_data_min     = membank4r_data;
                        membank3r_data_min     = membank5r_data;
                        membank4r_data_min     = membank6r_data;
                        membank5r_data_min     = membank7r_data;
                        membank6r_data_min     = membank0r_data;
                        membank7r_data_min     = membank1r_data;
                   end
               endcase
            end

            3'd3 : begin
                // membanksr_en    = {8{portr_en}};//8'b11111000 & {8{portr_en}};

                // membank0r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank1r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank2r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank3r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank4r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank5r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank6r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank7r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                case(min_valid_sub_portr_addr)
                    12'd0 : begin
                        membank0r_data_min     = membank3r_data;
                        membank1r_data_min     = membank3r_data;
                        membank2r_data_min     = membank3r_data;
                        membank3r_data_min     = membank3r_data;
                        membank4r_data_min     = membank3r_data;
                        membank5r_data_min     = membank3r_data;
                        membank6r_data_min     = membank3r_data;
                        membank7r_data_min     = membank3r_data;
                    end
                   12'd1 : begin
                        membank0r_data_min     = membank2r_data;
                        membank1r_data_min     = membank2r_data;
                        membank2r_data_min     = membank2r_data;
                        membank3r_data_min     = membank2r_data;
                        membank4r_data_min     = membank2r_data;
                        membank5r_data_min     = membank2r_data;
                        membank6r_data_min     = membank2r_data;
                        membank7r_data_min     = membank2r_data;
                    end
                   12'd2 : begin
                        membank0r_data_min     = membank1r_data;
                        membank1r_data_min     = membank1r_data;
                        membank2r_data_min     = membank1r_data;
                        membank3r_data_min     = membank1r_data;
                        membank4r_data_min     = membank1r_data;
                        membank5r_data_min     = membank1r_data;
                        membank6r_data_min     = membank1r_data;
                        membank7r_data_min     = membank2r_data;
                   end
                   12'd3 : begin
                        membank0r_data_min     = membank0r_data;
                        membank1r_data_min     = membank0r_data;
                        membank2r_data_min     = membank0r_data;
                        membank3r_data_min     = membank0r_data;
                        membank4r_data_min     = membank0r_data;
                        membank5r_data_min     = membank0r_data;
                        membank6r_data_min     = membank1r_data;
                        membank7r_data_min     = membank2r_data;
                   end
                   12'd4 : begin
                        membank0r_data_min     = membank7r_data;
                        membank1r_data_min     = membank7r_data;
                        membank2r_data_min     = membank7r_data;
                        membank3r_data_min     = membank7r_data;
                        membank4r_data_min     = membank7r_data;
                        membank5r_data_min     = membank0r_data;
                        membank6r_data_min     = membank1r_data;
                        membank7r_data_min     = membank2r_data;
                   end
                   12'd5 : begin
                        membank0r_data_min     = membank6r_data;
                        membank1r_data_min     = membank6r_data;
                        membank2r_data_min     = membank6r_data;
                        membank3r_data_min     = membank6r_data;
                        membank4r_data_min     = membank7r_data;
                        membank5r_data_min     = membank0r_data;
                        membank6r_data_min     = membank1r_data;
                        membank7r_data_min     = membank2r_data;
                   end
                   12'd6 : begin
                        membank0r_data_min     = membank5r_data;
                        membank1r_data_min     = membank5r_data;
                        membank2r_data_min     = membank5r_data;
                        membank3r_data_min     = membank6r_data;
                        membank4r_data_min     = membank7r_data;
                        membank5r_data_min     = membank0r_data;
                        membank6r_data_min     = membank1r_data;
                        membank7r_data_min     = membank2r_data;
                   end
                   12'd7 : begin
                        membank0r_data_min     = membank4r_data;
                        membank1r_data_min     = membank4r_data;
                        membank2r_data_min     = membank5r_data;
                        membank3r_data_min     = membank6r_data;
                        membank4r_data_min     = membank7r_data;
                        membank5r_data_min     = membank0r_data;
                        membank6r_data_min     = membank1r_data;
                        membank7r_data_min     = membank2r_data;
                   end
                    default : begin
                        membank0r_data_min     = membank3r_data;
                        membank1r_data_min     = membank4r_data;
                        membank2r_data_min     = membank5r_data;
                        membank3r_data_min     = membank6r_data;
                        membank4r_data_min     = membank7r_data;
                        membank5r_data_min     = membank0r_data;
                        membank6r_data_min     = membank1r_data;
                        membank7r_data_min     = membank2r_data;
                    end
                endcase
            end

            3'd4 : begin
                // membanksr_en    = {8{portr_en}};//8'b11110001 & {8{portr_en}};

                // membank0r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank1r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank2r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank3r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank4r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank5r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank6r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank7r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                case(min_valid_sub_portr_addr)
                    12'd0 : begin
                        membank0r_data_min     = membank4r_data;
                        membank1r_data_min     = membank4r_data;
                        membank2r_data_min     = membank4r_data;
                        membank3r_data_min     = membank4r_data;
                        membank4r_data_min     = membank4r_data;
                        membank5r_data_min     = membank4r_data;
                        membank6r_data_min     = membank4r_data;
                        membank7r_data_min     = membank4r_data;
                    end
                   12'd1 : begin
                        membank0r_data_min     = membank3r_data;
                        membank1r_data_min     = membank3r_data;
                        membank2r_data_min     = membank3r_data;
                        membank3r_data_min     = membank3r_data;
                        membank4r_data_min     = membank3r_data;
                        membank5r_data_min     = membank3r_data;
                        membank6r_data_min     = membank3r_data;
                        membank7r_data_min     = membank3r_data;
                    end
                   12'd2 : begin
                        membank0r_data_min     = membank2r_data;
                        membank1r_data_min     = membank2r_data;
                        membank2r_data_min     = membank2r_data;
                        membank3r_data_min     = membank2r_data;
                        membank4r_data_min     = membank2r_data;
                        membank5r_data_min     = membank2r_data;
                        membank6r_data_min     = membank2r_data;
                        membank7r_data_min     = membank3r_data;
                   end
                   12'd3 : begin
                        membank0r_data_min     = membank1r_data;
                        membank1r_data_min     = membank1r_data;
                        membank2r_data_min     = membank1r_data;
                        membank3r_data_min     = membank1r_data;
                        membank4r_data_min     = membank1r_data;
                        membank5r_data_min     = membank1r_data;
                        membank6r_data_min     = membank2r_data;
                        membank7r_data_min     = membank3r_data;
                   end
                   12'd4 : begin
                        membank0r_data_min     = membank0r_data;
                        membank1r_data_min     = membank0r_data;
                        membank2r_data_min     = membank0r_data;
                        membank3r_data_min     = membank0r_data;
                        membank4r_data_min     = membank0r_data;
                        membank5r_data_min     = membank1r_data;
                        membank6r_data_min     = membank2r_data;
                        membank7r_data_min     = membank3r_data;
                   end
                   12'd5 : begin
                        membank0r_data_min     = membank7r_data;
                        membank1r_data_min     = membank7r_data;
                        membank2r_data_min     = membank7r_data;
                        membank3r_data_min     = membank7r_data;
                        membank4r_data_min     = membank0r_data;
                        membank5r_data_min     = membank1r_data;
                        membank6r_data_min     = membank2r_data;
                        membank7r_data_min     = membank3r_data;
                   end
                   12'd6 : begin
                        membank0r_data_min     = membank6r_data;
                        membank1r_data_min     = membank6r_data;
                        membank2r_data_min     = membank6r_data;
                        membank3r_data_min     = membank7r_data;
                        membank4r_data_min     = membank0r_data;
                        membank5r_data_min     = membank1r_data;
                        membank6r_data_min     = membank2r_data;
                        membank7r_data_min     = membank3r_data;
                   end
                   12'd7 : begin
                        membank0r_data_min     = membank5r_data;
                        membank1r_data_min     = membank5r_data;
                        membank2r_data_min     = membank6r_data;
                        membank3r_data_min     = membank7r_data;
                        membank4r_data_min     = membank0r_data;
                        membank5r_data_min     = membank1r_data;
                        membank6r_data_min     = membank2r_data;
                        membank7r_data_min     = membank3r_data;
                   end
                    default : begin
                        membank0r_data_min     = membank4r_data;
                        membank1r_data_min     = membank5r_data;
                        membank2r_data_min     = membank6r_data;
                        membank3r_data_min     = membank7r_data;
                        membank4r_data_min     = membank0r_data;
                        membank5r_data_min     = membank1r_data;
                        membank6r_data_min     = membank2r_data;
                        membank7r_data_min     = membank3r_data;
                    end
                endcase
            end

            3'd5 : begin
                // membanksr_en    = {8{portr_en}};//8'b11000111 & {8{portr_en}};

                // membank0r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank1r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank2r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank3r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank4r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank5r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank6r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank7r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                case(min_valid_sub_portr_addr)
                    12'd0 : begin
                        membank0r_data_min     = membank5r_data;
                        membank1r_data_min     = membank5r_data;
                        membank2r_data_min     = membank5r_data;
                        membank3r_data_min     = membank5r_data;
                        membank4r_data_min     = membank5r_data;
                        membank5r_data_min     = membank5r_data;
                        membank6r_data_min     = membank5r_data;
                        membank7r_data_min     = membank5r_data;
                    end
                   12'd1 : begin
                        membank0r_data_min     = membank4r_data;
                        membank1r_data_min     = membank4r_data;
                        membank2r_data_min     = membank4r_data;
                        membank3r_data_min     = membank4r_data;
                        membank4r_data_min     = membank4r_data;
                        membank5r_data_min     = membank4r_data;
                        membank6r_data_min     = membank4r_data;
                        membank7r_data_min     = membank4r_data;
                    end
                   12'd2 : begin
                        membank0r_data_min     = membank3r_data;
                        membank1r_data_min     = membank3r_data;
                        membank2r_data_min     = membank3r_data;
                        membank3r_data_min     = membank3r_data;
                        membank4r_data_min     = membank3r_data;
                        membank5r_data_min     = membank3r_data;
                        membank6r_data_min     = membank3r_data;
                        membank7r_data_min     = membank4r_data;
                   end
                   12'd3 : begin
                        membank0r_data_min     = membank2r_data;
                        membank1r_data_min     = membank2r_data;
                        membank2r_data_min     = membank2r_data;
                        membank3r_data_min     = membank2r_data;
                        membank4r_data_min     = membank2r_data;
                        membank5r_data_min     = membank2r_data;
                        membank6r_data_min     = membank3r_data;
                        membank7r_data_min     = membank4r_data;
                   end
                   12'd4 : begin
                        membank0r_data_min     = membank1r_data;
                        membank1r_data_min     = membank1r_data;
                        membank2r_data_min     = membank1r_data;
                        membank3r_data_min     = membank1r_data;
                        membank4r_data_min     = membank1r_data;
                        membank5r_data_min     = membank2r_data;
                        membank6r_data_min     = membank3r_data;
                        membank7r_data_min     = membank4r_data;
                   end
                   12'd5 : begin
                        membank0r_data_min     = membank0r_data;
                        membank1r_data_min     = membank0r_data;
                        membank2r_data_min     = membank0r_data;
                        membank3r_data_min     = membank0r_data;
                        membank4r_data_min     = membank1r_data;
                        membank5r_data_min     = membank2r_data;
                        membank6r_data_min     = membank3r_data;
                        membank7r_data_min     = membank4r_data;
                   end
                   12'd6 : begin
                        membank0r_data_min     = membank7r_data;
                        membank1r_data_min     = membank7r_data;
                        membank2r_data_min     = membank7r_data;
                        membank3r_data_min     = membank0r_data;
                        membank4r_data_min     = membank1r_data;
                        membank5r_data_min     = membank2r_data;
                        membank6r_data_min     = membank3r_data;
                        membank7r_data_min     = membank4r_data;
                   end
                   12'd7 : begin
                        membank0r_data_min     = membank6r_data;
                        membank1r_data_min     = membank6r_data;
                        membank2r_data_min     = membank7r_data;
                        membank3r_data_min     = membank0r_data;
                        membank4r_data_min     = membank1r_data;
                        membank5r_data_min     = membank2r_data;
                        membank6r_data_min     = membank3r_data;
                        membank7r_data_min     = membank4r_data;
                   end
                    default : begin
                        membank0r_data_min     = membank5r_data;
                        membank1r_data_min     = membank6r_data;
                        membank2r_data_min     = membank7r_data;
                        membank3r_data_min     = membank0r_data;
                        membank4r_data_min     = membank1r_data;
                        membank5r_data_min     = membank2r_data;
                        membank6r_data_min     = membank3r_data;
                        membank7r_data_min     = membank4r_data;
                    end
                endcase
            end

            3'd6 : begin
                // membanksr_en     = {8{portr_en}};//8'b10001111 & {8{portr_en}};

                // membank0r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank1r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank2r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank3r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank4r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank5r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank6r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
                // membank7r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                case(min_valid_sub_portr_addr)
                    12'd0 : begin
                        membank0r_data_min     = membank6r_data;
                        membank1r_data_min     = membank6r_data;
                        membank2r_data_min     = membank6r_data;
                        membank3r_data_min     = membank6r_data;
                        membank4r_data_min     = membank6r_data;
                        membank5r_data_min     = membank6r_data;
                        membank6r_data_min     = membank6r_data;
                        membank7r_data_min     = membank6r_data;
                    end
                   12'd1 : begin
                        membank0r_data_min     = membank5r_data;
                        membank1r_data_min     = membank5r_data;
                        membank2r_data_min     = membank5r_data;
                        membank3r_data_min     = membank5r_data;
                        membank4r_data_min     = membank5r_data;
                        membank5r_data_min     = membank5r_data;
                        membank6r_data_min     = membank5r_data;
                        membank7r_data_min     = membank5r_data;
                    end
                   12'd2 : begin
                        membank0r_data_min     = membank4r_data;
                        membank1r_data_min     = membank4r_data;
                        membank2r_data_min     = membank4r_data;
                        membank3r_data_min     = membank4r_data;
                        membank4r_data_min     = membank4r_data;
                        membank5r_data_min     = membank4r_data;
                        membank6r_data_min     = membank4r_data;
                        membank7r_data_min     = membank5r_data;
                   end
                   12'd3 : begin
                        membank0r_data_min     = membank3r_data;
                        membank1r_data_min     = membank3r_data;
                        membank2r_data_min     = membank3r_data;
                        membank3r_data_min     = membank3r_data;
                        membank4r_data_min     = membank3r_data;
                        membank5r_data_min     = membank3r_data;
                        membank6r_data_min     = membank4r_data;
                        membank7r_data_min     = membank5r_data;
                   end
                   12'd4 : begin
                        membank0r_data_min     = membank2r_data;
                        membank1r_data_min     = membank2r_data;
                        membank2r_data_min     = membank2r_data;
                        membank3r_data_min     = membank2r_data;
                        membank4r_data_min     = membank2r_data;
                        membank5r_data_min     = membank3r_data;
                        membank6r_data_min     = membank4r_data;
                        membank7r_data_min     = membank5r_data;
                   end
                   12'd5 : begin
                        membank0r_data_min     = membank1r_data;
                        membank1r_data_min     = membank1r_data;
                        membank2r_data_min     = membank1r_data;
                        membank3r_data_min     = membank1r_data;
                        membank4r_data_min     = membank2r_data;
                        membank5r_data_min     = membank3r_data;
                        membank6r_data_min     = membank4r_data;
                        membank7r_data_min     = membank5r_data;
                   end
                   12'd6 : begin
                        membank0r_data_min     = membank0r_data;
                        membank1r_data_min     = membank0r_data;
                        membank2r_data_min     = membank0r_data;
                        membank3r_data_min     = membank1r_data;
                        membank4r_data_min     = membank2r_data;
                        membank5r_data_min     = membank3r_data;
                        membank6r_data_min     = membank4r_data;
                        membank7r_data_min     = membank5r_data;
                   end
                   12'd7 : begin
                        membank0r_data_min     = membank7r_data;
                        membank1r_data_min     = membank7r_data;
                        membank2r_data_min     = membank0r_data;
                        membank3r_data_min     = membank1r_data;
                        membank4r_data_min     = membank2r_data;
                        membank5r_data_min     = membank3r_data;
                        membank6r_data_min     = membank4r_data;
                        membank7r_data_min     = membank5r_data;
                   end
                    default : begin
                        membank0r_data_min     = membank6r_data;
                        membank1r_data_min     = membank7r_data;
                        membank2r_data_min     = membank0r_data;
                        membank3r_data_min     = membank1r_data;
                        membank4r_data_min     = membank2r_data;
                        membank5r_data_min     = membank3r_data;
                        membank6r_data_min     = membank4r_data;
                        membank7r_data_min     = membank5r_data;
                    end
                endcase
            end
            3'd7 : begin
                // membanksr_en     = {8{portr_en}};//8'b10001111 & {8{portr_en}};

                // membank0r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank1r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank2r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank3r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank4r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank5r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank6r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
                // membank7r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

                case(min_valid_sub_portr_addr)
                    12'd0 : begin
                        membank0r_data_min     = membank7r_data;
                        membank1r_data_min     = membank7r_data;
                        membank2r_data_min     = membank7r_data;
                        membank3r_data_min     = membank7r_data;
                        membank4r_data_min     = membank7r_data;
                        membank5r_data_min     = membank7r_data;
                        membank6r_data_min     = membank7r_data;
                        membank7r_data_min     = membank7r_data;
                    end
                   12'd1 : begin
                        membank0r_data_min     = membank6r_data;
                        membank1r_data_min     = membank6r_data;
                        membank2r_data_min     = membank6r_data;
                        membank3r_data_min     = membank6r_data;
                        membank4r_data_min     = membank6r_data;
                        membank5r_data_min     = membank6r_data;
                        membank6r_data_min     = membank6r_data;
                        membank7r_data_min     = membank6r_data;
                    end
                   12'd2 : begin
                        membank0r_data_min     = membank5r_data;
                        membank1r_data_min     = membank5r_data;
                        membank2r_data_min     = membank5r_data;
                        membank3r_data_min     = membank5r_data;
                        membank4r_data_min     = membank5r_data;
                        membank5r_data_min     = membank5r_data;
                        membank6r_data_min     = membank5r_data;
                        membank7r_data_min     = membank6r_data;
                   end
                   12'd3 : begin
                        membank0r_data_min     = membank4r_data;
                        membank1r_data_min     = membank4r_data;
                        membank2r_data_min     = membank4r_data;
                        membank3r_data_min     = membank4r_data;
                        membank4r_data_min     = membank4r_data;
                        membank5r_data_min     = membank4r_data;
                        membank6r_data_min     = membank5r_data;
                        membank7r_data_min     = membank6r_data;
                   end
                   12'd4 : begin
                        membank0r_data_min     = membank3r_data;
                        membank1r_data_min     = membank3r_data;
                        membank2r_data_min     = membank3r_data;
                        membank3r_data_min     = membank3r_data;
                        membank4r_data_min     = membank3r_data;
                        membank5r_data_min     = membank4r_data;
                        membank6r_data_min     = membank5r_data;
                        membank7r_data_min     = membank6r_data;
                   end
                   12'd5 : begin
                        membank0r_data_min     = membank2r_data;
                        membank1r_data_min     = membank2r_data;
                        membank2r_data_min     = membank2r_data;
                        membank3r_data_min     = membank2r_data;
                        membank4r_data_min     = membank3r_data;
                        membank5r_data_min     = membank4r_data;
                        membank6r_data_min     = membank5r_data;
                        membank7r_data_min     = membank6r_data;
                   end
                   12'd6 : begin
                        membank0r_data_min     = membank1r_data;
                        membank1r_data_min     = membank1r_data;
                        membank2r_data_min     = membank1r_data;
                        membank3r_data_min     = membank2r_data;
                        membank4r_data_min     = membank3r_data;
                        membank5r_data_min     = membank4r_data;
                        membank6r_data_min     = membank5r_data;
                        membank7r_data_min     = membank6r_data;
                   end
                   12'd7 : begin
                        membank0r_data_min     = membank0r_data;
                        membank1r_data_min     = membank0r_data;
                        membank2r_data_min     = membank1r_data;
                        membank3r_data_min     = membank2r_data;
                        membank4r_data_min     = membank3r_data;
                        membank5r_data_min     = membank4r_data;
                        membank6r_data_min     = membank5r_data;
                        membank7r_data_min     = membank6r_data;
                   end
                    default : begin
                        membank0r_data_min     = membank7r_data;
                        membank1r_data_min     = membank0r_data;
                        membank2r_data_min     = membank1r_data;
                        membank3r_data_min     = membank2r_data;
                        membank4r_data_min     = membank3r_data;
                        membank5r_data_min     = membank4r_data;
                        membank6r_data_min     = membank5r_data;
                        membank7r_data_min     = membank6r_data;
                    end
                endcase
            end
        endcase
    end
    always @(*) begin : read_mux_delayed
        case(max_valid_sub_portr_addr)
            12'd0 : begin
                portr_data0     = membank0r_data_min;
                portr_data1     = membank0r_data_min;
                portr_data2     = membank0r_data_min;
                portr_data3     = membank0r_data_min;
                portr_data4     = membank0r_data_min;
                portr_data5     = membank0r_data_min;
                portr_data6     = membank0r_data_min;
                portr_data7     = membank0r_data_min;
            end
            12'd1 : begin
                portr_data0     = membank0r_data_min;
                portr_data1     = membank1r_data_min;
                portr_data2     = membank1r_data_min;
                portr_data3     = membank1r_data_min;
                portr_data4     = membank1r_data_min;
                portr_data5     = membank1r_data_min;
                portr_data6     = membank1r_data_min;
                portr_data7     = membank1r_data_min;
            end
            12'd2 : begin
                portr_data0     = membank0r_data_min;
                portr_data1     = membank1r_data_min;
                portr_data2     = membank2r_data_min;
                portr_data3     = membank2r_data_min;
                portr_data4     = membank2r_data_min;
                portr_data5     = membank2r_data_min;
                portr_data6     = membank2r_data_min;
                portr_data7     = membank2r_data_min;
            end
            12'd3 : begin
                portr_data0     = membank0r_data_min;
                portr_data1     = membank1r_data_min;
                portr_data2     = membank2r_data_min;
                portr_data3     = membank3r_data_min;
                portr_data4     = membank3r_data_min;
                portr_data5     = membank3r_data_min;
                portr_data6     = membank3r_data_min;
                portr_data7     = membank3r_data_min;
            end
            12'd4 : begin
                portr_data0     = membank0r_data_min;
                portr_data1     = membank1r_data_min;
                portr_data2     = membank2r_data_min;
                portr_data3     = membank3r_data_min;
                portr_data4     = membank4r_data_min;
                portr_data5     = membank4r_data_min;
                portr_data6     = membank4r_data_min;
                portr_data7     = membank4r_data_min;
            end
            12'd5 : begin
                portr_data0     = membank0r_data_min;
                portr_data1     = membank1r_data_min;
                portr_data2     = membank2r_data_min;
                portr_data3     = membank3r_data_min;
                portr_data4     = membank4r_data_min;
                portr_data5     = membank5r_data_min;
                portr_data6     = membank5r_data_min;
                portr_data7     = membank5r_data_min;
            end
            12'd6 : begin
                portr_data0     = membank0r_data_min;
                portr_data1     = membank1r_data_min;
                portr_data2     = membank2r_data_min;
                portr_data3     = membank3r_data_min;
                portr_data4     = membank4r_data_min;
                portr_data5     = membank5r_data_min;
                portr_data6     = membank6r_data_min;
                portr_data7     = membank6r_data_min;
            end
            default: begin
                portr_data0     = membank0r_data_min;
                portr_data1     = membank1r_data_min;
                portr_data2     = membank2r_data_min;
                portr_data3     = membank3r_data_min;
                portr_data4     = membank4r_data_min;
                portr_data5     = membank5r_data_min;
                portr_data6     = membank6r_data_min;
                portr_data7     = membank7r_data_min;
            end
        endcase
    end
    // always @(*) begin : read_mux_delayed
    //     case(portr_addr_reg)
    //         3'd0 : begin
    //             // membanksr_en    = {8{portr_en}};//8'b00011111 & {8{portr_en}};

    //             // membank0r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank1r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank2r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank3r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank4r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank5r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank6r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank7r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

    //             case(max_valid_sub_portr_addr)
    //                 12'd0 : begin
    //                     portr_data0     = membank0r_data_min;
    //                     portr_data1     = membank0r_data_min;
    //                     portr_data2     = membank0r_data_min;
    //                     portr_data3     = membank0r_data_min;
    //                     portr_data4     = membank0r_data_min;
    //                     portr_data5     = membank0r_data_min;
    //                     portr_data6     = membank0r_data_min;
    //                     portr_data7     = membank0r_data_min;
    //                 end
    //                12'd1 : begin
    //                     portr_data0     = membank0r_data_min;
    //                     portr_data1     = membank1r_data_min;
    //                     portr_data2     = membank1r_data_min;
    //                     portr_data3     = membank1r_data_min;
    //                     portr_data4     = membank1r_data_min;
    //                     portr_data5     = membank1r_data_min;
    //                     portr_data6     = membank1r_data_min;
    //                     portr_data7     = membank1r_data_min;
    //                 end
    //                12'd2 : begin
    //                     portr_data0     = membank0r_data_min;
    //                     portr_data1     = membank1r_data_min;
    //                     portr_data2     = membank2r_data_min;
    //                     portr_data3     = membank2r_data_min;
    //                     portr_data4     = membank2r_data_min;
    //                     portr_data5     = membank2r_data_min;
    //                     portr_data6     = membank2r_data_min;
    //                     portr_data7     = membank2r_data_min;
    //                end
    //                12'd3 : begin
    //                     portr_data0     = membank0r_data_min;
    //                     portr_data1     = membank1r_data_min;
    //                     portr_data2     = membank2r_data_min;
    //                     portr_data3     = membank3r_data_min;
    //                     portr_data4     = membank3r_data_min;
    //                     portr_data5     = membank3r_data_min;
    //                     portr_data6     = membank3r_data_min;
    //                     portr_data7     = membank3r_data_min;
    //                end
    //                12'd4 : begin
    //                     portr_data0     = membank0r_data_min;
    //                     portr_data1     = membank1r_data_min;
    //                     portr_data2     = membank2r_data_min;
    //                     portr_data3     = membank3r_data_min;
    //                     portr_data4     = membank4r_data_min;
    //                     portr_data5     = membank4r_data_min;
    //                     portr_data6     = membank4r_data_min;
    //                     portr_data7     = membank4r_data_min;
    //                end
    //                12'd5 : begin
    //                     portr_data0     = membank0r_data_min;
    //                     portr_data1     = membank1r_data_min;
    //                     portr_data2     = membank2r_data_min;
    //                     portr_data3     = membank3r_data_min;
    //                     portr_data4     = membank4r_data_min;
    //                     portr_data5     = membank5r_data_min;
    //                     portr_data6     = membank5r_data_min;
    //                     portr_data7     = membank5r_data_min;
    //                end
    //                12'd6 : begin
    //                     portr_data0     = membank0r_data_min;
    //                     portr_data1     = membank1r_data_min;
    //                     portr_data2     = membank2r_data_min;
    //                     portr_data3     = membank3r_data_min;
    //                     portr_data4     = membank4r_data_min;
    //                     portr_data5     = membank5r_data_min;
    //                     portr_data6     = membank6r_data_min;
    //                     portr_data7     = membank6r_data_min;
    //                end
    //                default: begin
    //                     portr_data0     = membank0r_data_min;
    //                     portr_data1     = membank1r_data_min;
    //                     portr_data2     = membank2r_data_min;
    //                     portr_data3     = membank3r_data_min;
    //                     portr_data4     = membank4r_data_min;
    //                     portr_data5     = membank5r_data_min;
    //                     portr_data6     = membank6r_data_min;
    //                     portr_data7     = membank7r_data_min;
    //                end
    //            endcase
    //         end
    //         3'd1 : begin
    //             // membanksr_en    = {8{portr_en}};//8'b00111110 & {8{portr_en}};

    //             // membank0r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank1r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank2r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank3r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank4r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank5r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank6r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank7r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

    //             case(max_valid_sub_portr_addr)
    //                 12'd0 : begin
    //                     portr_data0     =  membank1r_data_min;
    //                     portr_data1     =  membank1r_data_min;
    //                     portr_data2     =  membank1r_data_min;
    //                     portr_data3     =  membank1r_data_min;
    //                     portr_data4     =  membank1r_data_min;
    //                     portr_data5     =  membank1r_data_min;
    //                     portr_data6     =  membank1r_data_min;
    //                     portr_data7     =  membank1r_data_min;
    //                 end
    //                 12'd1 : begin
    //                     portr_data0     =  membank1r_data_min;
    //                     portr_data1     =  membank2r_data_min;
    //                     portr_data2     =  membank2r_data_min;
    //                     portr_data3     =  membank2r_data_min;
    //                     portr_data4     =  membank2r_data_min;
    //                     portr_data5     =  membank2r_data_min;
    //                     portr_data6     =  membank2r_data_min;
    //                     portr_data7     =  membank2r_data_min;
    //                 end
    //                 12'd2 : begin
    //                     portr_data0     =  membank1r_data_min;
    //                     portr_data1     =  membank2r_data_min;
    //                     portr_data2     =  membank3r_data_min;
    //                     portr_data3     =  membank3r_data_min;
    //                     portr_data4     =  membank3r_data_min;
    //                     portr_data5     =  membank3r_data_min;
    //                     portr_data6     =  membank3r_data_min;
    //                     portr_data7     =  membank3r_data_min;
    //                 end
    //                 12'd3 : begin
    //                     portr_data0     =  membank1r_data_min;
    //                     portr_data1     =  membank2r_data_min;
    //                     portr_data2     =  membank3r_data_min;
    //                     portr_data3     =  membank4r_data_min;
    //                     portr_data4     =  membank4r_data_min;
    //                     portr_data5     =  membank4r_data_min;
    //                     portr_data6     =  membank4r_data_min;
    //                     portr_data7     =  membank4r_data_min;
    //                 end
    //                 12'd4 : begin
    //                     portr_data0     =  membank1r_data_min;
    //                     portr_data1     =  membank2r_data_min;
    //                     portr_data2     =  membank3r_data_min;
    //                     portr_data3     =  membank4r_data_min;
    //                     portr_data4     =  membank5r_data_min;
    //                     portr_data5     =  membank5r_data_min;
    //                     portr_data6     =  membank5r_data_min;
    //                     portr_data7     =  membank5r_data_min;
    //                 end
    //                 12'd5 : begin
    //                     portr_data0     =  membank1r_data_min;
    //                     portr_data1     =  membank2r_data_min;
    //                     portr_data2     =  membank3r_data_min;
    //                     portr_data3     =  membank4r_data_min;
    //                     portr_data4     =  membank5r_data_min;
    //                     portr_data5     =  membank6r_data_min;
    //                     portr_data6     =  membank6r_data_min;
    //                     portr_data7     =  membank6r_data_min;
    //                 end
    //                 12'd6 : begin
    //                     portr_data0     =  membank1r_data_min;
    //                     portr_data1     =  membank2r_data_min;
    //                     portr_data2     =  membank3r_data_min;
    //                     portr_data3     =  membank4r_data_min;
    //                     portr_data4     =  membank5r_data_min;
    //                     portr_data5     =  membank6r_data_min;
    //                     portr_data6     =  membank7r_data_min;
    //                     portr_data7     =  membank7r_data_min;
    //                 end
    //                 default : begin
    //                     portr_data0     =  membank1r_data_min;
    //                     portr_data1     =  membank2r_data_min;
    //                     portr_data2     =  membank3r_data_min;
    //                     portr_data3     =  membank4r_data_min;
    //                     portr_data4     =  membank5r_data_min;
    //                     portr_data5     =  membank6r_data_min;
    //                     portr_data6     =  membank7r_data_min;
    //                     portr_data7     =  membank0r_data_min;
    //                 end
    //             endcase
    //         end

    //         3'd2 : begin
    //             // membanksr_en    = {8{portr_en}};//8'b01111100 & {8{portr_en}};

    //             // membank0r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank1r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank2r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank3r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank4r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank5r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank6r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank7r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

    //             case(max_valid_sub_portr_addr)
    //                 12'd0 : begin
    //                     portr_data0     =  membank2r_data_min;
    //                     portr_data1     =  membank2r_data_min;
    //                     portr_data2     =  membank2r_data_min;
    //                     portr_data3     =  membank2r_data_min;
    //                     portr_data4     =  membank2r_data_min;
    //                     portr_data5     =  membank2r_data_min;
    //                     portr_data6     =  membank2r_data_min;
    //                     portr_data7     =  membank2r_data_min;
    //                 end
    //                 12'd1 : begin
    //                     portr_data0     =  membank2r_data_min;
    //                     portr_data1     =  membank3r_data_min;
    //                     portr_data2     =  membank3r_data_min;
    //                     portr_data3     =  membank3r_data_min;
    //                     portr_data4     =  membank3r_data_min;
    //                     portr_data5     =  membank3r_data_min;
    //                     portr_data6     =  membank3r_data_min;
    //                     portr_data7     =  membank3r_data_min;
    //                 end
    //                 12'd2 : begin
    //                     portr_data0     =  membank2r_data_min;
    //                     portr_data1     =  membank3r_data_min;
    //                     portr_data2     =  membank4r_data_min;
    //                     portr_data3     =  membank4r_data_min;
    //                     portr_data4     =  membank4r_data_min;
    //                     portr_data5     =  membank4r_data_min;
    //                     portr_data6     =  membank4r_data_min;
    //                     portr_data7     =  membank4r_data_min;
    //                 end
    //                 12'd3 : begin
    //                     portr_data0     =  membank2r_data_min;
    //                     portr_data1     =  membank3r_data_min;
    //                     portr_data2     =  membank4r_data_min;
    //                     portr_data3     =  membank5r_data_min;
    //                     portr_data4     =  membank5r_data_min;
    //                     portr_data5     =  membank5r_data_min;
    //                     portr_data6     =  membank5r_data_min;
    //                     portr_data7     =  membank5r_data_min;
    //                 end
    //                 12'd4 : begin
    //                     portr_data0     =  membank2r_data_min;
    //                     portr_data1     =  membank3r_data_min;
    //                     portr_data2     =  membank4r_data_min;
    //                     portr_data3     =  membank5r_data_min;
    //                     portr_data4     =  membank6r_data_min;
    //                     portr_data5     =  membank6r_data_min;
    //                     portr_data6     =  membank6r_data_min;
    //                     portr_data7     =  membank6r_data_min;
    //                 end
    //                 12'd5 : begin
    //                     portr_data0     =  membank2r_data_min;
    //                     portr_data1     =  membank3r_data_min;
    //                     portr_data2     =  membank4r_data_min;
    //                     portr_data3     =  membank5r_data_min;
    //                     portr_data4     =  membank6r_data_min;
    //                     portr_data5     =  membank7r_data_min;
    //                     portr_data6     =  membank7r_data_min;
    //                     portr_data7     =  membank7r_data_min;
    //                 end
    //                 12'd6 : begin
    //                     portr_data0     =  membank2r_data_min;
    //                     portr_data1     =  membank3r_data_min;
    //                     portr_data2     =  membank4r_data_min;
    //                     portr_data3     =  membank5r_data_min;
    //                     portr_data4     =  membank6r_data_min;
    //                     portr_data5     =  membank7r_data_min;
    //                     portr_data6     =  membank0r_data_min;
    //                     portr_data7     =  membank0r_data_min;
    //                 end
    //                 default : begin
    //                     portr_data0     =  membank2r_data_min;
    //                     portr_data1     =  membank3r_data_min;
    //                     portr_data2     =  membank4r_data_min;
    //                     portr_data3     =  membank5r_data_min;
    //                     portr_data4     =  membank6r_data_min;
    //                     portr_data5     =  membank7r_data_min;
    //                     portr_data6     =  membank0r_data_min;
    //                     portr_data7     =  membank1r_data_min;
    //                 end
    //             endcase
    //         end

    //         3'd3 : begin
    //             // membanksr_en    = {8{portr_en}};//8'b11111000 & {8{portr_en}};

    //             // membank0r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank1r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank2r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank3r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank4r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank5r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank6r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank7r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

    //             case(max_valid_sub_portr_addr)
    //                 12'd0 : begin
    //                     portr_data0     =  membank3r_data_min;
    //                     portr_data1     =  membank3r_data_min;
    //                     portr_data2     =  membank3r_data_min;
    //                     portr_data3     =  membank3r_data_min;
    //                     portr_data4     =  membank3r_data_min;
    //                     portr_data5     =  membank3r_data_min;
    //                     portr_data6     =  membank3r_data_min;
    //                     portr_data7     =  membank3r_data_min;
    //                 end
    //                 12'd1 : begin
    //                     portr_data0     =  membank3r_data_min;
    //                     portr_data1     =  membank4r_data_min;
    //                     portr_data2     =  membank4r_data_min;
    //                     portr_data3     =  membank4r_data_min;
    //                     portr_data4     =  membank4r_data_min;
    //                     portr_data5     =  membank4r_data_min;
    //                     portr_data6     =  membank4r_data_min;
    //                     portr_data7     =  membank4r_data_min;
    //                 end
    //                 12'd2 : begin
    //                     portr_data0     =  membank3r_data_min;
    //                     portr_data1     =  membank4r_data_min;
    //                     portr_data2     =  membank5r_data_min;
    //                     portr_data3     =  membank5r_data_min;
    //                     portr_data4     =  membank5r_data_min;
    //                     portr_data5     =  membank5r_data_min;
    //                     portr_data6     =  membank5r_data_min;
    //                     portr_data7     =  membank5r_data_min;
    //                 end
    //                 12'd3 : begin
    //                     portr_data0     =  membank3r_data_min;
    //                     portr_data1     =  membank4r_data_min;
    //                     portr_data2     =  membank5r_data_min;
    //                     portr_data3     =  membank6r_data_min;
    //                     portr_data4     =  membank6r_data_min;
    //                     portr_data5     =  membank6r_data_min;
    //                     portr_data6     =  membank6r_data_min;
    //                     portr_data7     =  membank6r_data_min;
    //                 end
    //                 12'd4 : begin
    //                     portr_data0     =  membank3r_data_min;
    //                     portr_data1     =  membank4r_data_min;
    //                     portr_data2     =  membank5r_data_min;
    //                     portr_data3     =  membank6r_data_min;
    //                     portr_data4     =  membank7r_data_min;
    //                     portr_data5     =  membank7r_data_min;
    //                     portr_data6     =  membank7r_data_min;
    //                     portr_data7     =  membank7r_data_min;
    //                 end
    //                 12'd5 : begin
    //                     portr_data0     =  membank3r_data_min;
    //                     portr_data1     =  membank4r_data_min;
    //                     portr_data2     =  membank5r_data_min;
    //                     portr_data3     =  membank6r_data_min;
    //                     portr_data4     =  membank7r_data_min;
    //                     portr_data5     =  membank0r_data_min;
    //                     portr_data6     =  membank0r_data_min;
    //                     portr_data7     =  membank0r_data_min;
    //                 end
    //                 12'd6 : begin
    //                     portr_data0     =  membank3r_data_min;
    //                     portr_data1     =  membank4r_data_min;
    //                     portr_data2     =  membank5r_data_min;
    //                     portr_data3     =  membank6r_data_min;
    //                     portr_data4     =  membank7r_data_min;
    //                     portr_data5     =  membank0r_data_min;
    //                     portr_data6     =  membank1r_data_min;
    //                     portr_data7     =  membank1r_data_min;
    //                 end
    //                 default : begin
    //                     portr_data0     =  membank3r_data_min;
    //                     portr_data1     =  membank4r_data_min;
    //                     portr_data2     =  membank5r_data_min;
    //                     portr_data3     =  membank6r_data_min;
    //                     portr_data4     =  membank7r_data_min;
    //                     portr_data5     =  membank0r_data_min;
    //                     portr_data6     =  membank1r_data_min;
    //                     portr_data7     =  membank2r_data_min;
    //                 end
    //             endcase
    //         end

    //         3'd4 : begin
    //             // membanksr_en    = {8{portr_en}};//8'b11110001 & {8{portr_en}};

    //             // membank0r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank1r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank2r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank3r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank4r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank5r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank6r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank7r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

    //             case(max_valid_sub_portr_addr)
    //                 12'd0 : begin
    //                     portr_data0     =  membank4r_data_min;
    //                     portr_data1     =  membank4r_data_min;
    //                     portr_data2     =  membank4r_data_min;
    //                     portr_data3     =  membank4r_data_min;
    //                     portr_data4     =  membank4r_data_min;
    //                     portr_data5     =  membank4r_data_min;
    //                     portr_data6     =  membank4r_data_min;
    //                     portr_data7     =  membank4r_data_min;
    //                 end
    //                 12'd1 : begin
    //                     portr_data0     =  membank4r_data_min;
    //                     portr_data1     =  membank5r_data_min;
    //                     portr_data2     =  membank5r_data_min;
    //                     portr_data3     =  membank5r_data_min;
    //                     portr_data4     =  membank5r_data_min;
    //                     portr_data5     =  membank5r_data_min;
    //                     portr_data6     =  membank5r_data_min;
    //                     portr_data7     =  membank5r_data_min;
    //                 end
    //                 12'd2 : begin
    //                     portr_data0     =  membank4r_data_min;
    //                     portr_data1     =  membank5r_data_min;
    //                     portr_data2     =  membank6r_data_min;
    //                     portr_data3     =  membank6r_data_min;
    //                     portr_data4     =  membank6r_data_min;
    //                     portr_data5     =  membank6r_data_min;
    //                     portr_data6     =  membank6r_data_min;
    //                     portr_data7     =  membank6r_data_min;
    //                 end
    //                 12'd3 : begin
    //                     portr_data0     =  membank4r_data_min;
    //                     portr_data1     =  membank5r_data_min;
    //                     portr_data2     =  membank6r_data_min;
    //                     portr_data3     =  membank7r_data_min;
    //                     portr_data4     =  membank7r_data_min;
    //                     portr_data5     =  membank7r_data_min;
    //                     portr_data6     =  membank7r_data_min;
    //                     portr_data7     =  membank7r_data_min;
    //                 end
    //                 12'd4 : begin
    //                     portr_data0     =  membank4r_data_min;
    //                     portr_data1     =  membank5r_data_min;
    //                     portr_data2     =  membank6r_data_min;
    //                     portr_data3     =  membank7r_data_min;
    //                     portr_data4     =  membank0r_data_min;
    //                     portr_data5     =  membank0r_data_min;
    //                     portr_data6     =  membank0r_data_min;
    //                     portr_data7     =  membank0r_data_min;
    //                 end
    //                 12'd5 : begin
    //                     portr_data0     =  membank4r_data_min;
    //                     portr_data1     =  membank5r_data_min;
    //                     portr_data2     =  membank6r_data_min;
    //                     portr_data3     =  membank7r_data_min;
    //                     portr_data4     =  membank0r_data_min;
    //                     portr_data5     =  membank1r_data_min;
    //                     portr_data6     =  membank1r_data_min;
    //                     portr_data7     =  membank1r_data_min;
    //                 end
    //                 12'd6 : begin
    //                     portr_data0     =  membank4r_data_min;
    //                     portr_data1     =  membank5r_data_min;
    //                     portr_data2     =  membank6r_data_min;
    //                     portr_data3     =  membank7r_data_min;
    //                     portr_data4     =  membank0r_data_min;
    //                     portr_data5     =  membank1r_data_min;
    //                     portr_data6     =  membank2r_data_min;
    //                     portr_data7     =  membank2r_data_min;
    //                 end
    //                 default : begin
    //                     portr_data0     =  membank4r_data_min;
    //                     portr_data1     =  membank5r_data_min;
    //                     portr_data2     =  membank6r_data_min;
    //                     portr_data3     =  membank7r_data_min;
    //                     portr_data4     =  membank0r_data_min;
    //                     portr_data5     =  membank1r_data_min;
    //                     portr_data6     =  membank2r_data_min;
    //                     portr_data7     =  membank3r_data_min;
    //                 end
    //             endcase
    //         end

    //         3'd5 : begin
    //             // membanksr_en    = {8{portr_en}};//8'b11000111 & {8{portr_en}};

    //             // membank0r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank1r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank2r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank3r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank4r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank5r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank6r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank7r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

    //             case(max_valid_sub_portr_addr)
    //                 12'd0 : begin
    //                     portr_data0     =  membank5r_data_min;
    //                     portr_data1     =  membank5r_data_min;
    //                     portr_data2     =  membank5r_data_min;
    //                     portr_data3     =  membank5r_data_min;
    //                     portr_data4     =  membank5r_data_min;
    //                     portr_data5     =  membank5r_data_min;
    //                     portr_data6     =  membank5r_data_min;
    //                     portr_data7     =  membank5r_data_min;
    //                 end
    //                 12'd1 : begin
    //                     portr_data0     =  membank5r_data_min;
    //                     portr_data1     =  membank6r_data_min;
    //                     portr_data2     =  membank6r_data_min;
    //                     portr_data3     =  membank6r_data_min;
    //                     portr_data4     =  membank6r_data_min;
    //                     portr_data5     =  membank6r_data_min;
    //                     portr_data6     =  membank6r_data_min;
    //                     portr_data7     =  membank6r_data_min;
    //                 end
    //                 12'd2 : begin
    //                     portr_data0     =  membank5r_data_min;
    //                     portr_data1     =  membank6r_data_min;
    //                     portr_data2     =  membank7r_data_min;
    //                     portr_data3     =  membank7r_data_min;
    //                     portr_data4     =  membank7r_data_min;
    //                     portr_data5     =  membank7r_data_min;
    //                     portr_data6     =  membank7r_data_min;
    //                     portr_data7     =  membank7r_data_min;
    //                 end
    //                 12'd3 : begin
    //                     portr_data0     =  membank5r_data_min;
    //                     portr_data1     =  membank6r_data_min;
    //                     portr_data2     =  membank7r_data_min;
    //                     portr_data3     =  membank0r_data_min;
    //                     portr_data4     =  membank0r_data_min;
    //                     portr_data5     =  membank0r_data_min;
    //                     portr_data6     =  membank0r_data_min;
    //                     portr_data7     =  membank0r_data_min;
    //                 end
    //                 12'd4 : begin
    //                     portr_data0     =  membank5r_data_min;
    //                     portr_data1     =  membank6r_data_min;
    //                     portr_data2     =  membank7r_data_min;
    //                     portr_data3     =  membank0r_data_min;
    //                     portr_data4     =  membank1r_data_min;
    //                     portr_data5     =  membank1r_data_min;
    //                     portr_data6     =  membank1r_data_min;
    //                     portr_data7     =  membank1r_data_min;
    //                 end
    //                 12'd5 : begin
    //                     portr_data0     =  membank5r_data_min;
    //                     portr_data1     =  membank6r_data_min;
    //                     portr_data2     =  membank7r_data_min;
    //                     portr_data3     =  membank0r_data_min;
    //                     portr_data4     =  membank1r_data_min;
    //                     portr_data5     =  membank2r_data_min;
    //                     portr_data6     =  membank2r_data_min;
    //                     portr_data7     =  membank2r_data_min;
    //                 end
    //                 12'd6 : begin
    //                     portr_data0     =  membank5r_data_min;
    //                     portr_data1     =  membank6r_data_min;
    //                     portr_data2     =  membank7r_data_min;
    //                     portr_data3     =  membank0r_data_min;
    //                     portr_data4     =  membank1r_data_min;
    //                     portr_data5     =  membank2r_data_min;
    //                     portr_data6     =  membank3r_data_min;
    //                     portr_data7     =  membank3r_data_min;
    //                 end
    //                 default : begin
    //                     portr_data0     =  membank5r_data_min;
    //                     portr_data1     =  membank6r_data_min;
    //                     portr_data2     =  membank7r_data_min;
    //                     portr_data3     =  membank0r_data_min;
    //                     portr_data4     =  membank1r_data_min;
    //                     portr_data5     =  membank2r_data_min;
    //                     portr_data6     =  membank3r_data_min;
    //                     portr_data7     =  membank4r_data_min;
    //                 end
    //             endcase
    //         end

    //         3'd6 : begin
    //             // membanksr_en     = {8{portr_en}};//8'b10001111 & {8{portr_en}};

    //             // membank0r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank1r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank2r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank3r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank4r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank5r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank6r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];
    //             // membank7r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

    //             case(max_valid_sub_portr_addr)
    //                 12'd0 : begin
    //                     portr_data0     =  membank6r_data_min;
    //                     portr_data1     =  membank6r_data_min;
    //                     portr_data2     =  membank6r_data_min;
    //                     portr_data3     =  membank6r_data_min;
    //                     portr_data4     =  membank6r_data_min;
    //                     portr_data5     =  membank6r_data_min;
    //                     portr_data6     =  membank6r_data_min;
    //                     portr_data7     =  membank6r_data_min;
    //                 end
    //                 12'd1 : begin
    //                     portr_data0     =  membank6r_data_min;
    //                     portr_data1     =  membank7r_data_min;
    //                     portr_data2     =  membank7r_data_min;
    //                     portr_data3     =  membank7r_data_min;
    //                     portr_data4     =  membank7r_data_min;
    //                     portr_data5     =  membank7r_data_min;
    //                     portr_data6     =  membank7r_data_min;
    //                     portr_data7     =  membank7r_data_min;
    //                 end
    //                 12'd2 : begin
    //                     portr_data0     =  membank6r_data_min;
    //                     portr_data1     =  membank7r_data_min;
    //                     portr_data2     =  membank0r_data_min;
    //                     portr_data3     =  membank0r_data_min;
    //                     portr_data4     =  membank0r_data_min;
    //                     portr_data5     =  membank0r_data_min;
    //                     portr_data6     =  membank0r_data_min;
    //                     portr_data7     =  membank0r_data_min;
    //                 end
    //                 12'd3 : begin
    //                     portr_data0     =  membank6r_data_min;
    //                     portr_data1     =  membank7r_data_min;
    //                     portr_data2     =  membank0r_data_min;
    //                     portr_data3     =  membank1r_data_min;
    //                     portr_data4     =  membank1r_data_min;
    //                     portr_data5     =  membank1r_data_min;
    //                     portr_data6     =  membank1r_data_min;
    //                     portr_data7     =  membank1r_data_min;
    //                 end
    //                 12'd4 : begin
    //                     portr_data0     =  membank6r_data_min;
    //                     portr_data1     =  membank7r_data_min;
    //                     portr_data2     =  membank0r_data_min;
    //                     portr_data3     =  membank1r_data_min;
    //                     portr_data4     =  membank2r_data_min;
    //                     portr_data5     =  membank2r_data_min;
    //                     portr_data6     =  membank2r_data_min;
    //                     portr_data7     =  membank2r_data_min;
    //                 end
    //                 12'd5 : begin
    //                     portr_data0     =  membank6r_data_min;
    //                     portr_data1     =  membank7r_data_min;
    //                     portr_data2     =  membank0r_data_min;
    //                     portr_data3     =  membank1r_data_min;
    //                     portr_data4     =  membank2r_data_min;
    //                     portr_data5     =  membank3r_data_min;
    //                     portr_data6     =  membank3r_data_min;
    //                     portr_data7     =  membank3r_data_min;
    //                 end
    //                 12'd6 : begin
    //                     portr_data0     =  membank6r_data_min;
    //                     portr_data1     =  membank7r_data_min;
    //                     portr_data2     =  membank0r_data_min;
    //                     portr_data3     =  membank1r_data_min;
    //                     portr_data4     =  membank2r_data_min;
    //                     portr_data5     =  membank3r_data_min;
    //                     portr_data6     =  membank4r_data_min;
    //                     portr_data7     =  membank4r_data_min;
    //                 end
    //                 default : begin
    //                     portr_data0     =  membank6r_data_min;
    //                     portr_data1     =  membank7r_data_min;
    //                     portr_data2     =  membank0r_data_min;
    //                     portr_data3     =  membank1r_data_min;
    //                     portr_data4     =  membank2r_data_min;
    //                     portr_data5     =  membank3r_data_min;
    //                     portr_data6     =  membank4r_data_min;
    //                     portr_data7     =  membank5r_data_min;
    //                 end
    //             endcase
    //         end
    //         3'd7 : begin
    //             // membanksr_en     = {8{portr_en}};//8'b10001111 & {8{portr_en}};

    //             // membank0r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank1r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank2r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank3r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank4r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank5r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank6r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS] + 1'b1;
    //             // membank7r_addr  = portr_addr[LOG2_FRAME_SIZE-1:LOG2_NUM_OF_BANKS];

    //             case(max_valid_sub_portr_addr)
    //                 12'd0 : begin
    //                     portr_data0     =  membank7r_data_min;
    //                     portr_data1     =  membank7r_data_min;
    //                     portr_data2     =  membank7r_data_min;
    //                     portr_data3     =  membank7r_data_min;
    //                     portr_data4     =  membank7r_data_min;
    //                     portr_data5     =  membank7r_data_min;
    //                     portr_data6     =  membank7r_data_min;
    //                     portr_data7     =  membank7r_data_min;
    //                 end
    //                 12'd1 : begin
    //                     portr_data0     =  membank7r_data_min;
    //                     portr_data1     =  membank0r_data_min;
    //                     portr_data2     =  membank0r_data_min;
    //                     portr_data3     =  membank0r_data_min;
    //                     portr_data4     =  membank0r_data_min;
    //                     portr_data5     =  membank0r_data_min;
    //                     portr_data6     =  membank0r_data_min;
    //                     portr_data7     =  membank0r_data_min;
    //                 end
    //                 12'd2 : begin
    //                     portr_data0     =  membank7r_data_min;
    //                     portr_data1     =  membank0r_data_min;
    //                     portr_data2     =  membank1r_data_min;
    //                     portr_data3     =  membank1r_data_min;
    //                     portr_data4     =  membank1r_data_min;
    //                     portr_data5     =  membank1r_data_min;
    //                     portr_data6     =  membank1r_data_min;
    //                     portr_data7     =  membank1r_data_min;
    //                 end
    //                 12'd3 : begin
    //                     portr_data0     =  membank7r_data_min;
    //                     portr_data1     =  membank0r_data_min;
    //                     portr_data2     =  membank1r_data_min;
    //                     portr_data3     =  membank2r_data_min;
    //                     portr_data4     =  membank2r_data_min;
    //                     portr_data5     =  membank2r_data_min;
    //                     portr_data6     =  membank2r_data_min;
    //                     portr_data7     =  membank2r_data_min;
    //                 end
    //                 12'd4 : begin
    //                     portr_data0     =  membank7r_data_min;
    //                     portr_data1     =  membank0r_data_min;
    //                     portr_data2     =  membank1r_data_min;
    //                     portr_data3     =  membank2r_data_min;
    //                     portr_data4     =  membank3r_data_min;
    //                     portr_data5     =  membank3r_data_min;
    //                     portr_data6     =  membank3r_data_min;
    //                     portr_data7     =  membank3r_data_min;
    //                 end
    //                 12'd5 : begin
    //                     portr_data0     =  membank7r_data_min;
    //                     portr_data1     =  membank0r_data_min;
    //                     portr_data2     =  membank1r_data_min;
    //                     portr_data3     =  membank2r_data_min;
    //                     portr_data4     =  membank3r_data_min;
    //                     portr_data5     =  membank4r_data_min;
    //                     portr_data6     =  membank4r_data_min;
    //                     portr_data7     =  membank4r_data_min;
    //                 end
    //                 12'd6 : begin
    //                     portr_data0     =  membank7r_data_min;
    //                     portr_data1     =  membank0r_data_min;
    //                     portr_data2     =  membank1r_data_min;
    //                     portr_data3     =  membank2r_data_min;
    //                     portr_data4     =  membank3r_data_min;
    //                     portr_data5     =  membank4r_data_min;
    //                     portr_data6     =  membank5r_data_min;
    //                     portr_data7     =  membank5r_data_min;
    //                 end
    //                 default : begin
    //                     portr_data0     =  membank7r_data_min;
    //                     portr_data1     =  membank0r_data_min;
    //                     portr_data2     =  membank1r_data_min;
    //                     portr_data3     =  membank2r_data_min;
    //                     portr_data4     =  membank3r_data_min;
    //                     portr_data5     =  membank4r_data_min;
    //                     portr_data6     =  membank5r_data_min;
    //                     portr_data7     =  membank6r_data_min;
    //                 end
    //             endcase
    //         end
    //     endcase
    // end




endmodule
