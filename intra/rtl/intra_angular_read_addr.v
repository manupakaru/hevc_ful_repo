module intra_angular_read_addr
    (
        clk,
        reset,
        enable,

        abs_intrapredangle_in,
        sgn_intrapredangle_in,
        inner_mainaxis_shifted_2_in,
        inner_auxaxis_in,
        main_t_in,
        // aux_t_in,
        // valid_in,
        mode_in,
        dc_done_in,
        dc_read_addr_offset_in,
        mode_modified_for_ver_hor_out,

        filter_flag_in,
        filter_done_in,
        filter_addr_offset_in,
        bi_int_flag_in,

        max_valid_range_in,
        max_valid_range_valid_in,

        read_from_aux_flag,
        read_addr_out,
        // valid_out,

        intrapredacc_lower5b,
        extended_arr_index_4clks_delay_out,
        extended_arr_index_out
        // extended_arr_index_4clks_delay_out_5clks_early_out
    );

//----------------------------------------------------
//LOCALPARAMS
//----------------------------------------------------
    localparam                      INTRA_PRED_ANGLE_WIDTH      = 6;
    localparam                      MAX_NTBS_SIZE               = 6;
    localparam                      NUM_OF_DATA_REGS            = 5;
    localparam                      PIXEL_WIDTH                 = 8;
    localparam                      LOG2_FRAME_SIZE             = 12;
    localparam                      VALID_RANGE_SHIFTER_LENGTH  = 1;
    localparam                      MODE_WIDTH                  = 2;

    localparam                      TOP_PRED_MODE_WIDTH         = 2;
    localparam                      TOP_PRED_MODE_DC            = 0;
    localparam                      TOP_PRED_MODE_PLANAR        = 1;
    localparam                      TOP_PRED_MODE_ANGULAR_TOP   = 2;
    localparam                      TOP_PRED_MODE_ANGULAR_LEFT  = 3;


//----------------------------------------------------
//PARAMETERS
//----------------------------------------------------

//----------------------------------------------------
//I/O Signals
//----------------------------------------------------


    input               clk;
    input               reset;
    input               enable;

    input               [INTRA_PRED_ANGLE_WIDTH - 1:0]  abs_intrapredangle_in;
    input                                               sgn_intrapredangle_in;
    input               [MAX_NTBS_SIZE - 1:0]           inner_mainaxis_shifted_2_in;
    input               [MAX_NTBS_SIZE - 1:0]           inner_auxaxis_in;
    input               [LOG2_FRAME_SIZE - 1:0]         main_t_in;
    // input               [LOG2_FRAME_SIZE - 1:0]         aux_t_in;
    // input                                               valid_in;
    input               [TOP_PRED_MODE_WIDTH - 1:0]     mode_in;
    input                                               dc_done_in;
    input               [MAX_NTBS_SIZE - 1:0]           dc_read_addr_offset_in;

    input                                               filter_flag_in;
    input                                               filter_done_in;
    input               [MAX_NTBS_SIZE - 1:0]           filter_addr_offset_in;
    input                                               bi_int_flag_in;

    input               [LOG2_FRAME_SIZE - 1:0]         max_valid_range_in;
    input                                               max_valid_range_valid_in;

    output reg                                          read_from_aux_flag;
    output reg          [LOG2_FRAME_SIZE - 1:0]         read_addr_out;
    // output reg                                          valid_out;

    output              [4:0]                           intrapredacc_lower5b;
    output              [6:0]                           extended_arr_index_4clks_delay_out;
    // output              [6:0]                           extended_arr_index_4clks_delay_out_5clks_early_out;
    output              [6 - 1:0]                       extended_arr_index_out;

    output reg          [TOP_PRED_MODE_WIDTH - 1:0]     mode_modified_for_ver_hor_out;

//----------------------------------------------------
//  Internal Regs and Wires
//----------------------------------------------------

    reg         [3:1]                   sgn_intrapredangle_reg;
    // reg                                 sgn_intrapredangle_reg_d;
    reg         [MAX_NTBS_SIZE - 1:0]   inner_mainaxis_shifted_2_reg[3:1];
    // reg         [MAX_NTBS_SIZE - 1:0]   inner_auxaxis_reg;

    reg         [LOG2_FRAME_SIZE - 1:0] intrapredangle_acc[3:1];
    reg         [5 - 1:0]               intrapredangle_acc_delay[1:0];
    // reg         [LOG2_FRAME_SIZE - 1:0] intrapredangle_acc_delay[2:0];
    reg         [LOG2_FRAME_SIZE - 1:0] read_addr;
    // reg         [LOG2_FRAME_SIZE - 1:0] angular_read_addr[2:0];
    // reg                                 valid_reg;
    reg         [LOG2_FRAME_SIZE - 1:0] inner_minus_predangle_offset_reg;
    reg         [LOG2_FRAME_SIZE - 1:0] abs_inner_minus_predangle_offset_int;
    reg         [7 - 1:0]               abs_inner_minus_predangle_offset7b_reg[3:1];

    reg         [2:0]                   force_read_from_aux_flag;

    reg         [LOG2_FRAME_SIZE - 1:0] max_valid_range_reg [VALID_RANGE_SHIFTER_LENGTH - 1:0];
	
	reg 								filter_done_delay;
	reg 								filtered_samples_read;

//----------------------------------------------------
//  Implementation
//----------------------------------------------------

	always @(posedge clk) begin
		if(reset) begin
			filter_done_delay <= 1'b0;
			filtered_samples_read <= 1'b0;
		end
		else begin
			filter_done_delay <= filter_done_in;
			filtered_samples_read <= (filtered_samples_read&filter_flag_in) | ((~filter_done_delay)&filter_done_in);
		end
	end

    always @(posedge clk) begin
        if (reset) begin
            // reset

        end
        else if (enable) begin
            if(mode_in == TOP_PRED_MODE_ANGULAR_LEFT || mode_in == TOP_PRED_MODE_ANGULAR_TOP) begin
                if(abs_intrapredangle_in == {INTRA_PRED_ANGLE_WIDTH{1'b0}}) begin
                    mode_modified_for_ver_hor_out <= TOP_PRED_MODE_PLANAR;
                end
                else begin
                    mode_modified_for_ver_hor_out <= mode_in;
                end
            end
            else begin
                mode_modified_for_ver_hor_out <= mode_in;
            end
        end
    end


    always @(posedge clk /*or posedge reset*/) begin : main
       integer i;
       if (reset) begin
            intrapredangle_acc[3]               <= {LOG2_FRAME_SIZE{1'b0}};
            inner_mainaxis_shifted_2_reg[3]     <= {MAX_NTBS_SIZE{1'b0}};
            intrapredangle_acc[2]               <= {LOG2_FRAME_SIZE{1'b0}};
            inner_mainaxis_shifted_2_reg[2]     <= {MAX_NTBS_SIZE{1'b0}};
            intrapredangle_acc[1]               <= {LOG2_FRAME_SIZE{1'b0}};
            inner_mainaxis_shifted_2_reg[1]     <= {MAX_NTBS_SIZE{1'b0}};
            // intrapredangle_acc[0]               <= {LOG2_FRAME_SIZE{1'b0}};
            // inner_mainaxis_shifted_2_reg[0]     <= {MAX_NTBS_SIZE{1'b0}};
            // intrapredangle_acc_delay[1]         <= {MAX_NTBS_SIZE{1'b0}};
            intrapredangle_acc_delay[1]         <= {5{1'b0}};
            intrapredangle_acc_delay[0]         <= {5{1'b0}};
            sgn_intrapredangle_reg              <= 4'd0;
       end
       else if(enable) begin
            // valid_reg                        <=   valid_in;
            // valid_out                        <=   valid_reg;

            if(max_valid_range_valid_in == 1'b1) begin
                max_valid_range_reg[0] <= max_valid_range_in;
                    for( i = 0 ; i < VALID_RANGE_SHIFTER_LENGTH - 1 ; i = i + 1 ) begin
                        max_valid_range_reg[i+1] <= max_valid_range_reg[i];
                    end
            end

            // if(valid_in) begin
               intrapredangle_acc[3]           <=   abs_intrapredangle_in*(inner_auxaxis_in + 1'b1);
               inner_mainaxis_shifted_2_reg[3] <=   inner_mainaxis_shifted_2_in;
               sgn_intrapredangle_reg[3]       <=   sgn_intrapredangle_in;
            // end

                intrapredangle_acc[2]           <=   intrapredangle_acc[3];
                inner_mainaxis_shifted_2_reg[2] <=   inner_mainaxis_shifted_2_reg[3];
                sgn_intrapredangle_reg[2]       <=   sgn_intrapredangle_reg[3];

                intrapredangle_acc[1]           <=   intrapredangle_acc[2];
                inner_mainaxis_shifted_2_reg[1] <=   inner_mainaxis_shifted_2_reg[2];
                sgn_intrapredangle_reg[1]       <=   sgn_intrapredangle_reg[2];

                intrapredangle_acc_delay[1]     <=   intrapredangle_acc[1][5 - 1:0];
                // inner_mainaxis_shifted_2_reg[0] <=   inner_mainaxis_shifted_2_reg[1];
                // sgn_intrapredangle_reg[0]       <=   sgn_intrapredangle_reg[1];

                intrapredangle_acc_delay[0]     <=   intrapredangle_acc_delay[1];
                // intrapredangle_acc_delay[0]     <=   intrapredangle_acc_delay[1];

                // angular_read_addr[2]        <=   main_t_in + inner_mainaxis_shifted_2_reg + intrapredangle_acc[11:5];
                // angular_read_addr[1]        <=   angular_read_addr[2];
                // angular_read_addr[0]        <=   angular_read_addr[1];

                // intrapredangle_acc_delay[2] <=   intrapredangle_acc[11:5];
                // intrapredangle_acc_delay[1] <=   intrapredangle_acc_delay[2];
                // intrapredangle_acc_delay[0] <=   intrapredangle_acc_delay[1];

                // sgn_intrapredangle_reg_d <= sgn_intrapredangle_reg[0];

                if (($signed(inner_minus_predangle_offset_reg) <= 0)&&(sgn_intrapredangle_reg[1] == 1'b1)) begin
                    force_read_from_aux_flag[2] <= 1'b1;
                end
                else begin
                    force_read_from_aux_flag[2] <= 1'b0;
                end
                force_read_from_aux_flag[1:0] <= force_read_from_aux_flag[2:1];


                if(filter_flag_in == 1'b0) begin
                    case(mode_modified_for_ver_hor_out)
                        TOP_PRED_MODE_ANGULAR_LEFT,TOP_PRED_MODE_ANGULAR_TOP : begin
                            if(sgn_intrapredangle_reg[2] == 1'b0) begin
                                read_addr           <= main_t_in + inner_mainaxis_shifted_2_reg[1] + intrapredangle_acc[1][11:5];
                                inner_minus_predangle_offset_reg <= {LOG2_FRAME_SIZE{1'b0}};
                                // read_from_aux_flag  <= 1'b0;
                            end
                            else begin
                                read_addr                        <= main_t_in + inner_mainaxis_shifted_2_reg[1] - intrapredangle_acc[1][11:5] - 1'b1;
                                inner_minus_predangle_offset_reg <= inner_mainaxis_shifted_2_reg[3] - intrapredangle_acc[3][11:5];
                                // read_from_aux_flag               <= 1'b1;
                            end
                        end
                        TOP_PRED_MODE_PLANAR : begin
                            read_addr           <= main_t_in + inner_mainaxis_shifted_2_reg[1] - 1'b1;
                            // read_from_aux_flag  <= 1'b1;
                        end
                        TOP_PRED_MODE_DC : begin
                            if(dc_done_in == 1'b1) begin
                                read_addr <= main_t_in + inner_mainaxis_shifted_2_in;
                            end
                            else begin
                                read_addr <= main_t_in + dc_read_addr_offset_in;
                            end
                            // read_from_aux_flag  <= 1'b0;
                        end
                    endcase
                end
                else begin
                    if(filter_done_in == 1'b1) begin
                        case(mode_modified_for_ver_hor_out)
                            TOP_PRED_MODE_ANGULAR_LEFT,TOP_PRED_MODE_ANGULAR_TOP : begin
                                if(sgn_intrapredangle_reg[2] == 1'b0) begin
                                    read_addr           <= inner_mainaxis_shifted_2_reg[1] + intrapredangle_acc[1][11:5];
                                    inner_minus_predangle_offset_reg <= {LOG2_FRAME_SIZE{1'b0}};
                                    // read_from_aux_flag  <= 1'b0;
                                end
                                else begin
                                    // read_addr           <= {{(LOG2_FRAME_SIZE-5){1'b0}},intrapredangle_acc[1][11:5]};
                                    read_addr           <= inner_mainaxis_shifted_2_reg[1] - intrapredangle_acc[1][11:5] - 1'b1;
                                    inner_minus_predangle_offset_reg <= inner_mainaxis_shifted_2_reg[3] - intrapredangle_acc[3][11:5];
                                    // read_from_aux_flag  <= 1'b1;
                                end
                            end
                            TOP_PRED_MODE_PLANAR : begin
                                read_addr           <= inner_mainaxis_shifted_2_reg[1] - 1'b1;
                                // read_from_aux_flag  <= 1'b1;
                            end
                            TOP_PRED_MODE_DC : begin
                                if(dc_done_in == 1'b1) begin
                                    read_addr <= /*main_t_in +*/ inner_mainaxis_shifted_2_in;
                                end
                                else begin
                                    read_addr <= /*main_t_in +*/ dc_read_addr_offset_in;
                                end
                                // read_from_aux_flag  <= 1'b0;
                            end
                        endcase
                    end
                    else begin
                        read_addr           <= main_t_in + filter_addr_offset_in;
                        // read_from_aux_flag  <= 1'b0;
                    end
                end



    end
end

    always @(*) begin

		if(((read_addr /*+ 1'b1*/ )> max_valid_range_reg[0])&&(read_addr[LOG2_FRAME_SIZE - 1] == 1'b0)&&(bi_int_flag_in == 1'b0)) begin
			if((filter_flag_in == 1'b1)&&(filtered_samples_read == 1'b1)) begin
				read_addr_out = (max_valid_range_reg[0] + 1'b1);
			end
			else begin
				read_addr_out = max_valid_range_reg[0];
			end
		end
		else begin
			// if((mode_modified_for_ver_hor_out == TOP_PRED_MODE_PLANAR)&&((filter_done_in | ~filter_flag_in) == 1'b1)) begin
			//     read_addr_out = read_addr - 1'b1;
			// end
			// else begin
				read_addr_out = read_addr;
			// end
		end
    end

    always @(*) begin
        case(mode_modified_for_ver_hor_out)
            TOP_PRED_MODE_ANGULAR_LEFT,TOP_PRED_MODE_ANGULAR_TOP : begin
 //               if((inner_minus_predangle_offset_reg[LOG2_FRAME_SIZE - 1] == 1'b1) || (force_read_from_aux_flag == 1'b1)) begin
                if(force_read_from_aux_flag[0] == 1'b1) begin
                    read_from_aux_flag = 1'b1;
                end
                else begin
                    read_from_aux_flag = 1'b0;
                end
            end
            TOP_PRED_MODE_PLANAR : begin
               read_from_aux_flag = 1'b1;
            end
            default : begin
               read_from_aux_flag = 1'b0;
            end
        endcase
    end

    always @(*) begin
        if(inner_minus_predangle_offset_reg[LOG2_FRAME_SIZE - 1] == 1'b1) begin
            abs_inner_minus_predangle_offset_int = -inner_minus_predangle_offset_reg;
        end
        else begin
            abs_inner_minus_predangle_offset_int = inner_minus_predangle_offset_reg;
        end
    end

    always @(posedge clk) begin
        if (reset) begin
            // reset

        end
        else if (enable) begin
            abs_inner_minus_predangle_offset7b_reg[3] <= abs_inner_minus_predangle_offset_int[7 - 1:0];
            abs_inner_minus_predangle_offset7b_reg[2] <= abs_inner_minus_predangle_offset7b_reg[3];
            abs_inner_minus_predangle_offset7b_reg[1] <= abs_inner_minus_predangle_offset7b_reg[2];
            // abs_inner_minus_predangle_offset7b_reg[0] <= abs_inner_minus_predangle_offset7b_reg[1];
        end
    end

assign intrapredacc_lower5b = intrapredangle_acc_delay[0];
assign extended_arr_index_4clks_delay_out = abs_inner_minus_predangle_offset7b_reg[1];
assign extended_arr_index_out = abs_inner_minus_predangle_offset_int[6 - 1:0];

// assign extended_arr_index_4clks_delay_out_5clks_early_out = intrapredangle_acc[3][11:5];

//    module intra_angular_refpixel_selection
//    (
//        .clk(clk),
//        .reset(reset),
//        .enable(1'b1),
//
//        .intra_pred_angle(abs_intrapredAngle_in),
//
//        .ref_pixel_minus_addr_in(read_addr[0]),
//        .ref_pixel_other_addr_out(read_other_addr_int),
//
//    );

//    always @(*) begin
//        if(max_valid_leftrange_in < read_other_addr_int) begin
//            read_other_addr_out = max_valid_leftrange_in;
//        end
//        else begin
//            read_other_addr_out = read_other_addr_int;
//        end
//    end

//    assign read_addr_0_out = read_addr_reg[0];
//    assign read_addr_1_out = read_addr_reg[1];
//    assign read_addr_2_out = read_addr_reg[2];
//    assign read_addr_3_out = read_addr_reg[3];
//    assign read_addr_4_out = read_addr_reg[4];


endmodule
