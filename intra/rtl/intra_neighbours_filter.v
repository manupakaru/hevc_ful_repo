`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:26:42 11/12/2013 
// Design Name: 
// Module Name:    intra_neighbours_filter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module intra_neighbours_filter
    (
        clk,
        reset,
        enable,
        
        top_zeroth_in,
        left_zeroth_in,
        top_2nt_in,
        left_2nt_in,
        corner_pixel_in,
        bi_int_flag_in,
        ntbs_in,
        top_all_invalid_in,
        left_all_invalid_in,
        
        top_data_0_in,
        top_data_1_in,
        top_data_2_in,
        top_data_3_in,
        top_data_4_in,
        top_data_5_in,
        top_data_6_in,
        top_data_7_in,
        
        left_data_0_in,
        left_data_1_in,
        left_data_2_in,
        left_data_3_in,
        left_data_4_in,
        left_data_5_in,
        left_data_6_in,
        left_data_7_in,
        
        filter_start_in,
        filter_done_out,
        filter_offset_out,

        corner_pixel_filtered_out,
        top_nt_plus1_filtered_out,
        left_nt_plus1_filtered_out,
        top_zeroth_filtered_out,
        left_zeroth_filtered_out,
        
        top_portr_addr,
        top_portr_data0,
        top_portr_data1,
        top_portr_data2,
        top_portr_data3,
        top_portr_data4,
        top_portr_data5,
        top_portr_data6,
        top_portr_data7,
        // top_portr_en,
        
        left_portr_addr,
        left_portr_data0,
        left_portr_data1,
        left_portr_data2,
        left_portr_data3,
        left_portr_data4,
        left_portr_data5,
        left_portr_data6,
        left_portr_data7
        // left_portr_en
        
    );
    
    
//----------------------------------------------------
//LOCALPARAMS
//----------------------------------------------------
    localparam                      INTRA_PRED_ANGLE_WIDTH      = 6;
    localparam                      MAX_NTBS_SIZE               = 6;
    localparam                      NUM_OF_DATA_REGS            = 5;
    localparam                      PIXEL_WIDTH                 = 8;
    localparam                      LOG2_FRAME_SIZE             = 12;
    localparam                      ANGULAR_MODE_WIDTH          = 2;
    localparam                      OUTPUT_BLOCK_SIZE           = 4;
    localparam                      INTRAPRED_ANGLE_LOW5B_WIDTH = 5;
    localparam                      INNER_X_LOW2B_WIDTH         = 2;
    localparam                      INNER_Y_LOW2B_WIDTH         = 2;
    localparam                      DC_MAXWIDTH                 = 8 + 9;
    localparam                      DC_COUNTER_WIDTH            = 4;
    localparam                      DC_CIDX_WIDTH               = 2;
    localparam                      BIT_DEPTH                   = 8;
    
    
    localparam                      FILTER_COUNTER_WIDTH        = 4;
    localparam                      LOG2NUM_OF_BANKS            = 3;
    localparam                      NUMBER_OF_BANKS             = (1 << LOG2NUM_OF_BANKS);

    localparam                      MODE_VERTICAL               = 0;
    localparam                      MODE_HORIZONTAL             = 1;
    
    localparam                      STATE_INIT                  = 0;
    localparam                      STATE_FILTER                = 1;

    localparam                      CORNER_ZERO_STATE_INIT      = 0;
    localparam                      CORNER_ZERO_STATE_WAIT1     = 1;
    localparam                      CORNER_ZERO_STATE_WAIT2     = 2;
    localparam                      CORNER_ZERO_STATE_WAIT3     = 3;
    localparam                      CORNER_ZERO_STATE_FILTER    = 4;


//----------------------------------------------------
// I/O
//----------------------------------------------------

    input                                   clk;
    input                                   reset;
    input                                   enable;
            
    input   [PIXEL_WIDTH - 1:0]             top_zeroth_in;
    input   [PIXEL_WIDTH - 1:0]             left_zeroth_in;
    input   [PIXEL_WIDTH - 1:0]             top_2nt_in;
    input   [PIXEL_WIDTH - 1:0]             left_2nt_in;
    input   [PIXEL_WIDTH - 1:0]             corner_pixel_in;
    input                                   bi_int_flag_in;
    input   [MAX_NTBS_SIZE - 1:0]           ntbs_in;
    input                                   top_all_invalid_in;
    input                                   left_all_invalid_in;
                
    input   [PIXEL_WIDTH - 1:0]             top_data_0_in;
    input   [PIXEL_WIDTH - 1:0]             top_data_1_in;
    input   [PIXEL_WIDTH - 1:0]             top_data_2_in;
    input   [PIXEL_WIDTH - 1:0]             top_data_3_in;
    input   [PIXEL_WIDTH - 1:0]             top_data_4_in;
    input   [PIXEL_WIDTH - 1:0]             top_data_5_in;
    input   [PIXEL_WIDTH - 1:0]             top_data_6_in;
    input   [PIXEL_WIDTH - 1:0]             top_data_7_in;
            
    input   [PIXEL_WIDTH - 1:0]             left_data_0_in;
    input   [PIXEL_WIDTH - 1:0]             left_data_1_in;
    input   [PIXEL_WIDTH - 1:0]             left_data_2_in;
    input   [PIXEL_WIDTH - 1:0]             left_data_3_in;
    input   [PIXEL_WIDTH - 1:0]             left_data_4_in;
    input   [PIXEL_WIDTH - 1:0]             left_data_5_in;
    input   [PIXEL_WIDTH - 1:0]             left_data_6_in;
    input   [PIXEL_WIDTH - 1:0]             left_data_7_in;
            
    input                                   filter_start_in;
    output                                  filter_done_out;
    output reg  [MAX_NTBS_SIZE - 1:0]       filter_offset_out;

    output reg  [PIXEL_WIDTH - 1:0]         corner_pixel_filtered_out;
    output reg  [PIXEL_WIDTH - 1:0]         top_nt_plus1_filtered_out;
    output reg  [PIXEL_WIDTH - 1:0]         left_nt_plus1_filtered_out;
    output reg  [PIXEL_WIDTH - 1:0]         top_zeroth_filtered_out;
    output reg  [PIXEL_WIDTH - 1:0]         left_zeroth_filtered_out;

    
    input       [MAX_NTBS_SIZE - 1:0]       top_portr_addr;
    output reg  [PIXEL_WIDTH - 1:0]         top_portr_data0;
    output reg  [PIXEL_WIDTH - 1:0]         top_portr_data1;
    output reg  [PIXEL_WIDTH - 1:0]         top_portr_data2;
    output reg  [PIXEL_WIDTH - 1:0]         top_portr_data3;
    output reg  [PIXEL_WIDTH - 1:0]         top_portr_data4;
    output reg  [PIXEL_WIDTH - 1:0]         top_portr_data5;
    output reg  [PIXEL_WIDTH - 1:0]         top_portr_data6;
    output reg  [PIXEL_WIDTH - 1:0]         top_portr_data7;
    // input                                   top_portr_en;
    
    input       [MAX_NTBS_SIZE - 1:0]       left_portr_addr;
    output reg  [PIXEL_WIDTH - 1:0]         left_portr_data0;
    output reg  [PIXEL_WIDTH - 1:0]         left_portr_data1;
    output reg  [PIXEL_WIDTH - 1:0]         left_portr_data2;
    output reg  [PIXEL_WIDTH - 1:0]         left_portr_data3;
    output reg  [PIXEL_WIDTH - 1:0]         left_portr_data4;
    output reg  [PIXEL_WIDTH - 1:0]         left_portr_data5;
    output reg  [PIXEL_WIDTH - 1:0]         left_portr_data6;
    output reg  [PIXEL_WIDTH - 1:0]         left_portr_data7;
    // input                                   left_portr_en;
    
//----------------------------------------------------
//  Internal Regs and Wires 
//----------------------------------------------------
    integer                                         state;
    integer                                         corner_zero_state;
    reg     [PIXEL_WIDTH - 1:0]                     top_2nt_reg;
    reg     [PIXEL_WIDTH - 1:0]                     left_2nt_reg;
    reg     [PIXEL_WIDTH - 1:0]                     top_zeroth_reg;
    reg     [PIXEL_WIDTH - 1:0]                     left_zeroth_reg;
    reg     [PIXEL_WIDTH - 1:0]                     topcorner_pixel_reg;
    reg     [PIXEL_WIDTH - 1:0]                     leftcorner_pixel_reg;
    reg                                             bi_int_flag_reg;
    reg                                             filter_done;

    reg     [PIXEL_WIDTH - 1:0]                     top_data_0_reg;
    reg     [PIXEL_WIDTH - 1:0]                     top_data_1_reg;
    reg     [PIXEL_WIDTH - 1:0]                     top_data_2_reg;
    reg     [PIXEL_WIDTH - 1:0]                     top_data_3_reg;
    reg     [PIXEL_WIDTH - 1:0]                     top_data_4_reg;
    reg     [PIXEL_WIDTH - 1:0]                     top_data_5_reg;
    reg     [PIXEL_WIDTH - 1:0]                     top_data_6_reg;
    reg     [PIXEL_WIDTH - 1:0]                     top_data_7_reg;
         
    reg     [PIXEL_WIDTH - 1:0]                     left_data_0_reg;
    reg     [PIXEL_WIDTH - 1:0]                     left_data_1_reg;
    reg     [PIXEL_WIDTH - 1:0]                     left_data_2_reg;
    reg     [PIXEL_WIDTH - 1:0]                     left_data_3_reg;
    reg     [PIXEL_WIDTH - 1:0]                     left_data_4_reg;
    reg     [PIXEL_WIDTH - 1:0]                     left_data_5_reg;
    reg     [PIXEL_WIDTH - 1:0]                     left_data_6_reg;
    reg     [PIXEL_WIDTH - 1:0]                     left_data_7_reg;
            
    reg     [FILTER_COUNTER_WIDTH - 1:0]            filter_counter;
    reg     [FILTER_COUNTER_WIDTH - 1:0]            filter_counter_d[3:0];
    reg     [FILTER_COUNTER_WIDTH - 1:0]            corner_zero_counter;
    
    wire     [FILTER_COUNTER_WIDTH + 3 - 1:0]       filter_counter_by8;
    wire     [FILTER_COUNTER_WIDTH + 3 - 1:0]       filter_counter_by8_plus1;
    wire     [FILTER_COUNTER_WIDTH + 3 - 1:0]       filter_counter_by8_plus2;
    wire     [FILTER_COUNTER_WIDTH + 3 - 1:0]       filter_counter_by8_plus3;
    wire     [FILTER_COUNTER_WIDTH + 3 - 1:0]       filter_counter_by8_plus4;
    wire     [FILTER_COUNTER_WIDTH + 3 - 1:0]       filter_counter_by8_plus5;
    wire     [FILTER_COUNTER_WIDTH + 3 - 1:0]       filter_counter_by8_plus6;
    wire     [FILTER_COUNTER_WIDTH + 3 - 1:0]       filter_counter_by8_plus7;
    
    reg     [MAX_NTBS_SIZE - 2 - 1:0]                   ntbs_reg;
            
    reg     [PIXEL_WIDTH - 1:0]                     topmem_bank_0_wdata;
    reg     [PIXEL_WIDTH - 1:0]                     topmem_bank_1_wdata;
    reg     [PIXEL_WIDTH - 1:0]                     topmem_bank_2_wdata;
    reg     [PIXEL_WIDTH - 1:0]                     topmem_bank_3_wdata;
    reg     [PIXEL_WIDTH - 1:0]                     topmem_bank_4_wdata;
    reg     [PIXEL_WIDTH - 1:0]                     topmem_bank_5_wdata;
    reg     [PIXEL_WIDTH - 1:0]                     topmem_bank_6_wdata;
    reg     [PIXEL_WIDTH - 1:0]                     topmem_bank_7_wdata;
    reg     [NUMBER_OF_BANKS - 1:0]                 topmem_write_enables [4:0];
    
    wire     [PIXEL_WIDTH - 1:0]                    topmem_bank_0_rdata;
    wire     [PIXEL_WIDTH - 1:0]                    topmem_bank_1_rdata;
    wire     [PIXEL_WIDTH - 1:0]                    topmem_bank_2_rdata;
    wire     [PIXEL_WIDTH - 1:0]                    topmem_bank_3_rdata;
    wire     [PIXEL_WIDTH - 1:0]                    topmem_bank_4_rdata;
    wire     [PIXEL_WIDTH - 1:0]                    topmem_bank_5_rdata;
    wire     [PIXEL_WIDTH - 1:0]                    topmem_bank_6_rdata;
    wire     [PIXEL_WIDTH - 1:0]                    topmem_bank_7_rdata;
    
    reg     [PIXEL_WIDTH - 1:0]                     leftmem_bank_0_wdata;
    reg     [PIXEL_WIDTH - 1:0]                     leftmem_bank_1_wdata;
    reg     [PIXEL_WIDTH - 1:0]                     leftmem_bank_2_wdata;
    reg     [PIXEL_WIDTH - 1:0]                     leftmem_bank_3_wdata;
    reg     [PIXEL_WIDTH - 1:0]                     leftmem_bank_4_wdata;
    reg     [PIXEL_WIDTH - 1:0]                     leftmem_bank_5_wdata;
    reg     [PIXEL_WIDTH - 1:0]                     leftmem_bank_6_wdata;
    reg     [PIXEL_WIDTH - 1:0]                     leftmem_bank_7_wdata;
    reg     [NUMBER_OF_BANKS - 1:0]                 leftmem_write_enables [4:0];
    
    wire     [PIXEL_WIDTH - 1:0]                    leftmem_bank_0_rdata;
    wire     [PIXEL_WIDTH - 1:0]                    leftmem_bank_1_rdata;
    wire     [PIXEL_WIDTH - 1:0]                    leftmem_bank_2_rdata;
    wire     [PIXEL_WIDTH - 1:0]                    leftmem_bank_3_rdata;
    wire     [PIXEL_WIDTH - 1:0]                    leftmem_bank_4_rdata;
    wire     [PIXEL_WIDTH - 1:0]                    leftmem_bank_5_rdata;
    wire     [PIXEL_WIDTH - 1:0]                    leftmem_bank_6_rdata;
    wire     [PIXEL_WIDTH - 1:0]                    leftmem_bank_7_rdata;
    
    reg     [MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1: 0]   top_membank0r_addr;
    reg     [MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1: 0]   top_membank1r_addr;
    reg     [MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1: 0]   top_membank2r_addr;
    reg     [MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1: 0]   top_membank3r_addr;
    reg     [MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1: 0]   top_membank4r_addr;
    reg     [MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1: 0]   top_membank5r_addr;
    reg     [MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1: 0]   top_membank6r_addr;
    reg     [MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1: 0]   top_membank7r_addr;
    
    reg     [MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1: 0]   left_membank0r_addr;
    reg     [MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1: 0]   left_membank1r_addr;
    reg     [MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1: 0]   left_membank2r_addr;
    reg     [MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1: 0]   left_membank3r_addr;
    reg     [MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1: 0]   left_membank4r_addr;
    reg     [MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1: 0]   left_membank5r_addr;
    reg     [MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1: 0]   left_membank6r_addr;
    reg     [MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1: 0]   left_membank7r_addr;
    
    reg     [PIXEL_WIDTH + 6 - 1:0]                     topmem_bank_0_3tap_p1;     
    reg     [PIXEL_WIDTH + 6 - 1:0]                     topmem_bank_0_3tap_p2;     
    reg     [PIXEL_WIDTH + 6 - 1:0]                     topmem_bank_1_3tap_p1;     
    reg     [PIXEL_WIDTH + 6 - 1:0]                     topmem_bank_1_3tap_p2;     
    reg     [PIXEL_WIDTH + 6 - 1:0]                     topmem_bank_2_3tap_p1;     
    reg     [PIXEL_WIDTH + 6 - 1:0]                     topmem_bank_2_3tap_p2;     
    reg     [PIXEL_WIDTH + 6 - 1:0]                     topmem_bank_3_3tap_p1;     
    reg     [PIXEL_WIDTH + 6 - 1:0]                     topmem_bank_3_3tap_p2;     
    reg     [PIXEL_WIDTH + 6 - 1:0]                     topmem_bank_4_3tap_p1;     
    reg     [PIXEL_WIDTH + 6 - 1:0]                     topmem_bank_4_3tap_p2;     
    reg     [PIXEL_WIDTH + 6 - 1:0]                     topmem_bank_5_3tap_p1;     
    reg     [PIXEL_WIDTH + 6 - 1:0]                     topmem_bank_5_3tap_p2;     
    reg     [PIXEL_WIDTH + 6 - 1:0]                     topmem_bank_6_3tap_p1;     
    reg     [PIXEL_WIDTH + 6 - 1:0]                     topmem_bank_6_3tap_p2;    
    reg     [PIXEL_WIDTH + 0 - 1:0]                     topmem_bank_7_3tap_buffer; 
    reg     [PIXEL_WIDTH + 6 - 1:0]                     topmem_bank_7_3tap_p1;     
    reg     [PIXEL_WIDTH + 6 - 1:0]                     topmem_bank_7_3tap_p2 ;    
    
    reg     [PIXEL_WIDTH + 6 - 1:0]                     leftmem_bank_0_3tap_p1;    
    reg     [PIXEL_WIDTH + 6 - 1:0]                     leftmem_bank_0_3tap_p2;    
    reg     [PIXEL_WIDTH + 6 - 1:0]                     leftmem_bank_1_3tap_p1;    
    reg     [PIXEL_WIDTH + 6 - 1:0]                     leftmem_bank_1_3tap_p2;    
    reg     [PIXEL_WIDTH + 6 - 1:0]                     leftmem_bank_2_3tap_p1;    
    reg     [PIXEL_WIDTH + 6 - 1:0]                     leftmem_bank_2_3tap_p2;      
    reg     [PIXEL_WIDTH + 6 - 1:0]                     leftmem_bank_3_3tap_p1;    
    reg     [PIXEL_WIDTH + 6 - 1:0]                     leftmem_bank_3_3tap_p2;     
    reg     [PIXEL_WIDTH + 6 - 1:0]                     leftmem_bank_4_3tap_p1;    
    reg     [PIXEL_WIDTH + 6 - 1:0]                     leftmem_bank_4_3tap_p2;     
    reg     [PIXEL_WIDTH + 6 - 1:0]                     leftmem_bank_5_3tap_p1;    
    reg     [PIXEL_WIDTH + 6 - 1:0]                     leftmem_bank_5_3tap_p2;    
    reg     [PIXEL_WIDTH + 6 - 1:0]                     leftmem_bank_6_3tap_p1;    
    reg     [PIXEL_WIDTH + 6 - 1:0]                     leftmem_bank_6_3tap_p2;    
    reg     [PIXEL_WIDTH + 0 - 1:0]                     leftmem_bank_7_3tap_buffer;
    reg     [PIXEL_WIDTH + 6 - 1:0]                     leftmem_bank_7_3tap_p1;    
    reg     [PIXEL_WIDTH + 6 - 1:0]                     leftmem_bank_7_3tap_p2; 

    reg     [3 - 1:0]                                   top_portr_addr_low3b_delayed;
    reg     [3 - 1:0]                                   left_portr_addr_low3b_delayed;

    reg     [PIXEL_WIDTH + 2 -1 : 0]                    corner_pixel_filter_p1;
    reg     [PIXEL_WIDTH + 2 -1 : 0]                    corner_pixel_filter_p2;
    wire    [PIXEL_WIDTH + 2 -1 : 0]                    corner_pixel_filter_p1_plus_p2_int;
        
//----------------------------------------------------
//  Implementation  
//----------------------------------------------------

    assign corner_pixel_filter_p1_plus_p2_int = corner_pixel_filter_p1 + corner_pixel_filter_p2;

    assign filter_done_out = filter_done;
    
    assign filter_counter_by8       = {filter_counter_d[1],3'b000};
    assign filter_counter_by8_plus1 = filter_counter_by8 + 1'd1;
    assign filter_counter_by8_plus2 = filter_counter_by8 + 2'd2;
    assign filter_counter_by8_plus3 = filter_counter_by8 + 2'd3;
    assign filter_counter_by8_plus4 = filter_counter_by8 + 3'd4;
    assign filter_counter_by8_plus5 = filter_counter_by8 + 3'd5;
    assign filter_counter_by8_plus6 = filter_counter_by8 + 3'd6;
    assign filter_counter_by8_plus7 = filter_counter_by8 + 3'd7;
    
//    always @(*) begin
//        case(ntbs_in)
//            {{(MAX_NTBS_SIZE - 6){1'b0}},6'd32} : log2ntbs = 5;
//            {{(MAX_NTBS_SIZE - 6){1'b0}},6'd16} : log2ntbs = 4;
//            {{(MAX_NTBS_SIZE - 6){1'b0}},6'd8}  : log2ntbs = 3;
//            {{(MAX_NTBS_SIZE - 6){1'b0}},6'd4 } : log2ntbs = 2;
//            default                             : log2ntbs = 0;
//        endcase
//    end

    always @(posedge clk) begin
        if (reset) begin
            // reset
            
        end
        else if (enable) begin
            if(top_all_invalid_in & left_all_invalid_in) begin
                top_data_0_reg      <=  (1'b1 << (BIT_DEPTH - 1'b1));
                top_data_1_reg      <=  (1'b1 << (BIT_DEPTH - 1'b1));
                top_data_2_reg      <=  (1'b1 << (BIT_DEPTH - 1'b1));
                top_data_3_reg      <=  (1'b1 << (BIT_DEPTH - 1'b1));
                top_data_4_reg      <=  (1'b1 << (BIT_DEPTH - 1'b1));
                top_data_5_reg      <=  (1'b1 << (BIT_DEPTH - 1'b1));
                top_data_6_reg      <=  (1'b1 << (BIT_DEPTH - 1'b1));
                top_data_7_reg      <=  (1'b1 << (BIT_DEPTH - 1'b1));
                
                left_data_0_reg     <=  (1'b1 << (BIT_DEPTH - 1'b1)); 
                left_data_1_reg     <=  (1'b1 << (BIT_DEPTH - 1'b1));
                left_data_2_reg     <=  (1'b1 << (BIT_DEPTH - 1'b1));
                left_data_3_reg     <=  (1'b1 << (BIT_DEPTH - 1'b1));
                left_data_4_reg     <=  (1'b1 << (BIT_DEPTH - 1'b1));
                left_data_5_reg     <=  (1'b1 << (BIT_DEPTH - 1'b1));
                left_data_6_reg     <=  (1'b1 << (BIT_DEPTH - 1'b1));
                left_data_7_reg     <=  (1'b1 << (BIT_DEPTH - 1'b1));
            end
            else if(top_all_invalid_in) begin

                top_data_0_reg      <=  left_zeroth_in;
                top_data_1_reg      <=  left_zeroth_in;
                top_data_2_reg      <=  left_zeroth_in;
                top_data_3_reg      <=  left_zeroth_in;
                top_data_4_reg      <=  left_zeroth_in;
                top_data_5_reg      <=  left_zeroth_in;
                top_data_6_reg      <=  left_zeroth_in;
                top_data_7_reg      <=  left_zeroth_in;
                
                left_data_0_reg     <=  left_data_0_in; 
                left_data_1_reg     <=  left_data_1_in;
                left_data_2_reg     <=  left_data_2_in;
                left_data_3_reg     <=  left_data_3_in;
                left_data_4_reg     <=  left_data_4_in;
                left_data_5_reg     <=  left_data_5_in;
                left_data_6_reg     <=  left_data_6_in;
                left_data_7_reg     <=  left_data_7_in;
                
            end
            else if(left_all_invalid_in) begin
                top_data_0_reg      <=  top_data_0_in;
                top_data_1_reg      <=  top_data_1_in;
                top_data_2_reg      <=  top_data_2_in;
                top_data_3_reg      <=  top_data_3_in;
                top_data_4_reg      <=  top_data_4_in;
                top_data_5_reg      <=  top_data_5_in;
                top_data_6_reg      <=  top_data_6_in;
                top_data_7_reg      <=  top_data_7_in;
                
                left_data_0_reg     <=  top_zeroth_in; 
                left_data_1_reg     <=  top_zeroth_in;
                left_data_2_reg     <=  top_zeroth_in;
                left_data_3_reg     <=  top_zeroth_in;
                left_data_4_reg     <=  top_zeroth_in;
                left_data_5_reg     <=  top_zeroth_in;
                left_data_6_reg     <=  top_zeroth_in;
                left_data_7_reg     <=  top_zeroth_in;  
            end
            else begin
                top_data_0_reg      <=  top_data_0_in;
                top_data_1_reg      <=  top_data_1_in;
                top_data_2_reg      <=  top_data_2_in;
                top_data_3_reg      <=  top_data_3_in;
                top_data_4_reg      <=  top_data_4_in;
                top_data_5_reg      <=  top_data_5_in;
                top_data_6_reg      <=  top_data_6_in;
                top_data_7_reg      <=  top_data_7_in;

                left_data_0_reg     <=  left_data_0_in; 
                left_data_1_reg     <=  left_data_1_in;
                left_data_2_reg     <=  left_data_2_in;
                left_data_3_reg     <=  left_data_3_in;
                left_data_4_reg     <=  left_data_4_in;
                left_data_5_reg     <=  left_data_5_in;
                left_data_6_reg     <=  left_data_6_in;
                left_data_7_reg     <=  left_data_7_in;  
            end
        end
    end

    always @(posedge clk) begin : pipelining
        if(enable) begin
            topmem_write_enables[3]     <= topmem_write_enables[4];
            topmem_write_enables[2]     <= topmem_write_enables[3];
            topmem_write_enables[1]     <= topmem_write_enables[2];
            topmem_write_enables[0]     <= topmem_write_enables[1];

            leftmem_write_enables[3]    <= leftmem_write_enables[4];
            leftmem_write_enables[2]    <= leftmem_write_enables[3];
            leftmem_write_enables[1]    <= leftmem_write_enables[2];
            leftmem_write_enables[0]    <= leftmem_write_enables[1];

            // filter_done[0]              <= filter_done[1]; 

            filter_counter_d[3]         <= filter_counter;
            filter_counter_d[2]         <= filter_counter_d[3];
            filter_counter_d[1]         <= filter_counter_d[2];
            filter_counter_d[0]         <= filter_counter_d[1];

            top_portr_addr_low3b_delayed  <= top_portr_addr[3 - 1:0];
            left_portr_addr_low3b_delayed <= left_portr_addr[3 - 1:0];
             
            
            if(bi_int_flag_reg == 1'b0) begin
                topmem_bank_0_3tap_p1     <= (topcorner_pixel_reg + top_data_0_reg);
                topmem_bank_0_3tap_p2     <= ((top_zeroth_reg << 1) + 2'd2);
                
                topmem_bank_1_3tap_p1     <= (top_zeroth_reg + top_data_1_reg);
                topmem_bank_1_3tap_p2     <= ((top_data_0_reg << 1) + 2'd2);
                
                topmem_bank_2_3tap_p1     <= (top_data_0_reg + top_data_2_reg);
                topmem_bank_2_3tap_p2     <= ((top_data_1_reg << 1) + 2'd2);
                
                topmem_bank_3_3tap_p1     <= (top_data_1_reg + top_data_3_reg);
                topmem_bank_3_3tap_p2     <= ((top_data_2_reg << 1) + 2'd2);
                
                topmem_bank_4_3tap_p1     <= (top_data_2_reg + top_data_4_reg);
                topmem_bank_4_3tap_p2     <= ((top_data_3_reg << 1) + 2'd2);
                
                topmem_bank_5_3tap_p1     <= (top_data_3_reg + top_data_5_reg);
                topmem_bank_5_3tap_p2     <= ((top_data_4_reg << 1) + 2'd2);
                
                topmem_bank_6_3tap_p1     <= (top_data_4_reg + top_data_6_reg);
                topmem_bank_6_3tap_p2     <= ((top_data_5_reg << 1) + 2'd2);
                
                topmem_bank_7_3tap_buffer <= top_data_6_reg;
                topmem_bank_7_3tap_p1     <= (top_data_5_reg + top_data_7_reg);
                topmem_bank_7_3tap_p2     <= ((top_data_6_reg << 1) + 2'd2);
                
                leftmem_bank_0_3tap_p1     <= (leftcorner_pixel_reg + left_data_0_reg);
                leftmem_bank_0_3tap_p2     <= ((left_zeroth_reg << 1) + 2'd2);
                
                leftmem_bank_1_3tap_p1     <= (left_zeroth_reg + left_data_1_reg);
                leftmem_bank_1_3tap_p2     <= ((left_data_0_reg << 1) + 2'd2);
                
                leftmem_bank_2_3tap_p1     <= (left_data_0_reg + left_data_2_reg);
                leftmem_bank_2_3tap_p2     <= ((left_data_1_reg << 1) + 2'd2);
                
                leftmem_bank_3_3tap_p1     <= (left_data_1_reg + left_data_3_reg);
                leftmem_bank_3_3tap_p2     <= ((left_data_2_reg << 1) + 2'd2);
                
                leftmem_bank_4_3tap_p1     <= (left_data_2_reg + left_data_4_reg);
                leftmem_bank_4_3tap_p2     <= ((left_data_3_reg << 1) + 2'd2);
                
                leftmem_bank_5_3tap_p1     <= (left_data_3_reg + left_data_5_reg);
                leftmem_bank_5_3tap_p2     <= ((left_data_4_reg << 1) + 2'd2);
                
                leftmem_bank_6_3tap_p1     <= (left_data_4_reg + left_data_6_reg);
                leftmem_bank_6_3tap_p2     <= ((left_data_5_reg << 1) + 2'd2);
                
                leftmem_bank_7_3tap_buffer <= left_data_6_reg;
                leftmem_bank_7_3tap_p1     <= (left_data_5_reg + left_data_7_reg);
                leftmem_bank_7_3tap_p2     <= ((left_data_6_reg << 1) + 2'd2);
            end
            else begin
                topmem_bank_0_3tap_p1     <= (topcorner_pixel_reg*(6'd63 - filter_counter_by8));
                topmem_bank_0_3tap_p2     <= ((filter_counter_by8 + 1'b1)*top_2nt_reg) + 6'd32;
                
                topmem_bank_1_3tap_p1     <= (topcorner_pixel_reg*(6'd63 - filter_counter_by8_plus1));
                topmem_bank_1_3tap_p2     <= ((filter_counter_by8_plus1 + 1'b1)*top_2nt_reg) + 6'd32;
                
                topmem_bank_2_3tap_p1     <= (topcorner_pixel_reg*(6'd63 - filter_counter_by8_plus2));
                topmem_bank_2_3tap_p2     <= ((filter_counter_by8_plus2 + 1'b1)*top_2nt_reg) + 6'd32;
                
                topmem_bank_3_3tap_p1     <= (topcorner_pixel_reg*(6'd63 - filter_counter_by8_plus3));
                topmem_bank_3_3tap_p2     <= ((filter_counter_by8_plus3 + 1'b1)*top_2nt_reg) + 6'd32;
                
                topmem_bank_4_3tap_p1     <= (topcorner_pixel_reg*(6'd63 - filter_counter_by8_plus4));
                topmem_bank_4_3tap_p2     <= ((filter_counter_by8_plus4 + 1'b1)*top_2nt_reg) + 6'd32;
                
                topmem_bank_5_3tap_p1     <= (topcorner_pixel_reg*(6'd63 - filter_counter_by8_plus5));
                topmem_bank_5_3tap_p2     <= ((filter_counter_by8_plus5 + 1'b1)*top_2nt_reg) + 6'd32;
                
                topmem_bank_6_3tap_p1     <= (topcorner_pixel_reg*(6'd63 - filter_counter_by8_plus6));
                topmem_bank_6_3tap_p2     <= ((filter_counter_by8_plus6 + 1'b1)*top_2nt_reg) + 6'd32;
                
                topmem_bank_7_3tap_buffer <= top_2nt_reg;
                topmem_bank_7_3tap_p1     <= (topcorner_pixel_reg*(6'd63 - filter_counter_by8_plus7));
                topmem_bank_7_3tap_p2     <= ((filter_counter_by8_plus7 + 1'b1)*top_2nt_reg) + 6'd32;
                
                leftmem_bank_0_3tap_p1     <= (leftcorner_pixel_reg*(6'd63 - filter_counter_by8));
                leftmem_bank_0_3tap_p2     <= ((filter_counter_by8 + 1'b1)*left_2nt_reg) + 6'd32;
                
                leftmem_bank_1_3tap_p1     <= (leftcorner_pixel_reg*(6'd63 - filter_counter_by8_plus1));
                leftmem_bank_1_3tap_p2     <= ((filter_counter_by8_plus1 + 1'b1)*left_2nt_reg) + 6'd32;
                
                leftmem_bank_2_3tap_p1     <= (leftcorner_pixel_reg*(6'd63 - filter_counter_by8_plus2));
                leftmem_bank_2_3tap_p2     <= ((filter_counter_by8_plus2 + 1'b1)*left_2nt_reg) + 6'd32;
                
                leftmem_bank_3_3tap_p1     <= (leftcorner_pixel_reg*(6'd63 - filter_counter_by8_plus3));
                leftmem_bank_3_3tap_p2     <= ((filter_counter_by8_plus3 + 1'b1)*left_2nt_reg) + 6'd32;
                
                leftmem_bank_4_3tap_p1     <= (leftcorner_pixel_reg*(6'd63 - filter_counter_by8_plus4));
                leftmem_bank_4_3tap_p2     <= ((filter_counter_by8_plus4 + 1'b1)*left_2nt_reg) + 6'd32;
                
                leftmem_bank_5_3tap_p1     <= (leftcorner_pixel_reg*(6'd63 - filter_counter_by8_plus5));
                leftmem_bank_5_3tap_p2     <= ((filter_counter_by8_plus5 + 1'b1)*left_2nt_reg) + 6'd32;
                
                leftmem_bank_6_3tap_p1     <= (leftcorner_pixel_reg*(6'd63 - filter_counter_by8_plus6));
                leftmem_bank_6_3tap_p2     <= ((filter_counter_by8_plus6 + 1'b1)*left_2nt_reg) + 6'd32;
                
                leftmem_bank_7_3tap_buffer <= left_2nt_reg;
                leftmem_bank_7_3tap_p1     <= (leftcorner_pixel_reg*(6'd63 - filter_counter_by8_plus7));
                leftmem_bank_7_3tap_p2     <= ((filter_counter_by8_plus7 + 1'b1)*left_2nt_reg) + 6'd32;
            
            end
        end
    end
    
    always @(*) begin //3tap_filters
        if(bi_int_flag_reg == 1'b0) begin
            topmem_bank_0_wdata = (topmem_bank_0_3tap_p1[PIXEL_WIDTH + 2 - 1:0] + topmem_bank_0_3tap_p2[PIXEL_WIDTH + 2 - 1:0]) >> 2; 
            topmem_bank_1_wdata = (topmem_bank_1_3tap_p1[PIXEL_WIDTH + 2 - 1:0] + topmem_bank_1_3tap_p2[PIXEL_WIDTH + 2 - 1:0]) >> 2;
            topmem_bank_2_wdata = (topmem_bank_2_3tap_p1[PIXEL_WIDTH + 2 - 1:0] + topmem_bank_2_3tap_p2[PIXEL_WIDTH + 2 - 1:0]) >> 2; 
            topmem_bank_3_wdata = (topmem_bank_3_3tap_p1[PIXEL_WIDTH + 2 - 1:0] + topmem_bank_3_3tap_p2[PIXEL_WIDTH + 2 - 1:0]) >> 2; 
            topmem_bank_4_wdata = (topmem_bank_4_3tap_p1[PIXEL_WIDTH + 2 - 1:0] + topmem_bank_4_3tap_p2[PIXEL_WIDTH + 2 - 1:0]) >> 2; 
            topmem_bank_5_wdata = (topmem_bank_5_3tap_p1[PIXEL_WIDTH + 2 - 1:0] + topmem_bank_5_3tap_p2[PIXEL_WIDTH + 2 - 1:0]) >> 2; 
            topmem_bank_6_wdata = (topmem_bank_6_3tap_p1[PIXEL_WIDTH + 2 - 1:0] + topmem_bank_6_3tap_p2[PIXEL_WIDTH + 2 - 1:0]) >> 2; 
            if( filter_counter_d[0] == (ntbs_reg - 1'b1) ) begin
                topmem_bank_7_wdata = topmem_bank_7_3tap_buffer;
            end
            else begin
                topmem_bank_7_wdata = (topmem_bank_7_3tap_p1[PIXEL_WIDTH + 2 - 1:0] + topmem_bank_7_3tap_p2[PIXEL_WIDTH + 2 - 1:0]) >> 2;
            end
        end
        else begin // 2tap_filters
            topmem_bank_0_wdata = (topmem_bank_0_3tap_p1[PIXEL_WIDTH + 6 - 1:0] + topmem_bank_0_3tap_p2[PIXEL_WIDTH + 6 - 1:0]) >> 6;
            topmem_bank_1_wdata = (topmem_bank_1_3tap_p1[PIXEL_WIDTH + 6 - 1:0] + topmem_bank_1_3tap_p2[PIXEL_WIDTH + 6 - 1:0]) >> 6;
            topmem_bank_2_wdata = (topmem_bank_2_3tap_p1[PIXEL_WIDTH + 6 - 1:0] + topmem_bank_2_3tap_p2[PIXEL_WIDTH + 6 - 1:0]) >> 6;
            topmem_bank_3_wdata = (topmem_bank_3_3tap_p1[PIXEL_WIDTH + 6 - 1:0] + topmem_bank_3_3tap_p2[PIXEL_WIDTH + 6 - 1:0]) >> 6;
            topmem_bank_4_wdata = (topmem_bank_4_3tap_p1[PIXEL_WIDTH + 6 - 1:0] + topmem_bank_4_3tap_p2[PIXEL_WIDTH + 6 - 1:0]) >> 6;
            topmem_bank_5_wdata = (topmem_bank_5_3tap_p1[PIXEL_WIDTH + 6 - 1:0] + topmem_bank_5_3tap_p2[PIXEL_WIDTH + 6 - 1:0]) >> 6;
            topmem_bank_6_wdata = (topmem_bank_6_3tap_p1[PIXEL_WIDTH + 6 - 1:0] + topmem_bank_6_3tap_p2[PIXEL_WIDTH + 6 - 1:0]) >> 6;
            if( filter_counter_d[0] == (ntbs_reg - 1'b1) ) begin
                topmem_bank_7_wdata = topmem_bank_7_3tap_buffer;
            end
            else begin
                topmem_bank_7_wdata = (topmem_bank_7_3tap_p1[PIXEL_WIDTH + 6 - 1:0] + topmem_bank_7_3tap_p2[PIXEL_WIDTH + 6 - 1:0]) >> 6;
            end
        end
    end
    
    always @(*) begin //3tap_filters
        if(bi_int_flag_reg == 1'b0) begin
            leftmem_bank_0_wdata = (leftmem_bank_0_3tap_p1[PIXEL_WIDTH + 2 - 1:0] + leftmem_bank_0_3tap_p2[PIXEL_WIDTH + 2 - 1:0]) >> 2;
            leftmem_bank_1_wdata = (leftmem_bank_1_3tap_p1[PIXEL_WIDTH + 2 - 1:0] + leftmem_bank_1_3tap_p2[PIXEL_WIDTH + 2 - 1:0]) >> 2;
            leftmem_bank_2_wdata = (leftmem_bank_2_3tap_p1[PIXEL_WIDTH + 2 - 1:0] + leftmem_bank_2_3tap_p2[PIXEL_WIDTH + 2 - 1:0]) >> 2;
            leftmem_bank_3_wdata = (leftmem_bank_3_3tap_p1[PIXEL_WIDTH + 2 - 1:0] + leftmem_bank_3_3tap_p2[PIXEL_WIDTH + 2 - 1:0]) >> 2;
            leftmem_bank_4_wdata = (leftmem_bank_4_3tap_p1[PIXEL_WIDTH + 2 - 1:0] + leftmem_bank_4_3tap_p2[PIXEL_WIDTH + 2 - 1:0]) >> 2;
            leftmem_bank_5_wdata = (leftmem_bank_5_3tap_p1[PIXEL_WIDTH + 2 - 1:0] + leftmem_bank_5_3tap_p2[PIXEL_WIDTH + 2 - 1:0]) >> 2;
            leftmem_bank_6_wdata = (leftmem_bank_6_3tap_p1[PIXEL_WIDTH + 2 - 1:0] + leftmem_bank_6_3tap_p2[PIXEL_WIDTH + 2 - 1:0]) >> 2;
            if( filter_counter_d[0] == (ntbs_reg  - 1'b1) ) begin
                leftmem_bank_7_wdata = leftmem_bank_7_3tap_buffer;
            end
            else begin
                leftmem_bank_7_wdata = (leftmem_bank_7_3tap_p1[PIXEL_WIDTH + 2 - 1:0] + leftmem_bank_7_3tap_p2[PIXEL_WIDTH + 2 - 1:0]) >> 2;
            end
        end
        else begin // 2tap_filters
            leftmem_bank_0_wdata = (leftmem_bank_0_3tap_p1[PIXEL_WIDTH + 6 - 1:0] + leftmem_bank_0_3tap_p2[PIXEL_WIDTH + 6 - 1:0]) >> 6;
            leftmem_bank_1_wdata = (leftmem_bank_1_3tap_p1[PIXEL_WIDTH + 6 - 1:0] + leftmem_bank_1_3tap_p2[PIXEL_WIDTH + 6 - 1:0]) >> 6;
            leftmem_bank_2_wdata = (leftmem_bank_2_3tap_p1[PIXEL_WIDTH + 6 - 1:0] + leftmem_bank_2_3tap_p2[PIXEL_WIDTH + 6 - 1:0]) >> 6;
            leftmem_bank_3_wdata = (leftmem_bank_3_3tap_p1[PIXEL_WIDTH + 6 - 1:0] + leftmem_bank_3_3tap_p2[PIXEL_WIDTH + 6 - 1:0]) >> 6;
            leftmem_bank_4_wdata = (leftmem_bank_4_3tap_p1[PIXEL_WIDTH + 6 - 1:0] + leftmem_bank_4_3tap_p2[PIXEL_WIDTH + 6 - 1:0]) >> 6;
            leftmem_bank_5_wdata = (leftmem_bank_5_3tap_p1[PIXEL_WIDTH + 6 - 1:0] + leftmem_bank_5_3tap_p2[PIXEL_WIDTH + 6 - 1:0]) >> 6;
            leftmem_bank_6_wdata = (leftmem_bank_6_3tap_p1[PIXEL_WIDTH + 6 - 1:0] + leftmem_bank_6_3tap_p2[PIXEL_WIDTH + 6 - 1:0]) >> 6;
            if( filter_counter_d[0] == (ntbs_reg - 1'b1) ) begin
                leftmem_bank_7_wdata = leftmem_bank_7_3tap_buffer;
            end
            else begin
                leftmem_bank_7_wdata = (leftmem_bank_7_3tap_p1[PIXEL_WIDTH + 6 - 1:0] + leftmem_bank_7_3tap_p2[PIXEL_WIDTH + 6 - 1:0]) >> 6;
            end
        end
    end
    
    always @(posedge clk) begin
        if(reset) begin
            state               <= STATE_INIT;
            filter_offset_out   <= {{(MAX_NTBS_SIZE-1){1'b0}},1'b1};
            filter_done      <= 1'b1;
            topmem_write_enables[4]     <= {NUMBER_OF_BANKS{1'b0}};
            leftmem_write_enables[4]    <= {NUMBER_OF_BANKS{1'b0}};
        end
        else if(enable) begin
            case(state)
                STATE_INIT : begin
                    if(filter_start_in == 1'b1) begin
                        top_2nt_reg                 <=  top_2nt_in;
                        left_2nt_reg                <=  left_2nt_in;
                        // top_zeroth_reg              <=  top_zeroth_in;
                        // left_zeroth_reg             <=  left_zeroth_in;
                        // topcorner_pixel_reg         <=  corner_pixel_in;
                        // leftcorner_pixel_reg        <=  corner_pixel_in;
                        ntbs_reg                    <=  ntbs_in[MAX_NTBS_SIZE - 1:2];
                        bi_int_flag_reg             <=  bi_int_flag_in;
                        topmem_write_enables[4]     <=  {NUMBER_OF_BANKS{1'b1}};
                        leftmem_write_enables[4]    <= {NUMBER_OF_BANKS{1'b1}};
                        state                       <=  STATE_FILTER;
                        filter_done                 <=  1'b0;
                        // filter_offset_out           <=  filter_offset_out + 4'd8;
                        filter_counter              <= {FILTER_COUNTER_WIDTH{1'b0}};             
                        
                        if(bi_int_flag_in == 1'b1) begin
                            corner_pixel_filtered_out <= corner_pixel_in;
                        end
                        else begin
                            corner_pixel_filter_p1 <= top_zeroth_in + left_zeroth_in;
                            corner_pixel_filter_p2 <= (corner_pixel_in + 1'b1) << 1;
                            //corner_pixel_filtered_out <= ((top_zeroth_in + left_zeroth_in) + 2*corner_pixel_in + 2'd2)) >> 2;
                        end
                        
                    end
                end
                STATE_FILTER : begin

                    if(bi_int_flag_in == 1'b0) begin
                        corner_pixel_filtered_out <= corner_pixel_filter_p1_plus_p2_int[PIXEL_WIDTH + 2 - 1:2];
                    end

                    if(filter_counter == (ntbs_reg - 1'b1) ) begin
                        state                       <= STATE_INIT;
                        filter_done                 <= 1'b1;          // NOT VALID  : this is done cycle earlier as required by the pipeline
                        topmem_write_enables[4]     <= {NUMBER_OF_BANKS{1'b0}};
                        leftmem_write_enables[4]    <= {NUMBER_OF_BANKS{1'b0}};
                        filter_offset_out           <= {{(MAX_NTBS_SIZE-1){1'b0}},1'b1};
                    end
                    else begin
                    
                        // if((filter_counter == ((ntbs_reg >> 2) - 2'd2) )) begin
                        //     filter_done[1]          <= 1'b1;
                        // end
                    
                        // if(bi_int_flag_reg == 1'b0) begin
                        //     topcorner_pixel_reg     <= top_data_6_reg;
                        //     top_zeroth_reg          <= top_data_7_reg;
                        //     leftcorner_pixel_reg    <= left_data_6_reg;
                        //     left_zeroth_reg         <= left_data_7_reg;
                        // end
                        filter_counter <= filter_counter + 1'b1;
                        filter_offset_out           <=  filter_offset_out + 4'd8;
                    end
                end
            endcase
        end
    end

    always @(posedge clk) begin
        if (reset) begin
            // reset
            corner_zero_state <= CORNER_ZERO_STATE_INIT;
        end
        else if (enable) begin
            case(corner_zero_state)
                CORNER_ZERO_STATE_INIT : begin
                    if(filter_start_in == 1'b1) begin
                        top_zeroth_reg              <=  top_zeroth_in;
                        left_zeroth_reg             <=  left_zeroth_in;
                        topcorner_pixel_reg         <=  corner_pixel_in;
                        leftcorner_pixel_reg        <=  corner_pixel_in;

                        corner_zero_counter         <= {FILTER_COUNTER_WIDTH{1'b0}};
                        corner_zero_state           <= CORNER_ZERO_STATE_WAIT1;
                    end
                    
                end
                CORNER_ZERO_STATE_WAIT1 : begin
                    corner_zero_state <= CORNER_ZERO_STATE_WAIT2;
                end
                CORNER_ZERO_STATE_WAIT2 : begin
                    corner_zero_state <= CORNER_ZERO_STATE_WAIT3;
                end
                CORNER_ZERO_STATE_WAIT3 : begin
                    corner_zero_state <= CORNER_ZERO_STATE_FILTER;
                end
                CORNER_ZERO_STATE_FILTER : begin

                    if(corner_zero_counter == ((ntbs_reg >> 1) + 1'b1) ) begin
                        top_nt_plus1_filtered_out       <= topmem_bank_0_wdata;
                        left_nt_plus1_filtered_out      <= leftmem_bank_0_wdata; 
                    end


                    if(corner_zero_counter == (ntbs_reg) ) begin
                        corner_zero_state           <= CORNER_ZERO_STATE_INIT;
                    end
                    else begin
                        if(bi_int_flag_reg == 1'b0) begin
                            topcorner_pixel_reg     <= top_data_6_reg;
                            top_zeroth_reg          <= top_data_7_reg;
                            leftcorner_pixel_reg    <= left_data_6_reg;
                            left_zeroth_reg         <= left_data_7_reg;
                        end

                        if(corner_zero_counter == {{(FILTER_COUNTER_WIDTH-1){1'b0}},1'b1}) begin
                            top_zeroth_filtered_out  <= topmem_bank_0_wdata;
                            left_zeroth_filtered_out <= leftmem_bank_0_wdata;
                        end

                        corner_zero_counter <= corner_zero_counter + 1'b1;
                    end
                end
            endcase
        end
    end


    intra_line_buffer_membank
    #(
        .LOG2_FRAME_SIZE(MAX_NTBS_SIZE)
    )
    intra_topline_buffer_filtered_membank_block0
    (
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),
        
        .portw_addr         (filter_counter_d[0][MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1:0]),
        .portw_data         (topmem_bank_0_wdata),
        .portw_en           (topmem_write_enables[0][0]),
        
        .portr_addr         (top_membank0r_addr),
        .portr_data         (topmem_bank_0_rdata)
        // .portr_en           (top_portr_en)
    );
    
    intra_line_buffer_membank
    #(
        .LOG2_FRAME_SIZE(MAX_NTBS_SIZE)
    )    
    intra_topline_buffer_filtered_membank_block1
    (
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),
        
        .portw_addr         (filter_counter_d[0][MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1:0]),
        .portw_data         (topmem_bank_1_wdata),
        .portw_en           (topmem_write_enables[0][1]),
        
        .portr_addr         (top_membank1r_addr),
        .portr_data         (topmem_bank_1_rdata)
        // .portr_en           (top_portr_en)
    );
    
    intra_line_buffer_membank    #(
        .LOG2_FRAME_SIZE(MAX_NTBS_SIZE)
    )
    intra_topline_buffer_filtered_membank_block2
    (
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),
        
        .portw_addr         (filter_counter_d[0][MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1:0]),
        .portw_data         (topmem_bank_2_wdata),
        .portw_en           (topmem_write_enables[0][2]),
        
        .portr_addr         (top_membank2r_addr),
        .portr_data         (topmem_bank_2_rdata)
        // .portr_en           (top_portr_en)
    );
    
    intra_line_buffer_membank    #(
        .LOG2_FRAME_SIZE(MAX_NTBS_SIZE)
    )
    intra_topline_buffer_filtered_membank_block3
    (
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),
        
        .portw_addr         (filter_counter_d[0][MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1:0]),
        .portw_data         (topmem_bank_3_wdata),
        .portw_en           (topmem_write_enables[0][3]),
        
        .portr_addr         (top_membank3r_addr),
        .portr_data         (topmem_bank_3_rdata)
        // .portr_en           (top_portr_en)
    );
    
    intra_line_buffer_membank    #(
        .LOG2_FRAME_SIZE(MAX_NTBS_SIZE)
    )
    intra_topline_buffer_filtered_membank_block4
    (
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),
        
        .portw_addr         (filter_counter_d[0][MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1:0]),
        .portw_data         (topmem_bank_4_wdata),
        .portw_en           (topmem_write_enables[0][4]),
        
        .portr_addr         (top_membank4r_addr),
        .portr_data         (topmem_bank_4_rdata)
        // .portr_en           (top_portr_en)
    );
    
    intra_line_buffer_membank    #(
        .LOG2_FRAME_SIZE(MAX_NTBS_SIZE)
    )
    intra_topline_buffer_filtered_membank_block5
    (
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),
        
        .portw_addr         (filter_counter_d[0][MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1:0]),
        .portw_data         (topmem_bank_5_wdata),
        .portw_en           (topmem_write_enables[0][5]),
        
        .portr_addr         (top_membank5r_addr),
        .portr_data         (topmem_bank_5_rdata)
        // .portr_en           (top_portr_en)
    );
    
    intra_line_buffer_membank    #(
        .LOG2_FRAME_SIZE(MAX_NTBS_SIZE)
    )
    intra_topline_buffer_filtered_membank_bloc6
    (
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),
        
        .portw_addr         (filter_counter_d[0][MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1:0]),
        .portw_data         (topmem_bank_6_wdata),
        .portw_en           (topmem_write_enables[0][6]),
        
        .portr_addr         (top_membank6r_addr),
        .portr_data         (topmem_bank_6_rdata)
        // .portr_en           (top_portr_en)
    );
    
    intra_line_buffer_membank    #(
        .LOG2_FRAME_SIZE(MAX_NTBS_SIZE)
    )
    intra_topline_buffer_filtered_membank_block7
    (
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),
        
        .portw_addr         (filter_counter_d[0][MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1:0]),
        .portw_data         (topmem_bank_7_wdata),
        .portw_en           (topmem_write_enables[0][7]),
        
        .portr_addr         (top_membank7r_addr),
        .portr_data         (topmem_bank_7_rdata)
        // .portr_en           (top_portr_en)
    );
    
    //----------------------------------------------------------------------
    // LEFT BANKS
    //----------------------------------------------------------------------
    
    intra_line_buffer_membank    #(
        .LOG2_FRAME_SIZE(MAX_NTBS_SIZE)
    )
    intra_leftline_buffer_filtered_membank_block0
    (
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),
        
        .portw_addr         (filter_counter_d[0][MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1:0]),
        .portw_data         (leftmem_bank_0_wdata),
        .portw_en           (leftmem_write_enables[0][0]),
        
        .portr_addr         (left_membank0r_addr),
        .portr_data         (leftmem_bank_0_rdata)
        // .portr_en           (left_portr_en)
    );
    
    intra_line_buffer_membank    #(
        .LOG2_FRAME_SIZE(MAX_NTBS_SIZE)
    )
    intra_leftline_buffer_filtered_membank_block1
    (
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),
        
        .portw_addr         (filter_counter_d[0][MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1:0]),
        .portw_data         (leftmem_bank_1_wdata),
        .portw_en           (leftmem_write_enables[0][1]),
        
        .portr_addr         (left_membank1r_addr),
        .portr_data         (leftmem_bank_1_rdata)
        // .portr_en           (left_portr_en)
    );
    
    intra_line_buffer_membank    #(
        .LOG2_FRAME_SIZE(MAX_NTBS_SIZE)
    )
    intra_leftline_buffer_filtered_membank_block2
    (
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),
        
        .portw_addr         (filter_counter_d[0][MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1:0]),
        .portw_data         (leftmem_bank_2_wdata),
        .portw_en           (leftmem_write_enables[0][2]),
        
        .portr_addr         (left_membank2r_addr),
        .portr_data         (leftmem_bank_2_rdata)
        // .portr_en           (left_portr_en)
    );
    
    intra_line_buffer_membank    #(
        .LOG2_FRAME_SIZE(MAX_NTBS_SIZE)
    )
    intra_leftline_buffer_filtered_membank_block3
    (
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),
        
        .portw_addr         (filter_counter_d[0][MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1:0]),
        .portw_data         (leftmem_bank_3_wdata),
        .portw_en           (leftmem_write_enables[0][3]),
        
        .portr_addr         (left_membank3r_addr),
        .portr_data         (leftmem_bank_3_rdata)
        // .portr_en           (left_portr_en)
    );
    
    intra_line_buffer_membank    #(
        .LOG2_FRAME_SIZE(MAX_NTBS_SIZE)
    )
    intra_leftline_buffer_filtered_membank_block4
    (
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),
        
        .portw_addr         (filter_counter_d[0][MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1:0]),
        .portw_data         (leftmem_bank_4_wdata),
        .portw_en           (leftmem_write_enables[0][4]),
        
        .portr_addr         (left_membank4r_addr),
        .portr_data         (leftmem_bank_4_rdata)
        // .portr_en           (left_portr_en)
    );
    
    intra_line_buffer_membank    #(
        .LOG2_FRAME_SIZE(MAX_NTBS_SIZE)
    )
    intra_leftline_buffer_filtered_membank_block5
    (
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),
        
        .portw_addr         (filter_counter_d[0][MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1:0]),
        .portw_data         (leftmem_bank_5_wdata),
        .portw_en           (leftmem_write_enables[0][5]),
        
        .portr_addr         (left_membank5r_addr),
        .portr_data         (leftmem_bank_5_rdata)
        // .portr_en           (left_portr_en)
    );
    
    intra_line_buffer_membank    #(
        .LOG2_FRAME_SIZE(MAX_NTBS_SIZE)
    )
    intra_leftline_buffer_filtered_membank_bloc6
    (
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),
        
        .portw_addr         (filter_counter_d[0][MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1:0]),
        .portw_data         (leftmem_bank_6_wdata),
        .portw_en           (leftmem_write_enables[0][6]),
        
        .portr_addr         (left_membank6r_addr),
        .portr_data         (leftmem_bank_6_rdata)
        // .portr_en           (left_portr_en)
    );
    
    intra_line_buffer_membank    #(
        .LOG2_FRAME_SIZE(MAX_NTBS_SIZE)
    )
    intra_leftline_buffer_filtered_membank_block7
    (
        .clk                (clk),
        .reset              (reset),
        .enable             (enable),
        
        .portw_addr         (filter_counter_d[0][MAX_NTBS_SIZE - LOG2NUM_OF_BANKS - 1:0]),
        .portw_data         (leftmem_bank_7_wdata),
        .portw_en           (leftmem_write_enables[0][7]),
        
        .portr_addr         (left_membank7r_addr),
        .portr_data         (leftmem_bank_7_rdata)
        // .portr_en           (left_portr_en)
    );
    
always @(*) begin : top_read_mux
        case(top_portr_addr[2:0])
            3'd0 : begin
                //top_membanksr_en    = {8{top_portr_en}};//8'b00011111 & {8{portr_en}};
                
                top_membank0r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank1r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank2r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank3r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank4r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank5r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank6r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank7r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
 
            end
            3'd1 : begin
                //top_membanksr_en    = {8{top_portr_en}};//8'b00111110 & {8{portr_en}};
                
                top_membank0r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank1r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank2r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank3r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank4r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank5r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank6r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank7r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                
            end
            
            3'd2 : begin
                //top_membanksr_en    = {8{top_portr_en}};//8'b01111100 & {8{portr_en}};
                
                top_membank0r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank1r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank2r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank3r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank4r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank5r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank6r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank7r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                
            end
            
            3'd3 : begin
                //top_membanksr_en    = {8{top_portr_en}};//8'b11111000 & {8{portr_en}};
                
                top_membank0r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank1r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank2r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank3r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank4r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank5r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank6r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank7r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];

            end
            
            3'd4 : begin
                //top_membanksr_en    = {8{top_portr_en}};//8'b11110001 & {8{portr_en}};
                
                top_membank0r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank1r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank2r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank3r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank4r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank5r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank6r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank7r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
    
            end
            
            3'd5 : begin
                //top_membanksr_en    = {8{top_portr_en}};//8'b11000111 & {8{portr_en}};
                
                top_membank0r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank1r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank2r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank3r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank4r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank5r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank6r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank7r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                
            end
            
            3'd6 : begin
                //top_membanksr_en     = {8{top_portr_en}};//8'b10001111 & {8{portr_en}};
                
                top_membank0r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank1r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank2r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank3r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank4r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank5r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank6r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                top_membank7r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                
            end
            3'd7 : begin
                //top_membanksr_en     = {8{top_portr_en}};//8'b10001111 & {8{portr_en}};
                
                top_membank0r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank1r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank2r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank3r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank4r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank5r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank6r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                top_membank7r_addr  = top_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                
            end
        endcase
    end
    
    always @(*) begin : top_read_mux_delayed_for_data
        case(top_portr_addr_low3b_delayed)
            3'd0 : begin
                top_portr_data0 = topmem_bank_0_rdata;
                top_portr_data1 = topmem_bank_1_rdata;
                top_portr_data2 = topmem_bank_2_rdata;
                top_portr_data3 = topmem_bank_3_rdata;
                top_portr_data4 = topmem_bank_4_rdata;
                top_portr_data5 = topmem_bank_5_rdata;
                top_portr_data6 = topmem_bank_6_rdata; 
                top_portr_data7 = topmem_bank_7_rdata;
            end
            3'd1 : begin
                top_portr_data0 = topmem_bank_1_rdata;
                top_portr_data1 = topmem_bank_2_rdata;
                top_portr_data2 = topmem_bank_3_rdata;
                top_portr_data3 = topmem_bank_4_rdata;
                top_portr_data4 = topmem_bank_5_rdata;
                top_portr_data5 = topmem_bank_6_rdata;
                top_portr_data6 = topmem_bank_7_rdata; 
                top_portr_data7 = topmem_bank_0_rdata;
            end
            3'd2 : begin
                top_portr_data0 = topmem_bank_2_rdata;
                top_portr_data1 = topmem_bank_3_rdata;
                top_portr_data2 = topmem_bank_4_rdata;
                top_portr_data3 = topmem_bank_5_rdata;
                top_portr_data4 = topmem_bank_6_rdata;
                top_portr_data5 = topmem_bank_7_rdata; 
                top_portr_data6 = topmem_bank_0_rdata;
                top_portr_data7 = topmem_bank_1_rdata;
            end
            3'd3 : begin
                top_portr_data0 = topmem_bank_3_rdata;
                top_portr_data1 = topmem_bank_4_rdata;
                top_portr_data2 = topmem_bank_5_rdata;
                top_portr_data3 = topmem_bank_6_rdata;
                top_portr_data4 = topmem_bank_7_rdata; 
                top_portr_data5 = topmem_bank_0_rdata;
                top_portr_data6 = topmem_bank_1_rdata;
                top_portr_data7 = topmem_bank_2_rdata;
            end
            3'd4 : begin
                top_portr_data0 = topmem_bank_4_rdata;
                top_portr_data1 = topmem_bank_5_rdata;
                top_portr_data2 = topmem_bank_6_rdata;
                top_portr_data3 = topmem_bank_7_rdata;
                top_portr_data4 = topmem_bank_0_rdata;
                top_portr_data5 = topmem_bank_1_rdata;
                top_portr_data6 = topmem_bank_2_rdata;
                top_portr_data7 = topmem_bank_3_rdata;
            end
            3'd5 : begin
                top_portr_data0 = topmem_bank_5_rdata;
                top_portr_data1 = topmem_bank_6_rdata;
                top_portr_data2 = topmem_bank_7_rdata;
                top_portr_data3 = topmem_bank_0_rdata;
                top_portr_data4 = topmem_bank_1_rdata;
                top_portr_data5 = topmem_bank_2_rdata;
                top_portr_data6 = topmem_bank_3_rdata; 
                top_portr_data7 = topmem_bank_4_rdata;
            end
            3'd6 : begin
                top_portr_data0 = topmem_bank_6_rdata;
                top_portr_data1 = topmem_bank_7_rdata;
                top_portr_data2 = topmem_bank_0_rdata;
                top_portr_data3 = topmem_bank_1_rdata;
                top_portr_data4 = topmem_bank_2_rdata;
                top_portr_data5 = topmem_bank_3_rdata; 
                top_portr_data6 = topmem_bank_4_rdata;
                top_portr_data7 = topmem_bank_5_rdata;
            end
            3'd7 : begin
                top_portr_data0 = topmem_bank_7_rdata;
                top_portr_data1 = topmem_bank_0_rdata;
                top_portr_data2 = topmem_bank_1_rdata;
                top_portr_data3 = topmem_bank_2_rdata;
                top_portr_data4 = topmem_bank_3_rdata; 
                top_portr_data5 = topmem_bank_4_rdata;
                top_portr_data6 = topmem_bank_5_rdata;
                top_portr_data7 = topmem_bank_6_rdata;
            end
        endcase
    end

    always @(*) begin : left_read_mux_delayed_for_data
        case(left_portr_addr_low3b_delayed)
            3'd0 : begin
                left_portr_data0 = leftmem_bank_0_rdata;
                left_portr_data1 = leftmem_bank_1_rdata;
                left_portr_data2 = leftmem_bank_2_rdata;
                left_portr_data3 = leftmem_bank_3_rdata;
                left_portr_data4 = leftmem_bank_4_rdata;
                left_portr_data5 = leftmem_bank_5_rdata;
                left_portr_data6 = leftmem_bank_6_rdata; 
                left_portr_data7 = leftmem_bank_7_rdata;
            end
            3'd1 : begin
                left_portr_data0 = leftmem_bank_1_rdata;
                left_portr_data1 = leftmem_bank_2_rdata;
                left_portr_data2 = leftmem_bank_3_rdata;
                left_portr_data3 = leftmem_bank_4_rdata;
                left_portr_data4 = leftmem_bank_5_rdata;
                left_portr_data5 = leftmem_bank_6_rdata;
                left_portr_data6 = leftmem_bank_7_rdata; 
                left_portr_data7 = leftmem_bank_0_rdata;
            end
            3'd2 : begin
                left_portr_data0 = leftmem_bank_2_rdata;
                left_portr_data1 = leftmem_bank_3_rdata;
                left_portr_data2 = leftmem_bank_4_rdata;
                left_portr_data3 = leftmem_bank_5_rdata;
                left_portr_data4 = leftmem_bank_6_rdata;
                left_portr_data5 = leftmem_bank_7_rdata;
                left_portr_data6 = leftmem_bank_0_rdata; 
                left_portr_data7 = leftmem_bank_1_rdata;
            end
            3'd3 : begin
                left_portr_data0 = leftmem_bank_3_rdata;
                left_portr_data1 = leftmem_bank_4_rdata;
                left_portr_data2 = leftmem_bank_5_rdata;
                left_portr_data3 = leftmem_bank_6_rdata;
                left_portr_data4 = leftmem_bank_7_rdata;
                left_portr_data5 = leftmem_bank_0_rdata;
                left_portr_data6 = leftmem_bank_1_rdata; 
                left_portr_data7 = leftmem_bank_2_rdata;
            end
            3'd4 : begin
                left_portr_data0 = leftmem_bank_4_rdata;
                left_portr_data1 = leftmem_bank_5_rdata;
                left_portr_data2 = leftmem_bank_6_rdata;
                left_portr_data3 = leftmem_bank_7_rdata;
                left_portr_data4 = leftmem_bank_0_rdata;
                left_portr_data5 = leftmem_bank_1_rdata;
                left_portr_data6 = leftmem_bank_2_rdata; 
                left_portr_data7 = leftmem_bank_3_rdata;
            end
            3'd5 : begin
                left_portr_data0 = leftmem_bank_5_rdata;
                left_portr_data1 = leftmem_bank_6_rdata;
                left_portr_data2 = leftmem_bank_7_rdata;
                left_portr_data3 = leftmem_bank_0_rdata;
                left_portr_data4 = leftmem_bank_1_rdata;
                left_portr_data5 = leftmem_bank_2_rdata;
                left_portr_data6 = leftmem_bank_3_rdata; 
                left_portr_data7 = leftmem_bank_4_rdata;
            end
            3'd6 : begin
                left_portr_data0 = leftmem_bank_6_rdata;
                left_portr_data1 = leftmem_bank_7_rdata;
                left_portr_data2 = leftmem_bank_0_rdata;
                left_portr_data3 = leftmem_bank_1_rdata;
                left_portr_data4 = leftmem_bank_2_rdata;
                left_portr_data5 = leftmem_bank_3_rdata;
                left_portr_data6 = leftmem_bank_4_rdata;
                left_portr_data7 = leftmem_bank_5_rdata;
            end
            3'd7 : begin
                left_portr_data0 = leftmem_bank_7_rdata;
                left_portr_data1 = leftmem_bank_0_rdata;
                left_portr_data2 = leftmem_bank_1_rdata;
                left_portr_data3 = leftmem_bank_2_rdata;
                left_portr_data4 = leftmem_bank_3_rdata;
                left_portr_data5 = leftmem_bank_4_rdata;
                left_portr_data6 = leftmem_bank_5_rdata;
                left_portr_data7 = leftmem_bank_6_rdata;
            end
        endcase
    end

    always @(*) begin : left_read_mux
        case(left_portr_addr[2:0])
            3'd0 : begin
                //left_membanksr_en    = {8{left_portr_en}};//8'b00011111 & {8{portr_en}};
                
                left_membank0r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank1r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank2r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank3r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank4r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank5r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank6r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank7r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
 
            end
            3'd1 : begin
                //left_membanksr_en    = {8{left_portr_en}};//8'b00111110 & {8{portr_en}};
                
                left_membank0r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank1r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank2r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank3r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank4r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank5r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank6r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank7r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                
            end
            
            3'd2 : begin
                //left_membanksr_en    = {8{left_portr_en}};//8'b01111100 & {8{portr_en}};
                
                left_membank0r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank1r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank2r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank3r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank4r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank5r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank6r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank7r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                
            end
            
            3'd3 : begin
                //left_membanksr_en    = {8{left_portr_en}};//8'b11111000 & {8{portr_en}};
                
                left_membank0r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank1r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank2r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank3r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank4r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank5r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank6r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank7r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];

            end
            
            3'd4 : begin
                //left_membanksr_en    = {8{left_portr_en}};//8'b11111000 & {8{portr_en}};
                
                left_membank0r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank1r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank2r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank3r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank4r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank5r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank6r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank7r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
    
            end
            
            3'd5 : begin
                //left_membanksr_en    = {8{left_portr_en}};//8'b11111000 & {8{portr_en}};
                
                left_membank0r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank1r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank2r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank3r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank4r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank5r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank6r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank7r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                
            end
            
            3'd6 : begin
                //left_membanksr_en    = {8{left_portr_en}};//8'b11111000 & {8{portr_en}};
                
                left_membank0r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank1r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank2r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank3r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank4r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank5r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank6r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                left_membank7r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                
            end
            3'd7 : begin
                //left_membanksr_en    = {8{left_portr_en}};//8'b11111000 & {8{portr_en}};
                
                left_membank0r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank1r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank2r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank3r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank4r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank5r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank6r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS] + 1'b1;
                left_membank7r_addr  = left_portr_addr[MAX_NTBS_SIZE-1:LOG2NUM_OF_BANKS];
                
            end
        endcase
    end
    
endmodule
