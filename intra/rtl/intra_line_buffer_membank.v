`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    07:55:48 10/17/2013 
// Design Name: 
// Module Name:    intra_line_buffer_membank 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module intra_line_buffer_membank(
        clk,
        reset,
        enable,
        
        portw_addr,
        portw_data,
        portw_en,
        portw_prev_data,
        
        portr_addr,
        portr_data,
        portr2_addr
        // portr_en
    );
    
//----------------------------------------------------
//PARAMETERS
//----------------------------------------------------
    parameter                       READ_FIRST              = 1;
    parameter                       LOG2_FRAME_SIZE         = 12;

    
//----------------------------------------------------
//LOCALPARAMS
//----------------------------------------------------
    localparam                      INTRA_PRED_ANGLE_WIDTH  = 6;
    localparam                      MAX_NTBS_SIZE           = 6;
    localparam                      NUM_OF_DATA_REGS        = 5;
    localparam                      PIXEL_WIDTH             = 8;
    
    localparam                      LOG2_NUM_OF_BANKS       = 3;
    localparam                      NUM_OF_BANKS            = 1 << LOG2_NUM_OF_BANKS;
    localparam                      MEMBANK_SIZE            = 1 << (LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS);
    
//----------------------------------------------------
//I/O Signals
//----------------------------------------------------

    input                                                       clk;
    input                                                       reset;
    input                                                       enable;
        
    input       [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]     portw_addr;
    input       [PIXEL_WIDTH - 1:0]                             portw_data;
    input                                                       portw_en;
    output reg  [PIXEL_WIDTH - 1:0]                             portw_prev_data;
    
    input       [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]     portr_addr;
    input       [LOG2_FRAME_SIZE - LOG2_NUM_OF_BANKS - 1:0]     portr2_addr;
    output reg  [PIXEL_WIDTH - 1:0]                             portr_data;
    // input                                                       portr_en;
    
//----------------------------------------------------
//  Internal Regs and Wires 
//----------------------------------------------------

    reg         [PIXEL_WIDTH - 1:0]           mem [MEMBANK_SIZE - 1:0];
    
    
//----------------------------------------------------
//  Implementation
//---------------------------------------------------- 

    always @(posedge clk) begin
    
        if(reset) begin
        
        
        
        end
        else if(enable) begin 
            if(READ_FIRST == 1) begin
                if(portw_en) begin
                    mem[portw_addr] <= portw_data;
                end
                
                // if(portr_en) begin
                    portr_data      <= mem[portr_addr]; 
                    portw_prev_data <= mem[portr2_addr];
                // end
            end
            else begin
                if(portw_en /*& portr_en*/) begin
                    if(portr_addr == portw_addr) begin
                        portr_data <= portw_data;
                        mem[portw_addr] <= portw_data;
                    end
                    else begin
                        portr_data <= mem[portr_addr]; 
                        mem[portw_addr] <= portw_data;
                    end
                end
                else if(portw_en) begin
                    mem[portw_addr] <= portw_data;
                end
                // else if(portr_en) begin
                    portr_data      <= mem[portr_addr]; 
                    portw_prev_data <= mem[portr2_addr];
                // end
            end
        end
    end

endmodule
