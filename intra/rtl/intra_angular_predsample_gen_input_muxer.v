`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:37:14 10/17/2013 
// Design Name: 
// Module Name:    intra_angular_predsample_gen 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module intra_angular_predsample_gen_input_muxer
    (
        clk,
        reset,
        enable,
        
        top_all_invalid_in,
        left_all_invalid_in,
        // filter_flag_in,
        filter_done_in,
        read_from_top_aux_flags_in,
        read_from_left_aux_flags_in,
        
        top_line_buffer_portr_0_in,
        left_line_buffer_portr_0_in,
        top_aux_data_0_in,
        left_aux_data_0_in,
        
        top_line_buffer_portr_1_in,
        left_line_buffer_portr_1_in,
        top_aux_data_1_in,
        left_aux_data_1_in,
        
        top_line_buffer_portr_2_in,
        left_line_buffer_portr_2_in,
        top_aux_data_2_in,
        left_aux_data_2_in,
        
        top_line_buffer_portr_3_in,
        left_line_buffer_portr_3_in,
        top_aux_data_3_in,
        left_aux_data_3_in,
       
        top_line_buffer_portr_4_in,
        left_line_buffer_portr_4_in,
        top_aux_data_4_in,
        left_aux_data_4_in,
        
        corner_pixel_data_in,
        
        // dc_data_0_in,
        // dc_data_1_in,
        // dc_data_2_in,
        // dc_data_3_in,
        // dc_data_4_in,
        dc_data_5_in,
        dc_data_6_in,
        dc_data_7_in,
        
        dc_aux_0_in,
        dc_aux_1_in,
        dc_aux_2_in,
        dc_aux_3_in,
        dc_aux_4_in,
        dc_aux_5_in,
        dc_aux_6_in,
        dc_aux_7_in,
        dc_read_addr_offset_out,
        dc_init_done_out,
        
        intrapredacc_lower5b_in,
        sgn_intrapredangle_in,
        // xt_in,
        // yt_in,
        inner_x_in,
        inner_y_in,
        top_zeroth_in,
        left_zeroth_in,
        planar_top_ntbs_in,
        planar_left_ntbs_in,
        ntbs_in,
        
        mode_in,
        top_mode_modified_for_ver_hor_in,
        left_mode_modified_for_ver_hor_in,
        
        one_before_last_4by4_out,
        predsample_4by4_out,
        predsample_4by4_valid_out,
        
        valid_in
        // valid_out
        
    );
    
//----------------------------------------------------
//PARAMETER
//----------------------------------------------------    
    
    parameter                       CIDX = 0;
    
//----------------------------------------------------
//LOCALPARAMS
//----------------------------------------------------
    localparam                      INTRA_PRED_ANGLE_WIDTH  = 6;
    localparam                      MAX_NTBS_SIZE           = 6;
    localparam                      NUM_OF_DATA_REGS        = 5;
    localparam                      PIXEL_WIDTH             = 8;
    localparam                      LOG2_FRAME_SIZE         = 12;
    localparam                      BIT_DEPTH               = 8;
    localparam                      OUTPUT_BLOCK_SIZE       = 4;
    
    localparam                      ANGULAR_MODE_WIDTH      = 1;
    localparam                      ANGULAR_VERTICAL_MODE   = 0;
    localparam                      ANGULAR_HORIZONTAL_MODE = 1;
    localparam                      PLANAR_MODE = 2;
    localparam                      DC_MODE = 3;
    localparam                      DC_CIDX_WIDTH           = 2;
    localparam                      CIDX_WIDTH = 2;
    
    localparam                      TOP_PRED_MODE_WIDTH         = 2;
    localparam                      TOP_PRED_MODE_DC            = 0;
    localparam                      TOP_PRED_MODE_PLANAR        = 1;
    localparam                      TOP_PRED_MODE_ANGULAR_TOP   = 2;
    localparam                      TOP_PRED_MODE_ANGULAR_LEFT  = 3;
    
//----------------------------------------------------
//I/O Signals
//----------------------------------------------------
    
    input                                   clk;
    input                                   reset;
    input                                   enable;
            
    input                                   top_all_invalid_in;
    input                                   left_all_invalid_in;
    input   [4:0]                           read_from_top_aux_flags_in;
    input   [4:0]                           read_from_left_aux_flags_in;
    input                                   filter_done_in;
    // input                                   filter_flag_in;
            
    input   [PIXEL_WIDTH - 1:0]             top_line_buffer_portr_0_in;
    input   [PIXEL_WIDTH - 1:0]             left_line_buffer_portr_0_in;
    input   [PIXEL_WIDTH - 1:0]             top_aux_data_0_in;
    input   [PIXEL_WIDTH - 1:0]             left_aux_data_0_in;
                
    input   [PIXEL_WIDTH - 1:0]             top_line_buffer_portr_1_in;
    input   [PIXEL_WIDTH - 1:0]             left_line_buffer_portr_1_in;
    input   [PIXEL_WIDTH - 1:0]             top_aux_data_1_in;
    input   [PIXEL_WIDTH - 1:0]             left_aux_data_1_in;
            
    input   [PIXEL_WIDTH - 1:0]             top_line_buffer_portr_2_in;
    input   [PIXEL_WIDTH - 1:0]             left_line_buffer_portr_2_in;
    input   [PIXEL_WIDTH - 1:0]             top_aux_data_2_in;
    input   [PIXEL_WIDTH - 1:0]             left_aux_data_2_in;
            
    input   [PIXEL_WIDTH - 1:0]             top_line_buffer_portr_3_in;
    input   [PIXEL_WIDTH - 1:0]             left_line_buffer_portr_3_in;
    input   [PIXEL_WIDTH - 1:0]             top_aux_data_3_in;
    input   [PIXEL_WIDTH - 1:0]             left_aux_data_3_in;
            
    input   [PIXEL_WIDTH - 1:0]             top_line_buffer_portr_4_in;
    input   [PIXEL_WIDTH - 1:0]             left_line_buffer_portr_4_in;
    input   [PIXEL_WIDTH - 1:0]             top_aux_data_4_in;
    input   [PIXEL_WIDTH - 1:0]             left_aux_data_4_in;
            
    input   [PIXEL_WIDTH - 1:0]             corner_pixel_data_in;

    // input   [PIXEL_WIDTH - 1:0]             dc_data_0_in;
    // input   [PIXEL_WIDTH - 1:0]             dc_data_1_in;
    // input   [PIXEL_WIDTH - 1:0]             dc_data_2_in;
    // input   [PIXEL_WIDTH - 1:0]             dc_data_3_in;
    // input   [PIXEL_WIDTH - 1:0]             dc_data_4_in;
    input   [PIXEL_WIDTH - 1:0]             dc_data_5_in;
    input   [PIXEL_WIDTH - 1:0]             dc_data_6_in;
    input   [PIXEL_WIDTH - 1:0]             dc_data_7_in;
        
    input   [PIXEL_WIDTH - 1:0]             dc_aux_0_in;
    input   [PIXEL_WIDTH - 1:0]             dc_aux_1_in;
    input   [PIXEL_WIDTH - 1:0]             dc_aux_2_in;
    input   [PIXEL_WIDTH - 1:0]             dc_aux_3_in;
    input   [PIXEL_WIDTH - 1:0]             dc_aux_4_in;
    input   [PIXEL_WIDTH - 1:0]             dc_aux_5_in;
    input   [PIXEL_WIDTH - 1:0]             dc_aux_6_in;
    input   [PIXEL_WIDTH - 1:0]             dc_aux_7_in; 

    output  [MAX_NTBS_SIZE - 1:0]           dc_read_addr_offset_out;
    output                                  dc_init_done_out;
    
    input   [4:0]                           intrapredacc_lower5b_in;
    input                                   sgn_intrapredangle_in;
    // input   [LOG2_FRAME_SIZE - 1:0]         xt_in;
    // input   [LOG2_FRAME_SIZE - 1:0]         yt_in;
    input   [MAX_NTBS_SIZE - 1:0]           inner_x_in;
    input   [MAX_NTBS_SIZE - 1:0]           inner_y_in;
    input   [PIXEL_WIDTH - 1:0]             top_zeroth_in;
    input   [PIXEL_WIDTH - 1:0]             left_zeroth_in;
    input   [PIXEL_WIDTH - 1:0]             planar_top_ntbs_in;
    input   [PIXEL_WIDTH - 1:0]             planar_left_ntbs_in;
    input   [MAX_NTBS_SIZE - 1:0]           ntbs_in;
    input   [TOP_PRED_MODE_WIDTH - 1:0]     mode_in;
    input   [TOP_PRED_MODE_WIDTH - 1:0]     top_mode_modified_for_ver_hor_in;
    input   [TOP_PRED_MODE_WIDTH - 1:0]     left_mode_modified_for_ver_hor_in;
    
    output                                                              one_before_last_4by4_out;
    output  [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]     predsample_4by4_out;
    output                                                              predsample_4by4_valid_out;
            
    input                                   valid_in;
    // output reg                              valid_out;
    
//----------------------------------------------------
//  Internal Regs and Wires 
//---------------------------------------------------- 

    

    reg [PIXEL_WIDTH - 1:0]         muxed_out_data0;
    reg [PIXEL_WIDTH - 1:0]         muxed_out_data1;
    reg [PIXEL_WIDTH - 1:0]         muxed_out_data2;
    reg [PIXEL_WIDTH - 1:0]         muxed_out_data3;
    reg [PIXEL_WIDTH - 1:0]         muxed_out_data4;
    
    reg [PIXEL_WIDTH - 1:0]         muxed_out_data0_reg;
    reg [PIXEL_WIDTH - 1:0]         muxed_out_data1_reg;
    reg [PIXEL_WIDTH - 1:0]         muxed_out_data2_reg;
    reg [PIXEL_WIDTH - 1:0]         muxed_out_data3_reg;
    reg [PIXEL_WIDTH - 1:0]         muxed_out_data4_reg;
    
//    reg [PIXEL_WIDTH - 1:0]         dc_data_0_reg;
//    reg [PIXEL_WIDTH - 1:0]         dc_data_1_reg;
//    reg [PIXEL_WIDTH - 1:0]         dc_data_2_reg;
//    reg [PIXEL_WIDTH - 1:0]         dc_data_3_reg;
//    reg [PIXEL_WIDTH - 1:0]         dc_data_4_reg;
    reg [PIXEL_WIDTH - 1:0]         dc_data_5_reg;
    reg [PIXEL_WIDTH - 1:0]         dc_data_6_reg;
    reg [PIXEL_WIDTH - 1:0]         dc_data_7_reg;
                
    reg [PIXEL_WIDTH - 1:0]         dc_aux_0_reg;
    reg [PIXEL_WIDTH - 1:0]         dc_aux_1_reg;
    reg [PIXEL_WIDTH - 1:0]         dc_aux_2_reg;
    reg [PIXEL_WIDTH - 1:0]         dc_aux_3_reg;
    reg [PIXEL_WIDTH - 1:0]         dc_aux_4_reg;
    reg [PIXEL_WIDTH - 1:0]         dc_aux_5_reg;
    reg [PIXEL_WIDTH - 1:0]         dc_aux_6_reg;
    reg [PIXEL_WIDTH - 1:0]         dc_aux_7_reg;

    reg [ANGULAR_MODE_WIDTH - 1:0]  angular_mode_reg;
    reg                             angular_hor_ver_mode_reg;

    reg                             angular_en;
    reg                             planar_en;
    reg                             dc_en;
    
//    integer                         dcstate;
    


//----------------------------------------------------
//  Implementation
//---------------------------------------------------- 


    always @(*) begin : input_muxing
        if((mode_in == TOP_PRED_MODE_ANGULAR_TOP)||(mode_in == TOP_PRED_MODE_PLANAR)||(mode_in == TOP_PRED_MODE_DC)) begin        
            //mux0
            if(top_all_invalid_in & left_all_invalid_in) begin
                muxed_out_data0 = 1'b1 << (BIT_DEPTH-1);
            end
            else if(read_from_top_aux_flags_in[0]) begin
                if(left_all_invalid_in) begin
                    muxed_out_data0 = top_zeroth_in;
                end
                else begin
                    muxed_out_data0 = top_aux_data_0_in;
                end
            end
            else if(top_all_invalid_in) begin
                muxed_out_data0 = left_zeroth_in;
            end
            else begin
                muxed_out_data0 = top_line_buffer_portr_0_in;
            end
            
            //mux1
            if(top_all_invalid_in & left_all_invalid_in) begin
                muxed_out_data1 = 1'b1 << (BIT_DEPTH-1);
            end
            else if(read_from_top_aux_flags_in[1]) begin
                if(left_all_invalid_in) begin
                    muxed_out_data1 = top_zeroth_in;
                end
                else begin
                    muxed_out_data1 = top_aux_data_1_in;
                end
            end
            else if(top_all_invalid_in) begin
                muxed_out_data1 = left_zeroth_in;
            end
            else begin
                muxed_out_data1 = top_line_buffer_portr_1_in;
            end
            
            //mux2
            if(top_all_invalid_in & left_all_invalid_in) begin
                muxed_out_data2 = 1'b1 << (BIT_DEPTH-1);
            end
            else if(read_from_top_aux_flags_in[2]) begin
                if(left_all_invalid_in) begin
                    muxed_out_data2 = top_zeroth_in;
                end
                else begin
                    muxed_out_data2 = top_aux_data_2_in;
                end
            end
            else if(top_all_invalid_in) begin
                muxed_out_data2 = left_zeroth_in;
            end
            else begin
                muxed_out_data2 = top_line_buffer_portr_2_in;
            end
            
            //mux3
            if(top_all_invalid_in & left_all_invalid_in) begin
                muxed_out_data3 = 1'b1 << (BIT_DEPTH-1);
            end
            else if(read_from_top_aux_flags_in[3]) begin
                if(left_all_invalid_in) begin
                    muxed_out_data3 = top_zeroth_in;
                end
                else begin
                    muxed_out_data3 = top_aux_data_3_in;
                end
            end
            else if(top_all_invalid_in) begin
                muxed_out_data3 = left_zeroth_in;
            end
            else begin
                muxed_out_data3 = top_line_buffer_portr_3_in;
            end
            
            //mux4
            if(top_all_invalid_in & left_all_invalid_in) begin
                muxed_out_data4 = 1'b1 << (BIT_DEPTH-1);
            end
            else if(read_from_top_aux_flags_in[4]) begin
                if(left_all_invalid_in) begin
                    muxed_out_data4 = top_zeroth_in;
                end
                else begin
                    muxed_out_data4 = top_aux_data_4_in;
                end
            end
            else if(top_all_invalid_in) begin
                muxed_out_data4 = left_zeroth_in;
            end
            else begin
                muxed_out_data4 = top_line_buffer_portr_4_in;
            end
        end
        else begin //HORIZONTAL MODE
            //mux0
            if(top_all_invalid_in & left_all_invalid_in) begin
                muxed_out_data0 = 1'b1 << (BIT_DEPTH-1);
            end
            else if(read_from_left_aux_flags_in[0]) begin
                if(top_all_invalid_in) begin
                    muxed_out_data0 = left_zeroth_in;
                end
                else begin
                    muxed_out_data0 = left_aux_data_0_in;
                end
            end
            else if(left_all_invalid_in) begin
                muxed_out_data0 = top_zeroth_in;
            end
            else begin
                muxed_out_data0 = left_line_buffer_portr_0_in;
            end
            
            //mux1
            if(top_all_invalid_in & left_all_invalid_in) begin
                muxed_out_data1 = 1'b1 << (BIT_DEPTH-1);
            end
            else if(read_from_left_aux_flags_in[1]) begin
                if(top_all_invalid_in) begin
                    muxed_out_data1 = left_zeroth_in;
                end
                else begin
                    muxed_out_data1 = left_aux_data_1_in;
                end
            end
            else if(left_all_invalid_in) begin
                muxed_out_data1 = top_zeroth_in;
            end
            else begin
                muxed_out_data1 = left_line_buffer_portr_1_in;
            end
            
            //mux2
            if(top_all_invalid_in & left_all_invalid_in) begin
                muxed_out_data2 = 1'b1 << (BIT_DEPTH-1);
            end
            else if(read_from_left_aux_flags_in[2]) begin
                if(top_all_invalid_in) begin
                    muxed_out_data2 = left_zeroth_in;
                end
                else begin
                    muxed_out_data2 = left_aux_data_2_in;
                end
            end
            else if(left_all_invalid_in) begin
                muxed_out_data2 = top_zeroth_in;
            end
            else begin
                muxed_out_data2 = left_line_buffer_portr_2_in;
            end
            
            //mux3
            if(top_all_invalid_in & left_all_invalid_in) begin
                muxed_out_data3 = 1'b1 << (BIT_DEPTH-1);
            end
            else if(read_from_left_aux_flags_in[3]) begin
                if(top_all_invalid_in) begin
                    muxed_out_data3 = left_zeroth_in;
                end
                else begin
                    muxed_out_data3 = left_aux_data_3_in;
                end
            end
            else if(left_all_invalid_in) begin
                muxed_out_data3 = top_zeroth_in;
            end
            else begin
                muxed_out_data3 = left_line_buffer_portr_3_in;
            end
            
            //mux4
            if(top_all_invalid_in & left_all_invalid_in) begin
                muxed_out_data4 = 1'b1 << (BIT_DEPTH-1);
            end
            else if(read_from_left_aux_flags_in[4]) begin
                if(top_all_invalid_in) begin
                    muxed_out_data4 = left_zeroth_in;
                end
                else begin
                    muxed_out_data4 = left_aux_data_4_in;
                end
            end
            else if(left_all_invalid_in) begin
                muxed_out_data4 = top_zeroth_in;
            end
            else begin
                muxed_out_data4 = left_line_buffer_portr_4_in;
            end
            
            // //mux4
            // if(top_all_invalid_in & left_all_invalid_in) begin
            //     muxed_out_data4 = 1'b1 << (BIT_DEPTH-1);
            // end
            // else if(read_from_top_aux_flags_in[4]) begin
            //     if(top_all_invalid_in) begin
            //         muxed_out_data4 = left_zeroth_in;
            //     end
            //     else begin
            //         muxed_out_data4 = left_aux_data_4_in;
            //     end
            // end
            // else if(left_all_invalid_in) begin
            //     muxed_out_data4 = top_zeroth_in;
            // end
            // else begin
            //     muxed_out_data4 = left_line_buffer_portr_4_in;
            // end
            
            
        end
    end

    always @(posedge clk) begin
        if(reset) begin
                
        end
        else if (enable) begin
            if(valid_in) begin
                if(filter_done_in == 1'b1) begin
                    // muxed_out_data0_reg <= muxed_out_data0;
                    // muxed_out_data1_reg <= muxed_out_data1;
                    // muxed_out_data2_reg <= muxed_out_data2;
                    // muxed_out_data3_reg <= muxed_out_data3;
                    // muxed_out_data4_reg <= muxed_out_data4;

                    case(mode_in)
                        TOP_PRED_MODE_ANGULAR_TOP,TOP_PRED_MODE_ANGULAR_LEFT : begin
                            angular_en  <= 1'b1;
                            planar_en   <= 1'b0;
                            dc_en       <= 1'b0;
                        end
                        TOP_PRED_MODE_PLANAR : begin
                            planar_en   <= 1'b1;
                            angular_en  <= 1'b0;
                            dc_en       <= 1'b0;
                        end
                        TOP_PRED_MODE_DC : begin
                            planar_en   <= 1'b0;
                            angular_en  <= 1'b0;
                            dc_en       <= 1'b1;
                            
                            // if(top_all_invalid_in & left_all_invalid_in) begin
                            //     muxed_out_data0_reg   <=  (1'b1 << (BIT_DEPTH - 1'b1)); 
                            //     muxed_out_data1_reg   <=  (1'b1 << (BIT_DEPTH - 1'b1)); 
                            //     muxed_out_data2_reg   <=  (1'b1 << (BIT_DEPTH - 1'b1)); 
                            //     muxed_out_data3_reg   <=  (1'b1 << (BIT_DEPTH - 1'b1)); 
                            //     muxed_out_data4_reg   <=  (1'b1 << (BIT_DEPTH - 1'b1)); 
                            //     dc_data_5_reg   <=  (1'b1 << (BIT_DEPTH - 1'b1)); 
                            //     dc_data_6_reg   <=  (1'b1 << (BIT_DEPTH - 1'b1)); 
                            //     dc_data_7_reg   <=  (1'b1 << (BIT_DEPTH - 1'b1)); 
                                
                            //     dc_aux_0_reg    <=  (1'b1 << (BIT_DEPTH - 1'b1));  
                            //     dc_aux_1_reg    <=  (1'b1 << (BIT_DEPTH - 1'b1));  
                            //     dc_aux_2_reg    <=  (1'b1 << (BIT_DEPTH - 1'b1));  
                            //     dc_aux_3_reg    <=  (1'b1 << (BIT_DEPTH - 1'b1));     
                            //     dc_aux_4_reg    <=  (1'b1 << (BIT_DEPTH - 1'b1));  
                            //     dc_aux_5_reg    <=  (1'b1 << (BIT_DEPTH - 1'b1));  
                            //     dc_aux_6_reg    <=  (1'b1 << (BIT_DEPTH - 1'b1));  
                            //     dc_aux_7_reg    <=  (1'b1 << (BIT_DEPTH - 1'b1));  
                            // end
                            // else begin
                            //     if(left_all_invalid_in == 1'b1) begin
                            //         dc_aux_0_reg    <= top_zeroth_in;
                            //         dc_aux_1_reg    <= top_zeroth_in;
                            //         dc_aux_2_reg    <= top_zeroth_in;
                            //         dc_aux_3_reg    <= top_zeroth_in;
                            //         dc_aux_4_reg    <= top_zeroth_in;
                            //         dc_aux_5_reg    <= top_zeroth_in;
                            //         dc_aux_6_reg    <= top_zeroth_in;
                            //         dc_aux_7_reg    <= top_zeroth_in;
                            //     end
                            //     else begin
                            //         dc_aux_0_reg    <= dc_aux_0_in;
                            //         dc_aux_1_reg    <= dc_aux_1_in;
                            //         dc_aux_2_reg    <= dc_aux_2_in;
                            //         dc_aux_3_reg    <= dc_aux_3_in;
                            //         dc_aux_4_reg    <= dc_aux_4_in;
                            //         dc_aux_5_reg    <= dc_aux_5_in;
                            //         dc_aux_6_reg    <= dc_aux_6_in;
                            //         dc_aux_7_reg    <= dc_aux_7_in;
                            //     end
                                
                            //     if(top_all_invalid_in == 1'b1) begin
                            //         muxed_out_data0_reg   <= left_zeroth_in;
                            //         muxed_out_data1_reg   <= left_zeroth_in;
                            //         muxed_out_data2_reg   <= left_zeroth_in;
                            //         muxed_out_data3_reg   <= left_zeroth_in;
                            //         muxed_out_data4_reg   <= left_zeroth_in;
                            //         dc_data_5_reg   <= left_zeroth_in;
                            //         dc_data_6_reg   <= left_zeroth_in;
                            //         dc_data_7_reg   <= left_zeroth_in;
                            //     end
                            //     else begin
                            //         muxed_out_data0_reg   <= dc_data_0_in;
                            //         muxed_out_data1_reg   <= dc_data_1_in;
                            //         muxed_out_data2_reg   <= dc_data_2_in;
                            //         muxed_out_data3_reg   <= dc_data_3_in;
                            //         muxed_out_data4_reg   <= dc_data_4_in;
                            //         dc_data_5_reg   <= dc_data_5_in;
                            //         dc_data_6_reg   <= dc_data_6_in;
                            //         dc_data_7_reg   <= dc_data_7_in;
                            //     end
                            // end
                        end
                    endcase                    
                end
                else begin
                    planar_en   <= 1'b0;
                    angular_en  <= 1'b0;
                    dc_en       <= 1'b0;
                end

            end
            else begin
                planar_en   <= 1'b0;
                angular_en  <= 1'b0;
                dc_en       <= 1'b0; 
            end
        end
    end

    always @(posedge clk) begin

        if((mode_in != top_mode_modified_for_ver_hor_in)||(mode_in != left_mode_modified_for_ver_hor_in)) begin
            angular_hor_ver_mode_reg <= 1'b1;
        end
        else begin
            angular_hor_ver_mode_reg <= 1'b0;
        end

        muxed_out_data0_reg <= muxed_out_data0;
        muxed_out_data1_reg <= muxed_out_data1;
        muxed_out_data2_reg <= muxed_out_data2;
        muxed_out_data3_reg <= muxed_out_data3;
        muxed_out_data4_reg <= muxed_out_data4;

        if(top_all_invalid_in & left_all_invalid_in) begin
            // muxed_out_data0_reg   <=  (1'b1 << (BIT_DEPTH - 1'b1)); 
            // muxed_out_data1_reg   <=  (1'b1 << (BIT_DEPTH - 1'b1)); 
            // muxed_out_data2_reg   <=  (1'b1 << (BIT_DEPTH - 1'b1)); 
            // muxed_out_data3_reg   <=  (1'b1 << (BIT_DEPTH - 1'b1)); 
            // muxed_out_data4_reg   <=  (1'b1 << (BIT_DEPTH - 1'b1)); 
            dc_data_5_reg   <=  (1'b1 << (BIT_DEPTH - 1'b1)); 
            dc_data_6_reg   <=  (1'b1 << (BIT_DEPTH - 1'b1)); 
            dc_data_7_reg   <=  (1'b1 << (BIT_DEPTH - 1'b1)); 
            
            dc_aux_0_reg    <=  (1'b1 << (BIT_DEPTH - 1'b1));  
            dc_aux_1_reg    <=  (1'b1 << (BIT_DEPTH - 1'b1));  
            dc_aux_2_reg    <=  (1'b1 << (BIT_DEPTH - 1'b1));  
            dc_aux_3_reg    <=  (1'b1 << (BIT_DEPTH - 1'b1));     
            dc_aux_4_reg    <=  (1'b1 << (BIT_DEPTH - 1'b1));  
            dc_aux_5_reg    <=  (1'b1 << (BIT_DEPTH - 1'b1));  
            dc_aux_6_reg    <=  (1'b1 << (BIT_DEPTH - 1'b1));  
            dc_aux_7_reg    <=  (1'b1 << (BIT_DEPTH - 1'b1));  
        end
        else begin
            if(left_all_invalid_in == 1'b1) begin
                dc_aux_0_reg    <= top_zeroth_in;
                dc_aux_1_reg    <= top_zeroth_in;
                dc_aux_2_reg    <= top_zeroth_in;
                dc_aux_3_reg    <= top_zeroth_in;
                dc_aux_4_reg    <= top_zeroth_in;
                dc_aux_5_reg    <= top_zeroth_in;
                dc_aux_6_reg    <= top_zeroth_in;
                dc_aux_7_reg    <= top_zeroth_in;
            end
            else begin
                dc_aux_0_reg    <= dc_aux_0_in;
                dc_aux_1_reg    <= dc_aux_1_in;
                dc_aux_2_reg    <= dc_aux_2_in;
                dc_aux_3_reg    <= dc_aux_3_in;
                dc_aux_4_reg    <= dc_aux_4_in;
                dc_aux_5_reg    <= dc_aux_5_in;
                dc_aux_6_reg    <= dc_aux_6_in;
                dc_aux_7_reg    <= dc_aux_7_in;
            end
            
            if(top_all_invalid_in == 1'b1) begin
                // muxed_out_data0_reg   <= left_zeroth_in;
                // muxed_out_data1_reg   <= left_zeroth_in;
                // muxed_out_data2_reg   <= left_zeroth_in;
                // muxed_out_data3_reg   <= left_zeroth_in;
                // muxed_out_data4_reg   <= left_zeroth_in;
                dc_data_5_reg   <= left_zeroth_in;
                dc_data_6_reg   <= left_zeroth_in;
                dc_data_7_reg   <= left_zeroth_in;
            end
            else begin
                // muxed_out_data0_reg   <= dc_data_0_in;
                // muxed_out_data1_reg   <= dc_data_1_in;
                // muxed_out_data2_reg   <= dc_data_2_in;
                // muxed_out_data3_reg   <= dc_data_3_in;
                // muxed_out_data4_reg   <= dc_data_4_in;
                dc_data_5_reg   <= dc_data_5_in;
                dc_data_6_reg   <= dc_data_6_in;
                dc_data_7_reg   <= dc_data_7_in;
            end
        end
    end
    
    always @(*) begin
        if(mode_in == TOP_PRED_MODE_ANGULAR_TOP) begin
            angular_mode_reg = ANGULAR_VERTICAL_MODE;
        end
        else begin
            angular_mode_reg = ANGULAR_HORIZONTAL_MODE;
        end
    end
    
    predsample_gen
    #(
        .CIDX(CIDX)
    )
    predsample_gen_block
    (
        .clk                         (clk),
        .reset                       (reset),
        .enable                      (enable),
        
        .intrapredangle_low5b_in     (intrapredacc_lower5b_in),
        .sgn_intrapredangle_in       (sgn_intrapredangle_in),
        .mode_in                     (angular_mode_reg),
        .angular_hor_ver_mode_in     (angular_hor_ver_mode_reg),
        .angular_en_in               (angular_en),
        .planar_en_in                (planar_en),
        .dc_en_in                    (dc_en),
        .corner_pixel_in             (corner_pixel_data_in),
        .top_zeroth_in               (top_zeroth_in),
        .left_zeroth_in              (left_zeroth_in),
        
        .dc_read_addr_offset_out     (dc_read_addr_offset_out),
        .dc_init_done_out            (dc_init_done_out),
        
        .data_0_in                   (muxed_out_data0_reg),
        .data_1_in                   (muxed_out_data1_reg),
        .data_2_in                   (muxed_out_data2_reg),
        .data_3_in                   (muxed_out_data3_reg),
        .data_4_in                   (muxed_out_data4_reg),
        .data_5_in                   (dc_data_5_reg),
        .data_6_in                   (dc_data_6_reg),
        .data_7_in                   (dc_data_7_reg),
        
//        .dc_cidx_in                  (CIDX),
        .dc_aux_0_in                 (dc_aux_0_reg),
        .dc_aux_1_in                 (dc_aux_1_reg),
        .dc_aux_2_in                 (dc_aux_2_reg),
        .dc_aux_3_in                 (dc_aux_3_reg),
        .dc_aux_4_in                 (dc_aux_4_reg),
        .dc_aux_5_in                 (dc_aux_5_reg),
        .dc_aux_6_in                 (dc_aux_6_reg),
        .dc_aux_7_in                 (dc_aux_7_reg),
        
        .planar_top_ntbs_in          (planar_top_ntbs_in),
        .planar_left_ntbs_in         (planar_left_ntbs_in),
        .inner_x_in                  (inner_x_in),
        .inner_y_in                  (inner_y_in),
        .ntbs_in                     (ntbs_in),
        
        .one_before_last_4by4_out    (one_before_last_4by4_out),
        .predsample_4by4_out         (predsample_4by4_out),
        .predsample_4by4_valid_out   (predsample_4by4_valid_out)   
    );
    

    


endmodule
