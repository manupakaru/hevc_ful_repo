module available_pixel_checker(
    clk,
    reset,
    enable,

    frame_width_in,
    frame_height_in,
    tile_width_in,
    tile_xc_in,
    tile_yc_in,
    slice_x_in,
    slice_y_in,

    log2ctbsize_in,
    log2ctbsize_config_in,

    curr_xin,
    curr_yin,
    ntbs_in,
    min_top_available_range_in,
    min_left_available_range_in,
    curr_valid_in,

    validrange_top_out,
    validrange_left_out,
    // validrange_topleft_out,
    all_invalid_top_out,
    all_invalid_left_out,
    valid_out

);

//-------------------------------------------------------------
//      Parameters
//-------------------------------------------------------------

    localparam           PIXEL_ADDR_LENGTH              = 12;
    localparam           MIN_CTB_SIZE                   = 2;
    localparam           INNER_CTB_PIXEL_ADDR_WIDTH     = PIXEL_ADDR_LENGTH - MIN_CTB_SIZE;
    localparam           MAX_LOG2CTBSIZE_WIDTH          = 3;
    localparam           VALID_RANGE_WIDTH              = 6;
    localparam           MAX_CTB_PIXEL_ADDR_WIDTH       = 6;

    localparam           STATE_INIT                     = 0;
    localparam           STATE_IDLE                     = 1;
    localparam           STATE_MAIN                     = 2;
    localparam           STATE_WAIT                     = 3;
	 localparam				 STATE_IDLE_CTB_4X4			= 4;
	 localparam			    STATE_IDLE_CTB_8X8				  = 5;
	 localparam				 STATE_IDLE_CTB_16X16			  = 6;
	 localparam				 STATE_IDLE_CTB_32X32           = 7;
	 localparam				 STATE_IDLE_CTB_64X64			  = 8;
	 localparam				 STATE_ERROR			    = 9;
//-------------------------------------------------------------
//      I/O
//-------------------------------------------------------------

    input                                        clk;
    input                                        reset;
    input                                        enable;

    input       [PIXEL_ADDR_LENGTH - 1:0]          frame_width_in;
    input       [PIXEL_ADDR_LENGTH - 1:0]          frame_height_in;
    input       [PIXEL_ADDR_LENGTH - 1:0]          tile_width_in;
    input       [PIXEL_ADDR_LENGTH - 1:0]          tile_xc_in;
    input       [PIXEL_ADDR_LENGTH - 1:0]          tile_yc_in;
    input       [PIXEL_ADDR_LENGTH - 1:0]          slice_x_in;
    input       [PIXEL_ADDR_LENGTH - 1:0]          slice_y_in;

    input       [MAX_LOG2CTBSIZE_WIDTH - 1:0]      log2ctbsize_in;
    input                                          log2ctbsize_config_in;

    input       [PIXEL_ADDR_LENGTH - 1:0]          curr_xin;
    input       [PIXEL_ADDR_LENGTH - 1:0]          curr_yin;
    input       [6-1:0]                            ntbs_in;
    input       [PIXEL_ADDR_LENGTH - 1:0]          min_top_available_range_in;
    input       [PIXEL_ADDR_LENGTH - 1:0]          min_left_available_range_in;
    input                                          curr_valid_in;

    output reg  [PIXEL_ADDR_LENGTH - 1:0]          validrange_top_out;
    output reg  [PIXEL_ADDR_LENGTH - 1:0]          validrange_left_out;
    // output reg                                     validrange_topleft_out;
    output reg                                     all_invalid_top_out;
    output reg                                     all_invalid_left_out;
    output reg                                     valid_out;

//Internal Wires and Regs
    integer                                             state;
    integer                                             old_state;
    integer                                             initial_state;

    wire        [VALID_RANGE_WIDTH - 1:0]               valid_range_top_int;
    wire        [VALID_RANGE_WIDTH - 1:0]               valid_range_left_int;

    reg         [PIXEL_ADDR_LENGTH - 1:0]      			curr_ctb_x;
    reg         [PIXEL_ADDR_LENGTH - 1:0]      			curr_ctb_y;
    reg         [MAX_CTB_PIXEL_ADDR_WIDTH - 1:0]        no_of_pixels_ctb;
    reg         [MAX_CTB_PIXEL_ADDR_WIDTH - 1:0]        curr_inner_x;
    reg         [MAX_CTB_PIXEL_ADDR_WIDTH - 1:0]        curr_inner_y;

    reg         [PIXEL_ADDR_LENGTH-1:0]                 x_plus_ntbs_reg;
    reg         [PIXEL_ADDR_LENGTH-1:0]                 inner_y_plus_ntbs_reg;


    reference_pixel_valid_range_mem
        #(
            .VALID_RANGE_WIDTH(VALID_RANGE_WIDTH)
        )
    reference_pixel_valid_range_mem_block
        (
            .curr_xin(curr_inner_x>>2),
            .curr_yin(curr_inner_y>>2),

            .valid_range_top_out(valid_range_top_int),
            .valid_range_left_out(valid_range_left_int)
        );

    always @(posedge clk) begin
        old_state <= state;
    end

    always @(posedge clk or posedge reset) begin
        if(reset) begin
                state           <= STATE_INIT;
                initial_state   <= STATE_INIT;
                valid_out       <= 1'b0;
        end
        else if (enable) begin
            case(state)
					STATE_INIT : begin
					  if(log2ctbsize_config_in) begin
							case(log2ctbsize_in)
								 2: state <= STATE_IDLE_CTB_4X4;
								 3: state <= STATE_IDLE_CTB_8X8;
								 4: state <= STATE_IDLE_CTB_16X16;
								 5: state <= STATE_IDLE_CTB_32X32;
								 6: state <= STATE_IDLE_CTB_64X64;
								 default : state <= STATE_ERROR;
							endcase
					  end
					end
					STATE_IDLE_CTB_4X4 : begin

						curr_ctb_x              <=      {curr_xin[PIXEL_ADDR_LENGTH - 1:2],2'd0};//curr_xin     >> 2;
						curr_ctb_y              <=      {curr_yin[PIXEL_ADDR_LENGTH - 1:2],2'd0};//curr_yin     >> 2;
						curr_inner_x            <=      curr_xin[1:0];
						curr_inner_y            <=      curr_yin[1:0];

                        x_plus_ntbs_reg         <=      curr_xin + (ntbs_in << 1);
                        inner_y_plus_ntbs_reg   <=      curr_yin[1:0] + (ntbs_in << 1);


						no_of_pixels_ctb        <=      4 - 1;
						initial_state           <=      STATE_IDLE_CTB_4X4;

                        if(curr_valid_in == 1'b1) begin
                            state                   <=      STATE_MAIN;
                            valid_out               <= 1'b0;
                        end

                        if(old_state == STATE_MAIN) begin
                            if((x_plus_ntbs_reg < min_top_available_range_in) && (curr_yin != 4)) begin
                                all_invalid_top_out <= 1'b1;
                            end

                            if((inner_y_plus_ntbs_reg < min_left_available_range_in) && (curr_xin != 4) && (curr_xin <= min_top_available_range_in)   ) begin
                                all_invalid_left_out <= 1'b1;
                            end
                        end
					end
					STATE_IDLE_CTB_8X8 : begin

						curr_ctb_x              <=      {curr_xin[PIXEL_ADDR_LENGTH - 1:3],3'd0};//curr_xin     >> 3;
						curr_ctb_y              <=      {curr_yin[PIXEL_ADDR_LENGTH - 1:3],3'd0};//curr_yin     >> 3;
						curr_inner_x            <=      curr_xin[2:0];
						curr_inner_y            <=      curr_yin[2:0];

                        x_plus_ntbs_reg         <=      curr_xin + (ntbs_in << 1);
                        inner_y_plus_ntbs_reg   <=      curr_yin[2:0] + (ntbs_in << 1);

						no_of_pixels_ctb        <=      8 - 1;
						initial_state           <=      STATE_IDLE_CTB_8X8;
						if(curr_valid_in == 1'b1) begin
                            state                   <=      STATE_MAIN;
                            valid_out               <= 1'b0;
                        end

                        if(old_state == STATE_MAIN) begin
                            if((x_plus_ntbs_reg < min_top_available_range_in) && (curr_yin != 4)) begin
                                all_invalid_top_out <= 1'b1;
                            end

                            if((inner_y_plus_ntbs_reg < min_left_available_range_in) && (curr_xin != 4) && (curr_xin <= min_top_available_range_in)) begin
                                all_invalid_left_out <= 1'b1;
                            end
                        end
					end
					STATE_IDLE_CTB_16X16 : begin

						curr_ctb_x              <=      {curr_xin[PIXEL_ADDR_LENGTH - 1:4],4'd0};//curr_xin     >> 4;
						curr_ctb_y              <=      {curr_yin[PIXEL_ADDR_LENGTH - 1:4],4'd0};//curr_yin     >> 4;
						curr_inner_x            <=      curr_xin[3:0];
						curr_inner_y            <=      curr_yin[3:0];

                        x_plus_ntbs_reg         <=      curr_xin + (ntbs_in << 1);
                        inner_y_plus_ntbs_reg   <=      curr_yin[3:0] + (ntbs_in << 1);

						no_of_pixels_ctb        <=      16 - 1;
						initial_state           <=      STATE_IDLE_CTB_16X16;

                        if(curr_valid_in == 1'b1) begin
                            state                   <=      STATE_MAIN;
                            valid_out               <= 1'b0;
                        end

                        if(old_state == STATE_MAIN) begin
                            if((x_plus_ntbs_reg < min_top_available_range_in) && (curr_yin != 4)) begin
                                all_invalid_top_out <= 1'b1;
                            end

                            if((inner_y_plus_ntbs_reg < min_left_available_range_in) && (curr_xin != 4) && (curr_xin <= min_top_available_range_in)) begin
                                all_invalid_left_out <= 1'b1;
                            end
                        end
					end
					STATE_IDLE_CTB_32X32 : begin

						curr_ctb_x              <=      {curr_xin[PIXEL_ADDR_LENGTH - 1:5],5'd0};//curr_xin     >> 5;
						curr_ctb_y              <=      {curr_yin[PIXEL_ADDR_LENGTH - 1:5],5'd0};//curr_yin     >> 5;
						curr_inner_x            <=      curr_xin[4:0];
						curr_inner_y            <=      curr_yin[4:0];

                        x_plus_ntbs_reg         <=      curr_xin + (ntbs_in << 1);
                        inner_y_plus_ntbs_reg   <=      curr_yin[4:0] + (ntbs_in << 1);

						no_of_pixels_ctb        <=      32 - 1;
						initial_state           <=      STATE_IDLE_CTB_32X32;

						if(curr_valid_in == 1'b1) begin
                            state                   <=      STATE_MAIN;
                            valid_out               <= 1'b0;
                        end

                        if(old_state == STATE_MAIN) begin
                            if((x_plus_ntbs_reg < min_top_available_range_in) && (curr_yin != 4)) begin
                                all_invalid_top_out <= 1'b1;
                            end

                            if((inner_y_plus_ntbs_reg < min_left_available_range_in) && (curr_xin != 4) && (curr_xin <= min_top_available_range_in)) begin
                                all_invalid_left_out <= 1'b1;
                            end
                        end
					end
					STATE_IDLE_CTB_64X64 : begin
						curr_ctb_x              <=      {curr_xin[PIXEL_ADDR_LENGTH - 1:6],6'd0};//curr_xin     >> 6;
						curr_ctb_y              <=      {curr_yin[PIXEL_ADDR_LENGTH - 1:6],6'd0}; //curr_yin     >> 6;
						curr_inner_x            <=      curr_xin[5:0];
						curr_inner_y            <=      curr_yin[5:0];

                        x_plus_ntbs_reg         <=      curr_xin + (ntbs_in << 1);
                        inner_y_plus_ntbs_reg   <=      curr_yin[5:0] + (ntbs_in << 1);

						no_of_pixels_ctb        <=      64 - 1;
						initial_state           <=      STATE_IDLE_CTB_64X64;

                        if(curr_valid_in == 1'b1) begin
                            state                   <=      STATE_MAIN;
                            valid_out               <= 1'b0;
                        end

                        if(old_state == STATE_MAIN) begin
                            if((x_plus_ntbs_reg < min_top_available_range_in) && (curr_yin != 4)) begin
                                all_invalid_top_out <= 1'b1;
                            end

                            if((inner_y_plus_ntbs_reg < min_left_available_range_in) && (curr_xin != 4) && (curr_xin <= min_top_available_range_in)) begin
                                all_invalid_left_out <= 1'b1;
                            end
                        end
					end
					STATE_MAIN : begin
						if(curr_yin == 0) begin
							all_invalid_top_out <= 1'b1;
						end
						else if(((curr_yin - 1) < tile_yc_in) ||  ((curr_yin - 1) < slice_y_in)) begin
							all_invalid_top_out <= 1'b1;
						end

						else if(curr_inner_y == 0)begin
							 validrange_top_out <= frame_width_in - 1'b1;//tile_xc_in + tile_width_in; // this covers frame width case too
                             all_invalid_top_out <= 1'b0;
						end
						else begin
							if(curr_ctb_x + valid_range_top_int >= frame_width_in) begin
								validrange_top_out  <= frame_width_in - 1'b1;
							end
							else begin
								validrange_top_out  <= curr_ctb_x + valid_range_top_int;
							end

                             all_invalid_top_out <= 1'b0;
						end

						if(curr_xin == 0) begin
							 all_invalid_left_out <= 1'b1;
						end
						else if(((curr_xin - 1) < tile_xc_in) ||  ((curr_xin - 1) < slice_x_in)) begin
							 all_invalid_left_out <= 1'b1;
						end

						else if(curr_inner_x == 0)begin
							 if(curr_ctb_y + no_of_pixels_ctb > frame_height_in) begin
								  validrange_left_out <= frame_height_in - 1'b1;
							 end
							 else begin
								  validrange_left_out <= curr_ctb_y + no_of_pixels_ctb;
							 end
                             all_invalid_left_out <= 1'b0;
						end
						else begin
							if( curr_ctb_y + valid_range_left_int >= frame_height_in) begin
								validrange_left_out  <= frame_height_in - 1'b1;
							end
							else begin
								validrange_left_out  <= curr_ctb_y + valid_range_left_int;
							end

                             all_invalid_left_out <= 1'b0;
						end

						// if( (curr_yin == 0) || (curr_xin == 0)) begin
							 // validrange_topleft_out <= 1'b0;
						// end
						// else if( ((curr_xin - 1) < (tile_xc_in)) ||  ((curr_xin - 1) < (slice_x_in)) || ((curr_yin - 1) < tile_yc_in) ||  ((curr_yin - 1) < slice_y_in) )begin
							 // validrange_topleft_out <= 1'b0;
						// end
						// else begin
							 // validrange_topleft_out <= 1'b1;
						// end

						valid_out <= 1'b1;
						state <= initial_state;
					end
					default : begin
						state <= initial_state;
					end
            endcase
        end
//        else begin
//            state <= initial_state;
//        end
    end


endmodule


