#-----------------------
#Creating new core.
ipx::create_core {xilinx.com} {ip} {dp_system} {1.0}
#-----------------------
set_property root_directory {} [ipx::current_core]
ipx::add_user_parameter Component_Name [ipx::current_core]
set_property {value_resolve_type} {user} [ipx::get_user_parameter  Component_Name [ipx::current_core]]
set_property {value_format} {string} [ipx::get_user_parameter  Component_Name [ipx::current_core]]
#-----------------------
# SUPORTED FAMILIES     
#-----------------------
set_property supported_families {{zynq} {Production}} [ipx::current_core]

#-----------------------
# OTHER ATTRIBUTES      
#-----------------------
set_property taxonomy {{/Packaged_BlockDiagram_Designs}} [ipx::current_core]
set_property company_url {http://www.xilinx.com} [ipx::current_core]
set_property description {dp_system} [ipx::current_core]
set_property display_name {dp_system} [ipx::current_core]

ipx::remove_all_port [ipx::current_core]
ipx::remove_all_file_group [ipx::current_core]
ipx::remove_all_bus_interface [ipx::current_core]

#-----------------------
# SYNTHESIS FILESET
#-----------------------
ipx::add_file_group {xilinx_verilogsynthesis} [ipx::current_core]
ipx::add_file dp_system_ooc.xdc [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file hw_handoff/dp_system.hwh [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_auto_pc_0/dp_system_auto_pc_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_axi_apb_bridge_0_0/dp_system_axi_apb_bridge_0_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_axi_iic_0_0/dp_system_axi_iic_0_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_axi_intc_0_0/dp_system_axi_intc_0_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_axi_timer_0_0/dp_system_axi_timer_0_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_c_shift_ram_0_0/dp_system_c_shift_ram_0_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_c_shift_ram_1_0/dp_system_c_shift_ram_1_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_clk_wiz_0_0/dp_system_clk_wiz_0_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_clk_wiz_0_1/dp_system_clk_wiz_0_1.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_clk_wiz_2_0/dp_system_clk_wiz_2_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_displayport_0_0/dp_system_displayport_0_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_proc_sys_reset_0_0/dp_system_proc_sys_reset_0_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_processing_system7_0_0/dp_system_processing_system7_0_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_util_vector_logic_0_0/dp_system_util_vector_logic_0_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_util_vector_logic_1_0/dp_system_util_vector_logic_1_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_vid_clkgen_0_0/dp_system_vid_clkgen_0_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_video_pat_gen_0_0/dp_system_video_pat_gen_0_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_video_tx_top_dp_0_0/dp_system_video_tx_top_dp_0_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_xbar_2/dp_system_xbar_2.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_xlconcat_0_0/dp_system_xlconcat_0_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_xlconstant_0_0/dp_system_xlconstant_0_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_xlconstant_0_1/dp_system_xlconstant_0_1.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file ip/dp_system_xlconstant_1_0/dp_system_xlconstant_1_0.xci [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
ipx::add_file hdl/dp_system.v [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]
set_property {model_name} {dp_system} [ipx::get_file_group xilinx_verilogsynthesis [ipx::current_core]]

#-----------------------
# SIMULATION FILESET
#-----------------------
ipx::add_file_group {xilinx_verilogbehavioralsimulation} [ipx::current_core]
ipx::add_file dp_system_ooc.xdc [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file hw_handoff/dp_system.hwh [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_auto_pc_0/dp_system_auto_pc_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_axi_apb_bridge_0_0/dp_system_axi_apb_bridge_0_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_axi_iic_0_0/dp_system_axi_iic_0_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_axi_intc_0_0/dp_system_axi_intc_0_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_axi_timer_0_0/dp_system_axi_timer_0_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_c_shift_ram_0_0/dp_system_c_shift_ram_0_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_c_shift_ram_1_0/dp_system_c_shift_ram_1_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_clk_wiz_0_0/dp_system_clk_wiz_0_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_clk_wiz_0_1/dp_system_clk_wiz_0_1.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_clk_wiz_2_0/dp_system_clk_wiz_2_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_displayport_0_0/dp_system_displayport_0_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_proc_sys_reset_0_0/dp_system_proc_sys_reset_0_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_processing_system7_0_0/dp_system_processing_system7_0_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_util_vector_logic_0_0/dp_system_util_vector_logic_0_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_util_vector_logic_1_0/dp_system_util_vector_logic_1_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_vid_clkgen_0_0/dp_system_vid_clkgen_0_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_video_pat_gen_0_0/dp_system_video_pat_gen_0_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_video_tx_top_dp_0_0/dp_system_video_tx_top_dp_0_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_xbar_2/dp_system_xbar_2.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_xlconcat_0_0/dp_system_xlconcat_0_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_xlconstant_0_0/dp_system_xlconstant_0_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_xlconstant_0_1/dp_system_xlconstant_0_1.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file ip/dp_system_xlconstant_1_0/dp_system_xlconstant_1_0.xci [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
ipx::add_file hdl/dp_system.v [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]
set_property {model_name} {dp_system} [ipx::get_file_group xilinx_verilogbehavioralsimulation [ipx::current_core]]

#-----------------------
# PORTS 
#-----------------------
ipx::add_ports_from_hdl [::ipx::current_core] -top_level_hdl_file ./hdl/dp_system.v -top_module_name dp_system

#-----------------------
# BUS INTERFACES 
#-----------------------
#------------------
#   Adding iic2inctc_irpt
#------------------
ipx::add_bus_interface {iic2inctc_irpt} [ipx::current_core]
set_property interface_mode {master} [ipx::get_bus_interface {iic2inctc_irpt} [ipx::current_core]]
set_property display_name {iic2inctc_irpt} [ipx::get_bus_interface {iic2inctc_irpt} [ipx::current_core]]

#   Adding Bus Type VNLV xilinx.com:interface:iic:1.0
set_property {bus_type_vlnv} {xilinx.com:interface:iic:1.0}  [ipx::get_bus_interface iic2inctc_irpt [ipx::current_core]]

#   Adding Abstraction VNLV xilinx.com:interface:iic_rtl:1.0
set_property {abstraction_type_vlnv} {xilinx.com:interface:iic_rtl:1.0}  [ipx::get_bus_interface iic2inctc_irpt [ipx::current_core]]

#   Adding PortMaps
set_property {physical_name} {iic2inctc_irpt_scl_i} [ipx::add_port_map {SCL_I}  [ipx::get_bus_interface {iic2inctc_irpt} [ipx::current_core]]]
set_property {physical_name} {iic2inctc_irpt_scl_o} [ipx::add_port_map {SCL_O}  [ipx::get_bus_interface {iic2inctc_irpt} [ipx::current_core]]]
set_property {physical_name} {iic2inctc_irpt_scl_t} [ipx::add_port_map {SCL_T}  [ipx::get_bus_interface {iic2inctc_irpt} [ipx::current_core]]]
set_property {physical_name} {iic2inctc_irpt_sda_i} [ipx::add_port_map {SDA_I}  [ipx::get_bus_interface {iic2inctc_irpt} [ipx::current_core]]]
set_property {physical_name} {iic2inctc_irpt_sda_o} [ipx::add_port_map {SDA_O}  [ipx::get_bus_interface {iic2inctc_irpt} [ipx::current_core]]]
set_property {physical_name} {iic2inctc_irpt_sda_t} [ipx::add_port_map {SDA_T}  [ipx::get_bus_interface {iic2inctc_irpt} [ipx::current_core]]]
#------------------
#------------------
#   Adding DDR
#------------------
ipx::add_bus_interface {DDR} [ipx::current_core]
set_property interface_mode {master} [ipx::get_bus_interface {DDR} [ipx::current_core]]
set_property display_name {DDR} [ipx::get_bus_interface {DDR} [ipx::current_core]]

#   Adding Bus Type VNLV xilinx.com:interface:ddrx:1.0
set_property {bus_type_vlnv} {xilinx.com:interface:ddrx:1.0}  [ipx::get_bus_interface DDR [ipx::current_core]]

#   Adding Abstraction VNLV xilinx.com:interface:ddrx_rtl:1.0
set_property {abstraction_type_vlnv} {xilinx.com:interface:ddrx_rtl:1.0}  [ipx::get_bus_interface DDR [ipx::current_core]]

#   Adding PortMaps
set_property {physical_name} {DDR_cas_n} [ipx::add_port_map {CAS_N}  [ipx::get_bus_interface {DDR} [ipx::current_core]]]
set_property {physical_name} {DDR_cke} [ipx::add_port_map {CKE}  [ipx::get_bus_interface {DDR} [ipx::current_core]]]
set_property {physical_name} {DDR_ck_n} [ipx::add_port_map {CK_N}  [ipx::get_bus_interface {DDR} [ipx::current_core]]]
set_property {physical_name} {DDR_ck_p} [ipx::add_port_map {CK_P}  [ipx::get_bus_interface {DDR} [ipx::current_core]]]
set_property {physical_name} {DDR_cs_n} [ipx::add_port_map {CS_N}  [ipx::get_bus_interface {DDR} [ipx::current_core]]]
set_property {physical_name} {DDR_reset_n} [ipx::add_port_map {RESET_N}  [ipx::get_bus_interface {DDR} [ipx::current_core]]]
set_property {physical_name} {DDR_odt} [ipx::add_port_map {ODT}  [ipx::get_bus_interface {DDR} [ipx::current_core]]]
set_property {physical_name} {DDR_ras_n} [ipx::add_port_map {RAS_N}  [ipx::get_bus_interface {DDR} [ipx::current_core]]]
set_property {physical_name} {DDR_we_n} [ipx::add_port_map {WE_N}  [ipx::get_bus_interface {DDR} [ipx::current_core]]]
set_property {physical_name} {DDR_ba} [ipx::add_port_map {BA}  [ipx::get_bus_interface {DDR} [ipx::current_core]]]
set_property {physical_name} {DDR_addr} [ipx::add_port_map {ADDR}  [ipx::get_bus_interface {DDR} [ipx::current_core]]]
set_property {physical_name} {DDR_dm} [ipx::add_port_map {DM}  [ipx::get_bus_interface {DDR} [ipx::current_core]]]
set_property {physical_name} {DDR_dq} [ipx::add_port_map {DQ}  [ipx::get_bus_interface {DDR} [ipx::current_core]]]
set_property {physical_name} {DDR_dqs_n} [ipx::add_port_map {DQS_N}  [ipx::get_bus_interface {DDR} [ipx::current_core]]]
set_property {physical_name} {DDR_dqs_p} [ipx::add_port_map {DQS_P}  [ipx::get_bus_interface {DDR} [ipx::current_core]]]
#------------------
#------------------
#   Adding FIXED_IO
#------------------
ipx::add_bus_interface {FIXED_IO} [ipx::current_core]
set_property interface_mode {master} [ipx::get_bus_interface {FIXED_IO} [ipx::current_core]]
set_property display_name {FIXED_IO} [ipx::get_bus_interface {FIXED_IO} [ipx::current_core]]

#   Adding Bus Type VNLV xilinx.com:display_processing_system7:fixedio:1.0
set_property {bus_type_vlnv} {xilinx.com:display_processing_system7:fixedio:1.0}  [ipx::get_bus_interface FIXED_IO [ipx::current_core]]

#   Adding Abstraction VNLV xilinx.com:display_processing_system7:fixedio_rtl:1.0
set_property {abstraction_type_vlnv} {xilinx.com:display_processing_system7:fixedio_rtl:1.0}  [ipx::get_bus_interface FIXED_IO [ipx::current_core]]

#   Adding PortMaps
set_property {physical_name} {FIXED_IO_mio} [ipx::add_port_map {MIO}  [ipx::get_bus_interface {FIXED_IO} [ipx::current_core]]]
set_property {physical_name} {FIXED_IO_ddr_vrn} [ipx::add_port_map {DDR_VRN}  [ipx::get_bus_interface {FIXED_IO} [ipx::current_core]]]
set_property {physical_name} {FIXED_IO_ddr_vrp} [ipx::add_port_map {DDR_VRP}  [ipx::get_bus_interface {FIXED_IO} [ipx::current_core]]]
set_property {physical_name} {FIXED_IO_ps_srstb} [ipx::add_port_map {PS_SRSTB}  [ipx::get_bus_interface {FIXED_IO} [ipx::current_core]]]
set_property {physical_name} {FIXED_IO_ps_clk} [ipx::add_port_map {PS_CLK}  [ipx::get_bus_interface {FIXED_IO} [ipx::current_core]]]
set_property {physical_name} {FIXED_IO_ps_porb} [ipx::add_port_map {PS_PORB}  [ipx::get_bus_interface {FIXED_IO} [ipx::current_core]]]
#------------------
#------------------
#   Adding SYS_CLK
#------------------
ipx::add_bus_interface {SYS_CLK} [ipx::current_core]
set_property interface_mode {slave} [ipx::get_bus_interface {SYS_CLK} [ipx::current_core]]
set_property display_name {SYS_CLK} [ipx::get_bus_interface {SYS_CLK} [ipx::current_core]]

#   Adding Bus Type VNLV xilinx.com:interface:diff_clock:1.0
set_property {bus_type_vlnv} {xilinx.com:interface:diff_clock:1.0}  [ipx::get_bus_interface SYS_CLK [ipx::current_core]]

#   Adding Abstraction VNLV xilinx.com:interface:diff_clock_rtl:1.0
set_property {abstraction_type_vlnv} {xilinx.com:interface:diff_clock_rtl:1.0}  [ipx::get_bus_interface SYS_CLK [ipx::current_core]]

#   Adding PortMaps
set_property {physical_name} {SYS_CLK_clk_n} [ipx::add_port_map {CLK_N}  [ipx::get_bus_interface {SYS_CLK} [ipx::current_core]]]
set_property {physical_name} {SYS_CLK_clk_p} [ipx::add_port_map {CLK_P}  [ipx::get_bus_interface {SYS_CLK} [ipx::current_core]]]
#------------------
#   Adding Parameters
ipx::add_bus_parameter {FREQ_HZ}  [ipx::get_bus_interface SYS_CLK [ipx::current_core]]
set_property {value} {200000000} [ipx::get_bus_parameter {FREQ_HZ}   [ipx::get_bus_interface SYS_CLK [ipx::current_core]]]

#------------------
#   Adding dp_mainlink
#------------------
ipx::add_bus_interface {dp_mainlink} [ipx::current_core]
set_property interface_mode {master} [ipx::get_bus_interface {dp_mainlink} [ipx::current_core]]
set_property display_name {dp_mainlink} [ipx::get_bus_interface {dp_mainlink} [ipx::current_core]]

#   Adding Bus Type VNLV xilinx.com:user:dp_main_lnk:1.0
set_property {bus_type_vlnv} {xilinx.com:user:dp_main_lnk:1.0}  [ipx::get_bus_interface dp_mainlink [ipx::current_core]]

#   Adding Abstraction VNLV xilinx.com:user:dp_main_lnk_rtl:1.0
set_property {abstraction_type_vlnv} {xilinx.com:user:dp_main_lnk_rtl:1.0}  [ipx::get_bus_interface dp_mainlink [ipx::current_core]]

#   Adding PortMaps
set_property {physical_name} {dp_mainlink_lnk_clk} [ipx::add_port_map {LNK_CLK}  [ipx::get_bus_interface {dp_mainlink} [ipx::current_core]]]
set_property {physical_name} {dp_mainlink_lnk_clk_n} [ipx::add_port_map {LNK_CLK_N}  [ipx::get_bus_interface {dp_mainlink} [ipx::current_core]]]
set_property {physical_name} {dp_mainlink_lnk_clk_p} [ipx::add_port_map {LNK_CLK_P}  [ipx::get_bus_interface {dp_mainlink} [ipx::current_core]]]
set_property {physical_name} {dp_mainlink_lnk_tx_lane_n} [ipx::add_port_map {LNK_TX_LANE_N}  [ipx::get_bus_interface {dp_mainlink} [ipx::current_core]]]
set_property {physical_name} {dp_mainlink_lnk_tx_lane_p} [ipx::add_port_map {LNK_TX_LANE_P}  [ipx::get_bus_interface {dp_mainlink} [ipx::current_core]]]
#------------------
#------------------
#   Adding dp_fifo_read
#------------------
ipx::add_bus_interface {dp_fifo_read} [ipx::current_core]
set_property interface_mode {master} [ipx::get_bus_interface {dp_fifo_read} [ipx::current_core]]
set_property display_name {dp_fifo_read} [ipx::get_bus_interface {dp_fifo_read} [ipx::current_core]]

#   Adding Bus Type VNLV xilinx.com:interface:fifo_read:1.0
set_property {bus_type_vlnv} {xilinx.com:interface:fifo_read:1.0}  [ipx::get_bus_interface dp_fifo_read [ipx::current_core]]

#   Adding Abstraction VNLV xilinx.com:interface:fifo_read_rtl:1.0
set_property {abstraction_type_vlnv} {xilinx.com:interface:fifo_read_rtl:1.0}  [ipx::get_bus_interface dp_fifo_read [ipx::current_core]]

#   Adding PortMaps
set_property {physical_name} {dp_fifo_read_rd_en} [ipx::add_port_map {RD_EN}  [ipx::get_bus_interface {dp_fifo_read} [ipx::current_core]]]
set_property {physical_name} {dp_fifo_read_rd_data} [ipx::add_port_map {RD_DATA}  [ipx::get_bus_interface {dp_fifo_read} [ipx::current_core]]]
set_property {physical_name} {dp_fifo_read_empty} [ipx::add_port_map {EMPTY}  [ipx::get_bus_interface {dp_fifo_read} [ipx::current_core]]]
#------------------
#------------------
#   Adding CLK.clk_out2
#------------------
ipx::add_bus_interface {CLK.clk_out2} [ipx::current_core]
set_property display_name {Clk} [ipx::get_bus_interface {CLK.clk_out2} [ipx::current_core]]
set_property interface_mode {slave} [ipx::get_bus_interface {CLK.clk_out2} [ipx::current_core]]

#   Adding Bus Type VNLV xilinx.com:signal:clock:1.0
set_property {bus_type_vlnv} {xilinx.com:signal:clock:1.0}  [ipx::get_bus_interface CLK.clk_out2 [ipx::current_core]]

#   Adding Abstraction VNLV xilinx.com:signal:clock_rtl:1.0
set_property {abstraction_type_vlnv} {xilinx.com:signal:clock_rtl:1.0}  [ipx::get_bus_interface CLK.clk_out2 [ipx::current_core]]

#   Adding PortMap
set_property {physical_name} {clk_out2} [ipx::add_port_map {CLK}  [ipx::get_bus_interface {CLK.clk_out2} [ipx::current_core]]]
#   Adding Parameters
ipx::add_bus_parameter {FREQ_HZ}  [ipx::get_bus_interface CLK.clk_out2 [ipx::current_core]]
set_property {value} {135000000} [ipx::get_bus_parameter {FREQ_HZ}   [ipx::get_bus_interface CLK.clk_out2 [ipx::current_core]]]

ipx::add_bus_parameter {PHASE}  [ipx::get_bus_interface CLK.clk_out2 [ipx::current_core]]
set_property {value} {0.0} [ipx::get_bus_parameter {PHASE}   [ipx::get_bus_interface CLK.clk_out2 [ipx::current_core]]]


#-----------------------
# SAVE CORE TO REPOS
#-----------------------
ipx::create_default_gui_files [ipx::current_core]
ipx::save_core [ipx::current_core]
ipx::check_integrity  [ipx::current_core]
update_ip_catalog
