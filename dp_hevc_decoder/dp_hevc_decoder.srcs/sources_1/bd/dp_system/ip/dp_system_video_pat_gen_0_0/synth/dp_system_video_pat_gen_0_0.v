// (c) Copyright 1995-2014 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:user:video_pat_gen:1.0
// IP Revision: 5

(* X_CORE_INFO = "video_pat_gen,Vivado 2014.2" *)
(* CHECK_LICENSE_TYPE = "dp_system_video_pat_gen_0_0,video_pat_gen,{}" *)
(* CORE_GENERATION_INFO = "dp_system_video_pat_gen_0_0,video_pat_gen,{x_ipProduct=Vivado 2014.2,x_ipVendor=xilinx.com,x_ipLibrary=user,x_ipName=video_pat_gen,x_ipVersion=1.0,x_ipCoreRevision=5,x_ipLanguage=VERILOG}" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module dp_system_video_pat_gen_0_0 (
  apb_clk,
  apb_reset,
  apb_select,
  apb_enable,
  apb_write,
  apb_pready,
  apb_pslverr,
  apb_addr,
  apb_wdata,
  apb_rdata,
  vid_clk_sel,
  vid_clk_M,
  vid_clk_D,
  vid_clk_prog,
  sw_vid_reset,
  run_pattern,
  vid_clk,
  vid_reset,
  vid_enable,
  vid_vsync,
  vid_hsync,
  vid_oddeven,
  vid_pixel0,
  vid_pixel1,
  patt_done
);

(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 apb_signal_clock CLK" *)
input wire apb_clk;
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 apb_signal_reset RST" *)
input wire apb_reset;
(* X_INTERFACE_INFO = "xilinx.com:interface:apb:1.0 vid_pat_apb_inf PSEL" *)
input wire apb_select;
(* X_INTERFACE_INFO = "xilinx.com:interface:apb:1.0 vid_pat_apb_inf PENABLE" *)
input wire apb_enable;
(* X_INTERFACE_INFO = "xilinx.com:interface:apb:1.0 vid_pat_apb_inf PWRITE" *)
input wire apb_write;
(* X_INTERFACE_INFO = "xilinx.com:interface:apb:1.0 vid_pat_apb_inf PREADY" *)
output wire apb_pready;
(* X_INTERFACE_INFO = "xilinx.com:interface:apb:1.0 vid_pat_apb_inf PSLVERR" *)
output wire apb_pslverr;
(* X_INTERFACE_INFO = "xilinx.com:interface:apb:1.0 vid_pat_apb_inf PADDR" *)
input wire [11 : 0] apb_addr;
(* X_INTERFACE_INFO = "xilinx.com:interface:apb:1.0 vid_pat_apb_inf PWDATA" *)
input wire [31 : 0] apb_wdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:apb:1.0 vid_pat_apb_inf PRDATA" *)
output wire [31 : 0] apb_rdata;
output wire vid_clk_sel;
output wire [7 : 0] vid_clk_M;
output wire [15 : 0] vid_clk_D;
output wire vid_clk_prog;
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 sw_vid_signal_reset RST" *)
output wire sw_vid_reset;
input wire run_pattern;
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 vid_signal_clock CLK" *)
input wire vid_clk;
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 vid_signal_reset RST" *)
input wire vid_reset;
output wire vid_enable;
output wire vid_vsync;
output wire vid_hsync;
output wire vid_oddeven;
output wire [47 : 0] vid_pixel0;
output wire [47 : 0] vid_pixel1;
output wire patt_done;

  video_pat_gen inst (
    .apb_clk(apb_clk),
    .apb_reset(apb_reset),
    .apb_select(apb_select),
    .apb_enable(apb_enable),
    .apb_write(apb_write),
    .apb_pready(apb_pready),
    .apb_pslverr(apb_pslverr),
    .apb_addr(apb_addr),
    .apb_wdata(apb_wdata),
    .apb_rdata(apb_rdata),
    .vid_clk_sel(vid_clk_sel),
    .vid_clk_M(vid_clk_M),
    .vid_clk_D(vid_clk_D),
    .vid_clk_prog(vid_clk_prog),
    .sw_vid_reset(sw_vid_reset),
    .run_pattern(run_pattern),
    .vid_clk(vid_clk),
    .vid_reset(vid_reset),
    .vid_enable(vid_enable),
    .vid_vsync(vid_vsync),
    .vid_hsync(vid_hsync),
    .vid_oddeven(vid_oddeven),
    .vid_pixel0(vid_pixel0),
    .vid_pixel1(vid_pixel1),
    .patt_done(patt_done)
  );
endmodule
