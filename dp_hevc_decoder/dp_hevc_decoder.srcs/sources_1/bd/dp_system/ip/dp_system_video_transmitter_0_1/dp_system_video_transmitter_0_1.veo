// (c) Copyright 1995-2014 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.

// IP VLNV: xilinx.com:user:video_transmitter:1.1
// IP Revision: 7

// The following must be inserted into your Verilog file for this
// core to be instantiated. Change the instance name and port connections
// (in parentheses) to your own signal names.

//----------- Begin Cut here for INSTANTIATION Template ---// INST_TAG
dp_system_video_transmitter_0_1 your_instance_name (
  .start(start),              // input wire start
  .vid_clk(vid_clk),          // input wire vid_clk
  .vid_run_clk(vid_run_clk),  // input wire vid_run_clk
  .reset(reset),              // input wire reset
  .config_done(config_done),  // input wire config_done
  .vid_clk_en(vid_clk_en),    // output wire vid_clk_en
  .vid_hsync(vid_hsync),      // output wire vid_hsync
  .vid_vsync(vid_vsync),      // output wire vid_vsync
  .vid_enable(vid_enable),    // output wire vid_enable
  .vid_oddeven(vid_oddeven),  // output wire vid_oddeven
  .vid_pixel0(vid_pixel0),    // output wire [47 : 0] vid_pixel0
  .vid_pixel1(vid_pixel1),    // output wire [47 : 0] vid_pixel1
  .vid_pixel2(vid_pixel2),    // output wire [47 : 0] vid_pixel2
  .vid_pixel3(vid_pixel3),    // output wire [47 : 0] vid_pixel3
  .pclk_count(pclk_count),    // output wire [23 : 0] pclk_count
  .hsync_count(hsync_count),  // output wire [12 : 0] hsync_count
  .DE_count(DE_count)        // output wire [12 : 0] DE_count
);
// INST_TAG_END ------ End INSTANTIATION Template ---------

// You must compile the wrapper file dp_system_video_transmitter_0_1.v when simulating
// the core, dp_system_video_transmitter_0_1. When compiling the wrapper file, be sure to
// reference the Verilog simulation library.

