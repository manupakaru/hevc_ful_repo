// (c) Copyright 2009 - 2013 Xilinx, Inc. All rights reserved. 
//
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
//
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
//
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
//
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.


`ifndef __DPORT_RX_DPCD_DEFS__
`define __DPORT_RX_DPCD_DEFS__

// ----------------------------------------------
//  Display Port Configuration Data Registers
// ----------------------------------------------
`define kDPORT_RX_DPCD_REG_COUNT     51
`define tDPORT_RX_DPCD_REGS      [396:0]

`define  kDPCD_SET_POWER_STATE_EXT              396    // 68*8 +: 1
`define  kDPCD_I2C_SPEED_CTRL               395:390    // 67*8 +: 6
`define kDPCD_DOWN_REPLY_READY                  389    // 66*8 +: 1
`define kDPCD_UPSTREAM_IS_SRC                   388    // 65*8 +: 1
`define kDPCD_UP_REQ_EN                         387    // 64*8 +: 1
`define kDPCD_MST_EN                            386    // 63*8 +: 1
`define  kDPCD_GUID_B15                     385:378    // 62*8 +: 8
`define  kDPCD_GUID_B14                     377:370    // 61*8 +: 8
`define  kDPCD_GUID_B13                     369:362    // 60*8 +: 8
`define  kDPCD_GUID_B12                     361:354    // 59*8 +: 8
`define  kDPCD_GUID_B11                     353:346    // 58*8 +: 8
`define  kDPCD_GUID_B10                     345:338    // 57*8 +: 8
`define  kDPCD_GUID_B9                      337:330    // 56*8 +: 8
`define  kDPCD_GUID_B8                      329:322    // 55*8 +: 8
`define  kDPCD_GUID_B7                      321:314    // 54*8 +: 8
`define  kDPCD_GUID_B6                      313:306    // 53*8 +: 8
`define  kDPCD_GUID_B5                      305:298    // 52*8 +: 8
`define  kDPCD_GUID_B4                      297:290    // 51*8 +: 8
`define  kDPCD_GUID_B3                      289:282    // 50*8 +: 8
`define  kDPCD_GUID_B2                      281:274    // 49*8 +: 8
`define  kDPCD_GUID_B1                      273:266    // 48*8 +: 8
`define  kDPCD_GUID_B0                      265:258    // 47*8 +: 8
`define  kDPCD_SINK_SPECIFIC_IRQ                257    // 46*8 +: 1
`define  kDPCD_LINK_QUAL_LANE3_SET          256:254    // 45*8 +: 3
`define  kDPCD_LINK_QUAL_LANE2_SET          253:251    // 44*8 +: 3
`define  kDPCD_LINK_QUAL_LANE1_SET          250:248    // 43*8 +: 3
`define  kDPCD_LINK_QUAL_LANE0_SET          247:245    // 42*8 +: 3
`define  kDPCD_TPS3_SUPPORT                     244    // 41*8 +: 1
`define  kDPCD_SYM_ERR_CNT_LANE3_H          243:236    // 40*8 +: 8
`define  kDPCD_SYM_ERR_CNT_LANE3_L          235:228    // 39*8 +: 8
`define  kDPCD_SYM_ERR_CNT_LANE2_H          227:220    // 38*8 +: 8
`define  kDPCD_SYM_ERR_CNT_LANE2_L          219:212    // 37*8 +: 8
`define  kDPCD_SYM_ERR_CNT_LANE1_H          211:204    // 36*8 +: 8
`define  kDPCD_SYM_ERR_CNT_LANE1_L          203:196    // 35*8 +: 8
`define  kDPCD_SYM_ERR_CNT_LANE0_H          195:188    // 34*8 +: 8
`define  kDPCD_SYM_ERR_CNT_LANE0_L          187:180    // 33*8 +: 8
`define  kDPCD_REMOTE_COMMAND_NEW               179    // 32*8 +: 1
`define  kDPCD_AKSV_INTERRUPT                   178    // 31*8 +: 1
`define  kDPCD_AN_BYTE_7                    177:170    // 30*8 +: 8
`define  kDPCD_AN_BYTE_6                    169:162    // 29*8 +: 8
`define  kDPCD_AN_BYTE_5                    161:154    // 28*8 +: 8
`define  kDPCD_AN_BYTE_4                    153:146    // 27*8 +: 8
`define  kDPCD_AN_BYTE_3                    145:138    // 26*8 +: 8
`define  kDPCD_AN_BYTE_2                    137:130    // 25*8 +: 8
`define  kDPCD_AN_BYTE_1                    129:122    // 24*8 +: 8
`define  kDPCD_AN_BYTE_0                    121:114    // 23*8 +: 8
`define  kDPCD_AKSV_BYTE_4                  113:106    // 22*8 +: 8
`define  kDPCD_AKSV_BYTE_3                  105:98     // 21*8 +: 8
`define  kDPCD_AKSV_BYTE_2                   97:90     // 20*8 +: 8
`define  kDPCD_AKSV_BYTE_1                   89:82     // 19*8 +: 8
`define  kDPCD_AKSV_BYTE_0                   81:74     // 18*8 +: 8
`define  kDPCD_SOURCE_OUI_BYTE_2             73:66     // 17*8 +: 8
`define  kDPCD_SOURCE_OUI_BYTE_1             65:58     // 16*8 +: 8
`define  kDPCD_SOURCE_OUI_BYTE_0             57:50     // 15*8 +: 8
`define  kDPCD_SET_POWER_STATE               49:48     // 14*8 +: 2
`define  kDPCD_MAIN_LINK_CHANNEL_CODING_SET     47     // 13*8 +: 1
`define  kDPCD_DOWNSPREAD_CTRL                  46     // 12*8 +: 1
`define  kDPCD_TRAINING_LANE3_SET            45:40     // 11*8 +: 6
`define  kDPCD_TRAINING_LANE2_SET            39:34     // 10*8 +: 6
`define  kDPCD_TRAINING_LANE1_SET            33:28     // 9 *8 +: 6
`define  kDPCD_TRAINING_LANE0_SET            27:22     // 8 *8 +: 6
`define  kDPCD_SYMBOL_ERROR_COUNT_SEL        21:20     // 7 *8 +: 2
`define  kDPCD_SCRAMBLING_DISABLE               19     // 6 *8 +: 1
`define  kDPCD_RECOVERED_CLOCK_OUT_EN           18     // 5 *8 +: 1
`define  kDPCD_LINK_QUAL_PATTERN_SET         17:16     // 4 *8 +: 2
`define  kDPCD_TRAINING_PATTERN_SET          15:14     // 3 *8 +: 2
`define  kDPCD_ENHANCED_FRAME_EN                13     // 2 *8 +: 1
`define  kDPCD_LANE_COUNT_SET                12:8      // 1 *8 +: 5
`define  kDPCD_LINK_BW_SET                    7:0      // 0 *8 +: 8

// ----------------------------------------------
//  constants (localparam)
// ----------------------------------------------
`define kDPCD_REGS_RESET {   \
/* kDPCD_SET_POWER_STATE_EXT             */  1'b0,   \
/* kDPCD_I2C_SPEED_CTRL             */  6'b0,   \
/* kDPCD_DOWN_REPLY_READY             */  1'b0,   \
/* kDPCD_UPSTREAM_IS_SRC              */  1'b0,   \
/* kDPCD_UP_REQ_EN                    */  1'b0,   \
/* kDPCD_MST_EN                       */  1'b0,   \
/* kDPCD_GUID_B15                      */  8'h0,   \
/* kDPCD_GUID_B14                      */  8'h0,   \
/* kDPCD_GUID_B13                      */  8'h0,   \
/* kDPCD_GUID_B12                      */  8'h0,   \
/* kDPCD_GUID_B11                      */  8'h0,   \
/* kDPCD_GUID_B10                      */  8'h0,   \
/* kDPCD_GUID_B9                      */  8'h0,   \
/* kDPCD_GUID_B8                      */  8'h0,   \
/* kDPCD_GUID_B7                      */  8'h0,   \
/* kDPCD_GUID_B6                      */  8'h0,   \
/* kDPCD_GUID_B5                      */  8'h0,   \
/* kDPCD_GUID_B4                      */  8'h0,   \
/* kDPCD_GUID_B3                      */  8'h0,   \
/* kDPCD_GUID_B2                      */  8'h0,   \
/* kDPCD_GUID_B1                      */  8'h0,   \
/* kDPCD_GUID_B0                      */  8'h0,   \
/* kDPCD_SINK_SPECIFIC_IRQ            */  1'h 0,  \
/* kDPCD_LINK_QUAL_LANE3_SET          */  3'h 0,  \
/* kDPCD_LINK_QUAL_LANE2_SET          */  3'h 0,  \
/* kDPCD_LINK_QUAL_LANE1_SET          */  3'h 0,  \
/* kDPCD_LINK_QUAL_LANE0_SET          */  3'h 0,  \
/* kDPCD_TPS3_SUPPORT                 */  1'h 0,  \
/* kDPCD_SYM_ERR_CNT_LANE3_H          */  8'h 0,  \
/* kDPCD_SYM_ERR_CNT_LANE3_L          */  8'h 0,  \
/* kDPCD_SYM_ERR_CNT_LANE2_H          */  8'h 0,  \
/* kDPCD_SYM_ERR_CNT_LANE2_L          */  8'h 0,  \
/* kDPCD_SYM_ERR_CNT_LANE1_H          */  8'h 0,  \
/* kDPCD_SYM_ERR_CNT_LANE1_L          */  8'h 0,  \
/* kDPCD_SYM_ERR_CNT_LANE0_H          */  8'h 0,  \
/* kDPCD_SYM_ERR_CNT_LANE0_L          */  8'h 0,  \
/* kDPCD_REMOTE_COMMAND_NEW           */  1'h 0,  \
/* kDPCD_AKSV_INTERRUPT               */  1'h 0,  \
/* kDPCD_AN_BYTE_7                    */  8'h 0,  \
/* kDPCD_AN_BYTE_6                    */  8'h 0,  \
/* kDPCD_AN_BYTE_5                    */  8'h 0,  \
/* kDPCD_AN_BYTE_4                    */  8'h 0,  \
/* kDPCD_AN_BYTE_3                    */  8'h 0,  \
/* kDPCD_AN_BYTE_2                    */  8'h 0,  \
/* kDPCD_AN_BYTE_1                    */  8'h 0,  \
/* kDPCD_AN_BYTE_0                    */  8'h 0,  \
/* kDPCD_AKSV_BYTE_4                  */  8'h 0,  \
/* kDPCD_AKSV_BYTE_3                  */  8'h 0,  \
/* kDPCD_AKSV_BYTE_2                  */  8'h 0,  \
/* kDPCD_AKSV_BYTE_1                  */  8'h 0,  \
/* kDPCD_AKSV_BYTE_0                  */  8'h 0,  \
/* kDPCD_SOURCE_OUI_BYTE_2            */  8'h 0,  \
/* kDPCD_SOURCE_OUI_BYTE_1            */  8'h 0,  \
/* kDPCD_SOURCE_OUI_BYTE_0            */  8'h 0,  \
/* kDPCD_SET_POWER_STATE              */  2'h 1,  \
/* kDPCD_MAIN_LINK_CHANNEL_CODING_SET */  1'h 1,  \
/* kDPCD_DOWNSPREAD_CTRL              */  1'h 0,  \
/* kDPCD_TRAINING_LANE3_SET           */  6'h 0,  \
/* kDPCD_TRAINING_LANE2_SET           */  6'h 0,  \
/* kDPCD_TRAINING_LANE1_SET           */  6'h 0,  \
/* kDPCD_TRAINING_LANE0_SET           */  6'h 0,  \
/* kDPCD_SYMBOL_ERROR_COUNT_SEL       */  2'h 0,  \
/* kDPCD_SCRAMBLING_DISABLE           */  1'h 1,  \
/* kDPCD_RECOVERED_CLOCK_OUT_EN       */  1'h 0,  \
/* kDPCD_LINK_QUAL_PATTERN_SET        */  2'h 0,  \
/* kDPCD_TRAINING_PATTERN_SET         */  2'h 0,  \
/* kDPCD_ENHANCED_FRAME_EN            */  1'h 0,  \
/* kDPCD_LANE_COUNT_SET               */  5'h 0,  \
/* kDPCD_LINK_BW_SET                  */  8'h 0   \
}

`endif
