//Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2014.2 (win64) Build 932637 Wed Jun 11 13:33:10 MDT 2014
//Date        : Thu Jan 01 03:30:01 2015
//Host        : GCLAP4 running 64-bit major release  (build 9200)
//Command     : generate_target dp_system.bd
//Design      : dp_system
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module axi4_lite_peripherals_imp_8FS6LK
   (APB_M_paddr,
    APB_M_penable,
    APB_M_prdata,
    APB_M_pready,
    APB_M_psel,
    APB_M_pslverr,
    APB_M_pwdata,
    APB_M_pwrite,
    IIC_scl_i,
    IIC_scl_o,
    IIC_scl_t,
    IIC_sda_i,
    IIC_sda_o,
    IIC_sda_t,
    In1,
    M03_AXI_araddr,
    M03_AXI_arprot,
    M03_AXI_arready,
    M03_AXI_arvalid,
    M03_AXI_awaddr,
    M03_AXI_awprot,
    M03_AXI_awready,
    M03_AXI_awvalid,
    M03_AXI_bready,
    M03_AXI_bresp,
    M03_AXI_bvalid,
    M03_AXI_rdata,
    M03_AXI_rready,
    M03_AXI_rresp,
    M03_AXI_rvalid,
    M03_AXI_wdata,
    M03_AXI_wready,
    M03_AXI_wstrb,
    M03_AXI_wvalid,
    S00_AXI_araddr,
    S00_AXI_arburst,
    S00_AXI_arcache,
    S00_AXI_arid,
    S00_AXI_arlen,
    S00_AXI_arlock,
    S00_AXI_arprot,
    S00_AXI_arqos,
    S00_AXI_arready,
    S00_AXI_arsize,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awburst,
    S00_AXI_awcache,
    S00_AXI_awid,
    S00_AXI_awlen,
    S00_AXI_awlock,
    S00_AXI_awprot,
    S00_AXI_awqos,
    S00_AXI_awready,
    S00_AXI_awsize,
    S00_AXI_awvalid,
    S00_AXI_bid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rid,
    S00_AXI_rlast,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wid,
    S00_AXI_wlast,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid,
    dcm_locked,
    ext_reset_in,
    gpo,
    peripheral_aresetn,
    peripheral_reset,
    s_axi_aclk);
  output [11:0]APB_M_paddr;
  output APB_M_penable;
  input [31:0]APB_M_prdata;
  input APB_M_pready;
  output APB_M_psel;
  input APB_M_pslverr;
  output [31:0]APB_M_pwdata;
  output APB_M_pwrite;
  input IIC_scl_i;
  output IIC_scl_o;
  output IIC_scl_t;
  input IIC_sda_i;
  output IIC_sda_o;
  output IIC_sda_t;
  input [0:0]In1;
  output [31:0]M03_AXI_araddr;
  output [2:0]M03_AXI_arprot;
  input M03_AXI_arready;
  output M03_AXI_arvalid;
  output [31:0]M03_AXI_awaddr;
  output [2:0]M03_AXI_awprot;
  input M03_AXI_awready;
  output M03_AXI_awvalid;
  output M03_AXI_bready;
  input [1:0]M03_AXI_bresp;
  input M03_AXI_bvalid;
  input [31:0]M03_AXI_rdata;
  output M03_AXI_rready;
  input [1:0]M03_AXI_rresp;
  input M03_AXI_rvalid;
  output [31:0]M03_AXI_wdata;
  input M03_AXI_wready;
  output [3:0]M03_AXI_wstrb;
  output M03_AXI_wvalid;
  input [31:0]S00_AXI_araddr;
  input [1:0]S00_AXI_arburst;
  input [3:0]S00_AXI_arcache;
  input [11:0]S00_AXI_arid;
  input [3:0]S00_AXI_arlen;
  input [1:0]S00_AXI_arlock;
  input [2:0]S00_AXI_arprot;
  input [3:0]S00_AXI_arqos;
  output S00_AXI_arready;
  input [2:0]S00_AXI_arsize;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [1:0]S00_AXI_awburst;
  input [3:0]S00_AXI_awcache;
  input [11:0]S00_AXI_awid;
  input [3:0]S00_AXI_awlen;
  input [1:0]S00_AXI_awlock;
  input [2:0]S00_AXI_awprot;
  input [3:0]S00_AXI_awqos;
  output S00_AXI_awready;
  input [2:0]S00_AXI_awsize;
  input S00_AXI_awvalid;
  output [11:0]S00_AXI_bid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  output [11:0]S00_AXI_rid;
  output S00_AXI_rlast;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  input [11:0]S00_AXI_wid;
  input S00_AXI_wlast;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;
  input dcm_locked;
  input ext_reset_in;
  output [1:0]gpo;
  output [0:0]peripheral_aresetn;
  output [0:0]peripheral_reset;
  input s_axi_aclk;

  wire [0:0]ARESETN_1;
  wire [31:0]Conn1_ARADDR;
  wire [1:0]Conn1_ARBURST;
  wire [3:0]Conn1_ARCACHE;
  wire [11:0]Conn1_ARID;
  wire [3:0]Conn1_ARLEN;
  wire [1:0]Conn1_ARLOCK;
  wire [2:0]Conn1_ARPROT;
  wire [3:0]Conn1_ARQOS;
  wire Conn1_ARREADY;
  wire [2:0]Conn1_ARSIZE;
  wire Conn1_ARVALID;
  wire [31:0]Conn1_AWADDR;
  wire [1:0]Conn1_AWBURST;
  wire [3:0]Conn1_AWCACHE;
  wire [11:0]Conn1_AWID;
  wire [3:0]Conn1_AWLEN;
  wire [1:0]Conn1_AWLOCK;
  wire [2:0]Conn1_AWPROT;
  wire [3:0]Conn1_AWQOS;
  wire Conn1_AWREADY;
  wire [2:0]Conn1_AWSIZE;
  wire Conn1_AWVALID;
  wire [11:0]Conn1_BID;
  wire Conn1_BREADY;
  wire [1:0]Conn1_BRESP;
  wire Conn1_BVALID;
  wire [31:0]Conn1_RDATA;
  wire [11:0]Conn1_RID;
  wire Conn1_RLAST;
  wire Conn1_RREADY;
  wire [1:0]Conn1_RRESP;
  wire Conn1_RVALID;
  wire [31:0]Conn1_WDATA;
  wire [11:0]Conn1_WID;
  wire Conn1_WLAST;
  wire Conn1_WREADY;
  wire [3:0]Conn1_WSTRB;
  wire Conn1_WVALID;
  wire [31:0]Conn3_ARADDR;
  wire [2:0]Conn3_ARPROT;
  wire Conn3_ARREADY;
  wire Conn3_ARVALID;
  wire [31:0]Conn3_AWADDR;
  wire [2:0]Conn3_AWPROT;
  wire Conn3_AWREADY;
  wire Conn3_AWVALID;
  wire Conn3_BREADY;
  wire [1:0]Conn3_BRESP;
  wire Conn3_BVALID;
  wire [31:0]Conn3_RDATA;
  wire Conn3_RREADY;
  wire [1:0]Conn3_RRESP;
  wire Conn3_RVALID;
  wire [31:0]Conn3_WDATA;
  wire Conn3_WREADY;
  wire [3:0]Conn3_WSTRB;
  wire Conn3_WVALID;
  wire GND_1;
  wire [0:0]In1_1;
  wire VCC_1;
  wire [31:0]axi_apb_bridge_1_APB_M_PADDR;
  wire axi_apb_bridge_1_APB_M_PENABLE;
  wire [31:0]axi_apb_bridge_1_APB_M_PRDATA;
  wire axi_apb_bridge_1_APB_M_PREADY;
  wire [0:0]axi_apb_bridge_1_APB_M_PSEL;
  wire axi_apb_bridge_1_APB_M_PSLVERR;
  wire [31:0]axi_apb_bridge_1_APB_M_PWDATA;
  wire axi_apb_bridge_1_APB_M_PWRITE;
  wire axi_iic_1_IIC_SCL_I;
  wire axi_iic_1_IIC_SCL_O;
  wire axi_iic_1_IIC_SCL_T;
  wire axi_iic_1_IIC_SDA_I;
  wire axi_iic_1_IIC_SDA_O;
  wire axi_iic_1_IIC_SDA_T;
  wire [1:0]axi_iic_1_gpo;
  wire axi_iic_1_iic2intc_irpt;
  wire [8:0]axi_interconnect_1_M00_AXI_ARADDR;
  wire axi_interconnect_1_M00_AXI_ARREADY;
  wire [0:0]axi_interconnect_1_M00_AXI_ARVALID;
  wire [8:0]axi_interconnect_1_M00_AXI_AWADDR;
  wire axi_interconnect_1_M00_AXI_AWREADY;
  wire [0:0]axi_interconnect_1_M00_AXI_AWVALID;
  wire [0:0]axi_interconnect_1_M00_AXI_BREADY;
  wire [1:0]axi_interconnect_1_M00_AXI_BRESP;
  wire axi_interconnect_1_M00_AXI_BVALID;
  wire [31:0]axi_interconnect_1_M00_AXI_RDATA;
  wire [0:0]axi_interconnect_1_M00_AXI_RREADY;
  wire [1:0]axi_interconnect_1_M00_AXI_RRESP;
  wire axi_interconnect_1_M00_AXI_RVALID;
  wire [31:0]axi_interconnect_1_M00_AXI_WDATA;
  wire axi_interconnect_1_M00_AXI_WREADY;
  wire [3:0]axi_interconnect_1_M00_AXI_WSTRB;
  wire [0:0]axi_interconnect_1_M00_AXI_WVALID;
  wire [31:0]axi_interconnect_1_M01_AXI_ARADDR;
  wire axi_interconnect_1_M01_AXI_ARREADY;
  wire [0:0]axi_interconnect_1_M01_AXI_ARVALID;
  wire [31:0]axi_interconnect_1_M01_AXI_AWADDR;
  wire axi_interconnect_1_M01_AXI_AWREADY;
  wire [0:0]axi_interconnect_1_M01_AXI_AWVALID;
  wire [0:0]axi_interconnect_1_M01_AXI_BREADY;
  wire [1:0]axi_interconnect_1_M01_AXI_BRESP;
  wire axi_interconnect_1_M01_AXI_BVALID;
  wire [31:0]axi_interconnect_1_M01_AXI_RDATA;
  wire [0:0]axi_interconnect_1_M01_AXI_RREADY;
  wire [1:0]axi_interconnect_1_M01_AXI_RRESP;
  wire axi_interconnect_1_M01_AXI_RVALID;
  wire [31:0]axi_interconnect_1_M01_AXI_WDATA;
  wire axi_interconnect_1_M01_AXI_WREADY;
  wire [0:0]axi_interconnect_1_M01_AXI_WVALID;
  wire [4:0]axi_interconnect_1_M02_AXI_ARADDR;
  wire axi_interconnect_1_M02_AXI_ARREADY;
  wire [0:0]axi_interconnect_1_M02_AXI_ARVALID;
  wire [4:0]axi_interconnect_1_M02_AXI_AWADDR;
  wire axi_interconnect_1_M02_AXI_AWREADY;
  wire [0:0]axi_interconnect_1_M02_AXI_AWVALID;
  wire [0:0]axi_interconnect_1_M02_AXI_BREADY;
  wire [1:0]axi_interconnect_1_M02_AXI_BRESP;
  wire axi_interconnect_1_M02_AXI_BVALID;
  wire [31:0]axi_interconnect_1_M02_AXI_RDATA;
  wire [0:0]axi_interconnect_1_M02_AXI_RREADY;
  wire [1:0]axi_interconnect_1_M02_AXI_RRESP;
  wire axi_interconnect_1_M02_AXI_RVALID;
  wire [31:0]axi_interconnect_1_M02_AXI_WDATA;
  wire axi_interconnect_1_M02_AXI_WREADY;
  wire [3:0]axi_interconnect_1_M02_AXI_WSTRB;
  wire [0:0]axi_interconnect_1_M02_AXI_WVALID;
  wire [8:0]axi_interconnect_1_M04_AXI_ARADDR;
  wire axi_interconnect_1_M04_AXI_ARREADY;
  wire [0:0]axi_interconnect_1_M04_AXI_ARVALID;
  wire [8:0]axi_interconnect_1_M04_AXI_AWADDR;
  wire axi_interconnect_1_M04_AXI_AWREADY;
  wire [0:0]axi_interconnect_1_M04_AXI_AWVALID;
  wire [0:0]axi_interconnect_1_M04_AXI_BREADY;
  wire [1:0]axi_interconnect_1_M04_AXI_BRESP;
  wire axi_interconnect_1_M04_AXI_BVALID;
  wire [31:0]axi_interconnect_1_M04_AXI_RDATA;
  wire [0:0]axi_interconnect_1_M04_AXI_RREADY;
  wire [1:0]axi_interconnect_1_M04_AXI_RRESP;
  wire axi_interconnect_1_M04_AXI_RVALID;
  wire [31:0]axi_interconnect_1_M04_AXI_WDATA;
  wire axi_interconnect_1_M04_AXI_WREADY;
  wire [3:0]axi_interconnect_1_M04_AXI_WSTRB;
  wire [0:0]axi_interconnect_1_M04_AXI_WVALID;
  wire axi_timer_1_interrupt;
  wire clk_wiz_1_clk_out3;
  wire dcm_locked_1;
  wire ext_reset_in_1;
  wire [0:0]proc_sys_reset_1_peripheral_aresetn1;
  wire [0:0]proc_sys_reset_1_peripheral_reset;
(* MARK_DEBUG *)   wire [4:0]xlconcat_1_dout;
  wire [0:0]xlconstant_0_dout;

  assign APB_M_paddr[11:0] = axi_apb_bridge_1_APB_M_PADDR[11:0];
  assign APB_M_penable = axi_apb_bridge_1_APB_M_PENABLE;
  assign APB_M_psel = axi_apb_bridge_1_APB_M_PSEL;
  assign APB_M_pwdata[31:0] = axi_apb_bridge_1_APB_M_PWDATA;
  assign APB_M_pwrite = axi_apb_bridge_1_APB_M_PWRITE;
  assign Conn1_ARADDR = S00_AXI_araddr[31:0];
  assign Conn1_ARBURST = S00_AXI_arburst[1:0];
  assign Conn1_ARCACHE = S00_AXI_arcache[3:0];
  assign Conn1_ARID = S00_AXI_arid[11:0];
  assign Conn1_ARLEN = S00_AXI_arlen[3:0];
  assign Conn1_ARLOCK = S00_AXI_arlock[1:0];
  assign Conn1_ARPROT = S00_AXI_arprot[2:0];
  assign Conn1_ARQOS = S00_AXI_arqos[3:0];
  assign Conn1_ARSIZE = S00_AXI_arsize[2:0];
  assign Conn1_ARVALID = S00_AXI_arvalid;
  assign Conn1_AWADDR = S00_AXI_awaddr[31:0];
  assign Conn1_AWBURST = S00_AXI_awburst[1:0];
  assign Conn1_AWCACHE = S00_AXI_awcache[3:0];
  assign Conn1_AWID = S00_AXI_awid[11:0];
  assign Conn1_AWLEN = S00_AXI_awlen[3:0];
  assign Conn1_AWLOCK = S00_AXI_awlock[1:0];
  assign Conn1_AWPROT = S00_AXI_awprot[2:0];
  assign Conn1_AWQOS = S00_AXI_awqos[3:0];
  assign Conn1_AWSIZE = S00_AXI_awsize[2:0];
  assign Conn1_AWVALID = S00_AXI_awvalid;
  assign Conn1_BREADY = S00_AXI_bready;
  assign Conn1_RREADY = S00_AXI_rready;
  assign Conn1_WDATA = S00_AXI_wdata[31:0];
  assign Conn1_WID = S00_AXI_wid[11:0];
  assign Conn1_WLAST = S00_AXI_wlast;
  assign Conn1_WSTRB = S00_AXI_wstrb[3:0];
  assign Conn1_WVALID = S00_AXI_wvalid;
  assign Conn3_ARREADY = M03_AXI_arready;
  assign Conn3_AWREADY = M03_AXI_awready;
  assign Conn3_BRESP = M03_AXI_bresp[1:0];
  assign Conn3_BVALID = M03_AXI_bvalid;
  assign Conn3_RDATA = M03_AXI_rdata[31:0];
  assign Conn3_RRESP = M03_AXI_rresp[1:0];
  assign Conn3_RVALID = M03_AXI_rvalid;
  assign Conn3_WREADY = M03_AXI_wready;
  assign IIC_scl_o = axi_iic_1_IIC_SCL_O;
  assign IIC_scl_t = axi_iic_1_IIC_SCL_T;
  assign IIC_sda_o = axi_iic_1_IIC_SDA_O;
  assign IIC_sda_t = axi_iic_1_IIC_SDA_T;
  assign In1_1 = In1[0];
  assign M03_AXI_araddr[31:0] = Conn3_ARADDR;
  assign M03_AXI_arprot[2:0] = Conn3_ARPROT;
  assign M03_AXI_arvalid = Conn3_ARVALID;
  assign M03_AXI_awaddr[31:0] = Conn3_AWADDR;
  assign M03_AXI_awprot[2:0] = Conn3_AWPROT;
  assign M03_AXI_awvalid = Conn3_AWVALID;
  assign M03_AXI_bready = Conn3_BREADY;
  assign M03_AXI_rready = Conn3_RREADY;
  assign M03_AXI_wdata[31:0] = Conn3_WDATA;
  assign M03_AXI_wstrb[3:0] = Conn3_WSTRB;
  assign M03_AXI_wvalid = Conn3_WVALID;
  assign S00_AXI_arready = Conn1_ARREADY;
  assign S00_AXI_awready = Conn1_AWREADY;
  assign S00_AXI_bid[11:0] = Conn1_BID;
  assign S00_AXI_bresp[1:0] = Conn1_BRESP;
  assign S00_AXI_bvalid = Conn1_BVALID;
  assign S00_AXI_rdata[31:0] = Conn1_RDATA;
  assign S00_AXI_rid[11:0] = Conn1_RID;
  assign S00_AXI_rlast = Conn1_RLAST;
  assign S00_AXI_rresp[1:0] = Conn1_RRESP;
  assign S00_AXI_rvalid = Conn1_RVALID;
  assign S00_AXI_wready = Conn1_WREADY;
  assign axi_apb_bridge_1_APB_M_PRDATA = APB_M_prdata[31:0];
  assign axi_apb_bridge_1_APB_M_PREADY = APB_M_pready;
  assign axi_apb_bridge_1_APB_M_PSLVERR = APB_M_pslverr;
  assign axi_iic_1_IIC_SCL_I = IIC_scl_i;
  assign axi_iic_1_IIC_SDA_I = IIC_sda_i;
  assign clk_wiz_1_clk_out3 = s_axi_aclk;
  assign dcm_locked_1 = dcm_locked;
  assign ext_reset_in_1 = ext_reset_in;
  assign gpo[1:0] = axi_iic_1_gpo;
  assign peripheral_aresetn[0] = proc_sys_reset_1_peripheral_aresetn1;
  assign peripheral_reset[0] = proc_sys_reset_1_peripheral_reset;
GND GND
       (.G(GND_1));
VCC VCC
       (.P(VCC_1));
dp_system_axi_apb_bridge_0_0 axi_apb_bridge_1
       (.m_apb_paddr(axi_apb_bridge_1_APB_M_PADDR),
        .m_apb_penable(axi_apb_bridge_1_APB_M_PENABLE),
        .m_apb_prdata(axi_apb_bridge_1_APB_M_PRDATA),
        .m_apb_pready(axi_apb_bridge_1_APB_M_PREADY),
        .m_apb_psel(axi_apb_bridge_1_APB_M_PSEL),
        .m_apb_pslverr(axi_apb_bridge_1_APB_M_PSLVERR),
        .m_apb_pwdata(axi_apb_bridge_1_APB_M_PWDATA),
        .m_apb_pwrite(axi_apb_bridge_1_APB_M_PWRITE),
        .s_axi_aclk(clk_wiz_1_clk_out3),
        .s_axi_araddr(axi_interconnect_1_M01_AXI_ARADDR),
        .s_axi_aresetn(proc_sys_reset_1_peripheral_aresetn1),
        .s_axi_arready(axi_interconnect_1_M01_AXI_ARREADY),
        .s_axi_arvalid(axi_interconnect_1_M01_AXI_ARVALID),
        .s_axi_awaddr(axi_interconnect_1_M01_AXI_AWADDR),
        .s_axi_awready(axi_interconnect_1_M01_AXI_AWREADY),
        .s_axi_awvalid(axi_interconnect_1_M01_AXI_AWVALID),
        .s_axi_bready(axi_interconnect_1_M01_AXI_BREADY),
        .s_axi_bresp(axi_interconnect_1_M01_AXI_BRESP),
        .s_axi_bvalid(axi_interconnect_1_M01_AXI_BVALID),
        .s_axi_rdata(axi_interconnect_1_M01_AXI_RDATA),
        .s_axi_rready(axi_interconnect_1_M01_AXI_RREADY),
        .s_axi_rresp(axi_interconnect_1_M01_AXI_RRESP),
        .s_axi_rvalid(axi_interconnect_1_M01_AXI_RVALID),
        .s_axi_wdata(axi_interconnect_1_M01_AXI_WDATA),
        .s_axi_wready(axi_interconnect_1_M01_AXI_WREADY),
        .s_axi_wvalid(axi_interconnect_1_M01_AXI_WVALID));
dp_system_axi_iic_0_0 axi_iic_1
       (.gpo(axi_iic_1_gpo),
        .iic2intc_irpt(axi_iic_1_iic2intc_irpt),
        .s_axi_aclk(clk_wiz_1_clk_out3),
        .s_axi_araddr(axi_interconnect_1_M04_AXI_ARADDR),
        .s_axi_aresetn(proc_sys_reset_1_peripheral_aresetn1),
        .s_axi_arready(axi_interconnect_1_M04_AXI_ARREADY),
        .s_axi_arvalid(axi_interconnect_1_M04_AXI_ARVALID),
        .s_axi_awaddr(axi_interconnect_1_M04_AXI_AWADDR),
        .s_axi_awready(axi_interconnect_1_M04_AXI_AWREADY),
        .s_axi_awvalid(axi_interconnect_1_M04_AXI_AWVALID),
        .s_axi_bready(axi_interconnect_1_M04_AXI_BREADY),
        .s_axi_bresp(axi_interconnect_1_M04_AXI_BRESP),
        .s_axi_bvalid(axi_interconnect_1_M04_AXI_BVALID),
        .s_axi_rdata(axi_interconnect_1_M04_AXI_RDATA),
        .s_axi_rready(axi_interconnect_1_M04_AXI_RREADY),
        .s_axi_rresp(axi_interconnect_1_M04_AXI_RRESP),
        .s_axi_rvalid(axi_interconnect_1_M04_AXI_RVALID),
        .s_axi_wdata(axi_interconnect_1_M04_AXI_WDATA),
        .s_axi_wready(axi_interconnect_1_M04_AXI_WREADY),
        .s_axi_wstrb(axi_interconnect_1_M04_AXI_WSTRB),
        .s_axi_wvalid(axi_interconnect_1_M04_AXI_WVALID),
        .scl_i(axi_iic_1_IIC_SCL_I),
        .scl_o(axi_iic_1_IIC_SCL_O),
        .scl_t(axi_iic_1_IIC_SCL_T),
        .sda_i(axi_iic_1_IIC_SDA_I),
        .sda_o(axi_iic_1_IIC_SDA_O),
        .sda_t(axi_iic_1_IIC_SDA_T));
dp_system_axi_intc_0_0 axi_intc_1
       (.intr(xlconcat_1_dout),
        .s_axi_aclk(clk_wiz_1_clk_out3),
        .s_axi_araddr(axi_interconnect_1_M00_AXI_ARADDR),
        .s_axi_aresetn(proc_sys_reset_1_peripheral_aresetn1),
        .s_axi_arready(axi_interconnect_1_M00_AXI_ARREADY),
        .s_axi_arvalid(axi_interconnect_1_M00_AXI_ARVALID),
        .s_axi_awaddr(axi_interconnect_1_M00_AXI_AWADDR),
        .s_axi_awready(axi_interconnect_1_M00_AXI_AWREADY),
        .s_axi_awvalid(axi_interconnect_1_M00_AXI_AWVALID),
        .s_axi_bready(axi_interconnect_1_M00_AXI_BREADY),
        .s_axi_bresp(axi_interconnect_1_M00_AXI_BRESP),
        .s_axi_bvalid(axi_interconnect_1_M00_AXI_BVALID),
        .s_axi_rdata(axi_interconnect_1_M00_AXI_RDATA),
        .s_axi_rready(axi_interconnect_1_M00_AXI_RREADY),
        .s_axi_rresp(axi_interconnect_1_M00_AXI_RRESP),
        .s_axi_rvalid(axi_interconnect_1_M00_AXI_RVALID),
        .s_axi_wdata(axi_interconnect_1_M00_AXI_WDATA),
        .s_axi_wready(axi_interconnect_1_M00_AXI_WREADY),
        .s_axi_wstrb(axi_interconnect_1_M00_AXI_WSTRB),
        .s_axi_wvalid(axi_interconnect_1_M00_AXI_WVALID));
dp_system_axi_interconnect_1_0 axi_interconnect_1
       (.ACLK(clk_wiz_1_clk_out3),
        .ARESETN(ARESETN_1),
        .M00_ACLK(clk_wiz_1_clk_out3),
        .M00_ARESETN(proc_sys_reset_1_peripheral_aresetn1),
        .M00_AXI_araddr(axi_interconnect_1_M00_AXI_ARADDR),
        .M00_AXI_arready(axi_interconnect_1_M00_AXI_ARREADY),
        .M00_AXI_arvalid(axi_interconnect_1_M00_AXI_ARVALID),
        .M00_AXI_awaddr(axi_interconnect_1_M00_AXI_AWADDR),
        .M00_AXI_awready(axi_interconnect_1_M00_AXI_AWREADY),
        .M00_AXI_awvalid(axi_interconnect_1_M00_AXI_AWVALID),
        .M00_AXI_bready(axi_interconnect_1_M00_AXI_BREADY),
        .M00_AXI_bresp(axi_interconnect_1_M00_AXI_BRESP),
        .M00_AXI_bvalid(axi_interconnect_1_M00_AXI_BVALID),
        .M00_AXI_rdata(axi_interconnect_1_M00_AXI_RDATA),
        .M00_AXI_rready(axi_interconnect_1_M00_AXI_RREADY),
        .M00_AXI_rresp(axi_interconnect_1_M00_AXI_RRESP),
        .M00_AXI_rvalid(axi_interconnect_1_M00_AXI_RVALID),
        .M00_AXI_wdata(axi_interconnect_1_M00_AXI_WDATA),
        .M00_AXI_wready(axi_interconnect_1_M00_AXI_WREADY),
        .M00_AXI_wstrb(axi_interconnect_1_M00_AXI_WSTRB),
        .M00_AXI_wvalid(axi_interconnect_1_M00_AXI_WVALID),
        .M01_ACLK(clk_wiz_1_clk_out3),
        .M01_ARESETN(proc_sys_reset_1_peripheral_aresetn1),
        .M01_AXI_araddr(axi_interconnect_1_M01_AXI_ARADDR),
        .M01_AXI_arready(axi_interconnect_1_M01_AXI_ARREADY),
        .M01_AXI_arvalid(axi_interconnect_1_M01_AXI_ARVALID),
        .M01_AXI_awaddr(axi_interconnect_1_M01_AXI_AWADDR),
        .M01_AXI_awready(axi_interconnect_1_M01_AXI_AWREADY),
        .M01_AXI_awvalid(axi_interconnect_1_M01_AXI_AWVALID),
        .M01_AXI_bready(axi_interconnect_1_M01_AXI_BREADY),
        .M01_AXI_bresp(axi_interconnect_1_M01_AXI_BRESP),
        .M01_AXI_bvalid(axi_interconnect_1_M01_AXI_BVALID),
        .M01_AXI_rdata(axi_interconnect_1_M01_AXI_RDATA),
        .M01_AXI_rready(axi_interconnect_1_M01_AXI_RREADY),
        .M01_AXI_rresp(axi_interconnect_1_M01_AXI_RRESP),
        .M01_AXI_rvalid(axi_interconnect_1_M01_AXI_RVALID),
        .M01_AXI_wdata(axi_interconnect_1_M01_AXI_WDATA),
        .M01_AXI_wready(axi_interconnect_1_M01_AXI_WREADY),
        .M01_AXI_wvalid(axi_interconnect_1_M01_AXI_WVALID),
        .M02_ACLK(clk_wiz_1_clk_out3),
        .M02_ARESETN(proc_sys_reset_1_peripheral_aresetn1),
        .M02_AXI_araddr(axi_interconnect_1_M02_AXI_ARADDR),
        .M02_AXI_arready(axi_interconnect_1_M02_AXI_ARREADY),
        .M02_AXI_arvalid(axi_interconnect_1_M02_AXI_ARVALID),
        .M02_AXI_awaddr(axi_interconnect_1_M02_AXI_AWADDR),
        .M02_AXI_awready(axi_interconnect_1_M02_AXI_AWREADY),
        .M02_AXI_awvalid(axi_interconnect_1_M02_AXI_AWVALID),
        .M02_AXI_bready(axi_interconnect_1_M02_AXI_BREADY),
        .M02_AXI_bresp(axi_interconnect_1_M02_AXI_BRESP),
        .M02_AXI_bvalid(axi_interconnect_1_M02_AXI_BVALID),
        .M02_AXI_rdata(axi_interconnect_1_M02_AXI_RDATA),
        .M02_AXI_rready(axi_interconnect_1_M02_AXI_RREADY),
        .M02_AXI_rresp(axi_interconnect_1_M02_AXI_RRESP),
        .M02_AXI_rvalid(axi_interconnect_1_M02_AXI_RVALID),
        .M02_AXI_wdata(axi_interconnect_1_M02_AXI_WDATA),
        .M02_AXI_wready(axi_interconnect_1_M02_AXI_WREADY),
        .M02_AXI_wstrb(axi_interconnect_1_M02_AXI_WSTRB),
        .M02_AXI_wvalid(axi_interconnect_1_M02_AXI_WVALID),
        .M03_ACLK(clk_wiz_1_clk_out3),
        .M03_ARESETN(proc_sys_reset_1_peripheral_aresetn1),
        .M03_AXI_araddr(Conn3_ARADDR),
        .M03_AXI_arprot(Conn3_ARPROT),
        .M03_AXI_arready(Conn3_ARREADY),
        .M03_AXI_arvalid(Conn3_ARVALID),
        .M03_AXI_awaddr(Conn3_AWADDR),
        .M03_AXI_awprot(Conn3_AWPROT),
        .M03_AXI_awready(Conn3_AWREADY),
        .M03_AXI_awvalid(Conn3_AWVALID),
        .M03_AXI_bready(Conn3_BREADY),
        .M03_AXI_bresp(Conn3_BRESP),
        .M03_AXI_bvalid(Conn3_BVALID),
        .M03_AXI_rdata(Conn3_RDATA),
        .M03_AXI_rready(Conn3_RREADY),
        .M03_AXI_rresp(Conn3_RRESP),
        .M03_AXI_rvalid(Conn3_RVALID),
        .M03_AXI_wdata(Conn3_WDATA),
        .M03_AXI_wready(Conn3_WREADY),
        .M03_AXI_wstrb(Conn3_WSTRB),
        .M03_AXI_wvalid(Conn3_WVALID),
        .M04_ACLK(clk_wiz_1_clk_out3),
        .M04_ARESETN(proc_sys_reset_1_peripheral_aresetn1),
        .M04_AXI_araddr(axi_interconnect_1_M04_AXI_ARADDR),
        .M04_AXI_arready(axi_interconnect_1_M04_AXI_ARREADY),
        .M04_AXI_arvalid(axi_interconnect_1_M04_AXI_ARVALID),
        .M04_AXI_awaddr(axi_interconnect_1_M04_AXI_AWADDR),
        .M04_AXI_awready(axi_interconnect_1_M04_AXI_AWREADY),
        .M04_AXI_awvalid(axi_interconnect_1_M04_AXI_AWVALID),
        .M04_AXI_bready(axi_interconnect_1_M04_AXI_BREADY),
        .M04_AXI_bresp(axi_interconnect_1_M04_AXI_BRESP),
        .M04_AXI_bvalid(axi_interconnect_1_M04_AXI_BVALID),
        .M04_AXI_rdata(axi_interconnect_1_M04_AXI_RDATA),
        .M04_AXI_rready(axi_interconnect_1_M04_AXI_RREADY),
        .M04_AXI_rresp(axi_interconnect_1_M04_AXI_RRESP),
        .M04_AXI_rvalid(axi_interconnect_1_M04_AXI_RVALID),
        .M04_AXI_wdata(axi_interconnect_1_M04_AXI_WDATA),
        .M04_AXI_wready(axi_interconnect_1_M04_AXI_WREADY),
        .M04_AXI_wstrb(axi_interconnect_1_M04_AXI_WSTRB),
        .M04_AXI_wvalid(axi_interconnect_1_M04_AXI_WVALID),
        .S00_ACLK(clk_wiz_1_clk_out3),
        .S00_ARESETN(proc_sys_reset_1_peripheral_aresetn1),
        .S00_AXI_araddr(Conn1_ARADDR),
        .S00_AXI_arburst(Conn1_ARBURST),
        .S00_AXI_arcache(Conn1_ARCACHE),
        .S00_AXI_arid(Conn1_ARID),
        .S00_AXI_arlen(Conn1_ARLEN),
        .S00_AXI_arlock(Conn1_ARLOCK),
        .S00_AXI_arprot(Conn1_ARPROT),
        .S00_AXI_arqos(Conn1_ARQOS),
        .S00_AXI_arready(Conn1_ARREADY),
        .S00_AXI_arsize(Conn1_ARSIZE),
        .S00_AXI_arvalid(Conn1_ARVALID),
        .S00_AXI_awaddr(Conn1_AWADDR),
        .S00_AXI_awburst(Conn1_AWBURST),
        .S00_AXI_awcache(Conn1_AWCACHE),
        .S00_AXI_awid(Conn1_AWID),
        .S00_AXI_awlen(Conn1_AWLEN),
        .S00_AXI_awlock(Conn1_AWLOCK),
        .S00_AXI_awprot(Conn1_AWPROT),
        .S00_AXI_awqos(Conn1_AWQOS),
        .S00_AXI_awready(Conn1_AWREADY),
        .S00_AXI_awsize(Conn1_AWSIZE),
        .S00_AXI_awvalid(Conn1_AWVALID),
        .S00_AXI_bid(Conn1_BID),
        .S00_AXI_bready(Conn1_BREADY),
        .S00_AXI_bresp(Conn1_BRESP),
        .S00_AXI_bvalid(Conn1_BVALID),
        .S00_AXI_rdata(Conn1_RDATA),
        .S00_AXI_rid(Conn1_RID),
        .S00_AXI_rlast(Conn1_RLAST),
        .S00_AXI_rready(Conn1_RREADY),
        .S00_AXI_rresp(Conn1_RRESP),
        .S00_AXI_rvalid(Conn1_RVALID),
        .S00_AXI_wdata(Conn1_WDATA),
        .S00_AXI_wid(Conn1_WID),
        .S00_AXI_wlast(Conn1_WLAST),
        .S00_AXI_wready(Conn1_WREADY),
        .S00_AXI_wstrb(Conn1_WSTRB),
        .S00_AXI_wvalid(Conn1_WVALID));
dp_system_axi_timer_0_0 axi_timer_1
       (.capturetrig0(GND_1),
        .capturetrig1(GND_1),
        .freeze(GND_1),
        .interrupt(axi_timer_1_interrupt),
        .s_axi_aclk(clk_wiz_1_clk_out3),
        .s_axi_araddr(axi_interconnect_1_M02_AXI_ARADDR),
        .s_axi_aresetn(proc_sys_reset_1_peripheral_aresetn1),
        .s_axi_arready(axi_interconnect_1_M02_AXI_ARREADY),
        .s_axi_arvalid(axi_interconnect_1_M02_AXI_ARVALID),
        .s_axi_awaddr(axi_interconnect_1_M02_AXI_AWADDR),
        .s_axi_awready(axi_interconnect_1_M02_AXI_AWREADY),
        .s_axi_awvalid(axi_interconnect_1_M02_AXI_AWVALID),
        .s_axi_bready(axi_interconnect_1_M02_AXI_BREADY),
        .s_axi_bresp(axi_interconnect_1_M02_AXI_BRESP),
        .s_axi_bvalid(axi_interconnect_1_M02_AXI_BVALID),
        .s_axi_rdata(axi_interconnect_1_M02_AXI_RDATA),
        .s_axi_rready(axi_interconnect_1_M02_AXI_RREADY),
        .s_axi_rresp(axi_interconnect_1_M02_AXI_RRESP),
        .s_axi_rvalid(axi_interconnect_1_M02_AXI_RVALID),
        .s_axi_wdata(axi_interconnect_1_M02_AXI_WDATA),
        .s_axi_wready(axi_interconnect_1_M02_AXI_WREADY),
        .s_axi_wstrb(axi_interconnect_1_M02_AXI_WSTRB),
        .s_axi_wvalid(axi_interconnect_1_M02_AXI_WVALID));
dp_system_proc_sys_reset_0_0 proc_sys_reset_1
       (.aux_reset_in(VCC_1),
        .dcm_locked(dcm_locked_1),
        .ext_reset_in(ext_reset_in_1),
        .interconnect_aresetn(ARESETN_1),
        .mb_debug_sys_rst(GND_1),
        .peripheral_aresetn(proc_sys_reset_1_peripheral_aresetn1),
        .peripheral_reset(proc_sys_reset_1_peripheral_reset),
        .slowest_sync_clk(clk_wiz_1_clk_out3));
dp_system_xlconcat_0_0 xlconcat_1
       (.In0(xlconstant_0_dout),
        .In1(In1_1),
        .In2(axi_timer_1_interrupt),
        .In3(xlconstant_0_dout),
        .In4(axi_iic_1_iic2intc_irpt),
        .dout(xlconcat_1_dout));
dp_system_xlconstant_0_0 xlconstant_0
       (.dout(xlconstant_0_dout));
endmodule

module cabac_modules_imp_AL9V63
   (S00_ACLK,
    S00_AXI_araddr,
    S00_AXI_arburst,
    S00_AXI_arcache,
    S00_AXI_arid,
    S00_AXI_arlen,
    S00_AXI_arlock,
    S00_AXI_arprot,
    S00_AXI_arqos,
    S00_AXI_arready,
    S00_AXI_arsize,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awburst,
    S00_AXI_awcache,
    S00_AXI_awid,
    S00_AXI_awlen,
    S00_AXI_awlock,
    S00_AXI_awprot,
    S00_AXI_awqos,
    S00_AXI_awready,
    S00_AXI_awsize,
    S00_AXI_awvalid,
    S00_AXI_bid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rid,
    S00_AXI_rlast,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wid,
    S00_AXI_wlast,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid,
    bus_struct_reset,
    ext_reset_in,
    fifo_cb_wr_data,
    fifo_cb_wr_en,
    fifo_cont_wr_data,
    fifo_cont_wr_en,
    fifo_cr_wr_data,
    fifo_cr_wr_en,
    fifo_y_wr_data,
    fifo_y_wr_en,
    fullCb,
    fullCont,
    fullCr,
    fullY);
  input S00_ACLK;
  input [31:0]S00_AXI_araddr;
  input [1:0]S00_AXI_arburst;
  input [3:0]S00_AXI_arcache;
  input [11:0]S00_AXI_arid;
  input [3:0]S00_AXI_arlen;
  input [1:0]S00_AXI_arlock;
  input [2:0]S00_AXI_arprot;
  input [3:0]S00_AXI_arqos;
  output S00_AXI_arready;
  input [2:0]S00_AXI_arsize;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [1:0]S00_AXI_awburst;
  input [3:0]S00_AXI_awcache;
  input [11:0]S00_AXI_awid;
  input [3:0]S00_AXI_awlen;
  input [1:0]S00_AXI_awlock;
  input [2:0]S00_AXI_awprot;
  input [3:0]S00_AXI_awqos;
  output S00_AXI_awready;
  input [2:0]S00_AXI_awsize;
  input S00_AXI_awvalid;
  output [11:0]S00_AXI_bid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  output [11:0]S00_AXI_rid;
  output S00_AXI_rlast;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  input [11:0]S00_AXI_wid;
  input S00_AXI_wlast;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;
  output [0:0]bus_struct_reset;
  input ext_reset_in;
  output [143:0]fifo_cb_wr_data;
  output fifo_cb_wr_en;
  output [31:0]fifo_cont_wr_data;
  output fifo_cont_wr_en;
  output [143:0]fifo_cr_wr_data;
  output fifo_cr_wr_en;
  output [143:0]fifo_y_wr_data;
  output fifo_y_wr_en;
  input fullCb;
  input fullCont;
  input fullCr;
  input fullY;

  wire [31:0]Conn1_WR_DATA;
  wire Conn1_WR_EN;
  wire [143:0]Conn2_WR_DATA;
  wire Conn2_WR_EN;
  wire [143:0]Conn3_WR_DATA;
  wire Conn3_WR_EN;
  wire [143:0]Conn4_WR_DATA;
  wire Conn4_WR_EN;
  wire GND_1;
  wire Net2;
  wire VCC_1;
  wire fullCb_1;
  wire fullCont_1;
  wire fullCr_1;
  wire fullY_1;
  wire processing_system7_0_FCLK_CLK0;
  wire [31:0]processing_system7_0_M_AXI_GP1_ARADDR;
  wire [1:0]processing_system7_0_M_AXI_GP1_ARBURST;
  wire [3:0]processing_system7_0_M_AXI_GP1_ARCACHE;
  wire [11:0]processing_system7_0_M_AXI_GP1_ARID;
  wire [3:0]processing_system7_0_M_AXI_GP1_ARLEN;
  wire [1:0]processing_system7_0_M_AXI_GP1_ARLOCK;
  wire [2:0]processing_system7_0_M_AXI_GP1_ARPROT;
  wire [3:0]processing_system7_0_M_AXI_GP1_ARQOS;
  wire processing_system7_0_M_AXI_GP1_ARREADY;
  wire [2:0]processing_system7_0_M_AXI_GP1_ARSIZE;
  wire processing_system7_0_M_AXI_GP1_ARVALID;
  wire [31:0]processing_system7_0_M_AXI_GP1_AWADDR;
  wire [1:0]processing_system7_0_M_AXI_GP1_AWBURST;
  wire [3:0]processing_system7_0_M_AXI_GP1_AWCACHE;
  wire [11:0]processing_system7_0_M_AXI_GP1_AWID;
  wire [3:0]processing_system7_0_M_AXI_GP1_AWLEN;
  wire [1:0]processing_system7_0_M_AXI_GP1_AWLOCK;
  wire [2:0]processing_system7_0_M_AXI_GP1_AWPROT;
  wire [3:0]processing_system7_0_M_AXI_GP1_AWQOS;
  wire processing_system7_0_M_AXI_GP1_AWREADY;
  wire [2:0]processing_system7_0_M_AXI_GP1_AWSIZE;
  wire processing_system7_0_M_AXI_GP1_AWVALID;
  wire [11:0]processing_system7_0_M_AXI_GP1_BID;
  wire processing_system7_0_M_AXI_GP1_BREADY;
  wire [1:0]processing_system7_0_M_AXI_GP1_BRESP;
  wire processing_system7_0_M_AXI_GP1_BVALID;
  wire [31:0]processing_system7_0_M_AXI_GP1_RDATA;
  wire [11:0]processing_system7_0_M_AXI_GP1_RID;
  wire processing_system7_0_M_AXI_GP1_RLAST;
  wire processing_system7_0_M_AXI_GP1_RREADY;
  wire [1:0]processing_system7_0_M_AXI_GP1_RRESP;
  wire processing_system7_0_M_AXI_GP1_RVALID;
  wire [31:0]processing_system7_0_M_AXI_GP1_WDATA;
  wire [11:0]processing_system7_0_M_AXI_GP1_WID;
  wire processing_system7_0_M_AXI_GP1_WLAST;
  wire processing_system7_0_M_AXI_GP1_WREADY;
  wire [3:0]processing_system7_0_M_AXI_GP1_WSTRB;
  wire processing_system7_0_M_AXI_GP1_WVALID;
  wire [15:0]processing_system7_0_axi_periph_M00_AXI_ARADDR;
  wire [1:0]processing_system7_0_axi_periph_M00_AXI_ARBURST;
  wire [3:0]processing_system7_0_axi_periph_M00_AXI_ARCACHE;
  wire [11:0]processing_system7_0_axi_periph_M00_AXI_ARID;
  wire [7:0]processing_system7_0_axi_periph_M00_AXI_ARLEN;
  wire [0:0]processing_system7_0_axi_periph_M00_AXI_ARLOCK;
  wire [2:0]processing_system7_0_axi_periph_M00_AXI_ARPROT;
  wire processing_system7_0_axi_periph_M00_AXI_ARREADY;
  wire [2:0]processing_system7_0_axi_periph_M00_AXI_ARSIZE;
  wire processing_system7_0_axi_periph_M00_AXI_ARVALID;
  wire [15:0]processing_system7_0_axi_periph_M00_AXI_AWADDR;
  wire [1:0]processing_system7_0_axi_periph_M00_AXI_AWBURST;
  wire [3:0]processing_system7_0_axi_periph_M00_AXI_AWCACHE;
  wire [11:0]processing_system7_0_axi_periph_M00_AXI_AWID;
  wire [7:0]processing_system7_0_axi_periph_M00_AXI_AWLEN;
  wire [0:0]processing_system7_0_axi_periph_M00_AXI_AWLOCK;
  wire [2:0]processing_system7_0_axi_periph_M00_AXI_AWPROT;
  wire processing_system7_0_axi_periph_M00_AXI_AWREADY;
  wire [2:0]processing_system7_0_axi_periph_M00_AXI_AWSIZE;
  wire processing_system7_0_axi_periph_M00_AXI_AWVALID;
  wire [11:0]processing_system7_0_axi_periph_M00_AXI_BID;
  wire processing_system7_0_axi_periph_M00_AXI_BREADY;
  wire [1:0]processing_system7_0_axi_periph_M00_AXI_BRESP;
  wire processing_system7_0_axi_periph_M00_AXI_BVALID;
  wire [31:0]processing_system7_0_axi_periph_M00_AXI_RDATA;
  wire [11:0]processing_system7_0_axi_periph_M00_AXI_RID;
  wire processing_system7_0_axi_periph_M00_AXI_RLAST;
  wire processing_system7_0_axi_periph_M00_AXI_RREADY;
  wire [1:0]processing_system7_0_axi_periph_M00_AXI_RRESP;
  wire processing_system7_0_axi_periph_M00_AXI_RVALID;
  wire [31:0]processing_system7_0_axi_periph_M00_AXI_WDATA;
  wire processing_system7_0_axi_periph_M00_AXI_WLAST;
  wire processing_system7_0_axi_periph_M00_AXI_WREADY;
  wire [3:0]processing_system7_0_axi_periph_M00_AXI_WSTRB;
  wire processing_system7_0_axi_periph_M00_AXI_WVALID;
  wire [0:0]rst_processing_system7_0_50M_bus_struct_reset;
  wire [0:0]rst_processing_system7_0_50M_interconnect_aresetn;
  wire [0:0]rst_processing_system7_0_50M_peripheral_aresetn;

  assign Net2 = ext_reset_in;
  assign S00_AXI_arready = processing_system7_0_M_AXI_GP1_ARREADY;
  assign S00_AXI_awready = processing_system7_0_M_AXI_GP1_AWREADY;
  assign S00_AXI_bid[11:0] = processing_system7_0_M_AXI_GP1_BID;
  assign S00_AXI_bresp[1:0] = processing_system7_0_M_AXI_GP1_BRESP;
  assign S00_AXI_bvalid = processing_system7_0_M_AXI_GP1_BVALID;
  assign S00_AXI_rdata[31:0] = processing_system7_0_M_AXI_GP1_RDATA;
  assign S00_AXI_rid[11:0] = processing_system7_0_M_AXI_GP1_RID;
  assign S00_AXI_rlast = processing_system7_0_M_AXI_GP1_RLAST;
  assign S00_AXI_rresp[1:0] = processing_system7_0_M_AXI_GP1_RRESP;
  assign S00_AXI_rvalid = processing_system7_0_M_AXI_GP1_RVALID;
  assign S00_AXI_wready = processing_system7_0_M_AXI_GP1_WREADY;
  assign bus_struct_reset[0] = rst_processing_system7_0_50M_bus_struct_reset;
  assign fifo_cb_wr_data[143:0] = Conn3_WR_DATA;
  assign fifo_cb_wr_en = Conn3_WR_EN;
  assign fifo_cont_wr_data[31:0] = Conn1_WR_DATA;
  assign fifo_cont_wr_en = Conn1_WR_EN;
  assign fifo_cr_wr_data[143:0] = Conn4_WR_DATA;
  assign fifo_cr_wr_en = Conn4_WR_EN;
  assign fifo_y_wr_data[143:0] = Conn2_WR_DATA;
  assign fifo_y_wr_en = Conn2_WR_EN;
  assign fullCb_1 = fullCb;
  assign fullCont_1 = fullCont;
  assign fullCr_1 = fullCr;
  assign fullY_1 = fullY;
  assign processing_system7_0_FCLK_CLK0 = S00_ACLK;
  assign processing_system7_0_M_AXI_GP1_ARADDR = S00_AXI_araddr[31:0];
  assign processing_system7_0_M_AXI_GP1_ARBURST = S00_AXI_arburst[1:0];
  assign processing_system7_0_M_AXI_GP1_ARCACHE = S00_AXI_arcache[3:0];
  assign processing_system7_0_M_AXI_GP1_ARID = S00_AXI_arid[11:0];
  assign processing_system7_0_M_AXI_GP1_ARLEN = S00_AXI_arlen[3:0];
  assign processing_system7_0_M_AXI_GP1_ARLOCK = S00_AXI_arlock[1:0];
  assign processing_system7_0_M_AXI_GP1_ARPROT = S00_AXI_arprot[2:0];
  assign processing_system7_0_M_AXI_GP1_ARQOS = S00_AXI_arqos[3:0];
  assign processing_system7_0_M_AXI_GP1_ARSIZE = S00_AXI_arsize[2:0];
  assign processing_system7_0_M_AXI_GP1_ARVALID = S00_AXI_arvalid;
  assign processing_system7_0_M_AXI_GP1_AWADDR = S00_AXI_awaddr[31:0];
  assign processing_system7_0_M_AXI_GP1_AWBURST = S00_AXI_awburst[1:0];
  assign processing_system7_0_M_AXI_GP1_AWCACHE = S00_AXI_awcache[3:0];
  assign processing_system7_0_M_AXI_GP1_AWID = S00_AXI_awid[11:0];
  assign processing_system7_0_M_AXI_GP1_AWLEN = S00_AXI_awlen[3:0];
  assign processing_system7_0_M_AXI_GP1_AWLOCK = S00_AXI_awlock[1:0];
  assign processing_system7_0_M_AXI_GP1_AWPROT = S00_AXI_awprot[2:0];
  assign processing_system7_0_M_AXI_GP1_AWQOS = S00_AXI_awqos[3:0];
  assign processing_system7_0_M_AXI_GP1_AWSIZE = S00_AXI_awsize[2:0];
  assign processing_system7_0_M_AXI_GP1_AWVALID = S00_AXI_awvalid;
  assign processing_system7_0_M_AXI_GP1_BREADY = S00_AXI_bready;
  assign processing_system7_0_M_AXI_GP1_RREADY = S00_AXI_rready;
  assign processing_system7_0_M_AXI_GP1_WDATA = S00_AXI_wdata[31:0];
  assign processing_system7_0_M_AXI_GP1_WID = S00_AXI_wid[11:0];
  assign processing_system7_0_M_AXI_GP1_WLAST = S00_AXI_wlast;
  assign processing_system7_0_M_AXI_GP1_WSTRB = S00_AXI_wstrb[3:0];
  assign processing_system7_0_M_AXI_GP1_WVALID = S00_AXI_wvalid;
GND GND
       (.G(GND_1));
VCC VCC
       (.P(VCC_1));
dp_system_cabac_ngc_0_0 cabac_ngc_0
       (.axi_araddr(processing_system7_0_axi_periph_M00_AXI_ARADDR),
        .axi_arburst(processing_system7_0_axi_periph_M00_AXI_ARBURST),
        .axi_arcache(processing_system7_0_axi_periph_M00_AXI_ARCACHE),
        .axi_arid(processing_system7_0_axi_periph_M00_AXI_ARID),
        .axi_arlen(processing_system7_0_axi_periph_M00_AXI_ARLEN),
        .axi_arlock(processing_system7_0_axi_periph_M00_AXI_ARLOCK),
        .axi_arprot(processing_system7_0_axi_periph_M00_AXI_ARPROT),
        .axi_arready(processing_system7_0_axi_periph_M00_AXI_ARREADY),
        .axi_arsize(processing_system7_0_axi_periph_M00_AXI_ARSIZE),
        .axi_arvalid(processing_system7_0_axi_periph_M00_AXI_ARVALID),
        .axi_awaddr(processing_system7_0_axi_periph_M00_AXI_AWADDR),
        .axi_awburst(processing_system7_0_axi_periph_M00_AXI_AWBURST),
        .axi_awcache(processing_system7_0_axi_periph_M00_AXI_AWCACHE),
        .axi_awid(processing_system7_0_axi_periph_M00_AXI_AWID),
        .axi_awlen(processing_system7_0_axi_periph_M00_AXI_AWLEN),
        .axi_awlock(processing_system7_0_axi_periph_M00_AXI_AWLOCK),
        .axi_awprot(processing_system7_0_axi_periph_M00_AXI_AWPROT),
        .axi_awready(processing_system7_0_axi_periph_M00_AXI_AWREADY),
        .axi_awsize(processing_system7_0_axi_periph_M00_AXI_AWSIZE),
        .axi_awvalid(processing_system7_0_axi_periph_M00_AXI_AWVALID),
        .axi_bid(processing_system7_0_axi_periph_M00_AXI_BID),
        .axi_bready(processing_system7_0_axi_periph_M00_AXI_BREADY),
        .axi_bresp(processing_system7_0_axi_periph_M00_AXI_BRESP),
        .axi_bvalid(processing_system7_0_axi_periph_M00_AXI_BVALID),
        .axi_rdata(processing_system7_0_axi_periph_M00_AXI_RDATA),
        .axi_rid(processing_system7_0_axi_periph_M00_AXI_RID),
        .axi_rlast(processing_system7_0_axi_periph_M00_AXI_RLAST),
        .axi_rready(processing_system7_0_axi_periph_M00_AXI_RREADY),
        .axi_rresp(processing_system7_0_axi_periph_M00_AXI_RRESP),
        .axi_rvalid(processing_system7_0_axi_periph_M00_AXI_RVALID),
        .axi_wdata(processing_system7_0_axi_periph_M00_AXI_WDATA),
        .axi_wlast(processing_system7_0_axi_periph_M00_AXI_WLAST),
        .axi_wready(processing_system7_0_axi_periph_M00_AXI_WREADY),
        .axi_wstrb(processing_system7_0_axi_periph_M00_AXI_WSTRB),
        .axi_wvalid(processing_system7_0_axi_periph_M00_AXI_WVALID),
        .clk(processing_system7_0_FCLK_CLK0),
        .fullCb(fullCb_1),
        .fullCont(fullCont_1),
        .fullCr(fullCr_1),
        .fullY(fullY_1),
        .outCb(Conn3_WR_DATA),
        .outCont(Conn1_WR_DATA),
        .outCr(Conn4_WR_DATA),
        .outY(Conn2_WR_DATA),
        .rst(rst_processing_system7_0_50M_peripheral_aresetn),
        .validCb(Conn3_WR_EN),
        .validCont(Conn1_WR_EN),
        .validCr(Conn4_WR_EN),
        .validY(Conn2_WR_EN));
dp_system_processing_system7_0_axi_periph_0 processing_system7_0_axi_periph
       (.ACLK(processing_system7_0_FCLK_CLK0),
        .ARESETN(rst_processing_system7_0_50M_interconnect_aresetn),
        .M00_ACLK(processing_system7_0_FCLK_CLK0),
        .M00_ARESETN(rst_processing_system7_0_50M_peripheral_aresetn),
        .M00_AXI_araddr(processing_system7_0_axi_periph_M00_AXI_ARADDR),
        .M00_AXI_arburst(processing_system7_0_axi_periph_M00_AXI_ARBURST),
        .M00_AXI_arcache(processing_system7_0_axi_periph_M00_AXI_ARCACHE),
        .M00_AXI_arid(processing_system7_0_axi_periph_M00_AXI_ARID),
        .M00_AXI_arlen(processing_system7_0_axi_periph_M00_AXI_ARLEN),
        .M00_AXI_arlock(processing_system7_0_axi_periph_M00_AXI_ARLOCK),
        .M00_AXI_arprot(processing_system7_0_axi_periph_M00_AXI_ARPROT),
        .M00_AXI_arready(processing_system7_0_axi_periph_M00_AXI_ARREADY),
        .M00_AXI_arsize(processing_system7_0_axi_periph_M00_AXI_ARSIZE),
        .M00_AXI_arvalid(processing_system7_0_axi_periph_M00_AXI_ARVALID),
        .M00_AXI_awaddr(processing_system7_0_axi_periph_M00_AXI_AWADDR),
        .M00_AXI_awburst(processing_system7_0_axi_periph_M00_AXI_AWBURST),
        .M00_AXI_awcache(processing_system7_0_axi_periph_M00_AXI_AWCACHE),
        .M00_AXI_awid(processing_system7_0_axi_periph_M00_AXI_AWID),
        .M00_AXI_awlen(processing_system7_0_axi_periph_M00_AXI_AWLEN),
        .M00_AXI_awlock(processing_system7_0_axi_periph_M00_AXI_AWLOCK),
        .M00_AXI_awprot(processing_system7_0_axi_periph_M00_AXI_AWPROT),
        .M00_AXI_awready(processing_system7_0_axi_periph_M00_AXI_AWREADY),
        .M00_AXI_awsize(processing_system7_0_axi_periph_M00_AXI_AWSIZE),
        .M00_AXI_awvalid(processing_system7_0_axi_periph_M00_AXI_AWVALID),
        .M00_AXI_bid(processing_system7_0_axi_periph_M00_AXI_BID),
        .M00_AXI_bready(processing_system7_0_axi_periph_M00_AXI_BREADY),
        .M00_AXI_bresp(processing_system7_0_axi_periph_M00_AXI_BRESP),
        .M00_AXI_bvalid(processing_system7_0_axi_periph_M00_AXI_BVALID),
        .M00_AXI_rdata(processing_system7_0_axi_periph_M00_AXI_RDATA),
        .M00_AXI_rid(processing_system7_0_axi_periph_M00_AXI_RID),
        .M00_AXI_rlast(processing_system7_0_axi_periph_M00_AXI_RLAST),
        .M00_AXI_rready(processing_system7_0_axi_periph_M00_AXI_RREADY),
        .M00_AXI_rresp(processing_system7_0_axi_periph_M00_AXI_RRESP),
        .M00_AXI_rvalid(processing_system7_0_axi_periph_M00_AXI_RVALID),
        .M00_AXI_wdata(processing_system7_0_axi_periph_M00_AXI_WDATA),
        .M00_AXI_wlast(processing_system7_0_axi_periph_M00_AXI_WLAST),
        .M00_AXI_wready(processing_system7_0_axi_periph_M00_AXI_WREADY),
        .M00_AXI_wstrb(processing_system7_0_axi_periph_M00_AXI_WSTRB),
        .M00_AXI_wvalid(processing_system7_0_axi_periph_M00_AXI_WVALID),
        .S00_ACLK(processing_system7_0_FCLK_CLK0),
        .S00_ARESETN(rst_processing_system7_0_50M_peripheral_aresetn),
        .S00_AXI_araddr(processing_system7_0_M_AXI_GP1_ARADDR),
        .S00_AXI_arburst(processing_system7_0_M_AXI_GP1_ARBURST),
        .S00_AXI_arcache(processing_system7_0_M_AXI_GP1_ARCACHE),
        .S00_AXI_arid(processing_system7_0_M_AXI_GP1_ARID),
        .S00_AXI_arlen(processing_system7_0_M_AXI_GP1_ARLEN),
        .S00_AXI_arlock(processing_system7_0_M_AXI_GP1_ARLOCK),
        .S00_AXI_arprot(processing_system7_0_M_AXI_GP1_ARPROT),
        .S00_AXI_arqos(processing_system7_0_M_AXI_GP1_ARQOS),
        .S00_AXI_arready(processing_system7_0_M_AXI_GP1_ARREADY),
        .S00_AXI_arsize(processing_system7_0_M_AXI_GP1_ARSIZE),
        .S00_AXI_arvalid(processing_system7_0_M_AXI_GP1_ARVALID),
        .S00_AXI_awaddr(processing_system7_0_M_AXI_GP1_AWADDR),
        .S00_AXI_awburst(processing_system7_0_M_AXI_GP1_AWBURST),
        .S00_AXI_awcache(processing_system7_0_M_AXI_GP1_AWCACHE),
        .S00_AXI_awid(processing_system7_0_M_AXI_GP1_AWID),
        .S00_AXI_awlen(processing_system7_0_M_AXI_GP1_AWLEN),
        .S00_AXI_awlock(processing_system7_0_M_AXI_GP1_AWLOCK),
        .S00_AXI_awprot(processing_system7_0_M_AXI_GP1_AWPROT),
        .S00_AXI_awqos(processing_system7_0_M_AXI_GP1_AWQOS),
        .S00_AXI_awready(processing_system7_0_M_AXI_GP1_AWREADY),
        .S00_AXI_awsize(processing_system7_0_M_AXI_GP1_AWSIZE),
        .S00_AXI_awvalid(processing_system7_0_M_AXI_GP1_AWVALID),
        .S00_AXI_bid(processing_system7_0_M_AXI_GP1_BID),
        .S00_AXI_bready(processing_system7_0_M_AXI_GP1_BREADY),
        .S00_AXI_bresp(processing_system7_0_M_AXI_GP1_BRESP),
        .S00_AXI_bvalid(processing_system7_0_M_AXI_GP1_BVALID),
        .S00_AXI_rdata(processing_system7_0_M_AXI_GP1_RDATA),
        .S00_AXI_rid(processing_system7_0_M_AXI_GP1_RID),
        .S00_AXI_rlast(processing_system7_0_M_AXI_GP1_RLAST),
        .S00_AXI_rready(processing_system7_0_M_AXI_GP1_RREADY),
        .S00_AXI_rresp(processing_system7_0_M_AXI_GP1_RRESP),
        .S00_AXI_rvalid(processing_system7_0_M_AXI_GP1_RVALID),
        .S00_AXI_wdata(processing_system7_0_M_AXI_GP1_WDATA),
        .S00_AXI_wid(processing_system7_0_M_AXI_GP1_WID),
        .S00_AXI_wlast(processing_system7_0_M_AXI_GP1_WLAST),
        .S00_AXI_wready(processing_system7_0_M_AXI_GP1_WREADY),
        .S00_AXI_wstrb(processing_system7_0_M_AXI_GP1_WSTRB),
        .S00_AXI_wvalid(processing_system7_0_M_AXI_GP1_WVALID));
dp_system_rst_processing_system7_0_50M_0 rst_processing_system7_0_50M
       (.aux_reset_in(VCC_1),
        .bus_struct_reset(rst_processing_system7_0_50M_bus_struct_reset),
        .dcm_locked(VCC_1),
        .ext_reset_in(Net2),
        .interconnect_aresetn(rst_processing_system7_0_50M_interconnect_aresetn),
        .mb_debug_sys_rst(GND_1),
        .peripheral_aresetn(rst_processing_system7_0_50M_peripheral_aresetn),
        .slowest_sync_clk(processing_system7_0_FCLK_CLK0));
endmodule

module dec_wo_cabac_imp_17VLMZ1
   (DDR3_addr,
    DDR3_ba,
    DDR3_cas_n,
    DDR3_ck_n,
    DDR3_ck_p,
    DDR3_cke,
    DDR3_cs_n,
    DDR3_dm,
    DDR3_dq,
    DDR3_dqs_n,
    DDR3_dqs_p,
    DDR3_odt,
    DDR3_ras_n,
    DDR3_reset_n,
    DDR3_we_n,
    SYS_CLK1_clk_n,
    SYS_CLK1_clk_p,
    cb_rd_data_count_in,
    cb_residual_read_inf_empty,
    cb_residual_read_inf_rd_data,
    cb_residual_read_inf_rd_en,
    config_fifo_read_info_rd_data,
    config_fifo_read_info_rd_en,
    cr_rd_data_count_in,
    cr_residual_read_inf_empty,
    cr_residual_read_inf_rd_data,
    cr_residual_read_inf_rd_en,
    design_reset,
    display_fifo_is_full,
    display_fifo_write_inf_wr_data,
    display_fifo_write_inf_wr_en,
    input_fifo_is_empty,
    pic_height_out,
    pic_width_out,
    sao_poc_out,
    sys_rst,
    ui_clk,
    y_rd_data_count_in,
    y_residual_interface_empty,
    y_residual_interface_rd_data,
    y_residual_interface_rd_en);
  output [13:0]DDR3_addr;
  output [2:0]DDR3_ba;
  output DDR3_cas_n;
  output [0:0]DDR3_ck_n;
  output [0:0]DDR3_ck_p;
  output [0:0]DDR3_cke;
  output [0:0]DDR3_cs_n;
  output [7:0]DDR3_dm;
  inout [63:0]DDR3_dq;
  inout [7:0]DDR3_dqs_n;
  inout [7:0]DDR3_dqs_p;
  output [0:0]DDR3_odt;
  output DDR3_ras_n;
  output DDR3_reset_n;
  output DDR3_we_n;
  input SYS_CLK1_clk_n;
  input SYS_CLK1_clk_p;
  input [10:0]cb_rd_data_count_in;
  input cb_residual_read_inf_empty;
  input [143:0]cb_residual_read_inf_rd_data;
  output cb_residual_read_inf_rd_en;
  input [31:0]config_fifo_read_info_rd_data;
  output config_fifo_read_info_rd_en;
  input [10:0]cr_rd_data_count_in;
  input cr_residual_read_inf_empty;
  input [143:0]cr_residual_read_inf_rd_data;
  output cr_residual_read_inf_rd_en;
  output design_reset;
  input display_fifo_is_full;
  output [783:0]display_fifo_write_inf_wr_data;
  output display_fifo_write_inf_wr_en;
  input input_fifo_is_empty;
  output [11:0]pic_height_out;
  output [11:0]pic_width_out;
  output [31:0]sao_poc_out;
  input [0:0]sys_rst;
  output ui_clk;
  input [12:0]y_rd_data_count_in;
  input y_residual_interface_empty;
  input [143:0]y_residual_interface_rd_data;
  output y_residual_interface_rd_en;

  wire [31:0]Conn1_RD_DATA;
  wire Conn1_RD_EN;
  wire Conn2_EMPTY;
  wire [143:0]Conn2_RD_DATA;
  wire Conn2_RD_EN;
  wire Conn3_EMPTY;
  wire [143:0]Conn3_RD_DATA;
  wire Conn3_RD_EN;
  wire Conn4_EMPTY;
  wire [143:0]Conn4_RD_DATA;
  wire Conn4_RD_EN;
  wire Conn5_CLK_N;
  wire Conn5_CLK_P;
  wire GND_1;
  wire VCC_1;
  wire [31:0]axi_inter_con_0_M00_AXI_ARADDR;
  wire [1:0]axi_inter_con_0_M00_AXI_ARBURST;
  wire [3:0]axi_inter_con_0_M00_AXI_ARCACHE;
  wire [3:0]axi_inter_con_0_M00_AXI_ARID;
  wire [7:0]axi_inter_con_0_M00_AXI_ARLEN;
  wire axi_inter_con_0_M00_AXI_ARLOCK;
  wire [2:0]axi_inter_con_0_M00_AXI_ARPROT;
  wire [3:0]axi_inter_con_0_M00_AXI_ARQOS;
  wire axi_inter_con_0_M00_AXI_ARREADY;
  wire [2:0]axi_inter_con_0_M00_AXI_ARSIZE;
  wire axi_inter_con_0_M00_AXI_ARVALID;
  wire [31:0]axi_inter_con_0_M00_AXI_AWADDR;
  wire [1:0]axi_inter_con_0_M00_AXI_AWBURST;
  wire [3:0]axi_inter_con_0_M00_AXI_AWCACHE;
  wire [3:0]axi_inter_con_0_M00_AXI_AWID;
  wire [7:0]axi_inter_con_0_M00_AXI_AWLEN;
  wire axi_inter_con_0_M00_AXI_AWLOCK;
  wire [2:0]axi_inter_con_0_M00_AXI_AWPROT;
  wire [3:0]axi_inter_con_0_M00_AXI_AWQOS;
  wire axi_inter_con_0_M00_AXI_AWREADY;
  wire [2:0]axi_inter_con_0_M00_AXI_AWSIZE;
  wire axi_inter_con_0_M00_AXI_AWVALID;
  wire [3:0]axi_inter_con_0_M00_AXI_BID;
  wire axi_inter_con_0_M00_AXI_BREADY;
  wire [1:0]axi_inter_con_0_M00_AXI_BRESP;
  wire axi_inter_con_0_M00_AXI_BVALID;
  wire [511:0]axi_inter_con_0_M00_AXI_RDATA;
  wire [3:0]axi_inter_con_0_M00_AXI_RID;
  wire axi_inter_con_0_M00_AXI_RLAST;
  wire axi_inter_con_0_M00_AXI_RREADY;
  wire [1:0]axi_inter_con_0_M00_AXI_RRESP;
  wire axi_inter_con_0_M00_AXI_RVALID;
  wire [511:0]axi_inter_con_0_M00_AXI_WDATA;
  wire axi_inter_con_0_M00_AXI_WLAST;
  wire axi_inter_con_0_M00_AXI_WREADY;
  wire [63:0]axi_inter_con_0_M00_AXI_WSTRB;
  wire axi_inter_con_0_M00_AXI_WVALID;
  wire [0:0]cabac_modules_bus_struct_reset;
  wire [10:0]cb_rd_data_count_in_1;
  wire [10:0]cr_rd_data_count_in_1;
  wire dis_buf_in_fifo_prog_full;
  wire [783:0]hevc_top_wo_dp_fifo_0_display_fifo_write_inf_WR_DATA;
  wire hevc_top_wo_dp_fifo_0_display_fifo_write_inf_WR_EN;
  wire [31:0]hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWADDR;
  wire [1:0]hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWBURST;
  wire [3:0]hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWCACHE;
  wire hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWID;
  wire [7:0]hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWLEN;
  wire hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWLOCK;
  wire [2:0]hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWPROT;
  wire hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWREADY;
  wire [2:0]hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWSIZE;
  wire hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWVALID;
  wire hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_BREADY;
  wire [1:0]hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_BRESP;
  wire hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_BVALID;
  wire [511:0]hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_WDATA;
  wire hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_WLAST;
  wire hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_WREADY;
  wire [63:0]hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_WSTRB;
  wire hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_WVALID;
  wire [31:0]hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARADDR;
  wire [1:0]hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARBURST;
  wire [3:0]hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARCACHE;
  wire hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARID;
  wire [7:0]hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARLEN;
  wire hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARLOCK;
  wire [2:0]hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARPROT;
  wire hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARREADY;
  wire [2:0]hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARSIZE;
  wire hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARVALID;
  wire [511:0]hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_RDATA;
  wire hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_RLAST;
  wire hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_RREADY;
  wire [1:0]hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_RRESP;
  wire hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_RVALID;
  wire [11:0]hevc_top_wo_dp_fifo_0_pic_height_out;
  wire [11:0]hevc_top_wo_dp_fifo_0_pic_width_out;
  wire [31:0]hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWADDR;
  wire [1:0]hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWBURST;
  wire [3:0]hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWCACHE;
  wire hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWID;
  wire [7:0]hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWLEN;
  wire hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWLOCK;
  wire [2:0]hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWPROT;
  wire hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWREADY;
  wire [2:0]hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWSIZE;
  wire hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWVALID;
  wire [0:0]hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_BID;
  wire hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_BREADY;
  wire [1:0]hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_BRESP;
  wire hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_BVALID;
  wire [511:0]hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_WDATA;
  wire hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_WLAST;
  wire hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_WREADY;
  wire [63:0]hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_WSTRB;
  wire hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_WVALID;
  wire [31:0]hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARADDR;
  wire [1:0]hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARBURST;
  wire [7:0]hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARLEN;
  wire [2:0]hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARPROT;
  wire hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARREADY;
  wire [2:0]hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARSIZE;
  wire hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARVALID;
  wire [511:0]hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_RDATA;
  wire hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_RLAST;
  wire hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_RREADY;
  wire [1:0]hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_RRESP;
  wire hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_RVALID;
  wire [31:0]hevc_top_wo_dp_fifo_0_sao_poc_out;
  wire input_fifo_is_empty_1;
  wire [13:0]mig_7series_0_DDR3_ADDR;
  wire [2:0]mig_7series_0_DDR3_BA;
  wire mig_7series_0_DDR3_CAS_N;
  wire [0:0]mig_7series_0_DDR3_CKE;
  wire [0:0]mig_7series_0_DDR3_CK_N;
  wire [0:0]mig_7series_0_DDR3_CK_P;
  wire [0:0]mig_7series_0_DDR3_CS_N;
  wire [7:0]mig_7series_0_DDR3_DM;
  wire [63:0]mig_7series_0_DDR3_DQ;
  wire [7:0]mig_7series_0_DDR3_DQS_N;
  wire [7:0]mig_7series_0_DDR3_DQS_P;
  wire [0:0]mig_7series_0_DDR3_ODT;
  wire mig_7series_0_DDR3_RAS_N;
  wire mig_7series_0_DDR3_RESET_N;
  wire mig_7series_0_DDR3_WE_N;
  wire mig_7series_0_init_calib_complete;
  wire mig_7series_0_mmcm_locked;
  wire mig_7series_0_ui_clk;
  wire mig_7series_0_ui_clk_sync_rst;
  wire mig_setup_reset_0_areset_n;
  wire mig_setup_reset_0_design_reset;
  wire mig_setup_reset_0_design_reset_n;
  wire [0:0]xlconstant_0_dout;
  wire [12:0]y_rd_data_count_in_1;

  assign Conn1_RD_DATA = config_fifo_read_info_rd_data[31:0];
  assign Conn2_EMPTY = y_residual_interface_empty;
  assign Conn2_RD_DATA = y_residual_interface_rd_data[143:0];
  assign Conn3_EMPTY = cb_residual_read_inf_empty;
  assign Conn3_RD_DATA = cb_residual_read_inf_rd_data[143:0];
  assign Conn4_EMPTY = cr_residual_read_inf_empty;
  assign Conn4_RD_DATA = cr_residual_read_inf_rd_data[143:0];
  assign Conn5_CLK_N = SYS_CLK1_clk_n;
  assign Conn5_CLK_P = SYS_CLK1_clk_p;
  assign DDR3_addr[13:0] = mig_7series_0_DDR3_ADDR;
  assign DDR3_ba[2:0] = mig_7series_0_DDR3_BA;
  assign DDR3_cas_n = mig_7series_0_DDR3_CAS_N;
  assign DDR3_ck_n[0] = mig_7series_0_DDR3_CK_N;
  assign DDR3_ck_p[0] = mig_7series_0_DDR3_CK_P;
  assign DDR3_cke[0] = mig_7series_0_DDR3_CKE;
  assign DDR3_cs_n[0] = mig_7series_0_DDR3_CS_N;
  assign DDR3_dm[7:0] = mig_7series_0_DDR3_DM;
  assign DDR3_odt[0] = mig_7series_0_DDR3_ODT;
  assign DDR3_ras_n = mig_7series_0_DDR3_RAS_N;
  assign DDR3_reset_n = mig_7series_0_DDR3_RESET_N;
  assign DDR3_we_n = mig_7series_0_DDR3_WE_N;
  assign cabac_modules_bus_struct_reset = sys_rst[0];
  assign cb_rd_data_count_in_1 = cb_rd_data_count_in[10:0];
  assign cb_residual_read_inf_rd_en = Conn3_RD_EN;
  assign config_fifo_read_info_rd_en = Conn1_RD_EN;
  assign cr_rd_data_count_in_1 = cr_rd_data_count_in[10:0];
  assign cr_residual_read_inf_rd_en = Conn4_RD_EN;
  assign design_reset = mig_setup_reset_0_design_reset;
  assign dis_buf_in_fifo_prog_full = display_fifo_is_full;
  assign display_fifo_write_inf_wr_data[783:0] = hevc_top_wo_dp_fifo_0_display_fifo_write_inf_WR_DATA;
  assign display_fifo_write_inf_wr_en = hevc_top_wo_dp_fifo_0_display_fifo_write_inf_WR_EN;
  assign input_fifo_is_empty_1 = input_fifo_is_empty;
  assign pic_height_out[11:0] = hevc_top_wo_dp_fifo_0_pic_height_out;
  assign pic_width_out[11:0] = hevc_top_wo_dp_fifo_0_pic_width_out;
  assign sao_poc_out[31:0] = hevc_top_wo_dp_fifo_0_sao_poc_out;
  assign ui_clk = mig_7series_0_ui_clk;
  assign y_rd_data_count_in_1 = y_rd_data_count_in[12:0];
  assign y_residual_interface_rd_en = Conn2_RD_EN;
GND GND
       (.G(GND_1));
VCC VCC
       (.P(VCC_1));
dp_system_axi_inter_con_0_0 axi_inter_con_0
       (.INTERCONNECT_ACLK(mig_7series_0_ui_clk),
        .INTERCONNECT_ARESETN(mig_setup_reset_0_design_reset_n),
        .M00_AXI_ACLK(mig_7series_0_ui_clk),
        .M00_AXI_ARADDR(axi_inter_con_0_M00_AXI_ARADDR),
        .M00_AXI_ARBURST(axi_inter_con_0_M00_AXI_ARBURST),
        .M00_AXI_ARCACHE(axi_inter_con_0_M00_AXI_ARCACHE),
        .M00_AXI_ARID(axi_inter_con_0_M00_AXI_ARID),
        .M00_AXI_ARLEN(axi_inter_con_0_M00_AXI_ARLEN),
        .M00_AXI_ARLOCK(axi_inter_con_0_M00_AXI_ARLOCK),
        .M00_AXI_ARPROT(axi_inter_con_0_M00_AXI_ARPROT),
        .M00_AXI_ARQOS(axi_inter_con_0_M00_AXI_ARQOS),
        .M00_AXI_ARREADY(axi_inter_con_0_M00_AXI_ARREADY),
        .M00_AXI_ARSIZE(axi_inter_con_0_M00_AXI_ARSIZE),
        .M00_AXI_ARVALID(axi_inter_con_0_M00_AXI_ARVALID),
        .M00_AXI_AWADDR(axi_inter_con_0_M00_AXI_AWADDR),
        .M00_AXI_AWBURST(axi_inter_con_0_M00_AXI_AWBURST),
        .M00_AXI_AWCACHE(axi_inter_con_0_M00_AXI_AWCACHE),
        .M00_AXI_AWID(axi_inter_con_0_M00_AXI_AWID),
        .M00_AXI_AWLEN(axi_inter_con_0_M00_AXI_AWLEN),
        .M00_AXI_AWLOCK(axi_inter_con_0_M00_AXI_AWLOCK),
        .M00_AXI_AWPROT(axi_inter_con_0_M00_AXI_AWPROT),
        .M00_AXI_AWQOS(axi_inter_con_0_M00_AXI_AWQOS),
        .M00_AXI_AWREADY(axi_inter_con_0_M00_AXI_AWREADY),
        .M00_AXI_AWSIZE(axi_inter_con_0_M00_AXI_AWSIZE),
        .M00_AXI_AWVALID(axi_inter_con_0_M00_AXI_AWVALID),
        .M00_AXI_BID(axi_inter_con_0_M00_AXI_BID),
        .M00_AXI_BREADY(axi_inter_con_0_M00_AXI_BREADY),
        .M00_AXI_BRESP(axi_inter_con_0_M00_AXI_BRESP),
        .M00_AXI_BVALID(axi_inter_con_0_M00_AXI_BVALID),
        .M00_AXI_RDATA(axi_inter_con_0_M00_AXI_RDATA),
        .M00_AXI_RID(axi_inter_con_0_M00_AXI_RID),
        .M00_AXI_RLAST(axi_inter_con_0_M00_AXI_RLAST),
        .M00_AXI_RREADY(axi_inter_con_0_M00_AXI_RREADY),
        .M00_AXI_RRESP(axi_inter_con_0_M00_AXI_RRESP),
        .M00_AXI_RVALID(axi_inter_con_0_M00_AXI_RVALID),
        .M00_AXI_WDATA(axi_inter_con_0_M00_AXI_WDATA),
        .M00_AXI_WLAST(axi_inter_con_0_M00_AXI_WLAST),
        .M00_AXI_WREADY(axi_inter_con_0_M00_AXI_WREADY),
        .M00_AXI_WSTRB(axi_inter_con_0_M00_AXI_WSTRB),
        .M00_AXI_WVALID(axi_inter_con_0_M00_AXI_WVALID),
        .S00_AXI_ACLK(mig_7series_0_ui_clk),
        .S00_AXI_ARADDR(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARADDR),
        .S00_AXI_ARBURST(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARBURST),
        .S00_AXI_ARCACHE({GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_ARID(GND_1),
        .S00_AXI_ARLEN(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARLEN),
        .S00_AXI_ARLOCK(GND_1),
        .S00_AXI_ARPROT(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARPROT),
        .S00_AXI_ARQOS({GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_ARREADY(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARREADY),
        .S00_AXI_ARSIZE(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARSIZE),
        .S00_AXI_ARVALID(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARVALID),
        .S00_AXI_AWADDR({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_AWBURST({VCC_1,VCC_1}),
        .S00_AXI_AWCACHE({GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_AWID(GND_1),
        .S00_AXI_AWLEN({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_AWLOCK(GND_1),
        .S00_AXI_AWPROT({GND_1,GND_1,GND_1}),
        .S00_AXI_AWQOS({GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_AWSIZE({GND_1,GND_1,GND_1}),
        .S00_AXI_AWVALID(GND_1),
        .S00_AXI_BREADY(GND_1),
        .S00_AXI_RDATA(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_RDATA),
        .S00_AXI_RLAST(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_RLAST),
        .S00_AXI_RREADY(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_RREADY),
        .S00_AXI_RRESP(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_RRESP),
        .S00_AXI_RVALID(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_RVALID),
        .S00_AXI_WDATA({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_WLAST(GND_1),
        .S00_AXI_WSTRB({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_WVALID(GND_1),
        .S01_AXI_ACLK(mig_7series_0_ui_clk),
        .S01_AXI_ARADDR({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S01_AXI_ARBURST({VCC_1,VCC_1}),
        .S01_AXI_ARCACHE({GND_1,GND_1,GND_1,GND_1}),
        .S01_AXI_ARID(GND_1),
        .S01_AXI_ARLEN({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S01_AXI_ARLOCK(GND_1),
        .S01_AXI_ARPROT({GND_1,GND_1,GND_1}),
        .S01_AXI_ARQOS({GND_1,GND_1,GND_1,GND_1}),
        .S01_AXI_ARSIZE({GND_1,GND_1,GND_1}),
        .S01_AXI_ARVALID(GND_1),
        .S01_AXI_AWADDR(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWADDR),
        .S01_AXI_AWBURST(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWBURST),
        .S01_AXI_AWCACHE(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWCACHE),
        .S01_AXI_AWID(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWID),
        .S01_AXI_AWLEN(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWLEN),
        .S01_AXI_AWLOCK(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWLOCK),
        .S01_AXI_AWPROT(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWPROT),
        .S01_AXI_AWQOS({GND_1,GND_1,GND_1,GND_1}),
        .S01_AXI_AWREADY(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWREADY),
        .S01_AXI_AWSIZE(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWSIZE),
        .S01_AXI_AWVALID(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWVALID),
        .S01_AXI_BID(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_BID),
        .S01_AXI_BREADY(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_BREADY),
        .S01_AXI_BRESP(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_BRESP),
        .S01_AXI_BVALID(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_BVALID),
        .S01_AXI_RREADY(GND_1),
        .S01_AXI_WDATA(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_WDATA),
        .S01_AXI_WLAST(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_WLAST),
        .S01_AXI_WREADY(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_WREADY),
        .S01_AXI_WSTRB(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_WSTRB),
        .S01_AXI_WVALID(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_WVALID),
        .S02_AXI_ACLK(mig_7series_0_ui_clk),
        .S02_AXI_ARADDR(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARADDR),
        .S02_AXI_ARBURST(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARBURST),
        .S02_AXI_ARCACHE(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARCACHE),
        .S02_AXI_ARID(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARID),
        .S02_AXI_ARLEN(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARLEN),
        .S02_AXI_ARLOCK(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARLOCK),
        .S02_AXI_ARPROT(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARPROT),
        .S02_AXI_ARQOS({GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_ARREADY(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARREADY),
        .S02_AXI_ARSIZE(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARSIZE),
        .S02_AXI_ARVALID(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARVALID),
        .S02_AXI_AWADDR({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_AWBURST({VCC_1,VCC_1}),
        .S02_AXI_AWCACHE({GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_AWID(GND_1),
        .S02_AXI_AWLEN({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_AWLOCK(GND_1),
        .S02_AXI_AWPROT({GND_1,GND_1,GND_1}),
        .S02_AXI_AWQOS({GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_AWSIZE({GND_1,GND_1,GND_1}),
        .S02_AXI_AWVALID(GND_1),
        .S02_AXI_BREADY(GND_1),
        .S02_AXI_RDATA(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_RDATA),
        .S02_AXI_RLAST(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_RLAST),
        .S02_AXI_RREADY(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_RREADY),
        .S02_AXI_RRESP(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_RRESP),
        .S02_AXI_RVALID(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_RVALID),
        .S02_AXI_WDATA({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_WLAST(GND_1),
        .S02_AXI_WSTRB({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_WVALID(GND_1),
        .S03_AXI_ACLK(mig_7series_0_ui_clk),
        .S03_AXI_ARADDR({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S03_AXI_ARBURST({VCC_1,VCC_1}),
        .S03_AXI_ARCACHE({GND_1,GND_1,GND_1,GND_1}),
        .S03_AXI_ARID(GND_1),
        .S03_AXI_ARLEN({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S03_AXI_ARLOCK(GND_1),
        .S03_AXI_ARPROT({GND_1,GND_1,GND_1}),
        .S03_AXI_ARQOS({GND_1,GND_1,GND_1,GND_1}),
        .S03_AXI_ARSIZE({GND_1,GND_1,GND_1}),
        .S03_AXI_ARVALID(GND_1),
        .S03_AXI_AWADDR(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWADDR),
        .S03_AXI_AWBURST(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWBURST),
        .S03_AXI_AWCACHE(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWCACHE),
        .S03_AXI_AWID(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWID),
        .S03_AXI_AWLEN(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWLEN),
        .S03_AXI_AWLOCK(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWLOCK),
        .S03_AXI_AWPROT(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWPROT),
        .S03_AXI_AWQOS({GND_1,GND_1,GND_1,GND_1}),
        .S03_AXI_AWREADY(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWREADY),
        .S03_AXI_AWSIZE(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWSIZE),
        .S03_AXI_AWVALID(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWVALID),
        .S03_AXI_BREADY(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_BREADY),
        .S03_AXI_BRESP(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_BRESP),
        .S03_AXI_BVALID(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_BVALID),
        .S03_AXI_RREADY(GND_1),
        .S03_AXI_WDATA(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_WDATA),
        .S03_AXI_WLAST(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_WLAST),
        .S03_AXI_WREADY(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_WREADY),
        .S03_AXI_WSTRB(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_WSTRB),
        .S03_AXI_WVALID(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_WVALID));
dp_system_hevc_top_wo_dp_fifo_0_0 hevc_top_wo_dp_fifo_0
       (.cb_rd_data_count_in(cb_rd_data_count_in_1),
        .cb_residual_fifo_in(Conn3_RD_DATA),
        .cb_residual_fifo_is_empty_in(Conn3_EMPTY),
        .cb_residual_fifo_read_en_out(Conn3_RD_EN),
        .clk(mig_7series_0_ui_clk),
        .cr_rd_data_count_in(cr_rd_data_count_in_1),
        .cr_residual_fifo_in(Conn4_RD_DATA),
        .cr_residual_fifo_is_empty_in(Conn4_EMPTY),
        .cr_residual_fifo_read_en_out(Conn4_RD_EN),
        .display_fifo_data(hevc_top_wo_dp_fifo_0_display_fifo_write_inf_WR_DATA),
        .display_fifo_data_wr_en(hevc_top_wo_dp_fifo_0_display_fifo_write_inf_WR_EN),
        .display_fifo_is_full(dis_buf_in_fifo_prog_full),
        .enable(xlconstant_0_dout),
        .fifo_in(Conn1_RD_DATA),
        .input_fifo_is_empty(input_fifo_is_empty_1),
        .mv_col_axi_awaddr(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWADDR),
        .mv_col_axi_awburst(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWBURST),
        .mv_col_axi_awcache(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWCACHE),
        .mv_col_axi_awid(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWID),
        .mv_col_axi_awlen(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWLEN),
        .mv_col_axi_awlock(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWLOCK),
        .mv_col_axi_awprot(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWPROT),
        .mv_col_axi_awready(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWREADY),
        .mv_col_axi_awsize(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWSIZE),
        .mv_col_axi_awvalid(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_AWVALID),
        .mv_col_axi_bready(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_BREADY),
        .mv_col_axi_bresp(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_BRESP),
        .mv_col_axi_bvalid(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_BVALID),
        .mv_col_axi_wdata(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_WDATA),
        .mv_col_axi_wlast(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_WLAST),
        .mv_col_axi_wready(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_WREADY),
        .mv_col_axi_wstrb(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_WSTRB),
        .mv_col_axi_wvalid(hevc_top_wo_dp_fifo_0_mv_col_axi_master_inf_WVALID),
        .mv_pref_axi_araddr(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARADDR),
        .mv_pref_axi_arburst(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARBURST),
        .mv_pref_axi_arcache(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARCACHE),
        .mv_pref_axi_arid(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARID),
        .mv_pref_axi_arlen(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARLEN),
        .mv_pref_axi_arlock(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARLOCK),
        .mv_pref_axi_arprot(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARPROT),
        .mv_pref_axi_arready(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARREADY),
        .mv_pref_axi_arsize(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARSIZE),
        .mv_pref_axi_arvalid(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_ARVALID),
        .mv_pref_axi_rdata(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_RDATA),
        .mv_pref_axi_rlast(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_RLAST),
        .mv_pref_axi_rready(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_RREADY),
        .mv_pref_axi_rresp(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_RRESP),
        .mv_pref_axi_rvalid(hevc_top_wo_dp_fifo_0_mv_pref_axi_master_inf_RVALID),
        .pic_height_out(hevc_top_wo_dp_fifo_0_pic_height_out),
        .pic_width_out(hevc_top_wo_dp_fifo_0_pic_width_out),
        .read_en_out(Conn1_RD_EN),
        .ref_pic_write_axi_awaddr(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWADDR),
        .ref_pic_write_axi_awburst(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWBURST),
        .ref_pic_write_axi_awcache(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWCACHE),
        .ref_pic_write_axi_awid(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWID),
        .ref_pic_write_axi_awlen(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWLEN),
        .ref_pic_write_axi_awlock(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWLOCK),
        .ref_pic_write_axi_awprot(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWPROT),
        .ref_pic_write_axi_awready(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWREADY),
        .ref_pic_write_axi_awsize(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWSIZE),
        .ref_pic_write_axi_awvalid(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_AWVALID),
        .ref_pic_write_axi_bid(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_BID),
        .ref_pic_write_axi_bready(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_BREADY),
        .ref_pic_write_axi_bresp(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_BRESP),
        .ref_pic_write_axi_bvalid(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_BVALID),
        .ref_pic_write_axi_wdata(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_WDATA),
        .ref_pic_write_axi_wlast(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_WLAST),
        .ref_pic_write_axi_wready(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_WREADY),
        .ref_pic_write_axi_wstrb(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_WSTRB),
        .ref_pic_write_axi_wvalid(hevc_top_wo_dp_fifo_0_ref_pic_write_axi_master_inf_WVALID),
        .ref_pix_axi_ar_addr(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARADDR),
        .ref_pix_axi_ar_burst(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARBURST),
        .ref_pix_axi_ar_len(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARLEN),
        .ref_pix_axi_ar_prot(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARPROT),
        .ref_pix_axi_ar_ready(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARREADY),
        .ref_pix_axi_ar_size(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARSIZE),
        .ref_pix_axi_ar_valid(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_ARVALID),
        .ref_pix_axi_r_data(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_RDATA),
        .ref_pix_axi_r_last(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_RLAST),
        .ref_pix_axi_r_ready(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_RREADY),
        .ref_pix_axi_r_resp(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_RRESP),
        .ref_pix_axi_r_valid(hevc_top_wo_dp_fifo_0_ref_pix_axi_mastr_inf_RVALID),
        .reset(mig_setup_reset_0_design_reset),
        .sao_poc_out(hevc_top_wo_dp_fifo_0_sao_poc_out),
        .y_rd_data_count_in(y_rd_data_count_in_1),
        .y_residual_fifo_in(Conn2_RD_DATA),
        .y_residual_fifo_is_empty_in(Conn2_EMPTY),
        .y_residual_fifo_read_en_out(Conn2_RD_EN));
dp_system_mig_7series_0_0 mig_7series_0
       (.aresetn(mig_setup_reset_0_areset_n),
        .ddr3_addr(mig_7series_0_DDR3_ADDR),
        .ddr3_ba(mig_7series_0_DDR3_BA),
        .ddr3_cas_n(mig_7series_0_DDR3_CAS_N),
        .ddr3_ck_n(mig_7series_0_DDR3_CK_N),
        .ddr3_ck_p(mig_7series_0_DDR3_CK_P),
        .ddr3_cke(mig_7series_0_DDR3_CKE),
        .ddr3_cs_n(mig_7series_0_DDR3_CS_N),
        .ddr3_dm(mig_7series_0_DDR3_DM),
        .ddr3_dq(DDR3_dq[63:0]),
        .ddr3_dqs_n(DDR3_dqs_n[7:0]),
        .ddr3_dqs_p(DDR3_dqs_p[7:0]),
        .ddr3_odt(mig_7series_0_DDR3_ODT),
        .ddr3_ras_n(mig_7series_0_DDR3_RAS_N),
        .ddr3_reset_n(mig_7series_0_DDR3_RESET_N),
        .ddr3_we_n(mig_7series_0_DDR3_WE_N),
        .init_calib_complete(mig_7series_0_init_calib_complete),
        .mmcm_locked(mig_7series_0_mmcm_locked),
        .s_axi_araddr(axi_inter_con_0_M00_AXI_ARADDR[29:0]),
        .s_axi_arburst(axi_inter_con_0_M00_AXI_ARBURST),
        .s_axi_arcache(axi_inter_con_0_M00_AXI_ARCACHE),
        .s_axi_arid(axi_inter_con_0_M00_AXI_ARID),
        .s_axi_arlen(axi_inter_con_0_M00_AXI_ARLEN),
        .s_axi_arlock(axi_inter_con_0_M00_AXI_ARLOCK),
        .s_axi_arprot(axi_inter_con_0_M00_AXI_ARPROT),
        .s_axi_arqos(axi_inter_con_0_M00_AXI_ARQOS),
        .s_axi_arready(axi_inter_con_0_M00_AXI_ARREADY),
        .s_axi_arsize(axi_inter_con_0_M00_AXI_ARSIZE),
        .s_axi_arvalid(axi_inter_con_0_M00_AXI_ARVALID),
        .s_axi_awaddr(axi_inter_con_0_M00_AXI_AWADDR[29:0]),
        .s_axi_awburst(axi_inter_con_0_M00_AXI_AWBURST),
        .s_axi_awcache(axi_inter_con_0_M00_AXI_AWCACHE),
        .s_axi_awid(axi_inter_con_0_M00_AXI_AWID),
        .s_axi_awlen(axi_inter_con_0_M00_AXI_AWLEN),
        .s_axi_awlock(axi_inter_con_0_M00_AXI_AWLOCK),
        .s_axi_awprot(axi_inter_con_0_M00_AXI_AWPROT),
        .s_axi_awqos(axi_inter_con_0_M00_AXI_AWQOS),
        .s_axi_awready(axi_inter_con_0_M00_AXI_AWREADY),
        .s_axi_awsize(axi_inter_con_0_M00_AXI_AWSIZE),
        .s_axi_awvalid(axi_inter_con_0_M00_AXI_AWVALID),
        .s_axi_bid(axi_inter_con_0_M00_AXI_BID),
        .s_axi_bready(axi_inter_con_0_M00_AXI_BREADY),
        .s_axi_bresp(axi_inter_con_0_M00_AXI_BRESP),
        .s_axi_bvalid(axi_inter_con_0_M00_AXI_BVALID),
        .s_axi_rdata(axi_inter_con_0_M00_AXI_RDATA),
        .s_axi_rid(axi_inter_con_0_M00_AXI_RID),
        .s_axi_rlast(axi_inter_con_0_M00_AXI_RLAST),
        .s_axi_rready(axi_inter_con_0_M00_AXI_RREADY),
        .s_axi_rresp(axi_inter_con_0_M00_AXI_RRESP),
        .s_axi_rvalid(axi_inter_con_0_M00_AXI_RVALID),
        .s_axi_wdata(axi_inter_con_0_M00_AXI_WDATA),
        .s_axi_wlast(axi_inter_con_0_M00_AXI_WLAST),
        .s_axi_wready(axi_inter_con_0_M00_AXI_WREADY),
        .s_axi_wstrb(axi_inter_con_0_M00_AXI_WSTRB),
        .s_axi_wvalid(axi_inter_con_0_M00_AXI_WVALID),
        .sys_clk_n(Conn5_CLK_N),
        .sys_clk_p(Conn5_CLK_P),
        .sys_rst(cabac_modules_bus_struct_reset),
        .ui_clk(mig_7series_0_ui_clk),
        .ui_clk_sync_rst(mig_7series_0_ui_clk_sync_rst));
dp_system_mig_setup_reset_0_0 mig_setup_reset_0
       (.areset_n(mig_setup_reset_0_areset_n),
        .clk(mig_7series_0_ui_clk),
        .design_reset(mig_setup_reset_0_design_reset),
        .design_reset_n(mig_setup_reset_0_design_reset_n),
        .init_calib_complete(mig_7series_0_init_calib_complete),
        .mmcm_locked(mig_7series_0_mmcm_locked),
        .rst(mig_7series_0_ui_clk_sync_rst));
dp_system_xlconstant_0_2 xlconstant_0
       (.dout(xlconstant_0_dout));
endmodule

module dp_modules_imp_TAI6SH
   (D,
    aux_tx_io_n,
    aux_tx_io_p,
    axi_int,
    clk_out1,
    dp_fifo_read_empty,
    dp_fifo_read_rd_data,
    dp_fifo_read_rd_en,
    dp_mainlink_lnk_clk,
    dp_mainlink_lnk_clk_n,
    dp_mainlink_lnk_clk_p,
    dp_mainlink_lnk_tx_lane_n,
    dp_mainlink_lnk_tx_lane_p,
    dp_s_axilite_araddr,
    dp_s_axilite_arprot,
    dp_s_axilite_arready,
    dp_s_axilite_arvalid,
    dp_s_axilite_awaddr,
    dp_s_axilite_awprot,
    dp_s_axilite_awready,
    dp_s_axilite_awvalid,
    dp_s_axilite_bready,
    dp_s_axilite_bresp,
    dp_s_axilite_bvalid,
    dp_s_axilite_rdata,
    dp_s_axilite_rready,
    dp_s_axilite_rresp,
    dp_s_axilite_rvalid,
    dp_s_axilite_wdata,
    dp_s_axilite_wready,
    dp_s_axilite_wstrb,
    dp_s_axilite_wvalid,
    fifo_prog_empty,
    progclk_i,
    s_axi_aclk,
    s_axi_aresetn,
    tx_hpd,
    vid_pat_apb_inf_paddr,
    vid_pat_apb_inf_penable,
    vid_pat_apb_inf_prdata,
    vid_pat_apb_inf_pready,
    vid_pat_apb_inf_psel,
    vid_pat_apb_inf_pslverr,
    vid_pat_apb_inf_pwdata,
    vid_pat_apb_inf_pwrite);
  input [0:0]D;
  inout aux_tx_io_n;
  inout aux_tx_io_p;
  output axi_int;
  output clk_out1;
  input dp_fifo_read_empty;
  input [63:0]dp_fifo_read_rd_data;
  output dp_fifo_read_rd_en;
  output dp_mainlink_lnk_clk;
  input dp_mainlink_lnk_clk_n;
  input dp_mainlink_lnk_clk_p;
  output [3:0]dp_mainlink_lnk_tx_lane_n;
  output [3:0]dp_mainlink_lnk_tx_lane_p;
  input [31:0]dp_s_axilite_araddr;
  input [2:0]dp_s_axilite_arprot;
  output dp_s_axilite_arready;
  input dp_s_axilite_arvalid;
  input [31:0]dp_s_axilite_awaddr;
  input [2:0]dp_s_axilite_awprot;
  output dp_s_axilite_awready;
  input dp_s_axilite_awvalid;
  input dp_s_axilite_bready;
  output [1:0]dp_s_axilite_bresp;
  output dp_s_axilite_bvalid;
  output [31:0]dp_s_axilite_rdata;
  input dp_s_axilite_rready;
  output [1:0]dp_s_axilite_rresp;
  output dp_s_axilite_rvalid;
  input [31:0]dp_s_axilite_wdata;
  output dp_s_axilite_wready;
  input [3:0]dp_s_axilite_wstrb;
  input dp_s_axilite_wvalid;
  input fifo_prog_empty;
  input progclk_i;
  input s_axi_aclk;
  input [0:0]s_axi_aresetn;
  input tx_hpd;
  input [11:0]vid_pat_apb_inf_paddr;
  input vid_pat_apb_inf_penable;
  output [31:0]vid_pat_apb_inf_prdata;
  output vid_pat_apb_inf_pready;
  input vid_pat_apb_inf_psel;
  output vid_pat_apb_inf_pslverr;
  input [31:0]vid_pat_apb_inf_pwdata;
  input vid_pat_apb_inf_pwrite;

  wire Conn1_EMPTY;
  wire [63:0]Conn1_RD_DATA;
  wire Conn1_RD_EN;
(* MARK_DEBUG *)   wire [12:0]DE_count;
  wire Net;
  wire Net1;
  wire [11:0]axi4_lite_peripherals_APB_M_PADDR;
  wire axi4_lite_peripherals_APB_M_PENABLE;
  wire [31:0]axi4_lite_peripherals_APB_M_PRDATA;
  wire axi4_lite_peripherals_APB_M_PREADY;
  wire axi4_lite_peripherals_APB_M_PSEL;
  wire axi4_lite_peripherals_APB_M_PSLVERR;
  wire [31:0]axi4_lite_peripherals_APB_M_PWDATA;
  wire axi4_lite_peripherals_APB_M_PWRITE;
  wire [31:0]axi4_lite_peripherals_M03_AXI_ARADDR;
  wire [2:0]axi4_lite_peripherals_M03_AXI_ARPROT;
  wire axi4_lite_peripherals_M03_AXI_ARREADY;
  wire axi4_lite_peripherals_M03_AXI_ARVALID;
  wire [31:0]axi4_lite_peripherals_M03_AXI_AWADDR;
  wire [2:0]axi4_lite_peripherals_M03_AXI_AWPROT;
  wire axi4_lite_peripherals_M03_AXI_AWREADY;
  wire axi4_lite_peripherals_M03_AXI_AWVALID;
  wire axi4_lite_peripherals_M03_AXI_BREADY;
  wire [1:0]axi4_lite_peripherals_M03_AXI_BRESP;
  wire axi4_lite_peripherals_M03_AXI_BVALID;
  wire [31:0]axi4_lite_peripherals_M03_AXI_RDATA;
  wire axi4_lite_peripherals_M03_AXI_RREADY;
  wire [1:0]axi4_lite_peripherals_M03_AXI_RRESP;
  wire axi4_lite_peripherals_M03_AXI_RVALID;
  wire [31:0]axi4_lite_peripherals_M03_AXI_WDATA;
  wire axi4_lite_peripherals_M03_AXI_WREADY;
  wire [3:0]axi4_lite_peripherals_M03_AXI_WSTRB;
  wire axi4_lite_peripherals_M03_AXI_WVALID;
  wire [0:0]c_shift_ram_0_Q;
  wire [0:0]c_shift_ram_1_Q;
  wire clk_wiz_1_clk_out1;
  wire clk_wiz_1_clk_out3;
  wire clk_wiz_2_clk_out1;
  wire clk_wiz_2_locked;
  wire displayport_0_axi_int;
  wire displayport_0_dp_mainlink_LNK_CLK;
  wire displayport_0_dp_mainlink_LNK_CLK_N;
  wire displayport_0_dp_mainlink_LNK_CLK_P;
  wire [3:0]displayport_0_dp_mainlink_LNK_TX_LANE_N;
  wire [3:0]displayport_0_dp_mainlink_LNK_TX_LANE_P;
  wire fifo_prog_empty_1;
(* MARK_DEBUG *)   wire [12:0]hsync_count;
(* MARK_DEBUG *)   wire [23:0]pclk_count;
  wire [0:0]proc_sys_reset_1_peripheral_aresetn;
  wire tx_hpd_1;
  wire [0:0]util_vector_logic_0_Res;
  wire [0:0]util_vector_logic_0_Res1;
  wire [0:0]util_vector_logic_1_Res;
(* MARK_DEBUG *)   wire video_tx_top_dp_1_dp_video_inf_TX_VID_ENABLE;
(* MARK_DEBUG *)   wire video_tx_top_dp_1_dp_video_inf_TX_VID_HSYNC;
(* MARK_DEBUG *)   wire video_tx_top_dp_1_dp_video_inf_TX_VID_ODDEVEN;
(* MARK_DEBUG *)   wire [47:0]video_tx_top_dp_1_dp_video_inf_TX_VID_PIXEL0;
(* MARK_DEBUG *)   wire [47:0]video_tx_top_dp_1_dp_video_inf_TX_VID_PIXEL1;
(* MARK_DEBUG *)   wire video_tx_top_dp_1_dp_video_inf_TX_VID_VSYNC;
  wire [0:0]xlconstant_0_dout;
  wire [0:0]xlconstant_1_dout;

  assign Conn1_EMPTY = dp_fifo_read_empty;
  assign Conn1_RD_DATA = dp_fifo_read_rd_data[63:0];
  assign axi4_lite_peripherals_APB_M_PADDR = vid_pat_apb_inf_paddr[11:0];
  assign axi4_lite_peripherals_APB_M_PENABLE = vid_pat_apb_inf_penable;
  assign axi4_lite_peripherals_APB_M_PSEL = vid_pat_apb_inf_psel;
  assign axi4_lite_peripherals_APB_M_PWDATA = vid_pat_apb_inf_pwdata[31:0];
  assign axi4_lite_peripherals_APB_M_PWRITE = vid_pat_apb_inf_pwrite;
  assign axi4_lite_peripherals_M03_AXI_ARADDR = dp_s_axilite_araddr[31:0];
  assign axi4_lite_peripherals_M03_AXI_ARPROT = dp_s_axilite_arprot[2:0];
  assign axi4_lite_peripherals_M03_AXI_ARVALID = dp_s_axilite_arvalid;
  assign axi4_lite_peripherals_M03_AXI_AWADDR = dp_s_axilite_awaddr[31:0];
  assign axi4_lite_peripherals_M03_AXI_AWPROT = dp_s_axilite_awprot[2:0];
  assign axi4_lite_peripherals_M03_AXI_AWVALID = dp_s_axilite_awvalid;
  assign axi4_lite_peripherals_M03_AXI_BREADY = dp_s_axilite_bready;
  assign axi4_lite_peripherals_M03_AXI_RREADY = dp_s_axilite_rready;
  assign axi4_lite_peripherals_M03_AXI_WDATA = dp_s_axilite_wdata[31:0];
  assign axi4_lite_peripherals_M03_AXI_WSTRB = dp_s_axilite_wstrb[3:0];
  assign axi4_lite_peripherals_M03_AXI_WVALID = dp_s_axilite_wvalid;
  assign axi_int = displayport_0_axi_int;
  assign clk_out1 = clk_wiz_2_clk_out1;
  assign clk_wiz_1_clk_out1 = progclk_i;
  assign clk_wiz_1_clk_out3 = s_axi_aclk;
  assign displayport_0_dp_mainlink_LNK_CLK_N = dp_mainlink_lnk_clk_n;
  assign displayport_0_dp_mainlink_LNK_CLK_P = dp_mainlink_lnk_clk_p;
  assign dp_fifo_read_rd_en = Conn1_RD_EN;
  assign dp_mainlink_lnk_clk = displayport_0_dp_mainlink_LNK_CLK;
  assign dp_mainlink_lnk_tx_lane_n[3:0] = displayport_0_dp_mainlink_LNK_TX_LANE_N;
  assign dp_mainlink_lnk_tx_lane_p[3:0] = displayport_0_dp_mainlink_LNK_TX_LANE_P;
  assign dp_s_axilite_arready = axi4_lite_peripherals_M03_AXI_ARREADY;
  assign dp_s_axilite_awready = axi4_lite_peripherals_M03_AXI_AWREADY;
  assign dp_s_axilite_bresp[1:0] = axi4_lite_peripherals_M03_AXI_BRESP;
  assign dp_s_axilite_bvalid = axi4_lite_peripherals_M03_AXI_BVALID;
  assign dp_s_axilite_rdata[31:0] = axi4_lite_peripherals_M03_AXI_RDATA;
  assign dp_s_axilite_rresp[1:0] = axi4_lite_peripherals_M03_AXI_RRESP;
  assign dp_s_axilite_rvalid = axi4_lite_peripherals_M03_AXI_RVALID;
  assign dp_s_axilite_wready = axi4_lite_peripherals_M03_AXI_WREADY;
  assign fifo_prog_empty_1 = fifo_prog_empty;
  assign proc_sys_reset_1_peripheral_aresetn = s_axi_aresetn[0];
  assign tx_hpd_1 = tx_hpd;
  assign util_vector_logic_0_Res = D[0];
  assign vid_pat_apb_inf_prdata[31:0] = axi4_lite_peripherals_APB_M_PRDATA;
  assign vid_pat_apb_inf_pready = axi4_lite_peripherals_APB_M_PREADY;
  assign vid_pat_apb_inf_pslverr = axi4_lite_peripherals_APB_M_PSLVERR;
dp_system_c_shift_ram_0_0 axi_lite_reset_shift_reg
       (.CLK(clk_wiz_1_clk_out3),
        .D(util_vector_logic_0_Res),
        .Q(c_shift_ram_0_Q));
dp_system_clk_wiz_2_0 clk_wiz_2
       (.clk_in1(clk_wiz_1_clk_out1),
        .clk_out1(clk_wiz_2_clk_out1),
        .locked(clk_wiz_2_locked),
        .reset(c_shift_ram_0_Q));
dp_system_displayport_0_0 displayport_0
       (.aux_tx_io_n(aux_tx_io_n),
        .aux_tx_io_p(aux_tx_io_p),
        .axi_int(displayport_0_axi_int),
        .lnk_clk(displayport_0_dp_mainlink_LNK_CLK),
        .lnk_clk_n(displayport_0_dp_mainlink_LNK_CLK_N),
        .lnk_clk_p(displayport_0_dp_mainlink_LNK_CLK_P),
        .lnk_tx_lane_n(displayport_0_dp_mainlink_LNK_TX_LANE_N),
        .lnk_tx_lane_p(displayport_0_dp_mainlink_LNK_TX_LANE_P),
        .s_axi_aclk(clk_wiz_1_clk_out3),
        .s_axi_araddr(axi4_lite_peripherals_M03_AXI_ARADDR),
        .s_axi_aresetn(proc_sys_reset_1_peripheral_aresetn),
        .s_axi_arprot(axi4_lite_peripherals_M03_AXI_ARPROT),
        .s_axi_arready(axi4_lite_peripherals_M03_AXI_ARREADY),
        .s_axi_arvalid(axi4_lite_peripherals_M03_AXI_ARVALID),
        .s_axi_awaddr(axi4_lite_peripherals_M03_AXI_AWADDR),
        .s_axi_awprot(axi4_lite_peripherals_M03_AXI_AWPROT),
        .s_axi_awready(axi4_lite_peripherals_M03_AXI_AWREADY),
        .s_axi_awvalid(axi4_lite_peripherals_M03_AXI_AWVALID),
        .s_axi_bready(axi4_lite_peripherals_M03_AXI_BREADY),
        .s_axi_bresp(axi4_lite_peripherals_M03_AXI_BRESP),
        .s_axi_bvalid(axi4_lite_peripherals_M03_AXI_BVALID),
        .s_axi_rdata(axi4_lite_peripherals_M03_AXI_RDATA),
        .s_axi_rready(axi4_lite_peripherals_M03_AXI_RREADY),
        .s_axi_rresp(axi4_lite_peripherals_M03_AXI_RRESP),
        .s_axi_rvalid(axi4_lite_peripherals_M03_AXI_RVALID),
        .s_axi_wdata(axi4_lite_peripherals_M03_AXI_WDATA),
        .s_axi_wready(axi4_lite_peripherals_M03_AXI_WREADY),
        .s_axi_wstrb(axi4_lite_peripherals_M03_AXI_WSTRB),
        .s_axi_wvalid(axi4_lite_peripherals_M03_AXI_WVALID),
        .tx_hpd(tx_hpd_1),
        .tx_vid_clk(clk_wiz_2_clk_out1),
        .tx_vid_enable(video_tx_top_dp_1_dp_video_inf_TX_VID_ENABLE),
        .tx_vid_hsync(video_tx_top_dp_1_dp_video_inf_TX_VID_HSYNC),
        .tx_vid_oddeven(video_tx_top_dp_1_dp_video_inf_TX_VID_ODDEVEN),
        .tx_vid_pixel0(video_tx_top_dp_1_dp_video_inf_TX_VID_PIXEL0),
        .tx_vid_pixel1(video_tx_top_dp_1_dp_video_inf_TX_VID_PIXEL1),
        .tx_vid_rst(c_shift_ram_1_Q),
        .tx_vid_vsync(video_tx_top_dp_1_dp_video_inf_TX_VID_VSYNC));
dp_system_c_shift_ram_1_0 dp_tx_vid_reset_shift_reg
       (.CLK(clk_wiz_1_clk_out3),
        .D(util_vector_logic_0_Res),
        .Q(c_shift_ram_1_Q));
dp_system_util_vector_logic_0_1 util_vector_logic_0
       (.Op1(fifo_prog_empty_1),
        .Res(util_vector_logic_0_Res1));
dp_system_util_vector_logic_1_0 util_vector_logic_1
       (.Op1(clk_wiz_2_locked),
        .Res(util_vector_logic_1_Res));
dp_system_video_pat_gen_0_0 video_pat_gen_0
       (.apb_addr(axi4_lite_peripherals_APB_M_PADDR),
        .apb_clk(clk_wiz_1_clk_out3),
        .apb_enable(axi4_lite_peripherals_APB_M_PENABLE),
        .apb_pready(axi4_lite_peripherals_APB_M_PREADY),
        .apb_pslverr(axi4_lite_peripherals_APB_M_PSLVERR),
        .apb_rdata(axi4_lite_peripherals_APB_M_PRDATA),
        .apb_reset(c_shift_ram_0_Q),
        .apb_select(axi4_lite_peripherals_APB_M_PSEL),
        .apb_wdata(axi4_lite_peripherals_APB_M_PWDATA),
        .apb_write(axi4_lite_peripherals_APB_M_PWRITE),
        .run_pattern(xlconstant_1_dout),
        .vid_clk(clk_wiz_2_clk_out1),
        .vid_reset(util_vector_logic_1_Res));
dp_system_video_transmitter_0_1 video_transmitter_0
       (.DE_count(DE_count),
        .config_done(xlconstant_0_dout),
        .hsync_count(hsync_count),
        .pclk_count(pclk_count),
        .reset(util_vector_logic_1_Res),
        .start(util_vector_logic_0_Res1),
        .vid_clk(clk_wiz_2_clk_out1),
        .vid_run_clk(clk_wiz_2_clk_out1));
dp_system_video_tx_top_dp_0_1 video_tx_top_dp_1
       (.clk(clk_wiz_2_clk_out1),
        .config_done_in(xlconstant_0_dout),
        .fifo_data_in(Conn1_RD_DATA),
        .fifo_empty(Conn1_EMPTY),
        .fifo_prog_empty(fifo_prog_empty_1),
        .fifo_rd_en(Conn1_RD_EN),
        .reset(util_vector_logic_1_Res),
        .vid_clk(clk_wiz_2_clk_out1),
        .vid_enable(video_tx_top_dp_1_dp_video_inf_TX_VID_ENABLE),
        .vid_hsync(video_tx_top_dp_1_dp_video_inf_TX_VID_HSYNC),
        .vid_oddeven(video_tx_top_dp_1_dp_video_inf_TX_VID_ODDEVEN),
        .vid_pixel0(video_tx_top_dp_1_dp_video_inf_TX_VID_PIXEL0),
        .vid_pixel1(video_tx_top_dp_1_dp_video_inf_TX_VID_PIXEL1),
        .vid_vsync(video_tx_top_dp_1_dp_video_inf_TX_VID_VSYNC));
dp_system_xlconstant_1_0 xlconstant_0
       (.dout(xlconstant_1_dout));
dp_system_xlconstant_0_1 xlconstant_1
       (.dout(xlconstant_0_dout));
endmodule

(* CORE_GENERATION_INFO = "dp_system,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLanguage=VERILOG,numBlks=52,numReposBlks=39,numNonXlnxBlks=0,numHierBlks=13,maxHierDepth=1,da_axi4_cnt=6,da_bram_cntlr_cnt=4,da_ps7_cnt=2}" *) 
module dp_system
   (DDR3_addr,
    DDR3_ba,
    DDR3_cas_n,
    DDR3_ck_n,
    DDR3_ck_p,
    DDR3_cke,
    DDR3_cs_n,
    DDR3_dm,
    DDR3_dq,
    DDR3_dqs_n,
    DDR3_dqs_p,
    DDR3_odt,
    DDR3_ras_n,
    DDR3_reset_n,
    DDR3_we_n,
    DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    SYS_CLK_clk_n,
    SYS_CLK_clk_p,
    aux_tx_io_n,
    aux_tx_io_p,
    clk_out2,
    dp_mainlink_lnk_clk,
    dp_mainlink_lnk_clk_n,
    dp_mainlink_lnk_clk_p,
    dp_mainlink_lnk_tx_lane_n,
    dp_mainlink_lnk_tx_lane_p,
    gpo,
    iic2inctc_irpt_scl_i,
    iic2inctc_irpt_scl_o,
    iic2inctc_irpt_scl_t,
    iic2inctc_irpt_sda_i,
    iic2inctc_irpt_sda_o,
    iic2inctc_irpt_sda_t,
    tx_hpd);
  output [13:0]DDR3_addr;
  output [2:0]DDR3_ba;
  output DDR3_cas_n;
  output [0:0]DDR3_ck_n;
  output [0:0]DDR3_ck_p;
  output [0:0]DDR3_cke;
  output [0:0]DDR3_cs_n;
  output [7:0]DDR3_dm;
  inout [63:0]DDR3_dq;
  inout [7:0]DDR3_dqs_n;
  inout [7:0]DDR3_dqs_p;
  output [0:0]DDR3_odt;
  output DDR3_ras_n;
  output DDR3_reset_n;
  output DDR3_we_n;
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  input SYS_CLK_clk_n;
  input SYS_CLK_clk_p;
  inout aux_tx_io_n;
  inout aux_tx_io_p;
  output clk_out2;
  output dp_mainlink_lnk_clk;
  input dp_mainlink_lnk_clk_n;
  input dp_mainlink_lnk_clk_p;
  output [3:0]dp_mainlink_lnk_tx_lane_n;
  output [3:0]dp_mainlink_lnk_tx_lane_p;
  output [1:0]gpo;
  input iic2inctc_irpt_scl_i;
  output iic2inctc_irpt_scl_o;
  output iic2inctc_irpt_scl_t;
  input iic2inctc_irpt_sda_i;
  output iic2inctc_irpt_sda_o;
  output iic2inctc_irpt_sda_t;
  input tx_hpd;

  wire GND_1;
  wire Net;
  wire Net1;
  wire Net2;
  wire SYS_CLK_1_CLK_N;
  wire SYS_CLK_1_CLK_P;
  wire VCC_1;
  wire [11:0]axi4_lite_peripherals_APB_M_PADDR;
  wire axi4_lite_peripherals_APB_M_PENABLE;
  wire [31:0]axi4_lite_peripherals_APB_M_PRDATA;
  wire axi4_lite_peripherals_APB_M_PREADY;
  wire axi4_lite_peripherals_APB_M_PSEL;
  wire axi4_lite_peripherals_APB_M_PSLVERR;
  wire [31:0]axi4_lite_peripherals_APB_M_PWDATA;
  wire axi4_lite_peripherals_APB_M_PWRITE;
  wire [31:0]axi4_lite_peripherals_M03_AXI_ARADDR;
  wire [2:0]axi4_lite_peripherals_M03_AXI_ARPROT;
  wire axi4_lite_peripherals_M03_AXI_ARREADY;
  wire axi4_lite_peripherals_M03_AXI_ARVALID;
  wire [31:0]axi4_lite_peripherals_M03_AXI_AWADDR;
  wire [2:0]axi4_lite_peripherals_M03_AXI_AWPROT;
  wire axi4_lite_peripherals_M03_AXI_AWREADY;
  wire axi4_lite_peripherals_M03_AXI_AWVALID;
  wire axi4_lite_peripherals_M03_AXI_BREADY;
  wire [1:0]axi4_lite_peripherals_M03_AXI_BRESP;
  wire axi4_lite_peripherals_M03_AXI_BVALID;
  wire [31:0]axi4_lite_peripherals_M03_AXI_RDATA;
  wire axi4_lite_peripherals_M03_AXI_RREADY;
  wire [1:0]axi4_lite_peripherals_M03_AXI_RRESP;
  wire axi4_lite_peripherals_M03_AXI_RVALID;
  wire [31:0]axi4_lite_peripherals_M03_AXI_WDATA;
  wire axi4_lite_peripherals_M03_AXI_WREADY;
  wire [3:0]axi4_lite_peripherals_M03_AXI_WSTRB;
  wire axi4_lite_peripherals_M03_AXI_WVALID;
  wire [31:0]axi_displaybuffer_handle_0_axi_ARADDR;
  wire [1:0]axi_displaybuffer_handle_0_axi_ARBURST;
  wire [3:0]axi_displaybuffer_handle_0_axi_ARID;
  wire [7:0]axi_displaybuffer_handle_0_axi_ARLEN;
  wire axi_displaybuffer_handle_0_axi_ARREADY;
  wire [2:0]axi_displaybuffer_handle_0_axi_ARSIZE;
  wire axi_displaybuffer_handle_0_axi_ARVALID;
  wire [31:0]axi_displaybuffer_handle_0_axi_AWADDR;
  wire [1:0]axi_displaybuffer_handle_0_axi_AWBURST;
  wire [3:0]axi_displaybuffer_handle_0_axi_AWID;
  wire [7:0]axi_displaybuffer_handle_0_axi_AWLEN;
  wire axi_displaybuffer_handle_0_axi_AWREADY;
  wire [2:0]axi_displaybuffer_handle_0_axi_AWSIZE;
  wire axi_displaybuffer_handle_0_axi_AWVALID;
  wire axi_displaybuffer_handle_0_axi_BREADY;
  wire [1:0]axi_displaybuffer_handle_0_axi_BRESP;
  wire axi_displaybuffer_handle_0_axi_BVALID;
  wire [511:0]axi_displaybuffer_handle_0_axi_RDATA;
  wire axi_displaybuffer_handle_0_axi_RLAST;
  wire axi_displaybuffer_handle_0_axi_RREADY;
  wire [1:0]axi_displaybuffer_handle_0_axi_RRESP;
  wire axi_displaybuffer_handle_0_axi_RVALID;
  wire [511:0]axi_displaybuffer_handle_0_axi_WDATA;
  wire axi_displaybuffer_handle_0_axi_WLAST;
  wire axi_displaybuffer_handle_0_axi_WREADY;
  wire [63:0]axi_displaybuffer_handle_0_axi_WSTRB;
  wire axi_displaybuffer_handle_0_axi_WVALID;
  wire axi_displaybuffer_handle_0_fifo_read_rtl_1_EMPTY;
  wire [783:0]axi_displaybuffer_handle_0_fifo_read_rtl_1_RD_DATA;
  wire axi_displaybuffer_handle_0_fifo_read_rtl_1_RD_EN;
(* MARK_DEBUG *)   wire [63:0]axi_displaybuffer_handle_0_hdmi_fifo_WR_DATA;
(* MARK_DEBUG *)   wire axi_displaybuffer_handle_0_hdmi_fifo_WR_EN;
  wire [31:0]axi_dwidth_converter_0_M_AXI_ARADDR;
  wire [1:0]axi_dwidth_converter_0_M_AXI_ARBURST;
  wire [3:0]axi_dwidth_converter_0_M_AXI_ARCACHE;
  wire [3:0]axi_dwidth_converter_0_M_AXI_ARLEN;
  wire [1:0]axi_dwidth_converter_0_M_AXI_ARLOCK;
  wire [2:0]axi_dwidth_converter_0_M_AXI_ARPROT;
  wire [3:0]axi_dwidth_converter_0_M_AXI_ARQOS;
  wire axi_dwidth_converter_0_M_AXI_ARREADY;
  wire [2:0]axi_dwidth_converter_0_M_AXI_ARSIZE;
  wire axi_dwidth_converter_0_M_AXI_ARVALID;
  wire [31:0]axi_dwidth_converter_0_M_AXI_AWADDR;
  wire [1:0]axi_dwidth_converter_0_M_AXI_AWBURST;
  wire [3:0]axi_dwidth_converter_0_M_AXI_AWCACHE;
  wire [3:0]axi_dwidth_converter_0_M_AXI_AWLEN;
  wire [1:0]axi_dwidth_converter_0_M_AXI_AWLOCK;
  wire [2:0]axi_dwidth_converter_0_M_AXI_AWPROT;
  wire [3:0]axi_dwidth_converter_0_M_AXI_AWQOS;
  wire axi_dwidth_converter_0_M_AXI_AWREADY;
  wire [2:0]axi_dwidth_converter_0_M_AXI_AWSIZE;
  wire axi_dwidth_converter_0_M_AXI_AWVALID;
  wire axi_dwidth_converter_0_M_AXI_BREADY;
  wire [1:0]axi_dwidth_converter_0_M_AXI_BRESP;
  wire axi_dwidth_converter_0_M_AXI_BVALID;
  wire [63:0]axi_dwidth_converter_0_M_AXI_RDATA;
  wire axi_dwidth_converter_0_M_AXI_RLAST;
  wire axi_dwidth_converter_0_M_AXI_RREADY;
  wire [1:0]axi_dwidth_converter_0_M_AXI_RRESP;
  wire axi_dwidth_converter_0_M_AXI_RVALID;
  wire [63:0]axi_dwidth_converter_0_M_AXI_WDATA;
  wire axi_dwidth_converter_0_M_AXI_WLAST;
  wire axi_dwidth_converter_0_M_AXI_WREADY;
  wire [7:0]axi_dwidth_converter_0_M_AXI_WSTRB;
  wire axi_dwidth_converter_0_M_AXI_WVALID;
  wire axi_iic_1_IIC_SCL_I;
  wire axi_iic_1_IIC_SCL_O;
  wire axi_iic_1_IIC_SCL_T;
  wire axi_iic_1_IIC_SDA_I;
  wire axi_iic_1_IIC_SDA_O;
  wire axi_iic_1_IIC_SDA_T;
  wire [1:0]axi_iic_1_gpo;
  wire [31:0]axi_protocol_converter_0_M_AXI_ARADDR;
  wire [1:0]axi_protocol_converter_0_M_AXI_ARBURST;
  wire [3:0]axi_protocol_converter_0_M_AXI_ARCACHE;
  wire [0:0]axi_protocol_converter_0_M_AXI_ARID;
  wire [3:0]axi_protocol_converter_0_M_AXI_ARLEN;
  wire [1:0]axi_protocol_converter_0_M_AXI_ARLOCK;
  wire [2:0]axi_protocol_converter_0_M_AXI_ARPROT;
  wire [3:0]axi_protocol_converter_0_M_AXI_ARQOS;
  wire axi_protocol_converter_0_M_AXI_ARREADY;
  wire [2:0]axi_protocol_converter_0_M_AXI_ARSIZE;
  wire axi_protocol_converter_0_M_AXI_ARVALID;
  wire [31:0]axi_protocol_converter_0_M_AXI_AWADDR;
  wire [1:0]axi_protocol_converter_0_M_AXI_AWBURST;
  wire [3:0]axi_protocol_converter_0_M_AXI_AWCACHE;
  wire [0:0]axi_protocol_converter_0_M_AXI_AWID;
  wire [3:0]axi_protocol_converter_0_M_AXI_AWLEN;
  wire [1:0]axi_protocol_converter_0_M_AXI_AWLOCK;
  wire [2:0]axi_protocol_converter_0_M_AXI_AWPROT;
  wire [3:0]axi_protocol_converter_0_M_AXI_AWQOS;
  wire axi_protocol_converter_0_M_AXI_AWREADY;
  wire [2:0]axi_protocol_converter_0_M_AXI_AWSIZE;
  wire axi_protocol_converter_0_M_AXI_AWVALID;
  wire [0:0]axi_protocol_converter_0_M_AXI_BID;
  wire axi_protocol_converter_0_M_AXI_BREADY;
  wire [1:0]axi_protocol_converter_0_M_AXI_BRESP;
  wire axi_protocol_converter_0_M_AXI_BVALID;
  wire [511:0]axi_protocol_converter_0_M_AXI_RDATA;
  wire [0:0]axi_protocol_converter_0_M_AXI_RID;
  wire axi_protocol_converter_0_M_AXI_RLAST;
  wire axi_protocol_converter_0_M_AXI_RREADY;
  wire [1:0]axi_protocol_converter_0_M_AXI_RRESP;
  wire axi_protocol_converter_0_M_AXI_RVALID;
  wire [511:0]axi_protocol_converter_0_M_AXI_WDATA;
  wire axi_protocol_converter_0_M_AXI_WLAST;
  wire axi_protocol_converter_0_M_AXI_WREADY;
  wire [63:0]axi_protocol_converter_0_M_AXI_WSTRB;
  wire axi_protocol_converter_0_M_AXI_WVALID;
  wire [0:0]cabac_modules_bus_struct_reset;
  wire [143:0]cabac_modules_fifo_cb_WR_DATA;
  wire cabac_modules_fifo_cb_WR_EN;
  wire [31:0]cabac_modules_fifo_cont_WR_DATA;
  wire cabac_modules_fifo_cont_WR_EN;
  wire [143:0]cabac_modules_fifo_cr_WR_DATA;
  wire cabac_modules_fifo_cr_WR_EN;
  wire [143:0]cabac_modules_fifo_y_WR_DATA;
  wire cabac_modules_fifo_y_WR_EN;
  wire clk_wiz_1_clk_out1;
  wire clk_wiz_1_clk_out2;
  wire clk_wiz_1_clk_out3;
  wire clk_wiz_1_clk_out4;
  wire clk_wiz_1_locked;
  wire dec_wo_cabac_cb_residual_read_inf_EMPTY;
  wire [143:0]dec_wo_cabac_cb_residual_read_inf_RD_DATA;
  wire dec_wo_cabac_cb_residual_read_inf_RD_EN;
  wire [31:0]dec_wo_cabac_config_fifo_read_info_RD_DATA;
  wire dec_wo_cabac_config_fifo_read_info_RD_EN;
  wire dec_wo_cabac_cr_residual_read_inf_EMPTY;
  wire [143:0]dec_wo_cabac_cr_residual_read_inf_RD_DATA;
  wire dec_wo_cabac_cr_residual_read_inf_RD_EN;
  wire dec_wo_cabac_y_residual_interface_EMPTY;
  wire [143:0]dec_wo_cabac_y_residual_interface_RD_DATA;
  wire dec_wo_cabac_y_residual_interface_RD_EN;
  wire dis_buf_in_fifo_prog_full;
(* MARK_DEBUG *)   wire dis_buf_out_fifo_prog_empty;
(* MARK_DEBUG *)   wire dis_buf_out_fifo_prog_full;
  wire displayport_0_axi_int;
  wire displayport_0_dp_mainlink_LNK_CLK;
  wire displayport_0_dp_mainlink_LNK_CLK_N;
  wire displayport_0_dp_mainlink_LNK_CLK_P;
  wire [3:0]displayport_0_dp_mainlink_LNK_TX_LANE_N;
  wire [3:0]displayport_0_dp_mainlink_LNK_TX_LANE_P;
  wire dp_modules_clk_out1;
(* MARK_DEBUG *)   wire dp_modules_dp_fifo_read_EMPTY;
(* MARK_DEBUG *)   wire [63:0]dp_modules_dp_fifo_read_RD_DATA;
(* MARK_DEBUG *)   wire dp_modules_dp_fifo_read_RD_EN;
  wire [783:0]hevc_top_wo_dp_fifo_0_display_fifo_write_inf_WR_DATA;
  wire hevc_top_wo_dp_fifo_0_display_fifo_write_inf_WR_EN;
  wire [11:0]hevc_top_wo_dp_fifo_0_pic_height_out;
  wire [11:0]hevc_top_wo_dp_fifo_0_pic_width_out;
  wire [31:0]hevc_top_wo_dp_fifo_0_sao_poc_out;
  wire [13:0]mig_7series_0_DDR3_ADDR;
  wire [2:0]mig_7series_0_DDR3_BA;
  wire mig_7series_0_DDR3_CAS_N;
  wire [0:0]mig_7series_0_DDR3_CKE;
  wire [0:0]mig_7series_0_DDR3_CK_N;
  wire [0:0]mig_7series_0_DDR3_CK_P;
  wire [0:0]mig_7series_0_DDR3_CS_N;
  wire [7:0]mig_7series_0_DDR3_DM;
  wire [63:0]mig_7series_0_DDR3_DQ;
  wire [7:0]mig_7series_0_DDR3_DQS_N;
  wire [7:0]mig_7series_0_DDR3_DQS_P;
  wire [0:0]mig_7series_0_DDR3_ODT;
  wire mig_7series_0_DDR3_RAS_N;
  wire mig_7series_0_DDR3_RESET_N;
  wire mig_7series_0_DDR3_WE_N;
  wire mig_7series_0_ui_clk;
  wire mig_setup_reset_0_design_reset;
  wire [0:0]proc_sys_reset_1_peripheral_aresetn;
  wire [0:0]proc_sys_reset_1_peripheral_reset;
  wire [14:0]processing_system7_0_DDR_ADDR;
  wire [2:0]processing_system7_0_DDR_BA;
  wire processing_system7_0_DDR_CAS_N;
  wire processing_system7_0_DDR_CKE;
  wire processing_system7_0_DDR_CK_N;
  wire processing_system7_0_DDR_CK_P;
  wire processing_system7_0_DDR_CS_N;
  wire [3:0]processing_system7_0_DDR_DM;
  wire [31:0]processing_system7_0_DDR_DQ;
  wire [3:0]processing_system7_0_DDR_DQS_N;
  wire [3:0]processing_system7_0_DDR_DQS_P;
  wire processing_system7_0_DDR_ODT;
  wire processing_system7_0_DDR_RAS_N;
  wire processing_system7_0_DDR_RESET_N;
  wire processing_system7_0_DDR_WE_N;
  wire processing_system7_0_FCLK_CLK0;
  wire processing_system7_0_FIXED_IO_DDR_VRN;
  wire processing_system7_0_FIXED_IO_DDR_VRP;
  wire [53:0]processing_system7_0_FIXED_IO_MIO;
  wire processing_system7_0_FIXED_IO_PS_CLK;
  wire processing_system7_0_FIXED_IO_PS_PORB;
  wire processing_system7_0_FIXED_IO_PS_SRSTB;
  wire [31:0]processing_system7_0_M_AXI_GP0_ARADDR;
  wire [1:0]processing_system7_0_M_AXI_GP0_ARBURST;
  wire [3:0]processing_system7_0_M_AXI_GP0_ARCACHE;
  wire [11:0]processing_system7_0_M_AXI_GP0_ARID;
  wire [3:0]processing_system7_0_M_AXI_GP0_ARLEN;
  wire [1:0]processing_system7_0_M_AXI_GP0_ARLOCK;
  wire [2:0]processing_system7_0_M_AXI_GP0_ARPROT;
  wire [3:0]processing_system7_0_M_AXI_GP0_ARQOS;
  wire processing_system7_0_M_AXI_GP0_ARREADY;
  wire [2:0]processing_system7_0_M_AXI_GP0_ARSIZE;
  wire processing_system7_0_M_AXI_GP0_ARVALID;
  wire [31:0]processing_system7_0_M_AXI_GP0_AWADDR;
  wire [1:0]processing_system7_0_M_AXI_GP0_AWBURST;
  wire [3:0]processing_system7_0_M_AXI_GP0_AWCACHE;
  wire [11:0]processing_system7_0_M_AXI_GP0_AWID;
  wire [3:0]processing_system7_0_M_AXI_GP0_AWLEN;
  wire [1:0]processing_system7_0_M_AXI_GP0_AWLOCK;
  wire [2:0]processing_system7_0_M_AXI_GP0_AWPROT;
  wire [3:0]processing_system7_0_M_AXI_GP0_AWQOS;
  wire processing_system7_0_M_AXI_GP0_AWREADY;
  wire [2:0]processing_system7_0_M_AXI_GP0_AWSIZE;
  wire processing_system7_0_M_AXI_GP0_AWVALID;
  wire [11:0]processing_system7_0_M_AXI_GP0_BID;
  wire processing_system7_0_M_AXI_GP0_BREADY;
  wire [1:0]processing_system7_0_M_AXI_GP0_BRESP;
  wire processing_system7_0_M_AXI_GP0_BVALID;
  wire [31:0]processing_system7_0_M_AXI_GP0_RDATA;
  wire [11:0]processing_system7_0_M_AXI_GP0_RID;
  wire processing_system7_0_M_AXI_GP0_RLAST;
  wire processing_system7_0_M_AXI_GP0_RREADY;
  wire [1:0]processing_system7_0_M_AXI_GP0_RRESP;
  wire processing_system7_0_M_AXI_GP0_RVALID;
  wire [31:0]processing_system7_0_M_AXI_GP0_WDATA;
  wire [11:0]processing_system7_0_M_AXI_GP0_WID;
  wire processing_system7_0_M_AXI_GP0_WLAST;
  wire processing_system7_0_M_AXI_GP0_WREADY;
  wire [3:0]processing_system7_0_M_AXI_GP0_WSTRB;
  wire processing_system7_0_M_AXI_GP0_WVALID;
  wire [31:0]processing_system7_0_M_AXI_GP1_ARADDR;
  wire [1:0]processing_system7_0_M_AXI_GP1_ARBURST;
  wire [3:0]processing_system7_0_M_AXI_GP1_ARCACHE;
  wire [11:0]processing_system7_0_M_AXI_GP1_ARID;
  wire [3:0]processing_system7_0_M_AXI_GP1_ARLEN;
  wire [1:0]processing_system7_0_M_AXI_GP1_ARLOCK;
  wire [2:0]processing_system7_0_M_AXI_GP1_ARPROT;
  wire [3:0]processing_system7_0_M_AXI_GP1_ARQOS;
  wire processing_system7_0_M_AXI_GP1_ARREADY;
  wire [2:0]processing_system7_0_M_AXI_GP1_ARSIZE;
  wire processing_system7_0_M_AXI_GP1_ARVALID;
  wire [31:0]processing_system7_0_M_AXI_GP1_AWADDR;
  wire [1:0]processing_system7_0_M_AXI_GP1_AWBURST;
  wire [3:0]processing_system7_0_M_AXI_GP1_AWCACHE;
  wire [11:0]processing_system7_0_M_AXI_GP1_AWID;
  wire [3:0]processing_system7_0_M_AXI_GP1_AWLEN;
  wire [1:0]processing_system7_0_M_AXI_GP1_AWLOCK;
  wire [2:0]processing_system7_0_M_AXI_GP1_AWPROT;
  wire [3:0]processing_system7_0_M_AXI_GP1_AWQOS;
  wire processing_system7_0_M_AXI_GP1_AWREADY;
  wire [2:0]processing_system7_0_M_AXI_GP1_AWSIZE;
  wire processing_system7_0_M_AXI_GP1_AWVALID;
  wire [11:0]processing_system7_0_M_AXI_GP1_BID;
  wire processing_system7_0_M_AXI_GP1_BREADY;
  wire [1:0]processing_system7_0_M_AXI_GP1_BRESP;
  wire processing_system7_0_M_AXI_GP1_BVALID;
  wire [31:0]processing_system7_0_M_AXI_GP1_RDATA;
  wire [11:0]processing_system7_0_M_AXI_GP1_RID;
  wire processing_system7_0_M_AXI_GP1_RLAST;
  wire processing_system7_0_M_AXI_GP1_RREADY;
  wire [1:0]processing_system7_0_M_AXI_GP1_RRESP;
  wire processing_system7_0_M_AXI_GP1_RVALID;
  wire [31:0]processing_system7_0_M_AXI_GP1_WDATA;
  wire [11:0]processing_system7_0_M_AXI_GP1_WID;
  wire processing_system7_0_M_AXI_GP1_WLAST;
  wire processing_system7_0_M_AXI_GP1_WREADY;
  wire [3:0]processing_system7_0_M_AXI_GP1_WSTRB;
  wire processing_system7_0_M_AXI_GP1_WVALID;
  wire res2pred_cb_prog_full;
  wire [10:0]res2pred_cb_rd_data_count;
  wire res2pred_config_prog_empty;
  wire res2pred_config_prog_full;
  wire res2pred_cr_prog_full;
  wire [10:0]res2pred_cr_rd_data_count;
  wire res2pred_y_prog_full;
  wire [12:0]res2pred_y_rd_data_count;
  wire tx_hpd_1;

  assign DDR3_addr[13:0] = mig_7series_0_DDR3_ADDR;
  assign DDR3_ba[2:0] = mig_7series_0_DDR3_BA;
  assign DDR3_cas_n = mig_7series_0_DDR3_CAS_N;
  assign DDR3_ck_n[0] = mig_7series_0_DDR3_CK_N;
  assign DDR3_ck_p[0] = mig_7series_0_DDR3_CK_P;
  assign DDR3_cke[0] = mig_7series_0_DDR3_CKE;
  assign DDR3_cs_n[0] = mig_7series_0_DDR3_CS_N;
  assign DDR3_dm[7:0] = mig_7series_0_DDR3_DM;
  assign DDR3_odt[0] = mig_7series_0_DDR3_ODT;
  assign DDR3_ras_n = mig_7series_0_DDR3_RAS_N;
  assign DDR3_reset_n = mig_7series_0_DDR3_RESET_N;
  assign DDR3_we_n = mig_7series_0_DDR3_WE_N;
  assign SYS_CLK_1_CLK_N = SYS_CLK_clk_n;
  assign SYS_CLK_1_CLK_P = SYS_CLK_clk_p;
  assign axi_iic_1_IIC_SCL_I = iic2inctc_irpt_scl_i;
  assign axi_iic_1_IIC_SDA_I = iic2inctc_irpt_sda_i;
  assign clk_out2 = clk_wiz_1_clk_out2;
  assign displayport_0_dp_mainlink_LNK_CLK_N = dp_mainlink_lnk_clk_n;
  assign displayport_0_dp_mainlink_LNK_CLK_P = dp_mainlink_lnk_clk_p;
  assign dp_mainlink_lnk_clk = displayport_0_dp_mainlink_LNK_CLK;
  assign dp_mainlink_lnk_tx_lane_n[3:0] = displayport_0_dp_mainlink_LNK_TX_LANE_N;
  assign dp_mainlink_lnk_tx_lane_p[3:0] = displayport_0_dp_mainlink_LNK_TX_LANE_P;
  assign gpo[1:0] = axi_iic_1_gpo;
  assign iic2inctc_irpt_scl_o = axi_iic_1_IIC_SCL_O;
  assign iic2inctc_irpt_scl_t = axi_iic_1_IIC_SCL_T;
  assign iic2inctc_irpt_sda_o = axi_iic_1_IIC_SDA_O;
  assign iic2inctc_irpt_sda_t = axi_iic_1_IIC_SDA_T;
  assign tx_hpd_1 = tx_hpd;
GND GND
       (.G(GND_1));
VCC VCC
       (.P(VCC_1));
axi4_lite_peripherals_imp_8FS6LK axi4_lite_peripherals
       (.APB_M_paddr(axi4_lite_peripherals_APB_M_PADDR),
        .APB_M_penable(axi4_lite_peripherals_APB_M_PENABLE),
        .APB_M_prdata(axi4_lite_peripherals_APB_M_PRDATA),
        .APB_M_pready(axi4_lite_peripherals_APB_M_PREADY),
        .APB_M_psel(axi4_lite_peripherals_APB_M_PSEL),
        .APB_M_pslverr(axi4_lite_peripherals_APB_M_PSLVERR),
        .APB_M_pwdata(axi4_lite_peripherals_APB_M_PWDATA),
        .APB_M_pwrite(axi4_lite_peripherals_APB_M_PWRITE),
        .IIC_scl_i(axi_iic_1_IIC_SCL_I),
        .IIC_scl_o(axi_iic_1_IIC_SCL_O),
        .IIC_scl_t(axi_iic_1_IIC_SCL_T),
        .IIC_sda_i(axi_iic_1_IIC_SDA_I),
        .IIC_sda_o(axi_iic_1_IIC_SDA_O),
        .IIC_sda_t(axi_iic_1_IIC_SDA_T),
        .In1(displayport_0_axi_int),
        .M03_AXI_araddr(axi4_lite_peripherals_M03_AXI_ARADDR),
        .M03_AXI_arprot(axi4_lite_peripherals_M03_AXI_ARPROT),
        .M03_AXI_arready(axi4_lite_peripherals_M03_AXI_ARREADY),
        .M03_AXI_arvalid(axi4_lite_peripherals_M03_AXI_ARVALID),
        .M03_AXI_awaddr(axi4_lite_peripherals_M03_AXI_AWADDR),
        .M03_AXI_awprot(axi4_lite_peripherals_M03_AXI_AWPROT),
        .M03_AXI_awready(axi4_lite_peripherals_M03_AXI_AWREADY),
        .M03_AXI_awvalid(axi4_lite_peripherals_M03_AXI_AWVALID),
        .M03_AXI_bready(axi4_lite_peripherals_M03_AXI_BREADY),
        .M03_AXI_bresp(axi4_lite_peripherals_M03_AXI_BRESP),
        .M03_AXI_bvalid(axi4_lite_peripherals_M03_AXI_BVALID),
        .M03_AXI_rdata(axi4_lite_peripherals_M03_AXI_RDATA),
        .M03_AXI_rready(axi4_lite_peripherals_M03_AXI_RREADY),
        .M03_AXI_rresp(axi4_lite_peripherals_M03_AXI_RRESP),
        .M03_AXI_rvalid(axi4_lite_peripherals_M03_AXI_RVALID),
        .M03_AXI_wdata(axi4_lite_peripherals_M03_AXI_WDATA),
        .M03_AXI_wready(axi4_lite_peripherals_M03_AXI_WREADY),
        .M03_AXI_wstrb(axi4_lite_peripherals_M03_AXI_WSTRB),
        .M03_AXI_wvalid(axi4_lite_peripherals_M03_AXI_WVALID),
        .S00_AXI_araddr(processing_system7_0_M_AXI_GP0_ARADDR),
        .S00_AXI_arburst(processing_system7_0_M_AXI_GP0_ARBURST),
        .S00_AXI_arcache(processing_system7_0_M_AXI_GP0_ARCACHE),
        .S00_AXI_arid(processing_system7_0_M_AXI_GP0_ARID),
        .S00_AXI_arlen(processing_system7_0_M_AXI_GP0_ARLEN),
        .S00_AXI_arlock(processing_system7_0_M_AXI_GP0_ARLOCK),
        .S00_AXI_arprot(processing_system7_0_M_AXI_GP0_ARPROT),
        .S00_AXI_arqos(processing_system7_0_M_AXI_GP0_ARQOS),
        .S00_AXI_arready(processing_system7_0_M_AXI_GP0_ARREADY),
        .S00_AXI_arsize(processing_system7_0_M_AXI_GP0_ARSIZE),
        .S00_AXI_arvalid(processing_system7_0_M_AXI_GP0_ARVALID),
        .S00_AXI_awaddr(processing_system7_0_M_AXI_GP0_AWADDR),
        .S00_AXI_awburst(processing_system7_0_M_AXI_GP0_AWBURST),
        .S00_AXI_awcache(processing_system7_0_M_AXI_GP0_AWCACHE),
        .S00_AXI_awid(processing_system7_0_M_AXI_GP0_AWID),
        .S00_AXI_awlen(processing_system7_0_M_AXI_GP0_AWLEN),
        .S00_AXI_awlock(processing_system7_0_M_AXI_GP0_AWLOCK),
        .S00_AXI_awprot(processing_system7_0_M_AXI_GP0_AWPROT),
        .S00_AXI_awqos(processing_system7_0_M_AXI_GP0_AWQOS),
        .S00_AXI_awready(processing_system7_0_M_AXI_GP0_AWREADY),
        .S00_AXI_awsize(processing_system7_0_M_AXI_GP0_AWSIZE),
        .S00_AXI_awvalid(processing_system7_0_M_AXI_GP0_AWVALID),
        .S00_AXI_bid(processing_system7_0_M_AXI_GP0_BID),
        .S00_AXI_bready(processing_system7_0_M_AXI_GP0_BREADY),
        .S00_AXI_bresp(processing_system7_0_M_AXI_GP0_BRESP),
        .S00_AXI_bvalid(processing_system7_0_M_AXI_GP0_BVALID),
        .S00_AXI_rdata(processing_system7_0_M_AXI_GP0_RDATA),
        .S00_AXI_rid(processing_system7_0_M_AXI_GP0_RID),
        .S00_AXI_rlast(processing_system7_0_M_AXI_GP0_RLAST),
        .S00_AXI_rready(processing_system7_0_M_AXI_GP0_RREADY),
        .S00_AXI_rresp(processing_system7_0_M_AXI_GP0_RRESP),
        .S00_AXI_rvalid(processing_system7_0_M_AXI_GP0_RVALID),
        .S00_AXI_wdata(processing_system7_0_M_AXI_GP0_WDATA),
        .S00_AXI_wid(processing_system7_0_M_AXI_GP0_WID),
        .S00_AXI_wlast(processing_system7_0_M_AXI_GP0_WLAST),
        .S00_AXI_wready(processing_system7_0_M_AXI_GP0_WREADY),
        .S00_AXI_wstrb(processing_system7_0_M_AXI_GP0_WSTRB),
        .S00_AXI_wvalid(processing_system7_0_M_AXI_GP0_WVALID),
        .dcm_locked(clk_wiz_1_locked),
        .ext_reset_in(Net2),
        .gpo(axi_iic_1_gpo),
        .peripheral_aresetn(proc_sys_reset_1_peripheral_aresetn),
        .peripheral_reset(proc_sys_reset_1_peripheral_reset),
        .s_axi_aclk(clk_wiz_1_clk_out3));
dp_system_axi_displaybuffer_handle_0_0 axi_displaybuffer_handle_0
       (.axi_araddr(axi_displaybuffer_handle_0_axi_ARADDR),
        .axi_arburst(axi_displaybuffer_handle_0_axi_ARBURST),
        .axi_arid(axi_displaybuffer_handle_0_axi_ARID),
        .axi_arlen(axi_displaybuffer_handle_0_axi_ARLEN),
        .axi_arready(axi_displaybuffer_handle_0_axi_ARREADY),
        .axi_arsize(axi_displaybuffer_handle_0_axi_ARSIZE),
        .axi_arvalid(axi_displaybuffer_handle_0_axi_ARVALID),
        .axi_awaddr(axi_displaybuffer_handle_0_axi_AWADDR),
        .axi_awburst(axi_displaybuffer_handle_0_axi_AWBURST),
        .axi_awid(axi_displaybuffer_handle_0_axi_AWID),
        .axi_awlen(axi_displaybuffer_handle_0_axi_AWLEN),
        .axi_awready(axi_displaybuffer_handle_0_axi_AWREADY),
        .axi_awsize(axi_displaybuffer_handle_0_axi_AWSIZE),
        .axi_awvalid(axi_displaybuffer_handle_0_axi_AWVALID),
        .axi_bready(axi_displaybuffer_handle_0_axi_BREADY),
        .axi_bresp(axi_displaybuffer_handle_0_axi_BRESP),
        .axi_bvalid(axi_displaybuffer_handle_0_axi_BVALID),
        .axi_clk(VCC_1),
        .axi_rdata(axi_displaybuffer_handle_0_axi_RDATA),
        .axi_rlast(axi_displaybuffer_handle_0_axi_RLAST),
        .axi_rready(axi_displaybuffer_handle_0_axi_RREADY),
        .axi_rresp(axi_displaybuffer_handle_0_axi_RRESP),
        .axi_rvalid(axi_displaybuffer_handle_0_axi_RVALID),
        .axi_wdata(axi_displaybuffer_handle_0_axi_WDATA),
        .axi_wlast(axi_displaybuffer_handle_0_axi_WLAST),
        .axi_wready(axi_displaybuffer_handle_0_axi_WREADY),
        .axi_wstrb(axi_displaybuffer_handle_0_axi_WSTRB),
        .axi_wvalid(axi_displaybuffer_handle_0_axi_WVALID),
        .clk(clk_wiz_1_clk_out4),
        .config_valid_in(VCC_1),
        .displaybuffer_fifo_almost_empty_in(GND_1),
        .displaybuffer_fifo_data_in(axi_displaybuffer_handle_0_fifo_read_rtl_1_RD_DATA),
        .displaybuffer_fifo_empty_in(axi_displaybuffer_handle_0_fifo_read_rtl_1_EMPTY),
        .displaybuffer_fifo_read_en_out(axi_displaybuffer_handle_0_fifo_read_rtl_1_RD_EN),
        .hdmi_fifo_almost_full(dis_buf_out_fifo_prog_full),
        .hdmi_fifo_data_out(axi_displaybuffer_handle_0_hdmi_fifo_WR_DATA),
        .hdmi_fifo_w_en(axi_displaybuffer_handle_0_hdmi_fifo_WR_EN),
        .pic_height_in(hevc_top_wo_dp_fifo_0_pic_height_out[10:0]),
        .pic_width_in(hevc_top_wo_dp_fifo_0_pic_width_out[10:0]),
        .poc_4bits_in(hevc_top_wo_dp_fifo_0_sao_poc_out[3:0]),
        .reset(proc_sys_reset_1_peripheral_reset));
dp_system_axi_dwidth_converter_0_0 axi_dwidth_converter_0
       (.m_axi_araddr(axi_dwidth_converter_0_M_AXI_ARADDR),
        .m_axi_arburst(axi_dwidth_converter_0_M_AXI_ARBURST),
        .m_axi_arcache(axi_dwidth_converter_0_M_AXI_ARCACHE),
        .m_axi_arlen(axi_dwidth_converter_0_M_AXI_ARLEN),
        .m_axi_arlock(axi_dwidth_converter_0_M_AXI_ARLOCK),
        .m_axi_arprot(axi_dwidth_converter_0_M_AXI_ARPROT),
        .m_axi_arqos(axi_dwidth_converter_0_M_AXI_ARQOS),
        .m_axi_arready(axi_dwidth_converter_0_M_AXI_ARREADY),
        .m_axi_arsize(axi_dwidth_converter_0_M_AXI_ARSIZE),
        .m_axi_arvalid(axi_dwidth_converter_0_M_AXI_ARVALID),
        .m_axi_awaddr(axi_dwidth_converter_0_M_AXI_AWADDR),
        .m_axi_awburst(axi_dwidth_converter_0_M_AXI_AWBURST),
        .m_axi_awcache(axi_dwidth_converter_0_M_AXI_AWCACHE),
        .m_axi_awlen(axi_dwidth_converter_0_M_AXI_AWLEN),
        .m_axi_awlock(axi_dwidth_converter_0_M_AXI_AWLOCK),
        .m_axi_awprot(axi_dwidth_converter_0_M_AXI_AWPROT),
        .m_axi_awqos(axi_dwidth_converter_0_M_AXI_AWQOS),
        .m_axi_awready(axi_dwidth_converter_0_M_AXI_AWREADY),
        .m_axi_awsize(axi_dwidth_converter_0_M_AXI_AWSIZE),
        .m_axi_awvalid(axi_dwidth_converter_0_M_AXI_AWVALID),
        .m_axi_bready(axi_dwidth_converter_0_M_AXI_BREADY),
        .m_axi_bresp(axi_dwidth_converter_0_M_AXI_BRESP),
        .m_axi_bvalid(axi_dwidth_converter_0_M_AXI_BVALID),
        .m_axi_rdata(axi_dwidth_converter_0_M_AXI_RDATA),
        .m_axi_rlast(axi_dwidth_converter_0_M_AXI_RLAST),
        .m_axi_rready(axi_dwidth_converter_0_M_AXI_RREADY),
        .m_axi_rresp(axi_dwidth_converter_0_M_AXI_RRESP),
        .m_axi_rvalid(axi_dwidth_converter_0_M_AXI_RVALID),
        .m_axi_wdata(axi_dwidth_converter_0_M_AXI_WDATA),
        .m_axi_wlast(axi_dwidth_converter_0_M_AXI_WLAST),
        .m_axi_wready(axi_dwidth_converter_0_M_AXI_WREADY),
        .m_axi_wstrb(axi_dwidth_converter_0_M_AXI_WSTRB),
        .m_axi_wvalid(axi_dwidth_converter_0_M_AXI_WVALID),
        .s_axi_aclk(clk_wiz_1_clk_out4),
        .s_axi_araddr(axi_protocol_converter_0_M_AXI_ARADDR),
        .s_axi_arburst(axi_protocol_converter_0_M_AXI_ARBURST),
        .s_axi_arcache(axi_protocol_converter_0_M_AXI_ARCACHE),
        .s_axi_aresetn(proc_sys_reset_1_peripheral_aresetn),
        .s_axi_arid(axi_protocol_converter_0_M_AXI_ARID),
        .s_axi_arlen(axi_protocol_converter_0_M_AXI_ARLEN),
        .s_axi_arlock(axi_protocol_converter_0_M_AXI_ARLOCK),
        .s_axi_arprot(axi_protocol_converter_0_M_AXI_ARPROT),
        .s_axi_arqos(axi_protocol_converter_0_M_AXI_ARQOS),
        .s_axi_arready(axi_protocol_converter_0_M_AXI_ARREADY),
        .s_axi_arsize(axi_protocol_converter_0_M_AXI_ARSIZE),
        .s_axi_arvalid(axi_protocol_converter_0_M_AXI_ARVALID),
        .s_axi_awaddr(axi_protocol_converter_0_M_AXI_AWADDR),
        .s_axi_awburst(axi_protocol_converter_0_M_AXI_AWBURST),
        .s_axi_awcache(axi_protocol_converter_0_M_AXI_AWCACHE),
        .s_axi_awid(axi_protocol_converter_0_M_AXI_AWID),
        .s_axi_awlen(axi_protocol_converter_0_M_AXI_AWLEN),
        .s_axi_awlock(axi_protocol_converter_0_M_AXI_AWLOCK),
        .s_axi_awprot(axi_protocol_converter_0_M_AXI_AWPROT),
        .s_axi_awqos(axi_protocol_converter_0_M_AXI_AWQOS),
        .s_axi_awready(axi_protocol_converter_0_M_AXI_AWREADY),
        .s_axi_awsize(axi_protocol_converter_0_M_AXI_AWSIZE),
        .s_axi_awvalid(axi_protocol_converter_0_M_AXI_AWVALID),
        .s_axi_bid(axi_protocol_converter_0_M_AXI_BID),
        .s_axi_bready(axi_protocol_converter_0_M_AXI_BREADY),
        .s_axi_bresp(axi_protocol_converter_0_M_AXI_BRESP),
        .s_axi_bvalid(axi_protocol_converter_0_M_AXI_BVALID),
        .s_axi_rdata(axi_protocol_converter_0_M_AXI_RDATA),
        .s_axi_rid(axi_protocol_converter_0_M_AXI_RID),
        .s_axi_rlast(axi_protocol_converter_0_M_AXI_RLAST),
        .s_axi_rready(axi_protocol_converter_0_M_AXI_RREADY),
        .s_axi_rresp(axi_protocol_converter_0_M_AXI_RRESP),
        .s_axi_rvalid(axi_protocol_converter_0_M_AXI_RVALID),
        .s_axi_wdata(axi_protocol_converter_0_M_AXI_WDATA),
        .s_axi_wlast(axi_protocol_converter_0_M_AXI_WLAST),
        .s_axi_wready(axi_protocol_converter_0_M_AXI_WREADY),
        .s_axi_wstrb(axi_protocol_converter_0_M_AXI_WSTRB),
        .s_axi_wvalid(axi_protocol_converter_0_M_AXI_WVALID));
dp_system_axi_protocol_converter_0_0 axi_protocol_converter_0
       (.aclk(clk_wiz_1_clk_out4),
        .aresetn(proc_sys_reset_1_peripheral_aresetn),
        .m_axi_araddr(axi_protocol_converter_0_M_AXI_ARADDR),
        .m_axi_arburst(axi_protocol_converter_0_M_AXI_ARBURST),
        .m_axi_arcache(axi_protocol_converter_0_M_AXI_ARCACHE),
        .m_axi_arid(axi_protocol_converter_0_M_AXI_ARID),
        .m_axi_arlen(axi_protocol_converter_0_M_AXI_ARLEN),
        .m_axi_arlock(axi_protocol_converter_0_M_AXI_ARLOCK),
        .m_axi_arprot(axi_protocol_converter_0_M_AXI_ARPROT),
        .m_axi_arqos(axi_protocol_converter_0_M_AXI_ARQOS),
        .m_axi_arready(axi_protocol_converter_0_M_AXI_ARREADY),
        .m_axi_arsize(axi_protocol_converter_0_M_AXI_ARSIZE),
        .m_axi_arvalid(axi_protocol_converter_0_M_AXI_ARVALID),
        .m_axi_awaddr(axi_protocol_converter_0_M_AXI_AWADDR),
        .m_axi_awburst(axi_protocol_converter_0_M_AXI_AWBURST),
        .m_axi_awcache(axi_protocol_converter_0_M_AXI_AWCACHE),
        .m_axi_awid(axi_protocol_converter_0_M_AXI_AWID),
        .m_axi_awlen(axi_protocol_converter_0_M_AXI_AWLEN),
        .m_axi_awlock(axi_protocol_converter_0_M_AXI_AWLOCK),
        .m_axi_awprot(axi_protocol_converter_0_M_AXI_AWPROT),
        .m_axi_awqos(axi_protocol_converter_0_M_AXI_AWQOS),
        .m_axi_awready(axi_protocol_converter_0_M_AXI_AWREADY),
        .m_axi_awsize(axi_protocol_converter_0_M_AXI_AWSIZE),
        .m_axi_awvalid(axi_protocol_converter_0_M_AXI_AWVALID),
        .m_axi_bid(axi_protocol_converter_0_M_AXI_BID),
        .m_axi_bready(axi_protocol_converter_0_M_AXI_BREADY),
        .m_axi_bresp(axi_protocol_converter_0_M_AXI_BRESP),
        .m_axi_bvalid(axi_protocol_converter_0_M_AXI_BVALID),
        .m_axi_rdata(axi_protocol_converter_0_M_AXI_RDATA),
        .m_axi_rid(axi_protocol_converter_0_M_AXI_RID),
        .m_axi_rlast(axi_protocol_converter_0_M_AXI_RLAST),
        .m_axi_rready(axi_protocol_converter_0_M_AXI_RREADY),
        .m_axi_rresp(axi_protocol_converter_0_M_AXI_RRESP),
        .m_axi_rvalid(axi_protocol_converter_0_M_AXI_RVALID),
        .m_axi_wdata(axi_protocol_converter_0_M_AXI_WDATA),
        .m_axi_wlast(axi_protocol_converter_0_M_AXI_WLAST),
        .m_axi_wready(axi_protocol_converter_0_M_AXI_WREADY),
        .m_axi_wstrb(axi_protocol_converter_0_M_AXI_WSTRB),
        .m_axi_wvalid(axi_protocol_converter_0_M_AXI_WVALID),
        .s_axi_araddr(axi_displaybuffer_handle_0_axi_ARADDR),
        .s_axi_arburst(axi_displaybuffer_handle_0_axi_ARBURST),
        .s_axi_arcache({GND_1,GND_1,GND_1,GND_1}),
        .s_axi_arid(axi_displaybuffer_handle_0_axi_ARID[0]),
        .s_axi_arlen(axi_displaybuffer_handle_0_axi_ARLEN),
        .s_axi_arlock(GND_1),
        .s_axi_arprot({GND_1,GND_1,GND_1}),
        .s_axi_arqos({GND_1,GND_1,GND_1,GND_1}),
        .s_axi_arready(axi_displaybuffer_handle_0_axi_ARREADY),
        .s_axi_arregion({GND_1,GND_1,GND_1,GND_1}),
        .s_axi_arsize(axi_displaybuffer_handle_0_axi_ARSIZE),
        .s_axi_arvalid(axi_displaybuffer_handle_0_axi_ARVALID),
        .s_axi_awaddr(axi_displaybuffer_handle_0_axi_AWADDR),
        .s_axi_awburst(axi_displaybuffer_handle_0_axi_AWBURST),
        .s_axi_awcache({GND_1,GND_1,GND_1,GND_1}),
        .s_axi_awid(axi_displaybuffer_handle_0_axi_AWID[0]),
        .s_axi_awlen(axi_displaybuffer_handle_0_axi_AWLEN),
        .s_axi_awlock(GND_1),
        .s_axi_awprot({GND_1,GND_1,GND_1}),
        .s_axi_awqos({GND_1,GND_1,GND_1,GND_1}),
        .s_axi_awready(axi_displaybuffer_handle_0_axi_AWREADY),
        .s_axi_awregion({GND_1,GND_1,GND_1,GND_1}),
        .s_axi_awsize(axi_displaybuffer_handle_0_axi_AWSIZE),
        .s_axi_awvalid(axi_displaybuffer_handle_0_axi_AWVALID),
        .s_axi_bready(axi_displaybuffer_handle_0_axi_BREADY),
        .s_axi_bresp(axi_displaybuffer_handle_0_axi_BRESP),
        .s_axi_bvalid(axi_displaybuffer_handle_0_axi_BVALID),
        .s_axi_rdata(axi_displaybuffer_handle_0_axi_RDATA),
        .s_axi_rlast(axi_displaybuffer_handle_0_axi_RLAST),
        .s_axi_rready(axi_displaybuffer_handle_0_axi_RREADY),
        .s_axi_rresp(axi_displaybuffer_handle_0_axi_RRESP),
        .s_axi_rvalid(axi_displaybuffer_handle_0_axi_RVALID),
        .s_axi_wdata(axi_displaybuffer_handle_0_axi_WDATA),
        .s_axi_wlast(axi_displaybuffer_handle_0_axi_WLAST),
        .s_axi_wready(axi_displaybuffer_handle_0_axi_WREADY),
        .s_axi_wstrb(axi_displaybuffer_handle_0_axi_WSTRB),
        .s_axi_wvalid(axi_displaybuffer_handle_0_axi_WVALID));
cabac_modules_imp_AL9V63 cabac_modules
       (.S00_ACLK(processing_system7_0_FCLK_CLK0),
        .S00_AXI_araddr(processing_system7_0_M_AXI_GP1_ARADDR),
        .S00_AXI_arburst(processing_system7_0_M_AXI_GP1_ARBURST),
        .S00_AXI_arcache(processing_system7_0_M_AXI_GP1_ARCACHE),
        .S00_AXI_arid(processing_system7_0_M_AXI_GP1_ARID),
        .S00_AXI_arlen(processing_system7_0_M_AXI_GP1_ARLEN),
        .S00_AXI_arlock(processing_system7_0_M_AXI_GP1_ARLOCK),
        .S00_AXI_arprot(processing_system7_0_M_AXI_GP1_ARPROT),
        .S00_AXI_arqos(processing_system7_0_M_AXI_GP1_ARQOS),
        .S00_AXI_arready(processing_system7_0_M_AXI_GP1_ARREADY),
        .S00_AXI_arsize(processing_system7_0_M_AXI_GP1_ARSIZE),
        .S00_AXI_arvalid(processing_system7_0_M_AXI_GP1_ARVALID),
        .S00_AXI_awaddr(processing_system7_0_M_AXI_GP1_AWADDR),
        .S00_AXI_awburst(processing_system7_0_M_AXI_GP1_AWBURST),
        .S00_AXI_awcache(processing_system7_0_M_AXI_GP1_AWCACHE),
        .S00_AXI_awid(processing_system7_0_M_AXI_GP1_AWID),
        .S00_AXI_awlen(processing_system7_0_M_AXI_GP1_AWLEN),
        .S00_AXI_awlock(processing_system7_0_M_AXI_GP1_AWLOCK),
        .S00_AXI_awprot(processing_system7_0_M_AXI_GP1_AWPROT),
        .S00_AXI_awqos(processing_system7_0_M_AXI_GP1_AWQOS),
        .S00_AXI_awready(processing_system7_0_M_AXI_GP1_AWREADY),
        .S00_AXI_awsize(processing_system7_0_M_AXI_GP1_AWSIZE),
        .S00_AXI_awvalid(processing_system7_0_M_AXI_GP1_AWVALID),
        .S00_AXI_bid(processing_system7_0_M_AXI_GP1_BID),
        .S00_AXI_bready(processing_system7_0_M_AXI_GP1_BREADY),
        .S00_AXI_bresp(processing_system7_0_M_AXI_GP1_BRESP),
        .S00_AXI_bvalid(processing_system7_0_M_AXI_GP1_BVALID),
        .S00_AXI_rdata(processing_system7_0_M_AXI_GP1_RDATA),
        .S00_AXI_rid(processing_system7_0_M_AXI_GP1_RID),
        .S00_AXI_rlast(processing_system7_0_M_AXI_GP1_RLAST),
        .S00_AXI_rready(processing_system7_0_M_AXI_GP1_RREADY),
        .S00_AXI_rresp(processing_system7_0_M_AXI_GP1_RRESP),
        .S00_AXI_rvalid(processing_system7_0_M_AXI_GP1_RVALID),
        .S00_AXI_wdata(processing_system7_0_M_AXI_GP1_WDATA),
        .S00_AXI_wid(processing_system7_0_M_AXI_GP1_WID),
        .S00_AXI_wlast(processing_system7_0_M_AXI_GP1_WLAST),
        .S00_AXI_wready(processing_system7_0_M_AXI_GP1_WREADY),
        .S00_AXI_wstrb(processing_system7_0_M_AXI_GP1_WSTRB),
        .S00_AXI_wvalid(processing_system7_0_M_AXI_GP1_WVALID),
        .bus_struct_reset(cabac_modules_bus_struct_reset),
        .ext_reset_in(Net2),
        .fifo_cb_wr_data(cabac_modules_fifo_cb_WR_DATA),
        .fifo_cb_wr_en(cabac_modules_fifo_cb_WR_EN),
        .fifo_cont_wr_data(cabac_modules_fifo_cont_WR_DATA),
        .fifo_cont_wr_en(cabac_modules_fifo_cont_WR_EN),
        .fifo_cr_wr_data(cabac_modules_fifo_cr_WR_DATA),
        .fifo_cr_wr_en(cabac_modules_fifo_cr_WR_EN),
        .fifo_y_wr_data(cabac_modules_fifo_y_WR_DATA),
        .fifo_y_wr_en(cabac_modules_fifo_y_WR_EN),
        .fullCb(res2pred_cb_prog_full),
        .fullCont(res2pred_config_prog_full),
        .fullCr(res2pred_cr_prog_full),
        .fullY(res2pred_y_prog_full));
dp_system_clk_wiz_0_0 clk_wiz_1
       (.clk_in1(mig_7series_0_ui_clk),
        .clk_out1(clk_wiz_1_clk_out1),
        .clk_out2(clk_wiz_1_clk_out2),
        .clk_out3(clk_wiz_1_clk_out3),
        .clk_out4(clk_wiz_1_clk_out4),
        .locked(clk_wiz_1_locked),
        .reset(mig_setup_reset_0_design_reset));
dec_wo_cabac_imp_17VLMZ1 dec_wo_cabac
       (.DDR3_addr(mig_7series_0_DDR3_ADDR),
        .DDR3_ba(mig_7series_0_DDR3_BA),
        .DDR3_cas_n(mig_7series_0_DDR3_CAS_N),
        .DDR3_ck_n(mig_7series_0_DDR3_CK_N),
        .DDR3_ck_p(mig_7series_0_DDR3_CK_P),
        .DDR3_cke(mig_7series_0_DDR3_CKE),
        .DDR3_cs_n(mig_7series_0_DDR3_CS_N),
        .DDR3_dm(mig_7series_0_DDR3_DM),
        .DDR3_dq(DDR3_dq[63:0]),
        .DDR3_dqs_n(DDR3_dqs_n[7:0]),
        .DDR3_dqs_p(DDR3_dqs_p[7:0]),
        .DDR3_odt(mig_7series_0_DDR3_ODT),
        .DDR3_ras_n(mig_7series_0_DDR3_RAS_N),
        .DDR3_reset_n(mig_7series_0_DDR3_RESET_N),
        .DDR3_we_n(mig_7series_0_DDR3_WE_N),
        .SYS_CLK1_clk_n(SYS_CLK_1_CLK_N),
        .SYS_CLK1_clk_p(SYS_CLK_1_CLK_P),
        .cb_rd_data_count_in(res2pred_cb_rd_data_count),
        .cb_residual_read_inf_empty(dec_wo_cabac_cb_residual_read_inf_EMPTY),
        .cb_residual_read_inf_rd_data(dec_wo_cabac_cb_residual_read_inf_RD_DATA),
        .cb_residual_read_inf_rd_en(dec_wo_cabac_cb_residual_read_inf_RD_EN),
        .config_fifo_read_info_rd_data(dec_wo_cabac_config_fifo_read_info_RD_DATA),
        .config_fifo_read_info_rd_en(dec_wo_cabac_config_fifo_read_info_RD_EN),
        .cr_rd_data_count_in(res2pred_cr_rd_data_count),
        .cr_residual_read_inf_empty(dec_wo_cabac_cr_residual_read_inf_EMPTY),
        .cr_residual_read_inf_rd_data(dec_wo_cabac_cr_residual_read_inf_RD_DATA),
        .cr_residual_read_inf_rd_en(dec_wo_cabac_cr_residual_read_inf_RD_EN),
        .design_reset(mig_setup_reset_0_design_reset),
        .display_fifo_is_full(dis_buf_in_fifo_prog_full),
        .display_fifo_write_inf_wr_data(hevc_top_wo_dp_fifo_0_display_fifo_write_inf_WR_DATA),
        .display_fifo_write_inf_wr_en(hevc_top_wo_dp_fifo_0_display_fifo_write_inf_WR_EN),
        .input_fifo_is_empty(res2pred_config_prog_empty),
        .pic_height_out(hevc_top_wo_dp_fifo_0_pic_height_out),
        .pic_width_out(hevc_top_wo_dp_fifo_0_pic_width_out),
        .sao_poc_out(hevc_top_wo_dp_fifo_0_sao_poc_out),
        .sys_rst(cabac_modules_bus_struct_reset),
        .ui_clk(mig_7series_0_ui_clk),
        .y_rd_data_count_in(res2pred_y_rd_data_count),
        .y_residual_interface_empty(dec_wo_cabac_y_residual_interface_EMPTY),
        .y_residual_interface_rd_data(dec_wo_cabac_y_residual_interface_RD_DATA),
        .y_residual_interface_rd_en(dec_wo_cabac_y_residual_interface_RD_EN));
dp_system_dis_buf_in_fifo_0 dis_buf_in_fifo
       (.din(hevc_top_wo_dp_fifo_0_display_fifo_write_inf_WR_DATA),
        .dout(axi_displaybuffer_handle_0_fifo_read_rtl_1_RD_DATA),
        .empty(axi_displaybuffer_handle_0_fifo_read_rtl_1_EMPTY),
        .prog_full(dis_buf_in_fifo_prog_full),
        .rd_clk(clk_wiz_1_clk_out4),
        .rd_en(axi_displaybuffer_handle_0_fifo_read_rtl_1_RD_EN),
        .rst(mig_setup_reset_0_design_reset),
        .wr_clk(mig_7series_0_ui_clk),
        .wr_en(hevc_top_wo_dp_fifo_0_display_fifo_write_inf_WR_EN));
dp_system_dis_buf_out_fifo_0 dis_buf_out_fifo
       (.din(axi_displaybuffer_handle_0_hdmi_fifo_WR_DATA),
        .dout(dp_modules_dp_fifo_read_RD_DATA),
        .empty(dp_modules_dp_fifo_read_EMPTY),
        .prog_empty(dis_buf_out_fifo_prog_empty),
        .prog_full(dis_buf_out_fifo_prog_full),
        .rd_clk(dp_modules_clk_out1),
        .rd_en(dp_modules_dp_fifo_read_RD_EN),
        .rst(proc_sys_reset_1_peripheral_reset),
        .wr_clk(clk_wiz_1_clk_out4),
        .wr_en(axi_displaybuffer_handle_0_hdmi_fifo_WR_EN));
dp_modules_imp_TAI6SH dp_modules
       (.D(mig_setup_reset_0_design_reset),
        .aux_tx_io_n(aux_tx_io_n),
        .aux_tx_io_p(aux_tx_io_p),
        .axi_int(displayport_0_axi_int),
        .clk_out1(dp_modules_clk_out1),
        .dp_fifo_read_empty(dp_modules_dp_fifo_read_EMPTY),
        .dp_fifo_read_rd_data(dp_modules_dp_fifo_read_RD_DATA),
        .dp_fifo_read_rd_en(dp_modules_dp_fifo_read_RD_EN),
        .dp_mainlink_lnk_clk(displayport_0_dp_mainlink_LNK_CLK),
        .dp_mainlink_lnk_clk_n(displayport_0_dp_mainlink_LNK_CLK_N),
        .dp_mainlink_lnk_clk_p(displayport_0_dp_mainlink_LNK_CLK_P),
        .dp_mainlink_lnk_tx_lane_n(displayport_0_dp_mainlink_LNK_TX_LANE_N),
        .dp_mainlink_lnk_tx_lane_p(displayport_0_dp_mainlink_LNK_TX_LANE_P),
        .dp_s_axilite_araddr(axi4_lite_peripherals_M03_AXI_ARADDR),
        .dp_s_axilite_arprot(axi4_lite_peripherals_M03_AXI_ARPROT),
        .dp_s_axilite_arready(axi4_lite_peripherals_M03_AXI_ARREADY),
        .dp_s_axilite_arvalid(axi4_lite_peripherals_M03_AXI_ARVALID),
        .dp_s_axilite_awaddr(axi4_lite_peripherals_M03_AXI_AWADDR),
        .dp_s_axilite_awprot(axi4_lite_peripherals_M03_AXI_AWPROT),
        .dp_s_axilite_awready(axi4_lite_peripherals_M03_AXI_AWREADY),
        .dp_s_axilite_awvalid(axi4_lite_peripherals_M03_AXI_AWVALID),
        .dp_s_axilite_bready(axi4_lite_peripherals_M03_AXI_BREADY),
        .dp_s_axilite_bresp(axi4_lite_peripherals_M03_AXI_BRESP),
        .dp_s_axilite_bvalid(axi4_lite_peripherals_M03_AXI_BVALID),
        .dp_s_axilite_rdata(axi4_lite_peripherals_M03_AXI_RDATA),
        .dp_s_axilite_rready(axi4_lite_peripherals_M03_AXI_RREADY),
        .dp_s_axilite_rresp(axi4_lite_peripherals_M03_AXI_RRESP),
        .dp_s_axilite_rvalid(axi4_lite_peripherals_M03_AXI_RVALID),
        .dp_s_axilite_wdata(axi4_lite_peripherals_M03_AXI_WDATA),
        .dp_s_axilite_wready(axi4_lite_peripherals_M03_AXI_WREADY),
        .dp_s_axilite_wstrb(axi4_lite_peripherals_M03_AXI_WSTRB),
        .dp_s_axilite_wvalid(axi4_lite_peripherals_M03_AXI_WVALID),
        .fifo_prog_empty(dis_buf_out_fifo_prog_empty),
        .progclk_i(clk_wiz_1_clk_out1),
        .s_axi_aclk(clk_wiz_1_clk_out3),
        .s_axi_aresetn(proc_sys_reset_1_peripheral_aresetn),
        .tx_hpd(tx_hpd_1),
        .vid_pat_apb_inf_paddr(axi4_lite_peripherals_APB_M_PADDR),
        .vid_pat_apb_inf_penable(axi4_lite_peripherals_APB_M_PENABLE),
        .vid_pat_apb_inf_prdata(axi4_lite_peripherals_APB_M_PRDATA),
        .vid_pat_apb_inf_pready(axi4_lite_peripherals_APB_M_PREADY),
        .vid_pat_apb_inf_psel(axi4_lite_peripherals_APB_M_PSEL),
        .vid_pat_apb_inf_pslverr(axi4_lite_peripherals_APB_M_PSLVERR),
        .vid_pat_apb_inf_pwdata(axi4_lite_peripherals_APB_M_PWDATA),
        .vid_pat_apb_inf_pwrite(axi4_lite_peripherals_APB_M_PWRITE));
dp_system_processing_system7_0_0 processing_system7_0
       (.Core0_nFIQ(GND_1),
        .DDR_Addr(DDR_addr[14:0]),
        .DDR_BankAddr(DDR_ba[2:0]),
        .DDR_CAS_n(DDR_cas_n),
        .DDR_CKE(DDR_cke),
        .DDR_CS_n(DDR_cs_n),
        .DDR_Clk(DDR_ck_p),
        .DDR_Clk_n(DDR_ck_n),
        .DDR_DM(DDR_dm[3:0]),
        .DDR_DQ(DDR_dq[31:0]),
        .DDR_DQS(DDR_dqs_p[3:0]),
        .DDR_DQS_n(DDR_dqs_n[3:0]),
        .DDR_DRSTB(DDR_reset_n),
        .DDR_ODT(DDR_odt),
        .DDR_RAS_n(DDR_ras_n),
        .DDR_VRN(FIXED_IO_ddr_vrn),
        .DDR_VRP(FIXED_IO_ddr_vrp),
        .DDR_WEB(DDR_we_n),
        .FCLK_CLK0(processing_system7_0_FCLK_CLK0),
        .FCLK_RESET0_N(Net2),
        .MIO(FIXED_IO_mio[53:0]),
        .M_AXI_GP0_ACLK(clk_wiz_1_clk_out3),
        .M_AXI_GP0_ARADDR(processing_system7_0_M_AXI_GP0_ARADDR),
        .M_AXI_GP0_ARBURST(processing_system7_0_M_AXI_GP0_ARBURST),
        .M_AXI_GP0_ARCACHE(processing_system7_0_M_AXI_GP0_ARCACHE),
        .M_AXI_GP0_ARID(processing_system7_0_M_AXI_GP0_ARID),
        .M_AXI_GP0_ARLEN(processing_system7_0_M_AXI_GP0_ARLEN),
        .M_AXI_GP0_ARLOCK(processing_system7_0_M_AXI_GP0_ARLOCK),
        .M_AXI_GP0_ARPROT(processing_system7_0_M_AXI_GP0_ARPROT),
        .M_AXI_GP0_ARQOS(processing_system7_0_M_AXI_GP0_ARQOS),
        .M_AXI_GP0_ARREADY(processing_system7_0_M_AXI_GP0_ARREADY),
        .M_AXI_GP0_ARSIZE(processing_system7_0_M_AXI_GP0_ARSIZE),
        .M_AXI_GP0_ARVALID(processing_system7_0_M_AXI_GP0_ARVALID),
        .M_AXI_GP0_AWADDR(processing_system7_0_M_AXI_GP0_AWADDR),
        .M_AXI_GP0_AWBURST(processing_system7_0_M_AXI_GP0_AWBURST),
        .M_AXI_GP0_AWCACHE(processing_system7_0_M_AXI_GP0_AWCACHE),
        .M_AXI_GP0_AWID(processing_system7_0_M_AXI_GP0_AWID),
        .M_AXI_GP0_AWLEN(processing_system7_0_M_AXI_GP0_AWLEN),
        .M_AXI_GP0_AWLOCK(processing_system7_0_M_AXI_GP0_AWLOCK),
        .M_AXI_GP0_AWPROT(processing_system7_0_M_AXI_GP0_AWPROT),
        .M_AXI_GP0_AWQOS(processing_system7_0_M_AXI_GP0_AWQOS),
        .M_AXI_GP0_AWREADY(processing_system7_0_M_AXI_GP0_AWREADY),
        .M_AXI_GP0_AWSIZE(processing_system7_0_M_AXI_GP0_AWSIZE),
        .M_AXI_GP0_AWVALID(processing_system7_0_M_AXI_GP0_AWVALID),
        .M_AXI_GP0_BID(processing_system7_0_M_AXI_GP0_BID),
        .M_AXI_GP0_BREADY(processing_system7_0_M_AXI_GP0_BREADY),
        .M_AXI_GP0_BRESP(processing_system7_0_M_AXI_GP0_BRESP),
        .M_AXI_GP0_BVALID(processing_system7_0_M_AXI_GP0_BVALID),
        .M_AXI_GP0_RDATA(processing_system7_0_M_AXI_GP0_RDATA),
        .M_AXI_GP0_RID(processing_system7_0_M_AXI_GP0_RID),
        .M_AXI_GP0_RLAST(processing_system7_0_M_AXI_GP0_RLAST),
        .M_AXI_GP0_RREADY(processing_system7_0_M_AXI_GP0_RREADY),
        .M_AXI_GP0_RRESP(processing_system7_0_M_AXI_GP0_RRESP),
        .M_AXI_GP0_RVALID(processing_system7_0_M_AXI_GP0_RVALID),
        .M_AXI_GP0_WDATA(processing_system7_0_M_AXI_GP0_WDATA),
        .M_AXI_GP0_WID(processing_system7_0_M_AXI_GP0_WID),
        .M_AXI_GP0_WLAST(processing_system7_0_M_AXI_GP0_WLAST),
        .M_AXI_GP0_WREADY(processing_system7_0_M_AXI_GP0_WREADY),
        .M_AXI_GP0_WSTRB(processing_system7_0_M_AXI_GP0_WSTRB),
        .M_AXI_GP0_WVALID(processing_system7_0_M_AXI_GP0_WVALID),
        .M_AXI_GP1_ACLK(processing_system7_0_FCLK_CLK0),
        .M_AXI_GP1_ARADDR(processing_system7_0_M_AXI_GP1_ARADDR),
        .M_AXI_GP1_ARBURST(processing_system7_0_M_AXI_GP1_ARBURST),
        .M_AXI_GP1_ARCACHE(processing_system7_0_M_AXI_GP1_ARCACHE),
        .M_AXI_GP1_ARID(processing_system7_0_M_AXI_GP1_ARID),
        .M_AXI_GP1_ARLEN(processing_system7_0_M_AXI_GP1_ARLEN),
        .M_AXI_GP1_ARLOCK(processing_system7_0_M_AXI_GP1_ARLOCK),
        .M_AXI_GP1_ARPROT(processing_system7_0_M_AXI_GP1_ARPROT),
        .M_AXI_GP1_ARQOS(processing_system7_0_M_AXI_GP1_ARQOS),
        .M_AXI_GP1_ARREADY(processing_system7_0_M_AXI_GP1_ARREADY),
        .M_AXI_GP1_ARSIZE(processing_system7_0_M_AXI_GP1_ARSIZE),
        .M_AXI_GP1_ARVALID(processing_system7_0_M_AXI_GP1_ARVALID),
        .M_AXI_GP1_AWADDR(processing_system7_0_M_AXI_GP1_AWADDR),
        .M_AXI_GP1_AWBURST(processing_system7_0_M_AXI_GP1_AWBURST),
        .M_AXI_GP1_AWCACHE(processing_system7_0_M_AXI_GP1_AWCACHE),
        .M_AXI_GP1_AWID(processing_system7_0_M_AXI_GP1_AWID),
        .M_AXI_GP1_AWLEN(processing_system7_0_M_AXI_GP1_AWLEN),
        .M_AXI_GP1_AWLOCK(processing_system7_0_M_AXI_GP1_AWLOCK),
        .M_AXI_GP1_AWPROT(processing_system7_0_M_AXI_GP1_AWPROT),
        .M_AXI_GP1_AWQOS(processing_system7_0_M_AXI_GP1_AWQOS),
        .M_AXI_GP1_AWREADY(processing_system7_0_M_AXI_GP1_AWREADY),
        .M_AXI_GP1_AWSIZE(processing_system7_0_M_AXI_GP1_AWSIZE),
        .M_AXI_GP1_AWVALID(processing_system7_0_M_AXI_GP1_AWVALID),
        .M_AXI_GP1_BID(processing_system7_0_M_AXI_GP1_BID),
        .M_AXI_GP1_BREADY(processing_system7_0_M_AXI_GP1_BREADY),
        .M_AXI_GP1_BRESP(processing_system7_0_M_AXI_GP1_BRESP),
        .M_AXI_GP1_BVALID(processing_system7_0_M_AXI_GP1_BVALID),
        .M_AXI_GP1_RDATA(processing_system7_0_M_AXI_GP1_RDATA),
        .M_AXI_GP1_RID(processing_system7_0_M_AXI_GP1_RID),
        .M_AXI_GP1_RLAST(processing_system7_0_M_AXI_GP1_RLAST),
        .M_AXI_GP1_RREADY(processing_system7_0_M_AXI_GP1_RREADY),
        .M_AXI_GP1_RRESP(processing_system7_0_M_AXI_GP1_RRESP),
        .M_AXI_GP1_RVALID(processing_system7_0_M_AXI_GP1_RVALID),
        .M_AXI_GP1_WDATA(processing_system7_0_M_AXI_GP1_WDATA),
        .M_AXI_GP1_WID(processing_system7_0_M_AXI_GP1_WID),
        .M_AXI_GP1_WLAST(processing_system7_0_M_AXI_GP1_WLAST),
        .M_AXI_GP1_WREADY(processing_system7_0_M_AXI_GP1_WREADY),
        .M_AXI_GP1_WSTRB(processing_system7_0_M_AXI_GP1_WSTRB),
        .M_AXI_GP1_WVALID(processing_system7_0_M_AXI_GP1_WVALID),
        .PS_CLK(FIXED_IO_ps_clk),
        .PS_PORB(FIXED_IO_ps_porb),
        .PS_SRSTB(FIXED_IO_ps_srstb),
        .S_AXI_HP0_ACLK(clk_wiz_1_clk_out4),
        .S_AXI_HP0_ARADDR(axi_dwidth_converter_0_M_AXI_ARADDR),
        .S_AXI_HP0_ARBURST(axi_dwidth_converter_0_M_AXI_ARBURST),
        .S_AXI_HP0_ARCACHE(axi_dwidth_converter_0_M_AXI_ARCACHE),
        .S_AXI_HP0_ARID({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_HP0_ARLEN(axi_dwidth_converter_0_M_AXI_ARLEN),
        .S_AXI_HP0_ARLOCK(axi_dwidth_converter_0_M_AXI_ARLOCK),
        .S_AXI_HP0_ARPROT(axi_dwidth_converter_0_M_AXI_ARPROT),
        .S_AXI_HP0_ARQOS(axi_dwidth_converter_0_M_AXI_ARQOS),
        .S_AXI_HP0_ARREADY(axi_dwidth_converter_0_M_AXI_ARREADY),
        .S_AXI_HP0_ARSIZE(axi_dwidth_converter_0_M_AXI_ARSIZE),
        .S_AXI_HP0_ARVALID(axi_dwidth_converter_0_M_AXI_ARVALID),
        .S_AXI_HP0_AWADDR(axi_dwidth_converter_0_M_AXI_AWADDR),
        .S_AXI_HP0_AWBURST(axi_dwidth_converter_0_M_AXI_AWBURST),
        .S_AXI_HP0_AWCACHE(axi_dwidth_converter_0_M_AXI_AWCACHE),
        .S_AXI_HP0_AWID({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_HP0_AWLEN(axi_dwidth_converter_0_M_AXI_AWLEN),
        .S_AXI_HP0_AWLOCK(axi_dwidth_converter_0_M_AXI_AWLOCK),
        .S_AXI_HP0_AWPROT(axi_dwidth_converter_0_M_AXI_AWPROT),
        .S_AXI_HP0_AWQOS(axi_dwidth_converter_0_M_AXI_AWQOS),
        .S_AXI_HP0_AWREADY(axi_dwidth_converter_0_M_AXI_AWREADY),
        .S_AXI_HP0_AWSIZE(axi_dwidth_converter_0_M_AXI_AWSIZE),
        .S_AXI_HP0_AWVALID(axi_dwidth_converter_0_M_AXI_AWVALID),
        .S_AXI_HP0_BREADY(axi_dwidth_converter_0_M_AXI_BREADY),
        .S_AXI_HP0_BRESP(axi_dwidth_converter_0_M_AXI_BRESP),
        .S_AXI_HP0_BVALID(axi_dwidth_converter_0_M_AXI_BVALID),
        .S_AXI_HP0_RDATA(axi_dwidth_converter_0_M_AXI_RDATA),
        .S_AXI_HP0_RDISSUECAP1_EN(GND_1),
        .S_AXI_HP0_RLAST(axi_dwidth_converter_0_M_AXI_RLAST),
        .S_AXI_HP0_RREADY(axi_dwidth_converter_0_M_AXI_RREADY),
        .S_AXI_HP0_RRESP(axi_dwidth_converter_0_M_AXI_RRESP),
        .S_AXI_HP0_RVALID(axi_dwidth_converter_0_M_AXI_RVALID),
        .S_AXI_HP0_WDATA(axi_dwidth_converter_0_M_AXI_WDATA),
        .S_AXI_HP0_WID({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_HP0_WLAST(axi_dwidth_converter_0_M_AXI_WLAST),
        .S_AXI_HP0_WREADY(axi_dwidth_converter_0_M_AXI_WREADY),
        .S_AXI_HP0_WRISSUECAP1_EN(GND_1),
        .S_AXI_HP0_WSTRB(axi_dwidth_converter_0_M_AXI_WSTRB),
        .S_AXI_HP0_WVALID(axi_dwidth_converter_0_M_AXI_WVALID));
dp_system_res2pred_cb_0 res2pred_cb
       (.din(cabac_modules_fifo_cb_WR_DATA),
        .dout(dec_wo_cabac_cb_residual_read_inf_RD_DATA),
        .empty(dec_wo_cabac_cb_residual_read_inf_EMPTY),
        .prog_full(res2pred_cb_prog_full),
        .rd_clk(mig_7series_0_ui_clk),
        .rd_data_count(res2pred_cb_rd_data_count),
        .rd_en(dec_wo_cabac_cb_residual_read_inf_RD_EN),
        .rst(mig_setup_reset_0_design_reset),
        .wr_clk(processing_system7_0_FCLK_CLK0),
        .wr_en(cabac_modules_fifo_cb_WR_EN));
dp_system_res2pred_config_0 res2pred_config
       (.din(cabac_modules_fifo_cont_WR_DATA),
        .dout(dec_wo_cabac_config_fifo_read_info_RD_DATA),
        .prog_empty(res2pred_config_prog_empty),
        .prog_full(res2pred_config_prog_full),
        .rd_clk(mig_7series_0_ui_clk),
        .rd_en(dec_wo_cabac_config_fifo_read_info_RD_EN),
        .rst(mig_setup_reset_0_design_reset),
        .wr_clk(processing_system7_0_FCLK_CLK0),
        .wr_en(cabac_modules_fifo_cont_WR_EN));
dp_system_res2pred_cr_0 res2pred_cr
       (.din(cabac_modules_fifo_cr_WR_DATA),
        .dout(dec_wo_cabac_cr_residual_read_inf_RD_DATA),
        .empty(dec_wo_cabac_cr_residual_read_inf_EMPTY),
        .prog_full(res2pred_cr_prog_full),
        .rd_clk(mig_7series_0_ui_clk),
        .rd_data_count(res2pred_cr_rd_data_count),
        .rd_en(dec_wo_cabac_cr_residual_read_inf_RD_EN),
        .rst(mig_setup_reset_0_design_reset),
        .wr_clk(processing_system7_0_FCLK_CLK0),
        .wr_en(cabac_modules_fifo_cr_WR_EN));
dp_system_res2pred_y_0 res2pred_y
       (.din(cabac_modules_fifo_y_WR_DATA),
        .dout(dec_wo_cabac_y_residual_interface_RD_DATA),
        .empty(dec_wo_cabac_y_residual_interface_EMPTY),
        .prog_full(res2pred_y_prog_full),
        .rd_clk(mig_7series_0_ui_clk),
        .rd_data_count(res2pred_y_rd_data_count),
        .rd_en(dec_wo_cabac_y_residual_interface_RD_EN),
        .rst(mig_setup_reset_0_design_reset),
        .wr_clk(processing_system7_0_FCLK_CLK0),
        .wr_en(cabac_modules_fifo_y_WR_EN));
endmodule

module dp_system_axi_interconnect_1_0
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arready,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awready,
    M00_AXI_awvalid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    M01_ACLK,
    M01_ARESETN,
    M01_AXI_araddr,
    M01_AXI_arready,
    M01_AXI_arvalid,
    M01_AXI_awaddr,
    M01_AXI_awready,
    M01_AXI_awvalid,
    M01_AXI_bready,
    M01_AXI_bresp,
    M01_AXI_bvalid,
    M01_AXI_rdata,
    M01_AXI_rready,
    M01_AXI_rresp,
    M01_AXI_rvalid,
    M01_AXI_wdata,
    M01_AXI_wready,
    M01_AXI_wvalid,
    M02_ACLK,
    M02_ARESETN,
    M02_AXI_araddr,
    M02_AXI_arready,
    M02_AXI_arvalid,
    M02_AXI_awaddr,
    M02_AXI_awready,
    M02_AXI_awvalid,
    M02_AXI_bready,
    M02_AXI_bresp,
    M02_AXI_bvalid,
    M02_AXI_rdata,
    M02_AXI_rready,
    M02_AXI_rresp,
    M02_AXI_rvalid,
    M02_AXI_wdata,
    M02_AXI_wready,
    M02_AXI_wstrb,
    M02_AXI_wvalid,
    M03_ACLK,
    M03_ARESETN,
    M03_AXI_araddr,
    M03_AXI_arprot,
    M03_AXI_arready,
    M03_AXI_arvalid,
    M03_AXI_awaddr,
    M03_AXI_awprot,
    M03_AXI_awready,
    M03_AXI_awvalid,
    M03_AXI_bready,
    M03_AXI_bresp,
    M03_AXI_bvalid,
    M03_AXI_rdata,
    M03_AXI_rready,
    M03_AXI_rresp,
    M03_AXI_rvalid,
    M03_AXI_wdata,
    M03_AXI_wready,
    M03_AXI_wstrb,
    M03_AXI_wvalid,
    M04_ACLK,
    M04_ARESETN,
    M04_AXI_araddr,
    M04_AXI_arready,
    M04_AXI_arvalid,
    M04_AXI_awaddr,
    M04_AXI_awready,
    M04_AXI_awvalid,
    M04_AXI_bready,
    M04_AXI_bresp,
    M04_AXI_bvalid,
    M04_AXI_rdata,
    M04_AXI_rready,
    M04_AXI_rresp,
    M04_AXI_rvalid,
    M04_AXI_wdata,
    M04_AXI_wready,
    M04_AXI_wstrb,
    M04_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arburst,
    S00_AXI_arcache,
    S00_AXI_arid,
    S00_AXI_arlen,
    S00_AXI_arlock,
    S00_AXI_arprot,
    S00_AXI_arqos,
    S00_AXI_arready,
    S00_AXI_arsize,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awburst,
    S00_AXI_awcache,
    S00_AXI_awid,
    S00_AXI_awlen,
    S00_AXI_awlock,
    S00_AXI_awprot,
    S00_AXI_awqos,
    S00_AXI_awready,
    S00_AXI_awsize,
    S00_AXI_awvalid,
    S00_AXI_bid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rid,
    S00_AXI_rlast,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wid,
    S00_AXI_wlast,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid);
  input ACLK;
  input [0:0]ARESETN;
  input M00_ACLK;
  input [0:0]M00_ARESETN;
  output [8:0]M00_AXI_araddr;
  input [0:0]M00_AXI_arready;
  output [0:0]M00_AXI_arvalid;
  output [8:0]M00_AXI_awaddr;
  input [0:0]M00_AXI_awready;
  output [0:0]M00_AXI_awvalid;
  output [0:0]M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input [0:0]M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  output [0:0]M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input [0:0]M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  input [0:0]M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output [0:0]M00_AXI_wvalid;
  input M01_ACLK;
  input [0:0]M01_ARESETN;
  output [31:0]M01_AXI_araddr;
  input [0:0]M01_AXI_arready;
  output [0:0]M01_AXI_arvalid;
  output [31:0]M01_AXI_awaddr;
  input [0:0]M01_AXI_awready;
  output [0:0]M01_AXI_awvalid;
  output [0:0]M01_AXI_bready;
  input [1:0]M01_AXI_bresp;
  input [0:0]M01_AXI_bvalid;
  input [31:0]M01_AXI_rdata;
  output [0:0]M01_AXI_rready;
  input [1:0]M01_AXI_rresp;
  input [0:0]M01_AXI_rvalid;
  output [31:0]M01_AXI_wdata;
  input [0:0]M01_AXI_wready;
  output [0:0]M01_AXI_wvalid;
  input M02_ACLK;
  input [0:0]M02_ARESETN;
  output [4:0]M02_AXI_araddr;
  input [0:0]M02_AXI_arready;
  output [0:0]M02_AXI_arvalid;
  output [4:0]M02_AXI_awaddr;
  input [0:0]M02_AXI_awready;
  output [0:0]M02_AXI_awvalid;
  output [0:0]M02_AXI_bready;
  input [1:0]M02_AXI_bresp;
  input [0:0]M02_AXI_bvalid;
  input [31:0]M02_AXI_rdata;
  output [0:0]M02_AXI_rready;
  input [1:0]M02_AXI_rresp;
  input [0:0]M02_AXI_rvalid;
  output [31:0]M02_AXI_wdata;
  input [0:0]M02_AXI_wready;
  output [3:0]M02_AXI_wstrb;
  output [0:0]M02_AXI_wvalid;
  input M03_ACLK;
  input [0:0]M03_ARESETN;
  output [31:0]M03_AXI_araddr;
  output [2:0]M03_AXI_arprot;
  input M03_AXI_arready;
  output M03_AXI_arvalid;
  output [31:0]M03_AXI_awaddr;
  output [2:0]M03_AXI_awprot;
  input M03_AXI_awready;
  output M03_AXI_awvalid;
  output M03_AXI_bready;
  input [1:0]M03_AXI_bresp;
  input M03_AXI_bvalid;
  input [31:0]M03_AXI_rdata;
  output M03_AXI_rready;
  input [1:0]M03_AXI_rresp;
  input M03_AXI_rvalid;
  output [31:0]M03_AXI_wdata;
  input M03_AXI_wready;
  output [3:0]M03_AXI_wstrb;
  output M03_AXI_wvalid;
  input M04_ACLK;
  input [0:0]M04_ARESETN;
  output [8:0]M04_AXI_araddr;
  input [0:0]M04_AXI_arready;
  output [0:0]M04_AXI_arvalid;
  output [8:0]M04_AXI_awaddr;
  input [0:0]M04_AXI_awready;
  output [0:0]M04_AXI_awvalid;
  output [0:0]M04_AXI_bready;
  input [1:0]M04_AXI_bresp;
  input [0:0]M04_AXI_bvalid;
  input [31:0]M04_AXI_rdata;
  output [0:0]M04_AXI_rready;
  input [1:0]M04_AXI_rresp;
  input [0:0]M04_AXI_rvalid;
  output [31:0]M04_AXI_wdata;
  input [0:0]M04_AXI_wready;
  output [3:0]M04_AXI_wstrb;
  output [0:0]M04_AXI_wvalid;
  input S00_ACLK;
  input [0:0]S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [1:0]S00_AXI_arburst;
  input [3:0]S00_AXI_arcache;
  input [11:0]S00_AXI_arid;
  input [3:0]S00_AXI_arlen;
  input [1:0]S00_AXI_arlock;
  input [2:0]S00_AXI_arprot;
  input [3:0]S00_AXI_arqos;
  output S00_AXI_arready;
  input [2:0]S00_AXI_arsize;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [1:0]S00_AXI_awburst;
  input [3:0]S00_AXI_awcache;
  input [11:0]S00_AXI_awid;
  input [3:0]S00_AXI_awlen;
  input [1:0]S00_AXI_awlock;
  input [2:0]S00_AXI_awprot;
  input [3:0]S00_AXI_awqos;
  output S00_AXI_awready;
  input [2:0]S00_AXI_awsize;
  input S00_AXI_awvalid;
  output [11:0]S00_AXI_bid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  output [11:0]S00_AXI_rid;
  output S00_AXI_rlast;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  input [11:0]S00_AXI_wid;
  input S00_AXI_wlast;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;

  wire M00_ACLK_1;
  wire [0:0]M00_ARESETN_1;
  wire M01_ACLK_1;
  wire [0:0]M01_ARESETN_1;
  wire M02_ACLK_1;
  wire [0:0]M02_ARESETN_1;
  wire M03_ACLK_1;
  wire [0:0]M03_ARESETN_1;
  wire M04_ACLK_1;
  wire [0:0]M04_ARESETN_1;
  wire S00_ACLK_1;
  wire [0:0]S00_ARESETN_1;
  wire axi_interconnect_1_ACLK_net;
  wire [0:0]axi_interconnect_1_ARESETN_net;
  wire [31:0]axi_interconnect_1_to_s00_couplers_ARADDR;
  wire [1:0]axi_interconnect_1_to_s00_couplers_ARBURST;
  wire [3:0]axi_interconnect_1_to_s00_couplers_ARCACHE;
  wire [11:0]axi_interconnect_1_to_s00_couplers_ARID;
  wire [3:0]axi_interconnect_1_to_s00_couplers_ARLEN;
  wire [1:0]axi_interconnect_1_to_s00_couplers_ARLOCK;
  wire [2:0]axi_interconnect_1_to_s00_couplers_ARPROT;
  wire [3:0]axi_interconnect_1_to_s00_couplers_ARQOS;
  wire axi_interconnect_1_to_s00_couplers_ARREADY;
  wire [2:0]axi_interconnect_1_to_s00_couplers_ARSIZE;
  wire axi_interconnect_1_to_s00_couplers_ARVALID;
  wire [31:0]axi_interconnect_1_to_s00_couplers_AWADDR;
  wire [1:0]axi_interconnect_1_to_s00_couplers_AWBURST;
  wire [3:0]axi_interconnect_1_to_s00_couplers_AWCACHE;
  wire [11:0]axi_interconnect_1_to_s00_couplers_AWID;
  wire [3:0]axi_interconnect_1_to_s00_couplers_AWLEN;
  wire [1:0]axi_interconnect_1_to_s00_couplers_AWLOCK;
  wire [2:0]axi_interconnect_1_to_s00_couplers_AWPROT;
  wire [3:0]axi_interconnect_1_to_s00_couplers_AWQOS;
  wire axi_interconnect_1_to_s00_couplers_AWREADY;
  wire [2:0]axi_interconnect_1_to_s00_couplers_AWSIZE;
  wire axi_interconnect_1_to_s00_couplers_AWVALID;
  wire [11:0]axi_interconnect_1_to_s00_couplers_BID;
  wire axi_interconnect_1_to_s00_couplers_BREADY;
  wire [1:0]axi_interconnect_1_to_s00_couplers_BRESP;
  wire axi_interconnect_1_to_s00_couplers_BVALID;
  wire [31:0]axi_interconnect_1_to_s00_couplers_RDATA;
  wire [11:0]axi_interconnect_1_to_s00_couplers_RID;
  wire axi_interconnect_1_to_s00_couplers_RLAST;
  wire axi_interconnect_1_to_s00_couplers_RREADY;
  wire [1:0]axi_interconnect_1_to_s00_couplers_RRESP;
  wire axi_interconnect_1_to_s00_couplers_RVALID;
  wire [31:0]axi_interconnect_1_to_s00_couplers_WDATA;
  wire [11:0]axi_interconnect_1_to_s00_couplers_WID;
  wire axi_interconnect_1_to_s00_couplers_WLAST;
  wire axi_interconnect_1_to_s00_couplers_WREADY;
  wire [3:0]axi_interconnect_1_to_s00_couplers_WSTRB;
  wire axi_interconnect_1_to_s00_couplers_WVALID;
  wire [8:0]m00_couplers_to_axi_interconnect_1_ARADDR;
  wire [0:0]m00_couplers_to_axi_interconnect_1_ARREADY;
  wire [0:0]m00_couplers_to_axi_interconnect_1_ARVALID;
  wire [8:0]m00_couplers_to_axi_interconnect_1_AWADDR;
  wire [0:0]m00_couplers_to_axi_interconnect_1_AWREADY;
  wire [0:0]m00_couplers_to_axi_interconnect_1_AWVALID;
  wire [0:0]m00_couplers_to_axi_interconnect_1_BREADY;
  wire [1:0]m00_couplers_to_axi_interconnect_1_BRESP;
  wire [0:0]m00_couplers_to_axi_interconnect_1_BVALID;
  wire [31:0]m00_couplers_to_axi_interconnect_1_RDATA;
  wire [0:0]m00_couplers_to_axi_interconnect_1_RREADY;
  wire [1:0]m00_couplers_to_axi_interconnect_1_RRESP;
  wire [0:0]m00_couplers_to_axi_interconnect_1_RVALID;
  wire [31:0]m00_couplers_to_axi_interconnect_1_WDATA;
  wire [0:0]m00_couplers_to_axi_interconnect_1_WREADY;
  wire [3:0]m00_couplers_to_axi_interconnect_1_WSTRB;
  wire [0:0]m00_couplers_to_axi_interconnect_1_WVALID;
  wire [31:0]m01_couplers_to_axi_interconnect_1_ARADDR;
  wire [0:0]m01_couplers_to_axi_interconnect_1_ARREADY;
  wire [0:0]m01_couplers_to_axi_interconnect_1_ARVALID;
  wire [31:0]m01_couplers_to_axi_interconnect_1_AWADDR;
  wire [0:0]m01_couplers_to_axi_interconnect_1_AWREADY;
  wire [0:0]m01_couplers_to_axi_interconnect_1_AWVALID;
  wire [0:0]m01_couplers_to_axi_interconnect_1_BREADY;
  wire [1:0]m01_couplers_to_axi_interconnect_1_BRESP;
  wire [0:0]m01_couplers_to_axi_interconnect_1_BVALID;
  wire [31:0]m01_couplers_to_axi_interconnect_1_RDATA;
  wire [0:0]m01_couplers_to_axi_interconnect_1_RREADY;
  wire [1:0]m01_couplers_to_axi_interconnect_1_RRESP;
  wire [0:0]m01_couplers_to_axi_interconnect_1_RVALID;
  wire [31:0]m01_couplers_to_axi_interconnect_1_WDATA;
  wire [0:0]m01_couplers_to_axi_interconnect_1_WREADY;
  wire [0:0]m01_couplers_to_axi_interconnect_1_WVALID;
  wire [4:0]m02_couplers_to_axi_interconnect_1_ARADDR;
  wire [0:0]m02_couplers_to_axi_interconnect_1_ARREADY;
  wire [0:0]m02_couplers_to_axi_interconnect_1_ARVALID;
  wire [4:0]m02_couplers_to_axi_interconnect_1_AWADDR;
  wire [0:0]m02_couplers_to_axi_interconnect_1_AWREADY;
  wire [0:0]m02_couplers_to_axi_interconnect_1_AWVALID;
  wire [0:0]m02_couplers_to_axi_interconnect_1_BREADY;
  wire [1:0]m02_couplers_to_axi_interconnect_1_BRESP;
  wire [0:0]m02_couplers_to_axi_interconnect_1_BVALID;
  wire [31:0]m02_couplers_to_axi_interconnect_1_RDATA;
  wire [0:0]m02_couplers_to_axi_interconnect_1_RREADY;
  wire [1:0]m02_couplers_to_axi_interconnect_1_RRESP;
  wire [0:0]m02_couplers_to_axi_interconnect_1_RVALID;
  wire [31:0]m02_couplers_to_axi_interconnect_1_WDATA;
  wire [0:0]m02_couplers_to_axi_interconnect_1_WREADY;
  wire [3:0]m02_couplers_to_axi_interconnect_1_WSTRB;
  wire [0:0]m02_couplers_to_axi_interconnect_1_WVALID;
  wire [31:0]m03_couplers_to_axi_interconnect_1_ARADDR;
  wire [2:0]m03_couplers_to_axi_interconnect_1_ARPROT;
  wire m03_couplers_to_axi_interconnect_1_ARREADY;
  wire m03_couplers_to_axi_interconnect_1_ARVALID;
  wire [31:0]m03_couplers_to_axi_interconnect_1_AWADDR;
  wire [2:0]m03_couplers_to_axi_interconnect_1_AWPROT;
  wire m03_couplers_to_axi_interconnect_1_AWREADY;
  wire m03_couplers_to_axi_interconnect_1_AWVALID;
  wire m03_couplers_to_axi_interconnect_1_BREADY;
  wire [1:0]m03_couplers_to_axi_interconnect_1_BRESP;
  wire m03_couplers_to_axi_interconnect_1_BVALID;
  wire [31:0]m03_couplers_to_axi_interconnect_1_RDATA;
  wire m03_couplers_to_axi_interconnect_1_RREADY;
  wire [1:0]m03_couplers_to_axi_interconnect_1_RRESP;
  wire m03_couplers_to_axi_interconnect_1_RVALID;
  wire [31:0]m03_couplers_to_axi_interconnect_1_WDATA;
  wire m03_couplers_to_axi_interconnect_1_WREADY;
  wire [3:0]m03_couplers_to_axi_interconnect_1_WSTRB;
  wire m03_couplers_to_axi_interconnect_1_WVALID;
  wire [8:0]m04_couplers_to_axi_interconnect_1_ARADDR;
  wire [0:0]m04_couplers_to_axi_interconnect_1_ARREADY;
  wire [0:0]m04_couplers_to_axi_interconnect_1_ARVALID;
  wire [8:0]m04_couplers_to_axi_interconnect_1_AWADDR;
  wire [0:0]m04_couplers_to_axi_interconnect_1_AWREADY;
  wire [0:0]m04_couplers_to_axi_interconnect_1_AWVALID;
  wire [0:0]m04_couplers_to_axi_interconnect_1_BREADY;
  wire [1:0]m04_couplers_to_axi_interconnect_1_BRESP;
  wire [0:0]m04_couplers_to_axi_interconnect_1_BVALID;
  wire [31:0]m04_couplers_to_axi_interconnect_1_RDATA;
  wire [0:0]m04_couplers_to_axi_interconnect_1_RREADY;
  wire [1:0]m04_couplers_to_axi_interconnect_1_RRESP;
  wire [0:0]m04_couplers_to_axi_interconnect_1_RVALID;
  wire [31:0]m04_couplers_to_axi_interconnect_1_WDATA;
  wire [0:0]m04_couplers_to_axi_interconnect_1_WREADY;
  wire [3:0]m04_couplers_to_axi_interconnect_1_WSTRB;
  wire [0:0]m04_couplers_to_axi_interconnect_1_WVALID;
  wire [31:0]s00_couplers_to_xbar_ARADDR;
  wire [2:0]s00_couplers_to_xbar_ARPROT;
  wire [0:0]s00_couplers_to_xbar_ARREADY;
  wire s00_couplers_to_xbar_ARVALID;
  wire [31:0]s00_couplers_to_xbar_AWADDR;
  wire [2:0]s00_couplers_to_xbar_AWPROT;
  wire [0:0]s00_couplers_to_xbar_AWREADY;
  wire s00_couplers_to_xbar_AWVALID;
  wire s00_couplers_to_xbar_BREADY;
  wire [1:0]s00_couplers_to_xbar_BRESP;
  wire [0:0]s00_couplers_to_xbar_BVALID;
  wire [31:0]s00_couplers_to_xbar_RDATA;
  wire s00_couplers_to_xbar_RREADY;
  wire [1:0]s00_couplers_to_xbar_RRESP;
  wire [0:0]s00_couplers_to_xbar_RVALID;
  wire [31:0]s00_couplers_to_xbar_WDATA;
  wire [0:0]s00_couplers_to_xbar_WREADY;
  wire [3:0]s00_couplers_to_xbar_WSTRB;
  wire s00_couplers_to_xbar_WVALID;
  wire [31:0]xbar_to_m00_couplers_ARADDR;
  wire [0:0]xbar_to_m00_couplers_ARREADY;
  wire [0:0]xbar_to_m00_couplers_ARVALID;
  wire [31:0]xbar_to_m00_couplers_AWADDR;
  wire [0:0]xbar_to_m00_couplers_AWREADY;
  wire [0:0]xbar_to_m00_couplers_AWVALID;
  wire [0:0]xbar_to_m00_couplers_BREADY;
  wire [1:0]xbar_to_m00_couplers_BRESP;
  wire [0:0]xbar_to_m00_couplers_BVALID;
  wire [31:0]xbar_to_m00_couplers_RDATA;
  wire [0:0]xbar_to_m00_couplers_RREADY;
  wire [1:0]xbar_to_m00_couplers_RRESP;
  wire [0:0]xbar_to_m00_couplers_RVALID;
  wire [31:0]xbar_to_m00_couplers_WDATA;
  wire [0:0]xbar_to_m00_couplers_WREADY;
  wire [3:0]xbar_to_m00_couplers_WSTRB;
  wire [0:0]xbar_to_m00_couplers_WVALID;
  wire [63:32]xbar_to_m01_couplers_ARADDR;
  wire [0:0]xbar_to_m01_couplers_ARREADY;
  wire [1:1]xbar_to_m01_couplers_ARVALID;
  wire [63:32]xbar_to_m01_couplers_AWADDR;
  wire [0:0]xbar_to_m01_couplers_AWREADY;
  wire [1:1]xbar_to_m01_couplers_AWVALID;
  wire [1:1]xbar_to_m01_couplers_BREADY;
  wire [1:0]xbar_to_m01_couplers_BRESP;
  wire [0:0]xbar_to_m01_couplers_BVALID;
  wire [31:0]xbar_to_m01_couplers_RDATA;
  wire [1:1]xbar_to_m01_couplers_RREADY;
  wire [1:0]xbar_to_m01_couplers_RRESP;
  wire [0:0]xbar_to_m01_couplers_RVALID;
  wire [63:32]xbar_to_m01_couplers_WDATA;
  wire [0:0]xbar_to_m01_couplers_WREADY;
  wire [1:1]xbar_to_m01_couplers_WVALID;
  wire [95:64]xbar_to_m02_couplers_ARADDR;
  wire [0:0]xbar_to_m02_couplers_ARREADY;
  wire [2:2]xbar_to_m02_couplers_ARVALID;
  wire [95:64]xbar_to_m02_couplers_AWADDR;
  wire [0:0]xbar_to_m02_couplers_AWREADY;
  wire [2:2]xbar_to_m02_couplers_AWVALID;
  wire [2:2]xbar_to_m02_couplers_BREADY;
  wire [1:0]xbar_to_m02_couplers_BRESP;
  wire [0:0]xbar_to_m02_couplers_BVALID;
  wire [31:0]xbar_to_m02_couplers_RDATA;
  wire [2:2]xbar_to_m02_couplers_RREADY;
  wire [1:0]xbar_to_m02_couplers_RRESP;
  wire [0:0]xbar_to_m02_couplers_RVALID;
  wire [95:64]xbar_to_m02_couplers_WDATA;
  wire [0:0]xbar_to_m02_couplers_WREADY;
  wire [11:8]xbar_to_m02_couplers_WSTRB;
  wire [2:2]xbar_to_m02_couplers_WVALID;
  wire [127:96]xbar_to_m03_couplers_ARADDR;
  wire [11:9]xbar_to_m03_couplers_ARPROT;
  wire xbar_to_m03_couplers_ARREADY;
  wire [3:3]xbar_to_m03_couplers_ARVALID;
  wire [127:96]xbar_to_m03_couplers_AWADDR;
  wire [11:9]xbar_to_m03_couplers_AWPROT;
  wire xbar_to_m03_couplers_AWREADY;
  wire [3:3]xbar_to_m03_couplers_AWVALID;
  wire [3:3]xbar_to_m03_couplers_BREADY;
  wire [1:0]xbar_to_m03_couplers_BRESP;
  wire xbar_to_m03_couplers_BVALID;
  wire [31:0]xbar_to_m03_couplers_RDATA;
  wire [3:3]xbar_to_m03_couplers_RREADY;
  wire [1:0]xbar_to_m03_couplers_RRESP;
  wire xbar_to_m03_couplers_RVALID;
  wire [127:96]xbar_to_m03_couplers_WDATA;
  wire xbar_to_m03_couplers_WREADY;
  wire [15:12]xbar_to_m03_couplers_WSTRB;
  wire [3:3]xbar_to_m03_couplers_WVALID;
  wire [159:128]xbar_to_m04_couplers_ARADDR;
  wire [0:0]xbar_to_m04_couplers_ARREADY;
  wire [4:4]xbar_to_m04_couplers_ARVALID;
  wire [159:128]xbar_to_m04_couplers_AWADDR;
  wire [0:0]xbar_to_m04_couplers_AWREADY;
  wire [4:4]xbar_to_m04_couplers_AWVALID;
  wire [4:4]xbar_to_m04_couplers_BREADY;
  wire [1:0]xbar_to_m04_couplers_BRESP;
  wire [0:0]xbar_to_m04_couplers_BVALID;
  wire [31:0]xbar_to_m04_couplers_RDATA;
  wire [4:4]xbar_to_m04_couplers_RREADY;
  wire [1:0]xbar_to_m04_couplers_RRESP;
  wire [0:0]xbar_to_m04_couplers_RVALID;
  wire [159:128]xbar_to_m04_couplers_WDATA;
  wire [0:0]xbar_to_m04_couplers_WREADY;
  wire [19:16]xbar_to_m04_couplers_WSTRB;
  wire [4:4]xbar_to_m04_couplers_WVALID;
  wire [14:0]NLW_xbar_m_axi_arprot_UNCONNECTED;
  wire [14:0]NLW_xbar_m_axi_awprot_UNCONNECTED;
  wire [19:0]NLW_xbar_m_axi_wstrb_UNCONNECTED;

  assign M00_ACLK_1 = M00_ACLK;
  assign M00_ARESETN_1 = M00_ARESETN[0];
  assign M00_AXI_araddr[8:0] = m00_couplers_to_axi_interconnect_1_ARADDR;
  assign M00_AXI_arvalid[0] = m00_couplers_to_axi_interconnect_1_ARVALID;
  assign M00_AXI_awaddr[8:0] = m00_couplers_to_axi_interconnect_1_AWADDR;
  assign M00_AXI_awvalid[0] = m00_couplers_to_axi_interconnect_1_AWVALID;
  assign M00_AXI_bready[0] = m00_couplers_to_axi_interconnect_1_BREADY;
  assign M00_AXI_rready[0] = m00_couplers_to_axi_interconnect_1_RREADY;
  assign M00_AXI_wdata[31:0] = m00_couplers_to_axi_interconnect_1_WDATA;
  assign M00_AXI_wstrb[3:0] = m00_couplers_to_axi_interconnect_1_WSTRB;
  assign M00_AXI_wvalid[0] = m00_couplers_to_axi_interconnect_1_WVALID;
  assign M01_ACLK_1 = M01_ACLK;
  assign M01_ARESETN_1 = M01_ARESETN[0];
  assign M01_AXI_araddr[31:0] = m01_couplers_to_axi_interconnect_1_ARADDR;
  assign M01_AXI_arvalid[0] = m01_couplers_to_axi_interconnect_1_ARVALID;
  assign M01_AXI_awaddr[31:0] = m01_couplers_to_axi_interconnect_1_AWADDR;
  assign M01_AXI_awvalid[0] = m01_couplers_to_axi_interconnect_1_AWVALID;
  assign M01_AXI_bready[0] = m01_couplers_to_axi_interconnect_1_BREADY;
  assign M01_AXI_rready[0] = m01_couplers_to_axi_interconnect_1_RREADY;
  assign M01_AXI_wdata[31:0] = m01_couplers_to_axi_interconnect_1_WDATA;
  assign M01_AXI_wvalid[0] = m01_couplers_to_axi_interconnect_1_WVALID;
  assign M02_ACLK_1 = M02_ACLK;
  assign M02_ARESETN_1 = M02_ARESETN[0];
  assign M02_AXI_araddr[4:0] = m02_couplers_to_axi_interconnect_1_ARADDR;
  assign M02_AXI_arvalid[0] = m02_couplers_to_axi_interconnect_1_ARVALID;
  assign M02_AXI_awaddr[4:0] = m02_couplers_to_axi_interconnect_1_AWADDR;
  assign M02_AXI_awvalid[0] = m02_couplers_to_axi_interconnect_1_AWVALID;
  assign M02_AXI_bready[0] = m02_couplers_to_axi_interconnect_1_BREADY;
  assign M02_AXI_rready[0] = m02_couplers_to_axi_interconnect_1_RREADY;
  assign M02_AXI_wdata[31:0] = m02_couplers_to_axi_interconnect_1_WDATA;
  assign M02_AXI_wstrb[3:0] = m02_couplers_to_axi_interconnect_1_WSTRB;
  assign M02_AXI_wvalid[0] = m02_couplers_to_axi_interconnect_1_WVALID;
  assign M03_ACLK_1 = M03_ACLK;
  assign M03_ARESETN_1 = M03_ARESETN[0];
  assign M03_AXI_araddr[31:0] = m03_couplers_to_axi_interconnect_1_ARADDR;
  assign M03_AXI_arprot[2:0] = m03_couplers_to_axi_interconnect_1_ARPROT;
  assign M03_AXI_arvalid = m03_couplers_to_axi_interconnect_1_ARVALID;
  assign M03_AXI_awaddr[31:0] = m03_couplers_to_axi_interconnect_1_AWADDR;
  assign M03_AXI_awprot[2:0] = m03_couplers_to_axi_interconnect_1_AWPROT;
  assign M03_AXI_awvalid = m03_couplers_to_axi_interconnect_1_AWVALID;
  assign M03_AXI_bready = m03_couplers_to_axi_interconnect_1_BREADY;
  assign M03_AXI_rready = m03_couplers_to_axi_interconnect_1_RREADY;
  assign M03_AXI_wdata[31:0] = m03_couplers_to_axi_interconnect_1_WDATA;
  assign M03_AXI_wstrb[3:0] = m03_couplers_to_axi_interconnect_1_WSTRB;
  assign M03_AXI_wvalid = m03_couplers_to_axi_interconnect_1_WVALID;
  assign M04_ACLK_1 = M04_ACLK;
  assign M04_ARESETN_1 = M04_ARESETN[0];
  assign M04_AXI_araddr[8:0] = m04_couplers_to_axi_interconnect_1_ARADDR;
  assign M04_AXI_arvalid[0] = m04_couplers_to_axi_interconnect_1_ARVALID;
  assign M04_AXI_awaddr[8:0] = m04_couplers_to_axi_interconnect_1_AWADDR;
  assign M04_AXI_awvalid[0] = m04_couplers_to_axi_interconnect_1_AWVALID;
  assign M04_AXI_bready[0] = m04_couplers_to_axi_interconnect_1_BREADY;
  assign M04_AXI_rready[0] = m04_couplers_to_axi_interconnect_1_RREADY;
  assign M04_AXI_wdata[31:0] = m04_couplers_to_axi_interconnect_1_WDATA;
  assign M04_AXI_wstrb[3:0] = m04_couplers_to_axi_interconnect_1_WSTRB;
  assign M04_AXI_wvalid[0] = m04_couplers_to_axi_interconnect_1_WVALID;
  assign S00_ACLK_1 = S00_ACLK;
  assign S00_ARESETN_1 = S00_ARESETN[0];
  assign S00_AXI_arready = axi_interconnect_1_to_s00_couplers_ARREADY;
  assign S00_AXI_awready = axi_interconnect_1_to_s00_couplers_AWREADY;
  assign S00_AXI_bid[11:0] = axi_interconnect_1_to_s00_couplers_BID;
  assign S00_AXI_bresp[1:0] = axi_interconnect_1_to_s00_couplers_BRESP;
  assign S00_AXI_bvalid = axi_interconnect_1_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = axi_interconnect_1_to_s00_couplers_RDATA;
  assign S00_AXI_rid[11:0] = axi_interconnect_1_to_s00_couplers_RID;
  assign S00_AXI_rlast = axi_interconnect_1_to_s00_couplers_RLAST;
  assign S00_AXI_rresp[1:0] = axi_interconnect_1_to_s00_couplers_RRESP;
  assign S00_AXI_rvalid = axi_interconnect_1_to_s00_couplers_RVALID;
  assign S00_AXI_wready = axi_interconnect_1_to_s00_couplers_WREADY;
  assign axi_interconnect_1_ACLK_net = ACLK;
  assign axi_interconnect_1_ARESETN_net = ARESETN[0];
  assign axi_interconnect_1_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign axi_interconnect_1_to_s00_couplers_ARBURST = S00_AXI_arburst[1:0];
  assign axi_interconnect_1_to_s00_couplers_ARCACHE = S00_AXI_arcache[3:0];
  assign axi_interconnect_1_to_s00_couplers_ARID = S00_AXI_arid[11:0];
  assign axi_interconnect_1_to_s00_couplers_ARLEN = S00_AXI_arlen[3:0];
  assign axi_interconnect_1_to_s00_couplers_ARLOCK = S00_AXI_arlock[1:0];
  assign axi_interconnect_1_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign axi_interconnect_1_to_s00_couplers_ARQOS = S00_AXI_arqos[3:0];
  assign axi_interconnect_1_to_s00_couplers_ARSIZE = S00_AXI_arsize[2:0];
  assign axi_interconnect_1_to_s00_couplers_ARVALID = S00_AXI_arvalid;
  assign axi_interconnect_1_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign axi_interconnect_1_to_s00_couplers_AWBURST = S00_AXI_awburst[1:0];
  assign axi_interconnect_1_to_s00_couplers_AWCACHE = S00_AXI_awcache[3:0];
  assign axi_interconnect_1_to_s00_couplers_AWID = S00_AXI_awid[11:0];
  assign axi_interconnect_1_to_s00_couplers_AWLEN = S00_AXI_awlen[3:0];
  assign axi_interconnect_1_to_s00_couplers_AWLOCK = S00_AXI_awlock[1:0];
  assign axi_interconnect_1_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign axi_interconnect_1_to_s00_couplers_AWQOS = S00_AXI_awqos[3:0];
  assign axi_interconnect_1_to_s00_couplers_AWSIZE = S00_AXI_awsize[2:0];
  assign axi_interconnect_1_to_s00_couplers_AWVALID = S00_AXI_awvalid;
  assign axi_interconnect_1_to_s00_couplers_BREADY = S00_AXI_bready;
  assign axi_interconnect_1_to_s00_couplers_RREADY = S00_AXI_rready;
  assign axi_interconnect_1_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign axi_interconnect_1_to_s00_couplers_WID = S00_AXI_wid[11:0];
  assign axi_interconnect_1_to_s00_couplers_WLAST = S00_AXI_wlast;
  assign axi_interconnect_1_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign axi_interconnect_1_to_s00_couplers_WVALID = S00_AXI_wvalid;
  assign m00_couplers_to_axi_interconnect_1_ARREADY = M00_AXI_arready[0];
  assign m00_couplers_to_axi_interconnect_1_AWREADY = M00_AXI_awready[0];
  assign m00_couplers_to_axi_interconnect_1_BRESP = M00_AXI_bresp[1:0];
  assign m00_couplers_to_axi_interconnect_1_BVALID = M00_AXI_bvalid[0];
  assign m00_couplers_to_axi_interconnect_1_RDATA = M00_AXI_rdata[31:0];
  assign m00_couplers_to_axi_interconnect_1_RRESP = M00_AXI_rresp[1:0];
  assign m00_couplers_to_axi_interconnect_1_RVALID = M00_AXI_rvalid[0];
  assign m00_couplers_to_axi_interconnect_1_WREADY = M00_AXI_wready[0];
  assign m01_couplers_to_axi_interconnect_1_ARREADY = M01_AXI_arready[0];
  assign m01_couplers_to_axi_interconnect_1_AWREADY = M01_AXI_awready[0];
  assign m01_couplers_to_axi_interconnect_1_BRESP = M01_AXI_bresp[1:0];
  assign m01_couplers_to_axi_interconnect_1_BVALID = M01_AXI_bvalid[0];
  assign m01_couplers_to_axi_interconnect_1_RDATA = M01_AXI_rdata[31:0];
  assign m01_couplers_to_axi_interconnect_1_RRESP = M01_AXI_rresp[1:0];
  assign m01_couplers_to_axi_interconnect_1_RVALID = M01_AXI_rvalid[0];
  assign m01_couplers_to_axi_interconnect_1_WREADY = M01_AXI_wready[0];
  assign m02_couplers_to_axi_interconnect_1_ARREADY = M02_AXI_arready[0];
  assign m02_couplers_to_axi_interconnect_1_AWREADY = M02_AXI_awready[0];
  assign m02_couplers_to_axi_interconnect_1_BRESP = M02_AXI_bresp[1:0];
  assign m02_couplers_to_axi_interconnect_1_BVALID = M02_AXI_bvalid[0];
  assign m02_couplers_to_axi_interconnect_1_RDATA = M02_AXI_rdata[31:0];
  assign m02_couplers_to_axi_interconnect_1_RRESP = M02_AXI_rresp[1:0];
  assign m02_couplers_to_axi_interconnect_1_RVALID = M02_AXI_rvalid[0];
  assign m02_couplers_to_axi_interconnect_1_WREADY = M02_AXI_wready[0];
  assign m03_couplers_to_axi_interconnect_1_ARREADY = M03_AXI_arready;
  assign m03_couplers_to_axi_interconnect_1_AWREADY = M03_AXI_awready;
  assign m03_couplers_to_axi_interconnect_1_BRESP = M03_AXI_bresp[1:0];
  assign m03_couplers_to_axi_interconnect_1_BVALID = M03_AXI_bvalid;
  assign m03_couplers_to_axi_interconnect_1_RDATA = M03_AXI_rdata[31:0];
  assign m03_couplers_to_axi_interconnect_1_RRESP = M03_AXI_rresp[1:0];
  assign m03_couplers_to_axi_interconnect_1_RVALID = M03_AXI_rvalid;
  assign m03_couplers_to_axi_interconnect_1_WREADY = M03_AXI_wready;
  assign m04_couplers_to_axi_interconnect_1_ARREADY = M04_AXI_arready[0];
  assign m04_couplers_to_axi_interconnect_1_AWREADY = M04_AXI_awready[0];
  assign m04_couplers_to_axi_interconnect_1_BRESP = M04_AXI_bresp[1:0];
  assign m04_couplers_to_axi_interconnect_1_BVALID = M04_AXI_bvalid[0];
  assign m04_couplers_to_axi_interconnect_1_RDATA = M04_AXI_rdata[31:0];
  assign m04_couplers_to_axi_interconnect_1_RRESP = M04_AXI_rresp[1:0];
  assign m04_couplers_to_axi_interconnect_1_RVALID = M04_AXI_rvalid[0];
  assign m04_couplers_to_axi_interconnect_1_WREADY = M04_AXI_wready[0];
m00_couplers_imp_11EFUEQ m00_couplers
       (.M_ACLK(M00_ACLK_1),
        .M_ARESETN(M00_ARESETN_1),
        .M_AXI_araddr(m00_couplers_to_axi_interconnect_1_ARADDR),
        .M_AXI_arready(m00_couplers_to_axi_interconnect_1_ARREADY),
        .M_AXI_arvalid(m00_couplers_to_axi_interconnect_1_ARVALID),
        .M_AXI_awaddr(m00_couplers_to_axi_interconnect_1_AWADDR),
        .M_AXI_awready(m00_couplers_to_axi_interconnect_1_AWREADY),
        .M_AXI_awvalid(m00_couplers_to_axi_interconnect_1_AWVALID),
        .M_AXI_bready(m00_couplers_to_axi_interconnect_1_BREADY),
        .M_AXI_bresp(m00_couplers_to_axi_interconnect_1_BRESP),
        .M_AXI_bvalid(m00_couplers_to_axi_interconnect_1_BVALID),
        .M_AXI_rdata(m00_couplers_to_axi_interconnect_1_RDATA),
        .M_AXI_rready(m00_couplers_to_axi_interconnect_1_RREADY),
        .M_AXI_rresp(m00_couplers_to_axi_interconnect_1_RRESP),
        .M_AXI_rvalid(m00_couplers_to_axi_interconnect_1_RVALID),
        .M_AXI_wdata(m00_couplers_to_axi_interconnect_1_WDATA),
        .M_AXI_wready(m00_couplers_to_axi_interconnect_1_WREADY),
        .M_AXI_wstrb(m00_couplers_to_axi_interconnect_1_WSTRB),
        .M_AXI_wvalid(m00_couplers_to_axi_interconnect_1_WVALID),
        .S_ACLK(axi_interconnect_1_ACLK_net),
        .S_ARESETN(axi_interconnect_1_ARESETN_net),
        .S_AXI_araddr(xbar_to_m00_couplers_ARADDR[8:0]),
        .S_AXI_arready(xbar_to_m00_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m00_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m00_couplers_AWADDR[8:0]),
        .S_AXI_awready(xbar_to_m00_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m00_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m00_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m00_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m00_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m00_couplers_RDATA),
        .S_AXI_rready(xbar_to_m00_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m00_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m00_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m00_couplers_WDATA),
        .S_AXI_wready(xbar_to_m00_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m00_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m00_couplers_WVALID));
m01_couplers_imp_T56WEK m01_couplers
       (.M_ACLK(M01_ACLK_1),
        .M_ARESETN(M01_ARESETN_1),
        .M_AXI_araddr(m01_couplers_to_axi_interconnect_1_ARADDR),
        .M_AXI_arready(m01_couplers_to_axi_interconnect_1_ARREADY),
        .M_AXI_arvalid(m01_couplers_to_axi_interconnect_1_ARVALID),
        .M_AXI_awaddr(m01_couplers_to_axi_interconnect_1_AWADDR),
        .M_AXI_awready(m01_couplers_to_axi_interconnect_1_AWREADY),
        .M_AXI_awvalid(m01_couplers_to_axi_interconnect_1_AWVALID),
        .M_AXI_bready(m01_couplers_to_axi_interconnect_1_BREADY),
        .M_AXI_bresp(m01_couplers_to_axi_interconnect_1_BRESP),
        .M_AXI_bvalid(m01_couplers_to_axi_interconnect_1_BVALID),
        .M_AXI_rdata(m01_couplers_to_axi_interconnect_1_RDATA),
        .M_AXI_rready(m01_couplers_to_axi_interconnect_1_RREADY),
        .M_AXI_rresp(m01_couplers_to_axi_interconnect_1_RRESP),
        .M_AXI_rvalid(m01_couplers_to_axi_interconnect_1_RVALID),
        .M_AXI_wdata(m01_couplers_to_axi_interconnect_1_WDATA),
        .M_AXI_wready(m01_couplers_to_axi_interconnect_1_WREADY),
        .M_AXI_wvalid(m01_couplers_to_axi_interconnect_1_WVALID),
        .S_ACLK(axi_interconnect_1_ACLK_net),
        .S_ARESETN(axi_interconnect_1_ARESETN_net),
        .S_AXI_araddr(xbar_to_m01_couplers_ARADDR),
        .S_AXI_arready(xbar_to_m01_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m01_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m01_couplers_AWADDR),
        .S_AXI_awready(xbar_to_m01_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m01_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m01_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m01_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m01_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m01_couplers_RDATA),
        .S_AXI_rready(xbar_to_m01_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m01_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m01_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m01_couplers_WDATA),
        .S_AXI_wready(xbar_to_m01_couplers_WREADY),
        .S_AXI_wvalid(xbar_to_m01_couplers_WVALID));
m02_couplers_imp_104YG0F m02_couplers
       (.M_ACLK(M02_ACLK_1),
        .M_ARESETN(M02_ARESETN_1),
        .M_AXI_araddr(m02_couplers_to_axi_interconnect_1_ARADDR),
        .M_AXI_arready(m02_couplers_to_axi_interconnect_1_ARREADY),
        .M_AXI_arvalid(m02_couplers_to_axi_interconnect_1_ARVALID),
        .M_AXI_awaddr(m02_couplers_to_axi_interconnect_1_AWADDR),
        .M_AXI_awready(m02_couplers_to_axi_interconnect_1_AWREADY),
        .M_AXI_awvalid(m02_couplers_to_axi_interconnect_1_AWVALID),
        .M_AXI_bready(m02_couplers_to_axi_interconnect_1_BREADY),
        .M_AXI_bresp(m02_couplers_to_axi_interconnect_1_BRESP),
        .M_AXI_bvalid(m02_couplers_to_axi_interconnect_1_BVALID),
        .M_AXI_rdata(m02_couplers_to_axi_interconnect_1_RDATA),
        .M_AXI_rready(m02_couplers_to_axi_interconnect_1_RREADY),
        .M_AXI_rresp(m02_couplers_to_axi_interconnect_1_RRESP),
        .M_AXI_rvalid(m02_couplers_to_axi_interconnect_1_RVALID),
        .M_AXI_wdata(m02_couplers_to_axi_interconnect_1_WDATA),
        .M_AXI_wready(m02_couplers_to_axi_interconnect_1_WREADY),
        .M_AXI_wstrb(m02_couplers_to_axi_interconnect_1_WSTRB),
        .M_AXI_wvalid(m02_couplers_to_axi_interconnect_1_WVALID),
        .S_ACLK(axi_interconnect_1_ACLK_net),
        .S_ARESETN(axi_interconnect_1_ARESETN_net),
        .S_AXI_araddr(xbar_to_m02_couplers_ARADDR[68:64]),
        .S_AXI_arready(xbar_to_m02_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m02_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m02_couplers_AWADDR[68:64]),
        .S_AXI_awready(xbar_to_m02_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m02_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m02_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m02_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m02_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m02_couplers_RDATA),
        .S_AXI_rready(xbar_to_m02_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m02_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m02_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m02_couplers_WDATA),
        .S_AXI_wready(xbar_to_m02_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m02_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m02_couplers_WVALID));
m03_couplers_imp_UIMCHD m03_couplers
       (.M_ACLK(M03_ACLK_1),
        .M_ARESETN(M03_ARESETN_1),
        .M_AXI_araddr(m03_couplers_to_axi_interconnect_1_ARADDR),
        .M_AXI_arprot(m03_couplers_to_axi_interconnect_1_ARPROT),
        .M_AXI_arready(m03_couplers_to_axi_interconnect_1_ARREADY),
        .M_AXI_arvalid(m03_couplers_to_axi_interconnect_1_ARVALID),
        .M_AXI_awaddr(m03_couplers_to_axi_interconnect_1_AWADDR),
        .M_AXI_awprot(m03_couplers_to_axi_interconnect_1_AWPROT),
        .M_AXI_awready(m03_couplers_to_axi_interconnect_1_AWREADY),
        .M_AXI_awvalid(m03_couplers_to_axi_interconnect_1_AWVALID),
        .M_AXI_bready(m03_couplers_to_axi_interconnect_1_BREADY),
        .M_AXI_bresp(m03_couplers_to_axi_interconnect_1_BRESP),
        .M_AXI_bvalid(m03_couplers_to_axi_interconnect_1_BVALID),
        .M_AXI_rdata(m03_couplers_to_axi_interconnect_1_RDATA),
        .M_AXI_rready(m03_couplers_to_axi_interconnect_1_RREADY),
        .M_AXI_rresp(m03_couplers_to_axi_interconnect_1_RRESP),
        .M_AXI_rvalid(m03_couplers_to_axi_interconnect_1_RVALID),
        .M_AXI_wdata(m03_couplers_to_axi_interconnect_1_WDATA),
        .M_AXI_wready(m03_couplers_to_axi_interconnect_1_WREADY),
        .M_AXI_wstrb(m03_couplers_to_axi_interconnect_1_WSTRB),
        .M_AXI_wvalid(m03_couplers_to_axi_interconnect_1_WVALID),
        .S_ACLK(axi_interconnect_1_ACLK_net),
        .S_ARESETN(axi_interconnect_1_ARESETN_net),
        .S_AXI_araddr(xbar_to_m03_couplers_ARADDR),
        .S_AXI_arprot(xbar_to_m03_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m03_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m03_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m03_couplers_AWADDR),
        .S_AXI_awprot(xbar_to_m03_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m03_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m03_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m03_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m03_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m03_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m03_couplers_RDATA),
        .S_AXI_rready(xbar_to_m03_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m03_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m03_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m03_couplers_WDATA),
        .S_AXI_wready(xbar_to_m03_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m03_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m03_couplers_WVALID));
m04_couplers_imp_13PNGSO m04_couplers
       (.M_ACLK(M04_ACLK_1),
        .M_ARESETN(M04_ARESETN_1),
        .M_AXI_araddr(m04_couplers_to_axi_interconnect_1_ARADDR),
        .M_AXI_arready(m04_couplers_to_axi_interconnect_1_ARREADY),
        .M_AXI_arvalid(m04_couplers_to_axi_interconnect_1_ARVALID),
        .M_AXI_awaddr(m04_couplers_to_axi_interconnect_1_AWADDR),
        .M_AXI_awready(m04_couplers_to_axi_interconnect_1_AWREADY),
        .M_AXI_awvalid(m04_couplers_to_axi_interconnect_1_AWVALID),
        .M_AXI_bready(m04_couplers_to_axi_interconnect_1_BREADY),
        .M_AXI_bresp(m04_couplers_to_axi_interconnect_1_BRESP),
        .M_AXI_bvalid(m04_couplers_to_axi_interconnect_1_BVALID),
        .M_AXI_rdata(m04_couplers_to_axi_interconnect_1_RDATA),
        .M_AXI_rready(m04_couplers_to_axi_interconnect_1_RREADY),
        .M_AXI_rresp(m04_couplers_to_axi_interconnect_1_RRESP),
        .M_AXI_rvalid(m04_couplers_to_axi_interconnect_1_RVALID),
        .M_AXI_wdata(m04_couplers_to_axi_interconnect_1_WDATA),
        .M_AXI_wready(m04_couplers_to_axi_interconnect_1_WREADY),
        .M_AXI_wstrb(m04_couplers_to_axi_interconnect_1_WSTRB),
        .M_AXI_wvalid(m04_couplers_to_axi_interconnect_1_WVALID),
        .S_ACLK(axi_interconnect_1_ACLK_net),
        .S_ARESETN(axi_interconnect_1_ARESETN_net),
        .S_AXI_araddr(xbar_to_m04_couplers_ARADDR[136:128]),
        .S_AXI_arready(xbar_to_m04_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m04_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m04_couplers_AWADDR[136:128]),
        .S_AXI_awready(xbar_to_m04_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m04_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m04_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m04_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m04_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m04_couplers_RDATA),
        .S_AXI_rready(xbar_to_m04_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m04_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m04_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m04_couplers_WDATA),
        .S_AXI_wready(xbar_to_m04_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m04_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m04_couplers_WVALID));
s00_couplers_imp_RPQM34 s00_couplers
       (.M_ACLK(axi_interconnect_1_ACLK_net),
        .M_ARESETN(axi_interconnect_1_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_xbar_ARADDR),
        .M_AXI_arprot(s00_couplers_to_xbar_ARPROT),
        .M_AXI_arready(s00_couplers_to_xbar_ARREADY),
        .M_AXI_arvalid(s00_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_xbar_AWADDR),
        .M_AXI_awprot(s00_couplers_to_xbar_AWPROT),
        .M_AXI_awready(s00_couplers_to_xbar_AWREADY),
        .M_AXI_awvalid(s00_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s00_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s00_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s00_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s00_couplers_to_xbar_RDATA),
        .M_AXI_rready(s00_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s00_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s00_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s00_couplers_to_xbar_WDATA),
        .M_AXI_wready(s00_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s00_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s00_couplers_to_xbar_WVALID),
        .S_ACLK(S00_ACLK_1),
        .S_ARESETN(S00_ARESETN_1),
        .S_AXI_araddr(axi_interconnect_1_to_s00_couplers_ARADDR),
        .S_AXI_arburst(axi_interconnect_1_to_s00_couplers_ARBURST),
        .S_AXI_arcache(axi_interconnect_1_to_s00_couplers_ARCACHE),
        .S_AXI_arid(axi_interconnect_1_to_s00_couplers_ARID),
        .S_AXI_arlen(axi_interconnect_1_to_s00_couplers_ARLEN),
        .S_AXI_arlock(axi_interconnect_1_to_s00_couplers_ARLOCK),
        .S_AXI_arprot(axi_interconnect_1_to_s00_couplers_ARPROT),
        .S_AXI_arqos(axi_interconnect_1_to_s00_couplers_ARQOS),
        .S_AXI_arready(axi_interconnect_1_to_s00_couplers_ARREADY),
        .S_AXI_arsize(axi_interconnect_1_to_s00_couplers_ARSIZE),
        .S_AXI_arvalid(axi_interconnect_1_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(axi_interconnect_1_to_s00_couplers_AWADDR),
        .S_AXI_awburst(axi_interconnect_1_to_s00_couplers_AWBURST),
        .S_AXI_awcache(axi_interconnect_1_to_s00_couplers_AWCACHE),
        .S_AXI_awid(axi_interconnect_1_to_s00_couplers_AWID),
        .S_AXI_awlen(axi_interconnect_1_to_s00_couplers_AWLEN),
        .S_AXI_awlock(axi_interconnect_1_to_s00_couplers_AWLOCK),
        .S_AXI_awprot(axi_interconnect_1_to_s00_couplers_AWPROT),
        .S_AXI_awqos(axi_interconnect_1_to_s00_couplers_AWQOS),
        .S_AXI_awready(axi_interconnect_1_to_s00_couplers_AWREADY),
        .S_AXI_awsize(axi_interconnect_1_to_s00_couplers_AWSIZE),
        .S_AXI_awvalid(axi_interconnect_1_to_s00_couplers_AWVALID),
        .S_AXI_bid(axi_interconnect_1_to_s00_couplers_BID),
        .S_AXI_bready(axi_interconnect_1_to_s00_couplers_BREADY),
        .S_AXI_bresp(axi_interconnect_1_to_s00_couplers_BRESP),
        .S_AXI_bvalid(axi_interconnect_1_to_s00_couplers_BVALID),
        .S_AXI_rdata(axi_interconnect_1_to_s00_couplers_RDATA),
        .S_AXI_rid(axi_interconnect_1_to_s00_couplers_RID),
        .S_AXI_rlast(axi_interconnect_1_to_s00_couplers_RLAST),
        .S_AXI_rready(axi_interconnect_1_to_s00_couplers_RREADY),
        .S_AXI_rresp(axi_interconnect_1_to_s00_couplers_RRESP),
        .S_AXI_rvalid(axi_interconnect_1_to_s00_couplers_RVALID),
        .S_AXI_wdata(axi_interconnect_1_to_s00_couplers_WDATA),
        .S_AXI_wid(axi_interconnect_1_to_s00_couplers_WID),
        .S_AXI_wlast(axi_interconnect_1_to_s00_couplers_WLAST),
        .S_AXI_wready(axi_interconnect_1_to_s00_couplers_WREADY),
        .S_AXI_wstrb(axi_interconnect_1_to_s00_couplers_WSTRB),
        .S_AXI_wvalid(axi_interconnect_1_to_s00_couplers_WVALID));
dp_system_xbar_2 xbar
       (.aclk(axi_interconnect_1_ACLK_net),
        .aresetn(axi_interconnect_1_ARESETN_net),
        .m_axi_araddr({xbar_to_m04_couplers_ARADDR,xbar_to_m03_couplers_ARADDR,xbar_to_m02_couplers_ARADDR,xbar_to_m01_couplers_ARADDR,xbar_to_m00_couplers_ARADDR}),
        .m_axi_arprot({xbar_to_m03_couplers_ARPROT,NLW_xbar_m_axi_arprot_UNCONNECTED[8:0]}),
        .m_axi_arready({xbar_to_m04_couplers_ARREADY,xbar_to_m03_couplers_ARREADY,xbar_to_m02_couplers_ARREADY,xbar_to_m01_couplers_ARREADY,xbar_to_m00_couplers_ARREADY}),
        .m_axi_arvalid({xbar_to_m04_couplers_ARVALID,xbar_to_m03_couplers_ARVALID,xbar_to_m02_couplers_ARVALID,xbar_to_m01_couplers_ARVALID,xbar_to_m00_couplers_ARVALID}),
        .m_axi_awaddr({xbar_to_m04_couplers_AWADDR,xbar_to_m03_couplers_AWADDR,xbar_to_m02_couplers_AWADDR,xbar_to_m01_couplers_AWADDR,xbar_to_m00_couplers_AWADDR}),
        .m_axi_awprot({xbar_to_m03_couplers_AWPROT,NLW_xbar_m_axi_awprot_UNCONNECTED[8:0]}),
        .m_axi_awready({xbar_to_m04_couplers_AWREADY,xbar_to_m03_couplers_AWREADY,xbar_to_m02_couplers_AWREADY,xbar_to_m01_couplers_AWREADY,xbar_to_m00_couplers_AWREADY}),
        .m_axi_awvalid({xbar_to_m04_couplers_AWVALID,xbar_to_m03_couplers_AWVALID,xbar_to_m02_couplers_AWVALID,xbar_to_m01_couplers_AWVALID,xbar_to_m00_couplers_AWVALID}),
        .m_axi_bready({xbar_to_m04_couplers_BREADY,xbar_to_m03_couplers_BREADY,xbar_to_m02_couplers_BREADY,xbar_to_m01_couplers_BREADY,xbar_to_m00_couplers_BREADY}),
        .m_axi_bresp({xbar_to_m04_couplers_BRESP,xbar_to_m03_couplers_BRESP,xbar_to_m02_couplers_BRESP,xbar_to_m01_couplers_BRESP,xbar_to_m00_couplers_BRESP}),
        .m_axi_bvalid({xbar_to_m04_couplers_BVALID,xbar_to_m03_couplers_BVALID,xbar_to_m02_couplers_BVALID,xbar_to_m01_couplers_BVALID,xbar_to_m00_couplers_BVALID}),
        .m_axi_rdata({xbar_to_m04_couplers_RDATA,xbar_to_m03_couplers_RDATA,xbar_to_m02_couplers_RDATA,xbar_to_m01_couplers_RDATA,xbar_to_m00_couplers_RDATA}),
        .m_axi_rready({xbar_to_m04_couplers_RREADY,xbar_to_m03_couplers_RREADY,xbar_to_m02_couplers_RREADY,xbar_to_m01_couplers_RREADY,xbar_to_m00_couplers_RREADY}),
        .m_axi_rresp({xbar_to_m04_couplers_RRESP,xbar_to_m03_couplers_RRESP,xbar_to_m02_couplers_RRESP,xbar_to_m01_couplers_RRESP,xbar_to_m00_couplers_RRESP}),
        .m_axi_rvalid({xbar_to_m04_couplers_RVALID,xbar_to_m03_couplers_RVALID,xbar_to_m02_couplers_RVALID,xbar_to_m01_couplers_RVALID,xbar_to_m00_couplers_RVALID}),
        .m_axi_wdata({xbar_to_m04_couplers_WDATA,xbar_to_m03_couplers_WDATA,xbar_to_m02_couplers_WDATA,xbar_to_m01_couplers_WDATA,xbar_to_m00_couplers_WDATA}),
        .m_axi_wready({xbar_to_m04_couplers_WREADY,xbar_to_m03_couplers_WREADY,xbar_to_m02_couplers_WREADY,xbar_to_m01_couplers_WREADY,xbar_to_m00_couplers_WREADY}),
        .m_axi_wstrb({xbar_to_m04_couplers_WSTRB,xbar_to_m03_couplers_WSTRB,xbar_to_m02_couplers_WSTRB,NLW_xbar_m_axi_wstrb_UNCONNECTED[7:4],xbar_to_m00_couplers_WSTRB}),
        .m_axi_wvalid({xbar_to_m04_couplers_WVALID,xbar_to_m03_couplers_WVALID,xbar_to_m02_couplers_WVALID,xbar_to_m01_couplers_WVALID,xbar_to_m00_couplers_WVALID}),
        .s_axi_araddr(s00_couplers_to_xbar_ARADDR),
        .s_axi_arprot(s00_couplers_to_xbar_ARPROT),
        .s_axi_arready(s00_couplers_to_xbar_ARREADY),
        .s_axi_arvalid(s00_couplers_to_xbar_ARVALID),
        .s_axi_awaddr(s00_couplers_to_xbar_AWADDR),
        .s_axi_awprot(s00_couplers_to_xbar_AWPROT),
        .s_axi_awready(s00_couplers_to_xbar_AWREADY),
        .s_axi_awvalid(s00_couplers_to_xbar_AWVALID),
        .s_axi_bready(s00_couplers_to_xbar_BREADY),
        .s_axi_bresp(s00_couplers_to_xbar_BRESP),
        .s_axi_bvalid(s00_couplers_to_xbar_BVALID),
        .s_axi_rdata(s00_couplers_to_xbar_RDATA),
        .s_axi_rready(s00_couplers_to_xbar_RREADY),
        .s_axi_rresp(s00_couplers_to_xbar_RRESP),
        .s_axi_rvalid(s00_couplers_to_xbar_RVALID),
        .s_axi_wdata(s00_couplers_to_xbar_WDATA),
        .s_axi_wready(s00_couplers_to_xbar_WREADY),
        .s_axi_wstrb(s00_couplers_to_xbar_WSTRB),
        .s_axi_wvalid(s00_couplers_to_xbar_WVALID));
endmodule

module dp_system_processing_system7_0_axi_periph_0
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arburst,
    M00_AXI_arcache,
    M00_AXI_arid,
    M00_AXI_arlen,
    M00_AXI_arlock,
    M00_AXI_arprot,
    M00_AXI_arready,
    M00_AXI_arsize,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awburst,
    M00_AXI_awcache,
    M00_AXI_awid,
    M00_AXI_awlen,
    M00_AXI_awlock,
    M00_AXI_awprot,
    M00_AXI_awready,
    M00_AXI_awsize,
    M00_AXI_awvalid,
    M00_AXI_bid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rid,
    M00_AXI_rlast,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wlast,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arburst,
    S00_AXI_arcache,
    S00_AXI_arid,
    S00_AXI_arlen,
    S00_AXI_arlock,
    S00_AXI_arprot,
    S00_AXI_arqos,
    S00_AXI_arready,
    S00_AXI_arsize,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awburst,
    S00_AXI_awcache,
    S00_AXI_awid,
    S00_AXI_awlen,
    S00_AXI_awlock,
    S00_AXI_awprot,
    S00_AXI_awqos,
    S00_AXI_awready,
    S00_AXI_awsize,
    S00_AXI_awvalid,
    S00_AXI_bid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rid,
    S00_AXI_rlast,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wid,
    S00_AXI_wlast,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid);
  input ACLK;
  input [0:0]ARESETN;
  input M00_ACLK;
  input [0:0]M00_ARESETN;
  output [15:0]M00_AXI_araddr;
  output [1:0]M00_AXI_arburst;
  output [3:0]M00_AXI_arcache;
  output [11:0]M00_AXI_arid;
  output [7:0]M00_AXI_arlen;
  output [0:0]M00_AXI_arlock;
  output [2:0]M00_AXI_arprot;
  input M00_AXI_arready;
  output [2:0]M00_AXI_arsize;
  output M00_AXI_arvalid;
  output [15:0]M00_AXI_awaddr;
  output [1:0]M00_AXI_awburst;
  output [3:0]M00_AXI_awcache;
  output [11:0]M00_AXI_awid;
  output [7:0]M00_AXI_awlen;
  output [0:0]M00_AXI_awlock;
  output [2:0]M00_AXI_awprot;
  input M00_AXI_awready;
  output [2:0]M00_AXI_awsize;
  output M00_AXI_awvalid;
  input [11:0]M00_AXI_bid;
  output M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  input [11:0]M00_AXI_rid;
  input M00_AXI_rlast;
  output M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  output M00_AXI_wlast;
  input M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output M00_AXI_wvalid;
  input S00_ACLK;
  input [0:0]S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [1:0]S00_AXI_arburst;
  input [3:0]S00_AXI_arcache;
  input [11:0]S00_AXI_arid;
  input [3:0]S00_AXI_arlen;
  input [1:0]S00_AXI_arlock;
  input [2:0]S00_AXI_arprot;
  input [3:0]S00_AXI_arqos;
  output S00_AXI_arready;
  input [2:0]S00_AXI_arsize;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [1:0]S00_AXI_awburst;
  input [3:0]S00_AXI_awcache;
  input [11:0]S00_AXI_awid;
  input [3:0]S00_AXI_awlen;
  input [1:0]S00_AXI_awlock;
  input [2:0]S00_AXI_awprot;
  input [3:0]S00_AXI_awqos;
  output S00_AXI_awready;
  input [2:0]S00_AXI_awsize;
  input S00_AXI_awvalid;
  output [11:0]S00_AXI_bid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  output [11:0]S00_AXI_rid;
  output S00_AXI_rlast;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  input [11:0]S00_AXI_wid;
  input S00_AXI_wlast;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;

  wire S00_ACLK_1;
  wire [0:0]S00_ARESETN_1;
  wire processing_system7_0_axi_periph_ACLK_net;
  wire [0:0]processing_system7_0_axi_periph_ARESETN_net;
  wire [31:0]processing_system7_0_axi_periph_to_s00_couplers_ARADDR;
  wire [1:0]processing_system7_0_axi_periph_to_s00_couplers_ARBURST;
  wire [3:0]processing_system7_0_axi_periph_to_s00_couplers_ARCACHE;
  wire [11:0]processing_system7_0_axi_periph_to_s00_couplers_ARID;
  wire [3:0]processing_system7_0_axi_periph_to_s00_couplers_ARLEN;
  wire [1:0]processing_system7_0_axi_periph_to_s00_couplers_ARLOCK;
  wire [2:0]processing_system7_0_axi_periph_to_s00_couplers_ARPROT;
  wire [3:0]processing_system7_0_axi_periph_to_s00_couplers_ARQOS;
  wire processing_system7_0_axi_periph_to_s00_couplers_ARREADY;
  wire [2:0]processing_system7_0_axi_periph_to_s00_couplers_ARSIZE;
  wire processing_system7_0_axi_periph_to_s00_couplers_ARVALID;
  wire [31:0]processing_system7_0_axi_periph_to_s00_couplers_AWADDR;
  wire [1:0]processing_system7_0_axi_periph_to_s00_couplers_AWBURST;
  wire [3:0]processing_system7_0_axi_periph_to_s00_couplers_AWCACHE;
  wire [11:0]processing_system7_0_axi_periph_to_s00_couplers_AWID;
  wire [3:0]processing_system7_0_axi_periph_to_s00_couplers_AWLEN;
  wire [1:0]processing_system7_0_axi_periph_to_s00_couplers_AWLOCK;
  wire [2:0]processing_system7_0_axi_periph_to_s00_couplers_AWPROT;
  wire [3:0]processing_system7_0_axi_periph_to_s00_couplers_AWQOS;
  wire processing_system7_0_axi_periph_to_s00_couplers_AWREADY;
  wire [2:0]processing_system7_0_axi_periph_to_s00_couplers_AWSIZE;
  wire processing_system7_0_axi_periph_to_s00_couplers_AWVALID;
  wire [11:0]processing_system7_0_axi_periph_to_s00_couplers_BID;
  wire processing_system7_0_axi_periph_to_s00_couplers_BREADY;
  wire [1:0]processing_system7_0_axi_periph_to_s00_couplers_BRESP;
  wire processing_system7_0_axi_periph_to_s00_couplers_BVALID;
  wire [31:0]processing_system7_0_axi_periph_to_s00_couplers_RDATA;
  wire [11:0]processing_system7_0_axi_periph_to_s00_couplers_RID;
  wire processing_system7_0_axi_periph_to_s00_couplers_RLAST;
  wire processing_system7_0_axi_periph_to_s00_couplers_RREADY;
  wire [1:0]processing_system7_0_axi_periph_to_s00_couplers_RRESP;
  wire processing_system7_0_axi_periph_to_s00_couplers_RVALID;
  wire [31:0]processing_system7_0_axi_periph_to_s00_couplers_WDATA;
  wire [11:0]processing_system7_0_axi_periph_to_s00_couplers_WID;
  wire processing_system7_0_axi_periph_to_s00_couplers_WLAST;
  wire processing_system7_0_axi_periph_to_s00_couplers_WREADY;
  wire [3:0]processing_system7_0_axi_periph_to_s00_couplers_WSTRB;
  wire processing_system7_0_axi_periph_to_s00_couplers_WVALID;
  wire [15:0]s00_couplers_to_processing_system7_0_axi_periph_ARADDR;
  wire [1:0]s00_couplers_to_processing_system7_0_axi_periph_ARBURST;
  wire [3:0]s00_couplers_to_processing_system7_0_axi_periph_ARCACHE;
  wire [11:0]s00_couplers_to_processing_system7_0_axi_periph_ARID;
  wire [7:0]s00_couplers_to_processing_system7_0_axi_periph_ARLEN;
  wire [0:0]s00_couplers_to_processing_system7_0_axi_periph_ARLOCK;
  wire [2:0]s00_couplers_to_processing_system7_0_axi_periph_ARPROT;
  wire s00_couplers_to_processing_system7_0_axi_periph_ARREADY;
  wire [2:0]s00_couplers_to_processing_system7_0_axi_periph_ARSIZE;
  wire s00_couplers_to_processing_system7_0_axi_periph_ARVALID;
  wire [15:0]s00_couplers_to_processing_system7_0_axi_periph_AWADDR;
  wire [1:0]s00_couplers_to_processing_system7_0_axi_periph_AWBURST;
  wire [3:0]s00_couplers_to_processing_system7_0_axi_periph_AWCACHE;
  wire [11:0]s00_couplers_to_processing_system7_0_axi_periph_AWID;
  wire [7:0]s00_couplers_to_processing_system7_0_axi_periph_AWLEN;
  wire [0:0]s00_couplers_to_processing_system7_0_axi_periph_AWLOCK;
  wire [2:0]s00_couplers_to_processing_system7_0_axi_periph_AWPROT;
  wire s00_couplers_to_processing_system7_0_axi_periph_AWREADY;
  wire [2:0]s00_couplers_to_processing_system7_0_axi_periph_AWSIZE;
  wire s00_couplers_to_processing_system7_0_axi_periph_AWVALID;
  wire [11:0]s00_couplers_to_processing_system7_0_axi_periph_BID;
  wire s00_couplers_to_processing_system7_0_axi_periph_BREADY;
  wire [1:0]s00_couplers_to_processing_system7_0_axi_periph_BRESP;
  wire s00_couplers_to_processing_system7_0_axi_periph_BVALID;
  wire [31:0]s00_couplers_to_processing_system7_0_axi_periph_RDATA;
  wire [11:0]s00_couplers_to_processing_system7_0_axi_periph_RID;
  wire s00_couplers_to_processing_system7_0_axi_periph_RLAST;
  wire s00_couplers_to_processing_system7_0_axi_periph_RREADY;
  wire [1:0]s00_couplers_to_processing_system7_0_axi_periph_RRESP;
  wire s00_couplers_to_processing_system7_0_axi_periph_RVALID;
  wire [31:0]s00_couplers_to_processing_system7_0_axi_periph_WDATA;
  wire s00_couplers_to_processing_system7_0_axi_periph_WLAST;
  wire s00_couplers_to_processing_system7_0_axi_periph_WREADY;
  wire [3:0]s00_couplers_to_processing_system7_0_axi_periph_WSTRB;
  wire s00_couplers_to_processing_system7_0_axi_periph_WVALID;

  assign M00_AXI_araddr[15:0] = s00_couplers_to_processing_system7_0_axi_periph_ARADDR;
  assign M00_AXI_arburst[1:0] = s00_couplers_to_processing_system7_0_axi_periph_ARBURST;
  assign M00_AXI_arcache[3:0] = s00_couplers_to_processing_system7_0_axi_periph_ARCACHE;
  assign M00_AXI_arid[11:0] = s00_couplers_to_processing_system7_0_axi_periph_ARID;
  assign M00_AXI_arlen[7:0] = s00_couplers_to_processing_system7_0_axi_periph_ARLEN;
  assign M00_AXI_arlock[0] = s00_couplers_to_processing_system7_0_axi_periph_ARLOCK;
  assign M00_AXI_arprot[2:0] = s00_couplers_to_processing_system7_0_axi_periph_ARPROT;
  assign M00_AXI_arsize[2:0] = s00_couplers_to_processing_system7_0_axi_periph_ARSIZE;
  assign M00_AXI_arvalid = s00_couplers_to_processing_system7_0_axi_periph_ARVALID;
  assign M00_AXI_awaddr[15:0] = s00_couplers_to_processing_system7_0_axi_periph_AWADDR;
  assign M00_AXI_awburst[1:0] = s00_couplers_to_processing_system7_0_axi_periph_AWBURST;
  assign M00_AXI_awcache[3:0] = s00_couplers_to_processing_system7_0_axi_periph_AWCACHE;
  assign M00_AXI_awid[11:0] = s00_couplers_to_processing_system7_0_axi_periph_AWID;
  assign M00_AXI_awlen[7:0] = s00_couplers_to_processing_system7_0_axi_periph_AWLEN;
  assign M00_AXI_awlock[0] = s00_couplers_to_processing_system7_0_axi_periph_AWLOCK;
  assign M00_AXI_awprot[2:0] = s00_couplers_to_processing_system7_0_axi_periph_AWPROT;
  assign M00_AXI_awsize[2:0] = s00_couplers_to_processing_system7_0_axi_periph_AWSIZE;
  assign M00_AXI_awvalid = s00_couplers_to_processing_system7_0_axi_periph_AWVALID;
  assign M00_AXI_bready = s00_couplers_to_processing_system7_0_axi_periph_BREADY;
  assign M00_AXI_rready = s00_couplers_to_processing_system7_0_axi_periph_RREADY;
  assign M00_AXI_wdata[31:0] = s00_couplers_to_processing_system7_0_axi_periph_WDATA;
  assign M00_AXI_wlast = s00_couplers_to_processing_system7_0_axi_periph_WLAST;
  assign M00_AXI_wstrb[3:0] = s00_couplers_to_processing_system7_0_axi_periph_WSTRB;
  assign M00_AXI_wvalid = s00_couplers_to_processing_system7_0_axi_periph_WVALID;
  assign S00_ACLK_1 = S00_ACLK;
  assign S00_ARESETN_1 = S00_ARESETN[0];
  assign S00_AXI_arready = processing_system7_0_axi_periph_to_s00_couplers_ARREADY;
  assign S00_AXI_awready = processing_system7_0_axi_periph_to_s00_couplers_AWREADY;
  assign S00_AXI_bid[11:0] = processing_system7_0_axi_periph_to_s00_couplers_BID;
  assign S00_AXI_bresp[1:0] = processing_system7_0_axi_periph_to_s00_couplers_BRESP;
  assign S00_AXI_bvalid = processing_system7_0_axi_periph_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = processing_system7_0_axi_periph_to_s00_couplers_RDATA;
  assign S00_AXI_rid[11:0] = processing_system7_0_axi_periph_to_s00_couplers_RID;
  assign S00_AXI_rlast = processing_system7_0_axi_periph_to_s00_couplers_RLAST;
  assign S00_AXI_rresp[1:0] = processing_system7_0_axi_periph_to_s00_couplers_RRESP;
  assign S00_AXI_rvalid = processing_system7_0_axi_periph_to_s00_couplers_RVALID;
  assign S00_AXI_wready = processing_system7_0_axi_periph_to_s00_couplers_WREADY;
  assign processing_system7_0_axi_periph_ACLK_net = M00_ACLK;
  assign processing_system7_0_axi_periph_ARESETN_net = M00_ARESETN[0];
  assign processing_system7_0_axi_periph_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_ARBURST = S00_AXI_arburst[1:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_ARCACHE = S00_AXI_arcache[3:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_ARID = S00_AXI_arid[11:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_ARLEN = S00_AXI_arlen[3:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_ARLOCK = S00_AXI_arlock[1:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_ARQOS = S00_AXI_arqos[3:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_ARSIZE = S00_AXI_arsize[2:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_ARVALID = S00_AXI_arvalid;
  assign processing_system7_0_axi_periph_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_AWBURST = S00_AXI_awburst[1:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_AWCACHE = S00_AXI_awcache[3:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_AWID = S00_AXI_awid[11:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_AWLEN = S00_AXI_awlen[3:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_AWLOCK = S00_AXI_awlock[1:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_AWQOS = S00_AXI_awqos[3:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_AWSIZE = S00_AXI_awsize[2:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_AWVALID = S00_AXI_awvalid;
  assign processing_system7_0_axi_periph_to_s00_couplers_BREADY = S00_AXI_bready;
  assign processing_system7_0_axi_periph_to_s00_couplers_RREADY = S00_AXI_rready;
  assign processing_system7_0_axi_periph_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_WID = S00_AXI_wid[11:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_WLAST = S00_AXI_wlast;
  assign processing_system7_0_axi_periph_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign processing_system7_0_axi_periph_to_s00_couplers_WVALID = S00_AXI_wvalid;
  assign s00_couplers_to_processing_system7_0_axi_periph_ARREADY = M00_AXI_arready;
  assign s00_couplers_to_processing_system7_0_axi_periph_AWREADY = M00_AXI_awready;
  assign s00_couplers_to_processing_system7_0_axi_periph_BID = M00_AXI_bid[11:0];
  assign s00_couplers_to_processing_system7_0_axi_periph_BRESP = M00_AXI_bresp[1:0];
  assign s00_couplers_to_processing_system7_0_axi_periph_BVALID = M00_AXI_bvalid;
  assign s00_couplers_to_processing_system7_0_axi_periph_RDATA = M00_AXI_rdata[31:0];
  assign s00_couplers_to_processing_system7_0_axi_periph_RID = M00_AXI_rid[11:0];
  assign s00_couplers_to_processing_system7_0_axi_periph_RLAST = M00_AXI_rlast;
  assign s00_couplers_to_processing_system7_0_axi_periph_RRESP = M00_AXI_rresp[1:0];
  assign s00_couplers_to_processing_system7_0_axi_periph_RVALID = M00_AXI_rvalid;
  assign s00_couplers_to_processing_system7_0_axi_periph_WREADY = M00_AXI_wready;
s00_couplers_imp_1ZC0NP s00_couplers
       (.M_ACLK(processing_system7_0_axi_periph_ACLK_net),
        .M_ARESETN(processing_system7_0_axi_periph_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_processing_system7_0_axi_periph_ARADDR),
        .M_AXI_arburst(s00_couplers_to_processing_system7_0_axi_periph_ARBURST),
        .M_AXI_arcache(s00_couplers_to_processing_system7_0_axi_periph_ARCACHE),
        .M_AXI_arid(s00_couplers_to_processing_system7_0_axi_periph_ARID),
        .M_AXI_arlen(s00_couplers_to_processing_system7_0_axi_periph_ARLEN),
        .M_AXI_arlock(s00_couplers_to_processing_system7_0_axi_periph_ARLOCK),
        .M_AXI_arprot(s00_couplers_to_processing_system7_0_axi_periph_ARPROT),
        .M_AXI_arready(s00_couplers_to_processing_system7_0_axi_periph_ARREADY),
        .M_AXI_arsize(s00_couplers_to_processing_system7_0_axi_periph_ARSIZE),
        .M_AXI_arvalid(s00_couplers_to_processing_system7_0_axi_periph_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_processing_system7_0_axi_periph_AWADDR),
        .M_AXI_awburst(s00_couplers_to_processing_system7_0_axi_periph_AWBURST),
        .M_AXI_awcache(s00_couplers_to_processing_system7_0_axi_periph_AWCACHE),
        .M_AXI_awid(s00_couplers_to_processing_system7_0_axi_periph_AWID),
        .M_AXI_awlen(s00_couplers_to_processing_system7_0_axi_periph_AWLEN),
        .M_AXI_awlock(s00_couplers_to_processing_system7_0_axi_periph_AWLOCK),
        .M_AXI_awprot(s00_couplers_to_processing_system7_0_axi_periph_AWPROT),
        .M_AXI_awready(s00_couplers_to_processing_system7_0_axi_periph_AWREADY),
        .M_AXI_awsize(s00_couplers_to_processing_system7_0_axi_periph_AWSIZE),
        .M_AXI_awvalid(s00_couplers_to_processing_system7_0_axi_periph_AWVALID),
        .M_AXI_bid(s00_couplers_to_processing_system7_0_axi_periph_BID),
        .M_AXI_bready(s00_couplers_to_processing_system7_0_axi_periph_BREADY),
        .M_AXI_bresp(s00_couplers_to_processing_system7_0_axi_periph_BRESP),
        .M_AXI_bvalid(s00_couplers_to_processing_system7_0_axi_periph_BVALID),
        .M_AXI_rdata(s00_couplers_to_processing_system7_0_axi_periph_RDATA),
        .M_AXI_rid(s00_couplers_to_processing_system7_0_axi_periph_RID),
        .M_AXI_rlast(s00_couplers_to_processing_system7_0_axi_periph_RLAST),
        .M_AXI_rready(s00_couplers_to_processing_system7_0_axi_periph_RREADY),
        .M_AXI_rresp(s00_couplers_to_processing_system7_0_axi_periph_RRESP),
        .M_AXI_rvalid(s00_couplers_to_processing_system7_0_axi_periph_RVALID),
        .M_AXI_wdata(s00_couplers_to_processing_system7_0_axi_periph_WDATA),
        .M_AXI_wlast(s00_couplers_to_processing_system7_0_axi_periph_WLAST),
        .M_AXI_wready(s00_couplers_to_processing_system7_0_axi_periph_WREADY),
        .M_AXI_wstrb(s00_couplers_to_processing_system7_0_axi_periph_WSTRB),
        .M_AXI_wvalid(s00_couplers_to_processing_system7_0_axi_periph_WVALID),
        .S_ACLK(S00_ACLK_1),
        .S_ARESETN(S00_ARESETN_1),
        .S_AXI_araddr(processing_system7_0_axi_periph_to_s00_couplers_ARADDR),
        .S_AXI_arburst(processing_system7_0_axi_periph_to_s00_couplers_ARBURST),
        .S_AXI_arcache(processing_system7_0_axi_periph_to_s00_couplers_ARCACHE),
        .S_AXI_arid(processing_system7_0_axi_periph_to_s00_couplers_ARID),
        .S_AXI_arlen(processing_system7_0_axi_periph_to_s00_couplers_ARLEN),
        .S_AXI_arlock(processing_system7_0_axi_periph_to_s00_couplers_ARLOCK),
        .S_AXI_arprot(processing_system7_0_axi_periph_to_s00_couplers_ARPROT),
        .S_AXI_arqos(processing_system7_0_axi_periph_to_s00_couplers_ARQOS),
        .S_AXI_arready(processing_system7_0_axi_periph_to_s00_couplers_ARREADY),
        .S_AXI_arsize(processing_system7_0_axi_periph_to_s00_couplers_ARSIZE),
        .S_AXI_arvalid(processing_system7_0_axi_periph_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(processing_system7_0_axi_periph_to_s00_couplers_AWADDR),
        .S_AXI_awburst(processing_system7_0_axi_periph_to_s00_couplers_AWBURST),
        .S_AXI_awcache(processing_system7_0_axi_periph_to_s00_couplers_AWCACHE),
        .S_AXI_awid(processing_system7_0_axi_periph_to_s00_couplers_AWID),
        .S_AXI_awlen(processing_system7_0_axi_periph_to_s00_couplers_AWLEN),
        .S_AXI_awlock(processing_system7_0_axi_periph_to_s00_couplers_AWLOCK),
        .S_AXI_awprot(processing_system7_0_axi_periph_to_s00_couplers_AWPROT),
        .S_AXI_awqos(processing_system7_0_axi_periph_to_s00_couplers_AWQOS),
        .S_AXI_awready(processing_system7_0_axi_periph_to_s00_couplers_AWREADY),
        .S_AXI_awsize(processing_system7_0_axi_periph_to_s00_couplers_AWSIZE),
        .S_AXI_awvalid(processing_system7_0_axi_periph_to_s00_couplers_AWVALID),
        .S_AXI_bid(processing_system7_0_axi_periph_to_s00_couplers_BID),
        .S_AXI_bready(processing_system7_0_axi_periph_to_s00_couplers_BREADY),
        .S_AXI_bresp(processing_system7_0_axi_periph_to_s00_couplers_BRESP),
        .S_AXI_bvalid(processing_system7_0_axi_periph_to_s00_couplers_BVALID),
        .S_AXI_rdata(processing_system7_0_axi_periph_to_s00_couplers_RDATA),
        .S_AXI_rid(processing_system7_0_axi_periph_to_s00_couplers_RID),
        .S_AXI_rlast(processing_system7_0_axi_periph_to_s00_couplers_RLAST),
        .S_AXI_rready(processing_system7_0_axi_periph_to_s00_couplers_RREADY),
        .S_AXI_rresp(processing_system7_0_axi_periph_to_s00_couplers_RRESP),
        .S_AXI_rvalid(processing_system7_0_axi_periph_to_s00_couplers_RVALID),
        .S_AXI_wdata(processing_system7_0_axi_periph_to_s00_couplers_WDATA),
        .S_AXI_wid(processing_system7_0_axi_periph_to_s00_couplers_WID),
        .S_AXI_wlast(processing_system7_0_axi_periph_to_s00_couplers_WLAST),
        .S_AXI_wready(processing_system7_0_axi_periph_to_s00_couplers_WREADY),
        .S_AXI_wstrb(processing_system7_0_axi_periph_to_s00_couplers_WSTRB),
        .S_AXI_wvalid(processing_system7_0_axi_periph_to_s00_couplers_WVALID));
endmodule

module m00_couplers_imp_11EFUEQ
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [8:0]M_AXI_araddr;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [8:0]M_AXI_awaddr;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [8:0]S_AXI_araddr;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [8:0]S_AXI_awaddr;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [8:0]m00_couplers_to_m00_couplers_ARADDR;
  wire [0:0]m00_couplers_to_m00_couplers_ARREADY;
  wire [0:0]m00_couplers_to_m00_couplers_ARVALID;
  wire [8:0]m00_couplers_to_m00_couplers_AWADDR;
  wire [0:0]m00_couplers_to_m00_couplers_AWREADY;
  wire [0:0]m00_couplers_to_m00_couplers_AWVALID;
  wire [0:0]m00_couplers_to_m00_couplers_BREADY;
  wire [1:0]m00_couplers_to_m00_couplers_BRESP;
  wire [0:0]m00_couplers_to_m00_couplers_BVALID;
  wire [31:0]m00_couplers_to_m00_couplers_RDATA;
  wire [0:0]m00_couplers_to_m00_couplers_RREADY;
  wire [1:0]m00_couplers_to_m00_couplers_RRESP;
  wire [0:0]m00_couplers_to_m00_couplers_RVALID;
  wire [31:0]m00_couplers_to_m00_couplers_WDATA;
  wire [0:0]m00_couplers_to_m00_couplers_WREADY;
  wire [3:0]m00_couplers_to_m00_couplers_WSTRB;
  wire [0:0]m00_couplers_to_m00_couplers_WVALID;

  assign M_AXI_araddr[8:0] = m00_couplers_to_m00_couplers_ARADDR;
  assign M_AXI_arvalid[0] = m00_couplers_to_m00_couplers_ARVALID;
  assign M_AXI_awaddr[8:0] = m00_couplers_to_m00_couplers_AWADDR;
  assign M_AXI_awvalid[0] = m00_couplers_to_m00_couplers_AWVALID;
  assign M_AXI_bready[0] = m00_couplers_to_m00_couplers_BREADY;
  assign M_AXI_rready[0] = m00_couplers_to_m00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m00_couplers_to_m00_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m00_couplers_to_m00_couplers_WSTRB;
  assign M_AXI_wvalid[0] = m00_couplers_to_m00_couplers_WVALID;
  assign S_AXI_arready[0] = m00_couplers_to_m00_couplers_ARREADY;
  assign S_AXI_awready[0] = m00_couplers_to_m00_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m00_couplers_to_m00_couplers_BRESP;
  assign S_AXI_bvalid[0] = m00_couplers_to_m00_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m00_couplers_to_m00_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m00_couplers_to_m00_couplers_RRESP;
  assign S_AXI_rvalid[0] = m00_couplers_to_m00_couplers_RVALID;
  assign S_AXI_wready[0] = m00_couplers_to_m00_couplers_WREADY;
  assign m00_couplers_to_m00_couplers_ARADDR = S_AXI_araddr[8:0];
  assign m00_couplers_to_m00_couplers_ARREADY = M_AXI_arready[0];
  assign m00_couplers_to_m00_couplers_ARVALID = S_AXI_arvalid[0];
  assign m00_couplers_to_m00_couplers_AWADDR = S_AXI_awaddr[8:0];
  assign m00_couplers_to_m00_couplers_AWREADY = M_AXI_awready[0];
  assign m00_couplers_to_m00_couplers_AWVALID = S_AXI_awvalid[0];
  assign m00_couplers_to_m00_couplers_BREADY = S_AXI_bready[0];
  assign m00_couplers_to_m00_couplers_BRESP = M_AXI_bresp[1:0];
  assign m00_couplers_to_m00_couplers_BVALID = M_AXI_bvalid[0];
  assign m00_couplers_to_m00_couplers_RDATA = M_AXI_rdata[31:0];
  assign m00_couplers_to_m00_couplers_RREADY = S_AXI_rready[0];
  assign m00_couplers_to_m00_couplers_RRESP = M_AXI_rresp[1:0];
  assign m00_couplers_to_m00_couplers_RVALID = M_AXI_rvalid[0];
  assign m00_couplers_to_m00_couplers_WDATA = S_AXI_wdata[31:0];
  assign m00_couplers_to_m00_couplers_WREADY = M_AXI_wready[0];
  assign m00_couplers_to_m00_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m00_couplers_to_m00_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module m01_couplers_imp_T56WEK
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [0:0]S_AXI_wvalid;

  wire [31:0]m01_couplers_to_m01_couplers_ARADDR;
  wire [0:0]m01_couplers_to_m01_couplers_ARREADY;
  wire [0:0]m01_couplers_to_m01_couplers_ARVALID;
  wire [31:0]m01_couplers_to_m01_couplers_AWADDR;
  wire [0:0]m01_couplers_to_m01_couplers_AWREADY;
  wire [0:0]m01_couplers_to_m01_couplers_AWVALID;
  wire [0:0]m01_couplers_to_m01_couplers_BREADY;
  wire [1:0]m01_couplers_to_m01_couplers_BRESP;
  wire [0:0]m01_couplers_to_m01_couplers_BVALID;
  wire [31:0]m01_couplers_to_m01_couplers_RDATA;
  wire [0:0]m01_couplers_to_m01_couplers_RREADY;
  wire [1:0]m01_couplers_to_m01_couplers_RRESP;
  wire [0:0]m01_couplers_to_m01_couplers_RVALID;
  wire [31:0]m01_couplers_to_m01_couplers_WDATA;
  wire [0:0]m01_couplers_to_m01_couplers_WREADY;
  wire [0:0]m01_couplers_to_m01_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m01_couplers_to_m01_couplers_ARADDR;
  assign M_AXI_arvalid[0] = m01_couplers_to_m01_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m01_couplers_to_m01_couplers_AWADDR;
  assign M_AXI_awvalid[0] = m01_couplers_to_m01_couplers_AWVALID;
  assign M_AXI_bready[0] = m01_couplers_to_m01_couplers_BREADY;
  assign M_AXI_rready[0] = m01_couplers_to_m01_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m01_couplers_to_m01_couplers_WDATA;
  assign M_AXI_wvalid[0] = m01_couplers_to_m01_couplers_WVALID;
  assign S_AXI_arready[0] = m01_couplers_to_m01_couplers_ARREADY;
  assign S_AXI_awready[0] = m01_couplers_to_m01_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m01_couplers_to_m01_couplers_BRESP;
  assign S_AXI_bvalid[0] = m01_couplers_to_m01_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m01_couplers_to_m01_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m01_couplers_to_m01_couplers_RRESP;
  assign S_AXI_rvalid[0] = m01_couplers_to_m01_couplers_RVALID;
  assign S_AXI_wready[0] = m01_couplers_to_m01_couplers_WREADY;
  assign m01_couplers_to_m01_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m01_couplers_to_m01_couplers_ARREADY = M_AXI_arready[0];
  assign m01_couplers_to_m01_couplers_ARVALID = S_AXI_arvalid[0];
  assign m01_couplers_to_m01_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m01_couplers_to_m01_couplers_AWREADY = M_AXI_awready[0];
  assign m01_couplers_to_m01_couplers_AWVALID = S_AXI_awvalid[0];
  assign m01_couplers_to_m01_couplers_BREADY = S_AXI_bready[0];
  assign m01_couplers_to_m01_couplers_BRESP = M_AXI_bresp[1:0];
  assign m01_couplers_to_m01_couplers_BVALID = M_AXI_bvalid[0];
  assign m01_couplers_to_m01_couplers_RDATA = M_AXI_rdata[31:0];
  assign m01_couplers_to_m01_couplers_RREADY = S_AXI_rready[0];
  assign m01_couplers_to_m01_couplers_RRESP = M_AXI_rresp[1:0];
  assign m01_couplers_to_m01_couplers_RVALID = M_AXI_rvalid[0];
  assign m01_couplers_to_m01_couplers_WDATA = S_AXI_wdata[31:0];
  assign m01_couplers_to_m01_couplers_WREADY = M_AXI_wready[0];
  assign m01_couplers_to_m01_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module m02_couplers_imp_104YG0F
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [4:0]M_AXI_araddr;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [4:0]M_AXI_awaddr;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [4:0]S_AXI_araddr;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [4:0]S_AXI_awaddr;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [4:0]m02_couplers_to_m02_couplers_ARADDR;
  wire [0:0]m02_couplers_to_m02_couplers_ARREADY;
  wire [0:0]m02_couplers_to_m02_couplers_ARVALID;
  wire [4:0]m02_couplers_to_m02_couplers_AWADDR;
  wire [0:0]m02_couplers_to_m02_couplers_AWREADY;
  wire [0:0]m02_couplers_to_m02_couplers_AWVALID;
  wire [0:0]m02_couplers_to_m02_couplers_BREADY;
  wire [1:0]m02_couplers_to_m02_couplers_BRESP;
  wire [0:0]m02_couplers_to_m02_couplers_BVALID;
  wire [31:0]m02_couplers_to_m02_couplers_RDATA;
  wire [0:0]m02_couplers_to_m02_couplers_RREADY;
  wire [1:0]m02_couplers_to_m02_couplers_RRESP;
  wire [0:0]m02_couplers_to_m02_couplers_RVALID;
  wire [31:0]m02_couplers_to_m02_couplers_WDATA;
  wire [0:0]m02_couplers_to_m02_couplers_WREADY;
  wire [3:0]m02_couplers_to_m02_couplers_WSTRB;
  wire [0:0]m02_couplers_to_m02_couplers_WVALID;

  assign M_AXI_araddr[4:0] = m02_couplers_to_m02_couplers_ARADDR;
  assign M_AXI_arvalid[0] = m02_couplers_to_m02_couplers_ARVALID;
  assign M_AXI_awaddr[4:0] = m02_couplers_to_m02_couplers_AWADDR;
  assign M_AXI_awvalid[0] = m02_couplers_to_m02_couplers_AWVALID;
  assign M_AXI_bready[0] = m02_couplers_to_m02_couplers_BREADY;
  assign M_AXI_rready[0] = m02_couplers_to_m02_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m02_couplers_to_m02_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m02_couplers_to_m02_couplers_WSTRB;
  assign M_AXI_wvalid[0] = m02_couplers_to_m02_couplers_WVALID;
  assign S_AXI_arready[0] = m02_couplers_to_m02_couplers_ARREADY;
  assign S_AXI_awready[0] = m02_couplers_to_m02_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m02_couplers_to_m02_couplers_BRESP;
  assign S_AXI_bvalid[0] = m02_couplers_to_m02_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m02_couplers_to_m02_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m02_couplers_to_m02_couplers_RRESP;
  assign S_AXI_rvalid[0] = m02_couplers_to_m02_couplers_RVALID;
  assign S_AXI_wready[0] = m02_couplers_to_m02_couplers_WREADY;
  assign m02_couplers_to_m02_couplers_ARADDR = S_AXI_araddr[4:0];
  assign m02_couplers_to_m02_couplers_ARREADY = M_AXI_arready[0];
  assign m02_couplers_to_m02_couplers_ARVALID = S_AXI_arvalid[0];
  assign m02_couplers_to_m02_couplers_AWADDR = S_AXI_awaddr[4:0];
  assign m02_couplers_to_m02_couplers_AWREADY = M_AXI_awready[0];
  assign m02_couplers_to_m02_couplers_AWVALID = S_AXI_awvalid[0];
  assign m02_couplers_to_m02_couplers_BREADY = S_AXI_bready[0];
  assign m02_couplers_to_m02_couplers_BRESP = M_AXI_bresp[1:0];
  assign m02_couplers_to_m02_couplers_BVALID = M_AXI_bvalid[0];
  assign m02_couplers_to_m02_couplers_RDATA = M_AXI_rdata[31:0];
  assign m02_couplers_to_m02_couplers_RREADY = S_AXI_rready[0];
  assign m02_couplers_to_m02_couplers_RRESP = M_AXI_rresp[1:0];
  assign m02_couplers_to_m02_couplers_RVALID = M_AXI_rvalid[0];
  assign m02_couplers_to_m02_couplers_WDATA = S_AXI_wdata[31:0];
  assign m02_couplers_to_m02_couplers_WREADY = M_AXI_wready[0];
  assign m02_couplers_to_m02_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m02_couplers_to_m02_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module m03_couplers_imp_UIMCHD
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire [31:0]m03_couplers_to_m03_couplers_ARADDR;
  wire [2:0]m03_couplers_to_m03_couplers_ARPROT;
  wire m03_couplers_to_m03_couplers_ARREADY;
  wire m03_couplers_to_m03_couplers_ARVALID;
  wire [31:0]m03_couplers_to_m03_couplers_AWADDR;
  wire [2:0]m03_couplers_to_m03_couplers_AWPROT;
  wire m03_couplers_to_m03_couplers_AWREADY;
  wire m03_couplers_to_m03_couplers_AWVALID;
  wire m03_couplers_to_m03_couplers_BREADY;
  wire [1:0]m03_couplers_to_m03_couplers_BRESP;
  wire m03_couplers_to_m03_couplers_BVALID;
  wire [31:0]m03_couplers_to_m03_couplers_RDATA;
  wire m03_couplers_to_m03_couplers_RREADY;
  wire [1:0]m03_couplers_to_m03_couplers_RRESP;
  wire m03_couplers_to_m03_couplers_RVALID;
  wire [31:0]m03_couplers_to_m03_couplers_WDATA;
  wire m03_couplers_to_m03_couplers_WREADY;
  wire [3:0]m03_couplers_to_m03_couplers_WSTRB;
  wire m03_couplers_to_m03_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m03_couplers_to_m03_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = m03_couplers_to_m03_couplers_ARPROT;
  assign M_AXI_arvalid = m03_couplers_to_m03_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m03_couplers_to_m03_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = m03_couplers_to_m03_couplers_AWPROT;
  assign M_AXI_awvalid = m03_couplers_to_m03_couplers_AWVALID;
  assign M_AXI_bready = m03_couplers_to_m03_couplers_BREADY;
  assign M_AXI_rready = m03_couplers_to_m03_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m03_couplers_to_m03_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m03_couplers_to_m03_couplers_WSTRB;
  assign M_AXI_wvalid = m03_couplers_to_m03_couplers_WVALID;
  assign S_AXI_arready = m03_couplers_to_m03_couplers_ARREADY;
  assign S_AXI_awready = m03_couplers_to_m03_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m03_couplers_to_m03_couplers_BRESP;
  assign S_AXI_bvalid = m03_couplers_to_m03_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m03_couplers_to_m03_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m03_couplers_to_m03_couplers_RRESP;
  assign S_AXI_rvalid = m03_couplers_to_m03_couplers_RVALID;
  assign S_AXI_wready = m03_couplers_to_m03_couplers_WREADY;
  assign m03_couplers_to_m03_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m03_couplers_to_m03_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m03_couplers_to_m03_couplers_ARREADY = M_AXI_arready;
  assign m03_couplers_to_m03_couplers_ARVALID = S_AXI_arvalid;
  assign m03_couplers_to_m03_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m03_couplers_to_m03_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m03_couplers_to_m03_couplers_AWREADY = M_AXI_awready;
  assign m03_couplers_to_m03_couplers_AWVALID = S_AXI_awvalid;
  assign m03_couplers_to_m03_couplers_BREADY = S_AXI_bready;
  assign m03_couplers_to_m03_couplers_BRESP = M_AXI_bresp[1:0];
  assign m03_couplers_to_m03_couplers_BVALID = M_AXI_bvalid;
  assign m03_couplers_to_m03_couplers_RDATA = M_AXI_rdata[31:0];
  assign m03_couplers_to_m03_couplers_RREADY = S_AXI_rready;
  assign m03_couplers_to_m03_couplers_RRESP = M_AXI_rresp[1:0];
  assign m03_couplers_to_m03_couplers_RVALID = M_AXI_rvalid;
  assign m03_couplers_to_m03_couplers_WDATA = S_AXI_wdata[31:0];
  assign m03_couplers_to_m03_couplers_WREADY = M_AXI_wready;
  assign m03_couplers_to_m03_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m03_couplers_to_m03_couplers_WVALID = S_AXI_wvalid;
endmodule

module m04_couplers_imp_13PNGSO
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [8:0]M_AXI_araddr;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [8:0]M_AXI_awaddr;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [8:0]S_AXI_araddr;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [8:0]S_AXI_awaddr;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [8:0]m04_couplers_to_m04_couplers_ARADDR;
  wire [0:0]m04_couplers_to_m04_couplers_ARREADY;
  wire [0:0]m04_couplers_to_m04_couplers_ARVALID;
  wire [8:0]m04_couplers_to_m04_couplers_AWADDR;
  wire [0:0]m04_couplers_to_m04_couplers_AWREADY;
  wire [0:0]m04_couplers_to_m04_couplers_AWVALID;
  wire [0:0]m04_couplers_to_m04_couplers_BREADY;
  wire [1:0]m04_couplers_to_m04_couplers_BRESP;
  wire [0:0]m04_couplers_to_m04_couplers_BVALID;
  wire [31:0]m04_couplers_to_m04_couplers_RDATA;
  wire [0:0]m04_couplers_to_m04_couplers_RREADY;
  wire [1:0]m04_couplers_to_m04_couplers_RRESP;
  wire [0:0]m04_couplers_to_m04_couplers_RVALID;
  wire [31:0]m04_couplers_to_m04_couplers_WDATA;
  wire [0:0]m04_couplers_to_m04_couplers_WREADY;
  wire [3:0]m04_couplers_to_m04_couplers_WSTRB;
  wire [0:0]m04_couplers_to_m04_couplers_WVALID;

  assign M_AXI_araddr[8:0] = m04_couplers_to_m04_couplers_ARADDR;
  assign M_AXI_arvalid[0] = m04_couplers_to_m04_couplers_ARVALID;
  assign M_AXI_awaddr[8:0] = m04_couplers_to_m04_couplers_AWADDR;
  assign M_AXI_awvalid[0] = m04_couplers_to_m04_couplers_AWVALID;
  assign M_AXI_bready[0] = m04_couplers_to_m04_couplers_BREADY;
  assign M_AXI_rready[0] = m04_couplers_to_m04_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m04_couplers_to_m04_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = m04_couplers_to_m04_couplers_WSTRB;
  assign M_AXI_wvalid[0] = m04_couplers_to_m04_couplers_WVALID;
  assign S_AXI_arready[0] = m04_couplers_to_m04_couplers_ARREADY;
  assign S_AXI_awready[0] = m04_couplers_to_m04_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m04_couplers_to_m04_couplers_BRESP;
  assign S_AXI_bvalid[0] = m04_couplers_to_m04_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m04_couplers_to_m04_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m04_couplers_to_m04_couplers_RRESP;
  assign S_AXI_rvalid[0] = m04_couplers_to_m04_couplers_RVALID;
  assign S_AXI_wready[0] = m04_couplers_to_m04_couplers_WREADY;
  assign m04_couplers_to_m04_couplers_ARADDR = S_AXI_araddr[8:0];
  assign m04_couplers_to_m04_couplers_ARREADY = M_AXI_arready[0];
  assign m04_couplers_to_m04_couplers_ARVALID = S_AXI_arvalid[0];
  assign m04_couplers_to_m04_couplers_AWADDR = S_AXI_awaddr[8:0];
  assign m04_couplers_to_m04_couplers_AWREADY = M_AXI_awready[0];
  assign m04_couplers_to_m04_couplers_AWVALID = S_AXI_awvalid[0];
  assign m04_couplers_to_m04_couplers_BREADY = S_AXI_bready[0];
  assign m04_couplers_to_m04_couplers_BRESP = M_AXI_bresp[1:0];
  assign m04_couplers_to_m04_couplers_BVALID = M_AXI_bvalid[0];
  assign m04_couplers_to_m04_couplers_RDATA = M_AXI_rdata[31:0];
  assign m04_couplers_to_m04_couplers_RREADY = S_AXI_rready[0];
  assign m04_couplers_to_m04_couplers_RRESP = M_AXI_rresp[1:0];
  assign m04_couplers_to_m04_couplers_RVALID = M_AXI_rvalid[0];
  assign m04_couplers_to_m04_couplers_WDATA = S_AXI_wdata[31:0];
  assign m04_couplers_to_m04_couplers_WREADY = M_AXI_wready[0];
  assign m04_couplers_to_m04_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m04_couplers_to_m04_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module s00_couplers_imp_1ZC0NP
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arid,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awid,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rid,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wid,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [15:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [11:0]M_AXI_arid;
  output [7:0]M_AXI_arlen;
  output [0:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  output [15:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [11:0]M_AXI_awid;
  output [7:0]M_AXI_awlen;
  output [0:0]M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  input [11:0]M_AXI_bid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  input [11:0]M_AXI_rid;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [11:0]S_AXI_arid;
  input [3:0]S_AXI_arlen;
  input [1:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [11:0]S_AXI_awid;
  input [3:0]S_AXI_awlen;
  input [1:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [11:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [11:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input [11:0]S_AXI_wid;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_pc_to_s00_couplers_ARADDR;
  wire [1:0]auto_pc_to_s00_couplers_ARBURST;
  wire [3:0]auto_pc_to_s00_couplers_ARCACHE;
  wire [11:0]auto_pc_to_s00_couplers_ARID;
  wire [7:0]auto_pc_to_s00_couplers_ARLEN;
  wire [0:0]auto_pc_to_s00_couplers_ARLOCK;
  wire [2:0]auto_pc_to_s00_couplers_ARPROT;
  wire auto_pc_to_s00_couplers_ARREADY;
  wire [2:0]auto_pc_to_s00_couplers_ARSIZE;
  wire auto_pc_to_s00_couplers_ARVALID;
  wire [31:0]auto_pc_to_s00_couplers_AWADDR;
  wire [1:0]auto_pc_to_s00_couplers_AWBURST;
  wire [3:0]auto_pc_to_s00_couplers_AWCACHE;
  wire [11:0]auto_pc_to_s00_couplers_AWID;
  wire [7:0]auto_pc_to_s00_couplers_AWLEN;
  wire [0:0]auto_pc_to_s00_couplers_AWLOCK;
  wire [2:0]auto_pc_to_s00_couplers_AWPROT;
  wire auto_pc_to_s00_couplers_AWREADY;
  wire [2:0]auto_pc_to_s00_couplers_AWSIZE;
  wire auto_pc_to_s00_couplers_AWVALID;
  wire [11:0]auto_pc_to_s00_couplers_BID;
  wire auto_pc_to_s00_couplers_BREADY;
  wire [1:0]auto_pc_to_s00_couplers_BRESP;
  wire auto_pc_to_s00_couplers_BVALID;
  wire [31:0]auto_pc_to_s00_couplers_RDATA;
  wire [11:0]auto_pc_to_s00_couplers_RID;
  wire auto_pc_to_s00_couplers_RLAST;
  wire auto_pc_to_s00_couplers_RREADY;
  wire [1:0]auto_pc_to_s00_couplers_RRESP;
  wire auto_pc_to_s00_couplers_RVALID;
  wire [31:0]auto_pc_to_s00_couplers_WDATA;
  wire auto_pc_to_s00_couplers_WLAST;
  wire auto_pc_to_s00_couplers_WREADY;
  wire [3:0]auto_pc_to_s00_couplers_WSTRB;
  wire auto_pc_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_auto_pc_ARADDR;
  wire [1:0]s00_couplers_to_auto_pc_ARBURST;
  wire [3:0]s00_couplers_to_auto_pc_ARCACHE;
  wire [11:0]s00_couplers_to_auto_pc_ARID;
  wire [3:0]s00_couplers_to_auto_pc_ARLEN;
  wire [1:0]s00_couplers_to_auto_pc_ARLOCK;
  wire [2:0]s00_couplers_to_auto_pc_ARPROT;
  wire [3:0]s00_couplers_to_auto_pc_ARQOS;
  wire s00_couplers_to_auto_pc_ARREADY;
  wire [2:0]s00_couplers_to_auto_pc_ARSIZE;
  wire s00_couplers_to_auto_pc_ARVALID;
  wire [31:0]s00_couplers_to_auto_pc_AWADDR;
  wire [1:0]s00_couplers_to_auto_pc_AWBURST;
  wire [3:0]s00_couplers_to_auto_pc_AWCACHE;
  wire [11:0]s00_couplers_to_auto_pc_AWID;
  wire [3:0]s00_couplers_to_auto_pc_AWLEN;
  wire [1:0]s00_couplers_to_auto_pc_AWLOCK;
  wire [2:0]s00_couplers_to_auto_pc_AWPROT;
  wire [3:0]s00_couplers_to_auto_pc_AWQOS;
  wire s00_couplers_to_auto_pc_AWREADY;
  wire [2:0]s00_couplers_to_auto_pc_AWSIZE;
  wire s00_couplers_to_auto_pc_AWVALID;
  wire [11:0]s00_couplers_to_auto_pc_BID;
  wire s00_couplers_to_auto_pc_BREADY;
  wire [1:0]s00_couplers_to_auto_pc_BRESP;
  wire s00_couplers_to_auto_pc_BVALID;
  wire [31:0]s00_couplers_to_auto_pc_RDATA;
  wire [11:0]s00_couplers_to_auto_pc_RID;
  wire s00_couplers_to_auto_pc_RLAST;
  wire s00_couplers_to_auto_pc_RREADY;
  wire [1:0]s00_couplers_to_auto_pc_RRESP;
  wire s00_couplers_to_auto_pc_RVALID;
  wire [31:0]s00_couplers_to_auto_pc_WDATA;
  wire [11:0]s00_couplers_to_auto_pc_WID;
  wire s00_couplers_to_auto_pc_WLAST;
  wire s00_couplers_to_auto_pc_WREADY;
  wire [3:0]s00_couplers_to_auto_pc_WSTRB;
  wire s00_couplers_to_auto_pc_WVALID;

  assign M_AXI_araddr[15:0] = auto_pc_to_s00_couplers_ARADDR[15:0];
  assign M_AXI_arburst[1:0] = auto_pc_to_s00_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_pc_to_s00_couplers_ARCACHE;
  assign M_AXI_arid[11:0] = auto_pc_to_s00_couplers_ARID;
  assign M_AXI_arlen[7:0] = auto_pc_to_s00_couplers_ARLEN;
  assign M_AXI_arlock[0] = auto_pc_to_s00_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_pc_to_s00_couplers_ARPROT;
  assign M_AXI_arsize[2:0] = auto_pc_to_s00_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_pc_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[15:0] = auto_pc_to_s00_couplers_AWADDR[15:0];
  assign M_AXI_awburst[1:0] = auto_pc_to_s00_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_pc_to_s00_couplers_AWCACHE;
  assign M_AXI_awid[11:0] = auto_pc_to_s00_couplers_AWID;
  assign M_AXI_awlen[7:0] = auto_pc_to_s00_couplers_AWLEN;
  assign M_AXI_awlock[0] = auto_pc_to_s00_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_pc_to_s00_couplers_AWPROT;
  assign M_AXI_awsize[2:0] = auto_pc_to_s00_couplers_AWSIZE;
  assign M_AXI_awvalid = auto_pc_to_s00_couplers_AWVALID;
  assign M_AXI_bready = auto_pc_to_s00_couplers_BREADY;
  assign M_AXI_rready = auto_pc_to_s00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_pc_to_s00_couplers_WDATA;
  assign M_AXI_wlast = auto_pc_to_s00_couplers_WLAST;
  assign M_AXI_wstrb[3:0] = auto_pc_to_s00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_pc_to_s00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_arready = s00_couplers_to_auto_pc_ARREADY;
  assign S_AXI_awready = s00_couplers_to_auto_pc_AWREADY;
  assign S_AXI_bid[11:0] = s00_couplers_to_auto_pc_BID;
  assign S_AXI_bresp[1:0] = s00_couplers_to_auto_pc_BRESP;
  assign S_AXI_bvalid = s00_couplers_to_auto_pc_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_auto_pc_RDATA;
  assign S_AXI_rid[11:0] = s00_couplers_to_auto_pc_RID;
  assign S_AXI_rlast = s00_couplers_to_auto_pc_RLAST;
  assign S_AXI_rresp[1:0] = s00_couplers_to_auto_pc_RRESP;
  assign S_AXI_rvalid = s00_couplers_to_auto_pc_RVALID;
  assign S_AXI_wready = s00_couplers_to_auto_pc_WREADY;
  assign auto_pc_to_s00_couplers_ARREADY = M_AXI_arready;
  assign auto_pc_to_s00_couplers_AWREADY = M_AXI_awready;
  assign auto_pc_to_s00_couplers_BID = M_AXI_bid[11:0];
  assign auto_pc_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_pc_to_s00_couplers_BVALID = M_AXI_bvalid;
  assign auto_pc_to_s00_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_pc_to_s00_couplers_RID = M_AXI_rid[11:0];
  assign auto_pc_to_s00_couplers_RLAST = M_AXI_rlast;
  assign auto_pc_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_pc_to_s00_couplers_RVALID = M_AXI_rvalid;
  assign auto_pc_to_s00_couplers_WREADY = M_AXI_wready;
  assign s00_couplers_to_auto_pc_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_auto_pc_ARBURST = S_AXI_arburst[1:0];
  assign s00_couplers_to_auto_pc_ARCACHE = S_AXI_arcache[3:0];
  assign s00_couplers_to_auto_pc_ARID = S_AXI_arid[11:0];
  assign s00_couplers_to_auto_pc_ARLEN = S_AXI_arlen[3:0];
  assign s00_couplers_to_auto_pc_ARLOCK = S_AXI_arlock[1:0];
  assign s00_couplers_to_auto_pc_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_auto_pc_ARQOS = S_AXI_arqos[3:0];
  assign s00_couplers_to_auto_pc_ARSIZE = S_AXI_arsize[2:0];
  assign s00_couplers_to_auto_pc_ARVALID = S_AXI_arvalid;
  assign s00_couplers_to_auto_pc_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_auto_pc_AWBURST = S_AXI_awburst[1:0];
  assign s00_couplers_to_auto_pc_AWCACHE = S_AXI_awcache[3:0];
  assign s00_couplers_to_auto_pc_AWID = S_AXI_awid[11:0];
  assign s00_couplers_to_auto_pc_AWLEN = S_AXI_awlen[3:0];
  assign s00_couplers_to_auto_pc_AWLOCK = S_AXI_awlock[1:0];
  assign s00_couplers_to_auto_pc_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_auto_pc_AWQOS = S_AXI_awqos[3:0];
  assign s00_couplers_to_auto_pc_AWSIZE = S_AXI_awsize[2:0];
  assign s00_couplers_to_auto_pc_AWVALID = S_AXI_awvalid;
  assign s00_couplers_to_auto_pc_BREADY = S_AXI_bready;
  assign s00_couplers_to_auto_pc_RREADY = S_AXI_rready;
  assign s00_couplers_to_auto_pc_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_auto_pc_WID = S_AXI_wid[11:0];
  assign s00_couplers_to_auto_pc_WLAST = S_AXI_wlast;
  assign s00_couplers_to_auto_pc_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_auto_pc_WVALID = S_AXI_wvalid;
dp_system_auto_pc_1 auto_pc
       (.aclk(S_ACLK_1),
        .aresetn(S_ARESETN_1),
        .m_axi_araddr(auto_pc_to_s00_couplers_ARADDR),
        .m_axi_arburst(auto_pc_to_s00_couplers_ARBURST),
        .m_axi_arcache(auto_pc_to_s00_couplers_ARCACHE),
        .m_axi_arid(auto_pc_to_s00_couplers_ARID),
        .m_axi_arlen(auto_pc_to_s00_couplers_ARLEN),
        .m_axi_arlock(auto_pc_to_s00_couplers_ARLOCK),
        .m_axi_arprot(auto_pc_to_s00_couplers_ARPROT),
        .m_axi_arready(auto_pc_to_s00_couplers_ARREADY),
        .m_axi_arsize(auto_pc_to_s00_couplers_ARSIZE),
        .m_axi_arvalid(auto_pc_to_s00_couplers_ARVALID),
        .m_axi_awaddr(auto_pc_to_s00_couplers_AWADDR),
        .m_axi_awburst(auto_pc_to_s00_couplers_AWBURST),
        .m_axi_awcache(auto_pc_to_s00_couplers_AWCACHE),
        .m_axi_awid(auto_pc_to_s00_couplers_AWID),
        .m_axi_awlen(auto_pc_to_s00_couplers_AWLEN),
        .m_axi_awlock(auto_pc_to_s00_couplers_AWLOCK),
        .m_axi_awprot(auto_pc_to_s00_couplers_AWPROT),
        .m_axi_awready(auto_pc_to_s00_couplers_AWREADY),
        .m_axi_awsize(auto_pc_to_s00_couplers_AWSIZE),
        .m_axi_awvalid(auto_pc_to_s00_couplers_AWVALID),
        .m_axi_bid(auto_pc_to_s00_couplers_BID),
        .m_axi_bready(auto_pc_to_s00_couplers_BREADY),
        .m_axi_bresp(auto_pc_to_s00_couplers_BRESP),
        .m_axi_bvalid(auto_pc_to_s00_couplers_BVALID),
        .m_axi_rdata(auto_pc_to_s00_couplers_RDATA),
        .m_axi_rid(auto_pc_to_s00_couplers_RID),
        .m_axi_rlast(auto_pc_to_s00_couplers_RLAST),
        .m_axi_rready(auto_pc_to_s00_couplers_RREADY),
        .m_axi_rresp(auto_pc_to_s00_couplers_RRESP),
        .m_axi_rvalid(auto_pc_to_s00_couplers_RVALID),
        .m_axi_wdata(auto_pc_to_s00_couplers_WDATA),
        .m_axi_wlast(auto_pc_to_s00_couplers_WLAST),
        .m_axi_wready(auto_pc_to_s00_couplers_WREADY),
        .m_axi_wstrb(auto_pc_to_s00_couplers_WSTRB),
        .m_axi_wvalid(auto_pc_to_s00_couplers_WVALID),
        .s_axi_araddr(s00_couplers_to_auto_pc_ARADDR),
        .s_axi_arburst(s00_couplers_to_auto_pc_ARBURST),
        .s_axi_arcache(s00_couplers_to_auto_pc_ARCACHE),
        .s_axi_arid(s00_couplers_to_auto_pc_ARID),
        .s_axi_arlen(s00_couplers_to_auto_pc_ARLEN),
        .s_axi_arlock(s00_couplers_to_auto_pc_ARLOCK),
        .s_axi_arprot(s00_couplers_to_auto_pc_ARPROT),
        .s_axi_arqos(s00_couplers_to_auto_pc_ARQOS),
        .s_axi_arready(s00_couplers_to_auto_pc_ARREADY),
        .s_axi_arsize(s00_couplers_to_auto_pc_ARSIZE),
        .s_axi_arvalid(s00_couplers_to_auto_pc_ARVALID),
        .s_axi_awaddr(s00_couplers_to_auto_pc_AWADDR),
        .s_axi_awburst(s00_couplers_to_auto_pc_AWBURST),
        .s_axi_awcache(s00_couplers_to_auto_pc_AWCACHE),
        .s_axi_awid(s00_couplers_to_auto_pc_AWID),
        .s_axi_awlen(s00_couplers_to_auto_pc_AWLEN),
        .s_axi_awlock(s00_couplers_to_auto_pc_AWLOCK),
        .s_axi_awprot(s00_couplers_to_auto_pc_AWPROT),
        .s_axi_awqos(s00_couplers_to_auto_pc_AWQOS),
        .s_axi_awready(s00_couplers_to_auto_pc_AWREADY),
        .s_axi_awsize(s00_couplers_to_auto_pc_AWSIZE),
        .s_axi_awvalid(s00_couplers_to_auto_pc_AWVALID),
        .s_axi_bid(s00_couplers_to_auto_pc_BID),
        .s_axi_bready(s00_couplers_to_auto_pc_BREADY),
        .s_axi_bresp(s00_couplers_to_auto_pc_BRESP),
        .s_axi_bvalid(s00_couplers_to_auto_pc_BVALID),
        .s_axi_rdata(s00_couplers_to_auto_pc_RDATA),
        .s_axi_rid(s00_couplers_to_auto_pc_RID),
        .s_axi_rlast(s00_couplers_to_auto_pc_RLAST),
        .s_axi_rready(s00_couplers_to_auto_pc_RREADY),
        .s_axi_rresp(s00_couplers_to_auto_pc_RRESP),
        .s_axi_rvalid(s00_couplers_to_auto_pc_RVALID),
        .s_axi_wdata(s00_couplers_to_auto_pc_WDATA),
        .s_axi_wid(s00_couplers_to_auto_pc_WID),
        .s_axi_wlast(s00_couplers_to_auto_pc_WLAST),
        .s_axi_wready(s00_couplers_to_auto_pc_WREADY),
        .s_axi_wstrb(s00_couplers_to_auto_pc_WSTRB),
        .s_axi_wvalid(s00_couplers_to_auto_pc_WVALID));
endmodule

module s00_couplers_imp_RPQM34
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wid,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [11:0]S_AXI_arid;
  input [3:0]S_AXI_arlen;
  input [1:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [11:0]S_AXI_awid;
  input [3:0]S_AXI_awlen;
  input [1:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [11:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [11:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input [11:0]S_AXI_wid;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_pc_to_s00_couplers_ARADDR;
  wire [2:0]auto_pc_to_s00_couplers_ARPROT;
  wire auto_pc_to_s00_couplers_ARREADY;
  wire auto_pc_to_s00_couplers_ARVALID;
  wire [31:0]auto_pc_to_s00_couplers_AWADDR;
  wire [2:0]auto_pc_to_s00_couplers_AWPROT;
  wire auto_pc_to_s00_couplers_AWREADY;
  wire auto_pc_to_s00_couplers_AWVALID;
  wire auto_pc_to_s00_couplers_BREADY;
  wire [1:0]auto_pc_to_s00_couplers_BRESP;
  wire auto_pc_to_s00_couplers_BVALID;
  wire [31:0]auto_pc_to_s00_couplers_RDATA;
  wire auto_pc_to_s00_couplers_RREADY;
  wire [1:0]auto_pc_to_s00_couplers_RRESP;
  wire auto_pc_to_s00_couplers_RVALID;
  wire [31:0]auto_pc_to_s00_couplers_WDATA;
  wire auto_pc_to_s00_couplers_WREADY;
  wire [3:0]auto_pc_to_s00_couplers_WSTRB;
  wire auto_pc_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_auto_pc_ARADDR;
  wire [1:0]s00_couplers_to_auto_pc_ARBURST;
  wire [3:0]s00_couplers_to_auto_pc_ARCACHE;
  wire [11:0]s00_couplers_to_auto_pc_ARID;
  wire [3:0]s00_couplers_to_auto_pc_ARLEN;
  wire [1:0]s00_couplers_to_auto_pc_ARLOCK;
  wire [2:0]s00_couplers_to_auto_pc_ARPROT;
  wire [3:0]s00_couplers_to_auto_pc_ARQOS;
  wire s00_couplers_to_auto_pc_ARREADY;
  wire [2:0]s00_couplers_to_auto_pc_ARSIZE;
  wire s00_couplers_to_auto_pc_ARVALID;
  wire [31:0]s00_couplers_to_auto_pc_AWADDR;
  wire [1:0]s00_couplers_to_auto_pc_AWBURST;
  wire [3:0]s00_couplers_to_auto_pc_AWCACHE;
  wire [11:0]s00_couplers_to_auto_pc_AWID;
  wire [3:0]s00_couplers_to_auto_pc_AWLEN;
  wire [1:0]s00_couplers_to_auto_pc_AWLOCK;
  wire [2:0]s00_couplers_to_auto_pc_AWPROT;
  wire [3:0]s00_couplers_to_auto_pc_AWQOS;
  wire s00_couplers_to_auto_pc_AWREADY;
  wire [2:0]s00_couplers_to_auto_pc_AWSIZE;
  wire s00_couplers_to_auto_pc_AWVALID;
  wire [11:0]s00_couplers_to_auto_pc_BID;
  wire s00_couplers_to_auto_pc_BREADY;
  wire [1:0]s00_couplers_to_auto_pc_BRESP;
  wire s00_couplers_to_auto_pc_BVALID;
  wire [31:0]s00_couplers_to_auto_pc_RDATA;
  wire [11:0]s00_couplers_to_auto_pc_RID;
  wire s00_couplers_to_auto_pc_RLAST;
  wire s00_couplers_to_auto_pc_RREADY;
  wire [1:0]s00_couplers_to_auto_pc_RRESP;
  wire s00_couplers_to_auto_pc_RVALID;
  wire [31:0]s00_couplers_to_auto_pc_WDATA;
  wire [11:0]s00_couplers_to_auto_pc_WID;
  wire s00_couplers_to_auto_pc_WLAST;
  wire s00_couplers_to_auto_pc_WREADY;
  wire [3:0]s00_couplers_to_auto_pc_WSTRB;
  wire s00_couplers_to_auto_pc_WVALID;

  assign M_AXI_araddr[31:0] = auto_pc_to_s00_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = auto_pc_to_s00_couplers_ARPROT;
  assign M_AXI_arvalid = auto_pc_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_pc_to_s00_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = auto_pc_to_s00_couplers_AWPROT;
  assign M_AXI_awvalid = auto_pc_to_s00_couplers_AWVALID;
  assign M_AXI_bready = auto_pc_to_s00_couplers_BREADY;
  assign M_AXI_rready = auto_pc_to_s00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_pc_to_s00_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = auto_pc_to_s00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_pc_to_s00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_arready = s00_couplers_to_auto_pc_ARREADY;
  assign S_AXI_awready = s00_couplers_to_auto_pc_AWREADY;
  assign S_AXI_bid[11:0] = s00_couplers_to_auto_pc_BID;
  assign S_AXI_bresp[1:0] = s00_couplers_to_auto_pc_BRESP;
  assign S_AXI_bvalid = s00_couplers_to_auto_pc_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_auto_pc_RDATA;
  assign S_AXI_rid[11:0] = s00_couplers_to_auto_pc_RID;
  assign S_AXI_rlast = s00_couplers_to_auto_pc_RLAST;
  assign S_AXI_rresp[1:0] = s00_couplers_to_auto_pc_RRESP;
  assign S_AXI_rvalid = s00_couplers_to_auto_pc_RVALID;
  assign S_AXI_wready = s00_couplers_to_auto_pc_WREADY;
  assign auto_pc_to_s00_couplers_ARREADY = M_AXI_arready;
  assign auto_pc_to_s00_couplers_AWREADY = M_AXI_awready;
  assign auto_pc_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_pc_to_s00_couplers_BVALID = M_AXI_bvalid;
  assign auto_pc_to_s00_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_pc_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_pc_to_s00_couplers_RVALID = M_AXI_rvalid;
  assign auto_pc_to_s00_couplers_WREADY = M_AXI_wready;
  assign s00_couplers_to_auto_pc_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_auto_pc_ARBURST = S_AXI_arburst[1:0];
  assign s00_couplers_to_auto_pc_ARCACHE = S_AXI_arcache[3:0];
  assign s00_couplers_to_auto_pc_ARID = S_AXI_arid[11:0];
  assign s00_couplers_to_auto_pc_ARLEN = S_AXI_arlen[3:0];
  assign s00_couplers_to_auto_pc_ARLOCK = S_AXI_arlock[1:0];
  assign s00_couplers_to_auto_pc_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_auto_pc_ARQOS = S_AXI_arqos[3:0];
  assign s00_couplers_to_auto_pc_ARSIZE = S_AXI_arsize[2:0];
  assign s00_couplers_to_auto_pc_ARVALID = S_AXI_arvalid;
  assign s00_couplers_to_auto_pc_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_auto_pc_AWBURST = S_AXI_awburst[1:0];
  assign s00_couplers_to_auto_pc_AWCACHE = S_AXI_awcache[3:0];
  assign s00_couplers_to_auto_pc_AWID = S_AXI_awid[11:0];
  assign s00_couplers_to_auto_pc_AWLEN = S_AXI_awlen[3:0];
  assign s00_couplers_to_auto_pc_AWLOCK = S_AXI_awlock[1:0];
  assign s00_couplers_to_auto_pc_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_auto_pc_AWQOS = S_AXI_awqos[3:0];
  assign s00_couplers_to_auto_pc_AWSIZE = S_AXI_awsize[2:0];
  assign s00_couplers_to_auto_pc_AWVALID = S_AXI_awvalid;
  assign s00_couplers_to_auto_pc_BREADY = S_AXI_bready;
  assign s00_couplers_to_auto_pc_RREADY = S_AXI_rready;
  assign s00_couplers_to_auto_pc_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_auto_pc_WID = S_AXI_wid[11:0];
  assign s00_couplers_to_auto_pc_WLAST = S_AXI_wlast;
  assign s00_couplers_to_auto_pc_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_auto_pc_WVALID = S_AXI_wvalid;
dp_system_auto_pc_0 auto_pc
       (.aclk(S_ACLK_1),
        .aresetn(S_ARESETN_1),
        .m_axi_araddr(auto_pc_to_s00_couplers_ARADDR),
        .m_axi_arprot(auto_pc_to_s00_couplers_ARPROT),
        .m_axi_arready(auto_pc_to_s00_couplers_ARREADY),
        .m_axi_arvalid(auto_pc_to_s00_couplers_ARVALID),
        .m_axi_awaddr(auto_pc_to_s00_couplers_AWADDR),
        .m_axi_awprot(auto_pc_to_s00_couplers_AWPROT),
        .m_axi_awready(auto_pc_to_s00_couplers_AWREADY),
        .m_axi_awvalid(auto_pc_to_s00_couplers_AWVALID),
        .m_axi_bready(auto_pc_to_s00_couplers_BREADY),
        .m_axi_bresp(auto_pc_to_s00_couplers_BRESP),
        .m_axi_bvalid(auto_pc_to_s00_couplers_BVALID),
        .m_axi_rdata(auto_pc_to_s00_couplers_RDATA),
        .m_axi_rready(auto_pc_to_s00_couplers_RREADY),
        .m_axi_rresp(auto_pc_to_s00_couplers_RRESP),
        .m_axi_rvalid(auto_pc_to_s00_couplers_RVALID),
        .m_axi_wdata(auto_pc_to_s00_couplers_WDATA),
        .m_axi_wready(auto_pc_to_s00_couplers_WREADY),
        .m_axi_wstrb(auto_pc_to_s00_couplers_WSTRB),
        .m_axi_wvalid(auto_pc_to_s00_couplers_WVALID),
        .s_axi_araddr(s00_couplers_to_auto_pc_ARADDR),
        .s_axi_arburst(s00_couplers_to_auto_pc_ARBURST),
        .s_axi_arcache(s00_couplers_to_auto_pc_ARCACHE),
        .s_axi_arid(s00_couplers_to_auto_pc_ARID),
        .s_axi_arlen(s00_couplers_to_auto_pc_ARLEN),
        .s_axi_arlock(s00_couplers_to_auto_pc_ARLOCK),
        .s_axi_arprot(s00_couplers_to_auto_pc_ARPROT),
        .s_axi_arqos(s00_couplers_to_auto_pc_ARQOS),
        .s_axi_arready(s00_couplers_to_auto_pc_ARREADY),
        .s_axi_arsize(s00_couplers_to_auto_pc_ARSIZE),
        .s_axi_arvalid(s00_couplers_to_auto_pc_ARVALID),
        .s_axi_awaddr(s00_couplers_to_auto_pc_AWADDR),
        .s_axi_awburst(s00_couplers_to_auto_pc_AWBURST),
        .s_axi_awcache(s00_couplers_to_auto_pc_AWCACHE),
        .s_axi_awid(s00_couplers_to_auto_pc_AWID),
        .s_axi_awlen(s00_couplers_to_auto_pc_AWLEN),
        .s_axi_awlock(s00_couplers_to_auto_pc_AWLOCK),
        .s_axi_awprot(s00_couplers_to_auto_pc_AWPROT),
        .s_axi_awqos(s00_couplers_to_auto_pc_AWQOS),
        .s_axi_awready(s00_couplers_to_auto_pc_AWREADY),
        .s_axi_awsize(s00_couplers_to_auto_pc_AWSIZE),
        .s_axi_awvalid(s00_couplers_to_auto_pc_AWVALID),
        .s_axi_bid(s00_couplers_to_auto_pc_BID),
        .s_axi_bready(s00_couplers_to_auto_pc_BREADY),
        .s_axi_bresp(s00_couplers_to_auto_pc_BRESP),
        .s_axi_bvalid(s00_couplers_to_auto_pc_BVALID),
        .s_axi_rdata(s00_couplers_to_auto_pc_RDATA),
        .s_axi_rid(s00_couplers_to_auto_pc_RID),
        .s_axi_rlast(s00_couplers_to_auto_pc_RLAST),
        .s_axi_rready(s00_couplers_to_auto_pc_RREADY),
        .s_axi_rresp(s00_couplers_to_auto_pc_RRESP),
        .s_axi_rvalid(s00_couplers_to_auto_pc_RVALID),
        .s_axi_wdata(s00_couplers_to_auto_pc_WDATA),
        .s_axi_wid(s00_couplers_to_auto_pc_WID),
        .s_axi_wlast(s00_couplers_to_auto_pc_WLAST),
        .s_axi_wready(s00_couplers_to_auto_pc_WREADY),
        .s_axi_wstrb(s00_couplers_to_auto_pc_WSTRB),
        .s_axi_wvalid(s00_couplers_to_auto_pc_WVALID));
endmodule
