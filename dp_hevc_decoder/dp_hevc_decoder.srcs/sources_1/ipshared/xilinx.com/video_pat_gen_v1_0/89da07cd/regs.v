////////////////////////////////////////////////////////////////////////////////////
//
// APB Bus Register Space Address Decoder
//
////////////////////////////////////////////////////////////////////////////////////
`include "def.v"

module regs ( 
  input  wire         run_pattern,                    // temp input
  input  wire         apb_clk,                        // APB bus clock
  input  wire         apb_reset,                      // APB domain reset
  input  wire         apb_select,                     // select
  input  wire         apb_enable,                     // enable
  input  wire         apb_write,                      // write enable
  input  wire [11:0]  apb_addr,                       // address
  input  wire [31:0]  apb_wdata,                      // write data
  output reg  [31:0]  apb_rdata,                      // read data
  output reg  [(`DISP_DTC_REGS_SIZE-1):0] disp_dtc_regs,
  output reg  [2:0]   hdcolorbar_cfg,
  output reg          vid_clk_sel,
  output reg  [7:0]   vid_clk_M, 
  output reg  [15:0]  vid_clk_D,
  output reg          sw_vid_reset,
  output reg  [7:0]   dp_misc0,
  output reg  [7:0]   dp_misc1,
  output reg  [2:0]   test_pattern,
  output reg          en_sw_pattern,
  output reg          dual_pixel_mode,
  input wire  [11:0]  vsync_counter,
  input wire  [11:0]  hsync_counter,
  input  wire         frame_crc_vld,
  input  wire [15:0]  rcrc0,
  input  wire [15:0]  gcrc0,
  input  wire [15:0]  bcrc0,
  input  wire [15:0]  rcrc1,
  input  wire [15:0]  gcrc1,
  input  wire [15:0]  bcrc1,
  input wire  [11:0]  data_en_counter
);

  reg        frame_crc_vld_d;
  reg        frame_crc_vld_2d;
  reg [15:0] rcrc0_flop;
  reg [15:0] gcrc0_flop;
  reg [15:0] bcrc0_flop;
  reg [15:0] rcrc1_flop;
  reg [15:0] gcrc1_flop;
  reg [15:0] bcrc1_flop;

  /////////////////////////////////
  // apb write                   //
  /////////////////////////////////
  wire vidpat_apb_write = apb_write & apb_enable & apb_select; 
  
  always @ ( posedge apb_clk ) begin
    if(apb_reset) begin
      disp_dtc_regs    <= {`DISP_DTC_REGS_SIZE{1'b0}};
      hdcolorbar_cfg   <= 3'b 0;
      vid_clk_sel      <= 1'b 0;
      dp_misc0 <= 0;
      dp_misc1 <= 0;
      test_pattern <= 3'b 0;
      en_sw_pattern <= 0; 
      dual_pixel_mode <= 0;
      
    end else if (vidpat_apb_write) begin
      case (apb_addr) /* synthesis full_case parallel_case */
        {12'h 000}: begin
          disp_dtc_regs[`ENABLE] <= apb_wdata[0];
          sw_vid_reset           <= apb_wdata[1];
        end

        {12'h 004 }: disp_dtc_regs[`VSYNC_POLARITY]       <= apb_wdata[0];
        {12'h 008 }: disp_dtc_regs[`HSYNC_POLARITY]       <= apb_wdata[0];
        {12'h 00c }: disp_dtc_regs[`DATA_ENABLE_POLARITY] <= apb_wdata[0];
        {12'h 010 }: disp_dtc_regs[`VSYNC_WIDTH]          <= apb_wdata[11:0];
        {12'h 014 }: disp_dtc_regs[`VERT_BACK_PORCH]      <= apb_wdata[11:0];
        {12'h 018 }: disp_dtc_regs[`VERT_FRONT_PORCH]     <= apb_wdata[11:0];
        {12'h 01c }: disp_dtc_regs[`VRES]                 <= apb_wdata[11:0];
        {12'h 020 }: disp_dtc_regs[`HSYNC_WIDTH]          <= apb_wdata[11:0];
        {12'h 024 }: disp_dtc_regs[`HORIZ_BACK_PORCH]     <= apb_wdata[11:0];
        {12'h 028 }: disp_dtc_regs[`HORIZ_FRONT_PORCH]    <= apb_wdata[11:0];
        {12'h 02c }: disp_dtc_regs[`HRES]                 <= apb_wdata[11:0];

        {12'h 034 }: begin
          disp_dtc_regs[`FRAMELOCK_ENABLE] <= apb_wdata[31];
          disp_dtc_regs[`FRAMELOCK_DELAY] <= apb_wdata[21:0];
        end

        {12'h 03c }: begin
          disp_dtc_regs[`FRAMELOCK_ALIGN_HSYNC]      <= apb_wdata[16];
          disp_dtc_regs[`FRAMELOCK_LINE_FRAC]        <= apb_wdata[10:0];
        end

        {12'h 040 }: hdcolorbar_cfg              <= apb_wdata[2:0];

        {12'h 044 }: disp_dtc_regs[`TC_HSBLNK] <= apb_wdata[11:0];
        {12'h 048 }: disp_dtc_regs[`TC_HSSYNC] <= apb_wdata[11:0];
        {12'h 04C }: disp_dtc_regs[`TC_HESYNC] <= apb_wdata[11:0];
        {12'h 050 }: disp_dtc_regs[`TC_HEBLNK] <= apb_wdata[11:0];
        {12'h 054 }: disp_dtc_regs[`TC_VSBLNK] <= apb_wdata[11:0];
        {12'h 058 }: disp_dtc_regs[`TC_VSSYNC] <= apb_wdata[11:0];
        {12'h 05C }: disp_dtc_regs[`TC_VESYNC] <= apb_wdata[11:0];
        {12'h 060 }: disp_dtc_regs[`TC_VEBLNK] <= apb_wdata[11:0];

        {12'h 100 }: vid_clk_sel               <= apb_wdata[0];
        {12'h 104 }: vid_clk_M                 <= apb_wdata[7:0];
        {12'h 108 }: vid_clk_D                 <= apb_wdata[15:0];

        {12'h 300 }: dp_misc0              <= apb_wdata[7:0];
        {12'h 304 }: dp_misc1              <= apb_wdata[7:0];
	{12'h 308 }: begin test_pattern    <= apb_wdata[2:0]; 
                           en_sw_pattern   <= apb_wdata[4]; 
                           dual_pixel_mode <= apb_wdata[8]; 
	             end
      endcase
    end
  end

  always @ ( posedge apb_clk )
  begin
     if ( apb_reset )
     begin
        frame_crc_vld_d  <= 1'b 0;
        frame_crc_vld_2d <= 1'b 0;
        rcrc0_flop <= 16'b 0;
        gcrc0_flop <= 16'b 0;
        bcrc0_flop <= 16'b 0;
        rcrc1_flop <= 16'b 0;
        gcrc1_flop <= 16'b 0;
        bcrc1_flop <= 16'b 0;
     end
     else
     begin
        frame_crc_vld_d  <= frame_crc_vld;
        frame_crc_vld_2d <= frame_crc_vld_d;
        if ( frame_crc_vld_2d ) rcrc0_flop <= rcrc0;
        if ( frame_crc_vld_2d ) gcrc0_flop <= gcrc0;
        if ( frame_crc_vld_2d ) bcrc0_flop <= bcrc0;
        if ( frame_crc_vld_2d ) rcrc1_flop <= rcrc1;
        if ( frame_crc_vld_2d ) gcrc1_flop <= gcrc1;
        if ( frame_crc_vld_2d ) bcrc1_flop <= bcrc1;
     end
  end
  /////////////////////////////////
  // apb read                    //
  /////////////////////////////////
  wire vidpat_apb_read;

  assign vidpat_apb_read = ~apb_write & apb_select;

  always @ (posedge apb_clk) begin  
    if(apb_reset)
      apb_rdata <= 32'b0;
    else if (vidpat_apb_read) begin
      case (apb_addr) /* synthesis full_case parallel_case */
        {12'h 000 }: apb_rdata <= {30'b0, sw_vid_reset, disp_dtc_regs[`ENABLE]};
        {12'h 004 }: apb_rdata <= {31'b0, disp_dtc_regs[`VSYNC_POLARITY]};
        {12'h 008 }: apb_rdata <= {31'b0, disp_dtc_regs[`HSYNC_POLARITY]};
        {12'h 00c }: apb_rdata <= {31'b0, disp_dtc_regs[`DATA_ENABLE_POLARITY]};
        {12'h 010 }: apb_rdata <= {23'b0, disp_dtc_regs[`VSYNC_WIDTH]};
        {12'h 014 }: apb_rdata <= {23'b0, disp_dtc_regs[`VERT_BACK_PORCH]};
        {12'h 018 }: apb_rdata <= {23'b0, disp_dtc_regs[`VERT_FRONT_PORCH]};
        {12'h 01c }: apb_rdata <= {22'b0, disp_dtc_regs[`VRES]};
        {12'h 020 }: apb_rdata <= {23'b0, disp_dtc_regs[`HSYNC_WIDTH]};
        {12'h 024 }: apb_rdata <= {23'b0, disp_dtc_regs[`HORIZ_BACK_PORCH]};
        {12'h 028 }: apb_rdata <= {23'b0, disp_dtc_regs[`HORIZ_FRONT_PORCH]};
        {12'h 02c }: apb_rdata <= {21'b0, disp_dtc_regs[`HRES]};

        {12'h 034 }: apb_rdata <= {disp_dtc_regs[`FRAMELOCK_ENABLE], 9'b0, disp_dtc_regs[`FRAMELOCK_DELAY]};

        {12'h 03c }: apb_rdata <= {disp_dtc_regs[`FRAMELOCK_ALIGN_HSYNC], 20'b0, disp_dtc_regs[`FRAMELOCK_LINE_FRAC]};

        {12'h 040 }: apb_rdata <= {29'b0, hdcolorbar_cfg};

        {12'h 044 }: apb_rdata <= {21'b0, disp_dtc_regs[`TC_HSBLNK]};
        {12'h 048 }: apb_rdata <= {21'b0, disp_dtc_regs[`TC_HSSYNC]};
        {12'h 04C }: apb_rdata <= {21'b0, disp_dtc_regs[`TC_HESYNC]};
        {12'h 050 }: apb_rdata <= {21'b0, disp_dtc_regs[`TC_HEBLNK]};
        {12'h 054 }: apb_rdata <= {21'b0, disp_dtc_regs[`TC_VSBLNK]};
        {12'h 058 }: apb_rdata <= {21'b0, disp_dtc_regs[`TC_VSSYNC]};
        {12'h 05C }: apb_rdata <= {21'b0, disp_dtc_regs[`TC_VESYNC]};
        {12'h 060 }: apb_rdata <= {21'b0, disp_dtc_regs[`TC_VEBLNK]};

        {12'h 100 }: apb_rdata <= {31'b0, vid_clk_sel};
        {12'h 104 }: apb_rdata <= {23'b0, vid_clk_M};
        {12'h 108 }: apb_rdata <= {16'b0, vid_clk_D};

        {12'h 200 }: apb_rdata <= {20'b0, vsync_counter};
        {12'h 204 }: apb_rdata <= {20'b0, hsync_counter};
        {12'h 208 }: apb_rdata <= {20'b0, data_en_counter};

        {12'h 400 }: apb_rdata <= rcrc0_flop;
        {12'h 404 }: apb_rdata <= gcrc0_flop;
        {12'h 408 }: apb_rdata <= bcrc0_flop;
        {12'h 410 }: apb_rdata <= rcrc1_flop;
        {12'h 414 }: apb_rdata <= gcrc1_flop;
        {12'h 418 }: apb_rdata <= bcrc1_flop;
      endcase
    end
  end

endmodule
