/* ***************************************************************************
 * ** Copyright (c) 2012-2013 Xilinx, Inc.  All rights reserved.            **
 * **   ____  ____                                                          **
 * **  /   /\/   /                                                          **
 * ** /___/  \  /   Vendor: Xilinx                                          **
 * ** \   \   \/                                                            **
 * **  \   \                                                                **
 * **  /   /                                                                **
 * ** /___/   /\                                                            **
 * ** \   \  /  \                                                           **
 * **  \___\/\___\                                                          **
 * **                                                                       **
 * ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
 * ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
 * ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
 * ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
 * ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
 * ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
 * ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
 * ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
 * ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
 * ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
 * ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
 * ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
 * ** FOR A PARTICULAR PURPOSE.                                             **
 * **                                                                       **
 * **************************************************************************/


#include "stdio.h"
#include "xparameters.h"
#include "xbasic_types.h"
#include "displayport_defs.h"
#include "displayport_lpm.h"
#include "xbasic_types.h"
#include "xil_ccc_app.h"
#include "xil_displayport.h"
#include "xtmrctr.h"
#include "xintc.h"
#include "xlib_string.h"
#include "displayport_tx_drv.h"
#include "xiic.h"


#define IIC_BASE_ADDRESS XPAR_IIC_0_BASEADDR
#define VID_TIMING_GEN_STREAM0_BASE_ADDRESS XPAR_TIMING_GEN_0_BASEADDR
#define VID_TIMING_GEN_STREAM1_BASE_ADDRESS XPAR_TIMING_GEN_1_BASEADDR


#define TMRCTR_DEVICE_ID	XPAR_TMRCTR_0_DEVICE_ID
#define DDR_ID              XPAR_AXI_7SDDR_0_DEVICE_ID
#define IIC_ID              XPAR_IIC_0_DEVICE_ID
#define LEDS_ID             XPAR_LEDS_8BITS_DEVICE_ID
#define PUSHBUTTON_ID       XPAR_PUSH_BUTTONS_5BITS_DEVICE_ID
#define INTC_DEVICE_ID		XPAR_INTC_0_DEVICE_ID


#define TMRCTR_INTERRUPT_ID	    XPAR_INTC_0_TMRCTR_0_VEC_ID
#define IIC_INTERRUPT_ID        XPAR_INTC_0_IIC_0_VEC_ID


typedef struct
		{
	UINT8 type;
	UINT8 version;
	UINT8 length;
	UINT8 audio_coding_type;
	UINT8 audio_channel_count;
	UINT8 sampling_frequency;
	UINT8 sample_size;
	UINT8 level_shift;
	UINT8 downmix_inhibit;
	UINT8 channel_allocation;
	UINT16 info_length;
		}XilAudioInfoFrame;


XilAudioInfoFrame xilInfoFrame;

#define EEPROM_TEST_START_ADDRESS	0x00
#define EEPROM_ADDRESS		0x54

#define PAGE_SIZE	8
#define IIC_SLAVE_ADDRESS	1

#define IIC_SWITCH_ADDRESS 0x74
// Connected to IIC Buses
// Bus 0
#define IIC_SI570_ADDRESS  0x5D
// Bus 1
#define IIC_FMC_HPC_ADDRESS 0x70
// Bus 2
#define IIC_FMC_LPC_ADDRESS 0x70
// Bus 3
#define IIC_EEPROM_ADDRESS 0x54
// Bus 4
#define IIC_SFP_ADDRESS 0x50
// Bus 5
#define IIC_ADV7511_ADDRESS 0x39
// Bus 6
#define IIC_DDR3_SPD_ADDRESS 0x50
#define IIC_DDR3_TEMP_ADDRESS 0x18
// Bus 7
#define IIC_SI5326_ADDRESS 0x68

#define IIC_BUS_0 0x01
#define IIC_BUS_1 0x02
#define IIC_BUS_2 0x04
#define IIC_BUS_3 0x08
#define IIC_BUS_4 0x10
#define IIC_BUS_5 0x20
#define IIC_BUS_6 0x40
#define IIC_BUS_7 0x80

/**************************** Type Definitions *******************************/
typedef u8 AddressType;

/**************************** Variable Definitions *******************************/
static XIntc Intc;
static XIic  IIC;

#define NUM_VIDEO_MODES 2
#define NUM_CLOCK_REGS 6

u8 UpdateBuffer[sizeof(AddressType) + PAGE_SIZE];
u8 WriteBuffer[sizeof(AddressType) + PAGE_SIZE]; /* Write buffer for writing a page. */
u8 ReadBuffer[PAGE_SIZE]; /* Read buffer for reading a page.  */
u8 DataBuf[PAGE_SIZE];

volatile u8 TransmitComplete;	/* Flag to check completion of Transmission */
volatile u8 ReceiveComplete;	/* Flag to check completion of Reception */

typedef u8 AddressType;

u8 EepromIicAddr; /* Variable for storing Eeprom IIC address */
volatile UINT8 TIMER1_INTERRUPT_FIRED=0;


 XTmrCtr TimerCounterInst;
 XTmrCtr TimerCounterInst1;

//XTmrCtr TimerCounter;
XStatus init_platform(void);

XStatus init_timer(XTmrCtr *TmrCtrInstancePtr);
XStatus init_timer1(XTmrCtr *TmrCtrInstancePtr);

#define TMRCXIL_DEVICE_ID    XPAR_AXI_TIMER_0_DEVICE_ID

#define TMRCTR_INTERRUPT_ID	    XPAR_INTC_0_TMRCTR_0_VEC_ID

#define TIMER_RESET_VALUE	 1000 // Hz
#define TIMER_CNTR_0	 0
#define TIMER_CNTR_1	 1

#define LINK_BW_SET           DISPLAYPORT_BASEADDR + 0x0000 //RW
#define LANE_CNT_SET          DISPLAYPORT_BASEADDR + 0x0004 //RW
#define ENHANCED_FRAME_EN     DISPLAYPORT_BASEADDR + 0x0008 //RW

#define NUM_AUDIO_MODES 2
#define NUM_CLOCK_REGS 6

volatile u8 TransmitComplete;	/* Flag to check completion of Transmission */
volatile u8 ReceiveComplete;	/* Flag to check completion of Reception */
int iteration_count=0;
int timer_value;

void InitDPAudio(XilAudioInfoFrame *xilInfoFrame);
void sendAudioInfoFrame(XilAudioInfoFrame *xilInfoFrame);

/************************** Function Prototypes ******************************/
XStatus     Setup_Interrupt_Handler();
XStatus     Init_IIC();
XStatus     Prog_Si5324();
int         write_si5324();
int 		read5324();
int         write_si570();
int         readsi570();
int         EepromWriteData(u16 ByteCount);
int 		EepromReadData(u8 *BufferPtr, u16 ByteCount);
int         EepromReadData2(AddressType addr, u8 *BufferPtr, u16 ByteCount);
int         EepromWriteByte(AddressType Address, u8 *BufferPtr, u8 ByteCount);
void        DisableInterruptSystem();
static void SendHandler(XIic * InstancePtr);
static void ReceiveHandler(XIic * InstancePtr);
static void StatusHandler(XIic * InstancePtr, int Event);
static int SetupInterruptSystem(XIic * IicInstPtr);

#define RUN
int main(void)
{  

  UINT32 status = 0;
  XILCCCAppControl *app_ctrl = xilcccGetAppControl();
  UINT8 command, term_key;
  UINT32 read_data=0;
  UINT8 aux_data[1];

    dbg_printf("\033[H\033[J"); //clears the screen
	dbg_printf("\r\n-------------------------------------------------------------------------------\r\n");
    dbg_printf("\r\n                    DisplayPort Source Policy Maker                            \r\n");
	dbg_printf("\r\n                           Press 'h'for Menu \r\n");
	dbg_printf("\r\n-------------------------------------------------------------------------------\r\n");


#if LLC_TEST_MODE
    dbg_printf("0: SET SOFTWARE FOR LINK TESTING \n\r");
    dbg_printf("1: SET SOFTWARE FOR PHY TESTING \n\r");
    term_key = xil_getc(0);
#else
    term_key = '0';
#endif

UINT32 volt, volt_disp;

    switch(term_key){
    case '0':
#ifdef RUN
        init_platform();
        Setup_Interrupt_Handler();



        write_si570();
		Prog_Si5324();


        status = xildpInitDisplayport();
        xilcccAppInit();

        xilcccAppRunLoop();
        status = dplpmConfigureLink(0, 0, 1);
#endif
        status = dplpmTrainLink(0, 1, 0);

        dbg_printf("DisplayPort Policy Maker Exiting \n\r");

    	break;
#if LLC_TEST_MODE
    case '1':
        init_platform();
    	dbg_printf("Choose test option\n\r"
    			"1 --> Set PRBS7 Pattern\n\r"
    			"2 --> Set D10.2 Pattern\n\r"
    			"3 --> Set Voltage Swing\n\r"
    			"4 --> Set Pre-Emphasis\n\r"
		        "5 --> Enable/Disable Main Link\n\r"
                "6 --> RBR 1.62G Link Rate\n\r"
                "7 --> HBR 2.7G Link Rate\n\r"
                "8 --> HBR2 5.4G Link Rate\n\r"
                "9 --> Set TP2 Pattern\n\r"
    	    	"a --> Set TP3 Pattern\n\r"
    	    	"b --> Read Link Status\n\r"
    	    	"m --> Send MTPs (Unallocated & Scrambler is Disabled)\n\r"
    			);


        while(1){
    	  command = xil_getc(0);
    	  switch(command){
    	  case '1':
    		  read_data = dptx_reg_read(XILINX_DISPLAYPORT_TX_PHY_TRANSMIT_PRBS7);
    		  if(read_data !=0){
    	        dptx_reg_write(XILINX_DISPLAYPORT_TX_PHY_TRANSMIT_PRBS7, 0x00);
    	    	dbg_printf("PRBS7 Pattern Disabled\n\r");
    		  }else
    		  {
      	        dptx_reg_write(XILINX_DISPLAYPORT_TX_PHY_TRANSMIT_PRBS7, 0x01);
      	    	dbg_printf("PRBS7 Pattern Enabled\n\r");
    		  }
    		  break;
    	  case '2':
    		  read_data = dptx_reg_read(XILINX_DISPLAYPORT_TX_TRAINING_PATTERN_SET);
    		  if(read_data !=0){
    	        dptx_reg_write(XILINX_DISPLAYPORT_TX_TRAINING_PATTERN_SET, 0x00);
    	    	dbg_printf("D10.2 Pattern Disabled\n\r");
    		  }else
    		  {
    			dptx_reg_write(XILINX_DISPLAYPORT_TX_SCRAMBLING_DISABLE, 0x01);
    			dptx_reg_write(XILINX_DISPLAYPORT_TX_LANE_COUNT_SET, 0x04);
      	        dptx_reg_write(XILINX_DISPLAYPORT_TX_TRAINING_PATTERN_SET, 0x01);
      	        xildpSetTrainingPattern(XILINX_DISPLAYPORT_TRAINING_PATTERN_1);
      	    	dbg_printf("D10.2 Pattern Enabled\n\r");
    		  }
    		  break;

          case 'v':
          	volt = dptx_reg_read(XILINX_DISPLAYPORT_TX_PHY_VOLTAGE_DIFF_LANE_0);
          	volt_disp;
          	if ( volt == 0xf ) {
          		volt = 0x0;
          	} else {
          		volt++;
          	}
              dptx_reg_write(XILINX_DISPLAYPORT_TX_PHY_VOLTAGE_DIFF_LANE_0, volt);
              dptx_reg_write(XILINX_DISPLAYPORT_TX_PHY_VOLTAGE_DIFF_LANE_1, volt);
              dptx_reg_write(XILINX_DISPLAYPORT_TX_PHY_VOLTAGE_DIFF_LANE_2, volt);
              dptx_reg_write(XILINX_DISPLAYPORT_TX_PHY_VOLTAGE_DIFF_LANE_3, volt);
          	dbg_printf("Voltage Swing Vdiff Set to %d mv\n\r",volt);
          	break;

    	  case '3':
    		  xilcccCommandProcessor('1');
    		  break;
    	  case '4':
    		  xilcccCommandProcessor('2');
    		  break;
    	  case '5':
    		  xilcccCommandProcessor('m');
    		  break;
    	  case '6':
                  dptx_set_clkspeed(XILINX_DISPLAYPORT_TX_PHY_CLK_FB_162);
        	      dbg_printf("RBR 1.62G Enabled\n\r");
                  dptx_reg_write(XILINX_DISPLAYPORT_TX_LINK_BW_SET, 0x06);
    		  break;
    	  case '7':
                  dptx_set_clkspeed(XILINX_DISPLAYPORT_TX_PHY_CLK_FB_270);
        	      dbg_printf("HBR 2.7G Enabled\n\r");
                  dptx_reg_write(XILINX_DISPLAYPORT_TX_LINK_BW_SET, 0x0A);
    		  break;
    	  case '8':
                  dptx_set_clkspeed(XILINX_DISPLAYPORT_TX_PHY_CLK_FB_540);
        	      dbg_printf("HBR2 5.4G Enabled\n\r");
                  dptx_reg_write(XILINX_DISPLAYPORT_TX_LINK_BW_SET, 0x14);
    		  break;
    	  case '9':
    		  read_data = dptx_reg_read(XILINX_DISPLAYPORT_TX_TRAINING_PATTERN_SET);
    		  if(read_data !=0){
    	        dptx_reg_write(XILINX_DISPLAYPORT_TX_TRAINING_PATTERN_SET, 0x00);
    	    	dbg_printf("TP2 Pattern Disabled\n\r");
    		  }else
    		  {
    			dptx_reg_write(XILINX_DISPLAYPORT_TX_SCRAMBLING_DISABLE, 0x01);
    			dptx_reg_write(XILINX_DISPLAYPORT_TX_LANE_COUNT_SET, 0x04);
      	        dptx_reg_write(XILINX_DISPLAYPORT_TX_TRAINING_PATTERN_SET, 0x02);
      	        xildpSetTrainingPattern(XILINX_DISPLAYPORT_TRAINING_PATTERN_2);
      	    	dbg_printf("TP2 Pattern Enabled\n\r");
    		  }
    		  break;

    	  case 'a':
    		  read_data = dptx_reg_read(XILINX_DISPLAYPORT_TX_TRAINING_PATTERN_SET);
    		  if(read_data !=0){
    	        dptx_reg_write(XILINX_DISPLAYPORT_TX_TRAINING_PATTERN_SET, 0x00);
    	    	dbg_printf("TP3 Pattern Disabled\n\r");
    		  }else
    		  {
    			dptx_reg_write(XILINX_DISPLAYPORT_TX_SCRAMBLING_DISABLE, 0x01);
    			dptx_reg_write(XILINX_DISPLAYPORT_TX_LANE_COUNT_SET, 0x04);
      	        dptx_reg_write(XILINX_DISPLAYPORT_TX_TRAINING_PATTERN_SET, 0x03);
      	        xildpSetTrainingPattern(XILINX_DISPLAYPORT_TRAINING_PATTERN_3);
      	    	dbg_printf("TP3 Pattern Enabled\n\r");
    		  }    		  break;

    	  case 'b':
              	  dplpmReadDPCDStatus();
    		  break;

    	  case 'm':
    		  read_data = dptx_reg_read(XILINX_DISPLAYPORT_TX_MST_CONFIG);
    		  if(read_data !=0){
    	        dptx_reg_write(XILINX_DISPLAYPORT_TX_MST_CONFIG, 0x00);
    	    	dbg_printf("MST Mode Disabled (Scrambler Disabled)\n\r");
    		  }else
    		  {
    			dptx_reg_write(XILINX_DISPLAYPORT_TX_SCRAMBLING_DISABLE, 0x01);
    			dptx_reg_write(XILINX_DISPLAYPORT_TX_LANE_COUNT_SET, 0x04);
      	        dptx_reg_write(XILINX_DISPLAYPORT_TX_MST_CONFIG, 0x01);
      	    	dbg_printf("MST Mode Enabled (Scrambler Disabled)\n\r");
    		  }    		  break;

    	  default:
    	    	dbg_printf("Choose test option\n\r"
    	    	"1 --> Set PRBS7 Pattern\n\r"
    	    	"2 --> Set D10.2 Pattern\n\r"
    	    	"3 --> Set Voltage Swing\n\r"
    	    	"4 --> Set Pre-Emphasis\n\r"
    		    "5 --> Enable/Disable Main Link\n\r"
    	        "6 --> RBR 1.62G Link Rate\n\r"
    	        "7 --> HBR 2.7G Link Rate\n\r"
    	        "8 --> HBR2 5.4G Link Rate\n\r"
    	        "9 --> Set TP2 Pattern\n\r"
    	    	"a --> Set TP3 Pattern\n\r"
    	    	"b --> Read Link Status\n\r"
    	    	"m --> Send MTPs (Unallocated & Scrambler is Disabled)\n\r"
    	        );
    		break;
    	  }
        }//end while
    	break;
#endif
    }
    // Never return
    while (1) {}

    DisableInterruptSystem();

  return 0;
}

XStatus init_platform(void)
{
    UINT32 status = 0;
    XStatus xil_status;

    dbg4_printf("Initializing platform");
    xil_status = init_timer(&TimerCounterInst);

//#if AUDIO_RESET_TESTS
    xil_status = init_timer1(&TimerCounterInst1);
//#endif

    if (xil_status != XST_SUCCESS)
    {
        return XST_FAILURE;
    }


    status = Init_IIC();
return 0;
}

XStatus Setup_Interrupt_Handler()
{
  XStatus Status;

  // Initialize interrupt controller
  Status = XIntc_Initialize(&Intc, INTC_DEVICE_ID);
  if (Status != XST_SUCCESS) {return XST_FAILURE;}

  //IIC Interrupt
  Status = XIntc_Connect(&Intc,IIC_INTERRUPT_ID,
         (XInterruptHandler)XIic_InterruptHandler,
         &IIC);
  if (Status != XST_SUCCESS) {return XST_FAILURE;}

    // Timer Interrupt
	Status = XIntc_Connect(&Intc, TMRCTR_INTERRUPT_ID,
				(XInterruptHandler)XTmrCtr_InterruptHandler,
				&TimerCounterInst);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	// Start Interrupt COntroller
	Status = XIntc_Start(&Intc, XIN_REAL_MODE);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

  XIntc_Disable(&Intc, IIC_INTERRUPT_ID);

 // microblaze_enable_interrupts();
  return XST_SUCCESS;
}

void DisableInterruptSystem()
{
  XIntc_Disconnect(&Intc, IIC_INTERRUPT_ID);

  XIntc_Disconnect(&Intc, TMRCTR_INTERRUPT_ID);
}


XStatus Init_IIC()
{

	XStatus Status;
	XIic_Config *ConfigPtr_IIC;	/* Pointer to configuration data */

	/*
	 * Initialize the IIC driver so that it is ready to use.
	 */
	ConfigPtr_IIC = XIic_LookupConfig(IIC_ID);
	if (ConfigPtr_IIC == NULL) {
		return XST_FAILURE;
	}

	Status = XIic_CfgInitialize(&IIC, ConfigPtr_IIC, ConfigPtr_IIC->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("IIC Initialized\r\n");
	return XST_SUCCESS;
}


XStatus init_timer(XTmrCtr *TmrCtrInstancePtr) {

    XStatus Status;
    //UINT32 Value1;
    //UINT32 Value2;

	Status = XTmrCtr_Initialize(&TimerCounterInst, TIMER_CNTR_0);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

//	XTmrCtr_SetHandler(&TimerCounterInst, TimerCounterHandler, &TimerCounterInst);

//	XTmrCtr_SetOptions(&TimerCounterInst, TIMER_CNTR_0,
//				XTC_INT_MODE_OPTION | XTC_AUTO_RELOAD_OPTION);

	XTmrCtr_SetResetValue(&TimerCounterInst, TIMER_CNTR_0, TIMER_RESET_VALUE);

	XTmrCtr_Start(&TimerCounterInst, TIMER_CNTR_0);

    return XST_SUCCESS;

}

XStatus init_timer1(XTmrCtr *TmrCtrInstancePtr) {

    XStatus Status;
    //UINT32 Value1;
    //UINT32 Value2;

	Status = XTmrCtr_Initialize(&TimerCounterInst1, TIMER_CNTR_1);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

//	XTmrCtr_SetHandler(&TimerCounterInst1, TimerCounterHandler, &TimerCounterInst1);

	XTmrCtr_SetOptions(&TimerCounterInst1, TIMER_CNTR_1,
				XTC_INT_MODE_OPTION | XTC_AUTO_RELOAD_OPTION);

	XTmrCtr_SetResetValue(&TimerCounterInst1, TIMER_CNTR_1, TIMER_RESET_VALUE);

	XTmrCtr_Start(&TimerCounterInst1, TIMER_CNTR_1);

    return XST_SUCCESS;

}

//Timer Interrupt Handler
void TimerCounterHandler()
{
//	int ii=0;

#if 0
	int status;
	UINT32 no_of_aud_sample_at_sink;
    UINT8 aux_read_data[16];

	iteration_count++;


  if(iteration_count>1 && detected_audio_failure==0)
  {
    xil_printf("Timer Interrupt: %0.1d\r\n", iteration_count);
	status = xildpAUXRead(XILINX_DISPLAYPORT_DPCD_GUID, 0x0F, aux_read_data);
	no_of_aud_sample_at_sink = (aux_read_data[3]<<24)|(aux_read_data[2]<<16)|(aux_read_data[1]<<8)|aux_read_data[0];
	xil_printf("Sink Receiving Samples (L Only)...: %0.1d\r\n", no_of_aud_sample_at_sink);
	if(no_of_aud_sample_at_sink<10000)
	{
	    xil_printf("AUDIO FAILURE DETECTED !!!\r\n");
		detected_audio_failure=1;
	}
  }

  if(detected_audio_failure==0)
	  InitDPAudio(&xilInfoFrame);

#endif

  TIMER1_INTERRUPT_FIRED = 1;
}

XStatus Prog_Si5324()
{
  	write_si5324();
  	return XST_SUCCESS;
}


int write_si5324()
{
	u32 Index;
	int Status;
	AddressType Address = EEPROM_TEST_START_ADDRESS;

	Status = XIic_Initialize(&IIC, IIC_ID);
		if (Status != XST_SUCCESS) {
			xil_printf("XIic_Initialize FAILED\r\n");
			return XST_FAILURE;
		}

		// Release reset on the PCA9548 IIC Switch
		XIic_SetGpOutput(&IIC, 0xFF);

	Status = SetupInterruptSystem(&IIC);
		if (Status != XST_SUCCESS) {
			xil_printf("SetupInterruptSystem FAILED\r\n");
			return XST_FAILURE;
		}

			/*
			 * Set the Handlers for transmit and reception.
			 */
		  XIic_SetSendHandler(&IIC, &IIC,
				    (XIic_Handler) SendHandler);
//		  xil_printf("write send handler end\r\n");
		  XIic_SetRecvHandler(&IIC, &IIC,
				    (XIic_Handler) ReceiveHandler);
//		  xil_printf("write Rec Handler end\r\n");
		  XIic_SetStatusHandler(&IIC, &IIC,
				      (XIic_StatusHandler) StatusHandler);
		/*
		 * Initialize the data to write and the read buffer.
		 */
		if (sizeof(Address) == 1) {
			WriteBuffer[0] = (u8) (Address);
		}
		else {
			WriteBuffer[0] = (u8) (Address >> 8);
			WriteBuffer[1] = (u8) (Address);
			ReadBuffer[Index] = 0;
		}

//		xil_printf("\r\n Setting slave address");
		/*
			 * Set the Slave address to the PCA9543A.
			 */
		Status = XIic_SetAddress(&IIC, XII_ADDR_TO_SEND_TYPE,
					 IIC_SWITCH_ADDRESS);
		if (Status != XST_SUCCESS) {
			xil_printf("XIic_SetAddress to PCA9548 FAILED\r\n");
			return XST_FAILURE;
		}
			/*
			 * Write to the IIC Switch.
			 */
//		xil_printf("\r\n Write to IIC switch");
		WriteBuffer[0] = IIC_BUS_4; //Select Bus7 - Si5326
		Status = EepromWriteData(1);
		if (Status != XST_SUCCESS) {
			xil_printf("PCA9548 FAILED to select Si5324 IIC Bus\r\n");
			return XST_FAILURE;
		}

		/*
		 * Set the Slave address to the EEPROM
		 */
		Status = XIic_SetAddress(&IIC, XII_ADDR_TO_SEND_TYPE,
				IIC_SI5326_ADDRESS);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		 * Initialize the data to write and the read buffer.
		 */
		if (sizeof(Address) == 1) {
			WriteBuffer[0] = (u8) (Address);
		}
		else {
			WriteBuffer[0] = (u8) (Address >> 8);
			WriteBuffer[1] = (u8) (Address);
			ReadBuffer[Index] = 0;
		}

		/*
		 * Read from the EEPROM.
		 */
//	    xil_printf("Reading data from Si5324 at Address Offset %X\r\n", Address);
		Status = EepromReadData(ReadBuffer, PAGE_SIZE);
//		xil_printf("Finshed reading status from EEPROM\n");
		if (Status != XST_SUCCESS) {
		    xil_printf("Si5324 EepromReadData FAILED\r\n");
			return XST_FAILURE;
		}
		else
		{
//			xil_printf("Si5324 EepromReadData passed\r\n");
		}

//		xil_printf("\r\n");
		for (Index = 0; Index < PAGE_SIZE; Index++) {
//			xil_printf("ReadBuffer[%02d] = 0x%02X\r\n", Index, ReadBuffer[Index]);
		}
//		xil_printf("\r\n\r\n");

	// Change to 1 to set Si5324 into Bypass PLL mode
	#if 0
		// Write Reg 0 to set BYPASS
		WriteBuffer[0] = 0;
		WriteBuffer[1] = 0x16; // 0x16 sets Bypass mode; 0x14 sets non-Bypass mode.
		Status = EepromWriteData(2);
		if (Status != XST_SUCCESS) {
			xil_printf("Write to Reg 0 FAILED\r\n");
			return XST_FAILURE;
		}

		/*
		 * Read from the EEPROM.
		 */
		Status = EepromReadData(ReadBuffer, PAGE_SIZE);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
		xil_printf("\r\n");
		for (Index = 0; Index < PAGE_SIZE; Index++) {
			xil_printf("ReadBuffer[%02d] = 0x%02X\r\n", Index, ReadBuffer[Index]);
		}
	#endif

	// Change to 1 to set Si5324 Loop Bandwidth (BWSEL)
	#if 1

//		xil_printf("\r\n Read Si5324 regs before update");
		read5324();

//		xil_printf("\r\n Read Si5324 regs");

		// Write Reg 2 to set Loop Bandwidth (BWSEL)
		WriteBuffer[0] = 2;
		WriteBuffer[1] = 0x72; //Default is 0x42 equals 487 Hz; 0x72 equals 58 Hz
		Status = EepromWriteData(2);
		if (Status != XST_SUCCESS) {
			xil_printf("Write to Reg 2 FAILED\r\n");
			return XST_FAILURE;
		}
		// Write Reg 25 to set N1_HS = 6
		WriteBuffer[0] = 25;
//		WriteBuffer[1] = 0x40;
		WriteBuffer[1] = (0x02 << 5);
		Status = EepromWriteData(2);
		if (Status != XST_SUCCESS) {
			xil_printf("Write to Reg 25 FAILED\r\n");
			return XST_FAILURE;
		}

		// Write Regs 31,32,33 to set NC1_LS = 5
		WriteBuffer[0] = 31;
		WriteBuffer[1] = 0x00;
		WriteBuffer[2] = 0x00;
		WriteBuffer[3] = 0x05;
		Status = EepromWriteData(4);
		if (Status != XST_SUCCESS) {
			xil_printf("Write to Regs 31,32,33 FAILED\r\n");
			return XST_FAILURE;
		}

		// Write Regs 31,32,33 to set NC1_LS = 5
		WriteBuffer[0] = 34;
		WriteBuffer[1] = 0x00;
		WriteBuffer[2] = 0x00;
		WriteBuffer[3] = 0x05;
		Status = EepromWriteData(4);
		if (Status != XST_SUCCESS) {
			xil_printf("Write to Regs 34,35,36 FAILED\r\n");
			return XST_FAILURE;
		}

		// Write Regs 40,41,42 to set N2_HS = 9, N2_LS = 263
		WriteBuffer[0] = 40;
		WriteBuffer[1] = 0x00;
		WriteBuffer[2] = 0x02;
		WriteBuffer[3] = 0x63;
		Status = EepromWriteData(4);
		if (Status != XST_SUCCESS) {
			xil_printf("Write to Regs 40,41,42 FAILED\r\n");
			return XST_FAILURE;
		}

		// Write Regs 43,44,45 to set N31 = 43
		WriteBuffer[0] = 43;
		WriteBuffer[1] = 0x00;
		WriteBuffer[2] = 0x00;
		WriteBuffer[3] = 0x43;
		Status = EepromWriteData(4);
		if (Status != XST_SUCCESS) {
			xil_printf("Write to Regs 43,44,45 FAILED\r\n");
			return XST_FAILURE;
		}


		// Write Regs 46,47,48 to set N32 = 43
		WriteBuffer[0] = 46;
		WriteBuffer[1] = 0x00;
		WriteBuffer[2] = 0x00;
		WriteBuffer[3] = 0x43;
		Status = EepromWriteData(4);
		if (Status != XST_SUCCESS) {
			xil_printf("Write to Regs 46,47,48 FAILED\r\n");
			return XST_FAILURE;
 		}


//		xil_printf("\r\n Read Si5324 regs AFTER update");
		// Read Si5324 regs after update
		read5324();
//		xil_printf("\r\n Read Si5324 regs ");

	    // Start Si5324 Internal Calibration process
		// Write Reg 136 to set ICAL = 1
		WriteBuffer[0] = 136;
		WriteBuffer[1] = 0x40;
		Status = EepromWriteData(2);
		if (Status != XST_SUCCESS) {
//			xil_printf("Write to Reg 136 FAILED\r\n");
			return XST_FAILURE;
		}
//		xil_printf("\r\n SI5326 PROGRAMMED SUCCESSFULLY \r\n");

	#endif

		return XST_SUCCESS;

}

int read5324()
{
	u32 Index;
	int Status;
//	AddressType Address = EEPROM_TEST_START_ADDRESS;
//	AddressType addr;
//	u32 delay;
	AddressType reg_addr;

    /*
 * Read from Si5324
 * Addr, Bit Field Description
 * 25, N1_HS
 * 31, NC1_LS
 * 40, N2_HS
 * 40, N2_LS
 * 43, N31
 */
//    for( delay = 0; delay < MAX_DELAY_COUNT; delay++);
	reg_addr = 0x00; //N1_HS
	XIic_InterruptHandler(&IIC);

	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}
	reg_addr = 0x03; //N1_HS
	XIic_InterruptHandler(&IIC);

	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}
	reg_addr = 0x06; //N1_HS
	XIic_InterruptHandler(&IIC);

	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}
	reg_addr = 0x09; //N1_HS
	XIic_InterruptHandler(&IIC);

	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}
	reg_addr = 19; //N1_HS
	XIic_InterruptHandler(&IIC);

	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}
	reg_addr = 22; //N1_HS
	XIic_InterruptHandler(&IIC);

	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}
	reg_addr = 25; //N1_HS
	XIic_InterruptHandler(&IIC);

	Status = EepromReadData2(reg_addr, ReadBuffer, 1);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}

	reg_addr = 31; //NC1_LS
	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: NC1_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}

	reg_addr = 34; //NC2_LS
		Status = EepromReadData2(reg_addr, ReadBuffer, 3);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
//	xil_printf("\r\n");
//		for (Index = 0; Index < 3; Index++) {
//			xil_printf("Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//		}

	reg_addr = 40; //N2_HS, N2_LS
	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: N2_HS_LS = 0x%02X\r\n",reg_addr++, ReadBuffer[Index]);
//	}

	reg_addr = 43; //N31
	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}



//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: N31 = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}

	reg_addr = 46; //N31
	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: N32 = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}

	reg_addr = 55; //N31
	Status = EepromReadData2(reg_addr, ReadBuffer, 1);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

//	xil_printf("\r\n");
//	for (Index = 0; Index < 1; Index++) {
//		xil_printf("Reg %d: N32 = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}
	reg_addr = 131; //N31
	Status = EepromReadData2(reg_addr, ReadBuffer, 2);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

//	xil_printf("\r\n");
//	for (Index = 0; Index < 2; Index++) {
//		xil_printf("Reg %d: N32 = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}

	reg_addr = 137; //N31
	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: N32 = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}

	reg_addr = 142; //N31
	Status = EepromReadData2(reg_addr, ReadBuffer, 2);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

//	xil_printf("\r\n");
//	for (Index = 0; Index < 2; Index++) {
//		xil_printf("Reg %d: N32 = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}

	reg_addr = 136; //N31
	Status = EepromReadData2(reg_addr, ReadBuffer, 1);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

//	xil_printf("\r\n");
//	for (Index = 0; Index < 1; Index++) {
//		xil_printf("Reg %d: N32 = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}
    return XST_SUCCESS;
}


int write_si570()
{

	u32 Index;
	int Status;
	AddressType Address = EEPROM_TEST_START_ADDRESS;
	AddressType reg_addr;

	Status = XIic_Initialize(&IIC, IIC_ID);
		if (Status != XST_SUCCESS) {
			xil_printf("XIic_Initialize FAILED\r\n");
			return XST_FAILURE;
		}

		// Release reset on the PCA9548 IIC Switch
		XIic_SetGpOutput(&IIC, 0xFF);

	Status = SetupInterruptSystem(&IIC);
		if (Status != XST_SUCCESS) {
			xil_printf("SetupInterruptSystem FAILED\r\n");
			return XST_FAILURE;
		}

			/*
			 * Set the Handlers for transmit and reception.
			 */
		  XIic_SetSendHandler(&IIC, &IIC,
				    (XIic_Handler) SendHandler);
//		  xil_printf("write send handler end\r\n");
		  XIic_SetRecvHandler(&IIC, &IIC,
				    (XIic_Handler) ReceiveHandler);
//		  xil_printf("write Rec Handler end\r\n");
		  XIic_SetStatusHandler(&IIC, &IIC,
				      (XIic_StatusHandler) StatusHandler);

		/*
		 * Initialize the data to write and the read buffer.
		 */
		if (sizeof(Address) == 1) {
			WriteBuffer[0] = (u8) (Address);
		}
		else {
			WriteBuffer[0] = (u8) (Address >> 8);
			WriteBuffer[1] = (u8) (Address);
			ReadBuffer[Index] = 0;
		}

//		xil_printf("\r\n Setting slave address");
		/*
			 * Set the Slave address to the PCA9543A.
			 */
		Status = XIic_SetAddress(&IIC, XII_ADDR_TO_SEND_TYPE,
					 IIC_SWITCH_ADDRESS);
		if (Status != XST_SUCCESS) {
			xil_printf("XIic_SetAddress to PCA9548 FAILED\r\n");
			return XST_FAILURE;
		}

			/*
			 * Write to the IIC Switch.
			 */
//		xil_printf("\r\n Write to IIC switch");
		WriteBuffer[0] = 0x01; //Select Bus1 - Si570
		Status = EepromWriteData(1);
		if (Status != XST_SUCCESS) {
			xil_printf("PCA9548 FAILED to select Si5324 IIC Bus\r\n");
			return XST_FAILURE;
		}

		/*
		 * Set the Slave address to the EEPROM
		 */
		Status = XIic_SetAddress(&IIC, XII_ADDR_TO_SEND_TYPE,
				IIC_SI570_ADDRESS);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		 * Initialize the data to write and the read buffer.
		 */
		if (sizeof(Address) == 1) {
			WriteBuffer[0] = (u8) (Address);
		}
		else {
			WriteBuffer[0] = (u8) (Address >> 8);
			WriteBuffer[1] = (u8) (Address);
			ReadBuffer[Index] = 0;
		}
		/*
		 * Read from the EEPROM.
		 */
//	    xil_printf("Reading data from Si570 at Address Offset %X\r\n", Address);
		Status = EepromReadData(ReadBuffer, PAGE_SIZE);
//		xil_printf("Finshed reading status from EEPROM\n");
		if (Status != XST_SUCCESS) {
		    xil_printf("Si5324 EepromReadData FAILED\r\n");
			return XST_FAILURE;
		}
		else
		{
//			xil_printf("Si5324 EepromReadData passed\r\n");
		}

	// Change to 1 to set Si5324 Loop Bandwidth (BWSEL)
	#if 1

		reg_addr = 0x07; //N1_HS
		XIic_InterruptHandler(&IIC);
		Status = EepromReadData2(reg_addr, ReadBuffer, 1);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
//		for (Index = 0; Index < 1; Index++) {
//			xil_printf("\r\n Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//		}

		reg_addr = 0x08; //N1_HS
		XIic_InterruptHandler(&IIC);
		Status = EepromReadData2(reg_addr, ReadBuffer, 1);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
//		for (Index = 0; Index < 1; Index++) {
//			xil_printf("\r\n Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//		}

		reg_addr = 0x09; //N1_HS
		XIic_InterruptHandler(&IIC);
		Status = EepromReadData2(reg_addr, ReadBuffer, 1);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
//		for (Index = 0; Index < 1; Index++) {
//			xil_printf("\r\n Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//		}

		reg_addr = 0x0A; //N1_HS
		XIic_InterruptHandler(&IIC);
		Status = EepromReadData2(reg_addr, ReadBuffer, 1);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
//		for (Index = 0; Index < 1; Index++) {
//			xil_printf("\r\n Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//		}

		reg_addr = 0x0B; //N1_HS
		XIic_InterruptHandler(&IIC);
		Status = EepromReadData2(reg_addr, ReadBuffer, 1);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
//		for (Index = 0; Index < 1; Index++) {
//			xil_printf("\r\n Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//		}

		reg_addr = 0x0C; //N1_HS
		XIic_InterruptHandler(&IIC);
		Status = EepromReadData2(reg_addr, ReadBuffer, 1);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
//		for (Index = 0; Index < 1; Index++) {
//			xil_printf("\r\n Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//		}

	    // Start Si570 Internal Calibration process
		// Write Reg 137 to set ICAL = 1
		WriteBuffer[0] = 137;
		WriteBuffer[1] = 0x08;
		Status = EepromWriteData(2);
		if (Status != XST_SUCCESS) {
//			xil_printf("Write to Reg 136 FAILED\r\n");
			return XST_FAILURE;
		}

		WriteBuffer[0] = 7;
		WriteBuffer[1] = 0xA5;
		Status = EepromWriteData(2);
		if (Status != XST_SUCCESS) {
//			xil_printf("Write to Reg 136 FAILED\r\n");
			return XST_FAILURE;
		}

		WriteBuffer[0] = 8;
		WriteBuffer[1] = 0xC2;
		Status = EepromWriteData(2);
		if (Status != XST_SUCCESS) {
//			xil_printf("Write to Reg 136 FAILED\r\n");
			return XST_FAILURE;
		}

		WriteBuffer[0] = 9;
		WriteBuffer[1] = 0xAA;
		Status = EepromWriteData(2);
		if (Status != XST_SUCCESS) {
//			xil_printf("Write to Reg 136 FAILED\r\n");
			return XST_FAILURE;
		}

		WriteBuffer[0] = 0x0A;
		WriteBuffer[1] = 0xCD;
		Status = EepromWriteData(2);
		if (Status != XST_SUCCESS) {
//			xil_printf("Write to Reg 136 FAILED\r\n");
			return XST_FAILURE;
		}

		WriteBuffer[0] = 0x0B;
		WriteBuffer[1] = 0x44;
		Status = EepromWriteData(2);
		if (Status != XST_SUCCESS) {
//			xil_printf("Write to Reg 136 FAILED\r\n");
			return XST_FAILURE;
		}

		WriteBuffer[0] = 0x0C;
		WriteBuffer[1] = 0x10;
		Status = EepromWriteData(2);
		if (Status != XST_SUCCESS) {
//			xil_printf("Write to Reg 136 FAILED\r\n");
			return XST_FAILURE;
		}

		WriteBuffer[0] = 137;
		WriteBuffer[1] = 0x00;
		Status = EepromWriteData(2);
		if (Status != XST_SUCCESS) {
//			xil_printf("Write to Reg 136 FAILED\r\n");
			return XST_FAILURE;
		}

		WriteBuffer[0] = 135;
		WriteBuffer[1] = 0x20;
		Status = EepromWriteData(2);
		if (Status != XST_SUCCESS) {
			xil_printf("Write to Reg 136 FAILED\r\n");
			return XST_FAILURE;
		}

		reg_addr = 0x07; //N1_HS
			XIic_InterruptHandler(&IIC);
			Status = EepromReadData2(reg_addr, ReadBuffer, 1);
			if (Status != XST_SUCCESS) {
				return XST_FAILURE;
			}
//			for (Index = 0; Index < 1; Index++) {
//				xil_printf("\r\n Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//			}

			reg_addr = 0x08; //N1_HS
			XIic_InterruptHandler(&IIC);
			Status = EepromReadData2(reg_addr, ReadBuffer, 1);
			if (Status != XST_SUCCESS) {
				return XST_FAILURE;
			}
//			for (Index = 0; Index < 1; Index++) {
//				xil_printf("\r\n Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//			}

			reg_addr = 0x09; //N1_HS
			XIic_InterruptHandler(&IIC);
			Status = EepromReadData2(reg_addr, ReadBuffer, 1);
			if (Status != XST_SUCCESS) {
				return XST_FAILURE;
			}
//			for (Index = 0; Index < 1; Index++) {
//				xil_printf("\r\n Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//			}

			reg_addr = 0x0A; //N1_HS
			XIic_InterruptHandler(&IIC);
			Status = EepromReadData2(reg_addr, ReadBuffer, 1);
			if (Status != XST_SUCCESS) {
				return XST_FAILURE;
			}
//			for (Index = 0; Index < 1; Index++) {
//				xil_printf("\r\n Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//			}

			reg_addr = 0x0B; //N1_HS
			XIic_InterruptHandler(&IIC);
			Status = EepromReadData2(reg_addr, ReadBuffer, 1);
			if (Status != XST_SUCCESS) {
				return XST_FAILURE;
			}
//			for (Index = 0; Index < 1; Index++) {
//				xil_printf("\r\n Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//			}

			reg_addr = 0x0C; //N1_HS
			XIic_InterruptHandler(&IIC);
			Status = EepromReadData2(reg_addr, ReadBuffer, 1);
			if (Status != XST_SUCCESS) {
				return XST_FAILURE;
			}
//			for (Index = 0; Index < 1; Index++) {
//				xil_printf("\r\n Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//			}
//		xil_printf("\r\n SI570 PROGRAMMED SUCCESSFULLY \r\n");


	#endif

		return XST_SUCCESS;

}

int readsi570()
{
	u32 Index;
	int Status;
//	AddressType Address = EEPROM_TEST_START_ADDRESS;
//	AddressType addr;
//	u32 delay;
	AddressType reg_addr;

    /*
 * Read from Si5324
 * Addr, Bit Field Description
 * 25, N1_HS
 * 31, NC1_LS
 * 40, N2_HS
 * 40, N2_LS
 * 43, N31
 */

	reg_addr = 0x00; //N1_HS
	XIic_InterruptHandler(&IIC);

	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("\r\n");

	reg_addr = 0x03; //N1_HS
	XIic_InterruptHandler(&IIC);

	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("\r\n");


	reg_addr = 0x06; //N1_HS
	XIic_InterruptHandler(&IIC);

	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}
	reg_addr = 0x09; //N1_HS
	XIic_InterruptHandler(&IIC);

	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}
	reg_addr = 19; //N1_HS
	XIic_InterruptHandler(&IIC);

	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}
	reg_addr = 22; //N1_HS
	XIic_InterruptHandler(&IIC);

	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}
	reg_addr = 25; //N1_HS
	XIic_InterruptHandler(&IIC);

	Status = EepromReadData2(reg_addr, ReadBuffer, 1);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}

	reg_addr = 31; //NC1_LS
	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: NC1_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}

	reg_addr = 34; //NC2_LS
		Status = EepromReadData2(reg_addr, ReadBuffer, 3);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
//	xil_printf("\r\n");
//		for (Index = 0; Index < 3; Index++) {
//			xil_printf("Reg %d: NC2_LS = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//		}

	reg_addr = 40; //N2_HS, N2_LS
	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: N2_HS_LS = 0x%02X\r\n",reg_addr++, ReadBuffer[Index]);
//	}

	reg_addr = 43; //N31
	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: N31 = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}

	reg_addr = 46; //N31
	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: N32 = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}

	reg_addr = 55; //N31
	Status = EepromReadData2(reg_addr, ReadBuffer, 1);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

//	xil_printf("\r\n");
//	for (Index = 0; Index < 1; Index++) {
//		xil_printf("Reg %d: N32 = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}
	reg_addr = 131; //N31
	Status = EepromReadData2(reg_addr, ReadBuffer, 2);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

//	xil_printf("\r\n");
//	for (Index = 0; Index < 2; Index++) {
//		xil_printf("Reg %d: N32 = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}

	reg_addr = 137; //N31
	Status = EepromReadData2(reg_addr, ReadBuffer, 3);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

//	xil_printf("\r\n");
//	for (Index = 0; Index < 3; Index++) {
//		xil_printf("Reg %d: N32 = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}

	reg_addr = 142; //N31
	Status = EepromReadData2(reg_addr, ReadBuffer, 2);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

//	xil_printf("\r\n");
//	for (Index = 0; Index < 2; Index++) {
//		xil_printf("Reg %d: N32 = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}

	reg_addr = 136; //N31
	Status = EepromReadData2(reg_addr, ReadBuffer, 1);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

//	xil_printf("\r\n");
//	for (Index = 0; Index < 1; Index++) {
//		xil_printf("Reg %d: N32 = 0x%02X\r\n", reg_addr++, ReadBuffer[Index]);
//	}
    return XST_SUCCESS;
}


/*****************************************************************************/
/**
* This Send handler is called asynchronously from an interrupt
* context and indicates that data in the specified buffer has been sent.
*
* @param	InstancePtr is not used, but contains a pointer to the IIC
*		device driver instance which the handler is being called for.
*
* @return	None.
*
* @note		None.
*
******************************************************************************/
static void SendHandler(XIic * InstancePtr)
{
	TransmitComplete = 0;
}

/*****************************************************************************/
/**
* This Receive handler is called asynchronously from an interrupt
* context and indicates that data in the specified buffer has been Received.
*
* @param	InstancePtr is not used, but contains a pointer to the IIC
*		device driver instance which the handler is being called for.
*
* @return	None.
*
* @note		None.
*
******************************************************************************/
static void ReceiveHandler(XIic * InstancePtr)
{
	ReceiveComplete = 0;
}

/*****************************************************************************/
/**
* This Status handler is called asynchronously from an interrupt
* context and indicates the events that have occurred.
*
* @param	InstancePtr is a pointer to the IIC driver instance for which
*		the handler is being called for.
* @param	Event indicates the condition that has occurred.
*
* @return	None.
*
* @note		None.
*
******************************************************************************/
static void StatusHandler(XIic * InstancePtr, int Event)
{

}

/*
 * *********************************************************
 * EepromReadData2 with address added as an input parameter
 * *********************************************************
 */


/*****************************************************************************/
/**
* This function reads data from the IIC serial EEPROM into a specified buffer.
*
* @param	BufferPtr contains the address of the data buffer to be filled.
* @param	ByteCount contains the number of bytes in the buffer to be read.
*
* @return	XST_SUCCESS if successful else XST_FAILURE.
*
* @note		None.
*
******************************************************************************/
int EepromReadData(u8 *BufferPtr, u16 ByteCount)
{
	int Status;
	AddressType Address = EEPROM_TEST_START_ADDRESS;

	/*
	 * Set the Defaults.
	 */
	ReceiveComplete = 1;

	/*
	 * Position the Pointer in EEPROM.
	 */
	if (sizeof(Address) == 1) {
		WriteBuffer[0] = (u8) (Address);
	}
	else {
		WriteBuffer[0] = (u8) (Address >> 8);
		WriteBuffer[1] = (u8) (Address);
	}

	Status = EepromWriteData(sizeof(Address));
//	xil_printf("In EEPROMREADDATA\n");
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	/*
	 * Start the IIC device.
	 */
	XIic_InterruptHandler(&IIC);
	Status = XIic_Start(&IIC);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	XIic_InterruptHandler(&IIC);
	/*
	 * Receive the Data.
	 */
	Status = XIic_MasterRecv(&IIC, BufferPtr, ByteCount);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	XIic_InterruptHandler(&IIC);
	/*
	 * Wait till all the data is received.
	 */
	while ((ReceiveComplete) || (XIic_IsIicBusy(&IIC) == TRUE)) {
		XIic_InterruptHandler(&IIC);
	}

	/*
	 * Stop the IIC device.
	 */
	Status = XIic_Stop(&IIC);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	XIic_InterruptHandler(&IIC);
	return XST_SUCCESS;
}



int EepromReadData2(AddressType addr, u8 *BufferPtr, u16 ByteCount)
{
	int Status;
//	AddressType Address = EEPROM_TEST_START_ADDRESS;
	AddressType Address;
	Address = addr;

	/*
	 * Set the Defaults.
	 */
	ReceiveComplete = 1;

	/*
	 * Position the Pointer in EEPROM.
	 */
	//xil_printf("st11\r\n");
	if (sizeof(Address) == 1) {
		WriteBuffer[0] = (u8) (Address);
	}
	else {
		WriteBuffer[0] = (u8) (Address >> 8);
		WriteBuffer[1] = (u8) (Address);
	}
	//xil_printf("st0\r\n");
	Status = EepromWriteData(sizeof(Address));
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	//xil_printf("st wr read2\r\n");
	/*
	 * Start the IIC device.
	 */
	XIic_InterruptHandler(&IIC);

	Status = XIic_Start(&IIC);
	XIic_InterruptHandler(&IIC);

	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	//xil_printf("start read2\r\n");

	/*
	 * Receive the Data.
	 */
	Status = XIic_MasterRecv(&IIC, BufferPtr, ByteCount);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	XIic_InterruptHandler(&IIC);

	/*
	 * Wait till all the data is received.
	 */
	//xil_printf("mst rec\r\n");
	while ((ReceiveComplete) || (XIic_IsIicBusy(&IIC) == TRUE)) {
		XIic_InterruptHandler(&IIC);
	}
	//xil_printf("rec cmpl\r\n");
	/*
	 * Stop the IIC device.
	 */
	Status = XIic_Stop(&IIC);
	XIic_InterruptHandler(&IIC);

	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	return XST_SUCCESS;
}

/*****************************************************************************/
/**
* This function writes a buffer of data to the IIC serial EEPROM.
*
* @param	ByteCount contains the number of bytes in the buffer to be
*		written.
*
* @return	XST_SUCCESS if successful else XST_FAILURE.
*
* @note		The Byte count should not exceed the page size of the EEPROM as
*		noted by the constant PAGE_SIZE.
*
******************************************************************************/
int EepromWriteData(u16 ByteCount)
{
	int Status;

	/*
	 * Set the defaults.
	 */
	TransmitComplete = 1;
	IIC.Stats.TxErrors = 0;

	/*
	 * Start the IIC device.
	 */
//	xil_printf("\r\nsat0\r\n");
	XIic_InterruptHandler(&IIC);
	Status = XIic_Start(&IIC);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	XIic_InterruptHandler(&IIC);
//	xil_printf("sat1\r\n");
	/*
	 * Send the Data.
	 */

	Status = XIic_MasterSend(&IIC, WriteBuffer, ByteCount);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	XIic_InterruptHandler(&IIC);
//	xil_printf("sat2\r\n");
	/*
	 * Wait till the transmission is completed.
	 */
	while ((TransmitComplete) || (XIic_IsIicBusy(&IIC) == TRUE)) {
		XIic_InterruptHandler(&IIC);
		/*
		 * This condition is required to be checked in the case where we
		 * are writing two consecutive buffers of data to the EEPROM.
		 * The EEPROM takes about 2 milliseconds time to update the data
		 * internally after a STOP has been sent on the bus.
		 * A NACK will be generated in the case of a second write before
		 * the EEPROM updates the data internally resulting in a
		 * Transmission Error.
		 */

//		if ((XIic_IsIicBusy(&IIC) == TRUE))
//		{
//			xil_printf("IIC is busy");
//		}


		if (IIC.Stats.TxErrors != 0) {

			XIic_InterruptHandler(&IIC);

			/*
			 * Enable the IIC device.
			 */
			Status = XIic_Start(&IIC);
			if (Status != XST_SUCCESS) {
				return XST_FAILURE;
			}
			XIic_InterruptHandler(&IIC);
			//xil_printf("sat3\r\n");

			if (!XIic_IsIicBusy(&IIC)) {
				/*
				 * Send the Data.
				 */
				Status = XIic_MasterSend(&IIC,
							 WriteBuffer,
							 ByteCount);
				if (Status == XST_SUCCESS) {
					IIC.Stats.TxErrors = 0;
				}
				else {
				}
			}
		}
	}
//	xil_printf("InEEPROMWriteData\n");
	/*
	 * Stop the IIC device.
	 */
	//xil_printf("sat4\r\n");
	XIic_InterruptHandler(&IIC);
	Status = XIic_Stop(&IIC);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	XIic_InterruptHandler(&IIC);
//	xil_printf("sat5\r\n");

	return XST_SUCCESS;
}

/*****************************************************************************/
/**
 * This function writes a buffer of bytes to the IIC serial EEPROM.
 *
 * @param	BufferPtr contains the address of the data to write.
 * @param	ByteCount contains the number of bytes in the buffer to be
 *		written. Note that this should not exceed the page size of the
 *		EEPROM as noted by the constant PAGE_SIZE.
 *
 * @return	The number of bytes written, a value less than that which was
 *		specified as an input indicates an error.
 *
 * @note		one.
 *
 ******************************************************************************/
int EepromWriteByte(AddressType Address, u8 *BufferPtr, u8 ByteCount)
{
	u8 SentByteCount;
	u8 WriteBuffer[sizeof(Address) + PAGE_SIZE];
	u8 Index;

	/*
	 * A temporary write buffer must be used which contains both the address
	 * and the data to be written, put the address in first based upon the
	 * size of the address for the EEPROM
	 */
	if (sizeof(AddressType) == 2) {
		WriteBuffer[0] = (u8) (Address >> 8);
		WriteBuffer[1] = (u8) (Address);
	} else if (sizeof(AddressType) == 1) {
		WriteBuffer[0] = (u8) (Address);
		EepromIicAddr |= (EEPROM_TEST_START_ADDRESS >> 8) & 0x7;
	}

	/*
	 * Put the data in the write buffer following the address.
	 */
	for (Index = 0; Index < ByteCount; Index++) {
		WriteBuffer[sizeof(Address) + Index] = BufferPtr[Index];
	}

	/*
	 * Write a page of data at the specified address to the EEPROM.
	 */
	SentByteCount = XIic_DynSend(IIC_BASE_ADDRESS, EepromIicAddr, WriteBuffer,
			sizeof(Address) + ByteCount, XIIC_STOP);

	/*
	 * Return the number of bytes written to the EEPROM.
	 */
	return SentByteCount - sizeof(Address);
}

/*****************************************************************************/
/**
* This function setups the interrupt system so interrupts can occur for the
* IIC device. The function is application-specific since the actual system may
* or may not have an interrupt controller. The IIC device could be directly
* connected to a processor without an interrupt controller. The user should
* modify this function to fit the application.
*
* @param	IicInstPtr contains a pointer to the instance of the IIC device
*		which is going to be connected to the interrupt controller.
*
* @return	XST_SUCCESS if successful else XST_FAILURE.
*
* @note		None.
*
******************************************************************************/
static int SetupInterruptSystem(XIic * IicInstPtr)
{
	int Status;

	if (Intc.IsStarted == XCOMPONENT_IS_STARTED) {
		return XST_SUCCESS;
	}

	/*
	 * Initialize the interrupt controller driver so that it's ready to use.
	 */
	Status = XIntc_Initialize(&Intc, INTC_DEVICE_ID);

	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Connect the device driver handler that will be called when an
	 * interrupt for the device occurs, the handler defined above performs
	 * the specific interrupt processing for the device.
	 */
	Status = XIntc_Connect(&Intc, IIC_INTERRUPT_ID,
			       (XInterruptHandler) XIic_InterruptHandler,
			       IicInstPtr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Start the interrupt controller so interrupts are enabled for all
	 * devices that cause interrupts.
	 */
	Status = XIntc_Start(&Intc, XIN_REAL_MODE);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Enable the interrupts for the IIC device.
	 */
	XIntc_Enable(&Intc, IIC_INTERRUPT_ID);

#ifdef __PPC__
	/*
	 * Initialize the PPC exception table.
	 */
	XExc_Init();

	/*
	 * Register the interrupt controller handler with the exception table.
	 */
	XExc_RegisterHandler(XEXC_ID_NON_CRITICAL_INT,
			     (XExceptionHandler) XIntc_InterruptHandler,
			     &InterruptController);

	/*
	 * Enable non-critical exceptions.
	 */
	XExc_mEnableExceptions(XEXC_NON_CRITICAL);
#endif

#ifdef __MICROBLAZE__
	/*
	 * Enable the Microblaze Interrupts.
	 */
	microblaze_enable_interrupts();
#endif

	return XST_SUCCESS;
}


void InitDPAudio(XilAudioInfoFrame *xilInfoFrame)
{
	int ii, maud, naud,misc0;


	  //Step1: Reset SPDIF Rx
//    dptx_reg_write((XILINX_DISPLAYPORT_RX_SPDIF_BASE_ADDRESS + (0x040)),  0x000A );
//    for(ii=0; ii<20; ii++){}

    //Step 2: Disable DP Audio
    dptx_reg_write((XILINX_DISPLAYPORT_TX_AUDIO_CONTROL),  0x0000 );
    for(ii=0; ii<20; ii++){}

//    sendAudioInfoFrame(xilInfoFrame);

    //Step 3: Write Channel Count (actual count - 1)
    dptx_reg_write((XILINX_DISPLAYPORT_TX_AUDIO_CHANNELS),  0x0001 );

    //Assuming, Audio Sample rate = 44.1 KHz, LInk Rate = 1.62G
    dptx_reg_write((XILINX_DISPLAYPORT_TX_AUDIO_MAUD),  225792 );
    dptx_reg_write((XILINX_DISPLAYPORT_TX_AUDIO_NAUD),  162 );

    dptx_reg_write((XILINX_DISPLAYPORT_TX_AUDIO_CONTROL),  0x0001 );

//    maud = dptx_reg_read((XILINX_DISPLAYPORT_TX_AUDIO_MAUD));
//    naud = dptx_reg_read((XILINX_DISPLAYPORT_TX_AUDIO_NAUD));
//    misc0 = dptx_reg_read((XILINX_DISPLAYPORT_TX_MAIN_LINK_MISC0));
	xil_printf("[Info]DP Audio:: Enabled\r\n");
//	xil_printf("[Info]DP Audio:: Disabled -> Enabled....Maud=%0.1d, Naud=%0.1d, misc0=0x%x\r\n",maud,naud,misc0);

}


void sendAudioInfoFrame(XilAudioInfoFrame *xilInfoFrame)
{
    UINT8 db1, db2, db3, db4;
    UINT32 temp;
    UINT8 RSVD=0;

    //Fixed paramaters
    UINT8  dp_version   = 0x11;

	//Write #1
    db1 = 0x00; //sec packet ID fixed to 0 - SST Mode
    db2 = 0x80 + xilInfoFrame->type;
    db3 = xilInfoFrame->info_length&0xFF;
    db4 = (dp_version<<2)|(xilInfoFrame->info_length>>8);
	temp = db4<<24|db3<<16|db2<<8|db1;
	dptx_reg_write(XILINX_DISPLAYPORT_TX_AUDIO_INFO_DATA, temp);
	dbg_printf("\n[AUDIO_INFOFRAME] Word1=0x%x\r",temp);

	//Write #2
	db1 = xilInfoFrame->audio_channel_count | (xilInfoFrame->audio_coding_type<<4) | (RSVD<<5);
	db2 = (RSVD<<5)| (xilInfoFrame->sampling_frequency<<2) | xilInfoFrame->sample_size;
	db3 = RSVD;
	db4 = xilInfoFrame->channel_allocation;
	temp = db4<<24|db3<<16|db2<<8|db1;
	dptx_reg_write(XILINX_DISPLAYPORT_TX_AUDIO_INFO_DATA, temp);
	dbg_printf("\n[AUDIO_INFOFRAME] Word2=0x%x\r",temp);

	//Write #3
	db1 = (xilInfoFrame->level_shift<<3)|RSVD|(xilInfoFrame->downmix_inhibit <<7);
	db2 = RSVD;
	db3 = RSVD;
	db4 = RSVD;
	temp = db4<<24|db3<<16|db2<<8|db1;
	dptx_reg_write(XILINX_DISPLAYPORT_TX_AUDIO_INFO_DATA, temp);
	dbg_printf("\n[AUDIO_INFOFRAME] Word3=0x%x\r",temp);

	//Write #4
	db1 = RSVD;
	db2 = RSVD;
	db3 = RSVD;
	db4 = RSVD;
	temp = 0x00000000;
	dptx_reg_write(XILINX_DISPLAYPORT_TX_AUDIO_INFO_DATA, temp);
	dbg_printf("\n[AUDIO_INFOFRAME] Word4-Word8=0x%x\r",temp);

	//Write #5
	dptx_reg_write(XILINX_DISPLAYPORT_TX_AUDIO_INFO_DATA, temp);

	//Write #6
	dptx_reg_write(XILINX_DISPLAYPORT_TX_AUDIO_INFO_DATA, temp);
	//Write #7
	dptx_reg_write(XILINX_DISPLAYPORT_TX_AUDIO_INFO_DATA, temp);
	//Write #8
	dptx_reg_write(XILINX_DISPLAYPORT_TX_AUDIO_INFO_DATA, temp);
}





