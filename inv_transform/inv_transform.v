`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:34:10 04/09/2014 
// Design Name: 
// Module Name:    inv_transform 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module inv_transform(
	  clk,
	  rst,

	  read_in,
     read_next,
	  read_empty,
	  
     outConfig,
	  validConfig,
	  fullConfig,
	  
	  outY,
	  validY,
	  fullY,
	
	  outCb,
	  validCb,
	  fullCb,
	  
	  outCr,
	  validCr,
	  fullCr,
	  
	  //test
	  trafo_x0,
	  trafo_y0,
	  trafo_POC,
	  trafo_clk_en,
	  
	  port5,
	  port6,
	  port7,
	  port8,
	  port9
    );
	 
	 input clk;
	 input rst;
	 
	 input [31:0] read_in;
	 output read_next;
	 input read_empty;
	 
	 output reg[31:0] outConfig;
	 output reg validConfig;
	 input fullConfig;
	 
	 output reg [143:0] outY, outCb, outCr;
	 output reg validY, validCb, validCr;
	 input fullY, fullCb, fullCr;
	 
	 output [11:0] trafo_x0, trafo_y0;
	 
	 output [15:0] trafo_POC;
	 output trafo_clk_en;
	 
	 output [27:0] port5, port6;
	 output [26:0] port7;
	 output [71:0] port8;
	 output [5:0] port9;
	 // internal
	 
	 wire clk_en, clk_en_fe;
	 wire [143:0] outY_w, outCb_w, outCr_w;
	 wire [31:0] outConfig_w;
	 wire validY_w, validCb_w, validCr_w, validConfig_w;
	 assign clk_en_fe = ~(read_empty | fullConfig | fullY | fullCb | fullCr);
	 assign clk_en = ~(fullConfig | fullY | fullCb | fullCr);
	 assign trafo_clk_en = clk_en;
	 
	 wire [15:0] fe_coeff, s_coeff, mem_coeff;
	 wire [4:0] fe_x, s_x, mcm_x, p_x;
	 wire [4:0] fe_y, s_y, mcm_y, p_y;
	
	 wire [5:0] fe_QP;
	 wire [1:0] fe_cIdx, s_cIdx, mcm_cIdx, p_cIdx, res_cIdx;
	
	 wire [2:0] fe_trafoSize, s_trafoSize, mcm_trafoSize, p_trafoSize, res_trafoSize;
	 wire fe_trType, s_trType, mcm_trType, p_trType;
	 
	 wire fe_transform_skip_flag, s_transform_skip_flag, mcm_transform_skip_flag, p_transform_skip_flag;
	 wire fe_transquant_bypass_flag, s_transquant_bypass_flag, mcm_transquant_bypass_flag, p_transquant_bypass_flag;
	
	 wire fe_tr_stage, s_tr_stage,  mcm_tr_stage,p_tr_stage;
	 wire fe_tr_valid, s_tr_valid,  mcm_tr_valid,p_tr_valid;
	 wire fe_new_row,  s_new_row,   mcm_new_row, p_new_row;
	 wire	fe_end_row,  s_end_row ,  mcm_end_row, p_end_row; 
	 
	 wire [23:0] x90,	x89,	x88,	x87,	x85,	x83,	x82,	x80,	x78,	x75,	x73,	x70,	x67,	x64,	x61,	x57,	x54,	x50,	x46,	x43,	x38,	x36,	x31,	x25,	x22,	x18,	x13,	x9,	x4,	x29,	x55,	x74,	x84;
	 wire [23:0] p_coeff [0:31];
	 
	 wire [31:0] mem_dout[0:31];
	 wire [31:0] acc_out[0:31];
	 wire [31:0] add_out[0:31];
	 wire [23:0] p_bypass;
	 
	 wire [8:0] res_out [0:31];
	 wire [4:0] res_row;
	 wire res_valid;
	 
	 wire [1:0] trafo_cIdx;
	 
	 assign port5 = {s_tr_stage, s_tr_valid, s_x, s_y, s_coeff};
	 assign port6 = {fe_tr_stage, fe_tr_valid, fe_x, fe_y, fe_coeff};
	 assign port7 = {trafo_x0, trafo_y0, trafo_cIdx, clk_en};
	 assign port8 = {res_out[0], res_out[1], res_out[2], res_out[3], res_out[4], res_out[5], res_out[6], res_out[7]};
	 assign port9 = {res_valid, res_row};
	 
	 always @(posedge clk) begin
		validConfig <= validConfig_w;
		validY <= validY_w;
		validCb <= validCb_w;
		validCr <= validCr_w;
		outConfig <= outConfig_w;
		outY <= outY_w;
		outCb <= outCb_w;
		outCr <= outCr_w;
	 end
	 
	 front_end front_end(
		.clk(clk),
		.clk_en(clk_en_fe), // set this if input fifo empty or output fifo full
		.rst(rst),
		
		.read_in(read_in),
		.read_next(read_next), //wire
		
		.coeff(fe_coeff),
		.x(fe_x),
		.y(fe_y),
		
		.QP(fe_QP),
		.cIdx(fe_cIdx),
		.trafoSize(fe_trafoSize),
		.trType(fe_trType),
		
		.transform_skip_flag(fe_transform_skip_flag),
		.transquant_bypass_flag(fe_transquant_bypass_flag),
		
		.tr_stage(fe_tr_stage), // row or column transform
		.tr_valid(fe_tr_valid), // down if currently in bypass mode
		.new_row(fe_new_row),// asseted at the beginning of new row when state=STATE_ROW
		.end_row(fe_end_row),
		
		//config fifo interface
		.config_fifo_in(outConfig_w), // wire
		.config_fifo_valid(validConfig_w),
		.trafo_x0(trafo_x0),
		.trafo_y0(trafo_y0),
		.trafo_POC(trafo_POC),
		.trafo_cIdx(trafo_cIdx)
    );
	 
	 scaling scaling(
		.clk(clk),
		.clk_en(clk_en), // set this if input fifo empty or output fifo full
		.rst(rst),
		
		.coeff(fe_coeff),
		.coeff_mem(mem_coeff),
		.x(fe_x),
		.y(fe_y),
		
		.QP(fe_QP),
		.cIdx(fe_cIdx),
		.trafoSize(fe_trafoSize),
		.trType(fe_trType),
		
		.transform_skip_flag(fe_transform_skip_flag),
		.transquant_bypass_flag(fe_transquant_bypass_flag),
		
		.tr_stage(fe_tr_stage), // row or column transform
		.tr_valid(fe_tr_valid), // down if currently in bypass mode
		.new_row(fe_new_row),// asseted at the beginning of new row when state=STATE_ROW
		.end_row(fe_end_row),
		
		.out_coeff(s_coeff),
		.out_x(s_x),
		.out_y(s_y),
		
		.out_cIdx(s_cIdx),
		.out_trafoSize(s_trafoSize),
		.out_trType(s_trType),
		
		.out_transform_skip_flag(s_transform_skip_flag),
		.out_transquant_bypass_flag(s_transquant_bypass_flag),
		
		.out_tr_stage(s_tr_stage), 
		.out_tr_valid(s_tr_valid),
		.out_new_row(s_new_row),
		.out_end_row(s_end_row)

    );
	 
	 
	mcm_33 mcm(
		.clk(clk),
		.clk_en(clk_en), // set this if input fifo empty or output fifo full
		.rst(rst),
		
		.coeff(s_coeff),
		.x(s_x),
		.y(s_y),
		
		.cIdx(s_cIdx),
		.trafoSize(s_trafoSize),
		.trType(s_trType),
		
		.transform_skip_flag(s_transform_skip_flag),
		.transquant_bypass_flag(s_transquant_bypass_flag),
		
		.tr_stage(s_tr_stage), // row or column transform
		.tr_valid(s_tr_valid), // down if currently in bypass mode
		.new_row(s_new_row),// asseted at the beginning of new row when state=STATE_ROW
		.end_row(s_end_row),
		
		.x90(x90),
		.x89(x89),
		.x88(x88),
		.x87(x87),
		.x85(x85),
		.x83(x83),
		.x82(x82),
		.x80(x80),
		.x78(x78),
		.x75(x75),
		.x73(x73),
		.x70(x70),
		.x67(x67),
		.x64(x64),
		.x61(x61),
		.x57(x57),
		.x54(x54),
		.x50(x50),
		.x46(x46),
		.x43(x43),
		.x38(x38),
		.x36(x36),
		.x31(x31),
		.x25(x25),
		.x22(x22),
		.x18(x18),
		.x13(x13),
		.x9(x9),
		.x4(x4),
		.x29(x29),
		.x55(x55),
		.x74(x74),
		.x84(x84),

		.out_x(mcm_x),
		.out_y(mcm_y),
		
		.out_cIdx(mcm_cIdx),
		.out_trafoSize(mcm_trafoSize),
		.out_trType(mcm_trType),
		
		.out_transform_skip_flag(mcm_transform_skip_flag),
		.out_transquant_bypass_flag(mcm_transquant_bypass_flag),
		
		.out_tr_stage(mcm_tr_stage), // row or column transform
		.out_tr_valid(mcm_tr_valid), // down if currently in bypass mode
		.out_new_row(mcm_new_row),// asseted at the beginning of new row when state=STATE_ROW
		.out_end_row(mcm_end_row)
    );

	permute_test permute(
		.clk(clk),
		.clk_en(clk_en), // set this if input fifo empty or output fifo full
		.rst(rst),
		
		.x(mcm_x),
		.y(mcm_y),
		
		.cIdx(mcm_cIdx),
		.trafoSize(mcm_trafoSize),
		.trType(mcm_trType),
		
		.transform_skip_flag(mcm_transform_skip_flag),
		.transquant_bypass_flag(mcm_transquant_bypass_flag),
		
		.tr_stage(mcm_tr_stage), // row or column transform
		.tr_valid(mcm_tr_valid), // down if currently in bypass mode
		.new_row(mcm_new_row),// asseted at the beginning of new row when state=STATE_ROW
		.end_row(mcm_end_row),
		
		.x90(x90),
		.x89(x89),
		.x88(x88),
		.x87(x87),
		.x85(x85),
		.x83(x83),
		.x82(x82),
		.x80(x80),
		.x78(x78),
		.x75(x75),
		.x73(x73),
		.x70(x70),
		.x67(x67),
		.x64(x64),
		.x61(x61),
		.x57(x57),
		.x54(x54),
		.x50(x50),
		.x46(x46),
		.x43(x43),
		.x38(x38),
		.x36(x36),
		.x31(x31),
		.x25(x25),
		.x22(x22),
		.x18(x18),
		.x13(x13),
		.x9(x9),
		.x4(x4),
		.x29(x29),
		.x55(x55),
		.x74(x74),
		.x84(x84),
		
		.out_0(p_coeff[0]),
		.out_1(p_coeff[1]),
		.out_2(p_coeff[2]),
		.out_3(p_coeff[3]),
		.out_4(p_coeff[4]),
		.out_5(p_coeff[5]),
		.out_6(p_coeff[6]),
		.out_7(p_coeff[7]),
		.out_8(p_coeff[8]),
		.out_9(p_coeff[9]),
		.out_10(p_coeff[10]),
		.out_11(p_coeff[11]),
		.out_12(p_coeff[12]),
		.out_13(p_coeff[13]),
		.out_14(p_coeff[14]),
		.out_15(p_coeff[15]),
		.out_16(p_coeff[16]),
		.out_17(p_coeff[17]),
		.out_18(p_coeff[18]),
		.out_19(p_coeff[19]),
		.out_20(p_coeff[20]),
		.out_21(p_coeff[21]),
		.out_22(p_coeff[22]),
		.out_23(p_coeff[23]),
		.out_24(p_coeff[24]),
		.out_25(p_coeff[25]),
		.out_26(p_coeff[26]),
		.out_27(p_coeff[27]),
		.out_28(p_coeff[28]),
		.out_29(p_coeff[29]),
		.out_30(p_coeff[30]),
		.out_31(p_coeff[31]),

		.out_x(p_x),
		.out_y(p_y),
		.out_bypass(p_bypass),
		.out_cIdx(p_cIdx),
		.out_trafoSize(p_trafoSize),
		.out_trType(p_trType),
		
		.out_transform_skip_flag(p_transform_skip_flag),
		.out_transquant_bypass_flag(p_transquant_bypass_flag),
		
		.out_tr_stage(p_tr_stage), // row or column transform
		.out_tr_valid(p_tr_valid), // down if currently in bypass mode
		.out_new_row(p_new_row),// asseted at the beginning of new row when state=STATE_ROW
		.out_end_row(p_end_row)
    );
	 
	 adder adder(
		.acc_0(acc_out[0]),    .mem_0(mem_dout[0]),       .p_0(p_coeff[0]),		.out_0(add_out[0]),
		.acc_1(acc_out[1]),    .mem_1(mem_dout[1]),       .p_1(p_coeff[1]),		.out_1(add_out[1]),
		.acc_2(acc_out[2]),    .mem_2(mem_dout[2]),       .p_2(p_coeff[2]),		.out_2(add_out[2]),
		.acc_3(acc_out[3]),    .mem_3(mem_dout[3]),       .p_3(p_coeff[3]),		.out_3(add_out[3]),
		.acc_4(acc_out[4]),    .mem_4(mem_dout[4]),       .p_4(p_coeff[4]),		.out_4(add_out[4]),
		.acc_5(acc_out[5]),    .mem_5(mem_dout[5]),       .p_5(p_coeff[5]),		.out_5(add_out[5]),
		.acc_6(acc_out[6]),    .mem_6(mem_dout[6]),       .p_6(p_coeff[6]),		.out_6(add_out[6]),
		.acc_7(acc_out[7]),    .mem_7(mem_dout[7]),       .p_7(p_coeff[7]),		.out_7(add_out[7]),
		.acc_8(acc_out[8]),    .mem_8(mem_dout[8]),       .p_8(p_coeff[8]),		.out_8(add_out[8]),
		.acc_9(acc_out[9]),    .mem_9(mem_dout[9]),       .p_9(p_coeff[9]),		.out_9(add_out[9]),
		.acc_10(acc_out[10]),  .mem_10(mem_dout[10]),     .p_10(p_coeff[10]),	.out_10(add_out[10]),
		.acc_11(acc_out[11]),  .mem_11(mem_dout[11]),     .p_11(p_coeff[11]),	.out_11(add_out[11]),
		.acc_12(acc_out[12]),  .mem_12(mem_dout[12]),     .p_12(p_coeff[12]),	.out_12(add_out[12]),
		.acc_13(acc_out[13]),  .mem_13(mem_dout[13]),     .p_13(p_coeff[13]),	.out_13(add_out[13]),
		.acc_14(acc_out[14]),  .mem_14(mem_dout[14]),     .p_14(p_coeff[14]),	.out_14(add_out[14]),
		.acc_15(acc_out[15]),  .mem_15(mem_dout[15]),     .p_15(p_coeff[15]),	.out_15(add_out[15]),
		.acc_16(acc_out[16]),  .mem_16(mem_dout[16]),     .p_16(p_coeff[16]),	.out_16(add_out[16]),
		.acc_17(acc_out[17]),  .mem_17(mem_dout[17]),     .p_17(p_coeff[17]),	.out_17(add_out[17]),
		.acc_18(acc_out[18]),  .mem_18(mem_dout[18]),     .p_18(p_coeff[18]),	.out_18(add_out[18]),
		.acc_19(acc_out[19]),  .mem_19(mem_dout[19]),     .p_19(p_coeff[19]),	.out_19(add_out[19]),
		.acc_20(acc_out[20]),  .mem_20(mem_dout[20]),     .p_20(p_coeff[20]),	.out_20(add_out[20]),
		.acc_21(acc_out[21]),  .mem_21(mem_dout[21]),     .p_21(p_coeff[21]),	.out_21(add_out[21]),
		.acc_22(acc_out[22]),  .mem_22(mem_dout[22]),     .p_22(p_coeff[22]),	.out_22(add_out[22]),
		.acc_23(acc_out[23]),  .mem_23(mem_dout[23]),     .p_23(p_coeff[23]),	.out_23(add_out[23]),
		.acc_24(acc_out[24]),  .mem_24(mem_dout[24]),     .p_24(p_coeff[24]),	.out_24(add_out[24]),
		.acc_25(acc_out[25]),  .mem_25(mem_dout[25]),     .p_25(p_coeff[25]),	.out_25(add_out[25]),
		.acc_26(acc_out[26]),  .mem_26(mem_dout[26]),     .p_26(p_coeff[26]),	.out_26(add_out[26]),
		.acc_27(acc_out[27]),  .mem_27(mem_dout[27]),     .p_27(p_coeff[27]),	.out_27(add_out[27]),
		.acc_28(acc_out[28]),  .mem_28(mem_dout[28]),     .p_28(p_coeff[28]),	.out_28(add_out[28]),
		.acc_29(acc_out[29]),  .mem_29(mem_dout[29]),     .p_29(p_coeff[29]),	.out_29(add_out[29]),
		.acc_30(acc_out[30]),  .mem_30(mem_dout[30]),     .p_30(p_coeff[30]),	.out_30(add_out[30]),
		.acc_31(acc_out[31]),  .mem_31(mem_dout[31]),     .p_31(p_coeff[31]),	.out_31(add_out[31]),
		
		.tr_stage(p_tr_stage),
		.new_row(p_new_row)
	 );
	 
	 int_mem int_mem(
		.clk(clk),
		.clk_en(clk_en), // set this if input fifo empty or output fifo full
		.rst(rst),
		
		.trafoSize(p_trafoSize),
		.y(p_y),
		.x(p_x),
		
		.transform_skip_flag(p_transform_skip_flag),
		.transquant_bypass_flag(p_transquant_bypass_flag),
		// y,
		.x_prev(mcm_x), // used as read address for the memory
		.y_prev(mcm_y), // select among banks for output

		
		.tr_stage(p_tr_stage),
		.tr_valid(p_tr_valid),
		.new_col(p_new_row),
		
		.in_bypass(p_bypass),
		.in_0(add_out[0]),   .dout_0(mem_dout[0]),
		.in_1(add_out[1]),   .dout_1(mem_dout[1]),
		.in_2(add_out[2]),   .dout_2(mem_dout[2]),
		.in_3(add_out[3]),   .dout_3(mem_dout[3]),
		.in_4(add_out[4]),   .dout_4(mem_dout[4]),
		.in_5(add_out[5]),   .dout_5(mem_dout[5]),
		.in_6(add_out[6]),   .dout_6(mem_dout[6]),
		.in_7(add_out[7]),   .dout_7(mem_dout[7]),
		.in_8(add_out[8]),   .dout_8(mem_dout[8]),
		.in_9(add_out[9]),   .dout_9(mem_dout[9]),
		.in_10(add_out[10]), .dout_10(mem_dout[10]),
		.in_11(add_out[11]), .dout_11(mem_dout[11]),
		.in_12(add_out[12]), .dout_12(mem_dout[12]),
		.in_13(add_out[13]), .dout_13(mem_dout[13]),
		.in_14(add_out[14]), .dout_14(mem_dout[14]),
		.in_15(add_out[15]), .dout_15(mem_dout[15]),
		.in_16(add_out[16]), .dout_16(mem_dout[16]),
		.in_17(add_out[17]), .dout_17(mem_dout[17]),
		.in_18(add_out[18]), .dout_18(mem_dout[18]),
		.in_19(add_out[19]), .dout_19(mem_dout[19]),
		.in_20(add_out[20]), .dout_20(mem_dout[20]),
		.in_21(add_out[21]), .dout_21(mem_dout[21]),
		.in_22(add_out[22]), .dout_22(mem_dout[22]),
		.in_23(add_out[23]), .dout_23(mem_dout[23]),
		.in_24(add_out[24]), .dout_24(mem_dout[24]),
		.in_25(add_out[25]), .dout_25(mem_dout[25]),
		.in_26(add_out[26]), .dout_26(mem_dout[26]),
		.in_27(add_out[27]), .dout_27(mem_dout[27]),
		.in_28(add_out[28]), .dout_28(mem_dout[28]),
		.in_29(add_out[29]), .dout_29(mem_dout[29]),
		.in_30(add_out[30]), .dout_30(mem_dout[30]),
		.in_31(add_out[31]), .dout_31(mem_dout[31]),
		
		.out_coeff(mem_coeff),
		.read_x(fe_x),
		.read_y(fe_y)

    );
	 
	 row_acc row_acc(
		.clk(clk),
		.clk_en(clk_en), // set this if input fifo empty or output fifo full
		.rst(rst),
		
		.cIdx(p_cIdx),
		.trafoSize(p_trafoSize),
		.x(p_x),
		.y(p_y),
		
		.tr_stage(p_tr_stage),
		.tr_valid(p_tr_valid),
		.new_row(p_new_row),
		.end_row(p_end_row),
		.transform_skip_flag(p_transform_skip_flag),
		.transquant_bypass_flag(p_transquant_bypass_flag),
		
		.in_bypass(p_bypass),
		.in_0(add_out[0]),   .out_0(res_out[0]),  	.acc_dout_0(acc_out[0]),
		.in_1(add_out[1]),   .out_1(res_out[1]),  	.acc_dout_1(acc_out[1]),
		.in_2(add_out[2]),   .out_2(res_out[2]),  	.acc_dout_2(acc_out[2]),
		.in_3(add_out[3]),   .out_3(res_out[3]),  	.acc_dout_3(acc_out[3]),
		.in_4(add_out[4]),   .out_4(res_out[4]),  	.acc_dout_4(acc_out[4]),
		.in_5(add_out[5]),   .out_5(res_out[5]),  	.acc_dout_5(acc_out[5]),
		.in_6(add_out[6]),   .out_6(res_out[6]),  	.acc_dout_6(acc_out[6]),
		.in_7(add_out[7]),   .out_7(res_out[7]),  	.acc_dout_7(acc_out[7]),
		.in_8(add_out[8]),   .out_8(res_out[8]),  	.acc_dout_8(acc_out[8]),
		.in_9(add_out[9]),   .out_9(res_out[9]),  	.acc_dout_9(acc_out[9]),
		.in_10(add_out[10]), .out_10(res_out[10]),	.acc_dout_10(acc_out[10]),
		.in_11(add_out[11]), .out_11(res_out[11]),	.acc_dout_11(acc_out[11]),
		.in_12(add_out[12]), .out_12(res_out[12]),	.acc_dout_12(acc_out[12]),
		.in_13(add_out[13]), .out_13(res_out[13]),	.acc_dout_13(acc_out[13]),
		.in_14(add_out[14]), .out_14(res_out[14]),	.acc_dout_14(acc_out[14]),
		.in_15(add_out[15]), .out_15(res_out[15]),	.acc_dout_15(acc_out[15]),
		.in_16(add_out[16]), .out_16(res_out[16]),	.acc_dout_16(acc_out[16]),
		.in_17(add_out[17]), .out_17(res_out[17]),	.acc_dout_17(acc_out[17]),
		.in_18(add_out[18]), .out_18(res_out[18]),	.acc_dout_18(acc_out[18]),
		.in_19(add_out[19]), .out_19(res_out[19]),	.acc_dout_19(acc_out[19]),
		.in_20(add_out[20]), .out_20(res_out[20]),	.acc_dout_20(acc_out[20]),
		.in_21(add_out[21]), .out_21(res_out[21]),	.acc_dout_21(acc_out[21]),
		.in_22(add_out[22]), .out_22(res_out[22]),	.acc_dout_22(acc_out[22]),
		.in_23(add_out[23]), .out_23(res_out[23]),	.acc_dout_23(acc_out[23]),
		.in_24(add_out[24]), .out_24(res_out[24]),	.acc_dout_24(acc_out[24]),
		.in_25(add_out[25]), .out_25(res_out[25]),	.acc_dout_25(acc_out[25]),
		.in_26(add_out[26]), .out_26(res_out[26]),	.acc_dout_26(acc_out[26]),
		.in_27(add_out[27]), .out_27(res_out[27]),	.acc_dout_27(acc_out[27]),
		.in_28(add_out[28]), .out_28(res_out[28]),	.acc_dout_28(acc_out[28]),
		.in_29(add_out[29]), .out_29(res_out[29]),	.acc_dout_29(acc_out[29]),
		.in_30(add_out[30]), .out_30(res_out[30]),	.acc_dout_30(acc_out[30]),
		.in_31(add_out[31]), .out_31(res_out[31]),	.acc_dout_31(acc_out[31]),
		
		.out_row(res_row),
		.out_trafoSize(res_trafoSize),
		.out_valid(res_valid),
		.out_cIdx(res_cIdx)

    );
	 
	 reorder reorder(
		.clk(clk),
		.rst(rst),
		.clk_en(clk_en),
		
		.row(res_row),
		.trafoSize(res_trafoSize),
		.in_valid(res_valid),
		.cIdx(res_cIdx),
		
		.in_0(res_out[0]),
		.in_1(res_out[1]),
		.in_2(res_out[2]),
		.in_3(res_out[3]),
		.in_4(res_out[4]),
		.in_5(res_out[5]),
		.in_6(res_out[6]),
		.in_7(res_out[7]),
		.in_8(res_out[8]),
		.in_9(res_out[9]),
		.in_10(res_out[10]),
		.in_11(res_out[11]),
		.in_12(res_out[12]),
		.in_13(res_out[13]),
		.in_14(res_out[14]),
		.in_15(res_out[15]),
		.in_16(res_out[16]),
		.in_17(res_out[17]),
		.in_18(res_out[18]),
		.in_19(res_out[19]),
		.in_20(res_out[20]),
		.in_21(res_out[21]),
		.in_22(res_out[22]),
		.in_23(res_out[23]),
		.in_24(res_out[24]),
		.in_25(res_out[25]),
		.in_26(res_out[26]),
		.in_27(res_out[27]),
		.in_28(res_out[28]),
		.in_29(res_out[29]),
		.in_30(res_out[30]),
		.in_31(res_out[31]),

		.outY(outY_w),
		.validY(validY_w),

		.outCb(outCb_w),
		.validCb(validCb_w),

		.outCr(outCr_w),
		.validCr(validCr_w)
		
    );
endmodule
