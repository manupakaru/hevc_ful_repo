`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:05:21 04/10/2014 
// Design Name: 
// Module Name:    mcm 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mcm_33(
		clk,
		clk_en, // set this if input fifo empty or output fifo full
		rst,
		
		coeff,
		x,
		y,
		
		cIdx,
		trafoSize,
		trType,
		
		transform_skip_flag,
		transquant_bypass_flag,
		
		tr_stage, // row or column transform
		tr_valid, // down if currently in bypass mode
		new_row,// asseted at the beginning of new row when state=STATE_ROW
		end_row,
		
		x90,
		x89,
		x88,
		x87,
		x85,
		x83,
		x82,
		x80,
		x78,
		x75,
		x73,
		x70,
		x67,
		x64,
		x61,
		x57,
		x54,
		x50,
		x46,
		x43,
		x38,
		x36,
		x31,
		x25,
		x22,
		x18,
		x13,
		x9,
		x4,
		x29,
		x55,
		x74,
		x84,
		out_x,
		out_y,

		out_cIdx,
		out_trafoSize,
		out_trType,
		
		out_transform_skip_flag,
		out_transquant_bypass_flag,
		
		out_tr_stage, // row or column transform
		out_tr_valid, // down if currently in bypass mode
		out_new_row,// asseted at the beginning of new row when state=STATE_ROW
		out_end_row
    );

 // I/O
	 input clk, rst, clk_en;
	 
	input signed [15:0] coeff;
	input [4:0] x,y;
	input [1:0] cIdx;
	
	input [2:0] trafoSize;
	input trType;
	input transform_skip_flag;
	input transquant_bypass_flag;
	
	input tr_stage, tr_valid;
	input new_row, end_row; 
	 
	output reg [4:0] out_x,out_y;
	output reg [1:0] out_cIdx;
	
	output reg [2:0] out_trafoSize;
	output reg out_trType;
	output reg out_transform_skip_flag;
	output reg out_transquant_bypass_flag;
	
	output reg out_tr_stage, out_tr_valid;
	output reg out_new_row, out_end_row;
	
	// Internal
	output reg signed [23:0] x90, x89, x88, x87, x85, x83;
	output reg signed [23:0] x82, x80, x78, x75, x73, x70;
	output reg signed [23:0] x67, x64, x61, x57, x54, x50;
	output reg signed [23:0] x46, x43, x38, x36, x31, x25;
	output reg signed [23:0] x22, x18, x13, x9 , x4		;

	output reg signed [23:0] x29, x55, x74, x84;
	
	wire [23:0] a1 = coeff;
	wire [23:0] a4 = a1 << 2;
	wire [23:0] a5 = a4 + a1;
	wire [23:0] a8 = a1 << 3;
	wire [23:0] a64 = a1 << 6;
	wire [23:0] a9 = a8+a1;
	wire [23:0] a32 = a1 << 5;
	wire [23:0] a31 = a32 - a1;
	wire [23:0] a72 = a9 << 3;
	wire [23:0] a73 = a72 + a1;
	wire [23:0] a62 = a31 << 1;
	wire [23:0] a67 = a62 + a5;
	wire [23:0] a61 = a62 - a1;
	wire [23:0] a57 = a61 - a4;
	wire [23:0] a2 = a1 << 1;
	wire [23:0] a55 = a64 - a9;
	wire [23:0] a40 = a5 << 3;
	wire [23:0] a41 = a40 + a1;
	wire [23:0] a39 = a40 - a1;
	wire [23:0] a36 = a9 << 2;
	wire [23:0] a35 = a36 - a1;
	wire [23:0] a37 = a36 + a1;
	wire [23:0] a29 = a31 - a2;
	wire [23:0] a27 = a31 - a4;
	wire [23:0] a25 = a29 - a4;
	wire [23:0] a23 = a31 - a8;
	wire [23:0] a20 = a5 << 2;
	wire [23:0] a21 = a1 + a20;
	wire [23:0] a18 = a9 << 1;
	wire [23:0] a19 = a18 + a1;
	wire [23:0] a13 = a9 + a4;
	wire [23:0] a10 = a5 << 1;
	wire [23:0] a11 = a10 + a1;
	wire [23:0] a42 = a21 << 1;
	wire [23:0] a43 = a42 + a1;
	wire [23:0] a44 = a11 << 2;
	wire [23:0] a45 = a44 + a1;
	wire [23:0] a74 = a37 << 1;
	wire [23:0] a75 = a74 + a1;
	wire [23:0] a82 = a41 << 1;
	wire [23:0] a83 = a82 + a1;
	wire [23:0] a84 = a21 << 2;
	wire [23:0] a85 = a84 + a1;
	wire [23:0] a86 = a43 << 1;
	
	wire [23:0] a87 = a64 + a23; //wire [23:0] a87 = a86 + a1;
	wire [23:0] a88 = a11 << 3;
	wire [23:0] a89 = a88 + a1;
	wire [23:0] a90 = a45 << 1;
	wire [23:0] a80 = a5 << 4;
	wire [23:0] a78 = a39 << 1;
	wire [23:0] a70 = a35 << 1;
	
	wire [23:0] a54 = a27 << 1;
	wire [23:0] a50 = a25 << 1;
	wire [23:0] a46 = a23 << 1;
	wire [23:0] a38 = a19 << 1;
	wire [23:0] a22 = a11 << 1;
	
	
	always @(posedge clk) begin
		if (clk_en) begin
			out_x <= x;
			out_y <= y;
			out_cIdx <= cIdx;
			out_trafoSize <= trafoSize;
			out_trType <= trType;
			out_transform_skip_flag <= transform_skip_flag;
			out_transquant_bypass_flag <= transquant_bypass_flag;
			out_tr_stage <= tr_stage;
			out_tr_valid <= tr_valid;
			out_new_row <= new_row;
			out_end_row <= end_row;
			if (tr_valid) begin
				x90 <= a90;
				x89 <= a89;
				x88 <= a88;
				x87 <= a87;
				x85 <= a85;
				x83 <= a83;
				x82 <= a82;
				x80 <= a80;
				x78 <= a78;
				x75 <= a75;
				x73 <= a73;
				x70 <= a70;
				x67 <= a67;
				x64 <= a64;
				x61 <= a61;
				x57 <= a57;
				x54 <= a54;
				x50 <= a50;
				x46 <= a46;
				x43 <= a43;
				x38 <= a38;
				x36 <= a36;
				x31 <= a31;
				x25 <= a25;
				x22 <= a22;
				x18 <= a18;
				x13 <= a13;
				x9  <=  a9;
				x4  <=  a4;
				x29 <= a29;
				x55 <= a55;
				x74 <= a74;
				x84 <= a84;
			end
		end
	end
	
	
endmodule
