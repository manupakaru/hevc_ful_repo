`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:52:26 05/01/2014 
// Design Name: 
// Module Name:    adder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module adder(
		input signed[31:0] acc_0,	acc_1,	acc_2,	acc_3,	acc_4,	acc_5,	acc_6,	acc_7,	acc_8,	acc_9,	acc_10,	acc_11,	acc_12,	acc_13,	acc_14,	acc_15,	acc_16,	acc_17,	acc_18,	acc_19,	acc_20,	acc_21,	acc_22,	acc_23,	acc_24,	acc_25,	acc_26,	acc_27,	acc_28,	acc_29,	acc_30,	acc_31,
		input signed[31:0] mem_0,	mem_1,	mem_2,	mem_3,	mem_4,	mem_5,	mem_6,	mem_7,	mem_8,	mem_9,	mem_10,	mem_11,	mem_12,	mem_13,	mem_14,	mem_15,	mem_16,	mem_17,	mem_18,	mem_19,	mem_20,	mem_21,	mem_22,	mem_23,	mem_24,	mem_25,	mem_26,	mem_27,	mem_28,	mem_29,	mem_30,	mem_31,
		
		input signed [23:0] p_0,	p_1,	p_2,	p_3,	p_4,	p_5,	p_6,	p_7,	p_8,	p_9,	p_10,	p_11,	p_12,	p_13,	p_14,	p_15,	p_16,	p_17,	p_18,	p_19,	p_20,	p_21,	p_22,	p_23,	p_24,	p_25,	p_26,	p_27,	p_28,	p_29,	p_30,	p_31,
		
		input tr_stage,
		input new_row,

		output reg signed [31:0] out_0,	out_1,	out_2,	out_3,	out_4,	out_5,	out_6,	out_7,	out_8,	out_9,	out_10,	out_11,	out_12,	out_13,	out_14,	out_15,	out_16,	out_17,	out_18,	out_19,	out_20,	out_21,	out_22,	out_23,	out_24,	out_25,	out_26,	out_27,	out_28,	out_29,	out_30,	out_31

    );

	
	always @(*) begin
		if (new_row) begin
			out_0 = p_0;
			out_1 = p_1;
			out_2 = p_2;
			out_3 = p_3;
			out_4 = p_4;
			out_5 = p_5;
			out_6 = p_6;
			out_7 = p_7;
			out_8 = p_8;
			out_9 = p_9;
			out_10 = p_10;
			out_11 = p_11;
			out_12 = p_12;
			out_13 = p_13;
			out_14 = p_14;
			out_15 = p_15;
			out_16 = p_16;
			out_17 = p_17;
			out_18 = p_18;
			out_19 = p_19;
			out_20 = p_20;
			out_21 = p_21;
			out_22 = p_22;
			out_23 = p_23;
			out_24 = p_24;
			out_25 = p_25;
			out_26 = p_26;
			out_27 = p_27;
			out_28 = p_28;
			out_29 = p_29;
			out_30 = p_30;
			out_31 = p_31;
		end else begin
			out_0 = p_0 +  (tr_stage ? acc_0 : mem_0);
			out_1 = p_1 +  (tr_stage ? acc_1 : mem_1);
			out_2 = p_2 +  (tr_stage ? acc_2 : mem_2);
			out_3 = p_3 +  (tr_stage ? acc_3 : mem_3);
			out_4 = p_4 +  (tr_stage ? acc_4 : mem_4);
			out_5 = p_5 +  (tr_stage ? acc_5 : mem_5);
			out_6 = p_6 +  (tr_stage ? acc_6 : mem_6);
			out_7 = p_7 +  (tr_stage ? acc_7 : mem_7);
			out_8 = p_8 +  (tr_stage ? acc_8 : mem_8);
			out_9 = p_9 +  (tr_stage ? acc_9 : mem_9);
			out_10 = p_10 +  (tr_stage ? acc_10 : mem_10);
			out_11 = p_11 +  (tr_stage ? acc_11 : mem_11);
			out_12 = p_12 +  (tr_stage ? acc_12 : mem_12);
			out_13 = p_13 +  (tr_stage ? acc_13 : mem_13);
			out_14 = p_14 +  (tr_stage ? acc_14 : mem_14);
			out_15 = p_15 +  (tr_stage ? acc_15 : mem_15);
			out_16 = p_16 +  (tr_stage ? acc_16 : mem_16);
			out_17 = p_17 +  (tr_stage ? acc_17 : mem_17);
			out_18 = p_18 +  (tr_stage ? acc_18 : mem_18);
			out_19 = p_19 +  (tr_stage ? acc_19 : mem_19);
			out_20 = p_20 +  (tr_stage ? acc_20 : mem_20);
			out_21 = p_21 +  (tr_stage ? acc_21 : mem_21);
			out_22 = p_22 +  (tr_stage ? acc_22 : mem_22);
			out_23 = p_23 +  (tr_stage ? acc_23 : mem_23);
			out_24 = p_24 +  (tr_stage ? acc_24 : mem_24);
			out_25 = p_25 +  (tr_stage ? acc_25 : mem_25);
			out_26 = p_26 +  (tr_stage ? acc_26 : mem_26);
			out_27 = p_27 +  (tr_stage ? acc_27 : mem_27);
			out_28 = p_28 +  (tr_stage ? acc_28 : mem_28);
			out_29 = p_29 +  (tr_stage ? acc_29 : mem_29);
			out_30 = p_30 +  (tr_stage ? acc_30 : mem_30);
			out_31 = p_31 +  (tr_stage ? acc_31 : mem_31);

		end
	
	end
	
endmodule
