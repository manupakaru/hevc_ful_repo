`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:34:59 04/16/2014 
// Design Name: 
// Module Name:    bank_col 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module bank_row(
		clk,
		rd_addr,
		wr_addr,
		din,
		rd_en,
		wr_en,
		dout
    );
	 
	 
	 reg [31:0] mem [0:31];
	 
	 input clk;
	 input [4:0] rd_addr, wr_addr;
	 input [31:0] din;
	 input wr_en, rd_en;
	 
	 output reg [31:0] dout;
	 
	 always @(posedge clk) begin
		
		if (wr_en) begin
			mem[wr_addr] <= din;
			if (rd_en) begin
				if (rd_addr == wr_addr) begin
					dout <= din;
				end else begin
					dout <= mem[rd_addr];
				end
			end
		end
		else begin
			if (rd_en) begin
				dout <= mem[rd_addr];
			end
		end
		//dout <= ( (rd_addr == wr_addr) && wr_en) ? din : mem[rd_addr];
	 end


endmodule
