`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:03:24 04/18/2014 
// Design Name: 
// Module Name:    reorder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module reorder(
		clk,
		rst,
		clk_en,
		
		row,
		trafoSize,
		in_valid,
		cIdx,
		
		in_0,
		in_1,
		in_2,
		in_3,
		in_4,
		in_5,
		in_6,
		in_7,
		in_8,
		in_9,
		in_10,
		in_11,
		in_12,
		in_13,
		in_14,
		in_15,
		in_16,
		in_17,
		in_18,
		in_19,
		in_20,
		in_21,
		in_22,
		in_23,
		in_24,
		in_25,
		in_26,
		in_27,
		in_28,
		in_29,
		in_30,
		in_31,

		outY,
		validY,

		outCb,
		validCb,

		outCr,
		validCr
		
    );

	input clk, rst, clk_en;
	input [4:0] row;
	input [2:0] trafoSize;
	input in_valid;
	input [1:0] cIdx;
	
	input [8:0] in_0 , in_1 , in_2 , in_3, in_4 , in_5 , in_6 , in_7, in_8 , in_9 , in_10 , in_11, in_12 , in_13 , in_14 , in_15, in_16 , in_17 , in_18 , in_19, in_20 , in_21 , in_22 , in_23, in_24 , in_25 , in_26 , in_27, in_28 , in_29 , in_30 , in_31;
	
	output reg [143:0] outY, outCb, outCr;
	output reg validY, validCb, validCr;
	
	// internal
	// line buffer size increased to 32x32 so that no delay is required in previous modules.
	reg [8:0] line_buf [0:31][0:31];
	reg [1:0] state;
	reg [2:0] push_row, push_col; 
	reg [1:0] push_cIdx; // registered
	reg [2:0] push_trafoSize; 
	
	reg [2:0] max_block_no;
	always @(*) begin
		case (push_trafoSize) 
			2 : max_block_no = 3'd0;
			3 : max_block_no = 3'd1;
			4 : max_block_no = 3'd3;
			5 : max_block_no = 3'd7;
		  default : max_block_no = 3'dx;
		endcase
	end
	
	localparam STATE_WAIT = 2'b00;
	localparam STATE_RASTER = 2'b01;
	localparam STATE_ZSCAN = 2'b10;
	
	always @(posedge clk) begin
		if (!rst) begin
			state <= STATE_WAIT;
		end else begin
			if (clk_en) begin
				if (in_valid) begin
					line_buf[ 0][row[4:0]] <= in_0 ; line_buf[ 1][row[4:0]] <= in_1 ; line_buf[ 2][row[4:0]] <= in_2 ; line_buf[ 3][row[4:0]] <= in_3;
					line_buf[ 4][row[4:0]] <= in_4 ; line_buf[ 5][row[4:0]] <= in_5 ; line_buf[ 6][row[4:0]] <= in_6 ; line_buf[ 7][row[4:0]] <= in_7;
					line_buf[ 8][row[4:0]] <= in_8 ; line_buf[ 9][row[4:0]] <= in_9 ; line_buf[10][row[4:0]] <= in_10; line_buf[11][row[4:0]] <= in_11;
					line_buf[12][row[4:0]] <= in_12; line_buf[13][row[4:0]] <= in_13; line_buf[14][row[4:0]] <= in_14; line_buf[15][row[4:0]] <= in_15;
				  line_buf[16][row[4:0]] <= in_16; line_buf[17][row[4:0]] <= in_17; line_buf[18][row[4:0]] <= in_18; line_buf[19][row[4:0]] <= in_19;
					line_buf[20][row[4:0]] <= in_20; line_buf[21][row[4:0]] <= in_21; line_buf[22][row[4:0]] <= in_22; line_buf[23][row[4:0]] <= in_23;
					line_buf[24][row[4:0]] <= in_24; line_buf[25][row[4:0]] <= in_25; line_buf[26][row[4:0]] <= in_26; line_buf[27][row[4:0]] <= in_27;
					line_buf[28][row[4:0]] <= in_28; line_buf[29][row[4:0]] <= in_29; line_buf[30][row[4:0]] <= in_30; line_buf[31][row[4:0]] <= in_31;
				
				end 	
				case (state)
					STATE_WAIT: begin
						if (in_valid) begin
							if (cIdx != 2'b00 || trafoSize == 3'd2) begin
								if (row[1:0] == 2'd3) begin
									state <= STATE_RASTER;
									push_row <= row[4:2];
									push_col <= 3'd0;
								end
							end else begin
								if (row[2:0] == 3'd7) begin
									state <= STATE_ZSCAN;
									push_row <= {row[4:3], 1'b0};
									push_col <= 3'd0;
								end
							end
							push_cIdx <= cIdx;
							push_trafoSize <= trafoSize;
						end
					end
					STATE_RASTER: begin
						if (push_col == max_block_no) begin // // end of row
							
							push_row <= push_row + 1'b1;
							push_col <= 3'd0;
							// if next row is available stay un the same state
							if (in_valid) begin
								if (row[1:0] == 2'd3 && row[4:2] != push_row) begin
									state <= STATE_RASTER;
								end else begin
								  state <= STATE_WAIT;
								end
							end else begin
								state <= STATE_WAIT;
							end
						end else begin
							push_col <= push_col + 1'b1;
						end
					end
					STATE_ZSCAN: begin
						if (push_row[0] == 3'd1 && push_col == max_block_no) begin
							if (in_valid) begin
								if (row[2:0] == 3'd7 && row[4:3] != push_row[2:1] ) begin
									state <= STATE_ZSCAN;
									//push_row <= {row[4:3], 1'b0};
									//push_col <= 3'd0;
								end else begin
								  state <= STATE_WAIT;
								end
							end else begin
								state <= STATE_WAIT;
							end
						end 
						if (push_col[0] == 1'b1) begin
							push_row[0] <= ~push_row[0];
						end
						
						// col
						push_col[0] <= ~ push_col[0];
						if (push_row[0] == 1'b1 && push_col[0] == 1'b1) begin					
							if (push_col == max_block_no) begin
							   push_row[2:1] <= push_row[2:1] + 1'b1;
							   push_col <= 3'd0;
							end else begin
							   push_col[2:1] <= push_col[2:1] + 1'b1;
							end							  
						end
					end
					
				endcase
			end
		end
	end
	
	reg [8:0] out [0:3][0:3]; //i-row, j-col
	reg [143:0] out_wire;
	
	integer i,j;
	always @(*) begin
		validY  = 1'b0;
		validCb = 1'b0;
		validCr = 1'b0;
		outY   = 144'dx;
		outCb  = 144'dx;
		outCr  = 144'dx;
		for (i=0;i<4;i=i+1) begin
			for (j=0;j<4;j=j+1) begin
				out[i][j] = line_buf[ {push_col, j[1:0]} ][ {push_row, i[1:0]} ];
				
				out_wire[143:135] = line_buf[ {push_col, 2'b00} ][ {push_row, 2'b00} ];
				out_wire[134:126] = line_buf[ {push_col, 2'b01} ][ {push_row, 2'b00} ];
				out_wire[125:117] = line_buf[ {push_col, 2'b10} ][ {push_row, 2'b00} ];
				out_wire[116:108] = line_buf[ {push_col, 2'b11} ][ {push_row, 2'b00} ];
				
				out_wire[107: 99] = line_buf[ {push_col, 2'b00} ][ {push_row, 2'b01} ];
				out_wire[ 98: 90] = line_buf[ {push_col, 2'b01} ][ {push_row, 2'b01} ];
				out_wire[ 89: 81] = line_buf[ {push_col, 2'b10} ][ {push_row, 2'b01} ];
				out_wire[ 80: 72] = line_buf[ {push_col, 2'b11} ][ {push_row, 2'b01} ];
				
				out_wire[ 71: 63] = line_buf[ {push_col, 2'b00} ][ {push_row, 2'b10} ];
				out_wire[ 62: 54] = line_buf[ {push_col, 2'b01} ][ {push_row, 2'b10} ];
				out_wire[ 53: 45] = line_buf[ {push_col, 2'b10} ][ {push_row, 2'b10} ];
				out_wire[ 44: 36] = line_buf[ {push_col, 2'b11} ][ {push_row, 2'b10} ];
				
				out_wire[ 35: 27] = line_buf[ {push_col, 2'b00} ][ {push_row, 2'b11} ];
				out_wire[ 26: 18] = line_buf[ {push_col, 2'b01} ][ {push_row, 2'b11} ];
				out_wire[ 17:  9] = line_buf[ {push_col, 2'b10} ][ {push_row, 2'b11} ];
				out_wire[  8:  0] = line_buf[ {push_col, 2'b11} ][ {push_row, 2'b11} ];				
			end
		end

		if (state == STATE_RASTER || state == STATE_ZSCAN) begin
			if (clk_en) begin
				case (push_cIdx) 
					0: validY = 1'b1;
					1: validCb = 1'b1;
					2: validCr = 1'b1;
				endcase
				case (push_cIdx) 
					0: outY = out_wire;
					1: outCb = out_wire;
					2: outCr = out_wire;
				endcase
			end
		end
		
	end
	

endmodule
