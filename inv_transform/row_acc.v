`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:36:52 04/17/2014 
// Design Name: 
// Module Name:    row_acc 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module row_acc(
		clk,
		clk_en, // set this if input fifo empty or output fifo full
		rst,
	
		x,
		y,
		
		cIdx,
		trafoSize,
		
		tr_stage, // row or column transform
		tr_valid, // down if currently in bypass mode
		new_row,// asseted at the beginning of new row when state=STATE_ROW
		end_row,
		
		transform_skip_flag,
		transquant_bypass_flag,
	
		in_bypass,
		in_0,
		in_1,
		in_2,
		in_3,
		in_4,
		in_5,
		in_6,
		in_7,
		in_8,
		in_9,
		in_10,
		in_11,
		in_12,
		in_13,
		in_14,
		in_15,
		in_16,
		in_17,
		in_18,
		in_19,
		in_20,
		in_21,
		in_22,
		in_23,
		in_24,
		in_25,
		in_26,
		in_27,
		in_28,
		in_29,
		in_30,
		in_31,
		
		out_0,   acc_dout_0,
		out_1,   acc_dout_1,
		out_2,   acc_dout_2,
		out_3,   acc_dout_3,
		out_4,   acc_dout_4,
		out_5,   acc_dout_5,
		out_6,   acc_dout_6,
		out_7,   acc_dout_7,
		out_8,   acc_dout_8,
		out_9,   acc_dout_9,
		out_10,  acc_dout_10,
		out_11,  acc_dout_11,
		out_12,  acc_dout_12,
		out_13,  acc_dout_13,
		out_14,  acc_dout_14,
		out_15,  acc_dout_15,
		out_16,  acc_dout_16,
		out_17,  acc_dout_17,
		out_18,  acc_dout_18,
		out_19,  acc_dout_19,
		out_20,  acc_dout_20,
		out_21,  acc_dout_21,
		out_22,  acc_dout_22,
		out_23,  acc_dout_23,
		out_24,  acc_dout_24,
		out_25,  acc_dout_25,
		out_26,  acc_dout_26,
		out_27,  acc_dout_27,
		out_28,  acc_dout_28,
		out_29,  acc_dout_29,
		out_30,  acc_dout_30,
		out_31,  acc_dout_31,
		
		out_row,
		out_trafoSize,
		out_valid,
		out_cIdx

    );
	 
	input clk, rst, clk_en;
	 
	input [4:0] x, y;
	input [1:0] cIdx;
	
	input [2:0] trafoSize;
	
	input tr_stage, tr_valid;
	input new_row, end_row; 
	
	input transform_skip_flag;
	input transquant_bypass_flag;

	input signed [23:0] in_bypass;
	input signed [31:0]  in_0 , in_1 , in_2 , in_3, in_4 , in_5 , in_6 , in_7, in_8 , in_9 , in_10 , in_11, in_12 , in_13 , in_14 , in_15, in_16 , in_17 , in_18 , in_19, in_20 , in_21 , in_22 , in_23, in_24 , in_25 , in_26 , in_27, in_28 , in_29 , in_30 , in_31;
	
	output signed [8:0]  out_0 , out_1 , out_2 , out_3, out_4 , out_5 , out_6 , out_7, out_8 , out_9 , out_10 , out_11, out_12 , out_13 , out_14 , out_15, out_16 , out_17 , out_18 , out_19, out_20 , out_21 , out_22 , out_23, out_24 , out_25 , out_26 , out_27, out_28 , out_29 , out_30 , out_31;
	output signed [31:0]  acc_dout_0 , acc_dout_1 , acc_dout_2 , acc_dout_3, acc_dout_4 , acc_dout_5 , acc_dout_6 , acc_dout_7, acc_dout_8 , acc_dout_9 , acc_dout_10 , acc_dout_11, acc_dout_12 , acc_dout_13 , acc_dout_14 , acc_dout_15, acc_dout_16 , acc_dout_17 , acc_dout_18 , acc_dout_19, acc_dout_20 , acc_dout_21 , acc_dout_22 , acc_dout_23, acc_dout_24 , acc_dout_25 , acc_dout_26 , acc_dout_27, acc_dout_28 , acc_dout_29 , acc_dout_30 , acc_dout_31;
	
	output reg [4:0] out_row;
	output reg [2:0] out_trafoSize;
	output reg out_valid;
	output reg [1:0] out_cIdx;
	
	// internal
	reg signed [31:0] acc [0:31] ;
	
	// input packing
	wire signed [31:0] in [0:31];
	reg signed [8:0] out [0:31];
	
	assign {in[ 0] , in[ 1] , in[ 2] , in[ 3], in[ 4] , in[ 5] , in[ 6] , in[ 7] } = { in_0 ,  in_1 ,  in_2 ,  in_3,  in_4 ,  in_5 ,  in_6 ,  in_7};
	assign {in[ 8] , in[ 9] , in[10] , in[11], in[12] , in[13] , in[14] , in[15] } = { in_8 ,  in_9 , in_10 , in_11, in_12 , in_13 , in_14 , in_15};
	assign {in[16] , in[17] , in[18] , in[19], in[20] , in[21] , in[22] , in[23] } = {in_16 , in_17 , in_18 , in_19, in_20 , in_21 , in_22 , in_23};
	assign {in[24] , in[25] , in[26] , in[27], in[28] , in[29] , in[30] , in[31] } = {in_24 , in_25 , in_26 , in_27, in_28 , in_29 , in_30 , in_31};

	assign { out_0 ,  out_1 ,  out_2 ,  out_3,  out_4 ,  out_5 ,  out_6 ,  out_7} = {out[ 0] , out[ 1] , out[ 2] , out[ 3], out[ 4] , out[ 5] , out[ 6] , out[ 7] };
	assign { out_8 ,  out_9 , out_10 , out_11, out_12 , out_13 , out_14 , out_15} = {out[ 8] , out[ 9] , out[10] , out[11], out[12] , out[13] , out[14] , out[15] }; 
	assign {out_16 , out_17 , out_18 , out_19, out_20 , out_21 , out_22 , out_23} = {out[16] , out[17] , out[18] , out[19], out[20] , out[21] , out[22] , out[23] }; 
	assign {out_24 , out_25 , out_26 , out_27, out_28 , out_29 , out_30 , out_31} = {out[24] , out[25] , out[26] , out[27], out[28] , out[29] , out[30] , out[31] }; 

	assign { acc_dout_0 ,  acc_dout_1 ,  acc_dout_2 ,  acc_dout_3,  acc_dout_4 ,  acc_dout_5 ,  acc_dout_6 ,  acc_dout_7} = {acc[ 0] , acc[ 1] , acc[ 2] , acc[ 3], acc[ 4] , acc[ 5] , acc[ 6] , acc[ 7] };
   assign { acc_dout_8 ,  acc_dout_9 , acc_dout_10 , acc_dout_11, acc_dout_12 , acc_dout_13 , acc_dout_14 , acc_dout_15} = {acc[ 8] , acc[ 9] , acc[10] , acc[11], acc[12] , acc[13] , acc[14] , acc[15] };
   assign {acc_dout_16 , acc_dout_17 , acc_dout_18 , acc_dout_19, acc_dout_20 , acc_dout_21 , acc_dout_22 , acc_dout_23} = {acc[16] , acc[17] , acc[18] , acc[19], acc[20] , acc[21] , acc[22] , acc[23] };
   assign {acc_dout_24 , acc_dout_25 , acc_dout_26 , acc_dout_27, acc_dout_28 , acc_dout_29 , acc_dout_30 , acc_dout_31} = {acc[24] , acc[25] , acc[26] , acc[27], acc[28] , acc[29] , acc[30] , acc[31] };
	
	
	integer i;
	always @(posedge clk) begin
		if (~rst) begin
			out_valid <= 1'b0;
		end else begin
			if (clk_en) begin
				if (tr_valid && (tr_stage == 1'b1)	) begin
					for (i=0;i<32;i=i+1) begin
						if (transform_skip_flag | transquant_bypass_flag) begin
						  if (new_row) begin
						    acc[i] <= (x==i) ? in_bypass : 32'd0;
						  end else begin
							  if (x==i) begin
								   acc[i] <= in_bypass;
							  end
							end
						end else begin
							acc[i] <= in[i];
//							if (new_row) begin
//								acc[i] <= in[i];
//							end else begin
//								acc[i] <= acc[i] + in[i];
//							end
						end
					end
				end
				out_row <= y;
				out_trafoSize <= trafoSize;
				out_valid <= end_row;
				out_cIdx <= cIdx;
			end
		end
	end
	
	reg signed [31:0] round [0:31];
	always @ (*) begin
		for (i=0;i<32;i=i+1) begin
			round[i] = acc[i] + 2048;
			out[i] = round[i][20:12];
		end
	end
	//always 
	
endmodule
