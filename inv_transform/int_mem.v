`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:19:41 04/16/2014 
// Design Name: 
// Module Name:    int_mem 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module int_mem(
		clk,
		rst,
		clk_en,
		
		trafoSize,
		x,
	   y,
		x_prev, // used as read address for the memory
		y_prev, // select among banks for output

		transform_skip_flag,
		transquant_bypass_flag,
		
		tr_stage,
		tr_valid,
		new_col,
		
		in_bypass,
		in_0,  dout_0,
		in_1,  dout_1,
		in_2,  dout_2,
		in_3,  dout_3,
		in_4,  dout_4,
		in_5,  dout_5,
		in_6,  dout_6,
		in_7,  dout_7,
		in_8,  dout_8,
		in_9,  dout_9,
		in_10, dout_10,
		in_11, dout_11,
		in_12, dout_12,
		in_13, dout_13,
		in_14, dout_14,
		in_15, dout_15,
		in_16, dout_16,
		in_17, dout_17,
		in_18, dout_18,
		in_19, dout_19,
		in_20, dout_20,
		in_21, dout_21,
		in_22, dout_22,
		in_23, dout_23,
		in_24, dout_24,
		in_25, dout_25,
		in_26, dout_26,
		in_27, dout_27,
		in_28, dout_28,
		in_29, dout_29,
		in_30, dout_30,
		in_31, dout_31,
	
		out_coeff,
		
		read_x, // if reading for the second stage the read addrss should be taken from fe stage
		read_y

    );

	input clk, rst, clk_en;
	 
	input [2:0] trafoSize;
	input [4:0] x, y, x_prev, y_prev;
	input [4:0] read_x, read_y;
	
	input tr_stage, tr_valid;
	input new_col;
	input transform_skip_flag;
	input	transquant_bypass_flag;
	
	input signed [23:0] in_bypass;
	input signed [31:0]  in_0 , in_1 , in_2 , in_3, in_4 , in_5 , in_6 , in_7, in_8 , in_9 , in_10 , in_11, in_12 , in_13 , in_14 , in_15, in_16 , in_17 , in_18 , in_19, in_20 , in_21 , in_22 , in_23, in_24 , in_25 , in_26 , in_27, in_28 , in_29 , in_30 , in_31;
	output signed [31:0]  dout_0 , dout_1 , dout_2 , dout_3, dout_4 , dout_5 , dout_6 , dout_7, dout_8 , dout_9 , dout_10 , dout_11, dout_12 , dout_13 , dout_14 , dout_15, dout_16 , dout_17 , dout_18 , dout_19, dout_20 , dout_21 , dout_22 , dout_23, dout_24 , dout_25 , dout_26 , dout_27, dout_28 , dout_29 , dout_30 , dout_31;

	output reg [15:0] out_coeff; // combinational
	
	// internal 
	
	wire signed [31:0] dout [0:31];
	reg [31:0] din [0:31];
	reg wr_en [0:31];
	
	reg [4:0] x_D1, y_D1;
	reg xy_D1_valid;
	reg new_coeff;
	
	wire signed [31:0] in [0:31];
	
	wire [4:0] rd_addr; // of banks
	assign rd_addr = tr_stage == 1'b1 ? read_x: x_prev;
	
	reg [4:0] read_y_D1;
	always @(posedge clk) begin
		if (clk_en) begin
			read_y_D1 <= read_y;
		end
	end
	always @(posedge clk) begin
		if (!rst) begin
			xy_D1_valid <= 1'b0;
			x_D1 <= 5'd0;
			y_D1 <= 5'd0;
		end else begin
			if (clk_en) begin
				x_D1 <= x;
				y_D1 <= y;
				if (tr_stage == 1'b1) xy_D1_valid <= 1'b0;
				else if (tr_valid) xy_D1_valid <= 1'b1;
			end
		end
	end
	always @(*) begin
		if (!xy_D1_valid) 
			new_coeff = 1'b1;
		else 
			new_coeff = !((x_D1 ==x) & (y_D1==y));
	end
	
	assign {in[ 0] , in[ 1] , in[ 2] , in[ 3], in[ 4] , in[ 5] , in[ 6] , in[ 7] } = { in_0 ,  in_1 ,  in_2 ,  in_3,  in_4 ,  in_5 ,  in_6 ,  in_7};
	assign {in[ 8] , in[ 9] , in[10] , in[11], in[12] , in[13] , in[14] , in[15] } = { in_8 ,  in_9 , in_10 , in_11, in_12 , in_13 , in_14 , in_15};
	assign {in[16] , in[17] , in[18] , in[19], in[20] , in[21] , in[22] , in[23] } = {in_16 , in_17 , in_18 , in_19, in_20 , in_21 , in_22 , in_23};
	assign {in[24] , in[25] , in[26] , in[27], in[28] , in[29] , in[30] , in[31] } = {in_24 , in_25 , in_26 , in_27, in_28 , in_29 , in_30 , in_31};
	
	assign {dout_0  , dout_1  , dout_2  , dout_3 , dout_4  , dout_5  , dout_6  , dout_7  } ={dout[ 0] , dout[ 1] , dout[ 2] , dout[ 3], dout[ 4] , dout[ 5] , dout[ 6] , dout[ 7]};
	assign {dout_8  , dout_9  , dout_10 , dout_11, dout_12 , dout_13 , dout_14 , dout_15 } ={dout[ 8] , dout[ 9] , dout[10] , dout[11], dout[12] , dout[13] , dout[14] , dout[15]};
	assign {dout_16 , dout_17 , dout_18 , dout_19, dout_20 , dout_21 , dout_22 , dout_23 } ={dout[16] , dout[17] , dout[18] , dout[19], dout[20] , dout[21] , dout[22] , dout[23]};
	assign {dout_24 , dout_25 , dout_26 , dout_27, dout_28 , dout_29 , dout_30 , dout_31 } ={dout[24] , dout[25] , dout[26] , dout[27], dout[28] , dout[29] , dout[30] , dout[31]};
	
	
	wire signed [31:0] out_round;
	wire signed [24:0] out_round_shifted;
	assign out_round = tr_stage == 1'b1 ? dout[read_y_D1] + 7'd64 :dout[y_prev]+ 7'd64;
	assign out_round_shifted = out_round[31:7];
	always @(*) begin
		// do clipping
		if (out_round_shifted > 32767) begin
			out_coeff = 16'd32767;
		end else if (out_round_shifted < -32768) begin
			out_coeff = -16'd32768;
		end else begin
			out_coeff = out_round_shifted[15:0];
		end
	end
	
	genvar i;
	generate 
		for (i=0;i<32;i=i+1) begin : bank
			bank_row bank_i(
				.clk(clk),
				.rd_addr(rd_addr),
				.wr_addr(x),
				.din(din[i]),
				.rd_en(clk_en),
				.wr_en(wr_en[i]),
				.dout(dout[i])
			);
		end
	
	endgenerate
	
	integer j;
	always @(*) begin
		for (j=0;j<32;j=j+1) begin
			if (!transform_skip_flag && !transquant_bypass_flag) begin
				if (tr_valid && new_coeff && tr_stage == 1'b0) begin
					case (trafoSize) 
						2: wr_en[j] = (j<4);
						3: wr_en[j] = (j<8);
						4: wr_en[j] = (j<16);
						5: wr_en[j] = 1'b1;
						default : wr_en[j] = 1'b0;
					endcase
				end else begin
					wr_en[j] = 1'b0;
				end
				din[j] = in[j];
//				if (new_col) begin 
//					din[j] = in[j];
//				end else begin
//					din[j] = dout[j] + in[j];
//				end
			end else begin
			  if (tr_valid && tr_stage == 1'b0) begin
				  if (new_col) begin
					   if (j!=y) begin
					 	   din[j] = 32'd0;
					   end else begin
					     din[j] = in_bypass;
    					   end
    					   wr_en[j] = 1'b1;
				  end else begin
    					   wr_en[j] = (j==y);
    					   din[j] = in_bypass;
				  end			
				end else begin
				    wr_en[j] = 1'b0;
				    din[j] = 32'dx;
				end
			end
		end
		
	end

endmodule
