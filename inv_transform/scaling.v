`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:25:43 04/10/2014 
// Design Name: 
// Module Name:    scaling 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module scaling(
		clk,
		clk_en, // set this if input fifo empty or output fifo full
		rst,
		
		coeff,
		coeff_mem,
		x,
		y,
		
		QP,
		cIdx,
		trafoSize,
		trType,
		
		transform_skip_flag,
		transquant_bypass_flag,
		
		tr_stage, // row or column transform
		tr_valid, // down if currently in bypass mode
		new_row,// asseted at the beginning of new row when state=STATE_ROW
		end_row,
		
		out_coeff,
		out_x,
		out_y,
		
		out_cIdx,
		out_trafoSize,
		out_trType,
		
		out_transform_skip_flag,
		out_transquant_bypass_flag,
		
		out_tr_stage, // row or column transform
		out_tr_valid, // down if currently in bypass mode
		out_new_row,// asseted at the beginning of new row when state=STATE_ROW
		out_end_row

    );
	 
	 // I/O
	 input clk, rst, clk_en;
	 
	input signed [15:0] coeff, coeff_mem;
	input [4:0] x,y;
	
	input [5:0] QP;
	input [1:0] cIdx;
	
	input [2:0] trafoSize;
	input trType;
	input transform_skip_flag;
	input transquant_bypass_flag;
	
	input tr_stage, tr_valid;
	input new_row, end_row; 
	 
	output reg [15:0] out_coeff;
	output reg [4:0] out_x,out_y;
	output reg [1:0] out_cIdx;
	
	output reg [2:0] out_trafoSize;
	output reg out_trType;
	output reg out_transform_skip_flag;
	output reg out_transquant_bypass_flag;
	
	output reg out_tr_stage, out_tr_valid;
	output reg out_new_row, out_end_row;
	
	// Stage 1
	// wires  
	reg [3:0] QP_by6; //Comb
	reg [2:0] QP_mod6; //Comb
	// registers
	reg [4:0] D1_x, D1_y;
	reg [1:0] D1_cIdx;
	
	reg [2:0] D1_trafoSize;
	reg D1_trType;
	reg D1_transform_skip_flag;
	reg D1_transquant_bypass_flag;
	
	reg D1_tr_stage, D1_tr_valid;
	reg D1_new_row, D1_end_row;
	reg [15:0] D1_in_coeff;
	
	reg [3:0] D1_QP_by6;
	reg signed [23:0] QP_levelScale_mult;
	always @(*) begin
		QP_by6 = (QP / 6)%16;
		QP_mod6 = QP % 6;
	end
	always @(posedge clk) begin
		if (clk_en) begin
			D1_x <= x;
			D1_y <= y;
			D1_cIdx <= cIdx;
			D1_trafoSize <= trafoSize;
			D1_trType <= trType;
			D1_transform_skip_flag <= transform_skip_flag;
			D1_transquant_bypass_flag <= transquant_bypass_flag;
			D1_tr_stage <= tr_stage;
			D1_tr_valid <= tr_valid;
			D1_new_row <= new_row;
			D1_end_row <= end_row;
			D1_in_coeff <= coeff;
			case (QP_mod6)
				0: QP_levelScale_mult <= coeff * 40;
				1: QP_levelScale_mult <= coeff * 45;
				2: QP_levelScale_mult <= coeff * 51;
				3: QP_levelScale_mult <= coeff * 57;
				4: QP_levelScale_mult <= coeff * 64;
				5: QP_levelScale_mult <= coeff * 72;
				default : QP_levelScale_mult <= 23'dx;
			endcase
			D1_QP_by6 <= QP_by6;
		end
	end
	
	// Stage 2
	//wires
	reg signed [36:0] QP_after_left_shift; // 
	reg signed [36:0] coeff_before_clip;
	always @(*) begin
		case (D1_QP_by6) 
			0: QP_after_left_shift = QP_levelScale_mult << 4;
			1: QP_after_left_shift = QP_levelScale_mult << 5;
			2: QP_after_left_shift = QP_levelScale_mult << 6;
			3: QP_after_left_shift = QP_levelScale_mult << 7;
			4: QP_after_left_shift = QP_levelScale_mult << 8;
			5: QP_after_left_shift = QP_levelScale_mult << 9;
			6: QP_after_left_shift = QP_levelScale_mult <<10 ;
			7: QP_after_left_shift = QP_levelScale_mult <<11;
			8: QP_after_left_shift = QP_levelScale_mult <<12;
			9: QP_after_left_shift = QP_levelScale_mult <<13;
			default : QP_after_left_shift = 37'dx;
		endcase
		case (trafoSize) 
			2: coeff_before_clip = (QP_after_left_shift + 16) >>> 5;
			3: coeff_before_clip = (QP_after_left_shift + 32) >>> 6;
			4: coeff_before_clip = (QP_after_left_shift + 64) >>> 7;
			5: coeff_before_clip = (QP_after_left_shift +128) >>> 8;
			default :  coeff_before_clip = 37'dx;
		endcase
	end

	 
	always @(posedge clk) begin
		//if (~rst) begin
			//out_coeff <= 16'd0;
		//end else begin
			if (clk_en) begin
				if (D1_transquant_bypass_flag) begin
					out_coeff <= D1_in_coeff;
				end else begin
					if (D1_tr_stage == 1'b0) begin // col
						if (coeff_before_clip < -32768) begin
							out_coeff <= -16'd32768;
						end else if (coeff_before_clip > 32767) begin
							out_coeff <= 16'd32767;
						end else begin
							out_coeff <= coeff_before_clip[15:0];
						end
					end else begin
						out_coeff <= coeff_mem;
					end
				end
				
				out_x <= D1_x;
				out_y <= D1_y;
				out_cIdx <= D1_cIdx;
				out_trafoSize <= D1_trafoSize;
				out_trType <= D1_trType;
				out_transform_skip_flag <= D1_transform_skip_flag;
				out_transquant_bypass_flag <= D1_transquant_bypass_flag;
				out_tr_stage <= D1_tr_stage;
				out_tr_valid <= D1_tr_valid;
				out_new_row <= D1_new_row;
				out_end_row <= D1_end_row;
			end
		//end
	 end


endmodule
