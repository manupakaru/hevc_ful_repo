`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:05:21 04/10/2014 
// Design Name: 
// Module Name:    mcm 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mcm(
		clk,
		clk_en, // set this if input fifo empty or output fifo full
		rst,
		
		coeff,
		x,
		y,
		
		cIdx,
		trafoSize,
		trType,
		
		transform_skip_flag,
		transquant_bypass_flag,
		
		tr_stage, // row or column transform
		tr_valid, // down if currently in bypass mode
		new_row,// asseted at the beginning of new row when state=STATE_ROW
		end_row,
		
		x90,
		x89,
		x88,
		x87,
		x85,
		x83,
		x82,
		x80,
		x78,
		x75,
		x73,
		x70,
		x67,
		x64,
		x61,
		x57,
		x54,
		x50,
		x46,
		x43,
		x38,
		x36,
		x31,
		x25,
		x22,
		x18,
		x13,
		x9,
		x4,
		x29,
		x55,
		x74,
		x84,
		out_x,
		out_y,

		out_cIdx,
		out_trafoSize,
		out_trType,
		
		out_transform_skip_flag,
		out_transquant_bypass_flag,
		
		out_tr_stage, // row or column transform
		out_tr_valid, // down if currently in bypass mode
		out_new_row,// asseted at the beginning of new row when state=STATE_ROW
		out_end_row
    );

 // I/O
	 input clk, rst, clk_en;
	 
	input signed [15:0] coeff;
	input [4:0] x,y;
	input [1:0] cIdx;
	
	input [2:0] trafoSize;
	input trType;
	input transform_skip_flag;
	input transquant_bypass_flag;
	
	input tr_stage, tr_valid;
	input new_row, end_row; 
	 
	output reg [4:0] out_x,out_y;
	output reg [1:0] out_cIdx;
	
	output reg [2:0] out_trafoSize;
	output reg out_trType;
	output reg out_transform_skip_flag;
	output reg out_transquant_bypass_flag;
	
	output reg out_tr_stage, out_tr_valid;
	output reg out_new_row, out_end_row;
	
	// Internal
	output reg signed [23:0] x90, x89, x88, x87, x85, x83;
	output reg signed [23:0] x82, x80, x78, x75, x73, x70;
	output reg signed [23:0] x67, x64, x61, x57, x54, x50;
	output reg signed [23:0] x46, x43, x38, x36, x31, x25;
	output reg signed [23:0] x22, x18, x13, x9 , x4		;

	output reg signed [23:0] x29, x55, x74, x84;
	
	
	always @(posedge clk) begin
		if (clk_en) begin
			out_x <= x;
			out_y <= y;
			out_cIdx <= cIdx;
			out_trafoSize <= trafoSize;
			out_trType <= trType;
			out_transform_skip_flag <= transform_skip_flag;
			out_transquant_bypass_flag <= transquant_bypass_flag;
			out_tr_stage <= tr_stage;
			out_tr_valid <= tr_valid;
			out_new_row <= new_row;
			out_end_row <= end_row;
			if (tr_valid) begin
				x90 = coeff * 90;
				x89 = coeff * 89;
				x88 = coeff * 88;
				x87 = coeff * 87;
				x85 = coeff * 85;
				x83 = coeff * 83;
				x82 = coeff * 82;
				x80 = coeff * 80;
				x78 = coeff * 78;
				x75 = coeff * 75;
				x73 = coeff * 73;
				x70 = coeff * 70;
				x67 = coeff * 67;
				x64 = coeff * 64;
				x61 = coeff * 61;
				x57 = coeff * 57;
				x54 = coeff * 54;
				x50 = coeff * 50;
				x46 = coeff * 46;
				x43 = coeff * 43;
				x38 = coeff * 38;
				x36 = coeff * 36;
				x31 = coeff * 31;
				x25 = coeff * 25;
				x22 = coeff * 22;
				x18 = coeff * 18;
				x13 = coeff * 13;
				x9  = coeff *  9;
				x4  = coeff *  4;
				x29 = coeff * 29;
				x55 = coeff * 55;
				x74 = coeff * 74;
				x84 = coeff * 84;
			end
		end
	end
	
	
endmodule
