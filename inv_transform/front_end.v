`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:20:34 04/09/2014 
// Design Name: 
// Module Name:    front_end 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

// Responsibility - Reads packets from entropy decoder output and processes them depending on the packet type
// For scaling module sets up the coefficient, for memory sets up the address
// Also handles the sparsity in column transform via filled_col bitmap.

module front_end(
		clk,
		clk_en, // set this if input fifo empty or output fifo full
		rst,
		
		read_in,
		read_next, //wire
		
		coeff,
		x,
		y,
		
		QP,
		cIdx,
		trafoSize,
		trType,
		
		transform_skip_flag,
		transquant_bypass_flag,
		
		tr_stage, // row or column transform
		tr_valid, // down if currently in bypass mode
		new_row,// asseted at the beginning of new row when state=STATE_ROW or new_col when STATE_COL
		end_row,
		
		//config fifo interface
		config_fifo_in, // wire
		config_fifo_valid,
		
		//test
		trafo_x0,
		trafo_y0,
		trafo_POC,
		trafo_cIdx
    );

	input clk;
	input clk_en; 
	input rst;
	
	input [31:0] read_in;
	output reg read_next;
	
	output reg [15:0] coeff;
	output reg [4:0] x,y;
	
	output reg [5:0] QP;
	output reg [1:0] cIdx;
	
	output reg [2:0] trafoSize;
	output reg trType;
	output reg transform_skip_flag;
	output reg transquant_bypass_flag;
	
	output reg tr_stage, tr_valid;
	output reg new_row, end_row;
	
	output reg [31:0] config_fifo_in;
	output reg config_fifo_valid;
	
	output [11:0] trafo_x0, trafo_y0;
	output [15:0] trafo_POC;
	output [1:0] trafo_cIdx;
	
	// Internal registers
	reg [2:0] state;
	reg [1:0] bypass_count;
	reg [0:31] filled_cols , filled_cols_cur;
	reg [4:0] first_filled_col, last_filled_col, num_filled_cols;
	
	reg [2:0] trafoSize_prev; // previous transform block's size. Used in determining pipeline delay
	
	reg res_present;
	reg mode_intra;

	reg signed [7:0] QPCb, QPCr;
	
	reg signed [4:0] pps_cb_qp_offset, pps_cr_qp_offset;
	reg signed [4:0] slice_cb_qp_offset, slice_cr_qp_offset;
	
	reg [4:0] last_row_no;
	reg [4:0] curr_y; // internal register, this is assigned with delay to output port
	
	reg [3:0] delay;
	
	reg [31:0] reg_h40;
	reg [31:0] reg_h41;
	
	//combinational
	reg signed [7:0] QPCb_adjust, QPCr_adjust; // 8 bits just to be on the safe side
	
	// test_only - not to be synthesized
	reg [11:0] x0, y0;
	reg [15:0] POC;
	reg next_POC;
	
	assign trafo_x0 = x0;
	assign trafo_y0 = y0;
	assign trafo_POC = POC;
	assign trafo_cIdx = cIdx;
	
	//wires
	wire [1:0] w_cIdx;
	wire [2:0] w_trafoSize;
	
	wire [4:0] largest_bit;
	wire no_bit_set;
	
	// parameters
	localparam STATE_BYPASS 	 = 3'b010;
	localparam STATE_COL 		 = 3'b000;
	localparam STATE_ROW 		 = 3'b001;
	localparam STATE_DELAY 		 = 3'b011;
	localparam STATE_POST_DELAY =	3'b100;
	localparam STATE_H40			 = 3'b101;
	localparam STATE_H41			 =	3'b110;
	
	localparam PIPELINE_DELAY = 4'd1;
	localparam POST_DELAY 	  = 4'd10;
	
	assign w_cIdx = read_in[19:18];
	assign w_trafoSize = read_in[22:20];
	
	// QP chroma adjustment
	always @(*) begin
		case (QPCb) 
			30,31,32,33,34	: QPCb_adjust = QPCb-3'd1;
			35,36				: QPCb_adjust = QPCb-3'd2;
			37,38				: QPCb_adjust = QPCb-3'd3;
			39,40				: QPCb_adjust = QPCb-3'd4;
			41,42				: QPCb_adjust = QPCb-3'd5;
			43,45,46,47,48,49,50,51,52,53,54,55,56,57
								: QPCb_adjust = QPCb-3'd6;
			default 			: QPCb_adjust = QPCb;
		endcase
		case (QPCr) 
			30,31,32,33,34	: QPCr_adjust = QPCr-3'd1;
			35,36				: QPCr_adjust = QPCr-3'd2;
			37,38				: QPCr_adjust = QPCr-3'd3;
			39,40				: QPCr_adjust = QPCr-3'd4;
			41,42				: QPCr_adjust = QPCr-3'd5;
			43,45,46,47,48,49,50,51,52,53,54,55,56,57
								: QPCr_adjust = QPCr-3'd6;
			default 			: QPCr_adjust = QPCr;
		endcase
	end
	
	largest_bit_set lbs(
		.bitmap(filled_cols_cur),
		.index(largest_bit),
		.no_bit_set(no_bit_set)
    );
	
	always @(posedge clk) begin
		
		if (~rst) begin
			filled_cols <= 32'd0;
			state <= STATE_BYPASS;
			bypass_count <= 2'd0;
			end_row <= 1'b0;
			trafoSize_prev <= 3'd2;
		end else begin
			if (clk_en) begin
				case (state)
					STATE_BYPASS: begin
						end_row <= 1'b0;
						
						if (bypass_count == 0) begin
							case(read_in[7:0])
								8'h01: begin
									pps_cb_qp_offset <= read_in[15:11];
									pps_cr_qp_offset <= read_in[20:16];
								end
								8'h12: begin
									slice_cb_qp_offset <= read_in[12: 8];
									slice_cr_qp_offset <= read_in[17:13];
								end
								8'h10: begin 
								  bypass_count <= 2'd1;
								  next_POC <= 1'b1;
								end
								8'h20: begin
								  x0 <= read_in[19:8];
								  y0 <= read_in[31:20];  
								end
								8'h30: begin
									transquant_bypass_flag <= read_in[27];
									mode_intra <= ~read_in[23];
								end
								8'h40: begin
									res_present <= read_in[29];
									transform_skip_flag <= read_in[30];
									cIdx <= w_cIdx;
									trafoSize <= w_trafoSize;
									x0[5:1] <= read_in[12:8];
									y0[5:1] <= read_in[17:13];
									reg_h40[31:23] = read_in[31:23];
									reg_h40[19: 0] = read_in[19: 0];
									if ( w_cIdx != 2'b00) begin
										reg_h40[22:20] = w_trafoSize + 1'b1;
									end else begin
										reg_h40[22:20] = w_trafoSize;
									end
									case (w_trafoSize)
										2: last_row_no <= 5'd3;
										3: last_row_no <= 5'd7;
										4: last_row_no <= 5'd15;
										5: last_row_no <= 5'd31;
									endcase
									if ( (w_cIdx != 2'b00) && read_in[29]) begin // cidx>0 && res_present
										state <= STATE_COL;
										filled_cols <= 32'd0;
										first_filled_col <= 5'd31;
										last_filled_col <= 5'd0;
										num_filled_cols <= 5'd0;
									end
									case (w_cIdx)
										1: QP <= (QPCb_adjust[7] == 1'b1) ? 6'd0 : (QPCb_adjust > 57 ? 6'd57 : QPCb_adjust[5:0]);
										2: QP <= (QPCr_adjust[7] == 1'b1) ? 6'd0 : (QPCr_adjust > 57 ? 6'd57 : QPCr_adjust[5:0]);
									endcase
									trType <= mode_intra & (w_cIdx==2'b00) & (w_trafoSize==3'd2);
								end
								8'h41: begin
									if (res_present) begin
										state <= STATE_COL;
										filled_cols <= 32'd0;
										first_filled_col <= 5'd31;
										last_filled_col <= 5'd0;
										num_filled_cols <= 5'd0;
									end
									reg_h41 <= read_in;
									QP <= read_in[17:12];
									// sign extend offsets
									QPCb <= read_in[17:12] +{pps_cb_qp_offset[4], pps_cb_qp_offset}+ {slice_cb_qp_offset[4] ,slice_cb_qp_offset};
									QPCr <= read_in[17:12] +{pps_cr_qp_offset[4], pps_cr_qp_offset}+ {slice_cr_qp_offset[4] ,slice_cr_qp_offset};
								end
								8'h50: if (~read_in[12]) bypass_count <= 2'd2; // if not merge mode
							endcase
						end else begin
							bypass_count <= bypass_count-1'b1;
							if (next_POC) begin 
							   POC <= read_in[15:0];
							   next_POC <= 1'b0;
						  end
						end
						tr_valid <= 1'b0;
					end
					STATE_COL: begin
						end_row <= 1'b0; // will change if transform_skip
						if (read_in[5:0] == 6'd62) begin							
							case ( {trafoSize_prev , num_filled_cols} )
							  { 3'd5, 5'd1 } : delay <= PIPELINE_DELAY + 4'd10;
							  { 3'd5, 5'd2 } : delay <= PIPELINE_DELAY + 4'd5;
							  default :        delay <= PIPELINE_DELAY;
							endcase			
							if (first_filled_col==last_filled_col && trafoSize == 3'd5)			begin
							   if (first_filled_col == 5'd0) begin
							       filled_cols[1] <= 1'b1;
							       last_filled_col <= 5'd1;
							       x <= 5'd1;
							   end else begin
							       filled_cols[0] <= 1'b1;
							       first_filled_col <= 5'd0;
							       x <= 5'd0;
							   end
							   num_filled_cols <= 5'd2;
							   coeff <= 16'd0;
							   tr_stage <= 1'b0;
							   tr_valid <= 1'b1;
							   y <= 5'd0;
							   new_row <= 1'b1;
							end else begin
							   state <= STATE_DELAY;
							   tr_stage <= 1'b1;
							   tr_valid <= 1'b0;
							   x <= first_filled_col;
							   y <= 5'd0;	
							   new_row <= 1'b0;
							end						
						end else begin
							x <= read_in[10:6];
							y <= read_in[15:11];
							coeff <= read_in[31:16];
							filled_cols[read_in[10:6]] <= 1'b1; // filled_cols[x]
							if (~filled_cols[read_in[10:6]]) begin
							  num_filled_cols <= num_filled_cols + 1'b1;
							end				
							if (read_in[10:6] < first_filled_col) begin
								first_filled_col <= read_in[10:6];
							end
							if (read_in[10:6] > last_filled_col) begin
								last_filled_col <= read_in[10:6];
							end
							new_row <= ~filled_cols[read_in[10:6]];
							tr_stage <= 1'b0;
							tr_valid <= 1'b1;
						end				
					end
					STATE_DELAY: begin
						end_row <= 1'b0;
						tr_stage <= 1'b1;
						tr_valid <= 1'b0;
						if (delay == 4'd0) begin
							state <= STATE_ROW;
							filled_cols_cur <= filled_cols;
							curr_y <= 5'd0;
						end
						delay <= delay - 1'b1;
					end
					STATE_ROW: begin
						if (curr_y == last_row_no && !no_bit_set && (largest_bit == last_filled_col) ) begin
							if (cIdx == 3'd2) begin
								state <= STATE_POST_DELAY;
								delay <= POST_DELAY;
							end else begin
								state <= STATE_BYPASS;
							end
							trafoSize_prev <= trafoSize;
						end
						x <= largest_bit;
						y <= curr_y;
						if (!no_bit_set && (largest_bit == last_filled_col)) begin
							filled_cols_cur <= filled_cols;
							end_row <= 1'b1;
							curr_y <= curr_y + 1'b1;
						end else begin
							end_row <= 1'b0;
							filled_cols_cur[largest_bit] <= 1'b0;
						end
						if (largest_bit == first_filled_col && !no_bit_set) begin
							new_row <= 1'b1;
						end else begin
							new_row <= 1'b0;
						end
						tr_stage<= 1'b1;
						tr_valid <= 1'b1;
					end
					STATE_POST_DELAY: begin
						if (delay==4'd0) begin
							state <= STATE_H40;
						end 
						delay <= delay -1'b1;
						tr_valid <= 1'b0;
						end_row <= 1'b0;
					end
					STATE_H40: begin
						state <= STATE_BYPASS;
						tr_valid <= 1'b0;
					end
				endcase
			
			end
		end
	end
	
	
	always @(*) begin
		config_fifo_in = read_in;
		config_fifo_valid = 1'b0;
		read_next = 1'b0;
		if (~rst) begin
			read_next = 1'b0;
		end else begin
			if (clk_en) begin
				case (state)
					STATE_BYPASS: begin
						config_fifo_valid = 1;
						if (bypass_count == 0 && read_in[5:0] == 6'd62) begin // no need to push down ERU packet
							config_fifo_valid = 1'b0;
						end
						if (w_cIdx == 3'd2 && bypass_count ==0 && read_in[7:0] == 8'h40 && read_in[29]) begin
							config_fifo_valid = 1'b0;
						end
						read_next = 1'b1;
						if (bypass_count == 2'd0 && read_in[7:0]==8'h40) begin
							if (w_cIdx!= 2'b00) begin
								config_fifo_in[22:20] = w_trafoSize + 1'b1;
							end
						end
					end
					STATE_COL: begin
						read_next = (read_in[5:0]==6'd63);
					end
					STATE_H40: begin
						config_fifo_in = reg_h40;
						config_fifo_valid = 1'b1;
					end
				endcase
			end
		end
	end
	
endmodule
