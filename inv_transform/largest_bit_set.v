`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:21:28 04/17/2014 
// Design Name: 
// Module Name:    largest_bit_set 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module largest_bit_set(
		bitmap,
		index,
		no_bit_set
    );
	 input [0:31] bitmap;
	 output reg [4:0] index;
	 output reg no_bit_set;
	 
	 always @(*) begin
		casex (bitmap)
			32'b 1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx : index = 5'd0;
			32'b 01xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx : index = 5'd1;
			32'b 001x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx : index = 5'd2;
			32'b 0001_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx : index = 5'd3;
			
			32'b 0000_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx : index = 5'd4;
			32'b 0000_01xx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx : index = 5'd5;
			32'b 0000_001x_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx : index = 5'd6;
			32'b 0000_0001_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx : index = 5'd7;
			
			32'b 0000_0000_1xxx_xxxx_xxxx_xxxx_xxxx_xxxx : index = 5'd8;
			32'b 0000_0000_01xx_xxxx_xxxx_xxxx_xxxx_xxxx : index = 5'd9;
			32'b 0000_0000_001x_xxxx_xxxx_xxxx_xxxx_xxxx : index = 5'd10;
			32'b 0000_0000_0001_xxxx_xxxx_xxxx_xxxx_xxxx : index = 5'd11;
			
			32'b 0000_0000_0000_1xxx_xxxx_xxxx_xxxx_xxxx : index = 5'd12;
			32'b 0000_0000_0000_01xx_xxxx_xxxx_xxxx_xxxx : index = 5'd13;
			32'b 0000_0000_0000_001x_xxxx_xxxx_xxxx_xxxx : index = 5'd14;
			32'b 0000_0000_0000_0001_xxxx_xxxx_xxxx_xxxx : index = 5'd15;
			
			32'b 0000_0000_0000_0000_1xxx_xxxx_xxxx_xxxx : index = 5'd16;
			32'b 0000_0000_0000_0000_01xx_xxxx_xxxx_xxxx : index = 5'd17;
			32'b 0000_0000_0000_0000_001x_xxxx_xxxx_xxxx : index = 5'd18;
			32'b 0000_0000_0000_0000_0001_xxxx_xxxx_xxxx : index = 5'd19;
			
			32'b 0000_0000_0000_0000_0000_1xxx_xxxx_xxxx : index = 5'd20;
			32'b 0000_0000_0000_0000_0000_01xx_xxxx_xxxx : index = 5'd21;
			32'b 0000_0000_0000_0000_0000_001x_xxxx_xxxx : index = 5'd22;
			32'b 0000_0000_0000_0000_0000_0001_xxxx_xxxx : index = 5'd23;
			
			32'b 0000_0000_0000_0000_0000_0000_1xxx_xxxx : index = 5'd24;
			32'b 0000_0000_0000_0000_0000_0000_01xx_xxxx : index = 5'd25;
			32'b 0000_0000_0000_0000_0000_0000_001x_xxxx : index = 5'd26;
			32'b 0000_0000_0000_0000_0000_0000_0001_xxxx : index = 5'd27;
			
			32'b 0000_0000_0000_0000_0000_0000_0000_1xxx : index = 5'd28;
			32'b 0000_0000_0000_0000_0000_0000_0000_01xx : index = 5'd29;
			32'b 0000_0000_0000_0000_0000_0000_0000_001x : index = 5'd30;
			32'b 0000_0000_0000_0000_0000_0000_0000_0001 : index = 5'd31;
			default : index = 5'dx;
		endcase
		no_bit_set = ~(|bitmap);
	 end


endmodule
