`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:45:28 04/10/2014 
// Design Name: 
// Module Name:    permute 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module permute_test(
		clk,
		clk_en, // set this if input fifo empty or output fifo full
		rst,
		
		x,
		y,
		
		cIdx,
		trafoSize,
		trType,
		
		transform_skip_flag,
		transquant_bypass_flag,
		
		tr_stage, // row or column transform
		tr_valid, // down if currently in bypass mode
		new_row,// asseted at the beginning of new row when state=STATE_ROW
		end_row,
		
		x90,
		x89,
		x88,
		x87,
		x85,
		x83,
		x82,
		x80,
		x78,
		x75,
		x73,
		x70,
		x67,
		x64,
		x61,
		x57,
		x54,
		x50,
		x46,
		x43,
		x38,
		x36,
		x31,
		x25,
		x22,
		x18,
		x13,
		x9,
		x4,
		x29,
		x55,
		x74,
		x84,

		out_0,
		out_1,
		out_2,
		out_3,
		out_4,
		out_5,
		out_6,
		out_7,
		out_8,
		out_9,
		out_10,
		out_11,
		out_12,
		out_13,
		out_14,
		out_15,
		out_16,
		out_17,
		out_18,
		out_19,
		out_20,
		out_21,
		out_22,
		out_23,
		out_24,
		out_25,
		out_26,
		out_27,
		out_28,
		out_29,
		out_30,
		out_31,

		out_x,
		out_y,
		out_bypass,
		out_cIdx,
		out_trafoSize,
		out_trType,
		
		out_transform_skip_flag,
		out_transquant_bypass_flag,
		
		out_tr_stage, // row or column transform
		out_tr_valid, // down if currently in bypass mode
		out_new_row,// asseted at the beginning of new row when state=STATE_ROW
		out_end_row
    );

// I/O
	 input clk, rst, clk_en;
	 
	input [4:0] x,y;
	input [1:0] cIdx;
	
	input [2:0] trafoSize;
	input trType;
	input transform_skip_flag;
	input transquant_bypass_flag;
	
	input tr_stage, tr_valid;
	input new_row, end_row; 
	 
	input signed [23:0] x90;
	input signed [23:0] x89;
	input signed [23:0] x88;
	input signed [23:0] x87;
	input signed [23:0] x85;
	input signed [23:0] x83;
	input signed [23:0] x82;
	input signed [23:0] x80;
	input signed [23:0] x78;
	input signed [23:0] x75;
	input signed [23:0] x73;
	input signed [23:0] x70;
	input signed [23:0] x67;
	input signed [23:0] x64;
	input signed [23:0] x61;
	input signed [23:0] x57;
	input signed [23:0] x54;
	input signed [23:0] x50;
	input signed [23:0] x46;
	input signed [23:0] x43;
	input signed [23:0] x38;
	input signed [23:0] x36;
	input signed [23:0] x31;
	input signed [23:0] x25;
	input signed [23:0] x22;
	input signed [23:0] x18;
	input signed [23:0] x13;
	input signed [23:0] x9;
	input signed [23:0] x4;
	input signed [23:0] x29;
	input signed [23:0] x55;
	input signed [23:0] x74;
	input signed [23:0] x84;

	output reg signed [23:0]  out_0 , out_1 , out_2 , out_3, out_4 , out_5 , out_6 , out_7, out_8 , out_9 , out_10 , out_11, out_12 , out_13 , out_14 , out_15, out_16 , out_17 , out_18 , out_19, out_20 , out_21 , out_22 , out_23, out_24 , out_25 , out_26 , out_27, out_28 , out_29 , out_30 , out_31;
	output reg [4:0] out_x,out_y;
	output reg [1:0] out_cIdx;
	output reg [23:0] out_bypass;
	output reg [2:0] out_trafoSize;
	output reg out_trType;
	output reg out_transform_skip_flag;
	output reg out_transquant_bypass_flag;
	
	output reg out_tr_stage, out_tr_valid;
	output reg out_new_row, out_end_row;
	
	reg signed [23:0]  outp_0 , outp_1 , outp_2 , outp_3, outp_4 , outp_5 , outp_6 , outp_7, outp_8 , outp_9 , outp_10 , outp_11, outp_12 , outp_13 , outp_14 , outp_15, outp_16 , outp_17 , outp_18 , outp_19, outp_20 , outp_21 , outp_22 , outp_23, outp_24 , outp_25 , outp_26 , outp_27, outp_28 , outp_29 , outp_30 , outp_31;
	`define DEF_OUTS { outp_0 , outp_1 , outp_2 , outp_3, outp_4 , outp_5 , outp_6 , outp_7, outp_8 , outp_9 , outp_10 , outp_11, outp_12 , outp_13 , outp_14 , outp_15, outp_16 , outp_17 , outp_18 , outp_19, outp_20 , outp_21 , outp_22 , outp_23, outp_24 , outp_25 , outp_26 , outp_27, outp_28 , outp_29 , outp_30 , outp_31  }
	
	reg [1:0] DST_sel;
	reg [4:0] DCT_sel;
	always @(*) begin
		case (trafoSize)
			2: DCT_sel = (tr_stage ? x : y) << 3;
			3: DCT_sel = (tr_stage ? x : y) << 2;
			4: DCT_sel = (tr_stage ? x : y) << 1;
			5: DCT_sel = (tr_stage ? x : y);
			default : DCT_sel = 5'dx;
		endcase
		DST_sel = (tr_stage ? x[1:0] : y[1:0]);
	end
	
	always @(*) begin
		if (trType == 1'b0) begin
			case (DCT_sel) 
				0 : `DEF_OUTS = {  x64, x64, x64, x64, x64, x64, x64, x64, x64, x64, x64, x64, x64, x64, x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64 }; 
				1 : `DEF_OUTS = {  x90, x90, x88, x85, x82, x78, x73, x67, x61, x54, x46, x38, x31, x22, x13,   x4,   x4,  x13,  x22,  x31,  x38,  x46,  x54,  x61,  x67,  x73,  x78,  x82,  x85,  x88,  x90,  x90 }; 
				2 : `DEF_OUTS = {  x90, x87, x80, x70, x57, x43, x25,  x9,  x9, x25, x43, x57, x70, x80, x87,  x90,  x90,  x87,  x80,  x70,  x57,  x43,  x25,   x9,   x9,  x25,  x43,  x57,  x70,  x80,  x87,  x90 }; 
				3 : `DEF_OUTS = {  x90, x82, x67, x46, x22,  x4, x31, x54, x73, x85, x90, x88, x78, x61, x38,  x13,  x13,  x38,  x61,  x78,  x88,  x90,  x85,  x73,  x54,  x31,   x4,  x22,  x46,  x67,  x82,  x90 }; 
				4 : `DEF_OUTS = {  x89, x75, x50, x18, x18, x50, x75, x89, x89, x75, x50, x18, x18, x50, x75,  x89,  x89,  x75,  x50,  x18,  x18,  x50,  x75,  x89,  x89,  x75,  x50,  x18,  x18,  x50,  x75,  x89 }; 
				5 : `DEF_OUTS = {  x88, x67, x31, x13, x54, x82, x90, x78, x46,  x4, x38, x73, x90, x85, x61,  x22,  x22,  x61,  x85,  x90,  x73,  x38,   x4,  x46,  x78,  x90,  x82,  x54,  x13,  x31,  x67,  x88 }; 
				6 : `DEF_OUTS = {  x87, x57,  x9, x43, x80, x90, x70, x25, x25, x70, x90, x80, x43,  x9, x57,  x87,  x87,  x57,   x9,  x43,  x80,  x90,  x70,  x25,  x25,  x70,  x90,  x80,  x43,   x9,  x57,  x87 }; 
				7 : `DEF_OUTS = {  x85, x46, x13, x67, x90, x73, x22, x38, x82, x88, x54,  x4, x61, x90, x78,  x31,  x31,  x78,  x90,  x61,   x4,  x54,  x88,  x82,  x38,  x22,  x73,  x90,  x67,  x13,  x46,  x85 }; 
				8 : `DEF_OUTS = {  x83, x36, x36, x83, x83, x36, x36, x83, x83, x36, x36, x83, x83, x36, x36,  x83,  x83,  x36,  x36,  x83,  x83,  x36,  x36,  x83,  x83,  x36,  x36,  x83,  x83,  x36,  x36,  x83 }; 
				9 : `DEF_OUTS = {  x82, x22, x54, x90, x61, x13, x78, x85, x31, x46, x90, x67,  x4, x73, x88,  x38,  x38,  x88,  x73,   x4,  x67,  x90,  x46,  x31,  x85,  x78,  x13,  x61,  x90,  x54,  x22,  x82 }; 
				10: `DEF_OUTS = {  x80,  x9, x70, x87, x25, x57, x90, x43, x43, x90, x57, x25, x87, x70,  x9,  x80,  x80,   x9,  x70,  x87,  x25,  x57,  x90,  x43,  x43,  x90,  x57,  x25,  x87,  x70,   x9,  x80 }; 
				11: `DEF_OUTS = {  x78,  x4, x82, x73, x13, x85, x67, x22, x88, x61, x31, x90, x54, x38, x90,  x46,  x46,  x90,  x38,  x54,  x90,  x31,  x61,  x88,  x22,  x67,  x85,  x13,  x73,  x82,   x4,  x78 }; 
				12: `DEF_OUTS = {  x75, x18, x89, x50, x50, x89, x18, x75, x75, x18, x89, x50, x50, x89, x18,  x75,  x75,  x18,  x89,  x50,  x50,  x89,  x18,  x75,  x75,  x18,  x89,  x50,  x50,  x89,  x18,  x75 }; 
				13: `DEF_OUTS = {  x73, x31, x90, x22, x78, x67, x38, x90, x13, x82, x61, x46, x88,  x4, x85,  x54,  x54,  x85,   x4,  x88,  x46,  x61,  x82,  x13,  x90,  x38,  x67,  x78,  x22,  x90,  x31,  x73 }; 
				14: `DEF_OUTS = {  x70, x43, x87,  x9, x90, x25, x80, x57, x57, x80, x25, x90,  x9, x87, x43,  x70,  x70,  x43,  x87,   x9,  x90,  x25,  x80,  x57,  x57,  x80,  x25,  x90,   x9,  x87,  x43,  x70 }; 
				15: `DEF_OUTS = {  x67, x54, x78, x38, x85, x22, x90,  x4, x90, x13, x88, x31, x82, x46, x73,  x61,  x61,  x73,  x46,  x82,  x31,  x88,  x13,  x90,   x4,  x90,  x22,  x85,  x38,  x78,  x54,  x67 }; 
				16: `DEF_OUTS = {  x64, x64, x64, x64, x64, x64, x64, x64, x64, x64, x64, x64, x64, x64, x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64,  x64 }; 
				17: `DEF_OUTS = {  x61, x73, x46, x82, x31, x88, x13, x90,  x4, x90, x22, x85, x38, x78, x54,  x67,  x67,  x54,  x78,  x38,  x85,  x22,  x90,   x4,  x90,  x13,  x88,  x31,  x82,  x46,  x73,  x61 }; 
				18: `DEF_OUTS = {  x57, x80, x25, x90,  x9, x87, x43, x70, x70, x43, x87,  x9, x90, x25, x80,  x57,  x57,  x80,  x25,  x90,   x9,  x87,  x43,  x70,  x70,  x43,  x87,   x9,  x90,  x25,  x80,  x57 }; 
				19: `DEF_OUTS = {  x54, x85,  x4, x88, x46, x61, x82, x13, x90, x38, x67, x78, x22, x90, x31,  x73,  x73,  x31,  x90,  x22,  x78,  x67,  x38,  x90,  x13,  x82,  x61,  x46,  x88,   x4,  x85,  x54 }; 
				20: `DEF_OUTS = {  x50, x89, x18, x75, x75, x18, x89, x50, x50, x89, x18, x75, x75, x18, x89,  x50,  x50,  x89,  x18,  x75,  x75,  x18,  x89,  x50,  x50,  x89,  x18,  x75,  x75,  x18,  x89,  x50 }; 
				21: `DEF_OUTS = {  x46, x90, x38, x54, x90, x31, x61, x88, x22, x67, x85, x13, x73, x82,  x4,  x78,  x78,   x4,  x82,  x73,  x13,  x85,  x67,  x22,  x88,  x61,  x31,  x90,  x54,  x38,  x90,  x46 }; 
				22: `DEF_OUTS = {  x43, x90, x57, x25, x87, x70,  x9, x80, x80,  x9, x70, x87, x25, x57, x90,  x43,  x43,  x90,  x57,  x25,  x87,  x70,   x9,  x80,  x80,   x9,  x70,  x87,  x25,  x57,  x90,  x43 }; 
				23: `DEF_OUTS = {  x38, x88, x73,  x4, x67, x90, x46, x31, x85, x78, x13, x61, x90, x54, x22,  x82,  x82,  x22,  x54,  x90,  x61,  x13,  x78,  x85,  x31,  x46,  x90,  x67,   x4,  x73,  x88,  x38 }; 
				24: `DEF_OUTS = {  x36, x83, x83, x36, x36, x83, x83, x36, x36, x83, x83, x36, x36, x83, x83,  x36,  x36,  x83,  x83,  x36,  x36,  x83,  x83,  x36,  x36,  x83,  x83,  x36,  x36,  x83,  x83,  x36 }; 
				25: `DEF_OUTS = {  x31, x78, x90, x61,  x4, x54, x88, x82, x38, x22, x73, x90, x67, x13, x46,  x85,  x85,  x46,  x13,  x67,  x90,  x73,  x22,  x38,  x82,  x88,  x54,   x4,  x61,  x90,  x78,  x31 }; 
				26: `DEF_OUTS = {  x25, x70, x90, x80, x43,  x9, x57, x87, x87, x57,  x9, x43, x80, x90, x70,  x25,  x25,  x70,  x90,  x80,  x43,   x9,  x57,  x87,  x87,  x57,   x9,  x43,  x80,  x90,  x70,  x25 }; 
				27: `DEF_OUTS = {  x22, x61, x85, x90, x73, x38,  x4, x46, x78, x90, x82, x54, x13, x31, x67,  x88,  x88,  x67,  x31,  x13,  x54,  x82,  x90,  x78,  x46,   x4,  x38,  x73,  x90,  x85,  x61,  x22 }; 
				28: `DEF_OUTS = {  x18, x50, x75, x89, x89, x75, x50, x18, x18, x50, x75, x89, x89, x75, x50,  x18,  x18,  x50,  x75,  x89,  x89,  x75,  x50,  x18,  x18,  x50,  x75,  x89,  x89,  x75,  x50,  x18 }; 
				29: `DEF_OUTS = {  x13, x38, x61, x78, x88, x90, x85, x73, x54, x31,  x4, x22, x46, x67, x82,  x90,  x90,  x82,  x67,  x46,  x22,   x4,  x31,  x54,  x73,  x85,  x90,  x88,  x78,  x61,  x38,  x13 }; 
				30: `DEF_OUTS = {   x9, x25, x43, x57, x70, x80, x87, x90, x90, x87, x80, x70, x57, x43, x25,   x9,   x9,  x25,  x43,  x57,  x70,  x80,  x87,  x90,  x90,  x87,  x80,  x70,  x57,  x43,  x25,   x9 }; 
				31: `DEF_OUTS = {   x4, x13, x22, x31, x38, x46, x54, x61, x67, x73, x78, x82, x85, x88, x90,  x90,  x90,  x90,  x88,  x85,  x82,  x78,  x73,  x67,  x61,  x54,  x46,  x38,  x31,  x22,  x13,   x4 }; 
			endcase
		end else begin
			outp_4 = 24'dx;
			outp_5 = 24'dx;
			outp_6 = 24'dx;
			outp_7 = 24'dx;
			outp_8 = 24'dx;
			outp_9 = 24'dx;
			outp_10 = 24'dx;
			outp_11 = 24'dx;
			outp_12 = 24'dx;
			outp_13 = 24'dx;
			outp_14 = 24'dx;
			outp_15 = 24'dx;
			outp_16 = 24'dx;
			outp_17 = 24'dx;
			outp_18 = 24'dx;
			outp_19 = 24'dx;
			outp_20 = 24'dx;
			outp_21 = 24'dx;
			outp_22 = 24'dx;
			outp_23 = 24'dx;
			outp_24 = 24'dx;
			outp_25 = 24'dx;
			outp_26 = 24'dx;
			outp_27 = 24'dx;
			outp_28 = 24'dx;
			outp_29 = 24'dx;
			outp_30 = 24'dx;
			outp_31 = 24'dx;
			case (DST_sel) 
				0: { outp_0 , outp_1 , outp_2 , outp_3 } = { x29, x55, x74,   x84};
				1: { outp_0 , outp_1 , outp_2 , outp_3 } = { x74, x74, 24'd0, x74};
				2: { outp_0 , outp_1 , outp_2 , outp_3 } = { x84, x29, x74,   x55};
				3: { outp_0 , outp_1 , outp_2 , outp_3 } = { x55, x84, x74,   x29};
			endcase
		end
	end
	
	always @(posedge clk) begin
		if (clk_en) begin
			if (tr_valid) begin
				if (trType == 1'b0) begin
					out_0 <= outp_0;
					out_1 <= ( (DCT_sel ==11) || (DCT_sel ==12) || (DCT_sel ==13) || (DCT_sel ==14) || (DCT_sel ==15) || (DCT_sel ==16) || (DCT_sel ==17) || (DCT_sel ==18) || (DCT_sel ==19) || (DCT_sel ==20) || (DCT_sel ==21) || (DCT_sel ==22) || (DCT_sel ==23) || (DCT_sel ==24) || (DCT_sel ==25) || (DCT_sel ==26) || (DCT_sel ==27) || (DCT_sel ==28) || (DCT_sel ==29) || (DCT_sel ==30) || (DCT_sel ==31)  )  ?  -outp_1 : outp_1;
					out_2 <= ( (DCT_sel ==7) || (DCT_sel ==8) || (DCT_sel ==9) || (DCT_sel ==10) || (DCT_sel ==11) || (DCT_sel ==12) || (DCT_sel ==13) || (DCT_sel ==14) || (DCT_sel ==15) || (DCT_sel ==16) || (DCT_sel ==17) || (DCT_sel ==18) || (DCT_sel ==19) )  ?  -outp_2 : outp_2;
					out_3 <= ( (DCT_sel ==5) || (DCT_sel ==6) || (DCT_sel ==7) || (DCT_sel ==8) || (DCT_sel ==9) || (DCT_sel ==10) || (DCT_sel ==11) || (DCT_sel ==12) || (DCT_sel ==13) || (DCT_sel ==23) || (DCT_sel ==24) || (DCT_sel ==25) || (DCT_sel ==26) || (DCT_sel ==27) || (DCT_sel ==28) || (DCT_sel ==29) || (DCT_sel ==30) || (DCT_sel ==31)  )  ?  -outp_3 : outp_3;
					out_4 <= ( (DCT_sel ==4) || (DCT_sel ==5) || (DCT_sel ==6) || (DCT_sel ==7) || (DCT_sel ==8) || (DCT_sel ==9) || (DCT_sel ==10) || (DCT_sel ==18) || (DCT_sel ==19) || (DCT_sel ==20) || (DCT_sel ==21) || (DCT_sel ==22) || (DCT_sel ==23) || (DCT_sel ==24) )  ?  -outp_4 : outp_4;
					out_5 <= ( (DCT_sel ==3) || (DCT_sel ==4) || (DCT_sel ==5) || (DCT_sel ==6) || (DCT_sel ==7) || (DCT_sel ==8) || (DCT_sel ==15) || (DCT_sel ==16) || (DCT_sel ==17) || (DCT_sel ==18) || (DCT_sel ==19) || (DCT_sel ==20) || (DCT_sel ==27) || (DCT_sel ==28) || (DCT_sel ==29) || (DCT_sel ==30) || (DCT_sel ==31) )  ?  -outp_5 : outp_5;
					out_6 <= ( (DCT_sel ==3) || (DCT_sel ==4) || (DCT_sel ==5) || (DCT_sel ==6) || (DCT_sel ==7) || (DCT_sel ==13) || (DCT_sel ==14) || (DCT_sel ==15) || (DCT_sel ==16) || (DCT_sel ==17) || (DCT_sel ==23) || (DCT_sel ==24) || (DCT_sel ==25) || (DCT_sel ==26) || (DCT_sel ==27) )  ?  -outp_6 : outp_6;
					out_7 <= ( (DCT_sel ==3) || (DCT_sel ==4) || (DCT_sel ==5) || (DCT_sel ==6) || (DCT_sel ==11) || (DCT_sel ==12) || (DCT_sel ==13) || (DCT_sel ==14) || (DCT_sel ==20) || (DCT_sel ==21) || (DCT_sel ==22) || (DCT_sel ==23) || (DCT_sel ==28) || (DCT_sel ==29) || (DCT_sel ==30) || (DCT_sel ==31) )  ?  -outp_7 : outp_7;
					out_8 <= ( (DCT_sel ==2) || (DCT_sel ==3) || (DCT_sel ==4) || (DCT_sel ==5) || (DCT_sel ==10) || (DCT_sel ==11) || (DCT_sel ==12) || (DCT_sel ==13) || (DCT_sel ==17) || (DCT_sel ==18) || (DCT_sel ==19) || (DCT_sel ==20) || (DCT_sel ==25) || (DCT_sel ==26) || (DCT_sel ==27) || (DCT_sel ==28) )  ?  -outp_8 : outp_8;
					out_9 <= ( (DCT_sel ==2) || (DCT_sel ==3) || (DCT_sel ==4) || (DCT_sel ==5) || (DCT_sel ==9) || (DCT_sel ==10) || (DCT_sel ==11) || (DCT_sel ==16) || (DCT_sel ==17) || (DCT_sel ==18) || (DCT_sel ==22) || (DCT_sel ==23) || (DCT_sel ==24) || (DCT_sel ==25) || (DCT_sel ==29) || (DCT_sel ==30) || (DCT_sel ==31) )  ?  -outp_9 : outp_9;
					out_10 <= ( (DCT_sel ==2) || (DCT_sel ==3) || (DCT_sel ==4) || (DCT_sel ==8) || (DCT_sel ==9) || (DCT_sel ==10) || (DCT_sel ==14) || (DCT_sel ==15) || (DCT_sel ==16) || (DCT_sel ==20) || (DCT_sel ==21) || (DCT_sel ==22) || (DCT_sel ==26) || (DCT_sel ==27) || (DCT_sel ==28) )  ?  -outp_10 : outp_10;
					out_11 <= ( (DCT_sel ==2) || (DCT_sel ==3) || (DCT_sel ==4) || (DCT_sel ==7) || (DCT_sel ==8) || (DCT_sel ==9) || (DCT_sel ==13) || (DCT_sel ==14) || (DCT_sel ==15) || (DCT_sel ==19) || (DCT_sel ==20) || (DCT_sel ==24) || (DCT_sel ==25) || (DCT_sel ==26) || (DCT_sel ==30) || (DCT_sel ==31) )  ?  -outp_11 : outp_11;
					out_12 <= ( (DCT_sel ==2) || (DCT_sel ==3) || (DCT_sel ==7) || (DCT_sel ==8) || (DCT_sel ==12) || (DCT_sel ==13) || (DCT_sel ==14) || (DCT_sel ==17) || (DCT_sel ==18) || (DCT_sel ==19) || (DCT_sel ==22) || (DCT_sel ==23) || (DCT_sel ==24) || (DCT_sel ==27) || (DCT_sel ==28) || (DCT_sel ==29) )  ?  -outp_12 : outp_12;
					out_13 <= ( (DCT_sel ==2) || (DCT_sel ==3) || (DCT_sel ==6) || (DCT_sel ==7) || (DCT_sel ==8) || (DCT_sel ==11) || (DCT_sel ==12) || (DCT_sel ==13) || (DCT_sel ==16) || (DCT_sel ==17) || (DCT_sel ==21) || (DCT_sel ==22) || (DCT_sel ==25) || (DCT_sel ==26) || (DCT_sel ==27) || (DCT_sel ==30) || (DCT_sel ==31) )  ?  -outp_13 : outp_13;
					out_14 <= ( (DCT_sel ==2) || (DCT_sel ==3) || (DCT_sel ==6) || (DCT_sel ==7) || (DCT_sel ==10) || (DCT_sel ==11) || (DCT_sel ==12) || (DCT_sel ==15) || (DCT_sel ==16) || (DCT_sel ==19) || (DCT_sel ==20) || (DCT_sel ==24) || (DCT_sel ==25) || (DCT_sel ==28) || (DCT_sel ==29) )  ?  -outp_14 : outp_14;
					out_15 <= ( (DCT_sel ==2) || (DCT_sel ==3) || (DCT_sel ==6) || (DCT_sel ==7) || (DCT_sel ==10) || (DCT_sel ==11) || (DCT_sel ==14) || (DCT_sel ==15) || (DCT_sel ==18) || (DCT_sel ==19) || (DCT_sel ==22) || (DCT_sel ==23) || (DCT_sel ==26) || (DCT_sel ==27) || (DCT_sel ==30) || (DCT_sel ==31) )  ?  -outp_15 : outp_15;
					out_16 <= ( (DCT_sel ==1) || (DCT_sel ==2) || (DCT_sel ==5) || (DCT_sel ==6) || (DCT_sel ==9) || (DCT_sel ==10) || (DCT_sel ==13) || (DCT_sel ==14) || (DCT_sel ==17) || (DCT_sel ==18) || (DCT_sel ==21) || (DCT_sel ==22) || (DCT_sel ==25) || (DCT_sel ==26) || (DCT_sel ==29) || (DCT_sel ==30) )  ?  -outp_16 : outp_16;
					out_17 <= ( (DCT_sel ==1) || (DCT_sel ==2) || (DCT_sel ==5) || (DCT_sel ==6) || (DCT_sel ==9) || (DCT_sel ==10) || (DCT_sel ==12) || (DCT_sel ==13) || (DCT_sel ==16) || (DCT_sel ==17) || (DCT_sel ==20) || (DCT_sel ==21) || (DCT_sel ==23) || (DCT_sel ==24) || (DCT_sel ==27) || (DCT_sel ==28) || (DCT_sel ==31) )  ?  -outp_17 : outp_17;
					out_18 <= ( (DCT_sel ==1) || (DCT_sel ==2) || (DCT_sel ==5) || (DCT_sel ==6) || (DCT_sel ==8) || (DCT_sel ==9) || (DCT_sel ==12) || (DCT_sel ==15) || (DCT_sel ==16) || (DCT_sel ==19) || (DCT_sel ==22) || (DCT_sel ==23) || (DCT_sel ==26) || (DCT_sel ==29) || (DCT_sel ==30) )  ?  -outp_18 : outp_18;
					out_19 <= ( (DCT_sel ==1) || (DCT_sel ==2) || (DCT_sel ==5) || (DCT_sel ==8) || (DCT_sel ==9) || (DCT_sel ==11) || (DCT_sel ==12) || (DCT_sel ==14) || (DCT_sel ==15) || (DCT_sel ==18) || (DCT_sel ==21) || (DCT_sel ==22) || (DCT_sel ==24) || (DCT_sel ==25) || (DCT_sel ==28) || (DCT_sel ==31) )  ?  -outp_19 : outp_19;
					out_20 <= ( (DCT_sel ==1) || (DCT_sel ==2) || (DCT_sel ==4) || (DCT_sel ==5) || (DCT_sel ==8) || (DCT_sel ==11) || (DCT_sel ==14) || (DCT_sel ==17) || (DCT_sel ==20) || (DCT_sel ==21) || (DCT_sel ==23) || (DCT_sel ==24) || (DCT_sel ==26) || (DCT_sel ==27) || (DCT_sel ==29) || (DCT_sel ==30) )  ?  -outp_20 : outp_20;
					out_21 <= ( (DCT_sel ==1) || (DCT_sel ==2) || (DCT_sel ==4) || (DCT_sel ==5) || (DCT_sel ==7) || (DCT_sel ==8) || (DCT_sel ==10) || (DCT_sel ==11) || (DCT_sel ==13) || (DCT_sel ==14) || (DCT_sel ==16) || (DCT_sel ==17) || (DCT_sel ==19) || (DCT_sel ==20) || (DCT_sel ==22) || (DCT_sel ==23) || (DCT_sel ==25) || (DCT_sel ==26) || (DCT_sel ==28) || (DCT_sel ==29) || (DCT_sel ==31) )  ?  -outp_21 : outp_21;
					out_22 <= ( (DCT_sel ==1) || (DCT_sel ==2) || (DCT_sel ==4) || (DCT_sel ==7) || (DCT_sel ==10) || (DCT_sel ==13) || (DCT_sel ==15) || (DCT_sel ==16) || (DCT_sel ==18) || (DCT_sel ==19) || (DCT_sel ==21) || (DCT_sel ==22) || (DCT_sel ==24) || (DCT_sel ==27) || (DCT_sel ==30) )  ?  -outp_22 : outp_22;
					out_23 <= ( (DCT_sel ==1) || (DCT_sel ==2) || (DCT_sel ==4) || (DCT_sel ==7) || (DCT_sel ==9) || (DCT_sel ==10) || (DCT_sel ==12) || (DCT_sel ==15) || (DCT_sel ==18) || (DCT_sel ==20) || (DCT_sel ==21) || (DCT_sel ==23) || (DCT_sel ==26) || (DCT_sel ==28) || (DCT_sel ==29) || (DCT_sel ==31) )  ?  -outp_23 : outp_23;
					out_24 <= ( (DCT_sel ==1) || (DCT_sel ==4) || (DCT_sel ==6) || (DCT_sel ==7) || (DCT_sel ==9) || (DCT_sel ==12) || (DCT_sel ==14) || (DCT_sel ==15) || (DCT_sel ==17) || (DCT_sel ==19) || (DCT_sel ==20) || (DCT_sel ==22) || (DCT_sel ==25) || (DCT_sel ==27) || (DCT_sel ==28) || (DCT_sel ==30) )  ?  -outp_24 : outp_24;
					out_25 <= ( (DCT_sel ==1) || (DCT_sel ==4) || (DCT_sel ==6) || (DCT_sel ==9) || (DCT_sel ==11) || (DCT_sel ==14) || (DCT_sel ==16) || (DCT_sel ==19) || (DCT_sel ==21) || (DCT_sel ==24) || (DCT_sel ==26) || (DCT_sel ==29) || (DCT_sel ==31) )  ?  -outp_25 : outp_25;
					out_26 <= ( (DCT_sel ==1) || (DCT_sel ==4) || (DCT_sel ==6) || (DCT_sel ==8) || (DCT_sel ==9) || (DCT_sel ==11) || (DCT_sel ==13) || (DCT_sel ==16) || (DCT_sel ==18) || (DCT_sel ==20) || (DCT_sel ==21) || (DCT_sel ==23) || (DCT_sel ==25) || (DCT_sel ==28) || (DCT_sel ==30) )  ?  -outp_26 : outp_26;
					out_27 <= ( (DCT_sel ==1) || (DCT_sel ==3) || (DCT_sel ==4) || (DCT_sel ==6) || (DCT_sel ==8) || (DCT_sel ==10) || (DCT_sel ==11) || (DCT_sel ==13) || (DCT_sel ==15) || (DCT_sel ==17) || (DCT_sel ==18) || (DCT_sel ==20) || (DCT_sel ==22) || (DCT_sel ==24) || (DCT_sel ==25) || (DCT_sel ==27) || (DCT_sel ==29) || (DCT_sel ==31) )  ?  -outp_27 : outp_27;
					out_28 <= ( (DCT_sel ==1) || (DCT_sel ==3) || (DCT_sel ==6) || (DCT_sel ==8) || (DCT_sel ==10) || (DCT_sel ==12) || (DCT_sel ==15) || (DCT_sel ==17) || (DCT_sel ==19) || (DCT_sel ==21) || (DCT_sel ==24) || (DCT_sel ==26) || (DCT_sel ==28) || (DCT_sel ==30) )  ?  -outp_28 : outp_28;
					out_29 <= ( (DCT_sel ==1) || (DCT_sel ==3) || (DCT_sel ==5) || (DCT_sel ==8) || (DCT_sel ==10) || (DCT_sel ==12) || (DCT_sel ==14) || (DCT_sel ==16) || (DCT_sel ==18) || (DCT_sel ==21) || (DCT_sel ==23) || (DCT_sel ==25) || (DCT_sel ==27) || (DCT_sel ==29) || (DCT_sel ==31) )  ?  -outp_29 : outp_29;
					out_30 <= ( (DCT_sel ==1) || (DCT_sel ==3) || (DCT_sel ==5) || (DCT_sel ==7) || (DCT_sel ==9) || (DCT_sel ==12) || (DCT_sel ==14) || (DCT_sel ==16) || (DCT_sel ==18) || (DCT_sel ==20) || (DCT_sel ==22) || (DCT_sel ==24) || (DCT_sel ==26) || (DCT_sel ==28) || (DCT_sel ==30) )  ?  -outp_30 : outp_30;
					out_31 <= ( (DCT_sel ==1) || (DCT_sel ==3) || (DCT_sel ==5) || (DCT_sel ==7) || (DCT_sel ==9) || (DCT_sel ==11) || (DCT_sel ==13) || (DCT_sel ==15) || (DCT_sel ==17) || (DCT_sel ==19) || (DCT_sel ==21) || (DCT_sel ==23) || (DCT_sel ==25) || (DCT_sel ==27) || (DCT_sel ==29) || (DCT_sel ==31) )  ?  -outp_31 : outp_31;

				end else begin
					out_0 <= outp_0;
					out_1 <= ( (DST_sel ==2) || (DST_sel ==3) ) ? -outp_1 : outp_1;
					out_2 <= ( (DST_sel ==2)  ) ? -outp_2 : outp_2;
					out_3 <= ( (DST_sel ==1) || (DST_sel ==3) ) ? -outp_3 : outp_3;
				end
				if (transform_skip_flag) begin
					out_bypass <= (tr_stage == 1'b0) ? (x64 << 1) : (x64 << 1); // x64 = coeff<<6 therefore 3 and 4 more shifts needed to t 
				end else if (transquant_bypass_flag) begin
					out_bypass <= (tr_stage == 1'b0) ? (x64 << 8) : (x64 << 6);
				end
			end
			// pipeline register updates
			out_x <= x;
			out_y <= y;
			out_cIdx <= cIdx;
			out_trafoSize <= trafoSize;
			out_trType <= trType;
			out_transform_skip_flag <= transform_skip_flag;
			out_transquant_bypass_flag <= transquant_bypass_flag;
			out_tr_stage <= tr_stage;
			out_tr_valid <= tr_valid;
			out_new_row <= new_row;
			out_end_row <= end_row;
		end
	end
	
endmodule
