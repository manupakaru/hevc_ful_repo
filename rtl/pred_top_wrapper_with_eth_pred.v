
`timescale 1ns / 1ps

module pred_top_wrapper_with_eth
    (
			wr_clk,
			clk,
        reset,
        enable,

        fifo_in,
        input_fifo_is_full,
        write_en_in,

        fifo_out,
        output_fifo_is_full,
        write_en_out,

        //luma residual fifo interface
        y_residual_fifo_in,
        y_residual_fifo_is_full_out,
        y_residual_fifo_write_en_in,

        //cb residual fifo interface
        cb_residual_fifo_in,
        cb_residual_fifo_is_full_out,
        cb_residual_fifo_write_en_in,

        //cr residual fifo interface
        cr_residual_fifo_in,
        cr_residual_fifo_is_full_out,
        cr_residual_fifo_write_en_in,
        
        //OUTPUT FIFO INTERFACES
        
        //luma dbf fifo interface
        y_dbf_fifo_is_full,
        y_dbf_fifo_data_out,
        y_dbf_fifo_wr_en_out,
        
        //cb dbf fifo interface
        cb_dbf_fifo_is_full,
        cb_dbf_fifo_data_out,
        cb_dbf_fifo_wr_en_out,
        
        //cr dbf fifo interface
        cr_dbf_fifo_is_full,
        cr_dbf_fifo_data_out,
        cr_dbf_fifo_wr_en_out,
        
		bs_fifo_data_out        ,
		bs_fifo_wr_en_out		,
		bs_fifo_full_in  		,
		
        mv_col_axi_awid,     
        mv_col_axi_awlen,    
        mv_col_axi_awsize,   
        mv_col_axi_awburst,  
        mv_col_axi_awlock,   
        mv_col_axi_awcache,  
        mv_col_axi_awprot,   
        mv_col_axi_awvalid,
        mv_col_axi_awaddr,
        mv_col_axi_awready,
        mv_col_axi_wstrb,
        mv_col_axi_wlast,
        mv_col_axi_wvalid,
        mv_col_axi_wdata,
        mv_col_axi_wready,
        // mv_col_axi_bid,
        mv_col_axi_bresp,
        mv_col_axi_bvalid,
        mv_col_axi_bready,
        
        mv_pref_axi_araddr              ,
        mv_pref_axi_arlen               ,
        mv_pref_axi_arsize              ,
        mv_pref_axi_arburst             ,
        mv_pref_axi_arprot              ,
        mv_pref_axi_arvalid             ,
        mv_pref_axi_arready             ,

        mv_pref_axi_rdata               ,
        mv_pref_axi_rresp               ,
        mv_pref_axi_rlast               ,
        mv_pref_axi_rvalid              ,
        mv_pref_axi_rready              ,

        mv_pref_axi_arlock              ,
        mv_pref_axi_arid                ,
        mv_pref_axi_arcache             ,

        ref_pix_axi_ar_addr         ,
        ref_pix_axi_ar_len          ,
        ref_pix_axi_ar_size         ,
        ref_pix_axi_ar_burst        ,
        ref_pix_axi_ar_prot         ,
        ref_pix_axi_ar_valid        ,
        ref_pix_axi_ar_ready        ,
        
        ref_pix_axi_r_data          ,
        ref_pix_axi_r_resp          ,
        ref_pix_axi_r_last          ,
        ref_pix_axi_r_valid         ,
        ref_pix_axi_r_ready         
		
		
		//,      
        // test interface        
        // y_res_added_int,
        // intra_y_predsample_4by4_x_reg,
        // intra_y_predsample_4by4_y_reg,
        // y_valid_addition_out,
        // cb_res_added_int,
        // cr_res_added_int,
        ,pred_state_low8b_out
		,inter_pred_stat_8b_out
		,mv_state_8bit_out
		,col_state_axi_write
		,intra_ready_out
		,y_rd_data_count_out
		,cb_rd_data_count_out
		,cr_rd_data_count_out
        ,wr_data_count_out
		,rd_cnf_data_count_out
		,cnf_fifo_out
		,yy_prog_empty_thresh_out
		,cb_prog_empty_thresh_out
		,cr_prog_empty_thresh_out
		,cnf_fifo_counter
		,y_residual_counter
		,yy_res_fifo_empty
		,intra_x_out 		
		,intra_y_out        
		,intra_4x4_valid_out
		,intra_luma_4x4_out 
		,inter_x_out 
		,inter_y_out 
		,inter_4x4_valid_out 
		,inter_luma_4x4_out 
		,comon_pre_lp_x_out 
		,comon_pre_lp_y_out 
		,comon_pre_lp_4x4_valid_out 
		,comon_pre_lp_luma_4x4_out 
		,test_xT_in_min_luma_filt
		,test_xT_in_min_luma_cache
		,test_yT_in_min_luma_filt
		,test_yT_in_min_luma_cache
		,test_luma_filter_out
		,test_luma_filter_ready
		,test_cache_addr
		,test_cache_luma_data
		,test_cache_valid_in	
		,test_ref_block_en	
		,test_cache_en	
		,test_ref_luma_data_4x4	
		,residual_read_inf	
		,res_pres_y_cb_cr_out	
		,current_poc_out	
		,cb_res_pres_wr_en		
		,cb_res_pres_rd_en		
		,cb_res_pres_empty		
		,cb_res_pres_full 		
		,cb_res_pres_din 			
		,cb_res_pres_dout			
		
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"
    `include "../sim/inter_axi_def.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                          FIFO_IN_WIDTH       = 32;
    localparam                          FIFO_OUT_WIDTH      = 32;  


//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
	
	 input 																				wr_clk;
    input                                   									clk;
    input                                   									reset;
    input                                   									enable;
									
										
    input           [FIFO_IN_WIDTH-1:0]     									fifo_in;
    output                                  									input_fifo_is_full;
    input                                   									write_en_in;
										
    output          [FIFO_OUT_WIDTH-1:0]    									fifo_out;
    input                                   									output_fifo_is_full;
    output                                  									write_en_out;
//    output          [REF_PIC_LIST_POC_DATA_WIDTH-1:0]       					temp_port;


    //luma residual fifo interface
    input       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     y_residual_fifo_in;
    output                                                                      y_residual_fifo_is_full_out;
    input                                                                       y_residual_fifo_write_en_in;
    //cb residual fifo interface
    input       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     cb_residual_fifo_in;
    output                                                                      cb_residual_fifo_is_full_out;
    input                                                                       cb_residual_fifo_write_en_in;
    //cr residual fifo interface                                
    input       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     cr_residual_fifo_in;
    output                                                                      cr_residual_fifo_is_full_out;
    input                                                                       cr_residual_fifo_write_en_in;

    // OUTPUT FIFO INTERFACES
    
    //luma dbf fifo interface
    input                                                                       								y_dbf_fifo_is_full;
    output  [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE + DBF_SAMPLE_XY_ADDR + DBF_SAMPLE_XY_ADDR - 1:0]	y_dbf_fifo_data_out;
    output                                                                      								y_dbf_fifo_wr_en_out;
    
    //cb dbf fifo interface
    input                                                                       								cb_dbf_fifo_is_full;
    output  [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]             								cb_dbf_fifo_data_out;
    output                                                                      								cb_dbf_fifo_wr_en_out;
    
    //cr dbf fifo interface
    input                                                                       								cr_dbf_fifo_is_full;
    output  [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]             								cr_dbf_fifo_data_out;
    output                                                                      								cr_dbf_fifo_wr_en_out;

    
    //--------------axi interface
    output                                          mv_col_axi_awid    ;// = 0;
    output      [7:0]                               mv_col_axi_awlen   ;// = MV_COL_AXI_AX_LEN;
    output      [2:0]                               mv_col_axi_awsize  ;// = MV_COL_AXI_AX_SIZE;
    output      [1:0]                               mv_col_axi_awburst ;// = `AX_BURST_INC;
    output                        	                mv_col_axi_awlock  ;// = `AX_LOCK_DEFAULT;
    output      [3:0]                               mv_col_axi_awcache ;// = `AX_CACHE_DEFAULT;
    output      [2:0]                               mv_col_axi_awprot  ;// = `AX_PROT_DATA;
    output                                          mv_col_axi_awvalid;
    output      [31:0]                              mv_col_axi_awaddr;

    input                       	                mv_col_axi_awready;

    // write data channel
    output      [64-1:0]                            mv_col_axi_wstrb;// = 64'hFFFFF_FFFFF_FFFFF_FFFFF_FFFFF;       // 40 bytes
    output                                          mv_col_axi_wlast;
    output                                          mv_col_axi_wvalid;
    output      [MV_COL_AXI_DATA_WIDTH -1:0]        mv_col_axi_wdata;

    input	                                        mv_col_axi_wready;

    //write response channel
    // input                       	                mv_col_axi_bid;
    input       [1:0]                               mv_col_axi_bresp;
    input                       	                mv_col_axi_bvalid;
    output                                          mv_col_axi_bready;  

    output	    [AXI_ADDR_WDTH-1:0]		            mv_pref_axi_araddr  ;
    output wire [7:0]					            mv_pref_axi_arlen   ;
    output wire	[2:0]					            mv_pref_axi_arsize  ;
    output wire [1:0]					            mv_pref_axi_arburst ;
    output wire [2:0]					            mv_pref_axi_arprot  ;
    output 	   						                mv_pref_axi_arvalid ;
    input 								            mv_pref_axi_arready ;
            
    input		[MV_COL_AXI_DATA_WIDTH-1:0]		    mv_pref_axi_rdata   ;
    input		[1:0]					            mv_pref_axi_rresp   ;
    input 								            mv_pref_axi_rlast   ;
    input								            mv_pref_axi_rvalid  ;
    output 						                    mv_pref_axi_rready  ;

    output                        	                mv_pref_axi_arlock  ;
    output                                          mv_pref_axi_arid    ;    
    output      [3:0]                               mv_pref_axi_arcache ;     
    
    // axi master interface         
    output      [AXI_ADDR_WDTH-1:0]                 ref_pix_axi_ar_addr     ;
    output      [7:0]                               ref_pix_axi_ar_len      ;
    output      [2:0]                               ref_pix_axi_ar_size     ;
    output      [1:0]                               ref_pix_axi_ar_burst    ;
    output      [2:0]                               ref_pix_axi_ar_prot     ;
    output                                          ref_pix_axi_ar_valid    ;
    input                                           ref_pix_axi_ar_ready    ;
                
    input       [AXI_CACHE_DATA_WDTH-1:0]           ref_pix_axi_r_data      ;
    input       [1:0]                               ref_pix_axi_r_resp      ;
    input                                           ref_pix_axi_r_last      ;
    input                                           ref_pix_axi_r_valid     ;
    output                                          ref_pix_axi_r_ready     ;   
    //-----------end of axi interface
    
    
/*     // test-interface
    output                                          y_valid_addition_out;

    output             [PIXEL_ADDR_LENGTH - 1:0]                               intra_y_predsample_4by4_x_reg;
    output             [PIXEL_ADDR_LENGTH - 1:0]                               intra_y_predsample_4by4_y_reg;

    output        [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]  y_res_added_int;
    output       [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]  cb_res_added_int;
    output       [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]  cr_res_added_int;
    */ 
	
	output [BS_FIFO_WIDTH-1:0]  bs_fifo_data_out       ;
    output                      bs_fifo_wr_en_out       ;
    input                       bs_fifo_full_in         ; // should ensure this line never asserted      
   	

    //test
    //output res_empty_out;
    
    output  [7:0]                                   pred_state_low8b_out;
    output  [7:0]                                   col_state_axi_write;
    output  [7:0]                                   inter_pred_stat_8b_out;
    output  [7:0]                                   mv_state_8bit_out;
    output  [5:0] 									intra_ready_out;
    output [12:0] wr_data_count_out;
    output [12:0] y_rd_data_count_out;
    output [12:0] cb_rd_data_count_out;
    output [12:0] cr_rd_data_count_out;
    output [10:0] rd_cnf_data_count_out;
	output [31:0] cnf_fifo_out;
	output [31:0] y_residual_counter;
	output [11:0] yy_prog_empty_thresh_out;
	output [11:0] cb_prog_empty_thresh_out;
	output [11:0] cr_prog_empty_thresh_out;
	//output reg [31:0] cnf_fifo_counter;
	output [31:0] cnf_fifo_counter;
	
	output [11:0] intra_x_out 			;
	output [11:0] intra_y_out          ;
	output intra_4x4_valid_out  ;
	output [128-1:0] intra_luma_4x4_out   ;
	
	output [11:0] inter_x_out;
	output [11:0] inter_y_out;
	output 			inter_4x4_valid_out;
	output [128-1:0] inter_luma_4x4_out;
	
	output [11:0] comon_pre_lp_x_out;
	output [11:0] comon_pre_lp_y_out;
	output 			comon_pre_lp_4x4_valid_out;
	output [128-1:0] comon_pre_lp_luma_4x4_out;
	output [128-1:0] test_ref_luma_data_4x4;
	
    output [9 - 1:0] test_xT_in_min_luma_filt;
    output [9 - 1:0] test_xT_in_min_luma_cache;
    output [9 - 1:0] test_yT_in_min_luma_cache;
    output [9 - 1:0] test_yT_in_min_luma_filt;	
    output        									test_luma_filter_ready;
    output        [256 -1:0]         test_luma_filter_out;    
	output [7-1:0] test_cache_addr;
	output [512-1:0] test_cache_luma_data;
	output test_cache_valid_in;
	output test_cache_en;
	output test_ref_block_en;
	output [7:0] residual_read_inf;
	output [2:0] res_pres_y_cb_cr_out;
	output [31:0] current_poc_out;
	output cb_res_pres_wr_en	;
	output cb_res_pres_rd_en    ;
	output cb_res_pres_empty    ;
	output cb_res_pres_full     ;
	output cb_res_pres_din 	    ;
	output cb_res_pres_dout	    ;
	
	
	
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

    wire         [FIFO_IN_WIDTH-1:0]        										pred_top_fifo_int;
    wire                                    										pred_top_input_fifo_is_empty_int;
    wire                                    										pred_top_input_fifo_is_prog_empty_int;
    wire                                    										pred_top_input_fifo_dyn_prog_empty_int;
    wire                                    										pred_top_input_fifo_read_en_int;

    wire         [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]        pred_top_y_residual_fifo_int;
    wire         [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]        pred_top_y_residual_fifo_out;
    wire                                    										pred_top_y_residual_fifo_is_empty_int;
    wire                                    										pred_top_y_residual_fifo_dyn_empty_int;
    wire                                    										pred_top_y_residual_fifo_read_en_int;

    wire         [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     	pred_top_cb_residual_fifo_int;
    wire         [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     	pred_top_cb_residual_fifo_out;
    wire                                    										pred_top_cb_residual_fifo_is_empty_int;
    wire                                    										pred_top_cb_residual_fifo_dyn_empty_int;
    wire                                    										pred_top_cb_residual_fifo_read_en_int;

    wire         [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]        pred_top_cr_residual_fifo_int;
    wire         [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]        pred_top_cr_residual_fifo_out;
    wire                                    										pred_top_cr_residual_fifo_is_empty_int;
    wire                                    										pred_top_cr_residual_fifo_dyn_empty_int;
    wire                                    										pred_top_cr_residual_fifo_read_en_int;
     
	
	wire res_present;
	wire intra_ready;
	output yy_res_fifo_empty;
	wire yy_res_fifo_full;
	wire cb_res_fifo_empty;
	wire cb_res_fifo_full;	
	wire cr_res_fifo_empty;
	wire cr_res_fifo_full;

	
	
	
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------

  
// synthesis translate_off	
fifo_inout_monitor_dclk 
#
(
 .WIDTH(32),
 .FILE_IN_WIDTH(32),
 .FILE_NAME("residual_to_inter"),
 .OUT_VERIFY(1),
 .DEBUG(0)
)
config_res_to_pred_monitor(
    .rclk(clk),
    .wclk(wr_clk),
    .reset(reset),
    .out(pred_top_fifo_int),
	.in(fifo_in),
    .empty(pred_top_input_fifo_is_empty_int),
    .rd_en(pred_top_input_fifo_read_en_int),
	.wr_en(write_en_in),
	.full(config_fifo_full)
);
// synthesis translate_on
	
 prediction_config_data_fifo_in_id_clk prediction_config_data_fifo_in_block (
  .rst(reset), // input rst
  .wr_clk(wr_clk), // input wr_clk
  .rd_clk(clk), // input rd_clk
  .din(fifo_in), // input [31 : 0] din
  .wr_en(write_en_in), // input wr_en
  .rd_en(pred_top_input_fifo_read_en_int), // input rd_en
  .dout(pred_top_fifo_int), // output [31 : 0] dout
  .full(config_fifo_full), // output full
  .empty(pred_top_input_fifo_is_empty_int), // output empty'
  // .almost_empty(pred_top_input_fifo_is_prog_empty_int),
  .prog_empty(pred_top_input_fifo_is_prog_empty_int),
  .rd_data_count(rd_cnf_data_count_out), // output [10 : 0] rd_data_count
  .prog_full(input_fifo_is_full) // output prog_full
);   



// synthesis translate_off	
residual_fifo_monitor 
#
(
 .STRING("Y"),
 .FILE_NAME("residual_to_inter_Y"),
 .OUT_VERIFY(1),
 .DEBUG(0)
)
y_res_monitor(
    .clk(clk),
    .reset(reset),
    .out(pred_top_y_residual_fifo_out),
    .empty(yy_res_fifo_empty),
    .rd_en(pred_top_y_residual_fifo_read_en_int)
);
// synthesis translate_on

prediction_residual_data_fifo_in_id_clk prediction_y_residual_data_fifo_in_block (
  .rst(reset), // input rst
  .wr_clk(wr_clk), // input wr_clk
  .rd_clk(clk), // input rd_clk
  .din(y_residual_fifo_in), // input [143 : 0] din
  .rd_data_count(y_rd_data_count_out), // output [12 : 0] rd_data_count
  //.wr_data_count(wr_data_count_out), // output [12 : 0] wr_data_count
  .wr_en(y_residual_fifo_write_en_in), // input wr_en
  .rd_en(pred_top_y_residual_fifo_read_en_int), // input rd_en
  // .prog_empty_thresh(yy_prog_empty_thresh_out), // input [11 : 0] prog_empty_thresh
  .prog_empty_thresh(12'd200), // input [11 : 0] prog_empty_thresh
  .dout(pred_top_y_residual_fifo_out), // output [143 : 0] dout
  .full(yy_res_fifo_full), // output full
  .empty(yy_res_fifo_empty), // output empty
  .prog_full(y_residual_fifo_is_full_out) ,// output prog_full
  .prog_empty(pred_top_y_residual_fifo_is_empty_int) // output prog_empty
);	
	


// synthesis translate_off
residual_fifo_monitor 
#
(
 .STRING("Cb"),
 .FILE_NAME("residual_to_inter_Cb"),
 .OUT_VERIFY(1),
 .DEBUG(0)
)
cb_res_monitor(
    .clk(clk),
    .reset(reset),
    .out(pred_top_cb_residual_fifo_out),
    .empty(cb_res_fifo_empty),
    .rd_en(pred_top_cb_residual_fifo_read_en_int)
);
// synthesis translate_on
	
	
pred_res_chroma_data_fifo_id_clk prediction_cb_residual_data_fifo_in_block (
  .rst(reset), // input rst
  .wr_clk(wr_clk), // input wr_clk
  .rd_clk(clk), // input rd_clk
  .din(cb_residual_fifo_in), // input [143 : 0] din
  .rd_data_count(cb_rd_data_count_out), // output [10 : 0] rd_data_count
  .wr_en(cb_residual_fifo_write_en_in), // input wr_en
  .rd_en(pred_top_cb_residual_fifo_read_en_int), // input rd_en
  // .prog_empty_thresh(cb_prog_empty_thresh_out), // input [9 : 0] prog_empty_thresh
  .prog_empty_thresh(10'd200), // input [9 : 0] prog_empty_thresh
  .dout(pred_top_cb_residual_fifo_out), // output [143 : 0] dout
  .full(cb_res_fifo_full), // output full
  .empty(cb_res_fifo_empty), // output empty
  .prog_full(cb_residual_fifo_is_full_out), // output prog_full
  .prog_empty(pred_top_cb_residual_fifo_is_empty_int) // output prog_empty
);	



  

// synthesis translate_off
residual_fifo_monitor 
#
(
 .STRING("Cr"),
 .FILE_NAME("residual_to_inter_Cr"),
 .OUT_VERIFY(1),
 .DEBUG(0)
)
cr_res_monitor(
    .clk(clk),
    .reset(reset),
    .out(pred_top_cr_residual_fifo_out),
    .empty(cr_res_fifo_empty),
    .rd_en(pred_top_cr_residual_fifo_read_en_int)
);
// synthesis translate_on	
	
pred_res_chroma_data_fifo_id_clk prediction_cr_residual_data_fifo_in_block (
  .rst(reset), // input rst
  .wr_clk(wr_clk), // input wr_clk
  .rd_clk(clk), // input rd_clk
  .din(cr_residual_fifo_in), // input [143 : 0] din
  .wr_en(cr_residual_fifo_write_en_in), // input wr_en
  .rd_en(pred_top_cr_residual_fifo_read_en_int), // input rd_en
  .rd_data_count(cr_rd_data_count_out), // output [10 : 0] rd_data_count
  // .prog_empty_thresh(cr_prog_empty_thresh_out), // input [9 : 0] prog_empty_thresh
  .prog_empty_thresh(10'd200), // input [9 : 0] prog_empty_thresh
  .dout(pred_top_cr_residual_fifo_out), // output [143 : 0] dout
  .full(cr_res_fifo_full), // output full
  .empty(cr_res_fifo_empty), // output empty
  .prog_full(cr_residual_fifo_is_full_out), // output prog_full
  .prog_empty(pred_top_cr_residual_fifo_is_empty_int) // output prog_empty
);	



generate
	genvar i,j;
	for(i=0; i<OUTPUT_BLOCK_SIZE; i=i+1) begin
		for(j=0; j<OUTPUT_BLOCK_SIZE; j=j+1) begin
			assign pred_top_y_residual_fifo_int[RESIDUAL_WIDTH*(i+4*j+1)-1:RESIDUAL_WIDTH*(i+4*j)]  = pred_top_y_residual_fifo_out[RESIDUAL_WIDTH*(i+4*j+1)-1:RESIDUAL_WIDTH*(i+4*j)];
			assign pred_top_cb_residual_fifo_int[RESIDUAL_WIDTH*(i+4*j+1)-1:RESIDUAL_WIDTH*(i+4*j)]  = pred_top_cb_residual_fifo_out[RESIDUAL_WIDTH*(i+4*j+1)-1:RESIDUAL_WIDTH*(i+4*j)];
			assign pred_top_cr_residual_fifo_int[RESIDUAL_WIDTH*(i+4*j+1)-1:RESIDUAL_WIDTH*(i+4*j)]  = pred_top_cr_residual_fifo_out[RESIDUAL_WIDTH*(i+4*j+1)-1:RESIDUAL_WIDTH*(i+4*j)];
		end
	end
endgenerate

    pred_top pred_top_block
    (
        .clk 							(clk),
        .reset 							(reset),
        .enable							(enable),

        .fifo_in 						(pred_top_fifo_int),
        .input_fifo_is_empty  			(pred_top_input_fifo_is_prog_empty_int),
        .read_en_out 					(pred_top_input_fifo_read_en_int),

        .fifo_out 						(fifo_out),
        .output_fifo_is_full  			(output_fifo_is_full),
        .write_en_out 					(write_en_out),

	
        // bs interface

        .bs_fifo_data_out    			(bs_fifo_data_out  ),
        .bs_fifo_wr_en_out   			(bs_fifo_wr_en_out ),
        .bs_fifo_full_in     			(bs_fifo_full_in   ),
		
        //luma residual fifo interface
        .y_residual_fifo_in             (pred_top_y_residual_fifo_int),
        // .y_residual_fifo_is_empty_in    (pred_top_y_residual_fifo_is_empty_int),
        .y_residual_fifo_is_empty_out    (pred_top_y_residual_fifo_dyn_empty_int),
        .y_residual_fifo_read_en_out    (pred_top_y_residual_fifo_read_en_int),
		.yy_prog_empty_thresh_out			(yy_prog_empty_thresh_out),

        //cb residual fifo interface
        .cb_residual_fifo_in            (pred_top_cb_residual_fifo_int),
        // .cb_residual_fifo_is_empty_in   (pred_top_cb_residual_fifo_is_empty_int),
        .cb_residual_fifo_is_empty_out   (pred_top_cb_residual_fifo_dyn_empty_int),
        .cb_residual_fifo_read_en_out   (pred_top_cb_residual_fifo_read_en_int),
		.cb_prog_empty_thresh_out			(cb_prog_empty_thresh_out),

        //cr residual fifo interface
        .cr_residual_fifo_in            (pred_top_cr_residual_fifo_int),
        .cr_residual_fifo_is_empty_out   (pred_top_cr_residual_fifo_dyn_empty_int),
        // .cr_residual_fifo_is_empty_in   (pred_top_cr_residual_fifo_is_empty_int),
        .cr_residual_fifo_read_en_out   (pred_top_cr_residual_fifo_read_en_int),
		.cr_prog_empty_thresh_out			(cr_prog_empty_thresh_out),
        
        //OUTPUT FIFO INTERFACES
        
        //luma dbf fifo interface
        .y_dbf_fifo_is_full				(y_dbf_fifo_is_full),
        .y_dbf_fifo_data_out            (y_dbf_fifo_data_out),
        .y_dbf_fifo_wr_en_out           (y_dbf_fifo_wr_en_out),
        
        //cb dbf fifo interface
        .cb_dbf_fifo_is_full			(cb_dbf_fifo_is_full),
        .cb_dbf_fifo_data_out            (cb_dbf_fifo_data_out),
        .cb_dbf_fifo_wr_en_out           (cb_dbf_fifo_wr_en_out),
        
        //cr dbf fifo interface
        .cr_dbf_fifo_is_full 			(cr_dbf_fifo_is_full),
        .cr_dbf_fifo_data_out            (cr_dbf_fifo_data_out),
        .cr_dbf_fifo_wr_en_out           (cr_dbf_fifo_wr_en_out),
        
        .mv_col_axi_awid 				(mv_col_axi_awid 	),     
        .mv_col_axi_awlen 				(mv_col_axi_awlen ),    
        .mv_col_axi_awsize  			(mv_col_axi_awsize ),   
        .mv_col_axi_awburst 			(mv_col_axi_awburst),  
        .mv_col_axi_awlock 				(mv_col_axi_awlock ),   
        .mv_col_axi_awcache 			(mv_col_axi_awcache),  
        .mv_col_axi_awprot  			(mv_col_axi_awprot ),   
        .mv_col_axi_awvalid  			(mv_col_axi_awvalid),
        .mv_col_axi_awaddr 				(mv_col_axi_awaddr ),
        .mv_col_axi_awready 			(mv_col_axi_awready),
        .mv_col_axi_wstrb  				(mv_col_axi_wstrb  ),
        .mv_col_axi_wlast 				(mv_col_axi_wlast ),
        .mv_col_axi_wvalid  			(mv_col_axi_wvalid ),
        .mv_col_axi_wdata 				(mv_col_axi_wdata ),
        .mv_col_axi_wready 				(mv_col_axi_wready ),
        // mv_col_axi_bid,
        .mv_col_axi_bresp 				(mv_col_axi_bresp ),
        .mv_col_axi_bvalid 				(mv_col_axi_bvalid ),
        .mv_col_axi_bready  			(mv_col_axi_bready ),
        
        .mv_pref_axi_araddr             (mv_pref_axi_araddr ),	
        .mv_pref_axi_arlen              (mv_pref_axi_arlen  ),
        .mv_pref_axi_arsize             (mv_pref_axi_arsize ),
        .mv_pref_axi_arburst            (mv_pref_axi_arburst),
        .mv_pref_axi_arprot             (mv_pref_axi_arprot ),
        .mv_pref_axi_arvalid            (mv_pref_axi_arvalid),
        .mv_pref_axi_arready            (mv_pref_axi_arready),

        .mv_pref_axi_rdata              (mv_pref_axi_rdata  ),
        .mv_pref_axi_rresp              (mv_pref_axi_rresp  ),
        .mv_pref_axi_rlast              (mv_pref_axi_rlast  ),
        .mv_pref_axi_rvalid             (mv_pref_axi_rvalid ),
        .mv_pref_axi_rready             (mv_pref_axi_rready ),

        .mv_pref_axi_arlock             (mv_pref_axi_arlock ),
        .mv_pref_axi_arid               (mv_pref_axi_arid   ),
        .mv_pref_axi_arcache            (mv_pref_axi_arcache),

        .ref_pix_axi_ar_addr            (ref_pix_axi_ar_addr ),
        .ref_pix_axi_ar_len             (ref_pix_axi_ar_len  ),
        .ref_pix_axi_ar_size            (ref_pix_axi_ar_size ),
        .ref_pix_axi_ar_burst        	(ref_pix_axi_ar_burst),
        .ref_pix_axi_ar_prot         	(ref_pix_axi_ar_prot ),
        .ref_pix_axi_ar_valid        	(ref_pix_axi_ar_valid),
        .ref_pix_axi_ar_ready        	(ref_pix_axi_ar_ready),
        
        .ref_pix_axi_r_data          	(ref_pix_axi_r_data ),
        .ref_pix_axi_r_resp          	(ref_pix_axi_r_resp ),
        .ref_pix_axi_r_last          	(ref_pix_axi_r_last ),
        .ref_pix_axi_r_valid         	(ref_pix_axi_r_valid),
        .ref_pix_axi_r_ready         	(ref_pix_axi_r_ready)
		  
`ifdef CHIPSCOPE_DEBUG
        //,   
		,.y_rd_data_count_in	(y_rd_data_count_out  )
		,.cb_rd_data_count_in	(cb_rd_data_count_out  )
		,.cr_rd_data_count_in	(cr_rd_data_count_out  )        
        
        // test interface        
        // .y_res_added_int				(y_res_added_int			),
        // .intra_y_predsample_4by4_x_reg	(intra_y_predsample_4by4_x_reg),
        // .intra_y_predsample_4by4_y_reg	(intra_y_predsample_4by4_y_reg),
        // .y_valid_addition_out			(y_valid_addition_out		),
        // .cb_res_added_int				(cb_res_added_int			),
        // .cr_res_added_int				(cr_res_added_int			),
        ,.pred_state_low8b_out           (pred_state_low8b_out)
		,.inter_pred_stat_8b_out(inter_pred_stat_8b_out)
		,.mv_state_8bit_out(mv_state_8bit_out)
		,.col_state_axi_write(col_state_axi_write)
		
		,.cnf_fifo_out	(cnf_fifo_out)
		,.cnf_fifo_counter	(cnf_fifo_counter)
		,.intra_ready_out(intra_ready_out)
		,.residual_read_inf(residual_read_inf)
		
		
		,.intra_x_out 			(intra_x_out )
		,.intra_y_out           (intra_y_out )
		,.intra_4x4_valid_out   (intra_4x4_valid_out)
		,.intra_luma_4x4_out    (intra_luma_4x4_out )
		,.inter_x_out 			(inter_x_out )
		,.inter_y_out           (inter_y_out )
		,.inter_4x4_valid_out   (inter_4x4_valid_out)
		,.inter_luma_4x4_out    (inter_luma_4x4_out )
		,.comon_pre_lp_x_out 			(comon_pre_lp_x_out )
		,.comon_pre_lp_y_out           (comon_pre_lp_y_out )
		,.comon_pre_lp_4x4_valid_out   (comon_pre_lp_4x4_valid_out)
		,.comon_pre_lp_luma_4x4_out    (comon_pre_lp_luma_4x4_out )		
		,.test_xT_in_min_luma_filt   (test_xT_in_min_luma_filt	)
		,.test_xT_in_min_luma_cache  (test_xT_in_min_luma_cache )
		,.test_yT_in_min_luma_cache  (test_yT_in_min_luma_cache )
		,.test_yT_in_min_luma_filt   (test_yT_in_min_luma_filt  )
		,.test_luma_filter_ready     (test_luma_filter_ready    )
		,.test_luma_filter_out       (test_luma_filter_out      )
		,.test_cache_addr            (test_cache_addr           )
		,.test_cache_luma_data       (test_cache_luma_data      )
		,.test_cache_valid_in		 (test_cache_valid_in		)		
		,.test_cache_en		 		 (test_cache_en)		
		,.test_ref_block_en		 		 (test_ref_block_en)		
		,.test_ref_luma_data_4x4		 		 (test_ref_luma_data_4x4)		
		,.res_pres_y_cb_cr_out		 		 (res_pres_y_cb_cr_out)		
		,.current_poc_out		 		 (current_poc_out)	
		,.cb_res_pres_wr_en		(cb_res_pres_wr_en	)
		,.cb_res_pres_rd_en     (cb_res_pres_rd_en  )
		,.cb_res_pres_empty     (cb_res_pres_empty  )
		,.cb_res_pres_full      (cb_res_pres_full   )
		,.cb_res_pres_din 	    (cb_res_pres_din 	)
		,.cb_res_pres_dout	    (cb_res_pres_dout	)
		,.y_residual_counter		(y_residual_counter)
`endif	
		
        );
     

    
   
    
    
endmodule