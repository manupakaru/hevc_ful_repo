`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:29:36 02/09/2014 
// Design Name: 
// Module Name:    pred_dbf_wrapper 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module pred_dbf_wrapper(

	clk,
	reset,
    enable,
   
    fifo_in,
    input_fifo_is_full,
    write_en_in,
	
    //luma residual fifo interface
    y_residual_fifo_in,
    y_residual_fifo_is_full_out,
    y_residual_fifo_write_en_in,
    
    cb_residual_fifo_in,
    cb_residual_fifo_is_full_out,
    cb_residual_fifo_write_en_in,
    
    cr_residual_fifo_in,
    cr_residual_fifo_is_full_out,
    cr_residual_fifo_write_en_in,
        
	display_fifo_is_empty_out,
	display_fifo_rd_en_in,
	display_fifo_data_out,		
			
	dpb_fifo_is_empty_out,
	dpb_fifo_rd_en_in,
	dpb_fifo_data_out,    
	
	dpb_axi_addr_out,
	sao_poc_out,
	
	log2_ctu_size_out,
	pic_width_out,
	pic_height_out,
	
	mv_col_axi_awid,     
	mv_col_axi_awlen,    
	mv_col_axi_awsize,   
	mv_col_axi_awburst,  
	mv_col_axi_awlock,   
	mv_col_axi_awcache,  
	mv_col_axi_awprot,   
	mv_col_axi_awvalid,
	mv_col_axi_awaddr,
	mv_col_axi_awready,
	mv_col_axi_wstrb,
	mv_col_axi_wlast,
	mv_col_axi_wvalid,
	mv_col_axi_wdata,
	mv_col_axi_wready,
	// mv_col_axi_bid,
	mv_col_axi_bresp,
	mv_col_axi_bvalid,
	mv_col_axi_bready,
	
	mv_pref_axi_araddr              ,
	mv_pref_axi_arlen               ,
	mv_pref_axi_arsize              ,
	mv_pref_axi_arburst             ,
	mv_pref_axi_arprot              ,
	mv_pref_axi_arvalid             ,
	mv_pref_axi_arready             ,

	mv_pref_axi_rdata               ,
	mv_pref_axi_rresp               ,
	mv_pref_axi_rlast               ,
	mv_pref_axi_rvalid              ,
	mv_pref_axi_rready              ,

	mv_pref_axi_arlock              ,
	mv_pref_axi_arid                ,
	mv_pref_axi_arcache             ,

	ref_pix_axi_ar_addr         ,
	ref_pix_axi_ar_len          ,
	ref_pix_axi_ar_size         ,
	ref_pix_axi_ar_burst        ,
	ref_pix_axi_ar_prot         ,
	ref_pix_axi_ar_valid        ,
	ref_pix_axi_ar_ready        ,
	
	ref_pix_axi_r_data          ,
	ref_pix_axi_r_resp          ,
	ref_pix_axi_r_last          ,
	ref_pix_axi_r_valid         ,
	ref_pix_axi_r_ready         
	
        ,pred_state_low8b_out
		,inter_pred_stat_8b_out
		,mv_state_8bit_out
		,pred_2_dbf_wr_inf
		,pred_2_dbf_yy
		,Xc_Yc_out
		,intra_ready_out
		
    );

`include "../sim/pred_def.v"
`include "../sim/inter_axi_def.v"



//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                          FIFO_IN_WIDTH       = 32;
    localparam                          FIFO_OUT_WIDTH      = 32;  

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input clk;
    input reset;
    input                                   enable;
    
    input           [FIFO_IN_WIDTH-1:0]     fifo_in;
    output                                  input_fifo_is_full;
    input                               	write_en_in;
	
    //luma residual fifo interface
    input       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     y_residual_fifo_in;
    output                                                                      y_residual_fifo_is_full_out;
    input                                                                       y_residual_fifo_write_en_in;
    //cb residual fifo interface
    input       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     cb_residual_fifo_in;
    output                                                                      cb_residual_fifo_is_full_out;
    input                                                                       cb_residual_fifo_write_en_in;
    //cr residual fifo interface                                
    input       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     cr_residual_fifo_in;
    output                                                                      cr_residual_fifo_is_full_out;
    input                                                                       cr_residual_fifo_write_en_in;


	output 								display_fifo_is_empty_out;
	input  								display_fifo_rd_en_in;
	output  [SAO_OUT_FIFO_WIDTH-1:0]	display_fifo_data_out;
	
	output 								dpb_fifo_is_empty_out;
	input  								dpb_fifo_rd_en_in;
	output  [SAO_OUT_FIFO_WIDTH-1:0]	dpb_fifo_data_out;    
	
	output [AXI_ADDR_WDTH -1:0]			dpb_axi_addr_out;
	output [POC_WIDTH -1:0]			sao_poc_out;
    output     [MAX_LOG2CTBSIZE_WIDTH - 1:0]              	  			  log2_ctu_size_out;
	
    output     [PIC_WIDTH_WIDTH-1:0]          							  pic_width_out;
    output     [PIC_WIDTH_WIDTH-1:0]          							  pic_height_out;

    //--------------axi interface
    output                                          mv_col_axi_awid    ;// = 0;
    output      [7:0]                               mv_col_axi_awlen   ;// = MV_COL_AXI_AX_LEN;
    output      [2:0]                               mv_col_axi_awsize  ;// = MV_COL_AXI_AX_SIZE;
    output      [1:0]                               mv_col_axi_awburst ;// = `AX_BURST_INC;
    output                        	                mv_col_axi_awlock  ;// = `AX_LOCK_DEFAULT;
    output      [3:0]                               mv_col_axi_awcache ;// = `AX_CACHE_DEFAULT;
    output      [2:0]                               mv_col_axi_awprot  ;// = `AX_PROT_DATA;
    output                                          mv_col_axi_awvalid;
    output      [31:0]                              mv_col_axi_awaddr;

    input                       	                mv_col_axi_awready;

    // write data channel
    output      [64-1:0]                            mv_col_axi_wstrb;// = 64'hFFFFF_FFFFF_FFFFF_FFFFF_FFFFF;       // 40 bytes
    output                                          mv_col_axi_wlast;
    output                                          mv_col_axi_wvalid;
    output      [MV_COL_AXI_DATA_WIDTH -1:0]        mv_col_axi_wdata;

    input	                                        mv_col_axi_wready;

    //write response channel
    // input                       	                mv_col_axi_bid;
    input       [1:0]                               mv_col_axi_bresp;
    input                       	                mv_col_axi_bvalid;
    output                                          mv_col_axi_bready;  

    output	    [AXI_ADDR_WDTH-1:0]		            mv_pref_axi_araddr  ;
    output wire [7:0]					            mv_pref_axi_arlen   ;
    output wire	[2:0]					            mv_pref_axi_arsize  ;
    output wire [1:0]					            mv_pref_axi_arburst ;
    output wire [2:0]					            mv_pref_axi_arprot  ;
    output 	   						                mv_pref_axi_arvalid ;
    input 								            mv_pref_axi_arready ;
            
    input		[MV_COL_AXI_DATA_WIDTH-1:0]		    mv_pref_axi_rdata   ;
    input		[1:0]					            mv_pref_axi_rresp   ;
    input 								            mv_pref_axi_rlast   ;
    input								            mv_pref_axi_rvalid  ;
    output 						                    mv_pref_axi_rready  ;

    output                        	                mv_pref_axi_arlock  ;
    output                                          mv_pref_axi_arid    ;    
    output      [3:0]                               mv_pref_axi_arcache ;     
    
    // axi master interface         
    output      [AXI_ADDR_WDTH-1:0]                 ref_pix_axi_ar_addr     ;
    output      [7:0]                               ref_pix_axi_ar_len      ;
    output      [2:0]                               ref_pix_axi_ar_size     ;
    output      [1:0]                               ref_pix_axi_ar_burst    ;
    output      [2:0]                               ref_pix_axi_ar_prot     ;
    output                                          ref_pix_axi_ar_valid    ;
    input                                           ref_pix_axi_ar_ready    ;
                
    input       [AXI_CACHE_DATA_WDTH-1:0]           ref_pix_axi_r_data      ;
    input       [1:0]                               ref_pix_axi_r_resp      ;
    input                                           ref_pix_axi_r_last      ;
    input                                           ref_pix_axi_r_valid     ;
    output                                          ref_pix_axi_r_ready     ;   
    //-----------end of axi interface
	
	output [7:0] pred_2_dbf_wr_inf;
    output  [7:0]                                   pred_state_low8b_out;
    output  [7:0]                                   inter_pred_stat_8b_out;
    output  [7:0]                                   mv_state_8bit_out;
	
	output [134-1:0] pred_2_dbf_yy;
	

	output [24-1:0] Xc_Yc_out;
    output  [5:0] 									intra_ready_out;
	//output [7:0] pred_2_dbf_rd_inf ;	
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//--------------------------------------------------------------------------------------------------------------------- 
		
	
	wire       		[FIFO_OUT_WIDTH-1:0]    	pred_to_dbf_config_fifo;
	wire       									pred_to_dbf_config_fifo_full;
	wire       									pred_to_dbf_config_fifo_wr_en;
	wire         [DPB_ADDR_WIDTH -1: 0]     	pred_to_dbf_current_pic_dpb_idx;    
	    
        //luma dbf fifo interface
    wire                                                                                        y_dbf_fifo_is_full_internal;
    wire   [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE + 2*DBF_SAMPLE_XY_ADDR - 1:0]       y_dbf_fifo_data_internal;
    wire                                                                                        y_dbf_fifo_wr_en_internal;
    
    //cb dbf fifo interface
    wire                                                                        cb_dbf_fifo_is_full_internal;
    wire    [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]             cb_dbf_fifo_data_internal;
    wire                                                                        cb_dbf_fifo_wr_en_internal;
    
    //cr dbf fifo interface
    wire                                                                        cr_dbf_fifo_is_full_internal;
    wire    [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]             cr_dbf_fifo_data_internal;
    wire                                                                        cr_dbf_fifo_wr_en_internal;
    
    
    wire [BS_FIFO_WIDTH-1:0]    bs_fifo_data_wire       ;
    wire                        bs_fifo_wr_en_wire       ;
    wire                        bs_fifo_full_wire         ; // should ensure this line never asserted   

	// assign pred_2_dbf_wr_inf = {pred_to_dbf_config_fifo_wr_en,y_dbf_fifo_wr_en_internal,cb_dbf_fifo_wr_en_internal,cr_dbf_fifo_wr_en_internal
								// ,pred_to_dbf_config_fifo_full,y_dbf_fifo_is_full_internal,cb_dbf_fifo_is_full_internal,cr_dbf_fifo_is_full_internal};
    
	
    // assign pred_2_dbf_yy = y_dbf_fifo_data_internal;	

	// Instantiate the module
pred_top_wrapper pred_wrap_block (
    .clk(clk), 
    .reset(reset), 
    .enable(enable), 
	
    .fifo_in			(fifo_in), 
    .input_fifo_is_full	(input_fifo_is_full), 
    .write_en_in		(write_en_in), 
    .fifo_out			(pred_to_dbf_config_fifo), 
    .output_fifo_is_full(pred_to_dbf_config_fifo_full), 
    .write_en_out		(pred_to_dbf_config_fifo_wr_en), 
	    
    .bs_fifo_data_out    (bs_fifo_data_wire), 
    .bs_fifo_wr_en_out   (bs_fifo_wr_en_wire), 
    .bs_fifo_full_in     (bs_fifo_full_wire) ,
	
    .y_residual_fifo_in				(y_residual_fifo_in), 
    .y_residual_fifo_is_full_out	(y_residual_fifo_is_full_out), 
    .y_residual_fifo_write_en_in	(y_residual_fifo_write_en_in), 
    .cb_residual_fifo_in			(cb_residual_fifo_in), 
    .cb_residual_fifo_is_full_out	(cb_residual_fifo_is_full_out), 
    .cb_residual_fifo_write_en_in	(cb_residual_fifo_write_en_in), 
    .cr_residual_fifo_in			(cr_residual_fifo_in), 
    .cr_residual_fifo_is_full_out	(cr_residual_fifo_is_full_out), 
    .cr_residual_fifo_write_en_in	(cr_residual_fifo_write_en_in), 
    
    
    .y_dbf_fifo_is_full				(y_dbf_fifo_is_full_internal), 
    .y_dbf_fifo_data_out			(y_dbf_fifo_data_internal), 
    .y_dbf_fifo_wr_en_out			(y_dbf_fifo_wr_en_internal), 
    .cb_dbf_fifo_is_full			(cb_dbf_fifo_is_full_internal), 
    .cb_dbf_fifo_data_out			(cb_dbf_fifo_data_internal), 
    .cb_dbf_fifo_wr_en_out			(cb_dbf_fifo_wr_en_internal), 
    .cr_dbf_fifo_is_full			(cr_dbf_fifo_is_full_internal), 
    .cr_dbf_fifo_data_out			(cr_dbf_fifo_data_internal), 
    .cr_dbf_fifo_wr_en_out			(cr_dbf_fifo_wr_en_internal), 
    
    .mv_col_axi_awid		(mv_col_axi_awid), 
    .mv_col_axi_awlen		(mv_col_axi_awlen), 
    .mv_col_axi_awsize		(mv_col_axi_awsize), 
    .mv_col_axi_awburst		(mv_col_axi_awburst), 
    .mv_col_axi_awlock		(mv_col_axi_awlock), 
    .mv_col_axi_awcache		(mv_col_axi_awcache), 
    .mv_col_axi_awprot		(mv_col_axi_awprot), 
    .mv_col_axi_awvalid		(mv_col_axi_awvalid), 
    .mv_col_axi_awaddr		(mv_col_axi_awaddr), 
    .mv_col_axi_awready		(mv_col_axi_awready), 
    .mv_col_axi_wstrb		(mv_col_axi_wstrb), 
    .mv_col_axi_wlast		(mv_col_axi_wlast), 
    .mv_col_axi_wvalid		(mv_col_axi_wvalid), 
    .mv_col_axi_wdata		(mv_col_axi_wdata), 
    .mv_col_axi_wready		(mv_col_axi_wready), 
    .mv_col_axi_bresp		(mv_col_axi_bresp), 
    .mv_col_axi_bvalid		(mv_col_axi_bvalid), 
    .mv_col_axi_bready		(mv_col_axi_bready), 
    .mv_pref_axi_araddr		(mv_pref_axi_araddr), 
    .mv_pref_axi_arlen		(mv_pref_axi_arlen), 
    .mv_pref_axi_arsize		(mv_pref_axi_arsize), 
    .mv_pref_axi_arburst	(mv_pref_axi_arburst), 
    .mv_pref_axi_arprot		(mv_pref_axi_arprot), 
    .mv_pref_axi_arvalid	(mv_pref_axi_arvalid), 
    .mv_pref_axi_arready	(mv_pref_axi_arready), 
    .mv_pref_axi_rdata		(mv_pref_axi_rdata), 
    .mv_pref_axi_rresp		(mv_pref_axi_rresp), 
    .mv_pref_axi_rlast		(mv_pref_axi_rlast), 
    .mv_pref_axi_rvalid		(mv_pref_axi_rvalid), 
    .mv_pref_axi_rready		(mv_pref_axi_rready), 
    .mv_pref_axi_arlock		(mv_pref_axi_arlock), 
    .mv_pref_axi_arid		(mv_pref_axi_arid), 
    .mv_pref_axi_arcache	(mv_pref_axi_arcache), 
    
    .ref_pix_axi_ar_addr    (ref_pix_axi_ar_addr), 
    .ref_pix_axi_ar_len     (ref_pix_axi_ar_len), 
    .ref_pix_axi_ar_size    (ref_pix_axi_ar_size), 
    .ref_pix_axi_ar_burst   (ref_pix_axi_ar_burst), 
    .ref_pix_axi_ar_prot    (ref_pix_axi_ar_prot), 
    .ref_pix_axi_ar_valid   (ref_pix_axi_ar_valid), 
    .ref_pix_axi_ar_ready   (ref_pix_axi_ar_ready), 
    .ref_pix_axi_r_data     (ref_pix_axi_r_data), 
    .ref_pix_axi_r_resp     (ref_pix_axi_r_resp), 
    .ref_pix_axi_r_last     (ref_pix_axi_r_last), 
    .ref_pix_axi_r_valid    (ref_pix_axi_r_valid), 
    .ref_pix_axi_r_ready    (ref_pix_axi_r_ready)
	
	
		,.pred_state_low8b_out           (pred_state_low8b_out)
		,.inter_pred_stat_8b_out(inter_pred_stat_8b_out)
		,.mv_state_8bit_out(mv_state_8bit_out)
		,.intra_ready_out	(intra_ready_out	)	
	
    );



// Instantiate the module
dbf_top dbf_block (
    .clk(clk), 
    .reset(reset),     
    .bs_fifo_data_in(bs_fifo_data_wire),
    .bs_fifo_full_out(bs_fifo_full_wire),
    .bs_fifo_wr_en_in(bs_fifo_wr_en_wire),
    
    .cnfig_dbf_fifo_is_full_out(pred_to_dbf_config_fifo_full), 
    .cnfig_dbf_fifo_data_in(pred_to_dbf_config_fifo), 
    .cnfig_dbf_fifo_wr_en_in(pred_to_dbf_config_fifo_wr_en), 
    
    .y_dbf_fifo_is_full_out		(y_dbf_fifo_is_full_internal), 
    .y_dbf_fifo_data_in			(y_dbf_fifo_data_internal), 
    .y_dbf_fifo_wr_en_in		(y_dbf_fifo_wr_en_internal), 
    .cb_dbf_fifo_is_full_out	(cb_dbf_fifo_is_full_internal), 
    .cb_dbf_fifo_data_in		(cb_dbf_fifo_data_internal), 
    .cb_dbf_fifo_wr_en_in		(cb_dbf_fifo_wr_en_internal), 
    .cr_dbf_fifo_is_full_out	(cr_dbf_fifo_is_full_internal), 
    .cr_dbf_fifo_data_in		(cr_dbf_fifo_data_internal), 
    .cr_dbf_fifo_wr_en_in		(cr_dbf_fifo_wr_en_internal), 
    
	.display_fifo_is_empty_out	(display_fifo_is_empty_out),
	.display_fifo_rd_en_in		(display_fifo_rd_en_in),				
	.display_fifo_data_out		(display_fifo_data_out),
	.dpb_fifo_is_empty_out		(dpb_fifo_is_empty_out),
	.dpb_fifo_rd_en_in			(dpb_fifo_rd_en_in),		
	.dpb_fifo_data_out			(dpb_fifo_data_out),
	.dpb_axi_addr_out			(dpb_axi_addr_out),
	.sao_poc_out			(sao_poc_out) ,
	
	.log2_ctu_size_out			(log2_ctu_size_out),	
	.pic_width_out				(pic_width_out),	
	.pic_height_out				(pic_height_out)
	
	,.Xc_Yc_out(Xc_Yc_out)
	,.pred_2_dbf_yy(pred_2_dbf_yy)
	,.pred_2_dbf_rd_inf(pred_2_dbf_wr_inf)

    );





endmodule
