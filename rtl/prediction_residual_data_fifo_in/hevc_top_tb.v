`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:53:32 01/01/2014
// Design Name:   hevc_top
// Module Name:   J:/hevc_final_intergration/hevc_top_v004/pred_top_total/rtl/prediction_residual_data_fifo_in/hevc_top_tb.v
// Project Name:  hevc_top
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: hevc_top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module hevc_top_tb;

	// Inputs
	reg glbl_rst;
	reg clk_in_p;
	wire clk_in_n;
	reg gtrefclk_p;
	reg gtrefclk_n;
	reg rxp;
	reg rxn;
	reg pause_req_s;
	reg [1:0] mac_speed;
	reg update_speed;
	reg config_board;
	reg gen_tx_data;
	reg chk_tx_data;
	reg reset_error;
	reg hevc_reset;

	// Outputs
	wire phy_resetn;
	wire txp;
	wire txn;
	wire synchronization_done;
	wire linkup;
	wire mdc;
	wire tx_statistics_s;
	wire rx_statistics_s;
	wire serial_response;
	wire frame_error;
	wire frame_errorn;
	wire activity_flash;
	wire activity_flashn;

	// Bidirs
	wire mdio;
    integer correct_res_added_file;

	// Instantiate the Unit Under Test (UUT)
	hevc_top uut (
		.glbl_rst(glbl_rst), 
		.clk_in_p(clk_in_p), 
		.clk_in_n(clk_in_n), 
		.phy_resetn(phy_resetn), 
		.gtrefclk_p(gtrefclk_p), 
		.gtrefclk_n(gtrefclk_n), 
		.txp(txp), 
		.txn(txn), 
		.rxp(rxp), 
		.rxn(rxn), 
		.synchronization_done(synchronization_done), 
		.linkup(linkup), 
		.mdio(mdio), 
		.mdc(mdc), 
		.tx_statistics_s(tx_statistics_s), 
		.rx_statistics_s(rx_statistics_s), 
		.pause_req_s(pause_req_s), 
		.mac_speed(mac_speed), 
		.update_speed(update_speed), 
		.config_board(config_board), 
		.serial_response(serial_response), 
		.gen_tx_data(gen_tx_data), 
		.chk_tx_data(chk_tx_data), 
		.reset_error(reset_error), 
		.frame_error(frame_error), 
		.frame_errorn(frame_errorn), 
		.activity_flash(activity_flash), 
		.activity_flashn(activity_flashn), 
		.hevc_reset(hevc_reset)

	);

	initial begin
		// Initialize Inputs
		glbl_rst = 0;
		clk_in_p = 0;
//		clk_in_n = 0;
		gtrefclk_p = 0;
		gtrefclk_n = 0;
		rxp = 0;
		rxn = 0;
		pause_req_s = 0;
		mac_speed = 0;
		update_speed = 0;
		config_board = 0;
		gen_tx_data = 0;
		chk_tx_data = 0;
		reset_error = 0;
		hevc_reset = 1;
        correct_res_added_file = $fopen("avatar_all_intra_pre_lp.yuv","rb");

		// Wait 100 ns for global reset to finish
		#100;
        hevc_reset = 0;
        
        
		// Add stimulus here

	end
    
    assign clk_in_n = ~clk_in_p;
    
    initial begin
        clk_in_p <= 1'b1;
        forever begin
            #2.5 clk_in_p <= ~clk_in_p;
        end
    end
    
    task intra_verify;
    integer i;
    integer j;
    reg [7:0] temp;
    begin

        if(uut.pred_top_wrapper_block.pred_top_block.y_valid_addition_reg == 1'b1) begin
            for (i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin
                for (j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin
                    $fseek(correct_res_added_file,((uut.pred_top_wrapper_block.pred_top_block.pic_width)*(j + uut.pred_top_wrapper_block.pred_top_block.intra_y_predsample_4by4_y_reg) + uut.pred_top_wrapper_block.pred_top_block.intra_y_predsample_4by4_x_reg + i),0);
                    temp = $fgetc(correct_res_added_file);

                    if (temp != uut.pred_top_wrapper_block.pred_top_block.y_res_added_int_arr[i][j]) begin
                        $display("correct_res_added_file : %d , uut.y_res_added_int_arr : %d \n at (y = %d, x = %d)",temp,uut.pred_top_wrapper_block.pred_top_block.y_res_added_int_arr[i][j],(j + uut.pred_top_wrapper_block.pred_top_block.intra_y_predsample_4by4_y_reg),(uut.pred_top_wrapper_block.pred_top_block.intra_y_predsample_4by4_x_reg + i));
                        $stop;
                    end
                end
            end
        end
        
        if(uut.pred_top_wrapper_block.pred_top_block.cb_valid_addition_reg == 1'b1) begin
            for (i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin
                for (j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin
                    $fseek(correct_res_added_file,((uut.pred_top_wrapper_block.pred_top_block.pic_width*uut.pred_top_wrapper_block.pred_top_block.pic_height)+(uut.pred_top_wrapper_block.pred_top_block.pic_width/2)*(j + uut.pred_top_wrapper_block.pred_top_block.intra_cb_predsample_4by4_y_reg) + uut.pred_top_wrapper_block.pred_top_block.intra_cb_predsample_4by4_x_reg + i),0);
                    temp = $fgetc(correct_res_added_file);

                    if (temp != uut.pred_top_wrapper_block.pred_top_block.cb_res_added_int_arr[i][j]) begin
                        $display("correct_res_added_file : %d , uut.cb_res_added_int_arr : %d \n at (y = %d, x = %d)",temp,uut.pred_top_wrapper_block.pred_top_block.cb_res_added_int_arr[i][j],(j + uut.pred_top_wrapper_block.pred_top_block.intra_cb_predsample_4by4_y_reg),(uut.pred_top_wrapper_block.pred_top_block.intra_cb_predsample_4by4_x_reg + i));
                        $stop;
                    end
                end
            end
        end
        
        if(uut.pred_top_wrapper_block.pred_top_block.cr_valid_addition_reg == 1'b1) begin
            for (i = 0 ; i < OUTPUT_BLOCK_SIZE ; i = i + 1) begin
                for (j = 0 ; j < OUTPUT_BLOCK_SIZE ; j = j + 1) begin
                    $fseek(correct_res_added_file,(((uut.pred_top_wrapper_block.pred_top_block.pic_width/2)*(uut.pred_top_wrapper_block.pred_top_block.pic_height/2))+(uut.pred_top_wrapper_block.pred_top_block.pic_width*uut.pred_top_wrapper_block.pred_top_block.pic_height)+(uut.pred_top_wrapper_block.pred_top_block.pic_width/2)*(j + uut.pred_top_wrapper_block.pred_top_block.intra_cr_predsample_4by4_y_reg) + uut.pred_top_wrapper_block.pred_top_block.intra_cr_predsample_4by4_x_reg + i),0);
                    temp = $fgetc(correct_res_added_file);

                    if (temp != uut.pred_top_wrapper_block.pred_top_block.cr_res_added_int_arr[i][j]) begin
                        $display("correct_res_added_file : %d , uut.cr_res_added_int_arr : %d \n at (y = %d, x = %d)",temp,uut.pred_top_wrapper_block.pred_top_block.cr_res_added_int_arr[i][j],(j + uut.pred_top_wrapper_block.pred_top_block.intra_cr_predsample_4by4_y_reg),(uut.pred_top_wrapper_block.pred_top_block.intra_cr_predsample_4by4_x_reg + i));
                        $stop;
                    end
                end
            end
        end
          
    end
endtask
      
endmodule

