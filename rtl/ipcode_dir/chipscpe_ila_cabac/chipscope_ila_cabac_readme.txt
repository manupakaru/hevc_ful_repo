The following files were generated for 'chipscope_ila_cabac' in directory
F:\GalaxyCores\HEVC\hevc_decoder_project\Dropbox\hdl_codes\hevc_ful_repo\rtl\ipcode_dir\chipscpe_ila_cabac\

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * chipscope_ila_cabac.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * chipscope_ila_cabac.cdc
   * chipscope_ila_cabac.constraints/chipscope_ila_cabac.ucf
   * chipscope_ila_cabac.constraints/chipscope_ila_cabac.xdc
   * chipscope_ila_cabac.ncf
   * chipscope_ila_cabac.ngc
   * chipscope_ila_cabac.ucf
   * chipscope_ila_cabac.v
   * chipscope_ila_cabac.veo
   * chipscope_ila_cabac.xdc
   * chipscope_ila_cabac_xmdf.tcl

Creates an HDL instantiation template:
   Creates an HDL instantiation template for the IP.

   * chipscope_ila_cabac.veo

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * chipscope_ila_cabac.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * chipscope_ila_cabac.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * chipscope_ila_cabac.gise
   * chipscope_ila_cabac.xise

Deliver Readme:
   Readme file for the IP.

   * chipscope_ila_cabac_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * chipscope_ila_cabac_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

