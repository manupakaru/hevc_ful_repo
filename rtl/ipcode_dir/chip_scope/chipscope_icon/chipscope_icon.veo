///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2014 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor     : Xilinx
// \   \   \/     Version    : 14.7
//  \   \         Application: Xilinx CORE Generator
//  /   /         Filename   : chipscope_icon.veo
// /___/   /\     Timestamp  : Tue Jul 29 11:56:03 Sri Lanka Standard Time 2014
// \   \  /  \
//  \___\/\___\
//
// Design Name: ISE Instantiation template
///////////////////////////////////////////////////////////////////////////////

// The following must be inserted into your Verilog file for this
// core to be instantiated. Change the instance name and port connections
// (in parentheses) to your own signal names.

//----------- Begin Cut here for INSTANTIATION Template ---// INST_TAG
chipscope_icon YourInstanceName (
    .CONTROL0(CONTROL0), // INOUT BUS [35:0]
    .CONTROL1(CONTROL1), // INOUT BUS [35:0]
    .CONTROL2(CONTROL2), // INOUT BUS [35:0]
    .CONTROL3(CONTROL3), // INOUT BUS [35:0]
    .CONTROL4(CONTROL4), // INOUT BUS [35:0]
    .CONTROL5(CONTROL5), // INOUT BUS [35:0]
    .CONTROL6(CONTROL6) // INOUT BUS [35:0]
);

// INST_TAG_END ------ End INSTANTIATION Template ---------

