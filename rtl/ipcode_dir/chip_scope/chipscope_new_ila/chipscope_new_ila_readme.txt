The following files were generated for 'chipscope_new_ila' in directory
F:\HEVC decoder\HDL codes\pred_top_total_repo_\rtl\ipcode_dir\chip_scope\chipscope_new_ila\

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * chipscope_new_ila.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * chipscope_new_ila.cdc
   * chipscope_new_ila.constraints/chipscope_new_ila.ucf
   * chipscope_new_ila.constraints/chipscope_new_ila.xdc
   * chipscope_new_ila.ncf
   * chipscope_new_ila.ngc
   * chipscope_new_ila.ucf
   * chipscope_new_ila.v
   * chipscope_new_ila.veo
   * chipscope_new_ila.xdc
   * chipscope_new_ila_xmdf.tcl

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * chipscope_new_ila.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * chipscope_new_ila.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * _xmsgs/pn_parser.xmsgs
   * chipscope_new_ila.gise
   * chipscope_new_ila.xise

Deliver Readme:
   Readme file for the IP.

   * chipscope_new_ila_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * chipscope_new_ila_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

