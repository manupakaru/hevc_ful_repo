///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2014 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor     : Xilinx
// \   \   \/     Version    : 14.7
//  \   \         Application: Xilinx CORE Generator
//  /   /         Filename   : chipscope_pixel_test.v
// /___/   /\     Timestamp  : Fri Jul 04 08:11:39 Sri Lanka Standard Time 2014
// \   \  /  \
//  \___\/\___\
//
// Design Name: Verilog Synthesis Wrapper
///////////////////////////////////////////////////////////////////////////////
// This wrapper is used to integrate with Project Navigator and PlanAhead

`timescale 1ns/1ps

module chipscope_pixel_test(
    CONTROL,
    CLK,
    TRIG0,
    TRIG1,
    TRIG2,
    TRIG3,
    TRIG4,
    TRIG5,
    TRIG6,
    TRIG7,
    TRIG8) /* synthesis syn_black_box syn_noprune=1 */;


inout [35 : 0] CONTROL;
input CLK;
input [127 : 0] TRIG0;
input [127 : 0] TRIG1;
input [127 : 0] TRIG2;
input [127 : 0] TRIG3;
input [23 : 0] TRIG4;
input [23 : 0] TRIG5;
input [23 : 0] TRIG6;
input [23 : 0] TRIG7;
input [3 : 0] TRIG8;

endmodule
