The following files were generated for 'chipscope_pixel_test' in directory
C:\Users\IC_2\Dropbox\hdl_codes\hevc_ful_repo\rtl\ipcode_dir\chipscope_pixel_test\

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * chipscope_pixel_test.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * chipscope_pixel_test.cdc
   * chipscope_pixel_test.constraints/chipscope_pixel_test.ucf
   * chipscope_pixel_test.constraints/chipscope_pixel_test.xdc
   * chipscope_pixel_test.ncf
   * chipscope_pixel_test.ngc
   * chipscope_pixel_test.ucf
   * chipscope_pixel_test.v
   * chipscope_pixel_test.veo
   * chipscope_pixel_test.xdc
   * chipscope_pixel_test_xmdf.tcl

Creates an HDL instantiation template:
   Creates an HDL instantiation template for the IP.

   * chipscope_pixel_test.veo

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * chipscope_pixel_test.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * chipscope_pixel_test.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * chipscope_pixel_test.gise
   * chipscope_pixel_test.xise

Deliver Readme:
   Readme file for the IP.

   * chipscope_pixel_test_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * chipscope_pixel_test_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

