The following files were generated for 'chipscop_ila3_sao' in directory
F:\HEVC decoder\HDL codes\pred_top_total_repo_\rtl\ipcode_dir\chipscope_ila4\

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * chipscop_ila3_sao.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * chipscop_ila3_sao.cdc
   * chipscop_ila3_sao.constraints/chipscop_ila3_sao.ucf
   * chipscop_ila3_sao.constraints/chipscop_ila3_sao.xdc
   * chipscop_ila3_sao.ncf
   * chipscop_ila3_sao.ngc
   * chipscop_ila3_sao.ucf
   * chipscop_ila3_sao.v
   * chipscop_ila3_sao.veo
   * chipscop_ila3_sao.xdc
   * chipscop_ila3_sao_xmdf.tcl

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * chipscop_ila3_sao.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * chipscop_ila3_sao.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * _xmsgs/pn_parser.xmsgs
   * chipscop_ila3_sao.gise
   * chipscop_ila3_sao.xise

Deliver Readme:
   Readme file for the IP.

   * chipscop_ila3_sao_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * chipscop_ila3_sao_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

