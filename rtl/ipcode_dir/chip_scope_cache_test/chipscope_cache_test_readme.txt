The following files were generated for 'chipscope_cache_test' in directory
F:\GalaxyCores\HEVC\hevc_decoder_project\Dropbox\hdl_codes\hevc_ful_repo\rtl\ipcode_dir\chip_scope_cache_test\

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * chipscope_cache_test.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * chipscope_cache_test.cdc
   * chipscope_cache_test.constraints/chipscope_cache_test.ucf
   * chipscope_cache_test.constraints/chipscope_cache_test.xdc
   * chipscope_cache_test.ncf
   * chipscope_cache_test.ngc
   * chipscope_cache_test.ucf
   * chipscope_cache_test.v
   * chipscope_cache_test.veo
   * chipscope_cache_test.xdc
   * chipscope_cache_test_xmdf.tcl

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * chipscope_cache_test.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * chipscope_cache_test.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * _xmsgs/pn_parser.xmsgs
   * chipscope_cache_test.gise
   * chipscope_cache_test.xise

Deliver Readme:
   Readme file for the IP.

   * chipscope_cache_test_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * chipscope_cache_test_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

