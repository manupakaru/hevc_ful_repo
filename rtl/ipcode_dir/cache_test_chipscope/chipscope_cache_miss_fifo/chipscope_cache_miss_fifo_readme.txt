The following files were generated for 'chipscope_cache_miss_fifo' in directory
F:\Geethan\hevc_ful_repo\rtl\ipcode_dir\cache_test_chipscope\chipscope_cache_miss_fifo\

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * chipscope_cache_miss_fifo.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * chipscope_cache_miss_fifo.cdc
   * chipscope_cache_miss_fifo.constraints/chipscope_cache_miss_fifo.ucf
   * chipscope_cache_miss_fifo.constraints/chipscope_cache_miss_fifo.xdc
   * chipscope_cache_miss_fifo.ncf
   * chipscope_cache_miss_fifo.ngc
   * chipscope_cache_miss_fifo.ucf
   * chipscope_cache_miss_fifo.v
   * chipscope_cache_miss_fifo.veo
   * chipscope_cache_miss_fifo.xdc
   * chipscope_cache_miss_fifo_xmdf.tcl

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * chipscope_cache_miss_fifo.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * chipscope_cache_miss_fifo.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * _xmsgs/pn_parser.xmsgs
   * chipscope_cache_miss_fifo.gise
   * chipscope_cache_miss_fifo.xise

Deliver Readme:
   Readme file for the IP.

   * chipscope_cache_miss_fifo_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * chipscope_cache_miss_fifo_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

