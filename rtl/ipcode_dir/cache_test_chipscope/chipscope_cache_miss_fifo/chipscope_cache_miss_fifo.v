///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2014 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor     : Xilinx
// \   \   \/     Version    : 14.7
//  \   \         Application: Xilinx CORE Generator
//  /   /         Filename   : chipscope_cache_miss_fifo.v
// /___/   /\     Timestamp  : Sun Jul 27 18:33:32 Sri Lanka Standard Time 2014
// \   \  /  \
//  \___\/\___\
//
// Design Name: Verilog Synthesis Wrapper
///////////////////////////////////////////////////////////////////////////////
// This wrapper is used to integrate with Project Navigator and PlanAhead

`timescale 1ns/1ps

module chipscope_cache_miss_fifo(
    CONTROL,
    CLK,
    TRIG0,
    TRIG1,
    TRIG2,
    TRIG3,
    TRIG4,
    TRIG5,
    TRIG6,
    TRIG7,
    TRIG8,
    TRIG9,
    TRIG10,
    TRIG11,
    TRIG12) /* synthesis syn_black_box syn_noprune=1 */;


inout [35 : 0] CONTROL;
input CLK;
input [127 : 0] TRIG0;
input [255 : 0] TRIG1;
input [17 : 0] TRIG2;
input [10 : 0] TRIG3;
input [11 : 0] TRIG4;
input [7 : 0] TRIG5;
input [13 : 0] TRIG6;
input [37 : 0] TRIG7;
input [3 : 0] TRIG8;
input [55 : 0] TRIG9;
input [169 : 0] TRIG10;
input [223 : 0] TRIG11;
input [223 : 0] TRIG12;

endmodule
