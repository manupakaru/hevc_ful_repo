///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2014 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor     : Xilinx
// \   \   \/     Version    : 14.7
//  \   \         Application: Xilinx CORE Generator
//  /   /         Filename   : chipscope_cache_miss_fifo.veo
// /___/   /\     Timestamp  : Sun Jul 27 18:33:32 Sri Lanka Standard Time 2014
// \   \  /  \
//  \___\/\___\
//
// Design Name: ISE Instantiation template
///////////////////////////////////////////////////////////////////////////////

// The following must be inserted into your Verilog file for this
// core to be instantiated. Change the instance name and port connections
// (in parentheses) to your own signal names.

//----------- Begin Cut here for INSTANTIATION Template ---// INST_TAG
chipscope_cache_miss_fifo YourInstanceName (
    .CONTROL(CONTROL), // INOUT BUS [35:0]
    .CLK(CLK), // IN
    .TRIG0(TRIG0), // IN BUS [127:0]
    .TRIG1(TRIG1), // IN BUS [255:0]
    .TRIG2(TRIG2), // IN BUS [17:0]
    .TRIG3(TRIG3), // IN BUS [10:0]
    .TRIG4(TRIG4), // IN BUS [11:0]
    .TRIG5(TRIG5), // IN BUS [7:0]
    .TRIG6(TRIG6), // IN BUS [13:0]
    .TRIG7(TRIG7), // IN BUS [37:0]
    .TRIG8(TRIG8), // IN BUS [3:0]
    .TRIG9(TRIG9), // IN BUS [55:0]
    .TRIG10(TRIG10), // IN BUS [169:0]
    .TRIG11(TRIG11), // IN BUS [223:0]
    .TRIG12(TRIG12) // IN BUS [223:0]
);

// INST_TAG_END ------ End INSTANTIATION Template ---------

