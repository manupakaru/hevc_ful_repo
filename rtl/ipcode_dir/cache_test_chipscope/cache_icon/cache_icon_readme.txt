The following files were generated for 'cache_icon' in directory
F:\GalaxyCores\HEVC\hevc_decoder_project\Dropbox\hdl_codes\hevc_ful_repo\rtl\ipcode_dir\cache_test_chipscope\cache_icon\

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * cache_icon.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * cache_icon.constraints/cache_icon.ucf
   * cache_icon.constraints/cache_icon.xdc
   * cache_icon.ngc
   * cache_icon.ucf
   * cache_icon.v
   * cache_icon.veo
   * cache_icon.xdc
   * cache_icon_xmdf.tcl

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * cache_icon.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * cache_icon.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * cache_icon.gise
   * cache_icon.xise

Deliver Readme:
   Readme file for the IP.

   * cache_icon_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * cache_icon_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

