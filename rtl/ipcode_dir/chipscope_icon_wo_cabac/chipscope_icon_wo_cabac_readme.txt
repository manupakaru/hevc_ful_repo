The following files were generated for 'chipscope_icon_wo_cabac' in directory
F:\GalaxyCores\HEVC\hevc_decoder_project\Dropbox\hdl_codes\hevc_ful_repo\rtl\ipcode_dir\chipscope_icon_wo_cabac\

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * chipscope_icon_wo_cabac.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * chipscope_icon_wo_cabac.constraints/chipscope_icon_wo_cabac.ucf
   * chipscope_icon_wo_cabac.constraints/chipscope_icon_wo_cabac.xdc
   * chipscope_icon_wo_cabac.ngc
   * chipscope_icon_wo_cabac.ucf
   * chipscope_icon_wo_cabac.v
   * chipscope_icon_wo_cabac.veo
   * chipscope_icon_wo_cabac.xdc
   * chipscope_icon_wo_cabac_xmdf.tcl

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * chipscope_icon_wo_cabac.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * chipscope_icon_wo_cabac.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * _xmsgs/pn_parser.xmsgs
   * chipscope_icon_wo_cabac.gise
   * chipscope_icon_wo_cabac.xise

Deliver Readme:
   Readme file for the IP.

   * chipscope_icon_wo_cabac_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * chipscope_icon_wo_cabac_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

