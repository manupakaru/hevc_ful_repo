`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:29:36 02/09/2014 
// Design Name: 
// Module Name:    pred_dbf_wrapper 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module pred_dbf_wrapper_with_eth(
	wr_clk,
	clk,
	reset,
    enable,
   
    fifo_in,
    input_fifo_is_full,
    write_en_in,
	
    //luma residual fifo interface
    y_residual_fifo_in,
    y_residual_fifo_is_full_out,
    y_residual_fifo_write_en_in,
    
    cb_residual_fifo_in,
    cb_residual_fifo_is_full_out,
    cb_residual_fifo_write_en_in,
    
    cr_residual_fifo_in,
    cr_residual_fifo_is_full_out,
    cr_residual_fifo_write_en_in,
        
	display_fifo_is_empty_out,
	display_fifo_rd_en_in,
	display_fifo_data_out,		
			
	dpb_fifo_is_empty_out,
	dpb_fifo_rd_en_in,
	dpb_fifo_data_out,    
	
	dpb_axi_addr_out,
	sao_poc_out,
	log2_ctu_size_out,
	
	pic_width_out,
	pic_height_out,
	
	mv_col_axi_awid,     
	mv_col_axi_awlen,    
	mv_col_axi_awsize,   
	mv_col_axi_awburst,  
	mv_col_axi_awlock,   
	mv_col_axi_awcache,  
	mv_col_axi_awprot,   
	mv_col_axi_awvalid,
	mv_col_axi_awaddr,
	mv_col_axi_awready,
	mv_col_axi_wstrb,
	mv_col_axi_wlast,
	mv_col_axi_wvalid,
	mv_col_axi_wdata,
	mv_col_axi_wready,
	// mv_col_axi_bid,
	mv_col_axi_bresp,
	mv_col_axi_bvalid,
	mv_col_axi_bready,
	
	mv_pref_axi_araddr              ,
	mv_pref_axi_arlen               ,
	mv_pref_axi_arsize              ,
	mv_pref_axi_arburst             ,
	mv_pref_axi_arprot              ,
	mv_pref_axi_arvalid             ,
	mv_pref_axi_arready             ,

	mv_pref_axi_rdata               ,
	mv_pref_axi_rresp               ,
	mv_pref_axi_rlast               ,
	mv_pref_axi_rvalid              ,
	mv_pref_axi_rready              ,

	mv_pref_axi_arlock              ,
	mv_pref_axi_arid                ,
	mv_pref_axi_arcache             ,

	ref_pix_axi_ar_addr         ,
	ref_pix_axi_ar_len          ,
	ref_pix_axi_ar_size         ,
	ref_pix_axi_ar_burst        ,
	ref_pix_axi_ar_prot         ,
	ref_pix_axi_ar_valid        ,
	ref_pix_axi_ar_ready        ,
	
	ref_pix_axi_r_data          ,
	ref_pix_axi_r_resp          ,
	ref_pix_axi_r_last          ,
	ref_pix_axi_r_valid         ,
	ref_pix_axi_r_ready         
	
        ,pred_state_low8b_out
		,inter_pred_stat_8b_out
		,mv_state_8bit_out
		,col_state_axi_write
		,pred_2_dbf_wr_inf
		,pred_2_dbf_yy       
		,pred_2_dbf_cb       
		,pred_2_dbf_cr	
		
		,dbf_main_in_valid	
		,dbf_main_out_valid	
		
		,Xc_Yc_out
		,intra_ready_out
		,rd_data_count_out
		,cb_rd_data_count_out
		,cr_rd_data_count_out
		,wr_data_count_out
		,rd_cnf_data_count_out
		,cnf_fifo_out
		,y_residual_counter
		,yy_res_fifo_empty
		,yy_prog_empty_thresh_out		
		,cb_prog_empty_thresh_out		
		,cr_prog_empty_thresh_out		
		,cnf_fifo_counter		

		,controller_in_valid 	
		,controller_in 			
		,controller_out_valid 			
		,controller_out 			
		,sao_wr_en_out 			
		,sao_rd_en_out 			
		,sao_wr_data 			
		,sao_rd_data 			
		
		,intra_x_out 		
		,intra_y_out        
		,intra_4x4_valid_out
		,intra_luma_4x4_out 
		,inter_x_out 
		,inter_y_out 
		,inter_4x4_valid_out 
		,inter_luma_4x4_out 
		,comon_pre_lp_x_out 
		,comon_pre_lp_y_out 
		,comon_pre_lp_4x4_valid_out 
		,comon_pre_lp_luma_4x4_out 
		,dbf_to_sao_out_valid
		,dbf_to_sao_out_x
		,dbf_to_sao_out_y
		,dbf_to_sao_out_q1		
		,test_xT_in_min_luma_filt
		,test_xT_in_min_luma_cache
		,test_yT_in_min_luma_filt
		,test_yT_in_min_luma_cache
		,test_luma_filter_out
		,test_luma_filter_ready
		,test_cache_addr
		,test_cache_luma_data
		,test_cache_valid_in		
		,test_cache_en		
		,test_ref_block_en		
		,test_ref_luma_data_4x4		
		,residual_read_inf		
		,res_pres_y_cb_cr_out		
		,current_poc_out
		,cb_res_pres_wr_en
		,cb_res_pres_rd_en		
		,cb_res_pres_empty		
		,cb_res_pres_full 		
		,cb_res_pres_din 			
		,cb_res_pres_dout			
		,display_buf_ful
    );

`include "../sim/pred_def.v"
`include "../sim/inter_axi_def.v"



//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                          FIFO_IN_WIDTH       = 32;
    localparam                          FIFO_OUT_WIDTH      = 32;  

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input wr_clk;
	 input clk;
    input reset;
    input                                   enable;
    
    input           [FIFO_IN_WIDTH-1:0]     fifo_in;
    output                                  input_fifo_is_full;
    input                               	write_en_in;
	
    //luma residual fifo interface
    input       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     y_residual_fifo_in;
    output                                                                      y_residual_fifo_is_full_out;
    input                                                                       y_residual_fifo_write_en_in;
    //cb residual fifo interface
    input       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     cb_residual_fifo_in;
    output                                                                      cb_residual_fifo_is_full_out;
    input                                                                       cb_residual_fifo_write_en_in;
    //cr residual fifo interface                                
    input       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     cr_residual_fifo_in;
    output                                                                      cr_residual_fifo_is_full_out;
    input                                                                       cr_residual_fifo_write_en_in;


	output 								display_fifo_is_empty_out;
	input  								display_fifo_rd_en_in;
	output  [SAO_OUT_FIFO_WIDTH-1:0]	display_fifo_data_out;
	
	output 								dpb_fifo_is_empty_out;
	input  								dpb_fifo_rd_en_in;
	output  [SAO_OUT_FIFO_WIDTH-1:0]	dpb_fifo_data_out;    
	
	output [AXI_ADDR_WDTH -1:0]			dpb_axi_addr_out;
	output [POC_WIDTH -1:0]			sao_poc_out;
	
    output     [PIC_WIDTH_WIDTH-1:0]          							  pic_width_out;
    output     [PIC_WIDTH_WIDTH-1:0]          							  pic_height_out;
    output     [MAX_LOG2CTBSIZE_WIDTH - 1:0]              	  			  log2_ctu_size_out;
	
    //--------------axi interface
    output                                          mv_col_axi_awid    ;// = 0;
    output      [7:0]                               mv_col_axi_awlen   ;// = MV_COL_AXI_AX_LEN;
    output      [2:0]                               mv_col_axi_awsize  ;// = MV_COL_AXI_AX_SIZE;
    output      [1:0]                               mv_col_axi_awburst ;// = `AX_BURST_INC;
    output                        	                mv_col_axi_awlock  ;// = `AX_LOCK_DEFAULT;
    output      [3:0]                               mv_col_axi_awcache ;// = `AX_CACHE_DEFAULT;
    output      [2:0]                               mv_col_axi_awprot  ;// = `AX_PROT_DATA;
    output                                          mv_col_axi_awvalid;
    output      [31:0]                              mv_col_axi_awaddr;

    input                       	                mv_col_axi_awready;

    // write data channel
    output      [64-1:0]                            mv_col_axi_wstrb;// = 64'hFFFFF_FFFFF_FFFFF_FFFFF_FFFFF;       // 40 bytes
    output                                          mv_col_axi_wlast;
    output                                          mv_col_axi_wvalid;
    output      [MV_COL_AXI_DATA_WIDTH -1:0]        mv_col_axi_wdata;

    input	                                        mv_col_axi_wready;

    //write response channel
    // input                       	                mv_col_axi_bid;
    input       [1:0]                               mv_col_axi_bresp;
    input                       	                mv_col_axi_bvalid;
    output                                          mv_col_axi_bready;  

    output	    [AXI_ADDR_WDTH-1:0]		            mv_pref_axi_araddr  ;
    output wire [7:0]					            mv_pref_axi_arlen   ;
    output wire	[2:0]					            mv_pref_axi_arsize  ;
    output wire [1:0]					            mv_pref_axi_arburst ;
    output wire [2:0]					            mv_pref_axi_arprot  ;
    output 	   						                mv_pref_axi_arvalid ;
    input 								            mv_pref_axi_arready ;
            
    input		[MV_COL_AXI_DATA_WIDTH-1:0]		    mv_pref_axi_rdata   ;
    input		[1:0]					            mv_pref_axi_rresp   ;
    input 								            mv_pref_axi_rlast   ;
    input								            mv_pref_axi_rvalid  ;
    output 						                    mv_pref_axi_rready  ;

    output                        	                mv_pref_axi_arlock  ;
    output                                          mv_pref_axi_arid    ;    
    output      [3:0]                               mv_pref_axi_arcache ;     
    
    // axi master interface         
    output      [AXI_ADDR_WDTH-1:0]                 ref_pix_axi_ar_addr     ;
    output      [7:0]                               ref_pix_axi_ar_len      ;
    output      [2:0]                               ref_pix_axi_ar_size     ;
    output      [1:0]                               ref_pix_axi_ar_burst    ;
    output      [2:0]                               ref_pix_axi_ar_prot     ;
    output                                          ref_pix_axi_ar_valid    ;
    input                                           ref_pix_axi_ar_ready    ;
                
    input       [AXI_CACHE_DATA_WDTH-1:0]           ref_pix_axi_r_data      ;
    input       [1:0]                               ref_pix_axi_r_resp      ;
    input                                           ref_pix_axi_r_last      ;
    input                                           ref_pix_axi_r_valid     ;
    output                                          ref_pix_axi_r_ready     ;   
    //-----------end of axi interface
	output [7:0] pred_2_dbf_wr_inf;
    output  [7:0]                                   pred_state_low8b_out;
    output  [7:0]                                   inter_pred_stat_8b_out;
    output  [7:0]                                   mv_state_8bit_out;
    output  [7:0]                                   col_state_axi_write;
	
	output [134-1:0] pred_2_dbf_yy;
	output [128-1:0] pred_2_dbf_cb;
	output [128-1:0] pred_2_dbf_cr;
	output  dbf_main_in_valid;
	output  dbf_main_out_valid;
	output  [2:0] res_pres_y_cb_cr_out;
	

	
    // assign pred_2_dbf_yy = y_dbf_fifo_data_internal;
	
	output [24-1:0] Xc_Yc_out;
    output  [5:0] 									intra_ready_out;
    output [12:0] rd_data_count_out;
    output [12:0] cb_rd_data_count_out;
    output [12:0] cr_rd_data_count_out;
    output [12:0] wr_data_count_out;
    output [10:0] rd_cnf_data_count_out;
	output [31:0] cnf_fifo_out;
	output [11:0] yy_prog_empty_thresh_out;
	output [11:0] cb_prog_empty_thresh_out;
	output [11:0] cr_prog_empty_thresh_out;
	output [31:0] cnf_fifo_counter;
	output [31:0] y_residual_counter;
	
	output 				controller_in_valid 	;
	output [128-1:0] 	controller_in 			;
	output 				controller_out_valid 	;
	output [128-1:0] 	controller_out 			;
	output 				sao_wr_en_out 			;
	output 				sao_rd_en_out 			;
	output [256-1:0] 	sao_wr_data 			;
	output [256-1:0] 	sao_rd_data 			;

	output [11:0] intra_x_out 			;
	output [11:0] intra_y_out          ;
	output intra_4x4_valid_out  ;
	output [128-1:0] intra_luma_4x4_out   ;	
	
	output [11:0] inter_x_out;
	output [11:0] inter_y_out;
	output 			inter_4x4_valid_out;
	output [128-1:0] inter_luma_4x4_out;
	
	output [11:0] comon_pre_lp_x_out;
	output [11:0] comon_pre_lp_y_out;
	output 			comon_pre_lp_4x4_valid_out;
	output [128-1:0] comon_pre_lp_luma_4x4_out;	
	
	
	output dbf_to_sao_out_valid;
	output [12-1:0] dbf_to_sao_out_x;
	output [12-1:0] dbf_to_sao_out_y;
	output [128-1:0] dbf_to_sao_out_q1;
	output [128-1:0] test_ref_luma_data_4x4;
		
    output [9 - 1:0] test_xT_in_min_luma_filt;
    output [9 - 1:0] test_xT_in_min_luma_cache;
    output [9 - 1:0] test_yT_in_min_luma_cache;
    output [9 - 1:0] test_yT_in_min_luma_filt;	
    output        									test_luma_filter_ready;
    output        [256 -1:0]         test_luma_filter_out;    
	output [7-1:0] test_cache_addr;
	output [512-1:0] test_cache_luma_data;
	output test_cache_valid_in ;	
	output test_cache_en ;	
	output test_ref_block_en ;	
	output [7:0] residual_read_inf ;	
	output [31:0] current_poc_out ;	
	output display_buf_ful;
	
	output cb_res_pres_wr_en	;
	output cb_res_pres_rd_en    ;
	output cb_res_pres_empty    ;
	output cb_res_pres_full     ;
	output cb_res_pres_din 	    ;
	output cb_res_pres_dout	    ;
	
	output yy_res_fifo_empty;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//--------------------------------------------------------------------------------------------------------------------- 
		
	
	wire       		[FIFO_OUT_WIDTH-1:0]    	pred_to_dbf_config_fifo;
	wire       									pred_to_dbf_config_fifo_full;
	wire       									pred_to_dbf_config_fifo_wr_en;
	wire         [DPB_ADDR_WIDTH -1: 0]     	pred_to_dbf_current_pic_dpb_idx;    
	    
        //luma dbf fifo interface
    wire                                                                                        y_dbf_fifo_is_full_internal;
    wire   [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE + 2*DBF_SAMPLE_XY_ADDR - 1:0]       y_dbf_fifo_data_internal;
    wire                                                                                        y_dbf_fifo_wr_en_internal;
    
    //cb dbf fifo interface
    wire                                                                        cb_dbf_fifo_is_full_internal;
    wire    [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]             cb_dbf_fifo_data_internal;
    wire                                                                        cb_dbf_fifo_wr_en_internal;
    
    //cr dbf fifo interface
    wire                                                                        cr_dbf_fifo_is_full_internal;
    wire    [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]             cr_dbf_fifo_data_internal;
    wire                                                                        cr_dbf_fifo_wr_en_internal;
    
    
    wire [BS_FIFO_WIDTH-1:0]    bs_fifo_data_wire       ;
    wire                        bs_fifo_wr_en_wire       ;
    wire                        bs_fifo_full_wire         ; // should ensure this line never asserted   

	 assign pred_2_dbf_wr_inf = {pred_to_dbf_config_fifo_wr_en,y_dbf_fifo_wr_en_internal,cb_dbf_fifo_wr_en_internal,cr_dbf_fifo_wr_en_internal
								 ,pred_to_dbf_config_fifo_full,y_dbf_fifo_is_full_internal,cb_dbf_fifo_is_full_internal,cr_dbf_fifo_is_full_internal};
    	

	// Instantiate the module
pred_top_wrapper_with_eth pred_wrap_block (
	 .wr_clk(wr_clk),
    .clk(clk), 
    .reset(reset), 
    .enable(enable), 
	
    .fifo_in			(fifo_in), 
    .input_fifo_is_full	(input_fifo_is_full), 
    .write_en_in		(write_en_in), 
    .fifo_out			(pred_to_dbf_config_fifo), 
    .output_fifo_is_full(pred_to_dbf_config_fifo_full), 
    .write_en_out		(pred_to_dbf_config_fifo_wr_en), 
	    
    .bs_fifo_data_out    (bs_fifo_data_wire), 
    .bs_fifo_wr_en_out   (bs_fifo_wr_en_wire), 
    .bs_fifo_full_in     (bs_fifo_full_wire) ,
	
    .y_residual_fifo_in				(y_residual_fifo_in), 
    .y_residual_fifo_is_full_out	(y_residual_fifo_is_full_out), 
    .y_residual_fifo_write_en_in	(y_residual_fifo_write_en_in), 
    .cb_residual_fifo_in			(cb_residual_fifo_in), 
    .cb_residual_fifo_is_full_out	(cb_residual_fifo_is_full_out), 
    .cb_residual_fifo_write_en_in	(cb_residual_fifo_write_en_in), 
    .cr_residual_fifo_in			(cr_residual_fifo_in), 
    .cr_residual_fifo_is_full_out	(cr_residual_fifo_is_full_out), 
    .cr_residual_fifo_write_en_in	(cr_residual_fifo_write_en_in), 
    
    
    .y_dbf_fifo_is_full				(y_dbf_fifo_is_full_internal), 
    .y_dbf_fifo_data_out			(y_dbf_fifo_data_internal), 
    .y_dbf_fifo_wr_en_out			(y_dbf_fifo_wr_en_internal), 
    .cb_dbf_fifo_is_full			(cb_dbf_fifo_is_full_internal), 
    .cb_dbf_fifo_data_out			(cb_dbf_fifo_data_internal), 
    .cb_dbf_fifo_wr_en_out			(cb_dbf_fifo_wr_en_internal), 
    .cr_dbf_fifo_is_full			(cr_dbf_fifo_is_full_internal), 
    .cr_dbf_fifo_data_out			(cr_dbf_fifo_data_internal), 
    .cr_dbf_fifo_wr_en_out			(cr_dbf_fifo_wr_en_internal), 
    
    .mv_col_axi_awid		(mv_col_axi_awid), 
    .mv_col_axi_awlen		(mv_col_axi_awlen), 
    .mv_col_axi_awsize		(mv_col_axi_awsize), 
    .mv_col_axi_awburst		(mv_col_axi_awburst), 
    .mv_col_axi_awlock		(mv_col_axi_awlock), 
    .mv_col_axi_awcache		(mv_col_axi_awcache), 
    .mv_col_axi_awprot		(mv_col_axi_awprot), 
    .mv_col_axi_awvalid		(mv_col_axi_awvalid), 
    .mv_col_axi_awaddr		(mv_col_axi_awaddr), 
    .mv_col_axi_awready		(mv_col_axi_awready), 
    .mv_col_axi_wstrb		(mv_col_axi_wstrb), 
    .mv_col_axi_wlast		(mv_col_axi_wlast), 
    .mv_col_axi_wvalid		(mv_col_axi_wvalid), 
    .mv_col_axi_wdata		(mv_col_axi_wdata), 
    .mv_col_axi_wready		(mv_col_axi_wready), 
    .mv_col_axi_bresp		(mv_col_axi_bresp), 
    .mv_col_axi_bvalid		(mv_col_axi_bvalid), 
    .mv_col_axi_bready		(mv_col_axi_bready), 
    .mv_pref_axi_araddr		(mv_pref_axi_araddr), 
    .mv_pref_axi_arlen		(mv_pref_axi_arlen), 
    .mv_pref_axi_arsize		(mv_pref_axi_arsize), 
    .mv_pref_axi_arburst	(mv_pref_axi_arburst), 
    .mv_pref_axi_arprot		(mv_pref_axi_arprot), 
    .mv_pref_axi_arvalid	(mv_pref_axi_arvalid), 
    .mv_pref_axi_arready	(mv_pref_axi_arready), 
    .mv_pref_axi_rdata		(mv_pref_axi_rdata), 
    .mv_pref_axi_rresp		(mv_pref_axi_rresp), 
    .mv_pref_axi_rlast		(mv_pref_axi_rlast), 
    .mv_pref_axi_rvalid		(mv_pref_axi_rvalid), 
    .mv_pref_axi_rready		(mv_pref_axi_rready), 
    .mv_pref_axi_arlock		(mv_pref_axi_arlock), 
    .mv_pref_axi_arid		(mv_pref_axi_arid), 
    .mv_pref_axi_arcache	(mv_pref_axi_arcache), 
    
    .ref_pix_axi_ar_addr    (ref_pix_axi_ar_addr), 
    .ref_pix_axi_ar_len     (ref_pix_axi_ar_len), 
    .ref_pix_axi_ar_size    (ref_pix_axi_ar_size), 
    .ref_pix_axi_ar_burst   (ref_pix_axi_ar_burst), 
    .ref_pix_axi_ar_prot    (ref_pix_axi_ar_prot), 
    .ref_pix_axi_ar_valid   (ref_pix_axi_ar_valid), 
    .ref_pix_axi_ar_ready   (ref_pix_axi_ar_ready), 
    .ref_pix_axi_r_data     (ref_pix_axi_r_data), 
    .ref_pix_axi_r_resp     (ref_pix_axi_r_resp), 
    .ref_pix_axi_r_last     (ref_pix_axi_r_last), 
    .ref_pix_axi_r_valid    (ref_pix_axi_r_valid), 
    .ref_pix_axi_r_ready    (ref_pix_axi_r_ready)
	
`ifdef CHIPSCOPE_DEBUG
		,.pred_state_low8b_out           (pred_state_low8b_out)
		,.inter_pred_stat_8b_out(inter_pred_stat_8b_out)
		,.mv_state_8bit_out(mv_state_8bit_out)
		//,.col_state_axi_write(col_state_axi_write)
		,.intra_ready_out	(intra_ready_out	)	
		,.y_rd_data_count_out	(rd_data_count_out  )
		,.cb_rd_data_count_out	(cb_rd_data_count_out  )
		,.cr_rd_data_count_out	(cr_rd_data_count_out  )
        ,.wr_data_count_out	(wr_data_count_out  )
		,.rd_cnf_data_count_out		(rd_cnf_data_count_out)
		,.cnf_fifo_out				(cnf_fifo_out)
		//,.yy_res_fifo_empty		(yy_res_fifo_empty)
		,.yy_prog_empty_thresh_out				(yy_prog_empty_thresh_out)
		,.cr_prog_empty_thresh_out				(cr_prog_empty_thresh_out)
		,.cb_prog_empty_thresh_out				(cb_prog_empty_thresh_out)
		,.cnf_fifo_counter				(cnf_fifo_counter)
		//,.y_residual_counter				(y_residual_counter)
			
		,.intra_x_out 			(intra_x_out )
		,.intra_y_out           (intra_y_out )
		,.intra_4x4_valid_out   (intra_4x4_valid_out)
		,.intra_luma_4x4_out    (intra_luma_4x4_out )
		,.inter_x_out 			(inter_x_out )
		,.inter_y_out           (inter_y_out )
		,.inter_4x4_valid_out   (inter_4x4_valid_out)
		,.inter_luma_4x4_out    (inter_luma_4x4_out )
		,.comon_pre_lp_x_out 			(comon_pre_lp_x_out )
		,.comon_pre_lp_y_out           (comon_pre_lp_y_out )
		,.comon_pre_lp_4x4_valid_out   (comon_pre_lp_4x4_valid_out)
		,.comon_pre_lp_luma_4x4_out    (comon_pre_lp_luma_4x4_out )		
		,.test_xT_in_min_luma_filt   (test_xT_in_min_luma_filt	)
		,.test_xT_in_min_luma_cache  (test_xT_in_min_luma_cache )
		,.test_yT_in_min_luma_cache  (test_yT_in_min_luma_cache )
		,.test_yT_in_min_luma_filt   (test_yT_in_min_luma_filt  )
		,.test_luma_filter_ready     (test_luma_filter_ready    )
		,.test_luma_filter_out       (test_luma_filter_out      )
		,.test_cache_addr            (test_cache_addr           )
		,.test_cache_luma_data       (test_cache_luma_data      )
		,.test_cache_valid_in		 (test_cache_valid_in		)		
		,.test_cache_en		 (test_cache_en		)		
		,.test_ref_block_en		 (test_ref_block_en		)		
		,.test_ref_luma_data_4x4		(test_ref_luma_data_4x4		)		
		,.residual_read_inf		 		(residual_read_inf		)		
		,.res_pres_y_cb_cr_out		 		(res_pres_y_cb_cr_out		)		
		,.current_poc_out		 		(current_poc_out		)		
		,.cb_res_pres_wr_en			(cb_res_pres_wr_en)
		,.cb_res_pres_rd_en			(cb_res_pres_rd_en)
		,.cb_res_pres_empty		    (cb_res_pres_empty)
		,.cb_res_pres_full 		    (cb_res_pres_full )
		,.cb_res_pres_din 			(cb_res_pres_din )
		,.cb_res_pres_dout			(cb_res_pres_dout)
`endif		
		
		
		
    );



// Instantiate the module
dbf_top dbf_block (
    .clk(clk), 
    .reset(reset),     
    .bs_fifo_data_in(bs_fifo_data_wire),
    .bs_fifo_full_out(bs_fifo_full_wire),
    .bs_fifo_wr_en_in(bs_fifo_wr_en_wire),
    
    .cnfig_dbf_fifo_is_full_out(pred_to_dbf_config_fifo_full), 
    .cnfig_dbf_fifo_data_in(pred_to_dbf_config_fifo), 
    .cnfig_dbf_fifo_wr_en_in(pred_to_dbf_config_fifo_wr_en), 
    
    .y_dbf_fifo_is_full_out		(y_dbf_fifo_is_full_internal), 
    .y_dbf_fifo_data_in			(y_dbf_fifo_data_internal), 
    .y_dbf_fifo_wr_en_in		(y_dbf_fifo_wr_en_internal), 
    .cb_dbf_fifo_is_full_out	(cb_dbf_fifo_is_full_internal), 
    .cb_dbf_fifo_data_in		(cb_dbf_fifo_data_internal), 
    .cb_dbf_fifo_wr_en_in		(cb_dbf_fifo_wr_en_internal), 
    .cr_dbf_fifo_is_full_out	(cr_dbf_fifo_is_full_internal), 
    .cr_dbf_fifo_data_in		(cr_dbf_fifo_data_internal), 
    .cr_dbf_fifo_wr_en_in		(cr_dbf_fifo_wr_en_internal), 
    
	.display_fifo_is_empty_out	(display_fifo_is_empty_out),
	.display_fifo_rd_en_in		(display_fifo_rd_en_in),				
	.display_fifo_data_out		(display_fifo_data_out),
	.dpb_fifo_is_empty_out		(dpb_fifo_is_empty_out),
	.dpb_fifo_rd_en_in			(dpb_fifo_rd_en_in),		
	.dpb_fifo_data_out			(dpb_fifo_data_out),
	.dpb_axi_addr_out			(dpb_axi_addr_out),
	.sao_poc_out				(sao_poc_out) ,
	
	.pic_width_out				(pic_width_out),	
	.pic_height_out				(pic_height_out),
	.log2_ctu_size_out			(log2_ctu_size_out)
	
`ifdef CHIPSCOPE_DEBUG
	,.Xc_Yc_out(Xc_Yc_out)
	,.pred_2_dbf_yy(pred_2_dbf_yy)
	,.pred_2_dbf_cb(pred_2_dbf_cb)
	,.pred_2_dbf_cr(pred_2_dbf_cr)
	//,.pred_2_dbf_rd_inf(pred_2_dbf_wr_inf)
	,.dbf_main_in_valid(dbf_main_in_valid)
	,.dbf_main_out_valid(dbf_main_out_valid)
	
	,.controller_in_valid 	(controller_in_valid 	)
	,.controller_in 		(controller_in 			)
	,.controller_out_valid  (controller_out_valid 	)
	,.controller_out 		(controller_out 		)
	,.sao_wr_en_out 		(sao_wr_en_out 			)
	,.sao_rd_en_out 		(sao_rd_en_out 			)
	,.sao_wr_data 			(sao_wr_data 			)
	,.sao_rd_data 			(sao_rd_data 			)

	,.dbf_to_sao_out_valid	(dbf_to_sao_out_valid	)
	,.dbf_to_sao_out_x		(dbf_to_sao_out_x		)			
	,.dbf_to_sao_out_y		(dbf_to_sao_out_y		)			
	,.dbf_to_sao_out_q1		(dbf_to_sao_out_q1		)	
	,.display_buf_ful	(display_buf_ful)
`endif
	
    );





endmodule
