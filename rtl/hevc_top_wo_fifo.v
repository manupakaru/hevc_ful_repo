`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:29:36 02/09/2014 
// Design Name: 
// Module Name:    pred_dbf_wrapper 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module hevc_top_wo_fifo(

	clk,
	reset,
    enable,
   
 
	y_rd_data_count_in,
	cb_rd_data_count_in,
	cr_rd_data_count_in,
	  
	fifo_in,
	input_fifo_is_empty,
	read_en_out,
	//luma residual fifo interface
	y_residual_fifo_in,
	y_residual_fifo_is_empty_in,
	y_residual_fifo_is_empty_out,
	y_residual_fifo_read_en_out,
	yy_prog_empty_thresh_out,

	//cb residual fifo interface
	cb_residual_fifo_in,
	cb_residual_fifo_is_empty_in,
	cb_residual_fifo_is_empty_out,
	cb_residual_fifo_read_en_out,
	cb_prog_empty_thresh_out,

	//cr residual fifo interface
	cr_residual_fifo_in,
	cr_residual_fifo_is_empty_in,
	cr_residual_fifo_is_empty_out,
	cr_residual_fifo_read_en_out,
	cr_prog_empty_thresh_out,

	display_fifo_is_empty_out,
	display_fifo_rd_en_in,
	display_fifo_data_out,		
			
	sao_poc_out,
	
	log2_ctu_size_out,
	pic_width_out,
	pic_height_out,
	
	mv_col_axi_awid				,     
	mv_col_axi_awlen			,    
	mv_col_axi_awsize			,   
	mv_col_axi_awburst			,  
	mv_col_axi_awlock			,   
	mv_col_axi_awcache			,  
	mv_col_axi_awprot			,   
	mv_col_axi_awvalid			,
	mv_col_axi_awaddr			,
	mv_col_axi_awready			,
	mv_col_axi_wstrb			,
	mv_col_axi_wlast			,
	mv_col_axi_wvalid			,
	mv_col_axi_wdata			,
	mv_col_axi_wready			,
	// mv_col_axi_bid			,
	mv_col_axi_bresp			,
	mv_col_axi_bvalid			,
	mv_col_axi_bready			,
	
	mv_pref_axi_araddr              ,
	mv_pref_axi_arlen               ,
	mv_pref_axi_arsize              ,
	mv_pref_axi_arburst             ,
	mv_pref_axi_arprot              ,
	mv_pref_axi_arvalid             ,
	mv_pref_axi_arready             ,

	mv_pref_axi_rdata               ,
	mv_pref_axi_rresp               ,
	mv_pref_axi_rlast               ,
	mv_pref_axi_rvalid              ,
	mv_pref_axi_rready              ,

	mv_pref_axi_arlock              ,
	mv_pref_axi_arid                ,
	mv_pref_axi_arcache             ,

	ref_pix_axi_ar_addr         ,
	ref_pix_axi_ar_len          ,
	ref_pix_axi_ar_size         ,
	ref_pix_axi_ar_burst        ,
	ref_pix_axi_ar_prot         ,
	ref_pix_axi_ar_valid        ,
	ref_pix_axi_ar_ready        ,
	
	ref_pix_axi_r_data          ,
	ref_pix_axi_r_resp          ,
	ref_pix_axi_r_last          ,
	ref_pix_axi_r_valid         ,
	ref_pix_axi_r_ready         ,
	
	ref_pic_write_axi_awid   	,
	ref_pic_write_axi_awlen  	,
	ref_pic_write_axi_awsize 	,
	ref_pic_write_axi_awburst	,
	ref_pic_write_axi_awlock 	,
	ref_pic_write_axi_awcache	,
	ref_pic_write_axi_awprot 	,
	ref_pic_write_axi_awvalid	,				
	ref_pic_write_axi_awaddr	,						
	ref_pic_write_axi_awready	,			
		
	ref_pic_write_axi_wstrb		,	
	ref_pic_write_axi_wlast		,	
	ref_pic_write_axi_wvalid	,	
	ref_pic_write_axi_wdata		,	
	ref_pic_write_axi_wready	,	
		
	ref_pic_write_axi_bid		,	
	ref_pic_write_axi_bresp		,	
	ref_pic_write_axi_bvalid	,	
	ref_pic_write_axi_bready	
	
        ,pred_state_low8b_out
		,inter_pred_stat_8b_out
		,mv_state_8bit_out
		,col_state_axi_write
		,pred_2_dbf_wr_inf
		,pred_2_dbf_yy
		,pred_2_dbf_cb
		,pred_2_dbf_cr
		,Xc_Yc_out
		,intra_ready_out
		,cnf_fifo_out					
		,cnf_fifo_counter		
		,y_residual_counter
		,dbf_main_in_valid				
		,dbf_main_out_valid				
		,sao_main_out_valid
		,sao_out_y_4x8

		,controller_in_valid 	
		,controller_in 			
		,controller_out_valid 	
		,controller_out 			
		,sao_wr_en_out 					
		,sao_rd_en_out 					
		,sao_wr_data 					
		,sao_rd_data 					
		,dpb_wr_fifo_data 					
		
		,intra_x_out 		
		,intra_y_out        
		,intra_4x4_valid_out
		,intra_luma_4x4_out 
		,inter_x_out 
		,inter_y_out 
		,inter_4x4_valid_out 
		,inter_luma_4x4_out 
		,comon_pre_lp_x_out 
		,comon_pre_lp_y_out 
		,comon_pre_lp_4x4_valid_out 
		,comon_pre_lp_luma_4x4_out 
		,dbf_to_sao_out_valid
		,dbf_to_sao_out_x
		,dbf_to_sao_out_y
		,dbf_to_sao_out_q1	 		
		,test_xT_in_min_luma_filt
		,test_xT_in_min_luma_cache
		,test_yT_in_min_luma_filt
		,test_yT_in_min_luma_cache
		,test_luma_filter_out
		,test_luma_filter_ready
		,test_cache_addr
		,test_cache_luma_data
		,test_cache_valid_in		
		,test_cache_en		
		,test_ref_block_en		
		,test_ref_luma_data_4x4		
		,residual_read_inf		
		,res_pres_y_cb_cr_out		
		,current_poc_out	
		,cb_res_pres_wr_en
		,cb_res_pres_rd_en		
		,cb_res_pres_empty		
		,cb_res_pres_full 
		,cb_res_pres_din 	
		,cb_res_pres_dout	
		,display_buf_ful
    );

`include "../sim/pred_def.v"
`include "../sim/inter_axi_def.v"



//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                          FIFO_IN_WIDTH       = 32;
    localparam                          FIFO_OUT_WIDTH      = 32;  

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
	input clk;
    input reset;
    input                                   enable;
    
    input [12:0] y_rd_data_count_in;
    input [10:0] cb_rd_data_count_in;
    input [10:0] cr_rd_data_count_in;
	 
    input           [FIFO_IN_WIDTH-1:0]     fifo_in;	 
    input                                   input_fifo_is_empty;
    output       	                        read_en_out;
	
    //luma residual fifo interface
    input       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     y_residual_fifo_in;
    input                                                                       y_residual_fifo_is_empty_in;
    output                                                                		y_residual_fifo_is_empty_out;
    output                                                                      y_residual_fifo_read_en_out;
	output 			[11:0] yy_prog_empty_thresh_out;
    //cb residual fifo interface
    input       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     cb_residual_fifo_in;
    input                                                                       cb_residual_fifo_is_empty_in;
    output                                                                   	cb_residual_fifo_is_empty_out;
    output                                                                      cb_residual_fifo_read_en_out;
	output 	  	[9:0] cb_prog_empty_thresh_out;
    //cr residual fifo interface
    input       [RESIDUAL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1 :0]     cr_residual_fifo_in;
    output                                                                  	cr_residual_fifo_is_empty_out;
    input                                                                       cr_residual_fifo_is_empty_in;
    output                                                                      cr_residual_fifo_read_en_out;
	output 	    	[9:0] cr_prog_empty_thresh_out;				
	

	output 								display_fifo_is_empty_out;
	input  								display_fifo_rd_en_in;
	output  [SAO_OUT_FIFO_WIDTH-1:0]	display_fifo_data_out;
	
	
	output [POC_WIDTH -1:0]			sao_poc_out;
    output     [MAX_LOG2CTBSIZE_WIDTH - 1:0]              	  			  log2_ctu_size_out;	
    output     [PIC_WIDTH_WIDTH-1:0]          							  pic_width_out;
    output     [PIC_WIDTH_WIDTH-1:0]          							  pic_height_out;

    //--------------axi interface
    output                                          mv_col_axi_awid    ;// = 0;
    output      [7:0]                               mv_col_axi_awlen   ;// = MV_COL_AXI_AX_LEN;
    output      [2:0]                               mv_col_axi_awsize  ;// = MV_COL_AXI_AX_SIZE;
    output      [1:0]                               mv_col_axi_awburst ;// = `AX_BURST_INC;
    output                        	                mv_col_axi_awlock  ;// = `AX_LOCK_DEFAULT;
    output      [3:0]                               mv_col_axi_awcache ;// = `AX_CACHE_DEFAULT;
    output      [2:0]                               mv_col_axi_awprot  ;// = `AX_PROT_DATA;
    output                                          mv_col_axi_awvalid;
    output      [31:0]                              mv_col_axi_awaddr;

    input                       	                mv_col_axi_awready;

    // write data channel
    output      [64-1:0]                            mv_col_axi_wstrb;// = 64'hFFFFF_FFFFF_FFFFF_FFFFF_FFFFF;       // 40 bytes
    output                                          mv_col_axi_wlast;
    output                                          mv_col_axi_wvalid;
    output      [MV_COL_AXI_DATA_WIDTH -1:0]        mv_col_axi_wdata;

    input	                                        mv_col_axi_wready;

    //write response channel
    // input                       	                mv_col_axi_bid;
    input       [1:0]                               mv_col_axi_bresp;
    input                       	                mv_col_axi_bvalid;
    output                                          mv_col_axi_bready;  

    output	    [AXI_ADDR_WDTH-1:0]		            mv_pref_axi_araddr  ;
    output wire [7:0]					            mv_pref_axi_arlen   ;
    output wire	[2:0]					            mv_pref_axi_arsize  ;
    output wire [1:0]					            mv_pref_axi_arburst ;
    output wire [2:0]					            mv_pref_axi_arprot  ;
    output 	   						                mv_pref_axi_arvalid ;
    input 								            mv_pref_axi_arready ;
            
    input		[MV_COL_AXI_DATA_WIDTH-1:0]		    mv_pref_axi_rdata   ;
    input		[1:0]					            mv_pref_axi_rresp   ;
    input 								            mv_pref_axi_rlast   ;
    input								            mv_pref_axi_rvalid  ;
    output 						                    mv_pref_axi_rready  ;

    output                        	                mv_pref_axi_arlock  ;
    output                                          mv_pref_axi_arid    ;    
    output      [3:0]                               mv_pref_axi_arcache ;     
    
    // axi master interface         
    output      [AXI_ADDR_WDTH-1:0]                 ref_pix_axi_ar_addr     ;
    output      [7:0]                               ref_pix_axi_ar_len      ;
    output      [2:0]                               ref_pix_axi_ar_size     ;
    output      [1:0]                               ref_pix_axi_ar_burst    ;
    output      [2:0]                               ref_pix_axi_ar_prot     ;
    output                                          ref_pix_axi_ar_valid    ;
    input                                           ref_pix_axi_ar_ready    ;
                
    input       [AXI_CACHE_DATA_WDTH-1:0]           ref_pix_axi_r_data      ;
    input       [1:0]                               ref_pix_axi_r_resp      ;
    input                                           ref_pix_axi_r_last      ;
    input                                           ref_pix_axi_r_valid     ;
    output                                          ref_pix_axi_r_ready     ;   
	
	
    output                                      	ref_pic_write_axi_awid    	;
    output      [7:0]                           	ref_pic_write_axi_awlen   	;
    output      [2:0]                           	ref_pic_write_axi_awsize  	;
    output      [1:0]                           	ref_pic_write_axi_awburst 	;
    output                        	            	ref_pic_write_axi_awlock  	;
    output      [3:0]                           	ref_pic_write_axi_awcache 	;
    output      [2:0]                           	ref_pic_write_axi_awprot  	;
    output 		                            		ref_pic_write_axi_awvalid	;
    output 		[AXI_ADDR_WDTH-1:0]             	ref_pic_write_axi_awaddr	;
    input                       	            	ref_pic_write_axi_awready	;
	
    // write data channel	
    output 	     [AXI_CACHE_DATA_WDTH/8-1:0]		ref_pic_write_axi_wstrb		;
    output 	                                		ref_pic_write_axi_wlast		;
    output 	                                		ref_pic_write_axi_wvalid	;
    output 	    [AXI_CACHE_DATA_WDTH -1:0]			ref_pic_write_axi_wdata		;
	
    input	                                    	ref_pic_write_axi_wready	;
	
    //write response channel	
    input                       	            	ref_pic_write_axi_bid		;
    input       [1:0]                           	ref_pic_write_axi_bresp		;
    input                       	            	ref_pic_write_axi_bvalid	;
    output  	                                	ref_pic_write_axi_bready	; 
	

		
    //-----------end of axi interface
    
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//--------------------------------------------------------------------------------------------------------------------- 
		
	wire 								dpb_fifo_is_empty_out;
	wire 								dpb_fifo_rd_en_in;
	wire  [SAO_OUT_FIFO_WIDTH-1:0]		dpb_fifo_data_out;    
	wire  [AXI_ADDR_WDTH -1:0]			dpb_axi_addr_out;
	
	
	output [7:0] pred_2_dbf_wr_inf;
    output  [7:0]                                   pred_state_low8b_out;
    output  [7:0]                                   inter_pred_stat_8b_out;
    output  [7:0]                                   mv_state_8bit_out;
    output  [7:0]                                   col_state_axi_write;
	
	output [134-1:0] pred_2_dbf_yy;
	output [128-1:0] pred_2_dbf_cb;
	output [128-1:0] pred_2_dbf_cr;
	output [24-1:0] Xc_Yc_out;
	output  [5:0] 									intra_ready_out;
	output [31:0] cnf_fifo_out;
	output [31:0] cnf_fifo_counter;
	output [31:0] y_residual_counter;
	output 	 dbf_main_in_valid;
	output 	 dbf_main_out_valid;
	output 	 sao_main_out_valid;
	output 	 [256-1:0] sao_out_y_4x8;
	
	output controller_in_valid 	;
	output [128-1:0] controller_in 		;
	output controller_out_valid ;
	output [128-1:0] controller_out 		;
	output sao_wr_en_out 		;
	output sao_rd_en_out 		;
	output [256-1:0] sao_wr_data 			;
	output [256-1:0] sao_rd_data 			;	
	output [SAO_OUT_FIFO_WIDTH-1:0] dpb_wr_fifo_data 			;	
	
	output [11:0] intra_x_out 			;
	output [11:0] intra_y_out          ;
	output intra_4x4_valid_out  ;
	output [128-1:0] intra_luma_4x4_out   ;	
	
	output [11:0] inter_x_out;
	output [11:0] inter_y_out;
	output 			inter_4x4_valid_out;
	output [128-1:0] inter_luma_4x4_out;
	
	output [11:0] comon_pre_lp_x_out;
	output [11:0] comon_pre_lp_y_out;
	output 			comon_pre_lp_4x4_valid_out;
	output [128-1:0] comon_pre_lp_luma_4x4_out;	
	output [128-1:0] test_ref_luma_data_4x4;	
	
	
	output dbf_to_sao_out_valid;
	output [12-1:0] dbf_to_sao_out_x;
	output [12-1:0] dbf_to_sao_out_y;
	output [128-1:0] dbf_to_sao_out_q1;

	
	assign sao_main_out_valid = dpb_fifo_rd_en_in;
	assign sao_out_y_4x8 = dpb_fifo_data_out[512-1:256];
	assign dpb_wr_fifo_data = dpb_fifo_data_out;
	
	
    output [9 - 1:0] test_xT_in_min_luma_filt;
    output [9 - 1:0] test_xT_in_min_luma_cache;
    output [9 - 1:0] test_yT_in_min_luma_cache;
    output [9 - 1:0] test_yT_in_min_luma_filt;	
    output        									test_luma_filter_ready;
    output        [256 -1:0]         test_luma_filter_out;    
	output [7-1:0] test_cache_addr;
	output [512-1:0] test_cache_luma_data;
	output test_cache_valid_in ;
	output test_ref_block_en ;
	output test_cache_en ;
	output [7:0] residual_read_inf ;
	output [2:0] res_pres_y_cb_cr_out ;
	output [31:0] current_poc_out ;
	output display_buf_ful;
	
	output cb_res_pres_wr_en	;
	output cb_res_pres_rd_en    ;
	output cb_res_pres_empty    ;
	output cb_res_pres_full     ;
	output cb_res_pres_din 	    ;
	output cb_res_pres_dout	    ;
	
	
	
	// Instantiate the module
pred_dbf_wrapper_wo_pred_fifo pred_dbf_block (

    .clk(clk), 
    .reset(reset), 
    .enable(enable), 
	
	.fifo_in 						(fifo_in),
	.input_fifo_is_empty  			(input_fifo_is_empty),
	.read_en_out 					(read_en_out),

	.y_rd_data_count_in					(y_rd_data_count_in),
	.cb_rd_data_count_in				(cb_rd_data_count_in),
	.cr_rd_data_count_in				(cr_rd_data_count_in),
	
	//luma residual fifo interface
	.y_residual_fifo_in             (y_residual_fifo_in),
	.y_residual_fifo_is_empty_in    (y_residual_fifo_is_empty_in),
	.y_residual_fifo_is_empty_out    (y_residual_fifo_is_empty_out),
	.y_residual_fifo_read_en_out    (y_residual_fifo_read_en_out),
	.yy_prog_empty_thresh_out		(yy_prog_empty_thresh_out),

	//cb residual fifo interface
	.cb_residual_fifo_in            (cb_residual_fifo_in),
	.cb_residual_fifo_is_empty_in   (cb_residual_fifo_is_empty_in),
	.cb_residual_fifo_is_empty_out   (cb_residual_fifo_is_empty_out),
	.cb_residual_fifo_read_en_out   (cb_residual_fifo_read_en_out),
	.cb_prog_empty_thresh_out		(cb_prog_empty_thresh_out),

	//cr residual fifo interface
	.cr_residual_fifo_in            (cr_residual_fifo_in),
	.cr_residual_fifo_is_empty_out   (cr_residual_fifo_is_empty_out),
	.cr_residual_fifo_is_empty_in   (cr_residual_fifo_is_empty_in),
	.cr_residual_fifo_read_en_out   (cr_residual_fifo_read_en_out),
	.cr_prog_empty_thresh_out		(cr_prog_empty_thresh_out),
	
	.display_fifo_is_empty_out	(display_fifo_is_empty_out),
	.display_fifo_rd_en_in		(display_fifo_rd_en_in),				
	.display_fifo_data_out		(display_fifo_data_out),
	.dpb_fifo_is_empty_out		(dpb_fifo_is_empty_out),
	.dpb_fifo_rd_en_in			(dpb_fifo_rd_en_in),		
	.dpb_fifo_data_out			(dpb_fifo_data_out),
	.dpb_axi_addr_out			(dpb_axi_addr_out),
	.sao_poc_out			(sao_poc_out) ,
	
	.log2_ctu_size_out			(log2_ctu_size_out),	
	.pic_width_out				(pic_width_out),	
	.pic_height_out				(pic_height_out),
    
    .mv_col_axi_awid		(mv_col_axi_awid), 
    .mv_col_axi_awlen		(mv_col_axi_awlen), 
    .mv_col_axi_awsize		(mv_col_axi_awsize), 
    .mv_col_axi_awburst		(mv_col_axi_awburst), 
    .mv_col_axi_awlock		(mv_col_axi_awlock), 
    .mv_col_axi_awcache		(mv_col_axi_awcache), 
    .mv_col_axi_awprot		(mv_col_axi_awprot), 
    .mv_col_axi_awvalid		(mv_col_axi_awvalid), 
    .mv_col_axi_awaddr		(mv_col_axi_awaddr), 
    .mv_col_axi_awready		(mv_col_axi_awready), 
    .mv_col_axi_wstrb		(mv_col_axi_wstrb), 
    .mv_col_axi_wlast		(mv_col_axi_wlast), 
    .mv_col_axi_wvalid		(mv_col_axi_wvalid), 
    .mv_col_axi_wdata		(mv_col_axi_wdata), 
    .mv_col_axi_wready		(mv_col_axi_wready), 
    .mv_col_axi_bresp		(mv_col_axi_bresp), 
    .mv_col_axi_bvalid		(mv_col_axi_bvalid), 
    .mv_col_axi_bready		(mv_col_axi_bready), 
    .mv_pref_axi_araddr		(mv_pref_axi_araddr), 
    .mv_pref_axi_arlen		(mv_pref_axi_arlen), 
    .mv_pref_axi_arsize		(mv_pref_axi_arsize), 
    .mv_pref_axi_arburst	(mv_pref_axi_arburst), 
    .mv_pref_axi_arprot		(mv_pref_axi_arprot), 
    .mv_pref_axi_arvalid	(mv_pref_axi_arvalid), 
    .mv_pref_axi_arready	(mv_pref_axi_arready), 
    .mv_pref_axi_rdata		(mv_pref_axi_rdata), 
    .mv_pref_axi_rresp		(mv_pref_axi_rresp), 
    .mv_pref_axi_rlast		(mv_pref_axi_rlast), 
    .mv_pref_axi_rvalid		(mv_pref_axi_rvalid), 
    .mv_pref_axi_rready		(mv_pref_axi_rready), 
    .mv_pref_axi_arlock		(mv_pref_axi_arlock), 
    .mv_pref_axi_arid		(mv_pref_axi_arid), 
    .mv_pref_axi_arcache	(mv_pref_axi_arcache), 
    
    .ref_pix_axi_ar_addr    (ref_pix_axi_ar_addr), 
    .ref_pix_axi_ar_len     (ref_pix_axi_ar_len), 
    .ref_pix_axi_ar_size    (ref_pix_axi_ar_size), 
    .ref_pix_axi_ar_burst   (ref_pix_axi_ar_burst), 
    .ref_pix_axi_ar_prot    (ref_pix_axi_ar_prot), 
    .ref_pix_axi_ar_valid   (ref_pix_axi_ar_valid), 
    .ref_pix_axi_ar_ready   (ref_pix_axi_ar_ready), 
    .ref_pix_axi_r_data     (ref_pix_axi_r_data), 
    .ref_pix_axi_r_resp     (ref_pix_axi_r_resp), 
    .ref_pix_axi_r_last     (ref_pix_axi_r_last), 
    .ref_pix_axi_r_valid    (ref_pix_axi_r_valid), 
    .ref_pix_axi_r_ready    (ref_pix_axi_r_ready)

`ifdef CHIPSCOPE_DEBUG
	,.pred_state_low8b_out		(pred_state_low8b_out  )
	,.inter_pred_stat_8b_out    (inter_pred_stat_8b_out)
	,.mv_state_8bit_out         (mv_state_8bit_out     )
	,.col_state_axi_write         (col_state_axi_write     )
	,.pred_2_dbf_wr_inf         (pred_2_dbf_wr_inf     )
	,.pred_2_dbf_yy             (pred_2_dbf_yy         )
	,.pred_2_dbf_cb             (pred_2_dbf_cb         )
	,.pred_2_dbf_cr             (pred_2_dbf_cr         )
	,.Xc_Yc_out					(Xc_Yc_out)
	,.intra_ready_out	(intra_ready_out	)	
	,.cnf_fifo_out				(cnf_fifo_out)
	,.cnf_fifo_counter				(cnf_fifo_counter)
	,.y_residual_counter				(y_residual_counter)
	,.dbf_main_in_valid				(dbf_main_in_valid)
	,.dbf_main_out_valid				(dbf_main_out_valid)
	
	,.controller_in_valid 	(controller_in_valid 	)
	,.controller_in 		(controller_in 			)
	,.controller_out_valid 	(controller_out_valid 	)
	,.controller_out 		(controller_out 		)
	,.sao_wr_en_out 		(sao_wr_en_out 			)
	,.sao_rd_en_out 		(sao_rd_en_out 			)
	,.sao_wr_data 			(sao_wr_data 			)
	,.sao_rd_data 			(sao_rd_data 			)	
	
		
		,.intra_x_out 			(intra_x_out )
		,.intra_y_out           (intra_y_out )
		,.intra_4x4_valid_out   (intra_4x4_valid_out)
		,.intra_luma_4x4_out    (intra_luma_4x4_out )
		,.inter_x_out 			(inter_x_out )
		,.inter_y_out           (inter_y_out )
		,.inter_4x4_valid_out   (inter_4x4_valid_out)
		,.inter_luma_4x4_out    (inter_luma_4x4_out )
		,.comon_pre_lp_x_out 			(comon_pre_lp_x_out )
		,.comon_pre_lp_y_out           (comon_pre_lp_y_out )
		,.comon_pre_lp_4x4_valid_out   (comon_pre_lp_4x4_valid_out)
		,.comon_pre_lp_luma_4x4_out    (comon_pre_lp_luma_4x4_out )		
		,.dbf_to_sao_out_valid	(dbf_to_sao_out_valid	)
		,.dbf_to_sao_out_x		(dbf_to_sao_out_x		)			
		,.dbf_to_sao_out_y		(dbf_to_sao_out_y		)			
		,.dbf_to_sao_out_q1		(dbf_to_sao_out_q1		)	
		,.test_xT_in_min_luma_filt   (test_xT_in_min_luma_filt	)
		,.test_xT_in_min_luma_cache  (test_xT_in_min_luma_cache )
		,.test_yT_in_min_luma_cache  (test_yT_in_min_luma_cache )
		,.test_yT_in_min_luma_filt   (test_yT_in_min_luma_filt  )
		,.test_luma_filter_ready     (test_luma_filter_ready    )
		,.test_luma_filter_out       (test_luma_filter_out      )
		,.test_cache_addr            (test_cache_addr           )
		,.test_cache_luma_data       (test_cache_luma_data      )
		,.test_cache_valid_in		 (test_cache_valid_in		)		
		,.test_cache_en		 		 (test_cache_en		)		
		,.test_ref_block_en		 		 (test_ref_block_en)		
		,.test_ref_luma_data_4x4		 		 (test_ref_luma_data_4x4)		
		,.residual_read_inf		 			 	(residual_read_inf)		
		,.res_pres_y_cb_cr_out		 			 	(res_pres_y_cb_cr_out)		
		,.current_poc_out		 			 	(current_poc_out)		
		,.cb_res_pres_wr_en		(cb_res_pres_wr_en)
		,.cb_res_pres_rd_en     (cb_res_pres_rd_en)
		,.cb_res_pres_empty     (cb_res_pres_empty)
		,.cb_res_pres_full      (cb_res_pres_full )
		,.cb_res_pres_din 	    (cb_res_pres_din 	)
		,.cb_res_pres_dout	    (cb_res_pres_dout	)
		,.display_buf_ful			 (display_buf_ful)
`endif

		
    );

ref_buf_to_axi_write_master axi_write_block(
    .clk						(clk),
    .reset					(reset),

	//fifo interface
	.fifo_is_empty_in		(dpb_fifo_is_empty_out),		
	.fifo_rd_en_out			(dpb_fifo_rd_en_in),			
	.fifo_data_in			(dpb_fifo_data_out),			   	
	                           			
	.dpb_axi_addr_in			(dpb_axi_addr_out),
	
    .pic_width_in			(pic_width_out),
    .pic_height_in			(pic_height_out),

	
    .axi_awid        		(ref_pic_write_axi_awid),     
    .axi_awlen       		(ref_pic_write_axi_awlen),    
    .axi_awsize      		(ref_pic_write_axi_awsize),   
    .axi_awburst     		(ref_pic_write_axi_awburst),  
    .axi_awlock      		(ref_pic_write_axi_awlock),   
    .axi_awcache     		(ref_pic_write_axi_awcache),  
    .axi_awprot      		(ref_pic_write_axi_awprot),   
    .axi_awvalid     		(ref_pic_write_axi_awvalid),  
    .axi_awaddr      		(ref_pic_write_axi_awaddr),   
    .axi_awready     		(ref_pic_write_axi_awready),  
    .axi_wstrb       		(ref_pic_write_axi_wstrb),    
    .axi_wlast       		(ref_pic_write_axi_wlast),    
    .axi_wvalid      		(ref_pic_write_axi_wvalid),   
    .axi_wdata       		(ref_pic_write_axi_wdata),    
    .axi_wready      		(ref_pic_write_axi_wready),   
    .axi_bid         		(ref_pic_write_axi_bid),     
    .axi_bresp       		(ref_pic_write_axi_bresp),    
    .axi_bvalid      		(ref_pic_write_axi_bvalid),   
    .axi_bready      		(ref_pic_write_axi_bready)    
 );                                 
 
endmodule                           	
