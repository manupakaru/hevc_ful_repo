//Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2014.2 (win64) Build 932637 Wed Jun 11 13:33:10 MDT 2014
//Date        : Wed Dec 10 23:28:26 2014
//Host        : GCLAP4 running 64-bit major release  (build 9200)
//Command     : generate_target zynq_design_1.bd
//Design      : zynq_design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module cabac_modules_imp_AL9V63
   (ACLK,
    B_test,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arburst,
    S00_AXI_arcache,
    S00_AXI_arid,
    S00_AXI_arlen,
    S00_AXI_arlock,
    S00_AXI_arprot,
    S00_AXI_arqos,
    S00_AXI_arready,
    S00_AXI_arsize,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awburst,
    S00_AXI_awcache,
    S00_AXI_awid,
    S00_AXI_awlen,
    S00_AXI_awlock,
    S00_AXI_awprot,
    S00_AXI_awqos,
    S00_AXI_awready,
    S00_AXI_awsize,
    S00_AXI_awvalid,
    S00_AXI_bid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rid,
    S00_AXI_rlast,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wid,
    S00_AXI_wlast,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid,
    bus_struct_reset,
    cabac_to_res_data,
    cabac_to_res_data_count,
    cabac_to_res_wr_en,
    cu_other,
    cu_state,
    cu_x0,
    cu_y0,
    ext_reset_in,
    fullCb,
    fullCont,
    fullCr,
    fullY,
    input_hevc_almost_empty_out_test,
    input_hevc_data_count,
    input_hevc_empty_out_test,
    input_hevc_full_out_test,
    interconnect_aresetn,
    outCb,
    outCont,
    outCr,
    outY,
    push_empty,
    push_empty_out_test,
    push_full_out_test,
    push_read,
    ru_state,
    trafo_POC,
    trafo_clk_en,
    trafo_x0,
    trafo_y0,
    validCb,
    validCont,
    validCr,
    validY);
  input ACLK;
  output [31:0]B_test;
  output [0:0]S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [1:0]S00_AXI_arburst;
  input [3:0]S00_AXI_arcache;
  input [11:0]S00_AXI_arid;
  input [3:0]S00_AXI_arlen;
  input [1:0]S00_AXI_arlock;
  input [2:0]S00_AXI_arprot;
  input [3:0]S00_AXI_arqos;
  output S00_AXI_arready;
  input [2:0]S00_AXI_arsize;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [1:0]S00_AXI_awburst;
  input [3:0]S00_AXI_awcache;
  input [11:0]S00_AXI_awid;
  input [3:0]S00_AXI_awlen;
  input [1:0]S00_AXI_awlock;
  input [2:0]S00_AXI_awprot;
  input [3:0]S00_AXI_awqos;
  output S00_AXI_awready;
  input [2:0]S00_AXI_awsize;
  input S00_AXI_awvalid;
  output [11:0]S00_AXI_bid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  output [11:0]S00_AXI_rid;
  output S00_AXI_rlast;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  input [11:0]S00_AXI_wid;
  input S00_AXI_wlast;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;
  output [0:0]bus_struct_reset;
  output [31:0]cabac_to_res_data;
  output [9:0]cabac_to_res_data_count;
  output cabac_to_res_wr_en;
  output [255:0]cu_other;
  output [6:0]cu_state;
  output [11:0]cu_x0;
  output [11:0]cu_y0;
  input ext_reset_in;
  input fullCb;
  input fullCont;
  input fullCr;
  input fullY;
  output input_hevc_almost_empty_out_test;
  output [13:0]input_hevc_data_count;
  output input_hevc_empty_out_test;
  output input_hevc_full_out_test;
  output [0:0]interconnect_aresetn;
  output [143:0]outCb;
  output [31:0]outCont;
  output [143:0]outCr;
  output [143:0]outY;
  output push_empty;
  output push_empty_out_test;
  output push_full_out_test;
  output push_read;
  output [4:0]ru_state;
  output [15:0]trafo_POC;
  output trafo_clk_en;
  output [11:0]trafo_x0;
  output [11:0]trafo_y0;
  output validCb;
  output validCont;
  output validCr;
  output validY;

  wire [31:0]B_test1;
  wire [31:0]Conn1_ARADDR;
  wire [1:0]Conn1_ARBURST;
  wire [3:0]Conn1_ARCACHE;
  wire [11:0]Conn1_ARID;
  wire [3:0]Conn1_ARLEN;
  wire [1:0]Conn1_ARLOCK;
  wire [2:0]Conn1_ARPROT;
  wire [3:0]Conn1_ARQOS;
  wire Conn1_ARREADY;
  wire [2:0]Conn1_ARSIZE;
  wire Conn1_ARVALID;
  wire [31:0]Conn1_AWADDR;
  wire [1:0]Conn1_AWBURST;
  wire [3:0]Conn1_AWCACHE;
  wire [11:0]Conn1_AWID;
  wire [3:0]Conn1_AWLEN;
  wire [1:0]Conn1_AWLOCK;
  wire [2:0]Conn1_AWPROT;
  wire [3:0]Conn1_AWQOS;
  wire Conn1_AWREADY;
  wire [2:0]Conn1_AWSIZE;
  wire Conn1_AWVALID;
  wire [11:0]Conn1_BID;
  wire Conn1_BREADY;
  wire [1:0]Conn1_BRESP;
  wire Conn1_BVALID;
  wire [31:0]Conn1_RDATA;
  wire [11:0]Conn1_RID;
  wire Conn1_RLAST;
  wire Conn1_RREADY;
  wire [1:0]Conn1_RRESP;
  wire Conn1_RVALID;
  wire [31:0]Conn1_WDATA;
  wire [11:0]Conn1_WID;
  wire Conn1_WLAST;
  wire Conn1_WREADY;
  wire [3:0]Conn1_WSTRB;
  wire Conn1_WVALID;
  wire GND_1;
  wire VCC_1;
(* MARK_DEBUG *)   wire [15:0]axi_addr_1;
(* MARK_DEBUG *)   wire [31:0]axi_count_1;
(* MARK_DEBUG *)   wire [31:0]axi_data_1;
(* MARK_DEBUG *)   wire [15:0]axi_mem_intercon_1_M00_AXI_ARADDR;
(* MARK_DEBUG *)   wire [1:0]axi_mem_intercon_1_M00_AXI_ARBURST;
(* MARK_DEBUG *)   wire [3:0]axi_mem_intercon_1_M00_AXI_ARCACHE;
(* MARK_DEBUG *)   wire [11:0]axi_mem_intercon_1_M00_AXI_ARID;
(* MARK_DEBUG *)   wire [7:0]axi_mem_intercon_1_M00_AXI_ARLEN;
(* MARK_DEBUG *)   wire axi_mem_intercon_1_M00_AXI_ARLOCK;
(* MARK_DEBUG *)   wire [2:0]axi_mem_intercon_1_M00_AXI_ARPROT;
(* MARK_DEBUG *)   wire axi_mem_intercon_1_M00_AXI_ARREADY;
(* MARK_DEBUG *)   wire [2:0]axi_mem_intercon_1_M00_AXI_ARSIZE;
(* MARK_DEBUG *)   wire axi_mem_intercon_1_M00_AXI_ARVALID;
(* MARK_DEBUG *)   wire [15:0]axi_mem_intercon_1_M00_AXI_AWADDR;
(* MARK_DEBUG *)   wire [1:0]axi_mem_intercon_1_M00_AXI_AWBURST;
(* MARK_DEBUG *)   wire [3:0]axi_mem_intercon_1_M00_AXI_AWCACHE;
(* MARK_DEBUG *)   wire [11:0]axi_mem_intercon_1_M00_AXI_AWID;
(* MARK_DEBUG *)   wire [7:0]axi_mem_intercon_1_M00_AXI_AWLEN;
(* MARK_DEBUG *)   wire axi_mem_intercon_1_M00_AXI_AWLOCK;
(* MARK_DEBUG *)   wire [2:0]axi_mem_intercon_1_M00_AXI_AWPROT;
(* MARK_DEBUG *)   wire axi_mem_intercon_1_M00_AXI_AWREADY;
(* MARK_DEBUG *)   wire [2:0]axi_mem_intercon_1_M00_AXI_AWSIZE;
(* MARK_DEBUG *)   wire axi_mem_intercon_1_M00_AXI_AWVALID;
(* MARK_DEBUG *)   wire [11:0]axi_mem_intercon_1_M00_AXI_BID;
(* MARK_DEBUG *)   wire axi_mem_intercon_1_M00_AXI_BREADY;
(* MARK_DEBUG *)   wire [1:0]axi_mem_intercon_1_M00_AXI_BRESP;
(* MARK_DEBUG *)   wire axi_mem_intercon_1_M00_AXI_BVALID;
(* MARK_DEBUG *)   wire [31:0]axi_mem_intercon_1_M00_AXI_RDATA;
(* MARK_DEBUG *)   wire [11:0]axi_mem_intercon_1_M00_AXI_RID;
(* MARK_DEBUG *)   wire axi_mem_intercon_1_M00_AXI_RLAST;
(* MARK_DEBUG *)   wire axi_mem_intercon_1_M00_AXI_RREADY;
(* MARK_DEBUG *)   wire [1:0]axi_mem_intercon_1_M00_AXI_RRESP;
(* MARK_DEBUG *)   wire axi_mem_intercon_1_M00_AXI_RVALID;
(* MARK_DEBUG *)   wire [31:0]axi_mem_intercon_1_M00_AXI_WDATA;
(* MARK_DEBUG *)   wire axi_mem_intercon_1_M00_AXI_WLAST;
(* MARK_DEBUG *)   wire axi_mem_intercon_1_M00_AXI_WREADY;
(* MARK_DEBUG *)   wire [3:0]axi_mem_intercon_1_M00_AXI_WSTRB;
(* MARK_DEBUG *)   wire axi_mem_intercon_1_M00_AXI_WVALID;
(* MARK_DEBUG *)   wire [2:0]axi_state_1;
  wire [143:0]cabac_ngc_0_outCb;
  wire [31:0]cabac_ngc_0_outCont;
  wire [143:0]cabac_ngc_0_outCr;
  wire [143:0]cabac_ngc_0_outY;
  wire cabac_ngc_0_validCb;
  wire cabac_ngc_0_validCont;
  wire cabac_ngc_0_validCr;
  wire cabac_ngc_0_validY;
  wire [31:0]cabac_to_res_data1;
  wire [9:0]cabac_to_res_data_count1;
  wire cabac_to_res_wr_en1;
  wire [255:0]cu_other1;
  wire [6:0]cu_state1;
  wire [11:0]cu_x1;
  wire [11:0]cu_y1;
  wire ext_reset_in_1;
  wire fullCb_1;
  wire fullCont_1;
  wire fullCr_1;
  wire fullY_1;
  wire input_hevc_almost_empty_out_test1;
  wire [13:0]input_hevc_data_count1;
  wire input_hevc_empty_out_test1;
  wire input_hevc_full_out_test1;
(* MARK_DEBUG *)   wire [27:0]port5;
  wire processing_system7_0_FCLK_CLK0;
  wire push_empty1;
  wire push_empty_out_test1;
  wire push_full_out_test1;
  wire push_read1;
  wire [0:0]rst_processing_system7_0_125M_bus_struct_reset;
  wire [0:0]rst_processing_system7_0_125M_interconnect_aresetn;
  wire [0:0]rst_processing_system7_0_125M_peripheral_aresetn;
  wire [4:0]ru_state1;
(* MARK_DEBUG *)   wire [31:0]total_cb;
(* MARK_DEBUG *)   wire [31:0]total_config;
(* MARK_DEBUG *)   wire [31:0]total_cr;
(* MARK_DEBUG *)   wire [31:0]total_push;
(* MARK_DEBUG *)   wire [31:0]total_y;
  wire [15:0]trafo_POC1;
  wire trafo_clk_en1;
  wire [11:0]trafo_x1;
  wire [11:0]trafo_y1;

  assign B_test[31:0] = B_test1;
  assign Conn1_ARADDR = S00_AXI_araddr[31:0];
  assign Conn1_ARBURST = S00_AXI_arburst[1:0];
  assign Conn1_ARCACHE = S00_AXI_arcache[3:0];
  assign Conn1_ARID = S00_AXI_arid[11:0];
  assign Conn1_ARLEN = S00_AXI_arlen[3:0];
  assign Conn1_ARLOCK = S00_AXI_arlock[1:0];
  assign Conn1_ARPROT = S00_AXI_arprot[2:0];
  assign Conn1_ARQOS = S00_AXI_arqos[3:0];
  assign Conn1_ARSIZE = S00_AXI_arsize[2:0];
  assign Conn1_ARVALID = S00_AXI_arvalid;
  assign Conn1_AWADDR = S00_AXI_awaddr[31:0];
  assign Conn1_AWBURST = S00_AXI_awburst[1:0];
  assign Conn1_AWCACHE = S00_AXI_awcache[3:0];
  assign Conn1_AWID = S00_AXI_awid[11:0];
  assign Conn1_AWLEN = S00_AXI_awlen[3:0];
  assign Conn1_AWLOCK = S00_AXI_awlock[1:0];
  assign Conn1_AWPROT = S00_AXI_awprot[2:0];
  assign Conn1_AWQOS = S00_AXI_awqos[3:0];
  assign Conn1_AWSIZE = S00_AXI_awsize[2:0];
  assign Conn1_AWVALID = S00_AXI_awvalid;
  assign Conn1_BREADY = S00_AXI_bready;
  assign Conn1_RREADY = S00_AXI_rready;
  assign Conn1_WDATA = S00_AXI_wdata[31:0];
  assign Conn1_WID = S00_AXI_wid[11:0];
  assign Conn1_WLAST = S00_AXI_wlast;
  assign Conn1_WSTRB = S00_AXI_wstrb[3:0];
  assign Conn1_WVALID = S00_AXI_wvalid;
  assign S00_ARESETN[0] = rst_processing_system7_0_125M_peripheral_aresetn;
  assign S00_AXI_arready = Conn1_ARREADY;
  assign S00_AXI_awready = Conn1_AWREADY;
  assign S00_AXI_bid[11:0] = Conn1_BID;
  assign S00_AXI_bresp[1:0] = Conn1_BRESP;
  assign S00_AXI_bvalid = Conn1_BVALID;
  assign S00_AXI_rdata[31:0] = Conn1_RDATA;
  assign S00_AXI_rid[11:0] = Conn1_RID;
  assign S00_AXI_rlast = Conn1_RLAST;
  assign S00_AXI_rresp[1:0] = Conn1_RRESP;
  assign S00_AXI_rvalid = Conn1_RVALID;
  assign S00_AXI_wready = Conn1_WREADY;
  assign bus_struct_reset[0] = rst_processing_system7_0_125M_bus_struct_reset;
  assign cabac_to_res_data[31:0] = cabac_to_res_data1;
  assign cabac_to_res_data_count[9:0] = cabac_to_res_data_count1;
  assign cabac_to_res_wr_en = cabac_to_res_wr_en1;
  assign cu_other[255:0] = cu_other1;
  assign cu_state[6:0] = cu_state1;
  assign cu_x0[11:0] = cu_x1;
  assign cu_y0[11:0] = cu_y1;
  assign ext_reset_in_1 = ext_reset_in;
  assign fullCb_1 = fullCb;
  assign fullCont_1 = fullCont;
  assign fullCr_1 = fullCr;
  assign fullY_1 = fullY;
  assign input_hevc_almost_empty_out_test = input_hevc_almost_empty_out_test1;
  assign input_hevc_data_count[13:0] = input_hevc_data_count1;
  assign input_hevc_empty_out_test = input_hevc_empty_out_test1;
  assign input_hevc_full_out_test = input_hevc_full_out_test1;
  assign interconnect_aresetn[0] = rst_processing_system7_0_125M_interconnect_aresetn;
  assign outCb[143:0] = cabac_ngc_0_outCb;
  assign outCont[31:0] = cabac_ngc_0_outCont;
  assign outCr[143:0] = cabac_ngc_0_outCr;
  assign outY[143:0] = cabac_ngc_0_outY;
  assign processing_system7_0_FCLK_CLK0 = ACLK;
  assign push_empty = push_empty1;
  assign push_empty_out_test = push_empty_out_test1;
  assign push_full_out_test = push_full_out_test1;
  assign push_read = push_read1;
  assign ru_state[4:0] = ru_state1;
  assign trafo_POC[15:0] = trafo_POC1;
  assign trafo_clk_en = trafo_clk_en1;
  assign trafo_x0[11:0] = trafo_x1;
  assign trafo_y0[11:0] = trafo_y1;
  assign validCb = cabac_ngc_0_validCb;
  assign validCont = cabac_ngc_0_validCont;
  assign validCr = cabac_ngc_0_validCr;
  assign validY = cabac_ngc_0_validY;
GND GND
       (.G(GND_1));
VCC VCC
       (.P(VCC_1));
zynq_design_1_axi_mem_intercon_1_0 axi_mem_intercon_1
       (.ACLK(processing_system7_0_FCLK_CLK0),
        .ARESETN(rst_processing_system7_0_125M_interconnect_aresetn),
        .M00_ACLK(processing_system7_0_FCLK_CLK0),
        .M00_ARESETN(rst_processing_system7_0_125M_peripheral_aresetn),
        .M00_AXI_araddr(axi_mem_intercon_1_M00_AXI_ARADDR),
        .M00_AXI_arburst(axi_mem_intercon_1_M00_AXI_ARBURST),
        .M00_AXI_arcache(axi_mem_intercon_1_M00_AXI_ARCACHE),
        .M00_AXI_arid(axi_mem_intercon_1_M00_AXI_ARID),
        .M00_AXI_arlen(axi_mem_intercon_1_M00_AXI_ARLEN),
        .M00_AXI_arlock(axi_mem_intercon_1_M00_AXI_ARLOCK),
        .M00_AXI_arprot(axi_mem_intercon_1_M00_AXI_ARPROT),
        .M00_AXI_arready(axi_mem_intercon_1_M00_AXI_ARREADY),
        .M00_AXI_arsize(axi_mem_intercon_1_M00_AXI_ARSIZE),
        .M00_AXI_arvalid(axi_mem_intercon_1_M00_AXI_ARVALID),
        .M00_AXI_awaddr(axi_mem_intercon_1_M00_AXI_AWADDR),
        .M00_AXI_awburst(axi_mem_intercon_1_M00_AXI_AWBURST),
        .M00_AXI_awcache(axi_mem_intercon_1_M00_AXI_AWCACHE),
        .M00_AXI_awid(axi_mem_intercon_1_M00_AXI_AWID),
        .M00_AXI_awlen(axi_mem_intercon_1_M00_AXI_AWLEN),
        .M00_AXI_awlock(axi_mem_intercon_1_M00_AXI_AWLOCK),
        .M00_AXI_awprot(axi_mem_intercon_1_M00_AXI_AWPROT),
        .M00_AXI_awready(axi_mem_intercon_1_M00_AXI_AWREADY),
        .M00_AXI_awsize(axi_mem_intercon_1_M00_AXI_AWSIZE),
        .M00_AXI_awvalid(axi_mem_intercon_1_M00_AXI_AWVALID),
        .M00_AXI_bid(axi_mem_intercon_1_M00_AXI_BID),
        .M00_AXI_bready(axi_mem_intercon_1_M00_AXI_BREADY),
        .M00_AXI_bresp(axi_mem_intercon_1_M00_AXI_BRESP),
        .M00_AXI_bvalid(axi_mem_intercon_1_M00_AXI_BVALID),
        .M00_AXI_rdata(axi_mem_intercon_1_M00_AXI_RDATA),
        .M00_AXI_rid(axi_mem_intercon_1_M00_AXI_RID),
        .M00_AXI_rlast(axi_mem_intercon_1_M00_AXI_RLAST),
        .M00_AXI_rready(axi_mem_intercon_1_M00_AXI_RREADY),
        .M00_AXI_rresp(axi_mem_intercon_1_M00_AXI_RRESP),
        .M00_AXI_rvalid(axi_mem_intercon_1_M00_AXI_RVALID),
        .M00_AXI_wdata(axi_mem_intercon_1_M00_AXI_WDATA),
        .M00_AXI_wlast(axi_mem_intercon_1_M00_AXI_WLAST),
        .M00_AXI_wready(axi_mem_intercon_1_M00_AXI_WREADY),
        .M00_AXI_wstrb(axi_mem_intercon_1_M00_AXI_WSTRB),
        .M00_AXI_wvalid(axi_mem_intercon_1_M00_AXI_WVALID),
        .S00_ACLK(processing_system7_0_FCLK_CLK0),
        .S00_ARESETN(rst_processing_system7_0_125M_peripheral_aresetn),
        .S00_AXI_araddr(Conn1_ARADDR),
        .S00_AXI_arburst(Conn1_ARBURST),
        .S00_AXI_arcache(Conn1_ARCACHE),
        .S00_AXI_arid(Conn1_ARID),
        .S00_AXI_arlen(Conn1_ARLEN),
        .S00_AXI_arlock(Conn1_ARLOCK),
        .S00_AXI_arprot(Conn1_ARPROT),
        .S00_AXI_arqos(Conn1_ARQOS),
        .S00_AXI_arready(Conn1_ARREADY),
        .S00_AXI_arsize(Conn1_ARSIZE),
        .S00_AXI_arvalid(Conn1_ARVALID),
        .S00_AXI_awaddr(Conn1_AWADDR),
        .S00_AXI_awburst(Conn1_AWBURST),
        .S00_AXI_awcache(Conn1_AWCACHE),
        .S00_AXI_awid(Conn1_AWID),
        .S00_AXI_awlen(Conn1_AWLEN),
        .S00_AXI_awlock(Conn1_AWLOCK),
        .S00_AXI_awprot(Conn1_AWPROT),
        .S00_AXI_awqos(Conn1_AWQOS),
        .S00_AXI_awready(Conn1_AWREADY),
        .S00_AXI_awsize(Conn1_AWSIZE),
        .S00_AXI_awvalid(Conn1_AWVALID),
        .S00_AXI_bid(Conn1_BID),
        .S00_AXI_bready(Conn1_BREADY),
        .S00_AXI_bresp(Conn1_BRESP),
        .S00_AXI_bvalid(Conn1_BVALID),
        .S00_AXI_rdata(Conn1_RDATA),
        .S00_AXI_rid(Conn1_RID),
        .S00_AXI_rlast(Conn1_RLAST),
        .S00_AXI_rready(Conn1_RREADY),
        .S00_AXI_rresp(Conn1_RRESP),
        .S00_AXI_rvalid(Conn1_RVALID),
        .S00_AXI_wdata(Conn1_WDATA),
        .S00_AXI_wid(Conn1_WID),
        .S00_AXI_wlast(Conn1_WLAST),
        .S00_AXI_wready(Conn1_WREADY),
        .S00_AXI_wstrb(Conn1_WSTRB),
        .S00_AXI_wvalid(Conn1_WVALID));
zynq_design_1_cabac_ngc_0_0 cabac_ngc_0
       (.B_test(B_test1),
        .axi_addr(axi_addr_1),
        .axi_araddr(axi_mem_intercon_1_M00_AXI_ARADDR),
        .axi_arburst(axi_mem_intercon_1_M00_AXI_ARBURST),
        .axi_arcache(axi_mem_intercon_1_M00_AXI_ARCACHE),
        .axi_arid(axi_mem_intercon_1_M00_AXI_ARID),
        .axi_arlen(axi_mem_intercon_1_M00_AXI_ARLEN),
        .axi_arlock(axi_mem_intercon_1_M00_AXI_ARLOCK),
        .axi_arprot(axi_mem_intercon_1_M00_AXI_ARPROT),
        .axi_arready(axi_mem_intercon_1_M00_AXI_ARREADY),
        .axi_arsize(axi_mem_intercon_1_M00_AXI_ARSIZE),
        .axi_arvalid(axi_mem_intercon_1_M00_AXI_ARVALID),
        .axi_awaddr(axi_mem_intercon_1_M00_AXI_AWADDR),
        .axi_awburst(axi_mem_intercon_1_M00_AXI_AWBURST),
        .axi_awcache(axi_mem_intercon_1_M00_AXI_AWCACHE),
        .axi_awid(axi_mem_intercon_1_M00_AXI_AWID),
        .axi_awlen(axi_mem_intercon_1_M00_AXI_AWLEN),
        .axi_awlock(axi_mem_intercon_1_M00_AXI_AWLOCK),
        .axi_awprot(axi_mem_intercon_1_M00_AXI_AWPROT),
        .axi_awready(axi_mem_intercon_1_M00_AXI_AWREADY),
        .axi_awsize(axi_mem_intercon_1_M00_AXI_AWSIZE),
        .axi_awvalid(axi_mem_intercon_1_M00_AXI_AWVALID),
        .axi_bid(axi_mem_intercon_1_M00_AXI_BID),
        .axi_bready(axi_mem_intercon_1_M00_AXI_BREADY),
        .axi_bresp(axi_mem_intercon_1_M00_AXI_BRESP),
        .axi_bvalid(axi_mem_intercon_1_M00_AXI_BVALID),
        .axi_count(axi_count_1),
        .axi_data(axi_data_1),
        .axi_rdata(axi_mem_intercon_1_M00_AXI_RDATA),
        .axi_rid(axi_mem_intercon_1_M00_AXI_RID),
        .axi_rlast(axi_mem_intercon_1_M00_AXI_RLAST),
        .axi_rready(axi_mem_intercon_1_M00_AXI_RREADY),
        .axi_rresp(axi_mem_intercon_1_M00_AXI_RRESP),
        .axi_rvalid(axi_mem_intercon_1_M00_AXI_RVALID),
        .axi_state(axi_state_1),
        .axi_wdata(axi_mem_intercon_1_M00_AXI_WDATA),
        .axi_wlast(axi_mem_intercon_1_M00_AXI_WLAST),
        .axi_wready(axi_mem_intercon_1_M00_AXI_WREADY),
        .axi_wstrb(axi_mem_intercon_1_M00_AXI_WSTRB),
        .axi_wvalid(axi_mem_intercon_1_M00_AXI_WVALID),
        .cabac_to_res_data(cabac_to_res_data1),
        .cabac_to_res_data_count(cabac_to_res_data_count1),
        .cabac_to_res_wr_en(cabac_to_res_wr_en1),
        .clk(processing_system7_0_FCLK_CLK0),
        .cu_other(cu_other1),
        .cu_state(cu_state1),
        .cu_x0(cu_x1),
        .cu_y0(cu_y1),
        .fullCb(fullCb_1),
        .fullCont(fullCont_1),
        .fullCr(fullCr_1),
        .fullY(fullY_1),
        .input_hevc_almost_empty_out_test(input_hevc_almost_empty_out_test1),
        .input_hevc_data_count(input_hevc_data_count1),
        .input_hevc_empty_out_test(input_hevc_empty_out_test1),
        .input_hevc_full_out_test(input_hevc_full_out_test1),
        .outCb(cabac_ngc_0_outCb),
        .outCont(cabac_ngc_0_outCont),
        .outCr(cabac_ngc_0_outCr),
        .outY(cabac_ngc_0_outY),
        .port5(port5),
        .push_empty(push_empty1),
        .push_empty_out_test(push_empty_out_test1),
        .push_full_out_test(push_full_out_test1),
        .push_read(push_read1),
        .rst(rst_processing_system7_0_125M_interconnect_aresetn),
        .ru_state(ru_state1),
        .total_cb(total_cb),
        .total_config(total_config),
        .total_cr(total_cr),
        .total_push(total_push),
        .total_y(total_y),
        .trafo_POC(trafo_POC1),
        .trafo_clk_en(trafo_clk_en1),
        .trafo_x0(trafo_x1),
        .trafo_y0(trafo_y1),
        .validCb(cabac_ngc_0_validCb),
        .validCont(cabac_ngc_0_validCont),
        .validCr(cabac_ngc_0_validCr),
        .validY(cabac_ngc_0_validY));
zynq_design_1_rst_processing_system7_0_125M_0 rst_processing_system7_0_125M
       (.aux_reset_in(VCC_1),
        .bus_struct_reset(rst_processing_system7_0_125M_bus_struct_reset),
        .dcm_locked(VCC_1),
        .ext_reset_in(ext_reset_in_1),
        .interconnect_aresetn(rst_processing_system7_0_125M_interconnect_aresetn),
        .mb_debug_sys_rst(GND_1),
        .peripheral_aresetn(rst_processing_system7_0_125M_peripheral_aresetn),
        .slowest_sync_clk(processing_system7_0_FCLK_CLK0));
endmodule

module dec_block_wo_cabac_imp_T478Q4
   (DDR3_addr,
    DDR3_ba,
    DDR3_cas_n,
    DDR3_ck_n,
    DDR3_ck_p,
    DDR3_cke,
    DDR3_cs_n,
    DDR3_dm,
    DDR3_dq,
    DDR3_dqs_n,
    DDR3_dqs_p,
    DDR3_odt,
    DDR3_ras_n,
    DDR3_reset_n,
    DDR3_we_n,
    SYS_CLK_clk_n,
    SYS_CLK_clk_p,
    cb_rd_data_count_in,
    cb_residual_read_inf_empty,
    cb_residual_read_inf_rd_data,
    cb_residual_read_inf_rd_en,
    cr_rd_data_count_in,
    cr_residual_read_inf_empty,
    cr_residual_read_inf_rd_data,
    cr_residual_read_inf_rd_en,
    design_reset,
    design_reset_n,
    display_fifo_read_inf_empty,
    display_fifo_read_inf_rd_data,
    display_fifo_read_inf_rd_en,
    fifo_in,
    input_fifo_is_empty,
    pic_height_out,
    pic_width_out,
    read_en_out,
    sao_poc_out,
    sys_rst,
    ui_clk,
    ui_clk_sync_rst,
    wr_clk,
    y_rd_data_count_in,
    y_residual_interface_empty,
    y_residual_interface_rd_data,
    y_residual_interface_rd_en);
  output [13:0]DDR3_addr;
  output [2:0]DDR3_ba;
  output DDR3_cas_n;
  output [0:0]DDR3_ck_n;
  output [0:0]DDR3_ck_p;
  output [0:0]DDR3_cke;
  output [0:0]DDR3_cs_n;
  output [7:0]DDR3_dm;
  inout [63:0]DDR3_dq;
  inout [7:0]DDR3_dqs_n;
  inout [7:0]DDR3_dqs_p;
  output [0:0]DDR3_odt;
  output DDR3_ras_n;
  output DDR3_reset_n;
  output DDR3_we_n;
  input SYS_CLK_clk_n;
  input SYS_CLK_clk_p;
  input [10:0]cb_rd_data_count_in;
  input cb_residual_read_inf_empty;
  input [143:0]cb_residual_read_inf_rd_data;
  output cb_residual_read_inf_rd_en;
  input [10:0]cr_rd_data_count_in;
  input cr_residual_read_inf_empty;
  input [143:0]cr_residual_read_inf_rd_data;
  output cr_residual_read_inf_rd_en;
  output design_reset;
  output design_reset_n;
  output display_fifo_read_inf_empty;
  output [783:0]display_fifo_read_inf_rd_data;
  input display_fifo_read_inf_rd_en;
  input [31:0]fifo_in;
  input input_fifo_is_empty;
  output [11:0]pic_height_out;
  output [11:0]pic_width_out;
  output read_en_out;
  output [31:0]sao_poc_out;
  input [0:0]sys_rst;
  output ui_clk;
  output ui_clk_sync_rst;
  input wr_clk;
  input [12:0]y_rd_data_count_in;
  input y_residual_interface_empty;
  input [143:0]y_residual_interface_rd_data;
  output y_residual_interface_rd_en;

(* MARK_DEBUG *)   wire Conn1_EMPTY;
(* MARK_DEBUG *)   wire [143:0]Conn1_RD_DATA;
(* MARK_DEBUG *)   wire Conn1_RD_EN;
  wire Conn2_EMPTY;
  wire [143:0]Conn2_RD_DATA;
  wire Conn2_RD_EN;
  wire Conn3_EMPTY;
  wire [143:0]Conn3_RD_DATA;
  wire Conn3_RD_EN;
  wire GND_1;
  wire SYS_CLK_1_CLK_N;
  wire SYS_CLK_1_CLK_P;
  wire VCC_1;
(* MARK_DEBUG *)   wire [23:0]Xc_Yc_out;
(* MARK_DEBUG *)   wire [31:0]axi_inter_con_0_M00_AXI_ARADDR;
(* MARK_DEBUG *)   wire [1:0]axi_inter_con_0_M00_AXI_ARBURST;
(* MARK_DEBUG *)   wire [3:0]axi_inter_con_0_M00_AXI_ARCACHE;
(* MARK_DEBUG *)   wire [3:0]axi_inter_con_0_M00_AXI_ARID;
(* MARK_DEBUG *)   wire [7:0]axi_inter_con_0_M00_AXI_ARLEN;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_ARLOCK;
(* MARK_DEBUG *)   wire [2:0]axi_inter_con_0_M00_AXI_ARPROT;
(* MARK_DEBUG *)   wire [3:0]axi_inter_con_0_M00_AXI_ARQOS;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_ARREADY;
(* MARK_DEBUG *)   wire [2:0]axi_inter_con_0_M00_AXI_ARSIZE;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_ARVALID;
(* MARK_DEBUG *)   wire [31:0]axi_inter_con_0_M00_AXI_AWADDR;
(* MARK_DEBUG *)   wire [1:0]axi_inter_con_0_M00_AXI_AWBURST;
(* MARK_DEBUG *)   wire [3:0]axi_inter_con_0_M00_AXI_AWCACHE;
(* MARK_DEBUG *)   wire [3:0]axi_inter_con_0_M00_AXI_AWID;
(* MARK_DEBUG *)   wire [7:0]axi_inter_con_0_M00_AXI_AWLEN;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_AWLOCK;
(* MARK_DEBUG *)   wire [2:0]axi_inter_con_0_M00_AXI_AWPROT;
(* MARK_DEBUG *)   wire [3:0]axi_inter_con_0_M00_AXI_AWQOS;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_AWREADY;
(* MARK_DEBUG *)   wire [2:0]axi_inter_con_0_M00_AXI_AWSIZE;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_AWVALID;
(* MARK_DEBUG *)   wire [3:0]axi_inter_con_0_M00_AXI_BID;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_BREADY;
(* MARK_DEBUG *)   wire [1:0]axi_inter_con_0_M00_AXI_BRESP;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_BVALID;
(* MARK_DEBUG *)   wire [511:0]axi_inter_con_0_M00_AXI_RDATA;
(* MARK_DEBUG *)   wire [3:0]axi_inter_con_0_M00_AXI_RID;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_RLAST;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_RREADY;
(* MARK_DEBUG *)   wire [1:0]axi_inter_con_0_M00_AXI_RRESP;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_RVALID;
(* MARK_DEBUG *)   wire [511:0]axi_inter_con_0_M00_AXI_WDATA;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_WLAST;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_WREADY;
(* MARK_DEBUG *)   wire [63:0]axi_inter_con_0_M00_AXI_WSTRB;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_WVALID;
(* MARK_DEBUG *)   wire [9:0]cb_prog_empty_thresh_out;
  wire [10:0]cb_rd_data_count_in_1;
(* MARK_DEBUG *)   wire [31:0]cnf_fifo_counter;
(* MARK_DEBUG *)   wire [31:0]cnf_fifo_out;
(* MARK_DEBUG *)   wire comon_pre_lp_4x4_valid_out_1;
(* MARK_DEBUG *)   wire [127:0]comon_pre_lp_luma_4x4_out_1;
(* MARK_DEBUG *)   wire [11:0]comon_pre_lp_x_out_1;
(* MARK_DEBUG *)   wire [11:0]comon_pre_lp_y_out_1;
  wire [10:0]cr_rd_data_count_in_1;
(* MARK_DEBUG *)   wire [31:0]current_poc_out;
(* MARK_DEBUG *)   wire dbf_to_sao_out_valid;
(* MARK_DEBUG *)   wire display_buf_ful;
(* MARK_DEBUG *)   wire display_fifo_read_inf_1_EMPTY;
(* MARK_DEBUG *)   wire [783:0]display_fifo_read_inf_1_RD_DATA;
(* MARK_DEBUG *)   wire display_fifo_read_inf_1_RD_EN;
(* MARK_DEBUG *)   wire [31:0]fifo_in_1;
  wire [31:0]hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWADDR;
  wire [1:0]hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWBURST;
  wire [3:0]hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWCACHE;
  wire hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWID;
  wire [7:0]hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWLEN;
  wire hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWLOCK;
  wire [2:0]hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWPROT;
  wire hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWREADY;
  wire [2:0]hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWSIZE;
  wire hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWVALID;
  wire hevc_top_wo_fifo_0_mv_col_axi_master_inf_BREADY;
  wire [1:0]hevc_top_wo_fifo_0_mv_col_axi_master_inf_BRESP;
  wire hevc_top_wo_fifo_0_mv_col_axi_master_inf_BVALID;
  wire [511:0]hevc_top_wo_fifo_0_mv_col_axi_master_inf_WDATA;
  wire hevc_top_wo_fifo_0_mv_col_axi_master_inf_WLAST;
  wire hevc_top_wo_fifo_0_mv_col_axi_master_inf_WREADY;
  wire [63:0]hevc_top_wo_fifo_0_mv_col_axi_master_inf_WSTRB;
  wire hevc_top_wo_fifo_0_mv_col_axi_master_inf_WVALID;
  wire [31:0]hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARADDR;
  wire [1:0]hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARBURST;
  wire [3:0]hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARCACHE;
  wire hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARID;
  wire [7:0]hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARLEN;
  wire hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARLOCK;
  wire [2:0]hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARPROT;
  wire hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARREADY;
  wire [2:0]hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARSIZE;
  wire hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARVALID;
  wire [511:0]hevc_top_wo_fifo_0_mv_pref_axi_master_inf_RDATA;
  wire hevc_top_wo_fifo_0_mv_pref_axi_master_inf_RLAST;
  wire hevc_top_wo_fifo_0_mv_pref_axi_master_inf_RREADY;
  wire [1:0]hevc_top_wo_fifo_0_mv_pref_axi_master_inf_RRESP;
  wire hevc_top_wo_fifo_0_mv_pref_axi_master_inf_RVALID;
  wire [11:0]hevc_top_wo_fifo_0_pic_height_out;
  wire [11:0]hevc_top_wo_fifo_0_pic_width_out;
(* MARK_DEBUG *)   wire hevc_top_wo_fifo_0_read_en_out;
  wire [31:0]hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWADDR;
  wire [1:0]hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWBURST;
  wire [3:0]hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWCACHE;
  wire hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWID;
  wire [7:0]hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWLEN;
  wire hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWLOCK;
  wire [2:0]hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWPROT;
  wire hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWREADY;
  wire [2:0]hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWSIZE;
  wire hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWVALID;
  wire [0:0]hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_BID;
  wire hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_BREADY;
  wire [1:0]hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_BRESP;
  wire hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_BVALID;
  wire [511:0]hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_WDATA;
  wire hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_WLAST;
  wire hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_WREADY;
  wire [63:0]hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_WSTRB;
  wire hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_WVALID;
  wire [31:0]hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARADDR;
  wire [1:0]hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARBURST;
  wire [7:0]hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARLEN;
  wire [2:0]hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARPROT;
  wire hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARREADY;
  wire [2:0]hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARSIZE;
  wire hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARVALID;
  wire [511:0]hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_RDATA;
  wire hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_RLAST;
  wire hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_RREADY;
  wire [1:0]hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_RRESP;
  wire hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_RVALID;
  wire [31:0]hevc_top_wo_fifo_0_sao_poc_out;
(* MARK_DEBUG *)   wire input_fifo_is_empty_1;
(* MARK_DEBUG *)   wire inter_4x4_valid_out;
(* MARK_DEBUG *)   wire [127:0]inter_luma_4x4_out_1;
(* MARK_DEBUG *)   wire [7:0]inter_pred_stat_8b_out;
(* MARK_DEBUG *)   wire [11:0]inter_x_out;
(* MARK_DEBUG *)   wire [11:0]inter_y_out;
  wire intra_4x4_valid_out_1;
(* MARK_DEBUG *)   wire [127:0]intra_luma_4x4_out;
(* MARK_DEBUG *)   wire [5:0]intra_ready_out;
(* MARK_DEBUG *)   wire [11:0]intra_x_out;
(* MARK_DEBUG *)   wire [11:0]intra_y_out;
  wire [13:0]mig_7series_0_DDR3_ADDR;
  wire [2:0]mig_7series_0_DDR3_BA;
  wire mig_7series_0_DDR3_CAS_N;
  wire [0:0]mig_7series_0_DDR3_CKE;
  wire [0:0]mig_7series_0_DDR3_CK_N;
  wire [0:0]mig_7series_0_DDR3_CK_P;
  wire [0:0]mig_7series_0_DDR3_CS_N;
  wire [7:0]mig_7series_0_DDR3_DM;
  wire [63:0]mig_7series_0_DDR3_DQ;
  wire [7:0]mig_7series_0_DDR3_DQS_N;
  wire [7:0]mig_7series_0_DDR3_DQS_P;
  wire [0:0]mig_7series_0_DDR3_ODT;
  wire mig_7series_0_DDR3_RAS_N;
  wire mig_7series_0_DDR3_RESET_N;
  wire mig_7series_0_DDR3_WE_N;
  wire mig_7series_0_init_calib_complete;
  wire mig_7series_0_mmcm_locked;
  wire mig_7series_0_ui_clk;
  wire mig_7series_0_ui_clk_sync_rst;
  wire mig_setup_reset_0_areset_n;
  wire mig_setup_reset_0_design_reset;
  wire mig_setup_reset_0_design_reset_n;
(* MARK_DEBUG *)   wire [7:0]mv_state_8bit_out;
(* MARK_DEBUG *)   wire [7:0]pred_state_low8b_out;
(* MARK_DEBUG *)   wire [7:0]residual_read_inf;
  wire [0:0]rst_processing_system7_0_125M_bus_struct_reset;
  wire [0:0]xlconstant_0_dout;
  wire [12:0]y_rd_data_count_in_1;
(* MARK_DEBUG *)   wire [31:0]y_residual_counter;
(* MARK_DEBUG *)   wire y_residual_fifo_is_empty_out;
(* MARK_DEBUG *)   wire [11:0]yy_prog_empty_thresh_out;

  assign Conn1_EMPTY = y_residual_interface_empty;
  assign Conn1_RD_DATA = y_residual_interface_rd_data[143:0];
  assign Conn2_EMPTY = cb_residual_read_inf_empty;
  assign Conn2_RD_DATA = cb_residual_read_inf_rd_data[143:0];
  assign Conn3_EMPTY = cr_residual_read_inf_empty;
  assign Conn3_RD_DATA = cr_residual_read_inf_rd_data[143:0];
  assign DDR3_addr[13:0] = mig_7series_0_DDR3_ADDR;
  assign DDR3_ba[2:0] = mig_7series_0_DDR3_BA;
  assign DDR3_cas_n = mig_7series_0_DDR3_CAS_N;
  assign DDR3_ck_n[0] = mig_7series_0_DDR3_CK_N;
  assign DDR3_ck_p[0] = mig_7series_0_DDR3_CK_P;
  assign DDR3_cke[0] = mig_7series_0_DDR3_CKE;
  assign DDR3_cs_n[0] = mig_7series_0_DDR3_CS_N;
  assign DDR3_dm[7:0] = mig_7series_0_DDR3_DM;
  assign DDR3_odt[0] = mig_7series_0_DDR3_ODT;
  assign DDR3_ras_n = mig_7series_0_DDR3_RAS_N;
  assign DDR3_reset_n = mig_7series_0_DDR3_RESET_N;
  assign DDR3_we_n = mig_7series_0_DDR3_WE_N;
  assign SYS_CLK_1_CLK_N = SYS_CLK_clk_n;
  assign SYS_CLK_1_CLK_P = SYS_CLK_clk_p;
  assign cb_rd_data_count_in_1 = cb_rd_data_count_in[10:0];
  assign cb_residual_read_inf_rd_en = Conn2_RD_EN;
  assign cr_rd_data_count_in_1 = cr_rd_data_count_in[10:0];
  assign cr_residual_read_inf_rd_en = Conn3_RD_EN;
  assign design_reset = mig_setup_reset_0_design_reset;
  assign design_reset_n = mig_setup_reset_0_design_reset_n;
  assign display_fifo_read_inf_1_RD_EN = display_fifo_read_inf_rd_en;
  assign display_fifo_read_inf_empty = display_fifo_read_inf_1_EMPTY;
  assign display_fifo_read_inf_rd_data[783:0] = display_fifo_read_inf_1_RD_DATA;
  assign fifo_in_1 = fifo_in[31:0];
  assign input_fifo_is_empty_1 = input_fifo_is_empty;
  assign pic_height_out[11:0] = hevc_top_wo_fifo_0_pic_height_out;
  assign pic_width_out[11:0] = hevc_top_wo_fifo_0_pic_width_out;
  assign read_en_out = hevc_top_wo_fifo_0_read_en_out;
  assign rst_processing_system7_0_125M_bus_struct_reset = sys_rst[0];
  assign sao_poc_out[31:0] = hevc_top_wo_fifo_0_sao_poc_out;
  assign ui_clk = mig_7series_0_ui_clk;
  assign ui_clk_sync_rst = mig_7series_0_ui_clk_sync_rst;
  assign y_rd_data_count_in_1 = y_rd_data_count_in[12:0];
  assign y_residual_interface_rd_en = Conn1_RD_EN;
GND GND
       (.G(GND_1));
VCC VCC
       (.P(VCC_1));
zynq_design_1_axi_inter_con_0_0 axi_inter_con_0
       (.INTERCONNECT_ACLK(mig_7series_0_ui_clk),
        .INTERCONNECT_ARESETN(mig_setup_reset_0_design_reset_n),
        .M00_AXI_ACLK(mig_7series_0_ui_clk),
        .M00_AXI_ARADDR(axi_inter_con_0_M00_AXI_ARADDR),
        .M00_AXI_ARBURST(axi_inter_con_0_M00_AXI_ARBURST),
        .M00_AXI_ARCACHE(axi_inter_con_0_M00_AXI_ARCACHE),
        .M00_AXI_ARID(axi_inter_con_0_M00_AXI_ARID),
        .M00_AXI_ARLEN(axi_inter_con_0_M00_AXI_ARLEN),
        .M00_AXI_ARLOCK(axi_inter_con_0_M00_AXI_ARLOCK),
        .M00_AXI_ARPROT(axi_inter_con_0_M00_AXI_ARPROT),
        .M00_AXI_ARQOS(axi_inter_con_0_M00_AXI_ARQOS),
        .M00_AXI_ARREADY(axi_inter_con_0_M00_AXI_ARREADY),
        .M00_AXI_ARSIZE(axi_inter_con_0_M00_AXI_ARSIZE),
        .M00_AXI_ARVALID(axi_inter_con_0_M00_AXI_ARVALID),
        .M00_AXI_AWADDR(axi_inter_con_0_M00_AXI_AWADDR),
        .M00_AXI_AWBURST(axi_inter_con_0_M00_AXI_AWBURST),
        .M00_AXI_AWCACHE(axi_inter_con_0_M00_AXI_AWCACHE),
        .M00_AXI_AWID(axi_inter_con_0_M00_AXI_AWID),
        .M00_AXI_AWLEN(axi_inter_con_0_M00_AXI_AWLEN),
        .M00_AXI_AWLOCK(axi_inter_con_0_M00_AXI_AWLOCK),
        .M00_AXI_AWPROT(axi_inter_con_0_M00_AXI_AWPROT),
        .M00_AXI_AWQOS(axi_inter_con_0_M00_AXI_AWQOS),
        .M00_AXI_AWREADY(axi_inter_con_0_M00_AXI_AWREADY),
        .M00_AXI_AWSIZE(axi_inter_con_0_M00_AXI_AWSIZE),
        .M00_AXI_AWVALID(axi_inter_con_0_M00_AXI_AWVALID),
        .M00_AXI_BID(axi_inter_con_0_M00_AXI_BID),
        .M00_AXI_BREADY(axi_inter_con_0_M00_AXI_BREADY),
        .M00_AXI_BRESP(axi_inter_con_0_M00_AXI_BRESP),
        .M00_AXI_BVALID(axi_inter_con_0_M00_AXI_BVALID),
        .M00_AXI_RDATA(axi_inter_con_0_M00_AXI_RDATA),
        .M00_AXI_RID(axi_inter_con_0_M00_AXI_RID),
        .M00_AXI_RLAST(axi_inter_con_0_M00_AXI_RLAST),
        .M00_AXI_RREADY(axi_inter_con_0_M00_AXI_RREADY),
        .M00_AXI_RRESP(axi_inter_con_0_M00_AXI_RRESP),
        .M00_AXI_RVALID(axi_inter_con_0_M00_AXI_RVALID),
        .M00_AXI_WDATA(axi_inter_con_0_M00_AXI_WDATA),
        .M00_AXI_WLAST(axi_inter_con_0_M00_AXI_WLAST),
        .M00_AXI_WREADY(axi_inter_con_0_M00_AXI_WREADY),
        .M00_AXI_WSTRB(axi_inter_con_0_M00_AXI_WSTRB),
        .M00_AXI_WVALID(axi_inter_con_0_M00_AXI_WVALID),
        .S00_AXI_ACLK(mig_7series_0_ui_clk),
        .S00_AXI_ARADDR(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARADDR),
        .S00_AXI_ARBURST(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARBURST),
        .S00_AXI_ARCACHE({GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_ARID(GND_1),
        .S00_AXI_ARLEN(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARLEN),
        .S00_AXI_ARLOCK(GND_1),
        .S00_AXI_ARPROT(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARPROT),
        .S00_AXI_ARQOS({GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_ARREADY(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARREADY),
        .S00_AXI_ARSIZE(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARSIZE),
        .S00_AXI_ARVALID(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARVALID),
        .S00_AXI_AWADDR({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_AWBURST({VCC_1,VCC_1}),
        .S00_AXI_AWCACHE({GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_AWID(GND_1),
        .S00_AXI_AWLEN({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_AWLOCK(GND_1),
        .S00_AXI_AWPROT({GND_1,GND_1,GND_1}),
        .S00_AXI_AWQOS({GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_AWSIZE({GND_1,GND_1,GND_1}),
        .S00_AXI_AWVALID(GND_1),
        .S00_AXI_BREADY(GND_1),
        .S00_AXI_RDATA(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_RDATA),
        .S00_AXI_RLAST(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_RLAST),
        .S00_AXI_RREADY(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_RREADY),
        .S00_AXI_RRESP(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_RRESP),
        .S00_AXI_RVALID(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_RVALID),
        .S00_AXI_WDATA({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_WLAST(GND_1),
        .S00_AXI_WSTRB({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_WVALID(GND_1),
        .S01_AXI_ACLK(mig_7series_0_ui_clk),
        .S01_AXI_ARADDR({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S01_AXI_ARBURST({VCC_1,VCC_1}),
        .S01_AXI_ARCACHE({GND_1,GND_1,GND_1,GND_1}),
        .S01_AXI_ARID(GND_1),
        .S01_AXI_ARLEN({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S01_AXI_ARLOCK(GND_1),
        .S01_AXI_ARPROT({GND_1,GND_1,GND_1}),
        .S01_AXI_ARQOS({GND_1,GND_1,GND_1,GND_1}),
        .S01_AXI_ARSIZE({GND_1,GND_1,GND_1}),
        .S01_AXI_ARVALID(GND_1),
        .S01_AXI_AWADDR(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWADDR),
        .S01_AXI_AWBURST(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWBURST),
        .S01_AXI_AWCACHE(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWCACHE),
        .S01_AXI_AWID(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWID),
        .S01_AXI_AWLEN(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWLEN),
        .S01_AXI_AWLOCK(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWLOCK),
        .S01_AXI_AWPROT(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWPROT),
        .S01_AXI_AWQOS({GND_1,GND_1,GND_1,GND_1}),
        .S01_AXI_AWREADY(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWREADY),
        .S01_AXI_AWSIZE(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWSIZE),
        .S01_AXI_AWVALID(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWVALID),
        .S01_AXI_BID(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_BID),
        .S01_AXI_BREADY(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_BREADY),
        .S01_AXI_BRESP(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_BRESP),
        .S01_AXI_BVALID(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_BVALID),
        .S01_AXI_RREADY(GND_1),
        .S01_AXI_WDATA(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_WDATA),
        .S01_AXI_WLAST(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_WLAST),
        .S01_AXI_WREADY(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_WREADY),
        .S01_AXI_WSTRB(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_WSTRB),
        .S01_AXI_WVALID(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_WVALID),
        .S02_AXI_ACLK(mig_7series_0_ui_clk),
        .S02_AXI_ARADDR(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARADDR),
        .S02_AXI_ARBURST(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARBURST),
        .S02_AXI_ARCACHE(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARCACHE),
        .S02_AXI_ARID(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARID),
        .S02_AXI_ARLEN(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARLEN),
        .S02_AXI_ARLOCK(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARLOCK),
        .S02_AXI_ARPROT(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARPROT),
        .S02_AXI_ARQOS({GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_ARREADY(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARREADY),
        .S02_AXI_ARSIZE(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARSIZE),
        .S02_AXI_ARVALID(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARVALID),
        .S02_AXI_AWADDR({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_AWBURST({VCC_1,VCC_1}),
        .S02_AXI_AWCACHE({GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_AWID(GND_1),
        .S02_AXI_AWLEN({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_AWLOCK(GND_1),
        .S02_AXI_AWPROT({GND_1,GND_1,GND_1}),
        .S02_AXI_AWQOS({GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_AWSIZE({GND_1,GND_1,GND_1}),
        .S02_AXI_AWVALID(GND_1),
        .S02_AXI_BREADY(GND_1),
        .S02_AXI_RDATA(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_RDATA),
        .S02_AXI_RLAST(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_RLAST),
        .S02_AXI_RREADY(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_RREADY),
        .S02_AXI_RRESP(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_RRESP),
        .S02_AXI_RVALID(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_RVALID),
        .S02_AXI_WDATA({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_WLAST(GND_1),
        .S02_AXI_WSTRB({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_WVALID(GND_1),
        .S03_AXI_ACLK(mig_7series_0_ui_clk),
        .S03_AXI_ARADDR({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S03_AXI_ARBURST({VCC_1,VCC_1}),
        .S03_AXI_ARCACHE({GND_1,GND_1,GND_1,GND_1}),
        .S03_AXI_ARID(GND_1),
        .S03_AXI_ARLEN({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S03_AXI_ARLOCK(GND_1),
        .S03_AXI_ARPROT({GND_1,GND_1,GND_1}),
        .S03_AXI_ARQOS({GND_1,GND_1,GND_1,GND_1}),
        .S03_AXI_ARSIZE({GND_1,GND_1,GND_1}),
        .S03_AXI_ARVALID(GND_1),
        .S03_AXI_AWADDR(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWADDR),
        .S03_AXI_AWBURST(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWBURST),
        .S03_AXI_AWCACHE(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWCACHE),
        .S03_AXI_AWID(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWID),
        .S03_AXI_AWLEN(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWLEN),
        .S03_AXI_AWLOCK(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWLOCK),
        .S03_AXI_AWPROT(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWPROT),
        .S03_AXI_AWQOS({GND_1,GND_1,GND_1,GND_1}),
        .S03_AXI_AWREADY(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWREADY),
        .S03_AXI_AWSIZE(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWSIZE),
        .S03_AXI_AWVALID(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWVALID),
        .S03_AXI_BREADY(hevc_top_wo_fifo_0_mv_col_axi_master_inf_BREADY),
        .S03_AXI_BRESP(hevc_top_wo_fifo_0_mv_col_axi_master_inf_BRESP),
        .S03_AXI_BVALID(hevc_top_wo_fifo_0_mv_col_axi_master_inf_BVALID),
        .S03_AXI_RREADY(GND_1),
        .S03_AXI_WDATA(hevc_top_wo_fifo_0_mv_col_axi_master_inf_WDATA),
        .S03_AXI_WLAST(hevc_top_wo_fifo_0_mv_col_axi_master_inf_WLAST),
        .S03_AXI_WREADY(hevc_top_wo_fifo_0_mv_col_axi_master_inf_WREADY),
        .S03_AXI_WSTRB(hevc_top_wo_fifo_0_mv_col_axi_master_inf_WSTRB),
        .S03_AXI_WVALID(hevc_top_wo_fifo_0_mv_col_axi_master_inf_WVALID));
zynq_design_1_hevc_top_wo_fifo_0_0 hevc_top_wo_fifo_0
       (.Xc_Yc_out(Xc_Yc_out),
        .cb_prog_empty_thresh_out(cb_prog_empty_thresh_out),
        .cb_rd_data_count_in(cb_rd_data_count_in_1),
        .cb_residual_fifo_in(Conn2_RD_DATA),
        .cb_residual_fifo_is_empty_in(Conn2_EMPTY),
        .cb_residual_fifo_read_en_out(Conn2_RD_EN),
        .clk(mig_7series_0_ui_clk),
        .cnf_fifo_counter(cnf_fifo_counter),
        .cnf_fifo_out(cnf_fifo_out),
        .comon_pre_lp_4x4_valid_out(comon_pre_lp_4x4_valid_out_1),
        .comon_pre_lp_luma_4x4_out(comon_pre_lp_luma_4x4_out_1),
        .comon_pre_lp_x_out(comon_pre_lp_x_out_1),
        .comon_pre_lp_y_out(comon_pre_lp_y_out_1),
        .cr_rd_data_count_in(cr_rd_data_count_in_1),
        .cr_residual_fifo_in(Conn3_RD_DATA),
        .cr_residual_fifo_is_empty_in(Conn3_EMPTY),
        .cr_residual_fifo_read_en_out(Conn3_RD_EN),
        .current_poc_out(current_poc_out),
        .dbf_to_sao_out_valid(dbf_to_sao_out_valid),
        .display_buf_ful(display_buf_ful),
        .display_fifo_data_out(display_fifo_read_inf_1_RD_DATA),
        .display_fifo_is_empty_out(display_fifo_read_inf_1_EMPTY),
        .display_fifo_rd_en_in(display_fifo_read_inf_1_RD_EN),
        .enable(xlconstant_0_dout),
        .fifo_in(fifo_in_1),
        .input_fifo_is_empty(input_fifo_is_empty_1),
        .inter_4x4_valid_out(inter_4x4_valid_out),
        .inter_luma_4x4_out(inter_luma_4x4_out_1),
        .inter_pred_stat_8b_out(inter_pred_stat_8b_out),
        .inter_x_out(inter_x_out),
        .inter_y_out(inter_y_out),
        .intra_4x4_valid_out(intra_4x4_valid_out_1),
        .intra_luma_4x4_out(intra_luma_4x4_out),
        .intra_ready_out(intra_ready_out),
        .intra_x_out(intra_x_out),
        .intra_y_out(intra_y_out),
        .mv_col_axi_awaddr(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWADDR),
        .mv_col_axi_awburst(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWBURST),
        .mv_col_axi_awcache(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWCACHE),
        .mv_col_axi_awid(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWID),
        .mv_col_axi_awlen(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWLEN),
        .mv_col_axi_awlock(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWLOCK),
        .mv_col_axi_awprot(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWPROT),
        .mv_col_axi_awready(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWREADY),
        .mv_col_axi_awsize(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWSIZE),
        .mv_col_axi_awvalid(hevc_top_wo_fifo_0_mv_col_axi_master_inf_AWVALID),
        .mv_col_axi_bready(hevc_top_wo_fifo_0_mv_col_axi_master_inf_BREADY),
        .mv_col_axi_bresp(hevc_top_wo_fifo_0_mv_col_axi_master_inf_BRESP),
        .mv_col_axi_bvalid(hevc_top_wo_fifo_0_mv_col_axi_master_inf_BVALID),
        .mv_col_axi_wdata(hevc_top_wo_fifo_0_mv_col_axi_master_inf_WDATA),
        .mv_col_axi_wlast(hevc_top_wo_fifo_0_mv_col_axi_master_inf_WLAST),
        .mv_col_axi_wready(hevc_top_wo_fifo_0_mv_col_axi_master_inf_WREADY),
        .mv_col_axi_wstrb(hevc_top_wo_fifo_0_mv_col_axi_master_inf_WSTRB),
        .mv_col_axi_wvalid(hevc_top_wo_fifo_0_mv_col_axi_master_inf_WVALID),
        .mv_pref_axi_araddr(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARADDR),
        .mv_pref_axi_arburst(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARBURST),
        .mv_pref_axi_arcache(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARCACHE),
        .mv_pref_axi_arid(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARID),
        .mv_pref_axi_arlen(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARLEN),
        .mv_pref_axi_arlock(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARLOCK),
        .mv_pref_axi_arprot(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARPROT),
        .mv_pref_axi_arready(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARREADY),
        .mv_pref_axi_arsize(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARSIZE),
        .mv_pref_axi_arvalid(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_ARVALID),
        .mv_pref_axi_rdata(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_RDATA),
        .mv_pref_axi_rlast(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_RLAST),
        .mv_pref_axi_rready(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_RREADY),
        .mv_pref_axi_rresp(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_RRESP),
        .mv_pref_axi_rvalid(hevc_top_wo_fifo_0_mv_pref_axi_master_inf_RVALID),
        .mv_state_8bit_out(mv_state_8bit_out),
        .pic_height_out(hevc_top_wo_fifo_0_pic_height_out),
        .pic_width_out(hevc_top_wo_fifo_0_pic_width_out),
        .pred_state_low8b_out(pred_state_low8b_out),
        .read_en_out(hevc_top_wo_fifo_0_read_en_out),
        .ref_pic_write_axi_awaddr(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWADDR),
        .ref_pic_write_axi_awburst(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWBURST),
        .ref_pic_write_axi_awcache(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWCACHE),
        .ref_pic_write_axi_awid(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWID),
        .ref_pic_write_axi_awlen(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWLEN),
        .ref_pic_write_axi_awlock(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWLOCK),
        .ref_pic_write_axi_awprot(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWPROT),
        .ref_pic_write_axi_awready(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWREADY),
        .ref_pic_write_axi_awsize(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWSIZE),
        .ref_pic_write_axi_awvalid(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_AWVALID),
        .ref_pic_write_axi_bid(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_BID),
        .ref_pic_write_axi_bready(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_BREADY),
        .ref_pic_write_axi_bresp(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_BRESP),
        .ref_pic_write_axi_bvalid(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_BVALID),
        .ref_pic_write_axi_wdata(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_WDATA),
        .ref_pic_write_axi_wlast(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_WLAST),
        .ref_pic_write_axi_wready(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_WREADY),
        .ref_pic_write_axi_wstrb(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_WSTRB),
        .ref_pic_write_axi_wvalid(hevc_top_wo_fifo_0_ref_pic_write_axi_master_inf_WVALID),
        .ref_pix_axi_ar_addr(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARADDR),
        .ref_pix_axi_ar_burst(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARBURST),
        .ref_pix_axi_ar_len(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARLEN),
        .ref_pix_axi_ar_prot(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARPROT),
        .ref_pix_axi_ar_ready(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARREADY),
        .ref_pix_axi_ar_size(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARSIZE),
        .ref_pix_axi_ar_valid(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_ARVALID),
        .ref_pix_axi_r_data(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_RDATA),
        .ref_pix_axi_r_last(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_RLAST),
        .ref_pix_axi_r_ready(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_RREADY),
        .ref_pix_axi_r_resp(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_RRESP),
        .ref_pix_axi_r_valid(hevc_top_wo_fifo_0_ref_pix_axi_mastr_inf_RVALID),
        .reset(mig_setup_reset_0_design_reset),
        .residual_read_inf(residual_read_inf),
        .sao_poc_out(hevc_top_wo_fifo_0_sao_poc_out),
        .y_rd_data_count_in(y_rd_data_count_in_1),
        .y_residual_counter(y_residual_counter),
        .y_residual_fifo_in(Conn1_RD_DATA),
        .y_residual_fifo_is_empty_in(Conn1_EMPTY),
        .y_residual_fifo_is_empty_out(y_residual_fifo_is_empty_out),
        .y_residual_fifo_read_en_out(Conn1_RD_EN),
        .yy_prog_empty_thresh_out(yy_prog_empty_thresh_out));
zynq_design_1_mig_7series_0_0 mig_7series_0
       (.aresetn(mig_setup_reset_0_areset_n),
        .ddr3_addr(mig_7series_0_DDR3_ADDR),
        .ddr3_ba(mig_7series_0_DDR3_BA),
        .ddr3_cas_n(mig_7series_0_DDR3_CAS_N),
        .ddr3_ck_n(mig_7series_0_DDR3_CK_N),
        .ddr3_ck_p(mig_7series_0_DDR3_CK_P),
        .ddr3_cke(mig_7series_0_DDR3_CKE),
        .ddr3_cs_n(mig_7series_0_DDR3_CS_N),
        .ddr3_dm(mig_7series_0_DDR3_DM),
        .ddr3_dq(DDR3_dq[63:0]),
        .ddr3_dqs_n(DDR3_dqs_n[7:0]),
        .ddr3_dqs_p(DDR3_dqs_p[7:0]),
        .ddr3_odt(mig_7series_0_DDR3_ODT),
        .ddr3_ras_n(mig_7series_0_DDR3_RAS_N),
        .ddr3_reset_n(mig_7series_0_DDR3_RESET_N),
        .ddr3_we_n(mig_7series_0_DDR3_WE_N),
        .init_calib_complete(mig_7series_0_init_calib_complete),
        .mmcm_locked(mig_7series_0_mmcm_locked),
        .s_axi_araddr(axi_inter_con_0_M00_AXI_ARADDR[29:0]),
        .s_axi_arburst(axi_inter_con_0_M00_AXI_ARBURST),
        .s_axi_arcache(axi_inter_con_0_M00_AXI_ARCACHE),
        .s_axi_arid(axi_inter_con_0_M00_AXI_ARID),
        .s_axi_arlen(axi_inter_con_0_M00_AXI_ARLEN),
        .s_axi_arlock(axi_inter_con_0_M00_AXI_ARLOCK),
        .s_axi_arprot(axi_inter_con_0_M00_AXI_ARPROT),
        .s_axi_arqos(axi_inter_con_0_M00_AXI_ARQOS),
        .s_axi_arready(axi_inter_con_0_M00_AXI_ARREADY),
        .s_axi_arsize(axi_inter_con_0_M00_AXI_ARSIZE),
        .s_axi_arvalid(axi_inter_con_0_M00_AXI_ARVALID),
        .s_axi_awaddr(axi_inter_con_0_M00_AXI_AWADDR[29:0]),
        .s_axi_awburst(axi_inter_con_0_M00_AXI_AWBURST),
        .s_axi_awcache(axi_inter_con_0_M00_AXI_AWCACHE),
        .s_axi_awid(axi_inter_con_0_M00_AXI_AWID),
        .s_axi_awlen(axi_inter_con_0_M00_AXI_AWLEN),
        .s_axi_awlock(axi_inter_con_0_M00_AXI_AWLOCK),
        .s_axi_awprot(axi_inter_con_0_M00_AXI_AWPROT),
        .s_axi_awqos(axi_inter_con_0_M00_AXI_AWQOS),
        .s_axi_awready(axi_inter_con_0_M00_AXI_AWREADY),
        .s_axi_awsize(axi_inter_con_0_M00_AXI_AWSIZE),
        .s_axi_awvalid(axi_inter_con_0_M00_AXI_AWVALID),
        .s_axi_bid(axi_inter_con_0_M00_AXI_BID),
        .s_axi_bready(axi_inter_con_0_M00_AXI_BREADY),
        .s_axi_bresp(axi_inter_con_0_M00_AXI_BRESP),
        .s_axi_bvalid(axi_inter_con_0_M00_AXI_BVALID),
        .s_axi_rdata(axi_inter_con_0_M00_AXI_RDATA),
        .s_axi_rid(axi_inter_con_0_M00_AXI_RID),
        .s_axi_rlast(axi_inter_con_0_M00_AXI_RLAST),
        .s_axi_rready(axi_inter_con_0_M00_AXI_RREADY),
        .s_axi_rresp(axi_inter_con_0_M00_AXI_RRESP),
        .s_axi_rvalid(axi_inter_con_0_M00_AXI_RVALID),
        .s_axi_wdata(axi_inter_con_0_M00_AXI_WDATA),
        .s_axi_wlast(axi_inter_con_0_M00_AXI_WLAST),
        .s_axi_wready(axi_inter_con_0_M00_AXI_WREADY),
        .s_axi_wstrb(axi_inter_con_0_M00_AXI_WSTRB),
        .s_axi_wvalid(axi_inter_con_0_M00_AXI_WVALID),
        .sys_clk_n(SYS_CLK_1_CLK_N),
        .sys_clk_p(SYS_CLK_1_CLK_P),
        .sys_rst(rst_processing_system7_0_125M_bus_struct_reset),
        .ui_clk(mig_7series_0_ui_clk),
        .ui_clk_sync_rst(mig_7series_0_ui_clk_sync_rst));
zynq_design_1_mig_setup_reset_0_0 mig_setup_reset_0
       (.areset_n(mig_setup_reset_0_areset_n),
        .clk(mig_7series_0_ui_clk),
        .design_reset(mig_setup_reset_0_design_reset),
        .design_reset_n(mig_setup_reset_0_design_reset_n),
        .init_calib_complete(mig_7series_0_init_calib_complete),
        .mmcm_locked(mig_7series_0_mmcm_locked),
        .rst(mig_7series_0_ui_clk_sync_rst));
zynq_design_1_xlconstant_0_0 xlconstant_0
       (.dout(xlconstant_0_dout));
endmodule

module dummy_pl_ps_interconnect_imp_K7QGHO
   (ACLK,
    ARESETN,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arburst,
    S00_AXI_arcache,
    S00_AXI_arid,
    S00_AXI_arlen,
    S00_AXI_arlock,
    S00_AXI_arprot,
    S00_AXI_arqos,
    S00_AXI_arready,
    S00_AXI_arsize,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awburst,
    S00_AXI_awcache,
    S00_AXI_awid,
    S00_AXI_awlen,
    S00_AXI_awlock,
    S00_AXI_awprot,
    S00_AXI_awqos,
    S00_AXI_awready,
    S00_AXI_awsize,
    S00_AXI_awvalid,
    S00_AXI_bid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rid,
    S00_AXI_rlast,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wid,
    S00_AXI_wlast,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid);
  input ACLK;
  input [0:0]ARESETN;
  input [0:0]S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [1:0]S00_AXI_arburst;
  input [3:0]S00_AXI_arcache;
  input [11:0]S00_AXI_arid;
  input [3:0]S00_AXI_arlen;
  input [1:0]S00_AXI_arlock;
  input [2:0]S00_AXI_arprot;
  input [3:0]S00_AXI_arqos;
  output S00_AXI_arready;
  input [2:0]S00_AXI_arsize;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [1:0]S00_AXI_awburst;
  input [3:0]S00_AXI_awcache;
  input [11:0]S00_AXI_awid;
  input [3:0]S00_AXI_awlen;
  input [1:0]S00_AXI_awlock;
  input [2:0]S00_AXI_awprot;
  input [3:0]S00_AXI_awqos;
  output S00_AXI_awready;
  input [2:0]S00_AXI_awsize;
  input S00_AXI_awvalid;
  output [11:0]S00_AXI_bid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  output [11:0]S00_AXI_rid;
  output S00_AXI_rlast;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  input [11:0]S00_AXI_wid;
  input S00_AXI_wlast;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;

  wire [15:0]axi_bram_ctrl_0_BRAM_PORTA_ADDR;
  wire axi_bram_ctrl_0_BRAM_PORTA_CLK;
  wire [31:0]axi_bram_ctrl_0_BRAM_PORTA_DIN;
  wire [31:0]axi_bram_ctrl_0_BRAM_PORTA_DOUT;
  wire axi_bram_ctrl_0_BRAM_PORTA_EN;
  wire axi_bram_ctrl_0_BRAM_PORTA_RST;
  wire [3:0]axi_bram_ctrl_0_BRAM_PORTA_WE;
  wire [15:0]axi_bram_ctrl_0_BRAM_PORTB_ADDR;
  wire axi_bram_ctrl_0_BRAM_PORTB_CLK;
  wire [31:0]axi_bram_ctrl_0_BRAM_PORTB_DIN;
  wire [31:0]axi_bram_ctrl_0_BRAM_PORTB_DOUT;
  wire axi_bram_ctrl_0_BRAM_PORTB_EN;
  wire axi_bram_ctrl_0_BRAM_PORTB_RST;
  wire [3:0]axi_bram_ctrl_0_BRAM_PORTB_WE;
  wire [15:0]axi_mem_intercon_M00_AXI_ARADDR;
  wire [1:0]axi_mem_intercon_M00_AXI_ARBURST;
  wire [3:0]axi_mem_intercon_M00_AXI_ARCACHE;
  wire [11:0]axi_mem_intercon_M00_AXI_ARID;
  wire [7:0]axi_mem_intercon_M00_AXI_ARLEN;
  wire [0:0]axi_mem_intercon_M00_AXI_ARLOCK;
  wire [2:0]axi_mem_intercon_M00_AXI_ARPROT;
  wire axi_mem_intercon_M00_AXI_ARREADY;
  wire [2:0]axi_mem_intercon_M00_AXI_ARSIZE;
  wire axi_mem_intercon_M00_AXI_ARVALID;
  wire [15:0]axi_mem_intercon_M00_AXI_AWADDR;
  wire [1:0]axi_mem_intercon_M00_AXI_AWBURST;
  wire [3:0]axi_mem_intercon_M00_AXI_AWCACHE;
  wire [11:0]axi_mem_intercon_M00_AXI_AWID;
  wire [7:0]axi_mem_intercon_M00_AXI_AWLEN;
  wire [0:0]axi_mem_intercon_M00_AXI_AWLOCK;
  wire [2:0]axi_mem_intercon_M00_AXI_AWPROT;
  wire axi_mem_intercon_M00_AXI_AWREADY;
  wire [2:0]axi_mem_intercon_M00_AXI_AWSIZE;
  wire axi_mem_intercon_M00_AXI_AWVALID;
  wire [11:0]axi_mem_intercon_M00_AXI_BID;
  wire axi_mem_intercon_M00_AXI_BREADY;
  wire [1:0]axi_mem_intercon_M00_AXI_BRESP;
  wire axi_mem_intercon_M00_AXI_BVALID;
  wire [31:0]axi_mem_intercon_M00_AXI_RDATA;
  wire [11:0]axi_mem_intercon_M00_AXI_RID;
  wire axi_mem_intercon_M00_AXI_RLAST;
  wire axi_mem_intercon_M00_AXI_RREADY;
  wire [1:0]axi_mem_intercon_M00_AXI_RRESP;
  wire axi_mem_intercon_M00_AXI_RVALID;
  wire [31:0]axi_mem_intercon_M00_AXI_WDATA;
  wire axi_mem_intercon_M00_AXI_WLAST;
  wire axi_mem_intercon_M00_AXI_WREADY;
  wire [3:0]axi_mem_intercon_M00_AXI_WSTRB;
  wire axi_mem_intercon_M00_AXI_WVALID;
  wire processing_system7_0_FCLK_CLK0;
  wire [31:0]processing_system7_0_M_AXI_GP0_ARADDR;
  wire [1:0]processing_system7_0_M_AXI_GP0_ARBURST;
  wire [3:0]processing_system7_0_M_AXI_GP0_ARCACHE;
  wire [11:0]processing_system7_0_M_AXI_GP0_ARID;
  wire [3:0]processing_system7_0_M_AXI_GP0_ARLEN;
  wire [1:0]processing_system7_0_M_AXI_GP0_ARLOCK;
  wire [2:0]processing_system7_0_M_AXI_GP0_ARPROT;
  wire [3:0]processing_system7_0_M_AXI_GP0_ARQOS;
  wire processing_system7_0_M_AXI_GP0_ARREADY;
  wire [2:0]processing_system7_0_M_AXI_GP0_ARSIZE;
  wire processing_system7_0_M_AXI_GP0_ARVALID;
  wire [31:0]processing_system7_0_M_AXI_GP0_AWADDR;
  wire [1:0]processing_system7_0_M_AXI_GP0_AWBURST;
  wire [3:0]processing_system7_0_M_AXI_GP0_AWCACHE;
  wire [11:0]processing_system7_0_M_AXI_GP0_AWID;
  wire [3:0]processing_system7_0_M_AXI_GP0_AWLEN;
  wire [1:0]processing_system7_0_M_AXI_GP0_AWLOCK;
  wire [2:0]processing_system7_0_M_AXI_GP0_AWPROT;
  wire [3:0]processing_system7_0_M_AXI_GP0_AWQOS;
  wire processing_system7_0_M_AXI_GP0_AWREADY;
  wire [2:0]processing_system7_0_M_AXI_GP0_AWSIZE;
  wire processing_system7_0_M_AXI_GP0_AWVALID;
  wire [11:0]processing_system7_0_M_AXI_GP0_BID;
  wire processing_system7_0_M_AXI_GP0_BREADY;
  wire [1:0]processing_system7_0_M_AXI_GP0_BRESP;
  wire processing_system7_0_M_AXI_GP0_BVALID;
  wire [31:0]processing_system7_0_M_AXI_GP0_RDATA;
  wire [11:0]processing_system7_0_M_AXI_GP0_RID;
  wire processing_system7_0_M_AXI_GP0_RLAST;
  wire processing_system7_0_M_AXI_GP0_RREADY;
  wire [1:0]processing_system7_0_M_AXI_GP0_RRESP;
  wire processing_system7_0_M_AXI_GP0_RVALID;
  wire [31:0]processing_system7_0_M_AXI_GP0_WDATA;
  wire [11:0]processing_system7_0_M_AXI_GP0_WID;
  wire processing_system7_0_M_AXI_GP0_WLAST;
  wire processing_system7_0_M_AXI_GP0_WREADY;
  wire [3:0]processing_system7_0_M_AXI_GP0_WSTRB;
  wire processing_system7_0_M_AXI_GP0_WVALID;
  wire [0:0]rst_processing_system7_0_125M_interconnect_aresetn;
  wire [0:0]rst_processing_system7_0_125M_peripheral_aresetn;

  assign S00_AXI_arready = processing_system7_0_M_AXI_GP0_ARREADY;
  assign S00_AXI_awready = processing_system7_0_M_AXI_GP0_AWREADY;
  assign S00_AXI_bid[11:0] = processing_system7_0_M_AXI_GP0_BID;
  assign S00_AXI_bresp[1:0] = processing_system7_0_M_AXI_GP0_BRESP;
  assign S00_AXI_bvalid = processing_system7_0_M_AXI_GP0_BVALID;
  assign S00_AXI_rdata[31:0] = processing_system7_0_M_AXI_GP0_RDATA;
  assign S00_AXI_rid[11:0] = processing_system7_0_M_AXI_GP0_RID;
  assign S00_AXI_rlast = processing_system7_0_M_AXI_GP0_RLAST;
  assign S00_AXI_rresp[1:0] = processing_system7_0_M_AXI_GP0_RRESP;
  assign S00_AXI_rvalid = processing_system7_0_M_AXI_GP0_RVALID;
  assign S00_AXI_wready = processing_system7_0_M_AXI_GP0_WREADY;
  assign processing_system7_0_FCLK_CLK0 = ACLK;
  assign processing_system7_0_M_AXI_GP0_ARADDR = S00_AXI_araddr[31:0];
  assign processing_system7_0_M_AXI_GP0_ARBURST = S00_AXI_arburst[1:0];
  assign processing_system7_0_M_AXI_GP0_ARCACHE = S00_AXI_arcache[3:0];
  assign processing_system7_0_M_AXI_GP0_ARID = S00_AXI_arid[11:0];
  assign processing_system7_0_M_AXI_GP0_ARLEN = S00_AXI_arlen[3:0];
  assign processing_system7_0_M_AXI_GP0_ARLOCK = S00_AXI_arlock[1:0];
  assign processing_system7_0_M_AXI_GP0_ARPROT = S00_AXI_arprot[2:0];
  assign processing_system7_0_M_AXI_GP0_ARQOS = S00_AXI_arqos[3:0];
  assign processing_system7_0_M_AXI_GP0_ARSIZE = S00_AXI_arsize[2:0];
  assign processing_system7_0_M_AXI_GP0_ARVALID = S00_AXI_arvalid;
  assign processing_system7_0_M_AXI_GP0_AWADDR = S00_AXI_awaddr[31:0];
  assign processing_system7_0_M_AXI_GP0_AWBURST = S00_AXI_awburst[1:0];
  assign processing_system7_0_M_AXI_GP0_AWCACHE = S00_AXI_awcache[3:0];
  assign processing_system7_0_M_AXI_GP0_AWID = S00_AXI_awid[11:0];
  assign processing_system7_0_M_AXI_GP0_AWLEN = S00_AXI_awlen[3:0];
  assign processing_system7_0_M_AXI_GP0_AWLOCK = S00_AXI_awlock[1:0];
  assign processing_system7_0_M_AXI_GP0_AWPROT = S00_AXI_awprot[2:0];
  assign processing_system7_0_M_AXI_GP0_AWQOS = S00_AXI_awqos[3:0];
  assign processing_system7_0_M_AXI_GP0_AWSIZE = S00_AXI_awsize[2:0];
  assign processing_system7_0_M_AXI_GP0_AWVALID = S00_AXI_awvalid;
  assign processing_system7_0_M_AXI_GP0_BREADY = S00_AXI_bready;
  assign processing_system7_0_M_AXI_GP0_RREADY = S00_AXI_rready;
  assign processing_system7_0_M_AXI_GP0_WDATA = S00_AXI_wdata[31:0];
  assign processing_system7_0_M_AXI_GP0_WID = S00_AXI_wid[11:0];
  assign processing_system7_0_M_AXI_GP0_WLAST = S00_AXI_wlast;
  assign processing_system7_0_M_AXI_GP0_WSTRB = S00_AXI_wstrb[3:0];
  assign processing_system7_0_M_AXI_GP0_WVALID = S00_AXI_wvalid;
  assign rst_processing_system7_0_125M_interconnect_aresetn = ARESETN[0];
  assign rst_processing_system7_0_125M_peripheral_aresetn = S00_ARESETN[0];
zynq_design_1_axi_bram_ctrl_0_bram_0 axi_bram_ctrl_0_bram
       (.addra(axi_bram_ctrl_0_BRAM_PORTA_ADDR),
        .addrb(axi_bram_ctrl_0_BRAM_PORTB_ADDR),
        .clka(axi_bram_ctrl_0_BRAM_PORTA_CLK),
        .clkb(axi_bram_ctrl_0_BRAM_PORTB_CLK),
        .dina(axi_bram_ctrl_0_BRAM_PORTA_DIN),
        .dinb(axi_bram_ctrl_0_BRAM_PORTB_DIN),
        .douta(axi_bram_ctrl_0_BRAM_PORTA_DOUT),
        .doutb(axi_bram_ctrl_0_BRAM_PORTB_DOUT),
        .ena(axi_bram_ctrl_0_BRAM_PORTA_EN),
        .enb(axi_bram_ctrl_0_BRAM_PORTB_EN),
        .rsta(axi_bram_ctrl_0_BRAM_PORTA_RST),
        .rstb(axi_bram_ctrl_0_BRAM_PORTB_RST),
        .wea(axi_bram_ctrl_0_BRAM_PORTA_WE),
        .web(axi_bram_ctrl_0_BRAM_PORTB_WE));
zynq_design_1_axi_mem_intercon_0 axi_mem_intercon
       (.ACLK(processing_system7_0_FCLK_CLK0),
        .ARESETN(rst_processing_system7_0_125M_interconnect_aresetn),
        .M00_ACLK(processing_system7_0_FCLK_CLK0),
        .M00_ARESETN(rst_processing_system7_0_125M_peripheral_aresetn),
        .M00_AXI_araddr(axi_mem_intercon_M00_AXI_ARADDR),
        .M00_AXI_arburst(axi_mem_intercon_M00_AXI_ARBURST),
        .M00_AXI_arcache(axi_mem_intercon_M00_AXI_ARCACHE),
        .M00_AXI_arid(axi_mem_intercon_M00_AXI_ARID),
        .M00_AXI_arlen(axi_mem_intercon_M00_AXI_ARLEN),
        .M00_AXI_arlock(axi_mem_intercon_M00_AXI_ARLOCK),
        .M00_AXI_arprot(axi_mem_intercon_M00_AXI_ARPROT),
        .M00_AXI_arready(axi_mem_intercon_M00_AXI_ARREADY),
        .M00_AXI_arsize(axi_mem_intercon_M00_AXI_ARSIZE),
        .M00_AXI_arvalid(axi_mem_intercon_M00_AXI_ARVALID),
        .M00_AXI_awaddr(axi_mem_intercon_M00_AXI_AWADDR),
        .M00_AXI_awburst(axi_mem_intercon_M00_AXI_AWBURST),
        .M00_AXI_awcache(axi_mem_intercon_M00_AXI_AWCACHE),
        .M00_AXI_awid(axi_mem_intercon_M00_AXI_AWID),
        .M00_AXI_awlen(axi_mem_intercon_M00_AXI_AWLEN),
        .M00_AXI_awlock(axi_mem_intercon_M00_AXI_AWLOCK),
        .M00_AXI_awprot(axi_mem_intercon_M00_AXI_AWPROT),
        .M00_AXI_awready(axi_mem_intercon_M00_AXI_AWREADY),
        .M00_AXI_awsize(axi_mem_intercon_M00_AXI_AWSIZE),
        .M00_AXI_awvalid(axi_mem_intercon_M00_AXI_AWVALID),
        .M00_AXI_bid(axi_mem_intercon_M00_AXI_BID),
        .M00_AXI_bready(axi_mem_intercon_M00_AXI_BREADY),
        .M00_AXI_bresp(axi_mem_intercon_M00_AXI_BRESP),
        .M00_AXI_bvalid(axi_mem_intercon_M00_AXI_BVALID),
        .M00_AXI_rdata(axi_mem_intercon_M00_AXI_RDATA),
        .M00_AXI_rid(axi_mem_intercon_M00_AXI_RID),
        .M00_AXI_rlast(axi_mem_intercon_M00_AXI_RLAST),
        .M00_AXI_rready(axi_mem_intercon_M00_AXI_RREADY),
        .M00_AXI_rresp(axi_mem_intercon_M00_AXI_RRESP),
        .M00_AXI_rvalid(axi_mem_intercon_M00_AXI_RVALID),
        .M00_AXI_wdata(axi_mem_intercon_M00_AXI_WDATA),
        .M00_AXI_wlast(axi_mem_intercon_M00_AXI_WLAST),
        .M00_AXI_wready(axi_mem_intercon_M00_AXI_WREADY),
        .M00_AXI_wstrb(axi_mem_intercon_M00_AXI_WSTRB),
        .M00_AXI_wvalid(axi_mem_intercon_M00_AXI_WVALID),
        .S00_ACLK(processing_system7_0_FCLK_CLK0),
        .S00_ARESETN(rst_processing_system7_0_125M_peripheral_aresetn),
        .S00_AXI_araddr(processing_system7_0_M_AXI_GP0_ARADDR),
        .S00_AXI_arburst(processing_system7_0_M_AXI_GP0_ARBURST),
        .S00_AXI_arcache(processing_system7_0_M_AXI_GP0_ARCACHE),
        .S00_AXI_arid(processing_system7_0_M_AXI_GP0_ARID),
        .S00_AXI_arlen(processing_system7_0_M_AXI_GP0_ARLEN),
        .S00_AXI_arlock(processing_system7_0_M_AXI_GP0_ARLOCK),
        .S00_AXI_arprot(processing_system7_0_M_AXI_GP0_ARPROT),
        .S00_AXI_arqos(processing_system7_0_M_AXI_GP0_ARQOS),
        .S00_AXI_arready(processing_system7_0_M_AXI_GP0_ARREADY),
        .S00_AXI_arsize(processing_system7_0_M_AXI_GP0_ARSIZE),
        .S00_AXI_arvalid(processing_system7_0_M_AXI_GP0_ARVALID),
        .S00_AXI_awaddr(processing_system7_0_M_AXI_GP0_AWADDR),
        .S00_AXI_awburst(processing_system7_0_M_AXI_GP0_AWBURST),
        .S00_AXI_awcache(processing_system7_0_M_AXI_GP0_AWCACHE),
        .S00_AXI_awid(processing_system7_0_M_AXI_GP0_AWID),
        .S00_AXI_awlen(processing_system7_0_M_AXI_GP0_AWLEN),
        .S00_AXI_awlock(processing_system7_0_M_AXI_GP0_AWLOCK),
        .S00_AXI_awprot(processing_system7_0_M_AXI_GP0_AWPROT),
        .S00_AXI_awqos(processing_system7_0_M_AXI_GP0_AWQOS),
        .S00_AXI_awready(processing_system7_0_M_AXI_GP0_AWREADY),
        .S00_AXI_awsize(processing_system7_0_M_AXI_GP0_AWSIZE),
        .S00_AXI_awvalid(processing_system7_0_M_AXI_GP0_AWVALID),
        .S00_AXI_bid(processing_system7_0_M_AXI_GP0_BID),
        .S00_AXI_bready(processing_system7_0_M_AXI_GP0_BREADY),
        .S00_AXI_bresp(processing_system7_0_M_AXI_GP0_BRESP),
        .S00_AXI_bvalid(processing_system7_0_M_AXI_GP0_BVALID),
        .S00_AXI_rdata(processing_system7_0_M_AXI_GP0_RDATA),
        .S00_AXI_rid(processing_system7_0_M_AXI_GP0_RID),
        .S00_AXI_rlast(processing_system7_0_M_AXI_GP0_RLAST),
        .S00_AXI_rready(processing_system7_0_M_AXI_GP0_RREADY),
        .S00_AXI_rresp(processing_system7_0_M_AXI_GP0_RRESP),
        .S00_AXI_rvalid(processing_system7_0_M_AXI_GP0_RVALID),
        .S00_AXI_wdata(processing_system7_0_M_AXI_GP0_WDATA),
        .S00_AXI_wid(processing_system7_0_M_AXI_GP0_WID),
        .S00_AXI_wlast(processing_system7_0_M_AXI_GP0_WLAST),
        .S00_AXI_wready(processing_system7_0_M_AXI_GP0_WREADY),
        .S00_AXI_wstrb(processing_system7_0_M_AXI_GP0_WSTRB),
        .S00_AXI_wvalid(processing_system7_0_M_AXI_GP0_WVALID));
(* BMM_INFO_ADDRESS_SPACE = "byte  0x40000000 32 >  zynq_design_1 dummy_pl_ps_interconnect/axi_bram_ctrl_0_bram" *) 
   (* KEEP_HIERARCHY = "yes" *) 
   zynq_design_1_axi_bram_ctrl_0_0 buffer_descriptor_memory
       (.bram_addr_a(axi_bram_ctrl_0_BRAM_PORTA_ADDR),
        .bram_addr_b(axi_bram_ctrl_0_BRAM_PORTB_ADDR),
        .bram_clk_a(axi_bram_ctrl_0_BRAM_PORTA_CLK),
        .bram_clk_b(axi_bram_ctrl_0_BRAM_PORTB_CLK),
        .bram_en_a(axi_bram_ctrl_0_BRAM_PORTA_EN),
        .bram_en_b(axi_bram_ctrl_0_BRAM_PORTB_EN),
        .bram_rddata_a(axi_bram_ctrl_0_BRAM_PORTA_DOUT),
        .bram_rddata_b(axi_bram_ctrl_0_BRAM_PORTB_DOUT),
        .bram_rst_a(axi_bram_ctrl_0_BRAM_PORTA_RST),
        .bram_rst_b(axi_bram_ctrl_0_BRAM_PORTB_RST),
        .bram_we_a(axi_bram_ctrl_0_BRAM_PORTA_WE),
        .bram_we_b(axi_bram_ctrl_0_BRAM_PORTB_WE),
        .bram_wrdata_a(axi_bram_ctrl_0_BRAM_PORTA_DIN),
        .bram_wrdata_b(axi_bram_ctrl_0_BRAM_PORTB_DIN),
        .s_axi_aclk(processing_system7_0_FCLK_CLK0),
        .s_axi_araddr(axi_mem_intercon_M00_AXI_ARADDR),
        .s_axi_arburst(axi_mem_intercon_M00_AXI_ARBURST),
        .s_axi_arcache(axi_mem_intercon_M00_AXI_ARCACHE),
        .s_axi_aresetn(rst_processing_system7_0_125M_peripheral_aresetn),
        .s_axi_arid(axi_mem_intercon_M00_AXI_ARID),
        .s_axi_arlen(axi_mem_intercon_M00_AXI_ARLEN),
        .s_axi_arlock(axi_mem_intercon_M00_AXI_ARLOCK),
        .s_axi_arprot(axi_mem_intercon_M00_AXI_ARPROT),
        .s_axi_arready(axi_mem_intercon_M00_AXI_ARREADY),
        .s_axi_arsize(axi_mem_intercon_M00_AXI_ARSIZE),
        .s_axi_arvalid(axi_mem_intercon_M00_AXI_ARVALID),
        .s_axi_awaddr(axi_mem_intercon_M00_AXI_AWADDR),
        .s_axi_awburst(axi_mem_intercon_M00_AXI_AWBURST),
        .s_axi_awcache(axi_mem_intercon_M00_AXI_AWCACHE),
        .s_axi_awid(axi_mem_intercon_M00_AXI_AWID),
        .s_axi_awlen(axi_mem_intercon_M00_AXI_AWLEN),
        .s_axi_awlock(axi_mem_intercon_M00_AXI_AWLOCK),
        .s_axi_awprot(axi_mem_intercon_M00_AXI_AWPROT),
        .s_axi_awready(axi_mem_intercon_M00_AXI_AWREADY),
        .s_axi_awsize(axi_mem_intercon_M00_AXI_AWSIZE),
        .s_axi_awvalid(axi_mem_intercon_M00_AXI_AWVALID),
        .s_axi_bid(axi_mem_intercon_M00_AXI_BID),
        .s_axi_bready(axi_mem_intercon_M00_AXI_BREADY),
        .s_axi_bresp(axi_mem_intercon_M00_AXI_BRESP),
        .s_axi_bvalid(axi_mem_intercon_M00_AXI_BVALID),
        .s_axi_rdata(axi_mem_intercon_M00_AXI_RDATA),
        .s_axi_rid(axi_mem_intercon_M00_AXI_RID),
        .s_axi_rlast(axi_mem_intercon_M00_AXI_RLAST),
        .s_axi_rready(axi_mem_intercon_M00_AXI_RREADY),
        .s_axi_rresp(axi_mem_intercon_M00_AXI_RRESP),
        .s_axi_rvalid(axi_mem_intercon_M00_AXI_RVALID),
        .s_axi_wdata(axi_mem_intercon_M00_AXI_WDATA),
        .s_axi_wlast(axi_mem_intercon_M00_AXI_WLAST),
        .s_axi_wready(axi_mem_intercon_M00_AXI_WREADY),
        .s_axi_wstrb(axi_mem_intercon_M00_AXI_WSTRB),
        .s_axi_wvalid(axi_mem_intercon_M00_AXI_WVALID));
endmodule

module s00_couplers_imp_17357AK
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arid,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awid,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rid,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wid,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [15:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [11:0]M_AXI_arid;
  output [7:0]M_AXI_arlen;
  output M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  output [15:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [11:0]M_AXI_awid;
  output [7:0]M_AXI_awlen;
  output M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  input [11:0]M_AXI_bid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  input [11:0]M_AXI_rid;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [11:0]S_AXI_arid;
  input [3:0]S_AXI_arlen;
  input [1:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [11:0]S_AXI_awid;
  input [3:0]S_AXI_awlen;
  input [1:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [11:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [11:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input [11:0]S_AXI_wid;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_pc_to_s00_couplers_ARADDR;
  wire [1:0]auto_pc_to_s00_couplers_ARBURST;
  wire [3:0]auto_pc_to_s00_couplers_ARCACHE;
  wire [11:0]auto_pc_to_s00_couplers_ARID;
  wire [7:0]auto_pc_to_s00_couplers_ARLEN;
  wire [0:0]auto_pc_to_s00_couplers_ARLOCK;
  wire [2:0]auto_pc_to_s00_couplers_ARPROT;
  wire auto_pc_to_s00_couplers_ARREADY;
  wire [2:0]auto_pc_to_s00_couplers_ARSIZE;
  wire auto_pc_to_s00_couplers_ARVALID;
  wire [31:0]auto_pc_to_s00_couplers_AWADDR;
  wire [1:0]auto_pc_to_s00_couplers_AWBURST;
  wire [3:0]auto_pc_to_s00_couplers_AWCACHE;
  wire [11:0]auto_pc_to_s00_couplers_AWID;
  wire [7:0]auto_pc_to_s00_couplers_AWLEN;
  wire [0:0]auto_pc_to_s00_couplers_AWLOCK;
  wire [2:0]auto_pc_to_s00_couplers_AWPROT;
  wire auto_pc_to_s00_couplers_AWREADY;
  wire [2:0]auto_pc_to_s00_couplers_AWSIZE;
  wire auto_pc_to_s00_couplers_AWVALID;
  wire [11:0]auto_pc_to_s00_couplers_BID;
  wire auto_pc_to_s00_couplers_BREADY;
  wire [1:0]auto_pc_to_s00_couplers_BRESP;
  wire auto_pc_to_s00_couplers_BVALID;
  wire [31:0]auto_pc_to_s00_couplers_RDATA;
  wire [11:0]auto_pc_to_s00_couplers_RID;
  wire auto_pc_to_s00_couplers_RLAST;
  wire auto_pc_to_s00_couplers_RREADY;
  wire [1:0]auto_pc_to_s00_couplers_RRESP;
  wire auto_pc_to_s00_couplers_RVALID;
  wire [31:0]auto_pc_to_s00_couplers_WDATA;
  wire auto_pc_to_s00_couplers_WLAST;
  wire auto_pc_to_s00_couplers_WREADY;
  wire [3:0]auto_pc_to_s00_couplers_WSTRB;
  wire auto_pc_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_auto_pc_ARADDR;
  wire [1:0]s00_couplers_to_auto_pc_ARBURST;
  wire [3:0]s00_couplers_to_auto_pc_ARCACHE;
  wire [11:0]s00_couplers_to_auto_pc_ARID;
  wire [3:0]s00_couplers_to_auto_pc_ARLEN;
  wire [1:0]s00_couplers_to_auto_pc_ARLOCK;
  wire [2:0]s00_couplers_to_auto_pc_ARPROT;
  wire [3:0]s00_couplers_to_auto_pc_ARQOS;
  wire s00_couplers_to_auto_pc_ARREADY;
  wire [2:0]s00_couplers_to_auto_pc_ARSIZE;
  wire s00_couplers_to_auto_pc_ARVALID;
  wire [31:0]s00_couplers_to_auto_pc_AWADDR;
  wire [1:0]s00_couplers_to_auto_pc_AWBURST;
  wire [3:0]s00_couplers_to_auto_pc_AWCACHE;
  wire [11:0]s00_couplers_to_auto_pc_AWID;
  wire [3:0]s00_couplers_to_auto_pc_AWLEN;
  wire [1:0]s00_couplers_to_auto_pc_AWLOCK;
  wire [2:0]s00_couplers_to_auto_pc_AWPROT;
  wire [3:0]s00_couplers_to_auto_pc_AWQOS;
  wire s00_couplers_to_auto_pc_AWREADY;
  wire [2:0]s00_couplers_to_auto_pc_AWSIZE;
  wire s00_couplers_to_auto_pc_AWVALID;
  wire [11:0]s00_couplers_to_auto_pc_BID;
  wire s00_couplers_to_auto_pc_BREADY;
  wire [1:0]s00_couplers_to_auto_pc_BRESP;
  wire s00_couplers_to_auto_pc_BVALID;
  wire [31:0]s00_couplers_to_auto_pc_RDATA;
  wire [11:0]s00_couplers_to_auto_pc_RID;
  wire s00_couplers_to_auto_pc_RLAST;
  wire s00_couplers_to_auto_pc_RREADY;
  wire [1:0]s00_couplers_to_auto_pc_RRESP;
  wire s00_couplers_to_auto_pc_RVALID;
  wire [31:0]s00_couplers_to_auto_pc_WDATA;
  wire [11:0]s00_couplers_to_auto_pc_WID;
  wire s00_couplers_to_auto_pc_WLAST;
  wire s00_couplers_to_auto_pc_WREADY;
  wire [3:0]s00_couplers_to_auto_pc_WSTRB;
  wire s00_couplers_to_auto_pc_WVALID;

  assign M_AXI_araddr[15:0] = auto_pc_to_s00_couplers_ARADDR[15:0];
  assign M_AXI_arburst[1:0] = auto_pc_to_s00_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_pc_to_s00_couplers_ARCACHE;
  assign M_AXI_arid[11:0] = auto_pc_to_s00_couplers_ARID;
  assign M_AXI_arlen[7:0] = auto_pc_to_s00_couplers_ARLEN;
  assign M_AXI_arlock = auto_pc_to_s00_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_pc_to_s00_couplers_ARPROT;
  assign M_AXI_arsize[2:0] = auto_pc_to_s00_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_pc_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[15:0] = auto_pc_to_s00_couplers_AWADDR[15:0];
  assign M_AXI_awburst[1:0] = auto_pc_to_s00_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_pc_to_s00_couplers_AWCACHE;
  assign M_AXI_awid[11:0] = auto_pc_to_s00_couplers_AWID;
  assign M_AXI_awlen[7:0] = auto_pc_to_s00_couplers_AWLEN;
  assign M_AXI_awlock = auto_pc_to_s00_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_pc_to_s00_couplers_AWPROT;
  assign M_AXI_awsize[2:0] = auto_pc_to_s00_couplers_AWSIZE;
  assign M_AXI_awvalid = auto_pc_to_s00_couplers_AWVALID;
  assign M_AXI_bready = auto_pc_to_s00_couplers_BREADY;
  assign M_AXI_rready = auto_pc_to_s00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_pc_to_s00_couplers_WDATA;
  assign M_AXI_wlast = auto_pc_to_s00_couplers_WLAST;
  assign M_AXI_wstrb[3:0] = auto_pc_to_s00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_pc_to_s00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_arready = s00_couplers_to_auto_pc_ARREADY;
  assign S_AXI_awready = s00_couplers_to_auto_pc_AWREADY;
  assign S_AXI_bid[11:0] = s00_couplers_to_auto_pc_BID;
  assign S_AXI_bresp[1:0] = s00_couplers_to_auto_pc_BRESP;
  assign S_AXI_bvalid = s00_couplers_to_auto_pc_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_auto_pc_RDATA;
  assign S_AXI_rid[11:0] = s00_couplers_to_auto_pc_RID;
  assign S_AXI_rlast = s00_couplers_to_auto_pc_RLAST;
  assign S_AXI_rresp[1:0] = s00_couplers_to_auto_pc_RRESP;
  assign S_AXI_rvalid = s00_couplers_to_auto_pc_RVALID;
  assign S_AXI_wready = s00_couplers_to_auto_pc_WREADY;
  assign auto_pc_to_s00_couplers_ARREADY = M_AXI_arready;
  assign auto_pc_to_s00_couplers_AWREADY = M_AXI_awready;
  assign auto_pc_to_s00_couplers_BID = M_AXI_bid[11:0];
  assign auto_pc_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_pc_to_s00_couplers_BVALID = M_AXI_bvalid;
  assign auto_pc_to_s00_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_pc_to_s00_couplers_RID = M_AXI_rid[11:0];
  assign auto_pc_to_s00_couplers_RLAST = M_AXI_rlast;
  assign auto_pc_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_pc_to_s00_couplers_RVALID = M_AXI_rvalid;
  assign auto_pc_to_s00_couplers_WREADY = M_AXI_wready;
  assign s00_couplers_to_auto_pc_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_auto_pc_ARBURST = S_AXI_arburst[1:0];
  assign s00_couplers_to_auto_pc_ARCACHE = S_AXI_arcache[3:0];
  assign s00_couplers_to_auto_pc_ARID = S_AXI_arid[11:0];
  assign s00_couplers_to_auto_pc_ARLEN = S_AXI_arlen[3:0];
  assign s00_couplers_to_auto_pc_ARLOCK = S_AXI_arlock[1:0];
  assign s00_couplers_to_auto_pc_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_auto_pc_ARQOS = S_AXI_arqos[3:0];
  assign s00_couplers_to_auto_pc_ARSIZE = S_AXI_arsize[2:0];
  assign s00_couplers_to_auto_pc_ARVALID = S_AXI_arvalid;
  assign s00_couplers_to_auto_pc_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_auto_pc_AWBURST = S_AXI_awburst[1:0];
  assign s00_couplers_to_auto_pc_AWCACHE = S_AXI_awcache[3:0];
  assign s00_couplers_to_auto_pc_AWID = S_AXI_awid[11:0];
  assign s00_couplers_to_auto_pc_AWLEN = S_AXI_awlen[3:0];
  assign s00_couplers_to_auto_pc_AWLOCK = S_AXI_awlock[1:0];
  assign s00_couplers_to_auto_pc_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_auto_pc_AWQOS = S_AXI_awqos[3:0];
  assign s00_couplers_to_auto_pc_AWSIZE = S_AXI_awsize[2:0];
  assign s00_couplers_to_auto_pc_AWVALID = S_AXI_awvalid;
  assign s00_couplers_to_auto_pc_BREADY = S_AXI_bready;
  assign s00_couplers_to_auto_pc_RREADY = S_AXI_rready;
  assign s00_couplers_to_auto_pc_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_auto_pc_WID = S_AXI_wid[11:0];
  assign s00_couplers_to_auto_pc_WLAST = S_AXI_wlast;
  assign s00_couplers_to_auto_pc_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_auto_pc_WVALID = S_AXI_wvalid;
zynq_design_1_auto_pc_0 auto_pc
       (.aclk(S_ACLK_1),
        .aresetn(S_ARESETN_1),
        .m_axi_araddr(auto_pc_to_s00_couplers_ARADDR),
        .m_axi_arburst(auto_pc_to_s00_couplers_ARBURST),
        .m_axi_arcache(auto_pc_to_s00_couplers_ARCACHE),
        .m_axi_arid(auto_pc_to_s00_couplers_ARID),
        .m_axi_arlen(auto_pc_to_s00_couplers_ARLEN),
        .m_axi_arlock(auto_pc_to_s00_couplers_ARLOCK),
        .m_axi_arprot(auto_pc_to_s00_couplers_ARPROT),
        .m_axi_arready(auto_pc_to_s00_couplers_ARREADY),
        .m_axi_arsize(auto_pc_to_s00_couplers_ARSIZE),
        .m_axi_arvalid(auto_pc_to_s00_couplers_ARVALID),
        .m_axi_awaddr(auto_pc_to_s00_couplers_AWADDR),
        .m_axi_awburst(auto_pc_to_s00_couplers_AWBURST),
        .m_axi_awcache(auto_pc_to_s00_couplers_AWCACHE),
        .m_axi_awid(auto_pc_to_s00_couplers_AWID),
        .m_axi_awlen(auto_pc_to_s00_couplers_AWLEN),
        .m_axi_awlock(auto_pc_to_s00_couplers_AWLOCK),
        .m_axi_awprot(auto_pc_to_s00_couplers_AWPROT),
        .m_axi_awready(auto_pc_to_s00_couplers_AWREADY),
        .m_axi_awsize(auto_pc_to_s00_couplers_AWSIZE),
        .m_axi_awvalid(auto_pc_to_s00_couplers_AWVALID),
        .m_axi_bid(auto_pc_to_s00_couplers_BID),
        .m_axi_bready(auto_pc_to_s00_couplers_BREADY),
        .m_axi_bresp(auto_pc_to_s00_couplers_BRESP),
        .m_axi_bvalid(auto_pc_to_s00_couplers_BVALID),
        .m_axi_rdata(auto_pc_to_s00_couplers_RDATA),
        .m_axi_rid(auto_pc_to_s00_couplers_RID),
        .m_axi_rlast(auto_pc_to_s00_couplers_RLAST),
        .m_axi_rready(auto_pc_to_s00_couplers_RREADY),
        .m_axi_rresp(auto_pc_to_s00_couplers_RRESP),
        .m_axi_rvalid(auto_pc_to_s00_couplers_RVALID),
        .m_axi_wdata(auto_pc_to_s00_couplers_WDATA),
        .m_axi_wlast(auto_pc_to_s00_couplers_WLAST),
        .m_axi_wready(auto_pc_to_s00_couplers_WREADY),
        .m_axi_wstrb(auto_pc_to_s00_couplers_WSTRB),
        .m_axi_wvalid(auto_pc_to_s00_couplers_WVALID),
        .s_axi_araddr(s00_couplers_to_auto_pc_ARADDR),
        .s_axi_arburst(s00_couplers_to_auto_pc_ARBURST),
        .s_axi_arcache(s00_couplers_to_auto_pc_ARCACHE),
        .s_axi_arid(s00_couplers_to_auto_pc_ARID),
        .s_axi_arlen(s00_couplers_to_auto_pc_ARLEN),
        .s_axi_arlock(s00_couplers_to_auto_pc_ARLOCK),
        .s_axi_arprot(s00_couplers_to_auto_pc_ARPROT),
        .s_axi_arqos(s00_couplers_to_auto_pc_ARQOS),
        .s_axi_arready(s00_couplers_to_auto_pc_ARREADY),
        .s_axi_arsize(s00_couplers_to_auto_pc_ARSIZE),
        .s_axi_arvalid(s00_couplers_to_auto_pc_ARVALID),
        .s_axi_awaddr(s00_couplers_to_auto_pc_AWADDR),
        .s_axi_awburst(s00_couplers_to_auto_pc_AWBURST),
        .s_axi_awcache(s00_couplers_to_auto_pc_AWCACHE),
        .s_axi_awid(s00_couplers_to_auto_pc_AWID),
        .s_axi_awlen(s00_couplers_to_auto_pc_AWLEN),
        .s_axi_awlock(s00_couplers_to_auto_pc_AWLOCK),
        .s_axi_awprot(s00_couplers_to_auto_pc_AWPROT),
        .s_axi_awqos(s00_couplers_to_auto_pc_AWQOS),
        .s_axi_awready(s00_couplers_to_auto_pc_AWREADY),
        .s_axi_awsize(s00_couplers_to_auto_pc_AWSIZE),
        .s_axi_awvalid(s00_couplers_to_auto_pc_AWVALID),
        .s_axi_bid(s00_couplers_to_auto_pc_BID),
        .s_axi_bready(s00_couplers_to_auto_pc_BREADY),
        .s_axi_bresp(s00_couplers_to_auto_pc_BRESP),
        .s_axi_bvalid(s00_couplers_to_auto_pc_BVALID),
        .s_axi_rdata(s00_couplers_to_auto_pc_RDATA),
        .s_axi_rid(s00_couplers_to_auto_pc_RID),
        .s_axi_rlast(s00_couplers_to_auto_pc_RLAST),
        .s_axi_rready(s00_couplers_to_auto_pc_RREADY),
        .s_axi_rresp(s00_couplers_to_auto_pc_RRESP),
        .s_axi_rvalid(s00_couplers_to_auto_pc_RVALID),
        .s_axi_wdata(s00_couplers_to_auto_pc_WDATA),
        .s_axi_wid(s00_couplers_to_auto_pc_WID),
        .s_axi_wlast(s00_couplers_to_auto_pc_WLAST),
        .s_axi_wready(s00_couplers_to_auto_pc_WREADY),
        .s_axi_wstrb(s00_couplers_to_auto_pc_WSTRB),
        .s_axi_wvalid(s00_couplers_to_auto_pc_WVALID));
endmodule

module s00_couplers_imp_Q87KJ3
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arid,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awid,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rid,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wid,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [15:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [11:0]M_AXI_arid;
  output [7:0]M_AXI_arlen;
  output [0:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  output [15:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [11:0]M_AXI_awid;
  output [7:0]M_AXI_awlen;
  output [0:0]M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  input [11:0]M_AXI_bid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  input [11:0]M_AXI_rid;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [11:0]S_AXI_arid;
  input [3:0]S_AXI_arlen;
  input [1:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [11:0]S_AXI_awid;
  input [3:0]S_AXI_awlen;
  input [1:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [11:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [11:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input [11:0]S_AXI_wid;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_pc_to_s00_couplers_ARADDR;
  wire [1:0]auto_pc_to_s00_couplers_ARBURST;
  wire [3:0]auto_pc_to_s00_couplers_ARCACHE;
  wire [11:0]auto_pc_to_s00_couplers_ARID;
  wire [7:0]auto_pc_to_s00_couplers_ARLEN;
  wire [0:0]auto_pc_to_s00_couplers_ARLOCK;
  wire [2:0]auto_pc_to_s00_couplers_ARPROT;
  wire auto_pc_to_s00_couplers_ARREADY;
  wire [2:0]auto_pc_to_s00_couplers_ARSIZE;
  wire auto_pc_to_s00_couplers_ARVALID;
  wire [31:0]auto_pc_to_s00_couplers_AWADDR;
  wire [1:0]auto_pc_to_s00_couplers_AWBURST;
  wire [3:0]auto_pc_to_s00_couplers_AWCACHE;
  wire [11:0]auto_pc_to_s00_couplers_AWID;
  wire [7:0]auto_pc_to_s00_couplers_AWLEN;
  wire [0:0]auto_pc_to_s00_couplers_AWLOCK;
  wire [2:0]auto_pc_to_s00_couplers_AWPROT;
  wire auto_pc_to_s00_couplers_AWREADY;
  wire [2:0]auto_pc_to_s00_couplers_AWSIZE;
  wire auto_pc_to_s00_couplers_AWVALID;
  wire [11:0]auto_pc_to_s00_couplers_BID;
  wire auto_pc_to_s00_couplers_BREADY;
  wire [1:0]auto_pc_to_s00_couplers_BRESP;
  wire auto_pc_to_s00_couplers_BVALID;
  wire [31:0]auto_pc_to_s00_couplers_RDATA;
  wire [11:0]auto_pc_to_s00_couplers_RID;
  wire auto_pc_to_s00_couplers_RLAST;
  wire auto_pc_to_s00_couplers_RREADY;
  wire [1:0]auto_pc_to_s00_couplers_RRESP;
  wire auto_pc_to_s00_couplers_RVALID;
  wire [31:0]auto_pc_to_s00_couplers_WDATA;
  wire auto_pc_to_s00_couplers_WLAST;
  wire auto_pc_to_s00_couplers_WREADY;
  wire [3:0]auto_pc_to_s00_couplers_WSTRB;
  wire auto_pc_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_auto_pc_ARADDR;
  wire [1:0]s00_couplers_to_auto_pc_ARBURST;
  wire [3:0]s00_couplers_to_auto_pc_ARCACHE;
  wire [11:0]s00_couplers_to_auto_pc_ARID;
  wire [3:0]s00_couplers_to_auto_pc_ARLEN;
  wire [1:0]s00_couplers_to_auto_pc_ARLOCK;
  wire [2:0]s00_couplers_to_auto_pc_ARPROT;
  wire [3:0]s00_couplers_to_auto_pc_ARQOS;
  wire s00_couplers_to_auto_pc_ARREADY;
  wire [2:0]s00_couplers_to_auto_pc_ARSIZE;
  wire s00_couplers_to_auto_pc_ARVALID;
  wire [31:0]s00_couplers_to_auto_pc_AWADDR;
  wire [1:0]s00_couplers_to_auto_pc_AWBURST;
  wire [3:0]s00_couplers_to_auto_pc_AWCACHE;
  wire [11:0]s00_couplers_to_auto_pc_AWID;
  wire [3:0]s00_couplers_to_auto_pc_AWLEN;
  wire [1:0]s00_couplers_to_auto_pc_AWLOCK;
  wire [2:0]s00_couplers_to_auto_pc_AWPROT;
  wire [3:0]s00_couplers_to_auto_pc_AWQOS;
  wire s00_couplers_to_auto_pc_AWREADY;
  wire [2:0]s00_couplers_to_auto_pc_AWSIZE;
  wire s00_couplers_to_auto_pc_AWVALID;
  wire [11:0]s00_couplers_to_auto_pc_BID;
  wire s00_couplers_to_auto_pc_BREADY;
  wire [1:0]s00_couplers_to_auto_pc_BRESP;
  wire s00_couplers_to_auto_pc_BVALID;
  wire [31:0]s00_couplers_to_auto_pc_RDATA;
  wire [11:0]s00_couplers_to_auto_pc_RID;
  wire s00_couplers_to_auto_pc_RLAST;
  wire s00_couplers_to_auto_pc_RREADY;
  wire [1:0]s00_couplers_to_auto_pc_RRESP;
  wire s00_couplers_to_auto_pc_RVALID;
  wire [31:0]s00_couplers_to_auto_pc_WDATA;
  wire [11:0]s00_couplers_to_auto_pc_WID;
  wire s00_couplers_to_auto_pc_WLAST;
  wire s00_couplers_to_auto_pc_WREADY;
  wire [3:0]s00_couplers_to_auto_pc_WSTRB;
  wire s00_couplers_to_auto_pc_WVALID;

  assign M_AXI_araddr[15:0] = auto_pc_to_s00_couplers_ARADDR[15:0];
  assign M_AXI_arburst[1:0] = auto_pc_to_s00_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_pc_to_s00_couplers_ARCACHE;
  assign M_AXI_arid[11:0] = auto_pc_to_s00_couplers_ARID;
  assign M_AXI_arlen[7:0] = auto_pc_to_s00_couplers_ARLEN;
  assign M_AXI_arlock[0] = auto_pc_to_s00_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_pc_to_s00_couplers_ARPROT;
  assign M_AXI_arsize[2:0] = auto_pc_to_s00_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_pc_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[15:0] = auto_pc_to_s00_couplers_AWADDR[15:0];
  assign M_AXI_awburst[1:0] = auto_pc_to_s00_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_pc_to_s00_couplers_AWCACHE;
  assign M_AXI_awid[11:0] = auto_pc_to_s00_couplers_AWID;
  assign M_AXI_awlen[7:0] = auto_pc_to_s00_couplers_AWLEN;
  assign M_AXI_awlock[0] = auto_pc_to_s00_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_pc_to_s00_couplers_AWPROT;
  assign M_AXI_awsize[2:0] = auto_pc_to_s00_couplers_AWSIZE;
  assign M_AXI_awvalid = auto_pc_to_s00_couplers_AWVALID;
  assign M_AXI_bready = auto_pc_to_s00_couplers_BREADY;
  assign M_AXI_rready = auto_pc_to_s00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_pc_to_s00_couplers_WDATA;
  assign M_AXI_wlast = auto_pc_to_s00_couplers_WLAST;
  assign M_AXI_wstrb[3:0] = auto_pc_to_s00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_pc_to_s00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_arready = s00_couplers_to_auto_pc_ARREADY;
  assign S_AXI_awready = s00_couplers_to_auto_pc_AWREADY;
  assign S_AXI_bid[11:0] = s00_couplers_to_auto_pc_BID;
  assign S_AXI_bresp[1:0] = s00_couplers_to_auto_pc_BRESP;
  assign S_AXI_bvalid = s00_couplers_to_auto_pc_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_auto_pc_RDATA;
  assign S_AXI_rid[11:0] = s00_couplers_to_auto_pc_RID;
  assign S_AXI_rlast = s00_couplers_to_auto_pc_RLAST;
  assign S_AXI_rresp[1:0] = s00_couplers_to_auto_pc_RRESP;
  assign S_AXI_rvalid = s00_couplers_to_auto_pc_RVALID;
  assign S_AXI_wready = s00_couplers_to_auto_pc_WREADY;
  assign auto_pc_to_s00_couplers_ARREADY = M_AXI_arready;
  assign auto_pc_to_s00_couplers_AWREADY = M_AXI_awready;
  assign auto_pc_to_s00_couplers_BID = M_AXI_bid[11:0];
  assign auto_pc_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_pc_to_s00_couplers_BVALID = M_AXI_bvalid;
  assign auto_pc_to_s00_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_pc_to_s00_couplers_RID = M_AXI_rid[11:0];
  assign auto_pc_to_s00_couplers_RLAST = M_AXI_rlast;
  assign auto_pc_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_pc_to_s00_couplers_RVALID = M_AXI_rvalid;
  assign auto_pc_to_s00_couplers_WREADY = M_AXI_wready;
  assign s00_couplers_to_auto_pc_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_auto_pc_ARBURST = S_AXI_arburst[1:0];
  assign s00_couplers_to_auto_pc_ARCACHE = S_AXI_arcache[3:0];
  assign s00_couplers_to_auto_pc_ARID = S_AXI_arid[11:0];
  assign s00_couplers_to_auto_pc_ARLEN = S_AXI_arlen[3:0];
  assign s00_couplers_to_auto_pc_ARLOCK = S_AXI_arlock[1:0];
  assign s00_couplers_to_auto_pc_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_auto_pc_ARQOS = S_AXI_arqos[3:0];
  assign s00_couplers_to_auto_pc_ARSIZE = S_AXI_arsize[2:0];
  assign s00_couplers_to_auto_pc_ARVALID = S_AXI_arvalid;
  assign s00_couplers_to_auto_pc_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_auto_pc_AWBURST = S_AXI_awburst[1:0];
  assign s00_couplers_to_auto_pc_AWCACHE = S_AXI_awcache[3:0];
  assign s00_couplers_to_auto_pc_AWID = S_AXI_awid[11:0];
  assign s00_couplers_to_auto_pc_AWLEN = S_AXI_awlen[3:0];
  assign s00_couplers_to_auto_pc_AWLOCK = S_AXI_awlock[1:0];
  assign s00_couplers_to_auto_pc_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_auto_pc_AWQOS = S_AXI_awqos[3:0];
  assign s00_couplers_to_auto_pc_AWSIZE = S_AXI_awsize[2:0];
  assign s00_couplers_to_auto_pc_AWVALID = S_AXI_awvalid;
  assign s00_couplers_to_auto_pc_BREADY = S_AXI_bready;
  assign s00_couplers_to_auto_pc_RREADY = S_AXI_rready;
  assign s00_couplers_to_auto_pc_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_auto_pc_WID = S_AXI_wid[11:0];
  assign s00_couplers_to_auto_pc_WLAST = S_AXI_wlast;
  assign s00_couplers_to_auto_pc_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_auto_pc_WVALID = S_AXI_wvalid;
zynq_design_1_auto_pc_1 auto_pc
       (.aclk(S_ACLK_1),
        .aresetn(S_ARESETN_1),
        .m_axi_araddr(auto_pc_to_s00_couplers_ARADDR),
        .m_axi_arburst(auto_pc_to_s00_couplers_ARBURST),
        .m_axi_arcache(auto_pc_to_s00_couplers_ARCACHE),
        .m_axi_arid(auto_pc_to_s00_couplers_ARID),
        .m_axi_arlen(auto_pc_to_s00_couplers_ARLEN),
        .m_axi_arlock(auto_pc_to_s00_couplers_ARLOCK),
        .m_axi_arprot(auto_pc_to_s00_couplers_ARPROT),
        .m_axi_arready(auto_pc_to_s00_couplers_ARREADY),
        .m_axi_arsize(auto_pc_to_s00_couplers_ARSIZE),
        .m_axi_arvalid(auto_pc_to_s00_couplers_ARVALID),
        .m_axi_awaddr(auto_pc_to_s00_couplers_AWADDR),
        .m_axi_awburst(auto_pc_to_s00_couplers_AWBURST),
        .m_axi_awcache(auto_pc_to_s00_couplers_AWCACHE),
        .m_axi_awid(auto_pc_to_s00_couplers_AWID),
        .m_axi_awlen(auto_pc_to_s00_couplers_AWLEN),
        .m_axi_awlock(auto_pc_to_s00_couplers_AWLOCK),
        .m_axi_awprot(auto_pc_to_s00_couplers_AWPROT),
        .m_axi_awready(auto_pc_to_s00_couplers_AWREADY),
        .m_axi_awsize(auto_pc_to_s00_couplers_AWSIZE),
        .m_axi_awvalid(auto_pc_to_s00_couplers_AWVALID),
        .m_axi_bid(auto_pc_to_s00_couplers_BID),
        .m_axi_bready(auto_pc_to_s00_couplers_BREADY),
        .m_axi_bresp(auto_pc_to_s00_couplers_BRESP),
        .m_axi_bvalid(auto_pc_to_s00_couplers_BVALID),
        .m_axi_rdata(auto_pc_to_s00_couplers_RDATA),
        .m_axi_rid(auto_pc_to_s00_couplers_RID),
        .m_axi_rlast(auto_pc_to_s00_couplers_RLAST),
        .m_axi_rready(auto_pc_to_s00_couplers_RREADY),
        .m_axi_rresp(auto_pc_to_s00_couplers_RRESP),
        .m_axi_rvalid(auto_pc_to_s00_couplers_RVALID),
        .m_axi_wdata(auto_pc_to_s00_couplers_WDATA),
        .m_axi_wlast(auto_pc_to_s00_couplers_WLAST),
        .m_axi_wready(auto_pc_to_s00_couplers_WREADY),
        .m_axi_wstrb(auto_pc_to_s00_couplers_WSTRB),
        .m_axi_wvalid(auto_pc_to_s00_couplers_WVALID),
        .s_axi_araddr(s00_couplers_to_auto_pc_ARADDR),
        .s_axi_arburst(s00_couplers_to_auto_pc_ARBURST),
        .s_axi_arcache(s00_couplers_to_auto_pc_ARCACHE),
        .s_axi_arid(s00_couplers_to_auto_pc_ARID),
        .s_axi_arlen(s00_couplers_to_auto_pc_ARLEN),
        .s_axi_arlock(s00_couplers_to_auto_pc_ARLOCK),
        .s_axi_arprot(s00_couplers_to_auto_pc_ARPROT),
        .s_axi_arqos(s00_couplers_to_auto_pc_ARQOS),
        .s_axi_arready(s00_couplers_to_auto_pc_ARREADY),
        .s_axi_arsize(s00_couplers_to_auto_pc_ARSIZE),
        .s_axi_arvalid(s00_couplers_to_auto_pc_ARVALID),
        .s_axi_awaddr(s00_couplers_to_auto_pc_AWADDR),
        .s_axi_awburst(s00_couplers_to_auto_pc_AWBURST),
        .s_axi_awcache(s00_couplers_to_auto_pc_AWCACHE),
        .s_axi_awid(s00_couplers_to_auto_pc_AWID),
        .s_axi_awlen(s00_couplers_to_auto_pc_AWLEN),
        .s_axi_awlock(s00_couplers_to_auto_pc_AWLOCK),
        .s_axi_awprot(s00_couplers_to_auto_pc_AWPROT),
        .s_axi_awqos(s00_couplers_to_auto_pc_AWQOS),
        .s_axi_awready(s00_couplers_to_auto_pc_AWREADY),
        .s_axi_awsize(s00_couplers_to_auto_pc_AWSIZE),
        .s_axi_awvalid(s00_couplers_to_auto_pc_AWVALID),
        .s_axi_bid(s00_couplers_to_auto_pc_BID),
        .s_axi_bready(s00_couplers_to_auto_pc_BREADY),
        .s_axi_bresp(s00_couplers_to_auto_pc_BRESP),
        .s_axi_bvalid(s00_couplers_to_auto_pc_BVALID),
        .s_axi_rdata(s00_couplers_to_auto_pc_RDATA),
        .s_axi_rid(s00_couplers_to_auto_pc_RID),
        .s_axi_rlast(s00_couplers_to_auto_pc_RLAST),
        .s_axi_rready(s00_couplers_to_auto_pc_RREADY),
        .s_axi_rresp(s00_couplers_to_auto_pc_RRESP),
        .s_axi_rvalid(s00_couplers_to_auto_pc_RVALID),
        .s_axi_wdata(s00_couplers_to_auto_pc_WDATA),
        .s_axi_wid(s00_couplers_to_auto_pc_WID),
        .s_axi_wlast(s00_couplers_to_auto_pc_WLAST),
        .s_axi_wready(s00_couplers_to_auto_pc_WREADY),
        .s_axi_wstrb(s00_couplers_to_auto_pc_WSTRB),
        .s_axi_wvalid(s00_couplers_to_auto_pc_WVALID));
endmodule

(* CORE_GENERATION_INFO = "zynq_design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLanguage=VERILOG,numBlks=30,numReposBlks=23,numNonXlnxBlks=0,numHierBlks=7,maxHierDepth=1,da_axi4_cnt=2,da_bram_cntlr_cnt=4,da_ps7_cnt=1}" *) 
module zynq_design_1
   (DDR3_addr,
    DDR3_ba,
    DDR3_cas_n,
    DDR3_ck_n,
    DDR3_ck_p,
    DDR3_cke,
    DDR3_cs_n,
    DDR3_dm,
    DDR3_dq,
    DDR3_dqs_n,
    DDR3_dqs_p,
    DDR3_odt,
    DDR3_ras_n,
    DDR3_reset_n,
    DDR3_we_n,
    DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    DE,
    Data_out,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    SYS_CLK_clk_n,
    SYS_CLK_clk_p,
    hsync,
    pclk,
    scl_pad,
    sda_pad,
    vsync);
  output [13:0]DDR3_addr;
  output [2:0]DDR3_ba;
  output DDR3_cas_n;
  output [0:0]DDR3_ck_n;
  output [0:0]DDR3_ck_p;
  output [0:0]DDR3_cke;
  output [0:0]DDR3_cs_n;
  output [7:0]DDR3_dm;
  inout [63:0]DDR3_dq;
  inout [7:0]DDR3_dqs_n;
  inout [7:0]DDR3_dqs_p;
  output [0:0]DDR3_odt;
  output DDR3_ras_n;
  output DDR3_reset_n;
  output DDR3_we_n;
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  output DE;
  output [23:0]Data_out;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  input SYS_CLK_clk_n;
  input SYS_CLK_clk_p;
  output hsync;
  output pclk;
  inout scl_pad;
  inout sda_pad;
  output vsync;

(* MARK_DEBUG *)   wire [31:0]B_test;
  wire GND_1;
  wire Net;
  wire Net1;
  wire [31:0]S00_AXI_1_ARADDR;
  wire [1:0]S00_AXI_1_ARBURST;
  wire [3:0]S00_AXI_1_ARCACHE;
  wire [11:0]S00_AXI_1_ARID;
  wire [3:0]S00_AXI_1_ARLEN;
  wire [1:0]S00_AXI_1_ARLOCK;
  wire [2:0]S00_AXI_1_ARPROT;
  wire [3:0]S00_AXI_1_ARQOS;
  wire S00_AXI_1_ARREADY;
  wire [2:0]S00_AXI_1_ARSIZE;
  wire S00_AXI_1_ARVALID;
  wire [31:0]S00_AXI_1_AWADDR;
  wire [1:0]S00_AXI_1_AWBURST;
  wire [3:0]S00_AXI_1_AWCACHE;
  wire [11:0]S00_AXI_1_AWID;
  wire [3:0]S00_AXI_1_AWLEN;
  wire [1:0]S00_AXI_1_AWLOCK;
  wire [2:0]S00_AXI_1_AWPROT;
  wire [3:0]S00_AXI_1_AWQOS;
  wire S00_AXI_1_AWREADY;
  wire [2:0]S00_AXI_1_AWSIZE;
  wire S00_AXI_1_AWVALID;
  wire [11:0]S00_AXI_1_BID;
  wire S00_AXI_1_BREADY;
  wire [1:0]S00_AXI_1_BRESP;
  wire S00_AXI_1_BVALID;
  wire [31:0]S00_AXI_1_RDATA;
  wire [11:0]S00_AXI_1_RID;
  wire S00_AXI_1_RLAST;
  wire S00_AXI_1_RREADY;
  wire [1:0]S00_AXI_1_RRESP;
  wire S00_AXI_1_RVALID;
  wire [31:0]S00_AXI_1_WDATA;
  wire [11:0]S00_AXI_1_WID;
  wire S00_AXI_1_WLAST;
  wire S00_AXI_1_WREADY;
  wire [3:0]S00_AXI_1_WSTRB;
  wire S00_AXI_1_WVALID;
  wire SYS_CLK_1_CLK_N;
  wire SYS_CLK_1_CLK_P;
  wire VCC_1;
  wire [31:0]axi_displaybuffer_handle_0_axi_ARADDR;
  wire [1:0]axi_displaybuffer_handle_0_axi_ARBURST;
  wire [3:0]axi_displaybuffer_handle_0_axi_ARID;
  wire [7:0]axi_displaybuffer_handle_0_axi_ARLEN;
  wire axi_displaybuffer_handle_0_axi_ARREADY;
  wire [2:0]axi_displaybuffer_handle_0_axi_ARSIZE;
  wire axi_displaybuffer_handle_0_axi_ARVALID;
  wire [31:0]axi_displaybuffer_handle_0_axi_AWADDR;
  wire [1:0]axi_displaybuffer_handle_0_axi_AWBURST;
  wire [3:0]axi_displaybuffer_handle_0_axi_AWID;
  wire [7:0]axi_displaybuffer_handle_0_axi_AWLEN;
  wire axi_displaybuffer_handle_0_axi_AWREADY;
  wire [2:0]axi_displaybuffer_handle_0_axi_AWSIZE;
  wire axi_displaybuffer_handle_0_axi_AWVALID;
  wire axi_displaybuffer_handle_0_axi_BREADY;
  wire [1:0]axi_displaybuffer_handle_0_axi_BRESP;
  wire axi_displaybuffer_handle_0_axi_BVALID;
  wire [511:0]axi_displaybuffer_handle_0_axi_RDATA;
  wire axi_displaybuffer_handle_0_axi_RLAST;
  wire axi_displaybuffer_handle_0_axi_RREADY;
  wire [1:0]axi_displaybuffer_handle_0_axi_RRESP;
  wire axi_displaybuffer_handle_0_axi_RVALID;
  wire [511:0]axi_displaybuffer_handle_0_axi_WDATA;
  wire axi_displaybuffer_handle_0_axi_WLAST;
  wire axi_displaybuffer_handle_0_axi_WREADY;
  wire [63:0]axi_displaybuffer_handle_0_axi_WSTRB;
  wire axi_displaybuffer_handle_0_axi_WVALID;
  wire axi_displaybuffer_handle_0_fifo_read_rtl_1_EMPTY;
  wire [783:0]axi_displaybuffer_handle_0_fifo_read_rtl_1_RD_DATA;
  wire axi_displaybuffer_handle_0_fifo_read_rtl_1_RD_EN;
  wire [63:0]axi_displaybuffer_handle_0_hdmi_fifo_data_out;
(* MARK_DEBUG *)   wire axi_displaybuffer_handle_0_hdmi_fifo_w_en;
  wire [31:0]axi_dwidth_converter_0_M_AXI_ARADDR;
  wire [1:0]axi_dwidth_converter_0_M_AXI_ARBURST;
  wire [3:0]axi_dwidth_converter_0_M_AXI_ARCACHE;
  wire [3:0]axi_dwidth_converter_0_M_AXI_ARLEN;
  wire [1:0]axi_dwidth_converter_0_M_AXI_ARLOCK;
  wire [2:0]axi_dwidth_converter_0_M_AXI_ARPROT;
  wire [3:0]axi_dwidth_converter_0_M_AXI_ARQOS;
  wire axi_dwidth_converter_0_M_AXI_ARREADY;
  wire [2:0]axi_dwidth_converter_0_M_AXI_ARSIZE;
  wire axi_dwidth_converter_0_M_AXI_ARVALID;
  wire [31:0]axi_dwidth_converter_0_M_AXI_AWADDR;
  wire [1:0]axi_dwidth_converter_0_M_AXI_AWBURST;
  wire [3:0]axi_dwidth_converter_0_M_AXI_AWCACHE;
  wire [3:0]axi_dwidth_converter_0_M_AXI_AWLEN;
  wire [1:0]axi_dwidth_converter_0_M_AXI_AWLOCK;
  wire [2:0]axi_dwidth_converter_0_M_AXI_AWPROT;
  wire [3:0]axi_dwidth_converter_0_M_AXI_AWQOS;
  wire axi_dwidth_converter_0_M_AXI_AWREADY;
  wire [2:0]axi_dwidth_converter_0_M_AXI_AWSIZE;
  wire axi_dwidth_converter_0_M_AXI_AWVALID;
  wire axi_dwidth_converter_0_M_AXI_BREADY;
  wire [1:0]axi_dwidth_converter_0_M_AXI_BRESP;
  wire axi_dwidth_converter_0_M_AXI_BVALID;
  wire [63:0]axi_dwidth_converter_0_M_AXI_RDATA;
  wire axi_dwidth_converter_0_M_AXI_RLAST;
  wire axi_dwidth_converter_0_M_AXI_RREADY;
  wire [1:0]axi_dwidth_converter_0_M_AXI_RRESP;
  wire axi_dwidth_converter_0_M_AXI_RVALID;
  wire [63:0]axi_dwidth_converter_0_M_AXI_WDATA;
  wire axi_dwidth_converter_0_M_AXI_WLAST;
  wire axi_dwidth_converter_0_M_AXI_WREADY;
  wire [7:0]axi_dwidth_converter_0_M_AXI_WSTRB;
  wire axi_dwidth_converter_0_M_AXI_WVALID;
  wire [31:0]axi_protocol_converter_0_M_AXI_ARADDR;
  wire [1:0]axi_protocol_converter_0_M_AXI_ARBURST;
  wire [3:0]axi_protocol_converter_0_M_AXI_ARCACHE;
  wire [0:0]axi_protocol_converter_0_M_AXI_ARID;
  wire [3:0]axi_protocol_converter_0_M_AXI_ARLEN;
  wire [1:0]axi_protocol_converter_0_M_AXI_ARLOCK;
  wire [2:0]axi_protocol_converter_0_M_AXI_ARPROT;
  wire [3:0]axi_protocol_converter_0_M_AXI_ARQOS;
  wire axi_protocol_converter_0_M_AXI_ARREADY;
  wire [2:0]axi_protocol_converter_0_M_AXI_ARSIZE;
  wire axi_protocol_converter_0_M_AXI_ARVALID;
  wire [31:0]axi_protocol_converter_0_M_AXI_AWADDR;
  wire [1:0]axi_protocol_converter_0_M_AXI_AWBURST;
  wire [3:0]axi_protocol_converter_0_M_AXI_AWCACHE;
  wire [0:0]axi_protocol_converter_0_M_AXI_AWID;
  wire [3:0]axi_protocol_converter_0_M_AXI_AWLEN;
  wire [1:0]axi_protocol_converter_0_M_AXI_AWLOCK;
  wire [2:0]axi_protocol_converter_0_M_AXI_AWPROT;
  wire [3:0]axi_protocol_converter_0_M_AXI_AWQOS;
  wire axi_protocol_converter_0_M_AXI_AWREADY;
  wire [2:0]axi_protocol_converter_0_M_AXI_AWSIZE;
  wire axi_protocol_converter_0_M_AXI_AWVALID;
  wire [0:0]axi_protocol_converter_0_M_AXI_BID;
  wire axi_protocol_converter_0_M_AXI_BREADY;
  wire [1:0]axi_protocol_converter_0_M_AXI_BRESP;
  wire axi_protocol_converter_0_M_AXI_BVALID;
  wire [511:0]axi_protocol_converter_0_M_AXI_RDATA;
  wire [0:0]axi_protocol_converter_0_M_AXI_RID;
  wire axi_protocol_converter_0_M_AXI_RLAST;
  wire axi_protocol_converter_0_M_AXI_RREADY;
  wire [1:0]axi_protocol_converter_0_M_AXI_RRESP;
  wire axi_protocol_converter_0_M_AXI_RVALID;
  wire [511:0]axi_protocol_converter_0_M_AXI_WDATA;
  wire axi_protocol_converter_0_M_AXI_WLAST;
  wire axi_protocol_converter_0_M_AXI_WREADY;
  wire [63:0]axi_protocol_converter_0_M_AXI_WSTRB;
  wire axi_protocol_converter_0_M_AXI_WVALID;
  wire [143:0]cabac_modules_outCb;
(* MARK_DEBUG *)   wire [31:0]cabac_modules_outCont;
  wire [143:0]cabac_modules_outCr;
(* MARK_DEBUG *)   wire [143:0]cabac_modules_outY;
  wire cabac_modules_validCb;
(* MARK_DEBUG *)   wire cabac_modules_validCont;
  wire cabac_modules_validCr;
(* MARK_DEBUG *)   wire cabac_modules_validY;
(* MARK_DEBUG *)   wire [31:0]cabac_to_res_data;
(* MARK_DEBUG *)   wire [9:0]cabac_to_res_data_count;
(* MARK_DEBUG *)   wire cabac_to_res_wr_en;
  wire clk_wiz_0_clk_out1;
  wire [255:0]cu_other;
(* MARK_DEBUG *)   wire [6:0]cu_state;
(* MARK_DEBUG *)   wire [11:0]cu_x0;
(* MARK_DEBUG *)   wire [11:0]cu_y0;
(* MARK_DEBUG *)   wire dec_block_wo_cabac_cb_residual_read_inf_EMPTY;
(* MARK_DEBUG *)   wire [143:0]dec_block_wo_cabac_cb_residual_read_inf_RD_DATA;
(* MARK_DEBUG *)   wire dec_block_wo_cabac_cb_residual_read_inf_RD_EN;
(* MARK_DEBUG *)   wire dec_block_wo_cabac_cr_residual_read_inf_EMPTY;
(* MARK_DEBUG *)   wire [143:0]dec_block_wo_cabac_cr_residual_read_inf_RD_DATA;
(* MARK_DEBUG *)   wire dec_block_wo_cabac_cr_residual_read_inf_RD_EN;
  wire dec_block_wo_cabac_design_reset_n;
  wire [11:0]dec_block_wo_cabac_pic_height_out;
  wire [11:0]dec_block_wo_cabac_pic_width_out;
  wire dec_block_wo_cabac_read_en_out;
  wire [31:0]dec_block_wo_cabac_sao_poc_out;
  wire dec_block_wo_cabac_ui_clk;
  wire dec_block_wo_cabac_ui_clk_sync_rst;
(* MARK_DEBUG *)   wire dec_block_wo_cabac_y_residual_interface_EMPTY;
(* MARK_DEBUG *)   wire [143:0]dec_block_wo_cabac_y_residual_interface_RD_DATA;
(* MARK_DEBUG *)   wire dec_block_wo_cabac_y_residual_interface_RD_EN;
  wire [63:0]fifo_generator_0_dout;
(* MARK_DEBUG *)   wire fifo_generator_0_empty;
(* MARK_DEBUG *)   wire fifo_generator_0_prog_empty;
(* MARK_DEBUG *)   wire fifo_generator_0_prog_full;
(* MARK_DEBUG *)   wire input_hevc_almost_empty_out_test;
(* MARK_DEBUG *)   wire [13:0]input_hevc_data_count;
(* MARK_DEBUG *)   wire input_hevc_empty_out_test;
(* MARK_DEBUG *)   wire input_hevc_full_out_test;
  wire [13:0]mig_7series_0_DDR3_ADDR;
  wire [2:0]mig_7series_0_DDR3_BA;
  wire mig_7series_0_DDR3_CAS_N;
  wire [0:0]mig_7series_0_DDR3_CKE;
  wire [0:0]mig_7series_0_DDR3_CK_N;
  wire [0:0]mig_7series_0_DDR3_CK_P;
  wire [0:0]mig_7series_0_DDR3_CS_N;
  wire [7:0]mig_7series_0_DDR3_DM;
  wire [63:0]mig_7series_0_DDR3_DQ;
  wire [7:0]mig_7series_0_DDR3_DQS_N;
  wire [7:0]mig_7series_0_DDR3_DQS_P;
  wire [0:0]mig_7series_0_DDR3_ODT;
  wire mig_7series_0_DDR3_RAS_N;
  wire mig_7series_0_DDR3_RESET_N;
  wire mig_7series_0_DDR3_WE_N;
  wire [14:0]processing_system7_0_DDR_ADDR;
  wire [2:0]processing_system7_0_DDR_BA;
  wire processing_system7_0_DDR_CAS_N;
  wire processing_system7_0_DDR_CKE;
  wire processing_system7_0_DDR_CK_N;
  wire processing_system7_0_DDR_CK_P;
  wire processing_system7_0_DDR_CS_N;
  wire [3:0]processing_system7_0_DDR_DM;
  wire [31:0]processing_system7_0_DDR_DQ;
  wire [3:0]processing_system7_0_DDR_DQS_N;
  wire [3:0]processing_system7_0_DDR_DQS_P;
  wire processing_system7_0_DDR_ODT;
  wire processing_system7_0_DDR_RAS_N;
  wire processing_system7_0_DDR_RESET_N;
  wire processing_system7_0_DDR_WE_N;
  wire processing_system7_0_FCLK_CLK0;
  wire processing_system7_0_FCLK_RESET0_N;
  wire processing_system7_0_FIXED_IO_DDR_VRN;
  wire processing_system7_0_FIXED_IO_DDR_VRP;
  wire [53:0]processing_system7_0_FIXED_IO_MIO;
  wire processing_system7_0_FIXED_IO_PS_CLK;
  wire processing_system7_0_FIXED_IO_PS_PORB;
  wire processing_system7_0_FIXED_IO_PS_SRSTB;
  wire [31:0]processing_system7_0_M_AXI_GP1_ARADDR;
  wire [1:0]processing_system7_0_M_AXI_GP1_ARBURST;
  wire [3:0]processing_system7_0_M_AXI_GP1_ARCACHE;
  wire [11:0]processing_system7_0_M_AXI_GP1_ARID;
  wire [3:0]processing_system7_0_M_AXI_GP1_ARLEN;
  wire [1:0]processing_system7_0_M_AXI_GP1_ARLOCK;
  wire [2:0]processing_system7_0_M_AXI_GP1_ARPROT;
  wire [3:0]processing_system7_0_M_AXI_GP1_ARQOS;
  wire processing_system7_0_M_AXI_GP1_ARREADY;
  wire [2:0]processing_system7_0_M_AXI_GP1_ARSIZE;
  wire processing_system7_0_M_AXI_GP1_ARVALID;
  wire [31:0]processing_system7_0_M_AXI_GP1_AWADDR;
  wire [1:0]processing_system7_0_M_AXI_GP1_AWBURST;
  wire [3:0]processing_system7_0_M_AXI_GP1_AWCACHE;
  wire [11:0]processing_system7_0_M_AXI_GP1_AWID;
  wire [3:0]processing_system7_0_M_AXI_GP1_AWLEN;
  wire [1:0]processing_system7_0_M_AXI_GP1_AWLOCK;
  wire [2:0]processing_system7_0_M_AXI_GP1_AWPROT;
  wire [3:0]processing_system7_0_M_AXI_GP1_AWQOS;
  wire processing_system7_0_M_AXI_GP1_AWREADY;
  wire [2:0]processing_system7_0_M_AXI_GP1_AWSIZE;
  wire processing_system7_0_M_AXI_GP1_AWVALID;
  wire [11:0]processing_system7_0_M_AXI_GP1_BID;
  wire processing_system7_0_M_AXI_GP1_BREADY;
  wire [1:0]processing_system7_0_M_AXI_GP1_BRESP;
  wire processing_system7_0_M_AXI_GP1_BVALID;
  wire [31:0]processing_system7_0_M_AXI_GP1_RDATA;
  wire [11:0]processing_system7_0_M_AXI_GP1_RID;
  wire processing_system7_0_M_AXI_GP1_RLAST;
  wire processing_system7_0_M_AXI_GP1_RREADY;
  wire [1:0]processing_system7_0_M_AXI_GP1_RRESP;
  wire processing_system7_0_M_AXI_GP1_RVALID;
  wire [31:0]processing_system7_0_M_AXI_GP1_WDATA;
  wire [11:0]processing_system7_0_M_AXI_GP1_WID;
  wire processing_system7_0_M_AXI_GP1_WLAST;
  wire processing_system7_0_M_AXI_GP1_WREADY;
  wire [3:0]processing_system7_0_M_AXI_GP1_WSTRB;
  wire processing_system7_0_M_AXI_GP1_WVALID;
(* MARK_DEBUG *)   wire push_empty;
(* MARK_DEBUG *)   wire push_empty_out_test;
(* MARK_DEBUG *)   wire push_full_out_test;
(* MARK_DEBUG *)   wire push_read;
(* MARK_DEBUG *)   wire res_to_pred_cb_prog_full;
(* MARK_DEBUG *)   wire [10:0]res_to_pred_cb_rd_data_count;
  wire [31:0]res_to_pred_config_dout;
(* MARK_DEBUG *)   wire res_to_pred_config_prog_empty;
(* MARK_DEBUG *)   wire res_to_pred_config_prog_full;
(* MARK_DEBUG *)   wire res_to_pred_cr_prog_full;
(* MARK_DEBUG *)   wire [10:0]res_to_pred_cr_rd_data_count;
(* MARK_DEBUG *)   wire res_to_pred_y_prog_full;
(* MARK_DEBUG *)   wire [12:0]res_to_pred_y_rd_data_count;
  wire [0:0]rst_processing_system7_0_125M_bus_struct_reset;
  wire [0:0]rst_processing_system7_0_125M_interconnect_aresetn;
  wire [0:0]rst_processing_system7_0_125M_peripheral_aresetn;
(* MARK_DEBUG *)   wire [4:0]ru_state;
(* MARK_DEBUG *)   wire [31:0]state_out;
(* MARK_DEBUG *)   wire [15:0]trafo_POC;
(* MARK_DEBUG *)   wire trafo_clk_en;
(* MARK_DEBUG *)   wire [11:0]trafo_x0;
(* MARK_DEBUG *)   wire [11:0]trafo_y0;
(* MARK_DEBUG *)   wire video_tx_top_var_fps_0_DE;
  wire [23:0]video_tx_top_var_fps_0_Data_out;
  wire video_tx_top_var_fps_0_fifo_rd_en;
(* MARK_DEBUG *)   wire video_tx_top_var_fps_0_hsync;
  wire video_tx_top_var_fps_0_pclk;
(* MARK_DEBUG *)   wire video_tx_top_var_fps_0_vsync;
  wire [31:0]vio_0_probe_out0;

  assign DDR3_addr[13:0] = mig_7series_0_DDR3_ADDR;
  assign DDR3_ba[2:0] = mig_7series_0_DDR3_BA;
  assign DDR3_cas_n = mig_7series_0_DDR3_CAS_N;
  assign DDR3_ck_n[0] = mig_7series_0_DDR3_CK_N;
  assign DDR3_ck_p[0] = mig_7series_0_DDR3_CK_P;
  assign DDR3_cke[0] = mig_7series_0_DDR3_CKE;
  assign DDR3_cs_n[0] = mig_7series_0_DDR3_CS_N;
  assign DDR3_dm[7:0] = mig_7series_0_DDR3_DM;
  assign DDR3_odt[0] = mig_7series_0_DDR3_ODT;
  assign DDR3_ras_n = mig_7series_0_DDR3_RAS_N;
  assign DDR3_reset_n = mig_7series_0_DDR3_RESET_N;
  assign DDR3_we_n = mig_7series_0_DDR3_WE_N;
  assign DE = video_tx_top_var_fps_0_DE;
  assign Data_out[23:0] = video_tx_top_var_fps_0_Data_out;
  assign SYS_CLK_1_CLK_N = SYS_CLK_clk_n;
  assign SYS_CLK_1_CLK_P = SYS_CLK_clk_p;
  assign hsync = video_tx_top_var_fps_0_hsync;
  assign pclk = video_tx_top_var_fps_0_pclk;
  assign vsync = video_tx_top_var_fps_0_vsync;
GND GND
       (.G(GND_1));
VCC VCC
       (.P(VCC_1));
zynq_design_1_axi_displaybuffer_handle_0_1 axi_displaybuffer_handle_0
       (.axi_araddr(axi_displaybuffer_handle_0_axi_ARADDR),
        .axi_arburst(axi_displaybuffer_handle_0_axi_ARBURST),
        .axi_arid(axi_displaybuffer_handle_0_axi_ARID),
        .axi_arlen(axi_displaybuffer_handle_0_axi_ARLEN),
        .axi_arready(axi_displaybuffer_handle_0_axi_ARREADY),
        .axi_arsize(axi_displaybuffer_handle_0_axi_ARSIZE),
        .axi_arvalid(axi_displaybuffer_handle_0_axi_ARVALID),
        .axi_awaddr(axi_displaybuffer_handle_0_axi_AWADDR),
        .axi_awburst(axi_displaybuffer_handle_0_axi_AWBURST),
        .axi_awid(axi_displaybuffer_handle_0_axi_AWID),
        .axi_awlen(axi_displaybuffer_handle_0_axi_AWLEN),
        .axi_awready(axi_displaybuffer_handle_0_axi_AWREADY),
        .axi_awsize(axi_displaybuffer_handle_0_axi_AWSIZE),
        .axi_awvalid(axi_displaybuffer_handle_0_axi_AWVALID),
        .axi_bready(axi_displaybuffer_handle_0_axi_BREADY),
        .axi_bresp(axi_displaybuffer_handle_0_axi_BRESP),
        .axi_bvalid(axi_displaybuffer_handle_0_axi_BVALID),
        .axi_clk(VCC_1),
        .axi_rdata(axi_displaybuffer_handle_0_axi_RDATA),
        .axi_rlast(axi_displaybuffer_handle_0_axi_RLAST),
        .axi_rready(axi_displaybuffer_handle_0_axi_RREADY),
        .axi_rresp(axi_displaybuffer_handle_0_axi_RRESP),
        .axi_rvalid(axi_displaybuffer_handle_0_axi_RVALID),
        .axi_wdata(axi_displaybuffer_handle_0_axi_WDATA),
        .axi_wlast(axi_displaybuffer_handle_0_axi_WLAST),
        .axi_wready(axi_displaybuffer_handle_0_axi_WREADY),
        .axi_wstrb(axi_displaybuffer_handle_0_axi_WSTRB),
        .axi_wvalid(axi_displaybuffer_handle_0_axi_WVALID),
        .clk(dec_block_wo_cabac_ui_clk),
        .config_valid_in(VCC_1),
        .displaybuffer_fifo_almost_empty_in(GND_1),
        .displaybuffer_fifo_data_in(axi_displaybuffer_handle_0_fifo_read_rtl_1_RD_DATA),
        .displaybuffer_fifo_empty_in(axi_displaybuffer_handle_0_fifo_read_rtl_1_EMPTY),
        .displaybuffer_fifo_read_en_out(axi_displaybuffer_handle_0_fifo_read_rtl_1_RD_EN),
        .hdmi_fifo_almost_full(fifo_generator_0_prog_full),
        .hdmi_fifo_data_out(axi_displaybuffer_handle_0_hdmi_fifo_data_out),
        .hdmi_fifo_w_en(axi_displaybuffer_handle_0_hdmi_fifo_w_en),
        .pic_height_in(dec_block_wo_cabac_pic_height_out[10:0]),
        .pic_width_in(dec_block_wo_cabac_pic_width_out[10:0]),
        .poc_4bits_in(dec_block_wo_cabac_sao_poc_out[3:0]),
        .reset(dec_block_wo_cabac_ui_clk_sync_rst));
zynq_design_1_axi_dwidth_converter_0_0 axi_dwidth_converter_0
       (.m_axi_araddr(axi_dwidth_converter_0_M_AXI_ARADDR),
        .m_axi_arburst(axi_dwidth_converter_0_M_AXI_ARBURST),
        .m_axi_arcache(axi_dwidth_converter_0_M_AXI_ARCACHE),
        .m_axi_arlen(axi_dwidth_converter_0_M_AXI_ARLEN),
        .m_axi_arlock(axi_dwidth_converter_0_M_AXI_ARLOCK),
        .m_axi_arprot(axi_dwidth_converter_0_M_AXI_ARPROT),
        .m_axi_arqos(axi_dwidth_converter_0_M_AXI_ARQOS),
        .m_axi_arready(axi_dwidth_converter_0_M_AXI_ARREADY),
        .m_axi_arsize(axi_dwidth_converter_0_M_AXI_ARSIZE),
        .m_axi_arvalid(axi_dwidth_converter_0_M_AXI_ARVALID),
        .m_axi_awaddr(axi_dwidth_converter_0_M_AXI_AWADDR),
        .m_axi_awburst(axi_dwidth_converter_0_M_AXI_AWBURST),
        .m_axi_awcache(axi_dwidth_converter_0_M_AXI_AWCACHE),
        .m_axi_awlen(axi_dwidth_converter_0_M_AXI_AWLEN),
        .m_axi_awlock(axi_dwidth_converter_0_M_AXI_AWLOCK),
        .m_axi_awprot(axi_dwidth_converter_0_M_AXI_AWPROT),
        .m_axi_awqos(axi_dwidth_converter_0_M_AXI_AWQOS),
        .m_axi_awready(axi_dwidth_converter_0_M_AXI_AWREADY),
        .m_axi_awsize(axi_dwidth_converter_0_M_AXI_AWSIZE),
        .m_axi_awvalid(axi_dwidth_converter_0_M_AXI_AWVALID),
        .m_axi_bready(axi_dwidth_converter_0_M_AXI_BREADY),
        .m_axi_bresp(axi_dwidth_converter_0_M_AXI_BRESP),
        .m_axi_bvalid(axi_dwidth_converter_0_M_AXI_BVALID),
        .m_axi_rdata(axi_dwidth_converter_0_M_AXI_RDATA),
        .m_axi_rlast(axi_dwidth_converter_0_M_AXI_RLAST),
        .m_axi_rready(axi_dwidth_converter_0_M_AXI_RREADY),
        .m_axi_rresp(axi_dwidth_converter_0_M_AXI_RRESP),
        .m_axi_rvalid(axi_dwidth_converter_0_M_AXI_RVALID),
        .m_axi_wdata(axi_dwidth_converter_0_M_AXI_WDATA),
        .m_axi_wlast(axi_dwidth_converter_0_M_AXI_WLAST),
        .m_axi_wready(axi_dwidth_converter_0_M_AXI_WREADY),
        .m_axi_wstrb(axi_dwidth_converter_0_M_AXI_WSTRB),
        .m_axi_wvalid(axi_dwidth_converter_0_M_AXI_WVALID),
        .s_axi_aclk(dec_block_wo_cabac_ui_clk),
        .s_axi_araddr(axi_protocol_converter_0_M_AXI_ARADDR),
        .s_axi_arburst(axi_protocol_converter_0_M_AXI_ARBURST),
        .s_axi_arcache(axi_protocol_converter_0_M_AXI_ARCACHE),
        .s_axi_aresetn(dec_block_wo_cabac_design_reset_n),
        .s_axi_arid(axi_protocol_converter_0_M_AXI_ARID),
        .s_axi_arlen(axi_protocol_converter_0_M_AXI_ARLEN),
        .s_axi_arlock(axi_protocol_converter_0_M_AXI_ARLOCK),
        .s_axi_arprot(axi_protocol_converter_0_M_AXI_ARPROT),
        .s_axi_arqos(axi_protocol_converter_0_M_AXI_ARQOS),
        .s_axi_arready(axi_protocol_converter_0_M_AXI_ARREADY),
        .s_axi_arsize(axi_protocol_converter_0_M_AXI_ARSIZE),
        .s_axi_arvalid(axi_protocol_converter_0_M_AXI_ARVALID),
        .s_axi_awaddr(axi_protocol_converter_0_M_AXI_AWADDR),
        .s_axi_awburst(axi_protocol_converter_0_M_AXI_AWBURST),
        .s_axi_awcache(axi_protocol_converter_0_M_AXI_AWCACHE),
        .s_axi_awid(axi_protocol_converter_0_M_AXI_AWID),
        .s_axi_awlen(axi_protocol_converter_0_M_AXI_AWLEN),
        .s_axi_awlock(axi_protocol_converter_0_M_AXI_AWLOCK),
        .s_axi_awprot(axi_protocol_converter_0_M_AXI_AWPROT),
        .s_axi_awqos(axi_protocol_converter_0_M_AXI_AWQOS),
        .s_axi_awready(axi_protocol_converter_0_M_AXI_AWREADY),
        .s_axi_awsize(axi_protocol_converter_0_M_AXI_AWSIZE),
        .s_axi_awvalid(axi_protocol_converter_0_M_AXI_AWVALID),
        .s_axi_bid(axi_protocol_converter_0_M_AXI_BID),
        .s_axi_bready(axi_protocol_converter_0_M_AXI_BREADY),
        .s_axi_bresp(axi_protocol_converter_0_M_AXI_BRESP),
        .s_axi_bvalid(axi_protocol_converter_0_M_AXI_BVALID),
        .s_axi_rdata(axi_protocol_converter_0_M_AXI_RDATA),
        .s_axi_rid(axi_protocol_converter_0_M_AXI_RID),
        .s_axi_rlast(axi_protocol_converter_0_M_AXI_RLAST),
        .s_axi_rready(axi_protocol_converter_0_M_AXI_RREADY),
        .s_axi_rresp(axi_protocol_converter_0_M_AXI_RRESP),
        .s_axi_rvalid(axi_protocol_converter_0_M_AXI_RVALID),
        .s_axi_wdata(axi_protocol_converter_0_M_AXI_WDATA),
        .s_axi_wlast(axi_protocol_converter_0_M_AXI_WLAST),
        .s_axi_wready(axi_protocol_converter_0_M_AXI_WREADY),
        .s_axi_wstrb(axi_protocol_converter_0_M_AXI_WSTRB),
        .s_axi_wvalid(axi_protocol_converter_0_M_AXI_WVALID));
zynq_design_1_axi_protocol_converter_0_0 axi_protocol_converter_0
       (.aclk(dec_block_wo_cabac_ui_clk),
        .aresetn(dec_block_wo_cabac_design_reset_n),
        .m_axi_araddr(axi_protocol_converter_0_M_AXI_ARADDR),
        .m_axi_arburst(axi_protocol_converter_0_M_AXI_ARBURST),
        .m_axi_arcache(axi_protocol_converter_0_M_AXI_ARCACHE),
        .m_axi_arid(axi_protocol_converter_0_M_AXI_ARID),
        .m_axi_arlen(axi_protocol_converter_0_M_AXI_ARLEN),
        .m_axi_arlock(axi_protocol_converter_0_M_AXI_ARLOCK),
        .m_axi_arprot(axi_protocol_converter_0_M_AXI_ARPROT),
        .m_axi_arqos(axi_protocol_converter_0_M_AXI_ARQOS),
        .m_axi_arready(axi_protocol_converter_0_M_AXI_ARREADY),
        .m_axi_arsize(axi_protocol_converter_0_M_AXI_ARSIZE),
        .m_axi_arvalid(axi_protocol_converter_0_M_AXI_ARVALID),
        .m_axi_awaddr(axi_protocol_converter_0_M_AXI_AWADDR),
        .m_axi_awburst(axi_protocol_converter_0_M_AXI_AWBURST),
        .m_axi_awcache(axi_protocol_converter_0_M_AXI_AWCACHE),
        .m_axi_awid(axi_protocol_converter_0_M_AXI_AWID),
        .m_axi_awlen(axi_protocol_converter_0_M_AXI_AWLEN),
        .m_axi_awlock(axi_protocol_converter_0_M_AXI_AWLOCK),
        .m_axi_awprot(axi_protocol_converter_0_M_AXI_AWPROT),
        .m_axi_awqos(axi_protocol_converter_0_M_AXI_AWQOS),
        .m_axi_awready(axi_protocol_converter_0_M_AXI_AWREADY),
        .m_axi_awsize(axi_protocol_converter_0_M_AXI_AWSIZE),
        .m_axi_awvalid(axi_protocol_converter_0_M_AXI_AWVALID),
        .m_axi_bid(axi_protocol_converter_0_M_AXI_BID),
        .m_axi_bready(axi_protocol_converter_0_M_AXI_BREADY),
        .m_axi_bresp(axi_protocol_converter_0_M_AXI_BRESP),
        .m_axi_bvalid(axi_protocol_converter_0_M_AXI_BVALID),
        .m_axi_rdata(axi_protocol_converter_0_M_AXI_RDATA),
        .m_axi_rid(axi_protocol_converter_0_M_AXI_RID),
        .m_axi_rlast(axi_protocol_converter_0_M_AXI_RLAST),
        .m_axi_rready(axi_protocol_converter_0_M_AXI_RREADY),
        .m_axi_rresp(axi_protocol_converter_0_M_AXI_RRESP),
        .m_axi_rvalid(axi_protocol_converter_0_M_AXI_RVALID),
        .m_axi_wdata(axi_protocol_converter_0_M_AXI_WDATA),
        .m_axi_wlast(axi_protocol_converter_0_M_AXI_WLAST),
        .m_axi_wready(axi_protocol_converter_0_M_AXI_WREADY),
        .m_axi_wstrb(axi_protocol_converter_0_M_AXI_WSTRB),
        .m_axi_wvalid(axi_protocol_converter_0_M_AXI_WVALID),
        .s_axi_araddr(axi_displaybuffer_handle_0_axi_ARADDR),
        .s_axi_arburst(axi_displaybuffer_handle_0_axi_ARBURST),
        .s_axi_arcache({GND_1,GND_1,GND_1,GND_1}),
        .s_axi_arid(axi_displaybuffer_handle_0_axi_ARID[0]),
        .s_axi_arlen(axi_displaybuffer_handle_0_axi_ARLEN),
        .s_axi_arlock(GND_1),
        .s_axi_arprot({GND_1,GND_1,GND_1}),
        .s_axi_arqos({GND_1,GND_1,GND_1,GND_1}),
        .s_axi_arready(axi_displaybuffer_handle_0_axi_ARREADY),
        .s_axi_arregion({GND_1,GND_1,GND_1,GND_1}),
        .s_axi_arsize(axi_displaybuffer_handle_0_axi_ARSIZE),
        .s_axi_arvalid(axi_displaybuffer_handle_0_axi_ARVALID),
        .s_axi_awaddr(axi_displaybuffer_handle_0_axi_AWADDR),
        .s_axi_awburst(axi_displaybuffer_handle_0_axi_AWBURST),
        .s_axi_awcache({GND_1,GND_1,GND_1,GND_1}),
        .s_axi_awid(axi_displaybuffer_handle_0_axi_AWID[0]),
        .s_axi_awlen(axi_displaybuffer_handle_0_axi_AWLEN),
        .s_axi_awlock(GND_1),
        .s_axi_awprot({GND_1,GND_1,GND_1}),
        .s_axi_awqos({GND_1,GND_1,GND_1,GND_1}),
        .s_axi_awready(axi_displaybuffer_handle_0_axi_AWREADY),
        .s_axi_awregion({GND_1,GND_1,GND_1,GND_1}),
        .s_axi_awsize(axi_displaybuffer_handle_0_axi_AWSIZE),
        .s_axi_awvalid(axi_displaybuffer_handle_0_axi_AWVALID),
        .s_axi_bready(axi_displaybuffer_handle_0_axi_BREADY),
        .s_axi_bresp(axi_displaybuffer_handle_0_axi_BRESP),
        .s_axi_bvalid(axi_displaybuffer_handle_0_axi_BVALID),
        .s_axi_rdata(axi_displaybuffer_handle_0_axi_RDATA),
        .s_axi_rlast(axi_displaybuffer_handle_0_axi_RLAST),
        .s_axi_rready(axi_displaybuffer_handle_0_axi_RREADY),
        .s_axi_rresp(axi_displaybuffer_handle_0_axi_RRESP),
        .s_axi_rvalid(axi_displaybuffer_handle_0_axi_RVALID),
        .s_axi_wdata(axi_displaybuffer_handle_0_axi_WDATA),
        .s_axi_wlast(axi_displaybuffer_handle_0_axi_WLAST),
        .s_axi_wready(axi_displaybuffer_handle_0_axi_WREADY),
        .s_axi_wstrb(axi_displaybuffer_handle_0_axi_WSTRB),
        .s_axi_wvalid(axi_displaybuffer_handle_0_axi_WVALID));
cabac_modules_imp_AL9V63 cabac_modules
       (.ACLK(processing_system7_0_FCLK_CLK0),
        .B_test(B_test),
        .S00_ARESETN(rst_processing_system7_0_125M_peripheral_aresetn),
        .S00_AXI_araddr(processing_system7_0_M_AXI_GP1_ARADDR),
        .S00_AXI_arburst(processing_system7_0_M_AXI_GP1_ARBURST),
        .S00_AXI_arcache(processing_system7_0_M_AXI_GP1_ARCACHE),
        .S00_AXI_arid(processing_system7_0_M_AXI_GP1_ARID),
        .S00_AXI_arlen(processing_system7_0_M_AXI_GP1_ARLEN),
        .S00_AXI_arlock(processing_system7_0_M_AXI_GP1_ARLOCK),
        .S00_AXI_arprot(processing_system7_0_M_AXI_GP1_ARPROT),
        .S00_AXI_arqos(processing_system7_0_M_AXI_GP1_ARQOS),
        .S00_AXI_arready(processing_system7_0_M_AXI_GP1_ARREADY),
        .S00_AXI_arsize(processing_system7_0_M_AXI_GP1_ARSIZE),
        .S00_AXI_arvalid(processing_system7_0_M_AXI_GP1_ARVALID),
        .S00_AXI_awaddr(processing_system7_0_M_AXI_GP1_AWADDR),
        .S00_AXI_awburst(processing_system7_0_M_AXI_GP1_AWBURST),
        .S00_AXI_awcache(processing_system7_0_M_AXI_GP1_AWCACHE),
        .S00_AXI_awid(processing_system7_0_M_AXI_GP1_AWID),
        .S00_AXI_awlen(processing_system7_0_M_AXI_GP1_AWLEN),
        .S00_AXI_awlock(processing_system7_0_M_AXI_GP1_AWLOCK),
        .S00_AXI_awprot(processing_system7_0_M_AXI_GP1_AWPROT),
        .S00_AXI_awqos(processing_system7_0_M_AXI_GP1_AWQOS),
        .S00_AXI_awready(processing_system7_0_M_AXI_GP1_AWREADY),
        .S00_AXI_awsize(processing_system7_0_M_AXI_GP1_AWSIZE),
        .S00_AXI_awvalid(processing_system7_0_M_AXI_GP1_AWVALID),
        .S00_AXI_bid(processing_system7_0_M_AXI_GP1_BID),
        .S00_AXI_bready(processing_system7_0_M_AXI_GP1_BREADY),
        .S00_AXI_bresp(processing_system7_0_M_AXI_GP1_BRESP),
        .S00_AXI_bvalid(processing_system7_0_M_AXI_GP1_BVALID),
        .S00_AXI_rdata(processing_system7_0_M_AXI_GP1_RDATA),
        .S00_AXI_rid(processing_system7_0_M_AXI_GP1_RID),
        .S00_AXI_rlast(processing_system7_0_M_AXI_GP1_RLAST),
        .S00_AXI_rready(processing_system7_0_M_AXI_GP1_RREADY),
        .S00_AXI_rresp(processing_system7_0_M_AXI_GP1_RRESP),
        .S00_AXI_rvalid(processing_system7_0_M_AXI_GP1_RVALID),
        .S00_AXI_wdata(processing_system7_0_M_AXI_GP1_WDATA),
        .S00_AXI_wid(processing_system7_0_M_AXI_GP1_WID),
        .S00_AXI_wlast(processing_system7_0_M_AXI_GP1_WLAST),
        .S00_AXI_wready(processing_system7_0_M_AXI_GP1_WREADY),
        .S00_AXI_wstrb(processing_system7_0_M_AXI_GP1_WSTRB),
        .S00_AXI_wvalid(processing_system7_0_M_AXI_GP1_WVALID),
        .bus_struct_reset(rst_processing_system7_0_125M_bus_struct_reset),
        .cabac_to_res_data(cabac_to_res_data),
        .cabac_to_res_data_count(cabac_to_res_data_count),
        .cabac_to_res_wr_en(cabac_to_res_wr_en),
        .cu_other(cu_other),
        .cu_state(cu_state),
        .cu_x0(cu_x0),
        .cu_y0(cu_y0),
        .ext_reset_in(processing_system7_0_FCLK_RESET0_N),
        .fullCb(res_to_pred_cb_prog_full),
        .fullCont(res_to_pred_config_prog_full),
        .fullCr(res_to_pred_cr_prog_full),
        .fullY(res_to_pred_y_prog_full),
        .input_hevc_almost_empty_out_test(input_hevc_almost_empty_out_test),
        .input_hevc_data_count(input_hevc_data_count),
        .input_hevc_empty_out_test(input_hevc_empty_out_test),
        .input_hevc_full_out_test(input_hevc_full_out_test),
        .interconnect_aresetn(rst_processing_system7_0_125M_interconnect_aresetn),
        .outCb(cabac_modules_outCb),
        .outCont(cabac_modules_outCont),
        .outCr(cabac_modules_outCr),
        .outY(cabac_modules_outY),
        .push_empty(push_empty),
        .push_empty_out_test(push_empty_out_test),
        .push_full_out_test(push_full_out_test),
        .push_read(push_read),
        .ru_state(ru_state),
        .trafo_POC(trafo_POC),
        .trafo_clk_en(trafo_clk_en),
        .trafo_x0(trafo_x0),
        .trafo_y0(trafo_y0),
        .validCb(cabac_modules_validCb),
        .validCont(cabac_modules_validCont),
        .validCr(cabac_modules_validCr),
        .validY(cabac_modules_validY));
zynq_design_1_clk_wiz_0_0 clk_wiz_0
       (.clk_in1(dec_block_wo_cabac_ui_clk),
        .clk_out1(clk_wiz_0_clk_out1));
dec_block_wo_cabac_imp_T478Q4 dec_block_wo_cabac
       (.DDR3_addr(mig_7series_0_DDR3_ADDR),
        .DDR3_ba(mig_7series_0_DDR3_BA),
        .DDR3_cas_n(mig_7series_0_DDR3_CAS_N),
        .DDR3_ck_n(mig_7series_0_DDR3_CK_N),
        .DDR3_ck_p(mig_7series_0_DDR3_CK_P),
        .DDR3_cke(mig_7series_0_DDR3_CKE),
        .DDR3_cs_n(mig_7series_0_DDR3_CS_N),
        .DDR3_dm(mig_7series_0_DDR3_DM),
        .DDR3_dq(DDR3_dq[63:0]),
        .DDR3_dqs_n(DDR3_dqs_n[7:0]),
        .DDR3_dqs_p(DDR3_dqs_p[7:0]),
        .DDR3_odt(mig_7series_0_DDR3_ODT),
        .DDR3_ras_n(mig_7series_0_DDR3_RAS_N),
        .DDR3_reset_n(mig_7series_0_DDR3_RESET_N),
        .DDR3_we_n(mig_7series_0_DDR3_WE_N),
        .SYS_CLK_clk_n(SYS_CLK_1_CLK_N),
        .SYS_CLK_clk_p(SYS_CLK_1_CLK_P),
        .cb_rd_data_count_in(res_to_pred_cb_rd_data_count),
        .cb_residual_read_inf_empty(dec_block_wo_cabac_cb_residual_read_inf_EMPTY),
        .cb_residual_read_inf_rd_data(dec_block_wo_cabac_cb_residual_read_inf_RD_DATA),
        .cb_residual_read_inf_rd_en(dec_block_wo_cabac_cb_residual_read_inf_RD_EN),
        .cr_rd_data_count_in(res_to_pred_cr_rd_data_count),
        .cr_residual_read_inf_empty(dec_block_wo_cabac_cr_residual_read_inf_EMPTY),
        .cr_residual_read_inf_rd_data(dec_block_wo_cabac_cr_residual_read_inf_RD_DATA),
        .cr_residual_read_inf_rd_en(dec_block_wo_cabac_cr_residual_read_inf_RD_EN),
        .design_reset(dec_block_wo_cabac_ui_clk_sync_rst),
        .design_reset_n(dec_block_wo_cabac_design_reset_n),
        .display_fifo_read_inf_empty(axi_displaybuffer_handle_0_fifo_read_rtl_1_EMPTY),
        .display_fifo_read_inf_rd_data(axi_displaybuffer_handle_0_fifo_read_rtl_1_RD_DATA),
        .display_fifo_read_inf_rd_en(axi_displaybuffer_handle_0_fifo_read_rtl_1_RD_EN),
        .fifo_in(res_to_pred_config_dout),
        .input_fifo_is_empty(res_to_pred_config_prog_empty),
        .pic_height_out(dec_block_wo_cabac_pic_height_out),
        .pic_width_out(dec_block_wo_cabac_pic_width_out),
        .read_en_out(dec_block_wo_cabac_read_en_out),
        .sao_poc_out(dec_block_wo_cabac_sao_poc_out),
        .sys_rst(rst_processing_system7_0_125M_bus_struct_reset),
        .ui_clk(dec_block_wo_cabac_ui_clk),
        .wr_clk(processing_system7_0_FCLK_CLK0),
        .y_rd_data_count_in(res_to_pred_y_rd_data_count),
        .y_residual_interface_empty(dec_block_wo_cabac_y_residual_interface_EMPTY),
        .y_residual_interface_rd_data(dec_block_wo_cabac_y_residual_interface_RD_DATA),
        .y_residual_interface_rd_en(dec_block_wo_cabac_y_residual_interface_RD_EN));
dummy_pl_ps_interconnect_imp_K7QGHO dummy_pl_ps_interconnect
       (.ACLK(processing_system7_0_FCLK_CLK0),
        .ARESETN(rst_processing_system7_0_125M_interconnect_aresetn),
        .S00_ARESETN(rst_processing_system7_0_125M_peripheral_aresetn),
        .S00_AXI_araddr(S00_AXI_1_ARADDR),
        .S00_AXI_arburst(S00_AXI_1_ARBURST),
        .S00_AXI_arcache(S00_AXI_1_ARCACHE),
        .S00_AXI_arid(S00_AXI_1_ARID),
        .S00_AXI_arlen(S00_AXI_1_ARLEN),
        .S00_AXI_arlock(S00_AXI_1_ARLOCK),
        .S00_AXI_arprot(S00_AXI_1_ARPROT),
        .S00_AXI_arqos(S00_AXI_1_ARQOS),
        .S00_AXI_arready(S00_AXI_1_ARREADY),
        .S00_AXI_arsize(S00_AXI_1_ARSIZE),
        .S00_AXI_arvalid(S00_AXI_1_ARVALID),
        .S00_AXI_awaddr(S00_AXI_1_AWADDR),
        .S00_AXI_awburst(S00_AXI_1_AWBURST),
        .S00_AXI_awcache(S00_AXI_1_AWCACHE),
        .S00_AXI_awid(S00_AXI_1_AWID),
        .S00_AXI_awlen(S00_AXI_1_AWLEN),
        .S00_AXI_awlock(S00_AXI_1_AWLOCK),
        .S00_AXI_awprot(S00_AXI_1_AWPROT),
        .S00_AXI_awqos(S00_AXI_1_AWQOS),
        .S00_AXI_awready(S00_AXI_1_AWREADY),
        .S00_AXI_awsize(S00_AXI_1_AWSIZE),
        .S00_AXI_awvalid(S00_AXI_1_AWVALID),
        .S00_AXI_bid(S00_AXI_1_BID),
        .S00_AXI_bready(S00_AXI_1_BREADY),
        .S00_AXI_bresp(S00_AXI_1_BRESP),
        .S00_AXI_bvalid(S00_AXI_1_BVALID),
        .S00_AXI_rdata(S00_AXI_1_RDATA),
        .S00_AXI_rid(S00_AXI_1_RID),
        .S00_AXI_rlast(S00_AXI_1_RLAST),
        .S00_AXI_rready(S00_AXI_1_RREADY),
        .S00_AXI_rresp(S00_AXI_1_RRESP),
        .S00_AXI_rvalid(S00_AXI_1_RVALID),
        .S00_AXI_wdata(S00_AXI_1_WDATA),
        .S00_AXI_wid(S00_AXI_1_WID),
        .S00_AXI_wlast(S00_AXI_1_WLAST),
        .S00_AXI_wready(S00_AXI_1_WREADY),
        .S00_AXI_wstrb(S00_AXI_1_WSTRB),
        .S00_AXI_wvalid(S00_AXI_1_WVALID));
zynq_design_1_fifo_generator_0_0 fifo_generator_0
       (.din(axi_displaybuffer_handle_0_hdmi_fifo_data_out),
        .dout(fifo_generator_0_dout),
        .empty(fifo_generator_0_empty),
        .prog_empty(fifo_generator_0_prog_empty),
        .prog_full(fifo_generator_0_prog_full),
        .rd_clk(clk_wiz_0_clk_out1),
        .rd_en(video_tx_top_var_fps_0_fifo_rd_en),
        .rst(dec_block_wo_cabac_ui_clk_sync_rst),
        .wr_clk(dec_block_wo_cabac_ui_clk),
        .wr_en(axi_displaybuffer_handle_0_hdmi_fifo_w_en));
(* BMM_INFO_PROCESSOR = "ARM > zynq_design_1 dummy_pl_ps_interconnect/buffer_descriptor_memory" *) 
   (* KEEP_HIERARCHY = "yes" *) 
   zynq_design_1_processing_system7_0_0 processing_system7_0
       (.DDR_Addr(DDR_addr[14:0]),
        .DDR_BankAddr(DDR_ba[2:0]),
        .DDR_CAS_n(DDR_cas_n),
        .DDR_CKE(DDR_cke),
        .DDR_CS_n(DDR_cs_n),
        .DDR_Clk(DDR_ck_p),
        .DDR_Clk_n(DDR_ck_n),
        .DDR_DM(DDR_dm[3:0]),
        .DDR_DQ(DDR_dq[31:0]),
        .DDR_DQS(DDR_dqs_p[3:0]),
        .DDR_DQS_n(DDR_dqs_n[3:0]),
        .DDR_DRSTB(DDR_reset_n),
        .DDR_ODT(DDR_odt),
        .DDR_RAS_n(DDR_ras_n),
        .DDR_VRN(FIXED_IO_ddr_vrn),
        .DDR_VRP(FIXED_IO_ddr_vrp),
        .DDR_WEB(DDR_we_n),
        .FCLK_CLK0(processing_system7_0_FCLK_CLK0),
        .FCLK_RESET0_N(processing_system7_0_FCLK_RESET0_N),
        .MIO(FIXED_IO_mio[53:0]),
        .M_AXI_GP0_ACLK(processing_system7_0_FCLK_CLK0),
        .M_AXI_GP0_ARADDR(S00_AXI_1_ARADDR),
        .M_AXI_GP0_ARBURST(S00_AXI_1_ARBURST),
        .M_AXI_GP0_ARCACHE(S00_AXI_1_ARCACHE),
        .M_AXI_GP0_ARID(S00_AXI_1_ARID),
        .M_AXI_GP0_ARLEN(S00_AXI_1_ARLEN),
        .M_AXI_GP0_ARLOCK(S00_AXI_1_ARLOCK),
        .M_AXI_GP0_ARPROT(S00_AXI_1_ARPROT),
        .M_AXI_GP0_ARQOS(S00_AXI_1_ARQOS),
        .M_AXI_GP0_ARREADY(S00_AXI_1_ARREADY),
        .M_AXI_GP0_ARSIZE(S00_AXI_1_ARSIZE),
        .M_AXI_GP0_ARVALID(S00_AXI_1_ARVALID),
        .M_AXI_GP0_AWADDR(S00_AXI_1_AWADDR),
        .M_AXI_GP0_AWBURST(S00_AXI_1_AWBURST),
        .M_AXI_GP0_AWCACHE(S00_AXI_1_AWCACHE),
        .M_AXI_GP0_AWID(S00_AXI_1_AWID),
        .M_AXI_GP0_AWLEN(S00_AXI_1_AWLEN),
        .M_AXI_GP0_AWLOCK(S00_AXI_1_AWLOCK),
        .M_AXI_GP0_AWPROT(S00_AXI_1_AWPROT),
        .M_AXI_GP0_AWQOS(S00_AXI_1_AWQOS),
        .M_AXI_GP0_AWREADY(S00_AXI_1_AWREADY),
        .M_AXI_GP0_AWSIZE(S00_AXI_1_AWSIZE),
        .M_AXI_GP0_AWVALID(S00_AXI_1_AWVALID),
        .M_AXI_GP0_BID(S00_AXI_1_BID),
        .M_AXI_GP0_BREADY(S00_AXI_1_BREADY),
        .M_AXI_GP0_BRESP(S00_AXI_1_BRESP),
        .M_AXI_GP0_BVALID(S00_AXI_1_BVALID),
        .M_AXI_GP0_RDATA(S00_AXI_1_RDATA),
        .M_AXI_GP0_RID(S00_AXI_1_RID),
        .M_AXI_GP0_RLAST(S00_AXI_1_RLAST),
        .M_AXI_GP0_RREADY(S00_AXI_1_RREADY),
        .M_AXI_GP0_RRESP(S00_AXI_1_RRESP),
        .M_AXI_GP0_RVALID(S00_AXI_1_RVALID),
        .M_AXI_GP0_WDATA(S00_AXI_1_WDATA),
        .M_AXI_GP0_WID(S00_AXI_1_WID),
        .M_AXI_GP0_WLAST(S00_AXI_1_WLAST),
        .M_AXI_GP0_WREADY(S00_AXI_1_WREADY),
        .M_AXI_GP0_WSTRB(S00_AXI_1_WSTRB),
        .M_AXI_GP0_WVALID(S00_AXI_1_WVALID),
        .M_AXI_GP1_ACLK(processing_system7_0_FCLK_CLK0),
        .M_AXI_GP1_ARADDR(processing_system7_0_M_AXI_GP1_ARADDR),
        .M_AXI_GP1_ARBURST(processing_system7_0_M_AXI_GP1_ARBURST),
        .M_AXI_GP1_ARCACHE(processing_system7_0_M_AXI_GP1_ARCACHE),
        .M_AXI_GP1_ARID(processing_system7_0_M_AXI_GP1_ARID),
        .M_AXI_GP1_ARLEN(processing_system7_0_M_AXI_GP1_ARLEN),
        .M_AXI_GP1_ARLOCK(processing_system7_0_M_AXI_GP1_ARLOCK),
        .M_AXI_GP1_ARPROT(processing_system7_0_M_AXI_GP1_ARPROT),
        .M_AXI_GP1_ARQOS(processing_system7_0_M_AXI_GP1_ARQOS),
        .M_AXI_GP1_ARREADY(processing_system7_0_M_AXI_GP1_ARREADY),
        .M_AXI_GP1_ARSIZE(processing_system7_0_M_AXI_GP1_ARSIZE),
        .M_AXI_GP1_ARVALID(processing_system7_0_M_AXI_GP1_ARVALID),
        .M_AXI_GP1_AWADDR(processing_system7_0_M_AXI_GP1_AWADDR),
        .M_AXI_GP1_AWBURST(processing_system7_0_M_AXI_GP1_AWBURST),
        .M_AXI_GP1_AWCACHE(processing_system7_0_M_AXI_GP1_AWCACHE),
        .M_AXI_GP1_AWID(processing_system7_0_M_AXI_GP1_AWID),
        .M_AXI_GP1_AWLEN(processing_system7_0_M_AXI_GP1_AWLEN),
        .M_AXI_GP1_AWLOCK(processing_system7_0_M_AXI_GP1_AWLOCK),
        .M_AXI_GP1_AWPROT(processing_system7_0_M_AXI_GP1_AWPROT),
        .M_AXI_GP1_AWQOS(processing_system7_0_M_AXI_GP1_AWQOS),
        .M_AXI_GP1_AWREADY(processing_system7_0_M_AXI_GP1_AWREADY),
        .M_AXI_GP1_AWSIZE(processing_system7_0_M_AXI_GP1_AWSIZE),
        .M_AXI_GP1_AWVALID(processing_system7_0_M_AXI_GP1_AWVALID),
        .M_AXI_GP1_BID(processing_system7_0_M_AXI_GP1_BID),
        .M_AXI_GP1_BREADY(processing_system7_0_M_AXI_GP1_BREADY),
        .M_AXI_GP1_BRESP(processing_system7_0_M_AXI_GP1_BRESP),
        .M_AXI_GP1_BVALID(processing_system7_0_M_AXI_GP1_BVALID),
        .M_AXI_GP1_RDATA(processing_system7_0_M_AXI_GP1_RDATA),
        .M_AXI_GP1_RID(processing_system7_0_M_AXI_GP1_RID),
        .M_AXI_GP1_RLAST(processing_system7_0_M_AXI_GP1_RLAST),
        .M_AXI_GP1_RREADY(processing_system7_0_M_AXI_GP1_RREADY),
        .M_AXI_GP1_RRESP(processing_system7_0_M_AXI_GP1_RRESP),
        .M_AXI_GP1_RVALID(processing_system7_0_M_AXI_GP1_RVALID),
        .M_AXI_GP1_WDATA(processing_system7_0_M_AXI_GP1_WDATA),
        .M_AXI_GP1_WID(processing_system7_0_M_AXI_GP1_WID),
        .M_AXI_GP1_WLAST(processing_system7_0_M_AXI_GP1_WLAST),
        .M_AXI_GP1_WREADY(processing_system7_0_M_AXI_GP1_WREADY),
        .M_AXI_GP1_WSTRB(processing_system7_0_M_AXI_GP1_WSTRB),
        .M_AXI_GP1_WVALID(processing_system7_0_M_AXI_GP1_WVALID),
        .PS_CLK(FIXED_IO_ps_clk),
        .PS_PORB(FIXED_IO_ps_porb),
        .PS_SRSTB(FIXED_IO_ps_srstb),
        .S_AXI_HP0_ACLK(dec_block_wo_cabac_ui_clk),
        .S_AXI_HP0_ARADDR(axi_dwidth_converter_0_M_AXI_ARADDR),
        .S_AXI_HP0_ARBURST(axi_dwidth_converter_0_M_AXI_ARBURST),
        .S_AXI_HP0_ARCACHE(axi_dwidth_converter_0_M_AXI_ARCACHE),
        .S_AXI_HP0_ARID({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_HP0_ARLEN(axi_dwidth_converter_0_M_AXI_ARLEN),
        .S_AXI_HP0_ARLOCK(axi_dwidth_converter_0_M_AXI_ARLOCK),
        .S_AXI_HP0_ARPROT(axi_dwidth_converter_0_M_AXI_ARPROT),
        .S_AXI_HP0_ARQOS(axi_dwidth_converter_0_M_AXI_ARQOS),
        .S_AXI_HP0_ARREADY(axi_dwidth_converter_0_M_AXI_ARREADY),
        .S_AXI_HP0_ARSIZE(axi_dwidth_converter_0_M_AXI_ARSIZE),
        .S_AXI_HP0_ARVALID(axi_dwidth_converter_0_M_AXI_ARVALID),
        .S_AXI_HP0_AWADDR(axi_dwidth_converter_0_M_AXI_AWADDR),
        .S_AXI_HP0_AWBURST(axi_dwidth_converter_0_M_AXI_AWBURST),
        .S_AXI_HP0_AWCACHE(axi_dwidth_converter_0_M_AXI_AWCACHE),
        .S_AXI_HP0_AWID({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_HP0_AWLEN(axi_dwidth_converter_0_M_AXI_AWLEN),
        .S_AXI_HP0_AWLOCK(axi_dwidth_converter_0_M_AXI_AWLOCK),
        .S_AXI_HP0_AWPROT(axi_dwidth_converter_0_M_AXI_AWPROT),
        .S_AXI_HP0_AWQOS(axi_dwidth_converter_0_M_AXI_AWQOS),
        .S_AXI_HP0_AWREADY(axi_dwidth_converter_0_M_AXI_AWREADY),
        .S_AXI_HP0_AWSIZE(axi_dwidth_converter_0_M_AXI_AWSIZE),
        .S_AXI_HP0_AWVALID(axi_dwidth_converter_0_M_AXI_AWVALID),
        .S_AXI_HP0_BREADY(axi_dwidth_converter_0_M_AXI_BREADY),
        .S_AXI_HP0_BRESP(axi_dwidth_converter_0_M_AXI_BRESP),
        .S_AXI_HP0_BVALID(axi_dwidth_converter_0_M_AXI_BVALID),
        .S_AXI_HP0_RDATA(axi_dwidth_converter_0_M_AXI_RDATA),
        .S_AXI_HP0_RDISSUECAP1_EN(GND_1),
        .S_AXI_HP0_RLAST(axi_dwidth_converter_0_M_AXI_RLAST),
        .S_AXI_HP0_RREADY(axi_dwidth_converter_0_M_AXI_RREADY),
        .S_AXI_HP0_RRESP(axi_dwidth_converter_0_M_AXI_RRESP),
        .S_AXI_HP0_RVALID(axi_dwidth_converter_0_M_AXI_RVALID),
        .S_AXI_HP0_WDATA(axi_dwidth_converter_0_M_AXI_WDATA),
        .S_AXI_HP0_WID({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S_AXI_HP0_WLAST(axi_dwidth_converter_0_M_AXI_WLAST),
        .S_AXI_HP0_WREADY(axi_dwidth_converter_0_M_AXI_WREADY),
        .S_AXI_HP0_WRISSUECAP1_EN(GND_1),
        .S_AXI_HP0_WSTRB(axi_dwidth_converter_0_M_AXI_WSTRB),
        .S_AXI_HP0_WVALID(axi_dwidth_converter_0_M_AXI_WVALID));
zynq_design_1_res_to_pred_y_0 res_to_pred_cb
       (.din(cabac_modules_outCb),
        .dout(dec_block_wo_cabac_cb_residual_read_inf_RD_DATA),
        .empty(dec_block_wo_cabac_cb_residual_read_inf_EMPTY),
        .prog_full(res_to_pred_cb_prog_full),
        .rd_clk(dec_block_wo_cabac_ui_clk),
        .rd_data_count(res_to_pred_cb_rd_data_count),
        .rd_en(dec_block_wo_cabac_cb_residual_read_inf_RD_EN),
        .rst(dec_block_wo_cabac_ui_clk_sync_rst),
        .wr_clk(processing_system7_0_FCLK_CLK0),
        .wr_en(cabac_modules_validCb));
zynq_design_1_fifo_generator_0_1 res_to_pred_config
       (.din(cabac_modules_outCont),
        .dout(res_to_pred_config_dout),
        .prog_empty(res_to_pred_config_prog_empty),
        .prog_full(res_to_pred_config_prog_full),
        .rd_clk(dec_block_wo_cabac_ui_clk),
        .rd_en(dec_block_wo_cabac_read_en_out),
        .rst(dec_block_wo_cabac_ui_clk_sync_rst),
        .wr_clk(processing_system7_0_FCLK_CLK0),
        .wr_en(cabac_modules_validCont));
zynq_design_1_res_to_pred_cb_0 res_to_pred_cr
       (.din(cabac_modules_outCr),
        .dout(dec_block_wo_cabac_cr_residual_read_inf_RD_DATA),
        .empty(dec_block_wo_cabac_cr_residual_read_inf_EMPTY),
        .prog_full(res_to_pred_cr_prog_full),
        .rd_clk(dec_block_wo_cabac_ui_clk),
        .rd_data_count(res_to_pred_cr_rd_data_count),
        .rd_en(dec_block_wo_cabac_cr_residual_read_inf_RD_EN),
        .rst(dec_block_wo_cabac_ui_clk_sync_rst),
        .wr_clk(processing_system7_0_FCLK_CLK0),
        .wr_en(cabac_modules_validCr));
zynq_design_1_fifo_generator_0_2 res_to_pred_y
       (.din(cabac_modules_outY),
        .dout(dec_block_wo_cabac_y_residual_interface_RD_DATA),
        .empty(dec_block_wo_cabac_y_residual_interface_EMPTY),
        .prog_full(res_to_pred_y_prog_full),
        .rd_clk(dec_block_wo_cabac_ui_clk),
        .rd_data_count(res_to_pred_y_rd_data_count),
        .rd_en(dec_block_wo_cabac_y_residual_interface_RD_EN),
        .rst(dec_block_wo_cabac_ui_clk_sync_rst),
        .wr_clk(processing_system7_0_FCLK_CLK0),
        .wr_en(cabac_modules_validY));
zynq_design_1_video_tx_top_var_fps_0_0 video_tx_top_var_fps_0
       (.DE(video_tx_top_var_fps_0_DE),
        .Data_out(video_tx_top_var_fps_0_Data_out),
        .clk(clk_wiz_0_clk_out1),
        .fifo_data_in(fifo_generator_0_dout),
        .fifo_empty(fifo_generator_0_empty),
        .fifo_prog_empty(fifo_generator_0_prog_empty),
        .fifo_rd_en(video_tx_top_var_fps_0_fifo_rd_en),
        .fps_in(vio_0_probe_out0),
        .hsync(video_tx_top_var_fps_0_hsync),
        .pclk(video_tx_top_var_fps_0_pclk),
        .reset(dec_block_wo_cabac_ui_clk_sync_rst),
        .scl_pad(scl_pad),
        .sda_pad(sda_pad),
        .state_out(state_out),
        .vsync(video_tx_top_var_fps_0_vsync));
zynq_design_1_vio_0_0 vio_0
       (.clk(clk_wiz_0_clk_out1),
        .probe_out0(vio_0_probe_out0));
endmodule

module zynq_design_1_axi_mem_intercon_0
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arburst,
    M00_AXI_arcache,
    M00_AXI_arid,
    M00_AXI_arlen,
    M00_AXI_arlock,
    M00_AXI_arprot,
    M00_AXI_arready,
    M00_AXI_arsize,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awburst,
    M00_AXI_awcache,
    M00_AXI_awid,
    M00_AXI_awlen,
    M00_AXI_awlock,
    M00_AXI_awprot,
    M00_AXI_awready,
    M00_AXI_awsize,
    M00_AXI_awvalid,
    M00_AXI_bid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rid,
    M00_AXI_rlast,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wlast,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arburst,
    S00_AXI_arcache,
    S00_AXI_arid,
    S00_AXI_arlen,
    S00_AXI_arlock,
    S00_AXI_arprot,
    S00_AXI_arqos,
    S00_AXI_arready,
    S00_AXI_arsize,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awburst,
    S00_AXI_awcache,
    S00_AXI_awid,
    S00_AXI_awlen,
    S00_AXI_awlock,
    S00_AXI_awprot,
    S00_AXI_awqos,
    S00_AXI_awready,
    S00_AXI_awsize,
    S00_AXI_awvalid,
    S00_AXI_bid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rid,
    S00_AXI_rlast,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wid,
    S00_AXI_wlast,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid);
  input ACLK;
  input [0:0]ARESETN;
  input M00_ACLK;
  input [0:0]M00_ARESETN;
  output [15:0]M00_AXI_araddr;
  output [1:0]M00_AXI_arburst;
  output [3:0]M00_AXI_arcache;
  output [11:0]M00_AXI_arid;
  output [7:0]M00_AXI_arlen;
  output [0:0]M00_AXI_arlock;
  output [2:0]M00_AXI_arprot;
  input M00_AXI_arready;
  output [2:0]M00_AXI_arsize;
  output M00_AXI_arvalid;
  output [15:0]M00_AXI_awaddr;
  output [1:0]M00_AXI_awburst;
  output [3:0]M00_AXI_awcache;
  output [11:0]M00_AXI_awid;
  output [7:0]M00_AXI_awlen;
  output [0:0]M00_AXI_awlock;
  output [2:0]M00_AXI_awprot;
  input M00_AXI_awready;
  output [2:0]M00_AXI_awsize;
  output M00_AXI_awvalid;
  input [11:0]M00_AXI_bid;
  output M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  input [11:0]M00_AXI_rid;
  input M00_AXI_rlast;
  output M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  output M00_AXI_wlast;
  input M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output M00_AXI_wvalid;
  input S00_ACLK;
  input [0:0]S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [1:0]S00_AXI_arburst;
  input [3:0]S00_AXI_arcache;
  input [11:0]S00_AXI_arid;
  input [3:0]S00_AXI_arlen;
  input [1:0]S00_AXI_arlock;
  input [2:0]S00_AXI_arprot;
  input [3:0]S00_AXI_arqos;
  output S00_AXI_arready;
  input [2:0]S00_AXI_arsize;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [1:0]S00_AXI_awburst;
  input [3:0]S00_AXI_awcache;
  input [11:0]S00_AXI_awid;
  input [3:0]S00_AXI_awlen;
  input [1:0]S00_AXI_awlock;
  input [2:0]S00_AXI_awprot;
  input [3:0]S00_AXI_awqos;
  output S00_AXI_awready;
  input [2:0]S00_AXI_awsize;
  input S00_AXI_awvalid;
  output [11:0]S00_AXI_bid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  output [11:0]S00_AXI_rid;
  output S00_AXI_rlast;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  input [11:0]S00_AXI_wid;
  input S00_AXI_wlast;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;

  wire S00_ACLK_1;
  wire [0:0]S00_ARESETN_1;
  wire axi_mem_intercon_ACLK_net;
  wire [0:0]axi_mem_intercon_ARESETN_net;
  wire [31:0]axi_mem_intercon_to_s00_couplers_ARADDR;
  wire [1:0]axi_mem_intercon_to_s00_couplers_ARBURST;
  wire [3:0]axi_mem_intercon_to_s00_couplers_ARCACHE;
  wire [11:0]axi_mem_intercon_to_s00_couplers_ARID;
  wire [3:0]axi_mem_intercon_to_s00_couplers_ARLEN;
  wire [1:0]axi_mem_intercon_to_s00_couplers_ARLOCK;
  wire [2:0]axi_mem_intercon_to_s00_couplers_ARPROT;
  wire [3:0]axi_mem_intercon_to_s00_couplers_ARQOS;
  wire axi_mem_intercon_to_s00_couplers_ARREADY;
  wire [2:0]axi_mem_intercon_to_s00_couplers_ARSIZE;
  wire axi_mem_intercon_to_s00_couplers_ARVALID;
  wire [31:0]axi_mem_intercon_to_s00_couplers_AWADDR;
  wire [1:0]axi_mem_intercon_to_s00_couplers_AWBURST;
  wire [3:0]axi_mem_intercon_to_s00_couplers_AWCACHE;
  wire [11:0]axi_mem_intercon_to_s00_couplers_AWID;
  wire [3:0]axi_mem_intercon_to_s00_couplers_AWLEN;
  wire [1:0]axi_mem_intercon_to_s00_couplers_AWLOCK;
  wire [2:0]axi_mem_intercon_to_s00_couplers_AWPROT;
  wire [3:0]axi_mem_intercon_to_s00_couplers_AWQOS;
  wire axi_mem_intercon_to_s00_couplers_AWREADY;
  wire [2:0]axi_mem_intercon_to_s00_couplers_AWSIZE;
  wire axi_mem_intercon_to_s00_couplers_AWVALID;
  wire [11:0]axi_mem_intercon_to_s00_couplers_BID;
  wire axi_mem_intercon_to_s00_couplers_BREADY;
  wire [1:0]axi_mem_intercon_to_s00_couplers_BRESP;
  wire axi_mem_intercon_to_s00_couplers_BVALID;
  wire [31:0]axi_mem_intercon_to_s00_couplers_RDATA;
  wire [11:0]axi_mem_intercon_to_s00_couplers_RID;
  wire axi_mem_intercon_to_s00_couplers_RLAST;
  wire axi_mem_intercon_to_s00_couplers_RREADY;
  wire [1:0]axi_mem_intercon_to_s00_couplers_RRESP;
  wire axi_mem_intercon_to_s00_couplers_RVALID;
  wire [31:0]axi_mem_intercon_to_s00_couplers_WDATA;
  wire [11:0]axi_mem_intercon_to_s00_couplers_WID;
  wire axi_mem_intercon_to_s00_couplers_WLAST;
  wire axi_mem_intercon_to_s00_couplers_WREADY;
  wire [3:0]axi_mem_intercon_to_s00_couplers_WSTRB;
  wire axi_mem_intercon_to_s00_couplers_WVALID;
  wire [15:0]s00_couplers_to_axi_mem_intercon_ARADDR;
  wire [1:0]s00_couplers_to_axi_mem_intercon_ARBURST;
  wire [3:0]s00_couplers_to_axi_mem_intercon_ARCACHE;
  wire [11:0]s00_couplers_to_axi_mem_intercon_ARID;
  wire [7:0]s00_couplers_to_axi_mem_intercon_ARLEN;
  wire [0:0]s00_couplers_to_axi_mem_intercon_ARLOCK;
  wire [2:0]s00_couplers_to_axi_mem_intercon_ARPROT;
  wire s00_couplers_to_axi_mem_intercon_ARREADY;
  wire [2:0]s00_couplers_to_axi_mem_intercon_ARSIZE;
  wire s00_couplers_to_axi_mem_intercon_ARVALID;
  wire [15:0]s00_couplers_to_axi_mem_intercon_AWADDR;
  wire [1:0]s00_couplers_to_axi_mem_intercon_AWBURST;
  wire [3:0]s00_couplers_to_axi_mem_intercon_AWCACHE;
  wire [11:0]s00_couplers_to_axi_mem_intercon_AWID;
  wire [7:0]s00_couplers_to_axi_mem_intercon_AWLEN;
  wire [0:0]s00_couplers_to_axi_mem_intercon_AWLOCK;
  wire [2:0]s00_couplers_to_axi_mem_intercon_AWPROT;
  wire s00_couplers_to_axi_mem_intercon_AWREADY;
  wire [2:0]s00_couplers_to_axi_mem_intercon_AWSIZE;
  wire s00_couplers_to_axi_mem_intercon_AWVALID;
  wire [11:0]s00_couplers_to_axi_mem_intercon_BID;
  wire s00_couplers_to_axi_mem_intercon_BREADY;
  wire [1:0]s00_couplers_to_axi_mem_intercon_BRESP;
  wire s00_couplers_to_axi_mem_intercon_BVALID;
  wire [31:0]s00_couplers_to_axi_mem_intercon_RDATA;
  wire [11:0]s00_couplers_to_axi_mem_intercon_RID;
  wire s00_couplers_to_axi_mem_intercon_RLAST;
  wire s00_couplers_to_axi_mem_intercon_RREADY;
  wire [1:0]s00_couplers_to_axi_mem_intercon_RRESP;
  wire s00_couplers_to_axi_mem_intercon_RVALID;
  wire [31:0]s00_couplers_to_axi_mem_intercon_WDATA;
  wire s00_couplers_to_axi_mem_intercon_WLAST;
  wire s00_couplers_to_axi_mem_intercon_WREADY;
  wire [3:0]s00_couplers_to_axi_mem_intercon_WSTRB;
  wire s00_couplers_to_axi_mem_intercon_WVALID;

  assign M00_AXI_araddr[15:0] = s00_couplers_to_axi_mem_intercon_ARADDR;
  assign M00_AXI_arburst[1:0] = s00_couplers_to_axi_mem_intercon_ARBURST;
  assign M00_AXI_arcache[3:0] = s00_couplers_to_axi_mem_intercon_ARCACHE;
  assign M00_AXI_arid[11:0] = s00_couplers_to_axi_mem_intercon_ARID;
  assign M00_AXI_arlen[7:0] = s00_couplers_to_axi_mem_intercon_ARLEN;
  assign M00_AXI_arlock[0] = s00_couplers_to_axi_mem_intercon_ARLOCK;
  assign M00_AXI_arprot[2:0] = s00_couplers_to_axi_mem_intercon_ARPROT;
  assign M00_AXI_arsize[2:0] = s00_couplers_to_axi_mem_intercon_ARSIZE;
  assign M00_AXI_arvalid = s00_couplers_to_axi_mem_intercon_ARVALID;
  assign M00_AXI_awaddr[15:0] = s00_couplers_to_axi_mem_intercon_AWADDR;
  assign M00_AXI_awburst[1:0] = s00_couplers_to_axi_mem_intercon_AWBURST;
  assign M00_AXI_awcache[3:0] = s00_couplers_to_axi_mem_intercon_AWCACHE;
  assign M00_AXI_awid[11:0] = s00_couplers_to_axi_mem_intercon_AWID;
  assign M00_AXI_awlen[7:0] = s00_couplers_to_axi_mem_intercon_AWLEN;
  assign M00_AXI_awlock[0] = s00_couplers_to_axi_mem_intercon_AWLOCK;
  assign M00_AXI_awprot[2:0] = s00_couplers_to_axi_mem_intercon_AWPROT;
  assign M00_AXI_awsize[2:0] = s00_couplers_to_axi_mem_intercon_AWSIZE;
  assign M00_AXI_awvalid = s00_couplers_to_axi_mem_intercon_AWVALID;
  assign M00_AXI_bready = s00_couplers_to_axi_mem_intercon_BREADY;
  assign M00_AXI_rready = s00_couplers_to_axi_mem_intercon_RREADY;
  assign M00_AXI_wdata[31:0] = s00_couplers_to_axi_mem_intercon_WDATA;
  assign M00_AXI_wlast = s00_couplers_to_axi_mem_intercon_WLAST;
  assign M00_AXI_wstrb[3:0] = s00_couplers_to_axi_mem_intercon_WSTRB;
  assign M00_AXI_wvalid = s00_couplers_to_axi_mem_intercon_WVALID;
  assign S00_ACLK_1 = S00_ACLK;
  assign S00_ARESETN_1 = S00_ARESETN[0];
  assign S00_AXI_arready = axi_mem_intercon_to_s00_couplers_ARREADY;
  assign S00_AXI_awready = axi_mem_intercon_to_s00_couplers_AWREADY;
  assign S00_AXI_bid[11:0] = axi_mem_intercon_to_s00_couplers_BID;
  assign S00_AXI_bresp[1:0] = axi_mem_intercon_to_s00_couplers_BRESP;
  assign S00_AXI_bvalid = axi_mem_intercon_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = axi_mem_intercon_to_s00_couplers_RDATA;
  assign S00_AXI_rid[11:0] = axi_mem_intercon_to_s00_couplers_RID;
  assign S00_AXI_rlast = axi_mem_intercon_to_s00_couplers_RLAST;
  assign S00_AXI_rresp[1:0] = axi_mem_intercon_to_s00_couplers_RRESP;
  assign S00_AXI_rvalid = axi_mem_intercon_to_s00_couplers_RVALID;
  assign S00_AXI_wready = axi_mem_intercon_to_s00_couplers_WREADY;
  assign axi_mem_intercon_ACLK_net = M00_ACLK;
  assign axi_mem_intercon_ARESETN_net = M00_ARESETN[0];
  assign axi_mem_intercon_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign axi_mem_intercon_to_s00_couplers_ARBURST = S00_AXI_arburst[1:0];
  assign axi_mem_intercon_to_s00_couplers_ARCACHE = S00_AXI_arcache[3:0];
  assign axi_mem_intercon_to_s00_couplers_ARID = S00_AXI_arid[11:0];
  assign axi_mem_intercon_to_s00_couplers_ARLEN = S00_AXI_arlen[3:0];
  assign axi_mem_intercon_to_s00_couplers_ARLOCK = S00_AXI_arlock[1:0];
  assign axi_mem_intercon_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign axi_mem_intercon_to_s00_couplers_ARQOS = S00_AXI_arqos[3:0];
  assign axi_mem_intercon_to_s00_couplers_ARSIZE = S00_AXI_arsize[2:0];
  assign axi_mem_intercon_to_s00_couplers_ARVALID = S00_AXI_arvalid;
  assign axi_mem_intercon_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign axi_mem_intercon_to_s00_couplers_AWBURST = S00_AXI_awburst[1:0];
  assign axi_mem_intercon_to_s00_couplers_AWCACHE = S00_AXI_awcache[3:0];
  assign axi_mem_intercon_to_s00_couplers_AWID = S00_AXI_awid[11:0];
  assign axi_mem_intercon_to_s00_couplers_AWLEN = S00_AXI_awlen[3:0];
  assign axi_mem_intercon_to_s00_couplers_AWLOCK = S00_AXI_awlock[1:0];
  assign axi_mem_intercon_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign axi_mem_intercon_to_s00_couplers_AWQOS = S00_AXI_awqos[3:0];
  assign axi_mem_intercon_to_s00_couplers_AWSIZE = S00_AXI_awsize[2:0];
  assign axi_mem_intercon_to_s00_couplers_AWVALID = S00_AXI_awvalid;
  assign axi_mem_intercon_to_s00_couplers_BREADY = S00_AXI_bready;
  assign axi_mem_intercon_to_s00_couplers_RREADY = S00_AXI_rready;
  assign axi_mem_intercon_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign axi_mem_intercon_to_s00_couplers_WID = S00_AXI_wid[11:0];
  assign axi_mem_intercon_to_s00_couplers_WLAST = S00_AXI_wlast;
  assign axi_mem_intercon_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign axi_mem_intercon_to_s00_couplers_WVALID = S00_AXI_wvalid;
  assign s00_couplers_to_axi_mem_intercon_ARREADY = M00_AXI_arready;
  assign s00_couplers_to_axi_mem_intercon_AWREADY = M00_AXI_awready;
  assign s00_couplers_to_axi_mem_intercon_BID = M00_AXI_bid[11:0];
  assign s00_couplers_to_axi_mem_intercon_BRESP = M00_AXI_bresp[1:0];
  assign s00_couplers_to_axi_mem_intercon_BVALID = M00_AXI_bvalid;
  assign s00_couplers_to_axi_mem_intercon_RDATA = M00_AXI_rdata[31:0];
  assign s00_couplers_to_axi_mem_intercon_RID = M00_AXI_rid[11:0];
  assign s00_couplers_to_axi_mem_intercon_RLAST = M00_AXI_rlast;
  assign s00_couplers_to_axi_mem_intercon_RRESP = M00_AXI_rresp[1:0];
  assign s00_couplers_to_axi_mem_intercon_RVALID = M00_AXI_rvalid;
  assign s00_couplers_to_axi_mem_intercon_WREADY = M00_AXI_wready;
s00_couplers_imp_Q87KJ3 s00_couplers
       (.M_ACLK(axi_mem_intercon_ACLK_net),
        .M_ARESETN(axi_mem_intercon_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_axi_mem_intercon_ARADDR),
        .M_AXI_arburst(s00_couplers_to_axi_mem_intercon_ARBURST),
        .M_AXI_arcache(s00_couplers_to_axi_mem_intercon_ARCACHE),
        .M_AXI_arid(s00_couplers_to_axi_mem_intercon_ARID),
        .M_AXI_arlen(s00_couplers_to_axi_mem_intercon_ARLEN),
        .M_AXI_arlock(s00_couplers_to_axi_mem_intercon_ARLOCK),
        .M_AXI_arprot(s00_couplers_to_axi_mem_intercon_ARPROT),
        .M_AXI_arready(s00_couplers_to_axi_mem_intercon_ARREADY),
        .M_AXI_arsize(s00_couplers_to_axi_mem_intercon_ARSIZE),
        .M_AXI_arvalid(s00_couplers_to_axi_mem_intercon_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_axi_mem_intercon_AWADDR),
        .M_AXI_awburst(s00_couplers_to_axi_mem_intercon_AWBURST),
        .M_AXI_awcache(s00_couplers_to_axi_mem_intercon_AWCACHE),
        .M_AXI_awid(s00_couplers_to_axi_mem_intercon_AWID),
        .M_AXI_awlen(s00_couplers_to_axi_mem_intercon_AWLEN),
        .M_AXI_awlock(s00_couplers_to_axi_mem_intercon_AWLOCK),
        .M_AXI_awprot(s00_couplers_to_axi_mem_intercon_AWPROT),
        .M_AXI_awready(s00_couplers_to_axi_mem_intercon_AWREADY),
        .M_AXI_awsize(s00_couplers_to_axi_mem_intercon_AWSIZE),
        .M_AXI_awvalid(s00_couplers_to_axi_mem_intercon_AWVALID),
        .M_AXI_bid(s00_couplers_to_axi_mem_intercon_BID),
        .M_AXI_bready(s00_couplers_to_axi_mem_intercon_BREADY),
        .M_AXI_bresp(s00_couplers_to_axi_mem_intercon_BRESP),
        .M_AXI_bvalid(s00_couplers_to_axi_mem_intercon_BVALID),
        .M_AXI_rdata(s00_couplers_to_axi_mem_intercon_RDATA),
        .M_AXI_rid(s00_couplers_to_axi_mem_intercon_RID),
        .M_AXI_rlast(s00_couplers_to_axi_mem_intercon_RLAST),
        .M_AXI_rready(s00_couplers_to_axi_mem_intercon_RREADY),
        .M_AXI_rresp(s00_couplers_to_axi_mem_intercon_RRESP),
        .M_AXI_rvalid(s00_couplers_to_axi_mem_intercon_RVALID),
        .M_AXI_wdata(s00_couplers_to_axi_mem_intercon_WDATA),
        .M_AXI_wlast(s00_couplers_to_axi_mem_intercon_WLAST),
        .M_AXI_wready(s00_couplers_to_axi_mem_intercon_WREADY),
        .M_AXI_wstrb(s00_couplers_to_axi_mem_intercon_WSTRB),
        .M_AXI_wvalid(s00_couplers_to_axi_mem_intercon_WVALID),
        .S_ACLK(S00_ACLK_1),
        .S_ARESETN(S00_ARESETN_1),
        .S_AXI_araddr(axi_mem_intercon_to_s00_couplers_ARADDR),
        .S_AXI_arburst(axi_mem_intercon_to_s00_couplers_ARBURST),
        .S_AXI_arcache(axi_mem_intercon_to_s00_couplers_ARCACHE),
        .S_AXI_arid(axi_mem_intercon_to_s00_couplers_ARID),
        .S_AXI_arlen(axi_mem_intercon_to_s00_couplers_ARLEN),
        .S_AXI_arlock(axi_mem_intercon_to_s00_couplers_ARLOCK),
        .S_AXI_arprot(axi_mem_intercon_to_s00_couplers_ARPROT),
        .S_AXI_arqos(axi_mem_intercon_to_s00_couplers_ARQOS),
        .S_AXI_arready(axi_mem_intercon_to_s00_couplers_ARREADY),
        .S_AXI_arsize(axi_mem_intercon_to_s00_couplers_ARSIZE),
        .S_AXI_arvalid(axi_mem_intercon_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(axi_mem_intercon_to_s00_couplers_AWADDR),
        .S_AXI_awburst(axi_mem_intercon_to_s00_couplers_AWBURST),
        .S_AXI_awcache(axi_mem_intercon_to_s00_couplers_AWCACHE),
        .S_AXI_awid(axi_mem_intercon_to_s00_couplers_AWID),
        .S_AXI_awlen(axi_mem_intercon_to_s00_couplers_AWLEN),
        .S_AXI_awlock(axi_mem_intercon_to_s00_couplers_AWLOCK),
        .S_AXI_awprot(axi_mem_intercon_to_s00_couplers_AWPROT),
        .S_AXI_awqos(axi_mem_intercon_to_s00_couplers_AWQOS),
        .S_AXI_awready(axi_mem_intercon_to_s00_couplers_AWREADY),
        .S_AXI_awsize(axi_mem_intercon_to_s00_couplers_AWSIZE),
        .S_AXI_awvalid(axi_mem_intercon_to_s00_couplers_AWVALID),
        .S_AXI_bid(axi_mem_intercon_to_s00_couplers_BID),
        .S_AXI_bready(axi_mem_intercon_to_s00_couplers_BREADY),
        .S_AXI_bresp(axi_mem_intercon_to_s00_couplers_BRESP),
        .S_AXI_bvalid(axi_mem_intercon_to_s00_couplers_BVALID),
        .S_AXI_rdata(axi_mem_intercon_to_s00_couplers_RDATA),
        .S_AXI_rid(axi_mem_intercon_to_s00_couplers_RID),
        .S_AXI_rlast(axi_mem_intercon_to_s00_couplers_RLAST),
        .S_AXI_rready(axi_mem_intercon_to_s00_couplers_RREADY),
        .S_AXI_rresp(axi_mem_intercon_to_s00_couplers_RRESP),
        .S_AXI_rvalid(axi_mem_intercon_to_s00_couplers_RVALID),
        .S_AXI_wdata(axi_mem_intercon_to_s00_couplers_WDATA),
        .S_AXI_wid(axi_mem_intercon_to_s00_couplers_WID),
        .S_AXI_wlast(axi_mem_intercon_to_s00_couplers_WLAST),
        .S_AXI_wready(axi_mem_intercon_to_s00_couplers_WREADY),
        .S_AXI_wstrb(axi_mem_intercon_to_s00_couplers_WSTRB),
        .S_AXI_wvalid(axi_mem_intercon_to_s00_couplers_WVALID));
endmodule

module zynq_design_1_axi_mem_intercon_1_0
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arburst,
    M00_AXI_arcache,
    M00_AXI_arid,
    M00_AXI_arlen,
    M00_AXI_arlock,
    M00_AXI_arprot,
    M00_AXI_arready,
    M00_AXI_arsize,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awburst,
    M00_AXI_awcache,
    M00_AXI_awid,
    M00_AXI_awlen,
    M00_AXI_awlock,
    M00_AXI_awprot,
    M00_AXI_awready,
    M00_AXI_awsize,
    M00_AXI_awvalid,
    M00_AXI_bid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rid,
    M00_AXI_rlast,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wlast,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arburst,
    S00_AXI_arcache,
    S00_AXI_arid,
    S00_AXI_arlen,
    S00_AXI_arlock,
    S00_AXI_arprot,
    S00_AXI_arqos,
    S00_AXI_arready,
    S00_AXI_arsize,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awburst,
    S00_AXI_awcache,
    S00_AXI_awid,
    S00_AXI_awlen,
    S00_AXI_awlock,
    S00_AXI_awprot,
    S00_AXI_awqos,
    S00_AXI_awready,
    S00_AXI_awsize,
    S00_AXI_awvalid,
    S00_AXI_bid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rid,
    S00_AXI_rlast,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wid,
    S00_AXI_wlast,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid);
  input ACLK;
  input [0:0]ARESETN;
  input M00_ACLK;
  input [0:0]M00_ARESETN;
  output [15:0]M00_AXI_araddr;
  output [1:0]M00_AXI_arburst;
  output [3:0]M00_AXI_arcache;
  output [11:0]M00_AXI_arid;
  output [7:0]M00_AXI_arlen;
  output M00_AXI_arlock;
  output [2:0]M00_AXI_arprot;
  input M00_AXI_arready;
  output [2:0]M00_AXI_arsize;
  output M00_AXI_arvalid;
  output [15:0]M00_AXI_awaddr;
  output [1:0]M00_AXI_awburst;
  output [3:0]M00_AXI_awcache;
  output [11:0]M00_AXI_awid;
  output [7:0]M00_AXI_awlen;
  output M00_AXI_awlock;
  output [2:0]M00_AXI_awprot;
  input M00_AXI_awready;
  output [2:0]M00_AXI_awsize;
  output M00_AXI_awvalid;
  input [11:0]M00_AXI_bid;
  output M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  input [11:0]M00_AXI_rid;
  input M00_AXI_rlast;
  output M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  output M00_AXI_wlast;
  input M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output M00_AXI_wvalid;
  input S00_ACLK;
  input [0:0]S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [1:0]S00_AXI_arburst;
  input [3:0]S00_AXI_arcache;
  input [11:0]S00_AXI_arid;
  input [3:0]S00_AXI_arlen;
  input [1:0]S00_AXI_arlock;
  input [2:0]S00_AXI_arprot;
  input [3:0]S00_AXI_arqos;
  output S00_AXI_arready;
  input [2:0]S00_AXI_arsize;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [1:0]S00_AXI_awburst;
  input [3:0]S00_AXI_awcache;
  input [11:0]S00_AXI_awid;
  input [3:0]S00_AXI_awlen;
  input [1:0]S00_AXI_awlock;
  input [2:0]S00_AXI_awprot;
  input [3:0]S00_AXI_awqos;
  output S00_AXI_awready;
  input [2:0]S00_AXI_awsize;
  input S00_AXI_awvalid;
  output [11:0]S00_AXI_bid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  output [11:0]S00_AXI_rid;
  output S00_AXI_rlast;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  input [11:0]S00_AXI_wid;
  input S00_AXI_wlast;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;

  wire S00_ACLK_1;
  wire [0:0]S00_ARESETN_1;
  wire axi_mem_intercon_1_ACLK_net;
  wire [0:0]axi_mem_intercon_1_ARESETN_net;
  wire [31:0]axi_mem_intercon_1_to_s00_couplers_ARADDR;
  wire [1:0]axi_mem_intercon_1_to_s00_couplers_ARBURST;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_ARCACHE;
  wire [11:0]axi_mem_intercon_1_to_s00_couplers_ARID;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_ARLEN;
  wire [1:0]axi_mem_intercon_1_to_s00_couplers_ARLOCK;
  wire [2:0]axi_mem_intercon_1_to_s00_couplers_ARPROT;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_ARQOS;
  wire axi_mem_intercon_1_to_s00_couplers_ARREADY;
  wire [2:0]axi_mem_intercon_1_to_s00_couplers_ARSIZE;
  wire axi_mem_intercon_1_to_s00_couplers_ARVALID;
  wire [31:0]axi_mem_intercon_1_to_s00_couplers_AWADDR;
  wire [1:0]axi_mem_intercon_1_to_s00_couplers_AWBURST;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_AWCACHE;
  wire [11:0]axi_mem_intercon_1_to_s00_couplers_AWID;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_AWLEN;
  wire [1:0]axi_mem_intercon_1_to_s00_couplers_AWLOCK;
  wire [2:0]axi_mem_intercon_1_to_s00_couplers_AWPROT;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_AWQOS;
  wire axi_mem_intercon_1_to_s00_couplers_AWREADY;
  wire [2:0]axi_mem_intercon_1_to_s00_couplers_AWSIZE;
  wire axi_mem_intercon_1_to_s00_couplers_AWVALID;
  wire [11:0]axi_mem_intercon_1_to_s00_couplers_BID;
  wire axi_mem_intercon_1_to_s00_couplers_BREADY;
  wire [1:0]axi_mem_intercon_1_to_s00_couplers_BRESP;
  wire axi_mem_intercon_1_to_s00_couplers_BVALID;
  wire [31:0]axi_mem_intercon_1_to_s00_couplers_RDATA;
  wire [11:0]axi_mem_intercon_1_to_s00_couplers_RID;
  wire axi_mem_intercon_1_to_s00_couplers_RLAST;
  wire axi_mem_intercon_1_to_s00_couplers_RREADY;
  wire [1:0]axi_mem_intercon_1_to_s00_couplers_RRESP;
  wire axi_mem_intercon_1_to_s00_couplers_RVALID;
  wire [31:0]axi_mem_intercon_1_to_s00_couplers_WDATA;
  wire [11:0]axi_mem_intercon_1_to_s00_couplers_WID;
  wire axi_mem_intercon_1_to_s00_couplers_WLAST;
  wire axi_mem_intercon_1_to_s00_couplers_WREADY;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_WSTRB;
  wire axi_mem_intercon_1_to_s00_couplers_WVALID;
  wire [15:0]s00_couplers_to_axi_mem_intercon_1_ARADDR;
  wire [1:0]s00_couplers_to_axi_mem_intercon_1_ARBURST;
  wire [3:0]s00_couplers_to_axi_mem_intercon_1_ARCACHE;
  wire [11:0]s00_couplers_to_axi_mem_intercon_1_ARID;
  wire [7:0]s00_couplers_to_axi_mem_intercon_1_ARLEN;
  wire s00_couplers_to_axi_mem_intercon_1_ARLOCK;
  wire [2:0]s00_couplers_to_axi_mem_intercon_1_ARPROT;
  wire s00_couplers_to_axi_mem_intercon_1_ARREADY;
  wire [2:0]s00_couplers_to_axi_mem_intercon_1_ARSIZE;
  wire s00_couplers_to_axi_mem_intercon_1_ARVALID;
  wire [15:0]s00_couplers_to_axi_mem_intercon_1_AWADDR;
  wire [1:0]s00_couplers_to_axi_mem_intercon_1_AWBURST;
  wire [3:0]s00_couplers_to_axi_mem_intercon_1_AWCACHE;
  wire [11:0]s00_couplers_to_axi_mem_intercon_1_AWID;
  wire [7:0]s00_couplers_to_axi_mem_intercon_1_AWLEN;
  wire s00_couplers_to_axi_mem_intercon_1_AWLOCK;
  wire [2:0]s00_couplers_to_axi_mem_intercon_1_AWPROT;
  wire s00_couplers_to_axi_mem_intercon_1_AWREADY;
  wire [2:0]s00_couplers_to_axi_mem_intercon_1_AWSIZE;
  wire s00_couplers_to_axi_mem_intercon_1_AWVALID;
  wire [11:0]s00_couplers_to_axi_mem_intercon_1_BID;
  wire s00_couplers_to_axi_mem_intercon_1_BREADY;
  wire [1:0]s00_couplers_to_axi_mem_intercon_1_BRESP;
  wire s00_couplers_to_axi_mem_intercon_1_BVALID;
  wire [31:0]s00_couplers_to_axi_mem_intercon_1_RDATA;
  wire [11:0]s00_couplers_to_axi_mem_intercon_1_RID;
  wire s00_couplers_to_axi_mem_intercon_1_RLAST;
  wire s00_couplers_to_axi_mem_intercon_1_RREADY;
  wire [1:0]s00_couplers_to_axi_mem_intercon_1_RRESP;
  wire s00_couplers_to_axi_mem_intercon_1_RVALID;
  wire [31:0]s00_couplers_to_axi_mem_intercon_1_WDATA;
  wire s00_couplers_to_axi_mem_intercon_1_WLAST;
  wire s00_couplers_to_axi_mem_intercon_1_WREADY;
  wire [3:0]s00_couplers_to_axi_mem_intercon_1_WSTRB;
  wire s00_couplers_to_axi_mem_intercon_1_WVALID;

  assign M00_AXI_araddr[15:0] = s00_couplers_to_axi_mem_intercon_1_ARADDR;
  assign M00_AXI_arburst[1:0] = s00_couplers_to_axi_mem_intercon_1_ARBURST;
  assign M00_AXI_arcache[3:0] = s00_couplers_to_axi_mem_intercon_1_ARCACHE;
  assign M00_AXI_arid[11:0] = s00_couplers_to_axi_mem_intercon_1_ARID;
  assign M00_AXI_arlen[7:0] = s00_couplers_to_axi_mem_intercon_1_ARLEN;
  assign M00_AXI_arlock = s00_couplers_to_axi_mem_intercon_1_ARLOCK;
  assign M00_AXI_arprot[2:0] = s00_couplers_to_axi_mem_intercon_1_ARPROT;
  assign M00_AXI_arsize[2:0] = s00_couplers_to_axi_mem_intercon_1_ARSIZE;
  assign M00_AXI_arvalid = s00_couplers_to_axi_mem_intercon_1_ARVALID;
  assign M00_AXI_awaddr[15:0] = s00_couplers_to_axi_mem_intercon_1_AWADDR;
  assign M00_AXI_awburst[1:0] = s00_couplers_to_axi_mem_intercon_1_AWBURST;
  assign M00_AXI_awcache[3:0] = s00_couplers_to_axi_mem_intercon_1_AWCACHE;
  assign M00_AXI_awid[11:0] = s00_couplers_to_axi_mem_intercon_1_AWID;
  assign M00_AXI_awlen[7:0] = s00_couplers_to_axi_mem_intercon_1_AWLEN;
  assign M00_AXI_awlock = s00_couplers_to_axi_mem_intercon_1_AWLOCK;
  assign M00_AXI_awprot[2:0] = s00_couplers_to_axi_mem_intercon_1_AWPROT;
  assign M00_AXI_awsize[2:0] = s00_couplers_to_axi_mem_intercon_1_AWSIZE;
  assign M00_AXI_awvalid = s00_couplers_to_axi_mem_intercon_1_AWVALID;
  assign M00_AXI_bready = s00_couplers_to_axi_mem_intercon_1_BREADY;
  assign M00_AXI_rready = s00_couplers_to_axi_mem_intercon_1_RREADY;
  assign M00_AXI_wdata[31:0] = s00_couplers_to_axi_mem_intercon_1_WDATA;
  assign M00_AXI_wlast = s00_couplers_to_axi_mem_intercon_1_WLAST;
  assign M00_AXI_wstrb[3:0] = s00_couplers_to_axi_mem_intercon_1_WSTRB;
  assign M00_AXI_wvalid = s00_couplers_to_axi_mem_intercon_1_WVALID;
  assign S00_ACLK_1 = S00_ACLK;
  assign S00_ARESETN_1 = S00_ARESETN[0];
  assign S00_AXI_arready = axi_mem_intercon_1_to_s00_couplers_ARREADY;
  assign S00_AXI_awready = axi_mem_intercon_1_to_s00_couplers_AWREADY;
  assign S00_AXI_bid[11:0] = axi_mem_intercon_1_to_s00_couplers_BID;
  assign S00_AXI_bresp[1:0] = axi_mem_intercon_1_to_s00_couplers_BRESP;
  assign S00_AXI_bvalid = axi_mem_intercon_1_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = axi_mem_intercon_1_to_s00_couplers_RDATA;
  assign S00_AXI_rid[11:0] = axi_mem_intercon_1_to_s00_couplers_RID;
  assign S00_AXI_rlast = axi_mem_intercon_1_to_s00_couplers_RLAST;
  assign S00_AXI_rresp[1:0] = axi_mem_intercon_1_to_s00_couplers_RRESP;
  assign S00_AXI_rvalid = axi_mem_intercon_1_to_s00_couplers_RVALID;
  assign S00_AXI_wready = axi_mem_intercon_1_to_s00_couplers_WREADY;
  assign axi_mem_intercon_1_ACLK_net = M00_ACLK;
  assign axi_mem_intercon_1_ARESETN_net = M00_ARESETN[0];
  assign axi_mem_intercon_1_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARBURST = S00_AXI_arburst[1:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARCACHE = S00_AXI_arcache[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARID = S00_AXI_arid[11:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARLEN = S00_AXI_arlen[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARLOCK = S00_AXI_arlock[1:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARQOS = S00_AXI_arqos[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARSIZE = S00_AXI_arsize[2:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARVALID = S00_AXI_arvalid;
  assign axi_mem_intercon_1_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWBURST = S00_AXI_awburst[1:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWCACHE = S00_AXI_awcache[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWID = S00_AXI_awid[11:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWLEN = S00_AXI_awlen[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWLOCK = S00_AXI_awlock[1:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWQOS = S00_AXI_awqos[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWSIZE = S00_AXI_awsize[2:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWVALID = S00_AXI_awvalid;
  assign axi_mem_intercon_1_to_s00_couplers_BREADY = S00_AXI_bready;
  assign axi_mem_intercon_1_to_s00_couplers_RREADY = S00_AXI_rready;
  assign axi_mem_intercon_1_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign axi_mem_intercon_1_to_s00_couplers_WID = S00_AXI_wid[11:0];
  assign axi_mem_intercon_1_to_s00_couplers_WLAST = S00_AXI_wlast;
  assign axi_mem_intercon_1_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_WVALID = S00_AXI_wvalid;
  assign s00_couplers_to_axi_mem_intercon_1_ARREADY = M00_AXI_arready;
  assign s00_couplers_to_axi_mem_intercon_1_AWREADY = M00_AXI_awready;
  assign s00_couplers_to_axi_mem_intercon_1_BID = M00_AXI_bid[11:0];
  assign s00_couplers_to_axi_mem_intercon_1_BRESP = M00_AXI_bresp[1:0];
  assign s00_couplers_to_axi_mem_intercon_1_BVALID = M00_AXI_bvalid;
  assign s00_couplers_to_axi_mem_intercon_1_RDATA = M00_AXI_rdata[31:0];
  assign s00_couplers_to_axi_mem_intercon_1_RID = M00_AXI_rid[11:0];
  assign s00_couplers_to_axi_mem_intercon_1_RLAST = M00_AXI_rlast;
  assign s00_couplers_to_axi_mem_intercon_1_RRESP = M00_AXI_rresp[1:0];
  assign s00_couplers_to_axi_mem_intercon_1_RVALID = M00_AXI_rvalid;
  assign s00_couplers_to_axi_mem_intercon_1_WREADY = M00_AXI_wready;
s00_couplers_imp_17357AK s00_couplers
       (.M_ACLK(axi_mem_intercon_1_ACLK_net),
        .M_ARESETN(axi_mem_intercon_1_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_axi_mem_intercon_1_ARADDR),
        .M_AXI_arburst(s00_couplers_to_axi_mem_intercon_1_ARBURST),
        .M_AXI_arcache(s00_couplers_to_axi_mem_intercon_1_ARCACHE),
        .M_AXI_arid(s00_couplers_to_axi_mem_intercon_1_ARID),
        .M_AXI_arlen(s00_couplers_to_axi_mem_intercon_1_ARLEN),
        .M_AXI_arlock(s00_couplers_to_axi_mem_intercon_1_ARLOCK),
        .M_AXI_arprot(s00_couplers_to_axi_mem_intercon_1_ARPROT),
        .M_AXI_arready(s00_couplers_to_axi_mem_intercon_1_ARREADY),
        .M_AXI_arsize(s00_couplers_to_axi_mem_intercon_1_ARSIZE),
        .M_AXI_arvalid(s00_couplers_to_axi_mem_intercon_1_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_axi_mem_intercon_1_AWADDR),
        .M_AXI_awburst(s00_couplers_to_axi_mem_intercon_1_AWBURST),
        .M_AXI_awcache(s00_couplers_to_axi_mem_intercon_1_AWCACHE),
        .M_AXI_awid(s00_couplers_to_axi_mem_intercon_1_AWID),
        .M_AXI_awlen(s00_couplers_to_axi_mem_intercon_1_AWLEN),
        .M_AXI_awlock(s00_couplers_to_axi_mem_intercon_1_AWLOCK),
        .M_AXI_awprot(s00_couplers_to_axi_mem_intercon_1_AWPROT),
        .M_AXI_awready(s00_couplers_to_axi_mem_intercon_1_AWREADY),
        .M_AXI_awsize(s00_couplers_to_axi_mem_intercon_1_AWSIZE),
        .M_AXI_awvalid(s00_couplers_to_axi_mem_intercon_1_AWVALID),
        .M_AXI_bid(s00_couplers_to_axi_mem_intercon_1_BID),
        .M_AXI_bready(s00_couplers_to_axi_mem_intercon_1_BREADY),
        .M_AXI_bresp(s00_couplers_to_axi_mem_intercon_1_BRESP),
        .M_AXI_bvalid(s00_couplers_to_axi_mem_intercon_1_BVALID),
        .M_AXI_rdata(s00_couplers_to_axi_mem_intercon_1_RDATA),
        .M_AXI_rid(s00_couplers_to_axi_mem_intercon_1_RID),
        .M_AXI_rlast(s00_couplers_to_axi_mem_intercon_1_RLAST),
        .M_AXI_rready(s00_couplers_to_axi_mem_intercon_1_RREADY),
        .M_AXI_rresp(s00_couplers_to_axi_mem_intercon_1_RRESP),
        .M_AXI_rvalid(s00_couplers_to_axi_mem_intercon_1_RVALID),
        .M_AXI_wdata(s00_couplers_to_axi_mem_intercon_1_WDATA),
        .M_AXI_wlast(s00_couplers_to_axi_mem_intercon_1_WLAST),
        .M_AXI_wready(s00_couplers_to_axi_mem_intercon_1_WREADY),
        .M_AXI_wstrb(s00_couplers_to_axi_mem_intercon_1_WSTRB),
        .M_AXI_wvalid(s00_couplers_to_axi_mem_intercon_1_WVALID),
        .S_ACLK(S00_ACLK_1),
        .S_ARESETN(S00_ARESETN_1),
        .S_AXI_araddr(axi_mem_intercon_1_to_s00_couplers_ARADDR),
        .S_AXI_arburst(axi_mem_intercon_1_to_s00_couplers_ARBURST),
        .S_AXI_arcache(axi_mem_intercon_1_to_s00_couplers_ARCACHE),
        .S_AXI_arid(axi_mem_intercon_1_to_s00_couplers_ARID),
        .S_AXI_arlen(axi_mem_intercon_1_to_s00_couplers_ARLEN),
        .S_AXI_arlock(axi_mem_intercon_1_to_s00_couplers_ARLOCK),
        .S_AXI_arprot(axi_mem_intercon_1_to_s00_couplers_ARPROT),
        .S_AXI_arqos(axi_mem_intercon_1_to_s00_couplers_ARQOS),
        .S_AXI_arready(axi_mem_intercon_1_to_s00_couplers_ARREADY),
        .S_AXI_arsize(axi_mem_intercon_1_to_s00_couplers_ARSIZE),
        .S_AXI_arvalid(axi_mem_intercon_1_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(axi_mem_intercon_1_to_s00_couplers_AWADDR),
        .S_AXI_awburst(axi_mem_intercon_1_to_s00_couplers_AWBURST),
        .S_AXI_awcache(axi_mem_intercon_1_to_s00_couplers_AWCACHE),
        .S_AXI_awid(axi_mem_intercon_1_to_s00_couplers_AWID),
        .S_AXI_awlen(axi_mem_intercon_1_to_s00_couplers_AWLEN),
        .S_AXI_awlock(axi_mem_intercon_1_to_s00_couplers_AWLOCK),
        .S_AXI_awprot(axi_mem_intercon_1_to_s00_couplers_AWPROT),
        .S_AXI_awqos(axi_mem_intercon_1_to_s00_couplers_AWQOS),
        .S_AXI_awready(axi_mem_intercon_1_to_s00_couplers_AWREADY),
        .S_AXI_awsize(axi_mem_intercon_1_to_s00_couplers_AWSIZE),
        .S_AXI_awvalid(axi_mem_intercon_1_to_s00_couplers_AWVALID),
        .S_AXI_bid(axi_mem_intercon_1_to_s00_couplers_BID),
        .S_AXI_bready(axi_mem_intercon_1_to_s00_couplers_BREADY),
        .S_AXI_bresp(axi_mem_intercon_1_to_s00_couplers_BRESP),
        .S_AXI_bvalid(axi_mem_intercon_1_to_s00_couplers_BVALID),
        .S_AXI_rdata(axi_mem_intercon_1_to_s00_couplers_RDATA),
        .S_AXI_rid(axi_mem_intercon_1_to_s00_couplers_RID),
        .S_AXI_rlast(axi_mem_intercon_1_to_s00_couplers_RLAST),
        .S_AXI_rready(axi_mem_intercon_1_to_s00_couplers_RREADY),
        .S_AXI_rresp(axi_mem_intercon_1_to_s00_couplers_RRESP),
        .S_AXI_rvalid(axi_mem_intercon_1_to_s00_couplers_RVALID),
        .S_AXI_wdata(axi_mem_intercon_1_to_s00_couplers_WDATA),
        .S_AXI_wid(axi_mem_intercon_1_to_s00_couplers_WID),
        .S_AXI_wlast(axi_mem_intercon_1_to_s00_couplers_WLAST),
        .S_AXI_wready(axi_mem_intercon_1_to_s00_couplers_WREADY),
        .S_AXI_wstrb(axi_mem_intercon_1_to_s00_couplers_WSTRB),
        .S_AXI_wvalid(axi_mem_intercon_1_to_s00_couplers_WVALID));
endmodule
