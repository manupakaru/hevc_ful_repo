
// ************************************************************************************************
//
// PROJECT      :   <project name where applicable>
// PRODUCT      :   <product name where applicable>
// FILE         :   <File Name/Module Name>
// AUTHOR       :   <Author's name>
// DESCRIPTION  :   <at least a short description about the module/file>
//                  Where does this file get inputs and send outputs?
//                  What does the guts of this file accomplish, and how does it do it?
//                  What module(s) does this file instantiate?
//
// ************************************************************************************************
//
// REVISIONS:
//
//	Date			Developer	Description
//	----			---------	-----------
//  29 Feb 2012		user123     bug fix - <jira issue id>
//  31 Oct 2007		userxyz		added xyz functionality - <jira issue id>
//  ...              ...         ...
//  ...              ...         ...
//
//**************************************************************************************************

`timescale 1ns / 1ps

module axi_displaybuffer_handle(

		clk,
		reset,

        displaybuffer_fifo_read_en_out,
        displaybuffer_fifo_empty_in,
        displaybuffer_fifo_data_in,

        log2_ctu_size_in,
        poc_4bits_in,
        stream_id,
        read_stream_id,
        pic_width_in,
        pic_height_in,
        config_valid_in,

        //axi write interface
		axi_clk,
      axi_awaddr,
      axi_awvalid,
      axi_awready,
      axi_awlen,
		axi_awid,
		axi_awburst,
		axi_awsize,
		

		  axi_wid,
        axi_wdata,
        axi_wlast,
        axi_wvalid,
        axi_wready,
        axi_wstrb,

        axi_bresp,
        axi_bvalid,
        axi_bready,

        //axi read interface
        axi_araddr,
        axi_arvalid,
        axi_arready,
        axi_arlen,
        axi_arburst,
		  axi_arsize,
		  axi_arid,
		
        axi_rdata,
        axi_rresp,
        axi_rlast,
        axi_rready,
        axi_rvalid,

        //hdmi interface
        hdmi_fifo_almost_full,
        hdmi_fifo_data_out,
        hdmi_fifo_w_en

    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
`include "pred_def.v"
`include "inter_axi_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
   // parameter                       MAX_LOG2CTBSIZE_WIDTH = 3;


    parameter                       PROFILE         = 4;
    localparam                       DISPBUF_BASE_ADDR    = 805306368 + 16;
    parameter                        DISP_BUF_AXI_WIDTH = 64;
    //parameter                       DISPBUF_BASE_ADDR    = 8053;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

    parameter      AXI_BRESP_WIDTH             = 2;
    parameter      MAX_CTU_SIZE                = 64;
    parameter      MAX_CTU_SIZE_DIV_8          = MAX_CTU_SIZE/8;

	//localparam 		PIXEL_WIDTH 				= 8;
	//localparam 		OUTPUT_BLOCK_SIZE 			= 4;
	//localparam		PIXEL_ADDR_LENGTH           = 12;

    localparam      MODE_INTER                  = 1;
    localparam      MODE_INTRA                  = 0;

    localparam      STATE_WRITEUP_FIRST32       = 1;
    localparam      STATE_WRITEUP_SECOND32      = 2;
    localparam      STATE_DONE                  = 0;

    localparam      INNER_CTU_ADDR_LENGTH       = 6;
    //localparam      MAX_LOG2CTBSIZE_WIDTH       = 3;

    localparam      DISPLAY_BUFFER_FRAME1_PTR   =   32'h8800000;
    localparam      DISPLAY_BUFFER_FRAME2_PTR   =   32'h97D2000;
    localparam      DISPLAY_BUFFER_FRAME3_PTR   =   32'hA7A4000;
    localparam      DISPLAY_BUFFER_FRAME4_PTR   =   32'hB776000;
    localparam      DISPLAY_BUFFER_FRAME5_PTR   =   32'hC748000;

    localparam      AXI_WR_STATE_ROW_1 = 1;
    localparam      AXI_WR_STATE_ROW_2 = 2;
    localparam      AXI_WR_STATE_ROW_3 = 3;
    localparam      AXI_WR_STATE_ROW_4 = 4;
    localparam      AXI_WR_STATE_ROW_5 = 5;
    localparam      AXI_WR_STATE_ROW_6 = 6;
    localparam      AXI_WR_STATE_ROW_7 = 7;
    localparam      AXI_WR_STATE_ROW_8 = 8;

    localparam      MAX_LUMA_PS_L4                  = 2_228_224;
    localparam      QUARTER_MAX_LUMA_PS_L4          = MAX_LUMA_PS_L4/4;
    localparam      HALF_MAX_LUMA_PS_L4             = MAX_LUMA_PS_L4/2;
    localparam      THREE_QUARTER_MAX_LUMA_PS_L4    = 3*MAX_LUMA_PS_L4/4;

    localparam      MAX_LUMA_PS_L5                  = 8_912_896;
    localparam      QUAD_QUAD_MAX_LUMA_PS_L5        = MAX_LUMA_PS_L5/16;
    localparam      QUARTER_MAX_LUMA_PS_L5          = MAX_LUMA_PS_L5/4;
    localparam      HALF_MAX_LUMA_PS_L5             = MAX_LUMA_PS_L5/2;
    localparam      THREE_QUARTER_MAX_LUMA_PS_L5    = 3*MAX_LUMA_PS_L5/4;
    
    localparam      MAX_MAX_STORE_IDX = 16;

    localparam      CONFIG_INIT_STATE = 0;
    localparam      CONFIG_STATE_REGISTER_INPUTS    = 1;
    localparam      CONFIG_STATE_FIDX_CALC_WAIT1    = 2;
    localparam      CONFIG_STATE_FIDX_CALC_WAIT2    = 3;
    localparam      CONFIG_STATE_FIDX_CALC_WAIT3    = 4;
    localparam      CONFIG_STATE_FIDX_CALC_WAIT4    = 5;
    localparam      CONFIG_STATE_FIDX_CALC_WAIT5    = 6;
    localparam      CONFIG_STATE_DONE               = 7;
    localparam      CONFIG_STATE_WRITE_BUSY         = 8;


    localparam      AXI_WRADDR_STATE_PREINIT = 0;
    localparam      AXI_WRADDR_STATE_PREINIT_WAIT = 2;
    localparam      AXI_WRADDR_STATE_INIT = 1;
    localparam      AXI_WRADDR_STATE_ACTIVE = 3;
    // localparam      AXI_WRADDR_STATE_WAIT = 2;
    // localparam      AXI_WRADDR_STATE_1ST_8X8 = 2;
    // localparam      AXI_WRADDR_STATE_2ND_8X8 = 3;
    // localparam      AXI_WRADDR_STATE_3RD_8X8 = 3;
    // localparam      AXI_WRADDR_STATE_4TH_8X8 = 4;
    // localparam      AXI_WRADDR_STATE_5TH_8X8 = 5;
    // localparam      AXI_WRADDR_STATE_6TH_8X8 = 6;
    // localparam      AXI_WRADDR_STATE_7TH_8X8 = 7;
    // localparam      AXI_WRADDR_STATE_8TH_8X8 = 8;

    localparam      AXI_WRDATA_STATE_PREINIT = 0;
    localparam      AXI_WRDATA_STATE_INIT = 1;
    localparam      AXI_WRDATA_STATE_ACTIVE = 2;
    localparam      AXI_WRDATA_STATE_DONE = 3;
    localparam      AXI_WRDATA_STATE_WAIT1 = 4;
    localparam      AXI_WRDATA_HIGH_BEAT = 5;
    localparam      AXI_WRDATA_HIGH_BEAT2 = 6;
    localparam      AXI_WRDATA_LOW_BEAT = 7;
    localparam      AXI_WRDATA_LOW_BEAT2 = 8;

    localparam      AXI_RDADDR_STATE_LENGTH_CAL = 5;
    localparam      AXI_RDADDR_STATE_INIT = 0;
    localparam      AXI_RDADDR_STATE_ACTIVE = 1;
    localparam      AXI_RDADDR_STATE_DONE = 2;
    localparam      AXI_RDADDR_STATE_DONE_WAIT = 3;
	localparam      AXI_RDADDR_STATE_PREDONE = 4;


    localparam      AXI_RDDATA_STATE_ACTIVE_0 = 0;
    localparam      AXI_RDDATA_STATE_ACTIVE_1 = 1;
    localparam      AXI_RDDATA_STATE_DONE = 2;
    localparam      AXI_RDDATA_WAIT_FOR_LAST = 3;


    localparam      AXI_WRDATA_BURST_STATE_HIGHER = 0;
    localparam      AXI_WRDATA_BURST_STATE_LOWER = 1;

   localparam        MAX_BURST_LEN = 192;


//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input 																clk;
    input 																axi_clk;
    input 																reset;

    output reg                                                          displaybuffer_fifo_read_en_out;
    input                                                               displaybuffer_fifo_empty_in;
    input     [SAO_OUT_FIFO_WIDTH - 1:0]                                displaybuffer_fifo_data_in;

    input     [MAX_LOG2CTBSIZE_WIDTH - 1:0]                             log2_ctu_size_in;
    input     [POC_WIDTH-1:0]                                           poc_4bits_in;
    input     [STR_ID_WIDTH-1:0]                                        stream_id;
    input     [STR_ID_WIDTH-1:0]                                        read_stream_id;
    input     [PIC_WIDTH_WIDTH  - 1:0]                                 pic_width_in;
    input     [PIC_WIDTH_WIDTH  - 1:0]                                 pic_height_in;
    input                                                               config_valid_in;

//     SAO output fifo
//      X_8x8addr   Y_8x8addr   LUMA_8x8    CB_8x8(4by4)    CR_8x8(4by4)
// bits    8            8       512         128              128             784

    //axi write channel

    output reg [AXI_ADDR_WDTH - 1:0]                                    axi_awaddr;
    output reg                                                          axi_awvalid;
    input                                                               axi_awready;
    output reg [8 - 1:0]                                                axi_awlen;

    output reg [DISP_BUF_AXI_WIDTH  - 1:0]                              axi_wdata;
    output reg                                                          axi_wlast;
    output reg                                                          axi_wvalid;
    input                                                               axi_wready;
    output reg [DISP_BUF_AXI_WIDTH /8 - 1:0]                            axi_wstrb;

    input      [AXI_BRESP_WIDTH - 1:0]                                  axi_bresp;
    input                                                               axi_bvalid;
    output                                                              axi_bready;

    //axi read channel

    output reg [AXI_ADDR_WDTH - 1:0]                                    axi_araddr;
    reg [AXI_ADDR_WDTH - 1:0]                                    axi_araddr_line_start;
    output reg                                                          axi_arvalid;
    input                                                               axi_arready;
    output reg [8 - 1:0]                                                axi_arlen;
    reg [8 - 1:0]                                                axi_arlen_start;
    output  [2 - 1:0]                                                   axi_arburst;
    output  [2 - 1:0]                                                   axi_awburst;

    input      [DISP_BUF_AXI_WIDTH  - 1:0]                              axi_rdata;
    input      [2 - 1:0]                                                axi_rresp;
    input                                                               axi_rlast;
    output reg                                                          axi_rready;
    input                                                               axi_rvalid;

	output  [3:0] axi_arid ; 
	output  [2:0] axi_arsize ; 
	output  [3:0] axi_awid; 
	output  [2:0] axi_awsize;  
	output  [0:0] axi_wid; 
    //hdmi channel
    input                                                               hdmi_fifo_almost_full;
    output reg [8*PIXEL_WIDTH - 1:0]                                    hdmi_fifo_data_out;
    output reg                                                          hdmi_fifo_w_en;






//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

    reg     [SAO_OUT_FIFO_WIDTH - 1:0]                                          displaybuffer_fifo_data_reg;

    wire    [(X_ADDR_WDTH-LOG2_MIN_DU_SIZE) - 1:0]                            current_sao_x_8x8_waddr_wire;
    wire    [(X_ADDR_WDTH-LOG2_MIN_DU_SIZE) - 1:0]                            current_sao_x_8x8_waddr_wire_next;
    reg     [(X_ADDR_WDTH-LOG2_MIN_DU_SIZE) - 1:0]                            current_sao_x_8x8_waddr_reg;
    wire    [(X_ADDR_WDTH-LOG2_MIN_DU_SIZE) - 1:0]                            current_sao_x_8x8_waddr_wire_temp;
    wire    [(X_ADDR_WDTH-LOG2_MIN_DU_SIZE) - 1:0]                            current_sao_x_8x8_waddr_wire_temp_next;

    wire     [(X_ADDR_WDTH-LOG2_MIN_DU_SIZE) - 1:0]                           current_sao_y_8x8_waddr_wire;
    wire     [(X_ADDR_WDTH-LOG2_MIN_DU_SIZE) - 1:0]                           current_sao_y_8x8_waddr_wire_next;
    reg     [(X_ADDR_WDTH-LOG2_MIN_DU_SIZE) - 1:0]                            current_sao_y_8x8_waddr_reg;
    wire    [(X_ADDR_WDTH-LOG2_MIN_DU_SIZE) - 1:0]                            current_sao_y_8x8_waddr_wire_temp;
    wire    [(X_ADDR_WDTH-LOG2_MIN_DU_SIZE) - 1:0]                            current_sao_y_8x8_waddr_wire_temp_next;

    reg     [INNER_CTU_ADDR_LENGTH - 1:0]                                       display_buffer_inner_y_addr_reg;
    reg     [16*PIXEL_WIDTH - 1:0]                                              dispbuf_ddr_wdata_mem[MAX_CTU_SIZE_DIV_8 - 1:0];
    wire     [MAX_CTU_SIZE_DIV_8*16*PIXEL_WIDTH - 1:0]                          dispbuf_ddr_wdata_wire;
    wire     [16*PIXEL_WIDTH - 1:0]                                             dispbuf_ddr_wdata_mem_sel;


    wire    [(PIXEL_WIDTH*DBF_OUT_Y_BLOCK_SIZE*DBF_OUT_Y_BLOCK_SIZE - 1) :0]    current_y_data_wire;
    wire    [(PIXEL_WIDTH*DBF_OUT_Y_BLOCK_SIZE*DBF_OUT_Y_BLOCK_SIZE - 1) :0]    current_y_data_wire_next;
    wire    [(PIXEL_WIDTH*DBF_OUT_CH_BLOCK_SIZE*DBF_OUT_CH_BLOCK_SIZE - 1) :0]  current_cb_data_wire;
    wire    [(PIXEL_WIDTH*DBF_OUT_CH_BLOCK_SIZE*DBF_OUT_CH_BLOCK_SIZE - 1) :0]  current_cb_data_wire_next;
    wire    [(PIXEL_WIDTH*DBF_OUT_CH_BLOCK_SIZE*DBF_OUT_CH_BLOCK_SIZE - 1) :0]  current_cr_data_wire;
    wire    [(PIXEL_WIDTH*DBF_OUT_CH_BLOCK_SIZE*DBF_OUT_CH_BLOCK_SIZE - 1) :0]  current_cr_data_wire_next;

    wire    [(PIXEL_WIDTH - 1) :0]                                      current_y_data_arr_wire    [DBF_OUT_Y_BLOCK_SIZE - 1:0][DBF_OUT_Y_BLOCK_SIZE - 1:0];
    wire    [(PIXEL_WIDTH - 1) :0]                                      current_cb_data_arr_wire   [DBF_OUT_CH_BLOCK_SIZE - 1:0][DBF_OUT_CH_BLOCK_SIZE - 1:0];
    wire    [(PIXEL_WIDTH - 1) :0]                                      current_cr_data_arr_wire   [DBF_OUT_CH_BLOCK_SIZE - 1:0][DBF_OUT_CH_BLOCK_SIZE - 1:0];

    wire    [PIXEL_WIDTH*DBF_OUT_Y_BLOCK_SIZE*2 - 1 : 0]                blockram_wdata_wire [ MAX_CTU_SIZE_DIV_8 - 1:0];

(* MARK_DEBUG *)    reg     [MAX_CTU_SIZE_DIV_8 - 1:0]                  ctu_row_done;
    reg     [MAX_CTU_SIZE_DIV_8 - 1:0]                                  ctu_row_done_d;


    reg     [MAX_LOG2CTBSIZE_WIDTH - 1:0]                               log2_ctu_size_reg;
    reg     [MAX_CTU_SIZE_DIV_8 - 1 : 0]                                ctu_8by8_write_done_mask_mem[MAX_CTU_SIZE_DIV_8 - 1:0];
    reg     [MAX_CTU_SIZE_DIV_8 - 1 : 0]                                ctu_8by8_read_done_rows_reg;
    reg     [3 - 1:0]                                                   current_ctu_row_reg;

    reg                                                                 blockram0_porta_en_reg;
    reg     [16*PIXEL_WIDTH - 1:0]                                      blockram0_porta_wdata_reg;
    reg     [(INNER_CTU_ADDR_LENGTH - 1) : 0]                           blockram0_porta_addr_reg;
    reg                                                                 blockram_porta_wen_reg;
    wire                                                                blockram0_porta_rdata_wire;

    reg                                                                 blockram0_portb_en_reg;
    reg     [16*PIXEL_WIDTH - 1:0]                                      blockram0_portb_wdata_reg;
    reg     [(INNER_CTU_ADDR_LENGTH - 1) : 0]                           blockram0_portb_addr_reg;
    // reg                                                                 blockram0_portb_wen_reg;
    wire    [16*PIXEL_WIDTH - 1 : 0]                                    blockram_portb_rdata_wire_arr[MAX_CTU_SIZE_DIV_8 - 1:0];

    reg                                                                 blockram1_porta_en_reg;
    reg     [16*PIXEL_WIDTH - 1:0]                                      blockram1_porta_wdata_reg;
    reg     [(INNER_CTU_ADDR_LENGTH - 1) : 0]                           blockram1_porta_addr_reg;
    // reg                                                                 blockram1_porta_wen_reg;
    wire                                                                blockram1_porta_rdata_wire;

    reg                                                                 blockram1_portb_en_reg;
    reg     [16*PIXEL_WIDTH - 1:0]                                      blockram1_portb_wdata_reg;
    reg     [(INNER_CTU_ADDR_LENGTH - 1) : 0]                           blockram1_portb_addr_reg;
    // reg                                                                 blockram1_portb_wen_reg;
    // reg                                                                 blockram1_portb_rdata_wire;

    reg                                                                 blockram2_porta_en_reg;
    reg     [16*PIXEL_WIDTH - 1:0]                                      blockram2_porta_wdata_reg;
    reg     [(INNER_CTU_ADDR_LENGTH - 1) : 0]                           blockram2_porta_addr_reg;
    // reg                                                                 blockram2_porta_wen_reg;
    wire                                                                blockram2_porta_rdata_wire;

    reg                                                                 blockram2_portb_en_reg;
    reg     [16*PIXEL_WIDTH - 1:0]                                      blockram2_portb_wdata_reg;
    reg     [(INNER_CTU_ADDR_LENGTH - 1) : 0]                           blockram2_portb_addr_reg;
    // reg                                                                 blockram2_portb_wen_reg;
    // reg                                                                 blockram2_portb_rdata_wire;

    reg                                                                 blockram3_porta_en_reg;
    reg     [16*PIXEL_WIDTH - 1:0]                                      blockram3_porta_wdata_reg;
    reg     [(INNER_CTU_ADDR_LENGTH - 1) : 0]                           blockram3_porta_addr_reg;
    // reg                                                                 blockram3_porta_wen_reg;
    wire                                                                blockram3_porta_rdata_wire;

    reg                                                                 blockram3_portb_en_reg;
    reg     [16*PIXEL_WIDTH - 1:0]                                      blockram3_portb_wdata_reg;
    reg     [(INNER_CTU_ADDR_LENGTH - 1) : 0]                           blockram3_portb_addr_reg;
    // reg                                                                 blockram3_portb_wen_reg;
    // reg                                                                 blockram3_portb_rdata_wire;

    reg                                                                 blockram4_porta_en_reg;
    reg     [16*PIXEL_WIDTH - 1:0]                                      blockram4_porta_wdata_reg;
    reg     [(INNER_CTU_ADDR_LENGTH - 1) : 0]                           blockram4_porta_addr_reg;
    // reg                                                                 blockram4_porta_wen_reg;
    wire                                                                blockram4_porta_rdata_wire;

    reg                                                                 blockram4_portb_en_reg;
    reg     [16*PIXEL_WIDTH - 1:0]                                      blockram4_portb_wdata_reg;
    reg     [(INNER_CTU_ADDR_LENGTH - 1) : 0]                           blockram4_portb_addr_reg;
    // reg                                                                 blockram4_portb_wen_reg;
    // reg                                                                 blockram4_portb_rdata_wire;


    reg                                                                 blockram5_porta_en_reg;
    reg     [16*PIXEL_WIDTH - 1:0]                                      blockram5_porta_wdata_reg;
    reg     [(INNER_CTU_ADDR_LENGTH - 1) : 0]                           blockram5_porta_addr_reg;
    // reg                                                                 blockram5_porta_wen_reg;
    wire                                                                blockram5_porta_rdata_wire;

    reg                                                                 blockram5_portb_en_reg;
    reg     [16*PIXEL_WIDTH - 1:0]                                      blockram5_portb_wdata_reg;
    reg     [(INNER_CTU_ADDR_LENGTH - 1) : 0]                           blockram5_portb_addr_reg;
    // reg                                                                 blockram5_portb_wen_reg;
    // reg                                                                 blockram5_portb_rdata_wire;

    reg                                                                 blockram6_porta_en_reg;
    reg     [16*PIXEL_WIDTH - 1:0]                                      blockram6_porta_wdata_reg;
    reg     [(INNER_CTU_ADDR_LENGTH - 1) : 0]                           blockram6_porta_addr_reg;
    // reg                                                                 blockram6_porta_wen_reg;
    wire                                                                blockram6_porta_rdata_wire;

    reg                                                                 blockram6_portb_en_reg;
    reg     [16*PIXEL_WIDTH - 1:0]                                      blockram6_portb_wdata_reg;
    reg     [(INNER_CTU_ADDR_LENGTH - 1) : 0]                           blockram6_portb_addr_reg;
    // reg                                                                 blockram6_portb_wen_reg;
    // reg                                                                 blockram6_portb_rdata_wire;

    reg                                                                 blockram7_porta_en_reg;
    reg     [16*PIXEL_WIDTH - 1:0]                                      blockram7_porta_wdata_reg;
    reg     [(INNER_CTU_ADDR_LENGTH - 1) : 0]                           blockram7_porta_addr_reg;
    // reg                                                                 blockram7_porta_wen_reg;
    wire                                                                blockram7_porta_rdata_wire;

    reg                                                                 blockram7_portb_en_reg;
    reg     [16*PIXEL_WIDTH - 1:0]                                      blockram7_portb_wdata_reg;
    reg     [(INNER_CTU_ADDR_LENGTH - 1) : 0]                           blockram7_portb_addr_reg;
    // reg                                                                 blockram7_portb_wen_reg;
    // reg                                                                 blockram7_portb_rdata_wire;

    reg     [PIXEL_ADDR_LENGTH - 1:0]                                   pic_width_reg;
    reg     [PIXEL_ADDR_LENGTH - 1:0]                                   pic_height_reg;

    wire     [PIXEL_ADDR_LENGTH - 1:0]                                  ctu_x_minus8;
    reg      [PIXEL_ADDR_LENGTH - 1:0]                                  ctu_x_minus8_d0;
    reg      [PIXEL_ADDR_LENGTH - 1:0]                                  ctu_x_minus8_d1;
    reg      [PIXEL_ADDR_LENGTH - 1:0]                                  ctu_x_minus8_d2;
    reg      [PIXEL_ADDR_LENGTH - 1:0]                                  ctu_x_minus8_d3;
    reg      [PIXEL_ADDR_LENGTH - 1:0]                                  ctu_x_minus8_d4;
    wire     [PIXEL_ADDR_LENGTH - 1:0]                                  ctu_y_minus8;
    reg      [PIXEL_ADDR_LENGTH - 1:0]                                  ctu_y_minus8_d0;
    reg      [PIXEL_ADDR_LENGTH - 1:0]                                  ctu_y_minus8_d1;
    reg      [PIXEL_ADDR_LENGTH - 1:0]                                  ctu_y_minus8_d2;
    reg      [PIXEL_ADDR_LENGTH - 1:0]                                  ctu_y_minus8_d3;
    reg      [PIXEL_ADDR_LENGTH - 1:0]                                  ctu_y_minus8_d4;

    wire     [PIXEL_ADDR_LENGTH - 1:0]                                  ctu_x;
    wire     [PIXEL_ADDR_LENGTH - 1:0]                                  ctu_y;
    wire                                                                ctu_y_is_not_zero;
    reg                                                                 ctu_y_is_not_zero_d0;
    reg                                                                 ctu_y_is_not_zero_d1;
    reg                                                                 ctu_y_is_not_zero_d2;
    reg                                                                 ctu_y_is_not_zero_d3;
    reg                                                                 ctu_y_is_not_zero_d4;

    reg     [MAX_CTU_SIZE_DIV_8 - 1:0]                                  last_8x8_block_in_pic;
    reg     [MAX_CTU_SIZE_DIV_8 - 1:0]                                  old_last_8x8_block_in_pic;

(* MARK_DEBUG *)    reg [10:0]                                             axi_wraddr_state;
(* MARK_DEBUG *)    reg [10:0]                                             axi_wrdata_state;
(* MARK_DEBUG *)    reg [10:0]                                             axi_rddata_state;
(* MARK_DEBUG *)    reg [10:0]                                             axi_rdaddr_state;
(* MARK_DEBUG *)    reg [10:0]                                          axi_wrdata_burst_state;
    integer                                                             config_state;

    reg     [32 - 1 : 0]                                                frame_store_idx_mem[MAX_MAX_STORE_IDX * STR_ID_WIDTH-1:0];
    reg     [32 - 1 : 0]                                                old_frame_store_idx_mem;
    reg     [8  - 1 : 0]                                                frame_store_idx_mem_addr_r;
    reg     [8  - 1 : 0]                                                frame_store_idx_mem_addr_w;
    reg     [32 - 1 : 0]                                                frame_store_idx_mem_wdata;
    reg     [32 - 1 : 0]                                                frame_store_idx_mem_rdata_r;
    reg     [32 - 1 : 0]                                                frame_store_idx_mem_rdata_w;
    reg                                                                 frame_store_idx_mem_wen;
    reg     [32 - 1 : 0]                                                frame_store_poc_mem[MAX_MAX_STORE_IDX * STR_ID_WIDTH-1:0];
(* MARK_DEBUG *)    reg     [MAX_MAX_STORE_IDX - 1:0]                   frame_store_valids;
(* MARK_DEBUG *)    reg     [MAX_MAX_STORE_IDX - 1:0]                   frame_store_valids_read;
(* MARK_DEBUG *)    reg     [MAX_MAX_STORE_IDX - 1:0]                   frame_store_valids_mem[STR_ID_WIDTH-1:0];
(* MARK_DEBUG *)    reg     [MAX_MAX_STORE_IDX - 1:0]                   poc_not_available;
    reg     [4 - 1:0]                                                   max_frame_store_idx;
(* MARK_DEBUG *)    reg     [4 - 1:0]                                   frame_counter;
(* MARK_DEBUG *)    reg     [4 - 1:0]                                   frame_store_write_pointer;
(* MARK_DEBUG *)    reg     [4 - 1:0]                                   old_frame_store_write_pointer;

(* MARK_DEBUG *)    reg     [4 - 1:0]                                   frame_store_read_pointer;
(* MARK_DEBUG *)    reg     [4 - 1:0]                                   old_frame_store_read_pointer;
(* MARK_DEBUG *)    reg     [MAX_MAX_STORE_IDX - 1:0]                   read_pointer_mask;
(* MARK_DEBUG *)    reg     [32 - 1:0]                                  poc_read;

(* MARK_DEBUG *)    reg     [32 - 1:0]                                  frame_ddr_wr_ptr;
    reg     [32 - 1:0]                                                  frame_ddr_wr_ptr_next;
    reg     [32 - 1:0]                                                  frame_ddr_rd_ptr;
    reg                                                                 frame_ddr_rd_done;

(* MARK_DEBUG *)    reg     [PIXEL_ADDR_LENGTH - 1:0]                   frame_row_counter_addr;
(* MARK_DEBUG *)    reg     [PIXEL_ADDR_LENGTH - 1:0]                   frame_col_counter_addr;
(* MARK_DEBUG *)    reg     [PIXEL_ADDR_LENGTH - 1:0]                   frame_row_counter_data;
(* MARK_DEBUG *)    reg     [PIXEL_ADDR_LENGTH - 1:0]                   frame_column_counter;
(* MARK_DEBUG *)    reg     [4 - 1:0]                                   axi_rddata_counter;

    reg     [8 - 1:0]                                                   axi_wraddr_current_ctu;
    reg     [4 - 1:0]                                                   axi_wraddr_counter;
    reg     [4 - 1:0]                                                   axi_wrdata_counter;
    reg     [4 - 1:0]                                                   axi_wrdata_subcounter;
    reg     [32 - 1:0]                                                  ctu_offset_reg;


    reg     [32 - 1:0]                                                  frame_samples_ddr;
    reg     [32 - 1:0]                                                  frame_samples_ddr_d1;
    reg     [32 - 1:0]                                                  frame_samples_ddr_d2;
    reg     [32 - 1:0]                                                  frame_samples_ddr_d3;

	(* MARK_DEBUG *) reg [4:0]                                           frame_store_valid_sum;
   reg [5:0] burst_counter;

	reg initial_poc_0;
   
   
   wire write_end_cond;
   wire write_en_cond;
   wire read_en_cond;
   reg [4-1:0] min_poc_indx;
   reg [32-1:0] min_poc_val;
   assign read_en_cond = frame_ddr_rd_done;
   assign write_end_cond = ((current_sao_x_8x8_waddr_wire_temp == pic_width_reg[X11_ADDR_WDTH-1:LOG2_MIN_DU_SIZE] - 1'b1) && (current_sao_y_8x8_waddr_wire_temp == pic_height_reg[X11_ADDR_WDTH-1:LOG2_MIN_DU_SIZE] - 1'd1) && displaybuffer_fifo_read_en_out ==1 );
   assign write_en_cond = ((current_sao_x_8x8_waddr_wire_temp == 'd0) && (current_sao_y_8x8_waddr_wire_temp == 'd0) && displaybuffer_fifo_read_en_out ==1 );
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------

		assign axi_bready = 1;
		assign axi_arburst  = 1;
		assign axi_awburst  = 1;
		assign axi_arid  = 0; 
		assign axi_arsize  = 3'b011;
		assign axi_awid = 0;
		assign axi_awsize =  3'b011; 
		assign axi_wid = 0;
      
   always@(*) begin
      case(old_frame_store_read_pointer)
         4'd0: begin   read_pointer_mask = 16'b0000_0000_0000_0001;  end
         4'd1: begin   read_pointer_mask = 16'b0000_0000_0000_0010;  end
         4'd2: begin   read_pointer_mask = 16'b0000_0000_0000_0100;  end
         4'd3: begin   read_pointer_mask = 16'b0000_0000_0000_1000;  end
         4'd4: begin   read_pointer_mask = 16'b0000_0000_0001_0000;  end
         4'd5: begin   read_pointer_mask = 16'b0000_0000_0010_0000;  end
         4'd6: begin   read_pointer_mask = 16'b0000_0000_0100_0000;  end
         4'd7: begin   read_pointer_mask = 16'b0000_0000_1000_0000;  end
         4'd8: begin   read_pointer_mask = 16'b0000_0001_0000_0000;  end
         4'd9: begin   read_pointer_mask = 16'b0000_0010_0000_0000;  end
         4'd10: begin  read_pointer_mask = 16'b0000_0100_0000_0000;  end
         4'd11: begin  read_pointer_mask = 16'b0000_1000_0000_0000;  end
         4'd12: begin  read_pointer_mask = 16'b0001_0000_0000_0000;  end
         4'd13: begin  read_pointer_mask = 16'b0010_0000_0000_0000;  end
         4'd14: begin  read_pointer_mask = 16'b0100_0000_0000_0000;  end
         4'd15: begin  read_pointer_mask = 16'b1000_0000_0000_0000;  end
      endcase
   end
      
   always@(posedge clk) begin
      frame_store_valids <= frame_store_valids_mem[stream_id];
      frame_store_valids_read <= frame_store_valids_mem[read_stream_id];
   end
	
	always@(*) begin
		frame_store_valid_sum = (frame_store_valids[0] + frame_store_valids[1]) + (frame_store_valids[2] + frame_store_valids[3]) + (frame_store_valids[4] + frame_store_valids[5]) +
								(frame_store_valids[6] + frame_store_valids[7]) + (frame_store_valids[8] + frame_store_valids[9]) + (frame_store_valids[10]+ frame_store_valids[11]) +
								(frame_store_valids[12]+ frame_store_valids[13])+ (frame_store_valids[14]+ frame_store_valids[15]);
	end
   always @(posedge clk) begin
        if((displaybuffer_fifo_read_en_out == 1'b1)/*&&(displaybuffer_fifo_almost_empty_in == 1'b1)*/) begin
            displaybuffer_fifo_read_en_out <= 1'b0;
            //displaybuffer_fifo_data_reg <= displaybuffer_fifo_data_in;
        end
        else if(displaybuffer_fifo_empty_in == 1'b0) begin
            displaybuffer_fifo_data_reg <= displaybuffer_fifo_data_in;

            if((ctu_8by8_write_done_mask_mem[current_sao_y_8x8_waddr_wire_next[3-1:0]][current_sao_x_8x8_waddr_wire_next[3-1:0]] == 1'b0)
                && (~(|(ctu_row_done)))  && (config_state==CONFIG_STATE_DONE) && (frame_store_valid_sum < (max_frame_store_idx+1'b1))) begin
                displaybuffer_fifo_read_en_out <= 1'b1;
                blockram_porta_wen_reg         <= 1'b1;
            end
            else begin
                displaybuffer_fifo_read_en_out <= 1'b0;
                blockram_porta_wen_reg         <= 1'b0;
            end
        end
        else begin
            displaybuffer_fifo_read_en_out <= 1'b0;
            blockram_porta_wen_reg         <= 1'b0;
        end
   end

   always@(*) begin
      frame_store_idx_mem_rdata_r = frame_store_idx_mem[frame_store_idx_mem_addr_r];
      frame_store_idx_mem_rdata_w = frame_store_idx_mem[frame_store_idx_mem_addr_w];
   end
   
   always @(posedge clk) begin : frame_store_idx_calculation_pipeline

        frame_samples_ddr <= ((pic_width_reg[PIXEL_ADDR_LENGTH - 1:6] + 2'd2) *pic_height_reg);
        frame_samples_ddr_d1 <= frame_samples_ddr << 7;
        frame_samples_ddr_d2 <= frame_samples_ddr_d1;
        frame_samples_ddr_d3 <= frame_samples_ddr_d2;


        if(displaybuffer_fifo_read_en_out) begin
            current_sao_x_8x8_waddr_reg <= current_sao_x_8x8_waddr_wire;
            current_sao_y_8x8_waddr_reg <= current_sao_y_8x8_waddr_wire;
        end
   end


    always @(posedge clk) begin : config_state_machine
        if(reset) begin
            config_state <= CONFIG_INIT_STATE;
            max_frame_store_idx <= 4'd8; //min value is 6 whateva the condition
            frame_store_write_pointer <= 4'd1;
			   frame_ddr_wr_ptr <= 0;
            frame_store_idx_mem_addr_w <= 'd0;
        end
        else begin
            case(config_state)
                CONFIG_INIT_STATE: begin
                     frame_store_idx_mem[MAX_MAX_STORE_IDX* frame_ddr_wr_ptr + 0] <= DISPBUF_BASE_ADDR;
                     old_frame_store_idx_mem <= DISPBUF_BASE_ADDR;
                     frame_store_valids_mem[frame_ddr_wr_ptr] <= 'd0;
                     
                     if(frame_ddr_wr_ptr != MAX_STREAMS -1) begin
                        frame_ddr_wr_ptr <= frame_ddr_wr_ptr + 1;
                     end  
                     if(displaybuffer_fifo_empty_in == 1'b0) begin
                        pic_width_reg   <= {1'b0,pic_width_in};//pic_width_in;   //this is only until SAO supports 4k;
                        pic_height_reg  <= {1'b0,pic_height_in};//pic_height_in;
                        config_state <= CONFIG_STATE_REGISTER_INPUTS;
                        frame_ddr_wr_ptr <= 0 ;
                     end
                     
                end
                CONFIG_STATE_REGISTER_INPUTS : begin
                  config_state <= CONFIG_STATE_FIDX_CALC_WAIT1;
                end
                CONFIG_STATE_FIDX_CALC_WAIT1 : begin
                  config_state <= CONFIG_STATE_FIDX_CALC_WAIT2;
                end
                CONFIG_STATE_FIDX_CALC_WAIT2 : begin
                     config_state <= CONFIG_STATE_FIDX_CALC_WAIT3;
                end
                CONFIG_STATE_FIDX_CALC_WAIT3 : begin
                        config_state <= CONFIG_STATE_FIDX_CALC_WAIT4;
                end
                CONFIG_STATE_FIDX_CALC_WAIT4 : begin
                     config_state <= CONFIG_STATE_FIDX_CALC_WAIT5;
                     if (frame_samples_ddr_d3 <= QUARTER_MAX_LUMA_PS_L5) begin
                        max_frame_store_idx <= 4'd15;
                     end
                     else if(frame_samples_ddr_d3 <= HALF_MAX_LUMA_PS_L5) begin
                        max_frame_store_idx <= 4'd12;
                     end
                     else if(frame_samples_ddr_d3 <= THREE_QUARTER_MAX_LUMA_PS_L5) begin
                        max_frame_store_idx <= 4'd8;
                     end
                     else begin
                        max_frame_store_idx <= 4'd6;
                     end
                end
                CONFIG_STATE_FIDX_CALC_WAIT5 : begin
                        if (frame_store_write_pointer < max_frame_store_idx) begin
                           frame_store_idx_mem[frame_ddr_wr_ptr* MAX_MAX_STORE_IDX + frame_store_write_pointer] <= old_frame_store_idx_mem + frame_samples_ddr_d3;
                           old_frame_store_idx_mem <= old_frame_store_idx_mem + frame_samples_ddr_d3;
                           frame_store_write_pointer <= frame_store_write_pointer + 1'b1;
                        end
                        else begin
                           if(frame_ddr_wr_ptr == MAX_STREAMS -1) begin
                              config_state <= CONFIG_STATE_DONE;
                              frame_store_write_pointer <= 0;
                              frame_store_idx_mem_addr_w <= stream_id* MAX_MAX_STORE_IDX +0;
                           end
                           else begin
                              frame_store_idx_mem[(frame_ddr_wr_ptr+1)* MAX_MAX_STORE_IDX + 0] <= old_frame_store_idx_mem + frame_samples_ddr_d3;
                              old_frame_store_idx_mem <= old_frame_store_idx_mem + frame_samples_ddr_d3;
                              frame_ddr_wr_ptr <= frame_ddr_wr_ptr + 1;
                              frame_store_write_pointer <= 4'd1;
                           end
                        end
                end
                CONFIG_STATE_DONE : begin
                     if(write_en_cond) begin
                        if((frame_store_valids[frame_store_write_pointer] == 0) && (frame_store_valid_sum < max_frame_store_idx)) begin
                           // frame_store_valids_mem[stream_id* MAX_MAX_STORE_IDX + frame_store_write_pointer] <= 1;
                           frame_store_poc_mem[stream_id* MAX_MAX_STORE_IDX + frame_store_write_pointer]<=  poc_4bits_in;
                           frame_ddr_wr_ptr <= frame_store_idx_mem_rdata_w;
                           old_frame_store_write_pointer <= frame_store_write_pointer;
                           if(frame_store_write_pointer < max_frame_store_idx -1) begin
                              frame_store_write_pointer <= frame_store_write_pointer + 1;
                              frame_store_idx_mem_addr_w <= stream_id* MAX_MAX_STORE_IDX + frame_store_write_pointer + 1;
                           end
                           else begin
                              frame_store_write_pointer <= 0;
                              frame_store_idx_mem_addr_w <= stream_id* MAX_MAX_STORE_IDX + 0;
                           end
                        end
                        else begin
                           config_state <= CONFIG_STATE_WRITE_BUSY;
                           $display("frame write busy!!");
                           $stop();
                           min_poc_val <= 32'hFFFF_FFFFF;
                           frame_store_write_pointer <= 0;
                           frame_store_idx_mem_addr_w <= stream_id* MAX_MAX_STORE_IDX + 0;
                           min_poc_indx <= 0;
                        end
                     end

                     if(write_end_cond) begin
                        frame_store_valids_mem[stream_id* MAX_MAX_STORE_IDX] <= (1<<old_frame_store_write_pointer) | frame_store_valids;
                     end
                     if(read_en_cond) begin
                        frame_store_valids_mem[read_stream_id*MAX_MAX_STORE_IDX ] <= (~read_pointer_mask) & frame_store_valids;
                     end

                end
               CONFIG_STATE_WRITE_BUSY: begin     // starting from zero assign earliest available position. Else assign oldest poc position
                     if(frame_store_write_pointer < max_frame_store_idx -1) begin
                        if((frame_store_valids[frame_store_write_pointer] == 0) && (frame_store_valid_sum <max_frame_store_idx)) begin
                           
                           frame_store_poc_mem[stream_id* MAX_MAX_STORE_IDX +frame_store_write_pointer]<=  poc_4bits_in;
                           frame_ddr_wr_ptr <= frame_store_idx_mem_rdata_w;
                           old_frame_store_write_pointer <= frame_store_write_pointer;
                           config_state <= CONFIG_STATE_DONE;
                           frame_store_idx_mem_addr_w <= stream_id* MAX_MAX_STORE_IDX + frame_store_write_pointer + 1'b1;
                           frame_store_write_pointer <= frame_store_write_pointer + 1'b1;
                        end
                        else begin
                           frame_store_write_pointer <= frame_store_write_pointer + 1;
                           if(frame_store_poc_mem[stream_id* MAX_MAX_STORE_IDX +frame_store_write_pointer] < min_poc_val) begin
                              min_poc_indx <= frame_store_write_pointer;
                              min_poc_val <= frame_store_poc_mem[stream_id* MAX_MAX_STORE_IDX +frame_store_write_pointer];
                              if(frame_store_write_pointer == max_frame_store_idx - 2) begin
                                 frame_store_idx_mem_addr_w <= stream_id* MAX_MAX_STORE_IDX + frame_store_write_pointer ;
                              end
                              else begin
                                 frame_store_idx_mem_addr_w <= stream_id* MAX_MAX_STORE_IDX + frame_store_write_pointer + 1'b1;
                              end
                           end
                           else begin
                              if(frame_store_write_pointer == max_frame_store_idx - 2) begin
                                 frame_store_idx_mem_addr_w <= stream_id* MAX_MAX_STORE_IDX + min_poc_indx ;
                              end     
                              else begin
                                 frame_store_idx_mem_addr_w <= stream_id* MAX_MAX_STORE_IDX + frame_store_write_pointer + 1'b1;
                              end
                           end
                        end

                     end
                     else begin
                        frame_store_write_pointer <= min_poc_indx + 1;
                        frame_store_idx_mem_addr_w <= stream_id* MAX_MAX_STORE_IDX + min_poc_indx + 1;
                        
                        frame_store_poc_mem[stream_id* MAX_MAX_STORE_IDX +min_poc_indx]<=  poc_4bits_in;
                        frame_ddr_wr_ptr <= frame_store_idx_mem_rdata_w;
                        old_frame_store_write_pointer <= min_poc_indx;
                        config_state <= CONFIG_STATE_DONE;
                           $display("min update busy!!");
                           $stop();
                     end
                     if(read_en_cond) begin
                        frame_store_valids_mem[read_stream_id*MAX_MAX_STORE_IDX + frame_store_read_pointer] <= 1'b0;
                     end
               end
            endcase
        end
    end





    assign {current_sao_x_8x8_waddr_wire_temp,current_sao_y_8x8_waddr_wire_temp,current_y_data_wire,current_cb_data_wire,current_cr_data_wire} = displaybuffer_fifo_data_reg;
    assign {current_sao_x_8x8_waddr_wire_temp_next,current_sao_y_8x8_waddr_wire_temp_next,current_y_data_wire_next,current_cb_data_wire_next,current_cr_data_wire_next} = displaybuffer_fifo_data_in;

    assign current_sao_x_8x8_waddr_wire_next = current_sao_x_8x8_waddr_wire_temp_next + 1'b1;
    assign current_sao_y_8x8_waddr_wire_next = current_sao_y_8x8_waddr_wire_temp_next + 1'b1;

    assign current_sao_x_8x8_waddr_wire = current_sao_x_8x8_waddr_wire_temp + 1'b1;
    assign current_sao_y_8x8_waddr_wire = current_sao_y_8x8_waddr_wire_temp + 1'b1;
    assign ctu_y_is_not_zero = |current_sao_y_8x8_waddr_wire[(X11_ADDR_WDTH-LOG2_MIN_DU_SIZE) - 1:3];
    assign ctu_x_minus8 = {current_sao_x_8x8_waddr_wire[(X11_ADDR_WDTH-LOG2_MIN_DU_SIZE) - 1:3],6'd0} - 5'd8;
    assign ctu_y_minus8 = ctu_y_is_not_zero ? ({current_sao_y_8x8_waddr_wire[(X11_ADDR_WDTH-LOG2_MIN_DU_SIZE) - 1:3],6'd0} - 5'd8) : 32'd8;
    assign ctu_x = {current_sao_x_8x8_waddr_wire[(X11_ADDR_WDTH-LOG2_MIN_DU_SIZE) - 1:3],6'd0};
    assign ctu_y = {current_sao_y_8x8_waddr_wire[(X11_ADDR_WDTH-LOG2_MIN_DU_SIZE) - 1:3],6'd0};



    generate // declare_wire_arr
        genvar i;
        genvar j;

        for (i = 0 ; i < 8 ; i = i + 1) begin
            for (j = 0 ; j < 8 ; j = j + 1) begin
            //current_y_data_arr_wire[x][y]
                assign current_y_data_arr_wire[i][j] = current_y_data_wire[j*DBF_OUT_Y_BLOCK_SIZE*PIXEL_WIDTH + (i+1)*PIXEL_WIDTH - 1: j*DBF_OUT_Y_BLOCK_SIZE*PIXEL_WIDTH + (i)*PIXEL_WIDTH];
            end
        end

        for (i = 0 ; i < 4 ; i = i + 1) begin
            for (j = 0 ; j < 4 ; j = j + 1) begin
            //current_cb_data_arr_wire[x][y]
                assign current_cb_data_arr_wire[i][j] = current_cb_data_wire[j*DBF_OUT_CH_BLOCK_SIZE*PIXEL_WIDTH + (i+1)*PIXEL_WIDTH - 1: j*DBF_OUT_CH_BLOCK_SIZE*PIXEL_WIDTH + (i)*PIXEL_WIDTH];
                assign current_cr_data_arr_wire[i][j] = current_cr_data_wire[j*DBF_OUT_CH_BLOCK_SIZE*PIXEL_WIDTH + (i+1)*PIXEL_WIDTH - 1: j*DBF_OUT_CH_BLOCK_SIZE*PIXEL_WIDTH + (i)*PIXEL_WIDTH];
            end
        end

        for (i = 0 ; i < MAX_CTU_SIZE_DIV_8 ; i = i + 1) begin
            assign blockram_wdata_wire[i] = {   current_cr_data_arr_wire[3][(i >> 1)],current_y_data_arr_wire[7][i],current_cb_data_arr_wire[3][(i >> 1)],current_y_data_arr_wire[6][i],
                                                current_cr_data_arr_wire[2][(i >> 1)],current_y_data_arr_wire[5][i],current_cb_data_arr_wire[2][(i >> 1)],current_y_data_arr_wire[4][i],
                                                current_cr_data_arr_wire[1][(i >> 1)],current_y_data_arr_wire[3][i],current_cb_data_arr_wire[1][(i >> 1)],current_y_data_arr_wire[2][i],
                                                current_cr_data_arr_wire[0][(i >> 1)],current_y_data_arr_wire[1][i],current_cb_data_arr_wire[0][(i >> 1)],current_y_data_arr_wire[0][i]};
        end
    endgenerate

    always @(posedge clk) begin : block_ram_data_write_handle
        case(current_sao_x_8x8_waddr_wire[3-1:0])
            3'd0 : begin
                blockram0_porta_wdata_reg <= blockram_wdata_wire[0];
                blockram0_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd0};

                blockram1_porta_wdata_reg <= blockram_wdata_wire[1];
                blockram1_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd1};

                blockram2_porta_wdata_reg <= blockram_wdata_wire[2];
                blockram2_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd2};

                blockram3_porta_wdata_reg <= blockram_wdata_wire[3];
                blockram3_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd3};

                blockram4_porta_wdata_reg <= blockram_wdata_wire[4];
                blockram4_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd4};

                blockram5_porta_wdata_reg <= blockram_wdata_wire[5];
                blockram5_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd5};

                blockram6_porta_wdata_reg <= blockram_wdata_wire[6];
                blockram6_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd6};

                blockram7_porta_wdata_reg <= blockram_wdata_wire[7];
                blockram7_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd7};
            end
            3'd1 : begin
                blockram0_porta_wdata_reg <= blockram_wdata_wire[7];
                blockram0_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd7};

                blockram1_porta_wdata_reg <= blockram_wdata_wire[0];
                blockram1_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd0};

                blockram2_porta_wdata_reg <= blockram_wdata_wire[1];
                blockram2_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd1};

                blockram3_porta_wdata_reg <= blockram_wdata_wire[2];
                blockram3_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd2};

                blockram4_porta_wdata_reg <= blockram_wdata_wire[3];
                blockram4_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd3};

                blockram5_porta_wdata_reg <= blockram_wdata_wire[4];
                blockram5_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd4};

                blockram6_porta_wdata_reg <= blockram_wdata_wire[5];
                blockram6_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd5};

                blockram7_porta_wdata_reg <= blockram_wdata_wire[6];
                blockram7_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd6};

            end
            3'd2 : begin
                blockram0_porta_wdata_reg <= blockram_wdata_wire[6];
                blockram0_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd6};

                blockram1_porta_wdata_reg <= blockram_wdata_wire[7];
                blockram1_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd7};

                blockram2_porta_wdata_reg <= blockram_wdata_wire[0];
                blockram2_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd0};

                blockram3_porta_wdata_reg <= blockram_wdata_wire[1];
                blockram3_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd1};

                blockram4_porta_wdata_reg <= blockram_wdata_wire[2];
                blockram4_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd2};

                blockram5_porta_wdata_reg <= blockram_wdata_wire[3];
                blockram5_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd3};

                blockram6_porta_wdata_reg <= blockram_wdata_wire[4];
                blockram6_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd4};

                blockram7_porta_wdata_reg <= blockram_wdata_wire[5];
                blockram7_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd5};
            end
            3'd3 : begin
                blockram0_porta_wdata_reg <= blockram_wdata_wire[5];
                blockram0_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd5};

                blockram1_porta_wdata_reg <= blockram_wdata_wire[6];
                blockram1_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd6};

                blockram2_porta_wdata_reg <= blockram_wdata_wire[7];
                blockram2_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd7};

                blockram3_porta_wdata_reg <= blockram_wdata_wire[0];
                blockram3_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd0};

                blockram4_porta_wdata_reg <= blockram_wdata_wire[1];
                blockram4_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd1};

                blockram5_porta_wdata_reg <= blockram_wdata_wire[2];
                blockram5_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd2};

                blockram6_porta_wdata_reg <= blockram_wdata_wire[3];
                blockram6_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd3};

                blockram7_porta_wdata_reg <= blockram_wdata_wire[4];
                blockram7_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd4};
            end
            3'd4 : begin
                blockram0_porta_wdata_reg <= blockram_wdata_wire[4];
                blockram0_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd4};

                blockram1_porta_wdata_reg <= blockram_wdata_wire[5];
                blockram1_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd5};

                blockram2_porta_wdata_reg <= blockram_wdata_wire[6];
                blockram2_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd6};

                blockram3_porta_wdata_reg <= blockram_wdata_wire[7];
                blockram3_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd7};

                blockram4_porta_wdata_reg <= blockram_wdata_wire[0];
                blockram4_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd0};

                blockram5_porta_wdata_reg <= blockram_wdata_wire[1];
                blockram5_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd1};

                blockram6_porta_wdata_reg <= blockram_wdata_wire[2];
                blockram6_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd2};

                blockram7_porta_wdata_reg <= blockram_wdata_wire[3];
                blockram7_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd3};
            end
            3'd5 : begin
                blockram0_porta_wdata_reg <= blockram_wdata_wire[3];
                blockram0_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd3};

                blockram1_porta_wdata_reg <= blockram_wdata_wire[4];
                blockram1_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd4};

                blockram2_porta_wdata_reg <= blockram_wdata_wire[5];
                blockram2_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd5};

                blockram3_porta_wdata_reg <= blockram_wdata_wire[6];
                blockram3_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd6};

                blockram4_porta_wdata_reg <= blockram_wdata_wire[7];
                blockram4_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd7};

                blockram5_porta_wdata_reg <= blockram_wdata_wire[0];
                blockram5_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd0};

                blockram6_porta_wdata_reg <= blockram_wdata_wire[1];
                blockram6_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd1};

                blockram7_porta_wdata_reg <= blockram_wdata_wire[2];
                blockram7_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd2};
            end
            3'd6 : begin
                blockram0_porta_wdata_reg <= blockram_wdata_wire[2];
                blockram0_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd2};

                blockram1_porta_wdata_reg <= blockram_wdata_wire[3];
                blockram1_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd3};

                blockram2_porta_wdata_reg <= blockram_wdata_wire[4];
                blockram2_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd4};

                blockram3_porta_wdata_reg <= blockram_wdata_wire[5];
                blockram3_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd5};

                blockram4_porta_wdata_reg <= blockram_wdata_wire[6];
                blockram4_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd6};

                blockram5_porta_wdata_reg <= blockram_wdata_wire[7];
                blockram5_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd7};

                blockram6_porta_wdata_reg <= blockram_wdata_wire[0];
                blockram6_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd0};

                blockram7_porta_wdata_reg <= blockram_wdata_wire[1];
                blockram7_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd1};
            end
            3'd7 : begin
                blockram0_porta_wdata_reg <= blockram_wdata_wire[1];
                blockram0_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd1};

                blockram1_porta_wdata_reg <= blockram_wdata_wire[2];
                blockram1_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd2};

                blockram2_porta_wdata_reg <= blockram_wdata_wire[3];
                blockram2_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd3};

                blockram3_porta_wdata_reg <= blockram_wdata_wire[4];
                blockram3_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd4};

                blockram4_porta_wdata_reg <= blockram_wdata_wire[5];
                blockram4_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd5};

                blockram5_porta_wdata_reg <= blockram_wdata_wire[6];
                blockram5_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd6};

                blockram6_porta_wdata_reg <= blockram_wdata_wire[7];
                blockram6_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd7};

                blockram7_porta_wdata_reg <= blockram_wdata_wire[0];
                blockram7_porta_addr_reg  <= {current_sao_y_8x8_waddr_wire[3 - 1:0],3'd0};
            end
        endcase
    end

    always @(posedge clk) begin
        blockram0_portb_addr_reg <= display_buffer_inner_y_addr_reg;
    end

    always @(*) begin : block_ram_data_read_handle
        integer i;
        case(blockram0_portb_addr_reg[3 - 1:0])
            3'd0 : begin
                for (i = 0 ; i < MAX_CTU_SIZE_DIV_8 ; i = i + 1) begin
                    dispbuf_ddr_wdata_mem[i] = blockram_portb_rdata_wire_arr[i];
                end
            end
            3'd1 : begin
                for (i = 0 ; i < MAX_CTU_SIZE_DIV_8 ; i = i + 1) begin
                    dispbuf_ddr_wdata_mem[(8 + i - 1)%8] = blockram_portb_rdata_wire_arr[i];
                end
            end
            3'd2 : begin
                for (i = 0 ; i < MAX_CTU_SIZE_DIV_8 ; i = i + 1) begin
                    dispbuf_ddr_wdata_mem[(8 + i - 2)%8] <= blockram_portb_rdata_wire_arr[i];
                end
            end
            3'd3 : begin
                for (i = 0 ; i < MAX_CTU_SIZE_DIV_8 ; i = i + 1) begin
                    dispbuf_ddr_wdata_mem[(8 + i - 3)%8] = blockram_portb_rdata_wire_arr[i];
                end
            end
            3'd4 : begin
                for (i = 0 ; i < MAX_CTU_SIZE_DIV_8 ; i = i + 1) begin
                    dispbuf_ddr_wdata_mem[(8 + i - 4)%8] = blockram_portb_rdata_wire_arr[i];
                end
            end
            3'd5 : begin
                for (i = 0 ; i < MAX_CTU_SIZE_DIV_8 ; i = i + 1) begin
                    dispbuf_ddr_wdata_mem[(8 + i - 5)%8] = blockram_portb_rdata_wire_arr[i];
                end
            end
            3'd6 : begin
                for (i = 0 ; i < MAX_CTU_SIZE_DIV_8 ; i = i + 1) begin
                    dispbuf_ddr_wdata_mem[(8 + i - 6)%8] = blockram_portb_rdata_wire_arr[i];
                end
            end
            3'd7 : begin
                for (i = 0 ; i < MAX_CTU_SIZE_DIV_8 ; i = i + 1) begin
                    dispbuf_ddr_wdata_mem[(8 + i - 7)%8] = blockram_portb_rdata_wire_arr[i];
                end
            end
        endcase
    end

    generate
        genvar k;
        for (k = 0 ; k < MAX_CTU_SIZE_DIV_8 ; k = k + 1) begin
            assign dispbuf_ddr_wdata_wire[(k+1)*16*PIXEL_WIDTH - 1:k*16*PIXEL_WIDTH] = dispbuf_ddr_wdata_mem[k];
        end
    endgenerate
   assign dispbuf_ddr_wdata_mem_sel =  dispbuf_ddr_wdata_mem[burst_counter];


    always @(posedge clk) begin
        log2_ctu_size_reg <= log2_ctu_size_in;
    end

    always @(posedge clk) begin : updating_the_mask
        integer i;
        if (reset) begin
            for (i = 0 ; i < MAX_CTU_SIZE_DIV_8 ; i = i + 1) begin
                ctu_8by8_write_done_mask_mem[i] <= {MAX_CTU_SIZE_DIV_8{1'b0}};
            end
            last_8x8_block_in_pic <= {MAX_CTU_SIZE_DIV_8{1'b0}};
        end
        else begin

            for (i = 0 ; i < MAX_CTU_SIZE_DIV_8 ; i = i + 1) begin
                if(ctu_row_done_d[i] == 1'b1) begin
                    ctu_8by8_write_done_mask_mem[i] <= ctu_8by8_write_done_mask_mem[i]&{MAX_CTU_SIZE_DIV_8{~ctu_8by8_read_done_rows_reg[i]}};
                    last_8x8_block_in_pic[i] <= last_8x8_block_in_pic[i]&(~ctu_8by8_read_done_rows_reg[i]);
                end
            end




            if(displaybuffer_fifo_read_en_out) begin
                //here y and x are changed
                ctu_8by8_write_done_mask_mem[current_sao_y_8x8_waddr_wire[3-1:0]][current_sao_x_8x8_waddr_wire[3-1:0]] <= 1'b1;

                if(current_sao_x_8x8_waddr_wire == pic_width_reg[PIXEL_ADDR_LENGTH - 1:3]) begin
                    last_8x8_block_in_pic[current_sao_y_8x8_waddr_wire[3-1:0]] <= 1'b1;
                end
                else begin
                    last_8x8_block_in_pic[current_sao_y_8x8_waddr_wire[3-1:0]] <= 1'b0;
                end
            end
            // else begin

            // end
        end
    end

    always @(*) begin : ctu_rows_completed
        integer i;
        for ( i = 0 ; i < MAX_CTU_SIZE_DIV_8 ; i = i + 1) begin
            ctu_row_done[i] = &ctu_8by8_write_done_mask_mem[i][7:1] | last_8x8_block_in_pic[i];
        end
    end

    always @(posedge clk) begin
        if(reset) begin
            ctu_x_minus8_d0 <= 0;
            ctu_y_minus8_d0 <= 0;
        end
        else begin
            ctu_row_done_d <= ctu_row_done;

            //if ((ctu_row_done_d[1] == 1'b0) && (ctu_row_done[1] == 1'b1)) begin
                ctu_x_minus8_d0 <= ctu_x_minus8;
            //end

            ctu_x_minus8_d1 <= ctu_x_minus8_d0;
            ctu_x_minus8_d2 <= ctu_x_minus8_d1;
            ctu_x_minus8_d3 <= ctu_x_minus8_d2;
            ctu_x_minus8_d4 <= ctu_x_minus8_d3;

            //if ((ctu_row_done_d[1] == 1'b0) && (ctu_row_done[1] == 1'b1)) begin
                ctu_y_minus8_d0 <= ctu_y_minus8;
           // end

            ctu_y_minus8_d1 <= ctu_y_minus8_d0;
            ctu_y_minus8_d2 <= ctu_y_minus8_d1;
            ctu_y_minus8_d3 <= ctu_y_minus8_d2;
            ctu_y_minus8_d4 <= ctu_y_minus8_d3;

            ctu_y_is_not_zero_d0 <= ctu_y_is_not_zero;
            ctu_y_is_not_zero_d1 <= ctu_y_is_not_zero_d0;
            ctu_y_is_not_zero_d2 <= ctu_y_is_not_zero_d1;
            ctu_y_is_not_zero_d3 <= ctu_y_is_not_zero_d2;
            ctu_y_is_not_zero_d4 <= ctu_y_is_not_zero_d3;
        end
    end

    always @(posedge clk) begin : axi4_write_addr_fsm
        if(reset) begin
            axi_wraddr_state <= AXI_WRADDR_STATE_PREINIT;
            axi_wraddr_counter <= 3'd0;
            axi_awlen <= 8'd15;
            axi_awvalid <= 1'b0;
            axi_wraddr_current_ctu <= 8'd0;
            old_last_8x8_block_in_pic <= {MAX_CTU_SIZE_DIV_8{1'b0}};
        end
        else begin
            case(axi_wraddr_state)
                AXI_WRADDR_STATE_PREINIT : begin
                    if(config_state == CONFIG_STATE_DONE) begin
                        if(|((ctu_row_done&(~axi_wraddr_current_ctu)) | (last_8x8_block_in_pic&(~old_last_8x8_block_in_pic))) ) begin
                            if(ctu_y_is_not_zero_d3) begin
                                if(|((ctu_row_done&(~axi_wraddr_current_ctu)) | (last_8x8_block_in_pic&(~old_last_8x8_block_in_pic))) ) begin
                                  ctu_offset_reg <= ({{(32 - PIXEL_ADDR_LENGTH){ctu_x_minus8_d3[PIXEL_ADDR_LENGTH - 1]}},ctu_x_minus8_d3} << 1) + ctu_y_minus8_d3*((pic_width_reg[PIXEL_ADDR_LENGTH - 1:6] + 2'd2) << (1+6));
                                end
                                axi_wraddr_state <= AXI_WRADDR_STATE_PREINIT_WAIT;
                            end
                            else begin
                                if(|((ctu_row_done&(~axi_wraddr_current_ctu)) | (last_8x8_block_in_pic&(~old_last_8x8_block_in_pic))) ) begin
                                  ctu_offset_reg <= ({{(32 - PIXEL_ADDR_LENGTH){ctu_x_minus8_d3[PIXEL_ADDR_LENGTH - 1]}},ctu_x_minus8_d3} << 1) - ctu_y_minus8_d3*((pic_width_reg[PIXEL_ADDR_LENGTH - 1:6] + 2'd2) << (1+6));
                                end
                                axi_wraddr_state <= AXI_WRADDR_STATE_PREINIT_WAIT;
                            end
                        end
                    end
                end
                AXI_WRADDR_STATE_PREINIT_WAIT : begin
                     if(ctu_y_is_not_zero_d4) begin
                        if(|((ctu_row_done_d&(~axi_wraddr_current_ctu)) | (last_8x8_block_in_pic&(~old_last_8x8_block_in_pic))) ) begin
                           ctu_offset_reg <= ({{(32 - PIXEL_ADDR_LENGTH){ctu_x_minus8_d4[PIXEL_ADDR_LENGTH - 1]}},ctu_x_minus8_d4} << 1) + ctu_y_minus8_d4*((pic_width_reg[PIXEL_ADDR_LENGTH - 1:6] + 2'd2) << (1+6));
                        end
                         axi_wraddr_state <= AXI_WRADDR_STATE_INIT;
                     end
                     else begin
                       if(|((ctu_row_done_d&(~axi_wraddr_current_ctu)) | (last_8x8_block_in_pic&(~old_last_8x8_block_in_pic))) ) begin
                           ctu_offset_reg <= ({{(32 - PIXEL_ADDR_LENGTH){ctu_x_minus8_d4[PIXEL_ADDR_LENGTH - 1]}},ctu_x_minus8_d4} << 1) - ctu_y_minus8_d4*((pic_width_reg[PIXEL_ADDR_LENGTH - 1:6] + 2'd2) << (1+6));
                       end
                         axi_wraddr_state <= AXI_WRADDR_STATE_INIT;
                     end
                end
                AXI_WRADDR_STATE_INIT : begin
                    // if(ctu_row_done[0]) begin


                        if ((ctu_row_done[0]&(~axi_wraddr_current_ctu[0]))|(last_8x8_block_in_pic[0]&~old_last_8x8_block_in_pic[0])) begin
                            axi_wraddr_state <= AXI_WRADDR_STATE_ACTIVE;
                            axi_awaddr <= frame_ddr_wr_ptr + ctu_offset_reg;// + (pic_width_reg << (1+3));
                            axi_awvalid <= 1'b1;
                            axi_wraddr_current_ctu <= 8'b0000_0001;
                        end
                        else if ((ctu_row_done[1]&(~axi_wraddr_current_ctu[1]))|(last_8x8_block_in_pic[1]&~old_last_8x8_block_in_pic[1])) begin
                            axi_wraddr_state <= AXI_WRADDR_STATE_ACTIVE;
                            axi_awaddr <= frame_ddr_wr_ptr + ctu_offset_reg + ((pic_width_reg[PIXEL_ADDR_LENGTH - 1:6] + 2'd2) << (1+3+6));
                            axi_awvalid <= 1'b1;
                            axi_wraddr_current_ctu <= 8'b0000_0010;
                        end
                        else if ((ctu_row_done[2]&(~axi_wraddr_current_ctu[2]))|(last_8x8_block_in_pic[2]&~old_last_8x8_block_in_pic[2])) begin
                            axi_wraddr_state <= AXI_WRADDR_STATE_ACTIVE;
                            axi_awaddr <= frame_ddr_wr_ptr + ctu_offset_reg + 2*((pic_width_reg[PIXEL_ADDR_LENGTH - 1:6] + 2'd2) << (1+3+6));
                            axi_awvalid <= 1'b1;
                            axi_wraddr_current_ctu <= 8'b0000_0100;
                        end
                        else if ((ctu_row_done[3]&(~axi_wraddr_current_ctu[3]))|(last_8x8_block_in_pic[3]&~old_last_8x8_block_in_pic[3])) begin
                            axi_wraddr_state <= AXI_WRADDR_STATE_ACTIVE;
                            axi_awaddr <= frame_ddr_wr_ptr + ctu_offset_reg + 3*((pic_width_reg[PIXEL_ADDR_LENGTH - 1:6] + 2'd2) << (1+3+6));
                            axi_awvalid <= 1'b1;
                            axi_wraddr_current_ctu <= 8'b0000_1000;
                        end
                        else if ((ctu_row_done[4]&(~axi_wraddr_current_ctu[4]))|(last_8x8_block_in_pic[4]&~old_last_8x8_block_in_pic[4])) begin
                            axi_wraddr_state <= AXI_WRADDR_STATE_ACTIVE;
                            axi_awaddr <= frame_ddr_wr_ptr + ctu_offset_reg + 4*((pic_width_reg[PIXEL_ADDR_LENGTH - 1:6] + 2'd2) << (1+3+6));
                            axi_awvalid <= 1'b1;
                            axi_wraddr_current_ctu <= 8'b0001_0000;
                        end
                        else if ((ctu_row_done[5]&(~axi_wraddr_current_ctu[5]))|(last_8x8_block_in_pic[5]&~old_last_8x8_block_in_pic[5])) begin
                            axi_wraddr_state <= AXI_WRADDR_STATE_ACTIVE;
                            axi_awaddr <= frame_ddr_wr_ptr + ctu_offset_reg + 5*((pic_width_reg[PIXEL_ADDR_LENGTH - 1:6] + 2'd2) << (1+3+6));
                            axi_awvalid <= 1'b1;
                            axi_wraddr_current_ctu <= 8'b0010_0000;
                        end
                        else if ((ctu_row_done[6]&(~axi_wraddr_current_ctu[6]))|(last_8x8_block_in_pic[6]&~old_last_8x8_block_in_pic[6])) begin
                            axi_wraddr_state <= AXI_WRADDR_STATE_ACTIVE;
                            axi_awaddr <= frame_ddr_wr_ptr + ctu_offset_reg + 6*((pic_width_reg[PIXEL_ADDR_LENGTH - 1:6] + 2'd2) << (1+3+6));
                            axi_awvalid <= 1'b1;
                            axi_wraddr_current_ctu <= 8'b0100_0000;
                        end
                        else if ((ctu_row_done[7]&(~axi_wraddr_current_ctu[7]))|(last_8x8_block_in_pic[7]&~old_last_8x8_block_in_pic[7])) begin
                            axi_wraddr_state <= AXI_WRADDR_STATE_ACTIVE;
                            axi_awaddr <= frame_ddr_wr_ptr + ctu_offset_reg + 7*((pic_width_reg[PIXEL_ADDR_LENGTH - 1:6] + 2'd2) << (1+3+6));
                            axi_awvalid <= 1'b1;
                            axi_wraddr_current_ctu <= 8'b1000_0000;
                        end
                    // end
                end
                // AXI_WRADDR_STATE_WAIT : begin
                //     axi_wraddr_state <= AXI_WRADDR_STATE_ACTIVE;
                // end
                AXI_WRADDR_STATE_ACTIVE : begin
                    old_last_8x8_block_in_pic <= last_8x8_block_in_pic;
                    if(axi_wraddr_counter == 3'd7) begin
                        if(axi_awready) begin
                            axi_awvalid <= 1'b0;

                            axi_wraddr_state <= AXI_WRADDR_STATE_PREINIT;
                            axi_wraddr_counter <= 3'd0;
                        end

                        // if(ctu_row_done[1]) begin
                        //     axi_wraddr_counter <= 3'd0;
                        //     // axi_awaddr <= axi_awaddr + (pic_width_reg << 1);
                        //     axi_awvalid <= 1'b1;
                        //     axi_wraddr_state <= AXI_WRADDR_STATE_2ND_8X8;
                        // end
                        // else begin
                        //     axi_awvalid <= 1'b0;
                        // end
                    end
                    else begin
                        if(axi_awready) begin
                            axi_awaddr <= axi_awaddr + ((pic_width_reg[PIXEL_ADDR_LENGTH - 1:6] + 2'd2) << (1+6));
                            axi_awvalid <= 1'b1;
                            axi_wraddr_counter <= axi_wraddr_counter + 1'b1;
                        end
                    end
                end
            endcase
        end
    end

    always @(posedge clk) begin : axi4_write_data_fsm
        if(reset) begin
            axi_wrdata_state <= AXI_WRDATA_STATE_PREINIT;
            display_buffer_inner_y_addr_reg <= {INNER_CTU_ADDR_LENGTH{1'b0}};
            axi_wrdata_counter <= 4'd0;
            axi_wrdata_subcounter <= 4'd0;

            ctu_8by8_read_done_rows_reg <= {MAX_CTU_SIZE_DIV_8{1'b0}};
            current_ctu_row_reg <= 3'd0;
            axi_wrdata_burst_state <= AXI_WRDATA_BURST_STATE_LOWER;
            axi_wstrb <= {64{1'b1}};
            axi_wvalid <= 1'b0;
        end
        else begin
            case(axi_wrdata_burst_state)
                AXI_WRDATA_BURST_STATE_LOWER : begin
                    //axi_wlast <= 1'b0;

                    // if(ctu_x == {PIXEL_ADDR_LENGTH{1'b0}}) begin
                    //     axi_wstrb <= {{(64 - 16){1'b1}},{16{1'b0}}};
                    // end
                    // else begin
                        axi_wstrb <= {64{1'b1}};
                    // end

                    case(axi_wrdata_state)
                        AXI_WRDATA_STATE_PREINIT : begin
                            axi_wlast <= 0;
                            ctu_8by8_read_done_rows_reg <= ~ctu_row_done;

                            if(ctu_row_done[0]) begin
                                display_buffer_inner_y_addr_reg <= 8'd0;
                                axi_wrdata_counter <= 4'd0;
                                axi_wrdata_state <= AXI_WRDATA_STATE_WAIT1;
                                // axi_wrdata_burst_state <= AXI_WRDATA_BURST_STATE_HIGHER;
                                current_ctu_row_reg <= 3'd0;
                               //ctu_8by8_read_done_rows_reg[0] <=  1'b0;
                            end
                            else if (ctu_row_done[1]) begin
                                display_buffer_inner_y_addr_reg <= 8'd8;
                                axi_wrdata_counter <= 4'd0;
                                axi_wrdata_state <= AXI_WRDATA_STATE_WAIT1;
                                // axi_wrdata_burst_state <= AXI_WRDATA_BURST_STATE_HIGHER;
                                current_ctu_row_reg <= 3'd1;
                                //ctu_8by8_read_done_rows_reg[1] <=  1'b0;
                            end
                            else if (ctu_row_done[2]) begin
                                display_buffer_inner_y_addr_reg <= 8'd16;
                                axi_wrdata_counter <= 4'd0;
                                axi_wrdata_state <= AXI_WRDATA_STATE_WAIT1;
                                // axi_wrdata_burst_state <= AXI_WRDATA_BURST_STATE_HIGHER;
                                current_ctu_row_reg <= 3'd2;
                               // ctu_8by8_read_done_rows_reg[2] <=  1'b0;
                            end
                            else if (ctu_row_done[3]) begin
                                display_buffer_inner_y_addr_reg <= 8'd24;
                                axi_wrdata_counter <= 4'd0;
                                axi_wrdata_state <= AXI_WRDATA_STATE_WAIT1;
                                // axi_wrdata_burst_state <= AXI_WRDATA_BURST_STATE_HIGHER;
                                current_ctu_row_reg <= 3'd3;
                                //ctu_8by8_read_done_rows_reg[3] <=  1'b0;
                            end
                            else if (ctu_row_done[4]) begin
                                display_buffer_inner_y_addr_reg <= 8'd32;
                                axi_wrdata_counter <= 4'd0;
                                axi_wrdata_state <= AXI_WRDATA_STATE_WAIT1;
                                // axi_wrdata_burst_state <= AXI_WRDATA_BURST_STATE_HIGHER;
                                current_ctu_row_reg <= 3'd4;
                               // ctu_8by8_read_done_rows_reg[4] <=  1'b0;
                            end
                            else if (ctu_row_done[5]) begin
                                display_buffer_inner_y_addr_reg <= 8'd40;
                                axi_wrdata_counter <= 4'd0;
                                axi_wrdata_state <= AXI_WRDATA_STATE_WAIT1;
                                // axi_wrdata_burst_state <= AXI_WRDATA_BURST_STATE_HIGHER;
                                current_ctu_row_reg <= 3'd5;
                                //ctu_8by8_read_done_rows_reg[5] <=  1'b0;
                            end
                            else if (ctu_row_done[6]) begin
                                display_buffer_inner_y_addr_reg <= 8'd48;
                                axi_wrdata_counter <= 4'd0;
                                axi_wrdata_state <= AXI_WRDATA_STATE_WAIT1;
                                // axi_wrdata_burst_state <= AXI_WRDATA_BURST_STATE_HIGHER;
                                current_ctu_row_reg <= 3'd6;
                                //ctu_8by8_read_done_rows_reg[6] <=  1'b0;
                            end
                            else if (ctu_row_done[7]) begin
                                display_buffer_inner_y_addr_reg <= 8'd56;
                                axi_wrdata_counter <= 4'd0;
                                axi_wrdata_state <= AXI_WRDATA_STATE_WAIT1;
                                // axi_wrdata_burst_state <= AXI_WRDATA_BURST_STATE_HIGHER;
                                current_ctu_row_reg <= 3'd7;
                               // ctu_8by8_read_done_rows_reg[7] <=  1'b0;
                            end
                        end
                        AXI_WRDATA_STATE_WAIT1 : begin
                            ctu_8by8_read_done_rows_reg <= ~ctu_row_done;
                            axi_wrdata_state <= AXI_WRDATA_STATE_INIT;
                            burst_counter <= 0;
                        end
                        AXI_WRDATA_STATE_INIT : begin
                                ctu_8by8_read_done_rows_reg <= ~ctu_row_done;
                                axi_wrdata_counter <= 4'd1;
                                axi_wdata <= dispbuf_ddr_wdata_mem_sel[64 - 1:0];
                                // axi_wdata <= dispbuf_ddr_wdata_wire[64*PIXEL_WIDTH - 1:0*PIXEL_WIDTH];
                                axi_wvalid <= 1'b1;
                                axi_wrdata_state <= AXI_WRDATA_HIGH_BEAT;
                        end
                        AXI_WRDATA_HIGH_BEAT: begin
                           if(axi_wready&axi_wvalid) begin
                              axi_wdata <= dispbuf_ddr_wdata_mem_sel[128 - 1:64];
                              // axi_wdata <= dispbuf_ddr_wdata_wire[128*PIXEL_WIDTH - 1:64*PIXEL_WIDTH];
                              if(burst_counter == 7) begin
                                 display_buffer_inner_y_addr_reg <= display_buffer_inner_y_addr_reg + 1'b1;
                                 axi_wrdata_state <= AXI_WRDATA_STATE_ACTIVE;
                                 axi_wrdata_burst_state <= AXI_WRDATA_BURST_STATE_HIGHER;
                                 axi_wlast <= 1'b1;
                                 burst_counter <= 0;
                              end
                              else begin
                                 axi_wrdata_state <= AXI_WRDATA_LOW_BEAT;
                                 burst_counter <= burst_counter + 1'b1;
                              end   
                           end
                        end
                        AXI_WRDATA_LOW_BEAT: begin
                           if(axi_wready&axi_wvalid) begin
                              axi_wdata <= dispbuf_ddr_wdata_mem_sel[64 - 1:0];
                              axi_wrdata_state <= AXI_WRDATA_HIGH_BEAT;
                           end
                        end
                        AXI_WRDATA_LOW_BEAT2: begin
                           if(axi_wready&axi_wvalid) begin
                              axi_wdata <= dispbuf_ddr_wdata_mem_sel[64 - 1:0];
                              axi_wrdata_state <= AXI_WRDATA_HIGH_BEAT2;
                           end
                        end
                        // AXI_WRDATA_STATE_WAIT2 : begin
                        //     //synthesis
                        //     axi_wrdata_state <= AXI_WRDATA_STATE_ACTIVE;
                        // end
                        AXI_WRDATA_STATE_ACTIVE : begin
                            ctu_8by8_read_done_rows_reg <= ~ctu_row_done;
                            if(axi_wrdata_counter == 4'd8) begin
                                ctu_8by8_read_done_rows_reg[current_ctu_row_reg] <=  1'b1;
                                axi_wrdata_state <= AXI_WRDATA_STATE_DONE;
                                axi_wvalid <= 1'b0;
                            end
                            else begin
                                axi_wdata <= dispbuf_ddr_wdata_mem_sel[64 - 1:0];
                                axi_wvalid <= 1'b1;
                                axi_wrdata_state <= AXI_WRDATA_HIGH_BEAT2;
                            end
                        end
                        AXI_WRDATA_HIGH_BEAT2: begin
                           if(axi_wready&axi_wvalid) begin
                              axi_wdata <= dispbuf_ddr_wdata_mem_sel[128 - 1:64];
                              // axi_wdata <= dispbuf_ddr_wdata_wire[128*PIXEL_WIDTH - 1:64*PIXEL_WIDTH];
                              if(burst_counter == 7) begin
                                 axi_wrdata_counter <= axi_wrdata_counter + 1'b1;
                                 display_buffer_inner_y_addr_reg <= display_buffer_inner_y_addr_reg + 1'b1;
                                 axi_wrdata_burst_state <= AXI_WRDATA_BURST_STATE_HIGHER;
                                 axi_wrdata_state <= AXI_WRDATA_STATE_ACTIVE;
                                 axi_wlast <= 1'b1;
                                 burst_counter <= 0;
                              end
                              else begin
                                 axi_wrdata_state <= AXI_WRDATA_LOW_BEAT2;
                                 burst_counter <= burst_counter + 1'b1;
                              end   
                           end
                        end
                        AXI_WRDATA_STATE_DONE : begin
                            axi_wrdata_state <= AXI_WRDATA_STATE_PREINIT;
                        end
                    endcase
                end
                AXI_WRDATA_BURST_STATE_HIGHER : begin

                 //   axi_wdata <= dispbuf_ddr_wdata_wire[128*PIXEL_WIDTH - 1:64*PIXEL_WIDTH];
                 //   axi_wlast <= 1'b1;
                 //   axi_wstrb <= {64{1'b1}};
                    // axi_wlast <= 1'b0;
                    // axi_wvalid <= 1'b0;
                    // axi_wrdata_burst_state <= AXI_WRDATA_BURST_STATE_LOWER;
                    ctu_8by8_read_done_rows_reg <= ~ctu_row_done;
                    if(axi_wready) begin
                        axi_wrdata_burst_state <= AXI_WRDATA_BURST_STATE_LOWER;
                        axi_wlast <= 1'b0;
                        axi_wvalid <= 1'b0;
                        burst_counter <= 0;
                        // axi_wvalid <= 1'b1;
                    end
                    else begin
                        // axi_wvalid <= 1'b0;
                    end
                end
                // AXI_WRDATA_BURST_STATE_WAIT1 : begin

                // end
                // AXI_WRDATA_BURST_STATE_WAIT2 : begin

                // end
            endcase
        end
    end

    always @(posedge clk) begin : axi4_read_addr_fsm
        if(reset) begin
            axi_rdaddr_state <= AXI_RDADDR_STATE_LENGTH_CAL;
            poc_read <= 32'd0;
            frame_ddr_rd_done <= 1'b0;
            frame_row_counter_addr <= {PIXEL_ADDR_LENGTH{1'b0}};
            frame_col_counter_addr <= {PIXEL_ADDR_LENGTH{1'b0}};
            axi_arvalid <= 1'b0;
            poc_not_available <= 'd0;
            initial_poc_0 <= 0;
            frame_store_idx_mem_addr_r <= 'd0;
            axi_araddr_line_start <= 'd0; 
        end
        else begin
            case(axi_rdaddr_state)
                AXI_RDADDR_STATE_LENGTH_CAL: begin
                  if(displaybuffer_fifo_read_en_out) begin
                     frame_col_counter_addr <= {PIXEL_ADDR_LENGTH{1'b0}};
                     if(((pic_width_reg >> (axi_arsize-1)) > MAX_BURST_LEN)) begin
                        axi_arlen_start <= MAX_BURST_LEN - 1'b1;
                     end
                     else begin
                        axi_arlen_start <= (pic_width_reg >> (axi_arsize-1)) -1;
                     end
                     axi_rdaddr_state <= AXI_RDADDR_STATE_INIT;
                  end
                end
                AXI_RDADDR_STATE_INIT : begin      // tries to find a location with matching poc and valid. If all are valid and still no matching poc is found, then increment poc. If at least one is invalid and stream the same old picture
                     if(frame_store_valids_read[frame_row_counter_addr] == 1) begin
                        if(frame_store_poc_mem[read_stream_id*MAX_MAX_STORE_IDX + frame_row_counter_addr] == poc_read ) begin
                           frame_store_read_pointer <= frame_row_counter_addr;
                           old_frame_store_read_pointer <= frame_store_read_pointer;
                           axi_rdaddr_state <= AXI_RDADDR_STATE_ACTIVE;
                           axi_arlen <= axi_arlen_start;
                           frame_col_counter_addr <= (axi_arlen_start + 1'b1) << (axi_arsize-1);
                           poc_read <= poc_read + 1;
                           frame_row_counter_addr <= 'd0;
                           frame_store_idx_mem_addr_r <= read_stream_id*MAX_MAX_STORE_IDX + 0;
                           axi_araddr <= frame_store_idx_mem_rdata_r;
                           axi_araddr_line_start <= frame_store_idx_mem_rdata_r;
                           axi_arvalid<= 1'b1;
                           frame_ddr_rd_done <= initial_poc_0;                        
                        end
                        else begin
                           frame_row_counter_addr <= frame_row_counter_addr + 1;
                           frame_store_idx_mem_addr_r <= read_stream_id*MAX_MAX_STORE_IDX + frame_row_counter_addr + 1;
                           poc_not_available[frame_row_counter_addr] <= 1;
                           if(frame_row_counter_addr == max_frame_store_idx -1) begin
                              frame_row_counter_addr <= 0;
                              frame_store_idx_mem_addr_r <= read_stream_id*MAX_MAX_STORE_IDX + 0;
                              if(&poc_not_available == 1) begin
                                 poc_read <= poc_read + 1;
                                 $display("frame read busy!!");
                                 $stop();
                              end
                              else begin
                                 if(initial_poc_0) begin
                                    frame_ddr_rd_done <= 0;
                                    axi_rdaddr_state <= AXI_RDADDR_STATE_ACTIVE;
                                    frame_col_counter_addr <= (axi_arlen_start + 1'b1) << (axi_arsize-1);
                                    axi_arlen <= axi_arlen_start;
                                    frame_row_counter_addr <= 'd0;
                                    frame_store_idx_mem_addr_r <= read_stream_id*MAX_MAX_STORE_IDX + 0;
                                    axi_arvalid<= 1'b1;
                                 end
                              end
                           end
                        end
                     end
                     else begin
                        frame_row_counter_addr <= frame_row_counter_addr + 1;
                        frame_store_idx_mem_addr_r <= read_stream_id*MAX_MAX_STORE_IDX + frame_row_counter_addr + 1;
                        poc_not_available <= 0;
                        if(frame_row_counter_addr == max_frame_store_idx -1) begin
                           frame_row_counter_addr <= 0;
                           frame_store_idx_mem_addr_r <= read_stream_id*MAX_MAX_STORE_IDX + 0;
                           if(&poc_not_available == 1) begin
                              poc_read <= poc_read + 1;
                              $display("frame read busy!!");
                              $stop();
                           end
                           else begin
                              if(initial_poc_0) begin
                                 frame_ddr_rd_done <= 0;
                                 axi_rdaddr_state <= AXI_RDADDR_STATE_ACTIVE;
                                 frame_col_counter_addr <= (axi_arlen_start + 1'b1) << (axi_arsize-1);
                                 axi_arlen <= axi_arlen_start;
                                 frame_row_counter_addr <= 'd0;
                                 frame_store_idx_mem_addr_r <= read_stream_id*MAX_MAX_STORE_IDX + 0;
                                 axi_arvalid<= 1'b1;
                              end
                           end
                        end
                     end
                end
                AXI_RDADDR_STATE_ACTIVE: begin
                    frame_ddr_rd_done <= 0;
                    if(axi_arready == 1'b1) begin
                       if(frame_col_counter_addr == pic_width_reg) begin
                          if(frame_row_counter_addr == pic_height_reg - 1'b1) begin
                              
                                axi_arvalid <= 1'b0;
                                axi_rdaddr_state <= AXI_RDADDR_STATE_DONE_WAIT;
                          end
                          else begin
                                  axi_araddr <= axi_araddr_line_start + ((pic_width_reg[PIXEL_ADDR_LENGTH - 1:6] + 2'd2) << (1+6));
                                  axi_araddr_line_start <= axi_araddr_line_start + ((pic_width_reg[PIXEL_ADDR_LENGTH - 1:6] + 2'd2) << (1+6));
                                  frame_row_counter_addr <= frame_row_counter_addr + 1'b1;
                                  axi_arlen <= axi_arlen_start;
                                  frame_col_counter_addr <= (axi_arlen_start + 1'b1) << (axi_arsize-1);
                          end
                       end
                       else begin
                              axi_araddr <= axi_araddr + (MAX_BURST_LEN * (1<<axi_arsize));  // byte address increase
                              if(((pic_width_reg >> (axi_arsize-1)) > (MAX_BURST_LEN + (frame_col_counter_addr>>(axi_arsize-1))))) begin
                                 axi_arlen <= MAX_BURST_LEN - 1'b1;
                                 frame_col_counter_addr <= frame_col_counter_addr + ((MAX_BURST_LEN) << (axi_arsize-1));
                              end
                              else begin
                                 axi_arlen <= ((pic_width_reg - frame_col_counter_addr) >> (axi_arsize-1)) -1;
                                 frame_col_counter_addr <= pic_width_reg;
                              end
                       end
                    end
                end
                AXI_RDADDR_STATE_DONE_WAIT : begin
                    if((frame_row_counter_data == frame_row_counter_addr)&&(axi_rddata_state == AXI_RDDATA_WAIT_FOR_LAST)) begin
                        axi_rdaddr_state <= AXI_RDADDR_STATE_DONE;
                        frame_row_counter_addr <= frame_store_read_pointer;
                        frame_store_idx_mem_addr_r <= read_stream_id*MAX_MAX_STORE_IDX + frame_store_read_pointer;
                    end
                end
                AXI_RDADDR_STATE_DONE : begin
                    axi_rdaddr_state <= AXI_RDADDR_STATE_INIT; 
                    axi_araddr <= frame_store_idx_mem_rdata_r;
                    axi_araddr_line_start <= frame_store_idx_mem_rdata_r;
                    initial_poc_0 <= 1;
                  frame_row_counter_addr <= 'd0;
                  frame_store_idx_mem_addr_r <= read_stream_id*MAX_MAX_STORE_IDX + 'd0;
                end
            endcase
        end
    end

    always @(posedge clk) begin : axi4_read_data_fsm
        if(reset) begin
            axi_rddata_state <= AXI_RDDATA_STATE_ACTIVE_0;
            axi_rready <= 1'b0;
            axi_rddata_counter <= 3'd0;
            frame_row_counter_data <= {PIXEL_ADDR_LENGTH{1'b0}};
            frame_column_counter <= {PIXEL_ADDR_LENGTH{1'b0}};
            hdmi_fifo_w_en <= 1'b0;
        end
        else begin
            case(axi_rddata_state)
                AXI_RDDATA_STATE_ACTIVE_0 : begin
                    axi_rready <= 1'b0;
                     if(axi_rvalid) begin
                        if(~hdmi_fifo_almost_full) begin
                           frame_column_counter <= frame_column_counter + 4'd4;
                           hdmi_fifo_w_en <= 1'b1;
                           axi_rddata_counter <= axi_rddata_counter + 1'b1;
                           case(axi_rddata_counter)
                                4'd0 : begin
                                    hdmi_fifo_data_out <= axi_rdata[8*PIXEL_WIDTH - 1:0];
                                    axi_rddata_state   <= AXI_RDDATA_STATE_DONE;
                                    axi_rready <= 1'b1;
                                end
                                // 4'd1 : begin
                                    // hdmi_fifo_data_out <= axi_rdata[(1+1)*8*PIXEL_WIDTH - 1:1*8*PIXEL_WIDTH];
                                    // frame_column_counter <= frame_column_counter + 1'b1;
                                // end
                                // 4'd2 : begin
                                    // hdmi_fifo_data_out <= axi_rdata[(2+1)*8*PIXEL_WIDTH - 1:2*8*PIXEL_WIDTH];
                                    // frame_column_counter <= frame_column_counter + 1'b1;
                                // end
                                // 4'd3 : begin
                                    // hdmi_fifo_data_out <= axi_rdata[(3+1)*8*PIXEL_WIDTH - 1:3*8*PIXEL_WIDTH];
                                    // frame_column_counter <= frame_column_counter + 1'b1;
                                // end
                                // 4'd4 : begin
                                    // hdmi_fifo_data_out <= axi_rdata[(4+1)*8*PIXEL_WIDTH - 1:4*8*PIXEL_WIDTH];
                                    // frame_column_counter <= frame_column_counter + 1'b1;
                                // end
                                // 4'd5 : begin
                                    // hdmi_fifo_data_out <= axi_rdata[(5+1)*8*PIXEL_WIDTH - 1:5*8*PIXEL_WIDTH];
                                    // frame_column_counter <= frame_column_counter + 1'b1;
                                // end
                                // 4'd6 : begin
                                    // hdmi_fifo_data_out <= axi_rdata[(6+1)*8*PIXEL_WIDTH - 1:6*8*PIXEL_WIDTH];
                                    // frame_column_counter <= frame_column_counter + 1'b1;
                                // end
                                // 4'd7 : begin
                                    // hdmi_fifo_data_out <= axi_rdata[(7+1)*8*PIXEL_WIDTH - 1:7*8*PIXEL_WIDTH];
                                    // frame_column_counter <= frame_column_counter + 1'b1;
                                    // axi_rddata_state   <= AXI_RDDATA_STATE_DONE;
                                    // axi_rready <= 1'b1;
                                // end                           
                           endcase 
                        end
                        else begin
                           hdmi_fifo_w_en <= 1'b0;
                        end
                     end
                     else begin
                        hdmi_fifo_w_en <= 1'b0;
                     end

                    // if(frame_ddr_rd_done == 1'b1) begin
                        // frame_row_counter_data <= {PIXEL_ADDR_LENGTH{1'b0}};
                    // end
                end
                
                AXI_RDDATA_STATE_DONE : begin
                    axi_rddata_counter <= 0;
                    axi_rready <= 1'b0;
                    hdmi_fifo_w_en <= 1'b0;
                    if(frame_column_counter == pic_width_reg) begin
                        axi_rddata_state <= AXI_RDDATA_WAIT_FOR_LAST;
                        frame_column_counter <= 'd0;
                    end
                    else begin
                        axi_rddata_state <= AXI_RDDATA_STATE_ACTIVE_0;
                    end
                end
                AXI_RDDATA_WAIT_FOR_LAST : begin
                        axi_rready <= 1'b0;

                        if(frame_row_counter_data == pic_height_reg - 1'b1) begin
                            frame_row_counter_data <= {PIXEL_ADDR_LENGTH{1'b0}};
                        end
                        else begin
                            frame_row_counter_data <= frame_row_counter_data + 1'b1;
                        end

                        hdmi_fifo_w_en <= 1'b0;
                        axi_rddata_state <= AXI_RDDATA_STATE_ACTIVE_0;
                end
            endcase
        end
    end



    truedual_port_blockram#(
        .DATA_WIDTH(2*8*PIXEL_WIDTH),
        .ADDR_WIDTH(INNER_CTU_ADDR_LENGTH)
    )
    truedual_port_blockram_block_0
    (
        .clk                (clk),

        .porta_en_in        (1'b1),
        .porta_addr_in      (blockram0_porta_addr_reg),
        .porta_wen_in       (blockram_porta_wen_reg),
        .porta_wdata_in     (blockram0_porta_wdata_reg),
        .porta_rdata_out    (),

        .portb_en_in        (1'b1),
        .portb_addr_in      (display_buffer_inner_y_addr_reg),
        .portb_wen_in       (1'b0),
        .portb_wdata_in     (),
        .portb_rdata_out    (blockram_portb_rdata_wire_arr[0])
    );

    truedual_port_blockram#(
        .DATA_WIDTH(2*8*PIXEL_WIDTH),
        .ADDR_WIDTH(INNER_CTU_ADDR_LENGTH)
    )
    truedual_port_blockram_block_1
    (
        .clk                (clk),

        .porta_en_in        (1'b1),
        .porta_addr_in      (blockram1_porta_addr_reg),
        .porta_wen_in       (blockram_porta_wen_reg),
        .porta_wdata_in     (blockram1_porta_wdata_reg),
        .porta_rdata_out    (),

        .portb_en_in        (1'b1),
        .portb_addr_in      (display_buffer_inner_y_addr_reg),
        .portb_wen_in       (1'b0),
        .portb_wdata_in     (),
        .portb_rdata_out    (blockram_portb_rdata_wire_arr[1])
    );

    truedual_port_blockram#(
        .DATA_WIDTH(2*8*PIXEL_WIDTH),
        .ADDR_WIDTH(INNER_CTU_ADDR_LENGTH)
    )
    truedual_port_blockram_block_2
    (
        .clk                (clk),

        .porta_en_in        (1'b1),
        .porta_addr_in      (blockram2_porta_addr_reg),
        .porta_wen_in       (blockram_porta_wen_reg),
        .porta_wdata_in     (blockram2_porta_wdata_reg),
        .porta_rdata_out    (),

        .portb_en_in        (1'b1),
        .portb_addr_in      (display_buffer_inner_y_addr_reg),
        .portb_wen_in       (1'b0),
        .portb_wdata_in     (),
        .portb_rdata_out    (blockram_portb_rdata_wire_arr[2])
    );

    truedual_port_blockram#(
        .DATA_WIDTH(2*8*PIXEL_WIDTH),
        .ADDR_WIDTH(INNER_CTU_ADDR_LENGTH)
    )
    truedual_port_blockram_block_3
    (
        .clk                (clk),

        .porta_en_in        (1'b1),
        .porta_addr_in      (blockram3_porta_addr_reg),
        .porta_wen_in       (blockram_porta_wen_reg),
        .porta_wdata_in     (blockram3_porta_wdata_reg),
        .porta_rdata_out    (),

        .portb_en_in        (1'b1),
        .portb_addr_in      (display_buffer_inner_y_addr_reg),
        .portb_wen_in       (1'b0),
        .portb_wdata_in     (),
        .portb_rdata_out    (blockram_portb_rdata_wire_arr[3])
    );

    truedual_port_blockram#(
        .DATA_WIDTH(2*8*PIXEL_WIDTH),
        .ADDR_WIDTH(INNER_CTU_ADDR_LENGTH)
    )
    truedual_port_blockram_block_4
    (
        .clk                (clk),

        .porta_en_in        (1'b1),
        .porta_addr_in      (blockram4_porta_addr_reg),
        .porta_wen_in       (blockram_porta_wen_reg),
        .porta_wdata_in     (blockram4_porta_wdata_reg),
        .porta_rdata_out    (),

        .portb_en_in        (1'b1),
        .portb_addr_in      (display_buffer_inner_y_addr_reg),
        .portb_wen_in       (1'b0),
        .portb_wdata_in     (),
        .portb_rdata_out    (blockram_portb_rdata_wire_arr[4])
    );

    truedual_port_blockram#(
        .DATA_WIDTH(2*8*PIXEL_WIDTH),
        .ADDR_WIDTH(INNER_CTU_ADDR_LENGTH)
    )
    truedual_port_blockram_block_5
    (
        .clk                (clk),

        .porta_en_in        (1'b1),
        .porta_addr_in      (blockram5_porta_addr_reg),
        .porta_wen_in       (blockram_porta_wen_reg),
        .porta_wdata_in     (blockram5_porta_wdata_reg),
        .porta_rdata_out    (),

        .portb_en_in        (1'b1),
        .portb_addr_in      (display_buffer_inner_y_addr_reg),
        .portb_wen_in       (1'b0),
        .portb_wdata_in     (),
        .portb_rdata_out    (blockram_portb_rdata_wire_arr[5])
    );

    truedual_port_blockram#(
        .DATA_WIDTH(2*8*PIXEL_WIDTH),
        .ADDR_WIDTH(INNER_CTU_ADDR_LENGTH)
    )
    truedual_port_blockram_block_6
    (
        .clk                (clk),

        .porta_en_in        (1'b1),
        .porta_addr_in      (blockram6_porta_addr_reg),
        .porta_wen_in       (blockram_porta_wen_reg),
        .porta_wdata_in     (blockram6_porta_wdata_reg),
        .porta_rdata_out    (),

        .portb_en_in        (1'b1),
        .portb_addr_in      (display_buffer_inner_y_addr_reg),
        .portb_wen_in       (1'b0),
        .portb_wdata_in     (),
        .portb_rdata_out    (blockram_portb_rdata_wire_arr[6])
    );

    truedual_port_blockram#(
        .DATA_WIDTH(2*8*PIXEL_WIDTH),
        .ADDR_WIDTH(INNER_CTU_ADDR_LENGTH)
    )
    truedual_port_blockram_block_7
    (
        .clk                (clk),

        .porta_en_in        (1'b1),
        .porta_addr_in      (blockram7_porta_addr_reg),
        .porta_wen_in       (blockram_porta_wen_reg),
        .porta_wdata_in     (blockram7_porta_wdata_reg),
        .porta_rdata_out    (),

        .portb_en_in        (1'b1),
        .portb_addr_in      (display_buffer_inner_y_addr_reg),
        .portb_wen_in       (1'b0),
        .portb_wdata_in     (),
        .portb_rdata_out    (blockram_portb_rdata_wire_arr[7])
    );


//ASSERTIONS

 always @(posedge clk) begin : proc
    //synthesis translate_off
        if (axi_awaddr < ( frame_store_idx_mem[0] - 16) ) begin
            $stop();
        end
    //synthesis translate_on
 end





endmodule