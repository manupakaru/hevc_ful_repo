// (c) Copyright 1995-2015 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:user:cabac_ngc:1.14
// IP Revision: 12

// Forward declaration of the netlist blackbox
(* black_box = "1" *)
module global_top (
  clk,
  rst,
  axi_arlock,
  axi_arvalid,
  axi_awlock,
  axi_awvalid,
  axi_bready,
  axi_rready,
  axi_wlast,
  axi_wvalid,
  fullCont,
  fullY,
  fullCb,
  fullCr,
  axi_arready,
  axi_awready,
  axi_bvalid,
  axi_rlast,
  axi_rvalid,
  axi_wready,
  validCont,
  validY,
  validCb,
  validCr,
  input_hevc_full_out_test,
  input_hevc_empty_out_test,
  input_hevc_almost_empty_out_test,
  push_full_out_test,
  push_empty_out_test,
  trafo_clk_en,
  cabac_to_res_wr_en,
  push_empty,
  push_read,
  axi_araddr,
  axi_arburst,
  axi_arcache,
  axi_arid,
  axi_arlen,
  axi_arprot,
  axi_arsize,
  axi_awaddr,
  axi_awburst,
  axi_awcache,
  axi_awid,
  axi_awlen,
  axi_awprot,
  axi_awsize,
  axi_wdata,
  axi_wstrb,
  axi_bid,
  axi_bresp,
  axi_rdata,
  axi_rid,
  axi_rresp,
  outCont,
  outY,
  outCb,
  outCr,
  B_test,
  cu_state,
  ru_state,
  total_push,
  total_config,
  total_y,
  total_cb,
  total_cr,
  cu_x0,
  cu_y0,
  cu_other,
  trafo_x0,
  trafo_y0,
  trafo_POC,
  axi_count,
  axi_data,
  axi_addr,
  axi_state,
  cabac_to_res_data_count,
  input_hevc_data_count,
  cabac_to_res_data,
  push_data_out,
  port5,
  port6,
  port7,
  port8,
  port9
);
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 signal_clock CLK" *)
input wire clk;
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 signal_reset RST" *)
input wire rst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARLOCK" *)
input wire axi_arlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARVALID" *)
input wire axi_arvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWLOCK" *)
input wire axi_awlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWVALID" *)
input wire axi_awvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi BREADY" *)
input wire axi_bready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RREADY" *)
input wire axi_rready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WLAST" *)
input wire axi_wlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WVALID" *)
input wire axi_wvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_cont FULL" *)
input wire fullCont;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_y FULL" *)
input wire fullY;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_cb FULL" *)
input wire fullCb;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_cr FULL" *)
input wire fullCr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARREADY" *)
output wire axi_arready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWREADY" *)
output wire axi_awready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi BVALID" *)
output wire axi_bvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RLAST" *)
output wire axi_rlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RVALID" *)
output wire axi_rvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WREADY" *)
output wire axi_wready;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_cont WR_EN" *)
output wire validCont;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_y WR_EN" *)
output wire validY;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_cb WR_EN" *)
output wire validCb;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_cr WR_EN" *)
output wire validCr;
output wire input_hevc_full_out_test;
output wire input_hevc_empty_out_test;
output wire input_hevc_almost_empty_out_test;
output wire push_full_out_test;
output wire push_empty_out_test;
output wire trafo_clk_en;
output wire cabac_to_res_wr_en;
output wire push_empty;
output wire push_read;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARADDR" *)
input wire [15 : 0] axi_araddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARBURST" *)
input wire [1 : 0] axi_arburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARCACHE" *)
input wire [3 : 0] axi_arcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARID" *)
input wire [11 : 0] axi_arid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARLEN" *)
input wire [7 : 0] axi_arlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARPROT" *)
input wire [2 : 0] axi_arprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARSIZE" *)
input wire [2 : 0] axi_arsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWADDR" *)
input wire [15 : 0] axi_awaddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWBURST" *)
input wire [1 : 0] axi_awburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWCACHE" *)
input wire [3 : 0] axi_awcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWID" *)
input wire [11 : 0] axi_awid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWLEN" *)
input wire [7 : 0] axi_awlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWPROT" *)
input wire [2 : 0] axi_awprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWSIZE" *)
input wire [2 : 0] axi_awsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WDATA" *)
input wire [31 : 0] axi_wdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WSTRB" *)
input wire [3 : 0] axi_wstrb;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi BID" *)
output wire [11 : 0] axi_bid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi BRESP" *)
output wire [1 : 0] axi_bresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RDATA" *)
output wire [31 : 0] axi_rdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RID" *)
output wire [11 : 0] axi_rid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RRESP" *)
output wire [1 : 0] axi_rresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_cont WR_DATA" *)
output wire [31 : 0] outCont;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_y WR_DATA" *)
output wire [143 : 0] outY;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_cb WR_DATA" *)
output wire [143 : 0] outCb;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_cr WR_DATA" *)
output wire [143 : 0] outCr;
output wire [31 : 0] B_test;
output wire [6 : 0] cu_state;
output wire [4 : 0] ru_state;
output wire [31 : 0] total_push;
output wire [31 : 0] total_config;
output wire [31 : 0] total_y;
output wire [31 : 0] total_cb;
output wire [31 : 0] total_cr;
output wire [11 : 0] cu_x0;
output wire [11 : 0] cu_y0;
output wire [255 : 0] cu_other;
output wire [11 : 0] trafo_x0;
output wire [11 : 0] trafo_y0;
output wire [15 : 0] trafo_POC;
output wire [31 : 0] axi_count;
output wire [31 : 0] axi_data;
output wire [15 : 0] axi_addr;
output wire [2 : 0] axi_state;
output wire [9 : 0] cabac_to_res_data_count;
output wire [13 : 0] input_hevc_data_count;
output wire [31 : 0] cabac_to_res_data;
output wire [31 : 0] push_data_out;
output wire [27 : 0] port5;
output wire [27 : 0] port6;
output wire [26 : 0] port7;
output wire [71 : 0] port8;
output wire [5 : 0] port9;
endmodule
// End of netlist blackbox

(* X_CORE_INFO = "global_top,Vivado 2014.2" *)
(* CHECK_LICENSE_TYPE = "zynq_design_1_cabac_ngc_0_0,global_top,{}" *)
(* CORE_GENERATION_INFO = "zynq_design_1_cabac_ngc_0_0,global_top,{x_ipProduct=Vivado 2014.2,x_ipVendor=xilinx.com,x_ipLibrary=user,x_ipName=cabac_ngc,x_ipVersion=1.14,x_ipCoreRevision=12,x_ipLanguage=VERILOG}" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module zynq_design_1_cabac_ngc_0_0 (
  clk,
  rst,
  axi_arlock,
  axi_arvalid,
  axi_awlock,
  axi_awvalid,
  axi_bready,
  axi_rready,
  axi_wlast,
  axi_wvalid,
  fullCont,
  fullY,
  fullCb,
  fullCr,
  axi_arready,
  axi_awready,
  axi_bvalid,
  axi_rlast,
  axi_rvalid,
  axi_wready,
  validCont,
  validY,
  validCb,
  validCr,
  input_hevc_full_out_test,
  input_hevc_empty_out_test,
  input_hevc_almost_empty_out_test,
  push_full_out_test,
  push_empty_out_test,
  trafo_clk_en,
  cabac_to_res_wr_en,
  push_empty,
  push_read,
  axi_araddr,
  axi_arburst,
  axi_arcache,
  axi_arid,
  axi_arlen,
  axi_arprot,
  axi_arsize,
  axi_awaddr,
  axi_awburst,
  axi_awcache,
  axi_awid,
  axi_awlen,
  axi_awprot,
  axi_awsize,
  axi_wdata,
  axi_wstrb,
  axi_bid,
  axi_bresp,
  axi_rdata,
  axi_rid,
  axi_rresp,
  outCont,
  outY,
  outCb,
  outCr,
  B_test,
  cu_state,
  ru_state,
  total_push,
  total_config,
  total_y,
  total_cb,
  total_cr,
  cu_x0,
  cu_y0,
  cu_other,
  trafo_x0,
  trafo_y0,
  trafo_POC,
  axi_count,
  axi_data,
  axi_addr,
  axi_state,
  cabac_to_res_data_count,
  input_hevc_data_count,
  cabac_to_res_data,
  push_data_out,
  port5,
  port6,
  port7,
  port8,
  port9
);

(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 signal_clock CLK" *)
input wire clk;
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 signal_reset RST" *)
input wire rst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARLOCK" *)
input wire axi_arlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARVALID" *)
input wire axi_arvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWLOCK" *)
input wire axi_awlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWVALID" *)
input wire axi_awvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi BREADY" *)
input wire axi_bready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RREADY" *)
input wire axi_rready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WLAST" *)
input wire axi_wlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WVALID" *)
input wire axi_wvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_cont FULL" *)
input wire fullCont;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_y FULL" *)
input wire fullY;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_cb FULL" *)
input wire fullCb;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_cr FULL" *)
input wire fullCr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARREADY" *)
output wire axi_arready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWREADY" *)
output wire axi_awready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi BVALID" *)
output wire axi_bvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RLAST" *)
output wire axi_rlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RVALID" *)
output wire axi_rvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WREADY" *)
output wire axi_wready;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_cont WR_EN" *)
output wire validCont;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_y WR_EN" *)
output wire validY;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_cb WR_EN" *)
output wire validCb;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_cr WR_EN" *)
output wire validCr;
output wire input_hevc_full_out_test;
output wire input_hevc_empty_out_test;
output wire input_hevc_almost_empty_out_test;
output wire push_full_out_test;
output wire push_empty_out_test;
output wire trafo_clk_en;
output wire cabac_to_res_wr_en;
output wire push_empty;
output wire push_read;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARADDR" *)
input wire [15 : 0] axi_araddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARBURST" *)
input wire [1 : 0] axi_arburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARCACHE" *)
input wire [3 : 0] axi_arcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARID" *)
input wire [11 : 0] axi_arid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARLEN" *)
input wire [7 : 0] axi_arlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARPROT" *)
input wire [2 : 0] axi_arprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARSIZE" *)
input wire [2 : 0] axi_arsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWADDR" *)
input wire [15 : 0] axi_awaddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWBURST" *)
input wire [1 : 0] axi_awburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWCACHE" *)
input wire [3 : 0] axi_awcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWID" *)
input wire [11 : 0] axi_awid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWLEN" *)
input wire [7 : 0] axi_awlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWPROT" *)
input wire [2 : 0] axi_awprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWSIZE" *)
input wire [2 : 0] axi_awsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WDATA" *)
input wire [31 : 0] axi_wdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WSTRB" *)
input wire [3 : 0] axi_wstrb;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi BID" *)
output wire [11 : 0] axi_bid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi BRESP" *)
output wire [1 : 0] axi_bresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RDATA" *)
output wire [31 : 0] axi_rdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RID" *)
output wire [11 : 0] axi_rid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RRESP" *)
output wire [1 : 0] axi_rresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_cont WR_DATA" *)
output wire [31 : 0] outCont;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_y WR_DATA" *)
output wire [143 : 0] outY;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_cb WR_DATA" *)
output wire [143 : 0] outCb;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 fifo_cr WR_DATA" *)
output wire [143 : 0] outCr;
output wire [31 : 0] B_test;
output wire [6 : 0] cu_state;
output wire [4 : 0] ru_state;
output wire [31 : 0] total_push;
output wire [31 : 0] total_config;
output wire [31 : 0] total_y;
output wire [31 : 0] total_cb;
output wire [31 : 0] total_cr;
output wire [11 : 0] cu_x0;
output wire [11 : 0] cu_y0;
output wire [255 : 0] cu_other;
output wire [11 : 0] trafo_x0;
output wire [11 : 0] trafo_y0;
output wire [15 : 0] trafo_POC;
output wire [31 : 0] axi_count;
output wire [31 : 0] axi_data;
output wire [15 : 0] axi_addr;
output wire [2 : 0] axi_state;
output wire [9 : 0] cabac_to_res_data_count;
output wire [13 : 0] input_hevc_data_count;
output wire [31 : 0] cabac_to_res_data;
output wire [31 : 0] push_data_out;
output wire [27 : 0] port5;
output wire [27 : 0] port6;
output wire [26 : 0] port7;
output wire [71 : 0] port8;
output wire [5 : 0] port9;

  global_top inst (
    .clk(clk),
    .rst(rst),
    .axi_arlock(axi_arlock),
    .axi_arvalid(axi_arvalid),
    .axi_awlock(axi_awlock),
    .axi_awvalid(axi_awvalid),
    .axi_bready(axi_bready),
    .axi_rready(axi_rready),
    .axi_wlast(axi_wlast),
    .axi_wvalid(axi_wvalid),
    .fullCont(fullCont),
    .fullY(fullY),
    .fullCb(fullCb),
    .fullCr(fullCr),
    .axi_arready(axi_arready),
    .axi_awready(axi_awready),
    .axi_bvalid(axi_bvalid),
    .axi_rlast(axi_rlast),
    .axi_rvalid(axi_rvalid),
    .axi_wready(axi_wready),
    .validCont(validCont),
    .validY(validY),
    .validCb(validCb),
    .validCr(validCr),
    .input_hevc_full_out_test(input_hevc_full_out_test),
    .input_hevc_empty_out_test(input_hevc_empty_out_test),
    .input_hevc_almost_empty_out_test(input_hevc_almost_empty_out_test),
    .push_full_out_test(push_full_out_test),
    .push_empty_out_test(push_empty_out_test),
    .trafo_clk_en(trafo_clk_en),
    .cabac_to_res_wr_en(cabac_to_res_wr_en),
    .push_empty(push_empty),
    .push_read(push_read),
    .axi_araddr(axi_araddr),
    .axi_arburst(axi_arburst),
    .axi_arcache(axi_arcache),
    .axi_arid(axi_arid),
    .axi_arlen(axi_arlen),
    .axi_arprot(axi_arprot),
    .axi_arsize(axi_arsize),
    .axi_awaddr(axi_awaddr),
    .axi_awburst(axi_awburst),
    .axi_awcache(axi_awcache),
    .axi_awid(axi_awid),
    .axi_awlen(axi_awlen),
    .axi_awprot(axi_awprot),
    .axi_awsize(axi_awsize),
    .axi_wdata(axi_wdata),
    .axi_wstrb(axi_wstrb),
    .axi_bid(axi_bid),
    .axi_bresp(axi_bresp),
    .axi_rdata(axi_rdata),
    .axi_rid(axi_rid),
    .axi_rresp(axi_rresp),
    .outCont(outCont),
    .outY(outY),
    .outCb(outCb),
    .outCr(outCr),
    .B_test(B_test),
    .cu_state(cu_state),
    .ru_state(ru_state),
    .total_push(total_push),
    .total_config(total_config),
    .total_y(total_y),
    .total_cb(total_cb),
    .total_cr(total_cr),
    .cu_x0(cu_x0),
    .cu_y0(cu_y0),
    .cu_other(cu_other),
    .trafo_x0(trafo_x0),
    .trafo_y0(trafo_y0),
    .trafo_POC(trafo_POC),
    .axi_count(axi_count),
    .axi_data(axi_data),
    .axi_addr(axi_addr),
    .axi_state(axi_state),
    .cabac_to_res_data_count(cabac_to_res_data_count),
    .input_hevc_data_count(input_hevc_data_count),
    .cabac_to_res_data(cabac_to_res_data),
    .push_data_out(push_data_out),
    .port5(port5),
    .port6(port6),
    .port7(port7),
    .port8(port8),
    .port9(port9)
  );
endmodule
