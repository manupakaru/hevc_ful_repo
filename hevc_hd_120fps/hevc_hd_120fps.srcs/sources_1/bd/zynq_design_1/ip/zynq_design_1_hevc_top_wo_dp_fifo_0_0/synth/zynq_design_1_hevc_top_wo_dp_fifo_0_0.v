// (c) Copyright 1995-2015 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:ip:hevc_top_wo_dp_fifo:2.1
// IP Revision: 17

// Forward declaration of the netlist blackbox
(* black_box = "1" *)
module hevc_top_wo_fifo_wo_dp_fifo (
  clk,
  reset,
  enable,
  input_fifo_is_empty,
  y_residual_fifo_is_empty_in,
  cb_residual_fifo_is_empty_in,
  cr_residual_fifo_is_empty_in,
  display_fifo_data_wr_en,
  mv_col_axi_awready,
  mv_col_axi_wready,
  mv_col_axi_bvalid,
  mv_pref_axi_arready,
  mv_pref_axi_rlast,
  mv_pref_axi_rvalid,
  ref_pix_axi_ar_ready,
  ref_pix_axi_r_last,
  ref_pix_axi_r_valid,
  ref_pic_write_axi_awready,
  ref_pic_write_axi_wready,
  ref_pic_write_axi_bid,
  ref_pic_write_axi_bvalid,
  read_en_out,
  y_residual_fifo_is_empty_out,
  y_residual_fifo_read_en_out,
  cb_residual_fifo_is_empty_out,
  cb_residual_fifo_read_en_out,
  cr_residual_fifo_is_empty_out,
  cr_residual_fifo_read_en_out,
  display_fifo_is_full,
  mv_col_axi_awid,
  mv_col_axi_awlock,
  mv_col_axi_awvalid,
  mv_col_axi_wlast,
  mv_col_axi_wvalid,
  mv_col_axi_bready,
  mv_pref_axi_arvalid,
  mv_pref_axi_rready,
  mv_pref_axi_arlock,
  mv_pref_axi_arid,
  ref_pix_axi_ar_valid,
  ref_pix_axi_r_ready,
  ref_pic_write_axi_awid,
  ref_pic_write_axi_awlock,
  ref_pic_write_axi_awvalid,
  ref_pic_write_axi_wlast,
  ref_pic_write_axi_wvalid,
  ref_pic_write_axi_bready,
  dbf_main_in_valid,
  dbf_main_out_valid,
  sao_main_out_valid,
  controller_in_valid,
  controller_out_valid,
  sao_wr_en_out,
  sao_rd_en_out,
  intra_4x4_valid_out,
  inter_4x4_valid_out,
  comon_pre_lp_4x4_valid_out,
  dbf_to_sao_out_valid,
  test_luma_filter_ready,
  test_cache_valid_in,
  test_cache_en,
  test_ref_block_en,
  cb_res_pres_wr_en,
  cb_res_pres_rd_en,
  cb_res_pres_empty,
  cb_res_pres_full,
  cb_res_pres_din,
  cb_res_pres_dout,
  display_buf_ful,
  y_rd_data_count_in,
  cb_rd_data_count_in,
  cr_rd_data_count_in,
  fifo_in,
  y_residual_fifo_in,
  cb_residual_fifo_in,
  cr_residual_fifo_in,
  mv_col_axi_bresp,
  mv_pref_axi_rdata,
  mv_pref_axi_rresp,
  ref_pix_axi_r_data,
  ref_pix_axi_r_resp,
  ref_pic_write_axi_bresp,
  yy_prog_empty_thresh_out,
  cb_prog_empty_thresh_out,
  cr_prog_empty_thresh_out,
  display_fifo_data,
  sao_poc_out,
  log2_ctu_size_out,
  pic_width_out,
  pic_height_out,
  mv_col_axi_awlen,
  mv_col_axi_awsize,
  mv_col_axi_awburst,
  mv_col_axi_awcache,
  mv_col_axi_awprot,
  mv_col_axi_awaddr,
  mv_col_axi_wstrb,
  mv_col_axi_wdata,
  mv_pref_axi_araddr,
  mv_pref_axi_arlen,
  mv_pref_axi_arsize,
  mv_pref_axi_arburst,
  mv_pref_axi_arprot,
  mv_pref_axi_arcache,
  ref_pix_axi_ar_addr,
  ref_pix_axi_ar_len,
  ref_pix_axi_ar_size,
  ref_pix_axi_ar_burst,
  ref_pix_axi_ar_prot,
  ref_pic_write_axi_awlen,
  ref_pic_write_axi_awsize,
  ref_pic_write_axi_awburst,
  ref_pic_write_axi_awcache,
  ref_pic_write_axi_awprot,
  ref_pic_write_axi_awaddr,
  ref_pic_write_axi_wstrb,
  ref_pic_write_axi_wdata,
  pred_state_low8b_out,
  inter_pred_stat_8b_out,
  mv_state_8bit_out,
  col_state_axi_write,
  pred_2_dbf_wr_inf,
  pred_2_dbf_yy,
  pred_2_dbf_cb,
  pred_2_dbf_cr,
  Xc_Yc_out,
  intra_ready_out,
  cnf_fifo_out,
  cnf_fifo_counter,
  y_residual_counter,
  sao_out_y_4x8,
  controller_in,
  controller_out,
  sao_wr_data,
  sao_rd_data,
  dpb_wr_fifo_data,
  intra_x_out,
  intra_y_out,
  intra_luma_4x4_out,
  inter_x_out,
  inter_y_out,
  inter_luma_4x4_out,
  comon_pre_lp_x_out,
  comon_pre_lp_y_out,
  comon_pre_lp_luma_4x4_out,
  dbf_to_sao_out_x,
  dbf_to_sao_out_y,
  dbf_to_sao_out_q1,
  test_xT_in_min_luma_filt,
  test_xT_in_min_luma_cache,
  test_yT_in_min_luma_filt,
  test_yT_in_min_luma_cache,
  test_luma_filter_out,
  test_cache_addr,
  test_cache_luma_data,
  test_ref_luma_data_4x4,
  residual_read_inf,
  res_pres_y_cb_cr_out,
  current_poc_out
);
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 signal_clock CLK" *)
input wire clk;
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 signal_reset RST" *)
input wire reset;
input wire enable;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 config_fifo_read_info EMPTY" *)
input wire input_fifo_is_empty;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 y_residual_interface EMPTY" *)
input wire y_residual_fifo_is_empty_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 cb_residual_read_inf EMPTY" *)
input wire cb_residual_fifo_is_empty_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 cr_residual_read_inf EMPTY" *)
input wire cr_residual_fifo_is_empty_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 display_fifo_write_inf WR_EN" *)
output wire display_fifo_data_wr_en;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWREADY" *)
input wire mv_col_axi_awready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf WREADY" *)
input wire mv_col_axi_wready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf BVALID" *)
input wire mv_col_axi_bvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARREADY" *)
input wire mv_pref_axi_arready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf RLAST" *)
input wire mv_pref_axi_rlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf RVALID" *)
input wire mv_pref_axi_rvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf ARREADY" *)
input wire ref_pix_axi_ar_ready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf RLAST" *)
input wire ref_pix_axi_r_last;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf RVALID" *)
input wire ref_pix_axi_r_valid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWREADY" *)
input wire ref_pic_write_axi_awready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf WREADY" *)
input wire ref_pic_write_axi_wready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf BID" *)
input wire ref_pic_write_axi_bid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf BVALID" *)
input wire ref_pic_write_axi_bvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 config_fifo_read_info RD_EN" *)
output wire read_en_out;
output wire y_residual_fifo_is_empty_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 y_residual_interface RD_EN" *)
output wire y_residual_fifo_read_en_out;
output wire cb_residual_fifo_is_empty_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 cb_residual_read_inf RD_EN" *)
output wire cb_residual_fifo_read_en_out;
output wire cr_residual_fifo_is_empty_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 cr_residual_read_inf RD_EN" *)
output wire cr_residual_fifo_read_en_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 display_fifo_write_inf FULL" *)
input wire display_fifo_is_full;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWID" *)
output wire mv_col_axi_awid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWLOCK" *)
output wire mv_col_axi_awlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWVALID" *)
output wire mv_col_axi_awvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf WLAST" *)
output wire mv_col_axi_wlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf WVALID" *)
output wire mv_col_axi_wvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf BREADY" *)
output wire mv_col_axi_bready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARVALID" *)
output wire mv_pref_axi_arvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf RREADY" *)
output wire mv_pref_axi_rready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARLOCK" *)
output wire mv_pref_axi_arlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARID" *)
output wire mv_pref_axi_arid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf ARVALID" *)
output wire ref_pix_axi_ar_valid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf RREADY" *)
output wire ref_pix_axi_r_ready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWID" *)
output wire ref_pic_write_axi_awid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWLOCK" *)
output wire ref_pic_write_axi_awlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWVALID" *)
output wire ref_pic_write_axi_awvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf WLAST" *)
output wire ref_pic_write_axi_wlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf WVALID" *)
output wire ref_pic_write_axi_wvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf BREADY" *)
output wire ref_pic_write_axi_bready;
output wire dbf_main_in_valid;
output wire dbf_main_out_valid;
output wire sao_main_out_valid;
output wire controller_in_valid;
output wire controller_out_valid;
output wire sao_wr_en_out;
output wire sao_rd_en_out;
output wire intra_4x4_valid_out;
output wire inter_4x4_valid_out;
output wire comon_pre_lp_4x4_valid_out;
output wire dbf_to_sao_out_valid;
output wire test_luma_filter_ready;
output wire test_cache_valid_in;
output wire test_cache_en;
output wire test_ref_block_en;
output wire cb_res_pres_wr_en;
output wire cb_res_pres_rd_en;
output wire cb_res_pres_empty;
output wire cb_res_pres_full;
output wire cb_res_pres_din;
output wire cb_res_pres_dout;
output wire display_buf_ful;
input wire [12 : 0] y_rd_data_count_in;
input wire [10 : 0] cb_rd_data_count_in;
input wire [10 : 0] cr_rd_data_count_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 config_fifo_read_info RD_DATA" *)
input wire [31 : 0] fifo_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 y_residual_interface RD_DATA" *)
input wire [143 : 0] y_residual_fifo_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 cb_residual_read_inf RD_DATA" *)
input wire [143 : 0] cb_residual_fifo_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 cr_residual_read_inf RD_DATA" *)
input wire [143 : 0] cr_residual_fifo_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf BRESP" *)
input wire [1 : 0] mv_col_axi_bresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf RDATA" *)
input wire [511 : 0] mv_pref_axi_rdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf RRESP" *)
input wire [1 : 0] mv_pref_axi_rresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf RDATA" *)
input wire [511 : 0] ref_pix_axi_r_data;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf RRESP" *)
input wire [1 : 0] ref_pix_axi_r_resp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf BRESP" *)
input wire [1 : 0] ref_pic_write_axi_bresp;
output wire [11 : 0] yy_prog_empty_thresh_out;
output wire [9 : 0] cb_prog_empty_thresh_out;
output wire [9 : 0] cr_prog_empty_thresh_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 display_fifo_write_inf WR_DATA" *)
output wire [785 : 0] display_fifo_data;
output wire [31 : 0] sao_poc_out;
output wire [2 : 0] log2_ctu_size_out;
output wire [11 : 0] pic_width_out;
output wire [11 : 0] pic_height_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWLEN" *)
output wire [7 : 0] mv_col_axi_awlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWSIZE" *)
output wire [2 : 0] mv_col_axi_awsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWBURST" *)
output wire [1 : 0] mv_col_axi_awburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWCACHE" *)
output wire [3 : 0] mv_col_axi_awcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWPROT" *)
output wire [2 : 0] mv_col_axi_awprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWADDR" *)
output wire [31 : 0] mv_col_axi_awaddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf WSTRB" *)
output wire [63 : 0] mv_col_axi_wstrb;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf WDATA" *)
output wire [511 : 0] mv_col_axi_wdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARADDR" *)
output wire [31 : 0] mv_pref_axi_araddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARLEN" *)
output wire [7 : 0] mv_pref_axi_arlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARSIZE" *)
output wire [2 : 0] mv_pref_axi_arsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARBURST" *)
output wire [1 : 0] mv_pref_axi_arburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARPROT" *)
output wire [2 : 0] mv_pref_axi_arprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARCACHE" *)
output wire [3 : 0] mv_pref_axi_arcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf ARADDR" *)
output wire [31 : 0] ref_pix_axi_ar_addr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf ARLEN" *)
output wire [7 : 0] ref_pix_axi_ar_len;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf ARSIZE" *)
output wire [2 : 0] ref_pix_axi_ar_size;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf ARBURST" *)
output wire [1 : 0] ref_pix_axi_ar_burst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf ARPROT" *)
output wire [2 : 0] ref_pix_axi_ar_prot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWLEN" *)
output wire [7 : 0] ref_pic_write_axi_awlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWSIZE" *)
output wire [2 : 0] ref_pic_write_axi_awsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWBURST" *)
output wire [1 : 0] ref_pic_write_axi_awburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWCACHE" *)
output wire [3 : 0] ref_pic_write_axi_awcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWPROT" *)
output wire [2 : 0] ref_pic_write_axi_awprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWADDR" *)
output wire [31 : 0] ref_pic_write_axi_awaddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf WSTRB" *)
output wire [63 : 0] ref_pic_write_axi_wstrb;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf WDATA" *)
output wire [511 : 0] ref_pic_write_axi_wdata;
output wire [7 : 0] pred_state_low8b_out;
output wire [7 : 0] inter_pred_stat_8b_out;
output wire [7 : 0] mv_state_8bit_out;
output wire [7 : 0] col_state_axi_write;
output wire [7 : 0] pred_2_dbf_wr_inf;
output wire [133 : 0] pred_2_dbf_yy;
output wire [127 : 0] pred_2_dbf_cb;
output wire [127 : 0] pred_2_dbf_cr;
output wire [23 : 0] Xc_Yc_out;
output wire [5 : 0] intra_ready_out;
output wire [31 : 0] cnf_fifo_out;
output wire [31 : 0] cnf_fifo_counter;
output wire [31 : 0] y_residual_counter;
output wire [255 : 0] sao_out_y_4x8;
output wire [127 : 0] controller_in;
output wire [127 : 0] controller_out;
output wire [255 : 0] sao_wr_data;
output wire [255 : 0] sao_rd_data;
output wire [783 : 0] dpb_wr_fifo_data;
output wire [11 : 0] intra_x_out;
output wire [11 : 0] intra_y_out;
output wire [127 : 0] intra_luma_4x4_out;
output wire [11 : 0] inter_x_out;
output wire [11 : 0] inter_y_out;
output wire [127 : 0] inter_luma_4x4_out;
output wire [11 : 0] comon_pre_lp_x_out;
output wire [11 : 0] comon_pre_lp_y_out;
output wire [127 : 0] comon_pre_lp_luma_4x4_out;
output wire [11 : 0] dbf_to_sao_out_x;
output wire [11 : 0] dbf_to_sao_out_y;
output wire [127 : 0] dbf_to_sao_out_q1;
output wire [8 : 0] test_xT_in_min_luma_filt;
output wire [8 : 0] test_xT_in_min_luma_cache;
output wire [8 : 0] test_yT_in_min_luma_filt;
output wire [8 : 0] test_yT_in_min_luma_cache;
output wire [255 : 0] test_luma_filter_out;
output wire [6 : 0] test_cache_addr;
output wire [511 : 0] test_cache_luma_data;
output wire [127 : 0] test_ref_luma_data_4x4;
output wire [7 : 0] residual_read_inf;
output wire [2 : 0] res_pres_y_cb_cr_out;
output wire [31 : 0] current_poc_out;
endmodule
// End of netlist blackbox

(* X_CORE_INFO = "hevc_top_wo_fifo_wo_dp_fifo,Vivado 2014.2" *)
(* CHECK_LICENSE_TYPE = "zynq_design_1_hevc_top_wo_dp_fifo_0_0,hevc_top_wo_fifo_wo_dp_fifo,{}" *)
(* CORE_GENERATION_INFO = "zynq_design_1_hevc_top_wo_dp_fifo_0_0,hevc_top_wo_fifo_wo_dp_fifo,{x_ipProduct=Vivado 2014.2,x_ipVendor=xilinx.com,x_ipLibrary=ip,x_ipName=hevc_top_wo_dp_fifo,x_ipVersion=2.1,x_ipCoreRevision=17,x_ipLanguage=VERILOG}" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module zynq_design_1_hevc_top_wo_dp_fifo_0_0 (
  clk,
  reset,
  enable,
  input_fifo_is_empty,
  y_residual_fifo_is_empty_in,
  cb_residual_fifo_is_empty_in,
  cr_residual_fifo_is_empty_in,
  display_fifo_data_wr_en,
  mv_col_axi_awready,
  mv_col_axi_wready,
  mv_col_axi_bvalid,
  mv_pref_axi_arready,
  mv_pref_axi_rlast,
  mv_pref_axi_rvalid,
  ref_pix_axi_ar_ready,
  ref_pix_axi_r_last,
  ref_pix_axi_r_valid,
  ref_pic_write_axi_awready,
  ref_pic_write_axi_wready,
  ref_pic_write_axi_bid,
  ref_pic_write_axi_bvalid,
  read_en_out,
  y_residual_fifo_is_empty_out,
  y_residual_fifo_read_en_out,
  cb_residual_fifo_is_empty_out,
  cb_residual_fifo_read_en_out,
  cr_residual_fifo_is_empty_out,
  cr_residual_fifo_read_en_out,
  display_fifo_is_full,
  mv_col_axi_awid,
  mv_col_axi_awlock,
  mv_col_axi_awvalid,
  mv_col_axi_wlast,
  mv_col_axi_wvalid,
  mv_col_axi_bready,
  mv_pref_axi_arvalid,
  mv_pref_axi_rready,
  mv_pref_axi_arlock,
  mv_pref_axi_arid,
  ref_pix_axi_ar_valid,
  ref_pix_axi_r_ready,
  ref_pic_write_axi_awid,
  ref_pic_write_axi_awlock,
  ref_pic_write_axi_awvalid,
  ref_pic_write_axi_wlast,
  ref_pic_write_axi_wvalid,
  ref_pic_write_axi_bready,
  dbf_main_in_valid,
  dbf_main_out_valid,
  sao_main_out_valid,
  controller_in_valid,
  controller_out_valid,
  sao_wr_en_out,
  sao_rd_en_out,
  intra_4x4_valid_out,
  inter_4x4_valid_out,
  comon_pre_lp_4x4_valid_out,
  dbf_to_sao_out_valid,
  test_luma_filter_ready,
  test_cache_valid_in,
  test_cache_en,
  test_ref_block_en,
  cb_res_pres_wr_en,
  cb_res_pres_rd_en,
  cb_res_pres_empty,
  cb_res_pres_full,
  cb_res_pres_din,
  cb_res_pres_dout,
  display_buf_ful,
  y_rd_data_count_in,
  cb_rd_data_count_in,
  cr_rd_data_count_in,
  fifo_in,
  y_residual_fifo_in,
  cb_residual_fifo_in,
  cr_residual_fifo_in,
  mv_col_axi_bresp,
  mv_pref_axi_rdata,
  mv_pref_axi_rresp,
  ref_pix_axi_r_data,
  ref_pix_axi_r_resp,
  ref_pic_write_axi_bresp,
  yy_prog_empty_thresh_out,
  cb_prog_empty_thresh_out,
  cr_prog_empty_thresh_out,
  display_fifo_data,
  sao_poc_out,
  log2_ctu_size_out,
  pic_width_out,
  pic_height_out,
  mv_col_axi_awlen,
  mv_col_axi_awsize,
  mv_col_axi_awburst,
  mv_col_axi_awcache,
  mv_col_axi_awprot,
  mv_col_axi_awaddr,
  mv_col_axi_wstrb,
  mv_col_axi_wdata,
  mv_pref_axi_araddr,
  mv_pref_axi_arlen,
  mv_pref_axi_arsize,
  mv_pref_axi_arburst,
  mv_pref_axi_arprot,
  mv_pref_axi_arcache,
  ref_pix_axi_ar_addr,
  ref_pix_axi_ar_len,
  ref_pix_axi_ar_size,
  ref_pix_axi_ar_burst,
  ref_pix_axi_ar_prot,
  ref_pic_write_axi_awlen,
  ref_pic_write_axi_awsize,
  ref_pic_write_axi_awburst,
  ref_pic_write_axi_awcache,
  ref_pic_write_axi_awprot,
  ref_pic_write_axi_awaddr,
  ref_pic_write_axi_wstrb,
  ref_pic_write_axi_wdata,
  pred_state_low8b_out,
  inter_pred_stat_8b_out,
  mv_state_8bit_out,
  col_state_axi_write,
  pred_2_dbf_wr_inf,
  pred_2_dbf_yy,
  pred_2_dbf_cb,
  pred_2_dbf_cr,
  Xc_Yc_out,
  intra_ready_out,
  cnf_fifo_out,
  cnf_fifo_counter,
  y_residual_counter,
  sao_out_y_4x8,
  controller_in,
  controller_out,
  sao_wr_data,
  sao_rd_data,
  dpb_wr_fifo_data,
  intra_x_out,
  intra_y_out,
  intra_luma_4x4_out,
  inter_x_out,
  inter_y_out,
  inter_luma_4x4_out,
  comon_pre_lp_x_out,
  comon_pre_lp_y_out,
  comon_pre_lp_luma_4x4_out,
  dbf_to_sao_out_x,
  dbf_to_sao_out_y,
  dbf_to_sao_out_q1,
  test_xT_in_min_luma_filt,
  test_xT_in_min_luma_cache,
  test_yT_in_min_luma_filt,
  test_yT_in_min_luma_cache,
  test_luma_filter_out,
  test_cache_addr,
  test_cache_luma_data,
  test_ref_luma_data_4x4,
  residual_read_inf,
  res_pres_y_cb_cr_out,
  current_poc_out
);

(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 signal_clock CLK" *)
input wire clk;
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 signal_reset RST" *)
input wire reset;
input wire enable;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 config_fifo_read_info EMPTY" *)
input wire input_fifo_is_empty;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 y_residual_interface EMPTY" *)
input wire y_residual_fifo_is_empty_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 cb_residual_read_inf EMPTY" *)
input wire cb_residual_fifo_is_empty_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 cr_residual_read_inf EMPTY" *)
input wire cr_residual_fifo_is_empty_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 display_fifo_write_inf WR_EN" *)
output wire display_fifo_data_wr_en;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWREADY" *)
input wire mv_col_axi_awready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf WREADY" *)
input wire mv_col_axi_wready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf BVALID" *)
input wire mv_col_axi_bvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARREADY" *)
input wire mv_pref_axi_arready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf RLAST" *)
input wire mv_pref_axi_rlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf RVALID" *)
input wire mv_pref_axi_rvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf ARREADY" *)
input wire ref_pix_axi_ar_ready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf RLAST" *)
input wire ref_pix_axi_r_last;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf RVALID" *)
input wire ref_pix_axi_r_valid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWREADY" *)
input wire ref_pic_write_axi_awready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf WREADY" *)
input wire ref_pic_write_axi_wready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf BID" *)
input wire ref_pic_write_axi_bid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf BVALID" *)
input wire ref_pic_write_axi_bvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 config_fifo_read_info RD_EN" *)
output wire read_en_out;
output wire y_residual_fifo_is_empty_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 y_residual_interface RD_EN" *)
output wire y_residual_fifo_read_en_out;
output wire cb_residual_fifo_is_empty_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 cb_residual_read_inf RD_EN" *)
output wire cb_residual_fifo_read_en_out;
output wire cr_residual_fifo_is_empty_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 cr_residual_read_inf RD_EN" *)
output wire cr_residual_fifo_read_en_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 display_fifo_write_inf FULL" *)
input wire display_fifo_is_full;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWID" *)
output wire mv_col_axi_awid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWLOCK" *)
output wire mv_col_axi_awlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWVALID" *)
output wire mv_col_axi_awvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf WLAST" *)
output wire mv_col_axi_wlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf WVALID" *)
output wire mv_col_axi_wvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf BREADY" *)
output wire mv_col_axi_bready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARVALID" *)
output wire mv_pref_axi_arvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf RREADY" *)
output wire mv_pref_axi_rready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARLOCK" *)
output wire mv_pref_axi_arlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARID" *)
output wire mv_pref_axi_arid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf ARVALID" *)
output wire ref_pix_axi_ar_valid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf RREADY" *)
output wire ref_pix_axi_r_ready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWID" *)
output wire ref_pic_write_axi_awid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWLOCK" *)
output wire ref_pic_write_axi_awlock;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWVALID" *)
output wire ref_pic_write_axi_awvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf WLAST" *)
output wire ref_pic_write_axi_wlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf WVALID" *)
output wire ref_pic_write_axi_wvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf BREADY" *)
output wire ref_pic_write_axi_bready;
output wire dbf_main_in_valid;
output wire dbf_main_out_valid;
output wire sao_main_out_valid;
output wire controller_in_valid;
output wire controller_out_valid;
output wire sao_wr_en_out;
output wire sao_rd_en_out;
output wire intra_4x4_valid_out;
output wire inter_4x4_valid_out;
output wire comon_pre_lp_4x4_valid_out;
output wire dbf_to_sao_out_valid;
output wire test_luma_filter_ready;
output wire test_cache_valid_in;
output wire test_cache_en;
output wire test_ref_block_en;
output wire cb_res_pres_wr_en;
output wire cb_res_pres_rd_en;
output wire cb_res_pres_empty;
output wire cb_res_pres_full;
output wire cb_res_pres_din;
output wire cb_res_pres_dout;
output wire display_buf_ful;
input wire [12 : 0] y_rd_data_count_in;
input wire [10 : 0] cb_rd_data_count_in;
input wire [10 : 0] cr_rd_data_count_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 config_fifo_read_info RD_DATA" *)
input wire [31 : 0] fifo_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 y_residual_interface RD_DATA" *)
input wire [143 : 0] y_residual_fifo_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 cb_residual_read_inf RD_DATA" *)
input wire [143 : 0] cb_residual_fifo_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 cr_residual_read_inf RD_DATA" *)
input wire [143 : 0] cr_residual_fifo_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf BRESP" *)
input wire [1 : 0] mv_col_axi_bresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf RDATA" *)
input wire [511 : 0] mv_pref_axi_rdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf RRESP" *)
input wire [1 : 0] mv_pref_axi_rresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf RDATA" *)
input wire [511 : 0] ref_pix_axi_r_data;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf RRESP" *)
input wire [1 : 0] ref_pix_axi_r_resp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf BRESP" *)
input wire [1 : 0] ref_pic_write_axi_bresp;
output wire [11 : 0] yy_prog_empty_thresh_out;
output wire [9 : 0] cb_prog_empty_thresh_out;
output wire [9 : 0] cr_prog_empty_thresh_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 display_fifo_write_inf WR_DATA" *)
output wire [785 : 0] display_fifo_data;
output wire [31 : 0] sao_poc_out;
output wire [2 : 0] log2_ctu_size_out;
output wire [11 : 0] pic_width_out;
output wire [11 : 0] pic_height_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWLEN" *)
output wire [7 : 0] mv_col_axi_awlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWSIZE" *)
output wire [2 : 0] mv_col_axi_awsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWBURST" *)
output wire [1 : 0] mv_col_axi_awburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWCACHE" *)
output wire [3 : 0] mv_col_axi_awcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWPROT" *)
output wire [2 : 0] mv_col_axi_awprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf AWADDR" *)
output wire [31 : 0] mv_col_axi_awaddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf WSTRB" *)
output wire [63 : 0] mv_col_axi_wstrb;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_col_axi_master_inf WDATA" *)
output wire [511 : 0] mv_col_axi_wdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARADDR" *)
output wire [31 : 0] mv_pref_axi_araddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARLEN" *)
output wire [7 : 0] mv_pref_axi_arlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARSIZE" *)
output wire [2 : 0] mv_pref_axi_arsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARBURST" *)
output wire [1 : 0] mv_pref_axi_arburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARPROT" *)
output wire [2 : 0] mv_pref_axi_arprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 mv_pref_axi_master_inf ARCACHE" *)
output wire [3 : 0] mv_pref_axi_arcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf ARADDR" *)
output wire [31 : 0] ref_pix_axi_ar_addr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf ARLEN" *)
output wire [7 : 0] ref_pix_axi_ar_len;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf ARSIZE" *)
output wire [2 : 0] ref_pix_axi_ar_size;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf ARBURST" *)
output wire [1 : 0] ref_pix_axi_ar_burst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pix_axi_mastr_inf ARPROT" *)
output wire [2 : 0] ref_pix_axi_ar_prot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWLEN" *)
output wire [7 : 0] ref_pic_write_axi_awlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWSIZE" *)
output wire [2 : 0] ref_pic_write_axi_awsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWBURST" *)
output wire [1 : 0] ref_pic_write_axi_awburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWCACHE" *)
output wire [3 : 0] ref_pic_write_axi_awcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWPROT" *)
output wire [2 : 0] ref_pic_write_axi_awprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf AWADDR" *)
output wire [31 : 0] ref_pic_write_axi_awaddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf WSTRB" *)
output wire [63 : 0] ref_pic_write_axi_wstrb;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 ref_pic_write_axi_master_inf WDATA" *)
output wire [511 : 0] ref_pic_write_axi_wdata;
output wire [7 : 0] pred_state_low8b_out;
output wire [7 : 0] inter_pred_stat_8b_out;
output wire [7 : 0] mv_state_8bit_out;
output wire [7 : 0] col_state_axi_write;
output wire [7 : 0] pred_2_dbf_wr_inf;
output wire [133 : 0] pred_2_dbf_yy;
output wire [127 : 0] pred_2_dbf_cb;
output wire [127 : 0] pred_2_dbf_cr;
output wire [23 : 0] Xc_Yc_out;
output wire [5 : 0] intra_ready_out;
output wire [31 : 0] cnf_fifo_out;
output wire [31 : 0] cnf_fifo_counter;
output wire [31 : 0] y_residual_counter;
output wire [255 : 0] sao_out_y_4x8;
output wire [127 : 0] controller_in;
output wire [127 : 0] controller_out;
output wire [255 : 0] sao_wr_data;
output wire [255 : 0] sao_rd_data;
output wire [783 : 0] dpb_wr_fifo_data;
output wire [11 : 0] intra_x_out;
output wire [11 : 0] intra_y_out;
output wire [127 : 0] intra_luma_4x4_out;
output wire [11 : 0] inter_x_out;
output wire [11 : 0] inter_y_out;
output wire [127 : 0] inter_luma_4x4_out;
output wire [11 : 0] comon_pre_lp_x_out;
output wire [11 : 0] comon_pre_lp_y_out;
output wire [127 : 0] comon_pre_lp_luma_4x4_out;
output wire [11 : 0] dbf_to_sao_out_x;
output wire [11 : 0] dbf_to_sao_out_y;
output wire [127 : 0] dbf_to_sao_out_q1;
output wire [8 : 0] test_xT_in_min_luma_filt;
output wire [8 : 0] test_xT_in_min_luma_cache;
output wire [8 : 0] test_yT_in_min_luma_filt;
output wire [8 : 0] test_yT_in_min_luma_cache;
output wire [255 : 0] test_luma_filter_out;
output wire [6 : 0] test_cache_addr;
output wire [511 : 0] test_cache_luma_data;
output wire [127 : 0] test_ref_luma_data_4x4;
output wire [7 : 0] residual_read_inf;
output wire [2 : 0] res_pres_y_cb_cr_out;
output wire [31 : 0] current_poc_out;

  hevc_top_wo_fifo_wo_dp_fifo inst (
    .clk(clk),
    .reset(reset),
    .enable(enable),
    .input_fifo_is_empty(input_fifo_is_empty),
    .y_residual_fifo_is_empty_in(y_residual_fifo_is_empty_in),
    .cb_residual_fifo_is_empty_in(cb_residual_fifo_is_empty_in),
    .cr_residual_fifo_is_empty_in(cr_residual_fifo_is_empty_in),
    .display_fifo_data_wr_en(display_fifo_data_wr_en),
    .mv_col_axi_awready(mv_col_axi_awready),
    .mv_col_axi_wready(mv_col_axi_wready),
    .mv_col_axi_bvalid(mv_col_axi_bvalid),
    .mv_pref_axi_arready(mv_pref_axi_arready),
    .mv_pref_axi_rlast(mv_pref_axi_rlast),
    .mv_pref_axi_rvalid(mv_pref_axi_rvalid),
    .ref_pix_axi_ar_ready(ref_pix_axi_ar_ready),
    .ref_pix_axi_r_last(ref_pix_axi_r_last),
    .ref_pix_axi_r_valid(ref_pix_axi_r_valid),
    .ref_pic_write_axi_awready(ref_pic_write_axi_awready),
    .ref_pic_write_axi_wready(ref_pic_write_axi_wready),
    .ref_pic_write_axi_bid(ref_pic_write_axi_bid),
    .ref_pic_write_axi_bvalid(ref_pic_write_axi_bvalid),
    .read_en_out(read_en_out),
    .y_residual_fifo_is_empty_out(y_residual_fifo_is_empty_out),
    .y_residual_fifo_read_en_out(y_residual_fifo_read_en_out),
    .cb_residual_fifo_is_empty_out(cb_residual_fifo_is_empty_out),
    .cb_residual_fifo_read_en_out(cb_residual_fifo_read_en_out),
    .cr_residual_fifo_is_empty_out(cr_residual_fifo_is_empty_out),
    .cr_residual_fifo_read_en_out(cr_residual_fifo_read_en_out),
    .display_fifo_is_full(display_fifo_is_full),
    .mv_col_axi_awid(mv_col_axi_awid),
    .mv_col_axi_awlock(mv_col_axi_awlock),
    .mv_col_axi_awvalid(mv_col_axi_awvalid),
    .mv_col_axi_wlast(mv_col_axi_wlast),
    .mv_col_axi_wvalid(mv_col_axi_wvalid),
    .mv_col_axi_bready(mv_col_axi_bready),
    .mv_pref_axi_arvalid(mv_pref_axi_arvalid),
    .mv_pref_axi_rready(mv_pref_axi_rready),
    .mv_pref_axi_arlock(mv_pref_axi_arlock),
    .mv_pref_axi_arid(mv_pref_axi_arid),
    .ref_pix_axi_ar_valid(ref_pix_axi_ar_valid),
    .ref_pix_axi_r_ready(ref_pix_axi_r_ready),
    .ref_pic_write_axi_awid(ref_pic_write_axi_awid),
    .ref_pic_write_axi_awlock(ref_pic_write_axi_awlock),
    .ref_pic_write_axi_awvalid(ref_pic_write_axi_awvalid),
    .ref_pic_write_axi_wlast(ref_pic_write_axi_wlast),
    .ref_pic_write_axi_wvalid(ref_pic_write_axi_wvalid),
    .ref_pic_write_axi_bready(ref_pic_write_axi_bready),
    .dbf_main_in_valid(dbf_main_in_valid),
    .dbf_main_out_valid(dbf_main_out_valid),
    .sao_main_out_valid(sao_main_out_valid),
    .controller_in_valid(controller_in_valid),
    .controller_out_valid(controller_out_valid),
    .sao_wr_en_out(sao_wr_en_out),
    .sao_rd_en_out(sao_rd_en_out),
    .intra_4x4_valid_out(intra_4x4_valid_out),
    .inter_4x4_valid_out(inter_4x4_valid_out),
    .comon_pre_lp_4x4_valid_out(comon_pre_lp_4x4_valid_out),
    .dbf_to_sao_out_valid(dbf_to_sao_out_valid),
    .test_luma_filter_ready(test_luma_filter_ready),
    .test_cache_valid_in(test_cache_valid_in),
    .test_cache_en(test_cache_en),
    .test_ref_block_en(test_ref_block_en),
    .cb_res_pres_wr_en(cb_res_pres_wr_en),
    .cb_res_pres_rd_en(cb_res_pres_rd_en),
    .cb_res_pres_empty(cb_res_pres_empty),
    .cb_res_pres_full(cb_res_pres_full),
    .cb_res_pres_din(cb_res_pres_din),
    .cb_res_pres_dout(cb_res_pres_dout),
    .display_buf_ful(display_buf_ful),
    .y_rd_data_count_in(y_rd_data_count_in),
    .cb_rd_data_count_in(cb_rd_data_count_in),
    .cr_rd_data_count_in(cr_rd_data_count_in),
    .fifo_in(fifo_in),
    .y_residual_fifo_in(y_residual_fifo_in),
    .cb_residual_fifo_in(cb_residual_fifo_in),
    .cr_residual_fifo_in(cr_residual_fifo_in),
    .mv_col_axi_bresp(mv_col_axi_bresp),
    .mv_pref_axi_rdata(mv_pref_axi_rdata),
    .mv_pref_axi_rresp(mv_pref_axi_rresp),
    .ref_pix_axi_r_data(ref_pix_axi_r_data),
    .ref_pix_axi_r_resp(ref_pix_axi_r_resp),
    .ref_pic_write_axi_bresp(ref_pic_write_axi_bresp),
    .yy_prog_empty_thresh_out(yy_prog_empty_thresh_out),
    .cb_prog_empty_thresh_out(cb_prog_empty_thresh_out),
    .cr_prog_empty_thresh_out(cr_prog_empty_thresh_out),
    .display_fifo_data(display_fifo_data),
    .sao_poc_out(sao_poc_out),
    .log2_ctu_size_out(log2_ctu_size_out),
    .pic_width_out(pic_width_out),
    .pic_height_out(pic_height_out),
    .mv_col_axi_awlen(mv_col_axi_awlen),
    .mv_col_axi_awsize(mv_col_axi_awsize),
    .mv_col_axi_awburst(mv_col_axi_awburst),
    .mv_col_axi_awcache(mv_col_axi_awcache),
    .mv_col_axi_awprot(mv_col_axi_awprot),
    .mv_col_axi_awaddr(mv_col_axi_awaddr),
    .mv_col_axi_wstrb(mv_col_axi_wstrb),
    .mv_col_axi_wdata(mv_col_axi_wdata),
    .mv_pref_axi_araddr(mv_pref_axi_araddr),
    .mv_pref_axi_arlen(mv_pref_axi_arlen),
    .mv_pref_axi_arsize(mv_pref_axi_arsize),
    .mv_pref_axi_arburst(mv_pref_axi_arburst),
    .mv_pref_axi_arprot(mv_pref_axi_arprot),
    .mv_pref_axi_arcache(mv_pref_axi_arcache),
    .ref_pix_axi_ar_addr(ref_pix_axi_ar_addr),
    .ref_pix_axi_ar_len(ref_pix_axi_ar_len),
    .ref_pix_axi_ar_size(ref_pix_axi_ar_size),
    .ref_pix_axi_ar_burst(ref_pix_axi_ar_burst),
    .ref_pix_axi_ar_prot(ref_pix_axi_ar_prot),
    .ref_pic_write_axi_awlen(ref_pic_write_axi_awlen),
    .ref_pic_write_axi_awsize(ref_pic_write_axi_awsize),
    .ref_pic_write_axi_awburst(ref_pic_write_axi_awburst),
    .ref_pic_write_axi_awcache(ref_pic_write_axi_awcache),
    .ref_pic_write_axi_awprot(ref_pic_write_axi_awprot),
    .ref_pic_write_axi_awaddr(ref_pic_write_axi_awaddr),
    .ref_pic_write_axi_wstrb(ref_pic_write_axi_wstrb),
    .ref_pic_write_axi_wdata(ref_pic_write_axi_wdata),
    .pred_state_low8b_out(pred_state_low8b_out),
    .inter_pred_stat_8b_out(inter_pred_stat_8b_out),
    .mv_state_8bit_out(mv_state_8bit_out),
    .col_state_axi_write(col_state_axi_write),
    .pred_2_dbf_wr_inf(pred_2_dbf_wr_inf),
    .pred_2_dbf_yy(pred_2_dbf_yy),
    .pred_2_dbf_cb(pred_2_dbf_cb),
    .pred_2_dbf_cr(pred_2_dbf_cr),
    .Xc_Yc_out(Xc_Yc_out),
    .intra_ready_out(intra_ready_out),
    .cnf_fifo_out(cnf_fifo_out),
    .cnf_fifo_counter(cnf_fifo_counter),
    .y_residual_counter(y_residual_counter),
    .sao_out_y_4x8(sao_out_y_4x8),
    .controller_in(controller_in),
    .controller_out(controller_out),
    .sao_wr_data(sao_wr_data),
    .sao_rd_data(sao_rd_data),
    .dpb_wr_fifo_data(dpb_wr_fifo_data),
    .intra_x_out(intra_x_out),
    .intra_y_out(intra_y_out),
    .intra_luma_4x4_out(intra_luma_4x4_out),
    .inter_x_out(inter_x_out),
    .inter_y_out(inter_y_out),
    .inter_luma_4x4_out(inter_luma_4x4_out),
    .comon_pre_lp_x_out(comon_pre_lp_x_out),
    .comon_pre_lp_y_out(comon_pre_lp_y_out),
    .comon_pre_lp_luma_4x4_out(comon_pre_lp_luma_4x4_out),
    .dbf_to_sao_out_x(dbf_to_sao_out_x),
    .dbf_to_sao_out_y(dbf_to_sao_out_y),
    .dbf_to_sao_out_q1(dbf_to_sao_out_q1),
    .test_xT_in_min_luma_filt(test_xT_in_min_luma_filt),
    .test_xT_in_min_luma_cache(test_xT_in_min_luma_cache),
    .test_yT_in_min_luma_filt(test_yT_in_min_luma_filt),
    .test_yT_in_min_luma_cache(test_yT_in_min_luma_cache),
    .test_luma_filter_out(test_luma_filter_out),
    .test_cache_addr(test_cache_addr),
    .test_cache_luma_data(test_cache_luma_data),
    .test_ref_luma_data_4x4(test_ref_luma_data_4x4),
    .residual_read_inf(residual_read_inf),
    .res_pres_y_cb_cr_out(res_pres_y_cb_cr_out),
    .current_poc_out(current_poc_out)
  );
endmodule
