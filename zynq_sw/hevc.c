/*
 * hevc.c
 *
 *  Created on: Jan 1, 2015
 *      Author: WINDOWS8
 */

#include "hevc.h"

#define SKIP_TO_NAL()  *(volatile u32*)(CABAC_BASE+ 0x830) = 0
#define READ_ANNEXB_VALID()  *(volatile u32*)(CABAC_BASE+ 0x840)
#define READ_U(n) *(volatile u32*)(CABAC_BASE+ 0x900 + ((n) << 4))

video_param_struct video_param;
sequence_param_struct* sequence_param;
picture_param_struct* picture_param;
slice_segment_struct slice_segment_header;

sequence_param_struct sequence_parameter_sets[16];
picture_param_struct picture_parameter_sets[64];

int current_pic_parameter_id = -1;
int PicOrderCntVal = 0;

void hevc_main(void){
	sleep(2);
	while(1){
		// skip to next NAL,
		SKIP_TO_NAL();
		while ( READ_ANNEXB_VALID() ==0);
		int forbidden_zero_bit = read_u(1);
		if (forbidden_zero_bit != 0)
			fprintf(stderr,"forbidden zero bit error\n");
		int nal_unit_type = read_u(6);
		int input_hevc_data_count = *(volatile u32*)(CABAC_BASE+ 0x410);
		int nuh_reserved_zero_6bits = read_u(6);
		int temporal_id = read_u(3);
		printf(" NAL Found %d %d\n",nal_unit_type,input_hevc_data_count);
		if (nal_unit_type==NAL_VPS_NUT){
			video_parameter_set_rbsp(&video_param);
		}
		if (nal_unit_type==NAL_SPS_NUT){
			sequence_parameter_set_rbsp();
		}
		if (nal_unit_type==NAL_PPS_NUT){
			picture_parameter_set_rbsp();
		}
		if (nal_unit_type==IDR_W_DLP){
			RapPicFlag = 1;
			IdrPicFlag = 1;
			RefPicture = 1;
			slice_segment_layer_rbsp(&slice_segment_header);
		} else if ( nal_unit_type==TRAIL_R || nal_unit_type == TRAIL_N || nal_unit_type==RASL_R || nal_unit_type == RASL_N){
			RapPicFlag = 0;
			IdrPicFlag = 0;
			RefPicture = ((nal_unit_type==TRAIL_R || nal_unit_type==RASL_R))? 1:0;
			RefPicture = 1; // For ALL INTRA (TODO: remove for non-all intra)
			//if (NAL_num!=5) return;
			slice_segment_layer_rbsp(&slice_segment_header);
		} else if (nal_unit_type ==CRA_NUT){
			RapPicFlag = 1;
			IdrPicFlag = 0;
			RefPicture = 1;
			RefPicture = 1;
			slice_segment_layer_rbsp(&slice_segment_header);
		}

	}
	return;
}

void video_parameter_set_rbsp(video_param_struct* vps){
	vps->vps_video_parameter_set_id = read_u(4);
	//if (read_u(2)!=3) printf("Reserved error");
	read_u(8);
	vps->vps_max_sub_layers_minus1 = read_u(3);
	//read_u(1);
	vps->vps_temporal_id_nesting_flag = read_u(1);
	if (read_u(8)!=0xff) fprintf(stderr,"Reserved 0xffff Error\n");
	if (read_u(8)!=0xff) fprintf(stderr,"Reserved 0xffff Error\n");
	//printf("Res %x\n",read_u(16));
	profile_tier_level(&(vps->vps_profile_tier_level),1,video_param.vps_max_sub_layers_minus1);
	//bit_rate_pic_rate_info(NULL,0,vps->vps_max_sub_layers_minus1);
	vps->vps_sub_layer_ordering_info_present_flag = read_u(1);
	int i,j,t=0;
	for (i= (vps->vps_sub_layer_ordering_info_present_flag ? 0 : vps->vps_max_sub_layers_minus1);i<=vps->vps_max_sub_layers_minus1;i++){
		// should only be called once;
		if (t==1) printf("Parameter Set Loop Multiple times\n");
		t=1;
		vps->vps_max_dec_pic_buffering = read_ue();
		vps->vps_max_num_reorder_pics = read_ue();
		vps->vps_max_latency_increase = read_ue();
	}
	//print_loc();
	vps->vps_max_layer_id = read_u(6);
	vps->vps_num_layer_sets_minus1 = read_ue();
	for (i=1;i<=vps->vps_num_layer_sets_minus1;i++){
		for (j=0;j<=vps->vps_max_layer_id;j++){
			read_u(1);
		}
	}
	vps->vps_timing_info_present_flag = read_u(1);
	if (vps->vps_timing_info_present_flag){
		printf("timing info present\n");
		while(1);
	}
	printf("vps done\n");
	//vps->vps_num_hrd_parameters = read_ue();
	//vps->vps_max_nuh_reserved_zero_layer_id = read_u(6);
	//vps->vps_num_op_sets_minus1 = read_ue();

}
void sequence_parameter_set_rbsp(){
	sequence_param_struct sps;
	current_pic_parameter_id = -1;

	sps.sps_video_parameter_set_id = read_u(4);
	sps.sps_max_sub_layers_minus1 = read_u(3);
	sps.sps_temporal_sub_layer_nesting_flag = read_u(1);
	profile_tier_level(&(sps.sps_profile_tier_level), 1,sps.sps_max_sub_layers_minus1);
	sps.sps_seq_parameter_set_id = read_ue();

	sps.chroma_format_idc = read_ue();
	if (sps.chroma_format_idc==3){
		fprintf(stderr,"Chroma Format cannot be 3\n");
	}
	sps.pic_width_in_luma_samples = read_ue();
	sps.pic_height_in_luma_samples = read_ue();
	sps.conformance_window_flag = read_u(1);
	if (sps.conformance_window_flag){
		sps.conf_win_left_offset = read_ue();
		sps.conf_win_right_offset = read_ue();
		sps.conf_win_top_offset = read_ue();
		sps.conf_win_bottom_offset = read_ue();
	}
	sps.bit_depth_luma_minus8 = read_ue();
	sps.bit_depth_chroma_minus8 = read_ue();
	sps.log2_max_pic_order_cnt_lsb_minus4 = read_ue();
	sps.sps_sub_layer_ordering_info_present_flag = read_u(1);
	int i,t=0;
	for (i= (sps.sps_sub_layer_ordering_info_present_flag ? 0 : sps.sps_max_sub_layers_minus1);i<=sps.sps_max_sub_layers_minus1;i++){
			// should only be called once;
			if (t==1) fprintf(stderr,"Parameter Set Loop Multiple times\n");
			t=1;
			sps.sps_max_dec_pic_buffering = read_ue();
			sps.sps_max_num_reorder_pics = read_ue();
			sps.sps_max_latency_increase = read_ue();
	}
	sps.log2_min_luma_coding_block_size_minus3 = read_ue();
	sps.log2_diff_max_min_luma_coding_block_size = read_ue();

	sps.Log2MinCbSizeY = sps.log2_min_luma_coding_block_size_minus3 +3;
	sps.Log2CtbSizeY = sps.Log2MinCbSizeY + sps.log2_diff_max_min_luma_coding_block_size;

	//PicHeightInCtbsY = ceil( (double)sps.pic_height_in_luma_samples/(1<<Log2CtbSizeY));
	sps.PicHeightInCtbsY = sps.pic_height_in_luma_samples >> sps.Log2CtbSizeY;
	if ( (sps.pic_height_in_luma_samples % (1<<sps.Log2CtbSizeY)) >0) sps.PicHeightInCtbsY++;
	//PicWidthInCtbsY = ceil( (double) sps.pic_width_in_luma_samples/(1<<Log2CtbSizeY));
	sps.PicWidthInCtbsY = sps.pic_width_in_luma_samples >> sps.Log2CtbSizeY;
	if ( (sps.pic_width_in_luma_samples % (1<<sps.Log2CtbSizeY)) >0) sps.PicWidthInCtbsY++;
	sps.PicSizeInCtbsY = sps.PicHeightInCtbsY  * sps.PicWidthInCtbsY ;
	//sao_list =(sao_struct*) malloc(sizeof(sao_struct)*PicSizeInCtbsY); // index according to ctbAddrRs
	sps.log2_min_transform_block_size_minus2 = read_ue();
	sps.log2_diff_max_min_transform_block_size = read_ue();
	sps.Log2MinTrafoSize = sps.log2_min_transform_block_size_minus2 +2;
	sps.Log2MaxTrafoSize = sps.Log2MinTrafoSize + sps.log2_diff_max_min_transform_block_size;

	initialize_scanOrder();


	sps.max_transform_hierarchy_depth_inter = read_ue();
	sps.max_transform_hierarchy_depth_intra = read_ue();
	sps.scaling_list_enable_flag = read_u(1);
	if (sps.scaling_list_enable_flag){
		int sps_scaling_list_data_present_flag = read_u(1);
		if (sps_scaling_list_data_present_flag) {
			//scaling_list_data();
		} else {
			// send_default_scaling_list;
		}
		printf("scaling list enable not implemented\n");
	}
	sps.amp_enabled_flag = read_u(1);
	sps.sample_adaptive_offset_enabled_flag = read_u(1);
	sps.pcm_enabled_flag = read_u(1);
		sps.pcm_loop_filter_disable_flag = 0;
	if (sps.pcm_enabled_flag) {
		sps.pcm_sample_bit_depth_luma_minus1 = read_u(4);
		sps.pcm_sample_bit_depth_chroma_minus1 = read_u(4);
		sps.log2_min_pcm_luma_coding_block_size_minus3 = read_ue();
		sps.log2_diff_max_min_pcm_luma_coding_block_size = read_ue();
		sps.pcm_loop_filter_disable_flag = read_u(1);

		sps.PCMBitDepthY = sps.pcm_sample_bit_depth_luma_minus1 + 1;
		sps.PCMBitDepthC = sps.pcm_sample_bit_depth_chroma_minus1 + 1;
		sps.Log2MinIpcmCbSizeY = sps.log2_min_pcm_luma_coding_block_size_minus3 + 3;
		sps.Log2MaxIpcmCbSizeY = sps.Log2MinIpcmCbSizeY + sps.log2_diff_max_min_pcm_luma_coding_block_size;
	}
	sps.num_short_term_ref_pic_sets = read_ue();
	for (i=0;i<sps.num_short_term_ref_pic_sets;i++){
		//sps.short_term_ref_pic_set[i] = (short_term_ref_pic_set_struct*) malloc(sizeof(short_term_ref_pic_set_struct));
		short_term_ref_pic_set(&sps,i);
	}
	//print_loc();
	printf("ref_pic lists done\n");

	sps.long_term_ref_pics_present_flag = read_u(1);
	if (sps.long_term_ref_pics_present_flag){
		//TODO
	}
	sps.sps_temporal_mvp_enable_flag = read_u(1);
	sps.strong_intra_smoothing_enable_flag = read_u(1);
	sps.vui_parameters_present_flag = read_u(1);
	/*if (sps.vui_parameters_present_flag){
		fprintf(stderr,"VUI Parameters Not Implemented\n");
	}
	sps.sps_extension_flag = read_u(1);
	if (sps.sps_extension_flag){
		fprintf(stderr,"SPS Extension Not Implemented\n");
	}*/

	sequence_parameter_sets[sps.sps_seq_parameter_set_id] = sps;

}


void picture_parameter_set_rbsp(){
	picture_param_struct pps;
	current_pic_parameter_id = -1;

	pps.pps_pic_parameter_set_id = read_ue();
	pps.pps_seq_parameter_set_id = read_ue();

	sequence_param = &(sequence_parameter_sets[pps.pps_seq_parameter_set_id]);

	pps.dependent_slice_segments_enabled_flag = read_u(1);
	pps.output_flag_present_flag = read_u(1);
	pps.num_extra_slice_header_bits = read_u(3);
	pps.sign_data_hiding_flag = read_u(1);
	pps.cabac_init_present_flag = read_u(1);
	pps.num_ref_idx_l0_default_active_minus1=read_ue();
	pps.num_ref_idx_l1_default_active_minus1=read_ue();
	pps.init_qp_minus26 = read_se();
	pps.constrained_intra_pred_flag = read_u(1);
	pps.transform_skip_enabled_flag = read_u(1);
	pps.cu_qp_delta_enabled_flag = read_u(1);
	pps.diff_cu_qp_delta_depth = 0;
	if (pps.cu_qp_delta_enabled_flag){
		pps.diff_cu_qp_delta_depth = read_ue();
	}
	sequence_param->Log2MinCuQpDeltaSize = sequence_param->Log2CtbSizeY - pps.diff_cu_qp_delta_depth;
	pps.pps_cb_qp_offset = read_se();
	pps.pps_cr_qp_offset = read_se();
	if (pps.pps_cb_qp_offset>0 || pps.pps_cr_qp_offset>0){
		//printf("pps_chroma_qp_offsets present\n"); exit(0);
	}
	pps.pps_slice_chroma_qp_offsets_present_flag = read_u(1);
	pps.weighted_pred_flag = read_u(1);
	pps.weighted_bipred_flag = read_u(1);
//	pps.output_flag_present_flag = read_u(1);
	pps.transquant_bypass_enable_flag = read_u(1);
	pps.tiles_enabled_flag = read_u(1);
	pps.entropy_coding_sync_enabled_flag = read_u(1);
	pps.loop_filter_across_tiles_enabled_flag=0;
	if (pps.tiles_enabled_flag){
		pps.num_tile_columns_minus1 = read_ue();
		pps.num_tile_rows_minus1 = read_ue();
		pps.uniform_spacing_flag = read_u(1);
		if (!pps.uniform_spacing_flag){
			//fprintf(stderr,"Not Uniform spaced tiles\n");
			int i;
			for (i=0;i<pps.num_tile_columns_minus1;i++){
				pps.column_width_minus1[i] = read_ue();
			}
			for (i=0;i<pps.num_tile_rows_minus1;i++){
				pps.row_height_minus1[i] = read_ue();
			}
		}
		pps.loop_filter_across_slices_enabled_flag = read_u(1);
	} else {
		pps.num_tile_columns_minus1 = 0;
		pps.num_tile_rows_minus1 = 0;
		pps.uniform_spacing_flag = 1;
	}

	pps.loop_filter_across_slices_enabled_flag = read_u(1);
	pps.deblocking_filter_control_present_flag = read_u(1);
	pps.pps_beta_offset_div2 =0;
	pps.pps_tc_offset_div2 = 0;
	pps.pps_disable_deblocking_filter_flag = 0;
	if (pps.deblocking_filter_control_present_flag){
		pps.pps_deblocking_filter_override_enabled_flag = read_u(1);
		pps.pps_disable_deblocking_filter_flag = read_u(1);
		if(!pps.pps_disable_deblocking_filter_flag){
			pps.pps_beta_offset_div2 = read_se();
			pps.pps_tc_offset_div2 = read_se();
		}
		//fprintf(stderr,"deblocking_filter_control_present_flag PRESENT\n");
	}
	pps.pps_scaling_list_data_present_flag = read_u(1);
	if (pps.pps_scaling_list_data_present_flag){
		//scaling_list_data();
		printf("pps_scaling_list_data_present_flag PRESENT\n");
	}
	pps.lists_modification_present_flag = read_u(1);
	pps.log2_parallel_merge_level_minus2 = read_ue();
	pps.slice_segment_header_extension_present_flag = read_u(1);
	pps.pps_extension_flag=read_u(1);



	picture_parameter_sets[pps.pps_pic_parameter_set_id] = pps;
	sequence_param = &(sequence_parameter_sets[pps.pps_seq_parameter_set_id]);

}

void slice_segment_layer_rbsp(slice_segment_struct* ssl){
	ssl->first_slice_segment_in_pic_flag = read_u(1);
	if (RapPicFlag){
		ssl->no_output_of_prior_pics_flag = read_u(1);
	}
	ssl->slice_pic_parameter_set_id = read_ue();

	picture_param = &(picture_parameter_sets[ssl->slice_pic_parameter_set_id]);
	sequence_param = &(sequence_parameter_sets[picture_param->pps_seq_parameter_set_id]);

	if (ssl->slice_pic_parameter_set_id != current_pic_parameter_id){
		current_pic_parameter_id = ssl->slice_pic_parameter_set_id;
		initialize_blocks();
	}

	if (!ssl->first_slice_segment_in_pic_flag){
		if (picture_param->dependent_slice_segments_enabled_flag)
			ssl->dependent_slice_segment_flag = read_u(1);
		ssl->slice_segment_address = read_u(log2_int(sequence_param->PicSizeInCtbsY));
		if (!ssl->dependent_slice_segment_flag) {
			curr_slice++;
			SliceAddrRS = ssl->slice_segment_address;
		}
	} else {
		printf("New Frame\n");
		if ( ! (ctbAddrInTS==0 || ctbAddrInTS==sequence_param->PicSizeInCtbsY)) {
			printf("Non Completed Frame..exiting\n");
			while(1);
		}

		curr_slice = 0;
		curr_tile = -1;
		ssl->slice_segment_address = 0;
		ssl->dependent_slice_segment_flag = 0;
		ssl->short_term_ref_pic_set_sps_flag= 0;
		SliceAddrRS = 0;
	}
	axi_write(0x0140, curr_slice);
	axi_write(0x0100, SliceAddrRS);
	if (!ssl->dependent_slice_segment_flag ){
		ctbAddrInRS = ssl->slice_segment_address;
		ctbAddrInTS = CtbAddrRstoTs[ctbAddrInRS];

		int i;
		for (i=0;i<picture_param->num_extra_slice_header_bits;i++) read_u(1);
		ssl->slice_type = read_ue();
		axi_write(0x0060, ssl->slice_type);
		if (picture_param->output_flag_present_flag){
			ssl->pic_output_flag = read_u(1);
		}
		ssl->short_term_ref_pic_set_sps_flag= 1;

		int PicOrderCntMsb = 0;

		int MaxPicOrderCntLsb = 1<<(sequence_param->log2_max_pic_order_cnt_lsb_minus4+4);

		ssl->pic_order_cnt_lsb = 0;
		struct slice_0 h_10;
		h_10.header = 0x10;
		h_10.null = 0;
		h_10.POC = 0;
		if (!IdrPicFlag){
			ssl->pic_order_cnt_lsb = read_u(sequence_param->log2_max_pic_order_cnt_lsb_minus4+4);

			if ( (ssl->pic_order_cnt_lsb < prevPicOrderCntLsb) && ( ( prevPicOrderCntLsb -ssl->pic_order_cnt_lsb) >= (MaxPicOrderCntLsb /2 ))){
				PicOrderCntMsb = prevPicOrderCntMsb + MaxPicOrderCntLsb;
			} else if ( (ssl->pic_order_cnt_lsb > prevPicOrderCntLsb) && ( ( ssl->pic_order_cnt_lsb - prevPicOrderCntLsb)> (MaxPicOrderCntLsb/2))) {
				PicOrderCntMsb = prevPicOrderCntMsb - MaxPicOrderCntLsb;
			} else {
				PicOrderCntMsb = prevPicOrderCntMsb;
			}
			PicOrderCntVal = PicOrderCntMsb + ssl->pic_order_cnt_lsb;
			h_10.POC = PicOrderCntVal;
			ssl->short_term_ref_pic_set_sps_flag = read_u(1);
			ssl->short_term_ref_pic_set_idx=0;
			if (!ssl->short_term_ref_pic_set_sps_flag){
				short_term_ref_pic_set(sequence_param, sequence_param->num_short_term_ref_pic_sets);
				//fprintf(stderr,"Short term ref pic set sps=0 havent tested\n");
			}
			else if (sequence_param->num_short_term_ref_pic_sets>1){
				int bits = log2_int(sequence_param->num_short_term_ref_pic_sets);
				ssl->short_term_ref_pic_set_idx = read_u(bits);
			}
			if (sequence_param->long_term_ref_pics_present_flag){
				fprintf(stderr,"long term ref pictures not supported\n"); //TODO
			}
			ssl->slice_temporal_mvp_enable_flag = 0;
			if (sequence_param->sps_temporal_mvp_enable_flag){
				ssl->slice_temporal_mvp_enable_flag = read_u(1);
			}
		} else {
			if (IDR_POC_NOT_RESET) {
				PicOrderCntVal = ((PicOrderCntVal + 128)/256) * 256;
				PicOrderCntMsb = PicOrderCntVal;
			} else {
				PicOrderCntVal = 0;
			}
			h_10.POC = PicOrderCntVal;
		}
		if (RefPicture){
			prevPicOrderCntLsb = ssl->pic_order_cnt_lsb;
			prevPicOrderCntMsb = PicOrderCntMsb;
		}
		if (ssl->first_slice_segment_in_pic_flag){
			write_cabac_to_res( *(unsigned int*) (&h_10) );
			write_cabac_to_res( PicOrderCntVal );
			//fwrite(&h_10,4,2,cabac_to_residual);
			//axi_write(0x0500, (void *) &h_10); axi_write(0x0500, PicOrderCntVal);
		}
		//if (ssl->pic_order_cnt_lsb!=80) return; //TODO Debug
		//printf("POC %d\n",PicOrderCntVal);
		//if (PicOrderCntVal>=0) return;

		if (sequence_param->sample_adaptive_offset_enabled_flag){
			ssl->slice_sao_luma_flag = read_u(1);
			ssl->slice_sao_chroma_flag = read_u(1);
		}
		axi_write(0x00f0, (ssl->slice_sao_luma_flag<<1 | ssl->slice_sao_chroma_flag));
		if (ssl->slice_type < 2 ){
			ssl->num_ref_idx_active_override_flag = read_u(1);
			ssl->num_ref_idx_l0_active_minus1 = picture_param->num_ref_idx_l0_default_active_minus1;
			ssl->num_ref_idx_l1_active_minus1 = picture_param->num_ref_idx_l1_default_active_minus1;
			if (ssl->num_ref_idx_active_override_flag){
				ssl->num_ref_idx_l0_active_minus1 = read_ue();
				if (ssl->slice_type==0){
					ssl->num_ref_idx_l1_active_minus1 = read_ue();
				}
			}
			axi_write(0x00a0, ssl->num_ref_idx_l0_active_minus1);
			axi_write(0x00b0, ssl->num_ref_idx_l1_active_minus1);
			if (picture_param->lists_modification_present_flag){
				fprintf(stderr,"lists modification present\n");
			}
			if (ssl->slice_type==0){
				ssl->mvd_l1_zero_flag = read_u(1);
			}
			axi_write(0x00c0, ssl->mvd_l1_zero_flag);
			ssl->cabac_init_flag = 0;
			if (picture_param->cabac_init_present_flag){
				ssl->cabac_init_flag = read_u(1);
				//print_loc();
			}

			if (ssl->slice_temporal_mvp_enable_flag){
				ssl->collocated_from_l0_flag=1;
				if (ssl->slice_type==0){
					ssl->collocated_from_l0_flag = read_u(1);
				}
				ssl->collocated_ref_idx=0;
				if ( (ssl->collocated_from_l0_flag && ssl->num_ref_idx_l0_active_minus1>0) || (!ssl->collocated_from_l0_flag && ssl->num_ref_idx_l1_active_minus1>0)){
					ssl->collocated_ref_idx = read_ue();
				}
			}
			if ( (picture_param->weighted_pred_flag && ssl->slice_type==1) || (picture_param->weighted_bipred_flag && ssl->slice_type==0)){
				fprintf(stderr,"Weighted Prediction Not enabled\n"); //TODO
				//pred_weight_table();
			}
			ssl->five_minus_max_num_merge_cand = read_ue();
			MaxNumMergeCand = 5-ssl->five_minus_max_num_merge_cand;
			axi_write(0x0090, MaxNumMergeCand);
		}
//		print_loc();
		ssl->slice_qp_delta = read_se();
		SliceQPY = 26 + picture_param->init_qp_minus26 + ssl->slice_qp_delta;
		ssl->slice_cb_qp_offset = 0;
		ssl->slice_cr_qp_offset = 0;
		ssl->slice_loop_filter_across_slices_enabled_flag = 0;
		ssl->slice_beta_offset_div2 = picture_param->pps_beta_offset_div2;
		ssl->slice_tc_offset_div2 = picture_param->pps_tc_offset_div2;
		ssl->slice_disable_deblocking_filter_flag = picture_param->pps_disable_deblocking_filter_flag;
		if (picture_param->pps_slice_chroma_qp_offsets_present_flag){
			//printf("slice_chroma_qp_offsets present\n"); exit(0);
			ssl->slice_cb_qp_offset = read_se();
			ssl->slice_cr_qp_offset = read_se();
		}
		if (picture_param->deblocking_filter_control_present_flag){
			ssl->deblocking_filter_override_flag = 0;

			if (picture_param->pps_deblocking_filter_override_enabled_flag){
				ssl->deblocking_filter_override_flag = read_u(1);
			}
			if (ssl->deblocking_filter_override_flag){
				ssl->slice_disable_deblocking_filter_flag = read_u(1);
				if (!ssl->slice_disable_deblocking_filter_flag){
					ssl->slice_beta_offset_div2 = read_se();
					ssl->slice_tc_offset_div2 = read_se();
				}
			}
		}
		if (picture_param->loop_filter_across_slices_enabled_flag && (ssl->slice_sao_luma_flag || ssl->slice_sao_chroma_flag || !ssl->slice_disable_deblocking_filter_flag )){
			ssl->slice_loop_filter_across_slices_enabled_flag = read_u(1);
		}
	} else {
		//fprintf(stderr,"Dependent Slices Not supported\n"); //TODO
	}
	ssl->num_entry_point_offsets = 0;

	int i;
	int partition_begin[256]; // 256 - max (no. of ctu rows = 4096/16, max_tiles=121)
	int partition_len[256];

	if (picture_param->tiles_enabled_flag || picture_param->entropy_coding_sync_enabled_flag){
		ssl->num_entry_point_offsets = read_ue();
		if (ssl->num_entry_point_offsets>0){
			int offset_len_minus_1 = read_ue();
			int offset;
			for (i =0;i<ssl->num_entry_point_offsets;i++){
				offset =read_u(offset_len_minus_1+1);
				partition_len[i] = offset + 1;
				//printf("partition %d offset %d\n",i,offset);
			}
			//fprintf(stderr,"Entry point offsets >0\n"); //TODO
		}
	}
	if (picture_param->slice_segment_header_extension_present_flag){
		int header_length = read_ue();
		for (i=0;i<header_length;i++) {
			int a = read_u(8);
			a++;
		}
		//fprintf(stderr,"Slice Header Extension Not implemented\n"); //TODO
	}
	//if (current->bit!=0) // remove for old streams
		//axi_write(0x810,1);
		byte_alignment();

	int num_partitions = ssl->num_entry_point_offsets + 1;
	int current_partition = 0;
	int current_partition_pos = 0;
	//partition_begin[0] = current->pos;
	//for (i=1;i<num_partitions;i++){
	//	partition_begin[i] = partition_begin[i-1] + partition_len[i-1];
	//}
	//partition_len[num_partitions-1] = current->len - partition_begin[num_partitions-1];

	int initType;
	if (ssl->slice_type==SLICE_I)
		initType = 0;
	else if (ssl->slice_type==SLICE_P)
		initType = ssl->cabac_init_flag ? 2 : 1;
	else
		initType = ssl->cabac_init_flag ? 1 : 2;
	axi_write(0x0640, initType);
	if (!ssl->dependent_slice_segment_flag) {
		int xCtb = (ctbAddrInRS % sequence_param->PicWidthInCtbsY) << sequence_param->Log2CtbSizeY;
		int yCtb = (ctbAddrInRS / sequence_param->PicWidthInCtbsY) << sequence_param->Log2CtbSizeY;
		struct new_slice_tile nsl;
		*(( int*) (&nsl)) = 0;
		nsl.header = 0x15;
		nsl.xC = xCtb;
		nsl.yC = yCtb;

		write_cabac_to_res( *(unsigned int*) (&nsl) );

		struct slice_1 h_11;
		*(( int*) (&h_11)) = 0;
		h_11.header = 0x11;
		h_11.short_term_ref_pic_sps = ssl->short_term_ref_pic_set_sps_flag;
		h_11.short_term_ref_pic_idx = ssl->short_term_ref_pic_set_idx;
		h_11.temporal_mvp_enabled = ssl->slice_temporal_mvp_enable_flag;
		h_11.sao_luma = ssl->slice_sao_luma_flag;
		h_11.sao_chroma = ssl->slice_sao_chroma_flag;
		h_11.num_ref_idx_l0_minus1 = ssl->num_ref_idx_l0_active_minus1;
		h_11.num_ref_idx_l1_minus1 = ssl->num_ref_idx_l1_active_minus1;
		h_11.five_minus_max_merge_cand = ssl->five_minus_max_num_merge_cand;
		h_11.slice_type = ssl->slice_type;
		h_11.collocated_from_l0 = ssl->collocated_from_l0_flag;
		h_11.slice_loop_filter_across_slices_enabled_flag = ssl->slice_loop_filter_across_slices_enabled_flag;
		write_cabac_to_res( *(unsigned int*) (&h_11) );

		struct slice_2 h_12;
		*(( int*) (&h_12)) = 0;
		h_12.header = 0x12;
		h_12.slice_cb_qp_offset = ssl->slice_cb_qp_offset;
		h_12.slice_cr_qp_offset = ssl->slice_cr_qp_offset;
		h_12.disable_dbf = ssl->slice_disable_deblocking_filter_flag;
		h_12.slice_beta_offset_div2 = ssl->slice_beta_offset_div2;
		h_12.slice_tc_offset_div2 = ssl->slice_tc_offset_div2;
		h_12.collocated_ref_idx = ssl->collocated_ref_idx;
		write_cabac_to_res( *(unsigned int*) (&h_12) );
		int i= sequence_param->num_short_term_ref_pic_sets;
		int j;
		if (!ssl->short_term_ref_pic_set_sps_flag) {
			struct slice_3 h_13;
			*(( int*) (&h_13)) = 0;
			h_13.header = 0x13;
			h_13.rps_id = i;
			h_13.num_negative = (sequence_param->stRPS[i].NumNegativePics  );
			h_13.num_positive = (sequence_param->stRPS[i].NumPositivePics  );
			write_cabac_to_res( *(unsigned int*) (&h_13) );
			for (j=0;j<sequence_param->stRPS[i].NumPositivePics;j++){
				struct slice_4 h_14;
				*(( int*) (&h_14)) = 0;
				h_14.header = 0x14;
				h_14.rps_id = i;
				h_14.used = sequence_param->stRPS[i].UsedByCurrPicS1[j];
				h_14.delta_POC = sequence_param->stRPS[i].DeltaPocS1[j];
				write_cabac_to_res( *(unsigned int*) (&h_14) );
			}
			for (j=0;j<sequence_param->stRPS[i].NumNegativePics;j++){
				struct slice_4 h_14;
				*(( int*) (&h_14)) = 0;
				h_14.header = 0x14;
				h_14.rps_id = i;
				h_14.used = sequence_param->stRPS[i].UsedByCurrPicS0[j];
				h_14.delta_POC = sequence_param->stRPS[i].DeltaPocS0[j];
				write_cabac_to_res( *(unsigned int*) (&h_14) );
			}
		}
	}

	int j;
	int new_tile=0;
	for (i=0;i<=picture_param->num_tile_rows_minus1;i++){
		for (j=0;j<=picture_param->num_tile_columns_minus1;j++){
			if (firstCtbRS[i][j]==ssl->slice_segment_address){
				new_tile=1;
				tile_i = i;
				tile_j = j;
				break;
			}
		}
	}

	if (!ssl->dependent_slice_segment_flag) {
		//initialize_cabac(&cabac_current,1);
		axi_write(0x0030,SliceQPY);
		init_cabac_contexts();
		init_cabac_engine();
		if (sequence_param->PicWidthInCtbsY ==1) { save_cabac_contexts(); }
	} else {
		//init_cabac_contexts();
		init_cabac_engine();
		// has to initialize contexts if start of new tile
		if (picture_param->tiles_enabled_flag && TileId[ctbAddrInTS] != TileId[ctbAddrInTS-1]){
			printf("New Tile (%d %d)\n", (ctbAddrInRS% sequence_param->PicWidthInCtbsY) << sequence_param->Log2CtbSizeY , (ctbAddrInRS / sequence_param->PicWidthInCtbsY ) << sequence_param->Log2CtbSizeY);
			init_cabac_contexts();
			axi_write(0x0030,SliceQPY);
		}

	}
	if (picture_param->entropy_coding_sync_enabled_flag && ((ctbAddrInTS %sequence_param->PicWidthInCtbsY)==0)){
		axi_write(0x0030,SliceQPY);
		if (SliceAddrRS <= ((ctbAddrInTS - sequence_param->PicWidthInCtbsY)+1)) {// slice starts before the previous ctu rows 2nd ctu (otherwise no valid context to load)
			load_cabac_contexts();
			axi_write(0x0610,(unsigned int) 0);
		}
	}


	if (new_tile) // new tile
		send_new_tile();

	//printf("Slice Header Done\n");
	//while(1);

	axi_write(0x0000, ctbAddrInTS); // Start cu_offload
	axi_write(0x03f0, 0x00000001); // Start cu_offload
	int end_of_slice_segment_flag=0;
	do {
		int ret;
		while ( (ret = *(volatile u32*)(CABAC_BASE+ 0x400)) > 65535 ){
			usleep(1);
		}
		ctbAddrInTS = ret & 0xffff;
		end_of_slice_segment_flag= *(volatile u32*)(CABAC_BASE+ 0x440);
		ctbAddrInRS = CtbAddrTstoRs[ctbAddrInTS];
		if (end_of_slice_segment_flag) printf("POC %d ctbAddrTS %d\n",PicOrderCntVal, ctbAddrInTS);

		if (!end_of_slice_segment_flag &&
				(
						(picture_param->tiles_enabled_flag && TileId[ctbAddrInTS] != TileId[ctbAddrInTS-1]) ||
						(picture_param->entropy_coding_sync_enabled_flag &&  ((ctbAddrInTS %sequence_param->PicWidthInCtbsY)==0) ) // RS TS wont matter
				)
			) {
				{
				// seems to be working without this. not writing to axi_trans
					//read_cabac_end_of_sub_stream_one_bit();
				}

			byte_alignment_if(); // THis is clearly wrong

			current_partition++;
			current_partition_pos=0;
			if (picture_param->tiles_enabled_flag) {
				//printf("New Tile (%d %d) offset %d\n", (ctbAddrInRS% sequence_param->PicWidthInCtbsY) << sequence_param->Log2CtbSizeY , (ctbAddrInRS / sequence_param->PicWidthInCtbsY ) << sequence_param->Log2CtbSizeY,current->pos);
				tile_j++;
				if (tile_j == picture_param->num_tile_columns_minus1+1){
					tile_j=0;
					tile_i++;
				}
				send_new_tile();
				axi_write(0x0030,SliceQPY);

				init_cabac_contexts();
				init_cabac_engine();

				axi_write(0x0000, ctbAddrInTS); // Start cu_offload
				axi_write(0x03f0, 0x00000001); // Start cu_offload

			}
			if (picture_param->entropy_coding_sync_enabled_flag) { // no tiles assumed
				int mod = ctbAddrInTS % sequence_param->PicWidthInCtbsY;
				// Also has to save if PicWidthInCtbsY ==2
				if (sequence_param->PicWidthInCtbsY ==2) {
					//axi_write(0x0600,(unsigned int) 0); // save_cabac
					save_cabac_contexts();
				}

				axi_write(0x0030,SliceQPY);

				if (SliceAddrRS <= ((ctbAddrInTS - sequence_param->PicWidthInCtbsY)+1))
					load_cabac_contexts();
				else
					init_cabac_contexts();
				init_cabac_engine();

				axi_write(0x0000, ctbAddrInTS); // Start cu_offload
				axi_write(0x03f0, 0x00000001); // Start cu_offload
			}
		}
	} while(!end_of_slice_segment_flag);

}

void short_term_ref_pic_set(sequence_param_struct* sps, int idxRps){ // 1st arg not needed
	int printing =0;
	//if (idxRps==2) printing =1;
//	print_loc();
	int inter_ref_pic_set_prediction_flag = (idxRps==0)?0: read_u(1);
	if (inter_ref_pic_set_prediction_flag){
		int delta_idx_minus1=0;
		if (idxRps==sps->num_short_term_ref_pic_sets){
			delta_idx_minus1 = read_ue();
		}
		if (printing) printf("short_term_ref_pic%d inter_ref.._flag=%d delta=%d\n",idxRps,inter_ref_pic_set_prediction_flag,delta_idx_minus1);

		int delta_rps_sign = read_u(1);
		int abs_delta_rps_minus1 = read_ue();
		int RIdx = idxRps - (delta_idx_minus1+1);
		if (printing) printf("delta_rps=%d abs=%d\n",delta_rps_sign,abs_delta_rps_minus1);
		int DeltaRPS = (1-2*delta_rps_sign)*(abs_delta_rps_minus1+1);
		int NumDeltaPocs = sps->stRPS[RIdx].NumDeltaPocs;
		int j;
		if (printing) printf("NumDeltaPocs=%d\n",NumDeltaPocs);
		int used_by_curr_pic_flag[16+1];
		int use_delta_flag[16+1];

		for (j=0;j<=NumDeltaPocs;j++){
			used_by_curr_pic_flag[j] = read_u(1);
			use_delta_flag[j] = 1;
			if (!used_by_curr_pic_flag[j]) {
				use_delta_flag[j] = read_u(1);
			}
		}
		// Negative
		int i=0;
		for (j=sps->stRPS[RIdx].NumPositivePics-1;j>=0;j--){
			int dPoc = sps->stRPS[RIdx].DeltaPocS1[j] + DeltaRPS;
			if (dPoc <0 && use_delta_flag[ sps->stRPS[RIdx].NumNegativePics +j ]){
				sps->stRPS[idxRps].DeltaPocS0[i] = dPoc;
				sps->stRPS[idxRps].UsedByCurrPicS0[i++] = used_by_curr_pic_flag[ sps->stRPS[RIdx].NumNegativePics +j];
			}
		}
		if (DeltaRPS<0 && use_delta_flag[sps->stRPS[RIdx].NumDeltaPocs]){
			sps->stRPS[idxRps].DeltaPocS0[i] = DeltaRPS;
			sps->stRPS[idxRps].UsedByCurrPicS0[i++] = used_by_curr_pic_flag[ sps->stRPS[RIdx].NumDeltaPocs];
		}
		for (j=0;j<sps->stRPS[RIdx].NumNegativePics;j++){
			int dPoc = sps->stRPS[RIdx].DeltaPocS0[j] + DeltaRPS;
			if (dPoc <0 && use_delta_flag[j]){
				sps->stRPS[idxRps].DeltaPocS0[i] = dPoc;
				sps->stRPS[idxRps].UsedByCurrPicS0[i++] = used_by_curr_pic_flag[j];
			}
		}
		sps->stRPS[idxRps].NumNegativePics = i;

		//Positive
		i=0;
		for (j=sps->stRPS[RIdx].NumNegativePics-1;j>=0;j--){
			int dPoc = sps->stRPS[RIdx].DeltaPocS0[j] + DeltaRPS;
			if (dPoc >0 && use_delta_flag[j]){
				sps->stRPS[idxRps].DeltaPocS1[i] = dPoc;
				sps->stRPS[idxRps].UsedByCurrPicS1[i++] = used_by_curr_pic_flag[j];
			}
		}
		if (DeltaRPS>0 && use_delta_flag[sps->stRPS[RIdx].NumDeltaPocs]){
			sps->stRPS[idxRps].DeltaPocS1[i] = DeltaRPS;
			sps->stRPS[idxRps].UsedByCurrPicS1[i++] = used_by_curr_pic_flag[ sps->stRPS[RIdx].NumDeltaPocs];
		}
		for (j=0;j<sps->stRPS[RIdx].NumPositivePics;j++){
			int dPoc = sps->stRPS[RIdx].DeltaPocS1[j] + DeltaRPS;
			if (dPoc >0 && use_delta_flag[ sps->stRPS[RIdx].NumNegativePics +j]){
				sps->stRPS[idxRps].DeltaPocS1[i] = dPoc;
				sps->stRPS[idxRps].UsedByCurrPicS1[i++] = used_by_curr_pic_flag[sps->stRPS[RIdx].NumNegativePics +j];
			}
		}
		sps->stRPS[idxRps].NumPositivePics = i;
		sps->stRPS[idxRps].NumDeltaPocs = sps->stRPS[idxRps].NumNegativePics + sps->stRPS[idxRps].NumPositivePics;

	} else {
		int num_negative_pics = read_ue();
		int num_positive_pics = read_ue();
		sps->stRPS[idxRps].NumNegativePics=num_negative_pics;
		sps->stRPS[idxRps].NumPositivePics=num_positive_pics;
		if (printing) printf("short_term_ref_pic%d negative=%d positive=%d\n",idxRps,num_negative_pics,num_positive_pics);
		int i;
		for (i=0;i<num_negative_pics;i++){

			int delta_poc_s0_minus1 = read_ue();
			sps->stRPS[idxRps].UsedByCurrPicS0[i]= read_u(1);
			if (i==0){
				sps->stRPS[idxRps].DeltaPocS0[i] = -(delta_poc_s0_minus1+1);
			} else {
				sps->stRPS[idxRps].DeltaPocS0[i] =  sps->stRPS[idxRps].DeltaPocS0[i-1] - (delta_poc_s0_minus1+1);
			}

		}
		for (i=0;i<num_positive_pics;i++){
			int delta_poc_s1_minus1 = read_ue();
			sps->stRPS[idxRps].UsedByCurrPicS1[i]= read_u(1);
			if (i==0){
				sps->stRPS[idxRps].DeltaPocS1[i] = (delta_poc_s1_minus1+1);
			} else {
				sps->stRPS[idxRps].DeltaPocS1[i] =  sps->stRPS[idxRps].DeltaPocS1[i-1] + (delta_poc_s1_minus1+1);
			}
		}
		sps->stRPS[idxRps].NumDeltaPocs = sps->stRPS[idxRps].NumNegativePics + sps->stRPS[idxRps].NumPositivePics;
	}
}


void profile_tier_level(profile_tier_level_struct* ptl, int profilePresentFlag, int maxNumSubLayersMinus1){
	int i;
	ptl->general_profile_space = read_u(2);
	ptl->general_tier_flag = read_u(1);
	ptl->general_profile_idc = read_u(5);
	ptl->general_profile_compatibility = read_u(8)<<24 | read_u(8)<<16 | read_u(8)<<8 | read_u(8);
	for (i=0;i<6;i++)
		read_u(8);
	ptl->general_level_idc = read_u(8);

	for (i=0;i<maxNumSubLayersMinus1;i++){
		printf("Error: Profile Tier Level\n");
	}
}
void bit_rate_pic_rate_info(void* brpr,int TempLevelLow, int TempLevelHigh ){
	int i;
	for (i=TempLevelLow;i<=TempLevelHigh;i++){
		int bit_rate_info_present_flag = read_u(1);
		int pic_rate_info_present_flag = read_u(1);
		if (bit_rate_info_present_flag){
			printf("Error: Bit Rate Info\n");
		}
		if (pic_rate_info_present_flag){
			printf("Error: Pic Rate Info\n");
		}
	}

}

void initialize_scanOrder(){
	int block_size;
	for (block_size=0;block_size<4;block_size++){
		// diag
		int i=0,x=0,y=0;
		int stopLoop = 0;
		int blk_size = (1<<block_size);
		while(!stopLoop){
			while (y>=0){
				if (x<blk_size && y<blk_size){
					ScanOrder[block_size][0][i][0] = x;
					ScanOrder[block_size][0][i][1] = y;
//					printf("size %d %d (%d,%d)\n",blk_size,i,x,y);
					i++;
				}
				y--;
				x++;
			}
			y=x;
			x=0;
			if (i>=blk_size*blk_size) stopLoop = 1;
		}
		//6.5.4
		i=0;
		for (y=0;y<blk_size;y++){
			for (x=0;x<blk_size;x++){
				ScanOrder[block_size][1][i][0]=x;
				ScanOrder[block_size][1][i][1]=y;
				i++;
			}
		}
		//6.5.5
		i=0;
		for (x=0;x<blk_size;x++){
			for (y=0;y<blk_size;y++){
				ScanOrder[block_size][2][i][0]=x;
				ScanOrder[block_size][2][i][1]=y;
				i++;
			}
		}
	}
}

unsigned int read_u(int n){
	if (n >16) printf("n>16\n");
	unsigned int r=0;
	if (n>8) {
		r = READ_U(n-8);
		r <<= 8;
		n=8;
	}
	r |= READ_U(n);
	return r;
}

unsigned int read_ue(){
	int leadingZeroBits = -1;
	int b;
	for (b=0;!b;leadingZeroBits++){
		b = read_u(1);
	}
	if (leadingZeroBits>20) fprintf(stderr,"Large leading zeros in ue(v)\n");

	unsigned int suffix = 0;
	if (leadingZeroBits > 8) suffix = read_u(leadingZeroBits-8) << 8;
	suffix += read_u(leadingZeroBits >8 ? 8 : leadingZeroBits);
	return ((( 1<< leadingZeroBits) - 1 ) + suffix);

}
int read_se(){
	unsigned int d = read_ue();
	int sign = (d%2 == 0) ? -1 : 1;
	int mag = (d+1)/2;
	return sign*mag;
}


void axi_write(unsigned int addr, unsigned int data){
	*(volatile u32*)(CABAC_BASE+ addr) = data;
}
void write_cabac_to_res(unsigned int data){
	*(volatile u32*)(CABAC_BASE+ 0x500) = data;
}


void send_new_tile(){

	curr_tile++;

	axi_write(0x0150, (unsigned int) tile_i*(picture_param->num_tile_columns_minus1+1) + tile_j);
	axi_write(0x0160, (unsigned int) firstCtbRS[tile_i][tile_j]);
	axi_write(0x0170, (unsigned int) colWidth[tile_j]);
	axi_write(0x0180, (unsigned int) colWidth[tile_j]* rowHeight[tile_i]);
	axi_write(0x0190, (unsigned int) colBd[tile_j]<<sequence_param->Log2CtbSizeY);


	struct new_slice_tile nsl;
	*(( int*) (&nsl)) = 0;
	nsl.header = 0x16;
	nsl.xC = colBd[tile_j]<<sequence_param->Log2CtbSizeY;
	nsl.yC = rowBd[tile_i]<<sequence_param->Log2CtbSizeY;
	//nsl.xC =0;
	//nsl.yC =0;
	write_cabac_to_res( *(unsigned int*) (&nsl) );
	//struct new_tile_width ntw;
	//*(( int*) (&ntw)) = 0;
	//ntw.header = 0x17;

	int width = 0;

	if (tile_j==picture_param->num_tile_columns_minus1) {
		width = sequence_param->pic_width_in_luma_samples-(colBd[tile_j]<<sequence_param->Log2CtbSizeY);
	} else {
		width = colWidth[tile_j]<<sequence_param->Log2CtbSizeY;
	}
	if (!picture_param->tiles_enabled_flag) {
		width = sequence_param->pic_width_in_luma_samples;
	}
	//ntw.width = width;
	//unsigned int ntw = 0x17 << 24 | (width << 8);
	unsigned int ntw = width << 8 | 0x17;
	write_cabac_to_res( ntw );
}

void byte_alignment(){
	axi_write(0x810,0);
}
void byte_alignment_if(){
	axi_write(0x820,0);
}
void init_cabac_contexts(){
	axi_write(0x630,0);
}
void save_cabac_contexts(){
	axi_write(0x600,0);
}
void load_cabac_contexts(){
	axi_write(0x610,0);
}
void init_cabac_engine(){
	axi_write(0x620,0);
}

void initialize_blocks(){

	//6.5.1
	int i;
	//colWidth = (int*) malloc(sizeof(int)* (pps.num_tile_columns_minus1+1) );
	//colBd =(int*) malloc(sizeof(int)* (pps.num_tile_columns_minus1+2) );
	if (picture_param->uniform_spacing_flag){
		for(i=0;i<=picture_param->num_tile_columns_minus1;i++){
			colWidth[i] = ( (i+1)*sequence_param->PicWidthInCtbsY )/ (picture_param->num_tile_columns_minus1 +1) - ( i*sequence_param->PicWidthInCtbsY )/ (picture_param->num_tile_columns_minus1 +1);
		}
	} else {
		colWidth[picture_param->num_tile_columns_minus1] = sequence_param->PicWidthInCtbsY;
		for (i=0;i<picture_param->num_tile_columns_minus1;i++){
			colWidth[i] = picture_param->column_width_minus1[i] +1;
			colWidth[picture_param->num_tile_columns_minus1] -= colWidth[i];
		}

	}
	for (colBd[0]=0,i=0;i<=picture_param->num_tile_columns_minus1;i++)
		colBd[i+1] = colBd[i] + colWidth[i];

	//rowHeight = (int*) malloc(sizeof(int)* (picture_param->num_tile_rows_minus1+1) );
	//rowBd =(int*) malloc(sizeof(int)* (picture_param->num_tile_rows_minus1+2) );
	if (picture_param->uniform_spacing_flag){
		for(i=0;i<=picture_param->num_tile_rows_minus1;i++){
			rowHeight[i] = ( (i+1)*sequence_param->PicHeightInCtbsY )/ (picture_param->num_tile_rows_minus1 +1) - ( i*sequence_param->PicHeightInCtbsY )/ (picture_param->num_tile_rows_minus1 +1);
		}
	} else {
		rowHeight[picture_param->num_tile_rows_minus1] = sequence_param->PicHeightInCtbsY;
		for (i=0;i<picture_param->num_tile_rows_minus1;i++){
			rowHeight[i] = picture_param->row_height_minus1[i] +1;
			rowHeight[picture_param->num_tile_rows_minus1] -= rowHeight[i];
		}
	}
	for (rowBd[0]=0,i=0;i<=picture_param->num_tile_rows_minus1;i++)
		rowBd[i+1] = rowBd[i] + rowHeight[i];
	int j;

	int ctbAddrTS=0;
	for (i=0;i<=picture_param->num_tile_rows_minus1;i++){
		for (j=0;j<=picture_param->num_tile_columns_minus1;j++){
			firstCtbTS[i][j] = ctbAddrTS;
			firstCtbRS[i][j] = rowBd[i]*sequence_param->PicWidthInCtbsY + colBd[j];
			ctbAddrTS+= (colWidth[j]*rowHeight[i]);
		}
	}

//	CtbAddrRstoTs =(short*) malloc(sizeof(int)*sequence_param->PicSizeInCtbsY);
	int ctbAddrRS;
	for (ctbAddrRS=0;ctbAddrRS<sequence_param->PicSizeInCtbsY;ctbAddrRS++){
		int tbX = ctbAddrRS % sequence_param->PicWidthInCtbsY;
		int tbY = ctbAddrRS / sequence_param->PicWidthInCtbsY;
		int tileX, tileY;
		for (i=0;i<=picture_param->num_tile_columns_minus1;i++)
			if (tbX >= colBd[i])
				tileX = i;
		for (i=0;i<=picture_param->num_tile_rows_minus1;i++)
			if (tbY >= rowBd[i])
				tileY = i;
		CtbAddrRstoTs[ctbAddrRS] = 0;
		for (i=0;i<tileX;i++)
			CtbAddrRstoTs[ctbAddrRS] += (rowHeight[tileY] * colWidth[i]);
		for (i=0;i<tileY;i++)
			CtbAddrRstoTs[ctbAddrRS] += (rowHeight[i] * sequence_param->PicWidthInCtbsY);
		CtbAddrRstoTs[ctbAddrRS] += (tbY-rowBd[tileY])*colWidth[tileX] + tbX - colBd[tileX];
//		printf("CtbAddrRS=%d TS=%d\n",ctbAddrRS, CtbAddrRstoTs[ctbAddrRS]);
	}
	//CtbAddrTstoRs =(int*) malloc(sizeof(int)*sequence_param->PicSizeInCtbsY);
	for (ctbAddrRS=0;ctbAddrRS<sequence_param->PicSizeInCtbsY;ctbAddrRS++){
		CtbAddrTstoRs[CtbAddrRstoTs[ctbAddrRS]] =  ctbAddrRS;
//		printf("CtbAddrTS=%d RS=%d\n",ctbAddrRS, CtbAddrTstoRs[ctbAddrRS]);
	}
//	TileId =(short*) malloc(sizeof(short)*PicSizeInCtbsY);
	int  tileIdx,x,y;
	for (j=0,tileIdx=0;j<=picture_param->num_tile_rows_minus1;j++){
		for (i=0;i<=picture_param->num_tile_columns_minus1;i++,tileIdx++){
			for (y=rowBd[j];y<rowBd[j+1];y++){
				for (x = colBd[i];x<colBd[i+1];x++){
					TileId[CtbAddrRstoTs[y*sequence_param->PicWidthInCtbsY+x]] = tileIdx;
				}
			}
		}
	}

	struct parameters_0 h_00;
	*(( int*) (&h_00)) = 0;
	h_00.header = 0x00;
	h_00.pic_width = sequence_param->pic_width_in_luma_samples;
	h_00.pic_height = sequence_param->pic_height_in_luma_samples;
	write_cabac_to_res( *(unsigned int*) (&h_00) );
	//axi_write(0x0500,(void *) &h_00);

	struct parameters_1 h_01;
	*(( int*) (&h_01)) = 0;
	h_01.header = 0x01;
	h_01.Log2CtbSizeY = sequence_param->Log2CtbSizeY;
	h_01.constrained_intra_pred = picture_param->constrained_intra_pred_flag;
	h_01.pps_cb_qp_offset = picture_param->pps_cb_qp_offset;
	h_01.pps_cr_qp_offset = picture_param->pps_cr_qp_offset;
	h_01.pps_beta_offset_div2 = picture_param->pps_beta_offset_div2;
	h_01.pps_tc_offset_div2 = picture_param->pps_tc_offset_div2;
	h_01.strong_intra_smoothing = sequence_param->strong_intra_smoothing_enable_flag;
	h_01.pcm_loop_filter_disable_flag = sequence_param->pcm_loop_filter_disable_flag;
	write_cabac_to_res( *(unsigned int*) (&h_01) );

	struct parameters_2 h_02;
	*(( int*) (&h_02)) = 0;
	h_02.header = 0x02;
	h_02.num_short_term_ref = sequence_param->num_short_term_ref_pic_sets;
	h_02.weighted_pred = picture_param->weighted_pred_flag;
	h_02.weighted_bipred = picture_param->weighted_bipred_flag;
	h_02.parallel_merge_level = picture_param->log2_parallel_merge_level_minus2;
	h_02.loop_filter_across_tiles_enabled_flag = picture_param->loop_filter_across_tiles_enabled_flag;
	h_02.scaling_list_enable_flag = sequence_param->scaling_list_enable_flag;
	write_cabac_to_res( *(unsigned int*) (&h_02) );

	for (i=0;i<sequence_param->num_short_term_ref_pic_sets;i++){
		struct parameters_3 h_03;
		*(( int*) (&h_03)) = 0;
		h_03.header = 0x03;
		h_03.rps_id = i;
		h_03.num_negative = (sequence_param->stRPS[i].NumNegativePics  );
		h_03.num_positive = (sequence_param->stRPS[i].NumPositivePics  );
		write_cabac_to_res( *(unsigned int*) (&h_03) );
		for (j=0;j<sequence_param->stRPS[i].NumPositivePics;j++){
			struct parameters_4 h_04;
			h_04.header = 0x04;
			h_04.rps_id = i;
			h_04.used = sequence_param->stRPS[i].UsedByCurrPicS1[j];
			h_04.delta_POC = sequence_param->stRPS[i].DeltaPocS1[j];
			write_cabac_to_res( *(unsigned int*) (&h_04) );
		}
		for (j=0;j<sequence_param->stRPS[i].NumNegativePics;j++){
			struct parameters_4 h_04;
			h_04.header = 0x04;
			h_04.rps_id = i;
			h_04.used = sequence_param->stRPS[i].UsedByCurrPicS0[j];
			h_04.delta_POC = sequence_param->stRPS[i].DeltaPocS0[j];
			write_cabac_to_res( *(unsigned int*) (&h_04) );
		}
	}

	axi_write(0x0010, sequence_param->pic_width_in_luma_samples);
	axi_write(0x0020, sequence_param->pic_height_in_luma_samples);
	axi_write(0x01a0, sequence_param->Log2CtbSizeY);
	axi_write(0x01b0, sequence_param->Log2MinCbSizeY);
	axi_write(0x01c0, sequence_param->Log2MaxTrafoSize);
	axi_write(0x01d0, sequence_param->Log2MinTrafoSize);

	axi_write(0x00d0, sequence_param->max_transform_hierarchy_depth_inter);
	axi_write(0x00e0, sequence_param->max_transform_hierarchy_depth_intra);
	axi_write(0x0080, sequence_param->amp_enabled_flag);
	axi_write(0x01e0, sequence_param->pcm_enabled_flag);

	if (sequence_param->pcm_enabled_flag) {
		axi_write(0x01f0, sequence_param->PCMBitDepthY);
		axi_write(0x0200, sequence_param->PCMBitDepthC);
		axi_write(0x0210, sequence_param->Log2MinIpcmCbSizeY);
		axi_write(0x0220, sequence_param->Log2MaxIpcmCbSizeY);
	}

	axi_write(0x0050, picture_param->sign_data_hiding_flag);
	axi_write(0x0070, picture_param->transform_skip_enabled_flag);
	axi_write(0x0110, picture_param->cu_qp_delta_enabled_flag);
	axi_write(0x0120, sequence_param->Log2MinCuQpDeltaSize);
	axi_write(0x0040, picture_param->transquant_bypass_enable_flag);
	axi_write(0x0130, picture_param->entropy_coding_sync_enabled_flag);


//	fclose(cabac_to_intra);
	//fclose(cabac_to_residual); exit(0);
}

unsigned int log2_int(unsigned int n){
	if (n<=2) return 1;
	if (n>=3 && n<= 4) return 2;
	if (n>=5 && n<= 8) return 3;
	if (n>=9 && n<= 16) return 4;
	if (n>=17 && n<= 32) return 5;
	if (n>=33 && n<= 64) return 6;
	if (n>=65 && n<= 128) return 7;
	if (n>=129 && n<= 256) return 8;
	if (n>=257 && n<= 512) return 9;
	if (n>=513 && n<= 1024) return 10;
	if (n>=1025 && n<= 2048) return 11;
	return 0;
}
