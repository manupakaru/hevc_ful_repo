/*
 * hevc.h
 *
 *  Created on: Dec 29, 2014
 *      Author: WINDOWS8
 */

#include "xil_types.h"
#include "stdio.h"
#include "math.h"

#ifndef HEVC_H_
#define HEVC_H_

typedef unsigned char BYTE;

unsigned int read_u(int n);
unsigned int read_ue(void);
int read_se();
int read_bit(void);
void byte_alignment(void);

void decode_next_NAL(void);

void hevc_main(void);

#define CABAC_BASE 0x43c00000

#define MAX_REF_PICS 16
#define MAX_RPS 17
#define MAX_WIDTH_CTU 64
#define MAX_WIDTH_CU 512
#define MAX_CTU 2048

#define TRAIL_N 0
#define TRAIL_R 1
#define RASL_N 8
#define RASL_R 9
#define IDR_W_DLP 19
#define CRA_NUT 21
#define NAL_VPS_NUT 32
#define NAL_SPS_NUT 33
#define NAL_PPS_NUT 34

#define SLICE_B 0
#define SLICE_P 1
#define SLICE_I 2

short curr_slice;
short curr_tile;
short tile_i, tile_j;
int same_slice_tile_left;
int same_slice_tile_up;

int SliceAddrRS;
//int PicOrderCntVal = 0;

#define IDR_POC_NOT_RESET 1

typedef struct {
	int NumNegativePics;
	int NumPositivePics;
	int UsedByCurrPicS0[MAX_REF_PICS];
	int UsedByCurrPicS1[MAX_REF_PICS];
	int DeltaPocS0[MAX_REF_PICS];
	int DeltaPocS1[MAX_REF_PICS];
	int NumDeltaPocs;
} short_term_ref_pic_set_struct;

typedef struct {
	int general_profile_space;
	int general_tier_flag;
	int general_profile_idc;
	unsigned int general_profile_compatibility;
	int general_level_idc;
} profile_tier_level_struct;

typedef struct {
	int vps_video_parameter_set_id;
	int vps_max_sub_layers_minus1;
	int vps_temporal_id_nesting_flag;
	profile_tier_level_struct vps_profile_tier_level;
	int vps_sub_layer_ordering_info_present_flag;
	int vps_max_dec_pic_buffering; // ideally these should be arrays. But we are using only 1 sub pic
	int vps_max_num_reorder_pics;
	int vps_max_latency_increase;
	int vps_max_layer_id;
	int vps_num_layer_sets_minus1;

	int vps_timing_info_present_flag;
	int vps_num_hrd_parameters;
	int vps_max_nuh_reserved_zero_layer_id;
	//int vps_num_op_sets_minus1;
	int vps_extension_flag;

} video_param_struct;

typedef struct {
	int sps_video_parameter_set_id;
	int sps_max_sub_layers_minus1;
	int sps_temporal_sub_layer_nesting_flag;
	profile_tier_level_struct sps_profile_tier_level;
	int sps_seq_parameter_set_id;
	int chroma_format_idc;
	int pic_width_in_luma_samples;
	int pic_height_in_luma_samples;
	int conformance_window_flag;
		int conf_win_left_offset;
		int conf_win_right_offset;
		int conf_win_top_offset;
		int conf_win_bottom_offset;
	int bit_depth_luma_minus8;
	int bit_depth_chroma_minus8;
	int log2_max_pic_order_cnt_lsb_minus4;
	int sps_sub_layer_ordering_info_present_flag;
		int sps_max_dec_pic_buffering; // ideally these should be arrays. But we are using only 1 sub pic
		int sps_max_num_reorder_pics;
		int sps_max_latency_increase;
	int log2_min_luma_coding_block_size_minus3;
	int log2_diff_max_min_luma_coding_block_size;
	int log2_min_transform_block_size_minus2;
	int log2_diff_max_min_transform_block_size;
	int max_transform_hierarchy_depth_inter;
	int max_transform_hierarchy_depth_intra;
	int scaling_list_enable_flag;
	int amp_enabled_flag; // asymetric motion partitions
	int sample_adaptive_offset_enabled_flag;
	int pcm_enabled_flag;
		int pcm_sample_bit_depth_luma_minus1;
		int pcm_sample_bit_depth_chroma_minus1;
		int log2_min_pcm_luma_coding_block_size_minus3;
		int log2_diff_max_min_pcm_luma_coding_block_size;
		int pcm_loop_filter_disable_flag;
		int PCMBitDepthY;
		int PCMBitDepthC;
		int Log2MinIpcmCbSizeY;
		int Log2MaxIpcmCbSizeY;
	int num_short_term_ref_pic_sets;
	int long_term_ref_pics_present_flag;
	int sps_temporal_mvp_enable_flag;
	int strong_intra_smoothing_enable_flag;
	int vui_parameters_present_flag;
	int sps_extension_flag;

	short_term_ref_pic_set_struct stRPS[MAX_RPS];
	int scaling_matrix_index[20];

	int Log2MinCbSizeY;
	int Log2CtbSizeY;
	int PicWidthInCtbsY;
	int PicHeightInCtbsY;
	int PicSizeInCtbsY;
	int Log2MinTrafoSize;
	int Log2MaxTrafoSize;
	int MaxTrafoDepth;
	int Log2MinCuQpDeltaSize;
} sequence_param_struct;

typedef struct {
	int pps_pic_parameter_set_id;
	int pps_seq_parameter_set_id ;
	int dependent_slice_segments_enabled_flag;
	int output_flag_present_flag;
	int sign_data_hiding_flag;
	int cabac_init_present_flag;
	int num_ref_idx_l0_default_active_minus1;
	int num_ref_idx_l1_default_active_minus1;
	int init_qp_minus26;
	int constrained_intra_pred_flag;
	int transform_skip_enabled_flag;
	int cu_qp_delta_enabled_flag;
	int diff_cu_qp_delta_depth;
	int pps_cb_qp_offset;
	int pps_cr_qp_offset;
	int pps_slice_chroma_qp_offsets_present_flag;
	int weighted_pred_flag;
	int weighted_bipred_flag;
	int transquant_bypass_enable_flag;
	int tiles_enabled_flag;
	int num_tile_columns_minus1;
	int num_tile_rows_minus1;
		int column_width_minus1[9];
		int row_height_minus1[9];
	int uniform_spacing_flag;
	int loop_filter_across_tiles_enabled_flag;
	int entropy_coding_sync_enabled_flag;
	int loop_filter_across_slices_enabled_flag;
	int deblocking_filter_control_present_flag;
	int pps_deblocking_filter_override_enabled_flag;
	int pps_disable_deblocking_filter_flag;
	int pps_beta_offset_div2;
	int pps_tc_offset_div2;
	int pps_scaling_list_data_present_flag;
	int lists_modification_present_flag;
	int log2_parallel_merge_level_minus2;
	int num_extra_slice_header_bits;
	int slice_segment_header_extension_present_flag;
	int pps_extension_flag;

} picture_param_struct;

typedef struct {
//	int IdrPicFlag;
//	int RapPicFlag; // random access picture nal_unit_type -(16,31)
	int first_slice_segment_in_pic_flag;
	int no_output_of_prior_pics_flag;
	int slice_pic_parameter_set_id;
	int dependent_slice_segment_flag;
	int slice_segment_address;
	int slice_type;
	int pic_output_flag;
	int pic_order_cnt_lsb;
	int short_term_ref_pic_set_sps_flag;
	int short_term_ref_pic_set_idx;
	int slice_temporal_mvp_enable_flag;

	int slice_sao_luma_flag;
	int slice_sao_chroma_flag;

	int num_ref_idx_active_override_flag;
	int num_ref_idx_l0_active_minus1;
	int num_ref_idx_l1_active_minus1;
	int mvd_l1_zero_flag;
	int cabac_init_flag;
	int collocated_from_l0_flag;
	int collocated_ref_idx;
	int five_minus_max_num_merge_cand;

	int slice_qp_delta;
	int slice_cb_qp_offset;
	int slice_cr_qp_offset;
	int deblocking_filter_override_flag;
	int slice_disable_deblocking_filter_flag;
	int slice_beta_offset_div2;
	int slice_tc_offset_div2;
	int slice_loop_filter_across_slices_enabled_flag;
	int num_entry_point_offsets;
} slice_segment_struct;



int IdrPicFlag;
int RapPicFlag;
int RefPicture;
int prevPicOrderCntMsb, prevPicOrderCntLsb;

int ctbAddrInRS;
int ctbAddrInTS;
int SliceQPY;

int MaxNumMergeCand;

int colWidth[MAX_WIDTH_CTU];
int rowHeight[MAX_WIDTH_CTU];
int colBd[MAX_WIDTH_CTU+1];
int rowBd[MAX_WIDTH_CTU+1];
short CtbAddrRstoTs[MAX_CTU];
short CtbAddrTstoRs[MAX_CTU];
short TileId[MAX_CTU];

int firstCtbTS[11][11];
int firstCtbRS[11][11];


//int ScalingMatrixValid[50];
//int ScalingMatrix[50][64];
//int ScalingMatrixDC[50];

short SliceId[MAX_CTU];

int ScanOrder[4][3][64][2];

struct parameters_0 {
	unsigned header: 8;
	unsigned pic_width: 12;
	unsigned pic_height: 12;
};
struct parameters_1 {
	unsigned header :8;
	unsigned Log2CtbSizeY : 3;
	signed pps_cb_qp_offset :5;
	signed pps_cr_qp_offset :5;
	signed pps_beta_offset_div2 : 4;
	signed pps_tc_offset_div2: 4;
	unsigned strong_intra_smoothing :1;
	unsigned constrained_intra_pred : 1;
	unsigned pcm_loop_filter_disable_flag : 1;
};
struct parameters_2 {
	unsigned header :8;
	unsigned num_short_term_ref : 6;
	unsigned weighted_pred:1;
	unsigned weighted_bipred:1;
	unsigned parallel_merge_level:3;
	unsigned loop_filter_across_tiles_enabled_flag: 1;
	unsigned scaling_list_enable_flag : 1;
};
struct parameters_3 {
	unsigned header :8;
	unsigned rps_id: 8;
	unsigned num_positive:8;
	unsigned num_negative:8;
};
struct parameters_4 {
	unsigned header :8;
	unsigned rps_id: 7;
	unsigned used:1;
	signed delta_POC:16;
};

struct scaling_list_pkt {
	unsigned header : 8;
	unsigned sizeId : 4;
	unsigned matrixId : 4;
	unsigned i : 8;
	unsigned val : 8;

};

struct slice_0 {
	unsigned header : 8;
	unsigned null:24;
	signed POC: 32;
};
struct new_slice_tile{
	unsigned header : 8;
	unsigned xC : 12;
	unsigned yC : 12;
};
struct new_tile_width {
	unsigned header :8;
	unsigned width :16;
};
struct slice_1{
	unsigned header :8;
	unsigned short_term_ref_pic_sps : 1;
	unsigned short_term_ref_pic_idx: 4;
	unsigned temporal_mvp_enabled : 1;
	unsigned sao_luma : 1;
	unsigned sao_chroma :1;
	unsigned num_ref_idx_l0_minus1 : 4;
	unsigned num_ref_idx_l1_minus1 : 4;
	unsigned five_minus_max_merge_cand : 3;
	unsigned slice_type :2;
	unsigned collocated_from_l0:1;
	unsigned slice_loop_filter_across_slices_enabled_flag: 1;
};
struct slice_2 {
	unsigned header :8;
	signed slice_cb_qp_offset:5;
	signed slice_cr_qp_offset:5;
	unsigned disable_dbf : 1;
	signed slice_beta_offset_div2 : 4;
	signed slice_tc_offset_div2 : 4;
	unsigned collocated_ref_idx : 5;
};
struct slice_3 {
	unsigned header :8;
	unsigned rps_id: 8;
	unsigned num_positive:8;
	unsigned num_negative:8;
};
struct slice_4 {
	unsigned header :8;
	unsigned rps_id: 7;
	unsigned used:1;
	signed delta_POC:16;
};

struct pred_weight_1 {
	unsigned header :8;
	unsigned luma_log2_weight_denom : 3;
	unsigned ChromaLog2WeightDenom  : 3;
};
struct pred_weight_2 {
	unsigned header : 8;
	unsigned ref_pic : 4;
	unsigned list : 1;
	unsigned cIdx : 2;
	signed Weight : 9;
	signed Offset : 8;
};

void video_parameter_set_rbsp(video_param_struct* vps);
void sequence_parameter_set_rbsp();
void picture_parameter_set_rbsp();
void slice_segment_layer_rbsp(slice_segment_struct* ssl);

void initialize_blocks();

void profile_tier_level(profile_tier_level_struct* ptl, int profilePresentFlag, int maxNumSubLayersMinus1);
void bit_rate_pic_rate_info(void* brpr,int TempLevelLow, int TempLevelHigh );
void pred_weight_table();
void scaling_list_data();
void short_term_ref_pic_set(sequence_param_struct* sps,int  idxRps);

int available_z(int xCurr,int  yCurr,int xN,int yN);
void initialize_scanOrder(void);

void axi_write(unsigned int addr, unsigned int data);
void write_cabac_to_res(unsigned int data);
void send_new_tile();

void byte_alignment();
void byte_alignment_if();

void init_cabac_contexts();
void save_cabac_contexts();
void load_cabac_contexts();
void init_cabac_engine();

unsigned int log2_int(unsigned int n);
#endif /* HEVC_H_ */
