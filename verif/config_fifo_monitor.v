`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:16:18 07/28/2014 
// Design Name: 
// Module Name:    cabac_to_residual_monitor 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module config_fifo_monitor(
    clk,
    reset,
    out,
    empty,
    rd_en
 );
 
 //---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
	parameter STRING = "";
	parameter FILE_NAME           = "";
	parameter OUT_VERIFY  = 1;
	parameter DEBUG = 0;


//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------

	input clk;
	input reset;
	input  empty,rd_en; 
	input [32-1:0] 	out;
	
	// synthesis translate_off
	integer file_in;
	integer i;
	reg[31:0] read_res;
	
	initial begin
	if(OUT_VERIFY) begin
		file_in = $fopen(FILE_NAME,"rb");
		if(file_in) begin
			
		end
		else begin
			$display("%s file not open!!",STRING);
			$stop;
		end
	end
end

always@(posedge clk) begin
   if(reset) begin
	end
	else begin
		if(!empty) begin
			if(rd_en) begin
				if(DEBUG ==1) begin
					$display("%s: %d read from fifo %m %x",STRING,$time,out);
				end
				if(OUT_VERIFY) begin
								
					 read_res[31:24] = $fgetc(file_in);
					 read_res[23:16] = $fgetc(file_in);
					 read_res[15: 8] = $fgetc(file_in);
					 read_res[ 7: 0] = $fgetc(file_in);
					
					if(read_res != out) begin
						$display("%s: wrong expected %x actual %x",STRING,read_res,out);
						$stop;
					end
					else begin
						if(DEBUG) begin
							$display("%s: correct expected %x actual %x",STRING,read_res,out);
							
						end
					end
					
				end
			end
		end
		else begin
			if(rd_en) begin
				$display("%d trying to read while empty %m!!",$time);
				$stop;
			end
		end
	end
end


// synthesis translate_on
	
endmodule
