`timescale 1ns / 1ps
module new_interpl_filter_verfiy(
    clk,
    xT_4x4_in,
    yT_4x4_in,
    luma_filter_ready2,
    luma_filter_out2 ,
    luma_filter_ready ,
    luma_filter_out
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"
    `define DUMP
	// `define COMPARE
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
	parameter BLOCK_WIDTH_4x4 = 4;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4_in;
    reg   [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4_in_reg;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4_in;
    reg   [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4_in_reg;
	input 	luma_filter_ready2; 
	input 	luma_filter_ready; 
	input [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 * FILTER_PIXEL_WIDTH -1:0]         luma_filter_out2;
	input [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 * FILTER_PIXEL_WIDTH -1:0]         luma_filter_out;
	reg [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 * FILTER_PIXEL_WIDTH -1:0]         luma_filter_out_reg;
	reg [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 * FILTER_PIXEL_WIDTH -1:0]         luma_filter_out_reg2;
	reg [BLOCK_WIDTH_4x4 *  BLOCK_WIDTH_4x4 * FILTER_PIXEL_WIDTH -1:0]         luma_filter_out_reg3;

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
	integer state;
	integer file_out;
	initial begin
		state =0;
	end
	`ifdef DUMP
		initial begin
			file_out = $fopen("luma_filter_dump.txt","wb");
		end
		always@(posedge clk) begin
			if(luma_filter_ready) begin
				$fwrite(file_out,"%d %d %x\n",xT_4x4_in, yT_4x4_in, luma_filter_out);
			end
		end
	`endif
	
	`ifdef COMPARE
		initial begin
			file_out = $fopen("luma_filter_dump.txt","rb");
		end
		always@(posedge clk) begin
			if(luma_filter_ready) begin
				$fscanf(file_out,"%d %d %x\n",xT_4x4_in_reg, yT_4x4_in_reg, luma_filter_out_reg);
				if(xT_4x4_in_reg == xT_4x4_in && yT_4x4_in_reg == yT_4x4_in && luma_filter_out_reg == luma_filter_out) begin
				end
				else begin
					$display("intple mismatch",$time);
					$display("hard %d %d %x",xT_4x4_in, yT_4x4_in, luma_filter_out);
					$display("soft %d %d %x",xT_4x4_in_reg, yT_4x4_in_reg, luma_filter_out_reg);
					$stop;					
				end
			end
		end	
	`endif
	
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

`ifndef COMPARE
/*	always@(posedge clk) begin
		case(state) 
			0: begin
				if(luma_filter_ready & luma_filter_ready2) begin
					if(luma_filter_out2 == luma_filter_out) begin
						// $display(" %d good",$time);
					end
					else begin
						$display(" %d intple mismatch",$time);
						$stop;
					end
				end
				else if(luma_filter_ready) begin
					state <= 1;
					luma_filter_out_reg <= luma_filter_out;
				end
				else if(luma_filter_ready2) begin
					state <= 2;
					luma_filter_out_reg <= luma_filter_out2;
				end
			end
			1: begin
				if(luma_filter_ready2 & luma_filter_ready) begin
					if(luma_filter_out2 == luma_filter_out_reg) begin
						// $display(" %d good",$time);	
					end
					else begin
						$display(" %d intple mismatch",$time);
						$stop;
					end	
					luma_filter_out_reg <= luma_filter_out;
					luma_filter_out_reg2 <= luma_filter_out_reg;					
					luma_filter_out_reg3 <= luma_filter_out_reg2;					
				end
				else if(luma_filter_ready2) begin
					if(luma_filter_out2 == luma_filter_out_reg) begin
						// $display(" %d good",$time);	
					end
					else begin
						$display(" %d intple mismatch",$time);
						$stop;
					end	
					state <= 0;
				end
				else if(luma_filter_ready) begin
					luma_filter_out_reg <= luma_filter_out;
					luma_filter_out_reg2 <= luma_filter_out_reg;
					state <= 3;
				end
			end
			2: begin
				if(luma_filter_ready2 & luma_filter_ready) begin
					if(luma_filter_out == luma_filter_out_reg) begin
						// $display(" %d good",$time);	
					end
					else begin
						$display(" %d intple mismatch",$time);
						$stop;
					end	
					luma_filter_out_reg <= luma_filter_out2;
					luma_filter_out_reg2 <= luma_filter_out_reg;					
					luma_filter_out_reg3 <= luma_filter_out_reg2;					
				end
				else if(luma_filter_ready) begin
					if(luma_filter_out == luma_filter_out_reg) begin
						// $display(" %d good",$time);
					end
					else begin
						$display(" %d intple mismatch",$time);
						$stop;
					end	
					state <= 0;
				end			
			end
			3: begin
				if(luma_filter_ready2 & luma_filter_ready) begin
					if(luma_filter_out2 == luma_filter_out_reg2) begin
						// $display(" %d good",$time);	
					end
					else begin
						$display(" %d intple mismatch",$time);
						$stop;
					end	
					luma_filter_out_reg <= luma_filter_out;
					luma_filter_out_reg2 <= luma_filter_out_reg;					
					luma_filter_out_reg3 <= luma_filter_out_reg2;					
				end
				else if(luma_filter_ready2) begin
					if(luma_filter_out2 == luma_filter_out_reg2) begin
						// $display(" %d good",$time);	
					end
					else begin
						$display(" %d intple mismatch",$time);
						$stop;
					end	
					state <= 1;

				end
				else if(luma_filter_ready) begin
					luma_filter_out_reg <= luma_filter_out;
					luma_filter_out_reg2 <= luma_filter_out_reg;
					luma_filter_out_reg3 <= luma_filter_out_reg2;
					state <= 4;
				end			
			
			end
			4: begin
				if(luma_filter_ready2) begin
					if(luma_filter_out2 == luma_filter_out_reg3) begin
						// $display(" %d good",$time);	
					end
					else begin
						$display(" %d intple mismatch",$time);
						$stop;
					end	
					state <= 3;

				end
				else if(luma_filter_ready) begin
					$display(" %d stack not enough",$time);
						$stop;
				end			
			
			end
		endcase
	end
// Instantiate the module
*/
`endif

endmodule 