`timescale 1ns / 1ps
module fifo_write_driver(
    clk,
    reset,
    out ,
	prog_full,
	wr_en
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
	parameter WIDTH 		= 8;
	parameter FILE_NAME           = "";
	parameter EMPTY_MODEL = 1;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
	input prog_full; 
    output reg wr_en;
    output [WIDTH-1:0] 	out;
	
// synthesis translate_off
	integer file_in;
	integer i;
	reg [7:0] temp;
	
	reg [7:0] out_arry[WIDTH/8-1:0];
	
	integer reset_counter ;
	
	integer write_wait_counter;
	integer write_upper_lim;
	integer write_state;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module

generate
        genvar ii;
        for (ii = 0 ; ii < WIDTH/8 ; ii = ii + 1) begin : row_iteration
				assign  out[(ii+1)*8-1:ii*8] = out_arry[ii];
        end
endgenerate

initial begin
	reset_counter = 0;
	file_in = $fopen(FILE_NAME,"rb");
	if(file_in) begin
		
	end
	else begin
		$display("%m %s file not open!!",FILE_NAME);
		$stop;
	end
end

always@(posedge clk) begin
	if(reset) begin
		reset_counter <= 0; 
	end
	else begin
		if(reset_counter <10) begin
			reset_counter <= reset_counter + 1; 
		end
	end
end

always@(posedge clk) begin    
	if(reset_counter < 7) begin
		wr_en <= 0;
		write_state <= 0;
	end
	else begin
		case(write_state) 
			0: begin
				wr_en <= 0;
				if(!prog_full) begin
					write_state <= 1;
					write_wait_counter <= 0;
					write_upper_lim <= $random;
				end
			end
			1: begin
				if(EMPTY_MODEL == 1) begin
					if(write_wait_counter > write_upper_lim[7:0]) begin
						wr_en <= 1;
						write_state <= 0;
						for(i=0; i<WIDTH/8;i=i+1) begin
							out_arry[i] <= $fgetc(file_in);
						end
					end
					else begin
						write_wait_counter <= write_wait_counter + 1;
					end
				end
				else begin
					wr_en <= 1;
					write_state <= 0;
					for(i=0; i<WIDTH/8;i=i+1) begin
						out_arry[i] <= $fgetc(file_in);
					end
				end
			end
		endcase
	end
end
// synthesis translate_on
endmodule