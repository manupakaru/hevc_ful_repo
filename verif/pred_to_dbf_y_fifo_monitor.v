`timescale 1ns / 1ps
module pred_to_dbf_y_fifo_monitor(
    clk,
    reset,
    out,
	y_common_y_int,
	y_common_x_int,
	empty,
	rd_en
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
	`include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
	parameter WIDTH 		= 8;
	parameter FILE_IN_WIDTH = 8;
	parameter FILE_NAME           = "";
	parameter OUT_VERIFY  = 1;
	parameter DEBUG = 0;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
	input  empty,rd_en; 
    input [WIDTH-1:0] 	out;
	input [PIXEL_ADDR_LENGTH -1:0] y_common_y_int;
	input [PIXEL_ADDR_LENGTH -1:0] y_common_x_int;
	
// synthesis translate_off
	integer file_in;
	integer i;
	reg [7:0] temp;
	
	wire [7:0] out_arry[FILE_IN_WIDTH/8-1:0];
	wire [2:0] x_8x8_in_ctu;
	wire [2:0] y_8x8_in_ctu;
	reg [15:0] dbf_soft_x8, dbf_soft_y8;
    
    
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
	assign {y_8x8_in_ctu,x_8x8_in_ctu} = out[WIDTH-1:FILE_IN_WIDTH];
// Instantiate the module

generate
        genvar ii;
        for (ii = 0 ; ii < FILE_IN_WIDTH/8 ; ii = ii + 1) begin
				assign  out_arry[ii]= out[(ii+1)*8-1:ii*8];
        end
endgenerate

initial begin
	if(OUT_VERIFY) begin
		file_in = $fopen(FILE_NAME,"rb");
		if(file_in) begin
			
		end
		else begin
			$display("file not open!!");
			$stop;
		end
	end
end


always@(posedge clk) begin
   if(reset) begin
	end
	else begin
		if(!empty) begin
			if(rd_en) begin
				if(DEBUG ==1) begin
					$display("%d read from fifo %m %x",$time,out);
				end
				if(OUT_VERIFY) begin
								
					dbf_soft_y8[7:0]   = $fgetc(file_in);
					dbf_soft_y8[15:8]   = $fgetc(file_in);
					dbf_soft_x8[7:0]   = $fgetc(file_in);
					dbf_soft_x8[15:8]   = $fgetc(file_in);
					
					if(dbf_soft_y8[2:0]!= y_8x8_in_ctu || dbf_soft_x8[2:0]!= x_8x8_in_ctu) begin
						$display(" dbf yy wrong x=%d y=%d %m",dbf_soft_x8,dbf_soft_y8);
						$stop;
					end
					else begin
						if(DEBUG) begin
							$display("%d dbf yy right x=%d y=%d in %m",$time,dbf_soft_x8,dbf_soft_y8);
							
						end
					end
					for(i=0; i<FILE_IN_WIDTH/8;i=i+1) begin
						temp = $fgetc(file_in);
						if(temp == out_arry[i]) begin
						  if(DEBUG ==1) begin
								$display("%d compare success in %m",$time);
							end
						end
						else begin
							$display("%d compare fail hard %x soft %x in %m at x=%d y=%d",$time, out_arry[i], temp,y_common_x_int,y_common_y_int);
							$stop;
						end
					end
				end
			end
		end
		else begin
			if(rd_en) begin
				$display("%d trying to read while empty %m!!",$time);
				$stop;
			end
		end
	end
end


// synthesis translate_on
endmodule