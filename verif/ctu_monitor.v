`timescale 1ns / 1ps
module ctu_monitor(
    clk,
    reset,
    valid_in,
    xT_4x4_in,
	ar_valid,
	ar_ready,
	bi_pred_block_cache_in,
    yT_4x4_in
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------


    
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
    input valid_in;
	input ar_valid;
	input ar_ready;
    input bi_pred_block_cache_in;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_4x4_in;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_4x4_in;
    
 // synthesis translate_off   

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

	(* MARK_DEBUG *) wire new_ctu = xT_4x4_in[3:0] == 0 && yT_4x4_in[3:0]==0 && valid_in && bi_pred_block_cache_in==0;
	wire new_poc = xT_4x4_in ==0 && yT_4x4_in ==0 && valid_in;
	
	(* MARK_DEBUG *)  reg [31:0] ctu_time;
	integer poc_counter;
	integer poc_time;
	real clock_period_in_ps;
    

  (* MARK_DEBUG *)  reg [31:0] cline_count;
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

	always@(posedge clk) begin
		if(reset) begin
			clock_period_in_ps <= 0;
			poc_time <= 0;
			ctu_time <= 0;
			poc_counter <= 0;
		end
		else begin
			if(new_ctu) begin
				if(xT_4x4_in ==0) begin
				    $display("@ %d, ctu_time = %d x= %d, y=%d, num_cls %d",$time,ctu_time, xT_4x4_in*4, yT_4x4_in*4,cline_count);
				end
				ctu_time <= 0;
			end
			else begin
				ctu_time <= ctu_time + 1;
			end
		
			if(new_poc) begin
				$display("new poc %d cycles %d usec %f", poc_counter,poc_time,(poc_time/1000000000)*(($time-clock_period_in_ps)));
				poc_time <= 0;
				poc_counter <= poc_counter + 1;
			end
			else begin
				poc_time <= poc_time + 1;
			end
			
			

		end
		clock_period_in_ps = $time;
	end
	
	always@(posedge clk) begin
	   if(reset) begin
	     			cline_count <= 0;  
	   end
	   else begin
	     if(new_ctu) begin
	       cline_count <= 0;  
	     end
	     else if(ar_valid & ar_ready) begin
			   cline_count <= cline_count + 1;
		   end	
	  end	
	end
// synthesis translate_on
endmodule 