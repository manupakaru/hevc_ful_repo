`timescale 1ns / 1ps
module mv_monitor(
    clk,
    current_mv_field_pred_flag_l0   ,
    current_mv_field_pred_flag_l1   ,
    current_mv_field_ref_idx_l0     ,
    current_mv_field_ref_idx_l1     ,
    current_mv_field_mv_x_l0        ,
    current_mv_field_mv_y_l0        ,
    current_mv_field_mv_x_l1        ,
    current_mv_field_mv_y_l1        ,
    current_dpb_idx_l0              ,
    current_dpb_idx_l1              ,
    current_mv_field_valid          ,
    
    xx_pb,
    hh_pb,
    yy_pb,
    ww_pb
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

	// `define COMPARE
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
	input clk;
    input  [X11_ADDR_WDTH - 1:0]       xx_pb;
    input  [CB_SIZE_WIDTH -1:0 ]       hh_pb;
    input  [Y11_ADDR_WDTH - 1:0]       yy_pb;
    input  [CB_SIZE_WIDTH -1:0 ]       ww_pb;  

    input                                  current_mv_field_pred_flag_l0;
    input                                  current_mv_field_pred_flag_l1;
    input [REF_IDX_LX_WIDTH -1:0]          current_mv_field_ref_idx_l0;
    input [REF_IDX_LX_WIDTH -1:0]          current_mv_field_ref_idx_l1;
    input signed [MVD_WIDTH -1:0]          current_mv_field_mv_x_l0;
    input signed [MVD_WIDTH -1:0]          current_mv_field_mv_y_l0;
    input signed [MVD_WIDTH -1:0]          current_mv_field_mv_x_l1;
    input signed [MVD_WIDTH -1:0]          current_mv_field_mv_y_l1;    
    input [DPB_ADDR_WIDTH -1:0]            current_dpb_idx_l0;    
    input [DPB_ADDR_WIDTH -1:0]            current_dpb_idx_l1;    
    input                                  current_mv_field_valid; 
	
	reg                                  test_current_mv_field_pred_flag_l0;
    reg                                  test_current_mv_field_pred_flag_l1;
    reg [REF_IDX_LX_WIDTH -1:0]          test_current_mv_field_ref_idx_l0;
    reg [REF_IDX_LX_WIDTH -1:0]          test_current_mv_field_ref_idx_l1;
    reg signed [MVD_WIDTH -1:0]          test_current_mv_field_mv_x_l0;
    reg signed [MVD_WIDTH -1:0]          test_current_mv_field_mv_y_l0;
    reg signed [MVD_WIDTH -1:0]          test_current_mv_field_mv_x_l1;
    reg signed [MVD_WIDTH -1:0]          test_current_mv_field_mv_y_l1;    
    reg [DPB_ADDR_WIDTH -1:0]            test_current_dpb_idx_l0;    
    reg [DPB_ADDR_WIDTH -1:0]            test_current_dpb_idx_l1;    

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
	integer read_file;
	
	initial begin
		read_file = $fopen("mv_dump_file","r");
	end
	
	always@(posedge clk) begin
		if(current_mv_field_valid && (current_mv_field_pred_flag_l0 || current_mv_field_pred_flag_l1)) begin
			$fscanf(read_file,"%x,%x,%x,%x,%x,%x,%x,%x\n"	,test_current_mv_field_pred_flag_l0
															,test_current_mv_field_pred_flag_l1
															,test_current_mv_field_ref_idx_l0
															,test_current_mv_field_ref_idx_l1
															,test_current_mv_field_mv_x_l0
															,test_current_mv_field_mv_y_l0
															,test_current_mv_field_mv_x_l1
															,test_current_mv_field_mv_y_l1);
			if(current_mv_field_pred_flag_l0) begin
				if(
					(current_mv_field_pred_flag_l0	!= test_current_mv_field_pred_flag_l0	) ||
					(current_mv_field_ref_idx_l0     != test_current_mv_field_ref_idx_l0    ) ||
					(current_mv_field_mv_x_l0        != test_current_mv_field_mv_x_l0       ) ||
					(current_mv_field_mv_y_l0        != test_current_mv_field_mv_y_l0       ) 				
				) begin
				$display("mv wrong at x %d y %d",xx_pb,yy_pb);
				$stop;
				end
			
			end
			if(current_mv_field_pred_flag_l1) begin
				if(
					(current_mv_field_pred_flag_l1   != test_current_mv_field_pred_flag_l1  ) ||
					(current_mv_field_ref_idx_l1     != test_current_mv_field_ref_idx_l1    ) ||
					(current_mv_field_mv_x_l1        != test_current_mv_field_mv_x_l1       ) ||
					(current_mv_field_mv_y_l1        != test_current_mv_field_mv_y_l1       ) 				
				) begin
				$display("mv wrong at x %d y %d",xx_pb,yy_pb);
				$stop;
				end
			end
		end
	end
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

endmodule 