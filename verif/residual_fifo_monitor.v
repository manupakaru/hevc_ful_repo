`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:31:05 07/28/2014 
// Design Name: 
// Module Name:    residual_fifo_monitor 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module residual_fifo_monitor(
    clk,
    reset,
    out,
    empty,
    rd_en
);

 //---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
	parameter STRING = "";
	parameter FILE_NAME           = "";
	parameter OUT_VERIFY  = 1;
	parameter DEBUG = 0;


//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------

	input clk;
	input reset;
	input  empty,rd_en; 
	input [144-1:0] 	out;
	
	// synthesis translate_off
	integer f;
	integer i,j,k;

	reg [8:0] read_in_res [0:3][0:3];
	reg [7:0] null;
	reg [8:0] temp;
	
	initial begin
		if(OUT_VERIFY) begin
			f = $fopen(FILE_NAME,"rb");
			if(f) begin
				
			end
			else begin
				$display("%s file not open!!", STRING);
				$stop;
			end
		end
	end
	
	always@(posedge clk) begin
		if(reset) begin
		end
		else begin
			if(!empty) begin
				if(rd_en) begin
					if(DEBUG ==1) begin
						$display("%s: %d read from fifo %m %x",STRING,$time,out);
					end
					if(OUT_VERIFY) begin
									
						 read_in_res[0][0][7:0] = $fgetc(f);
						{read_in_res[1][0][6:0] , read_in_res[0][0][8]} = $fgetc(f);
						{read_in_res[2][0][5:0] , read_in_res[1][0][8:7]} = $fgetc(f);
						{null[4:0] , read_in_res[2][0][8:6]} = $fgetc(f);
						
						read_in_res[3][0][7:0] = $fgetc(f);
						{read_in_res[0][1][6:0] , read_in_res[3][0][8]} = $fgetc(f);
						{read_in_res[1][1][5:0] , read_in_res[0][1][8:7]} = $fgetc(f);
						{null[4:0] , read_in_res[1][1][8:6]} = $fgetc(f);
						
						read_in_res[2][1][7:0] = $fgetc(f);
						{read_in_res[3][1][6:0] , read_in_res[2][1][8]} = $fgetc(f);
						{read_in_res[0][2][5:0] , read_in_res[3][1][8:7]} = $fgetc(f);
						{null[4:0] , read_in_res[0][2][8:6]} = $fgetc(f);
						
						read_in_res[1][2][7:0] = $fgetc(f);
						{read_in_res[2][2][6:0] , read_in_res[1][2][8]} = $fgetc(f);
						{read_in_res[3][2][5:0] , read_in_res[2][2][8:7]} = $fgetc(f);
						{null[4:0] , read_in_res[3][2][8:6]} = $fgetc(f);
						
						read_in_res[0][3][7:0] = $fgetc(f);
						{read_in_res[1][3][6:0] , read_in_res[0][3][8]} = $fgetc(f);
						{read_in_res[2][3][5:0] , read_in_res[1][3][8:7]} = $fgetc(f);
						{null[4:0] , read_in_res[2][3][8:6]} = $fgetc(f);
						
						read_in_res[3][3][7:0] = $fgetc(f);
						{null[6:0] , read_in_res[3][3][8]} = $fgetc(f);
						null = $fgetc(f);
						null = $fgetc(f);
						
						if ( out != { read_in_res[0][0] ,read_in_res[0][1],read_in_res[0][2],read_in_res[0][3], read_in_res[1][0] ,read_in_res[1][1],read_in_res[1][2],read_in_res[1][3], read_in_res[2][0] ,read_in_res[2][1],read_in_res[2][2],read_in_res[2][3], read_in_res[3][0] ,read_in_res[3][1],read_in_res[3][2],read_in_res[3][3] } ) begin
							$display("%s: wrong ",STRING);
							for (i=0;i<4;i=i+1) begin
								for (j=0;j<4;j=j+1) begin
									for(k=0;k<9;k=k+1) begin
										temp[k] = out[ (4*(3-i)+(3-j))*9 +k];
									end
									$display ( "\t (i,j) = (%d,%d), expected %d actual %d",i,j,temp,read_in_res[i][j]);
								end
							end
							$stop;
						end
						else begin
							if(DEBUG) begin
								$display("%s: correct ",STRING,read_in_res,out);
								
							end
						end
						
					end
				end
			end
			else begin
				if(rd_en) begin
					$display("%s: %d trying to read while empty %m!!",STRING,$time);
					$stop;
				end
			end
		end
end


// synthesis translate_on
endmodule
