`timescale 1ns / 1ps
module cache_print(
    clk,
    reset,
	cache_valid_in,
	bi_pred_block_cache_in,
	rready,
	xT_in_min_tus,
	yT_in_min_tus,
	delta_x,
	delta_y,
    valid_in,
    ready_in,
    iu_idx_val,
    iu_idx_row_val,
    ref_idx_val,
    bu_idx_val
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------

	input [15: 0]  bu_idx_val;
    input [20: 0]  iu_idx_val;
    input [25: 0]  iu_idx_row_val;
    input [31: 0]  ref_idx_val;
	input clk;
	input bi_pred_block_cache_in;
	input reset;
	input valid_in;
	input ready_in;
	input cache_valid_in;
	input rready;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus;
    input [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus;
    input    [1:0]                                   delta_x;    // possible 0,1,2
    input    [1:0]                                   delta_y;       

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------


	integer transaction_gap;
	integer total_misses;
	integer block_serve_time;
	integer blocks_per_ctu;
	integer consec_filt_not_ready;
	
	wire new_ctu = xT_in_min_tus[3:0] == 0 && yT_in_min_tus[3:0]==0 && cache_valid_in;
	
	reg cache_valid_in_d;
	reg bi_pred_block_cache_in_d;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] xT_in_min_tus_d;
    reg [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0] yT_in_min_tus_d;
	
	integer tot_delta_x;
	integer tot_delta_y;
	integer ctu_time;
	

	always@(posedge clk) begin
		cache_valid_in_d <= cache_valid_in;
		bi_pred_block_cache_in_d <= bi_pred_block_cache_in;
	end
    
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

	always@(posedge clk) begin
		if(reset) begin
			block_serve_time <=  0;
			consec_filt_not_ready <= 0;
			tot_delta_x <= 0;
			tot_delta_y <= 0;
			blocks_per_ctu <= 0;
			ctu_time <= 0;
		end
		else begin
			if(cache_valid_in) begin
				block_serve_time <=  0;
				consec_filt_not_ready <= 0;
				if(block_serve_time>8) begin
					// $display("%d, serve_time = %d x=%d, y=%d dx= %d dy=%d",$time,block_serve_time, xT_in_min_tus*4, yT_in_min_tus*4,delta_x,delta_y);
				end
			end
			else begin
				block_serve_time <=  block_serve_time + 1; 
			end
			
			if(new_ctu && bi_pred_block_cache_in == 0) begin
				blocks_per_ctu <= 0;
				tot_delta_x <= 0;
				tot_delta_y <= 0;
				ctu_time <= 0;
				// $display("%d, blocks_per_ctu = %d",$time,blocks_per_ctu);
				//$display("%d, ctu_time %d  x=%d, y=%d dx= %d dy=%d blocks %d missed %d",$time,ctu_time, xT_in_min_tus_d*4, yT_in_min_tus_d*4,tot_delta_x,tot_delta_y,blocks_per_ctu,total_misses);
				yT_in_min_tus_d <= yT_in_min_tus;
				xT_in_min_tus_d <= xT_in_min_tus;
			end
			else begin
				if(cache_valid_in_d & bi_pred_block_cache_in_d == 0) begin
					blocks_per_ctu <= blocks_per_ctu + 1;
					tot_delta_x <= tot_delta_x + delta_x; 
					tot_delta_y <= tot_delta_y + delta_y; 
					
				end
				ctu_time <= ctu_time + 1;
			end
			
			if(!rready) begin
				consec_filt_not_ready <= consec_filt_not_ready+1;
				// $display("%d, filter not ready detected %d ",$time,consec_filt_not_ready);
			end
			
		end
	end

	always@(posedge clk) begin
		if(reset) begin
			transaction_gap <= 0;
			total_misses <= 0;
		end
		else begin
			if(new_ctu && bi_pred_block_cache_in == 0) begin
				total_misses <= 0;
			end
			else if(valid_in & ready_in) begin
				// $display("%d, tr_time = %d bu= %d, iu=%d, iu_row=%d, ref=%d",$time,transaction_gap, bu_idx_val, iu_idx_val,iu_idx_row_val,ref_idx_val);
				transaction_gap <= 0;
				total_misses <= total_misses + 1;
			end
			else begin
				transaction_gap <= transaction_gap + 1;
			end
		end
	end

endmodule 