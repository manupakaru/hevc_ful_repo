`timescale 1ns / 1ps
module mv_buffer(
    clk,
    reset,
    enable,
    current_pic_dpb_idx,
    
    x_ctb_addr,
    y_ctb_addr,

    mv_col_axi_awid,     
    mv_col_axi_awlen,    
    mv_col_axi_awsize,   
    mv_col_axi_awburst,  
    mv_col_axi_awlock,   
    mv_col_axi_awcache,  
    mv_col_axi_awprot,   
    mv_col_axi_awvalid,
    mv_col_axi_awaddr,
    mv_col_axi_awready,
    mv_col_axi_wstrb,
    mv_col_axi_wlast,
    mv_col_axi_wvalid,
    mv_col_axi_wdata,
    mv_col_axi_wready,
    // mv_col_axi_bid,
    mv_col_axi_bresp,
    mv_col_axi_bvalid,
    mv_col_axi_bready,    

    last_pb_of_ctu_in,              // send this when all the motion vectors are coded
    ctu_write_done_out
    
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
    `include "../pred_def.v"
    `include "../inter_axi_def.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------

    parameter                   MV_COL_AXI_AX_SIZE  = `AX_SIZE_64;
    parameter                   MV_COL_AXI_AX_LEN   = `AX_LEN_4;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam  STATE_LINE_WRITE_IDLE = 0;
    localparam  STATE_LINE_WRITE_BS_MV_MOVE = 1;
    localparam  STATE_LINE_WRITE_CONRNER_MV_MOVE = 3;
    localparam  STATE_LINE_WRITE_BS_MV_MOVE_WAIT = 2;
    localparam  STATE_LINE_WRITE_REST_MV_MOVE = 4;
    
    localparam  STATE_COL_WRITE_IDLE = 0;
    localparam  STATE_COL_WRITE_START = 1;
    
    localparam  STATE_AXI_WRITE_WAIT = 0;
    localparam  STATE_AXI_WRITE_ADDRESS_SEND = 1;
    localparam  STATE_AXI_WRITE_DATA_SEND = 2;
    localparam  STATE_AXI_WRITE_DATA_ACK = 3;
    
    
    
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
    input enable;
    input   [DPB_ADDR_WIDTH -1: 0]                  current_pic_dpb_idx;
    
    
    
    
   
    input    [X11_ADDR_WDTH - CTB_SIZE_WIDTH -1:0] x_ctb_addr;// = x_P_pu_start_in[X11_ADDR_WDTH - LOG2_MIN_PU_SIZE -1: CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE];
    input    [X11_ADDR_WDTH - CTB_SIZE_WIDTH -1:0] y_ctb_addr;//= y_P_pu_start_in[X11_ADDR_WDTH - LOG2_MIN_PU_SIZE -1: CTB_SIZE_WIDTH - LOG2_MIN_PU_SIZE];

    
	 // write address channel
     input                                      last_pb_of_ctu_in;
     output  reg                                ctu_write_done_out;
	 output                                     mv_col_axi_awid    ;// = 0;
	 output      [7:0]                          mv_col_axi_awlen   ;// = MV_COL_AXI_AX_LEN;
	 output      [2:0]                          mv_col_axi_awsize  ;// = MV_COL_AXI_AX_SIZE;
	 output      [1:0]                          mv_col_axi_awburst ;// = `AX_BURST_INC;
	 output                        	            mv_col_axi_awlock  ;// = `AX_LOCK_DEFAULT;
	 output      [3:0]                          mv_col_axi_awcache ;// = `AX_CACHE_DEFAULT;
	 output      [2:0]                          mv_col_axi_awprot  ;// = `AX_PROT_DATA;
	 output reg                                 mv_col_axi_awvalid;
     output reg  [AXI_ADDR_WDTH-1:0]            mv_col_axi_awaddr;

	 input                       	            mv_col_axi_awready;
	 
	 // write data channel
	 output      [MV_COL_AXI_DATA_WIDTH/8-1:0]                       mv_col_axi_wstrb;
	 output reg                                 mv_col_axi_wlast;
	 output reg                                 mv_col_axi_wvalid;
	 output      [MV_COL_AXI_DATA_WIDTH -1:0]   mv_col_axi_wdata;
     
	 input	                                    mv_col_axi_wready;
	 
	 //write response channel
	 // input                       	            mv_col_axi_bid;
	 input       [1:0]                          mv_col_axi_bresp;
	 input                       	            mv_col_axi_bvalid;
	 output  reg                                mv_col_axi_bready;    
    
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    integer                         state_axi_write;
    


    
    
    
    reg [31:0] axi_frame_offet;
    reg [31:0] axi_ctu_row_offset;
    reg [31:0] axi_ctu_offset;    
    reg [2-1:0] mv_row_offset;
    
    reg [MV_FIELD_AXI_DATA_WIDTH * 4 - 1:0]   mv_col_axi_wdata_d;
    
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

    //todo: form axi write interface for collocated motion vectors. data width 512 to burst fist 4 MVs of CTU need 4 of such bursts - Col mv offset = 2^25, col_mv_frame_offset = 2^17 get ctu_x_offset and ctu_y_offset from prev_mv
    //-------done
    //Collocated MVs expect to write motion vector related to CUs that are coded INTRA, to achieve this on every the two pred_flag for collocated MV buffer is kept as registers and cleared at every ctu cycle
    
    
	 assign                 mv_col_axi_awid     = 0;
	 assign                 mv_col_axi_awlen    = MV_COL_AXI_AX_LEN;
	 assign                 mv_col_axi_awsize   = MV_COL_AXI_AX_SIZE;
	 assign                 mv_col_axi_awburst  = `AX_BURST_INC;
	 assign    	            mv_col_axi_awlock   = `AX_LOCK_DEFAULT;
	 assign                 mv_col_axi_awcache  = `AX_CACHE_DEFAULT;
	 assign                 mv_col_axi_awprot   = `AX_PROT_DATA;    
     assign                 mv_col_axi_wstrb = 64'h00000_FFFFF_FFFFF;       // 40 bytes
    
    assign mv_col_axi_wdata = {128'd0,mv_col_axi_wdata_d};
    
    
    
    
    


    always@(posedge clk) begin
        axi_frame_offet <= `COL_MV_ADDR_OFFSET + ((`COL_MV_FRAME_OFFSET*current_pic_dpb_idx));//%(1<<31));
    end
    always@(posedge clk) begin
        axi_ctu_row_offset <= (axi_frame_offet + `COL_MV_CTU_ROW_OFFSET* y_ctb_addr);//%(1<<(31));;
    end
    always@(posedge clk) begin
        axi_ctu_offset <= (axi_ctu_row_offset + `COL_MV_CTU_ROW_OFFSET * x_ctb_addr);//%(1<<(31));
    end
    
    
    
    always@(posedge clk or posedge reset) begin
        if (reset) begin
            state_axi_write <= STATE_AXI_WRITE_WAIT;
            ctu_write_done_out <= 1;
            mv_col_axi_awvalid <= 0;
            mv_col_axi_wvalid <= 0;
            mv_col_axi_bready <= 0;
        end
        else if(enable) begin
            case(state_axi_write)
                STATE_AXI_WRITE_WAIT: begin
                    if (last_pb_of_ctu_in) begin
                        state_axi_write <= STATE_AXI_WRITE_ADDRESS_SEND;
                        ctu_write_done_out <= 0;
                        mv_col_axi_awaddr <= axi_ctu_offset;
                        mv_col_axi_awvalid <= 1;  
                        mv_row_offset <= 0;
                    end
                    else begin
                        ctu_write_done_out <= 1;
                        mv_col_axi_awvalid <= 0;
                        mv_col_axi_bready <= 0;
                    end
                end
                STATE_AXI_WRITE_ADDRESS_SEND: begin
                    if(mv_col_axi_awready) begin
                        mv_col_axi_awvalid <= 0;
                        mv_row_offset <= (mv_row_offset + 1)%4;
//                        mv_col_axi_wvalid <= 1;
                        mv_col_axi_wlast <= 0;
                        mv_col_axi_wdata_d <= {350'd0, mv_col_axi_awaddr,mv_row_offset};
                        state_axi_write <= STATE_AXI_WRITE_DATA_SEND;
                    end
                end
                STATE_AXI_WRITE_DATA_SEND: begin
                    if(mv_col_axi_wready) begin
                        mv_row_offset <= (mv_row_offset + 1)%4;
                        mv_col_axi_wvalid <= 1;
                        mv_col_axi_wdata_d <= {350'd0, mv_col_axi_awaddr,mv_row_offset};                    
                        if(mv_row_offset == 0) begin
                            mv_col_axi_wlast <= 1;
                            state_axi_write <= STATE_AXI_WRITE_DATA_ACK;
									 mv_col_axi_bready <= 1;
                        end
                        else begin
                            mv_col_axi_wlast <= 0;
                        end
                    end
                end
                STATE_AXI_WRITE_DATA_ACK: begin
                    mv_col_axi_wvalid <= 0;
                    if(mv_col_axi_bvalid) begin
                        if(mv_col_axi_bresp == `XRESP_SLAV_ERROR) begin
                            state_axi_write <= STATE_AXI_WRITE_ADDRESS_SEND;
                            ctu_write_done_out <= 0;
                            mv_col_axi_awvalid <= 1;  
                            mv_row_offset <= 0;                            
                        end
                        else begin
                            state_axi_write <= STATE_AXI_WRITE_WAIT;
                            ctu_write_done_out <= 1;                        
                        end
                    end
                end
            endcase
        end
    end
    
    
    
endmodule