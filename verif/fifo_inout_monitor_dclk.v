`timescale 1ns / 1ps
module fifo_inout_monitor_dclk(
    rclk,
    wclk,
    reset,
    in ,
    out,
	full,
	empty,
	rd_en,
	wr_en
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
	parameter WIDTH 		= 8;
	parameter FILE_IN_WIDTH = WIDTH;
	parameter FILE_NAME           = "";
	parameter OUT_VERIFY  = 1;
	parameter DEBUG = 0;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input rclk;
    input wclk;
    input reset;
	input full, empty,wr_en,rd_en; 
    input [WIDTH-1:0] 	in;
    input [WIDTH-1:0] 	out;
	
// synthesis translate_off
	integer file_in;
	integer i;
	reg [7:0] temp;
	
	wire [7:0] out_arry[FILE_IN_WIDTH/8-1:0];
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module

generate
        genvar ii;
        for (ii = 0 ; ii < FILE_IN_WIDTH/8 ; ii = ii + 1) begin
				assign  out_arry[ii]= out[(ii+1)*8-1:ii*8];
        end
endgenerate

initial begin
	if(OUT_VERIFY) begin
		file_in = $fopen(FILE_NAME,"rb");
		if(file_in) begin
			
		end
		else begin
			$display("file not open!!");
			$stop;
		end
	end
end


always@(posedge rclk) begin
   if(reset) begin
	end
	else begin
		if(!empty) begin
			if(rd_en) begin
				if(DEBUG ==1) begin
					$display("%d read from fifo %m %x",$time,out);
				end
				if(OUT_VERIFY) begin
					for(i=0; i<FILE_IN_WIDTH/8;i=i+1) begin
						temp = $fgetc(file_in);
						if(temp == out_arry[i]) begin
							if(DEBUG ==1) begin
								$display("%d compare success in %m",$time);
							end
						end						
						else begin
							$display("%d compare fail hard %x soft %x in %m",$time, out_arry[i], temp);
							$stop;
						end
					end
				end
			end
		end
		else begin
			if(rd_en) begin
				$display("%d trying to read while empty %m!!",$time);
				$stop;
			end
		end
	end
end



always@(posedge wclk) begin    
	if(reset) begin
	end
	else begin
		if(!full) begin
			if(wr_en) begin
				if(DEBUG ==1) begin
					$display("%d write to fifo %m %x",$time,in);
				end
			end
		end
		else begin
			if(wr_en) begin
				$display("%d trying to write while full in %m!!",$time);
				$stop;
			end
		end
	end
end
// synthesis translate_on
endmodule