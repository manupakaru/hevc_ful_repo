/*
 ============================================================================
 Name        : hevcDecoder.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>
#include "defines.h"
#include "time.h"
clock_t startm, stopm;
clock_t quadtree_start, quadtree_end;
double quad_tree_time = 0.0;
#define START if ( (startm = clock()) == -1) {printf("Error calling clock");exit(1);}
#define STOP if ( (stopm = clock()) == -1) {printf("Error calling clock");exit(1);}
#define PRINTTIME printf( "%6.3f seconds used by the processor.\n", ((double)stopm-startm)/CLOCKS_PER_SEC);
//#define printf(a) printf("[CABAC] a")
int cu[7];

video_param_struct video_param;
sequence_param_struct* sequence_param;
picture_param_struct* picture_param;
slice_segment_struct slice_segment_header;

sequence_param_struct sequence_parameter_sets[16];
picture_param_struct picture_parameter_sets[64];

int current_pic_parameter_id = -1;

short curr_slice;
short curr_tile;
short tile_i, tile_j;

int NAL_num = -1;
int bin_count[30] = {0};
int bypass_count[2]  = {0};
int CTU_bins = 0;
int CTU_non_res = 0;
int CTU_bypass = 0;
int MPSCount=0;
int LPSCount=0;
int ctsBypass =0;
int ctsBypassCnt[30];
long tot_res = 0;
long cycles1 = 0l; // MIT
long cycles2 = 0l;
long cycles3 = 0l; // OUrs
long cycles4 = 0l;
long tot_sub_blocks = 0;
int renorm[9] = {0};
int ctsMPSCount[20] = {0};

int tot_frames = 0;
int tot_non_zero_res = 0;

int global_ctxInc =0 ; // used for bookkeeping

int cabac_print_enable = 1;
int in_header = 0;

FILE* axi_file;

#define FILE_PATH "G:\\HEVC\\4KSoft\\"
#define PAPER_PATH "G:\\inv transform paper\\streams\\4K\\"
#define BITSTREAM_PATH "G:\\HEVC\\bitstreams\\"

//#define CONFORMANCE_PATH "G:\\HEVC\\Conformance\\"
//#define CONFORMANCE_STREAM "AMP_A_Samsung_4"

const int START_POC =0;
const int END_POC = 1000;

int prev_NAL_start = 0;
int main(int argc, char *argv[]) {
	printf("HEVC Decoder\n");
	START
	
	char* in_file = FILE_PATH "EntropyC\\hobbit_nab_32.hevc";
	//char* in_file = FILE_PATH "EntropyC\\benchmark\\RA\\BasketballDrive_1920x1080_50_qp37.bin";
	if (argc>=2) in_file = argv[1];
	char* out_file = FILE_PATH "output\\cabac_to_residual_soft";
	char* axi_file_name = FILE_PATH "output\\axi_trans";
	//char* in_file = CONFORMANCE_PATH CONFORMANCE_STREAM BIT_FILE;
	//char* out_file = CONFORMANCE_PATH CONFORMANCE_STREAM "\\cabac_to_residual_soft";
	//char* axi_file_name = CONFORMANCE_PATH CONFORMANCE_STREAM "\\axi_trans";
	
	cabac_to_residual = fopen(out_file,"wb");
	printf("Reading %s\n",in_file);
	FILE* in = fopen(in_file,"rb");
	axi_file = fopen(axi_file_name,"wb");

	initialize_mmaps();
	int total_NAL_count=0;
	axi_write(0x0a00,100);
	while( decode_next_NAL_with_emulation(in)){
		total_NAL_count++;
		NAL_num = total_NAL_count;
		//if (total_NAL_count ==4) cabac_print_enable = 1;
		//printf("Total NAL Units %d Bin %d\n",total_NAL_count,bin_count);
		if (total_NAL_count==50) break;
		//STOP
		//PRINTTIME
	}

	printf("Total NAL Units %d\n",total_NAL_count); // last NAL not counted
	printf("quadtree time %f\n",quad_tree_time);
	int i=0;
	//for (i=0;i<30;i++)
	//	printf("Bin count i %d %d \n",i,bin_count[i]);
	printf("Bypass Count count %d %d  \n",bypass_count[0], bypass_count[1]);
	printf("CUs 8-%d 16-%d 32-%d 64-%d\n",cu[3], cu[4], cu[5], cu[6]);
	printf("Non Res %d\n",CTU_non_res);
	printf("MPS %d LPS %d\n",MPSCount, LPSCount);
	printf("Residual pixels %ld sub_blocks %ld\n",tot_res,tot_sub_blocks);

	printf("MPS Count ");
	for (i=0;i<20;i++){
		printf("%d ",ctsMPSCount[i]);
	}
	printf("\n");

	fclose(axi_file);
//	fclose(in);
	STOP
	PRINTTIME
	return EXIT_SUCCESS;
}

int decode_next_NAL_with_emulation(FILE* fp){
	BYTE delimeter[4];
	fread(delimeter,1,4,fp);
	if ( !(delimeter[0]==0 && delimeter[1]==0 && delimeter[2]==0 && delimeter[3]==1) && !(delimeter[0]==0 && delimeter[1]==0 && delimeter[2]==1 ) ){
		printf("Invalid NAL Boundary at %04x\n",(unsigned int) ftell(fp)-4);
		while(1);
		return 0;
	}
	// only 3 byte header found
	if (delimeter[2]==1){
		fseek(fp,-1,SEEK_CUR);
	}
	int NAL_start = ftell(fp);
	int rbsp_size = NAL_start-prev_NAL_start;
	prev_NAL_start = NAL_start;
	//printf("NAL begins at %d\n",NAL_start);
	int c;
	int ctsZeros = 0;
	int nal_unit_type = ( fgetc(fp) >> 1 ) & 63;
	int nal_temporalId = (fgetc(fp)&7 )-1; // unused byte from header
	int NAL_body_size = 0;
	
	rbsp_struct rbsp;
	rbsp.byte = (BYTE*) malloc(100000000);
	rbsp.pos = 0;
	rbsp.bit = 0;
	rbsp.len=0;
	int i=0;

	while( (c=fgetc(fp))!=-1){

		if (ctsZeros==2 && c==3){
			// emulation prevention
			//printf("Emulation Prevention at %x\n",(unsigned int)ftell(fp));
			ctsZeros = 0;
			rbsp.byte[i++] = c;
			rbsp.len++;
			//fgetc(fp);
			continue;
		}
		if (ctsZeros==2 && c==1){
			fseek(fp,-3,SEEK_CUR);
			break;
		}
		if (ctsZeros==3){
			// reached end of NAL
			fseek(fp,-4,SEEK_CUR);
			break;
		}
		if (c==0){
			ctsZeros++;
		} else {
			ctsZeros=0;
		}
		
		rbsp.byte[i++] = c;
		rbsp.len++;
	}
	int no_more_NALs = (c==-1);

	printf("\nNAL Type %d starting at %d size %d\n",nal_unit_type,NAL_start, rbsp.len);

	rbsp.ctsZeros=0;
	if (rbsp.byte[0] ==0) rbsp.ctsZeros=1; // special case. very impossible. but just in case
	axi_write(0x0a00,(rbsp.len+6)/4);
	axi_write(0x0830,1);
	axi_write(0x0840,1);

	// nal header
	int zero = 0;
	axi_write(0x0910,zero);
	axi_write(0x0960,nal_unit_type);
	axi_write(0x0960,zero);
	axi_write(0x0930,nal_temporalId+1);
	decode_rbsp(nal_unit_type,&rbsp);
	free(rbsp.byte);
	return !no_more_NALs;
}

int decode_next_NAL(FILE* fp) {
	BYTE delimeter[4];
	fread(delimeter,1,4,fp);
	if ( !(delimeter[0]==0 && delimeter[1]==0 && delimeter[2]==0 && delimeter[3]==1) && !(delimeter[0]==0 && delimeter[1]==0 && delimeter[2]==1 ) ){
		printf("Invalid NAL Boundary at %04x\n",(unsigned int) ftell(fp)-4);
		return 0;
	}
	// only 3 byte header found
	if (delimeter[2]==1){
		fseek(fp,-1,SEEK_CUR);
	}
	int NAL_start = ftell(fp);
	//printf("RBSP size %d\n",NAL_start-prev_NAL_start);
	prev_NAL_start = NAL_start;
	//printf("NAL begins at %d\n",NAL_start);
	int c;
	int ctsZeros = 0;
	int nal_unit_type = ( fgetc(fp) >> 1 ) & 63;
	int nal_temporalId = (fgetc(fp)&7 )-1; // unused byte from header
	//printf("NAL Unit start = %07d \ttype = %2d temporalId=%d\t",NAL_start-3,nal_unit_type,nal_temporalId);
	// Find out NAL Unit size (Here NAL is after correcting emulation prevention)

	int rbsp_size = 0;
	int NAL_body_size = 0;
	printf("\nNAL Type %d starting at %d temporalId %d\n",nal_unit_type,NAL_start, nal_temporalId);
	
	while( (c=fgetc(fp))!=-1){
		//printf ("c = %d size = %d\n",c,rbsp_size);
		//rbsp_size=ftell(fp_
		if (ctsZeros==2 && c==3){
			// emulation prevention
			//printf("Emulation Prevention at %x\n",(unsigned int)ftell(fp));
			ctsZeros = 0;
			//fgetc(fp);
			continue;
		}
		if (ctsZeros==2 && c==1){
			
			rbsp_size-=2;
			//fseek(fp,-3,SEEK_CUR);
			break;
		}
		if (ctsZeros==3){
			// reached end of NAL
			rbsp_size-=3;
			//printf("RBSP size %d\n",ftell(fp)-prev_NAL_start);
			//fseek(fp,-4,SEEK_CUR);

			break;
		}
		if (c==0){
			ctsZeros++;
			rbsp_size++;
		} else {
			ctsZeros=0;
			rbsp_size++;
		}

	}
	

	fseek(fp,NAL_start+2,SEEK_SET);
	//printf("End %d",ftell(fp));
	int no_more_NALs = (c==-1);
	if (no_more_NALs){
		printf("RBSP size %d\n",ftell(fp)-prev_NAL_start);
	}
	rbsp_struct rbsp;
	rbsp.byte = (BYTE*) malloc(rbsp_size);
	rbsp.len = rbsp_size;
	rbsp.pos = 0;
	rbsp.bit = 0;
	int i;
	//printf("RBSP Raw ");
	for (i=0;i<rbsp_size;i++){
		c = fgetc(fp);
		if (c==0) {
			ctsZeros++;
		}else {
			ctsZeros=0;
		}
		rbsp.byte[i] = c;
		if (ctsZeros==2){
			c=fgetc(fp);
			if (c!=3) rbsp.byte[++i]=c;
			ctsZeros=0;
		}
	}
//	for (i=0;i<rbsp_size;i++){
//		printf("%02x ",rbsp.byte[i]);
//		if (i%8==7) printf(" | ");
//	}
//	printf("\n");
	if (NAL_start!=0x2db444){
		//free(rbsp.byte);
		//return 1;
	}

	

	decode_rbsp(nal_unit_type,&rbsp);
	free(rbsp.byte);
	return !no_more_NALs;
}

rbsp_struct* current;
cabac_state_struct cabac_current;
cabac_context_struct* saved_context[27];

void decode_rbsp(int type,rbsp_struct* rbsp){
	current = rbsp;
	
	if (type==NAL_VPS_NUT){
		video_parameter_set_rbsp(&video_param);
//		print_video_parameter_set_rbsp(&video_param);
		//printf("Type %d byte %x\n",type,rbsp_byte[0]>128);
	}
	if (type==NAL_SPS_NUT){
		sequence_parameter_set_rbsp();
//		print_sequence_parameter_set_rbsp(&sequence_param);
	}
	if (type==NAL_PPS_NUT){
		picture_parameter_set_rbsp();
//		print_picture_parameter_set_rbsp(&picture_param);
	}
	if (type==IDR_W_DLP){
		RapPicFlag = 1;
		IdrPicFlag = 1;
		RefPicture = 1;
		slice_segment_layer_rbsp(&slice_segment_header);
//		print_slice_segment_layer_rbsp(&slice_segment_header);
	} else if ( type==TRAIL_R || type == TRAIL_N || type==RASL_R | type == RASL_N){
		RapPicFlag = 0;
		IdrPicFlag = 0;
		RefPicture = ((type==TRAIL_R || type==RASL_R))? 1:0;
		RefPicture = 1; // For ALL INTRA (TODO: remove for non-all intra)
		//if (NAL_num!=5) return;
		slice_segment_layer_rbsp(&slice_segment_header);
//		print_slice_segment_layer_rbsp(&slice_segment_header);
	} else if (type ==CRA_NUT){
		RapPicFlag = 1;
		IdrPicFlag = 0;
		RefPicture = 1;
		RefPicture = 1;
		slice_segment_layer_rbsp(&slice_segment_header);

	}

}

void video_parameter_set_rbsp(video_param_struct* vps){
	in_header = 1;
	vps->vps_video_parameter_set_id = read_u(4);
	//if (read_u(2)!=3) printf("Reserved error");
	read_u(8);
	vps->vps_max_sub_layers_minus1 = read_u(3);
	//read_u(1);
	vps->vps_temporal_id_nesting_flag = read_u(1);
	if (read_u(8)!=0xff) fprintf(stderr,"Reserved 0xffff Error\n");
	if (read_u(8)!=0xff) fprintf(stderr,"Reserved 0xffff Error\n");
	//printf("Res %x\n",read_u(16));
	profile_tier_level(&(vps->vps_profile_tier_level),1,video_param.vps_max_sub_layers_minus1);
	//bit_rate_pic_rate_info(NULL,0,vps->vps_max_sub_layers_minus1);
	vps->vps_sub_layer_ordering_info_present_flag = read_u(1);
	int i,j,t=0;
	for (i= (vps->vps_sub_layer_ordering_info_present_flag ? 0 : vps->vps_max_sub_layers_minus1);i<=vps->vps_max_sub_layers_minus1;i++){
		// should only be called once;
		if (t==1) fprintf(stderr,"Parameter Set Loop Multiple times\n");
		t=1;
		vps->vps_max_dec_pic_buffering = read_ue();
		vps->vps_max_num_reorder_pics = read_ue();
		vps->vps_max_latency_increase = read_ue();
	}
	//print_loc();
	vps->vps_max_layer_id = read_u(6);
	vps->vps_num_layer_sets_minus1 = read_ue();
	for (i=1;i<=vps->vps_num_layer_sets_minus1;i++){
		for (j=0;j<=vps->vps_max_layer_id;j++){
			read_u(1);
		}
	}
	vps->vps_timing_info_present_flag = read_u(1);
	if (vps->vps_timing_info_present_flag){
		fprintf(stderr,"timing info present\n"); 
		while(1);
	}
	//vps->vps_num_hrd_parameters = read_ue();
	//vps->vps_max_nuh_reserved_zero_layer_id = read_u(6);
	//vps->vps_num_op_sets_minus1 = read_ue();

}
void print_video_parameter_set_rbsp(video_param_struct* vps){
	printf("Reading video_parameter_set_rbsp\n");
	printf("\tvps_video_parameter_set_id=%d\n",vps->vps_video_parameter_set_id);
	printf("\tvps_max_sub_layers_minus1=%d\n",vps->vps_max_sub_layers_minus1);
	printf("\tvps_temporal_id_nesting_flag=%d\n",vps->vps_temporal_id_nesting_flag);

	printf("\tgeneral_profile_space=%d\n",vps->vps_profile_tier_level.general_profile_space);
	printf("\tgeneral_tier_flag=%d\n",vps->vps_profile_tier_level.general_tier_flag);
	printf("\tgeneral_profile_idc=%d\n",vps->vps_profile_tier_level.general_profile_idc);
	printf("\tgeneral_profile_compatibility=%08x\n",(unsigned int) vps->vps_profile_tier_level.general_profile_compatibility);
	printf("\tgeneral_level_idc=%d\n",vps->vps_profile_tier_level.general_level_idc);
	printf("\tvps_sub_layer_ordering_info_present_flag=%d\n",vps->vps_sub_layer_ordering_info_present_flag);
	printf("\tvps_max_dec_pic_buffering=%d\n",vps->vps_max_dec_pic_buffering);
	printf("\tvps_max_num_reorder_pics=%d\n",vps->vps_max_num_reorder_pics);
	printf("\tvps_max_latency_increase=%d\n",vps->vps_max_latency_increase);
	printf("\tvps_max_nuh_reserved_zero_layer_id=%d\n",vps->vps_max_nuh_reserved_zero_layer_id);
	//printf("\tvps_num_op_sets_minus1=%d\n",vps->vps_num_op_sets_minus1);

}

void sequence_parameter_set_rbsp(){
	in_header = 1;
	sequence_param_struct sps;
	current_pic_parameter_id = -1;

	sps.sps_video_parameter_set_id = read_u(4);
	sps.sps_max_sub_layers_minus1 = read_u(3);
	sps.sps_temporal_sub_layer_nesting_flag = read_u(1);
	profile_tier_level(&(sps.sps_profile_tier_level), 1,sps.sps_max_sub_layers_minus1);
	sps.sps_seq_parameter_set_id = read_ue();
	
	sps.chroma_format_idc = read_ue();
	if (sps.chroma_format_idc==3){
		fprintf(stderr,"Chroma Format cannot be 3\n");
	}
	sps.pic_width_in_luma_samples = read_ue();
	sps.pic_height_in_luma_samples = read_ue();
	sps.conformance_window_flag = read_u(1);
	if (sps.conformance_window_flag){
		sps.conf_win_left_offset = read_ue();
		sps.conf_win_right_offset = read_ue();
		sps.conf_win_top_offset = read_ue();
		sps.conf_win_bottom_offset = read_ue();
	}
	sps.bit_depth_luma_minus8 = read_ue();
	sps.bit_depth_chroma_minus8 = read_ue();
	sps.log2_max_pic_order_cnt_lsb_minus4 = read_ue();
	sps.sps_sub_layer_ordering_info_present_flag = read_u(1);
	int i,t=0;
	for (i= (sps.sps_sub_layer_ordering_info_present_flag ? 0 : sps.sps_max_sub_layers_minus1);i<=sps.sps_max_sub_layers_minus1;i++){
			// should only be called once;
			if (t==1) fprintf(stderr,"Parameter Set Loop Multiple times\n");
			t=1;
			sps.sps_max_dec_pic_buffering = read_ue();
			sps.sps_max_num_reorder_pics = read_ue();
			sps.sps_max_latency_increase = read_ue();
	}
	sps.log2_min_luma_coding_block_size_minus3 = read_ue();
	sps.log2_diff_max_min_luma_coding_block_size = read_ue();
	
	sps.Log2MinCbSizeY = sps.log2_min_luma_coding_block_size_minus3 +3;
	sps.Log2CtbSizeY = sps.Log2MinCbSizeY + sps.log2_diff_max_min_luma_coding_block_size;

	//PicHeightInCtbsY = ceil( (double)sps.pic_height_in_luma_samples/(1<<Log2CtbSizeY));
	sps.PicHeightInCtbsY = sps.pic_height_in_luma_samples >> sps.Log2CtbSizeY;
	if ( (sps.pic_height_in_luma_samples % (1<<sps.Log2CtbSizeY)) >0) sps.PicHeightInCtbsY++;
	//PicWidthInCtbsY = ceil( (double) sps.pic_width_in_luma_samples/(1<<Log2CtbSizeY));
	sps.PicWidthInCtbsY = sps.pic_width_in_luma_samples >> sps.Log2CtbSizeY;
	if ( (sps.pic_width_in_luma_samples % (1<<sps.Log2CtbSizeY)) >0) sps.PicWidthInCtbsY++;
	sps.PicSizeInCtbsY = sps.PicHeightInCtbsY  * sps.PicWidthInCtbsY ;
	//sao_list =(sao_struct*) malloc(sizeof(sao_struct)*PicSizeInCtbsY); // index according to ctbAddrRs
	sps.log2_min_transform_block_size_minus2 = read_ue();
	sps.log2_diff_max_min_transform_block_size = read_ue();
	sps.Log2MinTrafoSize = sps.log2_min_transform_block_size_minus2 +2;
	sps.Log2MaxTrafoSize = sps.Log2MinTrafoSize + sps.log2_diff_max_min_transform_block_size;

	initialize_scanOrder();


	sps.max_transform_hierarchy_depth_inter = read_ue();
	sps.max_transform_hierarchy_depth_intra = read_ue();
	sps.scaling_list_enable_flag = read_u(1);
	if (sps.scaling_list_enable_flag){
		int sps_scaling_list_data_present_flag = read_u(1);
		if (sps_scaling_list_data_present_flag) {
			scaling_list_data();
		} else {
			// send_default_scaling_list;
		}
		//fprintf(stderr,"scaling list enable not implemented\n");
	}
	sps.amp_enabled_flag = read_u(1);
	sps.sample_adaptive_offset_enabled_flag = read_u(1);
	sps.pcm_enabled_flag = read_u(1);
		sps.pcm_loop_filter_disable_flag = 0;
	if (sps.pcm_enabled_flag) {
		sps.pcm_sample_bit_depth_luma_minus1 = read_u(4);
		sps.pcm_sample_bit_depth_chroma_minus1 = read_u(4);
		sps.log2_min_pcm_luma_coding_block_size_minus3 = read_ue();
		sps.log2_diff_max_min_pcm_luma_coding_block_size = read_ue();
		sps.pcm_loop_filter_disable_flag = read_u(1);

		sps.PCMBitDepthY = sps.pcm_sample_bit_depth_luma_minus1 + 1;
		sps.PCMBitDepthC = sps.pcm_sample_bit_depth_chroma_minus1 + 1;
		sps.Log2MinIpcmCbSizeY = sps.log2_min_pcm_luma_coding_block_size_minus3 + 3;
		sps.Log2MaxIpcmCbSizeY = sps.Log2MinIpcmCbSizeY + sps.log2_diff_max_min_pcm_luma_coding_block_size;
	}
	sps.num_short_term_ref_pic_sets = read_ue();
	for (i=0;i<sps.num_short_term_ref_pic_sets;i++){
		//sps.short_term_ref_pic_set[i] = (short_term_ref_pic_set_struct*) malloc(sizeof(short_term_ref_pic_set_struct));
		short_term_ref_pic_set(&sps,i);
	}
	//print_loc();
	printf("ref_pic lists done\n");

	sps.long_term_ref_pics_present_flag = read_u(1);
	if (sps.long_term_ref_pics_present_flag){
		//TODO
	}
	sps.sps_temporal_mvp_enable_flag = read_u(1);
	sps.strong_intra_smoothing_enable_flag = read_u(1);
	sps.vui_parameters_present_flag = read_u(1);
	/*if (sps.vui_parameters_present_flag){
		fprintf(stderr,"VUI Parameters Not Implemented\n");
	}
	sps.sps_extension_flag = read_u(1);
	if (sps.sps_extension_flag){
		fprintf(stderr,"SPS Extension Not Implemented\n");
	}*/

	sequence_parameter_sets[sps.sps_seq_parameter_set_id] = sps;

}


void picture_parameter_set_rbsp(){
	in_header = 1;
	picture_param_struct pps;
	current_pic_parameter_id = -1;

	pps.pps_pic_parameter_set_id = read_ue();
	pps.pps_seq_parameter_set_id = read_ue();

	sequence_param = &(sequence_parameter_sets[pps.pps_seq_parameter_set_id]);

	pps.dependent_slice_segments_enabled_flag = read_u(1);
	pps.output_flag_present_flag = read_u(1);
	pps.num_extra_slice_header_bits = read_u(3);
	pps.sign_data_hiding_flag = read_u(1);
	pps.cabac_init_present_flag = read_u(1);
	pps.num_ref_idx_l0_default_active_minus1=read_ue();
	pps.num_ref_idx_l1_default_active_minus1=read_ue();
	pps.init_qp_minus26 = read_se();
	pps.constrained_intra_pred_flag = read_u(1);
	pps.transform_skip_enabled_flag = read_u(1);
	pps.cu_qp_delta_enabled_flag = read_u(1);
	pps.diff_cu_qp_delta_depth = 0;
	if (pps.cu_qp_delta_enabled_flag){
		pps.diff_cu_qp_delta_depth = read_ue();
	}
	sequence_param->Log2MinCuQpDeltaSize = sequence_param->Log2CtbSizeY - pps.diff_cu_qp_delta_depth;
	pps.pps_cb_qp_offset = read_se();
	pps.pps_cr_qp_offset = read_se();
	if (pps.pps_cb_qp_offset>0 || pps.pps_cr_qp_offset>0){
		//printf("pps_chroma_qp_offsets present\n"); exit(0);
	}
	pps.pps_slice_chroma_qp_offsets_present_flag = read_u(1);
	pps.weighted_pred_flag = read_u(1);
	pps.weighted_bipred_flag = read_u(1);
//	pps.output_flag_present_flag = read_u(1);
	pps.transquant_bypass_enable_flag = read_u(1);
	pps.tiles_enabled_flag = read_u(1);
	pps.entropy_coding_sync_enabled_flag = read_u(1);
	pps.loop_filter_across_tiles_enabled_flag=0;
	if (pps.tiles_enabled_flag){
		pps.num_tile_columns_minus1 = read_ue();
		pps.num_tile_rows_minus1 = read_ue();
		pps.uniform_spacing_flag = read_u(1);
		if (!pps.uniform_spacing_flag){
			//fprintf(stderr,"Not Uniform spaced tiles\n");
			int i;
			for (i=0;i<pps.num_tile_columns_minus1;i++){
				pps.column_width_minus1[i] = read_ue();
			}
			for (i=0;i<pps.num_tile_rows_minus1;i++){
				pps.row_height_minus1[i] = read_ue();
			}
		}
		pps.loop_filter_across_slices_enabled_flag = read_u(1);
	} else {
		pps.num_tile_columns_minus1 = 0;
		pps.num_tile_rows_minus1 = 0;
		pps.uniform_spacing_flag = 1;
	}

	pps.loop_filter_across_slices_enabled_flag = read_u(1);
	pps.deblocking_filter_control_present_flag = read_u(1);
	pps.pps_beta_offset_div2 =0;
	pps.pps_tc_offset_div2 = 0;
	pps.pps_disable_deblocking_filter_flag = 0;
	if (pps.deblocking_filter_control_present_flag){
		pps.pps_deblocking_filter_override_enabled_flag = read_u(1);
		pps.pps_disable_deblocking_filter_flag = read_u(1);
		if(!pps.pps_disable_deblocking_filter_flag){
			pps.pps_beta_offset_div2 = read_se();
			pps.pps_tc_offset_div2 = read_se();
		}
		//fprintf(stderr,"deblocking_filter_control_present_flag PRESENT\n");
	}
	pps.pps_scaling_list_data_present_flag = read_u(1);
	if (pps.pps_scaling_list_data_present_flag){
		scaling_list_data();
		//fprintf(stderr,"pps_scaling_list_data_present_flag PRESENT\n");
	}
	pps.lists_modification_present_flag = read_u(1);
	pps.log2_parallel_merge_level_minus2 = read_ue();
	pps.slice_segment_header_extension_present_flag = read_u(1);
	pps.pps_extension_flag=read_u(1);

	

	picture_parameter_sets[pps.pps_pic_parameter_set_id] = pps;
	sequence_param = &(sequence_parameter_sets[pps.pps_seq_parameter_set_id]);

}

void slice_segment_layer_rbsp(slice_segment_struct* ssl){
	in_header = 1;
	ssl->first_slice_segment_in_pic_flag = read_u(1);
	if (RapPicFlag){
		ssl->no_output_of_prior_pics_flag = read_u(1);
	}
	ssl->slice_pic_parameter_set_id = read_ue();

	picture_param = &(picture_parameter_sets[ssl->slice_pic_parameter_set_id]);
	sequence_param = &(sequence_parameter_sets[picture_param->pps_seq_parameter_set_id]);

	if (ssl->slice_pic_parameter_set_id != current_pic_parameter_id){
		current_pic_parameter_id = ssl->slice_pic_parameter_set_id;
		initialize_blocks();
	}

	if (!ssl->first_slice_segment_in_pic_flag){
		if (picture_param->dependent_slice_segments_enabled_flag)
			ssl->dependent_slice_segment_flag = read_u(1);
		ssl->slice_segment_address = read_u( log2_int(sequence_param->PicSizeInCtbsY ));
		if (!ssl->dependent_slice_segment_flag) {
			curr_slice++;
			SliceAddrRS = ssl->slice_segment_address;
		}
	} else {
		printf("New Frame\n");
		if ( ! (ctbAddrInTS==0 || ctbAddrInTS==sequence_param->PicSizeInCtbsY)) {
			printf("Non Completed Frame..exiting\n");
			while(1);
		}
			
		curr_slice = 0;
		curr_tile = -1;
		ssl->slice_segment_address = 0;
		ssl->dependent_slice_segment_flag = 0;
		ssl->short_term_ref_pic_set_sps_flag= 0;
		SliceAddrRS = 0;
	}
	axi_write(0x0140, curr_slice);
	axi_write(0x0100, SliceAddrRS);
	if (!ssl->dependent_slice_segment_flag ){
		ctbAddrInRS = ssl->slice_segment_address;
		ctbAddrInTS = CtbAddrRstoTs[ctbAddrInRS];
		
		int i;
		for (i=0;i<picture_param->num_extra_slice_header_bits;i++) read_u(1);
		ssl->slice_type = read_ue();
		axi_write(0x0060, ssl->slice_type);
		if (picture_param->output_flag_present_flag){
			ssl->pic_output_flag = read_u(1);
		}
		ssl->short_term_ref_pic_set_sps_flag= 1;

		int PicOrderCntMsb = 0;
		
		int MaxPicOrderCntLsb = 1<<(sequence_param->log2_max_pic_order_cnt_lsb_minus4+4);

		ssl->pic_order_cnt_lsb = 0;
		struct slice_0 h_10;
		h_10.header = 0x10;
		h_10.null = 0;
		h_10.POC = 0;
		if (!IdrPicFlag){
			ssl->pic_order_cnt_lsb = read_u(sequence_param->log2_max_pic_order_cnt_lsb_minus4+4);

			if ( (ssl->pic_order_cnt_lsb < prevPicOrderCntLsb) && ( ( prevPicOrderCntLsb -ssl->pic_order_cnt_lsb) >= (MaxPicOrderCntLsb /2 ))){
				PicOrderCntMsb = prevPicOrderCntMsb + MaxPicOrderCntLsb;
			} else if ( (ssl->pic_order_cnt_lsb > prevPicOrderCntLsb) && ( ( ssl->pic_order_cnt_lsb - prevPicOrderCntLsb)> (MaxPicOrderCntLsb/2))) {
				PicOrderCntMsb = prevPicOrderCntMsb - MaxPicOrderCntLsb;
			} else {
				PicOrderCntMsb = prevPicOrderCntMsb;
			}
			PicOrderCntVal = PicOrderCntMsb + ssl->pic_order_cnt_lsb;
			h_10.POC = PicOrderCntVal;
			ssl->short_term_ref_pic_set_sps_flag = read_u(1);
			ssl->short_term_ref_pic_set_idx=0;
			if (!ssl->short_term_ref_pic_set_sps_flag){
				short_term_ref_pic_set(sequence_param, sequence_param->num_short_term_ref_pic_sets);
				//fprintf(stderr,"Short term ref pic set sps=0 havent tested\n");
			}
			else if (sequence_param->num_short_term_ref_pic_sets>1){
				int bits = log2_int( sequence_param->num_short_term_ref_pic_sets);
				ssl->short_term_ref_pic_set_idx = read_u(bits);
			}
			if (sequence_param->long_term_ref_pics_present_flag){
				fprintf(stderr,"long term ref pictures not supported\n"); //TODO
			}
			ssl->slice_temporal_mvp_enable_flag = 0;
			if (sequence_param->sps_temporal_mvp_enable_flag){
				ssl->slice_temporal_mvp_enable_flag = read_u(1);
			}
		}
		if (RefPicture){
			prevPicOrderCntLsb = ssl->pic_order_cnt_lsb;
			prevPicOrderCntMsb = PicOrderCntMsb;
		}
		if (ssl->first_slice_segment_in_pic_flag){
			fwrite(&h_10,4,2,cabac_to_residual);
			axi_write(0x0500, (void *) &h_10); axi_write(0x0500, PicOrderCntVal);
		}
		//if (ssl->pic_order_cnt_lsb!=80) return; //TODO Debug
		printf("POC %d\n",PicOrderCntVal);
		tot_frames++;
		//if (PicOrderCntVal>=0) return;

		if (PicOrderCntVal < START_POC || PicOrderCntVal > END_POC) 
			return;
		if (PicOrderCntVal ==64) {
		//	cabac_printing = 1;
		}
		//if (current->len < 25000) return;
//		print_loc();
		if (sequence_param->sample_adaptive_offset_enabled_flag){
			ssl->slice_sao_luma_flag = read_u(1);
			ssl->slice_sao_chroma_flag = read_u(1);
		}
		axi_write(0x00f0, (ssl->slice_sao_luma_flag<<1 | ssl->slice_sao_chroma_flag));
		if (ssl->slice_type < 2 ){
			ssl->num_ref_idx_active_override_flag = read_u(1);
			ssl->num_ref_idx_l0_active_minus1 = picture_param->num_ref_idx_l0_default_active_minus1;
			ssl->num_ref_idx_l1_active_minus1 = picture_param->num_ref_idx_l1_default_active_minus1;
			if (ssl->num_ref_idx_active_override_flag){
				ssl->num_ref_idx_l0_active_minus1 = read_ue();
				if (ssl->slice_type==0){
					ssl->num_ref_idx_l1_active_minus1 = read_ue();
				}
			}
			axi_write(0x00a0, ssl->num_ref_idx_l0_active_minus1);
			axi_write(0x00b0, ssl->num_ref_idx_l1_active_minus1);
			if (picture_param->lists_modification_present_flag){
				fprintf(stderr,"lists modification present\n");
			}
			if (ssl->slice_type==0){
				ssl->mvd_l1_zero_flag = read_u(1);
			}
			axi_write(0x00c0, ssl->mvd_l1_zero_flag);
			ssl->cabac_init_flag = 0;
			if (picture_param->cabac_init_present_flag){
				ssl->cabac_init_flag = read_u(1);
				//print_loc();
			}
			
			if (ssl->slice_temporal_mvp_enable_flag){
				ssl->collocated_from_l0_flag=1;
				if (ssl->slice_type==0){
					ssl->collocated_from_l0_flag = read_u(1);
				}
				ssl->collocated_ref_idx=0;
				if ( (ssl->collocated_from_l0_flag && ssl->num_ref_idx_l0_active_minus1>0) || (!ssl->collocated_from_l0_flag && ssl->num_ref_idx_l1_active_minus1>0)){
					ssl->collocated_ref_idx = read_ue();
				}
			}
			if ( (picture_param->weighted_pred_flag && ssl->slice_type==1) || (picture_param->weighted_bipred_flag && ssl->slice_type==0)){
				//fprintf(stderr,"Weighted Prediction Not enabled\n"); //TODO
				pred_weight_table();
			}
			ssl->five_minus_max_num_merge_cand = read_ue();
			MaxNumMergeCand = 5-ssl->five_minus_max_num_merge_cand;
			axi_write(0x0090, MaxNumMergeCand);
		}
//		print_loc();
		ssl->slice_qp_delta = read_se();
		SliceQPY = 26 + picture_param->init_qp_minus26 + ssl->slice_qp_delta;
		ssl->slice_cb_qp_offset = 0;
		ssl->slice_cr_qp_offset = 0;
		ssl->slice_loop_filter_across_slices_enabled_flag = 0;
		ssl->slice_beta_offset_div2 = picture_param->pps_beta_offset_div2;
		ssl->slice_tc_offset_div2 = picture_param->pps_tc_offset_div2;
		ssl->slice_disable_deblocking_filter_flag = picture_param->pps_disable_deblocking_filter_flag;
		if (picture_param->pps_slice_chroma_qp_offsets_present_flag){
			//printf("slice_chroma_qp_offsets present\n"); exit(0);
			ssl->slice_cb_qp_offset = read_se();
			ssl->slice_cr_qp_offset = read_se();
		}
		if (picture_param->deblocking_filter_control_present_flag){
			ssl->deblocking_filter_override_flag = 0;

			if (picture_param->pps_deblocking_filter_override_enabled_flag){
				ssl->deblocking_filter_override_flag = read_u(1);
			}
			if (ssl->deblocking_filter_override_flag){
				ssl->slice_disable_deblocking_filter_flag = read_u(1);
				if (!ssl->slice_disable_deblocking_filter_flag){
					ssl->slice_beta_offset_div2 = read_se();
					ssl->slice_tc_offset_div2 = read_se();
				}
			}
		}
		if (picture_param->loop_filter_across_slices_enabled_flag && (ssl->slice_sao_luma_flag || ssl->slice_sao_chroma_flag || !ssl->slice_disable_deblocking_filter_flag )){
			ssl->slice_loop_filter_across_slices_enabled_flag = read_u(1);
		}
	} else {
		//fprintf(stderr,"Dependent Slices Not supported\n"); //TODO
	}
	ssl->num_entry_point_offsets = 0;

	int i;
	int partition_begin[256]; // 256 - max (no. of ctu rows = 4096/16, max_tiles=121)
	int partition_len[256];

	if (picture_param->tiles_enabled_flag || picture_param->entropy_coding_sync_enabled_flag){
		ssl->num_entry_point_offsets = read_ue();
		if (ssl->num_entry_point_offsets>0){
			int offset_len_minus_1 = read_ue();
			int offset;
			for (i =0;i<ssl->num_entry_point_offsets;i++){
				offset =read_u(offset_len_minus_1+1);
				partition_len[i] = offset + 1;
			}
			//fprintf(stderr,"Entry point offsets >0\n"); //TODO
		}
	}
	if (picture_param->slice_segment_header_extension_present_flag){
		int header_length = read_ue();
		for (int i=0;i<header_length;i++) {
			int a = read_u(8);
			a++;
		}
		//fprintf(stderr,"Slice Header Extension Not implemented\n"); //TODO
	}
	//if (current->bit!=0) // remove for old streams
	in_header = 0;
		axi_write(0x810,1);
		byte_alignment();
		
	print_loc();
	printf("size of ssl %d\n",current->len - current->pos);
	int num_partitions = ssl->num_entry_point_offsets + 1;
	int current_partition = 0;
	int current_partition_pos = 0;
	partition_begin[0] = current->pos;
	for (i=1;i<num_partitions;i++){
		partition_begin[i] = partition_begin[i-1] + partition_len[i-1];
	}
	partition_len[num_partitions-1] = current->len - partition_begin[num_partitions-1];

	int initType;
	if (ssl->slice_type==SLICE_I)
		initType = 0;
	else if (ssl->slice_type==SLICE_P)
		initType = ssl->cabac_init_flag ? 2 : 1;
	else
		initType = ssl->cabac_init_flag ? 1 : 2;
	axi_write(0x0640, initType);
	if (!ssl->dependent_slice_segment_flag) {
		int xCtb = (ctbAddrInRS % sequence_param->PicWidthInCtbsY) << sequence_param->Log2CtbSizeY;
		int yCtb = (ctbAddrInRS / sequence_param->PicWidthInCtbsY) << sequence_param->Log2CtbSizeY;
		struct new_slice_tile nsl;
		*(( int*) (&nsl)) = 0;
		nsl.header = 0x15;
		nsl.xC = xCtb;
		nsl.yC = yCtb;
		fwrite(&nsl,4,1,cabac_to_residual); axi_write(0x0500, (void *) &nsl);

		struct slice_1 h_11;
		*(( int*) (&h_11)) = 0;
		h_11.header = 0x11;
		h_11.short_term_ref_pic_sps = ssl->short_term_ref_pic_set_sps_flag;
		h_11.short_term_ref_pic_idx = ssl->short_term_ref_pic_set_idx;
		h_11.temporal_mvp_enabled = ssl->slice_temporal_mvp_enable_flag;
		h_11.sao_luma = ssl->slice_sao_luma_flag;
		h_11.sao_chroma = ssl->slice_sao_chroma_flag;
		h_11.num_ref_idx_l0_minus1 = ssl->num_ref_idx_l0_active_minus1;
		h_11.num_ref_idx_l1_minus1 = ssl->num_ref_idx_l1_active_minus1;
		h_11.five_minus_max_merge_cand = ssl->five_minus_max_num_merge_cand;
		h_11.slice_type = ssl->slice_type;
		h_11.collocated_from_l0 = ssl->collocated_from_l0_flag;
		h_11.slice_loop_filter_across_slices_enabled_flag = ssl->slice_loop_filter_across_slices_enabled_flag;
		fwrite(&h_11,4,1,cabac_to_residual); axi_write(0x0500, (void *) &h_11);

		struct slice_2 h_12;
		*(( int*) (&h_12)) = 0;
		h_12.header = 0x12;
		h_12.slice_cb_qp_offset = ssl->slice_cb_qp_offset;
		h_12.slice_cr_qp_offset = ssl->slice_cr_qp_offset;
		h_12.disable_dbf = ssl->slice_disable_deblocking_filter_flag;
		h_12.slice_beta_offset_div2 = ssl->slice_beta_offset_div2;
		h_12.slice_tc_offset_div2 = ssl->slice_tc_offset_div2;
		h_12.collocated_ref_idx = ssl->collocated_ref_idx;
		fwrite(&h_12,4,1,cabac_to_residual); axi_write(0x0500, (void *) &h_12);

		int i= sequence_param->num_short_term_ref_pic_sets;
		int j;
		if (!ssl->short_term_ref_pic_set_sps_flag) {
			struct slice_3 h_13;
			*(( int*) (&h_13)) = 0;
			h_13.header = 0x13;
			h_13.rps_id = i;
			h_13.num_negative = (sequence_param->stRPS[i].NumNegativePics  );
			h_13.num_positive = (sequence_param->stRPS[i].NumPositivePics  );
			fwrite(&h_13,4,1,cabac_to_residual); axi_write(0x0500, (void *) &h_13);
			for (j=0;j<sequence_param->stRPS[i].NumPositivePics;j++){
				struct slice_4 h_14;
				*(( int*) (&h_14)) = 0;
				h_14.header = 0x14;
				h_14.rps_id = i;
				h_14.used = sequence_param->stRPS[i].UsedByCurrPicS1[j];
				h_14.delta_POC = sequence_param->stRPS[i].DeltaPocS1[j];
				fwrite(&h_14,4,1,cabac_to_residual); axi_write(0x0500, (void *) &h_14);
			}
			for (j=0;j<sequence_param->stRPS[i].NumNegativePics;j++){
				struct slice_4 h_14;
				*(( int*) (&h_14)) = 0;
				h_14.header = 0x14;
				h_14.rps_id = i;
				h_14.used = sequence_param->stRPS[i].UsedByCurrPicS0[j];
				h_14.delta_POC = sequence_param->stRPS[i].DeltaPocS0[j];
				fwrite(&h_14,4,1,cabac_to_residual); axi_write(0x0500, (void *) &h_14);
			}
		}
	}


	int j;
	int new_tile=0;
	for (i=0;i<=picture_param->num_tile_rows_minus1;i++){
		for (j=0;j<=picture_param->num_tile_columns_minus1;j++){
			if (firstCtbRS[i][j]==ssl->slice_segment_address){
				new_tile=1;
				tile_i = i;
				tile_j = j;
				break;
			}
		}
	}

	if (!ssl->dependent_slice_segment_flag) {
		//initialize_cabac(&cabac_current,1);
		qPY = SliceQPY; axi_write(0x0030,SliceQPY);
		init_cabac_contexts(); axi_write(0x0630,(unsigned int) 0);
		init_cabac_engine(); axi_write(0x0620,(unsigned int) 0);
		if (sequence_param->PicWidthInCtbsY ==1) { save_cabac_contexts(); axi_write(0x0600,(unsigned int) 0);}
	} else {
		//init_cabac_contexts();
		init_cabac_engine(); axi_write(0x0620,(unsigned int) 0);
		// has to initialize contexts if start of new tile
		if (picture_param->tiles_enabled_flag && TileId[ctbAddrInTS] != TileId[ctbAddrInTS-1]){
			printf("New Tile (%d %d)\n", (ctbAddrInRS% sequence_param->PicWidthInCtbsY) << sequence_param->Log2CtbSizeY , (ctbAddrInRS / sequence_param->PicWidthInCtbsY ) << sequence_param->Log2CtbSizeY);
			init_cabac_contexts(); axi_write(0x0630,(unsigned int) 0);
			qPY = SliceQPY; axi_write(0x0030,SliceQPY);
		}

	}
	if (picture_param->entropy_coding_sync_enabled_flag && ((ctbAddrInTS %sequence_param->PicWidthInCtbsY)==0)){
		qPY = SliceQPY; axi_write(0x0030,SliceQPY);
		if (SliceAddrRS <= ((ctbAddrInTS - sequence_param->PicWidthInCtbsY)+1)) {// slice starts before the previous ctu rows 2nd ctu (otherwise no valid context to load)
			load_cabac_contexts();
			axi_write(0x0610,(unsigned int) 0);
		}
	}


	if (new_tile) // new tile
		send_new_tile();
	axi_write(0x0000, ctbAddrInTS); // Start cu_offload
	axi_write(0x03f0, 0x00000001); // Start cu_offload

	in_header = 0;
	int end_of_slice_segment_flag=0;
	do {

		SliceId[ctbAddrInRS] = curr_slice;

		coding_tree_unit();
		end_of_slice_segment_flag = read_cabac_end_of_slice_segment_flag();

		if (end_of_slice_segment_flag==1) {
			printf("%d end_of_slice_segment_flag (%d %d)\n",ctbAddrInTS, (ctbAddrInRS%sequence_param->PicWidthInCtbsY) << sequence_param->Log2CtbSizeY, (ctbAddrInRS/sequence_param->PicWidthInCtbsY) << sequence_param->Log2CtbSizeY); //print_cabac();
			
			int pix_per_frame = sequence_param->pic_width_in_luma_samples*sequence_param->pic_height_in_luma_samples*3/2;
			long tot_pix = ((long) tot_frames)*pix_per_frame;
			long freq1 = (long) (((cycles1+0.0)/(1000*tot_frames))*30);
			long freq2 = (long) (((cycles2+0.0)/(1000*tot_frames))*30);
			long freq3 = (long) (((cycles3+0.0)/(1000*tot_frames))*30);
			long freq4 = (long) (((cycles4+0.0)/(1000*tot_frames))*30);
			//printf("Cycle count %8ld %8ld %8ld \n",cycles1*120/(tot_frames*1000),cycles2*120/(tot_frames*1000),cycles3*120/(tot_frames*1000));
			//printf("Cycle count %8ld %8ld %8ld %8ld\n",freq1,freq2,freq3, freq4);
			printf("Cycle count %8ld %8ld\n",cycles1,cycles3);
			//cycles1 = 0;
			//cycles3 = 0;

		}

		ctbAddrInTS++;
		ctbAddrInRS = CtbAddrTstoRs[ctbAddrInTS];

		if (picture_param->entropy_coding_sync_enabled_flag && ( ((ctbAddrInTS %sequence_param->PicWidthInCtbsY)==2) || ( sequence_param->PicWidthInCtbsY==2 && ((ctbAddrInTS %sequence_param->PicWidthInCtbsY)==0) ))){
			save_cabac_contexts();
		}

		if (!end_of_slice_segment_flag &&
				(
						(picture_param->tiles_enabled_flag && TileId[ctbAddrInTS] != TileId[ctbAddrInTS-1]) ||
						(picture_param->entropy_coding_sync_enabled_flag &&  ((ctbAddrInTS %sequence_param->PicWidthInCtbsY)==0) ) // RS TS wont matter
				)
			) {
				read_cabac_end_of_sub_stream_one_bit();
				qPY = SliceQPY;

			
			if (current->bit!=0)
				byte_alignment(); // THis is clearly wrong
			//print_loc();

			
			current_partition++;
			current_partition_pos=0;
			// Only return to the host in the case of a new Tile. For new WPP rows, the relevant part is handled in cu_offload
			if (picture_param->tiles_enabled_flag) {
				printf("New Tile (%d %d) offset %d\n", (ctbAddrInRS% sequence_param->PicWidthInCtbsY) << sequence_param->Log2CtbSizeY , (ctbAddrInRS / sequence_param->PicWidthInCtbsY ) << sequence_param->Log2CtbSizeY,current->pos);
				tile_j++;
				if (tile_j == picture_param->num_tile_columns_minus1+1){
					tile_j=0;
					tile_i++;
				}
				axi_write(0x400,ctbAddrInTS);
	//			read_cabac_FL(1,0);
				axi_write(0x820,1);
				send_new_tile();
				axi_write(0x0030,SliceQPY);

				init_cabac_contexts(); axi_write(0x0630,(unsigned int) 0);
				init_cabac_engine(); axi_write(0x0620,(unsigned int) 0);

				axi_write(0x0000, ctbAddrInTS); // Start cu_offload
				axi_write(0x03f0, 0x00000001); // Start cu_offload

			}
			if (picture_param->entropy_coding_sync_enabled_flag) { // no tiles assumed
				int mod = ctbAddrInTS % sequence_param->PicWidthInCtbsY;
				// Also has to save if PicWidthInCtbsY ==2
				if (sequence_param->PicWidthInCtbsY ==2) { // Not In RTL // TODO
					axi_write(0x0600,(unsigned int) 0); // save_cabac
				}
				/* -- Moved to cu_ofload 17/6/2015
				if (SliceAddrRS <= ((ctbAddrInTS - sequence_param->PicWidthInCtbsY)+1))
					axi_write(0x0610,(unsigned int) 0); // load_cabac
				else
					axi_write(0x0630,(unsigned int) 0); // init ctx
				
				axi_write(0x0030,SliceQPY);
				axi_write(0x0620,(unsigned int) 0); // Init engine
				axi_write(0x0000, ctbAddrInTS); // Start cu_offload
				axi_write(0x03f0, 0x00000001); // Start cu_offload
				*/
				if (SliceAddrRS <= ((ctbAddrInTS - sequence_param->PicWidthInCtbsY)+1))
					load_cabac_contexts();
				else
					init_cabac_contexts();
				init_cabac_engine();
			}
			
		}
		//cabac_printing = 1;
//		if (ctbAddrInTS==11){ printf("End of Parsing\n"); exit(0); }
		
	} while (!end_of_slice_segment_flag);
	axi_write(0x400,ctbAddrInTS);
	// save the state if slice ended 2 CTUs in when WPP is enabled - Not In RTL // TODO
	if ( (ctbAddrInTS % sequence_param->PicWidthInCtbsY ==2) && picture_param->entropy_coding_sync_enabled_flag){
		axi_write(0x0600,(unsigned int) 0); // save_cabac
	}
}


void send_new_tile(){

	curr_tile++;

	axi_write(0x0150, (unsigned int) tile_i*(picture_param->num_tile_columns_minus1+1) + tile_j);
	axi_write(0x0160, (unsigned int) firstCtbRS[tile_i][tile_j]);
	axi_write(0x0170, (unsigned int) colWidth[tile_j]);
	axi_write(0x0180, (unsigned int) colWidth[tile_j]* rowHeight[tile_i]);
	axi_write(0x0190, (unsigned int) colBd[tile_j]<<sequence_param->Log2CtbSizeY);
	

	struct new_slice_tile nsl;
	*(( int*) (&nsl)) = 0;
	nsl.header = 0x16;
	nsl.xC = colBd[tile_j]<<sequence_param->Log2CtbSizeY;
	nsl.yC = rowBd[tile_i]<<sequence_param->Log2CtbSizeY;
	//nsl.xC =0;
	//nsl.yC =0;
	axi_write(0x0500, (void *) &nsl);
	struct new_tile_width ntw;
	*(( int*) (&ntw)) = 0;
	ntw.header = 0x17;

	int width = 0;
	
	if (tile_j==picture_param->num_tile_columns_minus1) {
		width = sequence_param->pic_width_in_luma_samples-(colBd[tile_j]<<sequence_param->Log2CtbSizeY);
	} else {
		width = colWidth[tile_j]<<sequence_param->Log2CtbSizeY;
	}
	if (!picture_param->tiles_enabled_flag) {
		width = sequence_param->pic_width_in_luma_samples;
	}
	ntw.width = width;
	axi_write(0x0500, (void *) &ntw);
}

int xCurr_n; int yCurr_n;
void coding_tree_unit(){

	int xCtb = (ctbAddrInRS % sequence_param->PicWidthInCtbsY) << sequence_param->Log2CtbSizeY;
	int yCtb = (ctbAddrInRS / sequence_param->PicWidthInCtbsY) << sequence_param->Log2CtbSizeY;

	int ctbRSL = ctbAddrInRS -1 ;
	int ctbRSU = ctbAddrInRS - sequence_param->PicWidthInCtbsY;
	int ctbTSL = CtbAddrRstoTs[ctbRSL];
	int ctbTSU = CtbAddrRstoTs[ctbRSU];
	same_slice_tile_left = ctbRSL >= 0 ? (SliceId[ctbAddrInRS] == SliceId[ctbRSL]) && ( TileId[ctbAddrInTS] == TileId[ctbTSL] ) : 1;
	same_slice_tile_up = ctbRSU >= 0 ?(SliceId[ctbAddrInRS] == SliceId[ctbRSU]) && ( TileId[ctbAddrInTS] == TileId[ctbTSU] ) : 1;
	//if (SliceId[ctbRSCurr]!=SliceId[ctbRSN]) return 0;
	//if (TileId[ctbTSCurr] != TileId[ctbTSN]) return 0;

	//printf("TS %d RS %d (%d,%d)\n",ctbAddrInTS,ctbAddrInRS, xCtb,yCtb);
	if (ctbAddrInTS==0 || (TileId[ctbAddrInTS]!=TileId[ctbAddrInTS-1]) ){
		struct new_slice_tile nsl;
		*(( int*) (&nsl)) = 0;
		nsl.header = 0x16;
		nsl.xC = xCtb;
		nsl.yC = yCtb;
		fwrite(&nsl,4,1,cabac_to_residual);
		struct new_tile_width ntw;
		*(( int*) (&ntw)) = 0;
		ntw.header = 0x17;
		int i=0;
		int width = 0;
		for (i=0;i<picture_param->num_tile_columns_minus1+1;i++){
			if (colBd[i]==(xCtb>>sequence_param->Log2CtbSizeY)){
				if (i==picture_param->num_tile_columns_minus1)
					width = sequence_param->pic_width_in_luma_samples-(colBd[i] * (1<<sequence_param->Log2CtbSizeY));
				else 
					width = colWidth[i]*(1<<sequence_param->Log2CtbSizeY);
			}
		}
		if (!picture_param->tiles_enabled_flag) {
			width = sequence_param->pic_width_in_luma_samples;
		}
		ntw.width = width;
		fwrite(&ntw,4,1,cabac_to_residual);
	}
	struct CTU_0 h_20;
	*(( int*) (&h_20)) = 0;
	h_20.header = 0x20;
	h_20.xC = xCtb;
	h_20.yC = yCtb;
	fwrite(&h_20,4,1,cabac_to_residual);
	struct CTU_1 h_21;
	*(( int*) (&h_21)) = 0;
	h_21.header = 0x21;
	h_21.slice_id = curr_slice;
	h_21.tile_id = TileId[ctbAddrInTS];
	fwrite(&h_21,4,1,cabac_to_residual);
	//if (slice_segment_header.pic_order_cnt_lsb==4) {
		//printf("CTU %d %d %d ",xCtb,yCtb, ctbAddrInTS);
		//print_cabac();
	//}



	if (slice_segment_header.slice_sao_luma_flag || slice_segment_header.slice_sao_chroma_flag){
		sao(xCtb >> sequence_param->Log2CtbSizeY, yCtb >> sequence_param->Log2CtbSizeY);

		int sao_x =(yCtb >> sequence_param->Log2CtbSizeY)*sequence_param->PicWidthInCtbsY + (xCtb >> sequence_param->Log2CtbSizeY);
		struct CTU_2 h_22;
		int i;
		for (i=0;i<3;i++){
			*(( int*) (&h_22)) = 0;
			h_22.header = 0x22+i;
			h_22.SaoType = sao_list[sao_x].SaoTypeIdx[i];
			if (h_22.SaoType==1){
				h_22.BandPos_EO = sao_list[sao_x].sao_band_position[i];
			} else {
				h_22.BandPos_EO = sao_list[sao_x].SaoEoClass[i];
			}
			h_22.sao_offset_abs_0 = sao_list[sao_x].sao_offset_abs[i][0];
			h_22.sao_offset_abs_1 = sao_list[sao_x].sao_offset_abs[i][1];
			h_22.sao_offset_abs_2 = sao_list[sao_x].sao_offset_abs[i][2];
			h_22.sao_offset_abs_3 = sao_list[sao_x].sao_offset_abs[i][3];
			h_22.sao_offset_sign_0 = sao_list[sao_x].sao_offset_sign[i][0];
			h_22.sao_offset_sign_1 = sao_list[sao_x].sao_offset_sign[i][1];
			h_22.sao_offset_sign_2 = sao_list[sao_x].sao_offset_sign[i][2];
			h_22.sao_offset_sign_3 = sao_list[sao_x].sao_offset_sign[i][3];
			if ( (i==0 && slice_segment_header.slice_sao_luma_flag) || (i>0 && slice_segment_header.slice_sao_chroma_flag))
			fwrite(&h_22,4,1,cabac_to_residual);
		}
	}


	int cu_mmap_write = 0  << 30 |  (xCtb ) << 18 | (yCtb ) << 6 | 6 << 3 | 0 << 2 | same_slice_tile_left << 1 | same_slice_tile_up;
		//fwrite(&cu_mmap_write,4,1,cu_mmap);
	coding_quadtree(xCtb,yCtb,sequence_param->Log2CtbSizeY,0);

}
void sao(int rx, int ry){
	int sao_merge_left_flag = 0;
	int sao_merge_up_flag = 0;
	int b = ry*sequence_param->PicWidthInCtbsY + rx;
	if (rx>0){
		//int leftCtbInSliceSeg = ctbAddrInRS>slice_segment_header.slice_segment_address;
		//int leftCtbInTile = TileId[ctbAddrInTS] == TileId[CtbAddrRstoTs[ctbAddrInRS-1]];
		if (same_slice_tile_left) {
		//if (leftCtbInSliceSeg && leftCtbInTile){
			sao_merge_left_flag = read_cabac_sao_merge_left();
			if (sao_merge_left_flag){
				sao_list[b] = sao_list[b-1];
			}
		}
//		fprintf(stderr, "sao rx>0\n");
	}
	if (ry > 0 && !sao_merge_left_flag){
		int upCtbInSliceSeg = (ctbAddrInRS-sequence_param->PicWidthInCtbsY) >= slice_segment_header.slice_segment_address;
		int upCtbInTile = (TileId[ctbAddrInTS] == TileId[CtbAddrRstoTs[ctbAddrInRS-sequence_param->PicWidthInCtbsY]]);
		if (same_slice_tile_up) {
		//if (upCtbInSliceSeg && upCtbInTile){
			sao_merge_up_flag = read_cabac_sao_merge_left();
			if (sao_merge_up_flag){
				sao_list[b] = sao_list[b-sequence_param->PicWidthInCtbsY];
			}
		}
//		fprintf(stderr, "sao ry>0\n");
	}
	if (!sao_merge_left_flag && !sao_merge_up_flag){
		int cIdx;
		sao_list[b].sao_band_position[0] = 0; sao_list[b].sao_band_position[1] = 0;  sao_list[b].sao_band_position[2] = 0; 
		sao_list[b].SaoEoClass[0] = 0; sao_list[b].SaoEoClass[1] = 0; sao_list[b].SaoEoClass[2] = 0;
		for (cIdx=0;cIdx<3;cIdx++){
			if (  (slice_segment_header.slice_sao_luma_flag && cIdx==0) || (slice_segment_header.slice_sao_chroma_flag && cIdx>0)){
				if (cIdx==0){
					sao_list[b].SaoTypeIdx[0] = read_cabac_sao_type_idx();
				} else if (cIdx==1){
					sao_list[b].SaoTypeIdx[1] = read_cabac_sao_type_idx();
					sao_list[b].SaoTypeIdx[2] = sao_list[b].SaoTypeIdx[1];
				}
				int i=0;
				for (i=0;i<4;i++) {
					sao_list[b].sao_offset_sign[cIdx][i] = 0;
					sao_list[b].sao_offset_abs[cIdx][i] = 0;
				}
				
				
				if (sao_list[b].SaoTypeIdx[cIdx]!=0){
					
					for (i=0;i<4;i++){
						sao_list[b].sao_offset_abs[cIdx][i] = read_cabac_sao_offset_abs();
					}
					if (sao_list[b].SaoTypeIdx[cIdx]==1){
						for (i=0;i<4;i++){
							if (sao_list[b].sao_offset_abs[cIdx][i]!=0){
								sao_list[b].sao_offset_sign[cIdx][i] = read_cabac_sao_offset_sign();
							}
						}
						sao_list[b].sao_band_position[cIdx] = read_cabac_sao_band_position();
					} else {
						if (cIdx==0){
							sao_list[b].SaoEoClass[0] = read_cabac_sao_eo_class_xxx();
						}
						if (cIdx==1){
							sao_list[b].SaoEoClass[1] = read_cabac_sao_eo_class_xxx();
							sao_list[b].SaoEoClass[2] =sao_list[b].SaoEoClass[1];
						}
					}
				}
			}
		}
	}
}

void coding_quadtree(int x0, int y0, int log2CbSize, int cqtDepth){

	if (cabac_printing) printf("Coding Quadtree %d %d %d\n",x0,y0,log2CbSize);
//	cabac_printing = 0;
//	if (x0==32 && y0 == 32){
//			printf("End of Parsing\n"); exit(0);
//		}
//	if (x0==0 && y0 == 56 ) break_point=1;;
	int split_cu_flag;
	//CtDepth[y0*sequence_param->pic_width_in_luma_samples+x0] = cqtDepth;
	CtDepth_cur = cqtDepth;
	if ( (x0 +(1<<log2CbSize) <= sequence_param->pic_width_in_luma_samples) &&
		 (y0 +(1<<log2CbSize) <= sequence_param->pic_height_in_luma_samples) &&
		 log2CbSize > sequence_param->Log2MinCbSizeY
		) {
		xCurr_n = x0; yCurr_n=y0;
		split_cu_flag = read_cabac_split_cu_flag();
	} else {
		if (log2CbSize>sequence_param->Log2MinCbSizeY) split_cu_flag =1;
		else split_cu_flag = 0;
	}
	if (picture_param->cu_qp_delta_enabled_flag && log2CbSize >= sequence_param->Log2MinCuQpDeltaSize) {
		IsCuQpDeltaCoded = 0;
		CuQpDelta = 0;
	}
	quadtree_start = clock();
	if (!split_cu_flag) {
		int nCbS = (1<<log2CbSize);
		int i,j;
		for (i=0;i<nCbS;i+=8){
			//for (j=0;j<nCbS;j++){
				//int linAddr = (y0+j)*sequence_param->pic_width_in_luma_samples+(x0+i);
				//CtDepth[linAddr] = cqtDepth;
				CtDepth_buf[(x0+i)>>3] = cqtDepth;
				//CtDepth_buf_left[ ((y0+j) & 63) >> 3] = cqtDepth;
//				printf("%d\n",linAddr);
			//}
		}
		for (j=0;j<nCbS;j+=8){
				//int linAddr = (y0+j)*sequence_param->pic_width_in_luma_samples+(x0+i);
				//CtDepth[linAddr] = cqtDepth;
				//CtDepth_buf[(x0+i)>>3] = cqtDepth;
				CtDepth_buf_left[ ((y0+j) & 63) >> 3] = cqtDepth;
//				printf("%d\n",linAddr);
			}
	}
	quadtree_end = clock();
	quad_tree_time +=  ( ((double)quadtree_end-quadtree_start)/CLOCKS_PER_SEC);
	if (split_cu_flag){
		int x1 = x0 + ((1<<log2CbSize)>>1);
		int y1 = y0 + ((1<<log2CbSize)>>1);
		coding_quadtree(x0,y0,log2CbSize-1,cqtDepth+1);
		if (x1<sequence_param->pic_width_in_luma_samples)
			coding_quadtree(x1,y0,log2CbSize-1,cqtDepth+1);
		if (y1<sequence_param->pic_height_in_luma_samples)
			coding_quadtree(x0,y1,log2CbSize-1,cqtDepth+1);
		if (x1<sequence_param->pic_width_in_luma_samples && y1<sequence_param->pic_height_in_luma_samples)
			coding_quadtree(x1,y1,log2CbSize-1,cqtDepth+1);

	} else {
		coding_unit(x0,y0,log2CbSize);
	}
}

void coding_unit(int x0,int y0, int log2CbSize){
	if (cabac_printing)
		printf("Coding Unit %d %d %d\n",x0,y0,log2CbSize);
	global_xBase = x0; global_yBase=y0;
	int pred_mode_flag=-1;
	int part_mode=-1;
	int PartMode = PART_2Nx2N;
	IntraSplitFlag =0;
	int nCbS = 1<<log2CbSize;
	int linx0y0 = y0*sequence_param->pic_width_in_luma_samples+x0;
	int cu_transquant_bypass_flag = 0;
	int pcm_flag = 0;
	struct CU_0 h_30;
	*(( int*) (&h_30)) = 0;
	h_30.header = 0x30;
	int CTUSize = 1<<sequence_param->Log2CtbSizeY;
	h_30.x0 = (x0 % CTUSize);
	h_30.y0 = (y0 % CTUSize);
	h_30.log2_cb_size = log2CbSize;
	h_30.predMode = MODE_INTER;
	h_30.PartMode = PartMode;
	h_30.bypass = cu_transquant_bypass_flag;
	h_30.pcm = 0;

	IntraPredModeY_cur[0] = 35; IntraPredModeY_cur[1] = 35; IntraPredModeY_cur[2] = 35; IntraPredModeY_cur[3] = 35; IntraPredModeC = 35; IntraSplitFlag = 0;

	if (picture_param->transquant_bypass_enable_flag){
		fprintf(stderr,"transquant bypass not supported\n"); //TODO
	}
	cu_skip_flag_cur = 0;
	if (slice_segment_header.slice_type!=SLICE_I){
		xCurr_n = x0; yCurr_n=y0;
		cu_skip_flag_cur = read_cabac_cu_skip_flag();
		int i,j;
		for (i=0;i<nCbS;i+=8){
			cu_skip_flag_buf[ (x0+i) >>3] = cu_skip_flag_cur;
		}
		for (j=0;j<nCbS;j+=8){
			cu_skip_flag_buf_left[ ((y0+j) & 63) >> 3] = cu_skip_flag_cur; 
		}
	}

	int x,y;
	switch (sequence_param->Log2CtbSizeY) {
		case 6 :   x = (x0 & 63) >>3; y = (y0 & 63) >>3; break;
		case 5 :   x = (x0 & 31) >>3; y = (y0 & 31) >>3; break;
		case 4 :   x = (x0 & 15) >>3; y = (y0 & 15) >>3; break;
	}
	// retain the same QP if not start of new Quantization Group
	int qPy_pred = (1+ (x==0? qPY : qPY_left[y]) + (y==0 ? qPY : qpY_top[x]))/2;
	if ( ((x0 & (( 1<< sequence_param->Log2MinCuQpDeltaSize) -1 )) == 0) && ((y0 & (( 1<< sequence_param->Log2MinCuQpDeltaSize) -1 )) == 0) ){
		qPY = qPy_pred;
	}
	int i;
	for (i=0;i<(1<<(log2CbSize-3));i+=1) {
		qPY_left[y+i] = qPY;
		qpY_top[x+i] = qPY;
	}
	if (cu_skip_flag_cur){
		cu[log2CbSize]++;
		//send_cu_inter(x0,y0,log2CbSize,PartMode);
		fwrite(&h_30,4,1,cabac_to_residual);// fwrite(&h_30,4,1,cu_out);
		prediction_unit(x0,y0,nCbS,nCbS,0);	
		int j;
		
		if (log2CbSize ==6) {
			struct RU_1 h_41;
			*(( int*) (&h_41)) = 0;
			h_41.header = 0x41;
//				h_41.BSh = BSh[linx0y0]; if (h_41.BSh==3) h_41.BSh=1;
//				h_41.BSv = BSv[linx0y0]; if (h_41.BSv==3) h_41.BSv=1;
			h_41.QP = qPY;
			send_tu_residual_v2(x0,y0,5,0,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);
			fwrite(&h_41,4,1,cabac_to_residual); //fwrite(&h_41,4,1,cu_out);
			send_tu_residual_v2(x0,y0,4,1,35,0); fwrite(&h_40,4,1,cabac_to_residual);// fwrite(&h_40,4,1,cu_out);
			send_tu_residual_v2(x0,y0,4,2,35,0); fwrite(&h_40,4,1,cabac_to_residual);// fwrite(&h_40,4,1,cu_out);

			send_tu_residual_v2(x0+32,y0,5,0,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);
			fwrite(&h_41,4,1,cabac_to_residual); //fwrite(&h_41,4,1,cu_out);
			send_tu_residual_v2(x0+32,y0,4,1,35,0); fwrite(&h_40,4,1,cabac_to_residual);// fwrite(&h_40,4,1,cu_out);
			send_tu_residual_v2(x0+32,y0,4,2,35,0); fwrite(&h_40,4,1,cabac_to_residual);// fwrite(&h_40,4,1,cu_out);

			send_tu_residual_v2(x0,y0+32,5,0,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);
			fwrite(&h_41,4,1,cabac_to_residual); //fwrite(&h_41,4,1,cu_out);
			send_tu_residual_v2(x0,y0+32,4,1,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);
			send_tu_residual_v2(x0,y0+32,4,2,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);

			send_tu_residual_v2(x0+32,y0+32,5,0,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);
			fwrite(&h_41,4,1,cabac_to_residual); //fwrite(&h_41,4,1,cu_out);
			send_tu_residual_v2(x0+32,y0+32,4,1,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);
			send_tu_residual_v2(x0+32,y0+32,4,2,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);

		} else {
			send_tu_residual_v2(x0,y0,log2CbSize,0,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);
			struct RU_1 h_41;
			*(( int*) (&h_41)) = 0;
			h_41.header = 0x41;
//				h_41.BSh = BSh[linx0y0]; if (h_41.BSh==3) h_41.BSh=1;
//				h_41.BSv = BSv[linx0y0]; if (h_41.BSv==3) h_41.BSv=1;
			h_41.QP = qPY;
			fwrite(&h_41,4,1,cabac_to_residual); //fwrite(&h_41,4,1,cu_out);
			send_tu_residual_v2(x0,y0,log2CbSize-1,1,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);
			send_tu_residual_v2(x0,y0,log2CbSize-1,2,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);
		}
		
		for (i=0;i<nCbS;i+=4){
			CuPredMode_buf[((x0+i) & 63)>> 2] = MODE_SKIP;
		}
		for (j=0;j<nCbS;j+=4){
			CuPredMode_buf_left[ ((y0+j) & 63)>> 2] = MODE_SKIP;
		}
		CuPredMode_cur = MODE_SKIP;

	} else {
		
		if (slice_segment_header.slice_type != SLICE_I){
			pred_mode_flag = read_cabac_pred_mode_flag();
			CuPredMode_cur =( pred_mode_flag == -1 ? ( slice_segment_header.slice_type==2 ? MODE_INTRA : MODE_SKIP) : (1-pred_mode_flag) );;

		} else {
			CuPredMode_cur = MODE_INTRA;
		}
		int i,j;

		h_30.predMode = (CuPredMode_cur == MODE_INTER);
		if (CuPredMode_cur==MODE_INTER){
			cu[log2CbSize]++;
		}
		if ( CuPredMode_cur != MODE_INTRA || log2CbSize == sequence_param->Log2MinCbSizeY){
			part_mode = read_cabac_part_mode(log2CbSize,x0,y0);
			if (CuPredMode_cur==MODE_INTRA) {
				PartMode = part_mode;
				if (PartMode==PART_NxN) IntraSplitFlag=1;
			} else {
				PartMode = part_mode;
				IntraSplitFlag=0;
			}
			h_30.PartMode = PartMode;
			//fprintf(stderr,"non I slice_type not supported || not minimum cb size\n");
			// part_mode
		}
		
		if (CuPredMode_cur == MODE_INTRA ){
			if (PartMode==PART_2Nx2N &&  sequence_param->pcm_enabled_flag && log2CbSize >= sequence_param->Log2MinIpcmCbSizeY && log2CbSize <= sequence_param->Log2MaxIpcmCbSizeY){
				pcm_flag = read_cabac_pcm_flag();
				h_30.pcm = pcm_flag;
			}
			fwrite(&h_30,4,1,cabac_to_residual); //fwrite(&h_30,4,1,cu_out);
			if (pcm_flag){
				axi_write(0x820,1);
				if (current->bit != 0){
					byte_alignment();
				}
				int pcm_sample;

				// update IntraPredMode buffers
				

				send_tu_residual_v2(x0,y0,log2CbSize,0,35,1); fwrite(&h_40,4,1,cabac_to_residual);
				struct RU_1 h_41; *(( int*) (&h_41)) = 0;
				h_41.header = 0x41; h_41.QP = qPY;
				fwrite(&h_41,4,1,cabac_to_residual);

				struct coeff coeff_i;
				coeff_i.header = 63;
				for (i=0;i< (1<<log2CbSize);i++) {
					coeff_i.y = i;
					if (i%4==0){
						IntraPredModeY_buf[((x0+i)&63)>>2] = 1; // Intra_DC
						IntraPredModeY_buf_left[((y0+i)&63)>>2] = 1;
					}
					for (j=0;j <(1<<log2CbSize);j++) {
						pcm_sample = read_u(sequence_param->PCMBitDepthY) << (8-sequence_param->PCMBitDepthY);
						coeff_i.x = j;
						coeff_i.val = pcm_sample;
						fwrite(&coeff_i,4,1,cabac_to_residual);
					}
				}

				int cIdx;
				for (cIdx=1;cIdx<=2;cIdx++){
					send_tu_residual_v2(x0,y0,log2CbSize-1,cIdx,35,1); fwrite(&h_40,4,1,cabac_to_residual);
					for (i=0;i< (1<<log2CbSize)/2;i++) {
						coeff_i.y = i;
						for (j=0;j <(1<<log2CbSize)/2;j++) {
							pcm_sample = read_u(sequence_param->PCMBitDepthC) << (8-sequence_param->PCMBitDepthC);
							coeff_i.x = j;
							coeff_i.val = pcm_sample;
							fwrite(&coeff_i,4,1,cabac_to_residual);
						}
					}
				}
				
				init_cabac_engine();
			} else {
				//int pbOffset = (PartMode == PART_NxN) ? nCbS/2:nCbS;
				int pb = PartMode==PART_NxN?2:1;
				int prev_intra_luma_pred_flag[4];
				int mpm_idx[4];
				int rem_intra_luma_pred_mode[4];
				int intra_chroma_pred_mode;
				int i,j;
				for (j=0;j<pb;j++){
					for (i=0;i<pb;i++){
						prev_intra_luma_pred_flag[j*2+i] = read_cabac_prev_intra_luma_pred_flag();
					}
				}
				for (j=0;j<pb;j++){
					for (i=0;i<pb;i++){
						if (prev_intra_luma_pred_flag[j*2+i]){
							mpm_idx[j*2+i] = read_cabac_mpm_idx();
						} else {
							rem_intra_luma_pred_mode[j*2+i] = read_cabac_rem_intra_luma_pred_flag();
						}
					}
				}
				//BYTE luma_mode[4];
				intra_chroma_pred_mode = read_cabac_chroma();
				int off = nCbS/2;
				for (j=0;j<pb;j++){
					for (i=0;i<pb;i++){
						int mode = derive_intra_mode(x0 + i*off, y0+j*off, prev_intra_luma_pred_flag[2*j+i], mpm_idx[2*j+i], rem_intra_luma_pred_mode[2*j+i],intra_chroma_pred_mode);
						IntraPredModeY_cur[j*2+i] = mode;
						if (pb==1) {
							IntraPredModeY_cur[1] = IntraPredModeY_cur[2] = IntraPredModeY_cur[3] = mode;
						}
						int size = pb==1?nCbS : nCbS/2;
						int x,y;
						for (x=0;x<size;x+=4){
							IntraPredModeY_buf[ ((x0+i*off+x) & 63) >> 2] = mode;
							CuPredMode_buf[  ((x0+i*off+x) & 63)>> 2] = MODE_INTRA;
						}
						for (y=0;y<size;y+=4){
							IntraPredModeY_buf_left[ ((y0+j*off+y) & 63) >> 2] = mode;
							CuPredMode_buf_left[ ((y0+j*off+y) & 63)>> 2] = MODE_INTRA;
						}
					}
				}

				int chroma_mode[5][5] = { {34,0,0,0,0}, {26,34,26,26,26}, {10,10,34,10,10}, {1,1,1,34,1}, {0,26,10,1,35} };
				switch (IntraPredModeY_cur[0]){
				case 0:
					IntraPredModeC = chroma_mode[intra_chroma_pred_mode][0];break;
				case 26:
					IntraPredModeC = chroma_mode[intra_chroma_pred_mode][1];break;
				case 10:
					IntraPredModeC = chroma_mode[intra_chroma_pred_mode][2];break;
				case 1:
					IntraPredModeC = chroma_mode[intra_chroma_pred_mode][3];break;
				default:
					IntraPredModeC = chroma_mode[intra_chroma_pred_mode][4];break;
				}
				if (IntraPredModeC==35) IntraPredModeC = IntraPredModeY_cur[0];

				if (cabac_printing)printf("Chroma mode %d\n",IntraPredModeC);

			}
		} else { // inter
			fwrite(&h_30,4,1,cabac_to_residual); //fwrite(&h_30,4,1,cu_out);
			for (i=0;i<nCbS;i+=4){
				//for (j=0;j<nCbS;j++){
					CuPredMode_buf[  ((x0+i) & 63)>> 2] = CuPredMode_cur;
					//CuPredMode_buf_left[ ((y0+j) & 63)>> 2] = CuPredMode_cur;
				//}
			}
			for (j=0;j<nCbS;j+=4){
					//CuPredMode_buf[ (x0+i)>> 2] = CuPredMode_cur;
					CuPredMode_buf_left[ ((y0+j) & 63)>> 2] = CuPredMode_cur;
				}
			//send_cu_inter(x0,y0,log2CbSize,PartMode);
			if (PartMode == PART_2Nx2N){
				prediction_unit(x0,y0,nCbS,nCbS,0);
			} else if (PartMode == PART_2NxN){
				prediction_unit(x0,y0,nCbS,nCbS/2,0);
				prediction_unit(x0,y0+nCbS/2,nCbS,nCbS/2,1);
			} else if (PartMode == PART_Nx2N){
				prediction_unit(x0,y0,nCbS/2,nCbS,0);
				prediction_unit(x0+nCbS/2,y0,nCbS/2,nCbS,1);
			} else if (PartMode == PART_2NxnU){
				prediction_unit(x0,y0,nCbS,nCbS/4,0);
				prediction_unit(x0,y0+nCbS/4,nCbS,nCbS*3/4,1);
			} else if (PartMode == PART_2NxnD){
				prediction_unit(x0,y0,nCbS,nCbS*3/4,0);
				prediction_unit(x0,y0+nCbS*3/4,nCbS,nCbS/4,1);
			} else if (PartMode == PART_nLx2N){
				prediction_unit(x0,y0,nCbS/4,nCbS,0);
				prediction_unit(x0+nCbS/4,y0,nCbS*3/4,nCbS,1);
			} else if (PartMode == PART_nRx2N){
				prediction_unit(x0,y0,nCbS*3/4,nCbS,0);
				prediction_unit(x0+nCbS*3/4,y0,nCbS/4,nCbS,1);
			} else {
				prediction_unit(x0,y0,nCbS/2,nCbS/2,0);
				prediction_unit(x0+nCbS/2,y0,nCbS/2,nCbS/2,1);
				prediction_unit(x0,y0+nCbS/2,nCbS/2,nCbS/2,2);
				prediction_unit(x0+nCbS/2,y0+nCbS/2,nCbS/2,nCbS/2,3);
			}
//			fprintf(stderr,"non I slice_type not supported\n"); exit(0);
		}
		int cu_out_write = IntraSplitFlag << 30 | IntraPredModeC << 24 | IntraPredModeY_cur[0] << 18 | IntraPredModeY_cur[1] << 12 | IntraPredModeY_cur[2] << 6 |IntraPredModeY_cur[3];
		//fwrite(&cu_out_write,4,1,cu_out);
		if (!pcm_flag){ // !pcm_flag
			int rqt_root_cbf = 1;
			if (CuPredMode_cur != MODE_INTRA && !(PartMode == PART_2Nx2N && merge_flag)) {
				rqt_root_cbf = read_cabac_rqt_root_cbf();
//				fprintf(stderr,"non I slice_type not supported\n");
			}
			//send_cu_residual(x0,y0,log2CbSize,rqt_root_cbf,cu_transquant_bypass_flag,CuPredMode[linx0y0] == MODE_INTRA);
			if (rqt_root_cbf){
				sequence_param->MaxTrafoDepth = (CuPredMode_cur==MODE_INTRA ? sequence_param->max_transform_hierarchy_depth_intra + IntraSplitFlag : sequence_param->max_transform_hierarchy_depth_inter);
				int interSplitFlag = (sequence_param->max_transform_hierarchy_depth_inter == 0) & (CuPredMode_cur == MODE_INTER) & (PartMode != PART_2Nx2N);
				transform_tree(x0,y0,x0,y0,log2CbSize,0,0,interSplitFlag);
			} else {
				for (i=0;i<(1<<(log2CbSize-3));i+=1) {
					qPY_left[y+i] = qPY;
					qpY_top[x+i] = qPY;
				}
				if (log2CbSize ==6) {
					struct RU_1 h_41;
					*(( int*) (&h_41)) = 0;
					h_41.header = 0x41;
					h_41.QP = qPY;
					send_tu_residual_v2(x0,y0,5,0,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);
					fwrite(&h_41,4,1,cabac_to_residual); //fwrite(&h_41,4,1,cu_out);
					send_tu_residual_v2(x0,y0,4,1,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);
					send_tu_residual_v2(x0,y0,4,2,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);

					send_tu_residual_v2(x0+32,y0,5,0,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);
					fwrite(&h_41,4,1,cabac_to_residual); //fwrite(&h_41,4,1,cu_out);
					send_tu_residual_v2(x0+32,y0,4,1,35,0); fwrite(&h_40,4,1,cabac_to_residual);// fwrite(&h_40,4,1,cu_out);
					send_tu_residual_v2(x0+32,y0,4,2,35,0); fwrite(&h_40,4,1,cabac_to_residual);// fwrite(&h_40,4,1,cu_out);

					send_tu_residual_v2(x0,y0+32,5,0,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);
					fwrite(&h_41,4,1,cabac_to_residual); //fwrite(&h_41,4,1,cu_out);
					send_tu_residual_v2(x0,y0+32,4,1,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);
					send_tu_residual_v2(x0,y0+32,4,2,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);

					send_tu_residual_v2(x0+32,y0+32,5,0,35,0); fwrite(&h_40,4,1,cabac_to_residual);// fwrite(&h_40,4,1,cu_out);
					fwrite(&h_41,4,1,cabac_to_residual);// fwrite(&h_41,4,1,cu_out);
					send_tu_residual_v2(x0+32,y0+32,4,1,35,0); fwrite(&h_40,4,1,cabac_to_residual);// fwrite(&h_40,4,1,cu_out);
					send_tu_residual_v2(x0+32,y0+32,4,2,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);

				} else {
					send_tu_residual_v2(x0,y0,log2CbSize,0,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);
					struct RU_1 h_41;
					*(( int*) (&h_41)) = 0;
					h_41.header = 0x41;
					h_41.QP = qPY;
					fwrite(&h_41,4,1,cabac_to_residual); //fwrite(&h_41,4,1,cu_out);
					send_tu_residual_v2(x0,y0,log2CbSize-1,1,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);
					send_tu_residual_v2(x0,y0,log2CbSize-1,2,35,0); fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);
				}
				
			}

		}
	}
	if (cabac_printing)printf("\n");
}
void transform_tree(int x0, int y0, int xBase, int yBase, int log2TrafoSize, int trafoDepth, int blkIdx, int interSplitFlag){
	if (cabac_printing) printf("Transform Tree %d %d %d\n",x0,y0,log2TrafoSize);
	int split_transform_flag;
	cabac_trafoSize = log2TrafoSize;
	cabac_trafoDepth = trafoDepth;
	int linC = y0*sequence_param->pic_width_in_luma_samples+x0;
	int linB = yBase*sequence_param->pic_width_in_luma_samples+xBase;

	if (log2TrafoSize > sequence_param->Log2MaxTrafoSize) split_transform_flag = 1;
	else if (IntraSplitFlag==1 && trafoDepth==0) split_transform_flag = 1;
	else if (interSplitFlag) split_transform_flag = 1;
	else if (log2TrafoSize<= sequence_param->Log2MinTrafoSize) split_transform_flag =0;
	else if (trafoDepth >= sequence_param->MaxTrafoDepth) split_transform_flag = 0;
	else {
		split_transform_flag = read_cabac_split_transform_flag();
	}
	if (log2TrafoSize>2){
		if (trafoDepth==0 || cbf_cb_B[trafoDepth-1]){
			//cbf_cb[trafoDepth][linC] = read_cabac_cbf_cx();
			cbf_cb_0[trafoDepth] = read_cabac_cbf_cx();
		} else {
			cbf_cb_0[trafoDepth]=0;
		}
		if (trafoDepth==0 || cbf_cr_B[trafoDepth-1]){
			//cbf_cr[trafoDepth][linC] = read_cabac_cbf_cx();
			cbf_cr_0[trafoDepth] = read_cabac_cbf_cx();
		} else {
			cbf_cr_0[trafoDepth]=0;
		}
	} else {
		if (trafoDepth>0){
			//cbf_cb[trafoDepth][linC] = cbf_cb_B[trafoDepth-1];
			cbf_cb_0[trafoDepth] = cbf_cb_B[trafoDepth-1];
		} else {
			cbf_cb_0[trafoDepth]=0;
		}
		if (trafoDepth>0){
			//cbf_cr[trafoDepth][linC] = cbf_cr[trafoDepth-1][linB];
			cbf_cr_0[trafoDepth] = cbf_cr_B[trafoDepth-1];
		} else {
			cbf_cr_0[trafoDepth]=0;
		}
	}
	cbf_cb_B[trafoDepth]  = cbf_cb_0[trafoDepth];
	cbf_cr_B[trafoDepth]  = cbf_cr_0[trafoDepth];
	if (split_transform_flag){
		//for (int k=0;k<4;k++)
			
		int x1 = x0 + ((1<<log2TrafoSize)>>1);
		int y1 = y0 + ((1<<log2TrafoSize)>>1);
		transform_tree(x0,y0,x0,y0,log2TrafoSize-1,trafoDepth+1,0,0);
		transform_tree(x1,y0,x0,y0,log2TrafoSize-1,trafoDepth+1,1,0);
		transform_tree(x0,y1,x0,y0,log2TrafoSize-1,trafoDepth+1,2,0);
		transform_tree(x1,y1,x0,y0,log2TrafoSize-1,trafoDepth+1,3,0);
	} else {
		if (CuPredMode_cur==MODE_INTRA || trafoDepth !=0 || cbf_cb_0[trafoDepth] || cbf_cr_0[trafoDepth] ){
			//cbf_luma[trafoDepth][linC] = read_cabac_cbf_luma();
			cbf_luma_0[trafoDepth] = read_cabac_cbf_luma();
		} else {
			//cbf_luma[trafoDepth][linC] = 1;
			cbf_luma_0[trafoDepth] = 1;
		}
		transform_unit(x0,y0,xBase,yBase,log2TrafoSize,trafoDepth,blkIdx);
	}


}
void transform_unit(int x0, int y0, int xBase, int yBase, int log2TrafoSize, int trafoDepth, int blkIdx){
	if (cabac_printing) printf("Transform Unit %d %d %d\n",x0,y0,log2TrafoSize);
	int linC = y0*sequence_param->pic_width_in_luma_samples+x0;
	int linB = yBase*sequence_param->pic_width_in_luma_samples+xBase;
	
	 if (picture_param->cu_qp_delta_enabled_flag){
		 // TODO
		// fprintf(stderr,"cu_qp_delta_enabled\n"); exit(0);
	 }
	 int nonZero = 0;
	 int cu_qp_delta_abs =0;
	 int cu_qp_delta_sign = 0;
	if (cbf_luma_0[trafoDepth] || cbf_cb_0[trafoDepth] || cbf_cr_0[trafoDepth]){
		if (picture_param->cu_qp_delta_enabled_flag & !IsCuQpDeltaCoded){
			IsCuQpDeltaCoded = 1;
			cu_qp_delta_abs = read_cabac_cu_qp_delta_abs();
			if (cu_qp_delta_abs)
				cu_qp_delta_sign = read_cabac_cu_qp_delta_sign();
			int i;

			int log2Cu = log2TrafoSize+trafoDepth;
			int xC = (x0 >>log2Cu ) << log2Cu;
			int yC = (y0 >>log2Cu ) << log2Cu;
			int x,y;
			switch (sequence_param->Log2CtbSizeY) {
				case 6 :   x = (xC & 63) >>3; y = (yC & 63) >>3; break;
				case 5 :   x = (xC & 31) >>3; y = (yC & 31) >>3; break;
				case 4 :   x = (xC & 15) >>3; y = (yC & 15) >>3; break;
			}
			//int qPy_pred = (1+ (x==0? qPY : qPY_left[y]) + (y==0 ? qPY : qpY_top[x]))/2;
			qPY = qPY+ cu_qp_delta_abs*(1-2*cu_qp_delta_sign);

			for (i=0;i<( 1<<(log2TrafoSize+trafoDepth-3));i+=1) {
				qPY_left[y+i] = qPY;
				qpY_top[x+i] = qPY;
			}
		}
	} 
	

	int send_mode = IntraPredModeY_cur[ ((y0>>(sequence_param->Log2MinCbSizeY-1))&1)*2 + ((x0>>(sequence_param->Log2MinCbSizeY-1))&1)];

	send_tu_residual_v2(x0,y0,log2TrafoSize,0,send_mode,cbf_luma_0[trafoDepth]);
	
	if (cbf_luma_0[trafoDepth]){
		nonZero = residual_coding(x0,y0,log2TrafoSize,0);
	} else {
		fwrite(&h_40,4,1,cabac_to_residual);// fwrite(&h_40,4,1,cu_out);
		struct RU_1 h_41;
		*(( int*) (&h_41)) = 0;
		h_41.header = 0x41;
		h_41.QP = qPY;
		fwrite(&h_41,4,1,cabac_to_residual); //fwrite(&h_41,4,1,cu_out);
	}
	
		 int nT = 1<<log2TrafoSize;

	if (log2TrafoSize>2){
		send_tu_residual_v2(x0,y0,log2TrafoSize-1,1,IntraPredModeC,cbf_cb_0[trafoDepth]);
		if (cbf_cb_0[trafoDepth]){
			residual_coding(x0,y0,log2TrafoSize-1,1);
		} else {
			fwrite(&h_40,4,1,cabac_to_residual);// fwrite(&h_40,4,1,cu_out);
		}
		send_tu_residual_v2(x0,y0,log2TrafoSize-1,2,IntraPredModeC,cbf_cr_0[trafoDepth]);
		if (cbf_cr_0[trafoDepth]){
			residual_coding(x0,y0,log2TrafoSize-1,2);
		} else {
			fwrite(&h_40,4,1,cabac_to_residual);// fwrite(&h_40,4,1,cu_out);
		}
	} else if (blkIdx==3){
		send_tu_residual_v2(xBase,yBase,log2TrafoSize,1,IntraPredModeC,cbf_cb_B[trafoDepth]);
		if (cbf_cb_B[trafoDepth]){
			residual_coding(xBase,yBase,log2TrafoSize,1);
		} else {
			fwrite(&h_40,4,1,cabac_to_residual);// fwrite(&h_40,4,1,cu_out);
		}
		send_tu_residual_v2(xBase,yBase,log2TrafoSize,2,IntraPredModeC,cbf_cr_B[trafoDepth]);
		if (cbf_cr_B[trafoDepth]){
			residual_coding(xBase,yBase,log2TrafoSize,2);
		} else {
			fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out);
		}
	}



}
void prediction_unit(int x0, int y0, int nPbW, int nPbH, int partIdx){
	// TODO set BS
	merge_flag=0;
	int merge_send = 0;
	if (cabac_printing) printf("Prediction Unit %d %d %d %d\n",x0, y0, nPbW, nPbH);
	int linx0y0 = y0*sequence_param->pic_width_in_luma_samples + x0;
	int merge_idx = 0;
	int inter_pred_idc = PRED_L0;
	int ref_idx_l0=0, ref_idx_l1=0;
	int mvp_l0_flag=0, mvp_l1_flag=0;
	unsigned int mvd_l0 = 0, mvd_l1= 0;
	if (cu_skip_flag_cur){
		merge_send = 1;
		if (MaxNumMergeCand>1){
			merge_idx = read_cabac_merge_idx(MaxNumMergeCand,1);
		}
//		fprintf(stderr,"cu_skip_flag not supported\n"); exit(0);
	} else {
		merge_flag= read_cabac_merge_flag();
		merge_send = merge_flag;
		if (merge_flag){
			if (MaxNumMergeCand>1)
				merge_idx = read_cabac_merge_idx(MaxNumMergeCand,0); // merge_idx
		} else {
			if (slice_segment_header.slice_type == SLICE_B){
				inter_pred_idc = read_cabac_inter_pred_idc(x0,y0,nPbW,nPbH);
			}
			if (inter_pred_idc != PRED_L1){
				if (slice_segment_header.num_ref_idx_l0_active_minus1>0){
					ref_idx_l0 = read_cabac_ref_idx_l0(slice_segment_header.num_ref_idx_l0_active_minus1);
				}
				mvd_l0 = mvd_coding(x0,y0,0);
				mvp_l0_flag = read_cabac_mvp_lx_flag(); // mvp_l0
			}
			if (inter_pred_idc != PRED_L0){
				if (slice_segment_header.num_ref_idx_l1_active_minus1>0){
					ref_idx_l1 =  read_cabac_ref_idx_l1(slice_segment_header.num_ref_idx_l1_active_minus1);
				}
				if (slice_segment_header.mvd_l1_zero_flag && inter_pred_idc==PRED_BI){

				} else {
					mvd_l1 = mvd_coding(x0,y0,1);
				}
				mvp_l1_flag = read_cabac_mvp_lx_flag(); // mvp_l1
			}
		}
	}
	
	struct PU_0 h_50;
	(*(int *) (&h_50)) = 0;
	h_50.header = 0x50;
	h_50.part_idx = partIdx;
	h_50.pred_idc = inter_pred_idc;
	h_50.merge_flag = merge_send;
	h_50.merge_idx = merge_idx;
	h_50.mvp_l0_flag = mvp_l0_flag;
	h_50.mvp_l1_flag = mvp_l1_flag;
	h_50.ref_idx_l0 = ref_idx_l0;
	h_50.ref_idx_l1 = ref_idx_l1;
//	unsigned int to_inter = 0xdd<<24;
//	to_inter |= (partIdx<<22);
//	to_inter |= (inter_pred_idc << 20);
//	to_inter |= (merge_send<<19);
//	to_inter |= (merge_idx <<16);
//	to_inter |= (mvp_l0_flag << 15);
//	to_inter |= (mvp_l1_flag << 14);
//	to_inter |= (ref_idx_l0 << 10);
//	to_inter |= (ref_idx_l1 << 6);
//	fwrite(&to_inter,4,1,cabac_to_inter);
	fwrite(&h_50,4,1,cabac_to_residual); //fwrite(&h_50,4,1,cu_out);
	if (merge_send ==0){
		// do not send mvd in merge mode
		fwrite(&mvd_l0,4,1,cabac_to_residual);// fwrite(&mvd_l0,4,1,cu_out);
		fwrite(&mvd_l1,4,1,cabac_to_residual);// fwrite(&mvd_l1,4,1,cu_out);
	}
//	printf("end of parsing\n"); exit(0);
}
unsigned int mvd_coding(int x0, int y0, int refList){

	if (cabac_printing) printf("  mvd %d %d %d\n",x0,y0,refList);
	int abs_mvd_greater0_flag[2];
	int abs_mvd_greater1_flag[2] = {0,0};
	int abs_mvd_minus2[2] = {0,0};
	int mvd_sign_flag[2] = {0,0};
	abs_mvd_greater0_flag[0] = read_cabac_abs_mvd_greater0_flag();
	abs_mvd_greater0_flag[1] = read_cabac_abs_mvd_greater0_flag();
	if (abs_mvd_greater0_flag[0]){
		abs_mvd_greater1_flag[0] = read_cabac_abs_mvd_greater1_flag();
	}
	if (abs_mvd_greater0_flag[1]){
		abs_mvd_greater1_flag[1] = read_cabac_abs_mvd_greater1_flag();
	}
	if (abs_mvd_greater0_flag[0]){
		if (abs_mvd_greater1_flag[0]){
			abs_mvd_minus2[0] = read_cabac_abs_mvd_minus2();
		}
		mvd_sign_flag[0] = read_cabac_mvd_sign_flag();
	}
	if (abs_mvd_greater0_flag[1]){
		if (abs_mvd_greater1_flag[1]){
			abs_mvd_minus2[1] = read_cabac_abs_mvd_minus2();
		}
		mvd_sign_flag[1] = read_cabac_mvd_sign_flag();
	}
	short mvd[2];
	int i;
	for (i=0;i<2;i++){
		mvd[i] = 0;
		if (abs_mvd_greater0_flag[i]>0){
			if (abs_mvd_greater1_flag[i]){
				mvd[i] = abs_mvd_minus2[i] +2;
			} else {
				mvd[i] = 1;
			}
		}
		if (mvd_sign_flag[i]) mvd[i] *= -1;
	}
	//printf("%d %d \n",mvd[0], mvd[1]);
	return *(unsigned int *)mvd;
}
int in_res = 0;
int residual_coding(int x0, int y0, int log2TrafoSize, int cIdx){ // returns if one or more non zero transcoeff
	int nonZero = 0;
	tot_res += (1<<log2TrafoSize)*(1<<log2TrafoSize);
	cycles1 += (1<<log2TrafoSize)*(1<<log2TrafoSize)/4; // for row
	cycles2 +=  (1<<log2TrafoSize)*2;
	switch (log2TrafoSize) {
		case 2:
			cycles4 += 8; break;
		case 3:
			cycles4 += 24; break;
		case 4:
			cycles4 += 128; break;
		case 5:
			cycles4 += 768; break;
	}
	in_res = 1;
	if (cabac_printing)printf("Residual (%d,%d) size %d, cIdx %d\n",x0,y0,log2TrafoSize,cIdx);
	short TransCoeffLevel[32][32];
	int u; int v;
	for (u=0;u<32;u++){
		for (v=0;v<32;v++){
			TransCoeffLevel[u][v] = 0;
		}
	}
	memset(TransCoeffLevel,0,(1<<log2TrafoSize)*(1<<log2TrafoSize)*sizeof(short));
	int linC = y0*sequence_param->pic_width_in_luma_samples+x0;
	memset(coded_sub_block_flag,0,sizeof(int)*81);
	memset(significant_coeff_flag,0,sizeof(int)*32*32);
	memset(cabac_greater1_invoked,0,sizeof(int)*64);
	int scanIdx;
	if (CuPredMode_cur==MODE_INTRA && (log2TrafoSize==2 || (log2TrafoSize ==3 && cIdx==0))){
		//int predModeIntra = cIdx==0? IntraPredModeY[linC]: IntraPredModeC;
		int predModeIntra;
		if (log2TrafoSize==2)
			
			if (sequence_param->Log2MinCbSizeY==4) 
				predModeIntra= cIdx==0? IntraPredModeY_cur[ ((y0&8)? 1 :0)*2  + ((x0&8)? 1 :0)]: IntraPredModeC;
			else
				predModeIntra= cIdx==0? IntraPredModeY_cur[ ((y0&4)? 1 :0)*2  + ((x0&4)? 1 :0)]: IntraPredModeC;
		else 
			// consider 2nd bit from lsb
			predModeIntra = IntraPredModeY_cur[ ((y0>>3)&1)*2 + ((x0>>3)&1)];

		if (predModeIntra >=6 && predModeIntra <=14 ) scanIdx =2;
		else if (predModeIntra >=22 && predModeIntra <=30 ) scanIdx =1;
		else scanIdx=0;
	} else scanIdx=0;
	if (cabac_printing) printf("  scanIdx %d\n",scanIdx);
	int transform_skip_flag = 0;
	if (picture_param->transform_skip_enabled_flag && !0 && (log2TrafoSize==2) ){
		transform_skip_flag = read_cabac_transform_skip_flag(cIdx);
	}
	h_40.transform_skip_flag = transform_skip_flag;
	fwrite(&h_40,4,1,cabac_to_residual); //fwrite(&h_40,4,1,cu_out); 
	struct RU_1 h_41;
	*(( int*) (&h_41)) = 0;
	h_41.header = 0x41;
	h_41.QP = qPY;
	if (cIdx==0){ fwrite(&h_41,4,1,cabac_to_residual);// fwrite(&h_41,4,1,cu_out);
	}
	//send_tu_residual(x0-global_xBase, y0-global_yBase,cIdx,log2TrafoSize,global_qP,transform_skip_flag,CuPredMode[linC]==MODE_INTRA);

	cabac_print_enable = 1;
	int header = 254 << 24 | x0 << 12 | y0 ;
		//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	header = 255 << 24 | picture_param->sign_data_hiding_flag << 8  |cIdx << 5 | scanIdx << 3 | log2TrafoSize; // cu_transquant_bypass <<7
		//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	cabac_cIdx = cIdx;
	cabac_trafoSize = log2TrafoSize;
	int last_significant_coeff_x_prefix = read_cabac_last_significant_coef_x_prefix(log2TrafoSize);
	int last_significant_coeff_y_prefix = read_cabac_last_significant_coef_y_prefix(log2TrafoSize);
	int last_significat_coeff_x_suffix,last_significat_coeff_y_suffix;
	int LastSignificantCoeffX = last_significant_coeff_x_prefix;
	int LastSignificantCoeffY = last_significant_coeff_y_prefix;
	if (last_significant_coeff_x_prefix>3){
		last_significat_coeff_x_suffix = read_cabac_last_significant_coef_x_suffix(last_significant_coeff_x_prefix);
		LastSignificantCoeffX = (1<<((last_significant_coeff_x_prefix>>1)-1))*(2+(last_significant_coeff_x_prefix&1))+last_significat_coeff_x_suffix;
	}
	if (last_significant_coeff_y_prefix>3){
		last_significat_coeff_y_suffix = read_cabac_last_significant_coef_y_suffix(last_significant_coeff_y_prefix);
		LastSignificantCoeffY = (1<<((last_significant_coeff_y_prefix>>1)-1))*(2+(last_significant_coeff_y_prefix&1))+last_significat_coeff_y_suffix;
	}
	if (scanIdx==2){
		int temp = LastSignificantCoeffX;
		LastSignificantCoeffX = LastSignificantCoeffY;
		LastSignificantCoeffY = temp;
	}

	int lastScanPos = 16;
	int lastSubBlock = (1<<(log2TrafoSize-2))*(1<<(log2TrafoSize-2))-1;
	int xC, yC ,xS, yS;
	do {
		if (lastScanPos==0){
			lastScanPos=16;
			lastSubBlock--;
		}
		lastScanPos--;
		xS = ScanOrder[log2TrafoSize-2][scanIdx][lastSubBlock][0];
		yS = ScanOrder[log2TrafoSize-2][scanIdx][lastSubBlock][1];
		xC = (xS<<2) + ScanOrder[2][scanIdx][lastScanPos][0];
		yC = (yS<<2) + ScanOrder[2][scanIdx][lastScanPos][1];
	} while(xC != LastSignificantCoeffX || yC != LastSignificantCoeffY);
	int i;
	coded_sub_block_flag[0][0] = 1;
	coded_sub_block_flag[LastSignificantCoeffX>>2][LastSignificantCoeffY>>2] = 1;
	significant_coeff_flag[LastSignificantCoeffX][LastSignificantCoeffY]=1;
	int greater1_invoked = 0;
	int coeff_in_column[32] = {0};
	for (i=lastSubBlock;i>=0;i--){
		memset(coeff_abs_greater1_flag,0,sizeof(int)*16);
		memset(coeff_abs_greater2_flag,0,sizeof(int)*16);
		memset(coeff_sign_flag,0,sizeof(int)*16);
		memset(coeff_abs_level_remaining,0,sizeof(int)*16);
		memset(AbsCoeff,0,sizeof(int)*16);
		xS = ScanOrder[log2TrafoSize-2][scanIdx][i][0];
		yS = ScanOrder[log2TrafoSize-2][scanIdx][i][1];
		int inferSbDcSigCoeffFlag = 0;
		if ( (i<lastSubBlock) &&  (i>0) ){
			//fprintf(stderr,"coded sub block flag not supported\n");
			cabac_cIdx = cIdx; cabac_trafoSize = log2TrafoSize;
			coded_sub_block_flag[xS][yS] = read_cabac_coded_sub_block(xS,yS,cIdx,log2TrafoSize);
			inferSbDcSigCoeffFlag = 1;
		}
		if (coded_sub_block_flag[xS][yS]) tot_sub_blocks++;
		int n;
		for (n=(i==lastSubBlock)?lastScanPos-1:15;n>=0;n--){
			xC = (xS<<2) + ScanOrder[2][scanIdx][n][0];
			yC = (yS<<2) + ScanOrder[2][scanIdx][n][1];
			if (coded_sub_block_flag[xS][yS] && (n>0 || !inferSbDcSigCoeffFlag)){
				cabac_cIdx = cIdx;
				cabac_xC = xC; cabac_yC = yC;
				cabac_scanIdx = scanIdx;
				cabac_trafoSize = log2TrafoSize;
				if (cabac_printing) printf("Sig coef read %d %d ",xC,yC);
				significant_coeff_flag[xC][yC] = read_cabac_significant_coeff_flag();
				if (significant_coeff_flag[xC][yC]) inferSbDcSigCoeffFlag =0;
			} else if (coded_sub_block_flag[xS][yS] && n==0 && inferSbDcSigCoeffFlag){
				significant_coeff_flag[xC][yC] = 1;
			}
		}
		int firstSigScanPos = 16;
		int lastSigScanPos = -1;
		int numGreater1Flag = 0;
		int lastGreater1ScanPos = -1;
		cabac_greater1_first_1_met=0;
		for (n=15;n>=0;n--){
			xC = (xS<<2) + ScanOrder[2][scanIdx][n][0];
			yC = (yS<<2) + ScanOrder[2][scanIdx][n][1];
			if (significant_coeff_flag[xC][yC]){
				if (cabac_printing)printf(" Significant at (%d,%d)\n",xC, yC);
				if (numGreater1Flag<8){
					coeff_abs_greater1_flag[n] = read_cabac_greater1_v2(cIdx,i,n,!greater1_invoked);
					numGreater1Flag++;
					greater1_invoked = 1;
					if (coeff_abs_greater1_flag[n] && lastGreater1ScanPos==-1)
						lastGreater1ScanPos = n;
				}
				if (lastSigScanPos == -1) lastSigScanPos = n;
				firstSigScanPos = n;
			}
		}
		int signHidden = (lastSigScanPos-firstSigScanPos>3 && !0);
		if (lastGreater1ScanPos != -1){
			cabac_cIdx = cIdx;
			coeff_abs_greater2_flag[lastGreater1ScanPos] = read_cabac_coeff_abs_level_greater2_flag();
		}
		for (n=15;n>=0;n--){
			xC = (xS<<2) + ScanOrder[2][scanIdx][n][0];
			yC = (yS<<2) + ScanOrder[2][scanIdx][n][1];
			if (significant_coeff_flag[xC][yC] &&
					(!picture_param->sign_data_hiding_flag || !signHidden || n != firstSigScanPos)){
						coeff_sign_flag[n] = read_cabac_coeff_sign_flag();
			}
		}
		int numSigCoeff = 0;
		int sumAbsLevel = 0;
		cabac_cLastRiceParam = 0;
		cNextRiceParam = 0;
		struct coeff coeff_i;
		coeff_i.header = 63;
		
		

		for (n=15;n>=0;n--){
			xC = (xS<<2) + ScanOrder[2][scanIdx][n][0];
			yC = (yS<<2) + ScanOrder[2][scanIdx][n][1];

			if (significant_coeff_flag[xC][yC]){
				int baseLevel = 1+coeff_abs_greater1_flag[n]+coeff_abs_greater2_flag[n];
				if (baseLevel == ((numSigCoeff<8)? ((n==lastGreater1ScanPos)?3 :2):1)){
					coeff_abs_level_remaining[n] = read_cabac_coeff_abs_level_remaining(n,baseLevel);
				}
				AbsCoeff[n] = baseLevel + coeff_abs_level_remaining[n];
				if (AbsCoeff[n]>0) nonZero = 1;
				TransCoeffLevel[xC][ yC] = AbsCoeff[n]*(1-2*coeff_sign_flag[n]); // yC and xC swapped around
				if (picture_param->sign_data_hiding_flag && signHidden){
					sumAbsLevel += AbsCoeff[n];
					if (n==firstSigScanPos && ((sumAbsLevel%2)==1)){
						TransCoeffLevel[xC][ yC] = - TransCoeffLevel[xC][ yC];
					}
				}
				coeff_i.x = xC;
				coeff_i.y = yC;
				coeff_i.val = TransCoeffLevel[xC][yC];
				fwrite(&coeff_i,4,1,cabac_to_residual);
				// write sigcoeff

				numSigCoeff++;

				coeff_in_column[yC]++;

			}
		}

	}
	coeff eRU;
	// End of RU
	eRU.header = 62;
	eRU.x = 0;
	eRU.y = 0;
	eRU.val = 0;
	fwrite(&eRU,4,1,cabac_to_residual);

	for (i=0;i<32;i++){
		if (coeff_in_column[i] >0){
			cycles1 += (1<<log2TrafoSize)/4;
			cycles3 += coeff_in_column[i] + (1<<log2TrafoSize);
		}
	}
	cycles3 += 0; // 3 for accurate estimation
	// send to residual
	int j;
	
//	fclose(cabac_to_residual);
//	fflush(cabac_to_residual);
	in_res=0;
	cabac_print_enable = 1;
	return nonZero;
}

void profile_tier_level(profile_tier_level_struct* ptl, int profilePresentFlag, int maxNumSubLayersMinus1){
	int i;
	ptl->general_profile_space = read_u(2);
	ptl->general_tier_flag = read_u(1);
	ptl->general_profile_idc = read_u(5);
	ptl->general_profile_compatibility = read_u(8)<<24 | read_u(8)<<16 | read_u(8)<<8 | read_u(8);
	for (i=0;i<6;i++)
		read_u(8);
	ptl->general_level_idc = read_u(8);
	
	for (i=0;i<maxNumSubLayersMinus1;i++){
		fprintf(stderr,"Error: Profile Tier Level\n"); exit(0);
	}
}
void bit_rate_pic_rate_info(void* brpr,int TempLevelLow, int TempLevelHigh ){
	int i;
	for (i=TempLevelLow;i<=TempLevelHigh;i++){
		int bit_rate_info_present_flag = read_u(1);
		int pic_rate_info_present_flag = read_u(1);
		if (bit_rate_info_present_flag){
			fprintf(stderr,"Error: Bit Rate Info\n"); exit(0);
		}
		if (pic_rate_info_present_flag){
			fprintf(stderr,"Error: Pic Rate Info\n");
		}
	}

}

void scaling_list_data(){
	// NOTE: DIFFERENT FROM SPEC
	// when scaling list is predicted, spec explicitly does not define dc_coeff prediction method (with the default value of 16 being implied)
	// This implementation assumes that dc_coeff is predicted from refMatrixId. 
	int sizeId, matrixId,i;
	int ScalingList[6][64]; // [sizeId not needed since no referencing across sizes]
	for (sizeId =0; sizeId<4; sizeId++) {
		int scaling_list_dc_coeff_minus8 [6] ;
		for (matrixId = 0; matrixId < (( sizeId==3) ? 2 : 6); matrixId++){
			int scaling_list_pred_mode_flag =  read_u(1);
			if (!scaling_list_pred_mode_flag) {
				int scaling_list_pred_matrix_id_delta = read_ue();
				if (scaling_list_pred_matrix_id_delta ==0) {
					// assign Default Scaling List 
					int j;
					if (sizeId ==0 ) {
						for (j=0;j<16;j++) {
							ScalingList[matrixId][j] = 16;
						}
					} else {
						for (j=0;j<64;j++) {
							if ( (sizeId < 3 & matrixId < 3) || (sizeId == 3 & matrixId == 0) ){
								ScalingList[matrixId][j] = ReferenceScalingList_012[j];
							} else {
								ScalingList[matrixId][j] = ReferenceScalingList_345[j];
							}
						}
					}
				} else {
					int j;
					int refMatrixId = matrixId-scaling_list_pred_matrix_id_delta;
					for (j=0;j< (sizeId == 0 ? 16 :64) ;j++){
						ScalingList[matrixId][j] = ScalingList[refMatrixId][j];
					}
					scaling_list_dc_coeff_minus8[matrixId] = scaling_list_dc_coeff_minus8[refMatrixId];
				}
				// send them down
				struct scaling_list_pkt sl;
				sl.header = 0x05;
				sl.sizeId = sizeId;
				sl.matrixId = matrixId;
				for (i=0;i< (sizeId == 0 ? 16 :64) ;i++){
					sl.i = i;
					sl.val = ScalingList[matrixId][i];
					fwrite(((void *)&sl),4,1,cabac_to_residual);
				}
				if (sizeId>1) {
					sl.i = 0xff;
					sl.val = scaling_list_dc_coeff_minus8[matrixId]+8;
					fwrite(((void *)&sl),4,1,cabac_to_residual);
				}
			} else {
				int nextCoef = 8;
				int coefNum = sizeId == 0 ? 16 : 64;
				struct scaling_list_pkt sl;
				sl.header = 0x05;
				sl.sizeId = sizeId;
				sl.matrixId = matrixId;
				if (sizeId>1) {
					scaling_list_dc_coeff_minus8[matrixId] = read_se();
					nextCoef = scaling_list_dc_coeff_minus8[matrixId] +8;
					sl.i = 0xff;
					sl.val = nextCoef;
					fwrite(((void *)&sl),4,1,cabac_to_residual);
				}
				
				for (i=0;i<coefNum;i++){
					int scaling_list_delta_coef = read_se();
					nextCoef = (nextCoef + scaling_list_delta_coef + 256) % 256;
					ScalingList[matrixId][i] = nextCoef;
					sl.i = i;
					sl.val = ScalingList[matrixId][i];
					fwrite(((void *)&sl),4,1,cabac_to_residual);
				}
			}
		}
	}
}

void pred_weight_table(){

	int luma_weight_l0_flag[16];
	int chroma_weight_l0_flag[16];
	int luma_weight_l1_flag[16];
	int chroma_weight_l1_flag[16];

	int luma_log2_weight_denom;
	int delta_chroma_log2_weight_denom;

	luma_log2_weight_denom = read_ue();
	if (sequence_param->chroma_format_idc != 0)
		delta_chroma_log2_weight_denom =read_se();

	struct pred_weight_1 pw1;
	*((int*) &pw1) = 0;
	pw1.header = 0x18;
	pw1.luma_log2_weight_denom = luma_log2_weight_denom;
	pw1.ChromaLog2WeightDenom = luma_log2_weight_denom + delta_chroma_log2_weight_denom;
	fwrite(((void *)&pw1),4,1,cabac_to_residual);  axi_write(0x0500,(void *) &pw1);

	int i;
	for (i=0;i<=slice_segment_header.num_ref_idx_l0_active_minus1;i++)
		luma_weight_l0_flag[i] = read_u(1);
	if (sequence_param->chroma_format_idc != 0) {
		for (i=0;i<=slice_segment_header.num_ref_idx_l0_active_minus1;i++)
			chroma_weight_l0_flag[i] = read_u(1);
	}
	for (i=0;i<=slice_segment_header.num_ref_idx_l0_active_minus1;i++) {
		if (luma_weight_l0_flag[i]) {
			int delta_luma_weight = read_se();
			int luma_offset = read_se();

			struct pred_weight_2 pw2;
			*((int*) &pw2) = 0;
			pw2.header = 0x19;
			pw2.ref_pic = i;
			pw2.cIdx = 0;
			pw2.list = 0;
			pw2.Weight = (1<<luma_log2_weight_denom) + delta_luma_weight;
			pw2.Offset = luma_offset;
			fwrite(((void *)&pw2),4,1,cabac_to_residual); axi_write(0x0500,(void *) &pw2);
		}
		if (chroma_weight_l0_flag[i]){
			int j;
			for (j=0;j<2;j++){
				int delta_chroma_weight = read_se();
				int chroma_offset = read_se();

				struct pred_weight_2 pw2;
				*((int*) &pw2) = 0;
				pw2.header = 0x19;
				pw2.ref_pic = i;
				pw2.cIdx = 1+j;
				pw2.list = 0;
				pw2.Weight = (1<<luma_log2_weight_denom) + delta_chroma_weight;
				int offset_before_clip = chroma_offset - (( 128 * pw2.Weight >> pw1.ChromaLog2WeightDenom) + 128);
				pw2.Offset = offset_before_clip > 127 ? 127 :( ( offset_before_clip < -128) ? -128 : offset_before_clip) ;
				fwrite(((void *)&pw2),4,1,cabac_to_residual); axi_write(0x0500,(void *) &pw2);
			}
		}
	}
	if (slice_segment_header.slice_type == SLICE_B){
		for (i=0;i<=slice_segment_header.num_ref_idx_l1_active_minus1;i++)
			luma_weight_l1_flag[i] = read_u(1);
		if (sequence_param->chroma_format_idc != 0) {
			for (i=0;i<=slice_segment_header.num_ref_idx_l1_active_minus1;i++)
				chroma_weight_l1_flag[i] = read_u(1);
		}
		for (i=0;i<=slice_segment_header.num_ref_idx_l1_active_minus1;i++) {
			if (luma_weight_l1_flag[i]) {
				int delta_luma_weight = read_se();
				int luma_offset = read_se();

				struct pred_weight_2 pw2;
				*((int*) &pw2) = 0;
				pw2.header = 0x19;
				pw2.ref_pic = i;
				pw2.cIdx = 0;
				pw2.list = 1;
				pw2.Weight = (1<<luma_log2_weight_denom) + delta_luma_weight;
				pw2.Offset = luma_offset;
				fwrite(((void *)&pw2),4,1,cabac_to_residual); axi_write(0x0500,(void *) &pw2);
			}
			if (chroma_weight_l1_flag[i]){
				int j;
				for (j=0;j<2;j++){
					int delta_chroma_weight = read_se();
					int chroma_offset = read_se();

					struct pred_weight_2 pw2;
					*((int*) &pw2) = 0;
					pw2.header = 0x19;
					pw2.ref_pic = i;
					pw2.cIdx = 1+j;
					pw2.list = 1;
					pw2.Weight = (1<<luma_log2_weight_denom) + delta_chroma_weight;
					int offset_before_clip = chroma_offset - (( 128 * pw2.Weight >> pw1.ChromaLog2WeightDenom) + 128);
					pw2.Offset = offset_before_clip > 127 ? 127 :( ( offset_before_clip < -128) ? -128 : offset_before_clip) ;
					fwrite(((void *)&pw2),4,1,cabac_to_residual); axi_write(0x0500,(void *) &pw2);
				}
			}
		}

	}
}

void short_term_ref_pic_set(sequence_param_struct* sps, int idxRps){ // 1st arg not needed
	int printing =0;
	//if (idxRps==2) printing =1;
//	print_loc();
	int inter_ref_pic_set_prediction_flag = (idxRps==0)?0: read_u(1);
	if (inter_ref_pic_set_prediction_flag){
		int delta_idx_minus1=0;
		if (idxRps==sps->num_short_term_ref_pic_sets){
			delta_idx_minus1 = read_ue();
		}
		if (printing) printf("short_term_ref_pic%d inter_ref.._flag=%d delta=%d\n",idxRps,inter_ref_pic_set_prediction_flag,delta_idx_minus1);

		int delta_rps_sign = read_u(1);
		int abs_delta_rps_minus1 = read_ue();
		int RIdx = idxRps - (delta_idx_minus1+1);
		if (printing) printf("delta_rps=%d abs=%d\n",delta_rps_sign,abs_delta_rps_minus1);
		int DeltaRPS = (1-2*delta_rps_sign)*(abs_delta_rps_minus1+1);
		int NumDeltaPocs = sps->stRPS[RIdx].NumDeltaPocs;
		int j;
		if (printing) printf("NumDeltaPocs=%d\n",NumDeltaPocs);
		int used_by_curr_pic_flag[16+1];
		int use_delta_flag[16+1];

		for (j=0;j<=NumDeltaPocs;j++){
			used_by_curr_pic_flag[j] = read_u(1);
			use_delta_flag[j] = 1;
			if (!used_by_curr_pic_flag[j]) {
				use_delta_flag[j] = read_u(1);
			}
		}
		// Negative
		int i=0;
		for (j=sps->stRPS[RIdx].NumPositivePics-1;j>=0;j--){
			int dPoc = sps->stRPS[RIdx].DeltaPocS1[j] + DeltaRPS;
			if (dPoc <0 && use_delta_flag[ sps->stRPS[RIdx].NumNegativePics +j ]){
				sps->stRPS[idxRps].DeltaPocS0[i] = dPoc;
				sps->stRPS[idxRps].UsedByCurrPicS0[i++] = used_by_curr_pic_flag[ sps->stRPS[RIdx].NumNegativePics +j];
			}
		}
		if (DeltaRPS<0 && use_delta_flag[sps->stRPS[RIdx].NumDeltaPocs]){
			sps->stRPS[idxRps].DeltaPocS0[i] = DeltaRPS;
			sps->stRPS[idxRps].UsedByCurrPicS0[i++] = used_by_curr_pic_flag[ sps->stRPS[RIdx].NumDeltaPocs];
		}
		for (j=0;j<sps->stRPS[RIdx].NumNegativePics;j++){
			int dPoc = sps->stRPS[RIdx].DeltaPocS0[j] + DeltaRPS;
			if (dPoc <0 && use_delta_flag[j]){
				sps->stRPS[idxRps].DeltaPocS0[i] = dPoc;
				sps->stRPS[idxRps].UsedByCurrPicS0[i++] = used_by_curr_pic_flag[j];
			}
		}
		sps->stRPS[idxRps].NumNegativePics = i;

		//Positive
		i=0;
		for (j=sps->stRPS[RIdx].NumNegativePics-1;j>=0;j--){
			int dPoc = sps->stRPS[RIdx].DeltaPocS0[j] + DeltaRPS;
			if (dPoc >0 && use_delta_flag[j]){
				sps->stRPS[idxRps].DeltaPocS1[i] = dPoc;
				sps->stRPS[idxRps].UsedByCurrPicS1[i++] = used_by_curr_pic_flag[j];
			}
		}
		if (DeltaRPS>0 && use_delta_flag[sps->stRPS[RIdx].NumDeltaPocs]){
			sps->stRPS[idxRps].DeltaPocS1[i] = DeltaRPS;
			sps->stRPS[idxRps].UsedByCurrPicS1[i++] = used_by_curr_pic_flag[ sps->stRPS[RIdx].NumDeltaPocs];
		}
		for (j=0;j<sps->stRPS[RIdx].NumPositivePics;j++){
			int dPoc = sps->stRPS[RIdx].DeltaPocS1[j] + DeltaRPS;
			if (dPoc >0 && use_delta_flag[ sps->stRPS[RIdx].NumNegativePics +j]){
				sps->stRPS[idxRps].DeltaPocS1[i] = dPoc;
				sps->stRPS[idxRps].UsedByCurrPicS1[i++] = used_by_curr_pic_flag[sps->stRPS[RIdx].NumNegativePics +j];
			}
		}
		sps->stRPS[idxRps].NumPositivePics = i;
		sps->stRPS[idxRps].NumDeltaPocs = sps->stRPS[idxRps].NumNegativePics + sps->stRPS[idxRps].NumPositivePics;

	} else {
		int num_negative_pics = read_ue();
		int num_positive_pics = read_ue();
		sps->stRPS[idxRps].NumNegativePics=num_negative_pics;
		sps->stRPS[idxRps].NumPositivePics=num_positive_pics;
		if (printing) printf("short_term_ref_pic%d negative=%d positive=%d\n",idxRps,num_negative_pics,num_positive_pics);
		int i;
		for (i=0;i<num_negative_pics;i++){

			int delta_poc_s0_minus1 = read_ue();
			sps->stRPS[idxRps].UsedByCurrPicS0[i]= read_u(1);
			if (i==0){
				sps->stRPS[idxRps].DeltaPocS0[i] = -(delta_poc_s0_minus1+1);
			} else {
				sps->stRPS[idxRps].DeltaPocS0[i] =  sps->stRPS[idxRps].DeltaPocS0[i-1] - (delta_poc_s0_minus1+1);
			}

		}
		for (i=0;i<num_positive_pics;i++){
			int delta_poc_s1_minus1 = read_ue();
			sps->stRPS[idxRps].UsedByCurrPicS1[i]= read_u(1);
			if (i==0){
				sps->stRPS[idxRps].DeltaPocS1[i] = (delta_poc_s1_minus1+1);
			} else {
				sps->stRPS[idxRps].DeltaPocS1[i] =  sps->stRPS[idxRps].DeltaPocS1[i-1] + (delta_poc_s1_minus1+1);
			}
		}
		sps->stRPS[idxRps].NumDeltaPocs = sps->stRPS[idxRps].NumNegativePics + sps->stRPS[idxRps].NumPositivePics;
	}
}


void init_cabac_contexts(){
	// types - branches in fig 9-3

	int i=0;
//	printf("%d\n",sizeof(cabac_context_struct));
	for (i=0;i<27;i++){
		//cabac_current.contexts[i] = (cabac_context_struct*) malloc(sizeof(cabac_context_struct)*(cabac_table_maxctxIDx[i]+1));
		int j=0;
		int* base = (int*) (initValue[i]);
//		printf("%d %d\n",i,*(base+1) );
		for (j=0;j<=cabac_table_maxctxIDx[i];j++){
			int initVal = *(base+j);
			int slopeIdx = initVal >> 4;
			int intersecIdx = initVal & 15;
			int m = slopeIdx*5-45;
			int n = (intersecIdx<<3)-16;
			int Clip3SQPY = SliceQPY < 0 ? 0 : SliceQPY > 51 ? 51 : SliceQPY;
			int inner  = ((m*Clip3SQPY)>>4)+n;
			int preCtxState = inner <1 ? 1 : inner >126 ? 126 : inner;
			cabac_current.contexts[i][j].valMPS = preCtxState<=63?0:1;
			cabac_current.contexts[i][j].pStateIdx = cabac_current.contexts[i][j].valMPS ? (preCtxState-64) : (63-preCtxState);
//			printf("  %d %d %d %d\n",j,initVal,cabac->contexts[i][j].valMPS,cabac->contexts[i][j].pStateIdx);
		}
	}
	// table initializations 
	/*
	maxBinIdxCtx[6][1] = 2; maxBinIdxCtx[6][2] = 2;
	maxBinIdxCtx[12][1] = 1; maxBinIdxCtx[12][2] = 1;
	maxBinIdxCtx[13][1] = 1; maxBinIdxCtx[13][2] = 1;
	maxBinIdxCtx[21][0] = 8; maxBinIdxCtx[21][1] = 8; maxBinIdxCtx[21][2] = 8;
	maxBinIdxCtx[22][0] = 8; maxBinIdxCtx[22][1] = 8; maxBinIdxCtx[22][2] = 8;
	*/
	
	//print_cabac();
}

void save_cabac_contexts() {
	int i=0, j=0;
	for (i=0;i<27;i++) {
		for (j=0;j<=cabac_table_maxctxIDx[i];j++){
			saved_context[i][j].pStateIdx = cabac_current.contexts[i][j].pStateIdx;
			saved_context[i][j].valMPS = cabac_current.contexts[i][j].valMPS;
		}
	}
}
void load_cabac_contexts() {
	int i=0, j=0;
	for (i=0;i<27;i++) {
		for (j=0;j<=cabac_table_maxctxIDx[i];j++){
			cabac_current.contexts[i][j].pStateIdx =saved_context[i][j].pStateIdx;
			cabac_current.contexts[i][j].valMPS = saved_context[i][j].valMPS;
		}
	}
}
void init_cabac_engine() {
	cabac_current.codIOffset = read_u(9);
	cabac_current.codIRange = 510;
}

void print_cabac(){
	printf("codIRange=0x%3x codIOffset=0x%3x\n",cabac_current.codIRange,cabac_current.codIOffset);
}
void print_loc(){
	printf("Current Loc: %x %d\n",current->pos,current->bit);
}
int read_cabac_FL(int cMax,int table){

	int binVal=0;
	int ret = 0;
	int binIdx = -1;
	int i=0; int limit = ceil(log((double) cMax+1)/log(2.));
	int ctxIdx=0;
	for (i=0;i<limit;i++) {
		binIdx++;

		if (table>0) ctxIdx= getCtxIdx(binIdx,table-5);
		int ctxIdxTable = table==0 ? -1 : table-5;;
		if (table == 32) ctxIdxTable = 14;
		if (table == 33) ctxIdxTable = 20;
		global_ctxInc = ctxIdx;
		binVal = DecodeBin(ctxIdxTable,ctxIdx);
		ret<<=1; ret |= binVal;
//		printf("Cabac table=%d bin=%d ctx=%d val=%d ",table,binIdx,ctxIdx, binVal);
//		print_cabac();
	}
	
	if (cabac_printing) { printf("  Cabac FL table=%d ctx=%d val=%d ",table,ctxIdx,ret); print_cabac(); }
	return ret;

}
int read_cabac_TU(int cMax,int table){
//	fprintf(stderr,"Read TU\n");
	int binVal=0;
	int ret = 0;
	int binIdx = -1;
	int ctxIdx=0;
	do {
		binIdx++;

		if (table>0) ctxIdx= getCtxIdx(binIdx,table-5);
		int ctxIdxTable = table==0 ? -1 : table-5;;
		if (table == 32) ctxIdxTable = 14;
		if (table == 33) ctxIdxTable = 20;
		global_ctxInc = ctxIdx;
		binVal = DecodeBin(ctxIdxTable,ctxIdx);
//		printf("Cabac table=%d bin=%d ctx=%d val=%d ",table,binIdx,ctxIdx, binVal);
//		print_cabac();
	} while(binIdx < (cMax-1) && binVal ==1);
	ret = binIdx + binVal;
	if (cabac_printing) {printf("  Cabac TU table=%d ctx=%d val=%d ",table,ctxIdx,ret); print_cabac(); }
	return ret;
}


int read_cabac_sao_merge_left() {
	unsigned int header = 1<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	int ret= read_cabac_FL(1,5);
	header = (1 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	return ret;
}
int read_cabac_sao_type_idx(){
	unsigned int header = 2<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	int ret= read_cabac_TU(2,6);
	header = (2 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	return ret;
}
int read_cabac_sao_band_position(){
	unsigned int header = 3<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	int ret =  read_cabac_FL(31,0); 
	header = (3 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	return ret;
}
int read_cabac_sao_offset_abs(){
	unsigned int header = 4<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	int ret = read_cabac_TU(7,0);
	header = (4 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	return ret;
}
int read_cabac_sao_offset_sign() {
	unsigned int header = 5<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	int ret = read_cabac_FL(1,0); 
	header = (5 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	return ret;
}
int read_cabac_sao_eo_class_xxx() {
	unsigned int header = 6<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	int ret = read_cabac_FL(3,0); 
	header = (6 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	return ret;
}
int read_cabac_end_of_slice_segment_flag() {
	unsigned int header = 7<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	int ret = DecodeTerminate();
	header = (7 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	return ret;
}
int read_cabac_end_of_sub_stream_one_bit() {
	unsigned int header =8<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	int ret =  DecodeTerminate();
	header = (8 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	return ret;
}
int read_cabac_split_cu_flag() {
	int availableL = available_left(xCurr_n);
	int availableA = available_up(yCurr_n);
	//int availableL= available_z(xCurr_n,yCurr_n,xCurr_n-1,yCurr_n);
	//int availableA= available_z(xCurr_n,yCurr_n,xCurr_n,yCurr_n-1);
	int condL=0, condA=0;
	if (availableL) condL = (CtDepth_buf_left[ ((yCurr_n) & 63) >>3] > CtDepth_cur);
	if (availableA) condA = (CtDepth_buf[ (xCurr_n) >>3] > CtDepth_cur);
	//if (availableL) condL = (CtDepth[ (yCurr_n)*sequence_param->pic_width_in_luma_samples + (xCurr_n-1)] > CtDepth[ (yCurr_n)*sequence_param->pic_width_in_luma_samples + xCurr_n]);
	//if (availableA) condA = (CtDepth[ (yCurr_n-1)*sequence_param->pic_width_in_luma_samples + (xCurr_n)] > CtDepth[ (yCurr_n)*sequence_param->pic_width_in_luma_samples + xCurr_n]);
//		printf("%d %d\n",condA, condL);
	unsigned int header = 9<<24 | availableL <<3 | condL <<2 | availableA << 1 | condA << 0;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_FL(1,7); 
	header = (9 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_cu_transquant_bypass_flag() { return 0; }
int read_cabac_cu_skip_flag() {
	int availableL = available_left(xCurr_n);
	int availableA = available_up(yCurr_n);
//	int availableL= available_z(xCurr_n,yCurr_n,xCurr_n-1,yCurr_n);
//	int availableA= available_z(xCurr_n,yCurr_n,xCurr_n,yCurr_n-1);
	int condL=0, condA=0;
	if (availableL) condL = cu_skip_flag_buf_left[  ((yCurr_n) & 63) >>3];	
	if (availableA) condA = cu_skip_flag_buf[ (xCurr_n) >>3];
	//if (availableL) condL = cu_skip_flag[ (yCurr_n)*sequence_param->pic_width_in_luma_samples + (xCurr_n-1)];
	//if (availableA) condA = cu_skip_flag[ (yCurr_n-1)*sequence_param->pic_width_in_luma_samples + (xCurr_n)];
	unsigned int header = 11<<24 | availableL <<3 | condL << 2 | availableA << 1 | condA << 0;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_FL(1,9);
	header = (11 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_cu_qp_delta_abs() { 
	int i=0;

	int prefix = read_cabac_TU(5,10);
	
	if (prefix<5) return prefix;

	int ones=0;
	int egk_suf=0;
	while(DecodeBypass())
		ones++;
	if (ones==0) return 5;
	for (i=0;i<ones;i++){
		int bin = DecodeBypass();
		egk_suf = (egk_suf<<1)|bin;
	}
	int egk =  (1<<ones) +  egk_suf;
	return egk+4;
}
int read_cabac_cu_qp_delta_sign() { 
	return DecodeBypass();
}
int read_cabac_pred_mode_flag() {
	unsigned int header =14<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_FL(1,11);
	header = (14 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_part_mode(int cLog2CbSize, int x0, int y0){
	int initType;
	int ret;
	if (slice_segment_header.slice_type==SLICE_I){
		initType = 0;
	} else if (slice_segment_header.slice_type==SLICE_P){
		initType = slice_segment_header.cabac_init_flag?2:1;
//		fprintf(stderr,"P slice\n");
	} else {
		initType = slice_segment_header.cabac_init_flag?1:2;
//		fprintf(stderr,"B slice\n");
	}
	int binIdx = initType>1 ? 5 : initType;
	if (CuPredMode_cur==MODE_INTRA){
		unsigned int header =15 <<24 | (cLog2CbSize & 7) << 21 | (sequence_param->Log2MinCbSizeY & 7) <<18 | 0 << 17 | sequence_param->amp_enabled_flag << 16 ;
		//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
		//fwrite(&header,4,1,cu_cabac_in);
		int bin = DecodeBin(7,binIdx);
		if (cabac_printing) {printf("  Cabac part mode %d 1-NxN 0-2Nx2N ",bin); print_cabac(); }
		ret = bin?PART_2Nx2N:PART_NxN;
		
		header = (15 << 24) | ret & ((1<<24)-1);
		//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
		//fwrite(&header,4,1,cu_cabac_out);
		return ret;
	} else {
		unsigned int header =15 <<24 | (cLog2CbSize & 7) << 21 | (sequence_param->Log2MinCbSizeY & 7) <<18 | 1 << 17 | sequence_param->amp_enabled_flag << 16 ;
		//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
		//fwrite(&header,4,1,cu_cabac_in);
		int ret = -1;
		int bin = DecodeBin(7,binIdx);
		if (bin){
			ret =  PART_2Nx2N;
		} else {
			int bin2 = DecodeBin(7,binIdx+1);
			if (cLog2CbSize > sequence_param->Log2MinCbSizeY){
				if (!sequence_param->amp_enabled_flag){
					ret = bin2 ? PART_2NxN : PART_Nx2N;
				} else {
					int bin3 = DecodeBin(7,binIdx+3);
					if (bin2==0 && bin3==0){
						int bin4 = DecodeBypass();
						ret = bin4 ? PART_nRx2N : PART_nLx2N;
					} else if (bin2==0 && bin3==1){
						ret = PART_Nx2N;
					} else if (bin2==1 && bin3==0){
						int bin4 = DecodeBypass();
						ret = bin4 ? PART_2NxnD : PART_2NxnU;
					} else {
						ret = PART_2NxN;
					}
				}
			} else {
				if (cLog2CbSize==3){
					ret = bin2 ? PART_2NxN : PART_Nx2N;
				} else {
					// should only get here if Log2MinCbSize>3
					if (bin2) ret = PART_2NxN;
					else {
						int bin3 = DecodeBin(7,binIdx+2);
						ret = bin3 ? PART_Nx2N : PART_NxN;
					}
				}
			}
		}
		if (cabac_printing) {printf("  Cabac part mode %d  ",ret); print_cabac(); }
		
		header = (15 << 24) | ret & ((1<<24)-1);
		//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
		//fwrite(&header,4,1,cu_cabac_out);
		return ret;
	}
	return 0;
}
int read_cabac_pcm_flag() {
	int bin = DecodeTerminate();
	return bin;
}
int read_cabac_prev_intra_luma_pred_flag() {
	unsigned int header =17<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_FL(1,13);
	header = (17 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_mpm_idx() {
	unsigned int header =18<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_TU(2,0);
	header = (18 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_rem_intra_luma_pred_flag() {
	unsigned int header =19<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_FL(31,0);
	header = (19 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_chroma(){
//	cabac_printing=0;
	unsigned int header =20<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int prefix = read_cabac_FL(1,14);
	int suffix = 0;
	if (prefix==1) suffix = read_cabac_FL(2,0);
	int ret = (1-prefix)*4+suffix;
	header = (20 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_merge_flag() {
	unsigned int header =21<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_FL(1,15);
	header = (21 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_merge_idx(int MaxNumMergeCand, int skip) { 
	unsigned int header =22<<24 | MaxNumMergeCand << 21;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//if (!skip) fwrite(&header,4,1,cu_cabac_in);
	int ret =  read_cabac_TU(MaxNumMergeCand-1,16); 
	header = (22 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//if (!skip) fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_inter_pred_idc(int x0, int y0, int nPbW, int nPbH){
	int ret =-1;
	int initType;
	unsigned int header =23<<24 | nPbW << 16 | nPbH << 8 | CtDepth_buf[x0 >> 3] << 5 ;
	//fwrite(&header,4,1,cu_cabac_in);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	if (slice_segment_header.slice_type==SLICE_I){
		initType = 0;
	} else if (slice_segment_header.slice_type==SLICE_P){
		initType = slice_segment_header.cabac_init_flag?2:1;
//		fprintf(stderr,"P slice\n");
	} else {
		initType = slice_segment_header.cabac_init_flag?1:2;
//		fprintf(stderr,"B slice\n");
	}
	int offset = initType==1? 0 : 5; // will not be 0
	if ((nPbH+nPbW)!=12){
		//int bin0 = DecodeBin(12,offset+ CtDepth[y0*sequence_param->pic_width_in_luma_samples+x0]);
		int bin0 = DecodeBin(12,offset+ CtDepth_buf[x0 >> 3]);
		if (bin0) ret = PRED_BI;
		else {
			int bin1 = DecodeBin(12,offset+4);
			ret = bin1 ? PRED_L1: PRED_L0;
		}
	} else {
		int bin0 = DecodeBin(12,offset+4);
		ret = bin0 ? PRED_L1: PRED_L0;
	}

	if (cabac_printing) {printf("  Cabac inter_pred_idc  %d  ",ret); print_cabac(); }
	header = (23 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
	
}
int read_cabac_ref_idx_l0(int num_ref_idx) {
	unsigned int header =24<<24 | num_ref_idx << 16;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret= read_cabac_TU(num_ref_idx,18);
	header = (24 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_ref_idx_l1(int num_ref_idx) {
	unsigned int header =25<<24 | num_ref_idx << 16;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_TU(num_ref_idx,18);
	header = (25 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_abs_mvd_greater0_flag() {
	unsigned int header =26<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_FL(1,19);
	header = (26 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_abs_mvd_greater1_flag() {
	unsigned int header =27<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_FL(1,32);
	header = (27 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_abs_mvd_minus2(){
	int egk_ones=-1;
	int bin;
	unsigned int header =28<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	do {
		egk_ones++;
		bin = DecodeBypass();
	} while (bin==1);
	int i;
	int egk_suf=0;
	for (i=0;i<egk_ones+1;i++){
		bin = DecodeBypass();
		egk_suf = (egk_suf<<1)|bin;
	}
	int egk = ( ((1<<egk_ones)-1)<<1) +  egk_suf;
	if (cabac_printing) printf("  Cabac mvd_minus2 %d\n",egk);
	int ret = egk;
	header = (28 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_mvd_sign_flag() {
	unsigned int header =29<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_FL(1,0);
	header = (29<< 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_mvp_lx_flag() { 
	unsigned int header =30<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_FL(1,20);
	header = (30 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_rqt_root_cbf() {
	unsigned int header =31<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_FL(1,21);
	header = (31 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_split_transform_flag() {
	unsigned int header =32<<24 | (5-cabac_trafoSize)<<2 ;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	header =32<<24 | (5-cabac_trafoSize)  ; // dont shift by 2 bits in hardware
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_FL(1,22); 
	header = (32 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_cbf_luma() {
	unsigned int header =33<<24 | cabac_trafoDepth<<2 ;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	header =33<<24 | cabac_trafoDepth ;
	//fwrite(&header,4,1,cu_cabac_in);
	int ret =  read_cabac_FL(1,23);
	header = (33 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_cbf_cx() {
	unsigned int header =34<<24 | cabac_trafoDepth<<2;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	header =34<<24 | cabac_trafoDepth;
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_FL(1,24);
	header = (34 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_transform_skip_flag(int cIdx) {
	unsigned int header =35<<24 | cIdx <<2 ;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	header =35<<24 | cIdx ;
	//fwrite(&header,4,1,cu_cabac_in);
	int ret=0;
	if (cIdx==0) 
		ret= read_cabac_FL(1,25);
	else 
		ret= read_cabac_FL(1,33);
	header = (35 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_last_significant_coef_x_prefix(int log2TrafoSize) {
	unsigned int header =36<<24 | cabac_trafoSize <<21 | cabac_cIdx << 19;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_TU( (log2TrafoSize<<1) -1, 26); 
	header = (36 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_last_significant_coef_y_prefix(int log2TrafoSize)	 {
	unsigned int header =37<<24 | cabac_trafoSize << 21 | cabac_cIdx << 19;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_TU( (log2TrafoSize<<1) -1, 27); 
	header = (37 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_last_significant_coef_x_suffix(int prefix) {
	unsigned int header =38<<24 | prefix << 20;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_FL( (1<<((prefix >>1)-1)-1),0);
	header = (38 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_last_significant_coef_y_suffix(int prefix) {
	unsigned int header =39<<24 | prefix << 20;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_FL( (1<<((prefix >>1)-1)-1),0);
	header = (39 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_coded_sub_block(int xS, int yS, int cIdx, int log2TrafoSize){
	int csbfCtx = 0;
	
	int csbf_r = 0, csbf_d =0;
	if (xS< ((1<<(log2TrafoSize-2)) -1) ){
		csbfCtx += coded_sub_block_flag[xS+1][yS];
		csbf_r=coded_sub_block_flag[xS+1][yS];
	}
	if (yS< ((1<<(log2TrafoSize-2)) -1) ){
		csbfCtx += coded_sub_block_flag[xS][yS+1];
		csbf_d=coded_sub_block_flag[xS][yS+1];
	}
	unsigned int header =40<<24 | log2TrafoSize << 21 | cIdx << 19 | csbf_d << 18 | csbf_r << 17;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ctxIdxInc;
	if (cIdx==0) ctxIdxInc = csbfCtx>1?1:csbfCtx;
	else  ctxIdxInc = 2 + (csbfCtx>1?1:csbfCtx);
	int initType;
	if (slice_segment_header.slice_type==SLICE_I){
		initType = 0;
	} else if (slice_segment_header.slice_type==SLICE_P){
		initType = slice_segment_header.cabac_init_flag?2:1;
	} else {
		initType = slice_segment_header.cabac_init_flag?1:2;
	}
	//	if (binIdx>maxBinIdxCtx[tableIdx][initType]) binIdx = maxBinIdxCtx[tableIdx][initType];
		int Offset = ctxIdxOffset[23][initType];
	int ret = DecodeBin(23,Offset+ctxIdxInc);
	if ( cabac_printing) {printf("Coded Sub Block %d ctx=%d ",ret,Offset+ctxIdxInc); print_cabac();}
//	exit(0);
	header = (40 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
	
}
int read_cabac_significant_coeff_flag() {
	
	int xS = (cabac_xC>>2); int yS = (cabac_yC>>2);
	int csbf_r = 0, csbf_d =0;
	if (xS < (1<<(cabac_trafoSize-2))-1) csbf_r += coded_sub_block_flag[xS+1][yS];
	if (yS < (1<<(cabac_trafoSize-2))-1) csbf_d += (coded_sub_block_flag[xS][yS+1]);

	unsigned int header =41<<24 | cabac_trafoSize << 21 | cabac_cIdx << 19 | csbf_d << 18 | csbf_r << 17 | cabac_xC << 12 | cabac_yC << 7 | cabac_scanIdx << 5;
	//fwrite(&header,4,1,cu_cabac_in);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	int ret = read_cabac_FL(1,29); 
	header = (41 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_greater1_v2(int cIdx, int i, int n,int first_in_tb){
	int initType;
	
	if (slice_segment_header.slice_type==2){
		initType = 0;
	} else if (slice_segment_header.slice_type==1){
		initType = slice_segment_header.cabac_init_flag?2:1;
	} else {
		initType = slice_segment_header.cabac_init_flag?1:2;
	}
//	if (binIdx>maxBinIdxCtx[tableIdx][initType]) binIdx = maxBinIdxCtx[tableIdx][initType];
	int Offset = ctxIdxOffset[25][initType];

	if (!cabac_greater1_invoked[i]){
		cabac_greater1_invoked[i] = 1;
		if (i==0 || cIdx >0) cabac_ctxSet = 0;
		else cabac_ctxSet = 2;
		int lastGreater1Ctx;
		if (first_in_tb) lastGreater1Ctx = 1;
		else lastGreater1Ctx = cabac_greater1Ctx_v2;
		if (lastGreater1Ctx==0) {
			cabac_ctxSet++;
		}
		cabac_greater1Ctx_v2 = 1;
	} else {
//		if (cabac_greater1Ctx >0) {
//			if (cabac_lastgreater1Flag) cabac_greater1Ctx = 0;
//			else cabac_greater1Ctx++;
//		}

	}
	unsigned int header =42<<24 | cIdx << 19 | cabac_greater1Ctx_v2 << 15 | cabac_ctxSet << 13;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ctxIdcInc = (cabac_ctxSet*4) + (cabac_greater1Ctx_v2>3 ? 3 : cabac_greater1Ctx_v2);
	if (cIdx > 0) ctxIdcInc+=16;
	global_ctxInc = ctxIdcInc;
	int binVal = DecodeBin(25,Offset+ctxIdcInc);
//	cabac_lastgreater1Flag = binVal;
	if (cabac_printing) {printf("  Cabac greater1 ctxSet=%d greater1Ctx=%d ctx=%d val=%d ",cabac_ctxSet,cabac_greater1Ctx_v2,ctxIdcInc,binVal); print_cabac(); }
	if (binVal){
		cabac_greater1_first_1_met=1;
		cabac_greater1Ctx_v2=0;
	} else {
		if (!cabac_greater1_first_1_met){
			cabac_greater1Ctx_v2++;
		}
	}
	header = (42 << 24) | binVal & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
//	return ret;
	return binVal;
}
int read_cabac_coeff_abs_level_greater2_flag() {
	unsigned int header =43<<24 | cabac_cIdx << 19 | cabac_ctxSet << 13;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_FL(1,31);
	header = (43 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_coeff_abs_level_remaining(int n, int baseLevel){
	int cLastAbsLevel,cLastRiceParam;
	if (n==15){
		cLastAbsLevel = 0; cLastRiceParam = 0;
	} else {
		cLastAbsLevel = AbsCoeff[n+1];
		cLastRiceParam = cabac_cLastRiceParam;
	}
	int cRiceParam = cLastRiceParam + (cLastAbsLevel>(3*(1<<cLastRiceParam))?1:0);
	if (cRiceParam>4) cRiceParam = 4;
	cRiceParam = cNextRiceParam; // testing
	if (cRiceParam>4) cRiceParam = 4;

	unsigned int header =44<<24 | cRiceParam << 21;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);

	cabac_cLastRiceParam = cRiceParam;
	int cTRMax = 4 << cRiceParam;
	int prefix = read_cabac_TU(4,0);
	int ret = 0;
	if (prefix==4) { // egk
		ret = cTRMax;
		int egk_ones=-1;
		int bin;
		do {
			egk_ones++;
			bin = DecodeBypass();
		} while (bin==1);
		int i;
		int egk_suf=0;
		for (i=0;i<egk_ones+cRiceParam+1;i++){
			bin = DecodeBypass();
			egk_suf = (egk_suf<<1)|bin;
		}
		int egk = ( ((1<<egk_ones)-1)<<(cRiceParam+1)) +  egk_suf;
		ret += egk; // OR ret = ( ((1<<egk_ones)+1)<<(cRiceParam+1)) +  egk_suf;
	} else {
		int i;
		ret = prefix;
		for (i=0;i<cRiceParam;i++){
			ret = (ret<<1)|DecodeBypass();
		}
	}
	cNextRiceParam+= ((baseLevel+ret)>(3*(1<<cRiceParam))?1:0);
	if (cabac_printing) { printf("  Coef %d RiceParam=%d cLastAbsLevel=%d ",ret,cRiceParam,cLastAbsLevel); print_cabac(); }
	header = (44 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}
int read_cabac_coeff_sign_flag() {
	unsigned int header =45<<24;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	//fwrite(&header,4,1,cu_cabac_in);
	int ret = read_cabac_FL(1,0);
	header = (45 << 24) | ret & ((1<<24)-1);
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_res);
	//fwrite(&header,4,1,cu_cabac_out);
	return ret;
}


int getCtxIdx(int binIdx, int tableIdx){
	int initType;
	if (slice_segment_header.slice_type==2){
		initType = 0;
	} else if (slice_segment_header.slice_type==1){
		initType = slice_segment_header.cabac_init_flag?2:1;
//		fprintf(stderr,"P slice\n");
	} else {
		initType = slice_segment_header.cabac_init_flag?1:2;
//		fprintf(stderr,"B slice\n");
	}
//	if (binIdx>maxBinIdxCtx[tableIdx][initType]) binIdx = maxBinIdxCtx[tableIdx][initType];
	int Offset = ctxIdxOffset[tableIdx][initType];
	int Inc = getCtxIdxInc(tableIdx,binIdx);
	if (Inc==-1) return -1;
	return Offset+Inc;
}
int getCtxIdxInc(int tableIdx, int binIdx){
	if (tableIdx==0) {
		return 0;
	} else if (tableIdx==1) {
		if (binIdx==0) return 0; else return -1;
	} else if (tableIdx==2) {
		// 9.2.3.1.1
		int availableL = available_left(xCurr_n);
		int availableA = available_up(yCurr_n);
		//int availableL= available_z(xCurr_n,yCurr_n,xCurr_n-1,yCurr_n);
		//int availableA= available_z(xCurr_n,yCurr_n,xCurr_n,yCurr_n-1);
		int condL, condA;
		//if (availableL) condL = (CtDepth[ (yCurr_n)*sequence_param->pic_width_in_luma_samples + (xCurr_n-1)] > CtDepth[ (yCurr_n)*sequence_param->pic_width_in_luma_samples + xCurr_n]);
		if (availableL) condL = (CtDepth_buf_left[ ((yCurr_n) & 63) >>3] > CtDepth_cur);
		//if (availableA) condA = (CtDepth[ (yCurr_n-1)*sequence_param->pic_width_in_luma_samples + (xCurr_n)] > CtDepth[ (yCurr_n)*sequence_param->pic_width_in_luma_samples + xCurr_n]);
		if (availableA) condA = (CtDepth_buf[ (xCurr_n) >>3] > CtDepth_cur);
//		printf("%d %d\n",condA, condL);
		return (availableL&& condL )+(  availableA && condA);
	} else if (tableIdx==4){
		// 9.2.3.1.1
		int availableL = available_left(xCurr_n);
		int availableA = available_up(yCurr_n);
		//int availableL= available_z(xCurr_n,yCurr_n,xCurr_n-1,yCurr_n);
		//int availableA= available_z(xCurr_n,yCurr_n,xCurr_n,yCurr_n-1);
		int condL, condA;
		//if (availableL) condL = cu_skip_flag[ (yCurr_n)*sequence_param->pic_width_in_luma_samples + (xCurr_n-1)];
		if (availableL) condL = cu_skip_flag_buf_left[  ((yCurr_n) & 63) >>3];
		//if (availableA) condA = cu_skip_flag[ (yCurr_n-1)*sequence_param->pic_width_in_luma_samples + (xCurr_n)];
		if (availableA) condA = cu_skip_flag_buf[ (xCurr_n) >>3];
		return (availableL&& condL )+(  availableA && condA);
	} else if (tableIdx==5) {
		return binIdx>0;
	} else if (tableIdx ==6){
		return 0;
	} else if (tableIdx==8 || tableIdx ==9){
		return 0;
	} else if (tableIdx==10){
		return 0;
	} else if (tableIdx==11){
		return binIdx==0?0:-1;
	} else if (tableIdx==13){
		return binIdx<=1?binIdx:-1;
	} else if (tableIdx==14){
		return 0;
	} else if (tableIdx==15){
		return 0;
	} else if (tableIdx==16){
		return 0;
	} else if (tableIdx==17){
		return 5-cabac_trafoSize;
	} else if (tableIdx==18){
		return cabac_trafoDepth==0;
	} else if (tableIdx==19){
		return cabac_trafoDepth;
	} else if (tableIdx==20) {
		return 0;
	} else if (tableIdx==21 || tableIdx ==22){
		int ctxOffset; int ctxShift;
		if (cabac_cIdx==0){
			ctxOffset = 3 * (cabac_trafoSize-2) + ( ( cabac_trafoSize -1)>>2);
			ctxShift = (cabac_trafoSize+1)>>2;
		} else {
			ctxOffset = 15; ctxShift = (cabac_trafoSize-2);
		}
		return (binIdx >> ctxShift) + ctxOffset;
	} else if (tableIdx==24){
		int ctxIdxMap[15] = {0,1,4,5,2,3,4,5,6,6,8,8,7,7,8};
		int sigCtx;
		int xC = cabac_xC; int yC = cabac_yC;
		if (cabac_trafoSize==2) {
			sigCtx = ctxIdxMap[ (yC<<2)+xC];
		}
		else if (xC+yC==0) sigCtx = 0;
		else {
			int xS = (xC>>2); int yS = (yC>>2);
			int prevCsbf = 0;
			if (xS < (1<<(cabac_trafoSize-2))-1) prevCsbf += coded_sub_block_flag[xS+1][yS];
			if (yS < (1<<(cabac_trafoSize-2))-1) prevCsbf += (coded_sub_block_flag[xS][yS+1])<<1;
			int xP = xC & 3; int yP = yC & 3;
			if (prevCsbf==0) sigCtx = (xP+yP==0)? 2: (xP+yP <3)?1:0;
			else if (prevCsbf==1) sigCtx = (yP==0)? 2: (yP==1)? 1:0;
			else if (prevCsbf==2) sigCtx = (xP==0)? 2: (xP==1)? 1:0;
			else sigCtx = 2;
			if (cabac_cIdx==0){
				if( (xS+yS)>0){
					sigCtx+=3;
				}
				if (cabac_trafoSize==3) sigCtx+= (cabac_scanIdx==0) ? 9 :15;
				else sigCtx +=21;
			} else {
				if (cabac_trafoSize==3) sigCtx+= 9;
				else sigCtx += 12;
			}
		}
		if (cabac_cIdx==0) return sigCtx;
		else return (27+sigCtx);
	} else if (tableIdx==26){
		return (cabac_cIdx>0) ? cabac_ctxSet +4 : cabac_ctxSet;
	} else if (tableIdx==27){
		return 0;
	} else if (tableIdx==28){
		return 0;
	}
	else {
		fprintf(stderr,"cabac table not implemented\n");

	}
	return 0; // shouldnt get here
}
int ctsMPS = 0;
int prevCtx = 0;
int DecodeBin(int ctxIdxTable, int ctxIdx){
	ctsBypassCnt[ctsBypass]++;
	ctsBypass = 0;
	int binVal = 0;
	if (ctxIdxTable<0 || ctxIdx < 0 )  return DecodeBypass();
//	else if (ctxIdxTable==0 && ctxIdx ==0) return DecodeTerminate();

	else {
		CTU_bins++;
		if (!in_res) CTU_non_res++;
		bin_count[ctxIdxTable]++;
		// DecodeDecision
		cabac_context_struct* ctx = &(cabac_current.contexts[ctxIdxTable][ctxIdx]);
		int qCodIRangeIdx = (cabac_current.codIRange>>6)&3;
		int codIRangeLPS = rangeTabLPS[ctx->pStateIdx][qCodIRangeIdx];
		cabac_current.codIRange -= codIRangeLPS;
		if (cabac_current.codIOffset>=cabac_current.codIRange){
			LPSCount++;
			ctsMPSCount[ctsMPS]++;
			ctsMPS=0;
			binVal = 1- ctx->valMPS;
			cabac_current.codIOffset-=cabac_current.codIRange;
			cabac_current.codIRange = codIRangeLPS;
			if (ctx->pStateIdx==0) ctx->valMPS = 1- ctx->valMPS;
			ctx->pStateIdx = transIdxLPS[ctx->pStateIdx];
		} else {
			if (ctxIdxTable*1000+ctxIdx==prevCtx) {
				ctsMPS++;
			} else {
				ctsMPSCount[ctsMPS]++;
				ctsMPS=1;
			}
			MPSCount++;

			if (ctsMPS>15) ctsMPS=15;
			binVal = ctx->valMPS;
			ctx->pStateIdx = transIdxMPS[ctx->pStateIdx];
		}
		// Renorm
		int renorm_iter= 0;
		while(cabac_current.codIRange<256){
			renorm_iter++;
			cabac_current.codIRange <<=1;
			cabac_current.codIOffset <<=1;
			cabac_current.codIOffset |= read_bit();
		}
		renorm[renorm_iter]++;
		prevCtx = ctxIdxTable*1000+ctxIdx;

		unsigned int header = 1 << 31 | cabac_current.codIOffset << 22 | cabac_current.codIRange << 13 | ctx->pStateIdx << 7 | ctx->valMPS << 6 | global_ctxInc  ;
	//	if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	}

	


	return binVal;
}
int DecodeBypass(){
	ctsBypass++;
	CTU_bins++;
	CTU_bypass++;
	ctsMPSCount[ctsMPS]++;
	ctsMPS=0;
	if (!in_res) CTU_non_res++;
	bypass_count[in_res]++;
	cabac_current.codIOffset <<=1;
	cabac_current.codIOffset |= read_bit();
	int binVal;
	if (cabac_current.codIOffset>=cabac_current.codIRange){
		binVal = 1;
		cabac_current.codIOffset-=cabac_current.codIRange;
	} else {
		binVal = 0;
	}
	unsigned int header = 1 << 31 | cabac_current.codIOffset << 22 | cabac_current.codIRange << 13 | 0 << 6 | 0  << 5 | binVal << 4  ;
	//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
	return binVal;
}
int DecodeTerminate() {
	//bin_count++;
	ctsMPSCount[ctsMPS]++;
	ctsMPS=0;
	int binVal =0;
	cabac_current.codIRange -=2;
	if (cabac_current.codIOffset>=cabac_current.codIRange){
		binVal= 1;
	} else {
		binVal = 0;
		// Renorm
		while(cabac_current.codIRange<256){
			cabac_current.codIRange <<=1;
			cabac_current.codIOffset <<=1;
			cabac_current.codIOffset |= read_bit();
		}
	}
	unsigned int header = 1 << 31 | cabac_current.codIOffset << 22 | cabac_current.codIRange << 13 | 63 << 7 | 0 << 5 | binVal << 4  ;
		//if (cabac_print_enable) fwrite(&header,4,1,cabac_out);
//	fprintf(stderr,"terminate not implemented\n");
	return binVal;
}

unsigned int read_ue(){
	int leadingZeroBits = -1;
	int b;
	for (b=0;!b;leadingZeroBits++){
		b = read_u(1);
	}
	if (leadingZeroBits>20) fprintf(stderr,"Large leading zeros in ue(v)\n");

	unsigned int suffix = 0;
	if (leadingZeroBits > 8) suffix = read_u(leadingZeroBits-8) << 8;
	suffix += read_u(leadingZeroBits >8 ? 8 : leadingZeroBits);
	return ((( 1<< leadingZeroBits) - 1 ) + suffix);

}
int read_se(){
	unsigned int d = read_ue();
	int sign = (d%2 == 0) ? -1 : 1;
	int mag = (d+1)/2;
	return sign*mag;
}
unsigned int read_u(int n){
	
	
	if (n==0) {
		if (in_header) 
		axi_write(0x900 | (n<<4),n); // n for 0
		return 0;
	}
	
	unsigned int ret = read_bit();
	int i;
	for (i=1;i<n;i++){
		ret<<=1;
		ret += read_bit();
	}
	if (in_header) {
		if ( n % 8 !=0 ){
			axi_write(0x900 | ((n%8)<<4),ret >> (n/8)*8);
			n = n - (n%8);
		}
		if (n==24){
			axi_write(0x900 | (8<<4), (ret & 0xff0000 >> 16) );
			n-=8;
		}
		if (n==16){
			axi_write(0x900 | (8<<4), (ret & 0xff00 >> 8) );
			n-=8;
		}
		if (n==8){
			axi_write(0x900 | (8<<4), (ret & 0xff) );
			n-=8;
		}
		
	}
	return ret;
}
int read_bit(){
	int ret =( (current->byte[current->pos] & (1<<(7-current->bit)) ) > 0);
	current->bit++;
	if (current->bit==8){
		current->bit=0;
		current->pos++;
		
		if (current->ctsZeros == 2 && current->byte[current->pos]==3){
			current->pos++;
			
			current->ctsZeros=0;
		} 
		{
			if (current->byte[current->pos] ==0) current->ctsZeros++;
			else current->ctsZeros = 0;
		}
		//if (PicOrderCntVal ==5)
		//	printf("%2x", current->byte[current->pos]);
	}
	//printf("%d",ret);
	return ret;
}

void byte_alignment(){
	int old_in_header = in_header;
	int one_bit = read_u(1); // should be 1
	while(current->bit!=0){
		int b = read_u(1);
		int c = 0;
	}
	in_header = old_in_header;
}

int available_left(int xCurr){
	if ( (xCurr & ( (1<<sequence_param->Log2CtbSizeY)-1)) ==0 ) {
		if (xCurr ==0) return 0;
		else return same_slice_tile_left;
	} else 
		return 1;
}
int available_up(int yCurr){
	if ( (yCurr & ( (1<<sequence_param->Log2CtbSizeY)-1)) ==0 ) {
		if (yCurr ==0) return 0;
		else return same_slice_tile_up;
	} else 
		return 1;
}

void initialize_scanOrder(){
	int block_size;
	for (block_size=0;block_size<4;block_size++){
		// diag
		int i=0,x=0,y=0;
		int stopLoop = 0;
		int blk_size = (1<<block_size);
		while(!stopLoop){
			while (y>=0){
				if (x<blk_size && y<blk_size){
					ScanOrder[block_size][0][i][0] = x;
					ScanOrder[block_size][0][i][1] = y;
//					printf("size %d %d (%d,%d)\n",blk_size,i,x,y);
					i++;
				}
				y--;
				x++;
			}
			y=x;
			x=0;
			if (i>=blk_size*blk_size) stopLoop = 1;
		}
		//6.5.4
		i=0;
		for (y=0;y<blk_size;y++){
			for (x=0;x<blk_size;x++){
				ScanOrder[block_size][1][i][0]=x;
				ScanOrder[block_size][1][i][1]=y;
				i++;
			}
		}
		//6.5.5
		i=0;
		for (x=0;x<blk_size;x++){
			for (y=0;y<blk_size;y++){
				ScanOrder[block_size][2][i][0]=x;
				ScanOrder[block_size][2][i][1]=y;
				i++;
			}
		}
	}
}
int derive_intra_mode(int xB, int yB,int prev_intra_luma_pred_flag,int mpm_idx,int rem_intra_luma_pred_mode, int intra_chroma_pred_mode) {

	int linB = yB*sequence_param->pic_width_in_luma_samples+xB;
	int linBA = yB*sequence_param->pic_width_in_luma_samples+xB-1;
	int linBB = (yB-1)*sequence_param->pic_width_in_luma_samples+xB;

	int availableA = available_left(xB);
	int availableB = available_up(yB);
	//int availableA = available_z(xB,yB,xB-1,yB);
	//int availableB = available_z(xB,yB,xB,yB-1);
	int candIntraPredModeA, candIntraPredModeB;
	int candModeList[3];

	int ret =0;

	if (!availableA || CuPredMode_buf_left[(yB & 63) >>2] != MODE_INTRA){
		candIntraPredModeA = 1;
	} else {
		//candIntraPredModeA = IntraPredModeY[linBA];
		candIntraPredModeA = IntraPredModeY_buf_left[(yB & 63) >>2];
	}
	if (!availableB || CuPredMode_buf[(xB & 63 ) >>2] != MODE_INTRA || (yB-1)< ((yB>>sequence_param->Log2CtbSizeY)<<sequence_param->Log2CtbSizeY)){
		candIntraPredModeB = 1;
	} else {
		candIntraPredModeB = IntraPredModeY_buf[(xB &63) >>2];
	}
	// step 3
	if (candIntraPredModeA==candIntraPredModeB){
		if (candIntraPredModeA<2){
			candModeList[0] = 0; candModeList[1] = 1; candModeList[2] = 26;
		} else {
			candModeList[0] = candIntraPredModeA;
			candModeList[1] = 2+ ((candIntraPredModeA+29)&31);
			candModeList[2] = 2+ ((candIntraPredModeA-2+1)&31);
		}
	} else {
		candModeList[0] = candIntraPredModeA;
		candModeList[1] = candIntraPredModeB;
		if (candModeList[0] != 0 && candModeList[1]!= 0) candModeList[2] = 0;
		else if(candModeList[0] != 1 && candModeList[1]!= 1) candModeList[2] = 1;
		else candModeList[2] = 26;
	}
	//step 4
	if (prev_intra_luma_pred_flag){
		//IntraPredModeY[linB] = candModeList[mpm_idx];
		ret = candModeList[mpm_idx];
		//IntraPredModeY_buf[xB >> 2] = candModeList[mpm_idx];
	} else {
		if (candModeList[0]>candModeList[1]){
			int temp = candModeList[0];
			candModeList[0] = candModeList[1];
			candModeList[1] = temp;
		}
		if (candModeList[0]>candModeList[2]){
			int temp = candModeList[0];
			candModeList[0] = candModeList[2];
			candModeList[2] = temp;
		}
		if (candModeList[1]>candModeList[2]){
			int temp = candModeList[1];
			candModeList[1] = candModeList[2];
			candModeList[2] = temp;
		}
		//IntraPredModeY[linB] = rem_intra_luma_pred_mode;
		ret = rem_intra_luma_pred_mode;
		//IntraPredModeY_buf[xB>>2] = rem_intra_luma_pred_mode;
		int i=0;
		for (i=0;i<3;i++){
			if (ret>=candModeList[i]) ret++;
		}
		
	}

	if (cabac_printing)printf("Intra Mode (%d,%d) mpm (%d %d %d) mpm_idx %d luma_mode %d \n",xB,yB,candModeList[0],candModeList[1],candModeList[2],mpm_idx,ret);
	return ret;
}


void initialize_blocks(){

	//6.5.1
	int i;
	//colWidth = (int*) malloc(sizeof(int)* (pps.num_tile_columns_minus1+1) );
	//colBd =(int*) malloc(sizeof(int)* (pps.num_tile_columns_minus1+2) );
	if (picture_param->uniform_spacing_flag){
		for(i=0;i<=picture_param->num_tile_columns_minus1;i++){
			colWidth[i] = ( (i+1)*sequence_param->PicWidthInCtbsY )/ (picture_param->num_tile_columns_minus1 +1) - ( i*sequence_param->PicWidthInCtbsY )/ (picture_param->num_tile_columns_minus1 +1);
		}
	} else {
		colWidth[picture_param->num_tile_columns_minus1] = sequence_param->PicWidthInCtbsY;
		for (i=0;i<picture_param->num_tile_columns_minus1;i++){
			colWidth[i] = picture_param->column_width_minus1[i] +1;
			colWidth[picture_param->num_tile_columns_minus1] -= colWidth[i];
		}

	}
	for (colBd[0]=0,i=0;i<=picture_param->num_tile_columns_minus1;i++)
		colBd[i+1] = colBd[i] + colWidth[i];

	//rowHeight = (int*) malloc(sizeof(int)* (picture_param->num_tile_rows_minus1+1) );
	//rowBd =(int*) malloc(sizeof(int)* (picture_param->num_tile_rows_minus1+2) );
	if (picture_param->uniform_spacing_flag){
		for(i=0;i<=picture_param->num_tile_rows_minus1;i++){
			rowHeight[i] = ( (i+1)*sequence_param->PicHeightInCtbsY )/ (picture_param->num_tile_rows_minus1 +1) - ( i*sequence_param->PicHeightInCtbsY )/ (picture_param->num_tile_rows_minus1 +1);
		}
	} else {
		rowHeight[picture_param->num_tile_rows_minus1] = sequence_param->PicHeightInCtbsY;
		for (i=0;i<picture_param->num_tile_rows_minus1;i++){
			rowHeight[i] = picture_param->row_height_minus1[i] +1;
			rowHeight[picture_param->num_tile_rows_minus1] -= rowHeight[i];
		}
	}
	for (rowBd[0]=0,i=0;i<=picture_param->num_tile_rows_minus1;i++)
		rowBd[i+1] = rowBd[i] + rowHeight[i];
	int j;

	int ctbAddrTS=0;
	for (i=0;i<=picture_param->num_tile_rows_minus1;i++){
		for (j=0;j<=picture_param->num_tile_columns_minus1;j++){
			firstCtbTS[i][j] = ctbAddrTS;
			firstCtbRS[i][j] = rowBd[i]*sequence_param->PicWidthInCtbsY + colBd[j];
			ctbAddrTS+= (colWidth[j]*rowHeight[i]);
		}
	}

//	CtbAddrRstoTs =(short*) malloc(sizeof(int)*sequence_param->PicSizeInCtbsY);
	int ctbAddrRS;
	for (ctbAddrRS=0;ctbAddrRS<sequence_param->PicSizeInCtbsY;ctbAddrRS++){
		int tbX = ctbAddrRS % sequence_param->PicWidthInCtbsY;
		int tbY = ctbAddrRS / sequence_param->PicWidthInCtbsY;
		int tileX, tileY;
		for (i=0;i<=picture_param->num_tile_columns_minus1;i++)
			if (tbX >= colBd[i])
				tileX = i;
		for (i=0;i<=picture_param->num_tile_rows_minus1;i++)
			if (tbY >= rowBd[i])
				tileY = i;
		CtbAddrRstoTs[ctbAddrRS] = 0;
		for (i=0;i<tileX;i++)
			CtbAddrRstoTs[ctbAddrRS] += (rowHeight[tileY] * colWidth[i]);
		for (i=0;i<tileY;i++)
			CtbAddrRstoTs[ctbAddrRS] += (rowHeight[i] * sequence_param->PicWidthInCtbsY);
		CtbAddrRstoTs[ctbAddrRS] += (tbY-rowBd[tileY])*colWidth[tileX] + tbX - colBd[tileX];
//		printf("CtbAddrRS=%d TS=%d\n",ctbAddrRS, CtbAddrRstoTs[ctbAddrRS]);
	}
	//CtbAddrTstoRs =(int*) malloc(sizeof(int)*sequence_param->PicSizeInCtbsY);
	for (ctbAddrRS=0;ctbAddrRS<sequence_param->PicSizeInCtbsY;ctbAddrRS++){
		CtbAddrTstoRs[CtbAddrRstoTs[ctbAddrRS]] =  ctbAddrRS;
//		printf("CtbAddrTS=%d RS=%d\n",ctbAddrRS, CtbAddrTstoRs[ctbAddrRS]);
	}
//	TileId =(short*) malloc(sizeof(short)*PicSizeInCtbsY);
	int  tileIdx,x,y;
	for (j=0,tileIdx=0;j<=picture_param->num_tile_rows_minus1;j++){
		for (i=0;i<=picture_param->num_tile_columns_minus1;i++,tileIdx++){
			for (y=rowBd[j];y<rowBd[j+1];y++){
				for (x = colBd[i];x<colBd[i+1];x++){
					TileId[CtbAddrRstoTs[y*sequence_param->PicWidthInCtbsY+x]] = tileIdx;
				}
			}
		}
	}

	struct parameters_0 h_00;
	*(( int*) (&h_00)) = 0;
	h_00.header = 0x00;
	h_00.pic_width = sequence_param->pic_width_in_luma_samples;
	h_00.pic_height = sequence_param->pic_height_in_luma_samples;
	fwrite(&h_00,4,1,cabac_to_residual);
	axi_write(0x0500,(void *) &h_00);
	
	struct parameters_1 h_01;
	*(( int*) (&h_01)) = 0;
	h_01.header = 0x01;
	h_01.Log2CtbSizeY = sequence_param->Log2CtbSizeY;
	h_01.constrained_intra_pred = picture_param->constrained_intra_pred_flag;
	h_01.pps_cb_qp_offset = picture_param->pps_cb_qp_offset;
	h_01.pps_cr_qp_offset = picture_param->pps_cr_qp_offset;
	h_01.pps_beta_offset_div2 = picture_param->pps_beta_offset_div2;
	h_01.pps_tc_offset_div2 = picture_param->pps_tc_offset_div2;
	h_01.strong_intra_smoothing = sequence_param->strong_intra_smoothing_enable_flag;
	h_01.pcm_loop_filter_disable_flag = sequence_param->pcm_loop_filter_disable_flag;
	fwrite(&h_01,4,1,cabac_to_residual);
	axi_write(0x0500,(void *) &h_01);

	struct parameters_2 h_02;
	*(( int*) (&h_02)) = 0;
	h_02.header = 0x02;
	h_02.num_short_term_ref = sequence_param->num_short_term_ref_pic_sets;
	h_02.weighted_pred = picture_param->weighted_pred_flag;
	h_02.weighted_bipred = picture_param->weighted_bipred_flag;
	h_02.parallel_merge_level = picture_param->log2_parallel_merge_level_minus2;
	h_02.loop_filter_across_tiles_enabled_flag = picture_param->loop_filter_across_tiles_enabled_flag;
	h_02.scaling_list_enable_flag = sequence_param->scaling_list_enable_flag;
	fwrite(&h_02,4,1,cabac_to_residual);
	axi_write(0x0500,(void *) &h_02);


	for (i=0;i<sequence_param->num_short_term_ref_pic_sets;i++){
		struct parameters_3 h_03;
		*(( int*) (&h_03)) = 0;
		h_03.header = 0x03;
		h_03.rps_id = i;
		h_03.num_negative = (sequence_param->stRPS[i].NumNegativePics  );
		h_03.num_positive = (sequence_param->stRPS[i].NumPositivePics  );
		fwrite(&h_03,4,1,cabac_to_residual); axi_write(0x0500,(void *) &h_03);
		for (j=0;j<sequence_param->stRPS[i].NumPositivePics;j++){
			struct parameters_4 h_04;
			h_04.header = 0x04;
			h_04.rps_id = i;
			h_04.used = sequence_param->stRPS[i].UsedByCurrPicS1[j];
			h_04.delta_POC = sequence_param->stRPS[i].DeltaPocS1[j];
			fwrite(&h_04,4,1,cabac_to_residual); axi_write(0x0500,(void *) &h_04);
		}
		for (j=0;j<sequence_param->stRPS[i].NumNegativePics;j++){
			struct parameters_4 h_04;
			h_04.header = 0x04;
			h_04.rps_id = i;
			h_04.used = sequence_param->stRPS[i].UsedByCurrPicS0[j];
			h_04.delta_POC = sequence_param->stRPS[i].DeltaPocS0[j];
			fwrite(&h_04,4,1,cabac_to_residual); axi_write(0x0500,(void *) &h_04);
		}
	}

	axi_write(0x0010, sequence_param->pic_width_in_luma_samples);
	axi_write(0x0020, sequence_param->pic_height_in_luma_samples);
	axi_write(0x01a0, sequence_param->Log2CtbSizeY);
	axi_write(0x01b0, sequence_param->Log2MinCbSizeY);
	axi_write(0x01c0, sequence_param->Log2MaxTrafoSize);
	axi_write(0x01d0, sequence_param->Log2MinTrafoSize);

	axi_write(0x00d0, sequence_param->max_transform_hierarchy_depth_inter);
	axi_write(0x00e0, sequence_param->max_transform_hierarchy_depth_intra);
	axi_write(0x0080, sequence_param->amp_enabled_flag);
	axi_write(0x01e0, sequence_param->pcm_enabled_flag);

	if (sequence_param->pcm_enabled_flag) {
		axi_write(0x01f0, sequence_param->PCMBitDepthY);
		axi_write(0x0200, sequence_param->PCMBitDepthC);
		axi_write(0x0210, sequence_param->Log2MinIpcmCbSizeY);
		axi_write(0x0220, sequence_param->Log2MaxIpcmCbSizeY);
	}

	axi_write(0x0050, picture_param->sign_data_hiding_flag);
	axi_write(0x0070, picture_param->transform_skip_enabled_flag);
	axi_write(0x0110, picture_param->cu_qp_delta_enabled_flag);
	axi_write(0x0120, sequence_param->Log2MinCuQpDeltaSize);
	axi_write(0x0040, picture_param->transquant_bypass_enable_flag);
	axi_write(0x0130, picture_param->entropy_coding_sync_enabled_flag);


//	fclose(cabac_to_intra);
	//fclose(cabac_to_residual); exit(0);
}


void initialize_mmaps(){


	int i=0;
	for (i=0;i<27;i++){
		cabac_current.contexts[i] = (cabac_context_struct*) malloc(sizeof(cabac_context_struct)*(cabac_table_maxctxIDx[i]+1));
		saved_context[i] = (cabac_context_struct*) malloc(sizeof(cabac_context_struct)*(cabac_table_maxctxIDx[i]+1));
	}
	printf("mmap error %d\n",errno);
} 


void send_tu_residual_v2(unsigned short xB,unsigned short  yB, int log2TrafoSize,int cidx, BYTE mode, int res_present){
//	unsigned int b0 = 0x99 << 24 | (xB << 12) | yB;
//	fwrite(&b0,4,1,cabac_to_intra);
	*(( int*) (&h_40)) = 0;
	h_40.header = 0x40;
	switch (sequence_param->Log2CtbSizeY) {
	case 6 :
		h_40.xT = ( xB % 64)>>1;
		h_40.yT = ( yB % 64)>>1;
		break;
	case 5 :
		h_40.xT = ( xB % 32)>>1;
		h_40.yT = ( yB % 32)>>1;
		break;
	case 4 :
		h_40.xT = ( xB % 16)>>1;
		h_40.yT = ( yB % 16)>>1;
		break;

	}
	h_40.size = log2TrafoSize;
	h_40.cidx = cidx;
	h_40.IntraMode = mode;
	h_40.res_present = res_present;

}

void axi_write(unsigned int addr, void * data){
	fwrite(&addr,4,1,axi_file);
	fwrite(data,4,1,axi_file);
}
void axi_write(unsigned int addr, unsigned int data){
	fwrite(&addr,4,1,axi_file);
	fwrite(&data,4,1,axi_file);
}


unsigned int log2_int(unsigned int n){
	if (n<=2) return 1;
	if (n>=3 && n<= 4) return 2;
	if (n>=5 && n<= 8) return 3;
	if (n>=9 && n<= 16) return 4;
	if (n>=17 && n<= 32) return 5;
	if (n>=33 && n<= 64) return 6;
	if (n>=65 && n<= 128) return 7;
	if (n>=129 && n<= 256) return 8;
	if (n>=257 && n<= 512) return 9;
	if (n>=513 && n<= 1024) return 10;
	if (n>=1025 && n<= 2048) return 11;
	return 0;
}