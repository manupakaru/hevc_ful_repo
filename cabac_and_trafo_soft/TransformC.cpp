
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
//#include "packets.h"
#include "defines.h"
#define FILE_PATH "G:\\HEVC\\4KSoft\\output"

//#define GT_PRESENT 1

FILE *CabactoRes;
//FILE *RestoIntra;
FILE *RestoInter;
FILE *RestoInterY;
FILE *RestoInterCb;
FILE *RestoInterCr;

#ifdef GT_PRESENT
FILE *gt_file;
#endif

struct parameters_1 PPS;
struct parameters_2 PPS_2;
struct scaling_list_pkt sl;
struct slice_2 Slice;
struct CTU_0 CTU;
struct CU_0 CU;
struct RU_0 RU0;
struct RU_1 RU1;
int POC = -1;
int QP;
const int transMatrix[32][32] =
{
  { 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64},
  { 90, 90, 88, 85, 82, 78, 73, 67, 61, 54, 46, 38, 31, 22, 13,  4, -4,-13,-22,-31,-38,-46,-54,-61,-67,-73,-78,-82,-85,-88,-90,-90},
  { 90, 87, 80, 70, 57, 43, 25,  9, -9,-25,-43,-57,-70,-80,-87,-90,-90,-87,-80,-70,-57,-43,-25, -9,  9, 25, 43, 57, 70, 80, 87, 90},
  { 90, 82, 67, 46, 22, -4,-31,-54,-73,-85,-90,-88,-78,-61,-38,-13, 13, 38, 61, 78, 88, 90, 85, 73, 54, 31,  4,-22,-46,-67,-82,-90},
  { 89, 75, 50, 18,-18,-50,-75,-89,-89,-75,-50,-18, 18, 50, 75, 89, 89, 75, 50, 18,-18,-50,-75,-89,-89,-75,-50,-18, 18, 50, 75, 89},
  { 88, 67, 31,-13,-54,-82,-90,-78,-46, -4, 38, 73, 90, 85, 61, 22,-22,-61,-85,-90,-73,-38,  4, 46, 78, 90, 82, 54, 13,-31,-67,-88},
  { 87, 57,  9,-43,-80,-90,-70,-25, 25, 70, 90, 80, 43, -9,-57,-87,-87,-57, -9, 43, 80, 90, 70, 25,-25,-70,-90,-80,-43,  9, 57, 87},
  { 85, 46,-13,-67,-90,-73,-22, 38, 82, 88, 54, -4,-61,-90,-78,-31, 31, 78, 90, 61,  4,-54,-88,-82,-38, 22, 73, 90, 67, 13,-46,-85},
  { 83, 36,-36,-83,-83,-36, 36, 83, 83, 36,-36,-83,-83,-36, 36, 83, 83, 36,-36,-83,-83,-36, 36, 83, 83, 36,-36,-83,-83,-36, 36, 83},
  { 82, 22,-54,-90,-61, 13, 78, 85, 31,-46,-90,-67,  4, 73, 88, 38,-38,-88,-73, -4, 67, 90, 46,-31,-85,-78,-13, 61, 90, 54,-22,-82},
  { 80,  9,-70,-87,-25, 57, 90, 43,-43,-90,-57, 25, 87, 70, -9,-80,-80, -9, 70, 87, 25,-57,-90,-43, 43, 90, 57,-25,-87,-70,  9, 80},
  { 78, -4,-82,-73, 13, 85, 67,-22,-88,-61, 31, 90, 54,-38,-90,-46, 46, 90, 38,-54,-90,-31, 61, 88, 22,-67,-85,-13, 73, 82,  4,-78},
  { 75,-18,-89,-50, 50, 89, 18,-75,-75, 18, 89, 50,-50,-89,-18, 75, 75,-18,-89,-50, 50, 89, 18,-75,-75, 18, 89, 50,-50,-89,-18, 75},
  { 73,-31,-90,-22, 78, 67,-38,-90,-13, 82, 61,-46,-88, -4, 85, 54,-54,-85,  4, 88, 46,-61,-82, 13, 90, 38,-67,-78, 22, 90, 31,-73},
  { 70,-43,-87,  9, 90, 25,-80,-57, 57, 80,-25,-90, -9, 87, 43,-70,-70, 43, 87, -9,-90,-25, 80, 57,-57,-80, 25, 90,  9,-87,-43, 70},
  { 67,-54,-78, 38, 85,-22,-90,  4, 90, 13,-88,-31, 82, 46,-73,-61, 61, 73,-46,-82, 31, 88,-13,-90, -4, 90, 22,-85,-38, 78, 54,-67},
  { 64,-64,-64, 64, 64,-64,-64, 64, 64,-64,-64, 64, 64,-64,-64, 64, 64,-64,-64, 64, 64,-64,-64, 64, 64,-64,-64, 64, 64,-64,-64, 64},
  { 61,-73,-46, 82, 31,-88,-13, 90, -4,-90, 22, 85,-38,-78, 54, 67,-67,-54, 78, 38,-85,-22, 90,  4,-90, 13, 88,-31,-82, 46, 73,-61},
  { 57,-80,-25, 90, -9,-87, 43, 70,-70,-43, 87,  9,-90, 25, 80,-57,-57, 80, 25,-90,  9, 87,-43,-70, 70, 43,-87, -9, 90,-25,-80, 57},
  { 54,-85, -4, 88,-46,-61, 82, 13,-90, 38, 67,-78,-22, 90,-31,-73, 73, 31,-90, 22, 78,-67,-38, 90,-13,-82, 61, 46,-88,  4, 85,-54},
  { 50,-89, 18, 75,-75,-18, 89,-50,-50, 89,-18,-75, 75, 18,-89, 50, 50,-89, 18, 75,-75,-18, 89,-50,-50, 89,-18,-75, 75, 18,-89, 50},
  { 46,-90, 38, 54,-90, 31, 61,-88, 22, 67,-85, 13, 73,-82,  4, 78,-78, -4, 82,-73,-13, 85,-67,-22, 88,-61,-31, 90,-54,-38, 90,-46},
  { 43,-90, 57, 25,-87, 70,  9,-80, 80, -9,-70, 87,-25,-57, 90,-43,-43, 90,-57,-25, 87,-70, -9, 80,-80,  9, 70,-87, 25, 57,-90, 43},
  { 38,-88, 73, -4,-67, 90,-46,-31, 85,-78, 13, 61,-90, 54, 22,-82, 82,-22,-54, 90,-61,-13, 78,-85, 31, 46,-90, 67,  4,-73, 88,-38},
  { 36,-83, 83,-36,-36, 83,-83, 36, 36,-83, 83,-36,-36, 83,-83, 36, 36,-83, 83,-36,-36, 83,-83, 36, 36,-83, 83,-36,-36, 83,-83, 36},
  { 31,-78, 90,-61,  4, 54,-88, 82,-38,-22, 73,-90, 67,-13,-46, 85,-85, 46, 13,-67, 90,-73, 22, 38,-82, 88,-54, -4, 61,-90, 78,-31},
  { 25,-70, 90,-80, 43,  9,-57, 87,-87, 57, -9,-43, 80,-90, 70,-25,-25, 70,-90, 80,-43, -9, 57,-87, 87,-57,  9, 43,-80, 90,-70, 25},
  { 22,-61, 85,-90, 73,-38, -4, 46,-78, 90,-82, 54,-13,-31, 67,-88, 88,-67, 31, 13,-54, 82,-90, 78,-46,  4, 38,-73, 90,-85, 61,-22},
  { 18,-50, 75,-89, 89,-75, 50,-18,-18, 50,-75, 89,-89, 75,-50, 18, 18,-50, 75,-89, 89,-75, 50,-18,-18, 50,-75, 89,-89, 75,-50, 18},
  { 13,-38, 61,-78, 88,-90, 85,-73, 54,-31,  4, 22,-46, 67,-82, 90,-90, 82,-67, 46,-22, -4, 31,-54, 73,-85, 90,-88, 78,-61, 38,-13},
  {  9,-25, 43,-57, 70,-80, 87,-90, 90,-87, 80,-70, 57,-43, 25, -9, -9, 25,-43, 57,-70, 80,-87, 90,-90, 87,-80, 70,-57, 43,-25,  9},
  {  4,-13, 22,-31, 38,-46, 54,-61, 67,-73, 78,-82, 85,-88, 90,-90, 90,-90, 88,-85, 82,-78, 73,-67, 61,-54, 46,-38, 31,-22, 13, -4}
};
const int sinetransMatrix[4][4] = {
		  {29,   55,    74,   84},
		  {74,   74,    0 ,  -74},
		  {84,  -29,   -74,   55},
		  {55,  -84,    74,  -29},
		};
const int levelscale[6] = {40,45,51,57,64,72};

int d[32][32];
int scaling_list_enable_flag = 0;
int ScalingList[4][6][64];
int DC_Scale [2][6];

int invScanOrder_0 [4][4];
int invScanOrder_1 [8][8];

int stat_frame_coeffs;
int stat_frame_col_coeffs;
int stat_total_coeffs;
int stat_frame_count;

void initialize_scanOrder(){
	int block_size;
	for (block_size=2;block_size<4;block_size++){
		// diag
		int i=0,x=0,y=0;
		int stopLoop = 0;
		int blk_size = (1<<block_size);
		while(!stopLoop){
			while (y>=0){
				if (x<blk_size && y<blk_size){
					//ScanOrder[block_size][0][i][0] = x;
					//ScanOrder[block_size][0][i][1] = y;
					if (block_size ==2)
						invScanOrder_0 [x][y] = i;
					else 
						invScanOrder_1 [x][y] = i;
//					printf("size %d %d (%d,%d)\n",blk_size,i,x,y);
					i++;
				}
				y--;
				x++;
			}
			y=x;
			x=0;
			if (i>=blk_size*blk_size) stopLoop = 1;
		}
		
	}
}

void oneDtransform(int trType, int nT, int *x, int* y)
{
//	int transMatrix[32][32];
//	int sinetransMatrix[4][4];
	int i,j;


	if(trType==1)
		{
			for(i=0;i<nT;i++)
				{
					int t = 0;
					for(j=0;j<nT;j++){
						t += (sinetransMatrix[j][i]* x[j]);
						//printf("%dx%d ",sinetransMatrix[j][i],x[j]);
					}
					//printf("%\n")
					y[i] = t;
				}
		}
	else
		{
			for(i=0;i<nT;i++)
				{
					int t =0;
					for(j=0;j<nT;j++)
						t += (transMatrix[32*j/nT][i]* x[j] );
					y[i] = t;
				}
		}

}

void scaling(int nT,int cIdx, int qP)
{
	int BitDepthY=8,BitDepthC=8,bdShift,i,j,temp;
	int levelscale[6] = {40,45,51,57,64,72};
	int m=16;  // scaling factor
	int log2_nT;
	switch(nT){
	case 4: log2_nT = 2; break;
	case 8: log2_nT = 3; break;
	case 16: log2_nT = 4; break;
	case 32: log2_nT = 5; break;
	}
	if(cIdx==0)
		bdShift=BitDepthY+log2_nT-5;
	else
		bdShift=BitDepthC+log2_nT-5;

	for(i=0;i<nT;i++)
		{
			for(j=0;j<nT;j++)
				{
					temp = ((d[i][j] * m * (levelscale[qP%6] <<(qP/6)))+(1<<(bdShift-1))) >> bdShift;
					if(temp>32767)
						temp=32767;
					else if(temp<-32768)
						temp=-32768;

					d[i][j] = temp;
				}
		}
}

void transformation(int CuPredMode, int nT, int cIdx)
{
	int trType=0,i,j;
	int x[32]; int y[32];

	if(CuPredMode==1 && nT==4 && cIdx==0)
		trType = 1;

	for(j=0;j<nT;j++)
		{
			for(i=0;i<nT;i++)
				x[i]= d[i][j];
			oneDtransform(trType,nT,x,y);

			for(i=0;i<nT;i++)
				{
					y[i] = (y[i]+64)>>7;
					if(y[i]>32767)
						y[i]=32767;
					if(y[i]<-32768)
						y[i]=-32768;
					d[i][j] = y[i];
				}

		}
	//printf("After 1D trans %d %d\n",d[1][1],d[1][2]);
//	for(i=0;i<nT;i++) {
//			for(j=0;j<nT;j++) {
//				//d[i][j] = (d[i][j] + (1<<(bdShift-1)))>>bdShift;
//				printf("%3d ",d[i][j]);
//			}
//			printf("\n");
//		}
//	const int bdShift = 12;
	for(i=0;i<nT;i++) {
		for(j=0;j<nT;j++)
			x[j]= d[i][j];
		oneDtransform(trType,nT,x,y);
		for(j=0;j<nT;j++)
			d[i][j] = y[j];
	}



}

void blockScaleTransform(int nT,int qP,int cIdx,int transform_skip_flag,int CuPredMode)
{
	int bdShift=12;	// hardcoded
	int i,j;

	scaling(nT,cIdx,qP);
	//printf("after scale %d %d\n",d[1][1], d[1][2]);
	if(transform_skip_flag == 0)
		transformation(CuPredMode,nT,cIdx);
	else {
		for(i=0;i<nT;i++) {
			for(j=0;j<nT;j++) {
					d[i][j] <<= 7;
			}
		}
	}

	for(i=0;i<nT;i++) {
		for(j=0;j<nT;j++) {
			d[i][j] = (d[i][j] + (1<<(bdShift-1)))>>bdShift;
//			printf("%3d ",d[i][j]);
		}
//		printf("\n");
	}
	//printf("final %d %d\n",d[1][1], d[1][2]);

}
short resSamplesL[64*64];
short resSamplesCb[64*64];
short resSamplesCr[64*64];

int cts_res_count[3];
int max_res_count[3];

int transquant_bypass;

// stats
int TUSizeCount[4];
int ColCount[32];


//int sum_mode_intra;
int ResidualDecodeTU()
{
	while(fread(&RU0,4,1,CabactoRes)==0);

	int log2TUsize = RU0.size;
	int TUsize = 1<<RU0.size;
	int xT = (RU0.xT<<1)-CU.x0;
	int yT = (RU0.yT<<1)-CU.y0;
	int CbSize = 1<<CU.log2_cb_size;

	int trType = (CU.predMode == 0 && TUsize==4 && RU0.cidx==0);
	int x0 = CTU.xC + RU0.xT*2;
	int y0 = CTU.yC + RU0.yT*2;

	int sc_sizeId = log2TUsize -2;
	int sc_matrixId;
	if (sc_sizeId == 3)
		sc_matrixId = CU.predMode;
	else 
		sc_matrixId = CU.predMode * 3 + RU0.cidx;
	
	int stat_coeff=0;
	int stat_col_coeff=0;

	if (RU0.res_present) {
		
		if (cts_res_count[RU0.cidx] > max_res_count[RU0.cidx]) max_res_count[RU0.cidx]=cts_res_count[RU0.cidx];
		cts_res_count[RU0.cidx]=0;
		TUSizeCount[RU0.size-2]++;
	} else {
		cts_res_count[RU0.cidx]++;
	}
	
	if (RU0.cidx!=0) {
		RU0.size++;
	}
	fwrite(&RU0,4,1,RestoInter);

	if (RU0.cidx==0){
			while(fread(&RU1,4,1,CabactoRes)==0);
			QP = RU1.QP;
			fwrite(&RU1,4,1,RestoInter);
		}
	int i=0,j=0;

	short* ResSamples;


	int qPi=0;
	switch (RU0.cidx){
	case 0:
		qPi = QP;
		ResSamples =(short *) resSamplesL; break;
	case 1:
		qPi = QP + PPS.pps_cb_qp_offset + Slice.slice_cb_qp_offset;

		ResSamples =(short *) resSamplesCb; xT/=2; yT/=2; CbSize/=2; break;
	case 2:
		qPi = QP + PPS.pps_cr_qp_offset + Slice.slice_cr_qp_offset;
		ResSamples =(short *) resSamplesCr; xT/=2; yT/=2; CbSize/=2 ;break;
	}
	if (qPi>57) qPi = 57; if (qPi<0) qPi = 0;
	int RU_QP = qPi;
	if (RU0.cidx>0) {
		if (qPi>=30) RU_QP--; // -1
		if (qPi>=35) RU_QP--; //-2
		if (qPi>=37) RU_QP--; //-3
		if (qPi>=39) RU_QP--; //-4
		if (qPi>=41) RU_QP--; //-5
		if (qPi>=43) RU_QP--; //-6
	}
	int m=16;  // scaling factor
	int bdShift = 8 + log2TUsize-5;


	if (RU0.res_present) {
		memset(d,0,32*32*sizeof(int));
		int filled_cols[32] = {0};
		// read each packet, if header != fc, set pointer back and proceed to row transform
		coeff c;
		while(fread(&c,4,1,CabactoRes)==0);
		int filledCol[32] = {0};
		int nCols= 0;
		while (c.header == 63){		
			stat_coeff++;
			int scaled = c.val;
			if (!filledCol[c.y]){
				filledCol[c.y]=1;
				nCols++;
			}
			if (!scaling_list_enable_flag) {
				m = 16;
			} else {
				if (log2TUsize >= 4 && c.x==0 && c.y==0) {
					m = DC_Scale[log2TUsize-4][sc_matrixId];
				} else {
					int xm = (TUsize == 32) ? c.x / 4 : ((TUsize == 16) ? c.x / 2 : c.x);
					int ym = (TUsize == 32) ? c.y / 4 : ((TUsize == 16) ? c.y / 2 : c.y);
					int sc_i = TUsize == 4 ? invScanOrder_0[xm][ym] : invScanOrder_1[xm][ym];
					m = ScalingList[sc_sizeId][sc_matrixId][sc_i];
				}
			}
			if (!transquant_bypass) {
				scaled = ((scaled * m * (levelscale[RU_QP%6] <<(RU_QP/6)))+(1<<(bdShift-1))) >> bdShift;
			}
			if (scaled > 32767) scaled = 32767;
			if (scaled < - 32768) scaled = -32768;
			if (!transquant_bypass && !RU0.transform_skip_flag) {
				for (i=0;i<TUsize;i++){
					if (!trType)
						d[i][c.x] += scaled * transMatrix[c.y*32/TUsize][i] ;
					else 
						d[i][c.x] += scaled * sinetransMatrix[c.y][i];
				}
			} else {
				if (RU0.transform_skip_flag)
					d[c.y][c.x] = scaled << 7;
				else 
					d[c.y][c.x] = scaled;
			}
			filled_cols[c.x] = 1;
			while(fread(&c,4,1,CabactoRes)==0);
		}
		if (c.header !=62) {
			printf("Error with 62 packet\n");
			while(1);
		}
		stat_col_coeff = nCols*TUsize;
		//stat_frame_coeffs +=3;
		stat_frame_coeffs+= stat_coeff;
		stat_frame_col_coeffs+= stat_col_coeff;
		
		if (!transquant_bypass && !RU0.transform_skip_flag) {
			// intermediate clipping
			for (j=0;j<TUsize;j++){
				if (filled_cols[j]){
					for (i=0;i<TUsize;i++){
						d[i][j] = (d[i][j] + 64)>>7;
						if (d[i][j] >  32767) d[i][j] =  32767;
						if (d[i][j] < -32768) d[i][j] = -32768;
					}
				}
			}
			// row transform
		
			for (i=0;i<TUsize;i++){
				int row[32];
				for (j=0;j<TUsize;j++) {
					row[j] = d[i][j];
					d[i][j] = 0;
				}
				for (j=0;j<TUsize;j++){
					if (filled_cols[j]){
						if (!trType)
							for (int k=0;k<TUsize;k++)
								//d[i][k] += row[j] * transMatrix[j][k*32/TUsize] ;
								d[i][k] += row[j] * transMatrix[j*32/TUsize][k] ;
						else 
							for (int k=0;k<TUsize;k++)
								d[i][k] += row[j] * sinetransMatrix[j][k];
						}
				}
			}
		}
		// final clipping
		if (!transquant_bypass) {
			for (j=0;j<TUsize;j++){
				for (i=0;i<TUsize;i++){
					d[i][j] = (d[i][j] + 2048)>>12;
				}	
			}
		}

		//fseek(CabactoRes,-4,SEEK_CUR);
		
		
		for(i=0;i<TUsize;i++)
		{
			for(j=0;j<TUsize;j++)
				{
					//int offset = (CbSize*yT)+xT+(CbSize*i)+j;
					ResSamples[i*TUsize+j] =(short) d[i][j];
				}
		}
		
		// Reorder

		if (RU0.cidx ==0) {
			int a=0; int b=0;
			for (i=0;i<TUsize;i+=8) {
				for (j=0;j<TUsize;j+=8) {
					int s = (TUsize>4?8:4);
					for (a=i; a<i+s;a+=4){
						for (b=j; b<j+s;b+=4) {
							struct fifo_element fe;
							//struct fifo_element fe;
							fe.a0 = d[a][b]; fe.a1 = d[a+1][b]; fe.a2 = d[a+2][b]; fe.a3 = d[a+3][b];
							fe.b0 = d[a][b+1]; fe.b1 = d[a+1][b+1]; fe.b2 = d[a+2][b+1]; fe.b3 = d[a+3][b+1];
							fe.c0 = d[a][b+2]; fe.c1 = d[a+1][b+2]; fe.c2 = d[a+2][b+2]; fe.c3 = d[a+3][b+2];
							fe.d0 = d[a][b+3]; fe.d1 = d[a+1][b+3]; fe.d2 = d[a+2][b+3]; fe.d3 = d[a+3][b+3];
							fwrite(&fe,1,24,RestoInterY);
							short hex[36] = {0};
							int k;
							for (k=0;k<4;k++) {
								hex[9*k+0] = d[a+k][b]>>5;
								hex[9*k+1] = d[a+k][b]>>1;
								hex[9*k+2] = (d[a+k][b] & 1) << 3 | ((d[a+k][b+1] >> 6) & 7);
								hex[9*k+3] = d[a+k][b+1] >> 2;
								hex[9*k+4] = (d[a+k][b+1] & 3) << 2 | ((d[a+k][b+2] >> 7) & 3);
								hex[9*k+5] = d[a+k][b+2] >> 3;
								hex[9*k+6] = (d[a+k][b+2] & 7) << 1 | ((d[a+k][b+3] >> 8) & 1);
								hex[9*k+7] = d[a+k][b+3] >> 4;
								hex[9*k+8] = d[a+k][b+3];
							}
						}
					}
					
				}
			}
		} else {
			int a=0; int b=0;
			for (a=0; a<TUsize;a+=4){
				for (b=0; b<TUsize;b+=4) {
					struct fifo_element fe;
					fe.a0 = d[a][b]; fe.a1 = d[a+1][b]; fe.a2 = d[a+2][b]; fe.a3 = d[a+3][b];
					fe.b0 = d[a][b+1]; fe.b1 = d[a+1][b+1]; fe.b2 = d[a+2][b+1]; fe.b3 = d[a+3][b+1];
					fe.c0 = d[a][b+2]; fe.c1 = d[a+1][b+2]; fe.c2 = d[a+2][b+2]; fe.c3 = d[a+3][b+2];
					fe.d0 = d[a][b+3]; fe.d1 = d[a+1][b+3]; fe.d2 = d[a+2][b+3]; fe.d3 = d[a+3][b+3];
					if (RU0.cidx==1)
						fwrite(&fe,1,24,RestoInterCb);
					else 
						fwrite(&fe,1,24,RestoInterCr);
				}
			}
		}

	} 

#ifdef GT_PRESENT
	// check with ground_truth

	

	int frame_start = POC*gt_width*gt_height*3;
	int plane_start = frame_start;
	if (RU0.cidx>0) {
		plane_start += gt_width*gt_height*2;
	}
	if (RU0.cidx >1 ) {
		plane_start += gt_width*gt_height/2;
	}
	short row[32];
	int mismatch = 0;
	if (RU0.res_present && POC <= 20) {
		for (i=0;i<TUsize;i++){
			int row_start;
			if (RU0.cidx ==0)
				row_start = plane_start + (gt_width*(y0+i) + x0) * 2;
			else
				row_start = plane_start + (gt_width*(y0/2+i) + x0) ;
			fseek(gt_file,row_start,SEEK_SET);
			fread(row,2,TUsize,gt_file);
			for (j=0;j<TUsize;j++) {
				if (d[i][j] != row[j]){
					mismatch = 1;
				}
			}
		}
		if (mismatch & ! CU.pcm) {
			printf("Mismatch at (%d,%d) cidx %d QP %d\n",x0,y0, RU0.cidx,RU_QP);
			while(1);
		} else if (!mismatch) {
			//printf("Success at (%d,%d) cidx %d\n",x0,y0, RU0.cidx);
		} else {
			printf("PCM at (%d,%d) cidx %d\n",x0,y0, RU0.cidx);
		}
	}
#endif
	return (TUsize * TUsize);
}
int frameCount = 0;
void ResidualDecodeCU()
{

	memset(resSamplesL,0,sizeof(short)*64*64);
	memset(resSamplesCb,0,sizeof(short)*64*64);
	memset(resSamplesCr,0,sizeof(short)*64*64);

	int cu_size = (1<<CU.log2_cb_size)*(1<<CU.log2_cb_size)*3/2;
	while(cu_size>0){
		cu_size -= ResidualDecodeTU();
	}
}

int Test()
{
	//while(fread(&RU0,4,1,CabactoRes)==0);

	int log2TUsize = 2;
	int TUsize = 4;
	int xT = 0;
	int yT =0;
//	int CbSize = 1<<CU.log2_cb_size;

	int trType = 0;
	int transform_skip_flag = 0;
	
	int i=0,j=0;

	short* ResSamples;


	int qPi=0;

	int RU_QP = 22;
	int m=16;  // scaling factor
	int bdShift = 8 + log2TUsize-5;

	coeff c1 = {63,1,3,-2};
	coeff c2 = {63,2,1,3};
	coeff c3 = {63,1,0,1};
	coeff c4 = {0, 1, 0,2};
	coeff cc[4] = {c1, c2, c3, c4};
	int cf  = 0;
	if (1) {
		memset(d,0,32*32*sizeof(int));
		int filled_cols[32] = {0};
		// read each packet, if header != fc, set pointer back and proceed to row transform
		
		coeff c = cc[cf++];
		int filledCol[32] = {0};
		int nCols= 0;
		while (c.header == 63){		
			int scaled = c.val;
			if (!filledCol[c.y]){
				filledCol[c.y]=1;
				nCols++;
			}
			
			if (!transquant_bypass) {
				scaled = ((scaled * m * (levelscale[RU_QP%6] <<(RU_QP/6)))+(1<<(bdShift-1))) >> bdShift;
			}
			if (scaled > 32767) scaled = 32767;
			if (scaled < - 32768) scaled = -32768;
			if (!transquant_bypass) {
				for (i=0;i<TUsize;i++){
					if (!trType)
						d[i][c.x] += scaled * transMatrix[c.y*32/TUsize][i] ;
					else 
						d[i][c.x] += scaled * sinetransMatrix[c.y][i];
				}
			} else {
				if (transform_skip_flag)
					d[c.y][c.x] = scaled << 7;
				else 
					d[c.y][c.x] = scaled;
			}
			filled_cols[c.x] = 1;
			c = cc[cf++];
		}
		
		if (!transquant_bypass && !RU0.transform_skip_flag) {
			// intermediate clipping
			for (j=0;j<TUsize;j++){
				if (filled_cols[j]){
					for (i=0;i<TUsize;i++){
						d[i][j] = (d[i][j] + 64)>>7;
						if (d[i][j] >  32767) d[i][j] =  32767;
						if (d[i][j] < -32768) d[i][j] = -32768;
					}
				}
			}
			// row transform
		
			for (i=0;i<TUsize;i++){
				int row[32];
				for (j=0;j<TUsize;j++) {
					row[j] = d[i][j];
					d[i][j] = 0;
				}
				for (j=0;j<TUsize;j++){
					if (filled_cols[j]){
						if (!trType)
							for (int k=0;k<TUsize;k++)
								//d[i][k] += row[j] * transMatrix[j][k*32/TUsize] ;
								d[i][k] += row[j] * transMatrix[j*32/TUsize][k] ;
						else 
							for (int k=0;k<TUsize;k++)
								d[i][k] += row[j] * sinetransMatrix[j][k];
						}
				}
			}
		}
		// final clipping
		if (!transquant_bypass) {
			for (j=0;j<TUsize;j++){
				for (i=0;i<TUsize;i++){
					d[i][j] = (d[i][j] + 2048)>>12;
				}	
			}
		}
	}
	return 0;
}

int main()
{
	//Test();
	//exit(0);
	/*
	char* cabac_to_res_file = CONFORMANCE_PATH CONFORMANCE_STREAM "\\cabac_to_residual_soft";
	char* res_to_inter_file = CONFORMANCE_PATH CONFORMANCE_STREAM "\\residual_to_inter";
	char* res_to_inter_Y_file = CONFORMANCE_PATH CONFORMANCE_STREAM "\\residual_to_inter_Y";
	char* res_to_inter_Cb_file = CONFORMANCE_PATH CONFORMANCE_STREAM "\\residual_to_inter_Cb";
	char* res_to_inter_Cr_file = CONFORMANCE_PATH CONFORMANCE_STREAM "\\residual_to_inter_Cr";
	*/
	
	char* cabac_to_res_file = FILE_PATH "\\cabac_to_residual_soft";
	char* res_to_inter_file = FILE_PATH  "\\residual_to_inter";
	char* res_to_inter_Y_file = FILE_PATH  "\\residual_to_inter_Y";
	char* res_to_inter_Cb_file = FILE_PATH  "\\residual_to_inter_Cb";
	char* res_to_inter_Cr_file = FILE_PATH  "\\residual_to_inter_Cr";
	char* res_to_intra_file = "C:\\Users\\Maleen\\Uni\\HEVC\\TransformC\\residual_to_inter";
	
	CabactoRes = fopen(cabac_to_res_file,"rb");
	//RestoIntra = fopen(res_to_inter_file,"wb");
	RestoInter = fopen(res_to_inter_file,"wb");
	RestoInterY = fopen(res_to_inter_Y_file,"wb");
	RestoInterCb = fopen(res_to_inter_Cb_file,"wb");
	RestoInterCr = fopen(res_to_inter_Cr_file,"wb");

#ifdef GT_PRESENT
//	char* gt_file_name = CONFORMANCE_PATH CONFORMANCE_STREAM GT_FILE;
	char* gt_file_name = FILE_PATH "\\ground_truth.yuv";
	gt_file = fopen(gt_file_name,"rb");
#endif
	struct h {
		unsigned header:8;
	} byte;
	printf("Residual\n");
	int i;
	initialize_scanOrder();
	for (i=0;i<1000000000;i++){
		if (fread(&byte,4,1,CabactoRes)==0) {
			fclose(RestoInter); //fclose(RestoIntra);
			fclose(RestoInterY);
			fclose(RestoInterCb);
			fclose(RestoInterCr);
			//printf("Max Res Present %d %d %d\n",max_res_count[0], max_res_count[1], max_res_count[2]);
			printf("TUSize %d %d %d %d\n",TUSizeCount[0], TUSizeCount[1] , TUSizeCount[2] , TUSizeCount[3]);
			int i;
			printf ("ColCount\n");
			for (i=0;i<32;i++)
				printf("\t %d %d",i,ColCount[i]);
			//while(1);
			exit(0);
		}
		//while(fread(&byte,4,1,CabactoRes)==0);
		switch(byte.header){
			case 0x00: case 0x01: case 0x02: case 0x03: case 0x04:
			case 0x10: case 0x11: case 0x12: case 0x13: case 0x14: case 0x15: case 0x16: case 0x17: {
				// forward
				fwrite(&byte,4,1,RestoInter);
				//fwrite(&byte,4,1,RestoIntra);
				if (byte.header==0x10){
					stat_total_coeffs += ( stat_frame_coeffs+stat_frame_col_coeffs);
					
					if (stat_frame_count>0) printf("Coeffs %8d %8d %8d %8d \n", stat_frame_coeffs, stat_frame_col_coeffs, stat_frame_coeffs+stat_frame_col_coeffs, stat_total_coeffs/stat_frame_count*120/1000);
					if (POC==1000){
						fclose(RestoInter); //fclose(RestoIntra);
						fclose(RestoInterY);
						fclose(RestoInterCb);
						fclose(RestoInterCr);
						printf("TUSize %d %d %d %d\n",TUSizeCount[0], TUSizeCount[1] , TUSizeCount[2] , TUSizeCount[3]);
						int i;
						printf ("ColCount\n");
						for (i=0;i<32;i++)
							printf("\t %d %d",i,ColCount[i]);
						//while(1);
						exit(0);
					}
					while(fread(&POC,4,1,CabactoRes)==0);
					stat_frame_count++;
					stat_frame_coeffs = 0;
					stat_frame_col_coeffs = 0;

					fwrite(&POC,4,1,RestoInter);
					//fwrite(&POC,4,1,RestoIntra);
					printf("POC = %3d ",POC);
				}
				if (byte.header==0x01){
					memcpy((void *) &PPS,(void *) &byte,4);
				}
				if (byte.header == 0x02) {
					memcpy((void *) &PPS_2,(void *) &byte,4);
					scaling_list_enable_flag = PPS_2.scaling_list_enable_flag;
				}
				if (byte.header==0x12){
					memcpy((void *) &Slice,(void *) &byte,4);
				}
				

				break;

			}
			case 0x05: {
				// Do not forward
				memcpy((void *) &sl,(void *) &byte,4);
				if (sl.i== 0xff) 
					DC_Scale[sl.sizeId-2][sl.matrixId] = sl.val;
				else 
					ScalingList[sl.sizeId][sl.matrixId][sl.i] = sl.val;
				break;
			}
			case 0x20: {
				fwrite(&byte,4,1,RestoInter);
				//fwrite(&byte,4,1,RestoIntra);
				memcpy((void *) &CTU,(void *) &byte,4);
				//CTU =(struct CTU_0* ) &byte;
				//printf("CTU %d %d\n",CTU.xC, CTU.yC);
				break;
			}
			case 0x21: case 0x22: case 0x23: case 0x24: {
				fwrite(&byte,4,1,RestoInter);
				//fwrite(&byte,4,1,RestoIntra);
				break;
			}
			case 0x30: {

				memcpy((void *) &CU,(void *) &byte,4);
				//fwrite(&byte,4,1,RestoInter);

				//printf("\tCU %d %d size %d\n",CTU.xC+CU.x0,CTU.yC+CU.y0,CU.log2_cb_size);
				if (CU.predMode) {
					fwrite(&byte,4,1,RestoInter);
					int predUnits;
					if (CU.PartMode==0) predUnits = 1;
					else if (CU.PartMode ==3) predUnits =4;
					else predUnits = 2;
					int i=0;
					struct PU_0 pu;
					for (i=0;i<predUnits;i++){
						while(fread(&pu,4,1,CabactoRes)==0);
						fwrite(&pu,4,1,RestoInter);
						if (pu.merge_flag==0){
							while(fread(&byte,4,1,CabactoRes)==0);
							fwrite(&byte,4,1,RestoInter);
							while(fread(&byte,4,1,CabactoRes)==0);
							fwrite(&byte,4,1,RestoInter);
						}
						//fwrite(&byte,4,1,RestoIntra);
					}
				} else {
					//fwrite(&byte,4,1,RestoIntra);
					fwrite(&byte,4,1,RestoInter);
				}
				transquant_bypass = CU.bypass | CU.pcm;
				ResidualDecodeCU();
				break;
			}
//			case 0x40: {
//				R =(struct RU_0* ) &byte;
//				//printf("\t\t RU %d %d cidx %d %d\n",h->xT, h->yT, h->cidx,h->size);
//			}


		}
		//printf("%x ",byte.header);
	}
	//while(1) ResidualDecodeCU(CabactoRes,RestoSum);
//	ResidualDecodeCU(CabactoRes,RestoSum);
//	ResidualDecodeCU(CabactoRes,RestoSum);
//	fclose(RestoSum);
	return 0;
}

