/*
 * defines.h
 *
 *  Created on: Jan 6, 2013
 *      Author: maleen
 */

#ifndef DEFINES_H_
#define DEFINES_H_

#define MAX_REF_PICS 16
#define MAX_RPS 17
#define MAX_WIDTH_CTU 64
#define MAX_WIDTH_CU 512
#define MAX_CTU 2048

#define CONFORMANCE_PATH "G:\\HEVC\\Conformance\\"
#define CONFORMANCE_STREAM "_TearsOfSteel"
#define BIT_FILE "\\tos.hevc" 
#define GT_FILE "\\tos_3840x1720_12bit_residual.yuv"
int gt_width = 1920;
int gt_height = 1080;
int cabac_printing =0;

typedef unsigned char BYTE;

typedef struct {
	int general_profile_space;
	int general_tier_flag;
	int general_profile_idc;
	unsigned int general_profile_compatibility;
	int general_level_idc;
} profile_tier_level_struct;


typedef struct {
	BYTE* byte;
	int len;
	int pos;
	int bit;
	int ctsZeros;
} rbsp_struct;

typedef struct {
	int NumNegativePics;
	int NumPositivePics;
	int UsedByCurrPicS0[MAX_REF_PICS];
	int UsedByCurrPicS1[MAX_REF_PICS];
	int DeltaPocS0[MAX_REF_PICS];
	int DeltaPocS1[MAX_REF_PICS];
	int NumDeltaPocs;
} short_term_ref_pic_set_struct;


typedef struct {
	int vps_video_parameter_set_id;
	int vps_max_sub_layers_minus1;
	int vps_temporal_id_nesting_flag;
	profile_tier_level_struct vps_profile_tier_level;
	int vps_sub_layer_ordering_info_present_flag;
	int vps_max_dec_pic_buffering; // ideally these should be arrays. But we are using only 1 sub pic
	int vps_max_num_reorder_pics;
	int vps_max_latency_increase;
	int vps_max_layer_id;
	int vps_num_layer_sets_minus1;

	int vps_timing_info_present_flag;
	int vps_num_hrd_parameters;
	int vps_max_nuh_reserved_zero_layer_id;
	//int vps_num_op_sets_minus1;
	int vps_extension_flag;

} video_param_struct;

typedef struct {
	int sps_video_parameter_set_id;
	int sps_max_sub_layers_minus1;
	int sps_temporal_sub_layer_nesting_flag;
	profile_tier_level_struct sps_profile_tier_level;
	int sps_seq_parameter_set_id;
	int chroma_format_idc;
	int pic_width_in_luma_samples;
	int pic_height_in_luma_samples;
	int conformance_window_flag;
		int conf_win_left_offset;
		int conf_win_right_offset;
		int conf_win_top_offset;
		int conf_win_bottom_offset;
	int bit_depth_luma_minus8;
	int bit_depth_chroma_minus8;
	int log2_max_pic_order_cnt_lsb_minus4;
	int sps_sub_layer_ordering_info_present_flag;
		int sps_max_dec_pic_buffering; // ideally these should be arrays. But we are using only 1 sub pic
		int sps_max_num_reorder_pics;
		int sps_max_latency_increase;
	int log2_min_luma_coding_block_size_minus3;
	int log2_diff_max_min_luma_coding_block_size;
	int log2_min_transform_block_size_minus2;
	int log2_diff_max_min_transform_block_size;
	int max_transform_hierarchy_depth_inter;
	int max_transform_hierarchy_depth_intra;
	int scaling_list_enable_flag;
	int amp_enabled_flag; // asymetric motion partitions
	int sample_adaptive_offset_enabled_flag;
	int pcm_enabled_flag;
		int pcm_sample_bit_depth_luma_minus1;
		int pcm_sample_bit_depth_chroma_minus1;
		int log2_min_pcm_luma_coding_block_size_minus3;
		int log2_diff_max_min_pcm_luma_coding_block_size;
		int pcm_loop_filter_disable_flag;
		int PCMBitDepthY;
		int PCMBitDepthC;
		int Log2MinIpcmCbSizeY;
		int Log2MaxIpcmCbSizeY;
	int num_short_term_ref_pic_sets;
	int long_term_ref_pics_present_flag;
	int sps_temporal_mvp_enable_flag;
	int strong_intra_smoothing_enable_flag;
	int vui_parameters_present_flag;
	int sps_extension_flag;

	short_term_ref_pic_set_struct stRPS[MAX_RPS];
	int scaling_matrix_index[20];

	int Log2MinCbSizeY;
	int Log2CtbSizeY;
	int PicWidthInCtbsY;
	int PicHeightInCtbsY;
	int PicSizeInCtbsY;
	int Log2MinTrafoSize;
	int Log2MaxTrafoSize;
	int MaxTrafoDepth;
	int Log2MinCuQpDeltaSize;
} sequence_param_struct;

typedef struct {
	int pps_pic_parameter_set_id;
	int pps_seq_parameter_set_id ;
	int dependent_slice_segments_enabled_flag;
	int output_flag_present_flag;
	int sign_data_hiding_flag;
	int cabac_init_present_flag;
	int num_ref_idx_l0_default_active_minus1;
	int num_ref_idx_l1_default_active_minus1;
	int init_qp_minus26;
	int constrained_intra_pred_flag;
	int transform_skip_enabled_flag;
	int cu_qp_delta_enabled_flag;
	int diff_cu_qp_delta_depth;
	int pps_cb_qp_offset;
	int pps_cr_qp_offset;
	int pps_slice_chroma_qp_offsets_present_flag;
	int weighted_pred_flag;
	int weighted_bipred_flag;
	int transquant_bypass_enable_flag;
	int tiles_enabled_flag;
	int num_tile_columns_minus1;
	int num_tile_rows_minus1;
		int column_width_minus1[9];
		int row_height_minus1[9];
	int uniform_spacing_flag;
	int loop_filter_across_tiles_enabled_flag;
	int entropy_coding_sync_enabled_flag;
	int loop_filter_across_slices_enabled_flag;
	int deblocking_filter_control_present_flag;
	int pps_deblocking_filter_override_enabled_flag;
	int pps_disable_deblocking_filter_flag;
	int pps_beta_offset_div2;
	int pps_tc_offset_div2;
	int pps_scaling_list_data_present_flag;
	int lists_modification_present_flag;
	int log2_parallel_merge_level_minus2;
	int num_extra_slice_header_bits;
	int slice_segment_header_extension_present_flag;
	int pps_extension_flag;

} picture_param_struct;

typedef struct {
//	int IdrPicFlag;
//	int RapPicFlag; // random access picture nal_unit_type -(16,31)
	int first_slice_segment_in_pic_flag;
	int no_output_of_prior_pics_flag;
	int slice_pic_parameter_set_id;
	int dependent_slice_segment_flag;
	int slice_segment_address;
	int slice_type;
	int pic_output_flag;
	int pic_order_cnt_lsb;
	int short_term_ref_pic_set_sps_flag;
	int short_term_ref_pic_set_idx;
	int slice_temporal_mvp_enable_flag;

	int slice_sao_luma_flag;
	int slice_sao_chroma_flag;

	int num_ref_idx_active_override_flag;
	int num_ref_idx_l0_active_minus1;
	int num_ref_idx_l1_active_minus1;
	int mvd_l1_zero_flag;
	int cabac_init_flag;
	int collocated_from_l0_flag;
	int collocated_ref_idx;
	int five_minus_max_num_merge_cand;

	int slice_qp_delta;
	int slice_cb_qp_offset;
	int slice_cr_qp_offset;
	int deblocking_filter_override_flag;
	int slice_disable_deblocking_filter_flag;
	int slice_beta_offset_div2;
	int slice_tc_offset_div2;
	int slice_loop_filter_across_slices_enabled_flag;
	int num_entry_point_offsets;
} slice_segment_struct;


int IdrPicFlag;
int RapPicFlag;
int RefPicture;
int prevPicOrderCntMsb, prevPicOrderCntLsb;

int ctbAddrInRS;
int ctbAddrInTS;
int SliceQPY;

int MaxNumMergeCand;

int colWidth[MAX_WIDTH_CTU];
int rowHeight[MAX_WIDTH_CTU];
int colBd[MAX_WIDTH_CTU+1];
int rowBd[MAX_WIDTH_CTU+1];
short CtbAddrRstoTs[MAX_CTU];
short CtbAddrTstoRs[MAX_CTU];
short TileId[MAX_CTU];

int firstCtbTS[11][11];
int firstCtbRS[11][11];

int ScalingMatrixValid[50];
int ScalingMatrix[50][64];
int ScalingMatrixDC[50];

short SliceId[MAX_CTU];
//unsigned int* MinTbAddrZS;
//int* CtDepth;
int CtDepth_buf[MAX_WIDTH_CU];
int CtDepth_buf_left[8];
int CtDepth_cur;

//int* cu_skip_flag;
int cu_skip_flag_buf[MAX_WIDTH_CU];
int cu_skip_flag_buf_left[8];
int cu_skip_flag_cur;

//BYTE* CuPredMode;
int CuPredMode_buf[16];
int CuPredMode_buf_left[16];
int CuPredMode_cur;

int qPY_left[8];
int qpY_top[8];
int qPY;

int IsCuQpDeltaCoded;
int CuQpDelta;

int IntraSplitFlag;
//int* cbf_cb[6]; int* cbf_cr[6]; //TODO luma memset to 1
int cbf_luma_0[4]; int cbf_cb_0[4]; int cbf_cr_0[4];
int cbf_cb_B[4]; int cbf_cr_B[4];
int ScanOrder[4][3][64][2];

int IntraPredModeY_buf[16];
int IntraPredModeY_buf_left[16];
int IntraPredModeY_cur[4];

//int* IntraPredModeY;
int IntraPredModeC;
int coded_sub_block_flag[9][9];
int significant_coeff_flag[32][32];
int coeff_abs_greater1_flag[16];
int coeff_abs_greater2_flag[16];
int coeff_sign_flag[16];
int coeff_abs_level_remaining[16];
int AbsCoeff[16];
int cNextRiceParam;
int merge_flag;
//int* QPY;
//BYTE* , *BSv;

int cabac_trafoSize;
int cabac_trafoDepth;
int cabac_cIdx;
int cabac_xC,cabac_yC;
int cabac_scanIdx;
int cabac_greater1_invoked[64] = {0};
int cabac_greater1Ctx;
int cabac_ctxSet;
int cabac_lastgreater1Flag;
int cabac_cLastRiceParam;
int cabac_greater1Ctx_v2;
int cabac_greater1_first_1_met;

int global_xBase;
int global_yBase;
int global_qP;
unsigned int global_chroma_mode;

int same_slice_tile_left;
int same_slice_tile_up;

int SliceAddrRS;
int PicOrderCntVal = 0;

int available_left(int xCurr);
int available_up(int yCurr);

typedef struct {
	int valMPS;
	int pStateIdx;
} cabac_context_struct;
typedef struct {
	short codIOffset;
	short codIRange;
	cabac_context_struct* contexts[27];
	// contexts
} cabac_state_struct;
int cabac_table_maxctxIDx[] = {2,2,8,2,5,5,1,8,2,2,1,1,9,3,3,1,1,8,5,11,5,53,53,11,125,71,17};
int initValue_5[] = {153,153,153};
int initValue_6[] = {200,185,160}; //TODO reverse order in draft
int initValue_7[] = {139,141,157,107,139,126,107,139,126};
int initValue_8[] = {154,154,154};
int initValue_9[] = {197,185,201,197,185,201};
int initValue_10[]= {154,154,154,154,154,154};
int initValue_11[]= {149,134};
int initValue_12[]= {184,154,139,154,154,154,139,154,154};
int initValue_13[]= {184,154,183};
int initValue_14[]= { 63,152,152};
int initValue_15[]= {110,154};
int initValue_16[]= {122,137};
int initValue_17[]= { 95, 79, 63, 31, 31, 95, 79, 63, 31, 31};
int initValue_18[]= {153,153,153,153};
int initValue_19[]= {140,169,198,198}; //{140,198,169,198};
int initValue_20[]= {168,168};
int initValue_21[]= { 79, 79};
int initValue_22[]= {153,138,138,124,138, 94,224,167,122};
//int initValue_22[]= {224,167,122,124,138, 94,153,138,138}; // reversed
int initValue_23[]= {111,141,153,111,153,111};
int initValue_24[]= { 94,138,182,154,149,107,167,154,149, 92,167,154};
int initValue_25[]= {139,139,139,139,139,139};
int initValue_26[]= {110,  110,  124,  125,  140,  153,  125,  127,  140,  109,  111,  143,  127,  111,   79,
	    108,  123,   63, 125,  110,   94,  110,   95,   79,  125,  111,  110,   78,  110,  111,  111,   95,   94,
	    108,  123,  108, 125,  110,  124,  110,   95,   94,  125,  111,  111,   79,  125,  126,  111,  111,   79,
	    108,  123,   93 };
int initValue_27[]= {110,  110,  124,  125,  140,  153,  125,  127,  140,  109,  111,  143,  127,  111,   79,
	    108,  123,   63, 125,  110,   94,  110,   95,   79,  125,  111,  110,   78,  110,  111,  111,   95,   94,
	    108,  123,  108, 125,  110,  124,  110,   95,   94,  125,  111,  111,   79,  125,  126,  111,  111,   79,
	    108,  123,   93 };
int initValue_28[]= { 91,171,134,141,121,140, 61,154,121,140, 61,154};
int initValue_29[]= {111,  111,  125,  110,  110,   94,  124,  108,  124,  107,  125,  141,  179,  153,  125,  107,  125,  141,  179,  153,  125,  107,  125,  141,  179,  153,  125,  140,  139,  182,  182,  152,  136,  152,  136,  153,  136,  139,  111,  136,  139,  111,
		155,  154,  139,  153,  139,  123,  123,   63,  153,  166,  183,  140,  136,  153,  154,  166,  183,  140,  136,  153,  154,  166,  183,  140,  136,  153,  154,  170,  153,  123,  123,  107,  121,  107,  121,  167,  151,  183,  140,  151,  183,  140,
		170,  154,  139,  153,  139,  123,  123,   63,  124,  166,  183,  140,  136,  153,  154,  166,  183,  140,  136,  153,  154,  166,  183,  140,  136,  153,  154,  170,  153,  138,  138,  122,  121,  122,  121,  167,  151,  183,  140,  151,  183,  140 };
int initValue_30[]= {140,   92,  137,  138,  140,  152,  138,  139,  153,   74,  149,   92,  139,  107,  122,  152,  140,  179,  166,  182,  140,  227,  122,  197,
		154,  196,  196,  167,  154,  152,  167,  182,  182,  134,  149,  136,  153,  121,  136,  137,  169,  194,  166,  167,  154,  167,  137,  182,
		154,  196,  167,  167,  154,  152,  167,  182,  182,  134,  149,  136,  153,  121,  136,  122,  169,  208,  166,  167,  154,  152,  167,  182 };
int initValue_31[]= {138,  153,  136,  167,  152,  152,107,  167,   91,  122,  107,  167,107,  167,   91,  107,  107,  167};


int* initValue[] = {initValue_5,initValue_6,initValue_7,initValue_8,initValue_9,initValue_10,initValue_11,initValue_12,initValue_13,initValue_14,
					initValue_15,initValue_16,initValue_17,initValue_18,initValue_19,initValue_20,initValue_21,initValue_22,initValue_23,initValue_24,
					initValue_25,initValue_26,initValue_27,initValue_28,initValue_29,initValue_30,initValue_31};

//int maxBinIdxCtx[29][3] = { {0} }; // non zero values initialized at init_cabac
int ctxIdxOffset[29][3] = { {0,1,2} , {0,1,2}, {0,3,6}, {0,1,2}, {0,0,3},
							{0,2,4} , {0,0,1}, {0,1,5}, {0,1,2}, {0,1,2},
							{0,0,1} , {0,0,1}, {0,0,5}, {0,0,2}, {0,0,1},
							{0,0,1} , {0,0,1}, {0,3,6}, {0,2,4}, {0,4,8},
							{0,1,2} , {0,18,36}, {0,18,36}, {0,4,8}, {0,42,84},
							{0,24,48} , {0,6,12}   ,  {0,2,3} , { 3,4,5}
						  };

int rangeTabLPS[64][4] = {
		  { 128, 176, 208, 240},
		  { 128, 167, 197, 227},
		  { 128, 158, 187, 216},
		  { 123, 150, 178, 205},
		  { 116, 142, 169, 195},
		  { 111, 135, 160, 185},
		  { 105, 128, 152, 175},
		  { 100, 122, 144, 166},
		  {  95, 116, 137, 158},
		  {  90, 110, 130, 150},
		  {  85, 104, 123, 142},
		  {  81,  99, 117, 135},
		  {  77,  94, 111, 128},
		  {  73,  89, 105, 122},
		  {  69,  85, 100, 116},
		  {  66,  80,  95, 110},
		  {  62,  76,  90, 104},
		  {  59,  72,  86,  99},
		  {  56,  69,  81,  94},
		  {  53,  65,  77,  89},
		  {  51,  62,  73,  85},
		  {  48,  59,  69,  80},
		  {  46,  56,  66,  76},
		  {  43,  53,  63,  72},
		  {  41,  50,  59,  69},
		  {  39,  48,  56,  65},
		  {  37,  45,  54,  62},
		  {  35,  43,  51,  59},
		  {  33,  41,  48,  56},
		  {  32,  39,  46,  53},
		  {  30,  37,  43,  50},
		  {  29,  35,  41,  48},
		  {  27,  33,  39,  45},
		  {  26,  31,  37,  43},
		  {  24,  30,  35,  41},
		  {  23,  28,  33,  39},
		  {  22,  27,  32,  37},
		  {  21,  26,  30,  35},
		  {  20,  24,  29,  33},
		  {  19,  23,  27,  31},
		  {  18,  22,  26,  30},
		  {  17,  21,  25,  28},
		  {  16,  20,  23,  27},
		  {  15,  19,  22,  25},
		  {  14,  18,  21,  24},
		  {  14,  17,  20,  23},
		  {  13,  16,  19,  22},
		  {  12,  15,  18,  21},
		  {  12,  14,  17,  20},
		  {  11,  14,  16,  19},
		  {  11,  13,  15,  18},
		  {  10,  12,  15,  17},
		  {  10,  12,  14,  16},
		  {   9,  11,  13,  15},
		  {   9,  11,  12,  14},
		  {   8,  10,  12,  14},
		  {   8,   9,  11,  13},
		  {   7,   9,  11,  12},
		  {   7,   9,  10,  12},
		  {   7,   8,  10,  11},
		  {   6,   8,   9,  11},
		  {   6,   7,   9,  10},
		  {   6,   7,   8,   9},
		  {   2,   2,   2,   2}
		};
int transIdxLPS[64] = {  0, 0, 1, 2, 2, 4, 4, 5, 6, 7, 8, 9, 9,11,11,12,
						13,13,15,15,16,16,18,18,19,19,21,21,22,22,23,24,
						24,25,26,26,27,27,28,29,29,30,30,30,31,32,32,33,
						33,33,34,34,35,35,35,36,36,36,37,37,37,38,38,63 };
int transIdxMPS[64] = {  1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,
						17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,
						33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,
						49,50,51,52,53,54,55,56,57,58,59,60,61,62,62,63 };

//#define MAX_WIDTH_CTU 30
int ReferenceScalingList_012[64] = { 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 17, 16, 17, 16, 17, 18,
									 17, 18, 18, 17, 18, 21, 19, 20, 21, 20, 19, 21, 24, 22, 22, 24,
									 24, 22, 22, 24, 25, 25, 27, 30, 27, 25, 25, 29, 31, 35, 35, 31,
									 29, 36, 41, 44, 41, 36, 47, 54, 54, 45, 65, 70, 65, 88, 88, 115 };
int ReferenceScalingList_345[64] =   { 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17, 17, 17, 18,
									   18, 18, 18, 18, 18, 20, 20, 20, 20, 20, 20, 20, 24, 24, 24, 24,
									   24, 24, 24, 24, 25, 25, 25, 25, 25, 25, 25, 25, 28, 28, 28, 28,
									   28, 33, 33, 33, 33, 33, 41, 41, 41, 41, 54, 54, 54, 71, 71, 91 };

typedef struct {
	int SaoTypeIdx[3];
	int sao_offset_abs[3][4];
	int sao_offset_sign[3][4];
	int SaoOffsetVal[3][5];
	int sao_band_position[3];
	int SaoEoClass[3];
} sao_struct;
sao_struct sao_list[4096];

#define TRAIL_N 0
#define TRAIL_R 1
#define RASL_N 8
#define RASL_R 9
#define IDR_W_DLP 19
#define CRA_NUT 21
#define NAL_VPS_NUT 32
#define NAL_SPS_NUT 33
#define NAL_PPS_NUT 34



#define MODE_INTRA 0
#define MODE_INTER 1
#define MODE_SKIP 2

#define PART_2Nx2N 0
#define PART_2NxN 1
#define PART_Nx2N 2
#define PART_NxN 3
#define PART_2NxnU 4
#define PART_2NxnD 5
#define PART_nLx2N 6
#define PART_nRx2N 7

#define PRED_L0 0
#define PRED_L1 1
#define PRED_BI 2

#define SLICE_B 0
#define SLICE_P 1
#define SLICE_I 2



int decode_next_NAL(FILE* fp);
int decode_next_NAL_with_emulation(FILE* fp);
void decode_rbsp(int type,rbsp_struct* rbsp);
void video_parameter_set_rbsp(video_param_struct* vps);
void sequence_parameter_set_rbsp();
void picture_parameter_set_rbsp();

void slice_segment_layer_rbsp(slice_segment_struct* ssl);
void send_new_tile();

void sao(int rx, int ry);
void coding_tree_unit(void);
void coding_quadtree(int x0, int y0, int log2CbSize, int cqtDepth);
void coding_unit(int x0,int y0, int log2CbSize);
void transform_tree(int x0, int y0, int xBase, int yBase, int log2TrafoSize, int trafoDepth, int blkIdx, int interSplitlag);
void transform_unit(int x0, int y0, int xBase, int yBase, int log2TrafoSize, int trafoDepth, int blkIdx);
int residual_coding(int x0, int y0, int log2TrafoSize, int cIdx);
void prediction_unit(int x0, int y0, int nPbW, int nPbH, int partIdx);
unsigned int mvd_coding(int x0, int y0, int refList);

void profile_tier_level(profile_tier_level_struct* ptl, int profilePresentFlag, int maxNumSubLayersMinus1);
void bit_rate_pic_rate_info(void* brpr,int TempLevelLow, int TempLevelHigh );
void pred_weight_table();
void scaling_list_data();
void short_term_ref_pic_set(sequence_param_struct* sps,int  idxRps);

void init_cabac_contexts();
void save_cabac_contexts();
void load_cabac_contexts();
void init_cabac_engine();

void print_cabac(void);
int read_cabac_FL(int cMax,int table);
int read_cabac_TU(int cMax,int table);
int read_cabac_chroma(void);
int read_cabac_abs_mvd_minus2();
int read_cabac_part_mode(int cLog2CbSize, int x0, int y0);
int read_cabac_inter_pred_idc(int x0, int y0, int nPbW, int nPbH);
int read_cabac_greater1(int cIdx, int i, int n,int first_in_tb);
int read_cabac_coeff_abs_level_remaining(int n, int baseLevel);
int read_cabac_greater1_v2(int cIdx, int i, int n,int first_in_tb);
int read_cabac_coded_sub_block(int xS, int yS, int cIdx, int log2TrafoSize);
int getCtxIdx(int binIdx, int tableIdx);
int getCtxIdxInc(int tableIdx, int binIdx);
int DecodeBin(int ctxIdxTable, int ctxIdx);
int DecodeBypass();
int DecodeTerminate();

int read_cabac_sao_merge_left();
int read_cabac_sao_type_idx();
int read_cabac_sao_band_position();
int read_cabac_sao_offset_abs();
int read_cabac_sao_offset_sign();
int read_cabac_sao_eo_class_xxx();
int read_cabac_end_of_slice_segment_flag();
int read_cabac_end_of_sub_stream_one_bit();
int read_cabac_split_cu_flag();
int read_cabac_cu_transquant_bypass_flag();
int read_cabac_cu_skip_flag();
int read_cabac_cu_qp_delta_abs();
int read_cabac_cu_qp_delta_sign();
int read_cabac_pred_mode_flag();
int read_cabac_part_mode();
int read_cabac_pcm_flag();
int read_cabac_prev_intra_luma_pred_flag();
int read_cabac_mpm_idx();
int read_cabac_rem_intra_luma_pred_flag();
int read_cabac_intra_chroma_pred_flag();
int read_cabac_merge_flag();
int read_cabac_merge_idx(int MaxNumMergeCand,int skip);
int read_cabac_inter_pred_idc();
int read_cabac_ref_idx_l0(int num_ref_idx); 
int read_cabac_ref_idx_l1(int num_ref_idx); 
int read_cabac_abs_mvd_greater0_flag();
int read_cabac_abs_mvd_greater1_flag();
int read_cabac_abs_mvd_minus2();
int read_cabac_mvd_sign_flag();
int read_cabac_mvp_lx_flag();
int read_cabac_rqt_root_cbf();
int read_cabac_split_transform_flag();
int read_cabac_cbf_luma();
int read_cabac_cbf_cx();
int read_cabac_transform_skip_flag(int cIdx);
int read_cabac_last_significant_coef_x_prefix(int log2TrafoSize);
int read_cabac_last_significant_coef_y_prefix(int log2TrafoSize);
int read_cabac_last_significant_coef_x_suffix(int prefix);
int read_cabac_last_significant_coef_y_suffix(int prefix);
int read_cabac_coded_sub_block_flag();
int read_cabac_significant_coeff_flag();
int read_cabac_coeff_abs_level_greater1_flag();
int read_cabac_coeff_abs_level_greater2_flag();
int read_cabac_coeff_abs_level_remaining();
int read_cabac_coeff_sign_flag();

void axi_write(unsigned int addr, unsigned int data);
void axi_write(unsigned int addr, void * data);

void print_loc(void);
unsigned int read_u(int n);
unsigned int read_ue(void);
int read_se();
int read_bit(void);
void byte_alignment(void);

int available_z(int xCurr,int  yCurr,int xN,int yN);
void initialize_scanOrder(void);
int derive_intra_mode(int x0, int y0,int prev_intra_luma_pred_flag,int mpm_idx,int rem_intra_luma_pred_mode, int intra_chroma_pred_mode);

int break_point = 0;

void initialize_blocks();
void initialize_mmaps();

FILE* cabac_to_intra;
FILE* cabac_to_inter;
//FILE* cabac_to_residual;
FILE* cabac_to_residual;
//FILE* cabac_to_residual_data;



void send_tu_residual_v2(unsigned short xB,unsigned short  yB, int log2TrafoSize,int cidx, BYTE mode,int QP);

struct parameters_0 {
	unsigned header: 8;
	unsigned pic_width: 12;
	unsigned pic_height: 12;
};
struct parameters_1 {
	unsigned header :8;
	unsigned Log2CtbSizeY : 3;
	signed pps_cb_qp_offset :5;
	signed pps_cr_qp_offset :5;
	signed pps_beta_offset_div2 : 4;
	signed pps_tc_offset_div2: 4;
	unsigned strong_intra_smoothing :1;
	unsigned constrained_intra_pred : 1;
	unsigned pcm_loop_filter_disable_flag : 1;
};
struct parameters_2 {
	unsigned header :8;
	unsigned num_short_term_ref : 6;
	unsigned weighted_pred:1;
	unsigned weighted_bipred:1;
	unsigned parallel_merge_level:3;
	unsigned loop_filter_across_tiles_enabled_flag: 1;
	unsigned scaling_list_enable_flag : 1;
};
struct parameters_3 {
	unsigned header :8;
	unsigned rps_id: 8;
	unsigned num_positive:8;
	unsigned num_negative:8;
};
struct parameters_4 {
	unsigned header :8;
	unsigned rps_id: 7;
	unsigned used:1;
	signed delta_POC:16;
};

struct scaling_list_pkt {
	unsigned header : 8;
	unsigned sizeId : 4;
	unsigned matrixId : 4;
	unsigned i : 8;
	unsigned val : 8;

};

struct slice_0 {
	unsigned header : 8;
	unsigned null:24;
	signed POC: 32;
};
struct new_slice_tile{
	unsigned header : 8;
	unsigned xC : 12;
	unsigned yC : 12;
};
struct new_tile_width {
	unsigned header :8;
	unsigned width :16;
};
struct slice_1{
	unsigned header :8;
	unsigned short_term_ref_pic_sps : 1;
	unsigned short_term_ref_pic_idx: 4;
	unsigned temporal_mvp_enabled : 1;
	unsigned sao_luma : 1;
	unsigned sao_chroma :1;
	unsigned num_ref_idx_l0_minus1 : 4;
	unsigned num_ref_idx_l1_minus1 : 4;
	unsigned five_minus_max_merge_cand : 3;
	unsigned slice_type :2;
	unsigned collocated_from_l0:1;
	unsigned slice_loop_filter_across_slices_enabled_flag: 1;
};
struct slice_2 {
	unsigned header :8;
	signed slice_cb_qp_offset:5;
	signed slice_cr_qp_offset:5;
	unsigned disable_dbf : 1;
	signed slice_beta_offset_div2 : 4;
	signed slice_tc_offset_div2 : 4;
	unsigned collocated_ref_idx : 5;
};
struct slice_3 {
	unsigned header :8;
	unsigned rps_id: 8;
	unsigned num_positive:8;
	unsigned num_negative:8;
};
struct slice_4 {
	unsigned header :8;
	unsigned rps_id: 7;
	unsigned used:1;
	signed delta_POC:16;
};

struct pred_weight_1 {
	unsigned header :8;
	unsigned luma_log2_weight_denom : 3;
	unsigned ChromaLog2WeightDenom  : 3;
};
struct pred_weight_2 {
	unsigned header : 8;
	unsigned ref_pic : 4;
	unsigned list : 1;
	unsigned cIdx : 2;
	signed Weight : 9;
	signed Offset : 8;
};

struct CTU_0 {
	unsigned header : 8;
	unsigned xC :12;
	unsigned yC:12;
};
struct CTU_1 {
	unsigned header : 8;
	unsigned tile_id : 8;
	unsigned slice_id : 8;
};
struct CTU_2 {
	unsigned header :8;
	unsigned SaoType:2;
	unsigned BandPos_EO : 6;
	unsigned sao_offset_abs_0 :3 ;
	unsigned sao_offset_sign_0 :1;
	unsigned sao_offset_abs_1 :3 ;
	unsigned sao_offset_sign_1 :1;
	unsigned sao_offset_abs_2 :3 ;
	unsigned sao_offset_sign_2 :1;
	unsigned sao_offset_abs_3 :3 ;
	unsigned sao_offset_sign_3 :1;
};
struct CU_0 {
	unsigned header :8;
	unsigned x0 :6;
	unsigned y0 :6;
	unsigned log2_cb_size:3;
	unsigned predMode : 1;
	unsigned PartMode :3;
	unsigned bypass : 1;
	unsigned pcm:1;
	unsigned not_used: 3;
};
struct PU_0{
	unsigned header : 8;
	unsigned part_idx : 2;
	unsigned pred_idc :2;
	unsigned merge_flag:1;
	unsigned merge_idx :3;
	unsigned mvp_l0_flag:1;
	unsigned mvp_l1_flag:1;
	unsigned ref_idx_l0 :4;
	unsigned ref_idx_l1 :4;
};
struct RU_0{
	unsigned header : 8;
	unsigned xT : 5;
	unsigned yT : 5;
	unsigned cidx : 2;
	unsigned size : 3;
	unsigned IntraMode :6;
	unsigned res_present:1;
	unsigned transform_skip_flag : 1;
};
struct RU_1 {
	unsigned header: 8;
	unsigned BSh:2;
	unsigned BSv:2;
	unsigned QP:6;
};

struct coeff {
	unsigned header : 6;
	unsigned x: 5;
	unsigned y: 5;
	signed val : 16;
};
struct fifo_element {
	signed a0 :9;
	signed a1 :9;
	signed a2 :9;
	signed a3 :9;
	signed b0 :9;
	signed b1 :9;
	signed b2 :9;
	signed b3 :9;
	signed c0 :9;
	signed c1 :9;
	signed c2 :9;
	signed c3 :9;
	signed d0 :9;
	signed d1 :9;
	signed d2 :9;
	signed d3 :9;
};

struct RU_0 h_40; // global needed because changing in res

unsigned int log2_int(unsigned int n);
#endif /* DEFINES_H_ */
