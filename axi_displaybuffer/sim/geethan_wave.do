onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /axi_displaybuffer_handle_wrapper/hdmi_fifo_almost_full
add wave -noupdate /axi_displaybuffer_handle_wrapper/hdmi_fifo_data_out
add wave -noupdate /axi_displaybuffer_handle_wrapper/hdmi_fifo_w_en
add wave -noupdate /axi_displaybuffer_handle_wrapper/displaybuffer_fifo_data_in
add wave -noupdate /axi_displaybuffer_handle_wrapper/displaybuffer_fifo_write_en_in
add wave -noupdate /axi_displaybuffer_handle_wrapper/reset
add wave -noupdate /axi_displaybuffer_handle_wrapper/displaybuffer_fifo_full_out
add wave -noupdate /axi_displaybuffer_handle_wrapper/displaybuffer_fifo_almost_empty_wire
add wave -noupdate /axi_displaybuffer_handle_wrapper/displaybuffer_fifo_read_en_wire
add wave -noupdate /axi_displaybuffer_handle_wrapper/displaybuffer_fifo_write_en_in
add wave -noupdate /axi_displaybuffer_handle_wrapper/hdmi_fifo_monitor_block/hdmi_pixel_arry
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {375110199 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 256
configure wave -valuecolwidth 332
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {375059128 ps} {375127222 ps}
