vlib work
vlog ../rtl/axi_displaybuffer_handle_wrapper.v
vlog ../rtl/truedual_port_blockram.v
vlog ../verif/*.v
vlog ../sim/axi_displaybuffer_handle_indepent_wrapper_tb.v
vlog ../sim/axi_displaybuffer_handle_translate.v
vlog  ../../verif/fifo_inout_monitor.v
vlog -sv ../../verif/memory_slave/rtl/*.v +incdir+../../verif/memory_slave/sim
vlog -sv ../../verif/memory_slave/rtl/*.sv +incdir+../../verif/memory_slave/sim
vlog  "F:/Xilinx/14.7/ISE_DS/ISE//verilog/src/glbl.v"
vsim -L simprims_ver -L secureip -voptargs="+acc" -t 1ps -sv_lib ../../verif/memory_slave/rtl/gb1_memory -lib work work.axi_displaybuffer_handle_indepent_wrapper_tb glbl

log -r /*