vlib work
vlog ../rtl/*.v
vlog ../verif/*.v
vlog -sv memory_slave/rtl/*.v +incdir+memory_slave/sim
vlog -sv memory_slave/rtl/*.sv +incdir+memory_slave/sim

vsim -voptargs="+acc" -t 1ps -sv_lib memory_slave/rtl/gb1_memory -lib work work.axi_displaybuffer_handle_wrapper -vcdstim vcd/sao_wr_inf.vcd

log -r /*