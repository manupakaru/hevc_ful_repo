
// ************************************************************************************************
//
// PROJECT      :   <project name where applicable>
// PRODUCT      :   <product name where applicable>
// FILE         :   <File Name/Module Name>
// AUTHOR       :   <Author's name>
// DESCRIPTION  :   <at least a short description about the module/file>
//                  Where does this file get inputs and send outputs?
//                  What does the guts of this file accomplish, and how does it do it?
//                  What module(s) does this file instantiate?
//
// ************************************************************************************************
//
// REVISIONS:
//
//	Date			Developer	Description
//	----			---------	-----------
//  29 Feb 2012		user123     bug fix - <jira issue id>
//  31 Oct 2007		userxyz		added xyz functionality - <jira issue id>
//  ...              ...         ...
//  ...              ...         ...
//
//**************************************************************************************************

`timescale 1ns / 1ps

module axi_displaybuffer_handle_indepent_with_dp_tb;
    //axi write channel
	`include "pred_def.v"
	`include "inter_axi_def.v"
	parameter                       AXI_BRESP_WIDTH = 2;
	
	reg clk;
	reg reset;
    wire                                                                axi_aclk;
    wire       [AXI_ADDR_WDTH - 1:0]                                    axi_awaddr;
    wire                                                                axi_awvalid;
    wire                                                                axi_awready;
    wire       [8 - 1:0]                                                axi_awlen;

    wire       [AXI_CACHE_DATA_WDTH - 1:0]                              axi_wdata;
    wire       [64 - 1:0]                                               axi_wstrb;
    wire                                                                axi_wlast;
    wire                                                                axi_wvalid;
    wire                                                                axi_wready;

    wire       [AXI_BRESP_WIDTH - 1:0]                                  axi_bresp;
    wire                                                                axi_bvalid;
    wire                                                                axi_bready;

    //axi read channel

    wire       [AXI_ADDR_WDTH - 1:0]                                    axi_araddr;
    wire                                                                axi_arvalid;
    wire                                                                axi_arready;
    wire       [8 - 1:0]                                                axi_arlen;
    wire       [3 - 1:0]                                                axi_arsize;
    wire       [2 - 1:0]                                                axi_arburst;

    wire       [AXI_CACHE_DATA_WDTH - 1:0]                              axi_rdata;
    wire       [2 - 1:0]                                                axi_rresp;
    wire                                                                axi_rlast;
    wire                                                                axi_rready;
    wire                                                                axi_rvalid;
	
	reg hdmi_fifo_almost_full;
	wire [63:0] hdmi_fifo_data_out;
	wire hdmi_fifo_w_en;
	
axi_displaybuffer_handle_indepent_wrapper_dp uut
    (
		.clk(clk),
		.reset(reset),
        //axi write interface
        .axi_awaddr							(axi_awaddr),
        .axi_awvalid						(axi_awvalid),
        .axi_awready						(axi_awready),
        .axi_awlen							(axi_awlen),

        .axi_wdata 							(axi_wdata),
        .axi_wstrb                          (axi_wstrb),
        .axi_wlast 							(axi_wlast),
        .axi_wvalid 						(axi_wvalid),
        .axi_wready 						(axi_wready),

        .axi_bresp 							(axi_bresp),
        .axi_bvalid  						(axi_bvalid),
        .axi_bready 						(axi_bready),

        //axi read interface
        .axi_araddr  						(axi_araddr),
        .axi_arvalid 						(axi_arvalid),
        .axi_arready 						(axi_arready),
        .axi_arlen  						(axi_arlen),
        .axi_arsize  						(axi_arsize),
        .axi_arburst  						(axi_arburst),

        .axi_rdata  						(axi_rdata),
        .axi_rresp  						(axi_rresp),
        .axi_rlast  						(axi_rlast),
        .axi_rready  						(axi_rready),
        .axi_rvalid  						(axi_rvalid)
    );
	

	initial begin
		clk = 0;
		reset  = 1;
		hdmi_fifo_almost_full = 0;
		#100;
		reset = 0;
	end
	
	always begin
		#10 clk = ~clk;
	end
// synthesis translate_off
    mem_slave_top_module
    #(.DUMMY_MEM(0),
      .READ_DATA_DEBUG(0),
      .WRITE_DATA_DEBUG(0))
    ddr_soft_mem_block
    (   .clk    (clk),
        .reset  (reset),

        .arid   (4'd0),
        .araddr (axi_araddr),
        .arlen  (axi_arlen),
        .arsize (axi_arsize),
        .arburst(2'b01),
        .arlock (),
        .arcache(),
        .arprot (),
        .arvalid(axi_arvalid),
        .arready(axi_arready),

        .rid    (),
        .rdata  (axi_rdata),
        .rresp  (axi_rresp),
        .rlast  (axi_rlast),
        .rvalid (axi_rvalid),
        .rready (axi_rready),

        .awid   (4'd0),
        .awaddr (axi_awaddr),
        .awlen  (axi_awlen),
        .awsize (3'b110),
        .awburst(2'b01),
        .awlock (),
        .awcache(),
        .awprot (),
        .awvalid(axi_awvalid),
        .awready(axi_awready),

        .wid    (1'b0),
        .wdata  (axi_wdata),
        .wstrb  (axi_wstrb),
        .wvalid (axi_wvalid),
        .wlast  (axi_wlast),
        .wready (axi_wready),

        .bid    (),
        .bresp  (axi_bresp),
        .bvalid (axi_bvalid),
        .bready  (1'b1)
     );




endmodule