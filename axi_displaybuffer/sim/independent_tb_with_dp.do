vlib work
vlog ../rtl/*.v
vlog ../verif/*.v
vlog  ../../verif/fifo_inout_monitor.v
vlog ../sim/axi_displaybuffer_handle_indepent_with_dp_tb.v
vlog -sv ../../verif/memory_slave/rtl/*.v +incdir+../../verif/memory_slave/sim
vlog -sv ../../verif/memory_slave/rtl/*.sv +incdir+../../verif/memory_slave/sim

vsim -voptargs="+acc" -t 1ps -sv_lib ../../verif/memory_slave/rtl/gb1_memory -lib work work.axi_displaybuffer_handle_indepent_with_dp_tb 

log -r /*