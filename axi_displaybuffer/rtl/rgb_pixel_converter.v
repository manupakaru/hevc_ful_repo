`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/23/2014 06:29:52 PM
// Design Name: 
// Module Name: rgb_pixel_converter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module rgb_pixel_converter(
		Y_in ,
		CB_in,
		CR_in,
		R_out,
		G_out,
		B_out
    );
	
	
//-------------------------------------------------------------------------------
// I/O signals
//-------------------------------------------------------------------------------
    
	input	[7:0]				Y_in;
	input	[7:0]				CB_in;
	input	[7:0]				CR_in;
	
	output	[7:0]				R_out;
	output	[7:0]				G_out;
	output	[7:0]				B_out;
	
//-------------------------------------------------------------------------------
// Internal wires and registers
//-------------------------------------------------------------------------------
    
	reg		[9:0]				R_add;
	reg		[9:0]				R_sub;
	reg		[9:0]				G_add;
	reg		[9:0]				G_sub;
	reg		[9:0]				B_add;
	reg		[9:0]				B_sub;
	
	wire	[9:0]				R_sub_n;
	wire	[9:0]				G_sub_n;
	wire	[9:0]				B_sub_n;
	
	wire	[9:0]				R_out_10;
	wire	[9:0]				G_out_10;
	wire	[9:0]				B_out_10;
	
	
//-------------------------------------------------------------------------------
// Implementation
//-------------------------------------------------------------------------------
	
	//----------------- Combinational Block to calculate intermediate signed values -------------------
	always @(*) begin
		R_add  =  Y_in + CR_in + CR_in[7:2] + CR_in[7:3] ;
		R_sub  =  10'd175 ;
		G_add  =  Y_in + 8'd132 ;
		G_sub  =  CB_in[7:2] + CB_in[7:4] + CR_in[7:1] + CR_in[7:3] + CR_in[7:4] ;		// In case of timing issues, replace with the following
		//G_sub  =  CB_in[7:2] + CB_in[7:4] + CR_in[7:1] + CR_in[7:2] ;
		B_add  =  Y_in + CB_in + CB_in[7:1] + CB_in[7:2] ;
		B_sub  =  10'd222 ;
	end
	
	
	assign	R_sub_n  =  ~({3'b000,R_sub}) + 1'b1 ;
	assign	G_sub_n  =  ~({3'b000,G_sub}) + 1'b1 ;
	assign	B_sub_n  =  ~({3'b000,B_sub}) + 1'b1 ;
	
	
	assign	R_out_10  =  R_add + R_sub_n ;					//  Y 				  + 1.371(CR-128)
	assign	G_out_10  =  G_add + G_sub_n ;                 	//  Y - 0.336(CB-128) - 0.698(CR-128)
	assign	B_out_10  =  B_add + B_sub_n ;                 	//  Y + 1.732(CB-128)
	
	assign	R_out  =  R_out_10[7:0] ;
	assign	G_out  =  G_out_10[7:0] ;
	assign	B_out  =  B_out_10[7:0] ;
	
	
	
	
endmodule
