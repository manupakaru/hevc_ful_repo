`timescale 1ns / 1ps
module dummy_display_fifo_driver(
    clk,
    reset,

	//fifo interface
	fifo_prog_full_in		,
	fifo_wr_en_out	    ,
	fifo_data_out		,
	
	poc_out,
	start,
    pic_width_out,
    pic_height_out


    
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
`include "pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------

	
	parameter YY_WIDTH = PIXEL_WIDTH*DBF_OUT_Y_BLOCK_SIZE*DBF_OUT_Y_BLOCK_SIZE;
	parameter CH_WIDTH = PIXEL_WIDTH*DBF_OUT_CH_BLOCK_SIZE*DBF_OUT_CH_BLOCK_SIZE;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    // localparam  STATE_AXI_WRITE_ADDRESS_SEND = 1;
    // localparam  STATE_AXI_WRITE_ADDRESS_SEND_WAIT = 2;
    // localparam  STATE_AXI_WRITE_WAIT = 3;
    // localparam  STATE_AXI_WRITE_RESP = 4;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
	input start;
    //luma sao fifo interface
	input 											fifo_prog_full_in	;
	output reg 										fifo_wr_en_out		;
	output  		[SAO_OUT_FIFO_WIDTH-1:0]		fifo_data_out		;
	
	output reg [3:0]								poc_out				;

	
    output     [PIC_WIDTH_WIDTH-1:0]    pic_width_out;
    output     [PIC_WIDTH_WIDTH-1:0]    pic_height_out;


//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

    
	reg [X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1: 0]				Xc_out_8x8;
	reg [X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1: 0]				Yc_out_8x8;	
	
	reg [PIXEL_WIDTH*DBF_OUT_Y_BLOCK_SIZE*DBF_OUT_Y_BLOCK_SIZE -1:0] 	yy_pixels_8x8;
	reg [PIXEL_WIDTH*DBF_OUT_CH_BLOCK_SIZE*DBF_OUT_CH_BLOCK_SIZE -1:0] cb_pixels_8x8;
	reg [PIXEL_WIDTH*DBF_OUT_CH_BLOCK_SIZE*DBF_OUT_CH_BLOCK_SIZE -1:0] cr_pixels_8x8;
	
	// reg [AXI_ADDR_WDTH -1:0]			current_pic_dpb_base_addr_d;
	// reg [AXI_ADDR_WDTH -1:0]			current_pic_dpb_base_addr_use;
  
    wire        [CTB_SIZE_WIDTH - LOG2_MIN_DU_SIZE - 1:0]      		y_8x8_in_ctu;
    wire        [CTB_SIZE_WIDTH - LOG2_MIN_DU_SIZE - 1:0]         	x_8x8_in_ctu;
    wire        [X11_ADDR_WDTH - CTB_SIZE_WIDTH - 1:0]          	x_ctu;
    wire        [X11_ADDR_WDTH - CTB_SIZE_WIDTH - 1:0]         		y_ctu;
	
	assign pic_width_out = 1920;
	assign pic_height_out = 1080;
	integer wr_en_counter;
	integer wait_counter;
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
	wire [X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1:0] pic_width_by_8 = pic_width_out[X11_ADDR_WDTH -1:LOG2_MIN_DU_SIZE];

					
	assign y_8x8_in_ctu = Yc_out_8x8[CTB_SIZE_WIDTH - LOG2_MIN_DU_SIZE - 1:0];
	assign x_8x8_in_ctu = Xc_out_8x8[CTB_SIZE_WIDTH - LOG2_MIN_DU_SIZE - 1:0];
	
	assign x_ctu = Xc_out_8x8[X11_ADDR_WDTH - LOG2_MIN_DU_SIZE - 1:CTB_SIZE_WIDTH - LOG2_MIN_DU_SIZE ];
	assign y_ctu = Yc_out_8x8[X11_ADDR_WDTH - LOG2_MIN_DU_SIZE - 1:CTB_SIZE_WIDTH - LOG2_MIN_DU_SIZE ];
		
	assign fifo_data_out = {Xc_out_8x8,Yc_out_8x8,yy_pixels_8x8,cb_pixels_8x8,cr_pixels_8x8};

	always@(posedge clk or posedge reset) begin
		if(reset) begin
			Xc_out_8x8 <= 0;
			Yc_out_8x8 <= 0;
			poc_out <= 0;
			yy_pixels_8x8 <= {YY_WIDTH{1'b1}};
			cb_pixels_8x8 <= {CH_WIDTH{1'b1}};
			cr_pixels_8x8 <= {CH_WIDTH{1'b1}};
			fifo_wr_en_out <= 0;
			wr_en_counter <= 0;
			wait_counter <= 0;
		end
		else begin
			if(wait_counter < 100) begin
				wait_counter <= wait_counter + 1;
				if(wait_counter == 0) begin
					fifo_wr_en_out <= 1;
				end
				else begin
					fifo_wr_en_out <= 0;
				end
					
			end
			else begin
				if(fifo_prog_full_in ==0) begin
					fifo_wr_en_out <= 1;
					wr_en_counter <= wr_en_counter + 1;
					if( (Xc_out_8x8[X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1:0] == pic_width_by_8 -1) && (Yc_out_8x8[X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1:0] == pic_height_out[X11_ADDR_WDTH -1:LOG2_MIN_DU_SIZE] -1))begin
						Xc_out_8x8 <= 0;
						Yc_out_8x8 <= 0;
						poc_out <= poc_out + 1;
						yy_pixels_8x8 <= {YY_WIDTH{1'b1}};
						cb_pixels_8x8 <= {CH_WIDTH{1'b1}};
						cr_pixels_8x8 <= {CH_WIDTH{1'b1}};
					end
					else begin
						if((Xc_out_8x8[X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1:0] == pic_width_by_8 -1) && (Yc_out_8x8[2:0] == 7)) begin
							Xc_out_8x8 <= 0;
							Yc_out_8x8[X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1:3] <= Yc_out_8x8[X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1:3] + 1;
							Yc_out_8x8[2:0] <= 0;
							yy_pixels_8x8[7:0] <= yy_pixels_8x8[7:0] + 2;
							yy_pixels_8x8[YY_WIDTH-1:8] <= yy_pixels_8x8[YY_WIDTH-8 -1:0];
							cb_pixels_8x8[7:0] <= cb_pixels_8x8[7:0] + 1;
							cb_pixels_8x8[CH_WIDTH-1:8] <= cb_pixels_8x8[CH_WIDTH-8 -1:0];
							cr_pixels_8x8[7:0] <= cr_pixels_8x8[7:0] - 1;
							cr_pixels_8x8[CH_WIDTH-1:8] <= cr_pixels_8x8[CH_WIDTH-8 -1:0];
						end
						else begin
							if(( (Yc_out_8x8[2:0] == 7) || (Yc_out_8x8[X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1:0] == pic_height_out[X11_ADDR_WDTH -1:LOG2_MIN_DU_SIZE] -1) ) && (Xc_out_8x8[2:0] == 7)) begin
								Yc_out_8x8[2:0] <= 0;
								Xc_out_8x8[2:0] <= 0;
								Xc_out_8x8[X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1:3] <= Xc_out_8x8[X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1:3] + 1;
								yy_pixels_8x8[7:0] <= yy_pixels_8x8[7:0] + 2;
								yy_pixels_8x8[YY_WIDTH-1:8] <= yy_pixels_8x8[YY_WIDTH-8 -1:0];
								cb_pixels_8x8[7:0] <= cb_pixels_8x8[7:0] + 1;
								cb_pixels_8x8[CH_WIDTH-1:8] <= cb_pixels_8x8[CH_WIDTH-8 -1:0];
								cr_pixels_8x8[7:0] <= cr_pixels_8x8[7:0] - 1;
								cr_pixels_8x8[CH_WIDTH-1:8] <= cr_pixels_8x8[CH_WIDTH-8 -1:0];
							end
							else begin
								if((Xc_out_8x8[2:0] == 7)) begin
									Xc_out_8x8[2:0] <= 0;
									Yc_out_8x8[2:0] <= Yc_out_8x8[2:0] + 1;
								end
								else begin
									Xc_out_8x8[2:0] <= Xc_out_8x8[2:0] + 1;
								end
							end
						end
					end
				end
				else begin
					fifo_wr_en_out <= 0;
				end
			
			end
		end
	end
	
	 // always@(*) begin
		// if(reset) begin
			// fifo_wr_en_out = 0;
		// end
		// else begin
			// fifo_wr_en_out = 0;
			// if(wait_counter < 200) begin
			// end
			// else begin
				// if(!fifo_prog_full_in) begin
					// fifo_wr_en_out = 1;
				// end
				// else begin
					// fifo_wr_en_out = 0;
				// end
			// end
			
		// end
	 // end

    
    

endmodule