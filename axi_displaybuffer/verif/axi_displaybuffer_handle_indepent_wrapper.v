
// ************************************************************************************************
//
// PROJECT      :   <project name where applicable>
// PRODUCT      :   <product name where applicable>
// FILE         :   <File Name/Module Name>
// AUTHOR       :   <Author's name>
// DESCRIPTION  :   <at least a short description about the module/file>
//                  Where does this file get inputs and send outputs?
//                  What does the guts of this file accomplish, and how does it do it?
//                  What module(s) does this file instantiate?
//
// ************************************************************************************************
//
// REVISIONS:
//
//	Date			Developer	Description
//	----			---------	-----------
//  29 Feb 2012		user123     bug fix - <jira issue id>
//  31 Oct 2007		userxyz		added xyz functionality - <jira issue id>
//  ...              ...         ...
//  ...              ...         ...
//
//**************************************************************************************************

`timescale 1ns / 1ps

module axi_displaybuffer_handle_indepent_wrapper
    (
		clk,
		reset,
		
        //axi write interface
        axi_awaddr,
        axi_awvalid,
        axi_awready,
        axi_awlen,

        axi_wdata,
        axi_wlast,
        axi_wvalid,
        axi_wready,
        axi_wstrb,

        axi_bresp,
        axi_bvalid,
        axi_bready,

        //axi read interface
        axi_araddr,
        axi_arvalid,
        axi_arready,
        axi_arlen,
        axi_arsize,
        axi_arburst,

        axi_rdata,
        axi_rresp,
        axi_rlast,
        axi_rready,
        axi_rvalid,

        hdmi_fifo_almost_full,
        hdmi_fifo_data_out,
        hdmi_fifo_w_en

    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
`include "pred_def.v"
`include "../sim/inter_axi_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
   // parameter                       MAX_LOG2CTBSIZE_WIDTH = 3;

    parameter                       AXI_BRESP_WIDTH = 2;
    parameter                       PROFILE         = 4;

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

    localparam      MAX_CTU_SIZE                = 64;
    localparam      MAX_CTU_SIZE_DIV_8          = MAX_CTU_SIZE/8;

	//localparam 		PIXEL_WIDTH 				= 8;
	//localparam 		OUTPUT_BLOCK_SIZE 			= 4;
	//localparam		PIXEL_ADDR_LENGTH           = 12;

    localparam      MODE_INTER                  = 1;
    localparam      MODE_INTRA                  = 0;

    localparam      STATE_WRITEUP_FIRST32       = 1;
    localparam      STATE_WRITEUP_SECOND32      = 2;
    localparam      STATE_DONE                  = 0;

    localparam      INNER_CTU_ADDR_LENGTH       = 6;
    //localparam      MAX_LOG2CTBSIZE_WIDTH       = 3;

    localparam      DISPLAY_BUFFER_FRAME1_PTR   =   32'h8800000;
    localparam      DISPLAY_BUFFER_FRAME2_PTR   =   32'h97D2000;
    localparam      DISPLAY_BUFFER_FRAME3_PTR   =   32'hA7A4000;
    localparam      DISPLAY_BUFFER_FRAME4_PTR   =   32'hB776000;
    localparam      DISPLAY_BUFFER_FRAME5_PTR   =   32'hC748000;

    localparam      AXI_WR_STATE_ROW_1 = 1;
    localparam      AXI_WR_STATE_ROW_2 = 2;
    localparam      AXI_WR_STATE_ROW_3 = 3;
    localparam      AXI_WR_STATE_ROW_4 = 4;
    localparam      AXI_WR_STATE_ROW_5 = 5;
    localparam      AXI_WR_STATE_ROW_6 = 6;
    localparam      AXI_WR_STATE_ROW_7 = 7;
    localparam      AXI_WR_STATE_ROW_8 = 8;

    localparam      MAX_LUMA_PS_L4                  = 2_228_224;
    localparam      QUARTER_MAX_LUMA_PS_L4          = MAX_LUMA_PS_L4/4;
    localparam      HALF_MAX_LUMA_PS_L4             = MAX_LUMA_PS_L4/2;
    localparam      THREE_QUARTER_MAX_LUMA_PS_L4    = 3*MAX_LUMA_PS_L4/4;

    localparam      MAX_LUMA_PS_L5                  = 8_912_896;
    localparam      QUARTER_MAX_LUMA_PS_L5          = MAX_LUMA_PS_L5/4;
    localparam      HALF_MAX_LUMA_PS_L5             = MAX_LUMA_PS_L5/2;
    localparam      THREE_QUARTER_MAX_LUMA_PS_L5    = 3*MAX_LUMA_PS_L5/4;

    localparam      CONFIG_STATE_REGISTER_INPUTS    = 1;
    localparam      CONFIG_STATE_FIDX_CALC_WAIT1    = 2;
    localparam      CONFIG_STATE_FIDX_CALC_WAIT2    = 3;
    localparam      CONFIG_STATE_FIDX_CALC_WAIT3    = 4;
    localparam      CONFIG_STATE_FIDX_CALC_WAIT4    = 5;
    localparam      CONFIG_STATE_FIDX_CALC_WAIT5    = 6;
    localparam      CONFIG_STATE_FIDX_CALC_WAIT6    = 7;
    localparam      CONFIG_STATE_DONE               = 8;

    localparam      AXI_WRADDR_STATE_PREINIT = 0;
    localparam      AXI_WRADDR_STATE_INIT = 1;
    localparam      AXI_WRADDR_STATE_1ST_8X8 = 2;
    localparam      AXI_WRADDR_STATE_2ND_8X8 = 3;
    localparam      AXI_WRADDR_STATE_3RD_8X8 = 3;
    localparam      AXI_WRADDR_STATE_4TH_8X8 = 4;
    localparam      AXI_WRADDR_STATE_5TH_8X8 = 5;
    localparam      AXI_WRADDR_STATE_6TH_8X8 = 6;
    localparam      AXI_WRADDR_STATE_7TH_8X8 = 7;
    localparam      AXI_WRADDR_STATE_8TH_8X8 = 8;

    localparam      AXI_WRDATA_STATE_INIT = 0;
    localparam      AXI_WRDATA_STATE_ACTIVE = 1;

    localparam      AXI_RDADDR_STATE_INIT = 0;
    localparam      AXI_RDADDR_STATE_ACTIVE = 1;

    localparam      AXI_RDDATA_STATE_ACTIVE_0 = 0;
    localparam      AXI_RDDATA_STATE_ACTIVE_1 = 1;




//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input 																clk;
    input 																reset;

    wire                                                               displaybuffer_fifo_write_en_in;
    wire                                                               displaybuffer_fifo_full_out;
    wire     [SAO_OUT_FIFO_WIDTH - 1:0]                                displaybuffer_fifo_data_in;



//     SAO output fifo
//      X_8x8addr   Y_8x8addr   LUMA_8x8    CB_8x8(4by4)    CR_8x8(4by4)
// bits    8            8       512         128              128             784



    //hdmi channel
    input                                                               hdmi_fifo_almost_full;
    output     [8*PIXEL_WIDTH - 1:0]                                    hdmi_fifo_data_out;
    output                                                              hdmi_fifo_w_en;

   //axi write channel


    output  [AXI_ADDR_WDTH - 1:0]                                    axi_awaddr;
    output                                                           axi_awvalid;
    input                                                               axi_awready;
    output  [8 - 1:0]                                                axi_awlen;

    output  [AXI_CACHE_DATA_WDTH - 1:0]                              axi_wdata;
    output                                                           axi_wlast;
    output                                                           axi_wvalid;
    input                                                               axi_wready;
    output  [64 - 1:0]                                               axi_wstrb;

    input      [AXI_BRESP_WIDTH - 1:0]                                  axi_bresp;
    input                                                               axi_bvalid;
    output                                                           axi_bready;

    //axi read channel

    output  [AXI_ADDR_WDTH - 1:0]                                    axi_araddr;
    output                                                           axi_arvalid;
    input                                                               axi_arready;
    output  [8 - 1:0]                                                axi_arlen;
    output  [3 - 1:0]                                                axi_arsize;
    output  [2 - 1:0]                                                axi_arburst;

    input      [AXI_CACHE_DATA_WDTH - 1:0]                              axi_rdata;
    input      [2 - 1:0]                                                axi_rresp;
    input                                                               axi_rlast;
    output                                                           axi_rready;
    input                                                               axi_rvalid;




//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

    wire                                                                displaybuffer_fifo_read_en_wire;
    wire                                                                displaybuffer_fifo_empty_wire;
    wire                                                                displaybuffer_fifo_almost_empty_wire;
    wire     [SAO_OUT_FIFO_WIDTH - 1:0]                                 displaybuffer_fifo_data_wire;





	
	wire [3:0]							poc_4bits_in				;

	
    wire     [PIC_WIDTH_WIDTH-1:0]    	pic_width_in;
    wire     [PIC_WIDTH_WIDTH-1:0]    	pic_height_in;


//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
dummy_display_fifo_driver dummy_display_fifo_driver_block(
    .clk(clk),
    .reset(reset),
	.start(1'b1),
	//fifo interface
	.fifo_prog_full_in(displaybuffer_fifo_full_out)		,
	.fifo_wr_en_out(displaybuffer_fifo_write_en_in)	    ,
	.fifo_data_out(displaybuffer_fifo_data_in)		,
	
	.poc_out(poc_4bits_in),

    .pic_width_out(pic_width_in),
    .pic_height_out(pic_height_in)


    
);
    geet_fifo_displaybuffer #(
        .FIFO_DATA_WIDTH(SAO_OUT_FIFO_WIDTH),
        .LOG2_FIFO_DEPTH(4)
    )
    geet_fifo_displaybuffer_block(
        .clk(clk),
        .reset(reset),
        .wr_en(displaybuffer_fifo_write_en_in),
        .rd_en(displaybuffer_fifo_read_en_wire),
        .d_in(displaybuffer_fifo_data_in),
        .d_out(displaybuffer_fifo_data_wire),
        .empty(displaybuffer_fifo_empty_wire),
        .almost_empty(displaybuffer_fifo_almost_empty_wire),
        .almost_full(displaybuffer_fifo_full_out),
        .program_full(),
        .full()
    );


   axi_displaybuffer_handle //#(.DISPBUF_BASE_ADDR(32'h3000_0000))
   axi_displaybuffer_handle_block
    (
		.clk								(clk),
		.reset 								(reset),

        .displaybuffer_fifo_read_en_out		(displaybuffer_fifo_read_en_wire),
        .displaybuffer_fifo_empty_in		(displaybuffer_fifo_empty_wire),
        // .displaybuffer_fifo_almost_empty_in (displaybuffer_fifo_almost_empty_wire),
        .displaybuffer_fifo_data_in			(displaybuffer_fifo_data_wire),

        .log2_ctu_size_in					(3'd3),
        .poc_4bits_in						(poc_4bits_in),
        .pic_width_in						(pic_width_in),
        .pic_height_in						(pic_height_in),
        .config_valid_in					(1'b1),

        //axi write interface
        //.axi_aclk							(clk),
        .axi_awaddr							(axi_awaddr),
        .axi_awvalid						(axi_awvalid),
        .axi_awready						(axi_awready),
        .axi_awlen							(axi_awlen),

        .axi_wdata 							(axi_wdata),
        .axi_wstrb                          (axi_wstrb),
        .axi_wlast 							(axi_wlast),
        .axi_wvalid 						(axi_wvalid),
        .axi_wready 						(axi_wready),

        .axi_bresp 							(axi_bresp),
        .axi_bvalid  						(axi_bvalid),
        .axi_bready 						(axi_bready),

        //axi read interface
        .axi_araddr  						(axi_araddr),
        .axi_arvalid 						(axi_arvalid),
        .axi_arready 						(axi_arready),
        .axi_arlen  						(axi_arlen),
        .axi_arsize  						(axi_arsize),
        .axi_arburst  						(axi_arburst),

        .axi_rdata  						(axi_rdata),
        .axi_rresp  						(axi_rresp),
        .axi_rlast  						(axi_rlast),
        .axi_rready  						(axi_rready),
        .axi_rvalid  						(axi_rvalid),

        //hdmi interface
        .hdmi_fifo_almost_full 				(hdmi_fifo_almost_full),
        .hdmi_fifo_data_out  				(hdmi_fifo_data_out),
        .hdmi_fifo_w_en 					(hdmi_fifo_w_en)

    );



endmodule