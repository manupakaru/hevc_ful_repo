`timescale 1ns / 1ps
module hdmi_fifo_monitor(
    clk,
    reset,
	full,
	empty,
	rd_en,
	hdmi_fifo_in,
	pic_width_in,
	pic_height_in,
	wr_en
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
	`include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------

	parameter FILE_NAME           = "";
	parameter OUT_VERIFY  = 1;
	parameter DEBUG = 0;
	parameter FIFO_WIDTH_IN_BYTES = 8;
	

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
	input wr_en,full,rd_en,empty; 
	input [FIFO_WIDTH_IN_BYTES*8-1:0] hdmi_fifo_in; 

	input     [PIC_WIDTH_WIDTH-1:0]    pic_width_in;
	input     [PIC_WIDTH_WIDTH-1:0]    pic_height_in;
	
	
// synthesis translate_off
	integer file_in;
	integer i,j;
	reg [7:0] temp;
	integer rfile;
	integer cb_offset, cr_offset;
	
	integer frame_count;
	integer frame_offset;
	
	integer luma_pointer;
	integer cb_pointer;
	integer cr_pointer;
	reg cb_cr_go_back;
	
	wire [7:0] hdmi_pixel_arry[0:FIFO_WIDTH_IN_BYTES-1];
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module
generate
        genvar ii;
        for (ii = 0 ; ii < FIFO_WIDTH_IN_BYTES ; ii = ii + 1) begin
			assign  hdmi_pixel_arry[ii]= hdmi_fifo_in[(ii+1)*8-1:ii*8];
        end
endgenerate


initial begin
	if(OUT_VERIFY) begin
		frame_count = 0;
		file_in = $fopen(FILE_NAME,"rb");
		if(file_in) begin
			
		end
		else begin
			$display("file not open!!");
			$stop;
		end
	end	
	frame_offset = 0;
	cb_offset = 0;
	cr_offset = 0;
	
	luma_pointer = 0;
	cb_pointer = 0;
	cr_pointer = 0;
	cb_cr_go_back = 1;
end




always@(posedge clk) begin
   if(reset) begin
	end
	else begin
		if(!full) begin
			if(wr_en) begin
				if(DEBUG ==1) begin
					$display("%d write to fifo %m =%x",$time,hdmi_fifo_in);
				end
            
				if(OUT_VERIFY) begin
			
					if(luma_pointer == 0 & frame_offset == 0) begin // do this only at very first time
						cb_offset  = pic_width_in*pic_height_in;
						cr_offset  = cb_offset + pic_width_in*pic_height_in/4;						
					end
					
					for(i=0; i<FIFO_WIDTH_IN_BYTES/2;i=i+1) begin
						rfile = $fseek(file_in,luma_pointer + frame_offset,0);
						temp= $fgetc(file_in);
						if(temp != hdmi_pixel_arry[2*i]) begin
							$display("%d hdmi in luma wrong hard %x soft %x at %d",$time,hdmi_pixel_arry[2*i],temp,luma_pointer);
							$stop;
						end
						luma_pointer = luma_pointer + 1;
						if(cb_pointer == cr_pointer) begin
							rfile = $fseek(file_in,cb_pointer + cb_offset,0);
							temp= $fgetc(file_in);
							if(temp != hdmi_pixel_arry[2*i+1]) begin
								$display("%d hdmi in cb wrong hard %x soft %x at %d",$time,hdmi_pixel_arry[2*i+1],temp,cb_pointer);
								$stop;
							end
							cb_pointer = cb_pointer + 1;
						end
						else begin
							rfile = $fseek(file_in,cr_pointer + cr_offset,0);
							temp= $fgetc(file_in);
							if(temp != hdmi_pixel_arry[2*i+1]) begin
								$display("%d hdmi in cr wrong hard %x soft %x at %d",$time,hdmi_pixel_arry[2*i+1],temp,cr_pointer);
								$stop;
							end
							cr_pointer = cr_pointer + 1;
						end
					end
					
					if(luma_pointer%pic_width_in==0) begin 
						if(cb_cr_go_back) begin
							cb_cr_go_back = 0;
							cb_offset = cb_offset - pic_width_in/2;
							cr_offset = cr_offset - pic_width_in/2;
						end
						else begin
							cb_cr_go_back = 1;
						end
					end
					if(luma_pointer == pic_width_in*pic_height_in) begin
						
	          cb_offset  = pic_width_in*pic_height_in+frame_offset;
	          cr_offset  = cb_offset + pic_width_in*pic_height_in/4;						

						luma_pointer = 0; 
						cb_pointer = 0;
						cr_pointer = 0;
					end
				end
			end
		end
		else begin
			if(wr_en) begin
				$display("%d trying to write while full %m!!",$time);
				$stop;
			end
		end
	end
end

always@(posedge clk) begin
  if(hevc_test_tb.uut.axi_displaybuffer_handle_block.frame_ddr_rd_done_out == 1) begin
    frame_count = frame_count + 1;
 	  frame_offset = frame_count*pic_width_in*pic_height_in*3/2;
	  cb_offset  = pic_width_in*pic_height_in+frame_offset;
	  cr_offset  = cb_offset + pic_width_in*pic_height_in/4;
	  if(luma_pointer != 0) begin
	    $display("frame offset update in the middle of a frame");
	    $stop();
	  end
	end
end

always@(posedge clk) begin
	if(!empty) begin
		if(rd_en) begin
			
		end
	end
	else begin
		$display("%d trying to read while empty %m!!",$time);
		$stop;
	end
end

// synthesis translate_on
endmodule