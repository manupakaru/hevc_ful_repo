// (c) Copyright 1995-2014 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.

// IP VLNV: xilinx.com:user:cabac_ngc:1.12
// IP Revision: 11

// The following must be inserted into your Verilog file for this
// core to be instantiated. Change the instance name and port connections
// (in parentheses) to your own signal names.

//----------- Begin Cut here for INSTANTIATION Template ---// INST_TAG
zynq_design_1_cabac_ngc_0_0 your_instance_name (
  .clk(clk),                                                            // input wire clk
  .rst(rst),                                                            // input wire rst
  .axi_arlock(axi_arlock),                                              // input wire axi_arlock
  .axi_arvalid(axi_arvalid),                                            // input wire axi_arvalid
  .axi_awlock(axi_awlock),                                              // input wire axi_awlock
  .axi_awvalid(axi_awvalid),                                            // input wire axi_awvalid
  .axi_bready(axi_bready),                                              // input wire axi_bready
  .axi_rready(axi_rready),                                              // input wire axi_rready
  .axi_wlast(axi_wlast),                                                // input wire axi_wlast
  .axi_wvalid(axi_wvalid),                                              // input wire axi_wvalid
  .fullCont(fullCont),                                                  // input wire fullCont
  .fullY(fullY),                                                        // input wire fullY
  .fullCb(fullCb),                                                      // input wire fullCb
  .fullCr(fullCr),                                                      // input wire fullCr
  .axi_arready(axi_arready),                                            // output wire axi_arready
  .axi_awready(axi_awready),                                            // output wire axi_awready
  .axi_bvalid(axi_bvalid),                                              // output wire axi_bvalid
  .axi_rlast(axi_rlast),                                                // output wire axi_rlast
  .axi_rvalid(axi_rvalid),                                              // output wire axi_rvalid
  .axi_wready(axi_wready),                                              // output wire axi_wready
  .validCont(validCont),                                                // output wire validCont
  .validY(validY),                                                      // output wire validY
  .validCb(validCb),                                                    // output wire validCb
  .validCr(validCr),                                                    // output wire validCr
  .input_hevc_full_out_test(input_hevc_full_out_test),                  // output wire input_hevc_full_out_test
  .input_hevc_empty_out_test(input_hevc_empty_out_test),                // output wire input_hevc_empty_out_test
  .input_hevc_almost_empty_out_test(input_hevc_almost_empty_out_test),  // output wire input_hevc_almost_empty_out_test
  .push_full_out_test(push_full_out_test),                              // output wire push_full_out_test
  .push_empty_out_test(push_empty_out_test),                            // output wire push_empty_out_test
  .trafo_clk_en(trafo_clk_en),                                          // output wire trafo_clk_en
  .cabac_to_res_wr_en(cabac_to_res_wr_en),                              // output wire cabac_to_res_wr_en
  .push_empty(push_empty),                                              // output wire push_empty
  .push_read(push_read),                                                // output wire push_read
  .axi_araddr(axi_araddr),                                              // input wire [15 : 0] axi_araddr
  .axi_arburst(axi_arburst),                                            // input wire [1 : 0] axi_arburst
  .axi_arcache(axi_arcache),                                            // input wire [3 : 0] axi_arcache
  .axi_arid(axi_arid),                                                  // input wire [11 : 0] axi_arid
  .axi_arlen(axi_arlen),                                                // input wire [7 : 0] axi_arlen
  .axi_arprot(axi_arprot),                                              // input wire [2 : 0] axi_arprot
  .axi_arsize(axi_arsize),                                              // input wire [2 : 0] axi_arsize
  .axi_awaddr(axi_awaddr),                                              // input wire [15 : 0] axi_awaddr
  .axi_awburst(axi_awburst),                                            // input wire [1 : 0] axi_awburst
  .axi_awcache(axi_awcache),                                            // input wire [3 : 0] axi_awcache
  .axi_awid(axi_awid),                                                  // input wire [11 : 0] axi_awid
  .axi_awlen(axi_awlen),                                                // input wire [7 : 0] axi_awlen
  .axi_awprot(axi_awprot),                                              // input wire [2 : 0] axi_awprot
  .axi_awsize(axi_awsize),                                              // input wire [2 : 0] axi_awsize
  .axi_wdata(axi_wdata),                                                // input wire [31 : 0] axi_wdata
  .axi_wstrb(axi_wstrb),                                                // input wire [3 : 0] axi_wstrb
  .axi_bid(axi_bid),                                                    // output wire [11 : 0] axi_bid
  .axi_bresp(axi_bresp),                                                // output wire [1 : 0] axi_bresp
  .axi_rdata(axi_rdata),                                                // output wire [31 : 0] axi_rdata
  .axi_rid(axi_rid),                                                    // output wire [11 : 0] axi_rid
  .axi_rresp(axi_rresp),                                                // output wire [1 : 0] axi_rresp
  .outCont(outCont),                                                    // output wire [31 : 0] outCont
  .outY(outY),                                                          // output wire [143 : 0] outY
  .outCb(outCb),                                                        // output wire [143 : 0] outCb
  .outCr(outCr),                                                        // output wire [143 : 0] outCr
  .B_test(B_test),                                                      // output wire [31 : 0] B_test
  .cu_state(cu_state),                                                  // output wire [6 : 0] cu_state
  .ru_state(ru_state),                                                  // output wire [4 : 0] ru_state
  .total_push(total_push),                                              // output wire [31 : 0] total_push
  .total_config(total_config),                                          // output wire [31 : 0] total_config
  .total_y(total_y),                                                    // output wire [31 : 0] total_y
  .total_cb(total_cb),                                                  // output wire [31 : 0] total_cb
  .total_cr(total_cr),                                                  // output wire [31 : 0] total_cr
  .cu_x0(cu_x0),                                                        // output wire [11 : 0] cu_x0
  .cu_y0(cu_y0),                                                        // output wire [11 : 0] cu_y0
  .cu_other(cu_other),                                                  // output wire [255 : 0] cu_other
  .trafo_x0(trafo_x0),                                                  // output wire [11 : 0] trafo_x0
  .trafo_y0(trafo_y0),                                                  // output wire [11 : 0] trafo_y0
  .trafo_POC(trafo_POC),                                                // output wire [15 : 0] trafo_POC
  .axi_count(axi_count),                                                // output wire [31 : 0] axi_count
  .axi_data(axi_data),                                                  // output wire [31 : 0] axi_data
  .axi_addr(axi_addr),                                                  // output wire [15 : 0] axi_addr
  .axi_state(axi_state),                                                // output wire [2 : 0] axi_state
  .cabac_to_res_data_count(cabac_to_res_data_count),                    // output wire [9 : 0] cabac_to_res_data_count
  .input_hevc_data_count(input_hevc_data_count),                        // output wire [13 : 0] input_hevc_data_count
  .cabac_to_res_data(cabac_to_res_data),                                // output wire [31 : 0] cabac_to_res_data
  .push_data_out(push_data_out),                                        // output wire [31 : 0] push_data_out
  .port5(port5),                                                        // output wire [27 : 0] port5
  .port6(port6),                                                        // output wire [27 : 0] port6
  .port7(port7),                                                        // output wire [26 : 0] port7
  .port8(port8),                                                        // output wire [71 : 0] port8
  .port9(port9)                                                        // output wire [5 : 0] port9
);
// INST_TAG_END ------ End INSTANTIATION Template ---------

// You must compile the wrapper file zynq_design_1_cabac_ngc_0_0.v when simulating
// the core, zynq_design_1_cabac_ngc_0_0. When compiling the wrapper file, be sure to
// reference the Verilog simulation library.

