// (c) Copyright 1995-2014 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.

// IP VLNV: xilinx.com:ip:hevc_top_wo_fifo:1.2
// IP Revision: 9

// The following must be inserted into your Verilog file for this
// core to be instantiated. Change the instance name and port connections
// (in parentheses) to your own signal names.

//----------- Begin Cut here for INSTANTIATION Template ---// INST_TAG
zynq_design_1_hevc_top_wo_fifo_0_0 your_instance_name (
  .clk(clk),                                                      // input wire clk
  .reset(reset),                                                  // input wire reset
  .enable(enable),                                                // input wire enable
  .input_fifo_is_empty(input_fifo_is_empty),                      // input wire input_fifo_is_empty
  .y_residual_fifo_is_empty_in(y_residual_fifo_is_empty_in),      // input wire y_residual_fifo_is_empty_in
  .cb_residual_fifo_is_empty_in(cb_residual_fifo_is_empty_in),    // input wire cb_residual_fifo_is_empty_in
  .cr_residual_fifo_is_empty_in(cr_residual_fifo_is_empty_in),    // input wire cr_residual_fifo_is_empty_in
  .display_fifo_rd_en_in(display_fifo_rd_en_in),                  // input wire display_fifo_rd_en_in
  .mv_col_axi_awready(mv_col_axi_awready),                        // input wire mv_col_axi_awready
  .mv_col_axi_wready(mv_col_axi_wready),                          // input wire mv_col_axi_wready
  .mv_col_axi_bvalid(mv_col_axi_bvalid),                          // input wire mv_col_axi_bvalid
  .mv_pref_axi_arready(mv_pref_axi_arready),                      // input wire mv_pref_axi_arready
  .mv_pref_axi_rlast(mv_pref_axi_rlast),                          // input wire mv_pref_axi_rlast
  .mv_pref_axi_rvalid(mv_pref_axi_rvalid),                        // input wire mv_pref_axi_rvalid
  .ref_pix_axi_ar_ready(ref_pix_axi_ar_ready),                    // input wire ref_pix_axi_ar_ready
  .ref_pix_axi_r_last(ref_pix_axi_r_last),                        // input wire ref_pix_axi_r_last
  .ref_pix_axi_r_valid(ref_pix_axi_r_valid),                      // input wire ref_pix_axi_r_valid
  .ref_pic_write_axi_awready(ref_pic_write_axi_awready),          // input wire ref_pic_write_axi_awready
  .ref_pic_write_axi_wready(ref_pic_write_axi_wready),            // input wire ref_pic_write_axi_wready
  .ref_pic_write_axi_bid(ref_pic_write_axi_bid),                  // input wire ref_pic_write_axi_bid
  .ref_pic_write_axi_bvalid(ref_pic_write_axi_bvalid),            // input wire ref_pic_write_axi_bvalid
  .read_en_out(read_en_out),                                      // output wire read_en_out
  .y_residual_fifo_is_empty_out(y_residual_fifo_is_empty_out),    // output wire y_residual_fifo_is_empty_out
  .y_residual_fifo_read_en_out(y_residual_fifo_read_en_out),      // output wire y_residual_fifo_read_en_out
  .cb_residual_fifo_is_empty_out(cb_residual_fifo_is_empty_out),  // output wire cb_residual_fifo_is_empty_out
  .cb_residual_fifo_read_en_out(cb_residual_fifo_read_en_out),    // output wire cb_residual_fifo_read_en_out
  .cr_residual_fifo_is_empty_out(cr_residual_fifo_is_empty_out),  // output wire cr_residual_fifo_is_empty_out
  .cr_residual_fifo_read_en_out(cr_residual_fifo_read_en_out),    // output wire cr_residual_fifo_read_en_out
  .display_fifo_is_empty_out(display_fifo_is_empty_out),          // output wire display_fifo_is_empty_out
  .mv_col_axi_awid(mv_col_axi_awid),                              // output wire mv_col_axi_awid
  .mv_col_axi_awlock(mv_col_axi_awlock),                          // output wire mv_col_axi_awlock
  .mv_col_axi_awvalid(mv_col_axi_awvalid),                        // output wire mv_col_axi_awvalid
  .mv_col_axi_wlast(mv_col_axi_wlast),                            // output wire mv_col_axi_wlast
  .mv_col_axi_wvalid(mv_col_axi_wvalid),                          // output wire mv_col_axi_wvalid
  .mv_col_axi_bready(mv_col_axi_bready),                          // output wire mv_col_axi_bready
  .mv_pref_axi_arvalid(mv_pref_axi_arvalid),                      // output wire mv_pref_axi_arvalid
  .mv_pref_axi_rready(mv_pref_axi_rready),                        // output wire mv_pref_axi_rready
  .mv_pref_axi_arlock(mv_pref_axi_arlock),                        // output wire mv_pref_axi_arlock
  .mv_pref_axi_arid(mv_pref_axi_arid),                            // output wire mv_pref_axi_arid
  .ref_pix_axi_ar_valid(ref_pix_axi_ar_valid),                    // output wire ref_pix_axi_ar_valid
  .ref_pix_axi_r_ready(ref_pix_axi_r_ready),                      // output wire ref_pix_axi_r_ready
  .ref_pic_write_axi_awid(ref_pic_write_axi_awid),                // output wire ref_pic_write_axi_awid
  .ref_pic_write_axi_awlock(ref_pic_write_axi_awlock),            // output wire ref_pic_write_axi_awlock
  .ref_pic_write_axi_awvalid(ref_pic_write_axi_awvalid),          // output wire ref_pic_write_axi_awvalid
  .ref_pic_write_axi_wlast(ref_pic_write_axi_wlast),              // output wire ref_pic_write_axi_wlast
  .ref_pic_write_axi_wvalid(ref_pic_write_axi_wvalid),            // output wire ref_pic_write_axi_wvalid
  .ref_pic_write_axi_bready(ref_pic_write_axi_bready),            // output wire ref_pic_write_axi_bready
  .dbf_main_in_valid(dbf_main_in_valid),                          // output wire dbf_main_in_valid
  .dbf_main_out_valid(dbf_main_out_valid),                        // output wire dbf_main_out_valid
  .sao_main_out_valid(sao_main_out_valid),                        // output wire sao_main_out_valid
  .controller_in_valid(controller_in_valid),                      // output wire controller_in_valid
  .controller_out_valid(controller_out_valid),                    // output wire controller_out_valid
  .sao_wr_en_out(sao_wr_en_out),                                  // output wire sao_wr_en_out
  .sao_rd_en_out(sao_rd_en_out),                                  // output wire sao_rd_en_out
  .intra_4x4_valid_out(intra_4x4_valid_out),                      // output wire intra_4x4_valid_out
  .inter_4x4_valid_out(inter_4x4_valid_out),                      // output wire inter_4x4_valid_out
  .comon_pre_lp_4x4_valid_out(comon_pre_lp_4x4_valid_out),        // output wire comon_pre_lp_4x4_valid_out
  .dbf_to_sao_out_valid(dbf_to_sao_out_valid),                    // output wire dbf_to_sao_out_valid
  .test_luma_filter_ready(test_luma_filter_ready),                // output wire test_luma_filter_ready
  .test_cache_valid_in(test_cache_valid_in),                      // output wire test_cache_valid_in
  .test_cache_en(test_cache_en),                                  // output wire test_cache_en
  .test_ref_block_en(test_ref_block_en),                          // output wire test_ref_block_en
  .cb_res_pres_wr_en(cb_res_pres_wr_en),                          // output wire cb_res_pres_wr_en
  .cb_res_pres_rd_en(cb_res_pres_rd_en),                          // output wire cb_res_pres_rd_en
  .cb_res_pres_empty(cb_res_pres_empty),                          // output wire cb_res_pres_empty
  .cb_res_pres_full(cb_res_pres_full),                            // output wire cb_res_pres_full
  .cb_res_pres_din(cb_res_pres_din),                              // output wire cb_res_pres_din
  .cb_res_pres_dout(cb_res_pres_dout),                            // output wire cb_res_pres_dout
  .display_buf_ful(display_buf_ful),                              // output wire display_buf_ful
  .y_rd_data_count_in(y_rd_data_count_in),                        // input wire [12 : 0] y_rd_data_count_in
  .cb_rd_data_count_in(cb_rd_data_count_in),                      // input wire [10 : 0] cb_rd_data_count_in
  .cr_rd_data_count_in(cr_rd_data_count_in),                      // input wire [10 : 0] cr_rd_data_count_in
  .fifo_in(fifo_in),                                              // input wire [31 : 0] fifo_in
  .y_residual_fifo_in(y_residual_fifo_in),                        // input wire [143 : 0] y_residual_fifo_in
  .cb_residual_fifo_in(cb_residual_fifo_in),                      // input wire [143 : 0] cb_residual_fifo_in
  .cr_residual_fifo_in(cr_residual_fifo_in),                      // input wire [143 : 0] cr_residual_fifo_in
  .mv_col_axi_bresp(mv_col_axi_bresp),                            // input wire [1 : 0] mv_col_axi_bresp
  .mv_pref_axi_rdata(mv_pref_axi_rdata),                          // input wire [511 : 0] mv_pref_axi_rdata
  .mv_pref_axi_rresp(mv_pref_axi_rresp),                          // input wire [1 : 0] mv_pref_axi_rresp
  .ref_pix_axi_r_data(ref_pix_axi_r_data),                        // input wire [511 : 0] ref_pix_axi_r_data
  .ref_pix_axi_r_resp(ref_pix_axi_r_resp),                        // input wire [1 : 0] ref_pix_axi_r_resp
  .ref_pic_write_axi_bresp(ref_pic_write_axi_bresp),              // input wire [1 : 0] ref_pic_write_axi_bresp
  .yy_prog_empty_thresh_out(yy_prog_empty_thresh_out),            // output wire [11 : 0] yy_prog_empty_thresh_out
  .cb_prog_empty_thresh_out(cb_prog_empty_thresh_out),            // output wire [9 : 0] cb_prog_empty_thresh_out
  .cr_prog_empty_thresh_out(cr_prog_empty_thresh_out),            // output wire [9 : 0] cr_prog_empty_thresh_out
  .display_fifo_data_out(display_fifo_data_out),                  // output wire [783 : 0] display_fifo_data_out
  .sao_poc_out(sao_poc_out),                                      // output wire [31 : 0] sao_poc_out
  .log2_ctu_size_out(log2_ctu_size_out),                          // output wire [2 : 0] log2_ctu_size_out
  .pic_width_out(pic_width_out),                                  // output wire [11 : 0] pic_width_out
  .pic_height_out(pic_height_out),                                // output wire [11 : 0] pic_height_out
  .mv_col_axi_awlen(mv_col_axi_awlen),                            // output wire [7 : 0] mv_col_axi_awlen
  .mv_col_axi_awsize(mv_col_axi_awsize),                          // output wire [2 : 0] mv_col_axi_awsize
  .mv_col_axi_awburst(mv_col_axi_awburst),                        // output wire [1 : 0] mv_col_axi_awburst
  .mv_col_axi_awcache(mv_col_axi_awcache),                        // output wire [3 : 0] mv_col_axi_awcache
  .mv_col_axi_awprot(mv_col_axi_awprot),                          // output wire [2 : 0] mv_col_axi_awprot
  .mv_col_axi_awaddr(mv_col_axi_awaddr),                          // output wire [31 : 0] mv_col_axi_awaddr
  .mv_col_axi_wstrb(mv_col_axi_wstrb),                            // output wire [63 : 0] mv_col_axi_wstrb
  .mv_col_axi_wdata(mv_col_axi_wdata),                            // output wire [511 : 0] mv_col_axi_wdata
  .mv_pref_axi_araddr(mv_pref_axi_araddr),                        // output wire [31 : 0] mv_pref_axi_araddr
  .mv_pref_axi_arlen(mv_pref_axi_arlen),                          // output wire [7 : 0] mv_pref_axi_arlen
  .mv_pref_axi_arsize(mv_pref_axi_arsize),                        // output wire [2 : 0] mv_pref_axi_arsize
  .mv_pref_axi_arburst(mv_pref_axi_arburst),                      // output wire [1 : 0] mv_pref_axi_arburst
  .mv_pref_axi_arprot(mv_pref_axi_arprot),                        // output wire [2 : 0] mv_pref_axi_arprot
  .mv_pref_axi_arcache(mv_pref_axi_arcache),                      // output wire [3 : 0] mv_pref_axi_arcache
  .ref_pix_axi_ar_addr(ref_pix_axi_ar_addr),                      // output wire [31 : 0] ref_pix_axi_ar_addr
  .ref_pix_axi_ar_len(ref_pix_axi_ar_len),                        // output wire [7 : 0] ref_pix_axi_ar_len
  .ref_pix_axi_ar_size(ref_pix_axi_ar_size),                      // output wire [2 : 0] ref_pix_axi_ar_size
  .ref_pix_axi_ar_burst(ref_pix_axi_ar_burst),                    // output wire [1 : 0] ref_pix_axi_ar_burst
  .ref_pix_axi_ar_prot(ref_pix_axi_ar_prot),                      // output wire [2 : 0] ref_pix_axi_ar_prot
  .ref_pic_write_axi_awlen(ref_pic_write_axi_awlen),              // output wire [7 : 0] ref_pic_write_axi_awlen
  .ref_pic_write_axi_awsize(ref_pic_write_axi_awsize),            // output wire [2 : 0] ref_pic_write_axi_awsize
  .ref_pic_write_axi_awburst(ref_pic_write_axi_awburst),          // output wire [1 : 0] ref_pic_write_axi_awburst
  .ref_pic_write_axi_awcache(ref_pic_write_axi_awcache),          // output wire [3 : 0] ref_pic_write_axi_awcache
  .ref_pic_write_axi_awprot(ref_pic_write_axi_awprot),            // output wire [2 : 0] ref_pic_write_axi_awprot
  .ref_pic_write_axi_awaddr(ref_pic_write_axi_awaddr),            // output wire [31 : 0] ref_pic_write_axi_awaddr
  .ref_pic_write_axi_wstrb(ref_pic_write_axi_wstrb),              // output wire [63 : 0] ref_pic_write_axi_wstrb
  .ref_pic_write_axi_wdata(ref_pic_write_axi_wdata),              // output wire [511 : 0] ref_pic_write_axi_wdata
  .pred_state_low8b_out(pred_state_low8b_out),                    // output wire [7 : 0] pred_state_low8b_out
  .inter_pred_stat_8b_out(inter_pred_stat_8b_out),                // output wire [7 : 0] inter_pred_stat_8b_out
  .mv_state_8bit_out(mv_state_8bit_out),                          // output wire [7 : 0] mv_state_8bit_out
  .col_state_axi_write(col_state_axi_write),                      // output wire [7 : 0] col_state_axi_write
  .pred_2_dbf_wr_inf(pred_2_dbf_wr_inf),                          // output wire [7 : 0] pred_2_dbf_wr_inf
  .pred_2_dbf_yy(pred_2_dbf_yy),                                  // output wire [133 : 0] pred_2_dbf_yy
  .pred_2_dbf_cb(pred_2_dbf_cb),                                  // output wire [127 : 0] pred_2_dbf_cb
  .pred_2_dbf_cr(pred_2_dbf_cr),                                  // output wire [127 : 0] pred_2_dbf_cr
  .Xc_Yc_out(Xc_Yc_out),                                          // output wire [23 : 0] Xc_Yc_out
  .intra_ready_out(intra_ready_out),                              // output wire [5 : 0] intra_ready_out
  .cnf_fifo_out(cnf_fifo_out),                                    // output wire [31 : 0] cnf_fifo_out
  .cnf_fifo_counter(cnf_fifo_counter),                            // output wire [31 : 0] cnf_fifo_counter
  .y_residual_counter(y_residual_counter),                        // output wire [31 : 0] y_residual_counter
  .sao_out_y_4x8(sao_out_y_4x8),                                  // output wire [255 : 0] sao_out_y_4x8
  .controller_in(controller_in),                                  // output wire [127 : 0] controller_in
  .controller_out(controller_out),                                // output wire [127 : 0] controller_out
  .sao_wr_data(sao_wr_data),                                      // output wire [255 : 0] sao_wr_data
  .sao_rd_data(sao_rd_data),                                      // output wire [255 : 0] sao_rd_data
  .dpb_wr_fifo_data(dpb_wr_fifo_data),                            // output wire [783 : 0] dpb_wr_fifo_data
  .intra_x_out(intra_x_out),                                      // output wire [11 : 0] intra_x_out
  .intra_y_out(intra_y_out),                                      // output wire [11 : 0] intra_y_out
  .intra_luma_4x4_out(intra_luma_4x4_out),                        // output wire [127 : 0] intra_luma_4x4_out
  .inter_x_out(inter_x_out),                                      // output wire [11 : 0] inter_x_out
  .inter_y_out(inter_y_out),                                      // output wire [11 : 0] inter_y_out
  .inter_luma_4x4_out(inter_luma_4x4_out),                        // output wire [127 : 0] inter_luma_4x4_out
  .comon_pre_lp_x_out(comon_pre_lp_x_out),                        // output wire [11 : 0] comon_pre_lp_x_out
  .comon_pre_lp_y_out(comon_pre_lp_y_out),                        // output wire [11 : 0] comon_pre_lp_y_out
  .comon_pre_lp_luma_4x4_out(comon_pre_lp_luma_4x4_out),          // output wire [127 : 0] comon_pre_lp_luma_4x4_out
  .dbf_to_sao_out_x(dbf_to_sao_out_x),                            // output wire [11 : 0] dbf_to_sao_out_x
  .dbf_to_sao_out_y(dbf_to_sao_out_y),                            // output wire [11 : 0] dbf_to_sao_out_y
  .dbf_to_sao_out_q1(dbf_to_sao_out_q1),                          // output wire [127 : 0] dbf_to_sao_out_q1
  .test_xT_in_min_luma_filt(test_xT_in_min_luma_filt),            // output wire [8 : 0] test_xT_in_min_luma_filt
  .test_xT_in_min_luma_cache(test_xT_in_min_luma_cache),          // output wire [8 : 0] test_xT_in_min_luma_cache
  .test_yT_in_min_luma_filt(test_yT_in_min_luma_filt),            // output wire [8 : 0] test_yT_in_min_luma_filt
  .test_yT_in_min_luma_cache(test_yT_in_min_luma_cache),          // output wire [8 : 0] test_yT_in_min_luma_cache
  .test_luma_filter_out(test_luma_filter_out),                    // output wire [255 : 0] test_luma_filter_out
  .test_cache_addr(test_cache_addr),                              // output wire [6 : 0] test_cache_addr
  .test_cache_luma_data(test_cache_luma_data),                    // output wire [511 : 0] test_cache_luma_data
  .test_ref_luma_data_4x4(test_ref_luma_data_4x4),                // output wire [127 : 0] test_ref_luma_data_4x4
  .residual_read_inf(residual_read_inf),                          // output wire [7 : 0] residual_read_inf
  .res_pres_y_cb_cr_out(res_pres_y_cb_cr_out),                    // output wire [2 : 0] res_pres_y_cb_cr_out
  .current_poc_out(current_poc_out)                              // output wire [31 : 0] current_poc_out
);
// INST_TAG_END ------ End INSTANTIATION Template ---------

// You must compile the wrapper file zynq_design_1_hevc_top_wo_fifo_0_0.v when simulating
// the core, zynq_design_1_hevc_top_wo_fifo_0_0. When compiling the wrapper file, be sure to
// reference the Verilog simulation library.

