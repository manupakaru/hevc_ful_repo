//Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2014.2 (win64) Build 932637 Wed Jun 11 13:33:10 MDT 2014
//Date        : Wed Aug 13 14:54:35 2014
//Host        : IC_1-PC running 64-bit major release  (build 7600)
//Command     : generate_target zynq_design_1.bd
//Design      : zynq_design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module s00_couplers_imp_5VZGPS
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arid,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awid,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rid,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wid,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [15:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [11:0]M_AXI_arid;
  output [7:0]M_AXI_arlen;
  output M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  output [15:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [11:0]M_AXI_awid;
  output [7:0]M_AXI_awlen;
  output M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  input [11:0]M_AXI_bid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  input [11:0]M_AXI_rid;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [11:0]S_AXI_arid;
  input [3:0]S_AXI_arlen;
  input [1:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [11:0]S_AXI_awid;
  input [3:0]S_AXI_awlen;
  input [1:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [11:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [11:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input [11:0]S_AXI_wid;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_pc_to_s00_couplers_ARADDR;
  wire [1:0]auto_pc_to_s00_couplers_ARBURST;
  wire [3:0]auto_pc_to_s00_couplers_ARCACHE;
  wire [11:0]auto_pc_to_s00_couplers_ARID;
  wire [7:0]auto_pc_to_s00_couplers_ARLEN;
  wire [0:0]auto_pc_to_s00_couplers_ARLOCK;
  wire [2:0]auto_pc_to_s00_couplers_ARPROT;
  wire auto_pc_to_s00_couplers_ARREADY;
  wire [2:0]auto_pc_to_s00_couplers_ARSIZE;
  wire auto_pc_to_s00_couplers_ARVALID;
  wire [31:0]auto_pc_to_s00_couplers_AWADDR;
  wire [1:0]auto_pc_to_s00_couplers_AWBURST;
  wire [3:0]auto_pc_to_s00_couplers_AWCACHE;
  wire [11:0]auto_pc_to_s00_couplers_AWID;
  wire [7:0]auto_pc_to_s00_couplers_AWLEN;
  wire [0:0]auto_pc_to_s00_couplers_AWLOCK;
  wire [2:0]auto_pc_to_s00_couplers_AWPROT;
  wire auto_pc_to_s00_couplers_AWREADY;
  wire [2:0]auto_pc_to_s00_couplers_AWSIZE;
  wire auto_pc_to_s00_couplers_AWVALID;
  wire [11:0]auto_pc_to_s00_couplers_BID;
  wire auto_pc_to_s00_couplers_BREADY;
  wire [1:0]auto_pc_to_s00_couplers_BRESP;
  wire auto_pc_to_s00_couplers_BVALID;
  wire [31:0]auto_pc_to_s00_couplers_RDATA;
  wire [11:0]auto_pc_to_s00_couplers_RID;
  wire auto_pc_to_s00_couplers_RLAST;
  wire auto_pc_to_s00_couplers_RREADY;
  wire [1:0]auto_pc_to_s00_couplers_RRESP;
  wire auto_pc_to_s00_couplers_RVALID;
  wire [31:0]auto_pc_to_s00_couplers_WDATA;
  wire auto_pc_to_s00_couplers_WLAST;
  wire auto_pc_to_s00_couplers_WREADY;
  wire [3:0]auto_pc_to_s00_couplers_WSTRB;
  wire auto_pc_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_auto_pc_ARADDR;
  wire [1:0]s00_couplers_to_auto_pc_ARBURST;
  wire [3:0]s00_couplers_to_auto_pc_ARCACHE;
  wire [11:0]s00_couplers_to_auto_pc_ARID;
  wire [3:0]s00_couplers_to_auto_pc_ARLEN;
  wire [1:0]s00_couplers_to_auto_pc_ARLOCK;
  wire [2:0]s00_couplers_to_auto_pc_ARPROT;
  wire [3:0]s00_couplers_to_auto_pc_ARQOS;
  wire s00_couplers_to_auto_pc_ARREADY;
  wire [2:0]s00_couplers_to_auto_pc_ARSIZE;
  wire s00_couplers_to_auto_pc_ARVALID;
  wire [31:0]s00_couplers_to_auto_pc_AWADDR;
  wire [1:0]s00_couplers_to_auto_pc_AWBURST;
  wire [3:0]s00_couplers_to_auto_pc_AWCACHE;
  wire [11:0]s00_couplers_to_auto_pc_AWID;
  wire [3:0]s00_couplers_to_auto_pc_AWLEN;
  wire [1:0]s00_couplers_to_auto_pc_AWLOCK;
  wire [2:0]s00_couplers_to_auto_pc_AWPROT;
  wire [3:0]s00_couplers_to_auto_pc_AWQOS;
  wire s00_couplers_to_auto_pc_AWREADY;
  wire [2:0]s00_couplers_to_auto_pc_AWSIZE;
  wire s00_couplers_to_auto_pc_AWVALID;
  wire [11:0]s00_couplers_to_auto_pc_BID;
  wire s00_couplers_to_auto_pc_BREADY;
  wire [1:0]s00_couplers_to_auto_pc_BRESP;
  wire s00_couplers_to_auto_pc_BVALID;
  wire [31:0]s00_couplers_to_auto_pc_RDATA;
  wire [11:0]s00_couplers_to_auto_pc_RID;
  wire s00_couplers_to_auto_pc_RLAST;
  wire s00_couplers_to_auto_pc_RREADY;
  wire [1:0]s00_couplers_to_auto_pc_RRESP;
  wire s00_couplers_to_auto_pc_RVALID;
  wire [31:0]s00_couplers_to_auto_pc_WDATA;
  wire [11:0]s00_couplers_to_auto_pc_WID;
  wire s00_couplers_to_auto_pc_WLAST;
  wire s00_couplers_to_auto_pc_WREADY;
  wire [3:0]s00_couplers_to_auto_pc_WSTRB;
  wire s00_couplers_to_auto_pc_WVALID;

  assign M_AXI_araddr[15:0] = auto_pc_to_s00_couplers_ARADDR[15:0];
  assign M_AXI_arburst[1:0] = auto_pc_to_s00_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_pc_to_s00_couplers_ARCACHE;
  assign M_AXI_arid[11:0] = auto_pc_to_s00_couplers_ARID;
  assign M_AXI_arlen[7:0] = auto_pc_to_s00_couplers_ARLEN;
  assign M_AXI_arlock = auto_pc_to_s00_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_pc_to_s00_couplers_ARPROT;
  assign M_AXI_arsize[2:0] = auto_pc_to_s00_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_pc_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[15:0] = auto_pc_to_s00_couplers_AWADDR[15:0];
  assign M_AXI_awburst[1:0] = auto_pc_to_s00_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_pc_to_s00_couplers_AWCACHE;
  assign M_AXI_awid[11:0] = auto_pc_to_s00_couplers_AWID;
  assign M_AXI_awlen[7:0] = auto_pc_to_s00_couplers_AWLEN;
  assign M_AXI_awlock = auto_pc_to_s00_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_pc_to_s00_couplers_AWPROT;
  assign M_AXI_awsize[2:0] = auto_pc_to_s00_couplers_AWSIZE;
  assign M_AXI_awvalid = auto_pc_to_s00_couplers_AWVALID;
  assign M_AXI_bready = auto_pc_to_s00_couplers_BREADY;
  assign M_AXI_rready = auto_pc_to_s00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_pc_to_s00_couplers_WDATA;
  assign M_AXI_wlast = auto_pc_to_s00_couplers_WLAST;
  assign M_AXI_wstrb[3:0] = auto_pc_to_s00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_pc_to_s00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_arready = s00_couplers_to_auto_pc_ARREADY;
  assign S_AXI_awready = s00_couplers_to_auto_pc_AWREADY;
  assign S_AXI_bid[11:0] = s00_couplers_to_auto_pc_BID;
  assign S_AXI_bresp[1:0] = s00_couplers_to_auto_pc_BRESP;
  assign S_AXI_bvalid = s00_couplers_to_auto_pc_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_auto_pc_RDATA;
  assign S_AXI_rid[11:0] = s00_couplers_to_auto_pc_RID;
  assign S_AXI_rlast = s00_couplers_to_auto_pc_RLAST;
  assign S_AXI_rresp[1:0] = s00_couplers_to_auto_pc_RRESP;
  assign S_AXI_rvalid = s00_couplers_to_auto_pc_RVALID;
  assign S_AXI_wready = s00_couplers_to_auto_pc_WREADY;
  assign auto_pc_to_s00_couplers_ARREADY = M_AXI_arready;
  assign auto_pc_to_s00_couplers_AWREADY = M_AXI_awready;
  assign auto_pc_to_s00_couplers_BID = M_AXI_bid[11:0];
  assign auto_pc_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_pc_to_s00_couplers_BVALID = M_AXI_bvalid;
  assign auto_pc_to_s00_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_pc_to_s00_couplers_RID = M_AXI_rid[11:0];
  assign auto_pc_to_s00_couplers_RLAST = M_AXI_rlast;
  assign auto_pc_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_pc_to_s00_couplers_RVALID = M_AXI_rvalid;
  assign auto_pc_to_s00_couplers_WREADY = M_AXI_wready;
  assign s00_couplers_to_auto_pc_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_auto_pc_ARBURST = S_AXI_arburst[1:0];
  assign s00_couplers_to_auto_pc_ARCACHE = S_AXI_arcache[3:0];
  assign s00_couplers_to_auto_pc_ARID = S_AXI_arid[11:0];
  assign s00_couplers_to_auto_pc_ARLEN = S_AXI_arlen[3:0];
  assign s00_couplers_to_auto_pc_ARLOCK = S_AXI_arlock[1:0];
  assign s00_couplers_to_auto_pc_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_auto_pc_ARQOS = S_AXI_arqos[3:0];
  assign s00_couplers_to_auto_pc_ARSIZE = S_AXI_arsize[2:0];
  assign s00_couplers_to_auto_pc_ARVALID = S_AXI_arvalid;
  assign s00_couplers_to_auto_pc_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_auto_pc_AWBURST = S_AXI_awburst[1:0];
  assign s00_couplers_to_auto_pc_AWCACHE = S_AXI_awcache[3:0];
  assign s00_couplers_to_auto_pc_AWID = S_AXI_awid[11:0];
  assign s00_couplers_to_auto_pc_AWLEN = S_AXI_awlen[3:0];
  assign s00_couplers_to_auto_pc_AWLOCK = S_AXI_awlock[1:0];
  assign s00_couplers_to_auto_pc_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_auto_pc_AWQOS = S_AXI_awqos[3:0];
  assign s00_couplers_to_auto_pc_AWSIZE = S_AXI_awsize[2:0];
  assign s00_couplers_to_auto_pc_AWVALID = S_AXI_awvalid;
  assign s00_couplers_to_auto_pc_BREADY = S_AXI_bready;
  assign s00_couplers_to_auto_pc_RREADY = S_AXI_rready;
  assign s00_couplers_to_auto_pc_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_auto_pc_WID = S_AXI_wid[11:0];
  assign s00_couplers_to_auto_pc_WLAST = S_AXI_wlast;
  assign s00_couplers_to_auto_pc_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_auto_pc_WVALID = S_AXI_wvalid;
zynq_design_1_auto_pc_0 auto_pc
       (.aclk(S_ACLK_1),
        .aresetn(S_ARESETN_1),
        .m_axi_araddr(auto_pc_to_s00_couplers_ARADDR),
        .m_axi_arburst(auto_pc_to_s00_couplers_ARBURST),
        .m_axi_arcache(auto_pc_to_s00_couplers_ARCACHE),
        .m_axi_arid(auto_pc_to_s00_couplers_ARID),
        .m_axi_arlen(auto_pc_to_s00_couplers_ARLEN),
        .m_axi_arlock(auto_pc_to_s00_couplers_ARLOCK),
        .m_axi_arprot(auto_pc_to_s00_couplers_ARPROT),
        .m_axi_arready(auto_pc_to_s00_couplers_ARREADY),
        .m_axi_arsize(auto_pc_to_s00_couplers_ARSIZE),
        .m_axi_arvalid(auto_pc_to_s00_couplers_ARVALID),
        .m_axi_awaddr(auto_pc_to_s00_couplers_AWADDR),
        .m_axi_awburst(auto_pc_to_s00_couplers_AWBURST),
        .m_axi_awcache(auto_pc_to_s00_couplers_AWCACHE),
        .m_axi_awid(auto_pc_to_s00_couplers_AWID),
        .m_axi_awlen(auto_pc_to_s00_couplers_AWLEN),
        .m_axi_awlock(auto_pc_to_s00_couplers_AWLOCK),
        .m_axi_awprot(auto_pc_to_s00_couplers_AWPROT),
        .m_axi_awready(auto_pc_to_s00_couplers_AWREADY),
        .m_axi_awsize(auto_pc_to_s00_couplers_AWSIZE),
        .m_axi_awvalid(auto_pc_to_s00_couplers_AWVALID),
        .m_axi_bid(auto_pc_to_s00_couplers_BID),
        .m_axi_bready(auto_pc_to_s00_couplers_BREADY),
        .m_axi_bresp(auto_pc_to_s00_couplers_BRESP),
        .m_axi_bvalid(auto_pc_to_s00_couplers_BVALID),
        .m_axi_rdata(auto_pc_to_s00_couplers_RDATA),
        .m_axi_rid(auto_pc_to_s00_couplers_RID),
        .m_axi_rlast(auto_pc_to_s00_couplers_RLAST),
        .m_axi_rready(auto_pc_to_s00_couplers_RREADY),
        .m_axi_rresp(auto_pc_to_s00_couplers_RRESP),
        .m_axi_rvalid(auto_pc_to_s00_couplers_RVALID),
        .m_axi_wdata(auto_pc_to_s00_couplers_WDATA),
        .m_axi_wlast(auto_pc_to_s00_couplers_WLAST),
        .m_axi_wready(auto_pc_to_s00_couplers_WREADY),
        .m_axi_wstrb(auto_pc_to_s00_couplers_WSTRB),
        .m_axi_wvalid(auto_pc_to_s00_couplers_WVALID),
        .s_axi_araddr(s00_couplers_to_auto_pc_ARADDR),
        .s_axi_arburst(s00_couplers_to_auto_pc_ARBURST),
        .s_axi_arcache(s00_couplers_to_auto_pc_ARCACHE),
        .s_axi_arid(s00_couplers_to_auto_pc_ARID),
        .s_axi_arlen(s00_couplers_to_auto_pc_ARLEN),
        .s_axi_arlock(s00_couplers_to_auto_pc_ARLOCK),
        .s_axi_arprot(s00_couplers_to_auto_pc_ARPROT),
        .s_axi_arqos(s00_couplers_to_auto_pc_ARQOS),
        .s_axi_arready(s00_couplers_to_auto_pc_ARREADY),
        .s_axi_arsize(s00_couplers_to_auto_pc_ARSIZE),
        .s_axi_arvalid(s00_couplers_to_auto_pc_ARVALID),
        .s_axi_awaddr(s00_couplers_to_auto_pc_AWADDR),
        .s_axi_awburst(s00_couplers_to_auto_pc_AWBURST),
        .s_axi_awcache(s00_couplers_to_auto_pc_AWCACHE),
        .s_axi_awid(s00_couplers_to_auto_pc_AWID),
        .s_axi_awlen(s00_couplers_to_auto_pc_AWLEN),
        .s_axi_awlock(s00_couplers_to_auto_pc_AWLOCK),
        .s_axi_awprot(s00_couplers_to_auto_pc_AWPROT),
        .s_axi_awqos(s00_couplers_to_auto_pc_AWQOS),
        .s_axi_awready(s00_couplers_to_auto_pc_AWREADY),
        .s_axi_awsize(s00_couplers_to_auto_pc_AWSIZE),
        .s_axi_awvalid(s00_couplers_to_auto_pc_AWVALID),
        .s_axi_bid(s00_couplers_to_auto_pc_BID),
        .s_axi_bready(s00_couplers_to_auto_pc_BREADY),
        .s_axi_bresp(s00_couplers_to_auto_pc_BRESP),
        .s_axi_bvalid(s00_couplers_to_auto_pc_BVALID),
        .s_axi_rdata(s00_couplers_to_auto_pc_RDATA),
        .s_axi_rid(s00_couplers_to_auto_pc_RID),
        .s_axi_rlast(s00_couplers_to_auto_pc_RLAST),
        .s_axi_rready(s00_couplers_to_auto_pc_RREADY),
        .s_axi_rresp(s00_couplers_to_auto_pc_RRESP),
        .s_axi_rvalid(s00_couplers_to_auto_pc_RVALID),
        .s_axi_wdata(s00_couplers_to_auto_pc_WDATA),
        .s_axi_wid(s00_couplers_to_auto_pc_WID),
        .s_axi_wlast(s00_couplers_to_auto_pc_WLAST),
        .s_axi_wready(s00_couplers_to_auto_pc_WREADY),
        .s_axi_wstrb(s00_couplers_to_auto_pc_WSTRB),
        .s_axi_wvalid(s00_couplers_to_auto_pc_WVALID));
endmodule

module s00_couplers_imp_R0HIN2
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arid,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awid,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rid,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wid,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input [0:0]M_ARESETN;
  output [15:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [11:0]M_AXI_arid;
  output [7:0]M_AXI_arlen;
  output M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  output [15:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [11:0]M_AXI_awid;
  output [7:0]M_AXI_awlen;
  output M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  input [11:0]M_AXI_bid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  input [11:0]M_AXI_rid;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input [0:0]S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [11:0]S_AXI_arid;
  input [3:0]S_AXI_arlen;
  input [1:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [11:0]S_AXI_awid;
  input [3:0]S_AXI_awlen;
  input [1:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [11:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [11:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input [11:0]S_AXI_wid;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire [0:0]S_ARESETN_1;
  wire [31:0]auto_pc_to_s00_couplers_ARADDR;
  wire [1:0]auto_pc_to_s00_couplers_ARBURST;
  wire [3:0]auto_pc_to_s00_couplers_ARCACHE;
  wire [11:0]auto_pc_to_s00_couplers_ARID;
  wire [7:0]auto_pc_to_s00_couplers_ARLEN;
  wire [0:0]auto_pc_to_s00_couplers_ARLOCK;
  wire [2:0]auto_pc_to_s00_couplers_ARPROT;
  wire [3:0]auto_pc_to_s00_couplers_ARQOS;
  wire auto_pc_to_s00_couplers_ARREADY;
  wire [2:0]auto_pc_to_s00_couplers_ARSIZE;
  wire auto_pc_to_s00_couplers_ARVALID;
  wire [31:0]auto_pc_to_s00_couplers_AWADDR;
  wire [1:0]auto_pc_to_s00_couplers_AWBURST;
  wire [3:0]auto_pc_to_s00_couplers_AWCACHE;
  wire [11:0]auto_pc_to_s00_couplers_AWID;
  wire [7:0]auto_pc_to_s00_couplers_AWLEN;
  wire [0:0]auto_pc_to_s00_couplers_AWLOCK;
  wire [2:0]auto_pc_to_s00_couplers_AWPROT;
  wire [3:0]auto_pc_to_s00_couplers_AWQOS;
  wire auto_pc_to_s00_couplers_AWREADY;
  wire [2:0]auto_pc_to_s00_couplers_AWSIZE;
  wire auto_pc_to_s00_couplers_AWVALID;
  wire [11:0]auto_pc_to_s00_couplers_BID;
  wire auto_pc_to_s00_couplers_BREADY;
  wire [1:0]auto_pc_to_s00_couplers_BRESP;
  wire auto_pc_to_s00_couplers_BVALID;
  wire [31:0]auto_pc_to_s00_couplers_RDATA;
  wire [11:0]auto_pc_to_s00_couplers_RID;
  wire auto_pc_to_s00_couplers_RLAST;
  wire auto_pc_to_s00_couplers_RREADY;
  wire [1:0]auto_pc_to_s00_couplers_RRESP;
  wire auto_pc_to_s00_couplers_RVALID;
  wire [31:0]auto_pc_to_s00_couplers_WDATA;
  wire auto_pc_to_s00_couplers_WLAST;
  wire auto_pc_to_s00_couplers_WREADY;
  wire [3:0]auto_pc_to_s00_couplers_WSTRB;
  wire auto_pc_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_auto_pc_ARADDR;
  wire [1:0]s00_couplers_to_auto_pc_ARBURST;
  wire [3:0]s00_couplers_to_auto_pc_ARCACHE;
  wire [11:0]s00_couplers_to_auto_pc_ARID;
  wire [3:0]s00_couplers_to_auto_pc_ARLEN;
  wire [1:0]s00_couplers_to_auto_pc_ARLOCK;
  wire [2:0]s00_couplers_to_auto_pc_ARPROT;
  wire [3:0]s00_couplers_to_auto_pc_ARQOS;
  wire s00_couplers_to_auto_pc_ARREADY;
  wire [2:0]s00_couplers_to_auto_pc_ARSIZE;
  wire s00_couplers_to_auto_pc_ARVALID;
  wire [31:0]s00_couplers_to_auto_pc_AWADDR;
  wire [1:0]s00_couplers_to_auto_pc_AWBURST;
  wire [3:0]s00_couplers_to_auto_pc_AWCACHE;
  wire [11:0]s00_couplers_to_auto_pc_AWID;
  wire [3:0]s00_couplers_to_auto_pc_AWLEN;
  wire [1:0]s00_couplers_to_auto_pc_AWLOCK;
  wire [2:0]s00_couplers_to_auto_pc_AWPROT;
  wire [3:0]s00_couplers_to_auto_pc_AWQOS;
  wire s00_couplers_to_auto_pc_AWREADY;
  wire [2:0]s00_couplers_to_auto_pc_AWSIZE;
  wire s00_couplers_to_auto_pc_AWVALID;
  wire [11:0]s00_couplers_to_auto_pc_BID;
  wire s00_couplers_to_auto_pc_BREADY;
  wire [1:0]s00_couplers_to_auto_pc_BRESP;
  wire s00_couplers_to_auto_pc_BVALID;
  wire [31:0]s00_couplers_to_auto_pc_RDATA;
  wire [11:0]s00_couplers_to_auto_pc_RID;
  wire s00_couplers_to_auto_pc_RLAST;
  wire s00_couplers_to_auto_pc_RREADY;
  wire [1:0]s00_couplers_to_auto_pc_RRESP;
  wire s00_couplers_to_auto_pc_RVALID;
  wire [31:0]s00_couplers_to_auto_pc_WDATA;
  wire [11:0]s00_couplers_to_auto_pc_WID;
  wire s00_couplers_to_auto_pc_WLAST;
  wire s00_couplers_to_auto_pc_WREADY;
  wire [3:0]s00_couplers_to_auto_pc_WSTRB;
  wire s00_couplers_to_auto_pc_WVALID;

  assign M_AXI_araddr[15:0] = auto_pc_to_s00_couplers_ARADDR[15:0];
  assign M_AXI_arburst[1:0] = auto_pc_to_s00_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_pc_to_s00_couplers_ARCACHE;
  assign M_AXI_arid[11:0] = auto_pc_to_s00_couplers_ARID;
  assign M_AXI_arlen[7:0] = auto_pc_to_s00_couplers_ARLEN;
  assign M_AXI_arlock = auto_pc_to_s00_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_pc_to_s00_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = auto_pc_to_s00_couplers_ARQOS;
  assign M_AXI_arsize[2:0] = auto_pc_to_s00_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_pc_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[15:0] = auto_pc_to_s00_couplers_AWADDR[15:0];
  assign M_AXI_awburst[1:0] = auto_pc_to_s00_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_pc_to_s00_couplers_AWCACHE;
  assign M_AXI_awid[11:0] = auto_pc_to_s00_couplers_AWID;
  assign M_AXI_awlen[7:0] = auto_pc_to_s00_couplers_AWLEN;
  assign M_AXI_awlock = auto_pc_to_s00_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_pc_to_s00_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = auto_pc_to_s00_couplers_AWQOS;
  assign M_AXI_awsize[2:0] = auto_pc_to_s00_couplers_AWSIZE;
  assign M_AXI_awvalid = auto_pc_to_s00_couplers_AWVALID;
  assign M_AXI_bready = auto_pc_to_s00_couplers_BREADY;
  assign M_AXI_rready = auto_pc_to_s00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_pc_to_s00_couplers_WDATA;
  assign M_AXI_wlast = auto_pc_to_s00_couplers_WLAST;
  assign M_AXI_wstrb[3:0] = auto_pc_to_s00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_pc_to_s00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN[0];
  assign S_AXI_arready = s00_couplers_to_auto_pc_ARREADY;
  assign S_AXI_awready = s00_couplers_to_auto_pc_AWREADY;
  assign S_AXI_bid[11:0] = s00_couplers_to_auto_pc_BID;
  assign S_AXI_bresp[1:0] = s00_couplers_to_auto_pc_BRESP;
  assign S_AXI_bvalid = s00_couplers_to_auto_pc_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_auto_pc_RDATA;
  assign S_AXI_rid[11:0] = s00_couplers_to_auto_pc_RID;
  assign S_AXI_rlast = s00_couplers_to_auto_pc_RLAST;
  assign S_AXI_rresp[1:0] = s00_couplers_to_auto_pc_RRESP;
  assign S_AXI_rvalid = s00_couplers_to_auto_pc_RVALID;
  assign S_AXI_wready = s00_couplers_to_auto_pc_WREADY;
  assign auto_pc_to_s00_couplers_ARREADY = M_AXI_arready;
  assign auto_pc_to_s00_couplers_AWREADY = M_AXI_awready;
  assign auto_pc_to_s00_couplers_BID = M_AXI_bid[11:0];
  assign auto_pc_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_pc_to_s00_couplers_BVALID = M_AXI_bvalid;
  assign auto_pc_to_s00_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_pc_to_s00_couplers_RID = M_AXI_rid[11:0];
  assign auto_pc_to_s00_couplers_RLAST = M_AXI_rlast;
  assign auto_pc_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_pc_to_s00_couplers_RVALID = M_AXI_rvalid;
  assign auto_pc_to_s00_couplers_WREADY = M_AXI_wready;
  assign s00_couplers_to_auto_pc_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_auto_pc_ARBURST = S_AXI_arburst[1:0];
  assign s00_couplers_to_auto_pc_ARCACHE = S_AXI_arcache[3:0];
  assign s00_couplers_to_auto_pc_ARID = S_AXI_arid[11:0];
  assign s00_couplers_to_auto_pc_ARLEN = S_AXI_arlen[3:0];
  assign s00_couplers_to_auto_pc_ARLOCK = S_AXI_arlock[1:0];
  assign s00_couplers_to_auto_pc_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_auto_pc_ARQOS = S_AXI_arqos[3:0];
  assign s00_couplers_to_auto_pc_ARSIZE = S_AXI_arsize[2:0];
  assign s00_couplers_to_auto_pc_ARVALID = S_AXI_arvalid;
  assign s00_couplers_to_auto_pc_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_auto_pc_AWBURST = S_AXI_awburst[1:0];
  assign s00_couplers_to_auto_pc_AWCACHE = S_AXI_awcache[3:0];
  assign s00_couplers_to_auto_pc_AWID = S_AXI_awid[11:0];
  assign s00_couplers_to_auto_pc_AWLEN = S_AXI_awlen[3:0];
  assign s00_couplers_to_auto_pc_AWLOCK = S_AXI_awlock[1:0];
  assign s00_couplers_to_auto_pc_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_auto_pc_AWQOS = S_AXI_awqos[3:0];
  assign s00_couplers_to_auto_pc_AWSIZE = S_AXI_awsize[2:0];
  assign s00_couplers_to_auto_pc_AWVALID = S_AXI_awvalid;
  assign s00_couplers_to_auto_pc_BREADY = S_AXI_bready;
  assign s00_couplers_to_auto_pc_RREADY = S_AXI_rready;
  assign s00_couplers_to_auto_pc_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_auto_pc_WID = S_AXI_wid[11:0];
  assign s00_couplers_to_auto_pc_WLAST = S_AXI_wlast;
  assign s00_couplers_to_auto_pc_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_auto_pc_WVALID = S_AXI_wvalid;
zynq_design_1_auto_pc_1 auto_pc
       (.aclk(S_ACLK_1),
        .aresetn(S_ARESETN_1),
        .m_axi_araddr(auto_pc_to_s00_couplers_ARADDR),
        .m_axi_arburst(auto_pc_to_s00_couplers_ARBURST),
        .m_axi_arcache(auto_pc_to_s00_couplers_ARCACHE),
        .m_axi_arid(auto_pc_to_s00_couplers_ARID),
        .m_axi_arlen(auto_pc_to_s00_couplers_ARLEN),
        .m_axi_arlock(auto_pc_to_s00_couplers_ARLOCK),
        .m_axi_arprot(auto_pc_to_s00_couplers_ARPROT),
        .m_axi_arqos(auto_pc_to_s00_couplers_ARQOS),
        .m_axi_arready(auto_pc_to_s00_couplers_ARREADY),
        .m_axi_arsize(auto_pc_to_s00_couplers_ARSIZE),
        .m_axi_arvalid(auto_pc_to_s00_couplers_ARVALID),
        .m_axi_awaddr(auto_pc_to_s00_couplers_AWADDR),
        .m_axi_awburst(auto_pc_to_s00_couplers_AWBURST),
        .m_axi_awcache(auto_pc_to_s00_couplers_AWCACHE),
        .m_axi_awid(auto_pc_to_s00_couplers_AWID),
        .m_axi_awlen(auto_pc_to_s00_couplers_AWLEN),
        .m_axi_awlock(auto_pc_to_s00_couplers_AWLOCK),
        .m_axi_awprot(auto_pc_to_s00_couplers_AWPROT),
        .m_axi_awqos(auto_pc_to_s00_couplers_AWQOS),
        .m_axi_awready(auto_pc_to_s00_couplers_AWREADY),
        .m_axi_awsize(auto_pc_to_s00_couplers_AWSIZE),
        .m_axi_awvalid(auto_pc_to_s00_couplers_AWVALID),
        .m_axi_bid(auto_pc_to_s00_couplers_BID),
        .m_axi_bready(auto_pc_to_s00_couplers_BREADY),
        .m_axi_bresp(auto_pc_to_s00_couplers_BRESP),
        .m_axi_bvalid(auto_pc_to_s00_couplers_BVALID),
        .m_axi_rdata(auto_pc_to_s00_couplers_RDATA),
        .m_axi_rid(auto_pc_to_s00_couplers_RID),
        .m_axi_rlast(auto_pc_to_s00_couplers_RLAST),
        .m_axi_rready(auto_pc_to_s00_couplers_RREADY),
        .m_axi_rresp(auto_pc_to_s00_couplers_RRESP),
        .m_axi_rvalid(auto_pc_to_s00_couplers_RVALID),
        .m_axi_wdata(auto_pc_to_s00_couplers_WDATA),
        .m_axi_wlast(auto_pc_to_s00_couplers_WLAST),
        .m_axi_wready(auto_pc_to_s00_couplers_WREADY),
        .m_axi_wstrb(auto_pc_to_s00_couplers_WSTRB),
        .m_axi_wvalid(auto_pc_to_s00_couplers_WVALID),
        .s_axi_araddr(s00_couplers_to_auto_pc_ARADDR),
        .s_axi_arburst(s00_couplers_to_auto_pc_ARBURST),
        .s_axi_arcache(s00_couplers_to_auto_pc_ARCACHE),
        .s_axi_arid(s00_couplers_to_auto_pc_ARID),
        .s_axi_arlen(s00_couplers_to_auto_pc_ARLEN),
        .s_axi_arlock(s00_couplers_to_auto_pc_ARLOCK),
        .s_axi_arprot(s00_couplers_to_auto_pc_ARPROT),
        .s_axi_arqos(s00_couplers_to_auto_pc_ARQOS),
        .s_axi_arready(s00_couplers_to_auto_pc_ARREADY),
        .s_axi_arsize(s00_couplers_to_auto_pc_ARSIZE),
        .s_axi_arvalid(s00_couplers_to_auto_pc_ARVALID),
        .s_axi_awaddr(s00_couplers_to_auto_pc_AWADDR),
        .s_axi_awburst(s00_couplers_to_auto_pc_AWBURST),
        .s_axi_awcache(s00_couplers_to_auto_pc_AWCACHE),
        .s_axi_awid(s00_couplers_to_auto_pc_AWID),
        .s_axi_awlen(s00_couplers_to_auto_pc_AWLEN),
        .s_axi_awlock(s00_couplers_to_auto_pc_AWLOCK),
        .s_axi_awprot(s00_couplers_to_auto_pc_AWPROT),
        .s_axi_awqos(s00_couplers_to_auto_pc_AWQOS),
        .s_axi_awready(s00_couplers_to_auto_pc_AWREADY),
        .s_axi_awsize(s00_couplers_to_auto_pc_AWSIZE),
        .s_axi_awvalid(s00_couplers_to_auto_pc_AWVALID),
        .s_axi_bid(s00_couplers_to_auto_pc_BID),
        .s_axi_bready(s00_couplers_to_auto_pc_BREADY),
        .s_axi_bresp(s00_couplers_to_auto_pc_BRESP),
        .s_axi_bvalid(s00_couplers_to_auto_pc_BVALID),
        .s_axi_rdata(s00_couplers_to_auto_pc_RDATA),
        .s_axi_rid(s00_couplers_to_auto_pc_RID),
        .s_axi_rlast(s00_couplers_to_auto_pc_RLAST),
        .s_axi_rready(s00_couplers_to_auto_pc_RREADY),
        .s_axi_rresp(s00_couplers_to_auto_pc_RRESP),
        .s_axi_rvalid(s00_couplers_to_auto_pc_RVALID),
        .s_axi_wdata(s00_couplers_to_auto_pc_WDATA),
        .s_axi_wid(s00_couplers_to_auto_pc_WID),
        .s_axi_wlast(s00_couplers_to_auto_pc_WLAST),
        .s_axi_wready(s00_couplers_to_auto_pc_WREADY),
        .s_axi_wstrb(s00_couplers_to_auto_pc_WSTRB),
        .s_axi_wvalid(s00_couplers_to_auto_pc_WVALID));
endmodule

(* CORE_GENERATION_INFO = "zynq_design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLanguage=VERILOG,numBlks=17,numReposBlks=13,numNonXlnxBlks=0,numHierBlks=4,maxHierDepth=0,da_axi4_cnt=2,da_bram_cntlr_cnt=4,da_ps7_cnt=1}" *) 
module zynq_design_1
   (DDR3_addr,
    DDR3_ba,
    DDR3_cas_n,
    DDR3_ck_n,
    DDR3_ck_p,
    DDR3_cke,
    DDR3_cs_n,
    DDR3_dm,
    DDR3_dq,
    DDR3_dqs_n,
    DDR3_dqs_p,
    DDR3_odt,
    DDR3_ras_n,
    DDR3_reset_n,
    DDR3_we_n,
    DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    SYS_CLK_clk_n,
    SYS_CLK_clk_p);
  output [13:0]DDR3_addr;
  output [2:0]DDR3_ba;
  output DDR3_cas_n;
  output [0:0]DDR3_ck_n;
  output [0:0]DDR3_ck_p;
  output [0:0]DDR3_cke;
  output [0:0]DDR3_cs_n;
  output [7:0]DDR3_dm;
  inout [63:0]DDR3_dq;
  inout [7:0]DDR3_dqs_n;
  inout [7:0]DDR3_dqs_p;
  output [0:0]DDR3_odt;
  output DDR3_ras_n;
  output DDR3_reset_n;
  output DDR3_we_n;
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  input SYS_CLK_clk_n;
  input SYS_CLK_clk_p;

  wire GND_1;
  wire SYS_CLK_1_CLK_N;
  wire SYS_CLK_1_CLK_P;
  wire VCC_1;
  wire [15:0]axi_bram_ctrl_0_BRAM_PORTA_ADDR;
  wire axi_bram_ctrl_0_BRAM_PORTA_CLK;
  wire [31:0]axi_bram_ctrl_0_BRAM_PORTA_DIN;
  wire [31:0]axi_bram_ctrl_0_BRAM_PORTA_DOUT;
  wire axi_bram_ctrl_0_BRAM_PORTA_EN;
  wire axi_bram_ctrl_0_BRAM_PORTA_RST;
  wire [3:0]axi_bram_ctrl_0_BRAM_PORTA_WE;
  wire [15:0]axi_bram_ctrl_0_BRAM_PORTB_ADDR;
  wire axi_bram_ctrl_0_BRAM_PORTB_CLK;
  wire [31:0]axi_bram_ctrl_0_BRAM_PORTB_DIN;
  wire [31:0]axi_bram_ctrl_0_BRAM_PORTB_DOUT;
  wire axi_bram_ctrl_0_BRAM_PORTB_EN;
  wire axi_bram_ctrl_0_BRAM_PORTB_RST;
  wire [3:0]axi_bram_ctrl_0_BRAM_PORTB_WE;
(* MARK_DEBUG *)   wire [4:0]axi_fifo_state_out;
(* MARK_DEBUG *)   wire [31:0]axi_inter_con_0_M00_AXI_ARADDR;
(* MARK_DEBUG *)   wire [1:0]axi_inter_con_0_M00_AXI_ARBURST;
(* MARK_DEBUG *)   wire [3:0]axi_inter_con_0_M00_AXI_ARCACHE;
(* MARK_DEBUG *)   wire [3:0]axi_inter_con_0_M00_AXI_ARID;
(* MARK_DEBUG *)   wire [7:0]axi_inter_con_0_M00_AXI_ARLEN;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_ARLOCK;
(* MARK_DEBUG *)   wire [2:0]axi_inter_con_0_M00_AXI_ARPROT;
(* MARK_DEBUG *)   wire [3:0]axi_inter_con_0_M00_AXI_ARQOS;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_ARREADY;
(* MARK_DEBUG *)   wire [2:0]axi_inter_con_0_M00_AXI_ARSIZE;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_ARVALID;
(* MARK_DEBUG *)   wire [31:0]axi_inter_con_0_M00_AXI_AWADDR;
(* MARK_DEBUG *)   wire [1:0]axi_inter_con_0_M00_AXI_AWBURST;
(* MARK_DEBUG *)   wire [3:0]axi_inter_con_0_M00_AXI_AWCACHE;
(* MARK_DEBUG *)   wire [3:0]axi_inter_con_0_M00_AXI_AWID;
(* MARK_DEBUG *)   wire [7:0]axi_inter_con_0_M00_AXI_AWLEN;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_AWLOCK;
(* MARK_DEBUG *)   wire [2:0]axi_inter_con_0_M00_AXI_AWPROT;
(* MARK_DEBUG *)   wire [3:0]axi_inter_con_0_M00_AXI_AWQOS;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_AWREADY;
(* MARK_DEBUG *)   wire [2:0]axi_inter_con_0_M00_AXI_AWSIZE;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_AWVALID;
(* MARK_DEBUG *)   wire [3:0]axi_inter_con_0_M00_AXI_BID;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_BREADY;
(* MARK_DEBUG *)   wire [1:0]axi_inter_con_0_M00_AXI_BRESP;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_BVALID;
(* MARK_DEBUG *)   wire [511:0]axi_inter_con_0_M00_AXI_RDATA;
(* MARK_DEBUG *)   wire [3:0]axi_inter_con_0_M00_AXI_RID;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_RLAST;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_RREADY;
(* MARK_DEBUG *)   wire [1:0]axi_inter_con_0_M00_AXI_RRESP;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_RVALID;
(* MARK_DEBUG *)   wire [511:0]axi_inter_con_0_M00_AXI_WDATA;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_WLAST;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_WREADY;
(* MARK_DEBUG *)   wire [63:0]axi_inter_con_0_M00_AXI_WSTRB;
(* MARK_DEBUG *)   wire axi_inter_con_0_M00_AXI_WVALID;
  wire [15:0]axi_mem_intercon_1_M00_AXI_ARADDR;
  wire [1:0]axi_mem_intercon_1_M00_AXI_ARBURST;
  wire [3:0]axi_mem_intercon_1_M00_AXI_ARCACHE;
  wire [11:0]axi_mem_intercon_1_M00_AXI_ARID;
  wire [7:0]axi_mem_intercon_1_M00_AXI_ARLEN;
  wire axi_mem_intercon_1_M00_AXI_ARLOCK;
  wire [2:0]axi_mem_intercon_1_M00_AXI_ARPROT;
  wire [3:0]axi_mem_intercon_1_M00_AXI_ARQOS;
  wire axi_mem_intercon_1_M00_AXI_ARREADY;
  wire [2:0]axi_mem_intercon_1_M00_AXI_ARSIZE;
  wire axi_mem_intercon_1_M00_AXI_ARVALID;
  wire [15:0]axi_mem_intercon_1_M00_AXI_AWADDR;
  wire [1:0]axi_mem_intercon_1_M00_AXI_AWBURST;
  wire [3:0]axi_mem_intercon_1_M00_AXI_AWCACHE;
  wire [11:0]axi_mem_intercon_1_M00_AXI_AWID;
  wire [7:0]axi_mem_intercon_1_M00_AXI_AWLEN;
  wire axi_mem_intercon_1_M00_AXI_AWLOCK;
  wire [2:0]axi_mem_intercon_1_M00_AXI_AWPROT;
  wire [3:0]axi_mem_intercon_1_M00_AXI_AWQOS;
  wire axi_mem_intercon_1_M00_AXI_AWREADY;
  wire [2:0]axi_mem_intercon_1_M00_AXI_AWSIZE;
  wire axi_mem_intercon_1_M00_AXI_AWVALID;
  wire [11:0]axi_mem_intercon_1_M00_AXI_BID;
  wire axi_mem_intercon_1_M00_AXI_BREADY;
  wire [1:0]axi_mem_intercon_1_M00_AXI_BRESP;
  wire axi_mem_intercon_1_M00_AXI_BVALID;
  wire [31:0]axi_mem_intercon_1_M00_AXI_RDATA;
  wire [11:0]axi_mem_intercon_1_M00_AXI_RID;
  wire axi_mem_intercon_1_M00_AXI_RLAST;
  wire axi_mem_intercon_1_M00_AXI_RREADY;
  wire [1:0]axi_mem_intercon_1_M00_AXI_RRESP;
  wire axi_mem_intercon_1_M00_AXI_RVALID;
  wire [31:0]axi_mem_intercon_1_M00_AXI_WDATA;
  wire axi_mem_intercon_1_M00_AXI_WLAST;
  wire axi_mem_intercon_1_M00_AXI_WREADY;
  wire [3:0]axi_mem_intercon_1_M00_AXI_WSTRB;
  wire axi_mem_intercon_1_M00_AXI_WVALID;
  wire [15:0]axi_mem_intercon_M00_AXI_ARADDR;
  wire [1:0]axi_mem_intercon_M00_AXI_ARBURST;
  wire [3:0]axi_mem_intercon_M00_AXI_ARCACHE;
  wire [11:0]axi_mem_intercon_M00_AXI_ARID;
  wire [7:0]axi_mem_intercon_M00_AXI_ARLEN;
  wire axi_mem_intercon_M00_AXI_ARLOCK;
  wire [2:0]axi_mem_intercon_M00_AXI_ARPROT;
  wire axi_mem_intercon_M00_AXI_ARREADY;
  wire [2:0]axi_mem_intercon_M00_AXI_ARSIZE;
  wire axi_mem_intercon_M00_AXI_ARVALID;
  wire [15:0]axi_mem_intercon_M00_AXI_AWADDR;
  wire [1:0]axi_mem_intercon_M00_AXI_AWBURST;
  wire [3:0]axi_mem_intercon_M00_AXI_AWCACHE;
  wire [11:0]axi_mem_intercon_M00_AXI_AWID;
  wire [7:0]axi_mem_intercon_M00_AXI_AWLEN;
  wire axi_mem_intercon_M00_AXI_AWLOCK;
  wire [2:0]axi_mem_intercon_M00_AXI_AWPROT;
  wire axi_mem_intercon_M00_AXI_AWREADY;
  wire [2:0]axi_mem_intercon_M00_AXI_AWSIZE;
  wire axi_mem_intercon_M00_AXI_AWVALID;
  wire [11:0]axi_mem_intercon_M00_AXI_BID;
  wire axi_mem_intercon_M00_AXI_BREADY;
  wire [1:0]axi_mem_intercon_M00_AXI_BRESP;
  wire axi_mem_intercon_M00_AXI_BVALID;
  wire [31:0]axi_mem_intercon_M00_AXI_RDATA;
  wire [11:0]axi_mem_intercon_M00_AXI_RID;
  wire axi_mem_intercon_M00_AXI_RLAST;
  wire axi_mem_intercon_M00_AXI_RREADY;
  wire [1:0]axi_mem_intercon_M00_AXI_RRESP;
  wire axi_mem_intercon_M00_AXI_RVALID;
  wire [31:0]axi_mem_intercon_M00_AXI_WDATA;
  wire axi_mem_intercon_M00_AXI_WLAST;
  wire axi_mem_intercon_M00_AXI_WREADY;
  wire [3:0]axi_mem_intercon_M00_AXI_WSTRB;
  wire axi_mem_intercon_M00_AXI_WVALID;
  wire axi_wr_slave_to_fifo_0_axi_write_clk;
  wire axi_wr_slave_to_fifo_0_cb_residual_fifo_write_FULL;
  wire [143:0]axi_wr_slave_to_fifo_0_cb_residual_fifo_write_WR_DATA;
  wire axi_wr_slave_to_fifo_0_cb_residual_fifo_write_WR_EN;
(* MARK_DEBUG *)   wire axi_wr_slave_to_fifo_0_config_fifo_write_FULL;
(* MARK_DEBUG *)   wire [31:0]axi_wr_slave_to_fifo_0_config_fifo_write_WR_DATA;
(* MARK_DEBUG *)   wire axi_wr_slave_to_fifo_0_config_fifo_write_WR_EN;
  wire axi_wr_slave_to_fifo_0_cr_residual_fifo_write_FULL;
  wire [143:0]axi_wr_slave_to_fifo_0_cr_residual_fifo_write_WR_DATA;
  wire axi_wr_slave_to_fifo_0_cr_residual_fifo_write_WR_EN;
  wire axi_wr_slave_to_fifo_0_y_residual_fifo_write_FULL;
  wire [143:0]axi_wr_slave_to_fifo_0_y_residual_fifo_write_WR_DATA;
  wire axi_wr_slave_to_fifo_0_y_residual_fifo_write_WR_EN;
(* MARK_DEBUG *)   wire [31:0]cnf_fifo_counter;
(* MARK_DEBUG *)   wire [31:0]cnf_fifo_out;
(* MARK_DEBUG *)   wire [127:0]comon_pre_lp_luma_4x4_out;
(* MARK_DEBUG *)   wire [11:0]comon_pre_lp_x_out;
(* MARK_DEBUG *)   wire [11:0]comon_pre_lp_y_out;
(* MARK_DEBUG *)   wire [31:0]current_poc_out;
(* MARK_DEBUG *)   wire [783:0]display_fifo_data_out;
  wire [783:0]dpb_wr_fifo_data;
(* MARK_DEBUG *)   wire fifo_read_driver_0_rd_en;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_display_fifo_is_empty_out;
(* MARK_DEBUG *)   wire [31:0]hevc_top_with_eth_0_mv_col_axi_master_inf_AWADDR;
(* MARK_DEBUG *)   wire [1:0]hevc_top_with_eth_0_mv_col_axi_master_inf_AWBURST;
(* MARK_DEBUG *)   wire [3:0]hevc_top_with_eth_0_mv_col_axi_master_inf_AWCACHE;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_mv_col_axi_master_inf_AWID;
(* MARK_DEBUG *)   wire [7:0]hevc_top_with_eth_0_mv_col_axi_master_inf_AWLEN;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_mv_col_axi_master_inf_AWLOCK;
(* MARK_DEBUG *)   wire [2:0]hevc_top_with_eth_0_mv_col_axi_master_inf_AWPROT;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_mv_col_axi_master_inf_AWREADY;
(* MARK_DEBUG *)   wire [2:0]hevc_top_with_eth_0_mv_col_axi_master_inf_AWSIZE;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_mv_col_axi_master_inf_AWVALID;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_mv_col_axi_master_inf_BREADY;
(* MARK_DEBUG *)   wire [1:0]hevc_top_with_eth_0_mv_col_axi_master_inf_BRESP;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_mv_col_axi_master_inf_BVALID;
(* MARK_DEBUG *)   wire [511:0]hevc_top_with_eth_0_mv_col_axi_master_inf_WDATA;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_mv_col_axi_master_inf_WLAST;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_mv_col_axi_master_inf_WREADY;
(* MARK_DEBUG *)   wire [63:0]hevc_top_with_eth_0_mv_col_axi_master_inf_WSTRB;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_mv_col_axi_master_inf_WVALID;
(* MARK_DEBUG *)   wire [31:0]hevc_top_with_eth_0_mv_pref_axi_master_inf_ARADDR;
(* MARK_DEBUG *)   wire [1:0]hevc_top_with_eth_0_mv_pref_axi_master_inf_ARBURST;
(* MARK_DEBUG *)   wire [3:0]hevc_top_with_eth_0_mv_pref_axi_master_inf_ARCACHE;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_mv_pref_axi_master_inf_ARID;
(* MARK_DEBUG *)   wire [7:0]hevc_top_with_eth_0_mv_pref_axi_master_inf_ARLEN;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_mv_pref_axi_master_inf_ARLOCK;
(* MARK_DEBUG *)   wire [2:0]hevc_top_with_eth_0_mv_pref_axi_master_inf_ARPROT;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_mv_pref_axi_master_inf_ARREADY;
(* MARK_DEBUG *)   wire [2:0]hevc_top_with_eth_0_mv_pref_axi_master_inf_ARSIZE;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_mv_pref_axi_master_inf_ARVALID;
(* MARK_DEBUG *)   wire [511:0]hevc_top_with_eth_0_mv_pref_axi_master_inf_RDATA;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_mv_pref_axi_master_inf_RLAST;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_mv_pref_axi_master_inf_RREADY;
(* MARK_DEBUG *)   wire [1:0]hevc_top_with_eth_0_mv_pref_axi_master_inf_RRESP;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_mv_pref_axi_master_inf_RVALID;
(* MARK_DEBUG *)   wire [31:0]hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWADDR;
(* MARK_DEBUG *)   wire [1:0]hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWBURST;
(* MARK_DEBUG *)   wire [3:0]hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWCACHE;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWID;
(* MARK_DEBUG *)   wire [7:0]hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWLEN;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWLOCK;
(* MARK_DEBUG *)   wire [2:0]hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWPROT;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWREADY;
(* MARK_DEBUG *)   wire [2:0]hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWSIZE;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWVALID;
(* MARK_DEBUG *)   wire [0:0]hevc_top_with_eth_0_ref_pic_write_axi_master_inf_BID;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_ref_pic_write_axi_master_inf_BREADY;
(* MARK_DEBUG *)   wire [1:0]hevc_top_with_eth_0_ref_pic_write_axi_master_inf_BRESP;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_ref_pic_write_axi_master_inf_BVALID;
(* MARK_DEBUG *)   wire [511:0]hevc_top_with_eth_0_ref_pic_write_axi_master_inf_WDATA;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_ref_pic_write_axi_master_inf_WLAST;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_ref_pic_write_axi_master_inf_WREADY;
(* MARK_DEBUG *)   wire [63:0]hevc_top_with_eth_0_ref_pic_write_axi_master_inf_WSTRB;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_ref_pic_write_axi_master_inf_WVALID;
(* MARK_DEBUG *)   wire [31:0]hevc_top_with_eth_0_ref_pix_axi_master_inf_ARADDR;
(* MARK_DEBUG *)   wire [1:0]hevc_top_with_eth_0_ref_pix_axi_master_inf_ARBURST;
(* MARK_DEBUG *)   wire [7:0]hevc_top_with_eth_0_ref_pix_axi_master_inf_ARLEN;
(* MARK_DEBUG *)   wire [2:0]hevc_top_with_eth_0_ref_pix_axi_master_inf_ARPROT;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_ref_pix_axi_master_inf_ARREADY;
(* MARK_DEBUG *)   wire [2:0]hevc_top_with_eth_0_ref_pix_axi_master_inf_ARSIZE;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_ref_pix_axi_master_inf_ARVALID;
(* MARK_DEBUG *)   wire [511:0]hevc_top_with_eth_0_ref_pix_axi_master_inf_RDATA;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_ref_pix_axi_master_inf_RLAST;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_ref_pix_axi_master_inf_RREADY;
(* MARK_DEBUG *)   wire [1:0]hevc_top_with_eth_0_ref_pix_axi_master_inf_RRESP;
(* MARK_DEBUG *)   wire hevc_top_with_eth_0_ref_pix_axi_master_inf_RVALID;
(* MARK_DEBUG *)   wire inter_4x4_valid_out;
  wire [127:0]inter_luma_4x4_out;
  wire [11:0]inter_x_out;
  wire [11:0]inter_y_out;
(* MARK_DEBUG *)   wire intra_4x4_valid_out;
(* MARK_DEBUG *)   wire [127:0]intra_luma_4x4_out;
(* MARK_DEBUG *)   wire [5:0]intra_ready_out;
(* MARK_DEBUG *)   wire [11:0]intra_x_out;
(* MARK_DEBUG *)   wire [11:0]intra_y_out;
  wire [13:0]mig_7series_0_DDR3_ADDR;
  wire [2:0]mig_7series_0_DDR3_BA;
  wire mig_7series_0_DDR3_CAS_N;
  wire [0:0]mig_7series_0_DDR3_CKE;
  wire [0:0]mig_7series_0_DDR3_CK_N;
  wire [0:0]mig_7series_0_DDR3_CK_P;
  wire [0:0]mig_7series_0_DDR3_CS_N;
  wire [7:0]mig_7series_0_DDR3_DM;
  wire [63:0]mig_7series_0_DDR3_DQ;
  wire [7:0]mig_7series_0_DDR3_DQS_N;
  wire [7:0]mig_7series_0_DDR3_DQS_P;
  wire [0:0]mig_7series_0_DDR3_ODT;
  wire mig_7series_0_DDR3_RAS_N;
  wire mig_7series_0_DDR3_RESET_N;
  wire mig_7series_0_DDR3_WE_N;
  wire mig_7series_0_init_calib_complete;
  wire mig_7series_0_mmcm_locked;
  wire mig_7series_0_ui_clk;
  wire mig_7series_0_ui_clk_sync_rst;
  wire mig_setup_reset_0_areset_n;
  wire mig_setup_reset_0_design_reset;
  wire mig_setup_reset_0_design_reset_n;
  wire mig_setup_reset_0_up_stream_reset;
(* MARK_DEBUG *)   wire [7:0]mv_state_8bit_out;
(* MARK_DEBUG *)   wire [31:0]poc_axi_addr_out;
(* MARK_DEBUG *)   wire [7:0]pred_state_low8b_out;
  wire [14:0]processing_system7_0_DDR_ADDR;
  wire [2:0]processing_system7_0_DDR_BA;
  wire processing_system7_0_DDR_CAS_N;
  wire processing_system7_0_DDR_CKE;
  wire processing_system7_0_DDR_CK_N;
  wire processing_system7_0_DDR_CK_P;
  wire processing_system7_0_DDR_CS_N;
  wire [3:0]processing_system7_0_DDR_DM;
  wire [31:0]processing_system7_0_DDR_DQ;
  wire [3:0]processing_system7_0_DDR_DQS_N;
  wire [3:0]processing_system7_0_DDR_DQS_P;
  wire processing_system7_0_DDR_ODT;
  wire processing_system7_0_DDR_RAS_N;
  wire processing_system7_0_DDR_RESET_N;
  wire processing_system7_0_DDR_WE_N;
  wire processing_system7_0_FCLK_CLK0;
  wire processing_system7_0_FCLK_RESET0_N;
  wire processing_system7_0_FIXED_IO_DDR_VRN;
  wire processing_system7_0_FIXED_IO_DDR_VRP;
  wire [53:0]processing_system7_0_FIXED_IO_MIO;
  wire processing_system7_0_FIXED_IO_PS_CLK;
  wire processing_system7_0_FIXED_IO_PS_PORB;
  wire processing_system7_0_FIXED_IO_PS_SRSTB;
  wire [31:0]processing_system7_0_M_AXI_GP0_ARADDR;
  wire [1:0]processing_system7_0_M_AXI_GP0_ARBURST;
  wire [3:0]processing_system7_0_M_AXI_GP0_ARCACHE;
  wire [11:0]processing_system7_0_M_AXI_GP0_ARID;
  wire [3:0]processing_system7_0_M_AXI_GP0_ARLEN;
  wire [1:0]processing_system7_0_M_AXI_GP0_ARLOCK;
  wire [2:0]processing_system7_0_M_AXI_GP0_ARPROT;
  wire [3:0]processing_system7_0_M_AXI_GP0_ARQOS;
  wire processing_system7_0_M_AXI_GP0_ARREADY;
  wire [2:0]processing_system7_0_M_AXI_GP0_ARSIZE;
  wire processing_system7_0_M_AXI_GP0_ARVALID;
  wire [31:0]processing_system7_0_M_AXI_GP0_AWADDR;
  wire [1:0]processing_system7_0_M_AXI_GP0_AWBURST;
  wire [3:0]processing_system7_0_M_AXI_GP0_AWCACHE;
  wire [11:0]processing_system7_0_M_AXI_GP0_AWID;
  wire [3:0]processing_system7_0_M_AXI_GP0_AWLEN;
  wire [1:0]processing_system7_0_M_AXI_GP0_AWLOCK;
  wire [2:0]processing_system7_0_M_AXI_GP0_AWPROT;
  wire [3:0]processing_system7_0_M_AXI_GP0_AWQOS;
  wire processing_system7_0_M_AXI_GP0_AWREADY;
  wire [2:0]processing_system7_0_M_AXI_GP0_AWSIZE;
  wire processing_system7_0_M_AXI_GP0_AWVALID;
  wire [11:0]processing_system7_0_M_AXI_GP0_BID;
  wire processing_system7_0_M_AXI_GP0_BREADY;
  wire [1:0]processing_system7_0_M_AXI_GP0_BRESP;
  wire processing_system7_0_M_AXI_GP0_BVALID;
  wire [31:0]processing_system7_0_M_AXI_GP0_RDATA;
  wire [11:0]processing_system7_0_M_AXI_GP0_RID;
  wire processing_system7_0_M_AXI_GP0_RLAST;
  wire processing_system7_0_M_AXI_GP0_RREADY;
  wire [1:0]processing_system7_0_M_AXI_GP0_RRESP;
  wire processing_system7_0_M_AXI_GP0_RVALID;
  wire [31:0]processing_system7_0_M_AXI_GP0_WDATA;
  wire [11:0]processing_system7_0_M_AXI_GP0_WID;
  wire processing_system7_0_M_AXI_GP0_WLAST;
  wire processing_system7_0_M_AXI_GP0_WREADY;
  wire [3:0]processing_system7_0_M_AXI_GP0_WSTRB;
  wire processing_system7_0_M_AXI_GP0_WVALID;
  wire [31:0]processing_system7_0_M_AXI_GP1_ARADDR;
  wire [1:0]processing_system7_0_M_AXI_GP1_ARBURST;
  wire [3:0]processing_system7_0_M_AXI_GP1_ARCACHE;
  wire [11:0]processing_system7_0_M_AXI_GP1_ARID;
  wire [3:0]processing_system7_0_M_AXI_GP1_ARLEN;
  wire [1:0]processing_system7_0_M_AXI_GP1_ARLOCK;
  wire [2:0]processing_system7_0_M_AXI_GP1_ARPROT;
  wire [3:0]processing_system7_0_M_AXI_GP1_ARQOS;
  wire processing_system7_0_M_AXI_GP1_ARREADY;
  wire [2:0]processing_system7_0_M_AXI_GP1_ARSIZE;
  wire processing_system7_0_M_AXI_GP1_ARVALID;
  wire [31:0]processing_system7_0_M_AXI_GP1_AWADDR;
  wire [1:0]processing_system7_0_M_AXI_GP1_AWBURST;
  wire [3:0]processing_system7_0_M_AXI_GP1_AWCACHE;
  wire [11:0]processing_system7_0_M_AXI_GP1_AWID;
  wire [3:0]processing_system7_0_M_AXI_GP1_AWLEN;
  wire [1:0]processing_system7_0_M_AXI_GP1_AWLOCK;
  wire [2:0]processing_system7_0_M_AXI_GP1_AWPROT;
  wire [3:0]processing_system7_0_M_AXI_GP1_AWQOS;
  wire processing_system7_0_M_AXI_GP1_AWREADY;
  wire [2:0]processing_system7_0_M_AXI_GP1_AWSIZE;
  wire processing_system7_0_M_AXI_GP1_AWVALID;
  wire [11:0]processing_system7_0_M_AXI_GP1_BID;
  wire processing_system7_0_M_AXI_GP1_BREADY;
  wire [1:0]processing_system7_0_M_AXI_GP1_BRESP;
  wire processing_system7_0_M_AXI_GP1_BVALID;
  wire [31:0]processing_system7_0_M_AXI_GP1_RDATA;
  wire [11:0]processing_system7_0_M_AXI_GP1_RID;
  wire processing_system7_0_M_AXI_GP1_RLAST;
  wire processing_system7_0_M_AXI_GP1_RREADY;
  wire [1:0]processing_system7_0_M_AXI_GP1_RRESP;
  wire processing_system7_0_M_AXI_GP1_RVALID;
  wire [31:0]processing_system7_0_M_AXI_GP1_WDATA;
  wire [11:0]processing_system7_0_M_AXI_GP1_WID;
  wire processing_system7_0_M_AXI_GP1_WLAST;
  wire processing_system7_0_M_AXI_GP1_WREADY;
  wire [3:0]processing_system7_0_M_AXI_GP1_WSTRB;
  wire processing_system7_0_M_AXI_GP1_WVALID;
(* MARK_DEBUG *)   wire [7:0]residual_read_inf;
  wire [0:0]rst_processing_system7_0_125M_bus_struct_reset;
  wire [0:0]rst_processing_system7_0_125M_interconnect_aresetn;
  wire [0:0]rst_processing_system7_0_125M_peripheral_aresetn;
  wire sao_wr_en_out;
  wire [0:0]xlconstant_0_dout;

  assign DDR3_addr[13:0] = mig_7series_0_DDR3_ADDR;
  assign DDR3_ba[2:0] = mig_7series_0_DDR3_BA;
  assign DDR3_cas_n = mig_7series_0_DDR3_CAS_N;
  assign DDR3_ck_n[0] = mig_7series_0_DDR3_CK_N;
  assign DDR3_ck_p[0] = mig_7series_0_DDR3_CK_P;
  assign DDR3_cke[0] = mig_7series_0_DDR3_CKE;
  assign DDR3_cs_n[0] = mig_7series_0_DDR3_CS_N;
  assign DDR3_dm[7:0] = mig_7series_0_DDR3_DM;
  assign DDR3_odt[0] = mig_7series_0_DDR3_ODT;
  assign DDR3_ras_n = mig_7series_0_DDR3_RAS_N;
  assign DDR3_reset_n = mig_7series_0_DDR3_RESET_N;
  assign DDR3_we_n = mig_7series_0_DDR3_WE_N;
  assign SYS_CLK_1_CLK_N = SYS_CLK_clk_n;
  assign SYS_CLK_1_CLK_P = SYS_CLK_clk_p;
GND GND
       (.G(GND_1));
VCC VCC
       (.P(VCC_1));
zynq_design_1_axi_bram_ctrl_0_bram_0 axi_bram_ctrl_0_bram
       (.addra(axi_bram_ctrl_0_BRAM_PORTA_ADDR),
        .addrb(axi_bram_ctrl_0_BRAM_PORTB_ADDR),
        .clka(axi_bram_ctrl_0_BRAM_PORTA_CLK),
        .clkb(axi_bram_ctrl_0_BRAM_PORTB_CLK),
        .dina(axi_bram_ctrl_0_BRAM_PORTA_DIN),
        .dinb(axi_bram_ctrl_0_BRAM_PORTB_DIN),
        .douta(axi_bram_ctrl_0_BRAM_PORTA_DOUT),
        .doutb(axi_bram_ctrl_0_BRAM_PORTB_DOUT),
        .ena(axi_bram_ctrl_0_BRAM_PORTA_EN),
        .enb(axi_bram_ctrl_0_BRAM_PORTB_EN),
        .rsta(axi_bram_ctrl_0_BRAM_PORTA_RST),
        .rstb(axi_bram_ctrl_0_BRAM_PORTB_RST),
        .wea(axi_bram_ctrl_0_BRAM_PORTA_WE),
        .web(axi_bram_ctrl_0_BRAM_PORTB_WE));
zynq_design_1_axi_inter_con_0_0 axi_inter_con_0
       (.INTERCONNECT_ACLK(mig_7series_0_ui_clk),
        .INTERCONNECT_ARESETN(mig_setup_reset_0_design_reset_n),
        .M00_AXI_ACLK(mig_7series_0_ui_clk),
        .M00_AXI_ARADDR(axi_inter_con_0_M00_AXI_ARADDR),
        .M00_AXI_ARBURST(axi_inter_con_0_M00_AXI_ARBURST),
        .M00_AXI_ARCACHE(axi_inter_con_0_M00_AXI_ARCACHE),
        .M00_AXI_ARID(axi_inter_con_0_M00_AXI_ARID),
        .M00_AXI_ARLEN(axi_inter_con_0_M00_AXI_ARLEN),
        .M00_AXI_ARLOCK(axi_inter_con_0_M00_AXI_ARLOCK),
        .M00_AXI_ARPROT(axi_inter_con_0_M00_AXI_ARPROT),
        .M00_AXI_ARQOS(axi_inter_con_0_M00_AXI_ARQOS),
        .M00_AXI_ARREADY(axi_inter_con_0_M00_AXI_ARREADY),
        .M00_AXI_ARSIZE(axi_inter_con_0_M00_AXI_ARSIZE),
        .M00_AXI_ARVALID(axi_inter_con_0_M00_AXI_ARVALID),
        .M00_AXI_AWADDR(axi_inter_con_0_M00_AXI_AWADDR),
        .M00_AXI_AWBURST(axi_inter_con_0_M00_AXI_AWBURST),
        .M00_AXI_AWCACHE(axi_inter_con_0_M00_AXI_AWCACHE),
        .M00_AXI_AWID(axi_inter_con_0_M00_AXI_AWID),
        .M00_AXI_AWLEN(axi_inter_con_0_M00_AXI_AWLEN),
        .M00_AXI_AWLOCK(axi_inter_con_0_M00_AXI_AWLOCK),
        .M00_AXI_AWPROT(axi_inter_con_0_M00_AXI_AWPROT),
        .M00_AXI_AWQOS(axi_inter_con_0_M00_AXI_AWQOS),
        .M00_AXI_AWREADY(axi_inter_con_0_M00_AXI_AWREADY),
        .M00_AXI_AWSIZE(axi_inter_con_0_M00_AXI_AWSIZE),
        .M00_AXI_AWVALID(axi_inter_con_0_M00_AXI_AWVALID),
        .M00_AXI_BID(axi_inter_con_0_M00_AXI_BID),
        .M00_AXI_BREADY(axi_inter_con_0_M00_AXI_BREADY),
        .M00_AXI_BRESP(axi_inter_con_0_M00_AXI_BRESP),
        .M00_AXI_BVALID(axi_inter_con_0_M00_AXI_BVALID),
        .M00_AXI_RDATA(axi_inter_con_0_M00_AXI_RDATA),
        .M00_AXI_RID(axi_inter_con_0_M00_AXI_RID),
        .M00_AXI_RLAST(axi_inter_con_0_M00_AXI_RLAST),
        .M00_AXI_RREADY(axi_inter_con_0_M00_AXI_RREADY),
        .M00_AXI_RRESP(axi_inter_con_0_M00_AXI_RRESP),
        .M00_AXI_RVALID(axi_inter_con_0_M00_AXI_RVALID),
        .M00_AXI_WDATA(axi_inter_con_0_M00_AXI_WDATA),
        .M00_AXI_WLAST(axi_inter_con_0_M00_AXI_WLAST),
        .M00_AXI_WREADY(axi_inter_con_0_M00_AXI_WREADY),
        .M00_AXI_WSTRB(axi_inter_con_0_M00_AXI_WSTRB),
        .M00_AXI_WVALID(axi_inter_con_0_M00_AXI_WVALID),
        .S00_AXI_ACLK(mig_7series_0_ui_clk),
        .S00_AXI_ARADDR(hevc_top_with_eth_0_ref_pix_axi_master_inf_ARADDR),
        .S00_AXI_ARBURST(hevc_top_with_eth_0_ref_pix_axi_master_inf_ARBURST),
        .S00_AXI_ARCACHE({GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_ARID(GND_1),
        .S00_AXI_ARLEN(hevc_top_with_eth_0_ref_pix_axi_master_inf_ARLEN),
        .S00_AXI_ARLOCK(GND_1),
        .S00_AXI_ARPROT(hevc_top_with_eth_0_ref_pix_axi_master_inf_ARPROT),
        .S00_AXI_ARQOS({GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_ARREADY(hevc_top_with_eth_0_ref_pix_axi_master_inf_ARREADY),
        .S00_AXI_ARSIZE(hevc_top_with_eth_0_ref_pix_axi_master_inf_ARSIZE),
        .S00_AXI_ARVALID(hevc_top_with_eth_0_ref_pix_axi_master_inf_ARVALID),
        .S00_AXI_AWADDR({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_AWBURST({VCC_1,VCC_1}),
        .S00_AXI_AWCACHE({GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_AWID(GND_1),
        .S00_AXI_AWLEN({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_AWLOCK(GND_1),
        .S00_AXI_AWPROT({GND_1,GND_1,GND_1}),
        .S00_AXI_AWQOS({GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_AWSIZE({GND_1,GND_1,GND_1}),
        .S00_AXI_AWVALID(GND_1),
        .S00_AXI_BREADY(GND_1),
        .S00_AXI_RDATA(hevc_top_with_eth_0_ref_pix_axi_master_inf_RDATA),
        .S00_AXI_RLAST(hevc_top_with_eth_0_ref_pix_axi_master_inf_RLAST),
        .S00_AXI_RREADY(hevc_top_with_eth_0_ref_pix_axi_master_inf_RREADY),
        .S00_AXI_RRESP(hevc_top_with_eth_0_ref_pix_axi_master_inf_RRESP),
        .S00_AXI_RVALID(hevc_top_with_eth_0_ref_pix_axi_master_inf_RVALID),
        .S00_AXI_WDATA({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_WLAST(GND_1),
        .S00_AXI_WSTRB({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S00_AXI_WVALID(GND_1),
        .S01_AXI_ACLK(mig_7series_0_ui_clk),
        .S01_AXI_ARADDR({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S01_AXI_ARBURST({VCC_1,VCC_1}),
        .S01_AXI_ARCACHE({GND_1,GND_1,GND_1,GND_1}),
        .S01_AXI_ARID(GND_1),
        .S01_AXI_ARLEN({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S01_AXI_ARLOCK(GND_1),
        .S01_AXI_ARPROT({GND_1,GND_1,GND_1}),
        .S01_AXI_ARQOS({GND_1,GND_1,GND_1,GND_1}),
        .S01_AXI_ARSIZE({GND_1,GND_1,GND_1}),
        .S01_AXI_ARVALID(GND_1),
        .S01_AXI_AWADDR(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWADDR),
        .S01_AXI_AWBURST(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWBURST),
        .S01_AXI_AWCACHE(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWCACHE),
        .S01_AXI_AWID(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWID),
        .S01_AXI_AWLEN(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWLEN),
        .S01_AXI_AWLOCK(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWLOCK),
        .S01_AXI_AWPROT(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWPROT),
        .S01_AXI_AWQOS({GND_1,GND_1,GND_1,GND_1}),
        .S01_AXI_AWREADY(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWREADY),
        .S01_AXI_AWSIZE(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWSIZE),
        .S01_AXI_AWVALID(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWVALID),
        .S01_AXI_BID(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_BID),
        .S01_AXI_BREADY(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_BREADY),
        .S01_AXI_BRESP(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_BRESP),
        .S01_AXI_BVALID(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_BVALID),
        .S01_AXI_RREADY(GND_1),
        .S01_AXI_WDATA(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_WDATA),
        .S01_AXI_WLAST(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_WLAST),
        .S01_AXI_WREADY(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_WREADY),
        .S01_AXI_WSTRB(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_WSTRB),
        .S01_AXI_WVALID(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_WVALID),
        .S02_AXI_ACLK(mig_7series_0_ui_clk),
        .S02_AXI_ARADDR(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARADDR),
        .S02_AXI_ARBURST(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARBURST),
        .S02_AXI_ARCACHE(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARCACHE),
        .S02_AXI_ARID(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARID),
        .S02_AXI_ARLEN(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARLEN),
        .S02_AXI_ARLOCK(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARLOCK),
        .S02_AXI_ARPROT(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARPROT),
        .S02_AXI_ARQOS({GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_ARREADY(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARREADY),
        .S02_AXI_ARSIZE(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARSIZE),
        .S02_AXI_ARVALID(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARVALID),
        .S02_AXI_AWADDR({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_AWBURST({VCC_1,VCC_1}),
        .S02_AXI_AWCACHE({GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_AWID(GND_1),
        .S02_AXI_AWLEN({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_AWLOCK(GND_1),
        .S02_AXI_AWPROT({GND_1,GND_1,GND_1}),
        .S02_AXI_AWQOS({GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_AWSIZE({GND_1,GND_1,GND_1}),
        .S02_AXI_AWVALID(GND_1),
        .S02_AXI_BREADY(GND_1),
        .S02_AXI_RDATA(hevc_top_with_eth_0_mv_pref_axi_master_inf_RDATA),
        .S02_AXI_RLAST(hevc_top_with_eth_0_mv_pref_axi_master_inf_RLAST),
        .S02_AXI_RREADY(hevc_top_with_eth_0_mv_pref_axi_master_inf_RREADY),
        .S02_AXI_RRESP(hevc_top_with_eth_0_mv_pref_axi_master_inf_RRESP),
        .S02_AXI_RVALID(hevc_top_with_eth_0_mv_pref_axi_master_inf_RVALID),
        .S02_AXI_WDATA({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_WLAST(GND_1),
        .S02_AXI_WSTRB({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S02_AXI_WVALID(GND_1),
        .S03_AXI_ACLK(mig_7series_0_ui_clk),
        .S03_AXI_ARADDR({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S03_AXI_ARBURST({VCC_1,VCC_1}),
        .S03_AXI_ARCACHE({GND_1,GND_1,GND_1,GND_1}),
        .S03_AXI_ARID(GND_1),
        .S03_AXI_ARLEN({GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1,GND_1}),
        .S03_AXI_ARLOCK(GND_1),
        .S03_AXI_ARPROT({GND_1,GND_1,GND_1}),
        .S03_AXI_ARQOS({GND_1,GND_1,GND_1,GND_1}),
        .S03_AXI_ARSIZE({GND_1,GND_1,GND_1}),
        .S03_AXI_ARVALID(GND_1),
        .S03_AXI_AWADDR(hevc_top_with_eth_0_mv_col_axi_master_inf_AWADDR),
        .S03_AXI_AWBURST(hevc_top_with_eth_0_mv_col_axi_master_inf_AWBURST),
        .S03_AXI_AWCACHE(hevc_top_with_eth_0_mv_col_axi_master_inf_AWCACHE),
        .S03_AXI_AWID(hevc_top_with_eth_0_mv_col_axi_master_inf_AWID),
        .S03_AXI_AWLEN(hevc_top_with_eth_0_mv_col_axi_master_inf_AWLEN),
        .S03_AXI_AWLOCK(hevc_top_with_eth_0_mv_col_axi_master_inf_AWLOCK),
        .S03_AXI_AWPROT(hevc_top_with_eth_0_mv_col_axi_master_inf_AWPROT),
        .S03_AXI_AWQOS({GND_1,GND_1,GND_1,GND_1}),
        .S03_AXI_AWREADY(hevc_top_with_eth_0_mv_col_axi_master_inf_AWREADY),
        .S03_AXI_AWSIZE(hevc_top_with_eth_0_mv_col_axi_master_inf_AWSIZE),
        .S03_AXI_AWVALID(hevc_top_with_eth_0_mv_col_axi_master_inf_AWVALID),
        .S03_AXI_BREADY(hevc_top_with_eth_0_mv_col_axi_master_inf_BREADY),
        .S03_AXI_BRESP(hevc_top_with_eth_0_mv_col_axi_master_inf_BRESP),
        .S03_AXI_BVALID(hevc_top_with_eth_0_mv_col_axi_master_inf_BVALID),
        .S03_AXI_RREADY(GND_1),
        .S03_AXI_WDATA(hevc_top_with_eth_0_mv_col_axi_master_inf_WDATA),
        .S03_AXI_WLAST(hevc_top_with_eth_0_mv_col_axi_master_inf_WLAST),
        .S03_AXI_WREADY(hevc_top_with_eth_0_mv_col_axi_master_inf_WREADY),
        .S03_AXI_WSTRB(hevc_top_with_eth_0_mv_col_axi_master_inf_WSTRB),
        .S03_AXI_WVALID(hevc_top_with_eth_0_mv_col_axi_master_inf_WVALID));
zynq_design_1_axi_mem_intercon_0 axi_mem_intercon
       (.ACLK(processing_system7_0_FCLK_CLK0),
        .ARESETN(rst_processing_system7_0_125M_interconnect_aresetn),
        .M00_ACLK(processing_system7_0_FCLK_CLK0),
        .M00_ARESETN(rst_processing_system7_0_125M_peripheral_aresetn),
        .M00_AXI_araddr(axi_mem_intercon_M00_AXI_ARADDR),
        .M00_AXI_arburst(axi_mem_intercon_M00_AXI_ARBURST),
        .M00_AXI_arcache(axi_mem_intercon_M00_AXI_ARCACHE),
        .M00_AXI_arid(axi_mem_intercon_M00_AXI_ARID),
        .M00_AXI_arlen(axi_mem_intercon_M00_AXI_ARLEN),
        .M00_AXI_arlock(axi_mem_intercon_M00_AXI_ARLOCK),
        .M00_AXI_arprot(axi_mem_intercon_M00_AXI_ARPROT),
        .M00_AXI_arready(axi_mem_intercon_M00_AXI_ARREADY),
        .M00_AXI_arsize(axi_mem_intercon_M00_AXI_ARSIZE),
        .M00_AXI_arvalid(axi_mem_intercon_M00_AXI_ARVALID),
        .M00_AXI_awaddr(axi_mem_intercon_M00_AXI_AWADDR),
        .M00_AXI_awburst(axi_mem_intercon_M00_AXI_AWBURST),
        .M00_AXI_awcache(axi_mem_intercon_M00_AXI_AWCACHE),
        .M00_AXI_awid(axi_mem_intercon_M00_AXI_AWID),
        .M00_AXI_awlen(axi_mem_intercon_M00_AXI_AWLEN),
        .M00_AXI_awlock(axi_mem_intercon_M00_AXI_AWLOCK),
        .M00_AXI_awprot(axi_mem_intercon_M00_AXI_AWPROT),
        .M00_AXI_awready(axi_mem_intercon_M00_AXI_AWREADY),
        .M00_AXI_awsize(axi_mem_intercon_M00_AXI_AWSIZE),
        .M00_AXI_awvalid(axi_mem_intercon_M00_AXI_AWVALID),
        .M00_AXI_bid(axi_mem_intercon_M00_AXI_BID),
        .M00_AXI_bready(axi_mem_intercon_M00_AXI_BREADY),
        .M00_AXI_bresp(axi_mem_intercon_M00_AXI_BRESP),
        .M00_AXI_bvalid(axi_mem_intercon_M00_AXI_BVALID),
        .M00_AXI_rdata(axi_mem_intercon_M00_AXI_RDATA),
        .M00_AXI_rid(axi_mem_intercon_M00_AXI_RID),
        .M00_AXI_rlast(axi_mem_intercon_M00_AXI_RLAST),
        .M00_AXI_rready(axi_mem_intercon_M00_AXI_RREADY),
        .M00_AXI_rresp(axi_mem_intercon_M00_AXI_RRESP),
        .M00_AXI_rvalid(axi_mem_intercon_M00_AXI_RVALID),
        .M00_AXI_wdata(axi_mem_intercon_M00_AXI_WDATA),
        .M00_AXI_wlast(axi_mem_intercon_M00_AXI_WLAST),
        .M00_AXI_wready(axi_mem_intercon_M00_AXI_WREADY),
        .M00_AXI_wstrb(axi_mem_intercon_M00_AXI_WSTRB),
        .M00_AXI_wvalid(axi_mem_intercon_M00_AXI_WVALID),
        .S00_ACLK(processing_system7_0_FCLK_CLK0),
        .S00_ARESETN(rst_processing_system7_0_125M_peripheral_aresetn),
        .S00_AXI_araddr(processing_system7_0_M_AXI_GP0_ARADDR),
        .S00_AXI_arburst(processing_system7_0_M_AXI_GP0_ARBURST),
        .S00_AXI_arcache(processing_system7_0_M_AXI_GP0_ARCACHE),
        .S00_AXI_arid(processing_system7_0_M_AXI_GP0_ARID),
        .S00_AXI_arlen(processing_system7_0_M_AXI_GP0_ARLEN),
        .S00_AXI_arlock(processing_system7_0_M_AXI_GP0_ARLOCK),
        .S00_AXI_arprot(processing_system7_0_M_AXI_GP0_ARPROT),
        .S00_AXI_arqos(processing_system7_0_M_AXI_GP0_ARQOS),
        .S00_AXI_arready(processing_system7_0_M_AXI_GP0_ARREADY),
        .S00_AXI_arsize(processing_system7_0_M_AXI_GP0_ARSIZE),
        .S00_AXI_arvalid(processing_system7_0_M_AXI_GP0_ARVALID),
        .S00_AXI_awaddr(processing_system7_0_M_AXI_GP0_AWADDR),
        .S00_AXI_awburst(processing_system7_0_M_AXI_GP0_AWBURST),
        .S00_AXI_awcache(processing_system7_0_M_AXI_GP0_AWCACHE),
        .S00_AXI_awid(processing_system7_0_M_AXI_GP0_AWID),
        .S00_AXI_awlen(processing_system7_0_M_AXI_GP0_AWLEN),
        .S00_AXI_awlock(processing_system7_0_M_AXI_GP0_AWLOCK),
        .S00_AXI_awprot(processing_system7_0_M_AXI_GP0_AWPROT),
        .S00_AXI_awqos(processing_system7_0_M_AXI_GP0_AWQOS),
        .S00_AXI_awready(processing_system7_0_M_AXI_GP0_AWREADY),
        .S00_AXI_awsize(processing_system7_0_M_AXI_GP0_AWSIZE),
        .S00_AXI_awvalid(processing_system7_0_M_AXI_GP0_AWVALID),
        .S00_AXI_bid(processing_system7_0_M_AXI_GP0_BID),
        .S00_AXI_bready(processing_system7_0_M_AXI_GP0_BREADY),
        .S00_AXI_bresp(processing_system7_0_M_AXI_GP0_BRESP),
        .S00_AXI_bvalid(processing_system7_0_M_AXI_GP0_BVALID),
        .S00_AXI_rdata(processing_system7_0_M_AXI_GP0_RDATA),
        .S00_AXI_rid(processing_system7_0_M_AXI_GP0_RID),
        .S00_AXI_rlast(processing_system7_0_M_AXI_GP0_RLAST),
        .S00_AXI_rready(processing_system7_0_M_AXI_GP0_RREADY),
        .S00_AXI_rresp(processing_system7_0_M_AXI_GP0_RRESP),
        .S00_AXI_rvalid(processing_system7_0_M_AXI_GP0_RVALID),
        .S00_AXI_wdata(processing_system7_0_M_AXI_GP0_WDATA),
        .S00_AXI_wid(processing_system7_0_M_AXI_GP0_WID),
        .S00_AXI_wlast(processing_system7_0_M_AXI_GP0_WLAST),
        .S00_AXI_wready(processing_system7_0_M_AXI_GP0_WREADY),
        .S00_AXI_wstrb(processing_system7_0_M_AXI_GP0_WSTRB),
        .S00_AXI_wvalid(processing_system7_0_M_AXI_GP0_WVALID));
zynq_design_1_axi_mem_intercon_1_0 axi_mem_intercon_1
       (.ACLK(processing_system7_0_FCLK_CLK0),
        .ARESETN(rst_processing_system7_0_125M_interconnect_aresetn),
        .M00_ACLK(processing_system7_0_FCLK_CLK0),
        .M00_ARESETN(rst_processing_system7_0_125M_peripheral_aresetn),
        .M00_AXI_araddr(axi_mem_intercon_1_M00_AXI_ARADDR),
        .M00_AXI_arburst(axi_mem_intercon_1_M00_AXI_ARBURST),
        .M00_AXI_arcache(axi_mem_intercon_1_M00_AXI_ARCACHE),
        .M00_AXI_arid(axi_mem_intercon_1_M00_AXI_ARID),
        .M00_AXI_arlen(axi_mem_intercon_1_M00_AXI_ARLEN),
        .M00_AXI_arlock(axi_mem_intercon_1_M00_AXI_ARLOCK),
        .M00_AXI_arprot(axi_mem_intercon_1_M00_AXI_ARPROT),
        .M00_AXI_arqos(axi_mem_intercon_1_M00_AXI_ARQOS),
        .M00_AXI_arready(axi_mem_intercon_1_M00_AXI_ARREADY),
        .M00_AXI_arsize(axi_mem_intercon_1_M00_AXI_ARSIZE),
        .M00_AXI_arvalid(axi_mem_intercon_1_M00_AXI_ARVALID),
        .M00_AXI_awaddr(axi_mem_intercon_1_M00_AXI_AWADDR),
        .M00_AXI_awburst(axi_mem_intercon_1_M00_AXI_AWBURST),
        .M00_AXI_awcache(axi_mem_intercon_1_M00_AXI_AWCACHE),
        .M00_AXI_awid(axi_mem_intercon_1_M00_AXI_AWID),
        .M00_AXI_awlen(axi_mem_intercon_1_M00_AXI_AWLEN),
        .M00_AXI_awlock(axi_mem_intercon_1_M00_AXI_AWLOCK),
        .M00_AXI_awprot(axi_mem_intercon_1_M00_AXI_AWPROT),
        .M00_AXI_awqos(axi_mem_intercon_1_M00_AXI_AWQOS),
        .M00_AXI_awready(axi_mem_intercon_1_M00_AXI_AWREADY),
        .M00_AXI_awsize(axi_mem_intercon_1_M00_AXI_AWSIZE),
        .M00_AXI_awvalid(axi_mem_intercon_1_M00_AXI_AWVALID),
        .M00_AXI_bid(axi_mem_intercon_1_M00_AXI_BID),
        .M00_AXI_bready(axi_mem_intercon_1_M00_AXI_BREADY),
        .M00_AXI_bresp(axi_mem_intercon_1_M00_AXI_BRESP),
        .M00_AXI_bvalid(axi_mem_intercon_1_M00_AXI_BVALID),
        .M00_AXI_rdata(axi_mem_intercon_1_M00_AXI_RDATA),
        .M00_AXI_rid(axi_mem_intercon_1_M00_AXI_RID),
        .M00_AXI_rlast(axi_mem_intercon_1_M00_AXI_RLAST),
        .M00_AXI_rready(axi_mem_intercon_1_M00_AXI_RREADY),
        .M00_AXI_rresp(axi_mem_intercon_1_M00_AXI_RRESP),
        .M00_AXI_rvalid(axi_mem_intercon_1_M00_AXI_RVALID),
        .M00_AXI_wdata(axi_mem_intercon_1_M00_AXI_WDATA),
        .M00_AXI_wlast(axi_mem_intercon_1_M00_AXI_WLAST),
        .M00_AXI_wready(axi_mem_intercon_1_M00_AXI_WREADY),
        .M00_AXI_wstrb(axi_mem_intercon_1_M00_AXI_WSTRB),
        .M00_AXI_wvalid(axi_mem_intercon_1_M00_AXI_WVALID),
        .S00_ACLK(processing_system7_0_FCLK_CLK0),
        .S00_ARESETN(rst_processing_system7_0_125M_peripheral_aresetn),
        .S00_AXI_araddr(processing_system7_0_M_AXI_GP1_ARADDR),
        .S00_AXI_arburst(processing_system7_0_M_AXI_GP1_ARBURST),
        .S00_AXI_arcache(processing_system7_0_M_AXI_GP1_ARCACHE),
        .S00_AXI_arid(processing_system7_0_M_AXI_GP1_ARID),
        .S00_AXI_arlen(processing_system7_0_M_AXI_GP1_ARLEN),
        .S00_AXI_arlock(processing_system7_0_M_AXI_GP1_ARLOCK),
        .S00_AXI_arprot(processing_system7_0_M_AXI_GP1_ARPROT),
        .S00_AXI_arqos(processing_system7_0_M_AXI_GP1_ARQOS),
        .S00_AXI_arready(processing_system7_0_M_AXI_GP1_ARREADY),
        .S00_AXI_arsize(processing_system7_0_M_AXI_GP1_ARSIZE),
        .S00_AXI_arvalid(processing_system7_0_M_AXI_GP1_ARVALID),
        .S00_AXI_awaddr(processing_system7_0_M_AXI_GP1_AWADDR),
        .S00_AXI_awburst(processing_system7_0_M_AXI_GP1_AWBURST),
        .S00_AXI_awcache(processing_system7_0_M_AXI_GP1_AWCACHE),
        .S00_AXI_awid(processing_system7_0_M_AXI_GP1_AWID),
        .S00_AXI_awlen(processing_system7_0_M_AXI_GP1_AWLEN),
        .S00_AXI_awlock(processing_system7_0_M_AXI_GP1_AWLOCK),
        .S00_AXI_awprot(processing_system7_0_M_AXI_GP1_AWPROT),
        .S00_AXI_awqos(processing_system7_0_M_AXI_GP1_AWQOS),
        .S00_AXI_awready(processing_system7_0_M_AXI_GP1_AWREADY),
        .S00_AXI_awsize(processing_system7_0_M_AXI_GP1_AWSIZE),
        .S00_AXI_awvalid(processing_system7_0_M_AXI_GP1_AWVALID),
        .S00_AXI_bid(processing_system7_0_M_AXI_GP1_BID),
        .S00_AXI_bready(processing_system7_0_M_AXI_GP1_BREADY),
        .S00_AXI_bresp(processing_system7_0_M_AXI_GP1_BRESP),
        .S00_AXI_bvalid(processing_system7_0_M_AXI_GP1_BVALID),
        .S00_AXI_rdata(processing_system7_0_M_AXI_GP1_RDATA),
        .S00_AXI_rid(processing_system7_0_M_AXI_GP1_RID),
        .S00_AXI_rlast(processing_system7_0_M_AXI_GP1_RLAST),
        .S00_AXI_rready(processing_system7_0_M_AXI_GP1_RREADY),
        .S00_AXI_rresp(processing_system7_0_M_AXI_GP1_RRESP),
        .S00_AXI_rvalid(processing_system7_0_M_AXI_GP1_RVALID),
        .S00_AXI_wdata(processing_system7_0_M_AXI_GP1_WDATA),
        .S00_AXI_wid(processing_system7_0_M_AXI_GP1_WID),
        .S00_AXI_wlast(processing_system7_0_M_AXI_GP1_WLAST),
        .S00_AXI_wready(processing_system7_0_M_AXI_GP1_WREADY),
        .S00_AXI_wstrb(processing_system7_0_M_AXI_GP1_WSTRB),
        .S00_AXI_wvalid(processing_system7_0_M_AXI_GP1_WVALID));
zynq_design_1_axi_wr_slave_to_fifo_0_1 axi_wr_slave_to_fifo_0
       (.S00_AXI_ACLK(processing_system7_0_FCLK_CLK0),
        .S00_AXI_ARADDR(axi_mem_intercon_1_M00_AXI_ARADDR),
        .S00_AXI_ARBURST(axi_mem_intercon_1_M00_AXI_ARBURST),
        .S00_AXI_ARCACHE(axi_mem_intercon_1_M00_AXI_ARCACHE),
        .S00_AXI_ARID(axi_mem_intercon_1_M00_AXI_ARID),
        .S00_AXI_ARLEN(axi_mem_intercon_1_M00_AXI_ARLEN),
        .S00_AXI_ARLOCK(axi_mem_intercon_1_M00_AXI_ARLOCK),
        .S00_AXI_ARPROT(axi_mem_intercon_1_M00_AXI_ARPROT),
        .S00_AXI_ARQOS(axi_mem_intercon_1_M00_AXI_ARQOS),
        .S00_AXI_ARREADY(axi_mem_intercon_1_M00_AXI_ARREADY),
        .S00_AXI_ARSIZE(axi_mem_intercon_1_M00_AXI_ARSIZE),
        .S00_AXI_ARVALID(axi_mem_intercon_1_M00_AXI_ARVALID),
        .S00_AXI_AWADDR(axi_mem_intercon_1_M00_AXI_AWADDR),
        .S00_AXI_AWBURST(axi_mem_intercon_1_M00_AXI_AWBURST),
        .S00_AXI_AWCACHE(axi_mem_intercon_1_M00_AXI_AWCACHE),
        .S00_AXI_AWID(axi_mem_intercon_1_M00_AXI_AWID),
        .S00_AXI_AWLEN(axi_mem_intercon_1_M00_AXI_AWLEN),
        .S00_AXI_AWLOCK(axi_mem_intercon_1_M00_AXI_AWLOCK),
        .S00_AXI_AWPROT(axi_mem_intercon_1_M00_AXI_AWPROT),
        .S00_AXI_AWQOS(axi_mem_intercon_1_M00_AXI_AWQOS),
        .S00_AXI_AWREADY(axi_mem_intercon_1_M00_AXI_AWREADY),
        .S00_AXI_AWSIZE(axi_mem_intercon_1_M00_AXI_AWSIZE),
        .S00_AXI_AWVALID(axi_mem_intercon_1_M00_AXI_AWVALID),
        .S00_AXI_BID(axi_mem_intercon_1_M00_AXI_BID),
        .S00_AXI_BREADY(axi_mem_intercon_1_M00_AXI_BREADY),
        .S00_AXI_BRESP(axi_mem_intercon_1_M00_AXI_BRESP),
        .S00_AXI_BVALID(axi_mem_intercon_1_M00_AXI_BVALID),
        .S00_AXI_RDATA(axi_mem_intercon_1_M00_AXI_RDATA),
        .S00_AXI_RID(axi_mem_intercon_1_M00_AXI_RID),
        .S00_AXI_RLAST(axi_mem_intercon_1_M00_AXI_RLAST),
        .S00_AXI_RREADY(axi_mem_intercon_1_M00_AXI_RREADY),
        .S00_AXI_RRESP(axi_mem_intercon_1_M00_AXI_RRESP),
        .S00_AXI_RVALID(axi_mem_intercon_1_M00_AXI_RVALID),
        .S00_AXI_WDATA(axi_mem_intercon_1_M00_AXI_WDATA),
        .S00_AXI_WLAST(axi_mem_intercon_1_M00_AXI_WLAST),
        .S00_AXI_WREADY(axi_mem_intercon_1_M00_AXI_WREADY),
        .S00_AXI_WSTRB(axi_mem_intercon_1_M00_AXI_WSTRB),
        .S00_AXI_WVALID(axi_mem_intercon_1_M00_AXI_WVALID),
        .axi_fifo_state_out(axi_fifo_state_out),
        .axi_write_clk(axi_wr_slave_to_fifo_0_axi_write_clk),
        .cb_residual_data_out(axi_wr_slave_to_fifo_0_cb_residual_fifo_write_WR_DATA),
        .cb_residual_full(axi_wr_slave_to_fifo_0_cb_residual_fifo_write_FULL),
        .cb_residual_wr_en(axi_wr_slave_to_fifo_0_cb_residual_fifo_write_WR_EN),
        .config_data_out(axi_wr_slave_to_fifo_0_config_fifo_write_WR_DATA),
        .config_full(axi_wr_slave_to_fifo_0_config_fifo_write_FULL),
        .config_wr_en(axi_wr_slave_to_fifo_0_config_fifo_write_WR_EN),
        .cr_residual_data_out(axi_wr_slave_to_fifo_0_cr_residual_fifo_write_WR_DATA),
        .cr_residual_full(axi_wr_slave_to_fifo_0_cr_residual_fifo_write_FULL),
        .cr_residual_wr_en(axi_wr_slave_to_fifo_0_cr_residual_fifo_write_WR_EN),
        .reset(mig_setup_reset_0_up_stream_reset),
        .y_residual_data_out(axi_wr_slave_to_fifo_0_y_residual_fifo_write_WR_DATA),
        .y_residual_full(axi_wr_slave_to_fifo_0_y_residual_fifo_write_FULL),
        .y_residual_wr_en(axi_wr_slave_to_fifo_0_y_residual_fifo_write_WR_EN));
(* BMM_INFO_ADDRESS_SPACE = "byte  0x40000000 32 >  zynq_design_1 axi_bram_ctrl_0_bram" *) 
   (* KEEP_HIERARCHY = "yes" *) 
   zynq_design_1_axi_bram_ctrl_0_0 buffer_descriptor_memory
       (.bram_addr_a(axi_bram_ctrl_0_BRAM_PORTA_ADDR),
        .bram_addr_b(axi_bram_ctrl_0_BRAM_PORTB_ADDR),
        .bram_clk_a(axi_bram_ctrl_0_BRAM_PORTA_CLK),
        .bram_clk_b(axi_bram_ctrl_0_BRAM_PORTB_CLK),
        .bram_en_a(axi_bram_ctrl_0_BRAM_PORTA_EN),
        .bram_en_b(axi_bram_ctrl_0_BRAM_PORTB_EN),
        .bram_rddata_a(axi_bram_ctrl_0_BRAM_PORTA_DOUT),
        .bram_rddata_b(axi_bram_ctrl_0_BRAM_PORTB_DOUT),
        .bram_rst_a(axi_bram_ctrl_0_BRAM_PORTA_RST),
        .bram_rst_b(axi_bram_ctrl_0_BRAM_PORTB_RST),
        .bram_we_a(axi_bram_ctrl_0_BRAM_PORTA_WE),
        .bram_we_b(axi_bram_ctrl_0_BRAM_PORTB_WE),
        .bram_wrdata_a(axi_bram_ctrl_0_BRAM_PORTA_DIN),
        .bram_wrdata_b(axi_bram_ctrl_0_BRAM_PORTB_DIN),
        .s_axi_aclk(processing_system7_0_FCLK_CLK0),
        .s_axi_araddr(axi_mem_intercon_M00_AXI_ARADDR),
        .s_axi_arburst(axi_mem_intercon_M00_AXI_ARBURST),
        .s_axi_arcache(axi_mem_intercon_M00_AXI_ARCACHE),
        .s_axi_aresetn(rst_processing_system7_0_125M_peripheral_aresetn),
        .s_axi_arid(axi_mem_intercon_M00_AXI_ARID),
        .s_axi_arlen(axi_mem_intercon_M00_AXI_ARLEN),
        .s_axi_arlock(axi_mem_intercon_M00_AXI_ARLOCK),
        .s_axi_arprot(axi_mem_intercon_M00_AXI_ARPROT),
        .s_axi_arready(axi_mem_intercon_M00_AXI_ARREADY),
        .s_axi_arsize(axi_mem_intercon_M00_AXI_ARSIZE),
        .s_axi_arvalid(axi_mem_intercon_M00_AXI_ARVALID),
        .s_axi_awaddr(axi_mem_intercon_M00_AXI_AWADDR),
        .s_axi_awburst(axi_mem_intercon_M00_AXI_AWBURST),
        .s_axi_awcache(axi_mem_intercon_M00_AXI_AWCACHE),
        .s_axi_awid(axi_mem_intercon_M00_AXI_AWID),
        .s_axi_awlen(axi_mem_intercon_M00_AXI_AWLEN),
        .s_axi_awlock(axi_mem_intercon_M00_AXI_AWLOCK),
        .s_axi_awprot(axi_mem_intercon_M00_AXI_AWPROT),
        .s_axi_awready(axi_mem_intercon_M00_AXI_AWREADY),
        .s_axi_awsize(axi_mem_intercon_M00_AXI_AWSIZE),
        .s_axi_awvalid(axi_mem_intercon_M00_AXI_AWVALID),
        .s_axi_bid(axi_mem_intercon_M00_AXI_BID),
        .s_axi_bready(axi_mem_intercon_M00_AXI_BREADY),
        .s_axi_bresp(axi_mem_intercon_M00_AXI_BRESP),
        .s_axi_bvalid(axi_mem_intercon_M00_AXI_BVALID),
        .s_axi_rdata(axi_mem_intercon_M00_AXI_RDATA),
        .s_axi_rid(axi_mem_intercon_M00_AXI_RID),
        .s_axi_rlast(axi_mem_intercon_M00_AXI_RLAST),
        .s_axi_rready(axi_mem_intercon_M00_AXI_RREADY),
        .s_axi_rresp(axi_mem_intercon_M00_AXI_RRESP),
        .s_axi_rvalid(axi_mem_intercon_M00_AXI_RVALID),
        .s_axi_wdata(axi_mem_intercon_M00_AXI_WDATA),
        .s_axi_wlast(axi_mem_intercon_M00_AXI_WLAST),
        .s_axi_wready(axi_mem_intercon_M00_AXI_WREADY),
        .s_axi_wstrb(axi_mem_intercon_M00_AXI_WSTRB),
        .s_axi_wvalid(axi_mem_intercon_M00_AXI_WVALID));
zynq_design_1_fifo_read_driver_0_0 fifo_read_driver_0
       (.empty(hevc_top_with_eth_0_display_fifo_is_empty_out),
        .rd_en(fifo_read_driver_0_rd_en));
zynq_design_1_hevc_top_with_eth_0_0 hevc_top_with_eth_0
       (.cb_residual_fifo_in(axi_wr_slave_to_fifo_0_cb_residual_fifo_write_WR_DATA),
        .cb_residual_fifo_is_full_out(axi_wr_slave_to_fifo_0_cb_residual_fifo_write_FULL),
        .cb_residual_fifo_write_en_in(axi_wr_slave_to_fifo_0_cb_residual_fifo_write_WR_EN),
        .clk(mig_7series_0_ui_clk),
        .cnf_fifo_counter(cnf_fifo_counter),
        .cnf_fifo_out(cnf_fifo_out),
        .comon_pre_lp_luma_4x4_out(comon_pre_lp_luma_4x4_out),
        .comon_pre_lp_x_out(comon_pre_lp_x_out),
        .comon_pre_lp_y_out(comon_pre_lp_y_out),
        .cr_residual_fifo_in(axi_wr_slave_to_fifo_0_cr_residual_fifo_write_WR_DATA),
        .cr_residual_fifo_is_full_out(axi_wr_slave_to_fifo_0_cr_residual_fifo_write_FULL),
        .cr_residual_fifo_write_en_in(axi_wr_slave_to_fifo_0_cr_residual_fifo_write_WR_EN),
        .current_poc_out(current_poc_out),
        .display_fifo_data_out(display_fifo_data_out),
        .display_fifo_is_empty_out(hevc_top_with_eth_0_display_fifo_is_empty_out),
        .display_fifo_rd_en_in(fifo_read_driver_0_rd_en),
        .dpb_wr_fifo_data(dpb_wr_fifo_data),
        .enable(xlconstant_0_dout),
        .fifo_in(axi_wr_slave_to_fifo_0_config_fifo_write_WR_DATA),
        .input_fifo_is_full(axi_wr_slave_to_fifo_0_config_fifo_write_FULL),
        .inter_4x4_valid_out(inter_4x4_valid_out),
        .inter_luma_4x4_out(inter_luma_4x4_out),
        .inter_x_out(inter_x_out),
        .inter_y_out(inter_y_out),
        .intra_4x4_valid_out(intra_4x4_valid_out),
        .intra_luma_4x4_out(intra_luma_4x4_out),
        .intra_ready_out(intra_ready_out),
        .intra_x_out(intra_x_out),
        .intra_y_out(intra_y_out),
        .mv_col_axi_awaddr(hevc_top_with_eth_0_mv_col_axi_master_inf_AWADDR),
        .mv_col_axi_awburst(hevc_top_with_eth_0_mv_col_axi_master_inf_AWBURST),
        .mv_col_axi_awcache(hevc_top_with_eth_0_mv_col_axi_master_inf_AWCACHE),
        .mv_col_axi_awid(hevc_top_with_eth_0_mv_col_axi_master_inf_AWID),
        .mv_col_axi_awlen(hevc_top_with_eth_0_mv_col_axi_master_inf_AWLEN),
        .mv_col_axi_awlock(hevc_top_with_eth_0_mv_col_axi_master_inf_AWLOCK),
        .mv_col_axi_awprot(hevc_top_with_eth_0_mv_col_axi_master_inf_AWPROT),
        .mv_col_axi_awready(hevc_top_with_eth_0_mv_col_axi_master_inf_AWREADY),
        .mv_col_axi_awsize(hevc_top_with_eth_0_mv_col_axi_master_inf_AWSIZE),
        .mv_col_axi_awvalid(hevc_top_with_eth_0_mv_col_axi_master_inf_AWVALID),
        .mv_col_axi_bready(hevc_top_with_eth_0_mv_col_axi_master_inf_BREADY),
        .mv_col_axi_bresp(hevc_top_with_eth_0_mv_col_axi_master_inf_BRESP),
        .mv_col_axi_bvalid(hevc_top_with_eth_0_mv_col_axi_master_inf_BVALID),
        .mv_col_axi_wdata(hevc_top_with_eth_0_mv_col_axi_master_inf_WDATA),
        .mv_col_axi_wlast(hevc_top_with_eth_0_mv_col_axi_master_inf_WLAST),
        .mv_col_axi_wready(hevc_top_with_eth_0_mv_col_axi_master_inf_WREADY),
        .mv_col_axi_wstrb(hevc_top_with_eth_0_mv_col_axi_master_inf_WSTRB),
        .mv_col_axi_wvalid(hevc_top_with_eth_0_mv_col_axi_master_inf_WVALID),
        .mv_pref_axi_araddr(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARADDR),
        .mv_pref_axi_arburst(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARBURST),
        .mv_pref_axi_arcache(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARCACHE),
        .mv_pref_axi_arid(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARID),
        .mv_pref_axi_arlen(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARLEN),
        .mv_pref_axi_arlock(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARLOCK),
        .mv_pref_axi_arprot(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARPROT),
        .mv_pref_axi_arready(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARREADY),
        .mv_pref_axi_arsize(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARSIZE),
        .mv_pref_axi_arvalid(hevc_top_with_eth_0_mv_pref_axi_master_inf_ARVALID),
        .mv_pref_axi_rdata(hevc_top_with_eth_0_mv_pref_axi_master_inf_RDATA),
        .mv_pref_axi_rlast(hevc_top_with_eth_0_mv_pref_axi_master_inf_RLAST),
        .mv_pref_axi_rready(hevc_top_with_eth_0_mv_pref_axi_master_inf_RREADY),
        .mv_pref_axi_rresp(hevc_top_with_eth_0_mv_pref_axi_master_inf_RRESP),
        .mv_pref_axi_rvalid(hevc_top_with_eth_0_mv_pref_axi_master_inf_RVALID),
        .mv_state_8bit_out(mv_state_8bit_out),
        .poc_axi_addr_out(poc_axi_addr_out),
        .pred_state_low8b_out(pred_state_low8b_out),
        .ref_pic_write_axi_awaddr(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWADDR),
        .ref_pic_write_axi_awburst(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWBURST),
        .ref_pic_write_axi_awcache(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWCACHE),
        .ref_pic_write_axi_awid(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWID),
        .ref_pic_write_axi_awlen(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWLEN),
        .ref_pic_write_axi_awlock(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWLOCK),
        .ref_pic_write_axi_awprot(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWPROT),
        .ref_pic_write_axi_awready(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWREADY),
        .ref_pic_write_axi_awsize(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWSIZE),
        .ref_pic_write_axi_awvalid(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_AWVALID),
        .ref_pic_write_axi_bid(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_BID),
        .ref_pic_write_axi_bready(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_BREADY),
        .ref_pic_write_axi_bresp(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_BRESP),
        .ref_pic_write_axi_bvalid(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_BVALID),
        .ref_pic_write_axi_wdata(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_WDATA),
        .ref_pic_write_axi_wlast(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_WLAST),
        .ref_pic_write_axi_wready(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_WREADY),
        .ref_pic_write_axi_wstrb(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_WSTRB),
        .ref_pic_write_axi_wvalid(hevc_top_with_eth_0_ref_pic_write_axi_master_inf_WVALID),
        .ref_pix_axi_ar_addr(hevc_top_with_eth_0_ref_pix_axi_master_inf_ARADDR),
        .ref_pix_axi_ar_burst(hevc_top_with_eth_0_ref_pix_axi_master_inf_ARBURST),
        .ref_pix_axi_ar_len(hevc_top_with_eth_0_ref_pix_axi_master_inf_ARLEN),
        .ref_pix_axi_ar_prot(hevc_top_with_eth_0_ref_pix_axi_master_inf_ARPROT),
        .ref_pix_axi_ar_ready(hevc_top_with_eth_0_ref_pix_axi_master_inf_ARREADY),
        .ref_pix_axi_ar_size(hevc_top_with_eth_0_ref_pix_axi_master_inf_ARSIZE),
        .ref_pix_axi_ar_valid(hevc_top_with_eth_0_ref_pix_axi_master_inf_ARVALID),
        .ref_pix_axi_r_data(hevc_top_with_eth_0_ref_pix_axi_master_inf_RDATA),
        .ref_pix_axi_r_last(hevc_top_with_eth_0_ref_pix_axi_master_inf_RLAST),
        .ref_pix_axi_r_ready(hevc_top_with_eth_0_ref_pix_axi_master_inf_RREADY),
        .ref_pix_axi_r_resp(hevc_top_with_eth_0_ref_pix_axi_master_inf_RRESP),
        .ref_pix_axi_r_valid(hevc_top_with_eth_0_ref_pix_axi_master_inf_RVALID),
        .reset(mig_setup_reset_0_design_reset),
        .residual_read_inf(residual_read_inf),
        .sao_wr_en_out(sao_wr_en_out),
        .wr_clk(axi_wr_slave_to_fifo_0_axi_write_clk),
        .write_en_in(axi_wr_slave_to_fifo_0_config_fifo_write_WR_EN),
        .y_residual_fifo_in(axi_wr_slave_to_fifo_0_y_residual_fifo_write_WR_DATA),
        .y_residual_fifo_is_full_out(axi_wr_slave_to_fifo_0_y_residual_fifo_write_FULL),
        .y_residual_fifo_write_en_in(axi_wr_slave_to_fifo_0_y_residual_fifo_write_WR_EN));
zynq_design_1_mig_7series_0_0 mig_7series_0
       (.aresetn(mig_setup_reset_0_areset_n),
        .ddr3_addr(mig_7series_0_DDR3_ADDR),
        .ddr3_ba(mig_7series_0_DDR3_BA),
        .ddr3_cas_n(mig_7series_0_DDR3_CAS_N),
        .ddr3_ck_n(mig_7series_0_DDR3_CK_N),
        .ddr3_ck_p(mig_7series_0_DDR3_CK_P),
        .ddr3_cke(mig_7series_0_DDR3_CKE),
        .ddr3_cs_n(mig_7series_0_DDR3_CS_N),
        .ddr3_dm(mig_7series_0_DDR3_DM),
        .ddr3_dq(DDR3_dq[63:0]),
        .ddr3_dqs_n(DDR3_dqs_n[7:0]),
        .ddr3_dqs_p(DDR3_dqs_p[7:0]),
        .ddr3_odt(mig_7series_0_DDR3_ODT),
        .ddr3_ras_n(mig_7series_0_DDR3_RAS_N),
        .ddr3_reset_n(mig_7series_0_DDR3_RESET_N),
        .ddr3_we_n(mig_7series_0_DDR3_WE_N),
        .init_calib_complete(mig_7series_0_init_calib_complete),
        .mmcm_locked(mig_7series_0_mmcm_locked),
        .s_axi_araddr(axi_inter_con_0_M00_AXI_ARADDR[29:0]),
        .s_axi_arburst(axi_inter_con_0_M00_AXI_ARBURST),
        .s_axi_arcache(axi_inter_con_0_M00_AXI_ARCACHE),
        .s_axi_arid(axi_inter_con_0_M00_AXI_ARID),
        .s_axi_arlen(axi_inter_con_0_M00_AXI_ARLEN),
        .s_axi_arlock(axi_inter_con_0_M00_AXI_ARLOCK),
        .s_axi_arprot(axi_inter_con_0_M00_AXI_ARPROT),
        .s_axi_arqos(axi_inter_con_0_M00_AXI_ARQOS),
        .s_axi_arready(axi_inter_con_0_M00_AXI_ARREADY),
        .s_axi_arsize(axi_inter_con_0_M00_AXI_ARSIZE),
        .s_axi_arvalid(axi_inter_con_0_M00_AXI_ARVALID),
        .s_axi_awaddr(axi_inter_con_0_M00_AXI_AWADDR[29:0]),
        .s_axi_awburst(axi_inter_con_0_M00_AXI_AWBURST),
        .s_axi_awcache(axi_inter_con_0_M00_AXI_AWCACHE),
        .s_axi_awid(axi_inter_con_0_M00_AXI_AWID),
        .s_axi_awlen(axi_inter_con_0_M00_AXI_AWLEN),
        .s_axi_awlock(axi_inter_con_0_M00_AXI_AWLOCK),
        .s_axi_awprot(axi_inter_con_0_M00_AXI_AWPROT),
        .s_axi_awqos(axi_inter_con_0_M00_AXI_AWQOS),
        .s_axi_awready(axi_inter_con_0_M00_AXI_AWREADY),
        .s_axi_awsize(axi_inter_con_0_M00_AXI_AWSIZE),
        .s_axi_awvalid(axi_inter_con_0_M00_AXI_AWVALID),
        .s_axi_bid(axi_inter_con_0_M00_AXI_BID),
        .s_axi_bready(axi_inter_con_0_M00_AXI_BREADY),
        .s_axi_bresp(axi_inter_con_0_M00_AXI_BRESP),
        .s_axi_bvalid(axi_inter_con_0_M00_AXI_BVALID),
        .s_axi_rdata(axi_inter_con_0_M00_AXI_RDATA),
        .s_axi_rid(axi_inter_con_0_M00_AXI_RID),
        .s_axi_rlast(axi_inter_con_0_M00_AXI_RLAST),
        .s_axi_rready(axi_inter_con_0_M00_AXI_RREADY),
        .s_axi_rresp(axi_inter_con_0_M00_AXI_RRESP),
        .s_axi_rvalid(axi_inter_con_0_M00_AXI_RVALID),
        .s_axi_wdata(axi_inter_con_0_M00_AXI_WDATA),
        .s_axi_wlast(axi_inter_con_0_M00_AXI_WLAST),
        .s_axi_wready(axi_inter_con_0_M00_AXI_WREADY),
        .s_axi_wstrb(axi_inter_con_0_M00_AXI_WSTRB),
        .s_axi_wvalid(axi_inter_con_0_M00_AXI_WVALID),
        .sys_clk_n(SYS_CLK_1_CLK_N),
        .sys_clk_p(SYS_CLK_1_CLK_P),
        .sys_rst(rst_processing_system7_0_125M_bus_struct_reset),
        .ui_clk(mig_7series_0_ui_clk),
        .ui_clk_sync_rst(mig_7series_0_ui_clk_sync_rst));
zynq_design_1_mig_setup_reset_0_0 mig_setup_reset_0
       (.areset_n(mig_setup_reset_0_areset_n),
        .clk(mig_7series_0_ui_clk),
        .design_reset(mig_setup_reset_0_design_reset),
        .design_reset_n(mig_setup_reset_0_design_reset_n),
        .init_calib_complete(mig_7series_0_init_calib_complete),
        .mmcm_locked(mig_7series_0_mmcm_locked),
        .rst(mig_7series_0_ui_clk_sync_rst),
        .up_stream_reset(mig_setup_reset_0_up_stream_reset));
(* BMM_INFO_PROCESSOR = "ARM > zynq_design_1 buffer_descriptor_memory" *) 
   (* KEEP_HIERARCHY = "yes" *) 
   zynq_design_1_processing_system7_0_0 processing_system7_0
       (.DDR_Addr(DDR_addr[14:0]),
        .DDR_BankAddr(DDR_ba[2:0]),
        .DDR_CAS_n(DDR_cas_n),
        .DDR_CKE(DDR_cke),
        .DDR_CS_n(DDR_cs_n),
        .DDR_Clk(DDR_ck_p),
        .DDR_Clk_n(DDR_ck_n),
        .DDR_DM(DDR_dm[3:0]),
        .DDR_DQ(DDR_dq[31:0]),
        .DDR_DQS(DDR_dqs_p[3:0]),
        .DDR_DQS_n(DDR_dqs_n[3:0]),
        .DDR_DRSTB(DDR_reset_n),
        .DDR_ODT(DDR_odt),
        .DDR_RAS_n(DDR_ras_n),
        .DDR_VRN(FIXED_IO_ddr_vrn),
        .DDR_VRP(FIXED_IO_ddr_vrp),
        .DDR_WEB(DDR_we_n),
        .FCLK_CLK0(processing_system7_0_FCLK_CLK0),
        .FCLK_RESET0_N(processing_system7_0_FCLK_RESET0_N),
        .MIO(FIXED_IO_mio[53:0]),
        .M_AXI_GP0_ACLK(processing_system7_0_FCLK_CLK0),
        .M_AXI_GP0_ARADDR(processing_system7_0_M_AXI_GP0_ARADDR),
        .M_AXI_GP0_ARBURST(processing_system7_0_M_AXI_GP0_ARBURST),
        .M_AXI_GP0_ARCACHE(processing_system7_0_M_AXI_GP0_ARCACHE),
        .M_AXI_GP0_ARID(processing_system7_0_M_AXI_GP0_ARID),
        .M_AXI_GP0_ARLEN(processing_system7_0_M_AXI_GP0_ARLEN),
        .M_AXI_GP0_ARLOCK(processing_system7_0_M_AXI_GP0_ARLOCK),
        .M_AXI_GP0_ARPROT(processing_system7_0_M_AXI_GP0_ARPROT),
        .M_AXI_GP0_ARQOS(processing_system7_0_M_AXI_GP0_ARQOS),
        .M_AXI_GP0_ARREADY(processing_system7_0_M_AXI_GP0_ARREADY),
        .M_AXI_GP0_ARSIZE(processing_system7_0_M_AXI_GP0_ARSIZE),
        .M_AXI_GP0_ARVALID(processing_system7_0_M_AXI_GP0_ARVALID),
        .M_AXI_GP0_AWADDR(processing_system7_0_M_AXI_GP0_AWADDR),
        .M_AXI_GP0_AWBURST(processing_system7_0_M_AXI_GP0_AWBURST),
        .M_AXI_GP0_AWCACHE(processing_system7_0_M_AXI_GP0_AWCACHE),
        .M_AXI_GP0_AWID(processing_system7_0_M_AXI_GP0_AWID),
        .M_AXI_GP0_AWLEN(processing_system7_0_M_AXI_GP0_AWLEN),
        .M_AXI_GP0_AWLOCK(processing_system7_0_M_AXI_GP0_AWLOCK),
        .M_AXI_GP0_AWPROT(processing_system7_0_M_AXI_GP0_AWPROT),
        .M_AXI_GP0_AWQOS(processing_system7_0_M_AXI_GP0_AWQOS),
        .M_AXI_GP0_AWREADY(processing_system7_0_M_AXI_GP0_AWREADY),
        .M_AXI_GP0_AWSIZE(processing_system7_0_M_AXI_GP0_AWSIZE),
        .M_AXI_GP0_AWVALID(processing_system7_0_M_AXI_GP0_AWVALID),
        .M_AXI_GP0_BID(processing_system7_0_M_AXI_GP0_BID),
        .M_AXI_GP0_BREADY(processing_system7_0_M_AXI_GP0_BREADY),
        .M_AXI_GP0_BRESP(processing_system7_0_M_AXI_GP0_BRESP),
        .M_AXI_GP0_BVALID(processing_system7_0_M_AXI_GP0_BVALID),
        .M_AXI_GP0_RDATA(processing_system7_0_M_AXI_GP0_RDATA),
        .M_AXI_GP0_RID(processing_system7_0_M_AXI_GP0_RID),
        .M_AXI_GP0_RLAST(processing_system7_0_M_AXI_GP0_RLAST),
        .M_AXI_GP0_RREADY(processing_system7_0_M_AXI_GP0_RREADY),
        .M_AXI_GP0_RRESP(processing_system7_0_M_AXI_GP0_RRESP),
        .M_AXI_GP0_RVALID(processing_system7_0_M_AXI_GP0_RVALID),
        .M_AXI_GP0_WDATA(processing_system7_0_M_AXI_GP0_WDATA),
        .M_AXI_GP0_WID(processing_system7_0_M_AXI_GP0_WID),
        .M_AXI_GP0_WLAST(processing_system7_0_M_AXI_GP0_WLAST),
        .M_AXI_GP0_WREADY(processing_system7_0_M_AXI_GP0_WREADY),
        .M_AXI_GP0_WSTRB(processing_system7_0_M_AXI_GP0_WSTRB),
        .M_AXI_GP0_WVALID(processing_system7_0_M_AXI_GP0_WVALID),
        .M_AXI_GP1_ACLK(processing_system7_0_FCLK_CLK0),
        .M_AXI_GP1_ARADDR(processing_system7_0_M_AXI_GP1_ARADDR),
        .M_AXI_GP1_ARBURST(processing_system7_0_M_AXI_GP1_ARBURST),
        .M_AXI_GP1_ARCACHE(processing_system7_0_M_AXI_GP1_ARCACHE),
        .M_AXI_GP1_ARID(processing_system7_0_M_AXI_GP1_ARID),
        .M_AXI_GP1_ARLEN(processing_system7_0_M_AXI_GP1_ARLEN),
        .M_AXI_GP1_ARLOCK(processing_system7_0_M_AXI_GP1_ARLOCK),
        .M_AXI_GP1_ARPROT(processing_system7_0_M_AXI_GP1_ARPROT),
        .M_AXI_GP1_ARQOS(processing_system7_0_M_AXI_GP1_ARQOS),
        .M_AXI_GP1_ARREADY(processing_system7_0_M_AXI_GP1_ARREADY),
        .M_AXI_GP1_ARSIZE(processing_system7_0_M_AXI_GP1_ARSIZE),
        .M_AXI_GP1_ARVALID(processing_system7_0_M_AXI_GP1_ARVALID),
        .M_AXI_GP1_AWADDR(processing_system7_0_M_AXI_GP1_AWADDR),
        .M_AXI_GP1_AWBURST(processing_system7_0_M_AXI_GP1_AWBURST),
        .M_AXI_GP1_AWCACHE(processing_system7_0_M_AXI_GP1_AWCACHE),
        .M_AXI_GP1_AWID(processing_system7_0_M_AXI_GP1_AWID),
        .M_AXI_GP1_AWLEN(processing_system7_0_M_AXI_GP1_AWLEN),
        .M_AXI_GP1_AWLOCK(processing_system7_0_M_AXI_GP1_AWLOCK),
        .M_AXI_GP1_AWPROT(processing_system7_0_M_AXI_GP1_AWPROT),
        .M_AXI_GP1_AWQOS(processing_system7_0_M_AXI_GP1_AWQOS),
        .M_AXI_GP1_AWREADY(processing_system7_0_M_AXI_GP1_AWREADY),
        .M_AXI_GP1_AWSIZE(processing_system7_0_M_AXI_GP1_AWSIZE),
        .M_AXI_GP1_AWVALID(processing_system7_0_M_AXI_GP1_AWVALID),
        .M_AXI_GP1_BID(processing_system7_0_M_AXI_GP1_BID),
        .M_AXI_GP1_BREADY(processing_system7_0_M_AXI_GP1_BREADY),
        .M_AXI_GP1_BRESP(processing_system7_0_M_AXI_GP1_BRESP),
        .M_AXI_GP1_BVALID(processing_system7_0_M_AXI_GP1_BVALID),
        .M_AXI_GP1_RDATA(processing_system7_0_M_AXI_GP1_RDATA),
        .M_AXI_GP1_RID(processing_system7_0_M_AXI_GP1_RID),
        .M_AXI_GP1_RLAST(processing_system7_0_M_AXI_GP1_RLAST),
        .M_AXI_GP1_RREADY(processing_system7_0_M_AXI_GP1_RREADY),
        .M_AXI_GP1_RRESP(processing_system7_0_M_AXI_GP1_RRESP),
        .M_AXI_GP1_RVALID(processing_system7_0_M_AXI_GP1_RVALID),
        .M_AXI_GP1_WDATA(processing_system7_0_M_AXI_GP1_WDATA),
        .M_AXI_GP1_WID(processing_system7_0_M_AXI_GP1_WID),
        .M_AXI_GP1_WLAST(processing_system7_0_M_AXI_GP1_WLAST),
        .M_AXI_GP1_WREADY(processing_system7_0_M_AXI_GP1_WREADY),
        .M_AXI_GP1_WSTRB(processing_system7_0_M_AXI_GP1_WSTRB),
        .M_AXI_GP1_WVALID(processing_system7_0_M_AXI_GP1_WVALID),
        .PS_CLK(FIXED_IO_ps_clk),
        .PS_PORB(FIXED_IO_ps_porb),
        .PS_SRSTB(FIXED_IO_ps_srstb),
        .USB0_VBUS_PWRFAULT(GND_1));
zynq_design_1_rst_processing_system7_0_125M_0 rst_processing_system7_0_125M
       (.aux_reset_in(VCC_1),
        .bus_struct_reset(rst_processing_system7_0_125M_bus_struct_reset),
        .dcm_locked(VCC_1),
        .ext_reset_in(processing_system7_0_FCLK_RESET0_N),
        .interconnect_aresetn(rst_processing_system7_0_125M_interconnect_aresetn),
        .mb_debug_sys_rst(GND_1),
        .peripheral_aresetn(rst_processing_system7_0_125M_peripheral_aresetn),
        .slowest_sync_clk(processing_system7_0_FCLK_CLK0));
zynq_design_1_xlconstant_0_0 xlconstant_0
       (.dout(xlconstant_0_dout));
endmodule

module zynq_design_1_axi_mem_intercon_0
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arburst,
    M00_AXI_arcache,
    M00_AXI_arid,
    M00_AXI_arlen,
    M00_AXI_arlock,
    M00_AXI_arprot,
    M00_AXI_arready,
    M00_AXI_arsize,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awburst,
    M00_AXI_awcache,
    M00_AXI_awid,
    M00_AXI_awlen,
    M00_AXI_awlock,
    M00_AXI_awprot,
    M00_AXI_awready,
    M00_AXI_awsize,
    M00_AXI_awvalid,
    M00_AXI_bid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rid,
    M00_AXI_rlast,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wlast,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arburst,
    S00_AXI_arcache,
    S00_AXI_arid,
    S00_AXI_arlen,
    S00_AXI_arlock,
    S00_AXI_arprot,
    S00_AXI_arqos,
    S00_AXI_arready,
    S00_AXI_arsize,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awburst,
    S00_AXI_awcache,
    S00_AXI_awid,
    S00_AXI_awlen,
    S00_AXI_awlock,
    S00_AXI_awprot,
    S00_AXI_awqos,
    S00_AXI_awready,
    S00_AXI_awsize,
    S00_AXI_awvalid,
    S00_AXI_bid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rid,
    S00_AXI_rlast,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wid,
    S00_AXI_wlast,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid);
  input ACLK;
  input [0:0]ARESETN;
  input M00_ACLK;
  input [0:0]M00_ARESETN;
  output [15:0]M00_AXI_araddr;
  output [1:0]M00_AXI_arburst;
  output [3:0]M00_AXI_arcache;
  output [11:0]M00_AXI_arid;
  output [7:0]M00_AXI_arlen;
  output M00_AXI_arlock;
  output [2:0]M00_AXI_arprot;
  input M00_AXI_arready;
  output [2:0]M00_AXI_arsize;
  output M00_AXI_arvalid;
  output [15:0]M00_AXI_awaddr;
  output [1:0]M00_AXI_awburst;
  output [3:0]M00_AXI_awcache;
  output [11:0]M00_AXI_awid;
  output [7:0]M00_AXI_awlen;
  output M00_AXI_awlock;
  output [2:0]M00_AXI_awprot;
  input M00_AXI_awready;
  output [2:0]M00_AXI_awsize;
  output M00_AXI_awvalid;
  input [11:0]M00_AXI_bid;
  output M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  input [11:0]M00_AXI_rid;
  input M00_AXI_rlast;
  output M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  output M00_AXI_wlast;
  input M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output M00_AXI_wvalid;
  input S00_ACLK;
  input [0:0]S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [1:0]S00_AXI_arburst;
  input [3:0]S00_AXI_arcache;
  input [11:0]S00_AXI_arid;
  input [3:0]S00_AXI_arlen;
  input [1:0]S00_AXI_arlock;
  input [2:0]S00_AXI_arprot;
  input [3:0]S00_AXI_arqos;
  output S00_AXI_arready;
  input [2:0]S00_AXI_arsize;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [1:0]S00_AXI_awburst;
  input [3:0]S00_AXI_awcache;
  input [11:0]S00_AXI_awid;
  input [3:0]S00_AXI_awlen;
  input [1:0]S00_AXI_awlock;
  input [2:0]S00_AXI_awprot;
  input [3:0]S00_AXI_awqos;
  output S00_AXI_awready;
  input [2:0]S00_AXI_awsize;
  input S00_AXI_awvalid;
  output [11:0]S00_AXI_bid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  output [11:0]S00_AXI_rid;
  output S00_AXI_rlast;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  input [11:0]S00_AXI_wid;
  input S00_AXI_wlast;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;

  wire S00_ACLK_1;
  wire [0:0]S00_ARESETN_1;
  wire axi_mem_intercon_ACLK_net;
  wire [0:0]axi_mem_intercon_ARESETN_net;
  wire [31:0]axi_mem_intercon_to_s00_couplers_ARADDR;
  wire [1:0]axi_mem_intercon_to_s00_couplers_ARBURST;
  wire [3:0]axi_mem_intercon_to_s00_couplers_ARCACHE;
  wire [11:0]axi_mem_intercon_to_s00_couplers_ARID;
  wire [3:0]axi_mem_intercon_to_s00_couplers_ARLEN;
  wire [1:0]axi_mem_intercon_to_s00_couplers_ARLOCK;
  wire [2:0]axi_mem_intercon_to_s00_couplers_ARPROT;
  wire [3:0]axi_mem_intercon_to_s00_couplers_ARQOS;
  wire axi_mem_intercon_to_s00_couplers_ARREADY;
  wire [2:0]axi_mem_intercon_to_s00_couplers_ARSIZE;
  wire axi_mem_intercon_to_s00_couplers_ARVALID;
  wire [31:0]axi_mem_intercon_to_s00_couplers_AWADDR;
  wire [1:0]axi_mem_intercon_to_s00_couplers_AWBURST;
  wire [3:0]axi_mem_intercon_to_s00_couplers_AWCACHE;
  wire [11:0]axi_mem_intercon_to_s00_couplers_AWID;
  wire [3:0]axi_mem_intercon_to_s00_couplers_AWLEN;
  wire [1:0]axi_mem_intercon_to_s00_couplers_AWLOCK;
  wire [2:0]axi_mem_intercon_to_s00_couplers_AWPROT;
  wire [3:0]axi_mem_intercon_to_s00_couplers_AWQOS;
  wire axi_mem_intercon_to_s00_couplers_AWREADY;
  wire [2:0]axi_mem_intercon_to_s00_couplers_AWSIZE;
  wire axi_mem_intercon_to_s00_couplers_AWVALID;
  wire [11:0]axi_mem_intercon_to_s00_couplers_BID;
  wire axi_mem_intercon_to_s00_couplers_BREADY;
  wire [1:0]axi_mem_intercon_to_s00_couplers_BRESP;
  wire axi_mem_intercon_to_s00_couplers_BVALID;
  wire [31:0]axi_mem_intercon_to_s00_couplers_RDATA;
  wire [11:0]axi_mem_intercon_to_s00_couplers_RID;
  wire axi_mem_intercon_to_s00_couplers_RLAST;
  wire axi_mem_intercon_to_s00_couplers_RREADY;
  wire [1:0]axi_mem_intercon_to_s00_couplers_RRESP;
  wire axi_mem_intercon_to_s00_couplers_RVALID;
  wire [31:0]axi_mem_intercon_to_s00_couplers_WDATA;
  wire [11:0]axi_mem_intercon_to_s00_couplers_WID;
  wire axi_mem_intercon_to_s00_couplers_WLAST;
  wire axi_mem_intercon_to_s00_couplers_WREADY;
  wire [3:0]axi_mem_intercon_to_s00_couplers_WSTRB;
  wire axi_mem_intercon_to_s00_couplers_WVALID;
  wire [15:0]s00_couplers_to_axi_mem_intercon_ARADDR;
  wire [1:0]s00_couplers_to_axi_mem_intercon_ARBURST;
  wire [3:0]s00_couplers_to_axi_mem_intercon_ARCACHE;
  wire [11:0]s00_couplers_to_axi_mem_intercon_ARID;
  wire [7:0]s00_couplers_to_axi_mem_intercon_ARLEN;
  wire s00_couplers_to_axi_mem_intercon_ARLOCK;
  wire [2:0]s00_couplers_to_axi_mem_intercon_ARPROT;
  wire s00_couplers_to_axi_mem_intercon_ARREADY;
  wire [2:0]s00_couplers_to_axi_mem_intercon_ARSIZE;
  wire s00_couplers_to_axi_mem_intercon_ARVALID;
  wire [15:0]s00_couplers_to_axi_mem_intercon_AWADDR;
  wire [1:0]s00_couplers_to_axi_mem_intercon_AWBURST;
  wire [3:0]s00_couplers_to_axi_mem_intercon_AWCACHE;
  wire [11:0]s00_couplers_to_axi_mem_intercon_AWID;
  wire [7:0]s00_couplers_to_axi_mem_intercon_AWLEN;
  wire s00_couplers_to_axi_mem_intercon_AWLOCK;
  wire [2:0]s00_couplers_to_axi_mem_intercon_AWPROT;
  wire s00_couplers_to_axi_mem_intercon_AWREADY;
  wire [2:0]s00_couplers_to_axi_mem_intercon_AWSIZE;
  wire s00_couplers_to_axi_mem_intercon_AWVALID;
  wire [11:0]s00_couplers_to_axi_mem_intercon_BID;
  wire s00_couplers_to_axi_mem_intercon_BREADY;
  wire [1:0]s00_couplers_to_axi_mem_intercon_BRESP;
  wire s00_couplers_to_axi_mem_intercon_BVALID;
  wire [31:0]s00_couplers_to_axi_mem_intercon_RDATA;
  wire [11:0]s00_couplers_to_axi_mem_intercon_RID;
  wire s00_couplers_to_axi_mem_intercon_RLAST;
  wire s00_couplers_to_axi_mem_intercon_RREADY;
  wire [1:0]s00_couplers_to_axi_mem_intercon_RRESP;
  wire s00_couplers_to_axi_mem_intercon_RVALID;
  wire [31:0]s00_couplers_to_axi_mem_intercon_WDATA;
  wire s00_couplers_to_axi_mem_intercon_WLAST;
  wire s00_couplers_to_axi_mem_intercon_WREADY;
  wire [3:0]s00_couplers_to_axi_mem_intercon_WSTRB;
  wire s00_couplers_to_axi_mem_intercon_WVALID;

  assign M00_AXI_araddr[15:0] = s00_couplers_to_axi_mem_intercon_ARADDR;
  assign M00_AXI_arburst[1:0] = s00_couplers_to_axi_mem_intercon_ARBURST;
  assign M00_AXI_arcache[3:0] = s00_couplers_to_axi_mem_intercon_ARCACHE;
  assign M00_AXI_arid[11:0] = s00_couplers_to_axi_mem_intercon_ARID;
  assign M00_AXI_arlen[7:0] = s00_couplers_to_axi_mem_intercon_ARLEN;
  assign M00_AXI_arlock = s00_couplers_to_axi_mem_intercon_ARLOCK;
  assign M00_AXI_arprot[2:0] = s00_couplers_to_axi_mem_intercon_ARPROT;
  assign M00_AXI_arsize[2:0] = s00_couplers_to_axi_mem_intercon_ARSIZE;
  assign M00_AXI_arvalid = s00_couplers_to_axi_mem_intercon_ARVALID;
  assign M00_AXI_awaddr[15:0] = s00_couplers_to_axi_mem_intercon_AWADDR;
  assign M00_AXI_awburst[1:0] = s00_couplers_to_axi_mem_intercon_AWBURST;
  assign M00_AXI_awcache[3:0] = s00_couplers_to_axi_mem_intercon_AWCACHE;
  assign M00_AXI_awid[11:0] = s00_couplers_to_axi_mem_intercon_AWID;
  assign M00_AXI_awlen[7:0] = s00_couplers_to_axi_mem_intercon_AWLEN;
  assign M00_AXI_awlock = s00_couplers_to_axi_mem_intercon_AWLOCK;
  assign M00_AXI_awprot[2:0] = s00_couplers_to_axi_mem_intercon_AWPROT;
  assign M00_AXI_awsize[2:0] = s00_couplers_to_axi_mem_intercon_AWSIZE;
  assign M00_AXI_awvalid = s00_couplers_to_axi_mem_intercon_AWVALID;
  assign M00_AXI_bready = s00_couplers_to_axi_mem_intercon_BREADY;
  assign M00_AXI_rready = s00_couplers_to_axi_mem_intercon_RREADY;
  assign M00_AXI_wdata[31:0] = s00_couplers_to_axi_mem_intercon_WDATA;
  assign M00_AXI_wlast = s00_couplers_to_axi_mem_intercon_WLAST;
  assign M00_AXI_wstrb[3:0] = s00_couplers_to_axi_mem_intercon_WSTRB;
  assign M00_AXI_wvalid = s00_couplers_to_axi_mem_intercon_WVALID;
  assign S00_ACLK_1 = S00_ACLK;
  assign S00_ARESETN_1 = S00_ARESETN[0];
  assign S00_AXI_arready = axi_mem_intercon_to_s00_couplers_ARREADY;
  assign S00_AXI_awready = axi_mem_intercon_to_s00_couplers_AWREADY;
  assign S00_AXI_bid[11:0] = axi_mem_intercon_to_s00_couplers_BID;
  assign S00_AXI_bresp[1:0] = axi_mem_intercon_to_s00_couplers_BRESP;
  assign S00_AXI_bvalid = axi_mem_intercon_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = axi_mem_intercon_to_s00_couplers_RDATA;
  assign S00_AXI_rid[11:0] = axi_mem_intercon_to_s00_couplers_RID;
  assign S00_AXI_rlast = axi_mem_intercon_to_s00_couplers_RLAST;
  assign S00_AXI_rresp[1:0] = axi_mem_intercon_to_s00_couplers_RRESP;
  assign S00_AXI_rvalid = axi_mem_intercon_to_s00_couplers_RVALID;
  assign S00_AXI_wready = axi_mem_intercon_to_s00_couplers_WREADY;
  assign axi_mem_intercon_ACLK_net = M00_ACLK;
  assign axi_mem_intercon_ARESETN_net = M00_ARESETN[0];
  assign axi_mem_intercon_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign axi_mem_intercon_to_s00_couplers_ARBURST = S00_AXI_arburst[1:0];
  assign axi_mem_intercon_to_s00_couplers_ARCACHE = S00_AXI_arcache[3:0];
  assign axi_mem_intercon_to_s00_couplers_ARID = S00_AXI_arid[11:0];
  assign axi_mem_intercon_to_s00_couplers_ARLEN = S00_AXI_arlen[3:0];
  assign axi_mem_intercon_to_s00_couplers_ARLOCK = S00_AXI_arlock[1:0];
  assign axi_mem_intercon_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign axi_mem_intercon_to_s00_couplers_ARQOS = S00_AXI_arqos[3:0];
  assign axi_mem_intercon_to_s00_couplers_ARSIZE = S00_AXI_arsize[2:0];
  assign axi_mem_intercon_to_s00_couplers_ARVALID = S00_AXI_arvalid;
  assign axi_mem_intercon_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign axi_mem_intercon_to_s00_couplers_AWBURST = S00_AXI_awburst[1:0];
  assign axi_mem_intercon_to_s00_couplers_AWCACHE = S00_AXI_awcache[3:0];
  assign axi_mem_intercon_to_s00_couplers_AWID = S00_AXI_awid[11:0];
  assign axi_mem_intercon_to_s00_couplers_AWLEN = S00_AXI_awlen[3:0];
  assign axi_mem_intercon_to_s00_couplers_AWLOCK = S00_AXI_awlock[1:0];
  assign axi_mem_intercon_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign axi_mem_intercon_to_s00_couplers_AWQOS = S00_AXI_awqos[3:0];
  assign axi_mem_intercon_to_s00_couplers_AWSIZE = S00_AXI_awsize[2:0];
  assign axi_mem_intercon_to_s00_couplers_AWVALID = S00_AXI_awvalid;
  assign axi_mem_intercon_to_s00_couplers_BREADY = S00_AXI_bready;
  assign axi_mem_intercon_to_s00_couplers_RREADY = S00_AXI_rready;
  assign axi_mem_intercon_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign axi_mem_intercon_to_s00_couplers_WID = S00_AXI_wid[11:0];
  assign axi_mem_intercon_to_s00_couplers_WLAST = S00_AXI_wlast;
  assign axi_mem_intercon_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign axi_mem_intercon_to_s00_couplers_WVALID = S00_AXI_wvalid;
  assign s00_couplers_to_axi_mem_intercon_ARREADY = M00_AXI_arready;
  assign s00_couplers_to_axi_mem_intercon_AWREADY = M00_AXI_awready;
  assign s00_couplers_to_axi_mem_intercon_BID = M00_AXI_bid[11:0];
  assign s00_couplers_to_axi_mem_intercon_BRESP = M00_AXI_bresp[1:0];
  assign s00_couplers_to_axi_mem_intercon_BVALID = M00_AXI_bvalid;
  assign s00_couplers_to_axi_mem_intercon_RDATA = M00_AXI_rdata[31:0];
  assign s00_couplers_to_axi_mem_intercon_RID = M00_AXI_rid[11:0];
  assign s00_couplers_to_axi_mem_intercon_RLAST = M00_AXI_rlast;
  assign s00_couplers_to_axi_mem_intercon_RRESP = M00_AXI_rresp[1:0];
  assign s00_couplers_to_axi_mem_intercon_RVALID = M00_AXI_rvalid;
  assign s00_couplers_to_axi_mem_intercon_WREADY = M00_AXI_wready;
s00_couplers_imp_5VZGPS s00_couplers
       (.M_ACLK(axi_mem_intercon_ACLK_net),
        .M_ARESETN(axi_mem_intercon_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_axi_mem_intercon_ARADDR),
        .M_AXI_arburst(s00_couplers_to_axi_mem_intercon_ARBURST),
        .M_AXI_arcache(s00_couplers_to_axi_mem_intercon_ARCACHE),
        .M_AXI_arid(s00_couplers_to_axi_mem_intercon_ARID),
        .M_AXI_arlen(s00_couplers_to_axi_mem_intercon_ARLEN),
        .M_AXI_arlock(s00_couplers_to_axi_mem_intercon_ARLOCK),
        .M_AXI_arprot(s00_couplers_to_axi_mem_intercon_ARPROT),
        .M_AXI_arready(s00_couplers_to_axi_mem_intercon_ARREADY),
        .M_AXI_arsize(s00_couplers_to_axi_mem_intercon_ARSIZE),
        .M_AXI_arvalid(s00_couplers_to_axi_mem_intercon_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_axi_mem_intercon_AWADDR),
        .M_AXI_awburst(s00_couplers_to_axi_mem_intercon_AWBURST),
        .M_AXI_awcache(s00_couplers_to_axi_mem_intercon_AWCACHE),
        .M_AXI_awid(s00_couplers_to_axi_mem_intercon_AWID),
        .M_AXI_awlen(s00_couplers_to_axi_mem_intercon_AWLEN),
        .M_AXI_awlock(s00_couplers_to_axi_mem_intercon_AWLOCK),
        .M_AXI_awprot(s00_couplers_to_axi_mem_intercon_AWPROT),
        .M_AXI_awready(s00_couplers_to_axi_mem_intercon_AWREADY),
        .M_AXI_awsize(s00_couplers_to_axi_mem_intercon_AWSIZE),
        .M_AXI_awvalid(s00_couplers_to_axi_mem_intercon_AWVALID),
        .M_AXI_bid(s00_couplers_to_axi_mem_intercon_BID),
        .M_AXI_bready(s00_couplers_to_axi_mem_intercon_BREADY),
        .M_AXI_bresp(s00_couplers_to_axi_mem_intercon_BRESP),
        .M_AXI_bvalid(s00_couplers_to_axi_mem_intercon_BVALID),
        .M_AXI_rdata(s00_couplers_to_axi_mem_intercon_RDATA),
        .M_AXI_rid(s00_couplers_to_axi_mem_intercon_RID),
        .M_AXI_rlast(s00_couplers_to_axi_mem_intercon_RLAST),
        .M_AXI_rready(s00_couplers_to_axi_mem_intercon_RREADY),
        .M_AXI_rresp(s00_couplers_to_axi_mem_intercon_RRESP),
        .M_AXI_rvalid(s00_couplers_to_axi_mem_intercon_RVALID),
        .M_AXI_wdata(s00_couplers_to_axi_mem_intercon_WDATA),
        .M_AXI_wlast(s00_couplers_to_axi_mem_intercon_WLAST),
        .M_AXI_wready(s00_couplers_to_axi_mem_intercon_WREADY),
        .M_AXI_wstrb(s00_couplers_to_axi_mem_intercon_WSTRB),
        .M_AXI_wvalid(s00_couplers_to_axi_mem_intercon_WVALID),
        .S_ACLK(S00_ACLK_1),
        .S_ARESETN(S00_ARESETN_1),
        .S_AXI_araddr(axi_mem_intercon_to_s00_couplers_ARADDR),
        .S_AXI_arburst(axi_mem_intercon_to_s00_couplers_ARBURST),
        .S_AXI_arcache(axi_mem_intercon_to_s00_couplers_ARCACHE),
        .S_AXI_arid(axi_mem_intercon_to_s00_couplers_ARID),
        .S_AXI_arlen(axi_mem_intercon_to_s00_couplers_ARLEN),
        .S_AXI_arlock(axi_mem_intercon_to_s00_couplers_ARLOCK),
        .S_AXI_arprot(axi_mem_intercon_to_s00_couplers_ARPROT),
        .S_AXI_arqos(axi_mem_intercon_to_s00_couplers_ARQOS),
        .S_AXI_arready(axi_mem_intercon_to_s00_couplers_ARREADY),
        .S_AXI_arsize(axi_mem_intercon_to_s00_couplers_ARSIZE),
        .S_AXI_arvalid(axi_mem_intercon_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(axi_mem_intercon_to_s00_couplers_AWADDR),
        .S_AXI_awburst(axi_mem_intercon_to_s00_couplers_AWBURST),
        .S_AXI_awcache(axi_mem_intercon_to_s00_couplers_AWCACHE),
        .S_AXI_awid(axi_mem_intercon_to_s00_couplers_AWID),
        .S_AXI_awlen(axi_mem_intercon_to_s00_couplers_AWLEN),
        .S_AXI_awlock(axi_mem_intercon_to_s00_couplers_AWLOCK),
        .S_AXI_awprot(axi_mem_intercon_to_s00_couplers_AWPROT),
        .S_AXI_awqos(axi_mem_intercon_to_s00_couplers_AWQOS),
        .S_AXI_awready(axi_mem_intercon_to_s00_couplers_AWREADY),
        .S_AXI_awsize(axi_mem_intercon_to_s00_couplers_AWSIZE),
        .S_AXI_awvalid(axi_mem_intercon_to_s00_couplers_AWVALID),
        .S_AXI_bid(axi_mem_intercon_to_s00_couplers_BID),
        .S_AXI_bready(axi_mem_intercon_to_s00_couplers_BREADY),
        .S_AXI_bresp(axi_mem_intercon_to_s00_couplers_BRESP),
        .S_AXI_bvalid(axi_mem_intercon_to_s00_couplers_BVALID),
        .S_AXI_rdata(axi_mem_intercon_to_s00_couplers_RDATA),
        .S_AXI_rid(axi_mem_intercon_to_s00_couplers_RID),
        .S_AXI_rlast(axi_mem_intercon_to_s00_couplers_RLAST),
        .S_AXI_rready(axi_mem_intercon_to_s00_couplers_RREADY),
        .S_AXI_rresp(axi_mem_intercon_to_s00_couplers_RRESP),
        .S_AXI_rvalid(axi_mem_intercon_to_s00_couplers_RVALID),
        .S_AXI_wdata(axi_mem_intercon_to_s00_couplers_WDATA),
        .S_AXI_wid(axi_mem_intercon_to_s00_couplers_WID),
        .S_AXI_wlast(axi_mem_intercon_to_s00_couplers_WLAST),
        .S_AXI_wready(axi_mem_intercon_to_s00_couplers_WREADY),
        .S_AXI_wstrb(axi_mem_intercon_to_s00_couplers_WSTRB),
        .S_AXI_wvalid(axi_mem_intercon_to_s00_couplers_WVALID));
endmodule

module zynq_design_1_axi_mem_intercon_1_0
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arburst,
    M00_AXI_arcache,
    M00_AXI_arid,
    M00_AXI_arlen,
    M00_AXI_arlock,
    M00_AXI_arprot,
    M00_AXI_arqos,
    M00_AXI_arready,
    M00_AXI_arsize,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awburst,
    M00_AXI_awcache,
    M00_AXI_awid,
    M00_AXI_awlen,
    M00_AXI_awlock,
    M00_AXI_awprot,
    M00_AXI_awqos,
    M00_AXI_awready,
    M00_AXI_awsize,
    M00_AXI_awvalid,
    M00_AXI_bid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rid,
    M00_AXI_rlast,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wlast,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arburst,
    S00_AXI_arcache,
    S00_AXI_arid,
    S00_AXI_arlen,
    S00_AXI_arlock,
    S00_AXI_arprot,
    S00_AXI_arqos,
    S00_AXI_arready,
    S00_AXI_arsize,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awburst,
    S00_AXI_awcache,
    S00_AXI_awid,
    S00_AXI_awlen,
    S00_AXI_awlock,
    S00_AXI_awprot,
    S00_AXI_awqos,
    S00_AXI_awready,
    S00_AXI_awsize,
    S00_AXI_awvalid,
    S00_AXI_bid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rid,
    S00_AXI_rlast,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wid,
    S00_AXI_wlast,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid);
  input ACLK;
  input [0:0]ARESETN;
  input M00_ACLK;
  input [0:0]M00_ARESETN;
  output [15:0]M00_AXI_araddr;
  output [1:0]M00_AXI_arburst;
  output [3:0]M00_AXI_arcache;
  output [11:0]M00_AXI_arid;
  output [7:0]M00_AXI_arlen;
  output M00_AXI_arlock;
  output [2:0]M00_AXI_arprot;
  output [3:0]M00_AXI_arqos;
  input M00_AXI_arready;
  output [2:0]M00_AXI_arsize;
  output M00_AXI_arvalid;
  output [15:0]M00_AXI_awaddr;
  output [1:0]M00_AXI_awburst;
  output [3:0]M00_AXI_awcache;
  output [11:0]M00_AXI_awid;
  output [7:0]M00_AXI_awlen;
  output M00_AXI_awlock;
  output [2:0]M00_AXI_awprot;
  output [3:0]M00_AXI_awqos;
  input M00_AXI_awready;
  output [2:0]M00_AXI_awsize;
  output M00_AXI_awvalid;
  input [11:0]M00_AXI_bid;
  output M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  input [11:0]M00_AXI_rid;
  input M00_AXI_rlast;
  output M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  output M00_AXI_wlast;
  input M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output M00_AXI_wvalid;
  input S00_ACLK;
  input [0:0]S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [1:0]S00_AXI_arburst;
  input [3:0]S00_AXI_arcache;
  input [11:0]S00_AXI_arid;
  input [3:0]S00_AXI_arlen;
  input [1:0]S00_AXI_arlock;
  input [2:0]S00_AXI_arprot;
  input [3:0]S00_AXI_arqos;
  output S00_AXI_arready;
  input [2:0]S00_AXI_arsize;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [1:0]S00_AXI_awburst;
  input [3:0]S00_AXI_awcache;
  input [11:0]S00_AXI_awid;
  input [3:0]S00_AXI_awlen;
  input [1:0]S00_AXI_awlock;
  input [2:0]S00_AXI_awprot;
  input [3:0]S00_AXI_awqos;
  output S00_AXI_awready;
  input [2:0]S00_AXI_awsize;
  input S00_AXI_awvalid;
  output [11:0]S00_AXI_bid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  output [11:0]S00_AXI_rid;
  output S00_AXI_rlast;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  input [11:0]S00_AXI_wid;
  input S00_AXI_wlast;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;

  wire S00_ACLK_1;
  wire [0:0]S00_ARESETN_1;
  wire axi_mem_intercon_1_ACLK_net;
  wire [0:0]axi_mem_intercon_1_ARESETN_net;
  wire [31:0]axi_mem_intercon_1_to_s00_couplers_ARADDR;
  wire [1:0]axi_mem_intercon_1_to_s00_couplers_ARBURST;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_ARCACHE;
  wire [11:0]axi_mem_intercon_1_to_s00_couplers_ARID;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_ARLEN;
  wire [1:0]axi_mem_intercon_1_to_s00_couplers_ARLOCK;
  wire [2:0]axi_mem_intercon_1_to_s00_couplers_ARPROT;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_ARQOS;
  wire axi_mem_intercon_1_to_s00_couplers_ARREADY;
  wire [2:0]axi_mem_intercon_1_to_s00_couplers_ARSIZE;
  wire axi_mem_intercon_1_to_s00_couplers_ARVALID;
  wire [31:0]axi_mem_intercon_1_to_s00_couplers_AWADDR;
  wire [1:0]axi_mem_intercon_1_to_s00_couplers_AWBURST;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_AWCACHE;
  wire [11:0]axi_mem_intercon_1_to_s00_couplers_AWID;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_AWLEN;
  wire [1:0]axi_mem_intercon_1_to_s00_couplers_AWLOCK;
  wire [2:0]axi_mem_intercon_1_to_s00_couplers_AWPROT;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_AWQOS;
  wire axi_mem_intercon_1_to_s00_couplers_AWREADY;
  wire [2:0]axi_mem_intercon_1_to_s00_couplers_AWSIZE;
  wire axi_mem_intercon_1_to_s00_couplers_AWVALID;
  wire [11:0]axi_mem_intercon_1_to_s00_couplers_BID;
  wire axi_mem_intercon_1_to_s00_couplers_BREADY;
  wire [1:0]axi_mem_intercon_1_to_s00_couplers_BRESP;
  wire axi_mem_intercon_1_to_s00_couplers_BVALID;
  wire [31:0]axi_mem_intercon_1_to_s00_couplers_RDATA;
  wire [11:0]axi_mem_intercon_1_to_s00_couplers_RID;
  wire axi_mem_intercon_1_to_s00_couplers_RLAST;
  wire axi_mem_intercon_1_to_s00_couplers_RREADY;
  wire [1:0]axi_mem_intercon_1_to_s00_couplers_RRESP;
  wire axi_mem_intercon_1_to_s00_couplers_RVALID;
  wire [31:0]axi_mem_intercon_1_to_s00_couplers_WDATA;
  wire [11:0]axi_mem_intercon_1_to_s00_couplers_WID;
  wire axi_mem_intercon_1_to_s00_couplers_WLAST;
  wire axi_mem_intercon_1_to_s00_couplers_WREADY;
  wire [3:0]axi_mem_intercon_1_to_s00_couplers_WSTRB;
  wire axi_mem_intercon_1_to_s00_couplers_WVALID;
  wire [15:0]s00_couplers_to_axi_mem_intercon_1_ARADDR;
  wire [1:0]s00_couplers_to_axi_mem_intercon_1_ARBURST;
  wire [3:0]s00_couplers_to_axi_mem_intercon_1_ARCACHE;
  wire [11:0]s00_couplers_to_axi_mem_intercon_1_ARID;
  wire [7:0]s00_couplers_to_axi_mem_intercon_1_ARLEN;
  wire s00_couplers_to_axi_mem_intercon_1_ARLOCK;
  wire [2:0]s00_couplers_to_axi_mem_intercon_1_ARPROT;
  wire [3:0]s00_couplers_to_axi_mem_intercon_1_ARQOS;
  wire s00_couplers_to_axi_mem_intercon_1_ARREADY;
  wire [2:0]s00_couplers_to_axi_mem_intercon_1_ARSIZE;
  wire s00_couplers_to_axi_mem_intercon_1_ARVALID;
  wire [15:0]s00_couplers_to_axi_mem_intercon_1_AWADDR;
  wire [1:0]s00_couplers_to_axi_mem_intercon_1_AWBURST;
  wire [3:0]s00_couplers_to_axi_mem_intercon_1_AWCACHE;
  wire [11:0]s00_couplers_to_axi_mem_intercon_1_AWID;
  wire [7:0]s00_couplers_to_axi_mem_intercon_1_AWLEN;
  wire s00_couplers_to_axi_mem_intercon_1_AWLOCK;
  wire [2:0]s00_couplers_to_axi_mem_intercon_1_AWPROT;
  wire [3:0]s00_couplers_to_axi_mem_intercon_1_AWQOS;
  wire s00_couplers_to_axi_mem_intercon_1_AWREADY;
  wire [2:0]s00_couplers_to_axi_mem_intercon_1_AWSIZE;
  wire s00_couplers_to_axi_mem_intercon_1_AWVALID;
  wire [11:0]s00_couplers_to_axi_mem_intercon_1_BID;
  wire s00_couplers_to_axi_mem_intercon_1_BREADY;
  wire [1:0]s00_couplers_to_axi_mem_intercon_1_BRESP;
  wire s00_couplers_to_axi_mem_intercon_1_BVALID;
  wire [31:0]s00_couplers_to_axi_mem_intercon_1_RDATA;
  wire [11:0]s00_couplers_to_axi_mem_intercon_1_RID;
  wire s00_couplers_to_axi_mem_intercon_1_RLAST;
  wire s00_couplers_to_axi_mem_intercon_1_RREADY;
  wire [1:0]s00_couplers_to_axi_mem_intercon_1_RRESP;
  wire s00_couplers_to_axi_mem_intercon_1_RVALID;
  wire [31:0]s00_couplers_to_axi_mem_intercon_1_WDATA;
  wire s00_couplers_to_axi_mem_intercon_1_WLAST;
  wire s00_couplers_to_axi_mem_intercon_1_WREADY;
  wire [3:0]s00_couplers_to_axi_mem_intercon_1_WSTRB;
  wire s00_couplers_to_axi_mem_intercon_1_WVALID;

  assign M00_AXI_araddr[15:0] = s00_couplers_to_axi_mem_intercon_1_ARADDR;
  assign M00_AXI_arburst[1:0] = s00_couplers_to_axi_mem_intercon_1_ARBURST;
  assign M00_AXI_arcache[3:0] = s00_couplers_to_axi_mem_intercon_1_ARCACHE;
  assign M00_AXI_arid[11:0] = s00_couplers_to_axi_mem_intercon_1_ARID;
  assign M00_AXI_arlen[7:0] = s00_couplers_to_axi_mem_intercon_1_ARLEN;
  assign M00_AXI_arlock = s00_couplers_to_axi_mem_intercon_1_ARLOCK;
  assign M00_AXI_arprot[2:0] = s00_couplers_to_axi_mem_intercon_1_ARPROT;
  assign M00_AXI_arqos[3:0] = s00_couplers_to_axi_mem_intercon_1_ARQOS;
  assign M00_AXI_arsize[2:0] = s00_couplers_to_axi_mem_intercon_1_ARSIZE;
  assign M00_AXI_arvalid = s00_couplers_to_axi_mem_intercon_1_ARVALID;
  assign M00_AXI_awaddr[15:0] = s00_couplers_to_axi_mem_intercon_1_AWADDR;
  assign M00_AXI_awburst[1:0] = s00_couplers_to_axi_mem_intercon_1_AWBURST;
  assign M00_AXI_awcache[3:0] = s00_couplers_to_axi_mem_intercon_1_AWCACHE;
  assign M00_AXI_awid[11:0] = s00_couplers_to_axi_mem_intercon_1_AWID;
  assign M00_AXI_awlen[7:0] = s00_couplers_to_axi_mem_intercon_1_AWLEN;
  assign M00_AXI_awlock = s00_couplers_to_axi_mem_intercon_1_AWLOCK;
  assign M00_AXI_awprot[2:0] = s00_couplers_to_axi_mem_intercon_1_AWPROT;
  assign M00_AXI_awqos[3:0] = s00_couplers_to_axi_mem_intercon_1_AWQOS;
  assign M00_AXI_awsize[2:0] = s00_couplers_to_axi_mem_intercon_1_AWSIZE;
  assign M00_AXI_awvalid = s00_couplers_to_axi_mem_intercon_1_AWVALID;
  assign M00_AXI_bready = s00_couplers_to_axi_mem_intercon_1_BREADY;
  assign M00_AXI_rready = s00_couplers_to_axi_mem_intercon_1_RREADY;
  assign M00_AXI_wdata[31:0] = s00_couplers_to_axi_mem_intercon_1_WDATA;
  assign M00_AXI_wlast = s00_couplers_to_axi_mem_intercon_1_WLAST;
  assign M00_AXI_wstrb[3:0] = s00_couplers_to_axi_mem_intercon_1_WSTRB;
  assign M00_AXI_wvalid = s00_couplers_to_axi_mem_intercon_1_WVALID;
  assign S00_ACLK_1 = S00_ACLK;
  assign S00_ARESETN_1 = S00_ARESETN[0];
  assign S00_AXI_arready = axi_mem_intercon_1_to_s00_couplers_ARREADY;
  assign S00_AXI_awready = axi_mem_intercon_1_to_s00_couplers_AWREADY;
  assign S00_AXI_bid[11:0] = axi_mem_intercon_1_to_s00_couplers_BID;
  assign S00_AXI_bresp[1:0] = axi_mem_intercon_1_to_s00_couplers_BRESP;
  assign S00_AXI_bvalid = axi_mem_intercon_1_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = axi_mem_intercon_1_to_s00_couplers_RDATA;
  assign S00_AXI_rid[11:0] = axi_mem_intercon_1_to_s00_couplers_RID;
  assign S00_AXI_rlast = axi_mem_intercon_1_to_s00_couplers_RLAST;
  assign S00_AXI_rresp[1:0] = axi_mem_intercon_1_to_s00_couplers_RRESP;
  assign S00_AXI_rvalid = axi_mem_intercon_1_to_s00_couplers_RVALID;
  assign S00_AXI_wready = axi_mem_intercon_1_to_s00_couplers_WREADY;
  assign axi_mem_intercon_1_ACLK_net = M00_ACLK;
  assign axi_mem_intercon_1_ARESETN_net = M00_ARESETN[0];
  assign axi_mem_intercon_1_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARBURST = S00_AXI_arburst[1:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARCACHE = S00_AXI_arcache[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARID = S00_AXI_arid[11:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARLEN = S00_AXI_arlen[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARLOCK = S00_AXI_arlock[1:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARQOS = S00_AXI_arqos[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARSIZE = S00_AXI_arsize[2:0];
  assign axi_mem_intercon_1_to_s00_couplers_ARVALID = S00_AXI_arvalid;
  assign axi_mem_intercon_1_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWBURST = S00_AXI_awburst[1:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWCACHE = S00_AXI_awcache[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWID = S00_AXI_awid[11:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWLEN = S00_AXI_awlen[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWLOCK = S00_AXI_awlock[1:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWQOS = S00_AXI_awqos[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWSIZE = S00_AXI_awsize[2:0];
  assign axi_mem_intercon_1_to_s00_couplers_AWVALID = S00_AXI_awvalid;
  assign axi_mem_intercon_1_to_s00_couplers_BREADY = S00_AXI_bready;
  assign axi_mem_intercon_1_to_s00_couplers_RREADY = S00_AXI_rready;
  assign axi_mem_intercon_1_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign axi_mem_intercon_1_to_s00_couplers_WID = S00_AXI_wid[11:0];
  assign axi_mem_intercon_1_to_s00_couplers_WLAST = S00_AXI_wlast;
  assign axi_mem_intercon_1_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign axi_mem_intercon_1_to_s00_couplers_WVALID = S00_AXI_wvalid;
  assign s00_couplers_to_axi_mem_intercon_1_ARREADY = M00_AXI_arready;
  assign s00_couplers_to_axi_mem_intercon_1_AWREADY = M00_AXI_awready;
  assign s00_couplers_to_axi_mem_intercon_1_BID = M00_AXI_bid[11:0];
  assign s00_couplers_to_axi_mem_intercon_1_BRESP = M00_AXI_bresp[1:0];
  assign s00_couplers_to_axi_mem_intercon_1_BVALID = M00_AXI_bvalid;
  assign s00_couplers_to_axi_mem_intercon_1_RDATA = M00_AXI_rdata[31:0];
  assign s00_couplers_to_axi_mem_intercon_1_RID = M00_AXI_rid[11:0];
  assign s00_couplers_to_axi_mem_intercon_1_RLAST = M00_AXI_rlast;
  assign s00_couplers_to_axi_mem_intercon_1_RRESP = M00_AXI_rresp[1:0];
  assign s00_couplers_to_axi_mem_intercon_1_RVALID = M00_AXI_rvalid;
  assign s00_couplers_to_axi_mem_intercon_1_WREADY = M00_AXI_wready;
s00_couplers_imp_R0HIN2 s00_couplers
       (.M_ACLK(axi_mem_intercon_1_ACLK_net),
        .M_ARESETN(axi_mem_intercon_1_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_axi_mem_intercon_1_ARADDR),
        .M_AXI_arburst(s00_couplers_to_axi_mem_intercon_1_ARBURST),
        .M_AXI_arcache(s00_couplers_to_axi_mem_intercon_1_ARCACHE),
        .M_AXI_arid(s00_couplers_to_axi_mem_intercon_1_ARID),
        .M_AXI_arlen(s00_couplers_to_axi_mem_intercon_1_ARLEN),
        .M_AXI_arlock(s00_couplers_to_axi_mem_intercon_1_ARLOCK),
        .M_AXI_arprot(s00_couplers_to_axi_mem_intercon_1_ARPROT),
        .M_AXI_arqos(s00_couplers_to_axi_mem_intercon_1_ARQOS),
        .M_AXI_arready(s00_couplers_to_axi_mem_intercon_1_ARREADY),
        .M_AXI_arsize(s00_couplers_to_axi_mem_intercon_1_ARSIZE),
        .M_AXI_arvalid(s00_couplers_to_axi_mem_intercon_1_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_axi_mem_intercon_1_AWADDR),
        .M_AXI_awburst(s00_couplers_to_axi_mem_intercon_1_AWBURST),
        .M_AXI_awcache(s00_couplers_to_axi_mem_intercon_1_AWCACHE),
        .M_AXI_awid(s00_couplers_to_axi_mem_intercon_1_AWID),
        .M_AXI_awlen(s00_couplers_to_axi_mem_intercon_1_AWLEN),
        .M_AXI_awlock(s00_couplers_to_axi_mem_intercon_1_AWLOCK),
        .M_AXI_awprot(s00_couplers_to_axi_mem_intercon_1_AWPROT),
        .M_AXI_awqos(s00_couplers_to_axi_mem_intercon_1_AWQOS),
        .M_AXI_awready(s00_couplers_to_axi_mem_intercon_1_AWREADY),
        .M_AXI_awsize(s00_couplers_to_axi_mem_intercon_1_AWSIZE),
        .M_AXI_awvalid(s00_couplers_to_axi_mem_intercon_1_AWVALID),
        .M_AXI_bid(s00_couplers_to_axi_mem_intercon_1_BID),
        .M_AXI_bready(s00_couplers_to_axi_mem_intercon_1_BREADY),
        .M_AXI_bresp(s00_couplers_to_axi_mem_intercon_1_BRESP),
        .M_AXI_bvalid(s00_couplers_to_axi_mem_intercon_1_BVALID),
        .M_AXI_rdata(s00_couplers_to_axi_mem_intercon_1_RDATA),
        .M_AXI_rid(s00_couplers_to_axi_mem_intercon_1_RID),
        .M_AXI_rlast(s00_couplers_to_axi_mem_intercon_1_RLAST),
        .M_AXI_rready(s00_couplers_to_axi_mem_intercon_1_RREADY),
        .M_AXI_rresp(s00_couplers_to_axi_mem_intercon_1_RRESP),
        .M_AXI_rvalid(s00_couplers_to_axi_mem_intercon_1_RVALID),
        .M_AXI_wdata(s00_couplers_to_axi_mem_intercon_1_WDATA),
        .M_AXI_wlast(s00_couplers_to_axi_mem_intercon_1_WLAST),
        .M_AXI_wready(s00_couplers_to_axi_mem_intercon_1_WREADY),
        .M_AXI_wstrb(s00_couplers_to_axi_mem_intercon_1_WSTRB),
        .M_AXI_wvalid(s00_couplers_to_axi_mem_intercon_1_WVALID),
        .S_ACLK(S00_ACLK_1),
        .S_ARESETN(S00_ARESETN_1),
        .S_AXI_araddr(axi_mem_intercon_1_to_s00_couplers_ARADDR),
        .S_AXI_arburst(axi_mem_intercon_1_to_s00_couplers_ARBURST),
        .S_AXI_arcache(axi_mem_intercon_1_to_s00_couplers_ARCACHE),
        .S_AXI_arid(axi_mem_intercon_1_to_s00_couplers_ARID),
        .S_AXI_arlen(axi_mem_intercon_1_to_s00_couplers_ARLEN),
        .S_AXI_arlock(axi_mem_intercon_1_to_s00_couplers_ARLOCK),
        .S_AXI_arprot(axi_mem_intercon_1_to_s00_couplers_ARPROT),
        .S_AXI_arqos(axi_mem_intercon_1_to_s00_couplers_ARQOS),
        .S_AXI_arready(axi_mem_intercon_1_to_s00_couplers_ARREADY),
        .S_AXI_arsize(axi_mem_intercon_1_to_s00_couplers_ARSIZE),
        .S_AXI_arvalid(axi_mem_intercon_1_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(axi_mem_intercon_1_to_s00_couplers_AWADDR),
        .S_AXI_awburst(axi_mem_intercon_1_to_s00_couplers_AWBURST),
        .S_AXI_awcache(axi_mem_intercon_1_to_s00_couplers_AWCACHE),
        .S_AXI_awid(axi_mem_intercon_1_to_s00_couplers_AWID),
        .S_AXI_awlen(axi_mem_intercon_1_to_s00_couplers_AWLEN),
        .S_AXI_awlock(axi_mem_intercon_1_to_s00_couplers_AWLOCK),
        .S_AXI_awprot(axi_mem_intercon_1_to_s00_couplers_AWPROT),
        .S_AXI_awqos(axi_mem_intercon_1_to_s00_couplers_AWQOS),
        .S_AXI_awready(axi_mem_intercon_1_to_s00_couplers_AWREADY),
        .S_AXI_awsize(axi_mem_intercon_1_to_s00_couplers_AWSIZE),
        .S_AXI_awvalid(axi_mem_intercon_1_to_s00_couplers_AWVALID),
        .S_AXI_bid(axi_mem_intercon_1_to_s00_couplers_BID),
        .S_AXI_bready(axi_mem_intercon_1_to_s00_couplers_BREADY),
        .S_AXI_bresp(axi_mem_intercon_1_to_s00_couplers_BRESP),
        .S_AXI_bvalid(axi_mem_intercon_1_to_s00_couplers_BVALID),
        .S_AXI_rdata(axi_mem_intercon_1_to_s00_couplers_RDATA),
        .S_AXI_rid(axi_mem_intercon_1_to_s00_couplers_RID),
        .S_AXI_rlast(axi_mem_intercon_1_to_s00_couplers_RLAST),
        .S_AXI_rready(axi_mem_intercon_1_to_s00_couplers_RREADY),
        .S_AXI_rresp(axi_mem_intercon_1_to_s00_couplers_RRESP),
        .S_AXI_rvalid(axi_mem_intercon_1_to_s00_couplers_RVALID),
        .S_AXI_wdata(axi_mem_intercon_1_to_s00_couplers_WDATA),
        .S_AXI_wid(axi_mem_intercon_1_to_s00_couplers_WID),
        .S_AXI_wlast(axi_mem_intercon_1_to_s00_couplers_WLAST),
        .S_AXI_wready(axi_mem_intercon_1_to_s00_couplers_WREADY),
        .S_AXI_wstrb(axi_mem_intercon_1_to_s00_couplers_WSTRB),
        .S_AXI_wvalid(axi_mem_intercon_1_to_s00_couplers_WVALID));
endmodule
