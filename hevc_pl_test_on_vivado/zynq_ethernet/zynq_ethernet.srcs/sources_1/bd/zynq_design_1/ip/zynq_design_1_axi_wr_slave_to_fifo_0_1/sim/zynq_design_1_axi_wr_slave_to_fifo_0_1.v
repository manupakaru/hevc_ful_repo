// (c) Copyright 1995-2014 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:user:axi_wr_slave_to_fifo:1.0
// IP Revision: 14

`timescale 1ns/1ps

(* DowngradeIPIdentifiedWarnings = "yes" *)
module zynq_design_1_axi_wr_slave_to_fifo_0_1 (
  reset,
  axi_write_clk,
  axi_fifo_state_out,
  config_data_out,
  y_residual_data_out,
  cb_residual_data_out,
  cr_residual_data_out,
  config_wr_en,
  y_residual_wr_en,
  cb_residual_wr_en,
  cr_residual_wr_en,
  config_full,
  y_residual_full,
  cr_residual_full,
  cb_residual_full,
  S00_AXI_ACLK,
  S00_AXI_AWID,
  S00_AXI_AWADDR,
  S00_AXI_AWLEN,
  S00_AXI_AWSIZE,
  S00_AXI_AWBURST,
  S00_AXI_AWLOCK,
  S00_AXI_AWCACHE,
  S00_AXI_AWPROT,
  S00_AXI_AWQOS,
  S00_AXI_AWVALID,
  S00_AXI_AWREADY,
  S00_AXI_WDATA,
  S00_AXI_WSTRB,
  S00_AXI_WLAST,
  S00_AXI_WVALID,
  S00_AXI_WREADY,
  S00_AXI_BID,
  S00_AXI_BRESP,
  S00_AXI_BVALID,
  S00_AXI_BREADY,
  S00_AXI_ARID,
  S00_AXI_ARADDR,
  S00_AXI_ARLEN,
  S00_AXI_ARSIZE,
  S00_AXI_ARBURST,
  S00_AXI_ARLOCK,
  S00_AXI_ARCACHE,
  S00_AXI_ARPROT,
  S00_AXI_ARQOS,
  S00_AXI_ARVALID,
  S00_AXI_ARREADY,
  S00_AXI_RID,
  S00_AXI_RDATA,
  S00_AXI_RRESP,
  S00_AXI_RLAST,
  S00_AXI_RVALID,
  S00_AXI_RREADY
);

(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 signal_reset RST" *)
input wire reset;
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 axi_write_signal_clock CLK" *)
output wire axi_write_clk;
output wire [4 : 0] axi_fifo_state_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 config_fifo_write WR_DATA" *)
output wire [31 : 0] config_data_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 y_residual_fifo_write WR_DATA" *)
output wire [143 : 0] y_residual_data_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 cb_residual_fifo_write WR_DATA" *)
output wire [143 : 0] cb_residual_data_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 cr_residual_fifo_write WR_DATA" *)
output wire [143 : 0] cr_residual_data_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 config_fifo_write WR_EN" *)
output wire config_wr_en;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 y_residual_fifo_write WR_EN" *)
output wire y_residual_wr_en;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 cb_residual_fifo_write WR_EN" *)
output wire cb_residual_wr_en;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 cr_residual_fifo_write WR_EN" *)
output wire cr_residual_wr_en;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 config_fifo_write FULL" *)
input wire config_full;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 y_residual_fifo_write FULL" *)
input wire y_residual_full;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 cr_residual_fifo_write FULL" *)
input wire cr_residual_full;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 cb_residual_fifo_write FULL" *)
input wire cb_residual_full;
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S00_AXI_signal_clock CLK" *)
input wire S00_AXI_ACLK;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWID" *)
input wire [11 : 0] S00_AXI_AWID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *)
input wire [15 : 0] S00_AXI_AWADDR;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWLEN" *)
input wire [7 : 0] S00_AXI_AWLEN;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWSIZE" *)
input wire [2 : 0] S00_AXI_AWSIZE;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWBURST" *)
input wire [1 : 0] S00_AXI_AWBURST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWLOCK" *)
input wire S00_AXI_AWLOCK;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWCACHE" *)
input wire [3 : 0] S00_AXI_AWCACHE;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *)
input wire [2 : 0] S00_AXI_AWPROT;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWQOS" *)
input wire [3 : 0] S00_AXI_AWQOS;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *)
input wire S00_AXI_AWVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *)
output wire S00_AXI_AWREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *)
input wire [31 : 0] S00_AXI_WDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *)
input wire [4 : 0] S00_AXI_WSTRB;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WLAST" *)
input wire S00_AXI_WLAST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *)
input wire S00_AXI_WVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *)
output wire S00_AXI_WREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BID" *)
output wire [11 : 0] S00_AXI_BID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *)
output wire [1 : 0] S00_AXI_BRESP;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *)
output wire S00_AXI_BVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *)
input wire S00_AXI_BREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARID" *)
input wire [11 : 0] S00_AXI_ARID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *)
input wire [15 : 0] S00_AXI_ARADDR;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARLEN" *)
input wire [7 : 0] S00_AXI_ARLEN;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARSIZE" *)
input wire [2 : 0] S00_AXI_ARSIZE;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARBURST" *)
input wire [1 : 0] S00_AXI_ARBURST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARLOCK" *)
input wire S00_AXI_ARLOCK;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARCACHE" *)
input wire [3 : 0] S00_AXI_ARCACHE;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *)
input wire [2 : 0] S00_AXI_ARPROT;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARQOS" *)
input wire [3 : 0] S00_AXI_ARQOS;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *)
input wire S00_AXI_ARVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *)
output wire S00_AXI_ARREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RID" *)
output wire [11 : 0] S00_AXI_RID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *)
output wire [31 : 0] S00_AXI_RDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *)
output wire [1 : 0] S00_AXI_RRESP;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RLAST" *)
output wire S00_AXI_RLAST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *)
output wire S00_AXI_RVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *)
input wire S00_AXI_RREADY;

  axi_wr_slave_to_fifo_wr_inf inst (
    .reset(reset),
    .axi_write_clk(axi_write_clk),
    .axi_fifo_state_out(axi_fifo_state_out),
    .config_data_out(config_data_out),
    .y_residual_data_out(y_residual_data_out),
    .cb_residual_data_out(cb_residual_data_out),
    .cr_residual_data_out(cr_residual_data_out),
    .config_wr_en(config_wr_en),
    .y_residual_wr_en(y_residual_wr_en),
    .cb_residual_wr_en(cb_residual_wr_en),
    .cr_residual_wr_en(cr_residual_wr_en),
    .config_full(config_full),
    .y_residual_full(y_residual_full),
    .cr_residual_full(cr_residual_full),
    .cb_residual_full(cb_residual_full),
    .S00_AXI_ACLK(S00_AXI_ACLK),
    .S00_AXI_AWID(S00_AXI_AWID),
    .S00_AXI_AWADDR(S00_AXI_AWADDR),
    .S00_AXI_AWLEN(S00_AXI_AWLEN),
    .S00_AXI_AWSIZE(S00_AXI_AWSIZE),
    .S00_AXI_AWBURST(S00_AXI_AWBURST),
    .S00_AXI_AWLOCK(S00_AXI_AWLOCK),
    .S00_AXI_AWCACHE(S00_AXI_AWCACHE),
    .S00_AXI_AWPROT(S00_AXI_AWPROT),
    .S00_AXI_AWQOS(S00_AXI_AWQOS),
    .S00_AXI_AWVALID(S00_AXI_AWVALID),
    .S00_AXI_AWREADY(S00_AXI_AWREADY),
    .S00_AXI_WDATA(S00_AXI_WDATA),
    .S00_AXI_WSTRB(S00_AXI_WSTRB),
    .S00_AXI_WLAST(S00_AXI_WLAST),
    .S00_AXI_WVALID(S00_AXI_WVALID),
    .S00_AXI_WREADY(S00_AXI_WREADY),
    .S00_AXI_BID(S00_AXI_BID),
    .S00_AXI_BRESP(S00_AXI_BRESP),
    .S00_AXI_BVALID(S00_AXI_BVALID),
    .S00_AXI_BREADY(S00_AXI_BREADY),
    .S00_AXI_ARID(S00_AXI_ARID),
    .S00_AXI_ARADDR(S00_AXI_ARADDR),
    .S00_AXI_ARLEN(S00_AXI_ARLEN),
    .S00_AXI_ARSIZE(S00_AXI_ARSIZE),
    .S00_AXI_ARBURST(S00_AXI_ARBURST),
    .S00_AXI_ARLOCK(S00_AXI_ARLOCK),
    .S00_AXI_ARCACHE(S00_AXI_ARCACHE),
    .S00_AXI_ARPROT(S00_AXI_ARPROT),
    .S00_AXI_ARQOS(S00_AXI_ARQOS),
    .S00_AXI_ARVALID(S00_AXI_ARVALID),
    .S00_AXI_ARREADY(S00_AXI_ARREADY),
    .S00_AXI_RID(S00_AXI_RID),
    .S00_AXI_RDATA(S00_AXI_RDATA),
    .S00_AXI_RRESP(S00_AXI_RRESP),
    .S00_AXI_RLAST(S00_AXI_RLAST),
    .S00_AXI_RVALID(S00_AXI_RVALID),
    .S00_AXI_RREADY(S00_AXI_RREADY)
  );
endmodule
