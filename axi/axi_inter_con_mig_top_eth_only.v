`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:29:36 02/09/2014 
// Design Name: 
// Module Name:    pred_dbf_wrapper 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module axi_inter_con_mig_top_eth_only(
	
		
		
		sys_clk_p,
		sys_clk_n,

		sys_rst,
		clk_200,
		up_stream_reset,

        
        
    );

`include "../sim/pred_def.v"
`include "../sim/inter_axi_def.v"

   input                                        sys_clk_p;
   input                                        sys_clk_n;

   // System reset - Default polarity of sys_rst pin is Active Low.
   // System reset polarity will change based on the option 
   // selected in GUI.



   input                                        sys_rst;

	wire pll_locked;
    output clk_200;

	output reg up_stream_reset;
	initial begin

		up_stream_reset = 1;
	end

    always@(posedge clk_200) begin
		if(~(pll_locked && sys_rst==0)) begin
			up_stream_reset <= 1;
		end
		else begin
			up_stream_reset <= 0;
		end
    end
	
  clock_gen_diff instance_name
   (// Clock in ports
    .CLK_IN1_P(sys_clk_p),    // IN
    .CLK_IN1_N(sys_clk_n),    // IN
    // Clock out ports
    .CLK_OUT1(clk_200),     // OUT
    // Status and control signals
    .RESET(sys_rst),// IN
    .LOCKED(pll_locked));      // OUT
endmodule
