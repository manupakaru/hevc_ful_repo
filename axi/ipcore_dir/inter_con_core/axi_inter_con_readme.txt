The following files were generated for 'axi_inter_con' in directory
C:\Users\IC_2\Dropbox\hdl_codes\hevc_ful_repo\axi\ipcore_dir\inter_con_core\

Core-level Timespec UCF Generator:
   Generate timing constraints for clock converters and resets.

   * axi_inter_con.ucf

Generate XCO file:
   CORE Generator input file containing the parameters used to generate a core.

   * axi_inter_con.xco

Misc Files Generator:
   Please see the core data sheet.

   * axi_inter_con/generate/axi_interconnect_v1_06_a_ucfgen.tcl

Generate Implementation Netlist:
   Binary Xilinx implementation netlist files containing the information
   required to implement the module in a Xilinx (R) FPGA.

   * axi_inter_con.ngc

Generate Instantiation Templates:
   Template files containing code that can be used as a model for instantiating
   a CORE Generator module in an HDL design.

   * axi_inter_con.veo

Synthesis Instantiation Wrapper Generator:
   Please see the core data sheet.

   * axi_inter_con_synth.v

Behavioral Source Generator:
   Please see the core data sheet.

   * axi_inter_con/hdl/verilog/axi_interconnect_v1_06_a.v
   * axi_inter_con/hdl/verilog/ict106_a_axi3_conv.v
   * axi_inter_con/hdl/verilog/ict106_a_downsizer.v
   * axi_inter_con/hdl/verilog/ict106_a_upsizer.v
   * axi_inter_con/hdl/verilog/ict106_addr_arbiter.v
   * axi_inter_con/hdl/verilog/ict106_addr_arbiter_sasd.v
   * axi_inter_con/hdl/verilog/ict106_addr_decoder.v
   * axi_inter_con/hdl/verilog/ict106_arbiter_resp.v
   * axi_inter_con/hdl/verilog/ict106_axi3_conv.v
   * axi_inter_con/hdl/verilog/ict106_axi_clock_converter.v
   * axi_inter_con/hdl/verilog/ict106_axi_crossbar.v
   * axi_inter_con/hdl/verilog/ict106_axi_data_fifo.v
   * axi_inter_con/hdl/verilog/ict106_axi_downsizer.v
   * axi_inter_con/hdl/verilog/ict106_axi_interconnect.v
   * axi_inter_con/hdl/verilog/ict106_axi_protocol_converter.v
   * axi_inter_con/hdl/verilog/ict106_axi_register_slice.v
   * axi_inter_con/hdl/verilog/ict106_axi_upsizer.v
   * axi_inter_con/hdl/verilog/ict106_axic_fifo.v
   * axi_inter_con/hdl/verilog/ict106_axic_reg_srl_fifo.v
   * axi_inter_con/hdl/verilog/ict106_axic_register_slice.v
   * axi_inter_con/hdl/verilog/ict106_axic_sample_cycle_ratio.v
   * axi_inter_con/hdl/verilog/ict106_axic_srl_fifo.v
   * axi_inter_con/hdl/verilog/ict106_axic_sync_clock_converter.v
   * axi_inter_con/hdl/verilog/ict106_axilite_conv.v
   * axi_inter_con/hdl/verilog/ict106_b_downsizer.v
   * axi_inter_con/hdl/verilog/ict106_carry.v
   * axi_inter_con/hdl/verilog/ict106_carry_and.v
   * axi_inter_con/hdl/verilog/ict106_carry_latch_and.v
   * axi_inter_con/hdl/verilog/ict106_carry_latch_or.v
   * axi_inter_con/hdl/verilog/ict106_carry_or.v
   * axi_inter_con/hdl/verilog/ict106_command_fifo.v
   * axi_inter_con/hdl/verilog/ict106_comparator.v
   * axi_inter_con/hdl/verilog/ict106_comparator_mask.v
   * axi_inter_con/hdl/verilog/ict106_comparator_mask_static.v
   * axi_inter_con/hdl/verilog/ict106_comparator_sel.v
   * axi_inter_con/hdl/verilog/ict106_comparator_sel_mask.v
   * axi_inter_con/hdl/verilog/ict106_comparator_sel_mask_static.v
   * axi_inter_con/hdl/verilog/ict106_comparator_sel_static.v
   * axi_inter_con/hdl/verilog/ict106_comparator_static.v
   * axi_inter_con/hdl/verilog/ict106_converter_bank.v
   * axi_inter_con/hdl/verilog/ict106_crossbar.v
   * axi_inter_con/hdl/verilog/ict106_crossbar_sasd.v
   * axi_inter_con/hdl/verilog/ict106_data_fifo_bank.v
   * axi_inter_con/hdl/verilog/ict106_decerr_slave.v
   * axi_inter_con/hdl/verilog/ict106_fifo_gen.v
   * axi_inter_con/hdl/verilog/ict106_mux.v
   * axi_inter_con/hdl/verilog/ict106_mux_enc.v
   * axi_inter_con/hdl/verilog/ict106_ndeep_srl.v
   * axi_inter_con/hdl/verilog/ict106_nto1_mux.v
   * axi_inter_con/hdl/verilog/ict106_protocol_conv_bank.v
   * axi_inter_con/hdl/verilog/ict106_r_axi3_conv.v
   * axi_inter_con/hdl/verilog/ict106_r_downsizer.v
   * axi_inter_con/hdl/verilog/ict106_r_upsizer.v
   * axi_inter_con/hdl/verilog/ict106_register_slice_bank.v
   * axi_inter_con/hdl/verilog/ict106_si_transactor.v
   * axi_inter_con/hdl/verilog/ict106_splitter.v
   * axi_inter_con/hdl/verilog/ict106_w_axi3_conv.v
   * axi_inter_con/hdl/verilog/ict106_w_downsizer.v
   * axi_inter_con/hdl/verilog/ict106_w_upsizer.v
   * axi_inter_con/hdl/verilog/ict106_wdata_mux.v
   * axi_inter_con/hdl/verilog/ict106_wdata_router.v

Behavioral Instantiation Wrapper Generator:
   Please see the core data sheet.

   * axi_inter_con_sim.v

All Documents Generator:
   Please see the core data sheet.

   * axi_inter_con/doc/axi_interconnect_v1_06_a_changelog.txt
   * axi_inter_con/doc/ds768_axi_interconnect.pdf

Deliver IP Symbol:
   Graphical symbol information file. Used by the ISE tools and some third party
   tools to create a symbol representing the core.

   * axi_inter_con.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * axi_inter_con.sym

Synthesis ISE Generator:
   Please see the core data sheet.

   * axi_inter_con.gise
   * axi_inter_con.xise

Deliver Readme:
   Readme file for the IP.

   * axi_inter_con_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * axi_inter_con_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

