`timescale 1ns / 1ps
module ref_buff_fifo_monitor(
    clk,
    reset,
	empty,
	Xc_out_8x8,
	Yc_out_8x8,
	yy_pixels_8x8,
	cb_pixels_8x8,
	cr_pixels_8x8,
	pic_width_in,
	pic_height_in,
	rd_en
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
	`include "../sim/pred_def.v"

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------

	parameter FILE_NAME           = "";
	parameter OUT_VERIFY  = 1;
	parameter DEBUG = 0;
	
	parameter YY_WIDTH = PIXEL_WIDTH*DBF_OUT_Y_BLOCK_SIZE*DBF_OUT_Y_BLOCK_SIZE;
	parameter CH_WIDTH = PIXEL_WIDTH*DBF_OUT_CH_BLOCK_SIZE*DBF_OUT_CH_BLOCK_SIZE;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
	input empty,rd_en; 
	input [X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1: 0]	Xc_out_8x8;
	input [X11_ADDR_WDTH -LOG2_MIN_DU_SIZE -1: 0]	Yc_out_8x8;
	input [YY_WIDTH -1:0] 		yy_pixels_8x8;
	input [CH_WIDTH -1:0]		cb_pixels_8x8;
	input [CH_WIDTH -1:0]		cr_pixels_8x8;    
	input     [PIC_WIDTH_WIDTH-1:0]    pic_width_in;
	input     [PIC_WIDTH_WIDTH-1:0]    pic_height_in;
	
	
// synthesis translate_off
	integer file_in;
	integer i,j;
	reg [7:0] temp;
	integer rfile;
	integer cb_offset, cr_offset;
	
	wire [7:0] yy_arry[YY_WIDTH/8-1:0];
	wire [7:0] cb_arry[CH_WIDTH/8-1:0];
	wire [7:0] cr_arry[CH_WIDTH/8-1:0];
	integer frame_count;
	integer frame_offset;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------

// Instantiate the module

generate
        genvar ii;
        for (ii = 0 ; ii < YY_WIDTH/8 ; ii = ii + 1) begin
				assign  yy_arry[ii]= yy_pixels_8x8[(ii+1)*8-1:ii*8];
        end
		for (ii = 0 ; ii < CH_WIDTH/8 ; ii = ii + 1) begin 
			assign  cb_arry[ii]= cb_pixels_8x8[(ii+1)*8-1:ii*8];
        end
		for (ii = 0 ; ii < CH_WIDTH/8 ; ii = ii + 1) begin 
			assign  cr_arry[ii]= cr_pixels_8x8[(ii+1)*8-1:ii*8];
        end
endgenerate

initial begin
	if(OUT_VERIFY) begin
		frame_count = -1;
		file_in = $fopen(FILE_NAME,"rb");
		if(file_in) begin
			
		end
		else begin
			$display("file not open!!");
			$stop;
		end
	end	
end


always@(posedge clk) begin
   if(reset) begin
	end
	else begin
		if(!empty) begin
			if(rd_en) begin
				if(DEBUG ==1) begin
					$display("%d read from fifo %m x=%d y=%d yy=%x cb=%x cr=%x",$time,Xc_out_8x8,Yc_out_8x8,yy_pixels_8x8,cb_pixels_8x8,cr_pixels_8x8);
				end
            
				if(OUT_VERIFY) begin
					if(Xc_out_8x8 ==0 && Yc_out_8x8 ==0) begin
						frame_count = frame_count + 1;
						frame_offset = frame_count*pic_width_in*pic_height_in*3/2;
						cb_offset  = pic_width_in*pic_height_in+frame_offset;
						cr_offset  = cb_offset + pic_width_in*pic_height_in/4;
					end
					for(i=0; i<DBF_OUT_Y_BLOCK_SIZE;i=i+1) begin
						for(j=0; j<DBF_OUT_Y_BLOCK_SIZE;j=j+1) begin
							rfile = $fseek(file_in,(frame_offset+(pic_width_in)*(i + Yc_out_8x8*DBF_OUT_Y_BLOCK_SIZE) + Xc_out_8x8*DBF_OUT_Y_BLOCK_SIZE + j),0);
							temp= $fgetc(file_in);
							if(temp != yy_arry[DBF_OUT_Y_BLOCK_SIZE*i+j]) begin
								$display("%d sao out y wrong hard %x soft %x at x=%d y=%d",$time,yy_arry[DBF_OUT_Y_BLOCK_SIZE*i+j],temp,Xc_out_8x8*DBF_OUT_Y_BLOCK_SIZE + j,i + Yc_out_8x8*DBF_OUT_Y_BLOCK_SIZE);
								$stop;
							end
						end
					end
					
					for(i=0; i<DBF_OUT_CH_BLOCK_SIZE;i=i+1) begin
						for(j=0; j<DBF_OUT_CH_BLOCK_SIZE;j=j+1) begin
							rfile = $fseek(file_in,(cb_offset+(pic_width_in/2)*(i + Yc_out_8x8*DBF_OUT_CH_BLOCK_SIZE) + Xc_out_8x8*DBF_OUT_CH_BLOCK_SIZE + j),0);
							temp= $fgetc(file_in);
							if(temp != cb_arry[DBF_OUT_CH_BLOCK_SIZE*i+j]) begin
								$display("%d sao out cb wrong hard %x soft %x at x=%d y=%d",$time,cb_arry[DBF_OUT_CH_BLOCK_SIZE*i+j],temp,Xc_out_8x8*DBF_OUT_CH_BLOCK_SIZE + j,i + Yc_out_8x8*DBF_OUT_CH_BLOCK_SIZE);
								$stop;
							end
						end
					end
					for(i=0; i<DBF_OUT_CH_BLOCK_SIZE;i=i+1) begin
						for(j=0; j<DBF_OUT_CH_BLOCK_SIZE;j=j+1) begin
							rfile = $fseek(file_in,(cr_offset+(pic_width_in/2)*(i + Yc_out_8x8*DBF_OUT_CH_BLOCK_SIZE) + Xc_out_8x8*DBF_OUT_CH_BLOCK_SIZE + j),0);
							temp= $fgetc(file_in);
							if(temp != cr_arry[DBF_OUT_CH_BLOCK_SIZE*i+j]) begin
								$display("%d sao out cr wrong hard %x soft %x at x=%d y=%d",$time,cr_arry[DBF_OUT_CH_BLOCK_SIZE*i+j],temp,Xc_out_8x8*DBF_OUT_CH_BLOCK_SIZE + j,i + Yc_out_8x8*DBF_OUT_CH_BLOCK_SIZE);
								$stop;
							end
							else begin
								if(DEBUG) begin
									$display("%d sao out cr right hard %x soft %x at x=%d y=%d",$time,cr_arry[DBF_OUT_CH_BLOCK_SIZE*i+j],temp,Xc_out_8x8*DBF_OUT_CH_BLOCK_SIZE + j,i + Yc_out_8x8*DBF_OUT_CH_BLOCK_SIZE);
								end
							end
						end
					end
				end
			end
		end
		else begin
			if(rd_en) begin
				$display("%d trying to read while empty %m!!",$time);
				$stop;
			end
		end
	end
end

// synthesis translate_on
endmodule