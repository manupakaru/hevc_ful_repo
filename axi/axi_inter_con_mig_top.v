`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:29:36 02/09/2014 
// Design Name: 
// Module Name:    pred_dbf_wrapper 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module axi_inter_con_mig_top(

        //--------------axi interface
		mv_col_axi_awid    			,
		mv_col_axi_awlen   			,
		mv_col_axi_awsize  			,
		mv_col_axi_awburst 			,
		mv_col_axi_awlock  			,
		mv_col_axi_awcache 			,
		mv_col_axi_awprot  			,
		mv_col_axi_awvalid			,
		mv_col_axi_awaddr			,
	
		mv_col_axi_awready			,
	
		
		mv_col_axi_wstrb			,
		mv_col_axi_wlast			,
		mv_col_axi_wvalid			,
		mv_col_axi_wdata			,
	
		mv_col_axi_wready			,
	
		
		//mv_col_axi_bid				,
		mv_col_axi_bresp			,
		mv_col_axi_bvalid			,
		mv_col_axi_bready			,  
	
		mv_pref_axi_araddr  		,
		mv_pref_axi_arlen   		,
		mv_pref_axi_arsize  		,
		mv_pref_axi_arburst 		,
		mv_pref_axi_arprot  		,
		mv_pref_axi_arvalid 		,
		mv_pref_axi_arready 		,
				
		mv_pref_axi_rdata   		,
		mv_pref_axi_rresp   		,
		mv_pref_axi_rlast   		,
		mv_pref_axi_rvalid  		,
		mv_pref_axi_rready  		,
			
		mv_pref_axi_arlock  		,
		mv_pref_axi_arid    		,    
		mv_pref_axi_arcache 		,     
		
		
		ref_pix_axi_ar_addr     	,
		ref_pix_axi_ar_len      	,
		ref_pix_axi_ar_size     	,
		ref_pix_axi_ar_burst    	,
		ref_pix_axi_ar_prot     	,
		ref_pix_axi_ar_valid    	,
		ref_pix_axi_ar_ready    	,
			
		ref_pix_axi_r_data      	,
		ref_pix_axi_r_resp      	,
		ref_pix_axi_r_last      	,
		ref_pix_axi_r_valid     	,
		ref_pix_axi_r_ready     	,   
		
		
		pixel_write_axi_awid    	,
		pixel_write_axi_awlen   	,
		pixel_write_axi_awsize  	,
		pixel_write_axi_awburst 	,
		pixel_write_axi_awlock  	,
		pixel_write_axi_awcache 	,
		pixel_write_axi_awprot  	,
		pixel_write_axi_awvalid		,
		pixel_write_axi_awaddr		,
		pixel_write_axi_awready		,
		
		
		pixel_write_axi_wstrb		,
		pixel_write_axi_wlast		,
		pixel_write_axi_wvalid		,
		pixel_write_axi_wdata		,
		
		pixel_write_axi_wready		,
		
		
		pixel_write_axi_bid			,
		pixel_write_axi_bresp		,
		pixel_write_axi_bvalid		,
		pixel_write_axi_bready		, 
	
	     





		ddr3_dq,
		ddr3_dqs_n,
		ddr3_dqs_p,
		
		
		ddr3_addr,
		ddr3_ba,
		ddr3_ras_n,
		ddr3_cas_n,
		ddr3_we_n,
		ddr3_reset_n,
		ddr3_ck_p,
		ddr3_ck_n,
		ddr3_cke,
		ddr3_cs_n,
		ddr3_dm,
		ddr3_odt,
		
		
		
		sys_clk_p,
		sys_clk_n,

		sys_rst,
		
		init_calib_complete,
		
		clk_200,
		design_clk,
		// reset_out,
		calib_complete_reset,
		up_stream_reset


        
        
    );

`include "../sim/pred_def.v"
`include "../sim/inter_axi_def.v"


//----------------------------------------------------mig parameters
   //***************************************************************************
   // Traffic Gen related parameters
   //***************************************************************************
   parameter BL_WIDTH              = 10;
   parameter PORT_MODE             = "BI_MODE";
   parameter DATA_MODE             = 4'b0010;
   parameter ADDR_MODE             = 4'b0011;
   parameter TST_MEM_INSTR_MODE    = "R_W_INSTR_MODE";
   parameter EYE_TEST              = "FALSE";
                                     // set EYE_TEST = "TRUE" to probe memory
                                     // signals. Traffic Generator will only
                                     // write to one single location and no
                                     // read transactions will be generated.
   parameter DATA_PATTERN          = "DGEN_ALL";
                                      // For small devices; choose one only.
                                      // For large device; choose "DGEN_ALL"
                                      // "DGEN_HAMMER"; "DGEN_WALKING1";
                                      // "DGEN_WALKING0";"DGEN_ADDR";"
                                      // "DGEN_NEIGHBOR";"DGEN_PRBS";"DGEN_ALL"
   parameter CMD_PATTERN           = "CGEN_ALL";
                                      // "CGEN_PRBS";"CGEN_FIXED";"CGEN_BRAM";
                                      // "CGEN_SEQUENTIAL"; "CGEN_ALL"
   parameter BEGIN_ADDRESS         = 32'h00000000;
   parameter END_ADDRESS           = 32'h00ffffff;
   parameter MEM_ADDR_ORDER
     = "TG_TEST";
   parameter PRBS_EADDR_MASK_POS   = 32'hff000000;
   parameter CMD_WDT               = 'h3FF;
   parameter WR_WDT                = 'h1FFF;
   parameter RD_WDT                = 'h3FF;
   parameter SEL_VICTIM_LINE       = 0;
   parameter ENFORCE_RD_WR         = 0;
   parameter ENFORCE_RD_WR_CMD     = 8'h11;
   parameter ENFORCE_RD_WR_PATTERN = 3'b000;
   parameter C_EN_WRAP_TRANS       = 0;
   parameter C_AXI_NBURST_TEST     = 0;

   //***************************************************************************
   // The following parameters refer to width of various ports
   //***************************************************************************
   parameter BANK_WIDTH            = 3;
                                     // # of memory Bank Address bits.
   parameter CK_WIDTH              = 1;
                                     // # of CK/CK# outputs to memory.
   parameter COL_WIDTH             = 10;
                                     // # of memory Column Address bits.
   parameter CS_WIDTH              = 1;
                                     // # of unique CS outputs to memory.
   parameter nCS_PER_RANK          = 1;
                                     // # of unique CS outputs per rank for phy
   parameter CKE_WIDTH             = 1;
                                     // # of CKE outputs to memory.
   parameter DATA_BUF_ADDR_WIDTH   = 5;
   parameter DQ_CNT_WIDTH          = 6;
                                     // = ceil(log2(DQ_WIDTH))
   parameter DQ_PER_DM             = 8;
   parameter DM_WIDTH              = 8;
                                     // # of DM (data mask)
   parameter DQ_WIDTH              = 64;
                                     // # of DQ (data)
   parameter DQS_WIDTH             = 8;
   parameter DQS_CNT_WIDTH         = 3;
                                     // = ceil(log2(DQS_WIDTH))
   parameter DRAM_WIDTH            = 8;
                                     // # of DQ per DQS
   parameter ECC                   = "OFF";
   parameter nBANK_MACHS           = 4;
   parameter RANKS                 = 1;
                                     // # of Ranks.
   parameter ODT_WIDTH             = 1;
                                     // # of ODT outputs to memory.
   parameter ROW_WIDTH             = 14;
                                     // # of memory Row Address bits.
   parameter ADDR_WIDTH            = 28;
                                     // # = RANK_WIDTH + BANK_WIDTH
                                     //     + ROW_WIDTH + COL_WIDTH;
                                     // Chip Select is always tied to low for
                                     // single rank devices
   parameter USE_CS_PORT          = 1;
                                     // # = 1; When Chip Select (CS#) output is enabled
                                     //   = 0; When Chip Select (CS#) output is disabled
                                     // If CS_N disabled; user must connect
                                     // DRAM CS_N input(s) to ground
   parameter USE_DM_PORT           = 1;
                                     // # = 1; When Data Mask option is enabled
                                     //   = 0; When Data Mask option is disbaled
                                     // When Data Mask option is disabled in
                                     // MIG Controller Options page; the logic
                                     // related to Data Mask should not get
                                     // synthesized
   parameter USE_ODT_PORT          = 1;
                                     // # = 1; When ODT output is enabled
                                     //   = 0; When ODT output is disabled
                                     // Parameter configuration for Dynamic ODT support:
                                     // USE_ODT_PORT = 0; RTT_NOM = "DISABLED"; RTT_WR = "60/120".
                                     // This configuration allows to save ODT pin mapping from FPGA.
                                     // The user can tie the ODT input of DRAM to HIGH.
   parameter PHY_CONTROL_MASTER_BANK = 1;
                                     // The bank index where master PHY_CONTROL resides;
                                     // equal to the PLL residing bank
   parameter MEM_DENSITY           = "1Gb";
                                     // Indicates the density of the Memory part
                                     // Added for the sake of Vivado simulations
   parameter MEM_SPEEDGRADE        = "125";
                                     // Indicates the Speed grade of Memory Part
                                     // Added for the sake of Vivado simulations
   parameter MEM_DEVICE_WIDTH      = 8;
                                     // Indicates the device width of the Memory Part
                                     // Added for the sake of Vivado simulations

   //***************************************************************************
   // The following parameters are mode register settings
   //***************************************************************************
   parameter AL                    = "0";
                                     // DDR3 SDRAM:
                                     // Additive Latency (Mode Register 1).

                                     // # = "0"; "CL-1"; "CL-2".
                                     // DDR2 SDRAM:
                                     // Additive Latency (Extended Mode Register).
   parameter nAL                   = 0;
                                     // # Additive Latency in number of clock
                                     // cycles.
   parameter BURST_MODE            = "8";
                                     // DDR3 SDRAM:
                                     // Burst Length (Mode Register 0).

                                     // # = "8"; "4"; "OTF".
                                     // DDR2 SDRAM:
                                     // Burst Length (Mode Register).

                                     // # = "8"; "4".
   parameter BURST_TYPE            = "SEQ";
                                     // DDR3 SDRAM: Burst Type (Mode Register 0).
                                     // DDR2 SDRAM: Burst Type (Mode Register).

                                     // # = "SEQ" - (Sequential);
                                     //   = "INT" - (Interleaved).

   parameter CL                    = 11;
                                     // in number of clock cycles
                                     // DDR3 SDRAM: CAS Latency (Mode Register 0).
                                     // DDR2 SDRAM: CAS Latency (Mode Register).

   parameter CWL                   = 8;
                                     // in number of clock cycles
                                     // DDR3 SDRAM: CAS Write Latency (Mode Register 2).
                                     // DDR2 SDRAM: Can be ignored
   parameter OUTPUT_DRV            = "HIGH";
                                     // Output Driver Impedance Control (Mode Register 1).

                                     // # = "HIGH" - RZQ/7;
                                     //   = "LOW" - RZQ/6.
   parameter RTT_NOM               = "40";
                                     // RTT_NOM (ODT) (Mode Register 1).


                                     //   = "120" - RZQ/2;
                                     //   = "60"  - RZQ/4;
                                     //   = "40"  - RZQ/6.
   parameter RTT_WR                = "OFF";
                                     // RTT_WR (ODT) (Mode Register 2).
                                     // # = "OFF" - Dynamic ODT off;


                                     //   = "120" - RZQ/2;
                                     //   = "60"  - RZQ/4;
   parameter ADDR_CMD_MODE         = "1T" ;

                                     // # = "1T"; "2T".
   parameter REG_CTRL              = "OFF";

                                     // # = "ON" - RDIMMs;
                                     //   = "OFF" - Components; SODIMMs; UDIMMs.
   parameter CA_MIRROR             = "OFF";
                                     // C/A mirror opt for DDR3 dual rank
   
   //***************************************************************************
   // The following parameters are multiplier and divisor factors for PLLE2.
   // Based on the selected design frequency these parameters vary.
   //***************************************************************************
   parameter CLKIN_PERIOD          = 5000;
                                     // Input Clock Period
   parameter CLKFBOUT_MULT         = 8;
                                     // write PLL VCO multiplier
   parameter DIVCLK_DIVIDE         = 1;
                                     // write PLL VCO divisor
   parameter CLKOUT0_PHASE         = 337.5;
                                     // Phase for PLL output clock (CLKOUT0)
   parameter CLKOUT0_DIVIDE        = 2;
                                     // VCO output divisor for PLL output clock (CLKOUT0)
   parameter CLKOUT1_DIVIDE        = 2;
                                     // VCO output divisor for PLL output clock (CLKOUT1)
   parameter CLKOUT2_DIVIDE        = 32;
                                     // VCO output divisor for PLL output clock (CLKOUT2)
   parameter CLKOUT3_DIVIDE        = 8;
                                     // VCO output divisor for PLL output clock (CLKOUT3)

   //***************************************************************************
   // Memory Timing Parameters. These parameters varies based on the selected
   // memory part.
   //***************************************************************************
   parameter tCKE                  = 5000;
                                     // memory tCKE paramter in pS
   parameter tFAW                  = 30000;
                                     // memory tRAW paramter in pS.
   parameter tRAS                  = 35000;
                                     // memory tRAS paramter in pS.
   parameter tRCD                  = 13125;
                                     // memory tRCD paramter in pS.
   parameter tREFI                 = 7800000;
                                     // memory tREFI paramter in pS.
   parameter tRFC                  = 110000;
                                     // memory tRFC paramter in pS.
   parameter tRP                   = 13125;
                                     // memory tRP paramter in pS.
   parameter tRRD                  = 6000;
                                     // memory tRRD paramter in pS.
   parameter tRTP                  = 7500;
                                     // memory tRTP paramter in pS.
   parameter tWTR                  = 7500;
                                     // memory tWTR paramter in pS.
   parameter tZQI                  = 128_000_000;
                                     // memory tZQI paramter in nS.
   parameter tZQCS                 = 64;
                                     // memory tZQCS paramter in clock cycles.

   //***************************************************************************
   // Simulation parameters
   //***************************************************************************
   parameter SIM_BYPASS_INIT_CAL   = "OFF";
                                     // # = "OFF" -  Complete memory init &
                                     //              calibration sequence
                                     // # = "SKIP" - Not supported
                                     // # = "FAST" - Complete memory init & use
                                     //              abbreviated calib sequence

   parameter SIMULATION            = "FALSE";
                                     // Should be TRUE during design simulations and
                                     // FALSE during implementations

   //***************************************************************************
   // The following parameters varies based on the pin out entered in MIG GUI.
   // Do not change any of these parameters directly by editing the RTL.
   // Any changes required should be done through GUI and the design regenerated.
   //***************************************************************************
   parameter BYTE_LANES_B0         = 4'b1111;
                                     // Byte lanes used in an IO column.
   parameter BYTE_LANES_B1         = 4'b1110;
                                     // Byte lanes used in an IO column.
   parameter BYTE_LANES_B2         = 4'b1111;
                                     // Byte lanes used in an IO column.
   parameter BYTE_LANES_B3         = 4'b0000;
                                     // Byte lanes used in an IO column.
   parameter BYTE_LANES_B4         = 4'b0000;
                                     // Byte lanes used in an IO column.
   parameter DATA_CTL_B0           = 4'b1111;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
   parameter DATA_CTL_B1           = 4'b0000;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
   parameter DATA_CTL_B2           = 4'b1111;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
   parameter DATA_CTL_B3           = 4'b0000;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
   parameter DATA_CTL_B4           = 4'b0000;
                                     // Indicates Byte lane is data byte lane
                                     // or control Byte lane. '1' in a bit
                                     // position indicates a data byte lane and
                                     // a '0' indicates a control byte lane
   parameter PHY_0_BITLANES        = 48'h3FE_1FF_1FF_2FF;
   parameter PHY_1_BITLANES        = 48'hFFE_F30_CB4_000;
   parameter PHY_2_BITLANES        = 48'h3FE_3FE_3BF_2FF;

   // control/address/data pin mapping parameters
   parameter CK_BYTE_MAP
     = 144'h00_00_00_00_00_00_00_00_00_00_00_00_00_00_00_00_00_11;
   parameter ADDR_MAP
     = 192'h000_000_132_136_135_133_139_124_131_129_137_134_13A_128_138_13B;
   parameter BANK_MAP   = 36'h125_12A_12B;
   parameter CAS_MAP    = 12'h115;
   parameter CKE_ODT_BYTE_MAP = 8'h00;
   parameter CKE_MAP    = 96'h000_000_000_000_000_000_000_117;
   parameter ODT_MAP    = 96'h000_000_000_000_000_000_000_112;
   parameter CS_MAP     = 120'h000_000_000_000_000_000_000_000_000_114;
   parameter PARITY_MAP = 12'h000;
   parameter RAS_MAP    = 12'h11A;
   parameter WE_MAP     = 12'h11B;
   parameter DQS_BYTE_MAP
     = 144'h00_00_00_00_00_00_00_00_00_00_20_21_22_23_03_02_01_00;
   parameter DATA0_MAP  = 96'h009_000_003_001_007_006_005_002;
   parameter DATA1_MAP  = 96'h014_018_010_011_017_016_012_013;
   parameter DATA2_MAP  = 96'h021_022_025_020_027_023_026_028;
   parameter DATA3_MAP  = 96'h033_039_031_035_032_038_034_037;
   parameter DATA4_MAP  = 96'h231_238_237_236_233_232_234_239;
   parameter DATA5_MAP  = 96'h226_227_225_229_221_222_224_228;
   parameter DATA6_MAP  = 96'h214_215_210_218_217_213_219_212;
   parameter DATA7_MAP  = 96'h207_203_204_206_202_201_205_209;
   parameter DATA8_MAP  = 96'h000_000_000_000_000_000_000_000;
   parameter DATA9_MAP  = 96'h000_000_000_000_000_000_000_000;
   parameter DATA10_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA11_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA12_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA13_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA14_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA15_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA16_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter DATA17_MAP = 96'h000_000_000_000_000_000_000_000;
   parameter MASK0_MAP  = 108'h000_200_211_223_235_036_024_015_004;
   parameter MASK1_MAP  = 108'h000_000_000_000_000_000_000_000_000;

   parameter SLOT_0_CONFIG         = 8'b0000_0001;
                                     // Mapping of Ranks.
   parameter SLOT_1_CONFIG         = 8'b0000_0000;
                                     // Mapping of Ranks.

   //***************************************************************************
   // IODELAY and PHY related parameters
   //***************************************************************************
   parameter IODELAY_HP_MODE       = "ON";
                                     // to phy_top
   parameter IBUF_LPWR_MODE        = "OFF";
                                     // to phy_top
   parameter DATA_IO_IDLE_PWRDWN   = "OFF";

                                     // # = "ON"; "OFF"
   parameter BANK_TYPE             = "HP_IO";
                                     // # = "HP_IO"; "HPL_IO"; "HR_IO"; "HRL_IO"
   parameter DATA_IO_PRIM_TYPE     = "DEFAULT";
                                     // # = "HP_LP"; "HR_LP"; "DEFAULT"
   parameter CKE_ODT_AUX           = "FALSE";
   parameter USER_REFRESH          = "OFF";
   parameter WRLVL                 = "ON";
                                     // # = "ON" - DDR3 SDRAM
                                     //   = "OFF" - DDR2 SDRAM.
   parameter ORDERING              = "NORM";
                                     // # = "NORM"; "STRICT"; "RELAXED".
   parameter CALIB_ROW_ADD         = 16'h0000;
                                     // Calibration row address will be used for
                                     // calibration read and write operations
   parameter CALIB_COL_ADD         = 12'h000;
                                     // Calibration column address will be used for
                                     // calibration read and write operations
   parameter CALIB_BA_ADD          = 3'h0;
                                     // Calibration bank address will be used for
                                     // calibration read and write operations
   parameter TCQ                   = 100;
   parameter IODELAY_GRP           = "IODELAY_MIG";
                                     // It is associated to a set of IODELAYs with
                                     // an IDELAYCTRL that have same IODELAY CONTROLLER
                                     // clock frequency.
   parameter SYSCLK_TYPE           = "DIFFERENTIAL";
                                     // System clock type DIFFERENTIAL; SINGLE_ENDED;
                                     // NO_BUFFER
   parameter REFCLK_TYPE           = "USE_SYSTEM_CLOCK";
                                     // Reference clock type DIFFERENTIAL; SINGLE_ENDED;
                                     // NO_BUFFER; USE_SYSTEM_CLOCK
   parameter SYS_RST_PORT          = "TRUE";
                                     // "TRUE" - if pin is selected for sys_rst
                                     //          and IBUF will be instantiated.
                                     // "FALSE" - if pin is not selected for sys_rst
      
   parameter DRAM_TYPE             = "DDR3";
   parameter CAL_WIDTH             = "HALF";
   parameter STARVE_LIMIT          = 2;

                                     // # = 2;3;4.

   //***************************************************************************
   // Referece clock frequency parameters
   //***************************************************************************
   parameter REFCLK_FREQ           = 200.0;
                                     // IODELAYCTRL reference clock frequency
   parameter DIFF_TERM_REFCLK      = "TRUE";
                                     // Differential Termination for idelay
                                     // reference clock input pins
   //***************************************************************************
   // System clock frequency parameters
   //***************************************************************************

   parameter tCK                   = 1250;
                                     // memory tCK paramter.
                                     // # = Clock Period in pS.
   parameter nCK_PER_CLK           = 4;
                                     // # of memory CKs per fabric CLK
   parameter DIFF_TERM_SYSCLK      = "FALSE";
                                     // Differential Termination for System
                                     // clock input pins

   
   //***************************************************************************
   // AXI4 Shim parameters
   //***************************************************************************
   
   parameter UI_EXTRA_CLOCKS = "FALSE";
                                     // Generates extra clocks as
                                     // 1/2; 1/4 and 1/8 of fabrick clock.
                                     // Valid for DDR2/DDR3 AXI interfaces
                                     // based on GUI selection
   parameter C_S_AXI_ID_WIDTH              = 4;
                                             // Width of all master and slave ID signals.
                                             // # = >= 1.
   parameter C_S_AXI_MEM_SIZE              = "1073741824";
                                     // Address Space required for this component
   parameter C_S_AXI_ADDR_WIDTH            = 32;
                                             // Width of S_AXI_AWADDR; S_AXI_ARADDR; M_AXI_AWADDR and
                                             // M_AXI_ARADDR for all SI/MI slots.
                                             // # = 32.
   parameter C_S_AXI_DATA_WIDTH            = AXI_MIG_DATA_WIDTH;
                                             // Width of WDATA and RDATA on SI slot.
                                             // Must be <= APP_DATA_WIDTH.

                                             // # = 32; 64; 128; 256.
   parameter C_MC_nCK_PER_CLK              = 4;
                                             // Indicates whether to instatiate upsizer

                                             // Range: 0; 1
   parameter C_S_AXI_SUPPORTS_NARROW_BURST = 1;
                                             // Indicates whether to instatiate upsizer

                                             // Range: 0; 1
   parameter C_RD_WR_ARB_ALGORITHM          = "RD_PRI_REG";
                                             // Indicates the Arbitration
                                             // Allowed values - "TDM"; "ROUND_ROBIN";
                                             // "RD_PRI_REG"; "RD_PRI_REG_STARVE_LIMIT"
                                             // "WRITE_PRIORITY"; "WRITE_PRIORITY_REG"
   parameter C_S_AXI_REG_EN0               = 20'h00000;
                                             // C_S_AXI_REG_EN0[00] = Reserved
                                             // C_S_AXI_REG_EN0[04] = AW CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN0[05] =  W CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN0[06] =  B CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN0[07] =  R CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN0[08] = AW CHANNEL UPSIZER REGISTER SLICE
                                             // C_S_AXI_REG_EN0[09] =  W CHANNEL UPSIZER REGISTER SLICE
                                             // C_S_AXI_REG_EN0[10] = AR CHANNEL UPSIZER REGISTER SLICE
                                             // C_S_AXI_REG_EN0[11] =  R CHANNEL UPSIZER REGISTER SLICE
   parameter C_S_AXI_REG_EN1               = 20'h00000;
                                             // Instatiates register slices after the upsizer.
                                             // The type of register is specified for each channel
                                             // in a vector. 4 bits per channel are used.
                                             // C_S_AXI_REG_EN1[03:00] = AW CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN1[07:04] =  W CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN1[11:08] =  B CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN1[15:12] = AR CHANNEL REGISTER SLICE
                                             // C_S_AXI_REG_EN1[20:16] =  R CHANNEL REGISTER SLICE
                                             // Possible values for each channel are:
                                             //
                                             //   0 => BYPASS    = The channel is just wired through the
                                             //                    module.
                                             //   1 => FWD       = The master VALID and payload signals
                                             //                    are registrated.
                                             //   2 => REV       = The slave ready signal is registrated
                                             //   3 => FWD_REV   = Both FWD and REV
                                             //   4 => SLAVE_FWD = All slave side signals and master
                                             //                    VALID and payload are registrated.
                                             //   5 => SLAVE_RDY = All slave side signals and master
                                             //                    READY are registrated.
                                             //   6 => INPUTS    = Slave and Master side inputs are
                                             //                    registrated.
                                             //   7 => ADDRESS   = Optimized for address channel
   parameter C_S_AXI_CTRL_ADDR_WIDTH       = 32;
                                             // Width of AXI-4-Lite address bus
   parameter C_S_AXI_CTRL_DATA_WIDTH       = 32;
                                             // Width of AXI-4-Lite data buses
   parameter C_S_AXI_BASEADDR              = 32'h0000_0000;
                                             // Base address of AXI4 Memory Mapped bus.
   parameter C_ECC_ONOFF_RESET_VALUE       = 1;
                                             // Controls ECC on/off value at startup/reset
   parameter C_ECC_CE_COUNTER_WIDTH        = 8;
                                             // The external memory to controller clock ratio.

   //***************************************************************************
   // Debug parameters
   //***************************************************************************
   parameter DEBUG_PORT            = "OFF";
                                     // # = "ON" Enable debug signals/controls.
                                     //   = "OFF" Disable debug signals/controls.

   //***************************************************************************
   // Temparature monitor parameter
   //***************************************************************************
   parameter TEMP_MON_CONTROL                          = "INTERNAL";
                                     // # = "INTERNAL"; "EXTERNAL"
      
   parameter RST_ACT_LOW           = 1;
                                     // =1 for active low reset,
                                     // =0 for active high.




//-----------------------------------------------------




// `ifndef SOFT_MEM_SLAVE
   inout [DQ_WIDTH-1:0]                         ddr3_dq;
   inout [DQS_WIDTH-1:0]                        ddr3_dqs_n;
   inout [DQS_WIDTH-1:0]                        ddr3_dqs_p;

   // Outputs
   output [ROW_WIDTH-1:0]                       ddr3_addr;
   output [BANK_WIDTH-1:0]                      ddr3_ba;




   output                                       ddr3_ras_n;
   output                                       ddr3_cas_n;
   output                                       ddr3_we_n;
   output                                       ddr3_reset_n;
   output [CK_WIDTH-1:0]                        ddr3_ck_p;
   output [CK_WIDTH-1:0]                        ddr3_ck_n;
   output [CKE_WIDTH-1:0]                       ddr3_cke;
   output [CS_WIDTH*nCS_PER_RANK-1:0]           ddr3_cs_n;
   output [DM_WIDTH-1:0]                        ddr3_dm;
   output [ODT_WIDTH-1:0]                       ddr3_odt;

   // Inputs
   // Differential system clocks








   input                                        sys_clk_p;
   input                                        sys_clk_n;

   // System reset - Default polarity of sys_rst pin is Active Low.
   // System reset polarity will change based on the option 
   // selected in GUI.



   input                                        sys_rst;
// `endif 		
   output wire                               	init_calib_complete;
//-------------------------------------------------------    

function integer clogb2 (input integer size);
    begin
      size = size - 1;
      for (clogb2=1; size>1; clogb2=clogb2+1)
        size = size >> 1;
    end
  endfunction // clogb2

  function integer STR_TO_INT;
    input [7:0] in;
    begin
      if(in == "8")
        STR_TO_INT = 8;
      else if(in == "4")
        STR_TO_INT = 4;
      else
        STR_TO_INT = 0;
    end
  endfunction


  localparam CMD_PIPE_PLUS1        = "ON";
                                     // add pipeline stage between MC and PHY
  localparam DATA_WIDTH            = 64;
  localparam ECC_WIDTH = (ECC == "OFF")?
                           0 : (DATA_WIDTH <= 4)?
                            4 : (DATA_WIDTH <= 10)?
                             5 : (DATA_WIDTH <= 26)?
                              6 : (DATA_WIDTH <= 57)?
                               7 : (DATA_WIDTH <= 120)?
                                8 : (DATA_WIDTH <= 247)?
                                 9 : 10;
  localparam ECC_TEST              = "OFF";
  localparam RANK_WIDTH = clogb2(RANKS);
  localparam DATA_BUF_OFFSET_WIDTH = 1;
  localparam MC_ERR_ADDR_WIDTH = ((CS_WIDTH == 1) ? 0 : RANK_WIDTH)
                                 + BANK_WIDTH + ROW_WIDTH + COL_WIDTH
                                 + DATA_BUF_OFFSET_WIDTH;
  localparam tPRDI                 = 1_000_000;
                                     // memory tPRDI paramter in pS.
  localparam PAYLOAD_WIDTH         = (ECC_TEST == "OFF") ? DATA_WIDTH : DQ_WIDTH;
  localparam BURST_LENGTH          = STR_TO_INT(BURST_MODE);
  localparam APP_DATA_WIDTH        = 2 * nCK_PER_CLK * PAYLOAD_WIDTH;
  localparam APP_MASK_WIDTH        = APP_DATA_WIDTH / 8;

  //***************************************************************************
  // Traffic Gen related parameters (derived)
  //***************************************************************************
  localparam  TG_ADDR_WIDTH = ((CS_WIDTH == 1) ? 0 : RANK_WIDTH)
                                 + BANK_WIDTH + ROW_WIDTH + COL_WIDTH;
  localparam MASK_SIZE             = DATA_WIDTH/8;
  localparam DBG_WR_STS_WIDTH      = 32;
  localparam DBG_RD_STS_WIDTH      = 32;	


//------------------------------------------  
    

        //--------------axi interface
    input                                          	mv_col_axi_awid    ;// = 0;
    input      [7:0]                               	mv_col_axi_awlen   ;// = MV_COL_AXI_AX_LEN;
    input      [2:0]                               	mv_col_axi_awsize  ;// = MV_COL_AXI_AX_SIZE;
    input      [1:0]                               	mv_col_axi_awburst ;// = `AX_BURST_INC;
    input                        	             	mv_col_axi_awlock  ;// = `AX_LOCK_DEFAULT;
    input      [3:0]                             	mv_col_axi_awcache ;// = `AX_CACHE_DEFAULT;
    input      [2:0]                               	mv_col_axi_awprot  ;// = `AX_PROT_DATA;
    input                                          	mv_col_axi_awvalid;
    input      [31:0]                              	mv_col_axi_awaddr;

    output                       	                mv_col_axi_awready;

    // write data channel
    input      [64-1:0]                            	mv_col_axi_wstrb;// = 64'hFFFFF_FFFFF_FFFFF_FFFFF_FFFFF;       // 40 bytes
    input                                          	mv_col_axi_wlast;
    input                                          	mv_col_axi_wvalid;
    input      [MV_COL_AXI_DATA_WIDTH -1:0]        	mv_col_axi_wdata;

    output	                                        mv_col_axi_wready;

    //write response channel
    // output                       	                mv_col_axi_bid;
    output       [1:0]                            	mv_col_axi_bresp;
    output                       	                mv_col_axi_bvalid;
    input                                          	mv_col_axi_bready;  

    input	    [AXI_ADDR_WDTH-1:0]		            mv_pref_axi_araddr  ;
    input wire [7:0]					            mv_pref_axi_arlen   ;
    input wire	[2:0]					            mv_pref_axi_arsize  ;
    input wire [1:0]					            mv_pref_axi_arburst ;
    input wire [2:0]					            mv_pref_axi_arprot  ;
    input 	   						                mv_pref_axi_arvalid ;
    output 								            mv_pref_axi_arready ;
            
    output		[MV_COL_AXI_DATA_WIDTH-1:0]		    mv_pref_axi_rdata   ;
    output		[1:0]					            mv_pref_axi_rresp   ;
    output 								            mv_pref_axi_rlast   ;
    output								            mv_pref_axi_rvalid  ;
    input 						                    mv_pref_axi_rready  ;

    input                        	                mv_pref_axi_arlock  ;
    input                                          	mv_pref_axi_arid    ;    
    input      [3:0]                               	mv_pref_axi_arcache ;     
    
    // axi master interface         
    input      [AXI_ADDR_WDTH-1:0]                 	ref_pix_axi_ar_addr     ;
    input      [7:0]                               	ref_pix_axi_ar_len      ;
    input      [2:0]                               	ref_pix_axi_ar_size     ;
    input      [1:0]                               	ref_pix_axi_ar_burst    ;
    input      [2:0]                               	ref_pix_axi_ar_prot     ;
    input                                          	ref_pix_axi_ar_valid    ;
    output                                     		ref_pix_axi_ar_ready    ;
                
    output       [AXI_CACHE_DATA_WDTH-1:0]      	ref_pix_axi_r_data      ;
    output       [1:0]                          	ref_pix_axi_r_resp      ;
    output                                      	ref_pix_axi_r_last      ;
    output                                      	ref_pix_axi_r_valid     ;
    input                                          	ref_pix_axi_r_ready     ;   
	
	
    input                                      		pixel_write_axi_awid    	;
    input      [7:0]                           		pixel_write_axi_awlen   	;
    input      [2:0]                           		pixel_write_axi_awsize  	;
    input      [1:0]                           		pixel_write_axi_awburst 	;
    input                        	            	pixel_write_axi_awlock  	;
    input      [3:0]                           		pixel_write_axi_awcache 	;
    input      [2:0]                           		pixel_write_axi_awprot  	;
    input 		                            		pixel_write_axi_awvalid	;
    input 		[AXI_ADDR_WDTH-1:0]             	pixel_write_axi_awaddr	;
    output                       	            	pixel_write_axi_awready	;
	
    // write data channel	
    input 	     [AXI_CACHE_DATA_WDTH/8-1:0]		pixel_write_axi_wstrb		;
    input 	                                		pixel_write_axi_wlast		;
    input 	                                		pixel_write_axi_wvalid	;
    input 	    [AXI_CACHE_DATA_WDTH -1:0]			pixel_write_axi_wdata		;
	
    output	                                    	pixel_write_axi_wready	;
	
    //write response channel	
    output                       	            	pixel_write_axi_bid		;
    output       [1:0]                           	pixel_write_axi_bresp		;
    output                       	            	pixel_write_axi_bvalid	;
    input  	                                		pixel_write_axi_bready	; 
	
	     
    //-----------end of axi interface

    wire M00_AXI_ARESET_OUT_N                                              ;
    // input M00_AXI_ACLK                                                       ;
    wire  [3 : 0] M00_AXI_AWID                                              ;
    wire  [31 : 0] M00_AXI_AWADDR                                           ;
    wire  [7 : 0] M00_AXI_AWLEN                                             ;
    wire  [2 : 0] M00_AXI_AWSIZE                                            ;
    wire  [1 : 0] M00_AXI_AWBURST                                           ;
    wire  M00_AXI_AWLOCK                                                    ;
    wire  [3 : 0] M00_AXI_AWCACHE                                           ;
    wire  [2 : 0] M00_AXI_AWPROT                                            ;
    wire  [3 : 0] M00_AXI_AWQOS                                             ;
    wire  M00_AXI_AWVALID                                                   ;
    wire  M00_AXI_AWREADY                                                    ;
    wire  [AXI_MIG_DATA_WIDTH-1 : 0] M00_AXI_WDATA                                           ;
    wire  [AXI_MIG_DATA_WIDTH/8-1 : 0] M00_AXI_WSTRB                                            ;
    wire  M00_AXI_WLAST                                                     ;
    wire  M00_AXI_WVALID                                                    ;
    wire  M00_AXI_WREADY                                                     ;
    wire  [3 : 0] M00_AXI_BID                                                ;
    wire  [1 : 0] M00_AXI_BRESP                                              ;
    wire  M00_AXI_BVALID                                                     ;
    wire  M00_AXI_BREADY                                                    ;
    wire  [3 : 0] M00_AXI_ARID                                              ;
    wire  [31 : 0] M00_AXI_ARADDR                                           ;
    wire  [7 : 0] M00_AXI_ARLEN                                             ;
    wire  [2 : 0] M00_AXI_ARSIZE                                            ;
    wire  [1 : 0] M00_AXI_ARBURST                                           ;
    wire  M00_AXI_ARLOCK                                                    ;
    wire  [3 : 0] M00_AXI_ARCACHE                                           ;
    wire  [2 : 0] M00_AXI_ARPROT                                            ;
    wire  [3 : 0] M00_AXI_ARQOS                                             ;
    wire  M00_AXI_ARVALID                                                   ;
    wire  M00_AXI_ARREADY                                                    ;
    wire  [3 : 0] M00_AXI_RID                                                ;
    wire  [AXI_MIG_DATA_WIDTH-1 : 0] M00_AXI_RDATA                                            ;
    wire  [1 : 0] M00_AXI_RRESP                                              ;
    wire  M00_AXI_RLAST                                                      ;
    wire  M00_AXI_RVALID                                                     ;
    wire  M00_AXI_RREADY                                                    ;
	
	wire clk;
	wire pll_locked;
    output clk_200;
	output design_clk;
	// output reset_out;
	
	
	// assign reset_out = reset;
    wire reset;
	reg [7:0] reset_counter;
    output reg calib_complete_reset;
	output reg up_stream_reset;
	reg calib_complete_reset1;
	reg calib_complete_reset2;
	initial begin
		calib_complete_reset1 = 1;
		calib_complete_reset2 = 1;
		calib_complete_reset = 1;
		up_stream_reset = 1;
	end
	
    always@(posedge clk) begin
		if(~((init_calib_complete)  && (pll_locked) && (reset==0))) begin
			calib_complete_reset1 <= 1;
			reset_counter <= 0;
			up_stream_reset <= 1;
		end
		else begin
			if(reset_counter <20) begin
				reset_counter <= reset_counter + 1'b1;
				calib_complete_reset1 <= 1;
				up_stream_reset <= 1;
			end
			else begin
				calib_complete_reset1 <= 0;
				up_stream_reset <= 0;
			end
		end
    end
	
	always@(posedge design_clk) begin
		calib_complete_reset2 <= calib_complete_reset1;
		calib_complete_reset <= calib_complete_reset2;		// synchronizer flops
	end
`ifdef AXI_INTER_CON_CDC
	// Ethernet is reset until inti calib complete to avoid fifo getting full during calib period
	assign clk_200 = clk;
	
  clock_gen clock_gen_block
   (// Clock in ports
    .CLK_IN1(clk),      // IN
    // Clock out ports
    .CLK_OUT1(design_clk),     // OUT
    // Status and control signals
    .RESET(reset),// IN
    .LOCKED(pll_locked));      // OUT
	
`else 
	assign pll_locked = 1;
	assign design_clk = clk;
  clock_gen_for_eth eth_clock_gen_block
   (// Clock in ports
    .CLK_IN1(clk),      // IN
    // Clock out ports
    .CLK_OUT1(clk_200),     // OUT
    // Status and control signals
    .RESET(reset)// IN
    //,.LOCKED(LOCKED)
	 );      // OUT
// INST_TAG_END ------ End INSTANTIATION Template ---------

`endif

    reg aresetn;
	always @(posedge clk) begin
		aresetn <= ~reset;
	end


`ifdef ETHERNET_ONLY_TEST
	assign init_calib_complete = 1;
`else
axi_inter_con axi_inter_con_block (
  .INTERCONNECT_ACLK        (clk), // input INTERCONNECT_ACLK
  .INTERCONNECT_ARESETN     (!calib_complete_reset), // input INTERCONNECT_ARESETN
  .S00_AXI_ARESET_OUT_N     (), // output S00_AXI_ARESET_OUT_N
  .S00_AXI_ACLK             (design_clk), // input S00_AXI_ACLK
  .S00_AXI_AWID             (), // input [0 : 0] S00_AXI_AWID
  .S00_AXI_AWADDR           (), // input [31 : 0] S00_AXI_AWADDR
  .S00_AXI_AWLEN            (), // input [7 : 0] S00_AXI_AWLEN
  .S00_AXI_AWSIZE           (), // input [2 : 0] S00_AXI_AWSIZE
  .S00_AXI_AWBURST          (), // input [1 : 0] S00_AXI_AWBURST
  .S00_AXI_AWLOCK           (), // input S00_AXI_AWLOCK
  .S00_AXI_AWCACHE          (), // input [3 : 0] S00_AXI_AWCACHE
  .S00_AXI_AWPROT           (), // input [2 : 0] S00_AXI_AWPROT
  .S00_AXI_AWQOS            (), // input [3 : 0] S00_AXI_AWQOS
  .S00_AXI_AWVALID          (1'b0), // input S00_AXI_AWVALID
  .S00_AXI_AWREADY          (), // output S00_AXI_AWREADY
  .S00_AXI_WDATA            (), // input [511 : 0] S00_AXI_WDATA
  .S00_AXI_WSTRB            (), // input [63 : 0] S00_AXI_WSTRB
  .S00_AXI_WLAST            (), // input S00_AXI_WLAST
  .S00_AXI_WVALID           (1'b0), // input S00_AXI_WVALID
  .S00_AXI_WREADY           (), // output S00_AXI_WREADY
  .S00_AXI_BID              (), // output [0 : 0] S00_AXI_BID
  .S00_AXI_BRESP            (), // output [1 : 0] S00_AXI_BRESP
  .S00_AXI_BVALID           (), // output S00_AXI_BVALID
  .S00_AXI_BREADY           (), // input S00_AXI_BREADY


  .S00_AXI_ARID             (1'b0),                 // input [0 : 0] S00_AXI_ARID
  .S00_AXI_ARADDR           (ref_pix_axi_ar_addr),                // input [31 : 0] S00_AXI_ARADDR
  .S00_AXI_ARLEN            (ref_pix_axi_ar_len),               // input [7 : 0] S00_AXI_ARLEN
  .S00_AXI_ARSIZE           (ref_pix_axi_ar_size),                       // input [2 : 0] S00_AXI_ARSIZE
  .S00_AXI_ARBURST          (ref_pix_axi_ar_burst),                           // input [1 : 0] S00_AXI_ARBURST
  .S00_AXI_ARLOCK           (1'b0),                                           // input S00_AXI_ARLOCK
  .S00_AXI_ARCACHE          (4'd0),                              // input [3 : 0] S00_AXI_ARCACHE
  .S00_AXI_ARPROT           (ref_pix_axi_ar_prot),                                       // input [2 : 0] S00_AXI_ARPROT
  .S00_AXI_ARQOS            (4'd0),                                            // input [3 : 0] S00_AXI_ARQOS
  .S00_AXI_ARVALID          (ref_pix_axi_ar_valid),                                   // input S00_AXI_ARVALID
  .S00_AXI_ARREADY          (ref_pix_axi_ar_ready),                                           // output S00_AXI_ARREADY
  .S00_AXI_RID              (),                              // output [0 : 0] S00_AXI_RID
  .S00_AXI_RDATA            (ref_pix_axi_r_data),                                    // output [511 : 0] S00_AXI_RDATA
  .S00_AXI_RRESP            (ref_pix_axi_r_resp),                                    // output [1 : 0] S00_AXI_RRESP
  .S00_AXI_RLAST            (ref_pix_axi_r_last),                                    // output S00_AXI_RLAST
  .S00_AXI_RVALID           (ref_pix_axi_r_valid),                            // output S00_AXI_RVALID
  .S00_AXI_RREADY           (ref_pix_axi_r_ready),                           // input S00_AXI_RREADY
  
  .S01_AXI_ARESET_OUT_N       (), // output S01_AXI_ARESET_OUT_N
  .S01_AXI_ACLK             (design_clk                        ), // input S01_AXI_ACLK
  .S01_AXI_AWID             (pixel_write_axi_awid        ),                             // input [0 : 0] S01_AXI_AWID
  .S01_AXI_AWADDR           (pixel_write_axi_awaddr      ),                       // input [31 : 0] S01_AXI_AWADDR
  .S01_AXI_AWLEN            (pixel_write_axi_awlen       ),                       // input [7 : 0] S01_AXI_AWLEN
  .S01_AXI_AWSIZE           (pixel_write_axi_awsize      ),                       // input [2 : 0] S01_AXI_AWSIZE
  .S01_AXI_AWBURST          (pixel_write_axi_awburst     ),                       // input [1 : 0] S01_AXI_AWBURST
  .S01_AXI_AWLOCK           (pixel_write_axi_awlock      ),                       // input S01_AXI_AWLOCK
  .S01_AXI_AWCACHE          (pixel_write_axi_awcache     ),                       // input [3 : 0] S01_AXI_AWCACHE
  .S01_AXI_AWPROT           (pixel_write_axi_awprot      ),                       // input [2 : 0] S01_AXI_AWPROT
  .S01_AXI_AWQOS            (4'd0                          ),                       // input [3 : 0] S01_AXI_AWQOS
  .S01_AXI_AWVALID          (pixel_write_axi_awvalid     ),                       // input S01_AXI_AWVALID
  .S01_AXI_AWREADY          (pixel_write_axi_awready     ),                       // output S01_AXI_AWREADY
  .S01_AXI_WDATA            (pixel_write_axi_wdata                               ),                       // input [511 : 0] S01_AXI_WDATA
  .S01_AXI_WSTRB            (pixel_write_axi_wstrb       ),                       // input [63 : 0] S01_AXI_WSTRB
  .S01_AXI_WLAST            (pixel_write_axi_wlast       ),                     // input S01_AXI_WLAST
  .S01_AXI_WVALID           (pixel_write_axi_wvalid       ),                       // input S01_AXI_WVALID
  .S01_AXI_WREADY           (pixel_write_axi_wready       ),                       // output S01_AXI_WREADY
  .S01_AXI_BID              (pixel_write_axi_bid         ),                      // output [0 : 0] S01_AXI_BID
  .S01_AXI_BRESP            (pixel_write_axi_bresp       ),                        // output [1 : 0] S01_AXI_BRESP
  .S01_AXI_BVALID           (pixel_write_axi_bvalid       ),                           // output S01_AXI_BVALID
  .S01_AXI_BREADY           (pixel_write_axi_bready       ),                           // input S01_AXI_BREADY
 
  .S01_AXI_ARID             (), // input [0 : 0] S01_AXI_ARID
  .S01_AXI_ARADDR           (), // input [31 : 0] S01_AXI_ARADDR
  .S01_AXI_ARLEN            (), // input [7 : 0] S01_AXI_ARLEN
  .S01_AXI_ARSIZE           (), // input [2 : 0] S01_AXI_ARSIZE
  .S01_AXI_ARBURST          (), // input [1 : 0] S01_AXI_ARBURST
  .S01_AXI_ARLOCK           (), // input S01_AXI_ARLOCK
  .S01_AXI_ARCACHE          (), // input [3 : 0] S01_AXI_ARCACHE
  .S01_AXI_ARPROT           (), // input [2 : 0] S01_AXI_ARPROT
  .S01_AXI_ARQOS            (), // input [3 : 0] S01_AXI_ARQOS
  .S01_AXI_ARVALID          (1'b0), // input S01_AXI_ARVALID
  .S01_AXI_ARREADY          (), // output S01_AXI_ARREADY
  .S01_AXI_RID              (), // output [0 : 0] S01_AXI_RID
  .S01_AXI_RDATA            (), // output [511 : 0] S01_AXI_RDATA
  .S01_AXI_RRESP            (), // output [1 : 0] S01_AXI_RRESP
  .S01_AXI_RLAST            (), // output S01_AXI_RLAST
  .S01_AXI_RVALID           (), // output S01_AXI_RVALID
  .S01_AXI_RREADY           (1'b0), // input S01_AXI_RREADY
  .S02_AXI_ARESET_OUT_N       (),                               // output S02_AXI_ARESET_OUT_N
  .S02_AXI_ACLK             (design_clk),                     // input S02_AXI_ACLK
  .S02_AXI_AWID             (),                     // input [0 : 0] S02_AXI_AWID
  .S02_AXI_AWADDR           (),                    // input [31 : 0] S02_AXI_AWADDR
  .S02_AXI_AWLEN            (),                    // input [7 : 0] S02_AXI_AWLEN
  .S02_AXI_AWSIZE           (),                    // input [2 : 0] S02_AXI_AWSIZE
  .S02_AXI_AWBURST          (),                    // input [1 : 0] S02_AXI_AWBURST
  .S02_AXI_AWLOCK           (),                    // input S02_AXI_AWLOCK
  .S02_AXI_AWCACHE          (),                    // input [3 : 0] S02_AXI_AWCACHE
  .S02_AXI_AWPROT           (),                    // input [2 : 0] S02_AXI_AWPROT
  .S02_AXI_AWQOS            (),                    // input [3 : 0] S02_AXI_AWQOS
  .S02_AXI_AWVALID          (1'b0),                    // input S02_AXI_AWVALID
  .S02_AXI_AWREADY          (),                    // output S02_AXI_AWREADY
  .S02_AXI_WDATA            (),                    // input [511 : 0] S02_AXI_WDATA
  .S02_AXI_WSTRB            (),                    // input [63 : 0] S02_AXI_WSTRB
  .S02_AXI_WLAST            (),                    // input S02_AXI_WLAST
  .S02_AXI_WVALID           (1'b0),                    // input S02_AXI_WVALID
  .S02_AXI_WREADY           (),                    // output S02_AXI_WREADY
  .S02_AXI_BID              (),                     //output [0 : 0] S02_AXI_BID
  .S02_AXI_BRESP            (),                    // output [1 : 0] S02_AXI_BRESP
  .S02_AXI_BVALID           (),                    // output S02_AXI_BVALID
  .S02_AXI_BREADY           (),                    // input S02_AXI_BREADY
    
  .S02_AXI_ARID             (1'b0),                    // input [0 : 0] S02_AXI_ARID
  .S02_AXI_ARADDR           (mv_pref_axi_araddr),                    // input [31 : 0] S02_AXI_ARADDR
  .S02_AXI_ARLEN            (mv_pref_axi_arlen),                    // input [7 : 0] S02_AXI_ARLEN
  .S02_AXI_ARSIZE           (mv_pref_axi_arsize),                    // input [2 : 0] S02_AXI_ARSIZE
  .S02_AXI_ARBURST          (mv_pref_axi_arburst),                    // input [1 : 0] S02_AXI_ARBURST
  .S02_AXI_ARLOCK           (1'b0),                    // input S02_AXI_ARLOCK
  .S02_AXI_ARCACHE          (4'd0),                    // input [3 : 0] S02_AXI_ARCACHE
  .S02_AXI_ARPROT           (mv_pref_axi_arprot),                    // input [2 : 0] S02_AXI_ARPROT
  .S02_AXI_ARQOS            (4'd0),                    // input [3 : 0] S02_AXI_ARQOS
  .S02_AXI_ARVALID          (mv_pref_axi_arvalid),                    // input S02_AXI_ARVALID
  .S02_AXI_ARREADY          (mv_pref_axi_arready),                    // output S02_AXI_ARREADY
  .S02_AXI_RID              (),                     //output [0 : 0] S02_AXI_RID
  .S02_AXI_RDATA            (mv_pref_axi_rdata),                    // output [511 : 0] S02_AXI_RDATA
  .S02_AXI_RRESP            (mv_pref_axi_rresp),                    // output [1 : 0] S02_AXI_RRESP
  .S02_AXI_RLAST            (mv_pref_axi_rlast),                    // output S02_AXI_RLAST
  .S02_AXI_RVALID           (mv_pref_axi_rvalid),                    // output S02_AXI_RVALID
  .S02_AXI_RREADY           (mv_pref_axi_rready),                    // input S02_AXI_RREADY
  .S03_AXI_ARESET_OUT_N     (),                     // output S03_AXI_ARESET_OUT_N
  .S03_AXI_ACLK             (design_clk),                 // input S03_AXI_ACLK
  

  .S03_AXI_AWID             (mv_col_axi_awid),                 // input [0 : 0] S03_AXI_AWID
  .S03_AXI_AWADDR           (mv_col_axi_awaddr),                 // input [31 : 0] S03_AXI_AWADDR
  .S03_AXI_AWLEN            (mv_col_axi_awlen),                 // input [7 : 0] S03_AXI_AWLEN
  .S03_AXI_AWSIZE           (mv_col_axi_awsize) ,                // input [2 : 0] S03_AXI_AWSIZE
  .S03_AXI_AWBURST          (mv_col_axi_awburst),                 // input [1 : 0] S03_AXI_AWBURST
  .S03_AXI_AWLOCK           (mv_col_axi_awlock),                 // input S03_AXI_AWLOCK
  .S03_AXI_AWCACHE          (mv_col_axi_awcache),                 // input [3 : 0] S03_AXI_AWCACHE
  .S03_AXI_AWPROT           (mv_col_axi_awprot) ,                // input [2 : 0] S03_AXI_AWPROT
  .S03_AXI_AWQOS            (4'd0),                 // input [3 : 0] S03_AXI_AWQOS
  .S03_AXI_AWVALID          (mv_col_axi_awvalid),                 // input S03_AXI_AWVALID
  .S03_AXI_AWREADY          (mv_col_axi_awready),                 // output S03_AXI_AWREADY
  .S03_AXI_WDATA            (mv_col_axi_wdata),                 // input [511 : 0] S03_AXI_WDATA
  .S03_AXI_WSTRB            (mv_col_axi_wstrb),                 // input [63 : 0] S03_AXI_WSTRB
  .S03_AXI_WLAST            (mv_col_axi_wlast),                 // input S03_AXI_WLAST
  .S03_AXI_WVALID           (mv_col_axi_wvalid),                 // input S03_AXI_WVALID
  .S03_AXI_WREADY           (mv_col_axi_wready),                 // output S03_AXI_WREADY
  
  .S03_AXI_BID              (),                 // output [0 : 0] S03_AXI_BID
  .S03_AXI_BRESP            (mv_col_axi_bresp),                 // output [1 : 0] S03_AXI_BRESP
  .S03_AXI_BVALID           (mv_col_axi_bvalid) ,                // output S03_AXI_BVALID
  .S03_AXI_BREADY           (mv_col_axi_bready) ,                // input S03_AXI_BREADY
  .S03_AXI_ARID             (),                 // input [0 : 0] S03_AXI_ARID
  .S03_AXI_ARADDR           (),                 // input [31 : 0] S03_AXI_ARADDR
  .S03_AXI_ARLEN            (),                 // input [7 : 0] S03_AXI_ARLEN
  .S03_AXI_ARSIZE           (),                 // input [2 : 0] S03_AXI_ARSIZE
  .S03_AXI_ARBURST          (),                 // input [1 : 0] S03_AXI_ARBURST
  .S03_AXI_ARLOCK           (),                 // input S03_AXI_ARLOCK
  .S03_AXI_ARCACHE          (),                 // input [3 : 0] S03_AXI_ARCACHE
  .S03_AXI_ARPROT           (),                 // input [2 : 0] S03_AXI_ARPROT
  .S03_AXI_ARQOS            (),                 // input [3 : 0] S03_AXI_ARQOS
  .S03_AXI_ARVALID          (1'b0 ),                // input S03_AXI_ARVALID
  .S03_AXI_ARREADY          (),                 // output S03_AXI_ARREADY
  .S03_AXI_RID              (),                // output [0 : 0] S03_AXI_RID
  .S03_AXI_RDATA            (),                 // output [511 : 0] S03_AXI_RDATA
  .S03_AXI_RRESP            (),                 // output [1 : 0] S03_AXI_RRESP
  .S03_AXI_RLAST            (),                 // output S03_AXI_RLAST
  .S03_AXI_RVALID           (),                 // output S03_AXI_RVALID
  .S03_AXI_RREADY           (1'b0),                 // input S03_AXI_RREADY
  .M00_AXI_ARESET_OUT_N     (M00_AXI_ARESET_OUT_N), // output M00_AXI_ARESET_OUT_N
  .M00_AXI_ACLK             (clk), // input M00_AXI_ACLK
  
  .M00_AXI_AWID             (M00_AXI_AWID), // output [3 : 0] M00_AXI_AWID
  .M00_AXI_AWADDR           (M00_AXI_AWADDR), // output [31 : 0] M00_AXI_AWADDR
  .M00_AXI_AWLEN            (M00_AXI_AWLEN), // output [7 : 0] M00_AXI_AWLEN
  .M00_AXI_AWSIZE           (M00_AXI_AWSIZE), // output [2 : 0] M00_AXI_AWSIZE
  .M00_AXI_AWBURST          (M00_AXI_AWBURST), // output [1 : 0] M00_AXI_AWBURST
  .M00_AXI_AWLOCK           (M00_AXI_AWLOCK), // output M00_AXI_AWLOCK
  .M00_AXI_AWCACHE          (M00_AXI_AWCACHE), // output [3 : 0] M00_AXI_AWCACHE
  .M00_AXI_AWPROT           (M00_AXI_AWPROT), // output [2 : 0] M00_AXI_AWPROT
  .M00_AXI_AWQOS            (M00_AXI_AWQOS), // output [3 : 0] M00_AXI_AWQOS
  .M00_AXI_AWVALID          (M00_AXI_AWVALID), // output M00_AXI_AWVALID
  .M00_AXI_AWREADY          (M00_AXI_AWREADY), // input M00_AXI_AWREADY
  .M00_AXI_WDATA            (M00_AXI_WDATA), // output [511 : 0] M00_AXI_WDATA
  .M00_AXI_WSTRB            (M00_AXI_WSTRB), // output [63 : 0] M00_AXI_WSTRB
  .M00_AXI_WLAST            (M00_AXI_WLAST), // output M00_AXI_WLAST
  .M00_AXI_WVALID           (M00_AXI_WVALID), // output M00_AXI_WVALID
  .M00_AXI_WREADY           (M00_AXI_WREADY), // input M00_AXI_WREADY
  .M00_AXI_BID              (M00_AXI_BID), // input [3 : 0] M00_AXI_BID
  .M00_AXI_BRESP            (M00_AXI_BRESP), // input [1 : 0] M00_AXI_BRESP
  .M00_AXI_BVALID           (M00_AXI_BVALID), // input M00_AXI_BVALID
  .M00_AXI_BREADY           (M00_AXI_BREADY), // output M00_AXI_BREADY
  .M00_AXI_ARID             (M00_AXI_ARID), // output [3 : 0] M00_AXI_ARID
  .M00_AXI_ARADDR           (M00_AXI_ARADDR), // output [31 : 0] M00_AXI_ARADDR
  .M00_AXI_ARLEN            (M00_AXI_ARLEN), // output [7 : 0] M00_AXI_ARLEN
  .M00_AXI_ARSIZE           (M00_AXI_ARSIZE), // output [2 : 0] M00_AXI_ARSIZE
  .M00_AXI_ARBURST          (M00_AXI_ARBURST), // output [1 : 0] M00_AXI_ARBURST
  .M00_AXI_ARLOCK           (M00_AXI_ARLOCK), // output M00_AXI_ARLOCK
  .M00_AXI_ARCACHE          (M00_AXI_ARCACHE), // output [3 : 0] M00_AXI_ARCACHE
  .M00_AXI_ARPROT           (M00_AXI_ARPROT), // output [2 : 0] M00_AXI_ARPROT
  .M00_AXI_ARQOS            (M00_AXI_ARQOS), // output [3 : 0] M00_AXI_ARQOS
  .M00_AXI_ARVALID          (M00_AXI_ARVALID), // output M00_AXI_ARVALID
  .M00_AXI_ARREADY          (M00_AXI_ARREADY), // input M00_AXI_ARREADY
  .M00_AXI_RID              (M00_AXI_RID), // input [3 : 0] M00_AXI_RID
  .M00_AXI_RDATA            (M00_AXI_RDATA), // input [511 : 0] M00_AXI_RDATA
  .M00_AXI_RRESP            (M00_AXI_RRESP), // input [1 : 0] M00_AXI_RRESP
  .M00_AXI_RLAST            (M00_AXI_RLAST), // input M00_AXI_RLAST
  .M00_AXI_RVALID           (M00_AXI_RVALID), // input M00_AXI_RVALID
  .M00_AXI_RREADY           (M00_AXI_RREADY) // output M00_AXI_RREADY
);    
    
    
`ifdef SOFT_MEM_SLAVE
	mig_soft_top_module #(
 	.CLKFBOUT_MULT (CLKFBOUT_MULT 	),
	.DIVCLK_DIVIDE (DIVCLK_DIVIDE   ),
	.CLKOUT0_PHASE (CLKOUT0_PHASE   ),
	.CLKOUT0_DIVIDE(CLKOUT0_DIVIDE  ),
	.CLKOUT1_DIVIDE(CLKOUT1_DIVIDE  ),
	.CLKOUT2_DIVIDE(CLKOUT2_DIVIDE  ),
    .CLKOUT3_DIVIDE(CLKOUT3_DIVIDE  ),
	.CLKIN_PERIOD  (CLKIN_PERIOD    )	
	)
	
	mem_slave (
			.clk			(clk),																
			.reset			(reset),     
			.init_calib_complete (init_calib_complete),
			.arid			(M00_AXI_ARID),                                                    	
			.araddr			(M00_AXI_ARADDR),                                                  	
			.arlen			(M00_AXI_ARLEN),                                                   	
			.arsize			(M00_AXI_ARSIZE),                                                  	
			.arburst		(M00_AXI_ARBURST),                                                  
			.arlock			(2'd0),                                                  	
			.arcache		(M00_AXI_ARCACHE),                                                 	             
			.arprot			(M00_AXI_ARPROT),                                                  	
			.arvalid		(M00_AXI_ARVALID),                                                  
			.arready		(M00_AXI_ARREADY),                                                  
			.rid			(M00_AXI_RID),                                                     
			.rdata			(M00_AXI_RDATA),                                                   
			.rresp			(M00_AXI_RRESP),                                                   
			.rlast			(M00_AXI_RLAST),                                                   
			.rvalid			(M00_AXI_RVALID),                                                  
			.rready			(M00_AXI_RREADY),                                                  
			.awid			(M00_AXI_AWID),	                                                 
			.awaddr			(M00_AXI_AWADDR),                                                
			.awlen			(M00_AXI_AWLEN),                                                 
			.awsize			(M00_AXI_AWSIZE),                                                
			.awburst		(M00_AXI_AWBURST),                                       
			.awlock			(2'd0),                                        
			.awcache		(M00_AXI_AWCACHE),                                       
			.awprot			(M00_AXI_AWPROT),                                        
			.awvalid		(M00_AXI_AWVALID),                                       
			.awready		(M00_AXI_AWREADY),                                       
			.wid			(M00_AXI_AWID),                                          
			.wdata			(M00_AXI_WDATA),                                         
			.wstrb			(M00_AXI_WSTRB),                                         
			.wlast			(M00_AXI_WLAST),                                           
			.wvalid			(M00_AXI_WVALID),                                        
			.wready			(M00_AXI_WREADY),                                        
			.bid			(M00_AXI_BID),                                           
			.bresp			(M00_AXI_BRESP),                                         
			.bvalid			(M00_AXI_BVALID),                                        
			.bready			(M00_AXI_BREADY)                                       
		);                                                                           


`else
ddr_mig #
    (
     
     .TCQ                              (TCQ),
     .ADDR_CMD_MODE                    (ADDR_CMD_MODE),
     .AL                               (AL),
     .PAYLOAD_WIDTH                    (PAYLOAD_WIDTH),
     .BANK_WIDTH                       (BANK_WIDTH),
     .BURST_MODE                       (BURST_MODE),
     .BURST_TYPE                       (BURST_TYPE),
     .CA_MIRROR                        (CA_MIRROR),
     .CK_WIDTH                         (CK_WIDTH),
     .COL_WIDTH                        (COL_WIDTH),
     .CMD_PIPE_PLUS1                   (CMD_PIPE_PLUS1),
     .CS_WIDTH                         (CS_WIDTH),
     .nCS_PER_RANK                     (nCS_PER_RANK),
     .CKE_WIDTH                        (CKE_WIDTH),
     .DATA_WIDTH                       (DATA_WIDTH),
     .DATA_BUF_ADDR_WIDTH              (DATA_BUF_ADDR_WIDTH),
     .DQ_CNT_WIDTH                     (DQ_CNT_WIDTH),
     .DQ_PER_DM                        (DQ_PER_DM),
     .DQ_WIDTH                         (DQ_WIDTH),
     .DQS_CNT_WIDTH                    (DQS_CNT_WIDTH),
     .DQS_WIDTH                        (DQS_WIDTH),
     .DRAM_WIDTH                       (DRAM_WIDTH),
     .ECC                              (ECC),
     .ECC_WIDTH                        (ECC_WIDTH),
     .ECC_TEST                         (ECC_TEST),
     .MC_ERR_ADDR_WIDTH                (MC_ERR_ADDR_WIDTH),
     .nAL                              (nAL),
     .nBANK_MACHS                      (nBANK_MACHS),
     .CKE_ODT_AUX                      (CKE_ODT_AUX),
     .ORDERING                         (ORDERING),
     .OUTPUT_DRV                       (OUTPUT_DRV),
     .IBUF_LPWR_MODE                   (IBUF_LPWR_MODE),
     .IODELAY_HP_MODE                  (IODELAY_HP_MODE),
     .DATA_IO_IDLE_PWRDWN              (DATA_IO_IDLE_PWRDWN),
     .BANK_TYPE                        (BANK_TYPE),
     .DATA_IO_PRIM_TYPE                (DATA_IO_PRIM_TYPE),
     .REG_CTRL                         (REG_CTRL),
     .RTT_NOM                          (RTT_NOM),
     .RTT_WR                           (RTT_WR),
     .CL                               (CL),
     .CWL                              (CWL),
     .tCKE                             (tCKE),
     .tFAW                             (tFAW),
     .tPRDI                            (tPRDI),
     .tRAS                             (tRAS),
     .tRCD                             (tRCD),
     .tREFI                            (tREFI),
     .tRFC                             (tRFC),
     .tRP                              (tRP),
     .tRRD                             (tRRD),
     .tRTP                             (tRTP),
     .tWTR                             (tWTR),
     .tZQI                             (tZQI),
     .tZQCS                            (tZQCS),
     .USER_REFRESH                     (USER_REFRESH),
     .WRLVL                            (WRLVL),
     .DEBUG_PORT                       (DEBUG_PORT),
     .RANKS                            (RANKS),
     .ODT_WIDTH                        (ODT_WIDTH),
     .ROW_WIDTH                        (ROW_WIDTH),
     .ADDR_WIDTH                       (ADDR_WIDTH),
     .SIM_BYPASS_INIT_CAL              (SIM_BYPASS_INIT_CAL),
     .SIMULATION                       (SIMULATION),
     .BYTE_LANES_B0                    (BYTE_LANES_B0),
     .BYTE_LANES_B1                    (BYTE_LANES_B1),
     .BYTE_LANES_B2                    (BYTE_LANES_B2),
     .BYTE_LANES_B3                    (BYTE_LANES_B3),
     .BYTE_LANES_B4                    (BYTE_LANES_B4),
     .DATA_CTL_B0                      (DATA_CTL_B0),
     .DATA_CTL_B1                      (DATA_CTL_B1),
     .DATA_CTL_B2                      (DATA_CTL_B2),
     .DATA_CTL_B3                      (DATA_CTL_B3),
     .DATA_CTL_B4                      (DATA_CTL_B4),
     .PHY_0_BITLANES                   (PHY_0_BITLANES),
     .PHY_1_BITLANES                   (PHY_1_BITLANES),
     .PHY_2_BITLANES                   (PHY_2_BITLANES),
     .CK_BYTE_MAP                      (CK_BYTE_MAP),
     .ADDR_MAP                         (ADDR_MAP),
     .BANK_MAP                         (BANK_MAP),
     .CAS_MAP                          (CAS_MAP),
     .CKE_ODT_BYTE_MAP                 (CKE_ODT_BYTE_MAP),
     .CKE_MAP                          (CKE_MAP),
     .ODT_MAP                          (ODT_MAP),
     .CS_MAP                           (CS_MAP),
     .PARITY_MAP                       (PARITY_MAP),
     .RAS_MAP                          (RAS_MAP),
     .WE_MAP                           (WE_MAP),
     .DQS_BYTE_MAP                     (DQS_BYTE_MAP),
     .DATA0_MAP                        (DATA0_MAP),
     .DATA1_MAP                        (DATA1_MAP),
     .DATA2_MAP                        (DATA2_MAP),
     .DATA3_MAP                        (DATA3_MAP),
     .DATA4_MAP                        (DATA4_MAP),
     .DATA5_MAP                        (DATA5_MAP),
     .DATA6_MAP                        (DATA6_MAP),
     .DATA7_MAP                        (DATA7_MAP),
     .DATA8_MAP                        (DATA8_MAP),
     .DATA9_MAP                        (DATA9_MAP),
     .DATA10_MAP                       (DATA10_MAP),
     .DATA11_MAP                       (DATA11_MAP),
     .DATA12_MAP                       (DATA12_MAP),
     .DATA13_MAP                       (DATA13_MAP),
     .DATA14_MAP                       (DATA14_MAP),
     .DATA15_MAP                       (DATA15_MAP),
     .DATA16_MAP                       (DATA16_MAP),
     .DATA17_MAP                       (DATA17_MAP),
     .MASK0_MAP                        (MASK0_MAP),
     .MASK1_MAP                        (MASK1_MAP),
     .CALIB_ROW_ADD                    (CALIB_ROW_ADD),
     .CALIB_COL_ADD                    (CALIB_COL_ADD),
     .CALIB_BA_ADD                     (CALIB_BA_ADD),
     .SLOT_0_CONFIG                    (SLOT_0_CONFIG),
     .SLOT_1_CONFIG                    (SLOT_1_CONFIG),
     .MEM_ADDR_ORDER                   (MEM_ADDR_ORDER),
     .USE_CS_PORT                      (USE_CS_PORT),
     .USE_DM_PORT                      (USE_DM_PORT),
     .USE_ODT_PORT                     (USE_ODT_PORT),
     .PHY_CONTROL_MASTER_BANK          (PHY_CONTROL_MASTER_BANK),
     .TEMP_MON_CONTROL                 (TEMP_MON_CONTROL),
      
     
     .DM_WIDTH                         (DM_WIDTH),
     
     .nCK_PER_CLK                      (nCK_PER_CLK),
     .tCK                              (tCK),
     .DIFF_TERM_SYSCLK                 (DIFF_TERM_SYSCLK),
     .CLKIN_PERIOD                     (CLKIN_PERIOD),
     .CLKFBOUT_MULT                    (CLKFBOUT_MULT),
     .DIVCLK_DIVIDE                    (DIVCLK_DIVIDE),
     .CLKOUT0_PHASE                    (CLKOUT0_PHASE),
     .CLKOUT0_DIVIDE                   (CLKOUT0_DIVIDE),
     .CLKOUT1_DIVIDE                   (CLKOUT1_DIVIDE),
     .CLKOUT2_DIVIDE                   (CLKOUT2_DIVIDE),
     .CLKOUT3_DIVIDE                   (CLKOUT3_DIVIDE),
     
     .UI_EXTRA_CLOCKS                 (UI_EXTRA_CLOCKS),
     .C_S_AXI_ID_WIDTH                 (C_S_AXI_ID_WIDTH),
     .C_S_AXI_ADDR_WIDTH               (C_S_AXI_ADDR_WIDTH),
     .C_S_AXI_DATA_WIDTH               (C_S_AXI_DATA_WIDTH),
     .C_MC_nCK_PER_CLK                 (C_MC_nCK_PER_CLK),
     .C_S_AXI_SUPPORTS_NARROW_BURST    (C_S_AXI_SUPPORTS_NARROW_BURST),
     .C_RD_WR_ARB_ALGORITHM            (C_RD_WR_ARB_ALGORITHM),
     .C_S_AXI_REG_EN0                  (C_S_AXI_REG_EN0),
     .C_S_AXI_REG_EN1                  (C_S_AXI_REG_EN1),
     .C_S_AXI_CTRL_ADDR_WIDTH          (C_S_AXI_CTRL_ADDR_WIDTH),
     .C_S_AXI_CTRL_DATA_WIDTH          (C_S_AXI_CTRL_DATA_WIDTH),
     .C_S_AXI_BASEADDR                 (C_S_AXI_BASEADDR),
     .C_ECC_ONOFF_RESET_VALUE          (C_ECC_ONOFF_RESET_VALUE),
     .C_ECC_CE_COUNTER_WIDTH           (C_ECC_CE_COUNTER_WIDTH),
      
     
     .SYSCLK_TYPE                      (SYSCLK_TYPE),
     .REFCLK_TYPE                      (REFCLK_TYPE),
     .SYS_RST_PORT                     (SYS_RST_PORT),
     .REFCLK_FREQ                      (REFCLK_FREQ),
     .DIFF_TERM_REFCLK                 (DIFF_TERM_REFCLK),
     .IODELAY_GRP                      (IODELAY_GRP),
      
     .CAL_WIDTH                        (CAL_WIDTH),
     .STARVE_LIMIT                     (STARVE_LIMIT),
     .DRAM_TYPE                        (DRAM_TYPE),
      
      
     .RST_ACT_LOW                      (RST_ACT_LOW)
     )
    u_DDR_MIG
      (
       
       
// Memory interface ports
       .ddr3_addr                      (ddr3_addr),
       .ddr3_ba                        (ddr3_ba),
       .ddr3_cas_n                     (ddr3_cas_n),
       .ddr3_ck_n                      (ddr3_ck_n),
       .ddr3_ck_p                      (ddr3_ck_p),
       .ddr3_cke                       (ddr3_cke),
       .ddr3_ras_n                     (ddr3_ras_n),
       .ddr3_reset_n                   (ddr3_reset_n),
       .ddr3_we_n                      (ddr3_we_n),
       .ddr3_dq                        (ddr3_dq),
       .ddr3_dqs_n                     (ddr3_dqs_n),
       .ddr3_dqs_p                     (ddr3_dqs_p),
       .init_calib_complete            (init_calib_complete),
      
       .ddr3_cs_n                      (ddr3_cs_n),
       .ddr3_dm                        (ddr3_dm),
       .ddr3_odt                       (ddr3_odt),
// Application interface ports
       .ui_clk                         (clk),
       .ui_clk_sync_rst                (reset),

       .mmcm_locked                    (mmcm_locked),
       .aresetn                        (aresetn),
       .app_sr_req                     (1'b0),
       .app_sr_active                  (app_sr_active),
       .app_ref_req                    (1'b0),
       .app_ref_ack                    (app_ref_ack),
       .app_zq_req                     (1'b0),
       .app_zq_ack                     (app_zq_ack),

// Slave Interface Write Address Ports

       .s_axi_awid                     (M00_AXI_AWID),			
       .s_axi_awaddr                   (M00_AXI_AWADDR), 
       .s_axi_awlen                    (M00_AXI_AWLEN),  
       .s_axi_awsize                   (M00_AXI_AWSIZE), 
       .s_axi_awburst                  (M00_AXI_AWBURST),
       .s_axi_awlock                   (M00_AXI_AWLOCK), 
       .s_axi_awcache                  (M00_AXI_AWCACHE),
       .s_axi_awprot                   (M00_AXI_AWPROT), 
       .s_axi_awqos                    (4'h0),           
       .s_axi_awvalid                  (M00_AXI_AWVALID),
       .s_axi_awready                  (M00_AXI_AWREADY),
// Slave Interface Write Data Ports    
       .s_axi_wdata                    (M00_AXI_WDATA),  
       .s_axi_wstrb                    (M00_AXI_WSTRB),  
       .s_axi_wlast                    (M00_AXI_WLAST),  
       .s_axi_wvalid                   (M00_AXI_WVALID), 
       .s_axi_wready                   (M00_AXI_WREADY), 
// Slave Interface Write Response Ports
       .s_axi_bid                      (M00_AXI_BID),    
       .s_axi_bresp                    (M00_AXI_BRESP),  
       .s_axi_bvalid                   (M00_AXI_BVALID), 
       .s_axi_bready                   (M00_AXI_BREADY), 
// Slave Interface Read Address Ports  
       .s_axi_arid                     (M00_AXI_ARID),   
       .s_axi_araddr                   (M00_AXI_ARADDR), 
       .s_axi_arlen                    (M00_AXI_ARLEN),  
       .s_axi_arsize                   (M00_AXI_ARSIZE), 
       .s_axi_arburst                  (M00_AXI_ARBURST),
       .s_axi_arlock                   (M00_AXI_ARLOCK), 
       .s_axi_arcache                  (M00_AXI_ARCACHE),
       .s_axi_arprot                   (M00_AXI_ARPROT), 
       .s_axi_arqos                    (4'h0),              
       .s_axi_arvalid                  (M00_AXI_ARVALID),
       .s_axi_arready                  (M00_AXI_ARREADY),
// Slave Interface Read Data Ports     
       .s_axi_rid                      (M00_AXI_RID),    
       .s_axi_rdata                    (M00_AXI_RDATA),  
       .s_axi_rresp                    (M00_AXI_RRESP),  
       .s_axi_rlast                    (M00_AXI_RLAST),  
       .s_axi_rvalid                   (M00_AXI_RVALID), 
       .s_axi_rready                   (M00_AXI_RREADY),  
      
       
// System Clock Ports
       .sys_clk_p                       (sys_clk_p),
       .sys_clk_n                       (sys_clk_n),
// Reference Clock Ports
      
       .sys_rst                        (sys_rst)
       );
// End of User Design top instance
`endif

`endif


endmodule
