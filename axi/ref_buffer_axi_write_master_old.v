`timescale 1ns / 1ps
module ref_buffer_axi_write_master(
    clk,
    reset,
    //luma sao fifo interface
    y_sao_fifo_is_full_out      ,
    y_sao_fifo_data_in          ,
    y_sao_fifo_wr_en_in         ,
    
    
    cb_sao_fifo_is_full_out     ,
    cb_sao_fifo_data_in         ,   
    cb_sao_fifo_wr_en_in        ,
    
    
    cr_sao_fifo_is_full_out     ,
    cr_sao_fifo_data_in         ,
    cr_sao_fifo_wr_en_in        ,
    
    current_pic_dpb_base_addr_in      ,
    
    pixel_write_axi_awid        ,     
    pixel_write_axi_awlen       ,    
    pixel_write_axi_awsize      ,   
    pixel_write_axi_awburst     ,  
    pixel_write_axi_awlock      ,   
    pixel_write_axi_awcache     ,  
    pixel_write_axi_awprot      ,   
    pixel_write_axi_awvalid     ,
    pixel_write_axi_awaddr      ,
    pixel_write_axi_awready     ,
    pixel_write_axi_wstrb       ,
    pixel_write_axi_wlast       ,
    pixel_write_axi_wvalid      ,
    pixel_write_axi_wdata       ,
    pixel_write_axi_wready      ,
    pixel_write_axi_bid         ,
    pixel_write_axi_bresp       ,
    pixel_write_axi_bvalid      ,
    pixel_write_axi_bready       
    
    
);
//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
`include "../sim/pred_def.v"
`include "../sim/inter_axi_def.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                   MV_COL_AXI_AX_SIZE  = `AX_SIZE_64;
    parameter                   MV_COL_AXI_AX_LEN   = `AX_LEN_2;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam  STATE_AXI_WRITE_ADDRESS_SEND = 1;
    localparam  STATE_AXI_WRITE_ADDRESS_SEND_WAIT = 2;
    localparam  STATE_AXI_WRITE_WAIT = 3;
    localparam  STATE_AXI_WRITE_RESP = 4;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    
    input clk;
    input reset;
    //luma sao fifo interface
    output                                                                        y_sao_fifo_is_full_out;
    input   [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE + 9 + 9 - 1:0]       y_sao_fifo_data_in;
    input                                                                         y_sao_fifo_wr_en_in;
    
    //cb sao fifo interface
    output                                                                         cb_sao_fifo_is_full_out;
    input    [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]               cb_sao_fifo_data_in;
    input                                                                          cb_sao_fifo_wr_en_in;
    
    //cr sao fifo interface
    output                                                                         cr_sao_fifo_is_full_out;
    input    [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]               cr_sao_fifo_data_in;
    input                                                                          cr_sao_fifo_wr_en_in;

    input         [DPB_REF_PIC_ADDR_WIDTH -1: 0]           current_pic_dpb_base_addr_in;
    
    output                                      pixel_write_axi_awid    ;// = 0;
    output      [7:0]                           pixel_write_axi_awlen   ;// = MV_COL_AXI_AX_LEN;
    output      [2:0]                           pixel_write_axi_awsize  ;// = MV_COL_AXI_AX_SIZE;
    output      [1:0]                           pixel_write_axi_awburst ;// = `AX_BURST_INC;
    output                        	            pixel_write_axi_awlock  ;// = `AX_LOCK_DEFAULT;
    output      [3:0]                           pixel_write_axi_awcache ;// = `AX_CACHE_DEFAULT;
    output      [2:0]                           pixel_write_axi_awprot  ;// = `AX_PROT_DATA;
    output reg                                  pixel_write_axi_awvalid;
    output reg  [AXI_ADDR_WDTH-1:0]             pixel_write_axi_awaddr;
    input                       	            pixel_write_axi_awready;

    // write data channel
    output reg      [AXI_CACHE_DATA_WDTH/8-1:0]    pixel_write_axi_wstrb;
    output reg                                 pixel_write_axi_wlast;
    output reg                                 pixel_write_axi_wvalid;
    output reg     [AXI_CACHE_DATA_WDTH -1:0]     pixel_write_axi_wdata;

    input	                                    pixel_write_axi_wready;

    //write response channel
    input                       	            pixel_write_axi_bid;
    input       [1:0]                           pixel_write_axi_bresp;
    input                       	            pixel_write_axi_bvalid;
    output  reg                                 pixel_write_axi_bready;  
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    //ref buf fifo read interface
    wire                                                                       y_ref_buf_fifo_is_empty;
    wire [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE + 9 + 9 - 1:0]       y_ref_buf_fifo_data_out;
    reg                                                                        y_ref_buf_fifo_rd_en;
    
    wire                                                                         cb_ref_buf_fifo_is_empty;
    wire   [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]               cb_ref_buf_fifo_data_out;
    reg                                                                          cb_ref_buf_fifo_rd_en;
    
    wire                                                                        cr_ref_buf_fifo_is_empty;
    wire  [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0]               cr_ref_buf_fifo_data_out;
    reg                                                                         cr_ref_buf_fifo_rd_en;
    
    reg [31:0]                         state_axi_write;
    reg [7:0] burst_counter;
    
    wire        [X11_ADDR_WDTH- LOG2_MIN_TU_SIZE - 1:0]                                y_4x4;
    wire        [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                                x_4x4;
    
    wire        [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                                y_4x4_in;
    wire        [X11_ADDR_WDTH - LOG2_MIN_TU_SIZE - 1:0]                                x_4x4_in;
    
    wire        [CTB_SIZE_WIDTH - LOG2_MIN_TU_SIZE - 2:0]                                 y_8x8_in_ctu;
    wire        [CTB_SIZE_WIDTH - LOG2_MIN_TU_SIZE - 2:0]                                 x_8x8_in_ctu;
    wire        [X11_ADDR_WDTH - CTB_SIZE_WIDTH - 1:0]                                x_ctu;
    wire        [X11_ADDR_WDTH - CTB_SIZE_WIDTH - 1:0]                                y_ctu;
    
    reg [PIXEL_WIDTH*LUMA_REF_BLOCK_SIZE*LUMA_REF_BLOCK_SIZE-1 : 0]   y_ref_buf_fifo_data_out_temp;
    reg [PIXEL_WIDTH*CHMA_REF_BLOCK_SIZE*CHMA_REF_BLOCK_SIZE-1 : 0]  cb_ref_buf_fifo_data_out_temp;   
    reg [PIXEL_WIDTH*CHMA_REF_BLOCK_SIZE*CHMA_REF_BLOCK_SIZE-1 : 0]  cr_ref_buf_fifo_data_out_temp;
	reg         [DPB_REF_PIC_ADDR_WIDTH -1: 0]           current_pic_dpb_base_addr_d;
	reg         [DPB_REF_PIC_ADDR_WIDTH -1: 0]           current_pic_dpb_base_addr_use;
	reg [PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE - 1:0] luma_4x4_pixel_reg;
	
	
	
	
    // synthesis translate_off
    wire y_sao_fifo_wr_en;
    reg [1:0] write_counter;
    integer final_pixl_file;
    integer frame_offset_pred_pixel;
	  integer frame_offset ;
    wire [11:0] PIC_HEIGHT;// = 240;
    wire [11:0] PIC_WIDTH; //= 416;  
    
	
	
    assign PIC_HEIGHT = uut.pred_block.inter_top_block.pic_height;
    assign PIC_WIDTH = uut.pred_block.inter_top_block.pic_width;
    
    

    
    
    initial begin
        final_pixl_file = $fopen("BQSquare_416x240_60_qp37_416x240_8bit_final.yuv","rb");
    end
    
    always@(posedge clk) begin
        if(reset) begin
            write_counter <= 0;
        end
        else begin
            if(y_sao_fifo_wr_en_in) 
            write_counter <= write_counter + 1'b1;
        end
    end     
    assign y_sao_fifo_wr_en = y_sao_fifo_wr_en_in & write_counter == 0;
    
    
    task fill_ref_buf_fifo_temp;
        
        integer cb_offset;
        integer cr_offset;
        integer i,j;
        integer bit_idx;
        reg [7:0] temp_pixel;
        integer rfile;
        begin
			if( (y_4x4>>1) == 0 && (x_4x4>>1) ==0)begin
                frame_offset = uut.pred_block.current__poc*PIC_WIDTH*PIC_HEIGHT*3/2;
            end
            cb_offset = frame_offset + PIC_WIDTH*PIC_HEIGHT;
            cr_offset = cb_offset + PIC_WIDTH*PIC_HEIGHT/4;
            for(j=0;j<8;j=j+1)begin
                for(i=0;i<8;i=i+1)begin
                    rfile = $fseek(final_pixl_file,(frame_offset + (PIC_WIDTH)*(j + (y_4x4*4)) + (x_4x4*4) + i),0);
                    temp_pixel = $fgetc(final_pixl_file);
                    for(bit_idx=0;bit_idx<8;bit_idx = bit_idx + 1) begin
                        y_ref_buf_fifo_data_out_temp[((j)*8+i)*8+bit_idx] = temp_pixel[bit_idx];
                    end
                end
            end
            
            for(j=0;j<4;j=j+1)begin
                for(i=0;i<4;i=i+1)begin
                    rfile = $fseek(final_pixl_file,(cb_offset + (PIC_WIDTH/2)*(j + (y_4x4*2)) + (x_4x4*2) + i),0);
                    temp_pixel = $fgetc(final_pixl_file);
                    for(bit_idx=0;bit_idx<8;bit_idx = bit_idx + 1) begin
                        cb_ref_buf_fifo_data_out_temp[((j)*4+i)*8+bit_idx] = temp_pixel[bit_idx];
                    end
                end
            end
            
            for(j=0;j<4;j=j+1)begin
                for(i=0;i<4;i=i+1)begin
                    rfile = $fseek(final_pixl_file,(cr_offset + (PIC_WIDTH/2)*(j + (y_4x4*2)) + (x_4x4*2) + i),0);
                    //$display("offset %d",(cr_offset + (PIC_WIDTH/2)*(j + (y_4x4*2)) + (x_4x4*2) + i));
                    temp_pixel = $fgetc(final_pixl_file);
                    for(bit_idx=0;bit_idx<8;bit_idx = bit_idx + 1) begin
                        cr_ref_buf_fifo_data_out_temp[((j)*4+i)*8+bit_idx] = temp_pixel[bit_idx];
                    end
                end
            end
        end
    endtask
    // synthesis translate_on
    

    
    
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------
	 assign                 pixel_write_axi_awid     = 0;
	 assign                 pixel_write_axi_awlen    = MV_COL_AXI_AX_LEN;
	 assign                 pixel_write_axi_awsize   = MV_COL_AXI_AX_SIZE;
	 assign                 pixel_write_axi_awburst  = `AX_BURST_INC;
	 assign    	            pixel_write_axi_awlock   = `AX_LOCK_DEFAULT;
	 assign                 pixel_write_axi_awcache  = `AX_CACHE_DEFAULT;
	 assign                 pixel_write_axi_awprot   = `AX_PROT_DATA;    
     // assign                 pixel_write_axi_wstrb = 64'hFFFF_FFFF;       // 64 bytes
	 
	 
	 always@(posedge clk) begin
		if(y_4x4 == 0 && x_4x4 == 0 && y_ref_buf_fifo_is_empty==0) begin
			current_pic_dpb_base_addr_d <= current_pic_dpb_base_addr_in;
		end
	 end
	 
	 always@(*) begin
		if(y_4x4==0 && x_4x4==0) begin
			current_pic_dpb_base_addr_use  = current_pic_dpb_base_addr_in;
		end
		else begin
			current_pic_dpb_base_addr_use = current_pic_dpb_base_addr_d;
		end
	 end

	 



assign {y_4x4,x_4x4} = y_ref_buf_fifo_data_out[PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE + 9 + 9 -1:PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE]    ;
assign {y_4x4_in,x_4x4_in} = y_sao_fifo_data_in[PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE + 9 + 9 -1:PIXEL_WIDTH*OUTPUT_BLOCK_SIZE*OUTPUT_BLOCK_SIZE]    ;

assign y_ctu = y_4x4[X11_ADDR_WDTH - LOG2_MIN_TU_SIZE -1: CTB_SIZE_WIDTH - LOG2_MIN_TU_SIZE ];
assign x_ctu = x_4x4[X11_ADDR_WDTH - LOG2_MIN_TU_SIZE -1: CTB_SIZE_WIDTH - LOG2_MIN_TU_SIZE ];

assign        y_8x8_in_ctu = y_4x4[CTB_SIZE_WIDTH - LOG2_MIN_TU_SIZE -1:1];
assign        x_8x8_in_ctu = x_4x4[CTB_SIZE_WIDTH - LOG2_MIN_TU_SIZE -1:1];    



    always@(*) begin
         y_ref_buf_fifo_rd_en = 0;
        cb_ref_buf_fifo_rd_en = 0;
        cr_ref_buf_fifo_rd_en = 0;
        case(state_axi_write)
            STATE_AXI_WRITE_ADDRESS_SEND_WAIT: begin
                if(pixel_write_axi_awready) begin
                    y_ref_buf_fifo_rd_en = 1;
                end
            end
            STATE_AXI_WRITE_WAIT: begin
                if(pixel_write_axi_wready) begin
                    cb_ref_buf_fifo_rd_en = 1;
                    cr_ref_buf_fifo_rd_en = 1;
                end
            end
        endcase
    end
    
    always@(posedge clk) begin
        if (reset) begin
            state_axi_write <= STATE_AXI_WRITE_ADDRESS_SEND;
            pixel_write_axi_awvalid <= 0;
            pixel_write_axi_wvalid <= 0;
            pixel_write_axi_bready <= 0;
            burst_counter <= 0;
            pixel_write_axi_wstrb <= 64'h0000_0000_0000_0000;
        end
        else begin
            case(state_axi_write)
                STATE_AXI_WRITE_ADDRESS_SEND: begin
                    if(y_ref_buf_fifo_is_empty ==0 && cr_ref_buf_fifo_is_empty ==0 && cb_ref_buf_fifo_is_empty ==0) begin
						pixel_write_axi_awaddr <= (current_pic_dpb_base_addr_use + `REF_PIX_IU_ROW_OFFSET* y_ctu+ `REF_PIX_IU_OFFSET * x_ctu +  y_8x8_in_ctu *`REF_PIX_BU_ROW_OFFSET + x_8x8_in_ctu* `REF_PIX_BU_OFFSET);//(axi_ctu_row_offset + `REF_PIX_IU_OFFSET * x_ctb_addr);//%(1<<(31));
                        pixel_write_axi_awvalid <= 1;
                        state_axi_write <= STATE_AXI_WRITE_ADDRESS_SEND_WAIT;
                        fill_ref_buf_fifo_temp();
                    end  
                    else begin
                        pixel_write_axi_awvalid <= 0;
                    end
                    pixel_write_axi_bready <= 0;
                    pixel_write_axi_wvalid <= 0;
                end
                STATE_AXI_WRITE_ADDRESS_SEND_WAIT: begin
                    if(pixel_write_axi_awready) begin
                        pixel_write_axi_awvalid <= 0;
                        pixel_write_axi_wlast <= 0;
                        burst_counter <= 0;
                        //pixel_write_axi_wdata <= y_ref_buf_fifo_data_out_temp;
                        pixel_write_axi_wdata <= {cr_ref_buf_fifo_data_out_temp[PIXEL_WIDTH*CHMA_REF_BLOCK_SIZE*CHMA_REF_BLOCK_SIZE/2-1:0],cb_ref_buf_fifo_data_out_temp[PIXEL_WIDTH*CHMA_REF_BLOCK_SIZE*CHMA_REF_BLOCK_SIZE/2-1:0],y_ref_buf_fifo_data_out_temp[PIXEL_WIDTH*LUMA_REF_BLOCK_SIZE*LUMA_REF_BLOCK_SIZE/2-1:0]};
                        pixel_write_axi_wstrb <= 64'hFFFF_FFFF_FFFF_FFFF;
                        pixel_write_axi_wvalid <= 1;
                        state_axi_write <= STATE_AXI_WRITE_WAIT;
                    end
                end
                STATE_AXI_WRITE_WAIT: begin
                    if(pixel_write_axi_wready) begin
                        //pixel_write_axi_wdata <= {cr_ref_buf_fifo_data_out_temp,cb_ref_buf_fifo_data_out_temp};
                        pixel_write_axi_wdata <= {cr_ref_buf_fifo_data_out_temp[PIXEL_WIDTH*CHMA_REF_BLOCK_SIZE*CHMA_REF_BLOCK_SIZE-1:PIXEL_WIDTH*CHMA_REF_BLOCK_SIZE*CHMA_REF_BLOCK_SIZE/2],cb_ref_buf_fifo_data_out_temp[PIXEL_WIDTH*CHMA_REF_BLOCK_SIZE*CHMA_REF_BLOCK_SIZE-1:PIXEL_WIDTH*CHMA_REF_BLOCK_SIZE*CHMA_REF_BLOCK_SIZE/2],y_ref_buf_fifo_data_out_temp[PIXEL_WIDTH*LUMA_REF_BLOCK_SIZE*LUMA_REF_BLOCK_SIZE-1:PIXEL_WIDTH*LUMA_REF_BLOCK_SIZE*LUMA_REF_BLOCK_SIZE/2]};
                        pixel_write_axi_wstrb <= 64'h0000_FFFF_FFFF_FFFF;
                        pixel_write_axi_wlast <= 1;
                        pixel_write_axi_bready <= 1;
                        state_axi_write <= STATE_AXI_WRITE_RESP;
                    end
                end
                STATE_AXI_WRITE_RESP: begin
                    if(pixel_write_axi_wready) begin
                        pixel_write_axi_wvalid <= 0;
                    end
                    if(pixel_write_axi_bvalid) begin
                        if(pixel_write_axi_bresp == `XRESP_SLAV_ERROR || pixel_write_axi_bid !=0) begin
                            state_axi_write <= STATE_AXI_WRITE_ADDRESS_SEND_WAIT;
                            pixel_write_axi_awvalid <= 1;            
                        end
                        else begin
                            pixel_write_axi_bready <= 0;
                            state_axi_write <= STATE_AXI_WRITE_ADDRESS_SEND;
                        end
                    end
                end
                
            endcase
        end
    end
    
    

endmodule