// (c) Copyright 1995-2014 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:user:axi_displaybuffer_handle:1.14
// IP Revision: 53

(* X_CORE_INFO = "axi_displaybuffer_handle,Vivado 2014.2" *)
(* CHECK_LICENSE_TYPE = "zynq_design_1_axi_displaybuffer_handle_0_1,axi_displaybuffer_handle,{}" *)
(* CORE_GENERATION_INFO = "zynq_design_1_axi_displaybuffer_handle_0_1,axi_displaybuffer_handle,{x_ipProduct=Vivado 2014.2,x_ipVendor=xilinx.com,x_ipLibrary=user,x_ipName=axi_displaybuffer_handle,x_ipVersion=1.14,x_ipCoreRevision=53,x_ipLanguage=VERILOG,MAX_PIC_WIDTH=2048,MAX_PIC_HEIGHT=2048,CTB_SIZE_WIDTH=6,CB_SIZE_WIDTH=7,TB_SIZE_WIDTH=6,LOG2_CTB_WIDTH=3,PIC_DIM_WIDTH=11,LOG2_MIN_COLLOCATE_SIZE=4,DBF_SAMPLE_XY_ADDR=3,RPS_HEADER_ADDR_WIDTH=6,NUM_NEG_POS_POC_WIDTH=4,RPS_HEADER_DATA_WIDTH=12,RPS_ENTRY_DATA_WIDTH=17,RPS_ENTRY_ADDR_WIDTH=10,RPS_HEADER_NUM_POS_POC_RANGE_HIGH=11,RPS_HEADER_NUM_POS_POC_RANGE_LOW=8,RPS_HEADER_NUM_NEG_POC_RANGE_HIGH=7,RPS_HEADER_NUM_NEG_POC_RANGE_LOW=4,RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH=3,RPS_HEADER_NUM_DELTA_POC_RANGE_LOW=0,RPS_ENTRY_USED_FLAG_RANGE_HIGH=16,RPS_ENTRY_USED_FLAG_RANGE_LOW=16,RPS_ENTRY_DELTA_POC_RANGE_HIGH=15,RPS_ENTRY_DELTA_POC_RANGE_LOW=0,RPS_ENTRY_DELTA_POC_MSB=15,BS_FIFO_WIDTH=8,X_ADDR_WDTH=12,X11_ADDR_WDTH=11,Y_ADDR_WDTH=12,Y11_ADDR_WDTH=11,LOG2_MIN_PU_SIZE=2,LOG2_MIN_TU_SIZE=2,LOG2_MIN_DU_SIZE=3,AVAILABLE_CONFIG_BUS_WIDTH=2,POC_WIDTH=32,DPB_FRAME_OFFSET_WIDTH=4,DPB_STATUS_WIDTH=3,DPB_FILLED_WIDTH=1,DPB_DATA_WIDTH=32,REF_PIC_LIST_POC_DATA_WIDTH=32,DPB_REF_PIC_ADDR_WIDTH=28,DPB_ADDR_WIDTH=4,REF_PIC_LIST_ADDR_WIDTH=4,REF_PIC_LIST_DATA_WIDTH=36,REF_POC_LIST5_ADDR_WIDTH=4,REF_POC_LIST5_DATA_WIDTH=19,INTER_TOP_CONFIG_BUS_MODE_WIDTH=5,INTER_TOP_CONFIG_BUS_WIDTH=32,MV_FIELD_DATA_WIDTH=74,MV_COL_AXI_DATA_WIDTH=512,DPB_POC_RANGE_HIGH=31,DPB_POC_RANGE_LOW=0,REF_PIC_LIST5_POC_RANGE_HIGH=15,REF_PIC_LIST5_POC_RANGE_LOW=0,REF_PIC_LIST5_DPB_STATE_HIGH=18,REF_PIC_LIST5_DPB_STATE_LOW=16,MERGE_CAND_TYPE_WIDTH=3,MAX_NUM_MERGE_CAND_CONST=5,AMVP_NUM_CAND_CONST=2,NUM_BI_PRED_CANDS=12,TX_WIDTH=16,DIST_SCALE_WIDTH=13,NUM_BI_PRED_CANDS_TYPES=4,SAO_OUT_FIFO_WIDTH=784,FILTER_PIXEL_WIDTH=16,NUM_IN_TO_8_FILTER=8,MAX_MV_PER_CU=4,STEP_8x8_IN_PUS=2,MV_L_FRAC_WIDTH_HIGH=2,MV_L_INT_WIDTH_LOW=2,MV_C_FRAC_WIDTH_HIGH=3,MV_C_INT_WIDTH_LOW=3,MV_FIELD_AXI_DATA_WIDTH=80,AXI_ADDR_WDTH=32,AXI_CACHE_DATA_WDTH=512,AXI_MIG_DATA_WIDTH=512,PROFILE=4,AXI_BRESP_WIDTH=2}" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module zynq_design_1_axi_displaybuffer_handle_0_1 (
  clk,
  reset,
  displaybuffer_fifo_read_en_out,
  displaybuffer_fifo_empty_in,
  displaybuffer_fifo_almost_empty_in,
  displaybuffer_fifo_data_in,
  log2_ctu_size_in,
  poc_4bits_in,
  pic_width_in,
  pic_height_in,
  config_valid_in,
  axi_clk,
  axi_awaddr,
  axi_awvalid,
  axi_awready,
  axi_awlen,
  axi_awid,
  axi_awburst,
  axi_awsize,
  axi_wid,
  axi_wdata,
  axi_wlast,
  axi_wvalid,
  axi_wready,
  axi_wstrb,
  axi_bresp,
  axi_bvalid,
  axi_bready,
  axi_araddr,
  axi_arvalid,
  axi_arready,
  axi_arlen,
  axi_arburst,
  axi_arsize,
  axi_arid,
  axi_rdata,
  axi_rresp,
  axi_rlast,
  axi_rready,
  axi_rvalid,
  hdmi_fifo_almost_full,
  hdmi_fifo_data_out,
  hdmi_fifo_w_en
);

(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 axi_signal_clock CLK, xilinx.com:signal:clock:1.0 signal_clock CLK" *)
input wire clk;
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 signal_reset RST" *)
input wire reset;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 fifo_read_rtl_1 RD_EN" *)
output wire displaybuffer_fifo_read_en_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 fifo_read_rtl_1 EMPTY" *)
input wire displaybuffer_fifo_empty_in;
input wire displaybuffer_fifo_almost_empty_in;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 fifo_read_rtl_1 RD_DATA" *)
input wire [783 : 0] displaybuffer_fifo_data_in;
input wire [2 : 0] log2_ctu_size_in;
input wire [3 : 0] poc_4bits_in;
input wire [10 : 0] pic_width_in;
input wire [10 : 0] pic_height_in;
input wire config_valid_in;
input wire axi_clk;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWADDR" *)
output wire [31 : 0] axi_awaddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWVALID" *)
output wire axi_awvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWREADY" *)
input wire axi_awready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWLEN" *)
output wire [7 : 0] axi_awlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWID" *)
output wire [3 : 0] axi_awid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWBURST" *)
output wire [1 : 0] axi_awburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi AWSIZE" *)
output wire [2 : 0] axi_awsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WID" *)
output wire [0 : 0] axi_wid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WDATA" *)
output wire [511 : 0] axi_wdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WLAST" *)
output wire axi_wlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WVALID" *)
output wire axi_wvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WREADY" *)
input wire axi_wready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi WSTRB" *)
output wire [63 : 0] axi_wstrb;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi BRESP" *)
input wire [1 : 0] axi_bresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi BVALID" *)
input wire axi_bvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi BREADY" *)
output wire axi_bready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARADDR" *)
output wire [31 : 0] axi_araddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARVALID" *)
output wire axi_arvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARREADY" *)
input wire axi_arready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARLEN" *)
output wire [7 : 0] axi_arlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARBURST" *)
output wire [1 : 0] axi_arburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARSIZE" *)
output wire [2 : 0] axi_arsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi ARID" *)
output wire [3 : 0] axi_arid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RDATA" *)
input wire [511 : 0] axi_rdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RRESP" *)
input wire [1 : 0] axi_rresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RLAST" *)
input wire axi_rlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RREADY" *)
output wire axi_rready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 axi RVALID" *)
input wire axi_rvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 hdmi_fifo ALMOST_FULL" *)
input wire hdmi_fifo_almost_full;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 hdmi_fifo WR_DATA" *)
output wire [63 : 0] hdmi_fifo_data_out;
(* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 hdmi_fifo WR_EN" *)
output wire hdmi_fifo_w_en;

  axi_displaybuffer_handle #(
    .MAX_PIC_WIDTH(2048),
    .MAX_PIC_HEIGHT(2048),
    .CTB_SIZE_WIDTH(6),
    .CB_SIZE_WIDTH(7),
    .TB_SIZE_WIDTH(6),
    .LOG2_CTB_WIDTH(3),
    .PIC_DIM_WIDTH(11),
    .LOG2_MIN_COLLOCATE_SIZE(4),
    .DBF_SAMPLE_XY_ADDR(3),
    .RPS_HEADER_ADDR_WIDTH(6),
    .NUM_NEG_POS_POC_WIDTH(4),
    .RPS_HEADER_DATA_WIDTH(12),
    .RPS_ENTRY_DATA_WIDTH(17),
    .RPS_ENTRY_ADDR_WIDTH(10),
    .RPS_HEADER_NUM_POS_POC_RANGE_HIGH(11),
    .RPS_HEADER_NUM_POS_POC_RANGE_LOW(8),
    .RPS_HEADER_NUM_NEG_POC_RANGE_HIGH(7),
    .RPS_HEADER_NUM_NEG_POC_RANGE_LOW(4),
    .RPS_HEADER_NUM_DELTA_POC_RANGE_HIGH(3),
    .RPS_HEADER_NUM_DELTA_POC_RANGE_LOW(0),
    .RPS_ENTRY_USED_FLAG_RANGE_HIGH(16),
    .RPS_ENTRY_USED_FLAG_RANGE_LOW(16),
    .RPS_ENTRY_DELTA_POC_RANGE_HIGH(15),
    .RPS_ENTRY_DELTA_POC_RANGE_LOW(0),
    .RPS_ENTRY_DELTA_POC_MSB(15),
    .BS_FIFO_WIDTH(8),
    .X_ADDR_WDTH(12),
    .X11_ADDR_WDTH(11),
    .Y_ADDR_WDTH(12),
    .Y11_ADDR_WDTH(11),
    .LOG2_MIN_PU_SIZE(2),
    .LOG2_MIN_TU_SIZE(2),
    .LOG2_MIN_DU_SIZE(3),
    .AVAILABLE_CONFIG_BUS_WIDTH(2),
    .POC_WIDTH(32),
    .DPB_FRAME_OFFSET_WIDTH(4),
    .DPB_STATUS_WIDTH(3),
    .DPB_FILLED_WIDTH(1),
    .DPB_DATA_WIDTH(32),
    .REF_PIC_LIST_POC_DATA_WIDTH(32),
    .DPB_REF_PIC_ADDR_WIDTH(28),
    .DPB_ADDR_WIDTH(4),
    .REF_PIC_LIST_ADDR_WIDTH(4),
    .REF_PIC_LIST_DATA_WIDTH(36),
    .REF_POC_LIST5_ADDR_WIDTH(4),
    .REF_POC_LIST5_DATA_WIDTH(19),
    .INTER_TOP_CONFIG_BUS_MODE_WIDTH(5),
    .INTER_TOP_CONFIG_BUS_WIDTH(32),
    .MV_FIELD_DATA_WIDTH(74),
    .MV_COL_AXI_DATA_WIDTH(512),
    .DPB_POC_RANGE_HIGH(31),
    .DPB_POC_RANGE_LOW(0),
    .REF_PIC_LIST5_POC_RANGE_HIGH(15),
    .REF_PIC_LIST5_POC_RANGE_LOW(0),
    .REF_PIC_LIST5_DPB_STATE_HIGH(18),
    .REF_PIC_LIST5_DPB_STATE_LOW(16),
    .MERGE_CAND_TYPE_WIDTH(3),
    .MAX_NUM_MERGE_CAND_CONST(5),
    .AMVP_NUM_CAND_CONST(2),
    .NUM_BI_PRED_CANDS(12),
    .TX_WIDTH(16),
    .DIST_SCALE_WIDTH(13),
    .NUM_BI_PRED_CANDS_TYPES(4),
    .SAO_OUT_FIFO_WIDTH(784),
    .FILTER_PIXEL_WIDTH(16),
    .NUM_IN_TO_8_FILTER(8),
    .MAX_MV_PER_CU(4),
    .STEP_8x8_IN_PUS(2),
    .MV_L_FRAC_WIDTH_HIGH(2),
    .MV_L_INT_WIDTH_LOW(2),
    .MV_C_FRAC_WIDTH_HIGH(3),
    .MV_C_INT_WIDTH_LOW(3),
    .MV_FIELD_AXI_DATA_WIDTH(80),
    .AXI_ADDR_WDTH(32),
    .AXI_CACHE_DATA_WDTH(512),
    .AXI_MIG_DATA_WIDTH(512),
    .PROFILE(4),
    .AXI_BRESP_WIDTH(2)
  ) inst (
    .clk(clk),
    .reset(reset),
    .displaybuffer_fifo_read_en_out(displaybuffer_fifo_read_en_out),
    .displaybuffer_fifo_empty_in(displaybuffer_fifo_empty_in),
    .displaybuffer_fifo_almost_empty_in(displaybuffer_fifo_almost_empty_in),
    .displaybuffer_fifo_data_in(displaybuffer_fifo_data_in),
    .log2_ctu_size_in(log2_ctu_size_in),
    .poc_4bits_in(poc_4bits_in),
    .pic_width_in(pic_width_in),
    .pic_height_in(pic_height_in),
    .config_valid_in(config_valid_in),
    .axi_clk(axi_clk),
    .axi_awaddr(axi_awaddr),
    .axi_awvalid(axi_awvalid),
    .axi_awready(axi_awready),
    .axi_awlen(axi_awlen),
    .axi_awid(axi_awid),
    .axi_awburst(axi_awburst),
    .axi_awsize(axi_awsize),
    .axi_wid(axi_wid),
    .axi_wdata(axi_wdata),
    .axi_wlast(axi_wlast),
    .axi_wvalid(axi_wvalid),
    .axi_wready(axi_wready),
    .axi_wstrb(axi_wstrb),
    .axi_bresp(axi_bresp),
    .axi_bvalid(axi_bvalid),
    .axi_bready(axi_bready),
    .axi_araddr(axi_araddr),
    .axi_arvalid(axi_arvalid),
    .axi_arready(axi_arready),
    .axi_arlen(axi_arlen),
    .axi_arburst(axi_arburst),
    .axi_arsize(axi_arsize),
    .axi_arid(axi_arid),
    .axi_rdata(axi_rdata),
    .axi_rresp(axi_rresp),
    .axi_rlast(axi_rlast),
    .axi_rready(axi_rready),
    .axi_rvalid(axi_rvalid),
    .hdmi_fifo_almost_full(hdmi_fifo_almost_full),
    .hdmi_fifo_data_out(hdmi_fifo_data_out),
    .hdmi_fifo_w_en(hdmi_fifo_w_en)
  );
endmodule
