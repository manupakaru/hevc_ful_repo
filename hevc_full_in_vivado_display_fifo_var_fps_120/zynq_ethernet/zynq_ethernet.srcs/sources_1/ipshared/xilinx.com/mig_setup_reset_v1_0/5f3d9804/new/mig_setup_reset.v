`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/06/2014 09:23:40 AM
// Design Name: 
// Module Name: mig_setup_reset
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mig_setup_reset(
    rst,
    clk,
    areset_n,
    init_calib_complete,
    up_stream_reset,
    design_reset,
    design_reset_n,
    mmcm_locked
    );
    
    input init_calib_complete;
    input rst;
    input clk;
    input mmcm_locked;
    
    output reg up_stream_reset;
    output reg design_reset;
    output design_reset_n;
    output reg areset_n;
    
    
    reg up_stream_reset_sync_reg;
    
    initial begin
        up_stream_reset = 1;
        up_stream_reset_sync_reg = 1;
        design_reset = 1;
         
    end
    
    assign design_reset_n = ~design_reset;
    
    always@(posedge clk) begin
        areset_n <= ~rst;
        up_stream_reset <= up_stream_reset_sync_reg;
    end
    
    always@(posedge clk) begin
        if(mmcm_locked & init_calib_complete & !rst) begin
            design_reset <= 0;
            up_stream_reset_sync_reg <= 0;
        end
        else begin
            design_reset <= 1;
            up_stream_reset_sync_reg <= 1;
        end
    end
endmodule
